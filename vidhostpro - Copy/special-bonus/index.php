<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <!-- Tell the browser to be responsive to screen width -->
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
      <meta name="title" content="VidHostPro Bonuses">
      <meta name="description" content="VidHostPro Bonuses">
      <meta name="keywords" content="Revealing A BLAZING-FAST Video Hosting, Player & Marketing Technology With No Monthly Fee Ever">
      <meta property="og:image" content="https://www.vidhostpro.co/special-bonus/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Dr. Amit Pareek">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="VidHostPro Bonuses">
      <meta property="og:description" content="Revealing A BLAZING-FAST Video Hosting, Player & Marketing Technology With No Monthly Fee Ever">
      <meta property="og:image" content="https://www.vidhostpro.co/special-bonus/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="VidHostPro Bonuses">
      <meta property="twitter:description" content="Revealing A BLAZING-FAST Video Hosting, Player & Marketing Technology With No Monthly Fee Ever">
      <meta property="twitter:image" content="https://www.vidhostpro.co/special-bonus/thumbnail.png">
      <title>VidHostPro Bonuses</title>
      <!-- Shortcut Icon  -->
      <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
      <!-- Css CDN Load Link -->
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <link rel="stylesheet" href="../common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" type="text/css" href="assets/css/bonus-style.css">
      <script src="../common_assets/js/jquery.min.js"></script>
      <!-- Google Tag Manager -->
      <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
         new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
         j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
         'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
         })(window,document,'script','dataLayer','GTM-NMK2MBB');
      </script>
      <!-- End Google Tag Manager -->
   </head>
   <body>
      <!-- Google Tag Manager (noscript) -->
      <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NMK2MBB"
         height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
      <!-- End Google Tag Manager (noscript) --> 
      <!-- New Timer  Start-->
      <?php
         $date = 'Nov 27 2023 11:59 PM EST';
         $exp_date = strtotime($date);
         $now = time();  
         /*
         
         $date = date('F d Y g:i:s A eO');
         $rand_time_add = rand(700, 1200);
         $exp_date = strtotime($date) + $rand_time_add;
         $now = time();*/
         
         if ($now < $exp_date) {
         ?>
      <?php
         } else {
         	echo "Times Up";
         }
         ?>
      <!-- New Timer End -->
      <?php
         if(!isset($_GET['afflink'])){
         $_GET['afflink'] = 'https://vidhostpro.co/special/';
         $_GET['name'] = 'Dr. Amit Pareek';      
         }
         else {
             
             $_GET['afflinkbundle'] = 'https://vidhostpro.co/bundle/'.$_GET['afflink'];
             $fe_id_array=explode("/",$_GET['afflink']); 
             $bundle_id="394877";
             $_GET['afflinkbundle']=str_replace($fe_id_array[5],$bundle_id,$_GET['afflink']);
         }
         ?>
          <?php
         if(!isset($_GET['afflinkbundle'])){
         $_GET['afflinkbundle'] = 'https://vidhostpro.co/bundle/';
         $_GET['name'] = 'Dr. Amit Pareek';      
         }
         ?>
      <div class="main-header">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12">
                  <img src="assets/images/sale.webp" class="img-fluid mx-auto d-block">
               </div>
               <div class="col-md-12 col-12 text-center mt20 mt-md30">
                  <div class="text-center">
                     <div class="f-md-32 f-20 lh140 w400 white-clr d-block d-md-flex align-items-center justify-content-center">
                        <span class="w600"><?php echo $_GET['name'];?>'s</span> &nbsp;special bonus for &nbsp;
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 829.07 133.22" style="max-height:55px">
                            <defs>
                            <style>.cls-1{fill:#fff;}.cls-2{fill:#ec1c24;}</style>
                            </defs>
                            <g id="Layer_2" data-name="Layer 2">
                            <g id="Layer_1-2" data-name="Layer 1">
                            <path class="cls-1" d="M142.09,14.94C89.5-8.93,68.3,32,65.87,37,48.19,38.59,36.52,52.3,32.31,67.64l-1.55-.54c-2.87-1-7-2.8-9.65-5.12a51.82,51.82,0,0,1,6.14-12h0a49.93,49.93,0,0,1,3.41-4.41c9.9-11.41,22.44-15.6,26.3-15.6C72.85.51,95.07,0,99.08,0,126.45-.2,142.09,14.94,142.09,14.94Z"></path>
                            <path class="cls-1" d="M103.29,128.84c-21.44,11.73-43.39-3.34-47.63-7.58-25.5-.6-35.11-20.89-37.43-32.69a121.37,121.37,0,0,0,14.3,4.56C36,103.65,44.31,111.7,58,112.66c23.66,18.51,45.7,7.08,45.7,7.08a40.15,40.15,0,0,0,23.47,3,56.14,56.14,0,0,0,22.25-10.11s11.38,1,18-6.81c3.34-3.95,6.36-7.52,8.22-12.5h0a114.47,114.47,0,0,0,13.44-4.5c-3.45,17.54-17.34,32.81-37.25,32.81-14,13.87-37.41,11.7-45.2,8.61"></path>
                            <path class="cls-1" d="M185.47,61.68a37.61,37.61,0,0,1-3.63,2.23,54,54,0,0,1-6.43,2.93c-6-14-24.8-19.71-24.8-19.71s-10.49-17.19-27.1-23.39c-16.26-6.08-41.44,1-42,1.1.49-.42,21.75-18.55,53.83-6,14.36,6.17,21.74,21.44,21.74,21.44a35.85,35.85,0,0,1,5,1.56,49.15,49.15,0,0,1,9.36,4.73h0A40.26,40.26,0,0,1,185.47,61.68Z"></path>
                            <path class="cls-2" d="M77,80.25V94c-17.82-1.23-33.48-3.67-46-7.14a100,100,0,0,1-13.4-4.67C6.24,77.22-.2,70.89,0,63.51,1.47,42.92,28.58,42.83,29,42.83c-.25.1-16.62,6.85-16.17,15.07.19,3.59,2.24,6.93,6.53,9.9a44.79,44.79,0,0,0,11.74,5.42C41.36,76.53,56.31,79,77,80.25Z"></path>
                            <path class="cls-2" d="M189.87,82.3A99,99,0,0,1,177.23,87c-11,3.29-25.29,5.89-43.6,7.12l11.78-7a8.27,8.27,0,0,0,4.06-7.16,7.84,7.84,0,0,0-.08-1.19c10.93-1.5,20.22-3.65,27.61-6.43a58.75,58.75,0,0,0,7.56-3.4c1.15-.63,2.21-1.26,3.18-1.9,16-10.49,6.87-21.31-22.83-28,36.12,7.83,43.62,16.49,44.7,22.28C210.57,66.47,205.7,75.09,189.87,82.3Z"></path>
                            <path class="cls-2" d="M142.5,82.2l-21,12.47-9.94,5.93-24.9,14.81a2.65,2.65,0,0,1-4-2.28V46.69a2.69,2.69,0,0,1,2-2.56l.68-.05.69,0,.68.29,50.33,30,5.48,3.27a2.66,2.66,0,0,1,0,4.57Z"></path>
                            <path class="cls-1" d="M267.45,89.27l18.83-62.49h19.06l-28.93,83.09H258.54L229.72,26.78h19Z"></path>
                            <path class="cls-1" d="M312,32.14a8.12,8.12,0,0,1,2.48-6.1,10.69,10.69,0,0,1,13.5,0,8.07,8.07,0,0,1,2.51,6.1,8.09,8.09,0,0,1-2.54,6.16,10.57,10.57,0,0,1-13.41,0A8.09,8.09,0,0,1,312,32.14Zm17.52,77.73H312.93V48.12h16.55Z"></path>
                            <path class="cls-1" d="M340.49,78.54q0-14.45,6.48-23T364.69,47a18.92,18.92,0,0,1,14.9,6.73V22.21h16.55v87.66h-14.9l-.8-6.56a19.41,19.41,0,0,1-15.86,7.7,20.94,20.94,0,0,1-17.49-8.59Q340.49,93.84,340.49,78.54ZM357,79.77q0,8.69,3,13.31a9.85,9.85,0,0,0,8.79,4.63q7.65,0,10.79-6.45V66.85q-3.09-6.46-10.67-6.46Q357,60.39,357,79.77Z"></path>
                            <path class="cls-1" d="M477.63,109.87H460.51V74.26H427.13v35.61H410V26.78h17.13V60.45h33.38V26.78h17.12Z"></path>
                            <path class="cls-1" d="M488.93,78.43a36.59,36.59,0,0,1,3.54-16.38,25.81,25.81,0,0,1,10.19-11.13A29.67,29.67,0,0,1,518.09,47q12.51,0,20.41,7.65t8.81,20.77l.12,4.22q0,14.22-7.93,22.8T518.21,111q-13.35,0-21.32-8.56t-8-23.28Zm16.5,1.17q0,8.79,3.31,13.46a11.9,11.9,0,0,0,18.83,0q3.36-4.59,3.37-14.71,0-8.64-3.37-13.38a11,11,0,0,0-9.48-4.74A10.75,10.75,0,0,0,508.74,65C506.53,68.14,505.43,73,505.43,79.6Z"></path>
                            <path class="cls-1" d="M591,92.81A5.32,5.32,0,0,0,588,88q-3-1.74-9.61-3.11-22-4.62-22-18.72a16.94,16.94,0,0,1,6.82-13.72Q570,47,581,47q11.76,0,18.81,5.54a17.45,17.45,0,0,1,7,14.38H590.4a8,8,0,0,0-2.28-5.85q-2.28-2.31-7.14-2.31a9.83,9.83,0,0,0-6.44,1.88,5.91,5.91,0,0,0-2.29,4.79,5.1,5.1,0,0,0,2.6,4.43q2.6,1.68,8.76,2.91A73,73,0,0,1,594,75.51q13.07,4.8,13.07,16.61,0,8.44-7.25,13.67T581.1,111a32.54,32.54,0,0,1-13.78-2.77,23,23,0,0,1-9.45-7.59,17.62,17.62,0,0,1-3.42-10.41h15.63A8.62,8.62,0,0,0,573.34,97a12.87,12.87,0,0,0,8.1,2.34q4.74,0,7.16-1.8A5.57,5.57,0,0,0,591,92.81Z"></path>
                            <path class="cls-1" d="M637.43,32.94V48.12H648v12.1H637.43V91c0,2.28.43,3.92,1.31,4.9s2.55,1.49,5,1.49a26,26,0,0,0,4.85-.4v12.5a33.89,33.89,0,0,1-10,1.48q-17.34,0-17.69-17.52V60.22h-9V48.12h9V32.94Z"></path>
                            <path class="cls-1" d="M675.38,80.59v29.28H658.26V26.78h32.41a37.58,37.58,0,0,1,16.47,3.42,25.29,25.29,0,0,1,10.93,9.73,27.23,27.23,0,0,1,3.82,14.35q0,12.22-8.36,19.27t-23.14,7Zm0-13.86h15.29q6.8,0,10.36-3.2t3.57-9.13a13.69,13.69,0,0,0-3.6-9.87q-3.6-3.78-9.93-3.88H675.38Z"></path>
                            <path class="cls-1" d="M767.26,63.59a44,44,0,0,0-5.94-.46q-9.36,0-12.27,6.33v40.41H732.56V48.12h15.58l.46,7.36q5-8.5,13.75-8.5a17.45,17.45,0,0,1,5.14.74Z"></path>
                            <path class="cls-1" d="M770.57,78.43a36.59,36.59,0,0,1,3.54-16.38,25.85,25.85,0,0,1,10.18-11.13A29.7,29.7,0,0,1,799.73,47q12.49,0,20.4,7.65T829,75.4l.12,4.22q0,14.22-7.94,22.8T799.85,111q-13.36,0-21.32-8.56t-8-23.28Zm16.49,1.17q0,8.79,3.31,13.46a11.91,11.91,0,0,0,18.84,0q3.36-4.59,3.36-14.71,0-8.64-3.36-13.38a11,11,0,0,0-9.48-4.74A10.78,10.78,0,0,0,790.37,65C788.17,68.14,787.06,73,787.06,79.6Z"></path>
                            </g>
                            </g>
                        </svg>
                     </div>
                  </div>
                  <div class="col-12 mt20 mt-md50 text-center">
                     <div class="preheadline f-18 f-md-22 w500 black-clr lh140">
                        Grab My 17 Exclusive Bonuses Before the Deal Ends… 
                     </div>
                  </div>
                  <div class="col-12 mt-md50 mt10 f-md-45 f-28 w500 text-center black-clr lh140 mainheadline">
                     Revealing A <span class="w700">BLAZING-FAST Video Hosting, Player & Marketing Technology</span> With No Monthly Fee Ever
                  </div>
                  <div class="col-12 mt20 mt-md50 f-md-22 f-18 w400 white-clr text-center lh150">
                     Check Out VidHostPro Webinar Replay In Action!
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md30">
               <div class="col-12 col-md-11 mx-auto">
                  <!-- <img src="assets/images/product-box.webp" class="img-fluid mx-auto d-block" alt="ProductBox"> -->
                  <div class="responsive-video">
                        <iframe src="https://vidhostpeo.oppyo.com/video/embed/vhp_webinar" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                    </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Header Section Start -->
      <div class="second-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row header-list-block">
                     <div class="col-12 col-md-6">
                        <ul class="features-list pl0 f-18 f-md-20 lh150 w400 black-clr text-capitalize">
                           <li> <span class="w600">Upload Unlimited Videos -</span> Sales, E-Learning, Training, Product Demo, Or ANY Video</li>
                           <li><span class="w600">Create Chapters in Video - </span> Analyze & Optimize Your Lead Generation and Traffic Strategies </li>
                           <li><span class="w600">Embed Playlist, Engage, & Deliver </span> Interactive Experiences For Your Customer's Website Visitors</li>
                           <li><span class="w600">Super-Fast Delivery (After All, Time Is Money!) </span> No Buffering. No Delay</li>
                           <li><span class="w600">Get Maximum Visitor Engagement- </span> No Traffic Leakage With Unwanted Ads Or Video Suggestions</li>
                           <li><span class="w600">Create 100% Mobile & SEO Optimized </span> Video Channels & Playlists</li>
                           <li><span class="w600">Stunning Promo & Social Ads Templates </span> For Monetization & Social Traffic</li>
                           <li>Fully Customizable Drag And Drop <span class="w600">WYSIWYG Video Ads Editor.</span></li>
                           <li><span class="w600">Intelligent Analytics </span> To Measure Video Performance</li>
                        </ul>
                     </div>
                     <div class="col-12 col-md-6 mt10 mt-md0">
                        <ul class="features-list pl0 f-18 f-md-20 lh150 w400 black-clr text-capitalize">
                           <li class="w600">Tap Into Fast-Growing $398 Billion E-Learning, Video Marketing & Video Agency Industry. </li>
                           <li> Revamp Your Playlists and Offer  <span class="w600">Exceptional Viewing Experience to Your Audience</span> </li>
                           <li><span class="w600"> Maximize Your Video Presentation</span> with A Customizable Player.</li>
                           <li> Manage All The Videos, Courses, & Clients Hassle-Free, <span class="w600">All-In-One Central Dashboard.</span>  </li>
                           <li>Robust Solution That Has <span class="w600">Served Over 69 Million Video Views For Customers</span>  </li>
                           <li>HLS Player- Optimized To <span class="w600">Work Beautifully With All Browsers, Devices & Page Builders</span>  </li>
                           <li><span class="w600">20+ Integrations </span> To Connect With Your Favorite Tools</li>
                           <li><span class="w600">Completely Cloud-Based </span> & Step-By-Step Video Training Included</li>
                           <li><span class="w600"> PLUS, YOU'LL RECEIVE - A COMMERCIAL LICENSE IF YOU BUY TODAY </span></li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black black-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-22 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                 
               </div>
               <div class="f-18 f-md-22 lh150 w400 text-center mt20 black-clr mt-md30">
                  Use Coupon Code <span class="w700 red-gradient">"BLACKFRIDAY"</span> for an Additional <span class="w700 red-gradient">30% Discount</span> on Bundle
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt10">
                  <a href="<?php echo $_GET['afflinkbundle']; ?>" class="text-center bonusbuy-btn-bundle">
                  <span class="text-center">Grab VidHostPro Bundle + My 17 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               <div class="f-18 f-md-22 lh150 w400 text-center mt20 black-clr mt-md30">
                     Use Coupon Code <span class="w700 red-gradient">"BLACKFRIDAY"</span> for an Additional <span class="w700 red-gradient">30% Discount</span> on Commercial Licence
                  </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt10">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn-bundle">
                  <span class="text-center">Grab VidHostPro + My 17 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md20" align="center">
                  <h3 class="f-md-35 f-md-28 f-20 w500 text-center black">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr"><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 smmltd">Days</span> </div><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div><div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div></div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- Header Section End -->
      <!-- Step Section End -->
      <div class="step-section">
        <div class="container"> 
            <div class="row">
                <div class="col-12">
                    <div class="f-md-50 f-28 w600 lh150 text-capitalize text-center black-clr">
                        With VidHostPro, Publish A Captivating Video <br class="d-none d-md-block"> <span class="f-md-70 w700 red-gradient">In 3 Easy Steps</span>
                    </div>
                </div>
            </div>
            <div class="row mt20 mt-md50">
                <div class="col-12 col-md-4">
                    <div class="steps-block">
                        <img src="assets/images/step1.webp" class="img-fluid mx-auto d-block" alt="Upload">
                        <div class="f-24 f-md-34 w700 black-clr lh150 mt20 mt-md30 text-center">
                            Upload
                        </div>
                        <div class="f-18 f-md-22 w400 black-clr lh150 mt15 text-center">
                            Upload one or more videos at a time and let VidHostPro optimize them for faster delivery on any browser, page &amp; device.
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-4 mt20 mt-md0">
                    <div class="steps-block">
                        <img src="assets/images/step2.webp" class="img-fluid mx-auto d-block" alt="Upload">
                        <div class="f-24 f-md-34 w700 black-clr lh150 mt20 mt-md30 text-center">
                            Customize
                        </div>
                        <div class="f-18 f-md-22 w400 black-clr lh150 mt15 text-center">
                        The look and feel of the player in just a few clicks to use it for your brand or to monetize it…
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-4 mt20 mt-md0">
                    <div class="steps-block">
                        <img src="assets/images/step3.webp" class="img-fluid mx-auto d-block" alt="Upload">
                        <div class="f-24 f-md-34 w700 black-clr lh150 mt20 mt-md30 text-center">
                            Publish
                        </div>
                        <div class="f-18 f-md-22 w400 black-clr lh150 mt15 mt-md20 text-center">
                            Your videos on any website or landing page and watch your customer engagement, sales and profits roll in fast.
                        </div>
                    </div>
                </div>
                <div class="col-12 w400 f-18 f-md-26 black-clr text-center lh150 mt5 mt20 mt-md40">
                    It Just Takes Minutes to Go Live…
                </div>
                <div class="col-12 w400 f-24 f-md-32 black-clr text-center lh150 mt5 mt10 ">
                    <span class="w700"> No Technical Skills</span> of Any Kind is Needed!
                </div>
                <div class="col-12 f-18 f-md-20 w400 lh150 mt20 text-center  ">
                    Plus, with included FREE commercial license, <span class="w600"> this is the easiest &amp; fastest way </span>to start 6 figure <br class="d-none d-md-block">business and help desperate local businesses in no time!
                </div>
            </div>
        </div>
    </div>
    <section class="faster-sec">
        <div class="container">
           <div class="row">
              <div class="col-12 text-center">
                 <div class="f-20 f-md-24 w400 lh140 black-clr">
                    We spent 3 years Planning, Developing, Testing, and Super-Optimizing with the World's Best Cloud Services. We've Combined Akamai, Aws and HLS Technology to Deliver to YOUR CUSTOMERS a Lightning Fast Video Hosting and Marketing Experience.
                 </div>
                 <div class="f-26 f-md-36 w600 lh140 black-clr mt20">
                    Compare for yourself, the player loading speed with Google's performance tracking tool...
                 </div>
                 
                 <img src="assets/images/down-arrow.webp" class="img-fluid mx-auto d-block downarrow mt20 mt-md30" alt="down-arrow">
              </div>
              <div class="col-12 mt30">
                <div class="col-12  mx-auto">
                    <div class="mt10 row">
                        <div class="col-md-4 col-12 text-center">
                           <img src="assets/images/vimeo-link.webp" class="img-fluid d-block mx-auto" alt="VID10">
                        </div>
                        <div class="col-md-4 col-12 text-center">
                           <img src="assets/images/wistia-link.webp" class="img-fluid d-block mx-auto" alt="VID10">
                        </div>
                        <div class="col-md-4 col-12 text-center">
                           <img src="assets/images/vidhostpro-link.webp" class="img-fluid d-block mx-auto" alt="VID10">
                        </div>
                    </div>
                  </div>
               </div>
            </div>
         </div>
     </section>
     <div class="dark-cta-sec">
         <div class="container">
           <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black black-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-22 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                 
               </div>
               <div class="f-18 f-md-22 lh150 w400 text-center mt20 black-clr mt-md30">
                  Use Coupon Code <span class="w700 red-gradient">"BLACKFRIDAY"</span> for an Additional <span class="w700 red-gradient">30% Discount</span> on Bundle
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt10">
                  <a href="<?php echo $_GET['afflinkbundle']; ?>" class="text-center bonusbuy-btn-bundle">
                  <span class="text-center">Grab VidHostPro Bundle + My 17 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               <div class="f-18 f-md-22 lh150 w400 text-center mt20 black-clr mt-md30">
                     Use Coupon Code <span class="w700 red-gradient">"BLACKFRIDAY"</span> for an Additional <span class="w700 red-gradient">30% Discount</span> on Commercial Licence
                  </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt10">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn-bundle">
                  <span class="text-center">Grab VidHostPro + My 17 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md20" align="center">
                  <h3 class="f-md-35 f-md-28 f-20 w500 text-center black">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr"><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 smmltd">Days</span> </div><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div><div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div></div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>

      <div class="testimonial-section">
        <div class="container ">
           <div class="row ">
              <div class="col-12 f-28 f-md-45 w700 lh140 black-clr text-center">
                 <span class="red-clr">Checkout What Marketers &amp; Early Users</span>  Have to Say About VidHostPro
              </div>
           </div>
          

           <div class="row row-cols-md-2 row-cols-1 gx4 mt-md80 mt0">
              <div class="col mt50">
                 <div class="single-testimonial">
                    <div class="st-img">
                        <img src="assets/images/tim.webp" class="img-fluid d-block mx-auto" alt="Samuel Marco">
                     </div>
                    <div class="mt20 md-md50 f-24 f-md-28 w600 lh140 black-clr">Tim Verdouw</div>
                    <div class="stars mt10">
                       <a href="#"><i class="fa fa-star"></i></a>
                       <a href="#"><i class="fa fa-star"></i></a>
                       <a href="#"><i class="fa fa-star"></i></a>
                       <a href="#"><i class="fa fa-star"></i></a>
                       <a href="#"><i class="fa fa-star"></i></a>
                    </div>
                    <img src="assets/images/quote.webp" alt="Quotes" class="mx-auto d-block img-fluid mt20 mt-md50">
                    <p class="mt20 f-18 f-md-20 lh140 w400 text-center">
                    If you're searching for a dependable video hosting service that won't break the bank, I highly recommend giving VidHostPro a try. Not only is it a reliable option, but it also offers a variety of useful features. The video player is top-notch and has an array of customizable options that make it easy to tailor your video to your specific needs. Overall, I found VidHostPro to be an excellent choice for anyone looking to host their videos online.
                    </p>
                 </div>
              </div>
              <div class="col mt20 mt-md50">
                 <div class="single-testimonial">
                    <div class="st-img">
                       <img src="assets/images/abhi.webp" class="img-fluid d-block mx-auto" alt="Samuel Marco">
                    </div>
                    <div class="mt20 md-md50 f-24 f-md-28 w600 lh140 black-clr"> Abhi Dwivedi  </div>
                    <div class="stars mt10">
                       <a href="#"><i class="fa fa-star"></i></a>
                       <a href="#"><i class="fa fa-star"></i></a>
                       <a href="#"><i class="fa fa-star"></i></a>
                       <a href="#"><i class="fa fa-star"></i></a>
                       <a href="#"><i class="fa fa-star"></i></a>
                    </div>
                    <img src="assets/images/quote.webp" alt="Quotes" class="mx-auto d-block img-fluid mt20 mt-md50">
                    <p class="mt20 f-18 f-md-20 lh140 w400 text-center">
                    VidHostPro is a solid alternative to expensive video hosting and video player platforms out there. With the amount of data storage and bandwidth you get for the price, it truly is a great deal. The app works as expected, the team behind this has been in the video hosting business for years now and offer solid support, Overall, VidHostPro is a solid addon to your online business in 2023.
                    </p>
                 </div>
              </div>
              <div class="col mt-md120">
                <div class="single-testimonial">
                    <div class="st-img">
                        <img src="assets/images/cindy.webp" class="img-fluid d-block mx-auto" alt="Samuel Marco">
                     </div>
                   <div class="mt20 md-md50 f-24 f-md-28 w600 lh140 black-clr"> Cindy Donovan  </div>
                   <div class="stars mt10">
                      <a href="#"><i class="fa fa-star"></i></a>
                      <a href="#"><i class="fa fa-star"></i></a>
                      <a href="#"><i class="fa fa-star"></i></a>
                      <a href="#"><i class="fa fa-star"></i></a>
                      <a href="#"><i class="fa fa-star"></i></a>
                   </div>
                   <img src="assets/images/quote.webp" alt="Quotes" class="mx-auto d-block img-fluid mt20 mt-md50">
                   <p class="mt20 f-18 f-md-20 lh140 w400 text-center">
                   VidHostPro combines ease-of-use with super-powerful video marketing tools. Lead generation is the most effective way to build residual income and combining that with the power of video marketing - this platform becomes a total no brainer.
                   </p>
                </div>
             </div>
             <div class="col mt-md120">
                <div class="single-testimonial">
                   <div class="st-img">
                       <img src="assets/images/samuel.webp" class="img-fluid d-block mx-auto" alt="Samuel Marco">
                    </div>
                   <div class="mt20 md-md50 f-24 f-md-28 w600 lh140 black-clr"> Samuel Marco   </div>
                   <div class="stars mt10">
                      <a href="#"><i class="fa fa-star"></i></a>
                      <a href="#"><i class="fa fa-star"></i></a>
                      <a href="#"><i class="fa fa-star"></i></a>
                      <a href="#"><i class="fa fa-star"></i></a>
                      <a href="#"><i class="fa fa-star"></i></a>
                   </div>
                   <img src="assets/images/quote.webp" alt="Quotes" class="mx-auto d-block img-fluid mt20 mt-md50">
                   <p class="mt20 f-18 f-md-20 lh140 w400 text-center">
                    I've been using VidHostPro for my marketing campaigns and it's been a game-changer. Not only does it make my videos look more professional (I can modify the look of my player to suit my brand), but it also gives me more control over my video traffic. Plus my pages load faster! I know all my videos are in safe hands with them. I cannot recommend it enough.
                   </p>
                </div>  
             </div>
           </div>
        </div>
     </div>


     
      <div class="proudly-sec" id="product">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center f-24 f-md-38 w500 lh140 white-clr mt10 mt-md50">
                  Proudly Presenting...
               </div>
               <div class="col-12 mt-md30 mt20 text-center">
               <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 829.07 133.22" style="max-height:55px">
                            <defs>
                            <style>.cls-1{fill:#fff;}.cls-2{fill:#ec1c24;}</style>
                            </defs>
                            <g id="Layer_2" data-name="Layer 2">
                            <g id="Layer_1-2" data-name="Layer 1">
                            <path class="cls-1" d="M142.09,14.94C89.5-8.93,68.3,32,65.87,37,48.19,38.59,36.52,52.3,32.31,67.64l-1.55-.54c-2.87-1-7-2.8-9.65-5.12a51.82,51.82,0,0,1,6.14-12h0a49.93,49.93,0,0,1,3.41-4.41c9.9-11.41,22.44-15.6,26.3-15.6C72.85.51,95.07,0,99.08,0,126.45-.2,142.09,14.94,142.09,14.94Z"></path>
                            <path class="cls-1" d="M103.29,128.84c-21.44,11.73-43.39-3.34-47.63-7.58-25.5-.6-35.11-20.89-37.43-32.69a121.37,121.37,0,0,0,14.3,4.56C36,103.65,44.31,111.7,58,112.66c23.66,18.51,45.7,7.08,45.7,7.08a40.15,40.15,0,0,0,23.47,3,56.14,56.14,0,0,0,22.25-10.11s11.38,1,18-6.81c3.34-3.95,6.36-7.52,8.22-12.5h0a114.47,114.47,0,0,0,13.44-4.5c-3.45,17.54-17.34,32.81-37.25,32.81-14,13.87-37.41,11.7-45.2,8.61"></path>
                            <path class="cls-1" d="M185.47,61.68a37.61,37.61,0,0,1-3.63,2.23,54,54,0,0,1-6.43,2.93c-6-14-24.8-19.71-24.8-19.71s-10.49-17.19-27.1-23.39c-16.26-6.08-41.44,1-42,1.1.49-.42,21.75-18.55,53.83-6,14.36,6.17,21.74,21.44,21.74,21.44a35.85,35.85,0,0,1,5,1.56,49.15,49.15,0,0,1,9.36,4.73h0A40.26,40.26,0,0,1,185.47,61.68Z"></path>
                            <path class="cls-2" d="M77,80.25V94c-17.82-1.23-33.48-3.67-46-7.14a100,100,0,0,1-13.4-4.67C6.24,77.22-.2,70.89,0,63.51,1.47,42.92,28.58,42.83,29,42.83c-.25.1-16.62,6.85-16.17,15.07.19,3.59,2.24,6.93,6.53,9.9a44.79,44.79,0,0,0,11.74,5.42C41.36,76.53,56.31,79,77,80.25Z"></path>
                            <path class="cls-2" d="M189.87,82.3A99,99,0,0,1,177.23,87c-11,3.29-25.29,5.89-43.6,7.12l11.78-7a8.27,8.27,0,0,0,4.06-7.16,7.84,7.84,0,0,0-.08-1.19c10.93-1.5,20.22-3.65,27.61-6.43a58.75,58.75,0,0,0,7.56-3.4c1.15-.63,2.21-1.26,3.18-1.9,16-10.49,6.87-21.31-22.83-28,36.12,7.83,43.62,16.49,44.7,22.28C210.57,66.47,205.7,75.09,189.87,82.3Z"></path>
                            <path class="cls-2" d="M142.5,82.2l-21,12.47-9.94,5.93-24.9,14.81a2.65,2.65,0,0,1-4-2.28V46.69a2.69,2.69,0,0,1,2-2.56l.68-.05.69,0,.68.29,50.33,30,5.48,3.27a2.66,2.66,0,0,1,0,4.57Z"></path>
                            <path class="cls-1" d="M267.45,89.27l18.83-62.49h19.06l-28.93,83.09H258.54L229.72,26.78h19Z"></path>
                            <path class="cls-1" d="M312,32.14a8.12,8.12,0,0,1,2.48-6.1,10.69,10.69,0,0,1,13.5,0,8.07,8.07,0,0,1,2.51,6.1,8.09,8.09,0,0,1-2.54,6.16,10.57,10.57,0,0,1-13.41,0A8.09,8.09,0,0,1,312,32.14Zm17.52,77.73H312.93V48.12h16.55Z"></path>
                            <path class="cls-1" d="M340.49,78.54q0-14.45,6.48-23T364.69,47a18.92,18.92,0,0,1,14.9,6.73V22.21h16.55v87.66h-14.9l-.8-6.56a19.41,19.41,0,0,1-15.86,7.7,20.94,20.94,0,0,1-17.49-8.59Q340.49,93.84,340.49,78.54ZM357,79.77q0,8.69,3,13.31a9.85,9.85,0,0,0,8.79,4.63q7.65,0,10.79-6.45V66.85q-3.09-6.46-10.67-6.46Q357,60.39,357,79.77Z"></path>
                            <path class="cls-1" d="M477.63,109.87H460.51V74.26H427.13v35.61H410V26.78h17.13V60.45h33.38V26.78h17.12Z"></path>
                            <path class="cls-1" d="M488.93,78.43a36.59,36.59,0,0,1,3.54-16.38,25.81,25.81,0,0,1,10.19-11.13A29.67,29.67,0,0,1,518.09,47q12.51,0,20.41,7.65t8.81,20.77l.12,4.22q0,14.22-7.93,22.8T518.21,111q-13.35,0-21.32-8.56t-8-23.28Zm16.5,1.17q0,8.79,3.31,13.46a11.9,11.9,0,0,0,18.83,0q3.36-4.59,3.37-14.71,0-8.64-3.37-13.38a11,11,0,0,0-9.48-4.74A10.75,10.75,0,0,0,508.74,65C506.53,68.14,505.43,73,505.43,79.6Z"></path>
                            <path class="cls-1" d="M591,92.81A5.32,5.32,0,0,0,588,88q-3-1.74-9.61-3.11-22-4.62-22-18.72a16.94,16.94,0,0,1,6.82-13.72Q570,47,581,47q11.76,0,18.81,5.54a17.45,17.45,0,0,1,7,14.38H590.4a8,8,0,0,0-2.28-5.85q-2.28-2.31-7.14-2.31a9.83,9.83,0,0,0-6.44,1.88,5.91,5.91,0,0,0-2.29,4.79,5.1,5.1,0,0,0,2.6,4.43q2.6,1.68,8.76,2.91A73,73,0,0,1,594,75.51q13.07,4.8,13.07,16.61,0,8.44-7.25,13.67T581.1,111a32.54,32.54,0,0,1-13.78-2.77,23,23,0,0,1-9.45-7.59,17.62,17.62,0,0,1-3.42-10.41h15.63A8.62,8.62,0,0,0,573.34,97a12.87,12.87,0,0,0,8.1,2.34q4.74,0,7.16-1.8A5.57,5.57,0,0,0,591,92.81Z"></path>
                            <path class="cls-1" d="M637.43,32.94V48.12H648v12.1H637.43V91c0,2.28.43,3.92,1.31,4.9s2.55,1.49,5,1.49a26,26,0,0,0,4.85-.4v12.5a33.89,33.89,0,0,1-10,1.48q-17.34,0-17.69-17.52V60.22h-9V48.12h9V32.94Z"></path>
                            <path class="cls-1" d="M675.38,80.59v29.28H658.26V26.78h32.41a37.58,37.58,0,0,1,16.47,3.42,25.29,25.29,0,0,1,10.93,9.73,27.23,27.23,0,0,1,3.82,14.35q0,12.22-8.36,19.27t-23.14,7Zm0-13.86h15.29q6.8,0,10.36-3.2t3.57-9.13a13.69,13.69,0,0,0-3.6-9.87q-3.6-3.78-9.93-3.88H675.38Z"></path>
                            <path class="cls-1" d="M767.26,63.59a44,44,0,0,0-5.94-.46q-9.36,0-12.27,6.33v40.41H732.56V48.12h15.58l.46,7.36q5-8.5,13.75-8.5a17.45,17.45,0,0,1,5.14.74Z"></path>
                            <path class="cls-1" d="M770.57,78.43a36.59,36.59,0,0,1,3.54-16.38,25.85,25.85,0,0,1,10.18-11.13A29.7,29.7,0,0,1,799.73,47q12.49,0,20.4,7.65T829,75.4l.12,4.22q0,14.22-7.94,22.8T799.85,111q-13.36,0-21.32-8.56t-8-23.28Zm16.49,1.17q0,8.79,3.31,13.46a11.91,11.91,0,0,0,18.84,0q3.36-4.59,3.36-14.71,0-8.64-3.36-13.38a11,11,0,0,0-9.48-4.74A10.78,10.78,0,0,0,790.37,65C788.17,68.14,787.06,73,787.06,79.6Z"></path>
                            </g>
                            </g>
                        </svg>
               </div>
               <div class="col-12 f-md-38 f-22 mt-md30 mt20 w600 text-center white-clr lh140">
                    BLAZING-FAST Video Hosting, Player &amp; Marketing Technology That Plays Videos on Any Website, Page &amp; Device Beautifully Without Any Delay or Buffering...
                </div>
               <div class="col-12 mt20 mt-md50">
                  <img src="assets/images/product-box.webp" class="img-fluid d-block mx-auto img-animation" alt="Proudly Presenting...">
               </div>
            </div>
         </div>
      </div>


      <!-- Bonus Section Header Start -->
      <div class="bonus-header">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-10 mx-auto heading-bg text-center">
                  <div class="f-24 f-md-36 lh140 w700"> When You Purchase VidHostPro, You Also Get <br class="hidden-xs"> Instant Access To These 17 Exclusive Bonuses</div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus Section Header End -->
      <!-- Bonus #1 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 1</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus1.webp" class="img-fluid mx-auto d-block" alt="Bonus1">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w600 lh150 bonus-title-color">Auto Video Creator</div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
                           <li>
                           This handy software will allow you to create videos quickly and easily in just a few minutes! Put together a variety of short videos and have them up on VidHostPro in no time to boost your business!
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #1 Section End -->
      <!-- Bonus #2 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 2</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30">
                  <div class="row">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="assets/images/bonus2.webp" class="img-fluid mx-auto d-block" alt="Bonus2">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w600 lh150 bonus-title-color mt20 mt-md0">
                           Text To Speech Converter
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
                           <li>
                           Ready to make your next audio so much easier? Allow this software to convert your text into your next speech - with ease! That’s right. Your text converted into speech. You can use this for an audio for your next video or reading a report or book outloud to yourself! Listen any time, anywhere.
                           </li>
                           <li>
                           Enhance your video marketing results & become a marketing pro.
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #2 Section End -->
      <!-- Bonus #3 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 3</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus3.webp" class="img-fluid mx-auto d-block " alt="Bonus3">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w600 lh150 bonus-title-color">
                           Pro Background Music Tracks
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
                           <li>Professional quality music tracks to enhance your videos. This easy to use software has countless quality tracks that vary in length from 30 seconds to 5 minutes and can be inserted anywhere in your videos.Give this highly professional touch to your videos and grab your viewers attention.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #3 Section End -->
      <!-- Bonus #4 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 4</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="assets/images/bonus4.webp" class="img-fluid mx-auto d-block " alt="Bonus4">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w600 lh150 bonus-title-color">
                           Covert Video Squeeze Page Creator
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
                           <li>							  
                           Video Squeeze Pages are the most effective way to build a list. This creator builds amazing and high-converting squeeze pages easily and will allow you to generate unlimited subscribers!</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #4 End -->
      <!-- Bonus #5 Start -->
      <!-- <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 5</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus5.webp" class="img-fluid mx-auto d-block " alt="Bonus5">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w600 lh150 bonus-title-color">
                           Getting Traffic Video Series
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
                           <li>
                              Selling digital media products is the simplest business model to make massive profits. Follow this system to make thousands per month as an eBay PowerSeller and digital media products seller on the world wide web!
                           </li>
                           <li>
                              Combine this system with VidHostPro to get even more sales and make even more profits from your online business.
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div> -->
      <!-- Bonus #5 End -->
      <!-- CTA Button Section Start -->
      <div class="dark-cta-sec">
         <div class="container">
         <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black black-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-22 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                 
               </div>
               <div class="f-18 f-md-22 lh150 w400 text-center mt20 black-clr mt-md30">
                  Use Coupon Code <span class="w700 red-gradient">"BLACKFRIDAY"</span> for an Additional <span class="w700 red-gradient">30% Discount</span> on Bundle
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt10">
                  <a href="<?php echo $_GET['afflinkbundle']; ?>" class="text-center bonusbuy-btn-bundle">
                  <span class="text-center">Grab VidHostPro Bundle + My 17 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               <div class="f-18 f-md-22 lh150 w400 text-center mt20 black-clr mt-md30">
                     Use Coupon Code <span class="w700 red-gradient">"BLACKFRIDAY"</span> for an Additional <span class="w700 red-gradient">30% Discount</span> on Commercial Licence
                  </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt10">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn-bundle">
                  <span class="text-center">Grab VidHostPro + My 17 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md20" align="center">
                  <h3 class="f-md-35 f-md-28 f-20 w500 text-center black">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr"><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 smmltd">Days</span> </div><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div><div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div></div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End   -->
      <!-- Bonus #6 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 5</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="assets/images/bonus6.webp" class="img-fluid mx-auto d-block " alt="Bonus6">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w600 lh150 bonus-title-color">
                           Branding Secrets
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
                           <li>
                           Want to know the secrets behind building a brand that people love? Find out all the tips, techniques and exact steps to build your brand and develop a perfect marketing strategy.
                           </li>
                           <li>
                           Integrate this with your overall marketing strategy & boost your brand building efforts along with VidHostPro for your video marketing branding.
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #6 End -->
      <!-- Bonus #7 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 6</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus7.webp" class="img-fluid mx-auto d-block " alt="Bonus7">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w600 lh150 bonus-title-color">
                           Turbo GIF Animator
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
                           <li>
                           Love those animated images that look like videos but aren’t? They are fantastic to use for short clips, a quick tip demo, or in places where you can’t use videos - like emails. 
                           </li>
                           <li>
                           These powerful graphics are great to use in promoting products or services online. Now you have a tool that will enable you to easily create these GIFs and have a lot of fun doing it.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #7 End -->
      <!-- Bonus #8 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 7</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus8.webp" class="img-fluid mx-auto d-block " alt="Bonus8">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w600 lh150 bonus-title-color">
                           Public Speaking
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
                           <li>Would you like to learn how to calm your nerves and speak easily and thoughtfully to an audience?</li>

                           <li>This package will help ease you to that moment where you can stand up and share your story with others. Having great confidence in front of more people will help you in all areas of your life, including your business.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #8 End -->
      <!-- Bonus #9 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 8</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus9.webp" class="img-fluid mx-auto d-block " alt="Bonus9">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w600 lh150 bonus-title-color">
                           The Animation Playbook
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
                           <li>This playbook will give you the simple steps to create animated videos that will attract new customers and prospects. You’ll discover the process to create multiple kinds of videos, multiple stories with multiple scenarios.</li>

                           <li>Hosting these fun to create videos on your VidHostPro account will bring in repeat viewers and more customers along with more sales.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #9 End -->
      <!-- Bonus #10 start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 9</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus10.webp" class="img-fluid mx-auto d-block " alt="Bonus10">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w600 lh150 bonus-title-color">
                           Video Launch Method
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
                           <li>Video product launches are the go-to for the world’s best brands. This guide will show you the best techniques used in videos during successful product launches that will help you improve your brand positioning. </li>

                           <li>Reap maximum benefits from your video launches while using these methods and hosting on VidHostPro.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #10 End -->
      <!-- CTA Button Section Start -->
      <div class="dark-cta-sec">
         <div class="container">
        <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black black-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-22 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                 
               </div>
               <div class="f-18 f-md-22 lh150 w400 text-center mt20 black-clr mt-md30">
                  Use Coupon Code <span class="w700 red-gradient">"BLACKFRIDAY"</span> for an Additional <span class="w700 red-gradient">30% Discount</span> on Bundle
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt10">
                  <a href="<?php echo $_GET['afflinkbundle']; ?>" class="text-center bonusbuy-btn-bundle">
                  <span class="text-center">Grab VidHostPro Bundle + My 17 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               <div class="f-18 f-md-22 lh150 w400 text-center mt20 black-clr mt-md30">
                     Use Coupon Code <span class="w700 red-gradient">"BLACKFRIDAY"</span> for an Additional <span class="w700 red-gradient">30% Discount</span> on Commercial Licence
                  </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt10">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn-bundle">
                  <span class="text-center">Grab VidHostPro + My 17 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md20" align="center">
                  <h3 class="f-md-35 f-md-28 f-20 w500 text-center black">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr"><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 smmltd">Days</span> </div><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div><div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div></div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->
      <!-- Bonus #11 start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 10</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus11.webp" class="img-fluid mx-auto d-block " alt="Bonus11">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w600 lh150 bonus-title-color">
                           60 Photoshop Action Scripts
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
                           <li>These Action Scripts create 3D eBook cover designs in just a few clicks. Grab your viewers’ attention and make them want to sign up for your lead magnets or latest reports or any number of other things. </li>

                           <li>You’ll be able to offer these on a form so that your viewers can sign up INSIDE your videos hosted on VidHostPro. </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #11 End -->
      <!-- Bonus #12 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 11</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus12.webp" class="img-fluid mx-auto d-block " alt="Bonus12">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w600 lh150 bonus-title-color">
                           300 Logo Templates
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
                           <li>Your brand - your logo - are so important to your business. You want the best look that quickly conveys the message that you want others to know about your company. You want them to remember it. </li>

                           <li>Even if you aren’t a graphic designer, you can select from 300 Logo Templates to start your logo selection.</li>

                           <li>Combine your logo and your color selections on your video player on VidHostPro and you have a BRAND WINNER!</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #12 End -->
      <!-- Bonus #13 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 12</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus13.webp" class="img-fluid mx-auto d-block " alt="Bonus13">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w600 lh150 bonus-title-color">
                           Animated Pop Over Window Generator
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
                           <li>Have something very special to say? Have a limited time offer? Have something that you don’t want your visitors to miss?  </li>
                           <li>We got you covered - literally!</li>
                           <li>This Pop Over “window” will allow you to add any of those things right over your beautiful page. Once they do whatever it is that you want, they can easily close this “window” and continue to the rest of your site.<br>
                           Use this to really build your business.</li>
                           
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #13 End -->
      <!-- Bonus #14 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 13</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus14.webp" class="img-fluid mx-auto d-block " alt="Bonus14">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w600 lh150 bonus-title-color">
                           Abstract Image Collection V4
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
                           <li>Abstract images can easily and creatively be used to set the tone for your project. We have a wide variety of these digital graphics that can be used immediately. Try different ones out - different colors - different flows - and you’ll soon find one that is “just right” for what you need. Use them for backgrounds for your videos to enhance your message.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #14 End -->
      <!-- Bonus #15 Start -->
      <!-- <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 15</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus15.webp" class="img-fluid mx-auto d-block " alt="Bonus15">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w600 lh150 bonus-title-color">
                           Instant Video Suite
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
                           <li>Creating multi-media streaming video presentations is used by all top brands today in order to scale their profits to the next level. Wondering how you can do the same, time to breathe a sigh of relief.</li>
                           <li>Every visitor with stunning, top-notch multi-media streaming video presentations and convert each of them into buying customers. So, get in active mode and use this bonus with VidHostPro to intensify your growth prospects like never before.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div> -->
      <!-- Bonus #15 End -->
      <!-- CTA Button Section Start -->
      <div class="dark-cta-sec">
         <div class="container">
        <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black black-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-22 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                 
               </div>
               <div class="f-18 f-md-22 lh150 w400 text-center mt20 black-clr mt-md30">
                  Use Coupon Code <span class="w700 red-gradient">"BLACKFRIDAY"</span> for an Additional <span class="w700 red-gradient">30% Discount</span> on Bundle
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt10">
                  <a href="<?php echo $_GET['afflinkbundle']; ?>" class="text-center bonusbuy-btn-bundle">
                  <span class="text-center">Grab VidHostPro Bundle + My 17 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               <div class="f-18 f-md-22 lh150 w400 text-center mt20 black-clr mt-md30">
                     Use Coupon Code <span class="w700 red-gradient">"BLACKFRIDAY"</span> for an Additional <span class="w700 red-gradient">30% Discount</span> on Commercial Licence
                  </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt10">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn-bundle">
                  <span class="text-center">Grab VidHostPro + My 17 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md20" align="center">
                  <h3 class="f-md-35 f-md-28 f-20 w500 text-center black">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr"><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 smmltd">Days</span> </div><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div><div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div></div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->

       <!-- Bonus #16 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 14</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                     <img src="assets/images/bonus16.webp" class="img-fluid mx-auto d-block " alt="Bonus16">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w600 lh150 bonus-title-color">
                        Podcasting Made Easy
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
                        <li>If you want to use the immense power of podcasting to build your business, we have just what you need.</li>
                           <li>With this 5-day crash course, you too can learn how to record your podcast & broadcast to a growing audience in several ways. This is an exciting way to talk to a group of your loyal fans and get your message out.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #16 End -->


      <!-- Bonus #17 Start -->
      <!-- <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 17</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus17.webp" class="img-fluid mx-auto d-block " alt="Bonus17">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w600 lh150 bonus-title-color">
                           Videos For Profit
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
                           <li>Every business whether online or offline uses videos to communicate with their clients.</li>
                           <li>Creating amazing YouTube videos will finally be a reality now as this 31-part video course helps to make powerful demos, presentation and tutorials with just a smartphone, your Mac &amp; affordable tools.</li>
                           <li>When used with the video marketing powers of VidHostPro, this package will surely become a top-notch business booster.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div> -->
      <!-- Bonus #17 End -->
      <!-- Bonus #18 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 15</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus18.webp" class="img-fluid mx-auto d-block " alt="Bonus18">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w600 lh150 bonus-title-color">
                           List Building Videos
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
                           <li>Streamline your efforts to build your list. These how-to videos will show you what’s needed to build a profitable list and use it to boost your affiliate commissions like a pro.</li>
                           <li>Put those skills to use and set up your list building form INSIDE your VidHostPro video and watch your list grow.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #18 End -->
      <!-- Bonus #19 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 16</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus19.webp" class="img-fluid mx-auto d-block " alt="Bonus19">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w600 lh150 bonus-title-color">
                           Latest Humans Stock Images
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
                           <li>Every marketer needs tons of stock images for their projects, such as ads, images, & videos. They especially need images of humans doing multiple things (working, playing, resting), in various positions (standing, sitting, waving); doing various activities (waiting on customers; taking care of a child; playing sports) and in different weather outdoors or different situations indoors, and so much more. </li>
                           <li>You will have a package of stock images for your videos that you will be able to use in whatever situation you need.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #19 End -->
      <!-- Bonus #20 start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 17</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus20.webp" class="img-fluid mx-auto d-block " alt="Bonus20">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w600 lh150 bonus-title-color">
                           Web Video Production
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
                           <li>Maximize your message in a short time to your globally scattered audience by conveying your message through video. This package will show you the best way to do that.</li>
                           <li>Then host your video on VidHostPro and get the right viewers watching!</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #20 End -->
      <!-- Huge Woth Section Start -->
      <div class="huge-area mt30 mt-md10">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="f-md-65 f-40 lh120 w700 white-clr">That’s Huge Worth of</div>
                  <br>
                  <div class="f-md-70 f-40 lh120 w800 red-gradient">$2895!</div>
               </div>
            </div>
         </div>
      </div>
      <!-- Huge Worth Section End -->
      <!-- text Area Start -->
      <div class="white-section pb0">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="f-md-36 f-25 lh140 w500">So what are you waiting for? You have a great opportunity <br class="hidden-xs"> ahead + <span class="w700 red-gradient">My 17 Bonus Products</span> are making it a <br class="hidden-xs"> <span class="w700 red-gradient">completely NO Brainer!!</span></div>
               </div>
            </div>
         </div>
      </div>
      <!-- text Area End -->
      <!-- CTA Button Section Start -->
      <div class="cta-btn-section">
         <div class="container">
           <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black black-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-22 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                 
               </div>
               <div class="f-18 f-md-22 lh150 w400 text-center mt20 black-clr mt-md30">
                  Use Coupon Code <span class="w700 red-gradient">"BLACKFRIDAY"</span> for an Additional <span class="w700 red-gradient">30% Discount</span> on Bundle
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt10">
                  <a href="<?php echo $_GET['afflinkbundle']; ?>" class="text-center bonusbuy-btn-bundle">
                  <span class="text-center">Grab VidHostPro Bundle + My 17 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               <div class="f-18 f-md-22 lh150 w400 text-center mt20 black-clr mt-md30">
                     Use Coupon Code <span class="w700 red-gradient">"BLACKFRIDAY"</span> for an Additional <span class="w700 red-gradient">30% Discount</span> on Commercial Licence
                  </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt10">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn-bundle">
                  <span class="text-center">Grab VidHostPro + My 17 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md20" align="center">
                  <h3 class="f-md-35 f-md-28 f-20 w500 text-center black">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr"><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 smmltd">Days</span> </div><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div><div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div></div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->
      <!-- Footer Section Start -->
      <div class="footer-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
               <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 829.07 133.22" style="max-height:55px">
                            <defs>
                            <style>.cls-1{fill:#fff;}.cls-2{fill:#ec1c24;}</style>
                            </defs>
                            <g id="Layer_2" data-name="Layer 2">
                            <g id="Layer_1-2" data-name="Layer 1">
                            <path class="cls-1" d="M142.09,14.94C89.5-8.93,68.3,32,65.87,37,48.19,38.59,36.52,52.3,32.31,67.64l-1.55-.54c-2.87-1-7-2.8-9.65-5.12a51.82,51.82,0,0,1,6.14-12h0a49.93,49.93,0,0,1,3.41-4.41c9.9-11.41,22.44-15.6,26.3-15.6C72.85.51,95.07,0,99.08,0,126.45-.2,142.09,14.94,142.09,14.94Z"></path>
                            <path class="cls-1" d="M103.29,128.84c-21.44,11.73-43.39-3.34-47.63-7.58-25.5-.6-35.11-20.89-37.43-32.69a121.37,121.37,0,0,0,14.3,4.56C36,103.65,44.31,111.7,58,112.66c23.66,18.51,45.7,7.08,45.7,7.08a40.15,40.15,0,0,0,23.47,3,56.14,56.14,0,0,0,22.25-10.11s11.38,1,18-6.81c3.34-3.95,6.36-7.52,8.22-12.5h0a114.47,114.47,0,0,0,13.44-4.5c-3.45,17.54-17.34,32.81-37.25,32.81-14,13.87-37.41,11.7-45.2,8.61"></path>
                            <path class="cls-1" d="M185.47,61.68a37.61,37.61,0,0,1-3.63,2.23,54,54,0,0,1-6.43,2.93c-6-14-24.8-19.71-24.8-19.71s-10.49-17.19-27.1-23.39c-16.26-6.08-41.44,1-42,1.1.49-.42,21.75-18.55,53.83-6,14.36,6.17,21.74,21.44,21.74,21.44a35.85,35.85,0,0,1,5,1.56,49.15,49.15,0,0,1,9.36,4.73h0A40.26,40.26,0,0,1,185.47,61.68Z"></path>
                            <path class="cls-2" d="M77,80.25V94c-17.82-1.23-33.48-3.67-46-7.14a100,100,0,0,1-13.4-4.67C6.24,77.22-.2,70.89,0,63.51,1.47,42.92,28.58,42.83,29,42.83c-.25.1-16.62,6.85-16.17,15.07.19,3.59,2.24,6.93,6.53,9.9a44.79,44.79,0,0,0,11.74,5.42C41.36,76.53,56.31,79,77,80.25Z"></path>
                            <path class="cls-2" d="M189.87,82.3A99,99,0,0,1,177.23,87c-11,3.29-25.29,5.89-43.6,7.12l11.78-7a8.27,8.27,0,0,0,4.06-7.16,7.84,7.84,0,0,0-.08-1.19c10.93-1.5,20.22-3.65,27.61-6.43a58.75,58.75,0,0,0,7.56-3.4c1.15-.63,2.21-1.26,3.18-1.9,16-10.49,6.87-21.31-22.83-28,36.12,7.83,43.62,16.49,44.7,22.28C210.57,66.47,205.7,75.09,189.87,82.3Z"></path>
                            <path class="cls-2" d="M142.5,82.2l-21,12.47-9.94,5.93-24.9,14.81a2.65,2.65,0,0,1-4-2.28V46.69a2.69,2.69,0,0,1,2-2.56l.68-.05.69,0,.68.29,50.33,30,5.48,3.27a2.66,2.66,0,0,1,0,4.57Z"></path>
                            <path class="cls-1" d="M267.45,89.27l18.83-62.49h19.06l-28.93,83.09H258.54L229.72,26.78h19Z"></path>
                            <path class="cls-1" d="M312,32.14a8.12,8.12,0,0,1,2.48-6.1,10.69,10.69,0,0,1,13.5,0,8.07,8.07,0,0,1,2.51,6.1,8.09,8.09,0,0,1-2.54,6.16,10.57,10.57,0,0,1-13.41,0A8.09,8.09,0,0,1,312,32.14Zm17.52,77.73H312.93V48.12h16.55Z"></path>
                            <path class="cls-1" d="M340.49,78.54q0-14.45,6.48-23T364.69,47a18.92,18.92,0,0,1,14.9,6.73V22.21h16.55v87.66h-14.9l-.8-6.56a19.41,19.41,0,0,1-15.86,7.7,20.94,20.94,0,0,1-17.49-8.59Q340.49,93.84,340.49,78.54ZM357,79.77q0,8.69,3,13.31a9.85,9.85,0,0,0,8.79,4.63q7.65,0,10.79-6.45V66.85q-3.09-6.46-10.67-6.46Q357,60.39,357,79.77Z"></path>
                            <path class="cls-1" d="M477.63,109.87H460.51V74.26H427.13v35.61H410V26.78h17.13V60.45h33.38V26.78h17.12Z"></path>
                            <path class="cls-1" d="M488.93,78.43a36.59,36.59,0,0,1,3.54-16.38,25.81,25.81,0,0,1,10.19-11.13A29.67,29.67,0,0,1,518.09,47q12.51,0,20.41,7.65t8.81,20.77l.12,4.22q0,14.22-7.93,22.8T518.21,111q-13.35,0-21.32-8.56t-8-23.28Zm16.5,1.17q0,8.79,3.31,13.46a11.9,11.9,0,0,0,18.83,0q3.36-4.59,3.37-14.71,0-8.64-3.37-13.38a11,11,0,0,0-9.48-4.74A10.75,10.75,0,0,0,508.74,65C506.53,68.14,505.43,73,505.43,79.6Z"></path>
                            <path class="cls-1" d="M591,92.81A5.32,5.32,0,0,0,588,88q-3-1.74-9.61-3.11-22-4.62-22-18.72a16.94,16.94,0,0,1,6.82-13.72Q570,47,581,47q11.76,0,18.81,5.54a17.45,17.45,0,0,1,7,14.38H590.4a8,8,0,0,0-2.28-5.85q-2.28-2.31-7.14-2.31a9.83,9.83,0,0,0-6.44,1.88,5.91,5.91,0,0,0-2.29,4.79,5.1,5.1,0,0,0,2.6,4.43q2.6,1.68,8.76,2.91A73,73,0,0,1,594,75.51q13.07,4.8,13.07,16.61,0,8.44-7.25,13.67T581.1,111a32.54,32.54,0,0,1-13.78-2.77,23,23,0,0,1-9.45-7.59,17.62,17.62,0,0,1-3.42-10.41h15.63A8.62,8.62,0,0,0,573.34,97a12.87,12.87,0,0,0,8.1,2.34q4.74,0,7.16-1.8A5.57,5.57,0,0,0,591,92.81Z"></path>
                            <path class="cls-1" d="M637.43,32.94V48.12H648v12.1H637.43V91c0,2.28.43,3.92,1.31,4.9s2.55,1.49,5,1.49a26,26,0,0,0,4.85-.4v12.5a33.89,33.89,0,0,1-10,1.48q-17.34,0-17.69-17.52V60.22h-9V48.12h9V32.94Z"></path>
                            <path class="cls-1" d="M675.38,80.59v29.28H658.26V26.78h32.41a37.58,37.58,0,0,1,16.47,3.42,25.29,25.29,0,0,1,10.93,9.73,27.23,27.23,0,0,1,3.82,14.35q0,12.22-8.36,19.27t-23.14,7Zm0-13.86h15.29q6.8,0,10.36-3.2t3.57-9.13a13.69,13.69,0,0,0-3.6-9.87q-3.6-3.78-9.93-3.88H675.38Z"></path>
                            <path class="cls-1" d="M767.26,63.59a44,44,0,0,0-5.94-.46q-9.36,0-12.27,6.33v40.41H732.56V48.12h15.58l.46,7.36q5-8.5,13.75-8.5a17.45,17.45,0,0,1,5.14.74Z"></path>
                            <path class="cls-1" d="M770.57,78.43a36.59,36.59,0,0,1,3.54-16.38,25.85,25.85,0,0,1,10.18-11.13A29.7,29.7,0,0,1,799.73,47q12.49,0,20.4,7.65T829,75.4l.12,4.22q0,14.22-7.94,22.8T799.85,111q-13.36,0-21.32-8.56t-8-23.28Zm16.49,1.17q0,8.79,3.31,13.46a11.91,11.91,0,0,0,18.84,0q3.36-4.59,3.36-14.71,0-8.64-3.36-13.38a11,11,0,0,0-9.48-4.74A10.78,10.78,0,0,0,790.37,65C788.17,68.14,787.06,73,787.06,79.6Z"></path>
                            </g>
                            </g>
                        </svg>
                  <!-- <div editabletype="text" class="f-16 f-md-16 w300 mt20 lh140 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div> -->
                  <br><br><br>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-16 f-md-16 w300 lh140 white-clr text-xs-center">Copyright © VidHostPro</div>
                  <ul class="footer-ul w300 f-16 f-md-16 white-clr text-center text-md-right">
                     <li><a href="https://support.oppyo.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://vidhostpro.co/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://vidhostpro.co/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://vidhostpro.co/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://vidhostpro.co/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://vidhostpro.co/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://vidhostpro.co/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!--Footer Section End -->
      <!-- timer --->
      <?php
         if ($now < $exp_date) {
         ?>
      <script type="text/javascript">
         // Count down milliseconds = server_end - server_now = client_end - client_now
         var server_end = <?php echo $exp_date; ?> * 1000;
         var server_now = <?php echo time(); ?> * 1000;
         var client_now = new Date().getTime();
         var end = server_end - server_now + client_now; // this is the real end time
         
         var noob = $('.countdown').length;
         
         var _second = 1000;
         var _minute = _second * 60;
         var _hour = _minute * 60;
         var _day = _hour * 24
         var timer;
         
         function showRemaining() {
         var now = new Date();
         var distance = end - now;
         if (distance < 0) {
         	clearInterval(timer);
         	document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
         	return;
         }
         
         var days = Math.floor(distance / _day);
         var hours = Math.floor((distance % _day) / _hour);
         var minutes = Math.floor((distance % _hour) / _minute);
         var seconds = Math.floor((distance % _minute) / _second);
         if (days < 10) {
         	days = "0" + days;
         }
         if (hours < 10) {
         	hours = "0" + hours;
         }
         if (minutes < 10) {
         	minutes = "0" + minutes;
         }
         if (seconds < 10) {
         	seconds = "0" + seconds;
         }
         var i;
         var countdown = document.getElementsByClassName('countdown');
         for (i = 0; i < noob; i++) {
         	countdown[i].innerHTML = '';
         
         	if (days) {
         		countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + days + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 smmltd">Days</span> </div>';
         	}
         
         	countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + hours + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>';
         
         	countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">' + minutes + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>';
         
         	countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">' + seconds + '</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>';
         }
         
         }
         timer = setInterval(showRemaining, 1000);
         	
      </script>
      <?php
         } else {
         echo "Times Up";
         }
         ?>
      <!--- timer end-->
   </body>
</html>
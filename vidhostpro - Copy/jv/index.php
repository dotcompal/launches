<html>
   <head>
      <title>JV Page - VidHostPro JV Invite</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=9">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
      <meta name="title" content="VidHostPro | JV">
      <meta name="description" content="Todd Gross & Dr. Amit Pareek Presents VidHostPro - EXTRAORDINARY Solution - Happily Serving 74 Million+ Video Views…">
      <meta name="keywords" content="VidHostPro">
      <meta property="og:image" content="https://www.vidhostpro.co/jv/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Dr. Amit Pareek">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="VidHostPro | JV">
      <meta property="og:description" content="Todd Gross & Dr. Amit Pareek Presents VidHostPro - EXTRAORDINARY Solution  - Happily Serving 74 Million+ Video Views…">
      <meta property="og:image" content="https://www.vidhostpro.co/jv/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="VidHostPro | JV">
      <meta property="twitter:description" content="Todd Gross & Dr. Amit Pareek Presents VidHostPro - EXTRAORDINARY Solution  - Happily Serving 74 Million+ Video Views…">
      <meta property="twitter:image" content="https://www.vidhostpro.co/jv/thumbnail.png">
      <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
      <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
      <link rel="stylesheet" href="../common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style.css" type="text/css">
      <link rel="stylesheet" href="assets/css/timer.css" type="text/css">
      <link rel="stylesheet" href="assets/css/jquery.bxslider.min.css" type="text/css">
      <script src="../common_assets/js/jquery.min.js"></script>
      <script src="assets/css/jquery.bxslider.min.js"></script>
      <!-- Google Tag Manager -->
      <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
         new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
         j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
         'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
         })(window,document,'script','dataLayer','GTM-NMK2MBB');
      </script>
      <!-- End Google Tag Manager -->
   </head>
   <body>
      <!-- Google Tag Manager (noscript) -->
      <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NMK2MBB"
         height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
      <!-- End Google Tag Manager (noscript) --> 
      <!-- New Timer  Start-->
      <?php
         $date = 'nov 24 2023 11:00 AM EST';
         $exp_date = strtotime($date);
         $now = time();  
         /*
         
         $date = date('F d Y g:i:s A eO');
         $rand_time_add = rand(700, 1200);
         $exp_date = strtotime($date) + $rand_time_add;
         $now = time();*/
         
         if ($now < $exp_date) {
         ?>
      <?php
         } else {
          echo "";
         }
         ?>
      <!-- New Timer End -->
      <!-- Header Section Start -->
      <div class="header-section">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12">
                  <img src="assets/images/sale.webp" class="img-fluid mx-auto d-block">
               </div>
               
               <div class="col-12 mt20 mt-md70 text-center ">
                  <div class="preheadline f-18 f-md-24 w600 black-clr lh140">
                 Todd Gross, Dr. Amit Pareek & Atul Pareek Presents Black Friday Special Deal
                  <!-- <img src="assets/images/sep-top.png" class="img-fluid d-block mx-auto"> -->
                  </div>
               </div>
               <div class="col-12 mt-md60 mt10 f-md-50 f-28 w700 text-center white-clr lh140">
                  <span class="w700 red-gradient">A BLAZING-FAST Video Hosting, Player & Marketing Technology </span> That Have Smartly...
                  
               </div>
               <div class="col-12 mt20 mt-md60 f-md-22 f-20 w400 white-clr text-center">
                  <ul class="bxslider text f-28 f-md-40">
                     <li class="text-clr1">
                        <div> <img src="assets/images/video-icon.png" class="img-fluid"></div>
                        <div class="text">Processed 88,202+ Videos Successfully</div>
                     </li>
                     <li class="text-clr2">
                        <div> <img src="assets/images/eye-icon.png" class="img-fluid"></div>
                        <div class="text">Got 59,160,109+ Video Views Till Now</div>
                     </li>
                     <li class="text-clr3">
                        <div> <img src="assets/images/rocket-icon.png" class="img-fluid"></div>
                        <div class="text">Played 12,616,412+ Minutes of Videos</div>
                     </li>
                     <li class="text-clr4">
                        <div> <img src="assets/images/bag-icon.png" class="img-fluid"></div>
                        <div class="text">Served 18,810+ Successful Businesses</div>
                     </li>
                  </ul>
               </div>
               <div class="col-12 col-md-12 mt20 mt-md30">
                  <div class="row">
                     <div class="col-md-8 col-12">
                        <div class="video-box">
                        <div class="responsive-video">
                           <iframe class="embed-responsive-item" src="https://promos.oppyo.com/video/embed/8qmrsey2o1" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                              box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                        </div></div>
                     </div>
                     <div class="col-md-4 col-12 mt20 mt-md0">
                   
                     <div class="calendar-wrap-inline">
                              <!-- <div class="text-uppercase f-26 f-md-32 w500">Thursday</div> -->
                              <!-- <div class="f-20 w600 white-clr lh140 d-flex align-items-center justify-content-center calender-text">
                                  Mark Your Calender <img src="assets/images/calender-icon.png" class="d-block img-fluid ml15"></div> -->
                              <div class="date">
                                  <div class="date-text">
                                      Nov 24<sup>th</sup>
                                  </div>
                                  <div class=" w600 lh140 f-28 f-md-45 white-clr">
                                     2023
                                  </div>
                              </div>
                              <div class="text-uppercase f-18 f-md-20 w600 mt20 white-clr clock-text">  <img src="assets/images/circular-clock.webp" alt="" class="mx-auto img-fluid d-block mr15">  At 11 AM EST</div>
                           </div>
                        <div class="clearfix"></div>
                        <div class="countdown counter-black mt10">
                           <div class="timer-label text-center"><span class="f-40 f-md-50 lh100 timerbg col-12">01</span><br><span class="f-20 w600">Days</span> </div>
                           <div class="timer-label text-center"><span class="f-40 f-md-50 lh100 timerbg">16</span><br><span class="f-20 w600">Hours</span> </div>
                           <div class="timer-label text-center"><span class="f-40 f-md-50 lh100 timerbg">59</span><br><span class="f-20 w600">Mins</span> </div>
                           <div class="timer-label text-center"><span class="f-40 f-md-50 lh100 timerbg">37</span><br><span class="f-20 w600">Sec</span> </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="live-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="formbg-up">
                  <div class="col-12 col-md-12 ">
                  <div class="f-24 f-md-50 w700 text-center lh140">
                     <span>Grab Your <span class="orange-clr">JVZoo </span>Affiliate Link to Jump on This Amazing Product Launch
                  </div>
               </div>
               <div class="col-md-12 mx-auto mt20">
                  <div class="row justify-content-center align-items-center">
                     <div class="col-12 col-md-3">
                        <img src="assets/images/jvzoo.png" class="img-fluid d-block mx-auto" />
                     </div>
                     <div class="col-12 col-md-4 affiliate-btn  mt-md19 mt15">
                        <a href="https://www.jvzoo.com/affiliate/affiliateinfonew/index/394879"  target="_blank"  class="f-22 f-md-24 w600 mx-auto">Request Affiliate Link</a>
                     </div>
                  </div>
               </div>
               <div class="col-12">
               
                  <div class="d-flex flex-wrap justify-content-center  mt15 mt-md50">
                     
                     <a href="https://docs.google.com/document/d/1_eVnSA0WoYRxjhgNEFfZoMfvhQaBOZKm/edit?usp=sharing&ouid=117555941573669202344&rtpof=true&sd=true"  target="_blank"  class="affiliate-link-btn ml-md15 mt10 mt-md0">JV Doc</a>
                     <a href="https://docs.google.com/document/d/1PaWM0mBrc-YjD1ZlpKqnRB3oRK9dLt6h/edit?usp=sharing&ouid=117555941573669202344&rtpof=true&sd=true"  target="_blank"  class="affiliate-link-btn ml-md15 mt10 mt-md0">Swipes</a>
                  </div>
               </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="list-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center d-flex mx-auto justify-content-center">
               <span class="promote f-20 f-md-45 w600 lh140 white-clr">6 Reasons</span><div class="f-20 f-md-44 w600 lh140 black-clr winner-heading"> To Promote VidHostPro</div>
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-12 col-md-5 mt20 mt-md0">
                  <img src="assets/images/thumbsup.webp" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 col-md-7 f-md-20 f-18 w500 lh150 mt30 mt-md0">
                  <ul class="f-18 f-md-20 w500 lh140 black-clr know-list pl0 mt0">
                    <li> We spent 3 years Planning, Developing, Testing, and Super-Optimizing with the World's Best Cloud Services: Akamai, Aws and HLS Technology to Deliver to YOUR CUSTOMERS a Lightning Fast Video Hosting and Marketing Experience. </li>
                    <li> Your customers will have Complete Control over their Video Traffic to boost engagement and conversions.  </li>
                    <li> Your customers will receive Deep Insight into their users' behavior & engagement to improve their marketing campaigns. </li>
                    <li> Your customers can customize their player, show lead forms and promo ads inside their videos PLUS more than 50 other cool features!  </li>  
                    <li> We've packaged this in an Elegant Look with Secure Video Delivery. </li>
                    <li> <span class="w700">Video Hosting Built By Video Marketers</span> Your customers will have direct access to hosting owners who are also video marketers.</li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
     
      <div class="problem-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-20 f-md-45 w600 white-clr heading-design">
                       The <span class="red-clr"><u>Problem</u></span> Is
                  </div>
                  
               </div>
               <div class="f-26 f-md-45 w600 lh140 white-clr">Not All Video Hosting Solutions Are Equal...</div>
            </div>
            <div class="row align-items-center mt20 mt-md50">
               <div class="col-md-7 col-12 mt-md0 mt20 ">
                  <div class="f-16 f-md-24 w600 red-clr lh140">
               Option 1- Use YouTube. It's Free Right?
               <br><span class="white-clr w400"> Yes. It's Free for a reason.</span>
               <ul class="terms-list pl0 m0 f-20 lh140 w300 mt20 white-clr">
                    
                     <li>It’s not designed to make YOUR CUSTOMERS money. It’s designed to make YouTube money and bring them more business. </li>
                     <li>Your Customers' Traffic Leaks From Disruptive Ads</li>
                     <li>Your Customers' Traffic Is Distracted By Related Video Suggestions</li>
                     <li>Your Customers' Video and Channel Can Be Terminated Anytime!</li>
                  </ul>
               </div>
               <div class=" mt-md40 mt20 f-18 f-md-24 w600 red-clr lh140">
               Option 2 – Use Old School HTML Players

               <ul class="terms-list pl0 m0 f-20 lh140 w300 mt20 white-clr">
                     <li>Your videos load slowly & ruin user's experience </li>
                  </ul>
               </div>

               <div class=" mt-md40 mt20 f-18 f-md-24 w600 red-clr lh140">
               Option 3 – Spend 100s of Dollar Every Month
               <ul class="terms-list pl0 m0 f-20 lh140 w300 mt20 white-clr">
               There are EXPENSIVE alternatives that your customers can spend money on EACH AND EVERY MONTH.
                  </ul>
               </div>
               <div class="mt-md40 mt20 f-18 f-md-28 w600 white-clr lh140">
               If this is what your customers have been doing...<br>
               then they may have been<br>
               shortchanging their businesses...<br>
               </div></div>
               <div class="col-md-5 col-12 ">
                  <img src="assets/images/problem-img.webp" class="img-fluid mx-auto d-block">
               </div>
            </div>
            </div>
         </div>
         <div class="butnotanymore-sec relative">   
            <div class="container">
               <div class="row">
                  <div class="col-12 text-center">
                     <div class="f-24 f-md-50 w600 red-clr lh140 relative">
                        But Not Anymore
                     </div>
                     <div class="f-24 f-md-45 w600 black-clr lh140  mt20 mt-md30">
                     After Years of Planning, Coding, <br class="d-none d-md-block"> Debugging & Adding Robust Technology
                     </div>
                     <div class="f-24 f-md-45 w600 red-clr lh140 trigger-block mt20 mt-md30">
                     BLAZING-FAST Video Hosting & Player
                     </div>
                     <div class="f-24 f-md-45 w600 black-clr lh140 mt20 mt-md30">
                     We are back with VidHostPro That Make <br class="d-none d-md-block"> It a Cut above the Rest
                     </div>
                     
                  </div>
               </div>
            </div>
            <img src="assets/images/butnotanymore-img.webp" alt="not any more" class="img-fluid d-none d-md-block ele1">
      </div>

      <div class="proudly-sec" id="product">
         <div class="container">
            <div class="row"> 
               <div class="col-12 text-center ">
                  <div class="f-28 f-md-50 w600 lh140 white-clr presenting-head caveat">
                  Proudly Presenting…
               </div></div>
               <div class="col-12 f-md-50 f-22 mt-md70 mt20 w700 text-center white-clr lh140">
               One Platform for All Your Customers'  <br class="d-none d-md-block">Video Marketing Needs

               </div>
               <div class="col-12 mx-auto mt10 ">
                  <img src="assets/images/product-image.webp" class="img-fluid d-block mx-auto img-animation">
               </div>
               <div class=" mt-md40 mt20 f-18 f-md-26 w600 red-clr lh140">
               Eliminate Any Obstacles That May Be Hindering Your Customers' Path To Success
               <ul class="terms-list pl0 m0 f-22 lh140 w300 mt20 white-clr">
               <li>Your Customers Can Play Their Marketing Videos & Courses - Ads Free</li>
               <li>Switch To One Time Pricing & Save Huge</li>
               <li>Blazing Fast Video Hosting, So Your Customres Never Loose Traffic Again.</li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!-- next-generation end -->
      <div class="marketing-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-30 f-md-56 w700 text-center">
                  Welcome to the <span class="red-gradient">Future of Video Marketing</span>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md60" data-aos="fade-up">
                  <div class="content-box">
                     <div class="title">Premium Video Hosting & Marketing For <br class="d-none d-md-block"> A One-Time Price
                     </div>
                     <img src="assets/images/m1.webp" class="img-fluid d-block mx-auto"> 
                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md60" data-aos="fade-up">
                  <div class="content-box">
                     <div class="title">Beautiful Conversion Focused <br class="d-none d-md-block"> Video Player
                     </div>
                     <img src="assets/images/m2.webp" class="img-fluid d-block mx-auto"> 
                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md60" data-aos="fade-up">
                  <div class="content-box">
                     <div class="title">Video Chapters To Segment Different Sections, Title, and Timestamp
                     </div>
                     <img src="assets/images/m3.webp" class="img-fluid d-block mx-auto"> 
                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md60" data-aos="fade-up">
                  <div class="content-box ">
                     <div class="title">Embed PlayList, Engage, & Deliver Interactive Experiences For Your Customers' Website Visitors.
                     </div>
                     <img src="assets/images/m4.gif" class="img-fluid d-block mx-auto mt45"> 
                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md60" data-aos="fade-up">
                  <div class="content-box">
                     <div class="title">Advanced Analytics & Stats Comparison For Various Videos
                     </div>
                     <img src="assets/images/m5.webp" class="img-fluid d-block mx-auto"> 
                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md60" data-aos="fade-up">
                  <div class="content-box">
                     <div class="title">Seamlessly Integrates With Major Autoresponders
                     </div>
                     <img src="assets/images/m6.webp" class="img-fluid d-block mx-auto"> 
                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md60" data-aos="fade-up">
                  <div class="content-box">
                     <div class="title">Create Beautiful & SEO Friendly Video Channels
                     </div>
                     <img src="assets/images/m7.webp" class="img-fluid d-block mx-auto"> 
                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md60" data-aos="fade-up">
                  <div class="content-box">
                     <div class="title">Sell, Promote or Collect Leads Directly Inside The Videos
                     </div>
                     <img src="assets/images/m8.webp" class="img-fluid d-block mx-auto"> 
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- FEATURE LIST SECTION START -->
      <!-- <div class="feature-sec">
         <div class="container-xxl">
            <div class="row align-items-center" data-aos="fade-left">
               <div class="col-md-6 col-12 f-md-32 f-22 w600">
                  <img src="assets/images/left-arrow.png" class="img-fluid ms-auto d-none d-md-block">
                  Lightning-FAST Video Hosting & Marketing Platform that Deliver HD Videos without any delay <br class="d-none d-md-block"> or buffering
               </div>
               <div class="col-md-6 col-12 mt-md0 mt20">
                  <img src="assets/images/f1.webp" class="img-fluid mx-auto d-block">
               </div>
            </div>
            <div class="row align-items-center mt-md80 mt30" data-aos="fade-right">
               <div class="col-md-6 col-12 f-md-32 f-22 w600 order-md-2">
                  <img src="assets/images/right-arrow.png" class="img-fluid d-none d-md-block">
                  Play Elegant Videos on Any Site, Page, or 
                  A device without touching any code, just 
                  copy-paste-play videos beautifully in 3 easy steps
               </div>
               <div class="col-md-6 col-12 mt-md0 mt20 order-md-1">
                  <img src="assets/images/f2.webp" class="img-fluid mx-auto d-block">
               </div>
            </div>
            <div class="row align-items-center mt-md80 mt30" data-aos="fade-left">
               <div class="col-md-6 col-12 f-md-32 f-22 w600">
                  <img src="assets/images/left-arrow.png" class="img-fluid ms-auto d-none d-md-block">
                  Create Unlimited Video <br class="d-none d-md-block"> Channels 
                  to Boost Your Brand
               </div>
               <div class="col-md-6 col-12 mt-md0 mt20">
                  <img src="assets/images/f3.webp" class="img-fluid mx-auto d-block">
               </div>
            </div>
            <div class="row align-items-center mt-md80 mt30" data-aos="fade-right">
               <div class="col-md-6 col-12 f-md-32 f-22 w600 order-md-2">
                  <img src="assets/images/right-arrow.png" class="img-fluid d-none d-md-block">
                  Have 100% Control over Your Videos Traffic
                  as No Traffic Leakage, Disturbances or 
                  Distractions due to Any 3rd Party Ads.
               </div>
               <div class="col-md-6 col-12 mt-md0 mt20 order-md-1">
                  <img src="assets/images/f4.webp" class="img-fluid mx-auto d-block">
               </div>
            </div>
            <div class="row align-items-center mt-md80 mt30" data-aos="fade-left">
               <div class="col-md-6 col-12 f-md-32 f-22 w600">
                  <img src="assets/images/left-arrow.png" class="img-fluid ms-auto d-none d-md-block">
                  Robust & Proven Solution with powerful 
                  battle-tested architecture and ultra-fast CDN 
               </div>
               <div class="col-md-6 col-12 mt-md0 mt20">
                  <img src="assets/images/f5.webp" class="img-fluid mx-auto d-block">
               </div>
            </div>
            <div class="row align-items-center mt-md80 mt30" data-aos="fade-right">
               <div class="col-md-6 col-12 f-md-32 f-22 w600 order-md-2">
                  <img src="assets/images/right-arrow.png" class="img-fluid d-none d-md-block">
                  Sell, Promote or Collect Leads 
                  Directly Inside the Videos
               </div>
               <div class="col-md-6 col-12 mt-md0 mt20 order-md-1">
                  <img src="assets/images/f6.webp" class="img-fluid mx-auto d-block">
               </div>
            </div>
            <div class="row align-items-center mt-md80 mt30" data-aos="fade-left">
               <div class="col-md-6 col-12 f-md-32 f-22 w600">
                  <img src="assets/images/left-arrow.png" class="img-fluid ms-auto d-none d-md-block">
                  Advanced Analytics & Compare 
                  Stats for Various Videos
               </div>
               <div class="col-md-6 col-12 mt-md0 mt20">
                  <img src="assets/images/f7.webp" class="img-fluid mx-auto d-block">
               </div>
            </div>
         </div>
      </div> -->
      <div class="potential-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-50 f-28 w600 lh140 text-center black-clr">
                  Here are Some More Amazing Features
               </div>
            </div>
            <div class="row">
               <div class="col-12 col-md-4 mt30">
                  <div class="feature-list-box">
                     <img src="assets/images/af1.png" class="img-fluid mx-auto d-block icon-position">
                     <p class="description">Autoplay Videos in All Browsers</p>
                  </div>
               </div>
               <div class="col-12 col-md-4 mt30">
                  <div class="feature-list-box">
                     <img src="assets/images/af2.png" class="img-fluid mx-auto d-block icon-position">
                     <p class="description">Manage Videos in Playlists Effortlessly</p>
                  </div>
               </div>
               <div class="col-12 col-md-4 mt30">
                  <div class="feature-list-box">
                     <img src="assets/images/af3.png" class="img-fluid mx-auto d-block icon-position">
                     <p class="description">Video A-B Repeat Functionality To Replay Videos For Specified Time</p>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-12 col-md-4 mt30">
                  <div class="feature-list-box">
                     <img src="assets/images/af4.png" class="img-fluid mx-auto d-block icon-position">
                     <p class="description">Full Feature Drag and Drop Editor To Edit Templates</p>
                  </div>
               </div>
               <div class="col-12 col-md-4 mt30">
                  <div class="feature-list-box">
                     <img src="assets/images/af5.png" class="img-fluid mx-auto d-block icon-position">
                     <p class="description">100% Mobile Responsive Video Pages And Players</p>
                  </div>
               </div>
               <div class="col-12 col-md-4 mt30">
                  <div class="feature-list-box">
                     <img src="assets/images/af6.png" class="img-fluid mx-auto d-block icon-position">
                     <p class="description">Unmatched Video Player Customization</p>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-12 col-md-4 mt30">
                  <div class="feature-list-box">
                     <img src="assets/images/af7.png" class="img-fluid mx-auto d-block icon-position">
                     <p class="description">Smooth Playback On All Devices & Browsers</p>
                  </div>
               </div>
               <div class="col-12 col-md-4 mt30">
                  <div class="feature-list-box">
                     <img src="assets/images/af8.png" class="img-fluid mx-auto d-block icon-position">
                     <p class="description">Advanced Integration With 1000+ Marketing Apps</p>
                  </div>
               </div>
               <div class="col-12 col-md-4 mt30">
                  <div class="feature-list-box">
                     <img src="assets/images/af9.png" class="img-fluid mx-auto d-block icon-position">
                     <p class="description">Advanced Advertisement Technology</p>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-12 col-md-4 mt30">
                  <div class="feature-list-box">
                     <img src="assets/images/af10.png" class="img-fluid mx-auto d-block icon-position">
                     <p class="description">Maximize Visitor Engagement with Ad-Free Videos</p>
                  </div>
               </div>
               <div class="col-12 col-md-4 mt30">
                  <div class="feature-list-box">
                     <img src="assets/images/af10.png" class="img-fluid mx-auto d-block icon-position">
                     <p class="description">Custom Domain</p>
                  </div>
               </div>
               <div class="col-12 col-md-4 mt30">
                  <div class="feature-list-box">
                     <img src="assets/images/af12.png" class="img-fluid mx-auto d-block icon-position">
                     <p class="description">MyDrive to Store Your Media Securely & Share Faster with Your Clients</p>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-12 col-md-4 mt30">
                  <div class="feature-list-box">
                     <img src="assets/images/af13.png" class="img-fluid mx-auto d-block icon-position">
                     <p class="description">Get More Leads with Premium Lead Generation Templates</p>
                  </div>
               </div>
               <div class="col-12 col-md-4 mt30">
                  <div class="feature-list-box">
                     <img src="assets/images/af14.png" class="img-fluid mx-auto d-block icon-position">
                     <p class="description">Stunning Promo Templates for Extra Monetization & Traffic</p>
                  </div>
               </div>
               <div class="col-12 col-md-4 mt30">
                  <div class="feature-list-box">
                     <img src="assets/images/af15.png" class="img-fluid mx-auto d-block icon-position">
                     <p class="description">Accounts System to Track Complete Leads Behaviour</p>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-12 col-md-4 mt30">
                  <div class="feature-list-box">
                     <img src="assets/images/af16.png" class="img-fluid mx-auto d-block icon-position">
                     <p class="description">Hassle-Free Video Management</p>
                  </div>
               </div>
               <div class="col-12 col-md-4 mt30">
                  <div class="feature-list-box">
                     <img src="assets/images/af17.png" class="img-fluid mx-auto d-block icon-position">
                     <p class="description">Comment Management System to Enhance Brand Reputation</p>
                  </div>
               </div>
               <div class="col-12 col-md-4 mt30">
                  <div class="feature-list-box">
                     <img src="assets/images/af18.png" class="img-fluid mx-auto d-block icon-position">
                     <p class="description">Newbie Friendly</p>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-12 col-md-4 mt30">
                  <div class="feature-list-box">
                     <img src="assets/images/af19.png" class="img-fluid mx-auto d-block icon-position">
                     <p class="description">Capture Unlimited Leads From Videos</p>
                  </div>
               </div>
               <div class="col-12 col-md-4 mt30">
                  <div class="feature-list-box">
                     <img src="assets/images/af20.png" class="img-fluid mx-auto d-block icon-position">
                     <p class="description">A To Z Complete Video Training Included</p>
                  </div>
               </div>
               <div class="col-12 col-md-4 mt30">
                  <div class="feature-list-box">
                     <img src="assets/images/af21.png" class="img-fluid mx-auto d-block icon-position">
                     <p class="description">50+ More Cool Features Included</p>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- DEMO SECTION START -->
      <div class="demo-sec">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-md-50 f-28 w700 lh140 text-center">
                     <span class="red-gradient">Watch The Demo</span>
                     <br class="d-none d-md-block">
                     Discover How Easy & Powerful It Is
                  </div>
               </div>
               <div class="col-12 col-md-8 mx-auto mt-md40 mt20">
                  <!-- <img src="assets/images/demo-video-poster.png" class="img-fluid d-block mx-auto"> -->
                     <div class="video-box-demo">
                        <div class="responsive-video">
                           <iframe class="embed-responsive-item" src="https://vidhostpeo.oppyo.com/video/embed/VHP_Demo_Video" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                              box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                        </div>
                     </div>
               </div>
            </div>
         </div>
      </div>
      <!-- DEMO SECTION END -->
      
      <!-- POTENTIAL SECTION START -->
      <div class="potential-sec">
         <div class="container">
            <div class="row">
            
               <div class="col-12 text-center">
                  <div class="f-md-50 f-28 w600 text-center">
                  Here’s A Never-Ending List Of Businesses Ready To Pay Your Customers For Their VIDEO Services
                  </div>
               </div>
            </div>
            <div class="row rows-cols-2 row-cols-md-6 mt20 mt-md50 gap30">
               <div class="col">
                  <div class="feature-list-box-2">
                     <img src="assets/images/n1.webp" class="img-fluid mx-auto d-block">
                     <p class="description">Business Coaches</p>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box-2">
                     <img src="assets/images/n2.webp" class="img-fluid mx-auto d-block">
                     <p class="description">Affiliate Marketers</p>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box-2">
                     <img src="assets/images/n3.webp" class="img-fluid mx-auto d-block">
                     <p class="description">E-Com Sellers</p>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box-2">
                     <img src="assets/images/n4.webp" class="img-fluid mx-auto d-block">
                     <p class="description">Freelancers /Agency</p>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box-2">
                     <img src="assets/images/n5.webp" class="img-fluid mx-auto d-block">
                     <p class="description">Influencers</p>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box-2">
                     <img src="assets/images/n6.webp" class="img-fluid mx-auto d-block">
                     <p class="description">Video Marketers</p>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box-2">
                     <img src="assets/images/n7.webp" class="img-fluid mx-auto d-block">
                     <p class="description">Sports Clubs</p>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box-2">
                     <img src="assets/images/n8.webp" class="img-fluid mx-auto d-block">
                     <p class="description">Bars</p>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box-2">
                     <img src="assets/images/n9.webp" class="img-fluid mx-auto d-block">
                     <p class="description">Restaurants</p>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box-2">
                     <img src="assets/images/n10.webp" class="img-fluid mx-auto d-block">
                     <p class="description">Hotels</p>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box-2">
                     <img src="assets/images/n11.webp" class="img-fluid mx-auto d-block">
                     <p class="description">Schools</p>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box-2">
                     <img src="assets/images/n12.webp" class="img-fluid mx-auto d-block">
                     <p class="description">Churches</p>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box-2">
                     <img src="assets/images/n13.webp" class="img-fluid mx-auto d-block">
                     <p class="description">Taxi Services</p>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box-2">
                     <img src="assets/images/n14.webp" class="img-fluid mx-auto d-block">
                     <p class="description">All Info</p>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box-2">
                     <img src="assets/images/n15.webp" class="img-fluid mx-auto d-block">
                     <p class="description">Dentists</p>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box-2">
                     <img src="assets/images/n16.webp" class="img-fluid mx-auto d-block">
                     <p class="description">Carpenters</p>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box-2">
                     <img src="assets/images/n17.webp" class="img-fluid mx-auto d-block">
                     <p class="description">Chiropractors</p>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box-2">
                     <img src="assets/images/n18.webp" class="img-fluid mx-auto d-block">
                     <p class="description">LockSmiths</p>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box-2">
                     <img src="assets/images/n19.webp" class="img-fluid mx-auto d-block">
                     <p class="description">Home Tutors</p>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box-2">
                     <img src="assets/images/n20.webp" class="img-fluid mx-auto d-block">
                     <p class="description">Real-Estate</p>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box-2">
                     <img src="assets/images/n21.webp" class="img-fluid mx-auto d-block">
                     <p class="description">Motor Garage</p>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box-2">
                     <img src="assets/images/n22.webp" class="img-fluid mx-auto d-block">
                     <p class="description">Lawyers</p>
                  </div>
               </div>
               
               <div class="col">
                  <div class="feature-list-box-2">
                     <p class="description">AND Basically EVERY BUSINESS...</p>
                  </div>
               </div>
            </div>
               <div class="col-12 mt30 mt-md50">
                  <div class="f-18 f-md-20 lh140 w600 black-clr text-center">
                  These are just a few examples, but Video Marketing is the Present & Future and Every Business Needs this.
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- POTENTIAL SECTION END -->
      <div class="deep-funnel" id="funnel">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-50 f-28 w600 text-center lh140">
               Our Deep & High Converting Sales Funnel
               </div>
               <div class="col-12 col-md-12 mt20 mt-md70">
                  <div>
                     <img src="assets/images/funnel.webp" class="img-fluid d-block mx-auto">
                  </div>
               </div>
            </div>
         </div>
      </div>
        <!-- Header Section End -->
        <!-- <div class="form-section">
         <div class="container-xxl">
            <div class="row">
               <div class="col-12 col-md-12">
                  <div class="auto_responder_form inp-phold formbg col-12">
                     <div class="f-md-32 f-24 d-block mb0 lh140 w600 text-center white-clr">
                        <span class="w700">Subscribe To Our JV List</span> and Be The First to Know Our<br class="d-none d-md-block">
                        Special Contest, Events and Discounts
                     </div>
                     <!-- Aweber Form Code -->
                     <!--<div class="mt15 mt-md50">
                        <form method="post" class="af-form-wrapper mb-md5" accept-charset="UTF-8" action="https://www.aweber.com/scripts/addlead.pl"  >
                           <div style="display: none;">
                              <input type="hidden" name="meta_web_form_id" value="905695664" />
                              <input type="hidden" name="meta_split_id" value="" />
                              <input type="hidden" name="listname" value="awlist6273250" />
                              <input type="hidden" name="redirect" value="#" id="#" />
                              <input type="hidden" name="meta_adtracking" value="Viddeyo_JV_Signup" />
                              <input type="hidden" name="meta_message" value="1" />
                              <input type="hidden" name="meta_required" value="name,email" />
                              <input type="hidden" name="meta_tooltip" value="" />
                           </div>
                           <div id="af-form-905695664" class="af-form">
                              <div id="af-body-905695664" class="af-body af-standards row justify-content-center">
                                 <div class="af-element col-md-4">
                                    <label class="previewLabel" for="awf_field-114140182" style="display:none;">Name: </label>
                                    <div class="af-textWrap mb15 mb-md15 input-type">
                                       <input id="awf_field-114140182" class="frm-ctr-popup form-control input-field" type="text" name="name" placeholder="Your Name" class="text" value=""  onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="500" />
                                    </div>
                                    <div class="af-clear"></div>
                                 </div>
                                 <div class="af-element mb15 mb-md25  col-md-4">
                                    <label class="previewLabel" for="awf_field-114140183" style="display:none;">Email: </label>
                                    <div class="af-textWrap input-type">
                                       <input class="text frm-ctr-popup form-control input-field" id="awf_field-114140183" type="text" name="email" placeholder="Your Email" value=""  tabindex="501" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " />
                                    </div>
                                    <div class="af-clear"></div>
                                 </div>
                                 <div class="af-element buttonContainer button-type form-btn white-clr col-md-4">
                                    <input name="submit" class="submit f-20 f-md-24 white-clr center-block" type="submit" value="Subscribe For JV Updates" tabindex="502" />
                                    <div class="af-clear"></div>
                                 </div>
                              </div>
                           </div>
                           <div style="display: none;"><img src="https://forms.aweber.com/form/displays.htm?id=nAysbJysbGws" alt="" /></div>
                        </form>
                        <!-- Aweber Form Code -->
                    <!-- </div>
                  </div>
               </div>
              
            </div>
         </div>
      </div> -->
      <!-- Header Section End -->
      <!-- exciting-launch -->
      <div class="exciting-launch">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-md-47 f-28 black-clr text-center w700 lh140">
This is Black Friday Special Deal With Big Bang Prelaunch Webinar
                  </div>
               </div>
               <div class="col-12 mt-md60 mt30">
                  <div class="row align-items-center">
                     <div class="col-md-6 col-12 order-md-2">
                        <img src="assets/images/phase1.webp" class="img-fluid d-block mx-auto" />
                     </div>
                     <div class="col-md-6 col-12 p-md0 mt-md0 mt20 order-md-1 black-clr">
                        <div class="f-md-34 f-24 lh140 w700">
                        To Make You Max Commissions
                        </div>
                        <ul class="launch-list f-md-20 f-18 w400 mb0 mt-md30 mt20 pl20">
                           <li>All Leads Are Hardcoded</li>
                           <li>We'll Re-Market Your Leads Heavily</li>
                           <li>Pitch Bundle Offer On Webinars</li>
                        </ul>
                     </div>
                  </div>
                  <div class="row mt-md50 mt40 align-items-center">
                     <div class="col-md-6 col-12">
                        <img src="assets/images/phase2.webp" class="img-fluid d-block mx-auto" />
                     </div>
                     <div class="col-md-6 col-12 mt-md0 mt20 black-clr">
                        <div class="f-md-34 f-24 lh140 w700">
                        Big Contest & Bundle Offer
                        </div>
                        <ul class="launch-list f-md-20 f-18 w400 mb0 mt-md30 mt20 pl20">
                           <li>High in Demand Product with Top Conversion</li>
                           <li>Deep Funnel To Make You Double Digit EPCs</li>
                           <li>Earn Up To $415/Sale</li>
                           <li>Huge $10,000 JV Prizes</li>
                           <!-- <li>We'll Re-Market Your Visitors Heavily</li> -->
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- exciting-launch end -->
      <div class="prize-value">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-50 f-28 w600 text-center white-clr lh140">
                  Get Ready to Grab Your Share of
               </div>
               <div class="col-12 f-md-72 f-40 w800 text-center red-gradient lh140">
                  Big JV Prizes
               </div>
            </div>
         </div>
      </div>
      <div class="prize-section">
         <div class="container">
            <div class="prize-inner-sec">
               
               <div class="row mt20 mt-md90">
                  <div class="col-md-6">
                     <img src="assets/images/prelaunch.webp" alt="PreLaunch" class="d-block img-fluid mx-auto">
                  </div>
                  <div class="col-md-6 mt20 mt-md0">
                     <img src="assets/images/exiciting-launch.webp" alt="Exiciting Launch" class="d-block img-fluid mx-auto">
                  </div>
               </div>
            </div>   
         </div>
         <!--<img src="assets/images/businessman.webp" alt="Businessman" class="img-fluid d-none d-md-block ele4">-->
      </div>
     
      <div class="reciprocate-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-md-50 f-28 lh140 w600 black-clr">
                     Our Solid Track Record of <br class="d-lg-block d-none"> <span class="red-gradient"> Launching Top Converting Products</span>
                  </div>
               </div>
               <div class="col-12 col-md-12 mt20">
                  <img src="assets/images/product-logo.webp" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 text-center f-md-45 f-28 lh140 w700 white-clr mt20 mt-md70 red-gradient">
                  Do We Reciprocate?
               </div>
               <div class="col-12 f-18 f-md-18 lh140 mt20 w400 text-center black-clr">
               We've been in top positions on hundreds of launch leaderboards & sent huge sales for our valued JVs. <br><br>  
               So, if you have a top-notch product with top conversions AND that fits our list, we would love to drive loads of sales for you. Here's just some results from our recent promotions.
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
                  <div class="logos-effect">
                     <img src="assets/images/logos.webp" class="img-fluid d-block mx-auto">
                  </div>
               </div>
               <div class="col-12 f-md-28 f-24 lh140 mt20 mt-md50 w600 text-center">
                  And The List Goes On And On...
               </div>
            </div>
         </div>
      </div>
     <div class="contact-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="contact-block">
                     <div class="row">
                        <div class="col-12 mb20 mb-md50">
                           <div class="f-md-45 f-28 w700 lh140 text-center white-clr">
                             <span class="orange-gradient">Have any Query?</span>   Contact us Anytime
                           </div>
                        </div>
                           </div>   
                        <!-- <div class="row"> -->
                           <!-- <div class="col-12 col-md-12 mx-auto"> -->
                              <div class="row gx-md-5">
                              <div class="col-md-3 col-12 text-center">
                                    <div class="contact-shape">
                                       <img src="assets/images/todd_gross.webp" class="img-fluid d-block mx-auto minus">
                                       <div class="f-24 f-md-30 w600  lh140 text-center white-clr mt20">
                                       Todd Gross
                                       </div><br><br><br>
                                       <!-- <div class="f-14 w400 lh140 text-center white-clr">
                                          
                                       </div> -->
                                       <!-- <div class="col-12 mt30 d-flex justify-content-center">
                                          <a href="http://facebook.com/Dr.AmitPareek" class="link-text mr20">
                                          <img src="assets/images/am-fb.webp" class="center-block">
                                          </a>
                                          <a href="skype:amit.pareek77" class="link-text">
                                             <div class="col-12 ">
                                                <img src="assets/images/am-skype.webp" class="center-block">
                                             </div>
                                          </a>
                                       </div> -->
                                    </div>
                                 </div>
                                 <div class="col-md-3 col-12 text-center mt20 mt-md0">
                                    <div class="contact-shape">
                                       <img src="assets/images/amit-pareek.webp" class="img-fluid d-block mx-auto minus">
                                       <div class="f-24 f-md-30 w600  lh140 text-center white-clr mt20">
                                          Dr Amit Pareek
                                       </div>
                                       <!-- <div class="f-14 w400 lh140 text-center white-clr">
                                          (Techpreneur &amp; Marketer)
                                       </div> -->
                                       <div class="col-12 mt30 d-flex justify-content-center">
                                          <a href="http://facebook.com/Dr.AmitPareek" class="link-text mr20">
                                          <img src="assets/images/am-fb.webp" class="center-block">
                                          </a>
                                          <a href="skype:amit.pareek77" class="link-text">
                                             <div class="col-12 ">
                                                <img src="assets/images/am-skype.webp" class="center-block">
                                             </div>
                                          </a>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-md-3 col-12 text-center mt20 mt-md0">
                                    <div class="contact-shape">
                                       <img src="assets/images/luann_beckman.webp" class="img-fluid d-block mx-auto minus pb15">
                                       <div class="f-24 f-md-28 w600 lh140 text-center white-clr mt10 ">
                                       LuAnn Beckman
                                       </div>
                                       <!-- <div class="f-14 w400 lh140 text-center white-clr">
                                       
                                       </div> -->
                                       <div class="col-12 mt30 d-flex justify-content-center">
                                          <!-- <a href="#" class="link-text mr20">
                                          <img src="assets/images/am-fb.webp" class="center-block">
                                          </a> -->
                                          <a href="skype:plan4it" class="link-text">
                                             <div class="col-12 ">
                                                <img src="assets/images/am-skype.webp" class="center-block">
                                             </div>
                                          </a>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-md-3 col-12 text-center mt20 mt-md0">
                                    <div class="contact-shape">
                                       <img src="assets/images/atul-parrek.webp" class="img-fluid d-block mx-auto minus">
                                       <div class="f-24 f-md-30 w600  lh140 text-center white-clr mt20">
                                          Atul Pareek
                                       </div>
                                       <!-- <div class="f-14 w400 lh140 text-center white-clr">
                                          (Entrepreneur &amp; Product Creator)
                                       </div> -->
                                       <div class="col-12 mt30 d-flex justify-content-center">
                                          <a href="https://www.facebook.com/profile.php?id=100076904623319" class="link-text mr20">
                                          <img src="assets/images/am-fb.webp" class="center-block">
                                          </a>
                                          <a href="skype:live:.cid.c3d2302c4a2815e0" class="link-text">
                                             <div class="col-12 ">
                                                <img src="assets/images/am-skype.webp" class="center-block">
                                             </div>
                                          </a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           <!-- </div> -->
                        <!-- </div> -->
                     <!-- </div> -->
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="terms-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-50 f-28 w600 lh140 text-center">
               Affiliate Promotions Terms & Conditions
               </div>
               <div class="col-md-12 col-12 f-18 lh140 p-md0 mt-md20 mt20 w400 text-center">
               YOU MUST READ AND AGREE TO THESE AFFILIATE TERMS before requesting your affiliate link and being a part of this launch. Violation of ANY of these terms is cause for immediate termination and instant removal from this launch and any other of our launches - current or past -- and you agree that your current commissions will be forfeited without recourse and you may be banned from our future launches. Some violations may also be cause for LEGAL ACTIONS.:
               </div>
               <div class="col-md-12 col-12 px-md-0 mt15 mt-md50">
                  <ul class="terms-list pl0 m0 f-16 lh140 w400">
                     <li>1). All email contacts MUST be your OWN opt in email list. You cannot send to lists that have been purchased or “gifted” from other vendors, buy solo ads, use safe lists, or obtained by illegal means. Email lists that are not your own are considered spam.</li>
                     <li>2) You may NOT create social media pages NOR purchase domain names with the PRODUCT NAME or BRAND NAME AND you may NOT use the PRODUCT NAME or the NAME OF THE VENDOR as your “from” in your emails instead of your own name AS IF YOU ARE THE PRODUCT OWNER. This is IMPERSONATION and will not be tolerated.</li>
                     <li>3) You may NOT purchase domain name(s) with the same or similar name as the PRODUCT NAME or BRAND NAME nor CLONE or otherwise copy our site and use that site to sell our product as your own. Furthermore, you may not add our product - whether purchased through us or obtained in any other manner - and sell or offer it on any type of “membership” site where multiple people have access to this product for any kind of fee or arrangement. This constitutes theft of our intellectual property rights and considered FRAUDULENT and is cause for LEGAL ACTIONS. </li>
                     <li>4) You may not encourage nor ask for or show a person HOW TO REFUND their purchase from another affiliate in order for them to purchase the same product through you.</li>
                     <li>5) You may not post OTO links on Review Sites because this will lead to confusion and refunds for those people who do not purchase the FE first and end up with NO LOGIN & NO software or main product. </li>
                     <li>6) You may not use "negative" campaigns such as "is Product Name / Owner Name a scam?" or any other method to attract controversial click thru rates that an ordinary person would deem to portray a negative view of the product. You may not use offensive nor negative domain names.</li>
                     <li>7) You may not use misleading claims, inaccurate information or false testimonials (or anything that does not comply with FTC guidelines).</li>
                     <li>8) You may not use gray-hat/black-hat marketing practices to drive sales or for any other reason.</li>
                     <li>9) You may not give cash rebates of any kind as it may increase refund rates.</li>
                     <li>10) You may not purchase from your own affiliate link. Any 'self' purchase commission may be nullified or held back.</li>
                  </ul>
               </div>
               <div class="col-md-12 col-12 f-18 lh140 p-md0 mt-md20 mt20 w400">
               <span class="w600">NOTE:</span> These terms may change at any time without notice. (Please check back here regularly).<br>
               <span class="w600">NOTE:</span> Affiliate payments will be set according to the platform rules.
                  <br><br>

               <span class="w600">CAUTION:</span> Do not send "raw" affiliate links. Utilize redirect links in emails & website campaigns instead of your direct affiliate link. This increases conversions for both of us.
               <br><br>
               We run a legitimate business, which means that we always correctly illustrate and represent our products and their features and benefits to the customer.
               <br><br>
               Please make sure you do the same.
               </div>
            </div>
         </div>
      </div>
      <!--Footer Section Start -->
      <div class="footer-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
               <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 829.07 133.22" style="max-height:55px">
               <defs>
               <style>.cls-1{fill:#fff;}.cls-2{fill:#ec1c24;}</style>
               </defs>
               <g id="Layer_2" data-name="Layer 2">
               <g id="Layer_1-2" data-name="Layer 1">
               <path class="cls-1" d="M142.09,14.94C89.5-8.93,68.3,32,65.87,37,48.19,38.59,36.52,52.3,32.31,67.64l-1.55-.54c-2.87-1-7-2.8-9.65-5.12a51.82,51.82,0,0,1,6.14-12h0a49.93,49.93,0,0,1,3.41-4.41c9.9-11.41,22.44-15.6,26.3-15.6C72.85.51,95.07,0,99.08,0,126.45-.2,142.09,14.94,142.09,14.94Z"/>
               <path class="cls-1" d="M103.29,128.84c-21.44,11.73-43.39-3.34-47.63-7.58-25.5-.6-35.11-20.89-37.43-32.69a121.37,121.37,0,0,0,14.3,4.56C36,103.65,44.31,111.7,58,112.66c23.66,18.51,45.7,7.08,45.7,7.08a40.15,40.15,0,0,0,23.47,3,56.14,56.14,0,0,0,22.25-10.11s11.38,1,18-6.81c3.34-3.95,6.36-7.52,8.22-12.5h0a114.47,114.47,0,0,0,13.44-4.5c-3.45,17.54-17.34,32.81-37.25,32.81-14,13.87-37.41,11.7-45.2,8.61"/>
               <path class="cls-1" d="M185.47,61.68a37.61,37.61,0,0,1-3.63,2.23,54,54,0,0,1-6.43,2.93c-6-14-24.8-19.71-24.8-19.71s-10.49-17.19-27.1-23.39c-16.26-6.08-41.44,1-42,1.1.49-.42,21.75-18.55,53.83-6,14.36,6.17,21.74,21.44,21.74,21.44a35.85,35.85,0,0,1,5,1.56,49.15,49.15,0,0,1,9.36,4.73h0A40.26,40.26,0,0,1,185.47,61.68Z"/>
               <path class="cls-2" d="M77,80.25V94c-17.82-1.23-33.48-3.67-46-7.14a100,100,0,0,1-13.4-4.67C6.24,77.22-.2,70.89,0,63.51,1.47,42.92,28.58,42.83,29,42.83c-.25.1-16.62,6.85-16.17,15.07.19,3.59,2.24,6.93,6.53,9.9a44.79,44.79,0,0,0,11.74,5.42C41.36,76.53,56.31,79,77,80.25Z"/>
               <path class="cls-2" d="M189.87,82.3A99,99,0,0,1,177.23,87c-11,3.29-25.29,5.89-43.6,7.12l11.78-7a8.27,8.27,0,0,0,4.06-7.16,7.84,7.84,0,0,0-.08-1.19c10.93-1.5,20.22-3.65,27.61-6.43a58.75,58.75,0,0,0,7.56-3.4c1.15-.63,2.21-1.26,3.18-1.9,16-10.49,6.87-21.31-22.83-28,36.12,7.83,43.62,16.49,44.7,22.28C210.57,66.47,205.7,75.09,189.87,82.3Z"/>
               <path class="cls-2" d="M142.5,82.2l-21,12.47-9.94,5.93-24.9,14.81a2.65,2.65,0,0,1-4-2.28V46.69a2.69,2.69,0,0,1,2-2.56l.68-.05.69,0,.68.29,50.33,30,5.48,3.27a2.66,2.66,0,0,1,0,4.57Z"/>
               <path class="cls-1" d="M267.45,89.27l18.83-62.49h19.06l-28.93,83.09H258.54L229.72,26.78h19Z"/>
               <path class="cls-1" d="M312,32.14a8.12,8.12,0,0,1,2.48-6.1,10.69,10.69,0,0,1,13.5,0,8.07,8.07,0,0,1,2.51,6.1,8.09,8.09,0,0,1-2.54,6.16,10.57,10.57,0,0,1-13.41,0A8.09,8.09,0,0,1,312,32.14Zm17.52,77.73H312.93V48.12h16.55Z"/>
               <path class="cls-1" d="M340.49,78.54q0-14.45,6.48-23T364.69,47a18.92,18.92,0,0,1,14.9,6.73V22.21h16.55v87.66h-14.9l-.8-6.56a19.41,19.41,0,0,1-15.86,7.7,20.94,20.94,0,0,1-17.49-8.59Q340.49,93.84,340.49,78.54ZM357,79.77q0,8.69,3,13.31a9.85,9.85,0,0,0,8.79,4.63q7.65,0,10.79-6.45V66.85q-3.09-6.46-10.67-6.46Q357,60.39,357,79.77Z"/>
               <path class="cls-1" d="M477.63,109.87H460.51V74.26H427.13v35.61H410V26.78h17.13V60.45h33.38V26.78h17.12Z"/>
               <path class="cls-1" d="M488.93,78.43a36.59,36.59,0,0,1,3.54-16.38,25.81,25.81,0,0,1,10.19-11.13A29.67,29.67,0,0,1,518.09,47q12.51,0,20.41,7.65t8.81,20.77l.12,4.22q0,14.22-7.93,22.8T518.21,111q-13.35,0-21.32-8.56t-8-23.28Zm16.5,1.17q0,8.79,3.31,13.46a11.9,11.9,0,0,0,18.83,0q3.36-4.59,3.37-14.71,0-8.64-3.37-13.38a11,11,0,0,0-9.48-4.74A10.75,10.75,0,0,0,508.74,65C506.53,68.14,505.43,73,505.43,79.6Z"/>
               <path class="cls-1" d="M591,92.81A5.32,5.32,0,0,0,588,88q-3-1.74-9.61-3.11-22-4.62-22-18.72a16.94,16.94,0,0,1,6.82-13.72Q570,47,581,47q11.76,0,18.81,5.54a17.45,17.45,0,0,1,7,14.38H590.4a8,8,0,0,0-2.28-5.85q-2.28-2.31-7.14-2.31a9.83,9.83,0,0,0-6.44,1.88,5.91,5.91,0,0,0-2.29,4.79,5.1,5.1,0,0,0,2.6,4.43q2.6,1.68,8.76,2.91A73,73,0,0,1,594,75.51q13.07,4.8,13.07,16.61,0,8.44-7.25,13.67T581.1,111a32.54,32.54,0,0,1-13.78-2.77,23,23,0,0,1-9.45-7.59,17.62,17.62,0,0,1-3.42-10.41h15.63A8.62,8.62,0,0,0,573.34,97a12.87,12.87,0,0,0,8.1,2.34q4.74,0,7.16-1.8A5.57,5.57,0,0,0,591,92.81Z"/>
               <path class="cls-1" d="M637.43,32.94V48.12H648v12.1H637.43V91c0,2.28.43,3.92,1.31,4.9s2.55,1.49,5,1.49a26,26,0,0,0,4.85-.4v12.5a33.89,33.89,0,0,1-10,1.48q-17.34,0-17.69-17.52V60.22h-9V48.12h9V32.94Z"/>
               <path class="cls-1" d="M675.38,80.59v29.28H658.26V26.78h32.41a37.58,37.58,0,0,1,16.47,3.42,25.29,25.29,0,0,1,10.93,9.73,27.23,27.23,0,0,1,3.82,14.35q0,12.22-8.36,19.27t-23.14,7Zm0-13.86h15.29q6.8,0,10.36-3.2t3.57-9.13a13.69,13.69,0,0,0-3.6-9.87q-3.6-3.78-9.93-3.88H675.38Z"/>
               <path class="cls-1" d="M767.26,63.59a44,44,0,0,0-5.94-.46q-9.36,0-12.27,6.33v40.41H732.56V48.12h15.58l.46,7.36q5-8.5,13.75-8.5a17.45,17.45,0,0,1,5.14.74Z"/>
               <path class="cls-1" d="M770.57,78.43a36.59,36.59,0,0,1,3.54-16.38,25.85,25.85,0,0,1,10.18-11.13A29.7,29.7,0,0,1,799.73,47q12.49,0,20.4,7.65T829,75.4l.12,4.22q0,14.22-7.94,22.8T799.85,111q-13.36,0-21.32-8.56t-8-23.28Zm16.49,1.17q0,8.79,3.31,13.46a11.91,11.91,0,0,0,18.84,0q3.36-4.59,3.36-14.71,0-8.64-3.36-13.38a11,11,0,0,0-9.48-4.74A10.78,10.78,0,0,0,790.37,65C788.17,68.14,787.06,73,787.06,79.6Z"/>
               </g>
               </g>
               </svg><br><br><br>
                  <!-- <div editabletype="text" class="f-16 f-md-16 w300 mt20 lh140 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div> -->
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-16 f-md-16 w300 lh140 white-clr text-xs-center">Copyright © VidHostPro</div>
                  <ul class="footer-ul w300 f-16 f-md-16 white-clr text-center text-md-right">
                     <li><a href="https://support.oppyo.com/hc/en-us" target="_blank" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://vidhostpro.co/legal/privacy-policy.html"  target="_blank" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://vidhostpro.co/legal/terms-of-service.html"  target="_blank" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://vidhostpro.co/legal/disclaimer.html"  target="_blank" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://vidhostpro.co/legal/gdpr.html"  target="_blank" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://vidhostpro.co/legal/dmca.html" target="_blank"  class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://vidhostpro.co/legal/anti-spam.html"  target="_blank" class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!--Footer Section End -->
      <!-- timer --->
      <?php
         if ($now < $exp_date) {
         
         ?>
      <script type="text/javascript">
         // Count down milliseconds = server_end - server_now = client_end - client_now
         var server_end = <?php echo $exp_date; ?> * 1000;
         var server_now = <?php echo time(); ?> * 1000;
         var client_now = new Date().getTime();
         var end = server_end - server_now + client_now; // this is the real end time
         
         var noob = $('.countdown').length;
         
         var _second = 1000;
         var _minute = _second * 60;
         var _hour = _minute * 60;
         var _day = _hour * 24
         var timer;
         
         function showRemaining() {
             var now = new Date();
             var distance = end - now;
             if (distance < 0) {
                 clearInterval(timer);
                 document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
                 return;
             }
         
             var days = Math.floor(distance / _day);
             var hours = Math.floor((distance % _day) / _hour);
             var minutes = Math.floor((distance % _hour) / _minute);
             var seconds = Math.floor((distance % _minute) / _second);
             if (days < 10) {
                 days = "0" + days;
             }
             if (hours < 10) {
                 hours = "0" + hours;
             }
             if (minutes < 10) {
                 minutes = "0" + minutes;
             }
             if (seconds < 10) {
                 seconds = "0" + seconds;
             }
             var i;
             var countdown = document.getElementsByClassName('countdown');
             for (i = 0; i < noob; i++) {
                 countdown[i].innerHTML = '';
         
                 if (days) {
                     countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-40 f-md-50 timerbg">' + days + '</span><br><span class="f-20 w600">Days</span> </div>';
                 }
         
                 countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-40 f-md-50 timerbg">' + hours + '</span><br><span class="f-20 w600">Hours</span> </div>';
         
                 countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-40 f-md-50 timerbg">' + minutes + '</span><br><span class="f-20 w600">Mins</span> </div>';
         
                 countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-40 f-md-50 timerbg">' + seconds + '</span><br><span class="f-20 w600">Sec</span> </div>';
             }
         
         }
         timer = setInterval(showRemaining, 1000);
      </script>
      <script type="text/javascript">
         // Special handling for in-app browsers that don't always support new windows
         (function() {
             function browserSupportsNewWindows(userAgent) {
                 var rules = [
                     'FBIOS',
                     'Twitter for iPhone',
                     'WebView',
                     '(iPhone|iPod|iPad)(?!.*Safari\/)',
                     'Android.*(wv|\.0\.0\.0)'
                 ];
                 var pattern = new RegExp('(' + rules.join('|') + ')', 'ig');
                 return !pattern.test(userAgent);
             }
         
             if (!browserSupportsNewWindows(navigator.userAgent || navigator.vendor || window.opera)) {
                 document.getElementById('af-form-905695664').parentElement.removeAttribute('target');
             }
         })();
      </script><script type="text/javascript">
         <!--
         (function() {
             var IE = /*@cc_on!@*/false;
             if (!IE) { return; }
             if (document.compatMode && document.compatMode == 'BackCompat') {
                 if (document.getElementById("af-form-905695664")) {
                     document.getElementById("af-form-905695664").className = 'af-form af-quirksMode';
                 }
                 if (document.getElementById("af-body-905695664")) {
                     document.getElementById("af-body-905695664").className = "af-body inline af-quirksMode";
                 }
                 if (document.getElementById("af-header-905695664")) {
                     document.getElementById("af-header-905695664").className = "af-header af-quirksMode";
                 }
                 if (document.getElementById("af-footer-905695664")) {
                     document.getElementById("af-footer-905695664").className = "af-footer af-quirksMode";
                 }
             }
         })();
         -->
      </script>
      <?php
         } else {
         echo "Times Up";
         }
         ?>
      <!--- timer end-->
      <script id="rendered-js" >
         $('.bxslider.text').bxSlider({
           mode: 'vertical',
           pager: false,
           controls: false,
           infiniteLoop: true,
           auto: true,
           speed: 300,
           pause: 2000 });
                
      </script>
   </body>
</html>
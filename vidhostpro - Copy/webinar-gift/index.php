<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<!-- Tell the browser to be responsive to screen width -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="title" content="VidHostPro Webinar Gift">
    <meta name="description" content="VidHostPro Webinar Gift">
    <meta name="keywords" content="Revealing A BLAZING-FAST Video Hosting, Player & Marketing Technology With No Monthly Fee Ever">
    <meta property="og:image" content="https://www.vidhostpro.co/special-bonus/thumbnail.png">
    <meta name="language" content="English">
    <meta name="revisit-after" content="1 days">
    <meta name="author" content="Dr. Amit Pareek">
    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:title" content="VidHostPro Webinar Gift">
    <meta property="og:description" content="Revealing A BLAZING-FAST Video Hosting, Player & Marketing Technology With No Monthly Fee Ever">
    <meta property="og:image" content="https://www.vidhostpro.co/special-bonus/thumbnail.png">
    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:title" content="VidHostPro Webinar Gift">
    <meta property="twitter:description" content="Revealing A BLAZING-FAST Video Hosting, Player & Marketing Technology With No Monthly Fee Ever">
    <meta property="twitter:image" content="https://www.vidhostpro.co/special-bonus/thumbnail.png">

	<title>VidHostPro Webinar Gift</title>
	<!-- Shortcut Icon  -->
	<link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
      <!-- Css CDN Load Link -->
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <link rel="stylesheet" href="../common_assets/css/bootstrap.min.css" type="text/css">
	<link rel="stylesheet" type="text/css" href="assets/css/bonus-style.css">
   <link rel="stylesheet" type="text/css" href="assets/css/style.css">
   <link rel="stylesheet" type="text/css" href="assets/css/timer.css">
   <script src="../common_assets/js/jquery.min.js"></script>

		
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-NMK2MBB');</script>
	<!-- End Google Tag Manager -->


</head>
<body>
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NMK2MBB"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) --> 

	<!-- New Timer  Start-->
<?php
         $date = 'Nov 27 2023 11:59 PM EST';
         $exp_date = strtotime($date);
         $now = time();  
         /*
         $date = date('F d Y g:i:s A eO');
         $rand_time_add = rand(700, 1200);
         $exp_date = strtotime($date) + $rand_time_add;
         $now = time();*/
         
         if ($now < $exp_date) {
         ?>
      <?php
         } else {
          echo "Timer Up";
         }
         ?>
	<!-- New Timer End -->

	<?php
		if(!isset($_GET['afflink'])){
		$_GET['afflink'] = 'https://vidhostpro.co/special/';
		$_GET['name'] = 'Dr. Amit Pareek';      
		}
	?>

	<div class="proudly-sec" id="product">
         <div class="container">
         <!-- <div class="row">
                <div class="col-12 text-center">
                    <div class="col-12 white-clr f-20 f-md-32">
                        Free Gifts by <span class="w600 red-gradient">Dr. Amit Pareek's</span> for SuperVIP Customers
                    </div>
                    <div class="uy col-12 mt20 mt-md40">
                        <div class="f-20 f-md-24 lh160 w500 black-clr">
                            <b class="f-20 f-md-24 red-gradient">Congratulations!</b> you have also <b><span class="red-gradient">unlocked Special Exclusive coupon <br class="d-none d-md-block">  "AMITVIP" $10 OFF</span></b> for my Lightning Fast Software <b><span class="red-gradient">"VidHostPro"</span></b> launch on <br class="d-none d-md-block"> <span class="red-gradient w700">03rd May 2023  @ 11:00 AM EST</span>
                        </div>

                        <div class="f-20 f-md-24 lh130 w600 mt20 gn-box">Good News :</div>

                        <div class="f-20 f-md-24 lh160 w400 mt20 mt-md30 black-clr">
                            If you want any other <b>BONUSES</b> that are relevant for your business <br class="d-none d-md-block"> or <b>any niche</b> to increase more of your profits then, <b>please feel free <br class="d-none d-md-block"> to email</b> us at <a href="https://support.oppyo.com/hc/en-us" target="_blank">https://support.oppyo.com/</a>
                        </div>
                    </div>
                </div>
            </div> -->
            <div class="row">
               
               <div class="col-12 mt-md30 mt20 text-center">
              <img src="assets/images/sale.webp" class="img-fluid mx-auto d-block">
               </div>
               <div class="col-12 mt30 mt-md50 white-clr text-center">
                   
                   <div class="col-12 f-md-42 f-28 w700 text-center white-clr lh150">                
                       <span class="red-gradient"> Host, Market, And Play Videos at Blazing-FAST Speed </span>With No Tech Hassles &amp; No Monthly Fee Ever…
                    </div>
                   <div class="f-md-22 f-18 w400 text-center lh150 mt30 white-clr">
                       Play Unlimited Sales Videos, E-Learning, Review, &amp; Demo Videos on Any Page and Device <br class="d-none d-md-block"> Beautifully with Zero Delays, Zero Traffic Leakage, &amp; Zero Disturbing Ads.
                   </div>
                   <div class="f-md-36 f-28 lh160 w700 mt30 white-clr">
				   You've Earned It - <br class="d-none d-md-block">
					Enjoy This Assure Gift For Attending Our Webinar!
                   </div>
                   <div class="mt10 f-20 f-md-32 lh160 ">
                       <span class="tde">Download Your Assure Gifts Below</span>
                   </div>
               </div>
               <div class="col-12 mt20 mt-md50">
                  <img src="assets/images/product-box.webp" class="img-fluid d-block mx-auto img-animation" alt="Proudly Presenting...">
               </div>
            </div>
         </div>
      
	<!-- Proudly Section End -->


	<!-- Bonus #1 Section Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-12 text-center">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">BONUS 1</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 col-12">
					  		<img src="assets/images/bonus1.webp" class="img-fluid mx-auto d-block" alt="Bonus1">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
					  		<div class="f-22 f-md-32 w600 lh150 bonus-title-color">The Rank Generator</div>
					  		<ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0 white-clr">
                        Do You Want To Increase Your Alexa Ranking By 50% In Just A Couple Of Days?
                        <br><br>
                        Increase your website ranking like never before. Rankbooster is GUARANTEED to increase your Website ranking by atleast 50%. Send well over 10,000 hits to your site per hour with this. Watch your Alexa ranking increase like you want it to.			
					  		</ul>
                       <div class="f-md-22 w700 lh160 d-btn">
                       <a href="https://super-vip-bonuses.dotcompal.co/doc/share/01QfEky9laKcwMSOeA3J" target="_blank">Download Now 🎁</a>
                        </div>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #1 Section End -->

	<!-- Bonus #2 Section Start -->
	<div class="section-bonus mt20 mt-md80">
	   	<div class="container highlight-bg">
		  	<div class="row">
				<div class="col-12 xstext1">
					<div class="bonus-title-black-bg">
					   <div class="f-22 f-md-28 lh120 w700 white-clr">BONUS 2</div>
					</div>
				</div>
				 
			 	<div class="col-12 mt20 mt-md30">
					<div class="row">
						<div class="col-md-5 col-12 order-md-2">
							<img src="assets/images/bonus2.webp" class="img-fluid mx-auto d-block" alt="Bonus2">
						</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
					  		<div class="f-22 f-md-32 w600 lh150 bonus-title-color mt20 mt-md0">
							  Profit Negotiator
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0 black-clr">
								
                        Learn How You Can Convert More Visitors Into Customers With Very Little Effort Using This Ethical Exit Traffic Technology!
								<br><br>
                        Internet Customers Are Getting Smart, Ignoring Sales Messages & Becoming "Immune" To Ads Altogether! So Many Online Business Owners Are Spending All Their Time On The Latest Conversion "Fads" Just To Keep Their Site From Going Under. Don't You Deserve More Profits And Higher Conversions On The Existing Traffic To Your Website?
								
							</ul>
                     <div class="f-md-22 w700 lh160 d-btn">
                     <a href="https://super-vip-bonuses.dotcompal.co/doc/share/GPV49vdytn16US0B2oXs" target="_blank">Download Now 🎁</a>
                        </div>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #2 Section End -->

	<!-- Bonus #3 Section Start -->
	<div class="section-bonus mt20 mt-md80">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-12 text-center">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">BONUS 3</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 col-12">
					  		<img src="assets/images/bonus3.webp" class="img-fluid mx-auto d-block " alt="Bonus3">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
					  		<div class="f-22 f-md-32 w600 lh150 bonus-title-color">
							  Blackmask Marketing
							</div>
					  		<ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0 white-clr">
                       Over 20 Million Users Already Have This Controversial Software Installed and Thousands More Are Installing It Each And Every Day! They Have Been Secretly Making BOATLOADS of CASH with Virtually NO Competition Until NOW!! NOW learn how to show THESE surfers YOUR ads at the EXACT MOMENT they are looking for it!
					  		</ul>
                       <div class="f-md-22 w700 lh160 d-btn">
                       <a href="https://super-vip-bonuses.dotcompal.co/doc/share/ReNOcHZFMagSlQrDn9BK" target="_blank">Download Now 🎁</a>
                        </div>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #3 Section End -->

	<!-- Bonus #4 Start -->
	<div class="section-bonus mt20 mt-md80">
	   	<div class="container highlight-bg">
		  	<div class="row">
			 	<div class="col-12 xstext1">
					<div class="bonus-title-black-bg">
				   		<div class="f-22 f-md-28 lh120 w700 white-clr">BONUS 4</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 col-12 order-md-2">
					  		<img src="assets/images/bonus4.webp" class="img-fluid mx-auto d-block " alt="Bonus4">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
					 	 	<div class="f-22 f-md-32 w600 lh150 bonus-title-color">
							  Fast Content Producer
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0 black-clr">
                       Discover How to Build Hundreds of Content Rich, Dynamically Changing, Keyword Covered Web Pages in Mere Minutes. Start Building the High-Quality Content Sites You Need to Succeed on the Internet Today!
                       <br><br>
                       This amazing software program holds the secret key to chopping up syndicated content and public domain works to help you win the search engine game, get higher rankings and bring in more traffic than you can ever hope to handle!
							</ul>
                     <div class="f-md-22 w700 lh160 d-btn">
                     <a href="https://super-vip-bonuses.dotcompal.co/doc/share/s3jyxXY0QgJPkpCiOT8c" target="_blank">Download Now 🎁</a>
                        </div>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #4 End -->

	<!-- Bonus #5 Start -->
	<div class="section-bonus mt20 mt-md80">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-12 text-center">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">BONUS 5</div>
					</div>
			 	</div>
				<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 col-12">
					  		<img src="assets/images/bonus5.webp" class="img-fluid mx-auto d-block " alt="Bonus5">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
					  		<div class="f-22 f-md-32 w600 lh150 bonus-title-color">
							  Iframe Plugin Generator
					  		</div>
							<ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0 white-clr">
                     Quickly Make And Brand Your Own Iframes!
                     <br>
                     This iFrame generator helps you to instantly create customized iFrames for your web site/blog. Fill in the fields below and within seconds you will receive the HTML code for your custom iFrame.
                     <br><br>
                     Simply follow the steps below to generate your iFrame code, and freely use it on your website blogs:- Go to generator page by clicking on “iFrame Generator” button below, Customize the settings as you need, Click on “Generate” button to get HTML code for iFrame,
							</ul>
                     <div class="f-md-22 w700 lh160 d-btn">
                     <a href="https://super-vip-bonuses.dotcompal.co/doc/share/q0sDZ1ASrv2bVtk9omJE" target="_blank">Download Now 🎁</a>
                        </div>
				   		</div>
					</div>
				</div>
			</div>
			<div class="row mt20 mt-md70">
               <!-- CTA Btn Section Start -->
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-22 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  
               </div>
               <div class="f-18 f-md-22 lh150 w400 text-center mt20 white-clr mt-md30">
                  Use Coupon Code <span class="w700 red-gradient">"BLACKFRIDAY"</span> for an Additional <span class="w700 red-gradient">30% Discount</span> on Bundle
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt10">
                  <a href="https://vidhostpro.co/bundle/" class="text-center bonusbuy-btn-bundle">
                  <span class="text-center">Grab VidHostPro Bundle + My 17 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               <div class="f-18 f-md-22 lh150 w400 text-center mt20 white-clr mt-md30">
                     Use Coupon Code <span class="w700 red-gradient">"BLACKFRIDAY"</span> for an Additional <span class="w700 red-gradient">30% Discount</span> on Commercial Licence
                  </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt10">
                  <a href="https://vidhostpro.co/special/" class="text-center bonusbuy-btn-bundle">
                  <span class="text-center">Grab VidHostPro + My 17 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
                  <img src="https://cdn.dotcompaltest.com/uploads/launches/vidhostpro/special-bonus/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md20" align="center">
                  <h3 class="f-md-35 f-md-28 f-20 w500 text-center white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-black"><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">06&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 smmltd">Days</span> </div><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">21&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">42&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div><div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">27</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div></div>
               </div>
               <!-- Timer End -->
               <!-- CTA Button Section End   -->
            </div>
		</div>					
	</div>
						
	
	<!-- Bonus #5 End -->

	</div>
		</div>
	<!-- Footer Section Start -->
	<div class="footer-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
               <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 829.07 133.22" style="max-height:55px">
                            <defs>
                            <style>.cls-1{fill:#fff;}.cls-2{fill:#ec1c24;}</style>
                            </defs>
                            <g id="Layer_2" data-name="Layer 2">
                            <g id="Layer_1-2" data-name="Layer 1">
                            <path class="cls-1" d="M142.09,14.94C89.5-8.93,68.3,32,65.87,37,48.19,38.59,36.52,52.3,32.31,67.64l-1.55-.54c-2.87-1-7-2.8-9.65-5.12a51.82,51.82,0,0,1,6.14-12h0a49.93,49.93,0,0,1,3.41-4.41c9.9-11.41,22.44-15.6,26.3-15.6C72.85.51,95.07,0,99.08,0,126.45-.2,142.09,14.94,142.09,14.94Z"></path>
                            <path class="cls-1" d="M103.29,128.84c-21.44,11.73-43.39-3.34-47.63-7.58-25.5-.6-35.11-20.89-37.43-32.69a121.37,121.37,0,0,0,14.3,4.56C36,103.65,44.31,111.7,58,112.66c23.66,18.51,45.7,7.08,45.7,7.08a40.15,40.15,0,0,0,23.47,3,56.14,56.14,0,0,0,22.25-10.11s11.38,1,18-6.81c3.34-3.95,6.36-7.52,8.22-12.5h0a114.47,114.47,0,0,0,13.44-4.5c-3.45,17.54-17.34,32.81-37.25,32.81-14,13.87-37.41,11.7-45.2,8.61"></path>
                            <path class="cls-1" d="M185.47,61.68a37.61,37.61,0,0,1-3.63,2.23,54,54,0,0,1-6.43,2.93c-6-14-24.8-19.71-24.8-19.71s-10.49-17.19-27.1-23.39c-16.26-6.08-41.44,1-42,1.1.49-.42,21.75-18.55,53.83-6,14.36,6.17,21.74,21.44,21.74,21.44a35.85,35.85,0,0,1,5,1.56,49.15,49.15,0,0,1,9.36,4.73h0A40.26,40.26,0,0,1,185.47,61.68Z"></path>
                            <path class="cls-2" d="M77,80.25V94c-17.82-1.23-33.48-3.67-46-7.14a100,100,0,0,1-13.4-4.67C6.24,77.22-.2,70.89,0,63.51,1.47,42.92,28.58,42.83,29,42.83c-.25.1-16.62,6.85-16.17,15.07.19,3.59,2.24,6.93,6.53,9.9a44.79,44.79,0,0,0,11.74,5.42C41.36,76.53,56.31,79,77,80.25Z"></path>
                            <path class="cls-2" d="M189.87,82.3A99,99,0,0,1,177.23,87c-11,3.29-25.29,5.89-43.6,7.12l11.78-7a8.27,8.27,0,0,0,4.06-7.16,7.84,7.84,0,0,0-.08-1.19c10.93-1.5,20.22-3.65,27.61-6.43a58.75,58.75,0,0,0,7.56-3.4c1.15-.63,2.21-1.26,3.18-1.9,16-10.49,6.87-21.31-22.83-28,36.12,7.83,43.62,16.49,44.7,22.28C210.57,66.47,205.7,75.09,189.87,82.3Z"></path>
                            <path class="cls-2" d="M142.5,82.2l-21,12.47-9.94,5.93-24.9,14.81a2.65,2.65,0,0,1-4-2.28V46.69a2.69,2.69,0,0,1,2-2.56l.68-.05.69,0,.68.29,50.33,30,5.48,3.27a2.66,2.66,0,0,1,0,4.57Z"></path>
                            <path class="cls-1" d="M267.45,89.27l18.83-62.49h19.06l-28.93,83.09H258.54L229.72,26.78h19Z"></path>
                            <path class="cls-1" d="M312,32.14a8.12,8.12,0,0,1,2.48-6.1,10.69,10.69,0,0,1,13.5,0,8.07,8.07,0,0,1,2.51,6.1,8.09,8.09,0,0,1-2.54,6.16,10.57,10.57,0,0,1-13.41,0A8.09,8.09,0,0,1,312,32.14Zm17.52,77.73H312.93V48.12h16.55Z"></path>
                            <path class="cls-1" d="M340.49,78.54q0-14.45,6.48-23T364.69,47a18.92,18.92,0,0,1,14.9,6.73V22.21h16.55v87.66h-14.9l-.8-6.56a19.41,19.41,0,0,1-15.86,7.7,20.94,20.94,0,0,1-17.49-8.59Q340.49,93.84,340.49,78.54ZM357,79.77q0,8.69,3,13.31a9.85,9.85,0,0,0,8.79,4.63q7.65,0,10.79-6.45V66.85q-3.09-6.46-10.67-6.46Q357,60.39,357,79.77Z"></path>
                            <path class="cls-1" d="M477.63,109.87H460.51V74.26H427.13v35.61H410V26.78h17.13V60.45h33.38V26.78h17.12Z"></path>
                            <path class="cls-1" d="M488.93,78.43a36.59,36.59,0,0,1,3.54-16.38,25.81,25.81,0,0,1,10.19-11.13A29.67,29.67,0,0,1,518.09,47q12.51,0,20.41,7.65t8.81,20.77l.12,4.22q0,14.22-7.93,22.8T518.21,111q-13.35,0-21.32-8.56t-8-23.28Zm16.5,1.17q0,8.79,3.31,13.46a11.9,11.9,0,0,0,18.83,0q3.36-4.59,3.37-14.71,0-8.64-3.37-13.38a11,11,0,0,0-9.48-4.74A10.75,10.75,0,0,0,508.74,65C506.53,68.14,505.43,73,505.43,79.6Z"></path>
                            <path class="cls-1" d="M591,92.81A5.32,5.32,0,0,0,588,88q-3-1.74-9.61-3.11-22-4.62-22-18.72a16.94,16.94,0,0,1,6.82-13.72Q570,47,581,47q11.76,0,18.81,5.54a17.45,17.45,0,0,1,7,14.38H590.4a8,8,0,0,0-2.28-5.85q-2.28-2.31-7.14-2.31a9.83,9.83,0,0,0-6.44,1.88,5.91,5.91,0,0,0-2.29,4.79,5.1,5.1,0,0,0,2.6,4.43q2.6,1.68,8.76,2.91A73,73,0,0,1,594,75.51q13.07,4.8,13.07,16.61,0,8.44-7.25,13.67T581.1,111a32.54,32.54,0,0,1-13.78-2.77,23,23,0,0,1-9.45-7.59,17.62,17.62,0,0,1-3.42-10.41h15.63A8.62,8.62,0,0,0,573.34,97a12.87,12.87,0,0,0,8.1,2.34q4.74,0,7.16-1.8A5.57,5.57,0,0,0,591,92.81Z"></path>
                            <path class="cls-1" d="M637.43,32.94V48.12H648v12.1H637.43V91c0,2.28.43,3.92,1.31,4.9s2.55,1.49,5,1.49a26,26,0,0,0,4.85-.4v12.5a33.89,33.89,0,0,1-10,1.48q-17.34,0-17.69-17.52V60.22h-9V48.12h9V32.94Z"></path>
                            <path class="cls-1" d="M675.38,80.59v29.28H658.26V26.78h32.41a37.58,37.58,0,0,1,16.47,3.42,25.29,25.29,0,0,1,10.93,9.73,27.23,27.23,0,0,1,3.82,14.35q0,12.22-8.36,19.27t-23.14,7Zm0-13.86h15.29q6.8,0,10.36-3.2t3.57-9.13a13.69,13.69,0,0,0-3.6-9.87q-3.6-3.78-9.93-3.88H675.38Z"></path>
                            <path class="cls-1" d="M767.26,63.59a44,44,0,0,0-5.94-.46q-9.36,0-12.27,6.33v40.41H732.56V48.12h15.58l.46,7.36q5-8.5,13.75-8.5a17.45,17.45,0,0,1,5.14.74Z"></path>
                            <path class="cls-1" d="M770.57,78.43a36.59,36.59,0,0,1,3.54-16.38,25.85,25.85,0,0,1,10.18-11.13A29.7,29.7,0,0,1,799.73,47q12.49,0,20.4,7.65T829,75.4l.12,4.22q0,14.22-7.94,22.8T799.85,111q-13.36,0-21.32-8.56t-8-23.28Zm16.49,1.17q0,8.79,3.31,13.46a11.91,11.91,0,0,0,18.84,0q3.36-4.59,3.36-14.71,0-8.64-3.36-13.38a11,11,0,0,0-9.48-4.74A10.78,10.78,0,0,0,790.37,65C788.17,68.14,787.06,73,787.06,79.6Z"></path>
                            </g>
                            </g>
                        </svg>
						<br><br><br>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-16 f-md-16 w300 lh140 white-clr text-xs-center">Copyright © VidHostPro</div>
                  <ul class="footer-ul w300 f-16 f-md-16 white-clr text-center text-md-right">
                     <li><a href="https://support.oppyo.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://vidhostpro.co/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://vidhostpro.co/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://vidhostpro.co/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://vidhostpro.co/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://vidhostpro.co/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://vidhostpro.co/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!--Footer Section End -->

 <?php
         if ($now < $exp_date) {
         
         ?>
      <script type="text/javascript">
         // Count down milliseconds = server_end - server_now = client_end - client_now
         var server_end = <?php echo $exp_date; ?> * 1000;
         var server_now = <?php echo time(); ?> * 1000;
         var client_now = new Date().getTime();
         var end = server_end - server_now + client_now; // this is the real end time
         
         var noob = $('.countdown').length;
         
         var _second = 1000;
         var _minute = _second * 60;
         var _hour = _minute * 60;
         var _day = _hour * 24
         var timer;
         
         function showRemaining() {
             var now = new Date();
             var distance = end - now;
             if (distance < 0) {
                 clearInterval(timer);
                 document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
                 return;
             }
         
             var days = Math.floor(distance / _day);
             var hours = Math.floor((distance % _day) / _hour);
             var minutes = Math.floor((distance % _hour) / _minute);
             var seconds = Math.floor((distance % _minute) / _second);
             if (days < 10) {
                 days = "0" + days;
             }
             if (hours < 10) {
                 hours = "0" + hours;
             }
             if (minutes < 10) {
                 minutes = "0" + minutes;
             }
             if (seconds < 10) {
                 seconds = "0" + seconds;
             }
             var i;
             var countdown = document.getElementsByClassName('countdown');
             for (i = 0; i < noob; i++) {
                 countdown[i].innerHTML = '';
         
                 if (days) {
                     countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-40 f-md-40 timerbg">' + days + '</span><br><span class="f-20 w600">Days</span> </div>';
                 }
         
                 countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-40 f-md-40 timerbg">' + hours + '</span><br><span class="f-20 w600">Hours</span> </div>';
         
                 countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-40 f-md-40 timerbg">' + minutes + '</span><br><span class="f-20 w600">Mins</span> </div>';
         
                 countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-40 f-md-40 timerbg">' + seconds + '</span><br><span class="f-20 w600">Sec</span> </div>';
             }
         
         }
         timer = setInterval(showRemaining, 1000);
      </script>
    
      <?php
         } else {
         echo "Times Up";
         }
         ?>
      <!--- timer end-->
</body>
</html>

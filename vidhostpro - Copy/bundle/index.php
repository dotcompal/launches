<!Doctype html>
<html>

<head>
    <title>VidHostPro Exclusive Bundle Deal</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=9">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <!------Meta Tags-------->
    <meta name="title" content="VidHostPro Exclusive Bundle Deal">
    <meta name="description" content="Host, Market, And Play Unlimited Videos at Blazing-FAST Speed With No Tech Hassles & No Monthly Fee Ever…">
    <meta name="keywords" content="VidHostPro">
    <meta property="og:image" content="https://www.vidhostpro.co/bundle/thumbnail.png">
    <meta name="language" content="English">
    <meta name="revisit-after" content="1 days">
    <meta name="author" content="Dr. Amit Pareek">
    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:title" content="VidHostPro Exclusive Bundle Deal">
    <meta property="og:description" content="Host, Market, And Play Unlimited Videos at Blazing-FAST Speed With No Tech Hassles & No Monthly Fee Ever…">
    <meta property="og:image" content="https://www.vidhostpro.co/bundle/thumbnail.png">
    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:title" content="VidHostPro Exclusive Bundle Deal">
    <meta property="twitter:description" content="Host, Market, And Play Unlimited Videos at Blazing-FAST Speed With No Tech Hassles & No Monthly Fee Ever…">
    <meta property="twitter:image" content="https://www.vidhostpro.co/bundle/thumbnail.png">
    <!------Meta Tags-------->
    <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    
    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    <!-- Start Editor required -->
    <link rel="stylesheet" href="../common_assets/css/bootstrap.min.css" type="text/css">
     <script src="../common_assets/js/jquery.min.js"></script>
    <script type="text/javascript">
        // Special handling for facebook iOS since it cannot open new windows
        (function() {
            if (navigator.userAgent.indexOf('FBIOS') !== -1 || navigator.userAgent.indexOf('Twitter for iPhone') !== -1) {
                document.getElementById('af-form-1637034915').parentElement.removeAttribute('target');
            }
        })();
    </script>
    <link rel="stylesheet" href="assets/css/style.css" type="text/css">
                <link rel="stylesheet" href="assets/css/timer.css" type="text/css">
                <script src="assets/css/jquery.bxslider.min"></script>
    <!-- End -->
   <!-- Google Tag Manager -->
      <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-NMK2MBB');
     </script>

     <script>
        setInterval(function () { $(".page_push_down").not(':first').remove(); }, 100);
     </script>

    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NMK2MBB"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    
    <script>
        (function(w, i, d, g, e, t, s) {
            if(window.businessDomain != undefined){
                console.log("Your page have duplicate embed code. Please check it.");
                return false;
            }
            businessDomain = 'vidhostproo';
            allowedDomain = 'vidhostpro.co';
            if(!window.location.hostname.includes(allowedDomain)){
                console.log("Your page have not authorized. Please check it.");
                return false;
            }
            console.log("Your script is ready...");
            w[d] = w[d] || [];
            t = i.createElement(g);
            t.async = 1;
            t.src = e;
            s = i.getElementsByTagName(g)[0];
            s.parentNode.insertBefore(t, s);
        })(window, document, '_gscq', 'script', 'https://cdn.staticdcp.com/apps/engage/smart_engage/js/config-loader.js');
        </script>
</head>

<?php
         $date = 'Nov 27 2023 11:59 PM EST';
         $exp_date = strtotime($date);
         $now = time();  
         /*
         
         $date = date('F d Y g:i:s A eO');
         $rand_time_add = rand(700, 1200);
         $exp_date = strtotime($date) + $rand_time_add;
         $now = time();*/
         
         if ($now < $exp_date) {
         ?>
      <?php
         } else {
          echo "Timer Up";
         }
         ?>

<body>
    <div class="timer-header-top fixed-top">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-12 col-md-4">
                  <div class="tht-left f-16 f-md-20 text-md-start text-center white-clr">
                     Use Coupon Code <span class="red-clr w600">"BLACKFRIDAY"</span>  for <br class="d-block d-md-none">Extra <span class="red-clr w600"> 30% Discount </span>
                  </div>
               </div>
               <div class="col-8 col-md-5 text-center">
               <div class="countdown counter-white text-center"><div class="timer-label text-center"><span class="f-20 f-md-25 timerbg oswald">00
               </span><br><span class="f-14 f-md-14 w400 smmltd">Days</span> </div><div class="timer-label text-center"><span class="f-20 f-md-25 timerbg oswald">00</span>
               <br><span class="f-14 f-md-14 w400 smmltd">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-20 f-md-25 timerbg oswald">28</span>
               <br><span class="f-14 f-md-14 w400 smmltd">Mins</span> </div><div class="timer-label text-center "><span class="f-20 f-md-25 timerbg oswald">13</span><br>
               <span class="f-14 f-md-14 w400 smmltd">Sec</span> </div></div>
               </div>
               <div class="col-4 col-md-3 text-center text-md-right">
                  <div class="affiliate-link-btn"><a href="#buynow" class="t-decoration-none">Buy Now</a></div>
               </div>
            </div>
         </div>
      </div>
    
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NMK2MBB"
         height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <!--1. Header Section Start -->
    <div class="header-section content-mbl-space mt-md115">
        <div class="container">
            <div class="row">
            <div class="col-12 col-md-12">
                <img src="assets/images/sale.webp" class="img-fluid mx-auto d-block">
            </div>
            </div>
            <div class="row">
                <div class="col-12 mt20 mt-md50 text-center">
                    <div class="preheadline f-18 f-md-24 w400 black-clr lh140">
                        <span class="red-gradient w700">"Hurry!</span> The Exclusive Black Friday Bundle Discount is Ending Soon.
                    </div>
                </div>
                <div class="col-12 f-md-50 f-28 w500 black-clr lh140 mt20 mt-md80 relative">
                    <div class="gametext d-none d-md-block">
                      Cutting-Edge Technology
                    </div>
                    <div class="mainheadline text-center">
                        <span class="w700">Host, Market, And Play Unlimited Videos at Blazing-FAST Speed</span> With No Tech Hassles &amp; No Monthly Fee Ever…
                    </div>
                 </div>
                <div class="col-12 mt20 mt-md40 f-md-27 f-18 w400 white-clr text-center lh150">
                    GET VidHostPro With All The Upgrades For 63% Off & Save Over $630 When You Grab This Highly-Discounted Bundle Deal...
                </div>
                <div class="col-12 col-md-11 mt20 mt-md60 mx-auto">
                     <div class="responsive-video">
                        <iframe src="https://vidhostpeo.oppyo.com/video/embed/vhp_webinar" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                        box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                    </div> 
                      <!--<img src="assets/images/pbb-img.webp" class="img-fluid d-block mx-auto img-shadow" alt="ProductBox">-->
                </div>
            </div>
        </div>
    </div>
    <!--1. Header Section End -->
    <!---new Section Start---->
    <div class="new-section content-mbl-space">
        <div class="container">
            <div class="row">
                <div class="col-12 f-md-45 f-28 w500 text-center black-clr lh170 line-center ">
                    Save $630 RIGHT AWAY-
                    <div class="gradient-clr w700 f-md-70 f-40">
                        Deal Ends Soon
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-md-6">
                   <div class="f-16 f-md-18 lh140 w500">
                      <ul class="menu-list pl0 m0">
                         <li>GET Complete VidHostPro Package (FE + ALL Upgrades + Agency License)</li>
                         <li>Developed By Marketers FOR Video Marketers</li>
                         <li>GET Priority Support from Our Dedicated Support Engineers</li>
                      </ul>
                   </div>
                </div>
                <div class="col-12 col-md-6">
                   <div class="f-16 f-md-18 lh140 w500">
                      <ul class="menu-list pl0 m0">
                         <li>Provide Top-Notch Services to Your Clients </li>
                         <li>Grab All Benefits For A Low, ONE-TIME Discounted Price</li>
                         <li>GET 30-Days Money Back Guarantee</li>
                      </ul>
                   </div>
                </div>
             </div>
            <div class="row">
                <div class="col-12 mt20 mt-md40 f-md-26 f-22 lh140 w400 text-center black-clr">
                    <div class="f-24 f-md-36 w500 lh150 text-center black-clr">
                        <img src="assets/images/hurry.png " alt="Hurry " class="mr10 hurry-img">
                        <span class="red-clr ">Hurry!</span> Special One Time Price for Limited Time Only!
                    </div>
                    <div>
                        Use Coupon Code <span class="w700 red-gradient">"BLACKFRIDAY"</span> for an Additional <span class="w700 red-gradient">30% Discount</span> 
                    </div>
                    <div class="col-md-8 col-12 instant-btn mt-md30 mt20 f-24 f-md-30 mx-auto px0">
                        <a href="#buybundle">
                     GET Bundle Offer Now &nbsp;<i class="fa fa-angle-double-right f-32 f-md-42 angle-right" aria-hidden="true"></i>
                     </a>
                    </div>
                    <div class="col-12 mt-md20 mt20 f-18 f-md-18 w600 lh140 text-center black-clr">
                        No Download or Installation Required
                    </div>
                    <div class="col-12 mt20 mt-md20">
                        <img src="assets/images/compaitable-with.png" class="img-fluid mx-auto d-block">
                     </div>
                </div>
            </div>
        </div>
    </div>
    <!--1. New Section End -->
    <!--2. Second Section Start -->
    <div class=" section1">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="f-md-45 f-28 black-clr  text-center w500 lh140">
                        Here's What You GET With <span class="red-gradient w700">Limited Time <br class="d-none d-md-block"> VidHostPro Discounted</span> Bundle Today
                    </div>
                </div>
                <!-- AGENCY BUNDLE START -->
                <div class="col-12 mt20 mt-md70 section-margin px-md-0">
                    <div class="col-12 plan1-shape">
                        <div class="col-md-10 col-12 px-md15 mt-md0 mt20 mx-auto">
                            <div class="pimg">
                                <img src="assets/images/commercial.webp" class="img-fluid d-block mx-auto" alt="Commercial">
                            </div>
                        </div>
                        <div class="row p0 mt20 mx-0 mt-md0">
                            <div class="col-12 p0 text-center mt-md50 mt20">
                                <div class="title-shape">
                                    <div class="f-24 f-md-38 w600 lh150 white-clr text-center">
                                        VidHostPro Commercial ($97)
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 px0 px-md15">
                                <div class="f-20 f-md-20 lh150 w500 mt20 mt-md50">
                                    <ul class="kaptick pl0">
                                        <li> Host Unlimited Videos</li>
                                        <li> 50 Businesses/Subdomains </li>
                                        <li> Unlimited Video Chapters For Easier Explanations</li>
                                        <li> Capture Unlimited Customer Leads</li>
                                        <li> Capture Unlimited Audience Data</li>
                                        <li> Up To 30,000 Pages Visits/Month</li>
                                        <li> FREE Hosting Up To 250 GB Bandwidth</li>
                                        <li> FREE Storage Up To 250 GB</li>
                                        <li> 50 SEO Friendly Inbuilt Video Channels With Content Customization</li>
                                        <li> 50 Video Playlists/Channels - To Use As A Course Or Training</li>
                                        <li> 1 Ultra-Light & Attractive Video Player With Complete Customization</li>
                                        <li> Lightning-Fast Loading Speed Means More Sales…</li>
                                        <li> Maximize Visitor Engagement with Ad-Free Videos</li>
                                        <li> 1 Line Embed Code To Publish Your Videos On ANY Website, Landing Page, Online Shop or Membership Site!</li>
                                        <li> HLS Player (HTTP Live Streaming Player) - Optimized To Work On All Devices</li>
                                        <li> 100% Mobile Responsive Viral Video Pages And Player</li>
                                        <li> Autoplay (Unmute & Mute) For Your Videos</li>
                                        <li> Effortless Comment Management System To Manage More Leads</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 px0 px-md15">
                                <div class="f-20 f-md-20 lh150 w500 mt20 mt-md50">
                                    <ul class="kaptick pl0">

                                        <li>Have 100% Control on Your Traffic – Zero Leakage</li>
                                        <li>Premium Lead Generation Templates To Sell, Promote, Or Collect Leads Right Inside Your Video Player</li>
                                        <li> Stunning Promo & Social Templates For Extra Monetization & Traffic</li>
                                        <li>Seamless Integration With Top Autoresponders</li>
                                        <li>Full Feature Drag And Drop Editor To Edit Templates In A Few Clicks</li>
                                        <li>Check Visitor Activity On Your Viral Page</li>
                                        <li>HDR (High Dynamic Range) Video Support</li>
                                        <li>Ad-Free Videos To Maximize Visitor Engagement</li>
                                        <li>Capture Thumbnails From Your Videos OR Upload Your Own</li>
                                        <li>Add Videos In Mp4 | Mov | Wmv | Flv | Avi | Avchd | Webm | Mkv Format</li>
                                        <li>Define Annotations & Sitemap</li>
                                        <li>Precise Analytics Included</li>
                                        <li>Schedule Notification Timelines With Lead App & Promo App</li>
                                        <li>Beautiful Conversions Focused Viral Pages</li>
                                        <li>Extremely User-Friendly Dashboard</li>
                                        <li>A-Z Complete Video Training Included</li>
                                        <li>100% Newbie Friendly</li>
                                        <li>FREE Commercial License to Service Your Customers With Top-Notch Trending Solutions</li>
                                    </ul>
                                </div>
                            </div>
                            <!-- <div class="col-12 mt20">
                                <ul class="kaptick pl0 f-20 f-md-20 lh150 w400 black-clr text-capitalize">
                                    <li class="w600">PLUS, YOU'LL ALSO RECEIVE – LIMITED AGENCY LICENSE IF YOU BUY NOW.</li>
                                </ul>
                            </div> -->
                        </div>
                    </div>
                </div>
                <!-- AGENCY BUNDLE END -->
                <!-- ELITE BUNDLE START -->
                <div class="col-12 px-md0 mt40 mt-md140 section-margin">
                    <div class="col-12 plan2-shape">
                        <div class="col-md-12 mx-auto mt-md0 mt20">
                            <div class="pimg">
                                <img src="assets/images/elite.webp" class="img-fluid d-block mx-auto" alt="Elite">
                            </div>
                        </div>
                        <div class="row mt-md0 mt20 mx-0">
                            <div class="col-12 p0 text-center mt-md50 mt20">
                                <div class="title-shape">
                                    <div class="f-22 f-md-28 w600 white-clr lh150 text-center">
                                        Pro Upgrade 1- VidHostPro Elite ($297)
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 px0 px-md15">
                                <div class="f-20 f-md-20 lh150 w500 mt20 mt-md50">
                                    <ul class="kaptick pl0">
                                        <li>1 TB Bandwidth PER MONTH For Your Videos</li>
                                        <li>Create Unlimited Custom Domains/Subdomains/Businesses</li>
                                        <li>UNLIMITED Video Channels</li>
                                        <li>Unlimited Video Playlists</li>
                                        <li>Unlimited Page Visits</li>
                                        <li>GET 5 More Stunning And Ready-To-Use Fully Customizable Video Players</li>
                                        <li>Customize Player - Color, Theme To Give It The Look & Feel Of Your Brand</li>
                                        <li>Customize Your Player With 8 Attractive And Eye-Catching Frames</li>
                                        <li>Video A-B Repeat Functionality</li>
                                        <li>VidHostPro Drives Unlimited Leads For Your Offers & Boost Sales And Conversions</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 px0 px-md15">
                                <div class="f-20 f-md-20 lh150 w500 mt20 mt-md50">
                                    <ul class="kaptick pl0">
                                        <li>Embed Unlimited Playlists</li>
                                        <li>40 EXTRA Templates To Collect Almost Every Visitor's Details Right Inside The Video<br>
                                        • Lead Templates<br>
                                        • Promo Templates<br>
                                        • Social Templates</li>
                                        <li>Capture More Leads & Visitor's Feedback By Allowing Them To Interact On Your VIDEO PAGE Or Channel</li>
                                        <li>Collect Leads And Sell Products Right Inside The Video And Boost Your Profits</li>
                                        <li>GET Your Subscribers Auto Registered For Your Presentations With Webinar Platform Integrations</li>
                                        <li>GET All These Benefits At An Unparalleled Price</li>
                                        <li>Up To 10 Team Members</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ELITE BUNDLE END -->
                <!-- ENTERPRISE BUNDLE START -->
                <div class="col-12 px-md-0 mt20 mt-md70 section-margin">
                    <div class="col-12 plan1-shape">
                        <div class="col-md-12 mx-auto px-md15 mt-md0 mt20">
                            <div class="pimg">
                                <img src="assets/images/enterprise.webp" class="img-fluid d-block mx-auto" alt="Enterprise">
                            </div>
                        </div>
                        <div class="row p0 mt20 mx-0 mt-md0">
                            <div class="col-12 p0 text-center mt-md50 mt20">
                                <div class="title-shape">
                                    <div class="f-22 f-md-28 w600 white-clr lh150 text-center">
                                        Pro Upgrade 2- VidHostPro Enterprise Commercial ($97)
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 px0 px-md15">
                                <div class="f-20 f-md-20 lh150 w500 mt20 mt-md50">
                                    <ul class="kaptick pl0">
                                        <li>Use The POWER Of Stats To Boost Your PROFIT With NO Extra Effort</li>
                                        <li>Compare Stats For Various Videos And Find Out The Best Performing Ones</li>
                                        <li>Remove Our Branding from Video Player To Intensify Your Brand Presence</li>
                                        <li>Play Video In Current Language While Viewers Read Subtitles In Their Languages</li>
                                        <li>Monetize Your Videos In Multiple Ways With Our Advanced Advertisement Technology</li> 
                                    </ul>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 px0 px-md15">
                                <div class="f-20 f-md-20 lh150 w500 mt20 mt-md50">
                                    <ul class="kaptick pl0">
                                        <li>Define Notification Timelines With Share App, HTML And Video Move, Replace & Copy Your Videos With Ease</li>
                                        <li>Compare Project & Video Stats</li>
                                        <li>Boost Your Business With Our Advanced Project & Campaign Management System</li>
                                        <li>Add Your Team Up To 50 & Manage With Ease</li>
                                        <li>Integrate Your CRM (Customer Resoure Management) Easily</li>
                                        <li>Commercial License - Provide Services To Clients & Establish Recurring Services For Continuous Profits</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ENTERPRISE BUNDLE END -->
                <!-- BUSINESS DRIVE BUNDLE START -->
                <div class="col-12 px-md0 mt20 mt-md70 section-margin">
                    <div class="col-12 plan2-shape">
                        <div class="col-md-10 mx-auto mt-md0 mt20">
                            <div class="pimg">
                                <img src="assets/images/agency.webp" class="img-fluid d-block mx-auto" alt="Agency">
                            </div>
                        </div>
                        <div class="row mt-md0 mt20 mx-0">
                            <div class=" col-12 p0 text-center mt-md50 mt20">
                                <div class="title-shape">
                                    <div class="f-22 f-md-28 w600 white-clr lh150 text-center">
                                        Pro Upgrade 3 - VidHostPro Agency Unlimited Client Plan ($197)
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 px0 px-md15">
                                <div class="f-20 f-md-20 lh150 w500 mt20 mt-md50">
                                    <ul class="kaptick pl0">
                                        <li>Agency License to Serve Unlimited Clients & Add Unlimited Team Members</li>
                                        <li>Create Unlimited Custom Domains/Subdomains/ Businesses</li>
                                        <li>Biz Management Panel</li>
                                        <li>Add Your Own LOGO for Complete White Labelling of Video Player</li>  
                                    </ul>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 px0 px-md15">
                                <div class="f-20 f-md-20 lh150 w500 mt20 mt-md50">
                                    <ul class="kaptick pl0">
                                        <li>Accurate Analysis for Team Members' Activities For Effective Monitoring</li>
                                        <li>Support For You & Your Clients - Including Chat Support Directly From Software</li>
                                        <li>Subscription Management System to Manage Your Clients Plans</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- BUSINESS DRIVE BUNDLE END -->
                <!-- FAST ACTION BONUSES BUNDLE START -->
                <div class="col-12 px-md-0 mt20 mt-md70 section-margin">
                    <div class="col-12 plan1-shape">
                        <div class="mx-auto mt-md0 mt20">
                            <div class="pimg ">
                                <img src="assets/images/fast-action.webp" class="img-fluid d-block mx-auto" alt="Fast-Action">
                            </div>
                        </div>
                        <div class="row mt20">
                            <div class="col-12 p0 mt-md50 mt20 text-center">
                                <div class="title-shape1">
                                    <div class="f-22 f-md-28 w600 white-clr lh150 text-center text-capitalize">
                                        And you're also getting these premium fast action bonuses worth ($309)
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-12 px0 px-md15">
                                <div class="f-20 f-md-20 lh150 w600 mt20 mt-md50">
                                    <ul class="kaptick pl0">
                                        <li>Bonus #1 – <span class="red-gradient w600 ">LIVE Training</span> </li>
                                        <li>Bonus #2 – <span class="red-gradient w600 ">Oppyo - All-In-One Solution For Marketers and Agencies!</span> </li>
                                        
                                        <li>Bonus #3 – <span class="red-gradient w600 ">Advanced Video Marketing</span></li>
                                        <li>Bonus #4 – <span class="red-gradient w600 ">Content Syndication</span></li>
                                        <li>Bonus #5 – <span class="red-gradient w600 ">Animation 4K Stock Videos</span></li>
                                        <li>Bonus #6 – <span class="red-gradient w600 ">Plus 13 More OTO Bonuses!!</span></li>
                                    </ul>
                                </div>
                            </div>
                            <!-- <div class="col-12 col-md-6 px0 px-md15">
                                <div class="f-20 f-md-20 lh150 w600 mt20 mt-md50">
                                    <ul class="kaptick pl0">
                                        <li>Super Value Bonus #5 – Video Training To Monetizing Your Website </li>
                                        <li>Super Value Bonus #6 – Training To Start Affiliate Marketing </li>
                                        <li>Super Value Bonus #7 – Video Training On Viral Marketing </li>
                                        <li>Super Value Bonus #8 – Video Marketing Profit Kit </li>
                                    </ul>
                                </div>
                            </div> -->
                        </div>
                    </div>
                </div>
                <!-- FAST ACTION BONUSES BUNDLE END -->
            </div>
        </div>
    </div>
    <!--2. Second Section End -->
    <!--3. Third Section Start -->
    <div class="section3">
        <div class="container">
            <div class="row ">
                <div class="col-12 px0 px-md15" id="buybundle">
                    <div class="f-md-45 f-28 black-clr  text-center w700 lh140">
                        Grab This Exclusive BUNDLE DISCOUNT Today…
                    </div>
                    <div class="f-18 f-md-24 w400 lh150 text-center mt20 black-clr">
                        Use Coupon Code <span class="red-gradient w600 ">"BLACKFRIDAY"</span> for an Additional <span class="red-gradient w600 "> 30% Discount</span> 
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-6 col-12 mt25 mt-md50 px-md15">
                    <div class="tablebox1">
                        <div class="col-12 tablebox-inner-bg text-center">
                            <div class="relative col-12 p0">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 829.07 133.22" style="max-height:55px">
                                    <defs>
                                    <style>.cls-1{fill:#fff;}.cls-2{fill:#ec1c24;}</style>
                                    </defs>
                                    <g id="Layer_2" data-name="Layer 2">
                                    <g id="Layer_1-2" data-name="Layer 1">
                                    <path class="cls-1" d="M142.09,14.94C89.5-8.93,68.3,32,65.87,37,48.19,38.59,36.52,52.3,32.31,67.64l-1.55-.54c-2.87-1-7-2.8-9.65-5.12a51.82,51.82,0,0,1,6.14-12h0a49.93,49.93,0,0,1,3.41-4.41c9.9-11.41,22.44-15.6,26.3-15.6C72.85.51,95.07,0,99.08,0,126.45-.2,142.09,14.94,142.09,14.94Z"></path>
                                    <path class="cls-1" d="M103.29,128.84c-21.44,11.73-43.39-3.34-47.63-7.58-25.5-.6-35.11-20.89-37.43-32.69a121.37,121.37,0,0,0,14.3,4.56C36,103.65,44.31,111.7,58,112.66c23.66,18.51,45.7,7.08,45.7,7.08a40.15,40.15,0,0,0,23.47,3,56.14,56.14,0,0,0,22.25-10.11s11.38,1,18-6.81c3.34-3.95,6.36-7.52,8.22-12.5h0a114.47,114.47,0,0,0,13.44-4.5c-3.45,17.54-17.34,32.81-37.25,32.81-14,13.87-37.41,11.7-45.2,8.61"></path>
                                    <path class="cls-1" d="M185.47,61.68a37.61,37.61,0,0,1-3.63,2.23,54,54,0,0,1-6.43,2.93c-6-14-24.8-19.71-24.8-19.71s-10.49-17.19-27.1-23.39c-16.26-6.08-41.44,1-42,1.1.49-.42,21.75-18.55,53.83-6,14.36,6.17,21.74,21.44,21.74,21.44a35.85,35.85,0,0,1,5,1.56,49.15,49.15,0,0,1,9.36,4.73h0A40.26,40.26,0,0,1,185.47,61.68Z"></path>
                                    <path class="cls-2" d="M77,80.25V94c-17.82-1.23-33.48-3.67-46-7.14a100,100,0,0,1-13.4-4.67C6.24,77.22-.2,70.89,0,63.51,1.47,42.92,28.58,42.83,29,42.83c-.25.1-16.62,6.85-16.17,15.07.19,3.59,2.24,6.93,6.53,9.9a44.79,44.79,0,0,0,11.74,5.42C41.36,76.53,56.31,79,77,80.25Z"></path>
                                    <path class="cls-2" d="M189.87,82.3A99,99,0,0,1,177.23,87c-11,3.29-25.29,5.89-43.6,7.12l11.78-7a8.27,8.27,0,0,0,4.06-7.16,7.84,7.84,0,0,0-.08-1.19c10.93-1.5,20.22-3.65,27.61-6.43a58.75,58.75,0,0,0,7.56-3.4c1.15-.63,2.21-1.26,3.18-1.9,16-10.49,6.87-21.31-22.83-28,36.12,7.83,43.62,16.49,44.7,22.28C210.57,66.47,205.7,75.09,189.87,82.3Z"></path>
                                    <path class="cls-2" d="M142.5,82.2l-21,12.47-9.94,5.93-24.9,14.81a2.65,2.65,0,0,1-4-2.28V46.69a2.69,2.69,0,0,1,2-2.56l.68-.05.69,0,.68.29,50.33,30,5.48,3.27a2.66,2.66,0,0,1,0,4.57Z"></path>
                                    <path class="cls-1" d="M267.45,89.27l18.83-62.49h19.06l-28.93,83.09H258.54L229.72,26.78h19Z"></path>
                                    <path class="cls-1" d="M312,32.14a8.12,8.12,0,0,1,2.48-6.1,10.69,10.69,0,0,1,13.5,0,8.07,8.07,0,0,1,2.51,6.1,8.09,8.09,0,0,1-2.54,6.16,10.57,10.57,0,0,1-13.41,0A8.09,8.09,0,0,1,312,32.14Zm17.52,77.73H312.93V48.12h16.55Z"></path>
                                    <path class="cls-1" d="M340.49,78.54q0-14.45,6.48-23T364.69,47a18.92,18.92,0,0,1,14.9,6.73V22.21h16.55v87.66h-14.9l-.8-6.56a19.41,19.41,0,0,1-15.86,7.7,20.94,20.94,0,0,1-17.49-8.59Q340.49,93.84,340.49,78.54ZM357,79.77q0,8.69,3,13.31a9.85,9.85,0,0,0,8.79,4.63q7.65,0,10.79-6.45V66.85q-3.09-6.46-10.67-6.46Q357,60.39,357,79.77Z"></path>
                                    <path class="cls-1" d="M477.63,109.87H460.51V74.26H427.13v35.61H410V26.78h17.13V60.45h33.38V26.78h17.12Z"></path>
                                    <path class="cls-1" d="M488.93,78.43a36.59,36.59,0,0,1,3.54-16.38,25.81,25.81,0,0,1,10.19-11.13A29.67,29.67,0,0,1,518.09,47q12.51,0,20.41,7.65t8.81,20.77l.12,4.22q0,14.22-7.93,22.8T518.21,111q-13.35,0-21.32-8.56t-8-23.28Zm16.5,1.17q0,8.79,3.31,13.46a11.9,11.9,0,0,0,18.83,0q3.36-4.59,3.37-14.71,0-8.64-3.37-13.38a11,11,0,0,0-9.48-4.74A10.75,10.75,0,0,0,508.74,65C506.53,68.14,505.43,73,505.43,79.6Z"></path>
                                    <path class="cls-1" d="M591,92.81A5.32,5.32,0,0,0,588,88q-3-1.74-9.61-3.11-22-4.62-22-18.72a16.94,16.94,0,0,1,6.82-13.72Q570,47,581,47q11.76,0,18.81,5.54a17.45,17.45,0,0,1,7,14.38H590.4a8,8,0,0,0-2.28-5.85q-2.28-2.31-7.14-2.31a9.83,9.83,0,0,0-6.44,1.88,5.91,5.91,0,0,0-2.29,4.79,5.1,5.1,0,0,0,2.6,4.43q2.6,1.68,8.76,2.91A73,73,0,0,1,594,75.51q13.07,4.8,13.07,16.61,0,8.44-7.25,13.67T581.1,111a32.54,32.54,0,0,1-13.78-2.77,23,23,0,0,1-9.45-7.59,17.62,17.62,0,0,1-3.42-10.41h15.63A8.62,8.62,0,0,0,573.34,97a12.87,12.87,0,0,0,8.1,2.34q4.74,0,7.16-1.8A5.57,5.57,0,0,0,591,92.81Z"></path>
                                    <path class="cls-1" d="M637.43,32.94V48.12H648v12.1H637.43V91c0,2.28.43,3.92,1.31,4.9s2.55,1.49,5,1.49a26,26,0,0,0,4.85-.4v12.5a33.89,33.89,0,0,1-10,1.48q-17.34,0-17.69-17.52V60.22h-9V48.12h9V32.94Z"></path>
                                    <path class="cls-1" d="M675.38,80.59v29.28H658.26V26.78h32.41a37.58,37.58,0,0,1,16.47,3.42,25.29,25.29,0,0,1,10.93,9.73,27.23,27.23,0,0,1,3.82,14.35q0,12.22-8.36,19.27t-23.14,7Zm0-13.86h15.29q6.8,0,10.36-3.2t3.57-9.13a13.69,13.69,0,0,0-3.6-9.87q-3.6-3.78-9.93-3.88H675.38Z"></path>
                                    <path class="cls-1" d="M767.26,63.59a44,44,0,0,0-5.94-.46q-9.36,0-12.27,6.33v40.41H732.56V48.12h15.58l.46,7.36q5-8.5,13.75-8.5a17.45,17.45,0,0,1,5.14.74Z"></path>
                                    <path class="cls-1" d="M770.57,78.43a36.59,36.59,0,0,1,3.54-16.38,25.85,25.85,0,0,1,10.18-11.13A29.7,29.7,0,0,1,799.73,47q12.49,0,20.4,7.65T829,75.4l.12,4.22q0,14.22-7.94,22.8T799.85,111q-13.36,0-21.32-8.56t-8-23.28Zm16.49,1.17q0,8.79,3.31,13.46a11.91,11.91,0,0,0,18.84,0q3.36-4.59,3.36-14.71,0-8.64-3.36-13.38a11,11,0,0,0-9.48-4.74A10.78,10.78,0,0,0,790.37,65C788.17,68.14,787.06,73,787.06,79.6Z"></path>
                                    </g>
                                    </g>
                                </svg>
                            </div>
                            <div class="col-12 mt-md40 mt20 table-headline-shape">
                                <div class="w700 f-md-32 f-22 text-center text-uppercase black-clr lh150 ">
                                    Bundle Offer
                                </div>
                            </div>
                            <div class="d-flex align-items-center justify-content-between py15 px30">
                                <div class="w600 f-md-28 f-22 text-left text-xs-center text-uppercase white-clr lh150 mt8">
                                    Commercial
                                </div>
                                <div class="w600 f-md-35 f-22 text-center white-clr lh150">
                                    $97
                                </div>
                            </div>
                            <div class="d-flex align-items-center justify-content-between light-blue py15 px30">
                                <div class="w600 f-md-28 f-22 text-left text-xs-center text-uppercase black-clr lh150 mt8">
                                    ELITE
                                </div>
                                <div class="w600 f-md-35 f-22 text-center black-clr lh150">
                                    $297
                                </div>
                            </div>
                            <div class="d-flex align-items-center justify-content-between py15 px30">
                                <div class="w600 f-md-28 f-22 text-left text-xs-center text-uppercase white-clr lh150 mt8">
                                    ENTERPRISE
                                </div>
                                <div class="w600 f-md-35 f-22 text-center white-clr lh150">
                                    $97
                                </div>
                            </div>
                            <div class="d-flex align-items-center justify-content-between light-blue py15 px30">
                                <div class="w600 f-md-28 f-22 text-left text-xs-center text-uppercase black-clr lh150 mt8">
                                    AGENCY
                                </div>
                                <div class="w600 f-md-35 f-22 text-center black-clr lh150">
                                    $197
                                </div>
                            </div>
                            <div class="d-flex align-items-center justify-content-between py15 px30">
                                <div class="w600 f-md-28 f-22 text-left text-xs-center text-uppercase white-clr lh150 mt8">
                                    FAST ACTION BONUSES
                                </div>
                                <div class="w600 f-md-35 f-22 text-center white-clr lh150">
                                    $309
                                </div>
                            </div>
                            
                            <!-- PLAN 5 END -->
                            <div class="col-12 px-md0 px15 mt-md30 mt20 w500 f-md-35 f-22 white-clr text-center">
                                Regular <strike>$997</strike>
                            </div>
                            <!--<div class="justify-content-center">-->
                            <!--    <div class="col-md-8 col-12 now-price mt20 w700 f-md-50 f-32 white-clr text-center mx-auto ">-->
                            <!--        Now $367-->
                            <!--    </div>-->
                            <!--</div>-->
                            <div class="col-12 px-md0 px15 mt20 w500 f-md-25 f-20 white-clr text-center">
                                One Time
                            </div>
                            <div class="mt20 red-clr w600 f-22 d-md-27 lh140">
                                <img src="assets/images/red-close.webp" alt="Hurry" class="mr10 close-img">
                              No Recurring | No Monthly | No Yearly
                           </div>
                            <div class="col-12 mx-auto justify-content-center">
                                <div class="myfeatures f-md-25 f-16 w400 text-center lh160 hideme">
                                    <div class="hideme-button">
                                        <a  href="https://www.jvzoo.com/b/108597/394877/2"><img src="https://i.jvzoo.com/108597/394877/2" alt="VidHostPro Bundle" border="0"  target="_blank" class="img-fluid d-block mx-auto" /></a>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <div class="col-12 px0 px-md15 mt-md50 mt20 text-center">
                    <div class="f-18 f-md-22 w400 lh150 text-center mt10 white-clr head-design">
                        Use Coupon Code <span class="black-clr w600 ">"BLACKFRIDAY"</span> for Additional <span class="black-clr w600 "> 30% Discount</span>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    <!--3. Third Section End -->
    <!----Second Section---->
    <div class="second-header-section">
        <div class="container ">
            <div class="row">
                <div class="col-12 text-center">
                    <div class="f-md-45 f-24 w600 text-center black-clr">
                        TOP Reasons Why You Can't Ignore
                    </div>
                    <div class="f-md-50 f-24 w700 lh140 text-center black-clr">
                        VidHostPro Bundle Deal
                    </div>
                </div>
            </div>
            <div class="row mt50 mt-md65">
                <div class="col-12 col-md-10 mx-auto">
                    <div class="step-shape">
                        <div class="f-20 f-md-22 w600 black-clr lh150">
                            Reason #1
                        </div>
                        <div class="f-18 f-md-18 w400 black-clr lh150 mt10 ">
                            GET All The Benefits Of VidHostPro & Premium Upgrades To Multiply Your Results... Save More Time, Work Like A Pro & Ultimately Boost Your Agency Profits.
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto mt30">
                    <div class="step-shape">
                        <div class="f-20 f-md-22 w600 black-clr lh150">
                            Reason #2
                        </div>
                        <div class="f-18 f-md-18 w400 black-clr lh150 mt10 ">
                            Regular Price For VidHostPro, All Upgrades & Bonuses Is $997. You Are Saving $630 Today When You Grab The Exclusive Bundle Deal Now at ONLY $367.
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto mt30">
                    <div class="step-shape">
                        <div class="f-20 f-md-22 w600 black-clr lh150">
                            Reason #3
                        </div>
                        <div class="f-18 f-md-18 w400 black-clr lh150 mt10 ">
                            This Limited Time Additional 30% Coupon Will Expire As Soon As Timer Hits Zero. So, Take Action Now.
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto mt30">
                    <div class="step-shape">
                        <div class="f-20 f-md-22 w600 black-clr lh150">
                            Reason #4
                        </div>
                        <div class="f-18 f-md-18 w400 black-clr lh150 mt10 ">
                            GET Priority Support From Our Dedicated Support Team To Assist With Your Success.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!----Second Section---->
    <div class="riskfree-section">
        <div class="container ">
            <div class="row align-items-center ">
                <div class="f-28 f-md-45 w700 white-clr text-center col-12 mb-md40">Test Drive <u>EVERYTHING</u> VidHostPro Has To Offer Risk Free For 30 Days
                </div>
                <!--<div class="col-md-7 col-12 order-md-2">-->
                <!--    <div class="f-md-18 f-18 w400 lh150 white-clr mt15 "><span class="w600">Your purchase is secured with our 30-day 100% money back guarantee.</span>-->
                <!--        <br><br>-->
                <!--        <span class="w600 ">So, you can buy with full confidence and try it for yourself and for your client's risk free.</span> If you face any Issue or don't GET results you desired after using it, just raise a ticket on support desk-->
                <!--        within 30 days and we'll refund you everything, down to the last penny.<br><br> We would like to clearly state that we do NOT offer a “no questions asked” money back guarantee. You must provide a genuine reason when you contact-->
                <!--        our support and we will refund your hard-earned money.-->
                <!--    </div>-->
                <!--</div>-->
                <!--<div class="col-md-5 order-md-1 col-12 mt20 mt-md0 ">-->
                <!--    <img src="assets/images/riskfree-img.png " class="img-fluid d-block mx-auto " alt="Risk Free">-->
                <!--</div>-->
                <div class="col-md-6 col-12 mt15 mt-md0">
                    <div class="f-md-18 f-18 w400 lh150 white-clr"><span class="w600">Your purchase is secured with our 30-day 100% money back guarantee.</span>
                        <br><br>
                        So, you can buy with full confidence and try it for yourself and for your customers risk free.&nbsp;If you face any issue or don't get the results you desire after using it, just raise a ticket on our support desk within 30 days and we'll refund you everything, down to the last penny.
                        <br><br>
                        However, we want to clearly state that we do NOT offer a “no questions asked” money back guarantee. You <span class="w700"> must </span> provide a <span class="w700"> genuine reason </span> when you contact our support and we will refund your hard-earned money.
                    </div>
                </div>
                <div class="col-md-6 col-12 mt20 mt-md0">
                    <img src="assets/images/riskfree-img.webp " class="img-fluid d-block mx-auto" alt="Money Back Guarantee">
                </div>
            </div>
        </div>
    </div>
    <!--3. Third Section Start -->
    <div class="section2">
        <div class="container">
            <div class="row ">
                <div class="col-12 px0 px-md15" id="buybundle">
                    <div class="f-md-45 f-28 black-clr  text-center w700 lh140">
                        Limited Time Offer!
                    </div>
                    <div class="f-22 f-md-34 w500 lh150 text-center mt20 black-clr">
                        GET Complete Package of All <span class="w700">VidHostPro</span> Products<br class="d-md-block d-none"> for A Low One-Time Fee
                    </div>
                </div>
                <div class="col-12 f-18 f-md-26 w400 lh150 text-center mt20 black-clr">
                    Use Coupon Code <span class="red-gradient w600 ">"BLACKFRIDAY"</span> for an Additional <span class="red-gradient w600 "> 30% Discount</span> 
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-6 col-12 mt25 mt-md50 px-md15">
                    <div class="tablebox1">
                        <div class="col-12 tablebox-inner-bg text-center">
                            <div class="relative col-12 p0">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 829.07 133.22" style="max-height:55px">
                                    <defs>
                                    <style>.cls-1{fill:#fff;}.cls-2{fill:#ec1c24;}</style>
                                    </defs>
                                    <g id="Layer_2" data-name="Layer 2">
                                    <g id="Layer_1-2" data-name="Layer 1">
                                    <path class="cls-1" d="M142.09,14.94C89.5-8.93,68.3,32,65.87,37,48.19,38.59,36.52,52.3,32.31,67.64l-1.55-.54c-2.87-1-7-2.8-9.65-5.12a51.82,51.82,0,0,1,6.14-12h0a49.93,49.93,0,0,1,3.41-4.41c9.9-11.41,22.44-15.6,26.3-15.6C72.85.51,95.07,0,99.08,0,126.45-.2,142.09,14.94,142.09,14.94Z"></path>
                                    <path class="cls-1" d="M103.29,128.84c-21.44,11.73-43.39-3.34-47.63-7.58-25.5-.6-35.11-20.89-37.43-32.69a121.37,121.37,0,0,0,14.3,4.56C36,103.65,44.31,111.7,58,112.66c23.66,18.51,45.7,7.08,45.7,7.08a40.15,40.15,0,0,0,23.47,3,56.14,56.14,0,0,0,22.25-10.11s11.38,1,18-6.81c3.34-3.95,6.36-7.52,8.22-12.5h0a114.47,114.47,0,0,0,13.44-4.5c-3.45,17.54-17.34,32.81-37.25,32.81-14,13.87-37.41,11.7-45.2,8.61"></path>
                                    <path class="cls-1" d="M185.47,61.68a37.61,37.61,0,0,1-3.63,2.23,54,54,0,0,1-6.43,2.93c-6-14-24.8-19.71-24.8-19.71s-10.49-17.19-27.1-23.39c-16.26-6.08-41.44,1-42,1.1.49-.42,21.75-18.55,53.83-6,14.36,6.17,21.74,21.44,21.74,21.44a35.85,35.85,0,0,1,5,1.56,49.15,49.15,0,0,1,9.36,4.73h0A40.26,40.26,0,0,1,185.47,61.68Z"></path>
                                    <path class="cls-2" d="M77,80.25V94c-17.82-1.23-33.48-3.67-46-7.14a100,100,0,0,1-13.4-4.67C6.24,77.22-.2,70.89,0,63.51,1.47,42.92,28.58,42.83,29,42.83c-.25.1-16.62,6.85-16.17,15.07.19,3.59,2.24,6.93,6.53,9.9a44.79,44.79,0,0,0,11.74,5.42C41.36,76.53,56.31,79,77,80.25Z"></path>
                                    <path class="cls-2" d="M189.87,82.3A99,99,0,0,1,177.23,87c-11,3.29-25.29,5.89-43.6,7.12l11.78-7a8.27,8.27,0,0,0,4.06-7.16,7.84,7.84,0,0,0-.08-1.19c10.93-1.5,20.22-3.65,27.61-6.43a58.75,58.75,0,0,0,7.56-3.4c1.15-.63,2.21-1.26,3.18-1.9,16-10.49,6.87-21.31-22.83-28,36.12,7.83,43.62,16.49,44.7,22.28C210.57,66.47,205.7,75.09,189.87,82.3Z"></path>
                                    <path class="cls-2" d="M142.5,82.2l-21,12.47-9.94,5.93-24.9,14.81a2.65,2.65,0,0,1-4-2.28V46.69a2.69,2.69,0,0,1,2-2.56l.68-.05.69,0,.68.29,50.33,30,5.48,3.27a2.66,2.66,0,0,1,0,4.57Z"></path>
                                    <path class="cls-1" d="M267.45,89.27l18.83-62.49h19.06l-28.93,83.09H258.54L229.72,26.78h19Z"></path>
                                    <path class="cls-1" d="M312,32.14a8.12,8.12,0,0,1,2.48-6.1,10.69,10.69,0,0,1,13.5,0,8.07,8.07,0,0,1,2.51,6.1,8.09,8.09,0,0,1-2.54,6.16,10.57,10.57,0,0,1-13.41,0A8.09,8.09,0,0,1,312,32.14Zm17.52,77.73H312.93V48.12h16.55Z"></path>
                                    <path class="cls-1" d="M340.49,78.54q0-14.45,6.48-23T364.69,47a18.92,18.92,0,0,1,14.9,6.73V22.21h16.55v87.66h-14.9l-.8-6.56a19.41,19.41,0,0,1-15.86,7.7,20.94,20.94,0,0,1-17.49-8.59Q340.49,93.84,340.49,78.54ZM357,79.77q0,8.69,3,13.31a9.85,9.85,0,0,0,8.79,4.63q7.65,0,10.79-6.45V66.85q-3.09-6.46-10.67-6.46Q357,60.39,357,79.77Z"></path>
                                    <path class="cls-1" d="M477.63,109.87H460.51V74.26H427.13v35.61H410V26.78h17.13V60.45h33.38V26.78h17.12Z"></path>
                                    <path class="cls-1" d="M488.93,78.43a36.59,36.59,0,0,1,3.54-16.38,25.81,25.81,0,0,1,10.19-11.13A29.67,29.67,0,0,1,518.09,47q12.51,0,20.41,7.65t8.81,20.77l.12,4.22q0,14.22-7.93,22.8T518.21,111q-13.35,0-21.32-8.56t-8-23.28Zm16.5,1.17q0,8.79,3.31,13.46a11.9,11.9,0,0,0,18.83,0q3.36-4.59,3.37-14.71,0-8.64-3.37-13.38a11,11,0,0,0-9.48-4.74A10.75,10.75,0,0,0,508.74,65C506.53,68.14,505.43,73,505.43,79.6Z"></path>
                                    <path class="cls-1" d="M591,92.81A5.32,5.32,0,0,0,588,88q-3-1.74-9.61-3.11-22-4.62-22-18.72a16.94,16.94,0,0,1,6.82-13.72Q570,47,581,47q11.76,0,18.81,5.54a17.45,17.45,0,0,1,7,14.38H590.4a8,8,0,0,0-2.28-5.85q-2.28-2.31-7.14-2.31a9.83,9.83,0,0,0-6.44,1.88,5.91,5.91,0,0,0-2.29,4.79,5.1,5.1,0,0,0,2.6,4.43q2.6,1.68,8.76,2.91A73,73,0,0,1,594,75.51q13.07,4.8,13.07,16.61,0,8.44-7.25,13.67T581.1,111a32.54,32.54,0,0,1-13.78-2.77,23,23,0,0,1-9.45-7.59,17.62,17.62,0,0,1-3.42-10.41h15.63A8.62,8.62,0,0,0,573.34,97a12.87,12.87,0,0,0,8.1,2.34q4.74,0,7.16-1.8A5.57,5.57,0,0,0,591,92.81Z"></path>
                                    <path class="cls-1" d="M637.43,32.94V48.12H648v12.1H637.43V91c0,2.28.43,3.92,1.31,4.9s2.55,1.49,5,1.49a26,26,0,0,0,4.85-.4v12.5a33.89,33.89,0,0,1-10,1.48q-17.34,0-17.69-17.52V60.22h-9V48.12h9V32.94Z"></path>
                                    <path class="cls-1" d="M675.38,80.59v29.28H658.26V26.78h32.41a37.58,37.58,0,0,1,16.47,3.42,25.29,25.29,0,0,1,10.93,9.73,27.23,27.23,0,0,1,3.82,14.35q0,12.22-8.36,19.27t-23.14,7Zm0-13.86h15.29q6.8,0,10.36-3.2t3.57-9.13a13.69,13.69,0,0,0-3.6-9.87q-3.6-3.78-9.93-3.88H675.38Z"></path>
                                    <path class="cls-1" d="M767.26,63.59a44,44,0,0,0-5.94-.46q-9.36,0-12.27,6.33v40.41H732.56V48.12h15.58l.46,7.36q5-8.5,13.75-8.5a17.45,17.45,0,0,1,5.14.74Z"></path>
                                    <path class="cls-1" d="M770.57,78.43a36.59,36.59,0,0,1,3.54-16.38,25.85,25.85,0,0,1,10.18-11.13A29.7,29.7,0,0,1,799.73,47q12.49,0,20.4,7.65T829,75.4l.12,4.22q0,14.22-7.94,22.8T799.85,111q-13.36,0-21.32-8.56t-8-23.28Zm16.49,1.17q0,8.79,3.31,13.46a11.91,11.91,0,0,0,18.84,0q3.36-4.59,3.36-14.71,0-8.64-3.36-13.38a11,11,0,0,0-9.48-4.74A10.78,10.78,0,0,0,790.37,65C788.17,68.14,787.06,73,787.06,79.6Z"></path>
                                    </g>
                                    </g>
                                </svg>
                            </div>
                            <div class="col-12 mt-md40 mt20 table-headline-shape">
                                <div class="w700 f-md-32 f-22 text-center text-uppercase black-clr lh150 ">
                                    Bundle Offer
                                </div>
                            </div>
                            <div class="d-flex align-items-center justify-content-between py15 px30 text-center">
                                <div class="w600 f-md-28 f-22 text-left text-xs-center text-uppercase white-clr lh150 mt8">
                                    Commercial
                                </div>
                                <div class="w600 f-md-35 f-22 text-center white-clr lh150">
                                    $97
                                </div>
                            </div>
                            <div class="d-flex align-items-center justify-content-between light-blue py15 px30">
                                <div class="w600 f-md-28 f-22 text-left text-xs-center text-uppercase black-clr lh150 mt8">
                                    ELITE
                                </div>
                                <div class="w600 f-md-35 f-22 text-center black-clr lh150">
                                    $297
                                </div>
                            </div>
                            <div class="d-flex align-items-center justify-content-between py15 px30">
                                <div class="w600 f-md-28 f-22 text-left text-xs-center text-uppercase white-clr lh150 mt8">
                                    ENTERPRISE
                                </div>
                                <div class="w600 f-md-35 f-22 text-center white-clr lh150">
                                    $97
                                </div>
                            </div>
                            <div class="d-flex align-items-center justify-content-between light-blue py15 px30">
                                <div class="w600 f-md-28 f-22 text-left text-xs-center text-uppercase black-clr lh150 mt8">
                                    AGENCY
                                </div>
                                <div class="w600 f-md-35 f-22 text-center black-clr lh150">
                                    $197
                                </div>
                            </div>
                            <div class="d-flex align-items-center justify-content-between py15 px30">
                                <div class="w600 f-md-28 f-22 text-left text-xs-center text-uppercase white-clr lh150 mt8">
                                    FAST ACTION BONUSES
                                </div>
                                <div class="w600 f-md-35 f-22 text-center white-clr lh150">
                                    $309
                                </div>
                            </div>
                            
                            <div class="col-12 px-md0 px15 mt-md30 mt20 w500 f-md-35 f-22 white-clr text-center">
                                Regular <strike>$997</strike>
                            </div>
                            <!--<div class="justify-content-center ">-->
                            <!--    <div class="col-md-8 col-12 now-price mt20 w700 f-md-50 f-32 white-clr text-center mx-auto ">-->
                            <!--        Now $367-->
                            <!--    </div>-->
                            <!--</div>-->
                            <div class="col-12 px-md0 px15 mt20 w500 f-md-25 f-20 white-clr text-center">
                                One Time
                            </div>
                            <div class="mt20 red-clr w600 f-22 d-md-27 lh140">
                                <img src="assets/images/red-close.webp" alt="Hurry" class="mr10 close-img">
                              No Recurring | No Monthly | No Yearly
                           </div>
                            <div class="justify-content-center ">
                                <div class="myfeatures f-md-25 f-16 w400 text-center lh160 hideme">
                                    <div class="hideme-button">
                                        <a href="https://www.jvzoo.com/b/108597/394877/2"><img src="https://i.jvzoo.com/108597/394877/2" alt="VidHostPro Bundle" border="0"  target="_blank"  class="img-fluid d-block mx-auto" /></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 px0 px-md15 mt-md50 mt20 text-center">
                    <div class="f-18 f-md-22 w400 lh150 text-center mt10 white-clr head-design">
                        Use Coupon Code <span class="black-clr w600 ">"BLACKFRIDAY"</span> for Additional <span class="black-clr w600 "> 30% Discount</span>
                    </div>
                    <div class="f-md-36 f-24 black-clr mt20 text-center w700 lh140">
                        We are always here by your side <br class="d-none d-md-block">
                        <span class="red-clr">GET in touch with us …</span>
                    </div>
                </div>
            </div>
            <div></div>
        </div>
    </div>
    <!--3. Third Section End -->
    <!--Footer Section Start -->
    <div class="footer-section">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 829.07 133.22" style="max-height:55px">
                        <defs>
                        <style>.cls-1{fill:#fff;}.cls-2{fill:#ec1c24;}</style>
                        </defs>
                        <g id="Layer_2" data-name="Layer 2">
                        <g id="Layer_1-2" data-name="Layer 1">
                        <path class="cls-1" d="M142.09,14.94C89.5-8.93,68.3,32,65.87,37,48.19,38.59,36.52,52.3,32.31,67.64l-1.55-.54c-2.87-1-7-2.8-9.65-5.12a51.82,51.82,0,0,1,6.14-12h0a49.93,49.93,0,0,1,3.41-4.41c9.9-11.41,22.44-15.6,26.3-15.6C72.85.51,95.07,0,99.08,0,126.45-.2,142.09,14.94,142.09,14.94Z"></path>
                        <path class="cls-1" d="M103.29,128.84c-21.44,11.73-43.39-3.34-47.63-7.58-25.5-.6-35.11-20.89-37.43-32.69a121.37,121.37,0,0,0,14.3,4.56C36,103.65,44.31,111.7,58,112.66c23.66,18.51,45.7,7.08,45.7,7.08a40.15,40.15,0,0,0,23.47,3,56.14,56.14,0,0,0,22.25-10.11s11.38,1,18-6.81c3.34-3.95,6.36-7.52,8.22-12.5h0a114.47,114.47,0,0,0,13.44-4.5c-3.45,17.54-17.34,32.81-37.25,32.81-14,13.87-37.41,11.7-45.2,8.61"></path>
                        <path class="cls-1" d="M185.47,61.68a37.61,37.61,0,0,1-3.63,2.23,54,54,0,0,1-6.43,2.93c-6-14-24.8-19.71-24.8-19.71s-10.49-17.19-27.1-23.39c-16.26-6.08-41.44,1-42,1.1.49-.42,21.75-18.55,53.83-6,14.36,6.17,21.74,21.44,21.74,21.44a35.85,35.85,0,0,1,5,1.56,49.15,49.15,0,0,1,9.36,4.73h0A40.26,40.26,0,0,1,185.47,61.68Z"></path>
                        <path class="cls-2" d="M77,80.25V94c-17.82-1.23-33.48-3.67-46-7.14a100,100,0,0,1-13.4-4.67C6.24,77.22-.2,70.89,0,63.51,1.47,42.92,28.58,42.83,29,42.83c-.25.1-16.62,6.85-16.17,15.07.19,3.59,2.24,6.93,6.53,9.9a44.79,44.79,0,0,0,11.74,5.42C41.36,76.53,56.31,79,77,80.25Z"></path>
                        <path class="cls-2" d="M189.87,82.3A99,99,0,0,1,177.23,87c-11,3.29-25.29,5.89-43.6,7.12l11.78-7a8.27,8.27,0,0,0,4.06-7.16,7.84,7.84,0,0,0-.08-1.19c10.93-1.5,20.22-3.65,27.61-6.43a58.75,58.75,0,0,0,7.56-3.4c1.15-.63,2.21-1.26,3.18-1.9,16-10.49,6.87-21.31-22.83-28,36.12,7.83,43.62,16.49,44.7,22.28C210.57,66.47,205.7,75.09,189.87,82.3Z"></path>
                        <path class="cls-2" d="M142.5,82.2l-21,12.47-9.94,5.93-24.9,14.81a2.65,2.65,0,0,1-4-2.28V46.69a2.69,2.69,0,0,1,2-2.56l.68-.05.69,0,.68.29,50.33,30,5.48,3.27a2.66,2.66,0,0,1,0,4.57Z"></path>
                        <path class="cls-1" d="M267.45,89.27l18.83-62.49h19.06l-28.93,83.09H258.54L229.72,26.78h19Z"></path>
                        <path class="cls-1" d="M312,32.14a8.12,8.12,0,0,1,2.48-6.1,10.69,10.69,0,0,1,13.5,0,8.07,8.07,0,0,1,2.51,6.1,8.09,8.09,0,0,1-2.54,6.16,10.57,10.57,0,0,1-13.41,0A8.09,8.09,0,0,1,312,32.14Zm17.52,77.73H312.93V48.12h16.55Z"></path>
                        <path class="cls-1" d="M340.49,78.54q0-14.45,6.48-23T364.69,47a18.92,18.92,0,0,1,14.9,6.73V22.21h16.55v87.66h-14.9l-.8-6.56a19.41,19.41,0,0,1-15.86,7.7,20.94,20.94,0,0,1-17.49-8.59Q340.49,93.84,340.49,78.54ZM357,79.77q0,8.69,3,13.31a9.85,9.85,0,0,0,8.79,4.63q7.65,0,10.79-6.45V66.85q-3.09-6.46-10.67-6.46Q357,60.39,357,79.77Z"></path>
                        <path class="cls-1" d="M477.63,109.87H460.51V74.26H427.13v35.61H410V26.78h17.13V60.45h33.38V26.78h17.12Z"></path>
                        <path class="cls-1" d="M488.93,78.43a36.59,36.59,0,0,1,3.54-16.38,25.81,25.81,0,0,1,10.19-11.13A29.67,29.67,0,0,1,518.09,47q12.51,0,20.41,7.65t8.81,20.77l.12,4.22q0,14.22-7.93,22.8T518.21,111q-13.35,0-21.32-8.56t-8-23.28Zm16.5,1.17q0,8.79,3.31,13.46a11.9,11.9,0,0,0,18.83,0q3.36-4.59,3.37-14.71,0-8.64-3.37-13.38a11,11,0,0,0-9.48-4.74A10.75,10.75,0,0,0,508.74,65C506.53,68.14,505.43,73,505.43,79.6Z"></path>
                        <path class="cls-1" d="M591,92.81A5.32,5.32,0,0,0,588,88q-3-1.74-9.61-3.11-22-4.62-22-18.72a16.94,16.94,0,0,1,6.82-13.72Q570,47,581,47q11.76,0,18.81,5.54a17.45,17.45,0,0,1,7,14.38H590.4a8,8,0,0,0-2.28-5.85q-2.28-2.31-7.14-2.31a9.83,9.83,0,0,0-6.44,1.88,5.91,5.91,0,0,0-2.29,4.79,5.1,5.1,0,0,0,2.6,4.43q2.6,1.68,8.76,2.91A73,73,0,0,1,594,75.51q13.07,4.8,13.07,16.61,0,8.44-7.25,13.67T581.1,111a32.54,32.54,0,0,1-13.78-2.77,23,23,0,0,1-9.45-7.59,17.62,17.62,0,0,1-3.42-10.41h15.63A8.62,8.62,0,0,0,573.34,97a12.87,12.87,0,0,0,8.1,2.34q4.74,0,7.16-1.8A5.57,5.57,0,0,0,591,92.81Z"></path>
                        <path class="cls-1" d="M637.43,32.94V48.12H648v12.1H637.43V91c0,2.28.43,3.92,1.31,4.9s2.55,1.49,5,1.49a26,26,0,0,0,4.85-.4v12.5a33.89,33.89,0,0,1-10,1.48q-17.34,0-17.69-17.52V60.22h-9V48.12h9V32.94Z"></path>
                        <path class="cls-1" d="M675.38,80.59v29.28H658.26V26.78h32.41a37.58,37.58,0,0,1,16.47,3.42,25.29,25.29,0,0,1,10.93,9.73,27.23,27.23,0,0,1,3.82,14.35q0,12.22-8.36,19.27t-23.14,7Zm0-13.86h15.29q6.8,0,10.36-3.2t3.57-9.13a13.69,13.69,0,0,0-3.6-9.87q-3.6-3.78-9.93-3.88H675.38Z"></path>
                        <path class="cls-1" d="M767.26,63.59a44,44,0,0,0-5.94-.46q-9.36,0-12.27,6.33v40.41H732.56V48.12h15.58l.46,7.36q5-8.5,13.75-8.5a17.45,17.45,0,0,1,5.14.74Z"></path>
                        <path class="cls-1" d="M770.57,78.43a36.59,36.59,0,0,1,3.54-16.38,25.85,25.85,0,0,1,10.18-11.13A29.7,29.7,0,0,1,799.73,47q12.49,0,20.4,7.65T829,75.4l.12,4.22q0,14.22-7.94,22.8T799.85,111q-13.36,0-21.32-8.56t-8-23.28Zm16.49,1.17q0,8.79,3.31,13.46a11.91,11.91,0,0,0,18.84,0q3.36-4.59,3.36-14.71,0-8.64-3.36-13.38a11,11,0,0,0-9.48-4.74A10.78,10.78,0,0,0,790.37,65C788.17,68.14,787.06,73,787.06,79.6Z"></path>
                        </g>
                        </g>
                    </svg>
                    <br><br><br>
                    <!-- <div editabletype="text" class="f-16 f-md-16 w300 mt20 lh140 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div> -->
                </div>
                <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                    <div class="f-16 f-md-16 w300 lh140 white-clr text-xs-center">Copyright © VidHostPro</div>
                    <ul class="footer-ul w300 f-16 f-md-16 white-clr text-center text-md-right">
                        <li><a href="https://support.oppyo.com/hc/en-us"  target="_blank"  class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                        <li><a href="http://vidhostpro.co/legal/privacy-policy.html" target="_blank"  class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                        <li><a href="http://vidhostpro.co/legal/terms-of-service.html" target="_blank"  class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                        <li><a href="http://vidhostpro.co/legal/disclaimer.html"  target="_blank" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                        <li><a href="http://vidhostpro.co/legal/gdpr.html"  target="_blank" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                        <li><a href="http://vidhostpro.co/legal/dmca.html"  target="_blank" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                        <li><a href="http://vidhostpro.co/legal/anti-spam.html"  target="_blank" class="white-clr t-decoration-none">Anti-Spam</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!--Footer Section End -->
    
    <?php
         if ($now < $exp_date) {
         
         ?>
      <script type="text/javascript">
         // Count down milliseconds = server_end - server_now = client_end - client_now
         var server_end = <?php echo $exp_date; ?> * 1000;
         var server_now = <?php echo time(); ?> * 1000;
         var client_now = new Date().getTime();
         var end = server_end - server_now + client_now; // this is the real end time
         
         var noob = $('.countdown').length;
         
         var _second = 1000;
         var _minute = _second * 60;
         var _hour = _minute * 60;
         var _day = _hour * 24
         var timer;
         
         function showRemaining() {
             var now = new Date();
             var distance = end - now;
             if (distance < 0) {
                 clearInterval(timer);
                 document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
                 return;
             }
         
             var days = Math.floor(distance / _day);
             var hours = Math.floor((distance % _day) / _hour);
             var minutes = Math.floor((distance % _hour) / _minute);
             var seconds = Math.floor((distance % _minute) / _second);
             if (days < 10) {
                 days = "0" + days;
             }
             if (hours < 10) {
                 hours = "0" + hours;
             }
             if (minutes < 10) {
                 minutes = "0" + minutes;
             }
             if (seconds < 10) {
                 seconds = "0" + seconds;
             }
             var i;
             var countdown = document.getElementsByClassName('countdown');
             for (i = 0; i < noob; i++) {
                 countdown[i].innerHTML = '';
         
                 if (days) {
                     countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-40 f-md-40 timerbg">' + days + '</span><br><span class="f-20 w600">Days</span> </div>';
                 }
         
                 countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-40 f-md-40 timerbg">' + hours + '</span><br><span class="f-20 w600">Hours</span> </div>';
         
                 countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-40 f-md-40 timerbg">' + minutes + '</span><br><span class="f-20 w600">Mins</span> </div>';
         
                 countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-40 f-md-40 timerbg">' + seconds + '</span><br><span class="f-20 w600">Sec</span> </div>';
             }
         
         }
         timer = setInterval(showRemaining, 1000);
      </script>
    
      <?php
         } else {
         echo "Times Up";
         }
         ?>
      <!--- timer end-->
    <!-- Bottom End -->
    <script id="rendered-js" >
        $('.bxslider.text').bxSlider({
          mode: 'vertical',
          pager: false,
          controls: false,
          infiniteLoop: true,
          auto: true,
          speed: 300,
          pause: 2000 });
               
     </script>
    
</body>

</html>
<!Doctype html>
<html>
   <head>
      <title>Entertainerz Special</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=9">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
      <meta name="title" content="Entertainerz | Special">
      <meta name="description" content="Entertainerz">
      <meta name="keywords" content="Entertainerz">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta property="og:image" content="https://www.entertainerz.co/special/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Ayush Jain">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="Entertainerz | Special">
      <meta property="og:description" content="Entertainerz">
      <meta property="og:image" content="https://www.entertainerz.co/special/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="Entertainerz | Special">
      <meta property="twitter:description" content="Entertainerz">
      <meta property="twitter:image" content="https://www.entertainerz.co/special/thumbnail.png">
      <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Spartan:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
      <!-- Start Editor required -->
      <link rel="stylesheet" href="https://cdn.oppyo.com/launches/webpull/common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style-feature.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style-bottom.css" type="text/css">
      <link rel="stylesheet" href="assets/css/timer.css" type="text/css">
      <script src="https://cdn.oppyo.com/launches/webpull/common_assets/js/jquery.min.js"></script>  
      <script>
         (function(w, i, d, g, e, t, s) {
             if (window.businessDomain != undefined) {
                 console.log("Your page have duplicate embed code. Please check it.");
                 return false;
             }
             businessDomain = 'entertainerz';
             allowedDomain = 'entertainerz.co';
             if (!window.location.hostname.includes(allowedDomain)) {
                 console.log("Your page have not authorized. Please check it.");
                 return false;
             }
             console.log("Your script is ready...");
             w[d] = w[d] || [];
             t = i.createElement(g);
             t.async = 1;
             t.src = e;
             s = i.getElementsByTagName(g)[0];
             s.parentNode.insertBefore(t, s);
         })(window, document, '_gscq', 'script', 'https://cdn.staticdcp.com/apps/engage/smart_engage/js/config-loader.js');
      </script>
   </head>
   <body>
      <!-- Header Section Start -->
      <div class="header-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row">
                     <div class="col-md-3 text-center text-md-start">
                        <svg id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 189.77 50" style="max-height:80px">
                           <defs>
                              <style>.cls-1{fill:url(#linear-gradient-2);}.cls-2{fill:#fff;}.cls-3{fill:#001730;}.cls-4{fill:url(#linear-gradient-3);}.cls-5{fill:url(#linear-gradient);}</style>
                              <linearGradient id="linear-gradient" x1="20.65" y1="26.83" x2="42.69" y2="26.83" gradientUnits="userSpaceOnUse">
                                 <stop offset="0" stop-color="#bf003b"/>
                                 <stop offset="1" stop-color="#e23940"/>
                              </linearGradient>
                              <linearGradient id="linear-gradient-2" x1=".12" y1="29.26" x2="25.92" y2="29.26" gradientUnits="userSpaceOnUse">
                                 <stop offset="0" stop-color="#f38227"/>
                                 <stop offset="1" stop-color="#fbb826"/>
                              </linearGradient>
                              <linearGradient id="linear-gradient-3" x1="3.09" y1="36.12" x2="22.33" y2="28.06" gradientUnits="userSpaceOnUse">
                                 <stop offset="0" stop-color="#3369e9"/>
                                 <stop offset="1" stop-color="#002b7f"/>
                              </linearGradient>
                           </defs>
                           <path class="cls-2" d="M22.29,0c-.94,0-1.69,.76-1.69,1.69,0,.75,.48,1.39,1.16,1.61l-2.95,10.01-.61,2.09,2.13,.26,.41-2.75v-.03s1.44-9.5,1.44-9.5c.03,0,.07,0,.11,0,.93,0,1.69-.76,1.69-1.7s-.76-1.69-1.69-1.69Z"/>
                           <path class="cls-2" d="M16.2,15.76l-1.74,.96-5.3-6.31c-.29,.23-.64,.36-1.03,.36-.94,0-1.7-.75-1.7-1.69s.76-1.69,1.7-1.69,1.69,.76,1.69,1.69c0,.41-.15,.79-.4,1.08l6.78,5.6Z"/>
                           <g>
                              <path class="cls-2" d="M63.68,34.98c0,.14-.04,.35-.11,.62-.08,.34-.13,.55-.15,.62-.03,.32-.09,.79-.19,1.41-.05,.16-.16,.24-.35,.24-.09,0-.26-.02-.51-.05-1.1-.16-2.82-.24-5.13-.24-.66,0-1.64,.02-2.96,.05s-2.31,.05-2.96,.05c-.3,0-.44-.13-.44-.39,0-1.01,.06-2.52,.17-4.53,.11-2.02,.17-3.53,.17-4.53s-.05-2.93-.16-5.72c-.02-.72-.07-1.79-.15-3.22v-.19c-.02-.21,.09-.31,.34-.31,.63,0,1.57,.02,2.83,.05,1.26,.04,2.2,.05,2.83,.05s1.5-.02,2.7-.05c1.2-.04,2.1-.05,2.7-.05,.42,0,.65,.13,.69,.39,.19,1.19,.28,1.92,.28,2.21,0,.21-.1,.31-.3,.31-.76,0-1.92-.04-3.47-.13-1.55-.09-2.71-.13-3.5-.13-.74,0-1.2,.1-1.39,.3-.19,.2-.29,.69-.29,1.47v2.01c0,.55,.01,.87,.04,.96,.08,.28,.31,.42,.7,.42,.11,0,.26,0,.46-.01h.46c1.76,0,3.34-.03,4.74-.09,.55-.02,1.19-.06,1.94-.13,.19-.02,.28,.06,.28,.24,0,.29-.03,.73-.1,1.32-.07,.59-.1,1.04-.1,1.33,0,.16-.13,.24-.4,.24-.11,0-.51-.03-1.2-.09-.5-.05-1.63-.08-3.39-.08-1.48,0-2.42,.02-2.82,.05-.36,.03-.57,.22-.62,.57,0-.06,.01,.28,.01,1.04v1.99c0,.93,.14,1.52,.43,1.75,.22,.19,.79,.28,1.68,.28h1.48c.63-.01,2.39-.12,5.29-.34h.2c.18-.03,.27,.08,.27,.32Z"/>
                              <path class="cls-2" d="M77.61,37.42c0,.2-.09,.3-.27,.3-.3,0-.74,0-1.33-.01-.59,0-1.03-.01-1.33-.01-.16,0-.24-.13-.24-.39,0-.44,.01-1.11,.04-2.01,.03-.9,.04-1.57,.04-2.02,0-.36,0-.9-.01-1.61,0-.71-.01-1.25-.01-1.61,0-1.02-.15-1.75-.44-2.17-.36-.5-1.02-.75-1.99-.75-.45,0-1.07,.25-1.87,.75-.84,.53-1.27,1-1.27,1.41v8.08c0,.22-.09,.34-.26,.34-.29,0-.72,0-1.29-.01-.57,0-1.01-.01-1.29-.01-.19,0-.28-.1-.28-.31,0-.71,.01-1.77,.03-3.19,.02-1.42,.03-2.49,.03-3.21,0-1.91-.2-3.68-.61-5.29-.03-.08-.04-.14-.04-.18,0-.09,.06-.15,.18-.19,.06,0,.58-.09,1.54-.25,.97-.16,1.47-.24,1.52-.24,.05,0,.09,.07,.11,.22,.04,.56,.15,1.12,.31,1.68,.47-.37,1.08-.83,1.85-1.37,.94-.57,1.85-.86,2.73-.86,1.69,0,2.84,.49,3.46,1.47,.46,.72,.69,1.88,.69,3.49,0,.28,0,.7-.02,1.28-.01,.57-.02,1.01-.02,1.29,0,.6,0,1.5,.03,2.69,.02,1.19,.03,2.09,.03,2.69Z"/>
                              <path class="cls-2" d="M87.67,35.21c0,.2-.02,.47-.05,.81-.05,.42-.07,.7-.08,.82,0,.16-.02,.26-.04,.3-.04,.06-.12,.13-.26,.19-.74,.35-1.89,.53-3.48,.53-2.1,0-3.15-.95-3.15-2.86,0-.76,.02-1.91,.05-3.44,.04-1.53,.05-2.68,.05-3.44,0-.37-.18-.57-.55-.59-.33,0-.66,0-.98-.01-.19-.04-.28-.41-.28-1.1,0-.26,.02-.53,.05-.82,.03-.22,.17-.35,.43-.39,.22,0,.44,0,.66-.01,.41-.02,.62-.2,.62-.55s-.01-.91-.03-1.64c-.02-.74-.03-1.29-.03-1.66,0-.66,.13-.98,.38-.98,.08,0,.92,.14,2.53,.42,.21,.03,.31,.13,.31,.3,0,.4-.03,.99-.09,1.78-.06,.79-.09,1.38-.09,1.78,0,.26,.12,.39,.36,.39h3.23c.15,0,.23,.05,.23,.16s-.02,.28-.05,.52c-.03,.24-.05,.42-.05,.53,0,.13,0,.34,.01,.62,0,.28,.01,.48,.01,.62,0,.17-.13,.26-.38,.26-.36,0-.9-.02-1.63-.07-.73-.05-1.27-.07-1.63-.07-.07,0-.13,.26-.16,.77-.05,.64-.07,1.53-.07,2.68v2.01c0,.77,.07,1.31,.22,1.62,.22,.47,.66,.7,1.33,.7,.28,0,.69-.06,1.23-.18s.94-.18,1.2-.18c.11,0,.16,.08,.16,.24Z"/>
                              <path class="cls-2" d="M101.01,30.46c0,.66-.29,1.11-.86,1.33-.39,.14-3.17,.41-8.34,.79,.12,.79,.55,1.46,1.31,2.02,.72,.53,1.49,.79,2.32,.79,1.42,0,2.72-.46,3.91-1.37,.14-.12,.29-.23,.43-.35,.07,0,.12,0,.14,.03,.02,.02,.19,.33,.51,.94,.31,.61,.47,.93,.47,.98,0,.05-.07,.16-.22,.31-1.32,1.4-3.01,2.1-5.08,2.1s-3.74-.62-4.98-1.87c-1.24-1.24-1.86-2.9-1.86-4.98,0-1.85,.59-3.46,1.78-4.84,1.25-1.43,2.78-2.14,4.59-2.14,1.69,0,3.1,.62,4.23,1.86,1.1,1.2,1.66,2.67,1.66,4.39Zm-3.21-.82c0-.75-.29-1.41-.88-1.98s-1.25-.86-1.99-.86c-.83,0-1.56,.33-2.2,1-.64,.66-.96,1.41-.96,2.24,0,.19,.16,.28,.48,.28,1.33,0,2.92-.13,4.77-.4,.52-.07,.78-.16,.78-.27Z"/>
                              <path class="cls-2" d="M110.6,25.04c0,.99-.07,1.89-.2,2.69-.03,.21-.12,.31-.27,.31-.19,0-.47-.02-.86-.06-.38-.04-.67-.06-.87-.06-1.13,0-1.85,.32-2.17,.96-.19,.39-.28,1.19-.28,2.41v1.48c0,.5,.02,1.27,.06,2.29,.04,1.02,.06,1.79,.06,2.29,0,.24-.1,.36-.31,.36-.28,0-.7,0-1.27-.01-.57,0-.99-.01-1.27-.01-.21,0-.31-.11-.31-.34,0-.73,.01-1.82,.03-3.29,.02-1.46,.03-2.56,.03-3.3,0-2.22-.21-4-.62-5.35-.02-.07-.03-.13-.03-.16,0-.11,.06-.18,.18-.23,.33-.04,.8-.09,1.4-.16,.97-.18,1.42-.27,1.35-.27,.14,0,.24,.26,.28,.79,.04,.53,.13,.79,.24,.79,.02,0,.04,0,.07-.03,.37-.23,.74-.47,1.1-.7,.4-.24,.82-.42,1.24-.54,.35-.11,.8-.16,1.35-.16,.7,0,1.05,.09,1.05,.28Z"/>
                              <path class="cls-2" d="M119.58,35.21c0,.2-.02,.47-.05,.81-.05,.42-.07,.7-.08,.82,0,.16-.02,.26-.04,.3-.04,.06-.12,.13-.26,.19-.74,.35-1.89,.53-3.48,.53-2.1,0-3.15-.95-3.15-2.86,0-.76,.02-1.91,.05-3.44,.04-1.53,.05-2.68,.05-3.44,0-.37-.18-.57-.55-.59-.33,0-.66,0-.98-.01-.19-.04-.28-.41-.28-1.1,0-.26,.02-.53,.05-.82,.03-.22,.17-.35,.43-.39,.22,0,.44,0,.66-.01,.41-.02,.62-.2,.62-.55s-.01-.91-.03-1.64c-.02-.74-.03-1.29-.03-1.66,0-.66,.13-.98,.38-.98,.08,0,.92,.14,2.53,.42,.21,.03,.31,.13,.31,.3,0,.4-.03,.99-.09,1.78-.06,.79-.09,1.38-.09,1.78,0,.26,.12,.39,.36,.39h3.23c.15,0,.23,.05,.23,.16s-.02,.28-.05,.52c-.03,.24-.05,.42-.05,.53,0,.13,0,.34,.01,.62,0,.28,.01,.48,.01,.62,0,.17-.13,.26-.38,.26-.36,0-.9-.02-1.63-.07-.73-.05-1.27-.07-1.63-.07-.07,0-.13,.26-.16,.77-.05,.64-.07,1.53-.07,2.68v2.01c0,.77,.07,1.31,.22,1.62,.22,.47,.66,.7,1.33,.7,.28,0,.69-.06,1.23-.18s.94-.18,1.2-.18c.11,0,.16,.08,.16,.24Z"/>
                              <path class="cls-2" d="M134.6,25.5s-.01,.12-.04,.24c-.35,2.06-.53,3.83-.53,5.32,0,.09,.11,2.11,.34,6.05v.19c.02,.26-.1,.39-.34,.39s-.62,.03-1.12,.09c-.5,.06-.87,.09-1.11,.09-.17,0-.3-.31-.39-.94-.09-.63-.18-.94-.26-.94-.05,0-.3,.18-.73,.53-.53,.43-1.02,.76-1.48,.98-.71,.36-1.42,.54-2.13,.54-1.74,0-3.21-.7-4.4-2.09-1.13-1.33-1.7-2.88-1.7-4.65,0-1.99,.57-3.64,1.71-4.94,1.19-1.36,2.77-2.03,4.71-2.03,1.37,0,2.57,.48,3.6,1.43,.15,.18,.39,.44,.71,.78,.03,.03,.05,.04,.08,.04,.05,0,.13-.28,.23-.85,.1-.57,.22-.85,.38-.85,.24,0,.7,.07,1.36,.2,.74,.16,1.1,.31,1.1,.43Zm-3.43,5.63c0-1.1-.33-2.05-.98-2.84-.69-.85-1.58-1.28-2.65-1.28s-1.98,.42-2.69,1.27c-.69,.81-1.04,1.76-1.04,2.86s.35,2.03,1.04,2.83c.72,.84,1.62,1.25,2.69,1.25s1.93-.43,2.64-1.28c.66-.8,1-1.73,1-2.8Z"/>
                              <path class="cls-2" d="M140.3,21.58c0,1.1-.62,1.66-1.85,1.66s-1.87-.55-1.87-1.66c0-.49,.19-.9,.58-1.23,.36-.31,.79-.46,1.29-.46s.94,.16,1.3,.47c.36,.31,.55,.72,.55,1.21Zm-.09,3.65c0,.69-.04,1.74-.13,3.13-.09,1.4-.13,2.44-.13,3.15,0,.66,.01,1.64,.04,2.94,.03,1.31,.04,2.28,.04,2.93,0,.19-.08,.28-.24,.28h-2.63c-.19,0-.28-.18-.28-.55,0-.62,.02-1.55,.05-2.8,.03-1.24,.05-2.18,.05-2.81,0-.7-.05-1.75-.16-3.15s-.15-2.45-.15-3.15c0-.15,.09-.23,.28-.23,.16,0,.41,.02,.75,.07,.34,.04,.59,.07,.76,.07s.42-.02,.76-.07c.34-.04,.59-.07,.76-.07,.15,0,.23,.09,.23,.26Z"/>
                              <path class="cls-2" d="M154.74,37.42c0,.2-.09,.3-.27,.3-.3,0-.74,0-1.33-.01-.59,0-1.03-.01-1.33-.01-.16,0-.24-.13-.24-.39,0-.44,.01-1.11,.04-2.01,.03-.9,.04-1.57,.04-2.02,0-.36,0-.9-.01-1.61,0-.71-.01-1.25-.01-1.61,0-1.02-.15-1.75-.44-2.17-.36-.5-1.02-.75-1.99-.75-.45,0-1.07,.25-1.87,.75-.84,.53-1.27,1-1.27,1.41v8.08c0,.22-.09,.34-.26,.34-.29,0-.72,0-1.29-.01-.57,0-1.01-.01-1.29-.01-.19,0-.28-.1-.28-.31,0-.71,.01-1.77,.03-3.19,.02-1.42,.03-2.49,.03-3.21,0-1.91-.2-3.68-.61-5.29-.03-.08-.04-.14-.04-.18,0-.09,.06-.15,.18-.19,.06,0,.58-.09,1.54-.25,.97-.16,1.47-.24,1.52-.24,.05,0,.09,.07,.11,.22,.04,.56,.15,1.12,.31,1.68,.47-.37,1.08-.83,1.85-1.37,.94-.57,1.85-.86,2.73-.86,1.69,0,2.84,.49,3.46,1.47,.46,.72,.69,1.88,.69,3.49,0,.28,0,.7-.02,1.28-.01,.57-.02,1.01-.02,1.29,0,.6,0,1.5,.03,2.69,.02,1.19,.03,2.09,.03,2.69Z"/>
                              <path class="cls-2" d="M168.7,30.46c0,.66-.29,1.11-.86,1.33-.39,.14-3.17,.41-8.34,.79,.12,.79,.55,1.46,1.31,2.02,.72,.53,1.49,.79,2.32,.79,1.42,0,2.72-.46,3.91-1.37,.14-.12,.29-.23,.43-.35,.07,0,.12,0,.14,.03,.02,.02,.19,.33,.51,.94,.31,.61,.47,.93,.47,.98,0,.05-.07,.16-.22,.31-1.32,1.4-3.01,2.1-5.08,2.1s-3.74-.62-4.98-1.87c-1.24-1.24-1.86-2.9-1.86-4.98,0-1.85,.59-3.46,1.78-4.84,1.25-1.43,2.78-2.14,4.59-2.14,1.69,0,3.1,.62,4.23,1.86,1.1,1.2,1.66,2.67,1.66,4.39Zm-3.21-.82c0-.75-.29-1.41-.88-1.98s-1.25-.86-1.99-.86c-.83,0-1.56,.33-2.2,1-.64,.66-.96,1.41-.96,2.24,0,.19,.16,.28,.48,.28,1.33,0,2.92-.13,4.77-.4,.52-.07,.78-.16,.78-.27Z"/>
                              <path class="cls-2" d="M178.29,25.04c0,.99-.07,1.89-.2,2.69-.03,.21-.12,.31-.27,.31-.19,0-.47-.02-.86-.06-.38-.04-.67-.06-.87-.06-1.13,0-1.85,.32-2.17,.96-.19,.39-.28,1.19-.28,2.41v1.48c0,.5,.02,1.27,.06,2.29s.06,1.79,.06,2.29c0,.24-.1,.36-.31,.36-.28,0-.7,0-1.27-.01-.57,0-.99-.01-1.27-.01-.21,0-.31-.11-.31-.34,0-.73,.01-1.82,.03-3.29,.02-1.46,.03-2.56,.03-3.3,0-2.22-.21-4-.62-5.35-.02-.07-.03-.13-.03-.16,0-.11,.06-.18,.18-.23,.33-.04,.8-.09,1.4-.16,.97-.18,1.42-.27,1.35-.27,.14,0,.24,.26,.28,.79,.04,.53,.13,.79,.24,.79,.02,0,.04,0,.07-.03,.37-.23,.74-.47,1.1-.7,.4-.24,.82-.42,1.24-.54,.35-.11,.8-.16,1.35-.16,.7,0,1.05,.09,1.05,.28Z"/>
                              <path class="cls-2" d="M189.77,35.13s-.07,.36-.2,.96c-.14,.64-.22,1.03-.24,1.19-.04,.31-.12,.47-.24,.47-.31,0-.74-.02-1.27-.05-.6-.04-1.02-.07-1.25-.08-.6-.02-1.46-.03-2.59-.03-.87,0-1.84,.04-2.9,.11-1.33,.09-1.9,.13-1.71,.13-.19,0-.31-.08-.36-.24-.13-.43-.19-.93-.19-1.51,0-.08,0-.2,.01-.34s.01-.26,.01-.33c0-.13,.1-.28,.29-.45,.19-.18,1.15-1.19,2.88-3.05,.87-.91,2.11-2.3,3.7-4.19h-5.94c-.14,0-.22-.09-.22-.28,0-.26,.06-.65,.17-1.17,.11-.52,.17-.9,.17-1.15,0-.11,.13-.16,.38-.16,.5,0,1.25,.02,2.25,.07s1.75,.07,2.25,.07c2.46,0,3.85,.02,4.16,.05,.22,.03,.34,.12,.34,.28,0,1.36-.05,2.12-.16,2.29-.09,.13-.87,1.01-2.33,2.65-.94,1.07-2.37,2.66-4.27,4.77,2.99-.08,5.31-.12,6.95-.12,.21,0,.31,.04,.31,.12Z"/>
                           </g>
                           <path class="cls-2" d="M16.61,49.17c.96-1.64,6.11-6.32,13.48-6.77,2.78-.17,5.12,.34,6.8,.88,0,0,.84,.29,.15,.94s-11.41-1.5-19.36,5.17c-1.6,1.37-1.2,.01-1.07-.22Z"/>
                           <path class="cls-5" d="M42.69,31.94c-.01,2.52-.67,4.93-2.77,6.32-1.62,1.06-3.89,2.45-6.95,3.79,.63-1.27,.93-2.75,.99-4.28,.01-.21,.01-.43,.01-.64,.01-9.53-3.63-17.69-6.99-21.14-.16-.16-.32-.32-.5-.47-1.51-1.33-3.74-2.37-5.74-2.61-.03,0-.06-.01-.09-.01,.03-.01,.07-.01,.1-.02,3.12-.66,6.47-1.1,9.93-1.26,2.07-.09,4.7,1.12,6.16,2.62,2.81,2.89,5.86,9.72,5.85,17.7Z"/>
                           <path class="cls-1" d="M33.97,37.13c0,.21,0,.43-.01,.64-.06,1.53-.36,3.01-.99,4.28-.27,.12-.54,.23-.82,.35-3.01,1.24-6.73,2.41-11.29,3.21v-.21c0-.47-.01-.95-.03-1.41-.02-.5-.04-.99-.07-1.48-.01-.04-.01-.07-.01-.11-.03-.46-.07-.91-.12-1.36-.02-.25-.05-.49-.08-.74-.08-.69-.17-1.36-.28-2.03-.07-.45-.15-.89-.23-1.32-.01-.08-.03-.17-.05-.25-.1-.54-.22-1.08-.34-1.6-.03-.13-.06-.26-.09-.39-.11-.44-.22-.87-.34-1.3-.11-.44-.24-.87-.37-1.29-.06-.2-.12-.4-.19-.6-.14-.44-.28-.88-.44-1.3-.18-.51-.37-1.01-.56-1.5-.25-.62-.51-1.22-.77-1.8-.6-1.29-1.23-2.47-1.88-3.53-.12-.2-.25-.4-.38-.59-.12-.2-.25-.39-.38-.57-.63-.92-1.28-1.72-1.92-2.38-.16-.16-.33-.33-.51-.48-.02-.03-.05-.05-.08-.08-.06-.05-.12-.1-.18-.15-.45-.38-.95-.74-1.48-1.07-.14-.09-.28-.17-.42-.25-.48-.28-.98-.53-1.49-.75,1.46-.71,3.09-1.4,4.87-2.04,.53-.18,1.07-.37,1.62-.54,1.32-.43,2.71-.82,4.15-1.18,.6-.14,1.22-.28,1.84-.41,.03,0,.06,.01,.09,.01,2,.24,4.23,1.28,5.74,2.61,.18,.15,.34,.31,.5,.47,3.36,3.45,7,11.61,6.99,21.14Z"/>
                           <path class="cls-4" d="M20.86,45.61c-3.31,.58-7.05,.95-11.28,1.01-.83,.01-1.62-.36-2.33-.79-2.42-1.46-5.57-7.74-7.18-18.65-.38-2.58,.81-5,2.76-6.72,1.27-1.12,3.09-2.28,5.33-3.39,1.36,.59,2.64,1.4,3.65,2.3,.18,.16,.34,.32,.5,.48,3.71,3.82,7.71,12.34,8.42,22.55,.08,1.05,.11,2.12,.11,3.21Z"/>
                           <path class="cls-2" d="M6.78,22.31c3.46-3.05,12.1-6.51,21.9-6.95,1.63-.07,3.7,.88,4.84,2.06,2.21,2.28,4.61,7.65,4.6,13.93,0,1.98-.64,3.72-2.18,4.97-3.42,2.78-10.62,7.14-23.86,6.57-.65-.03-1.28-.28-1.84-.62-1.9-1.15-4.38-6.09-5.65-14.67-.3-2.03,.64-3.93,2.17-5.29Z"/>
                           <path class="cls-3" d="M26.11,26.61l-5.61-1.56c-1.31-.36-2.85,.26-3.44,1.38-.23,.43-.29,.9-.17,1.33l1.51,5.69c.3,1.12,1.67,1.73,3.08,1.36,.54-.14,1.04-.42,1.4-.8l4.1-4.13c.96-.96,.86-2.3-.21-2.99-.2-.13-.42-.22-.66-.29h0Z"/>
                        </svg>
                     </div>
                     <div class="col-md-7 mt20 mt-md5">
                        <ul class="leader-ul f-md-18 f-16 w500 white-clr text-md-end text-center">
                           <li><a href="#features" class="white-clr t-decoration-none">Features</a><span class="pl9 white-clr">|</span></li>
                           <li><a href="#demo" class="white-clr t-decoration-none">Demo</a></li>
                           <li class="d-md-none affiliate-link-btn"><a href="https://warriorplus.com/o2/buy/h5143v/y8157d/g63083" class="white-clr t-decoration-none ">Buy Now</a></li>
                        </ul>
                     </div>
                     <div class="col-md-2 affiliate-link-btn text-md-end text-center mt15 mt-md0 d-none d-md-block">
                        <a href="https://warriorplus.com/o2/buy/h5143v/y8157d/g63083">Buy Now</a>
                     </div>
                  </div>
               </div>
               <div class="col-12 text-center lh150 mt20 mt-md50">
                  <div class="f-md-20 f-18 w600 lh150 white-clr">
                  <span class="orange-clr">Legally Exploit a Little-Known Loophole</span> to Instantly Get Access to <br class="d-none d-md-block"> 800 million+ YouTube Videos & Entertainment News…
                  </div>
                  <img src="https://cdn.oppyo.com/launches/entertainerz/special/preheadline.webp" class="img-fluid d-block mx-auto">
               </div>
               <!-- <div class="col-12 mt-md30 mt20 f-md-40 f-28 w600 text-center white-clr lh150">
               3-Click SIMPLE Software <span class="orange-clr w800">Makes Us $528/Day Creating AUTO-Updating & Traffic-Pulling Entertainment Sites</span> in Just 60 Seconds…
               </div> -->
               <!-- <div class="col-12 mt-md30 mt20 f-md-40 f-28 w600 text-center white-clr lh150">
               SUPER SIMPLE Software <span class="orange-clr w800">Makes Us $528/Day Creating AUTO-Updating & Traffic-Pulling Entertainment Sites</span> in Just 7 Minutes…
               </div> -->
               <div class="col-12 mt-md30 mt20 f-md-40 f-28 w600 text-center white-clr lh150">
               SUPER SIMPLE Software <span class="orange-clr w800"> Makes Us $528/Day Creating AUTO-Updating & Traffic-Pulling Entertainment News Sites </span> in Just 60 Seconds…
               </div>
               <div class="col-12 mt-md25 mt20 f-18 f-md-21 w600 text-center lh150 white-clr">
               No Content Creation. No Camera. No Tech Hassles Ever... 100% Beginner Friendly!
               </div>
            </div>
            <div class="row mt20 mt-md40 gx-md-5 align-items-center">
               <div class="col-md-7 col-12">
                  <!-- <img src="https://cdn.oppyo.com/launches/entertainerz/special/proudly.webp" class="img-fluid d-block mx-auto"> -->
                  <div class="col-12 responsive-video">
                  <iframe src="https://entertainerz.dotcompal.com/video/embed/u1o4dj3t3c" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                     </div>
                  <div class="col-12 mt20 mt-md40 d-none d-md-block">
                     <div class="f-22 f-md-24 w500 lh140 text-center white-clr">
                        (Get Free 30 Reseller License + Commercial License + Special Bonuses)
                     </div>
                     <div class="row">
                        <div class="col-12 mt20 mt-md20 text-center ">
                           <a href="https://warriorplus.com/o2/buy/h5143v/y8157d/g63083" class="cta-link-btn px0 d-block">Get Instant Access To Entertainerz</a>
                        </div>
                     </div>
                     <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30 ">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/compaitable-with1.webp " class="img-fluid d-block mx-xs-center md-img-right ">
                        <div class="d-md-block d-none px-md30"><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/v-line.webp" class="img-fluid "></div>
                        <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/days-gurantee1.webp " class="img-fluid d-block mx-xs-auto mt15 mt-md0 ">
                     </div>
                  </div>
               </div>
               <div class="col-md-5 col-12 mt20 mt-md0">
                  <div class="key-features-bg">
                     <ul class="list-head pl0 m0 f-16 f-md-18 lh160 w500 white-clr">
                     <li>Legally Use from 800 million+ Trending Content Created by Others</li>
                     <li>Set And Forget System with Single Keyword</li>
                     <li>AI-Powered Software Puts Most Profitable Links on Your Websites</li>
                     <li>Built-In Automated Traffic Generating System</li>
                     <li>Complete Social Media Automation</li>
                     <li>100% SEO Friendly Website And Built-In Remarketing System</li>
                     <li>Automatically Translate Your Sites In 15+ Language According For More Traffic</li>
                     <li>Integration With Major Autoresponders And Social Media Apps</li>
                     <li>In-Built Content Spinner To Make Your Content Fresh</li>
                     <li>Make 5K-10K With Commercial License</li>
                     <li>A-Z Complete Video Training Is Included</li>
                     <li>Limited-Time Special Bonuses Worth $2285 If You Buy Today</li>
                     </ul>
                  </div>
                  <div class="col-12 mt20 mt-md60 d-md-none">
                     <div class="f-20 f-md-24 w500 lh140 text-center white-clr">
                        (Get Free 30 Reseller License + Commercial License + Special Bonuses)
                     </div>
                     <div class="row">
                        <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center ">
                           <a href="https://warriorplus.com/o2/buy/h5143v/y8157d/g63083" class="cta-link-btn px0 d-block">Get Instant Access To Entertainerz</a>
                        </div>
                     </div>
                     <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30 ">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/compaitable-with1.webp " class="img-fluid d-block mx-xs-center md-img-right ">
                        <div class="d-md-block d-none px-md30"><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/v-line.webp" class="img-fluid "></div>
                        <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/days-gurantee1.webp " class="img-fluid d-block mx-xs-auto mt15 mt-md0 ">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Header Section End -->
      <div class="offer-new">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="offer">
                     <div><img src="https://cdn.oppyo.com/launches/appzilo/special/error.webp"></div>
                     <div class="f-18 w400 white-clr lh140"><b>FAIR WARNING! After Every Hour The Price Goes Up,</b> So Act Now or Come Later & Pay More</div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Step Section End -->
      <div class="no-installation">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-md-36 f-28 w700 lh150 text-capitalize black-clr text-center">
                     Drive Massive Traffic & Sales To Any Offer, <br class="d-none d-md-block">
Page, Or Link In Just <span class="w800 purple-clr step-line">3 Simple Steps…</span>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md60">
                  <div class="row">
                     <div class="col-12 col-md-4">
                        <div class="steps-block">
                           <div class="step-bg">
                              <img src="https://cdn.oppyo.com/launches/entertainerz/special/step-1.webp">
                              <div class="f-22 f-md-26 w700 black-clr">
                                 Step #1
                              </div>
                           </div>
                           <div class="f-18 f-md-20 w700 black-clr lh150 mt20 mt-md30 text-center">
                              Login into our <br class="d-none d-md-block"> cloud-based software.
                           </div>
                        </div>
                     </div>
                     <div class="col-12 col-md-4 mt20 mt-md0">
                        <div class="steps-block">
                           <div class="step-bg">
                              <img src="https://cdn.oppyo.com/launches/entertainerz/special/step-2.webp">
                              <div class="f-22 f-md-26 w700 black-clr">
                                 Step #2
                              </div>
                           </div>
                           <div class="f-18 f-md-20 w700 black-clr lh150 mt20 mt-md30 text-center">
                              Just enter a keyword.
                           </div>
                        </div>
                     </div>
                     <div class="col-12 col-md-4 mt20 mt-md0">
                        <div class="steps-block">
                           <div class="step-bg">
                              <img src="https://cdn.oppyo.com/launches/entertainerz/special/step-3.webp">
                              <div class="f-22 f-md-26 w700 black-clr">
                                 Step #3
                              </div>
                           </div>
                           <div class="f-18 f-md-20 w700 black-clr lh150 mt20 mt-md30 text-center">
                              Select the trending content from YouTube & top authority
                              sites
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md80">
                  <div class="f-20 f-md-22 w600 text-center black-clr lh150">
                     That’s it. All hard work is done. Now just sit back, relax, and watch your TRAFFIC & profits grow leaps and bounds.
                  </div>
               </div>
               <div class="col-12 col-md-12 mx-auto mt40 mt-md50">
                  <div class="row">
                     <div class="col-12 col-md-4 col-md-4">
                        <!-- <img src="https://cdn.oppyo.com/launches/entertainerz/special/no1.webp" class="img-fluid d-block mx-auto"> -->
                        <div class="w800 f-18 f-md-24 purple-clr text-center lh150">
                           No Download/Installation 
                        </div>
                     </div>
                     <div class="col-12 col-md-4 mt20 mt-md0">
                        <!--  <img src="https://cdn.oppyo.com/launches/entertainerz/special/no2.webp" class="img-fluid d-block mx-auto"> -->
                        <div class="w800 f-18 f-md-24 purple-clr text-center lh150">
                           No Prior Knowledge 	
                        </div>
                     </div>
                     <div class="col-12 col-md-4 mt20 mt-md0">
                        <!-- <img src="https://cdn.oppyo.com/launches/entertainerz/special/no3.webp" class="img-fluid d-block mx-auto"> -->
                        <div class="w800 f-18 f-md-24 purple-clr text-center lh150">
                           100% Beginners Friendly
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="proof-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-45 f-28 w700 lh150 text-center black-clr">
                  We Are Getting Thousands of FREE TRAFFIC Monthly 
                  for Each Site - 100% Hands-Free
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
                  <img src="https://cdn.oppyo.com/launches/entertainerz/special/proof1.webp" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 mt20 mt-md80">
                  <div class="f-md-45 f-28 w700 lh150 text-center black-clr">
                     Which Made Us $16,029 Last Month - That’s an 
                     Average <u>$528 In Profits Each & Every Day</u>
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
                  <img src="https://cdn.oppyo.com/launches/entertainerz/special/proof2.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>
         </div>
      </div>
      <div class="testimonial-section">
         <div class="container ">
            <div class="row ">
               <div class="col-12">
                  <div class="f-md-45 f-28 lh150 w700 text-center black-clr">Checkout What Entertainerz Early <br class="d-none d-md-block"> Users Have to SAY</div>
               </div>
            </div>
            <div class="row row-cols-md-2 row-cols-1 gx4">
               <div class="col mt20 mt-md50">
                  <img src="https://cdn.oppyo.com/launches/entertainerz/special/t1.webp" class="img-fluid d-block mx-auto">
               </div>
               <div class="col mt20 mt-md50">
               <img src="https://cdn.oppyo.com/launches/entertainerz/special/t2.webp" class="img-fluid d-block mx-auto">
               </div>
               <div class="col mt20 mt-md20">
               <img src="https://cdn.oppyo.com/launches/entertainerz/special/t3.webp" class="img-fluid d-block mx-auto">
               </div>
               <div class="col mt20 mt-md20">
               <img src="https://cdn.oppyo.com/launches/entertainerz/special/t4.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>
            <!-- <div class="row row-cols-md-2 row-cols-1 gx4">
               <div class="col mt50">
                  <div class="single-testimonial">
                     <p>
                        Entertainerz is one of the Best Android &amp; iOS Mobile App Builders I have come across. Being a freelancer, it was never easy for me to build mobile applications for my clients from different businesses. And I was able to serve hardly 2-3 clients monthly, and cost more due to the various plugins I must buy.<br><br>
                        <span class="w600">But DANG! Entertainerz has changed it over, and now I am serving 20-25 clients per month and making up to $17,000 monthly,</span> with no additional cost of app development, and that is 4X more profits for me.
                     </p>
                     <div class="client-info">
                        <img src="https://cdn.oppyo.com/launches/entertainerz/special/client1.webp" class="img-fluid d-block mx-auto">
                        <div class="client-details">
                           John Warner
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col mt50">
                  <div class="single-testimonial">
                     <p>
                        Entertainerz helped me to create my first real estate Mobile Application in just 30 minutes and the best part is that I am not a tech geek.<br><br>
                        <span class="w600">Now, I’m able to get more FREE traffic &amp; targeted leads online through my App. It helped me reach my prospects at a much lower cost &amp; boosted profits by almost 3 times.</span> This is something that I really won’t forget in my life.
                     </p>
                     <div class="client-info">
                        <img src="https://cdn.oppyo.com/launches/entertainerz/special/client2.webp" class="img-fluid d-block mx-auto">
                        <div class="client-details">
                           Chuk Akspan 
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col mt50">
                  <div class="single-testimonial">
                     <p>
                        I started my freelance mobile app developer journey 4 months back, but being a newbie, I was struggling in creating a good Applications for Android and iOS.
                        <br><br>But thanks for early access to the Entertainerz team, now <span class="w600">I can make a stunning, highly professional Mobile Application quick &amp; easy and able to serve more clients who happily pay me $1000 - $1500 each.</span>
                     </p>
                     <div class="client-info">
                        <img src="https://cdn.oppyo.com/launches/entertainerz/special/client3.webp" class="img-fluid d-block mx-auto">
                        <div class="client-details">
                           Stuart Johnson
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col mt50">
                  <div class="single-testimonial">
                     <p>
                        If someone told me that one can develop as many as 20-25 Mobile Apps in a month for their clients, I’d laugh them off. Because being an App Developer for 5 years, I know how much it takes to create even a single Application.  <br><br>
                        <span class="w600">But after getting beta access to Entertainerz, I’m truly stunned. I can create a Professional Android &amp; iOS Application within minutes. Now I can make 4X more profits by serving more and more clients.</span>
                     </p>
                     <div class="client-info">
                        <img src="https://cdn.oppyo.com/launches/entertainerz/special/client4.webp" class="img-fluid d-block mx-auto">
                        <div class="client-details">
                           David Shaw
                        </div>
                     </div>
                  </div>
               </div>
            </div> -->
         </div>
      </div>
     
      <!-- CTA Section Start -->
      <div class="cta-section-white">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-12 col-md-5">
                  <img src="https://cdn.oppyo.com/launches/entertainerz/special/limited-time-offer-banner.webp" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 col-md-7 mt30 mt-md0">
                  <div class="f-18 f-md-20 w500 lh150 text-center white-clr">
                     (Free Commercial License + Low 1-Time Price + 6 Special Bonuses worth $2255)
                  </div>
                  <div class="row">
                     <div class="col-12 mt20 mt-md20 text-center">
                        <a href="https://warriorplus.com/o2/buy/h5143v/y8157d/g63083" class="cta-link-btn">Get Instant Access To Entertainerz</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                     <img src="https://cdn.oppyo.com/launches/entertainerz/special/compaitable-with1.webp" class="img-responsive mx-xs-center md-img-right">
                     <div class="d-md-block d-none px-md30"><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/v-line.webp" class="img-fluid "></div>
                     <img src="https://cdn.oppyo.com/launches/entertainerz/special/days-gurantee1.webp" class="img-responsive mx-xs-auto mt15 mt-md0">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="offer-new">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="offer">
                     <div><img src="https://cdn.oppyo.com/launches/appzilo/special/error.webp"></div>
                     <div class="f-18 w400 white-clr lh140"><b>FAIR WARNING! After Every Hour The Price Goes Up,</b> So Act Now or Come Later & Pay More</div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- CTA Section End -->
      <div class="thatsall-section">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-10 mx-auto text-center">
                  <img src="https://cdn.oppyo.com/launches/entertainerz/special/line.webp">
                  <div class="f-40 f-md-70 w800 purple-clr mt10">Did You Know?</div>
               </div>
               <div class="col-12 f-md-38 f-28 w700 lh150 text-center mt20">
                  Entertainment is No. 1 Watched <br class="d-none d-md-block">
Category on YouTube

               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-12 col-md-6 f-md-24 f-20 w600 lh150">
               And Global Entertainment News Media 
is A Huge $2.93 Trillion Market

               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                  <img src="https://cdn.oppyo.com/launches/entertainerz/special/growth.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-12 text-center">
                  <div class="thatsall-heading f-24 f-md-38 w700 white-clr lh150">                  
                    
Now, Let Me Ask You a Question! 

                  </div>
               </div>
               <div class="col-12 text-center mt20">
                  <div class="f-22 f-md-28 w700 lh150">                  
                  How many times you have read or watched headlines like?
                  </div>
               </div>
            </div>
            <div class="row mt-md50 mt20 align-items-center">
               <div class="col-12 col-md-6">
                  <div class="avengame-bg">
                     <div class="f-24 f-md-32 w800 black-clr lh150">“10 reasons why G.O.T season 8 is going to change the series forever”</div>
                  </div>
               </div>
               <div class="col-12 col-md-6 mx-auto mt20 mt-md0">
                  <div class="avengame-bg1">
                     <div class="f-24 f-md-32 w800 black-clr lh150">“We finally understand<br class="d-none d-md-block"> why Marvel started with<br class="d-none d-md-block"> Iron Man” 
                  </div>
                  </div>
               </div>
               <div class="col-12 text-center f-md-20 f-18 w500 lh150 mt20 mt-md70">               
                 
Let’s Admit, we all have clicked them, read that content, and watched that video.  
<br> 
These are Entertainment topics that almost everyone is into and wants to know more and more about that.
               </div>
            </div>
         </div>
      </div>
      <div class="doubt-section">
         <div class="container">
            <div class="row">
            <div class="col-12 text-center">
                  <div class="winner-heading f-28 f-md-45 w700 white-clr lh140">                  
                     Trending Content is the WINNER in <br> 2022 & beyond...
                  </div>
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-12 col-md-6 f-md-20 f-18 w500 lh150">
                  <span class="w700">Old & Boring Content Does Not Work Today!</span>
                  <br><br>
                  Those are not working on social media, not on YouTube, and not even worth 
                  publishing on your own website. And that’s the reason why you don’t get FREE 
                  traffic, lose sales & commission, and must think of PAID traffic methods my 
                  friend.<br><br>
                  <span class="w700">So, <u>People only want trendy articles and videos.</u> This is working like a miracle</span>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                  <img src="https://cdn.oppyo.com/launches/entertainerz/special/doubt.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>
            <div class="row mt-md30 mt20 align-items-center">
               <div class="col-12 text-center f-md-26 f-20 w600 lh150"> The idea is simple: the more trending content you have, the more traffic you get, and 
                  the more leads and commissions you generate. 
               </div>
            </div>
         </div>
      </div>
      <div class="still-there-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-28 f-md-42 w700 lh150 text-center  black-clr">
                     And Do You Know How Much People Are Making 
                     Using the POWER of Trending Content & Videos? 
                  </div>
               </div>
            </div>
            <div class="row d-flex align-items-center flex-wrap mt-md50 mt30">
               <div class="col-12 mx-auto col-md-6">
                  <img src="https://cdn.oppyo.com/launches/entertainerz/special/ryan.webp" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 col-md-6 mt30 mt-md0">
                  <div class="f-md-24 f-20 lh150 w400 text-center text-md-start">
                     Here’s  <span class="w600">“Kevin Frazier”</span> host of “Entertainment Tonight” which has 
                     over 8 Mn Followers with a whooping  <span class="w600">Net Worth of $19.49 Mn... </span>
                  </div>
               </div>
            </div>
            <div class="row d-flex align-items-center flex-wrap mt-md50 mt30">
               <div class="col-12 mx-auto col-md-6 order-md-2">
                  <img src="https://cdn.oppyo.com/launches/entertainerz/special/gary-vaynerchuk.webp" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 col-md-6 mt30 mt-md0 order-md-1">
                  <div class="f-md-24 f-20 lh150 w400 text-center text-md-start">                    
                     And,  <span class="w600">“William R Wilkerson”</span> fonder of “The Hollywood Reporter” 
                     with 6 Mn followers and a  <span class="w600">Net Worth of $8 Mn+</span>
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md70">
               <div class="col-12 f-md-32 f-28 w700 lh150 black-clr text-center">
                  Yes! They are making BIG time. 
               </div>
               <div class="col-12 text-center mt20 mt-md50">
                  <div class="black-design1">
                     <div class="f-30 f-md-50 w700 lh150 text-center white-clr skew-normal">
                        Impressive, right?
                     </div>
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md20">
               <div class="col-12  f-22 f-md-28 lh160 w500 text-center black-clr lh150">
                  So, it can be safely stated that… <br>
                  <span class="f-24 f-md-36 w600 mt25 d-block">Trending Content & Videos Is The BIGGEST, Traffic and Income Opportunity Right Now!  </span>
               </div>
            </div>
         </div>
      </div>     
      <div class="new-dcp-team-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-md-24 f-20 w600 lh150 white-clr">
                     Dear Struggling Marketer,
                  </div>
                  <div class="f-md-32 f-24 w700 lh150 orange-clr mt10">
                     From the desk of Ayush Jain and Pranshu Gupta
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50">
                  <div class="row d-flex align-items-center flex-wrap">
                     <div class="col-12 col-md-7">
                        <div class="f-md-20 f-18 w400 lh150 white-clr ">
                           I’m Ayush Jain along with my friend Pranshu Gupta.
                           <br><br>
                           We are happily living the internet lifestyle for the last 10 years with 
                           full flexibility of working from anywhere at any time.<br><br>
                           Over time, we’ve learned that to be successful online, you must 
                           get your hands on a foolproof system that works again and again.
                        </div>
                        <div class="mt20 f-md-26 w600 f-22 lh150 white-clr">
                        It’s fair to say that Trendy Videos is the BIGGEST Traffic and Income Opportunity to Encash Right Now!
                        </div>
                        <div class="mt20">
                           <div class="f-md-20 f-18 w400 lh150 white-clr ">
                              It is an untapped goldmine to tap into oceans of viral traffic that’s ready to be pounced upon to boost leads, sales & profits for your offers.
                           </div>
                        </div>
                     </div>
                     <div class="col-md-5 col-12 text-center mt20 mt-md0">
                        <img src="https://cdn.oppyo.com/launches/entertainerz/special/ayush-pranshu.webp" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="traffic-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-md-40 f-28 w700 lh130 text-capitalize text-center black-clr">
                     We are Getting 1000s Of Visitors Every Day Following This Proven System… 
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md30">
                  <img src="https://cdn.oppyo.com/launches/entertainerz/special/proof4.webp" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-11 col-md-7 mt20 mt-md50 mx-auto">
                  <video loop autoplay muted id="vid" class="w-100">
                     <source type="video/mp4" src="https://www.buzzify.biz/special/assets/images/traffic.mp4">
                     </source>
                     <source type="video/ogg" src="https://www.buzzify.biz/special/assets/images/traffic.ogg">
                     </source>
                  </video>
               </div>
               <div class="col-12 f-md-24 f-20 w400 lh150 black-clr">
                  When it’s used wisely to reach out to your globally scattered audience, <span class="w700">the power 
                  it provides to your business IS TRULY MAGICAL. </span>
               </div>
            </div>
         </div>
      </div>
      
      <div class="problem-bg-section">
         <div class="container">
            <div class="row">
               <div class="col-12 px40 text-center">
                  <div class="f-28 f-md-45 w500 lh150 text-center black-clr skew-normal">
                     That’s A REAL Deal but
                  </div>
                  <div class="f-30 f-md-60 w700 lh150 text-center red-clr skew-normal">
                     But Here’s the Problem…
                  </div>
               </div>
            </div>
            <div class="row d-flex align-items-center flex-wrap mt20 mt-md70">
               <div class="col-12 problme-shape">
                  <div class="f-24 f-md-36 w700 lh150 yellow-clr text-center">
                     Creating Engaging Content Daily Is PAINFUL & Time-Consuming
                  </div>
                  <div class="f-20 f-md-22 w400 lh150 white-clr mt20 mt-md30">
                     <ul class="noneed-listing5 pl0">
                        <li><span class="w600">You need to research, plan, and write content daily.</span> And staying up-to-date with latest niche trends needs lots of effort </li>
                        <li><span class="w600">You Need To Be On Camera.</span> This can be a major blocker in case you are shy or introvert and have a fear that people would judge and laugh at your videos. </li>
                        <li><span class="w600">You need to learn COMPLEX video & audio editing skills.</span> Most software are complex and difficult to learn, especially if you are a non-technical guy like us. </li>
                     </ul>
                  </div>
               </div>
            </div>
            <div class="row d-flex align-items-center flex-wrap mt20 mt-md70">
               <div class="col-12 col-md-7 mt-md50">
                  <div class="f-24 f-md-36 w700 lh150 black-clr">
                     Buying Expensive Equipment Can Leave Your Back Accounts Dry
                  </div>
                  <div class="f-20 f-md-21 w400 lh150 black-clr mt10">
                     To even get started with first video, you need expensive equipment, like a
                  </div>
                  <div class="f-20 f-md-22 w400 lh150 black-clr mt20 mt-md30">
                     <ul class="noneed-listing3 pl0">
                        <li class="w600">Nice camera, </li>
                        <li class="w600">Microphone, and </li>
                        <li><span class="w600">Video-audio editing software</span> That would cost you THOUSANDS of dollars. </li>
                     </ul>
                  </div>
               </div>
               <div class="col-12 col-md-5">
                  <img src="https://cdn.oppyo.com/launches/entertainerz/special/need-img.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>
            <div class="row d-flex align-items-center flex-wrap mt20 mt-md40">
               <div class="col-12 col-md-7 order-md-2">
                  <div class="f-24 f-md-36 w700 lh150 black-clr">
                     Driving TRAFFIC to your content is the BIGGEST challenge and costs hundreds of dollars
                  </div>
                  <div class="f-20 f-md-22 w400 lh150 black-clr mt20 mt-md30">
                     <ul class="noneed-listing3 pl0">
                        <li><span class="w600">You need to be consistent</span> with uploading videos every day for months until you see good visitors. </li>
                        <li><span class="w600">If you only have channels on YouTube, you’re 100% DEPENDING on them.</span> Many times, they Shut down accounts without any prior notice. That’s a scary stuff so you need FULL control on your business by
                           having self-growing video channels on Your own domains. 
                        </li>
                     </ul>
                  </div>
               </div>
               <div class="col-12 col-md-5 order-md-1">
                  <img src="https://cdn.oppyo.com/launches/entertainerz/special/sad.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>
           
            <div class="row">
               <div class="col-12 f-24 f-md-34 w600 lh150 text-center black-clr mt20 mt-md50">
                  That’s why over 80% entrepreneurs fail during their first year & only a handful of people are successful at making it big.  
               </div>
               <div class="col-12 f-20 f-md-22 w400 lh150 text-center black-clr mt20">
                  There’re tons of areas where marketers can face challenges which we had faced in our own success journey. 
               </div>
            </div>
            <div class="row mt20 mt-md50">
               <div class="col-12 text-center">
                  <div class="black-design11">
                     <div class="f-28 f-md-40 w700 lh150 text-center white-clr skew-normal">
                        BUT NOT ANYMORE!
                     </div>
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md20">
               <div class="col-12  f-20 f-md-24 lh160 w400 text-center black-clr lh150">
                  That is why we have created a software that allows you to bring massive traffic in just a couple of clicks, and you don’t have to waste any time on creating content, funnels, SEO, paid traffic, complex tools etc. once & for all. 
               </div>
            </div>
         </div>
      </div>
      <div class="proudly-section" id="product">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-36 f-24 w700 text-center white-clr lh150 text-uppercase">
                  Introducing…
               </div>
               <div class="col-12">
                  <img src="http://localhost/launches/entertainerz/jv/assets/images/h-line.webp" class="d-block mx-auto img-fluid">
               </div>
               <div class="col-12 mt-md30 mt20 text-center">
                  <svg id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 189.77 50" style="max-height:140px">
                     <defs>
                        <style>.cls-1{fill:url(#linear-gradient-2);}.cls-2{fill:#fff;}.cls-3{fill:#001730;}.cls-4{fill:url(#linear-gradient-3);}.cls-5{fill:url(#linear-gradient);}</style>
                        <linearGradient id="linear-gradient" x1="20.65" y1="26.83" x2="42.69" y2="26.83" gradientUnits="userSpaceOnUse">
                           <stop offset="0" stop-color="#bf003b"/>
                           <stop offset="1" stop-color="#e23940"/>
                        </linearGradient>
                        <linearGradient id="linear-gradient-2" x1=".12" y1="29.26" x2="25.92" y2="29.26" gradientUnits="userSpaceOnUse">
                           <stop offset="0" stop-color="#f38227"/>
                           <stop offset="1" stop-color="#fbb826"/>
                        </linearGradient>
                        <linearGradient id="linear-gradient-3" x1="3.09" y1="36.12" x2="22.33" y2="28.06" gradientUnits="userSpaceOnUse">
                           <stop offset="0" stop-color="#3369e9"/>
                           <stop offset="1" stop-color="#002b7f"/>
                        </linearGradient>
                     </defs>
                     <path class="cls-2" d="M22.29,0c-.94,0-1.69,.76-1.69,1.69,0,.75,.48,1.39,1.16,1.61l-2.95,10.01-.61,2.09,2.13,.26,.41-2.75v-.03s1.44-9.5,1.44-9.5c.03,0,.07,0,.11,0,.93,0,1.69-.76,1.69-1.7s-.76-1.69-1.69-1.69Z"/>
                     <path class="cls-2" d="M16.2,15.76l-1.74,.96-5.3-6.31c-.29,.23-.64,.36-1.03,.36-.94,0-1.7-.75-1.7-1.69s.76-1.69,1.7-1.69,1.69,.76,1.69,1.69c0,.41-.15,.79-.4,1.08l6.78,5.6Z"/>
                     <g>
                        <path class="cls-2" d="M63.68,34.98c0,.14-.04,.35-.11,.62-.08,.34-.13,.55-.15,.62-.03,.32-.09,.79-.19,1.41-.05,.16-.16,.24-.35,.24-.09,0-.26-.02-.51-.05-1.1-.16-2.82-.24-5.13-.24-.66,0-1.64,.02-2.96,.05s-2.31,.05-2.96,.05c-.3,0-.44-.13-.44-.39,0-1.01,.06-2.52,.17-4.53,.11-2.02,.17-3.53,.17-4.53s-.05-2.93-.16-5.72c-.02-.72-.07-1.79-.15-3.22v-.19c-.02-.21,.09-.31,.34-.31,.63,0,1.57,.02,2.83,.05,1.26,.04,2.2,.05,2.83,.05s1.5-.02,2.7-.05c1.2-.04,2.1-.05,2.7-.05,.42,0,.65,.13,.69,.39,.19,1.19,.28,1.92,.28,2.21,0,.21-.1,.31-.3,.31-.76,0-1.92-.04-3.47-.13-1.55-.09-2.71-.13-3.5-.13-.74,0-1.2,.1-1.39,.3-.19,.2-.29,.69-.29,1.47v2.01c0,.55,.01,.87,.04,.96,.08,.28,.31,.42,.7,.42,.11,0,.26,0,.46-.01h.46c1.76,0,3.34-.03,4.74-.09,.55-.02,1.19-.06,1.94-.13,.19-.02,.28,.06,.28,.24,0,.29-.03,.73-.1,1.32-.07,.59-.1,1.04-.1,1.33,0,.16-.13,.24-.4,.24-.11,0-.51-.03-1.2-.09-.5-.05-1.63-.08-3.39-.08-1.48,0-2.42,.02-2.82,.05-.36,.03-.57,.22-.62,.57,0-.06,.01,.28,.01,1.04v1.99c0,.93,.14,1.52,.43,1.75,.22,.19,.79,.28,1.68,.28h1.48c.63-.01,2.39-.12,5.29-.34h.2c.18-.03,.27,.08,.27,.32Z"/>
                        <path class="cls-2" d="M77.61,37.42c0,.2-.09,.3-.27,.3-.3,0-.74,0-1.33-.01-.59,0-1.03-.01-1.33-.01-.16,0-.24-.13-.24-.39,0-.44,.01-1.11,.04-2.01,.03-.9,.04-1.57,.04-2.02,0-.36,0-.9-.01-1.61,0-.71-.01-1.25-.01-1.61,0-1.02-.15-1.75-.44-2.17-.36-.5-1.02-.75-1.99-.75-.45,0-1.07,.25-1.87,.75-.84,.53-1.27,1-1.27,1.41v8.08c0,.22-.09,.34-.26,.34-.29,0-.72,0-1.29-.01-.57,0-1.01-.01-1.29-.01-.19,0-.28-.1-.28-.31,0-.71,.01-1.77,.03-3.19,.02-1.42,.03-2.49,.03-3.21,0-1.91-.2-3.68-.61-5.29-.03-.08-.04-.14-.04-.18,0-.09,.06-.15,.18-.19,.06,0,.58-.09,1.54-.25,.97-.16,1.47-.24,1.52-.24,.05,0,.09,.07,.11,.22,.04,.56,.15,1.12,.31,1.68,.47-.37,1.08-.83,1.85-1.37,.94-.57,1.85-.86,2.73-.86,1.69,0,2.84,.49,3.46,1.47,.46,.72,.69,1.88,.69,3.49,0,.28,0,.7-.02,1.28-.01,.57-.02,1.01-.02,1.29,0,.6,0,1.5,.03,2.69,.02,1.19,.03,2.09,.03,2.69Z"/>
                        <path class="cls-2" d="M87.67,35.21c0,.2-.02,.47-.05,.81-.05,.42-.07,.7-.08,.82,0,.16-.02,.26-.04,.3-.04,.06-.12,.13-.26,.19-.74,.35-1.89,.53-3.48,.53-2.1,0-3.15-.95-3.15-2.86,0-.76,.02-1.91,.05-3.44,.04-1.53,.05-2.68,.05-3.44,0-.37-.18-.57-.55-.59-.33,0-.66,0-.98-.01-.19-.04-.28-.41-.28-1.1,0-.26,.02-.53,.05-.82,.03-.22,.17-.35,.43-.39,.22,0,.44,0,.66-.01,.41-.02,.62-.2,.62-.55s-.01-.91-.03-1.64c-.02-.74-.03-1.29-.03-1.66,0-.66,.13-.98,.38-.98,.08,0,.92,.14,2.53,.42,.21,.03,.31,.13,.31,.3,0,.4-.03,.99-.09,1.78-.06,.79-.09,1.38-.09,1.78,0,.26,.12,.39,.36,.39h3.23c.15,0,.23,.05,.23,.16s-.02,.28-.05,.52c-.03,.24-.05,.42-.05,.53,0,.13,0,.34,.01,.62,0,.28,.01,.48,.01,.62,0,.17-.13,.26-.38,.26-.36,0-.9-.02-1.63-.07-.73-.05-1.27-.07-1.63-.07-.07,0-.13,.26-.16,.77-.05,.64-.07,1.53-.07,2.68v2.01c0,.77,.07,1.31,.22,1.62,.22,.47,.66,.7,1.33,.7,.28,0,.69-.06,1.23-.18s.94-.18,1.2-.18c.11,0,.16,.08,.16,.24Z"/>
                        <path class="cls-2" d="M101.01,30.46c0,.66-.29,1.11-.86,1.33-.39,.14-3.17,.41-8.34,.79,.12,.79,.55,1.46,1.31,2.02,.72,.53,1.49,.79,2.32,.79,1.42,0,2.72-.46,3.91-1.37,.14-.12,.29-.23,.43-.35,.07,0,.12,0,.14,.03,.02,.02,.19,.33,.51,.94,.31,.61,.47,.93,.47,.98,0,.05-.07,.16-.22,.31-1.32,1.4-3.01,2.1-5.08,2.1s-3.74-.62-4.98-1.87c-1.24-1.24-1.86-2.9-1.86-4.98,0-1.85,.59-3.46,1.78-4.84,1.25-1.43,2.78-2.14,4.59-2.14,1.69,0,3.1,.62,4.23,1.86,1.1,1.2,1.66,2.67,1.66,4.39Zm-3.21-.82c0-.75-.29-1.41-.88-1.98s-1.25-.86-1.99-.86c-.83,0-1.56,.33-2.2,1-.64,.66-.96,1.41-.96,2.24,0,.19,.16,.28,.48,.28,1.33,0,2.92-.13,4.77-.4,.52-.07,.78-.16,.78-.27Z"/>
                        <path class="cls-2" d="M110.6,25.04c0,.99-.07,1.89-.2,2.69-.03,.21-.12,.31-.27,.31-.19,0-.47-.02-.86-.06-.38-.04-.67-.06-.87-.06-1.13,0-1.85,.32-2.17,.96-.19,.39-.28,1.19-.28,2.41v1.48c0,.5,.02,1.27,.06,2.29,.04,1.02,.06,1.79,.06,2.29,0,.24-.1,.36-.31,.36-.28,0-.7,0-1.27-.01-.57,0-.99-.01-1.27-.01-.21,0-.31-.11-.31-.34,0-.73,.01-1.82,.03-3.29,.02-1.46,.03-2.56,.03-3.3,0-2.22-.21-4-.62-5.35-.02-.07-.03-.13-.03-.16,0-.11,.06-.18,.18-.23,.33-.04,.8-.09,1.4-.16,.97-.18,1.42-.27,1.35-.27,.14,0,.24,.26,.28,.79,.04,.53,.13,.79,.24,.79,.02,0,.04,0,.07-.03,.37-.23,.74-.47,1.1-.7,.4-.24,.82-.42,1.24-.54,.35-.11,.8-.16,1.35-.16,.7,0,1.05,.09,1.05,.28Z"/>
                        <path class="cls-2" d="M119.58,35.21c0,.2-.02,.47-.05,.81-.05,.42-.07,.7-.08,.82,0,.16-.02,.26-.04,.3-.04,.06-.12,.13-.26,.19-.74,.35-1.89,.53-3.48,.53-2.1,0-3.15-.95-3.15-2.86,0-.76,.02-1.91,.05-3.44,.04-1.53,.05-2.68,.05-3.44,0-.37-.18-.57-.55-.59-.33,0-.66,0-.98-.01-.19-.04-.28-.41-.28-1.1,0-.26,.02-.53,.05-.82,.03-.22,.17-.35,.43-.39,.22,0,.44,0,.66-.01,.41-.02,.62-.2,.62-.55s-.01-.91-.03-1.64c-.02-.74-.03-1.29-.03-1.66,0-.66,.13-.98,.38-.98,.08,0,.92,.14,2.53,.42,.21,.03,.31,.13,.31,.3,0,.4-.03,.99-.09,1.78-.06,.79-.09,1.38-.09,1.78,0,.26,.12,.39,.36,.39h3.23c.15,0,.23,.05,.23,.16s-.02,.28-.05,.52c-.03,.24-.05,.42-.05,.53,0,.13,0,.34,.01,.62,0,.28,.01,.48,.01,.62,0,.17-.13,.26-.38,.26-.36,0-.9-.02-1.63-.07-.73-.05-1.27-.07-1.63-.07-.07,0-.13,.26-.16,.77-.05,.64-.07,1.53-.07,2.68v2.01c0,.77,.07,1.31,.22,1.62,.22,.47,.66,.7,1.33,.7,.28,0,.69-.06,1.23-.18s.94-.18,1.2-.18c.11,0,.16,.08,.16,.24Z"/>
                        <path class="cls-2" d="M134.6,25.5s-.01,.12-.04,.24c-.35,2.06-.53,3.83-.53,5.32,0,.09,.11,2.11,.34,6.05v.19c.02,.26-.1,.39-.34,.39s-.62,.03-1.12,.09c-.5,.06-.87,.09-1.11,.09-.17,0-.3-.31-.39-.94-.09-.63-.18-.94-.26-.94-.05,0-.3,.18-.73,.53-.53,.43-1.02,.76-1.48,.98-.71,.36-1.42,.54-2.13,.54-1.74,0-3.21-.7-4.4-2.09-1.13-1.33-1.7-2.88-1.7-4.65,0-1.99,.57-3.64,1.71-4.94,1.19-1.36,2.77-2.03,4.71-2.03,1.37,0,2.57,.48,3.6,1.43,.15,.18,.39,.44,.71,.78,.03,.03,.05,.04,.08,.04,.05,0,.13-.28,.23-.85,.1-.57,.22-.85,.38-.85,.24,0,.7,.07,1.36,.2,.74,.16,1.1,.31,1.1,.43Zm-3.43,5.63c0-1.1-.33-2.05-.98-2.84-.69-.85-1.58-1.28-2.65-1.28s-1.98,.42-2.69,1.27c-.69,.81-1.04,1.76-1.04,2.86s.35,2.03,1.04,2.83c.72,.84,1.62,1.25,2.69,1.25s1.93-.43,2.64-1.28c.66-.8,1-1.73,1-2.8Z"/>
                        <path class="cls-2" d="M140.3,21.58c0,1.1-.62,1.66-1.85,1.66s-1.87-.55-1.87-1.66c0-.49,.19-.9,.58-1.23,.36-.31,.79-.46,1.29-.46s.94,.16,1.3,.47c.36,.31,.55,.72,.55,1.21Zm-.09,3.65c0,.69-.04,1.74-.13,3.13-.09,1.4-.13,2.44-.13,3.15,0,.66,.01,1.64,.04,2.94,.03,1.31,.04,2.28,.04,2.93,0,.19-.08,.28-.24,.28h-2.63c-.19,0-.28-.18-.28-.55,0-.62,.02-1.55,.05-2.8,.03-1.24,.05-2.18,.05-2.81,0-.7-.05-1.75-.16-3.15s-.15-2.45-.15-3.15c0-.15,.09-.23,.28-.23,.16,0,.41,.02,.75,.07,.34,.04,.59,.07,.76,.07s.42-.02,.76-.07c.34-.04,.59-.07,.76-.07,.15,0,.23,.09,.23,.26Z"/>
                        <path class="cls-2" d="M154.74,37.42c0,.2-.09,.3-.27,.3-.3,0-.74,0-1.33-.01-.59,0-1.03-.01-1.33-.01-.16,0-.24-.13-.24-.39,0-.44,.01-1.11,.04-2.01,.03-.9,.04-1.57,.04-2.02,0-.36,0-.9-.01-1.61,0-.71-.01-1.25-.01-1.61,0-1.02-.15-1.75-.44-2.17-.36-.5-1.02-.75-1.99-.75-.45,0-1.07,.25-1.87,.75-.84,.53-1.27,1-1.27,1.41v8.08c0,.22-.09,.34-.26,.34-.29,0-.72,0-1.29-.01-.57,0-1.01-.01-1.29-.01-.19,0-.28-.1-.28-.31,0-.71,.01-1.77,.03-3.19,.02-1.42,.03-2.49,.03-3.21,0-1.91-.2-3.68-.61-5.29-.03-.08-.04-.14-.04-.18,0-.09,.06-.15,.18-.19,.06,0,.58-.09,1.54-.25,.97-.16,1.47-.24,1.52-.24,.05,0,.09,.07,.11,.22,.04,.56,.15,1.12,.31,1.68,.47-.37,1.08-.83,1.85-1.37,.94-.57,1.85-.86,2.73-.86,1.69,0,2.84,.49,3.46,1.47,.46,.72,.69,1.88,.69,3.49,0,.28,0,.7-.02,1.28-.01,.57-.02,1.01-.02,1.29,0,.6,0,1.5,.03,2.69,.02,1.19,.03,2.09,.03,2.69Z"/>
                        <path class="cls-2" d="M168.7,30.46c0,.66-.29,1.11-.86,1.33-.39,.14-3.17,.41-8.34,.79,.12,.79,.55,1.46,1.31,2.02,.72,.53,1.49,.79,2.32,.79,1.42,0,2.72-.46,3.91-1.37,.14-.12,.29-.23,.43-.35,.07,0,.12,0,.14,.03,.02,.02,.19,.33,.51,.94,.31,.61,.47,.93,.47,.98,0,.05-.07,.16-.22,.31-1.32,1.4-3.01,2.1-5.08,2.1s-3.74-.62-4.98-1.87c-1.24-1.24-1.86-2.9-1.86-4.98,0-1.85,.59-3.46,1.78-4.84,1.25-1.43,2.78-2.14,4.59-2.14,1.69,0,3.1,.62,4.23,1.86,1.1,1.2,1.66,2.67,1.66,4.39Zm-3.21-.82c0-.75-.29-1.41-.88-1.98s-1.25-.86-1.99-.86c-.83,0-1.56,.33-2.2,1-.64,.66-.96,1.41-.96,2.24,0,.19,.16,.28,.48,.28,1.33,0,2.92-.13,4.77-.4,.52-.07,.78-.16,.78-.27Z"/>
                        <path class="cls-2" d="M178.29,25.04c0,.99-.07,1.89-.2,2.69-.03,.21-.12,.31-.27,.31-.19,0-.47-.02-.86-.06-.38-.04-.67-.06-.87-.06-1.13,0-1.85,.32-2.17,.96-.19,.39-.28,1.19-.28,2.41v1.48c0,.5,.02,1.27,.06,2.29s.06,1.79,.06,2.29c0,.24-.1,.36-.31,.36-.28,0-.7,0-1.27-.01-.57,0-.99-.01-1.27-.01-.21,0-.31-.11-.31-.34,0-.73,.01-1.82,.03-3.29,.02-1.46,.03-2.56,.03-3.3,0-2.22-.21-4-.62-5.35-.02-.07-.03-.13-.03-.16,0-.11,.06-.18,.18-.23,.33-.04,.8-.09,1.4-.16,.97-.18,1.42-.27,1.35-.27,.14,0,.24,.26,.28,.79,.04,.53,.13,.79,.24,.79,.02,0,.04,0,.07-.03,.37-.23,.74-.47,1.1-.7,.4-.24,.82-.42,1.24-.54,.35-.11,.8-.16,1.35-.16,.7,0,1.05,.09,1.05,.28Z"/>
                        <path class="cls-2" d="M189.77,35.13s-.07,.36-.2,.96c-.14,.64-.22,1.03-.24,1.19-.04,.31-.12,.47-.24,.47-.31,0-.74-.02-1.27-.05-.6-.04-1.02-.07-1.25-.08-.6-.02-1.46-.03-2.59-.03-.87,0-1.84,.04-2.9,.11-1.33,.09-1.9,.13-1.71,.13-.19,0-.31-.08-.36-.24-.13-.43-.19-.93-.19-1.51,0-.08,0-.2,.01-.34s.01-.26,.01-.33c0-.13,.1-.28,.29-.45,.19-.18,1.15-1.19,2.88-3.05,.87-.91,2.11-2.3,3.7-4.19h-5.94c-.14,0-.22-.09-.22-.28,0-.26,.06-.65,.17-1.17,.11-.52,.17-.9,.17-1.15,0-.11,.13-.16,.38-.16,.5,0,1.25,.02,2.25,.07s1.75,.07,2.25,.07c2.46,0,3.85,.02,4.16,.05,.22,.03,.34,.12,.34,.28,0,1.36-.05,2.12-.16,2.29-.09,.13-.87,1.01-2.33,2.65-.94,1.07-2.37,2.66-4.27,4.77,2.99-.08,5.31-.12,6.95-.12,.21,0,.31,.04,.31,.12Z"/>
                     </g>
                     <path class="cls-2" d="M16.61,49.17c.96-1.64,6.11-6.32,13.48-6.77,2.78-.17,5.12,.34,6.8,.88,0,0,.84,.29,.15,.94s-11.41-1.5-19.36,5.17c-1.6,1.37-1.2,.01-1.07-.22Z"/>
                     <path class="cls-5" d="M42.69,31.94c-.01,2.52-.67,4.93-2.77,6.32-1.62,1.06-3.89,2.45-6.95,3.79,.63-1.27,.93-2.75,.99-4.28,.01-.21,.01-.43,.01-.64,.01-9.53-3.63-17.69-6.99-21.14-.16-.16-.32-.32-.5-.47-1.51-1.33-3.74-2.37-5.74-2.61-.03,0-.06-.01-.09-.01,.03-.01,.07-.01,.1-.02,3.12-.66,6.47-1.1,9.93-1.26,2.07-.09,4.7,1.12,6.16,2.62,2.81,2.89,5.86,9.72,5.85,17.7Z"/>
                     <path class="cls-1" d="M33.97,37.13c0,.21,0,.43-.01,.64-.06,1.53-.36,3.01-.99,4.28-.27,.12-.54,.23-.82,.35-3.01,1.24-6.73,2.41-11.29,3.21v-.21c0-.47-.01-.95-.03-1.41-.02-.5-.04-.99-.07-1.48-.01-.04-.01-.07-.01-.11-.03-.46-.07-.91-.12-1.36-.02-.25-.05-.49-.08-.74-.08-.69-.17-1.36-.28-2.03-.07-.45-.15-.89-.23-1.32-.01-.08-.03-.17-.05-.25-.1-.54-.22-1.08-.34-1.6-.03-.13-.06-.26-.09-.39-.11-.44-.22-.87-.34-1.3-.11-.44-.24-.87-.37-1.29-.06-.2-.12-.4-.19-.6-.14-.44-.28-.88-.44-1.3-.18-.51-.37-1.01-.56-1.5-.25-.62-.51-1.22-.77-1.8-.6-1.29-1.23-2.47-1.88-3.53-.12-.2-.25-.4-.38-.59-.12-.2-.25-.39-.38-.57-.63-.92-1.28-1.72-1.92-2.38-.16-.16-.33-.33-.51-.48-.02-.03-.05-.05-.08-.08-.06-.05-.12-.1-.18-.15-.45-.38-.95-.74-1.48-1.07-.14-.09-.28-.17-.42-.25-.48-.28-.98-.53-1.49-.75,1.46-.71,3.09-1.4,4.87-2.04,.53-.18,1.07-.37,1.62-.54,1.32-.43,2.71-.82,4.15-1.18,.6-.14,1.22-.28,1.84-.41,.03,0,.06,.01,.09,.01,2,.24,4.23,1.28,5.74,2.61,.18,.15,.34,.31,.5,.47,3.36,3.45,7,11.61,6.99,21.14Z"/>
                     <path class="cls-4" d="M20.86,45.61c-3.31,.58-7.05,.95-11.28,1.01-.83,.01-1.62-.36-2.33-.79-2.42-1.46-5.57-7.74-7.18-18.65-.38-2.58,.81-5,2.76-6.72,1.27-1.12,3.09-2.28,5.33-3.39,1.36,.59,2.64,1.4,3.65,2.3,.18,.16,.34,.32,.5,.48,3.71,3.82,7.71,12.34,8.42,22.55,.08,1.05,.11,2.12,.11,3.21Z"/>
                     <path class="cls-2" d="M6.78,22.31c3.46-3.05,12.1-6.51,21.9-6.95,1.63-.07,3.7,.88,4.84,2.06,2.21,2.28,4.61,7.65,4.6,13.93,0,1.98-.64,3.72-2.18,4.97-3.42,2.78-10.62,7.14-23.86,6.57-.65-.03-1.28-.28-1.84-.62-1.9-1.15-4.38-6.09-5.65-14.67-.3-2.03,.64-3.93,2.17-5.29Z"/>
                     <path class="cls-3" d="M26.11,26.61l-5.61-1.56c-1.31-.36-2.85,.26-3.44,1.38-.23,.43-.29,.9-.17,1.33l1.51,5.69c.3,1.12,1.67,1.73,3.08,1.36,.54-.14,1.04-.42,1.4-.8l4.1-4.13c.96-.96,.86-2.3-.21-2.99-.2-.13-.42-.22-.66-.29h0Z"/>
                  </svg>
               </div>
               <div class="f-md-32 f-22 w700 white-clr lh150 col-12 mt-md30 mt20 text-center">
               Super SIMPLE Software That Uses A Secret Method To Make Us $528/Day Over And Over Again By Creating Self-Updating Entertainment News Site Packaged with Trendy Content & Videos 
               </div>
               <div class="col-12 mt-md30 mt20">
                  <img src="https://cdn.oppyo.com/launches/entertainerz/special/proudly.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>
         </div>
      </div>
      
      <div class="new-steps-bg">
         <div class="container">
            <div class="row">
               <div class="col-lg-12">
               <div class="f-md-36 f-28 w700 lh150 text-capitalize black-clr text-center">
                     Drive Massive Traffic & Sales To Any Offer, <br class="d-none d-md-block">
Page, Or Link In Just <span class="w800 purple-clr step-line">3 Simple Steps…</span>
                  </div>
               </div>
               <div class="col-12 mt-md70 mt20">
                  <div class="row align-items-center">
                     <div class="col-12 col-md-6 shutacc-li vline-yellow align-self-center step-pl15px relative">
                        <img src="https://cdn.oppyo.com/launches/entertainerz/special/step-left-border.webp" class="step-left-boredr-image d-lg-block d-none">
                        <span class="f-md-26 f-20 w600 step-btn-style">Step #1</span>
                        <div class="mt-md40 mt20 black-clr f-md-24 f-22 w500 lh150">Login into our cloud-based software Entertainerz, and choose your 
                           business/website name, subdomain, add logo and other details
                        </div>
                     </div>
                     <div class="col-12 col-md-6 align-self-center mt-md0 mt20">
                        <div class="step-image-back">
                           <img  src="https://cdn.oppyo.com/launches/entertainerz/special/sm-steps1.webp" class="img-fluid d-block mx-auto"/>
                        </div>
                     </div>
                  </div>
                  <div class="row align-items-center mt-md50 mt20">
                     <div class="col-12 col-md-6 order-md-2 shutacc-li vline-blue step-plr5px relative">
                        <img src="https://cdn.oppyo.com/launches/entertainerz/special/step-right-border.webp" class="step-right-boredr-image d-lg-block d-none">
                        <span class="f-md-26 f-20 w600 step-btn-style">Step #2</span>
                        <div class="mt-md40 mt20 black-clr f-md-24 f-22 w500 lh150">Just enter your keywords, Entertainerz will find super engaging 
                           videos and articles to post on your website and<br class="d-none d-md-block"> share on social 
                           media platforms legally.
                        </div>
                     </div>
                     <div class="col-12 col-md-6 order-md-1 align-self-center mt-md0 mt20">
                        <div class="step-image-back1">
                           <img  src="https://cdn.oppyo.com/launches/entertainerz/special/sm-steps2.webp" class="img-fluid d-block mx-auto"/>
                        </div>
                     </div>
                  </div>
                  <div class="row align-items-center mt-md50 mt20">
                     <div class="col-12 col-md-6 shutacc-li vline-yellow align-self-center step-pl15px relative">
                        <img src="https://cdn.oppyo.com/launches/entertainerz/special/step-left-border.webp" class="step-left-boredr-image d-lg-block d-none">
                        <span class="f-md-26 f-20 w600 step-btn-style">Step #3</span>
                        <div class="mt-md40 mt20 black-clr f-md-24 f-22 w500 lh150">Select the trending videos & articles from top authority sites you 
                           want to add to your website or set an automation rule.
                        </div>
                     </div>
                     <div class="col-12 col-md-6 align-self-center mt-md0 mt20">
                        <div class="step-image-back3">
                           <img  src="https://cdn.oppyo.com/launches/entertainerz/special/sm-steps3.webp" class="img-fluid d-block mx-auto"/>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Demo Section Start -->
      <div class="demo-section" id="demo">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-45 f-28 w700 lh150 text-center">
                  Watch Entertainerz in Action
               </div>
               <div class="col-12 col-md-9 mx-auto mt15 mt-md50 relative">
                  <!-- <img src="https://cdn.oppyo.com/launches/entertainerz/special/video-thumb.webp" class="img-fluid d-block mx-auto"> -->
                  <div class="col-12 responsive-video">
                     <iframe src="https://entertainerz.dotcompal.com/video/mydrive/185148%22" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                        box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe> 
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--!Demo Section End--->
      <!-- CTA Section Start -->
      <div class="cta-section-white">
         <div class="container">
            <div class="row align-items-center">
               <!-- CTA Btn Section Start -->
               <div class="col-12 col-md-5">
                  <img src="https://cdn.oppyo.com/launches/entertainerz/special/limited-time-offer-banner.webp" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 col-md-7 mt30 mt-md0">
                  <div class="f-18 f-md-20 w500 lh150 text-center white-clr">
                     (Free Commercial License + Low 1-Time Price + 6 Special Bonuses worth $2255)
                  </div>
                  <div class="row">
                     <div class="col-12 mt20 mt-md20 text-center">
                        <a href="https://warriorplus.com/o2/buy/h5143v/y8157d/g63083" class="cta-link-btn">Get Instant Access To Entertainerz</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                     <img src="https://cdn.oppyo.com/launches/entertainerz/special/compaitable-with1.webp" class="img-responsive mx-xs-center md-img-right">
                     <div class="d-md-block d-none px-md30"><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/v-line.webp" class="img-fluid "></div>
                     <img src="https://cdn.oppyo.com/launches/entertainerz/special/days-gurantee1.webp" class="img-responsive mx-xs-auto mt15 mt-md0">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="offer-new">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="offer">
                     <div><img src="https://cdn.oppyo.com/launches/appzilo/special/error.webp"></div>
                     <div class="f-18 w400 white-clr lh140"><b>FAIR WARNING! After Every Hour The Price Goes Up,</b> So Act Now or Come Later & Pay More</div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- CTA Section End -->
     
      <!-- Features 1 Section Start -->
      <div class="features-one" id="features">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="main-heading-bg f-28 f-md-45 w700 lh150 white-clr">
                     Here Are the GROUND-BREAKING Features
                  </div>
                  <div class="f-28 f-md-43 w700 lh150 white-clr mt-md15 mt15">
                     That Makes Entertainerz A CUT ABOVE The Rest
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50">
                  <div class="row d-flex align-items-center flex-wrap">
                     <div class="col-12 f-18 mt20 mt-md0 ">
                        <div class="f-22 f-md-30 w700 lh150 text-capitalize white-clr text-center">
                           Create Unlimited Self-Updating Sites with DFY Hot Trending Entertainment Content  
                        </div>
                        <div class="f-18 f-md-20 w400 lh150 white-clr mt20">
                           Entertainerz gives you the power to create beautifully designed & self-updating sites 
                           with Hot & Trending Content from Entertainment Industry that’s fully ready to monetize 
                           the traffic with your products or affiliate offers.  
                        </div>
                     </div>
                     <div class="col-10 mx-auto mt-md40 mt20">
                        <img src="https://cdn.oppyo.com/launches/entertainerz/special/feature1.webp" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Features 1 Section End -->
      <!-- Features 1 Section Start -->
      <div class="features-two">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row d-flex align-items-center flex-wrap">
                     <div class="col-12 col-md-6 order-md-2 f-18 mt20 mt-md0 ">
                        <div class="f-22 f-md-32 w700 lh150 text-capitalize black-clr text-md-start text-center">
                           Find Hot & Trendy Content in Seconds.
                        </div>
                        <div class="f-18 f-md-20 w400 lh150 black-clr mt15 text-md-start text-center">
                           Trendy content is the KING, and Entertainerz helps you to fill your sites with trendy 
                           content that grabs eyeballs. Just enter the related keywords and BANG!!!
                        </div>
                     </div>
                     <div class="col-12 col-md-6 order-md-1 mt20 mt-md0">
                        <img src="https://cdn.oppyo.com/launches/entertainerz/special/feature2.webp" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Features 1 Section End -->
      <!-- Features 1 Section Start -->
      <div class="features-three">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row d-flex align-items-center flex-wrap">
                     <div class="col-12 col-md-6 f-18 mt20 mt-md0 ">
                        <div class="f-22 f-md-32 w700 lh150 text-capitalize black-clr text-md-start text-center">
                           Legally Use Other People’s Trendy Videos about Movie Reviews, 
                           Industry Gossip etc, and Profit from It
                        </div>
                        <div class="f-18 f-md-20 w400 lh150 black-clr mt15 text-md-start text-center">
                           Videos are the best way to share more in a few seconds. But creating them is 
                           not everyone’s cup of tea. With Entertainerz, that’ll be an issue of the past.<br><br>
                           Just enter a few keywords and you have all the HOT & Popular videos from 
                           the video-sharing Giant, YouTube. The secret formula is 100% Legal and 
                           you can start PROFITING from Today!
                        </div>
                     </div>
                     <div class="col-12 col-md-6 mt20 mt-md0">
                        <img src="https://cdn.oppyo.com/launches/entertainerz/special/feature3.webp" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Features 1 Section End -->
      <!-- Features 2 Section Start -->
      <div class="features-two">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row d-flex align-items-center flex-wrap">
                     <div class="col-12 col-md-6 f-18 mt20 mt-md0 order-md-2">
                        <div class="f-22 f-md-32 w700 lh150 text-capitalize black-clr text-md-start text-center">
                           Make Your Content Viral to Drive Tons of Targeted Traffic
                        </div>
                        <div class="f-18 f-md-20 w400 lh150 black-clr mt15">
                           With Entertainerz, you can easily share your articles & videos on TOP social platforms 
                           without investing time & money in creating and optimizing posts. As you already have 
                           the Power of Viral Content added with engagement boosters.
                        </div>
                     </div>
                     <div class="col-12 col-md-6 mt20 mt-md0 order-md-1 fe-box-img">
                        <img src="https://cdn.oppyo.com/launches/entertainerz/special/feature4.webp" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Features 2 Section End -->
      <!-- Features 3 Section Start -->
      <div class="features-three">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row d-flex align-items-center flex-wrap">
                     <div class="col-12 col-md-6 f-18 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w700 lh150 text-capitalize black-clr text-md-start text-center">
                           Set And Forget System with Just One Keyword
                        </div>
                        <div class="f-18 f-md-20 w400 lh150 black-clr mt15">
                           Gone are the days of doing stuff manually and wasting time and energy.<br><br>
                           Simply by settings rules once, you have the POWER to get on TOP of the 
                           content each time flowing right to you as soon as they are published. 
                           Entertainerz has built-in settings for automating content curation from 
                           Top & trusted web publishers.
                        </div>
                     </div>
                     <div class="col-12 col-md-6 mt20 mt-md0 order-md-2 fe-box-img">
                        <img src="https://cdn.oppyo.com/launches/entertainerz/special/feature5.webp" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Features 3 Section End -->
      <!-- Features 4 Section Start -->
      <div class="features-four">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row d-flex align-items-center flex-wrap">
                     <div class="col-12 col-md-6 order-md-5 f-18 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh150 text-capitalize black-clr text-md-start text-center">
                           Get Unstoppable Leads In Your Favourite Autoresponder or The Dashboard 
                        </div>
                        <div class="f-18 f-md-20 w400 lh150 black-clr mt15 text-md-start text-center">
                           Entertainerz has been custom created to enable you get fast and easy 
                           integration with TOP autoresponders. Import your leads directly to your 
                           favorite autoresponder and you’re all set to rock.
                        </div>
                     </div>
                     <div class="col-12 col-md-6 order-md-0 mt20 mt-md0 fe-box-img">
                        <img src="https://cdn.oppyo.com/launches/entertainerz/special/feature6.webp" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Features 4 Section End -->
      <div class="features-three">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row d-flex align-items-center flex-wrap">
                     <div class="col-12 col-md-6 f-18 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w700 lh150 text-capitalize black-clr text-md-start text-center">
                           DFY Colour Themes to Create Your Beautiful & Trendy 
                           Content-Rich Viral Sites
                        </div>
                        <div class="f-18 f-md-20 w400 lh150 black-clr mt15">
                           These attractive, eye-catchy, and premium themes have been created keeping 
                           every marketer’s needs in mind. They will prove to be a great add-on to your purchase.
                        </div>
                     </div>
                     <div class="col-12 col-md-6 mt20 mt-md0 order-md-2 fe-box-img">
                        <img src="https://cdn.oppyo.com/launches/entertainerz/special/feature7.webp" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="features-four">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row d-flex align-items-center flex-wrap">
                     <div class="col-12 col-md-6 order-md-5 f-18 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh150 text-capitalize black-clr text-md-start text-center">
                           Proven Converting & Ready-To-Use Lead & Promo 
                           Templates with Easy & FAST Editor
                        </div>
                        <div class="f-18 f-md-20 w400 lh150 black-clr mt15">
                           Just tweak them within minutes using our editor to create a 
                           high-converting promo to maximize profits.
                        </div>
                     </div>
                     <div class="col-12 col-md-6 order-md-0 mt20 mt-md0 fe-box-img">
                        <img src="https://cdn.oppyo.com/launches/entertainerz/special/feature8.webp" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="features-three">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row d-flex align-items-center flex-wrap">
                     <div class="col-12 col-md-6 f-18 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh150 text-capitalize black-clr text-md-start text-center">
                           Market Your Content in 15+ Languages in One Click
                        </div>
                        <div class="f-18 f-md-20 w400 lh150 black-clr mt15">
                           Entertainerz comes pre-loaded with 15+ languages that 
                           get every visitor glued to your sites.
                        </div>
                     </div>
                     <div class="col-12 col-md-6 mt20 mt-md0 fe-box-img">
                        <img src="https://cdn.oppyo.com/launches/entertainerz/special/feature9.webp" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="features-four">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row d-flex align-items-center flex-wrap">
                     <div class="col-12 col-md-6 f-18 mt20 mt-md0 order-md-2">
                        <div class="f-22 f-md-32 w700 lh150 text-capitalize black-clr text-md-start text-center">
                           Create SEO Friendly Pages Automatically
                        </div>
                        <div class="f-18 f-md-20 w400 lh150 black-clr mt15">
                           Yes, you have built-in settings for making all your sites completely 
                           SEO friendly and all that comes extremely fast and easy. 
                        </div>
                     </div>
                     <div class="col-12 col-md-6 mt20 mt-md0 order-md-1 fe-box-img">
                        <img src="https://cdn.oppyo.com/launches/entertainerz/special/feature11.webp" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="features-three">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row d-flex align-items-center flex-wrap">
                     <div class="col-12 col-md-6  f-18 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh150 text-capitalize black-clr text-md-start text-center">
                           No Domain or Hosting Required
                        </div>
                        <div class="f-18 f-md-20 w400 lh150 black-clr mt15">
                           With Entertainerz, you save hundreds of dollars as yearly charges 
                           for hosting services. Now, that we have everything in place for you, 
                           you just sit and relax and save your money.
                        </div>
                     </div>
                     <div class="col-12 col-md-6  mt20 mt-md0 fe-box-img">
                        <img src="https://cdn.oppyo.com/launches/entertainerz/special/feature12.webp" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="features-four">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row d-flex align-items-center flex-wrap">
                     <div class="col-12 col-md-6 f-18 mt20 mt-md0 order-md-2">
                        <div class="f-22 f-md-32 w700 lh150 text-capitalize black-clr text-md-start text-center">
                           Precise Analytics Included 
                        </div>
                        <div class="f-18 f-md-20 w400 lh150 black-clr mt15">
                           Get your viral site analytics with smart metrics on articles and videos 
                           count leads captured, and views along with quick-to-understand graphs 
                           for articles and videos. So you can see how successful your campaigns 
                           are and scale them accordingly.
                        </div>
                     </div>
                     <div class="col-12 col-md-6 mt20 mt-md0 order-md-1 fe-box-img">
                        <img src="https://cdn.oppyo.com/launches/entertainerz/special/feature13.webp" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="features-three">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row d-flex align-items-center flex-wrap">
                     <div class="col-12 col-md-6 f-18 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh150 text-capitalize black-clr text-md-start text-center">
                           Complete A-Z Training
                        </div>
                        <div class="f-18 f-md-20 w400 lh150 black-clr mt15">
                           When you are diving into such a large pool of features, we are not 
                           leaving you alone there. You will see complete step-by-step training 
                           in PDF and Video format to take you by the hand and make everything 
                           fast and easy.
                        </div>
                     </div>
                     <div class="col-12 col-md-6 mt20 mt-md0 fe-box-img">
                        <img src="https://cdn.oppyo.com/launches/entertainerz/special/feature14.webp" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- CTA Section Start -->
      <div class="cta-section-white">
         <div class="container">
            <div class="row align-items-center">
               <!-- CTA Btn Section Start -->
               <div class="col-12 col-md-5">
                  <img src="https://cdn.oppyo.com/launches/entertainerz/special/limited-time-offer-banner.webp" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 col-md-7 mt30 mt-md0">
                  <div class="f-18 f-md-20 w500 lh150 text-center white-clr">
                     (Free Commercial License + Low 1-Time Price + 6 Special Bonuses worth $2255)
                  </div>
                  <div class="row">
                     <div class="col-12 mt20 mt-md20 text-center">
                        <a href="https://warriorplus.com/o2/buy/h5143v/y8157d/g63083" class="cta-link-btn">Get Instant Access To Entertainerz</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                     <img src="https://cdn.oppyo.com/launches/entertainerz/special/compaitable-with1.webp" class="img-responsive mx-xs-center md-img-right">
                     <div class="d-md-block d-none px-md30"><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/v-line.webp" class="img-fluid "></div>
                     <img src="https://cdn.oppyo.com/launches/entertainerz/special/days-gurantee1.webp" class="img-responsive mx-xs-auto mt15 mt-md0">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="offer-new">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="offer">
                     <div><img src="https://cdn.oppyo.com/launches/appzilo/special/error.webp"></div>
                     <div class="f-18 w400 white-clr lh140"><b>FAIR WARNING! After Every Hour The Price Goes Up,</b> So Act Now or Come Later & Pay More</div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- CTA Section End -->
      <section class="table-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-45 w700 lh150 black-clr">
                     We Literally Have no Competition
                  </div>
               </div>
            </div>
            <div class="row g-0 mt70 mt-md100">
               <div class="col-md-6 col-6">
                  <div class="fist-row">
                     <ul class="f-md-16 f-14 w500">
                        <li class="f-md-20 f-16">Features</li>
                        <li><span class="w600">Set and Forget System </span> with Single Keyword </li>
                        <li>Self-Updating & Beautiful Trendy Sites </li>
                        <li>Built-In Automated Traffic Generating System  </li>
                        <li>Auto-Curate Trendy Videos </li>
                        <li>Auto-Curate Trendy Articles </li>
                        <li>UNLIMITED Campaigns </li>
                        <li>Puts Most Profitable Affiliate Links using AI </li>
                        <li>Done-For-You Promo & Lead Gen Templates </li>
                        <li>Excellent, Fast & Easy Template Editor </li>
                        <li>Campaign Scheduling (pop ups effect)</li>
                        <li>Built-in Content Spinner </li>
                        <li>100% SEO Friendly Website and Built-In Remarketing System </li>
                        <li>Accelerated Mobile Pages </li>
                        <li>Built-in Slider Management </li>
                        <li>Amazon Ads Integration </li>
                        <li>Monetization with Banner Ads </li>
                        <li>Precise Site Analytics </li>
                        <li>SSl Certification & GDPR Compliance </li>
                        <li>One-Time Payment </li>
                        <li>FREE Commercial License Included </li>
                     </ul>
                  </div>
               </div>
               <div class="col-md-3 col-3">
                  <div class="second-row">
                     <ul class="f-md-16 f-14">
                        <li>
                           <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 190.8 50.3" style="enable-background:new 0 0 190.8 50.3; max-height:100px" xml:space="preserve">
                              <style type="text/css">
                                 .st0{fill:#FFFFFF;}
                                 .st1{fill:#E23940;}
                                 .st2{fill:#FBB826;}
                                 .st3{fill:#3369E9;}
                                 .st4{enable-background:new    ;}
                                 .st5{fill:#001730;}
                              </style>
                              <g>
                                 <g>
                                    <path class="st0" d="M16.7,49.4c1-1.6,6.1-6.3,13.6-6.8c2.8-0.2,5.2,0.3,6.8,0.9c0,0,0.8,0.3,0.2,1c-0.7,0.6-11.5-1.5-19.5,5.2
                                       C16.1,51,16.5,49.7,16.7,49.4z"></path>
                                 </g>
                                 <g>
                                    <polygon class="st0" points="16.3,15.8 14.5,16.8 7.9,8.9 7.9,8.9 		"></polygon>
                                 </g>
                                 <g>
                                    <polygon class="st0" points="18.3,15.5 20.4,15.8 22.8,0.3 22.8,0.3 		"></polygon>
                                 </g>
                                 <path class="st1" d="M42.5,27.2c-0.1-0.7-0.2-1.4-0.4-2.1c-0.1-0.5-0.3-1-0.4-1.5c-0.1-0.4-0.2-0.9-0.4-1.3c0,0,0,0,0,0
                                    c-0.1-0.4-0.2-0.7-0.4-1.1c-0.1-0.3-0.3-0.7-0.4-1c-0.1-0.3-0.3-0.6-0.4-0.9c-0.1-0.3-0.3-0.5-0.4-0.8c-0.1-0.3-0.3-0.5-0.4-0.7
                                    c-0.1-0.2-0.3-0.5-0.4-0.7c-0.1-0.2-0.3-0.4-0.4-0.6c-0.1-0.2-0.3-0.4-0.4-0.5c0,0,0,0,0,0c-0.1-0.2-0.3-0.3-0.4-0.5
                                    c-0.1-0.2-0.3-0.3-0.4-0.5c-0.1-0.2-0.3-0.3-0.4-0.4c0,0,0,0,0-0.1c-0.1-0.1-0.2-0.2-0.3-0.3c-0.1-0.1-0.3-0.2-0.4-0.3
                                    c-0.1-0.1-0.3-0.2-0.4-0.3c-0.1-0.1-0.3-0.2-0.4-0.3c-0.1-0.1-0.3-0.2-0.4-0.2c0,0,0,0,0,0c-0.1-0.1-0.3-0.1-0.4-0.2
                                    s-0.3-0.1-0.4-0.2c-0.1-0.1-0.3-0.1-0.4-0.2c-0.1-0.1-0.3-0.1-0.4-0.1c-0.1,0-0.3-0.1-0.4-0.1c-0.1,0-0.3-0.1-0.4-0.1
                                    c-0.1,0-0.3-0.1-0.4-0.1c-0.1,0-0.3,0-0.4-0.1c0,0,0,0,0,0c-0.1,0-0.3,0-0.4-0.1c-0.1,0-0.3,0-0.4,0c-0.1,0-0.3,0-0.4,0
                                    c0,0,0,0,0,0c-0.1,0-0.2,0-0.4,0c-0.1,0-0.3,0-0.4,0c-0.1,0-0.3,0-0.4,0s-0.3,0-0.4,0c-0.1,0-0.3,0-0.4,0c0,0,0,0,0,0
                                    c-0.1,0-0.3,0-0.4,0c-0.1,0-0.3,0-0.4,0c-0.1,0-0.3,0-0.4,0c-0.1,0-0.3,0-0.4,0c-0.1,0-0.3,0-0.4,0c-0.1,0-0.3,0-0.4,0
                                    s-0.3,0-0.4,0c-0.1,0-0.3,0-0.4,0c0,0,0,0,0,0c-0.1,0-0.3,0-0.4,0.1c-0.1,0-0.3,0-0.4,0.1c-0.1,0-0.3,0-0.4,0.1s-0.3,0-0.4,0.1
                                    c-0.1,0-0.3,0-0.4,0.1c-0.1,0-0.3,0-0.4,0.1c-0.1,0-0.3,0-0.4,0.1c-0.1,0-0.3,0-0.4,0.1c0,0,0,0,0,0c-0.1,0-0.3,0-0.4,0.1
                                    c-0.1,0-0.3,0-0.4,0.1c-0.1,0-0.3,0-0.4,0.1c-0.1,0-0.3,0-0.4,0.1c-0.1,0-0.3,0-0.4,0.1c0.1,0,0.3,0,0.4,0.1c0.1,0,0.3,0,0.4,0.1
                                    c0.1,0,0.3,0.1,0.4,0.1c0.1,0,0.3,0.1,0.4,0.1c0.1,0,0.3,0.1,0.4,0.1c0,0,0,0,0,0c0.1,0,0.3,0.1,0.4,0.1c0.1,0,0.3,0.1,0.4,0.1
                                    c0.1,0.1,0.3,0.1,0.4,0.2c0.1,0.1,0.3,0.1,0.4,0.2c0.1,0.1,0.3,0.1,0.4,0.2s0.3,0.1,0.4,0.2c0.1,0.1,0.3,0.2,0.4,0.3
                                    c0.1,0.1,0.3,0.2,0.4,0.2c0,0,0,0,0,0c0.1,0.1,0.3,0.2,0.4,0.3c0.1,0.1,0.3,0.2,0.4,0.3c0.1,0.1,0.3,0.2,0.4,0.4
                                    c0,0,0.1,0.1,0.1,0.1c0.1,0.1,0.2,0.2,0.2,0.3c0.1,0.1,0.3,0.3,0.4,0.4c0.1,0.2,0.3,0.3,0.4,0.5c0.1,0.2,0.3,0.3,0.4,0.5
                                    c0.1,0.2,0.3,0.4,0.4,0.5c0,0,0,0,0,0c0.1,0.2,0.3,0.4,0.4,0.6c0.1,0.2,0.3,0.4,0.4,0.6c0.1,0.2,0.3,0.5,0.4,0.7
                                    c0.1,0.2,0.3,0.5,0.4,0.8c0.1,0.3,0.3,0.5,0.4,0.8c0.1,0.3,0.3,0.6,0.4,0.9c0.1,0.3,0.3,0.6,0.4,1c0.1,0.3,0.3,0.7,0.4,1
                                    c0,0,0,0,0,0c0.1,0.4,0.3,0.7,0.4,1.1c0.1,0.4,0.3,0.9,0.4,1.3c0.1,0.5,0.3,1,0.4,1.6c0.1,0.6,0.3,1.3,0.4,2
                                    c0.2,0.9,0.3,1.9,0.4,2.9c0.1,1.2,0.2,2.4,0.2,3.6c0,0.7-0.1,1.4-0.2,2.1c-0.1,0.6-0.2,1.2-0.4,1.7c-0.1,0.3-0.2,0.6-0.4,1
                                    c0,0,0,0.1-0.1,0.1c0,0,0,0,0.1,0c0.1-0.1,0.3-0.1,0.4-0.2c0.1-0.1,0.3-0.1,0.4-0.2c0.1-0.1,0.3-0.1,0.4-0.2
                                    c0.1-0.1,0.3-0.1,0.4-0.2c0.1-0.1,0.3-0.1,0.4-0.2c0,0,0,0,0,0c0.1-0.1,0.3-0.1,0.4-0.2c0.1-0.1,0.3-0.1,0.4-0.2
                                    c0.1-0.1,0.3-0.1,0.4-0.2c0.1-0.1,0.3-0.1,0.4-0.2s0.3-0.1,0.4-0.2c0.1-0.1,0.3-0.1,0.4-0.2c0.1-0.1,0.3-0.2,0.4-0.2
                                    c0.1-0.1,0.3-0.2,0.4-0.2c0,0,0,0,0,0c0.1-0.1,0.3-0.2,0.4-0.2c0.1-0.1,0.3-0.2,0.4-0.2c0.1-0.1,0.3-0.2,0.4-0.2
                                    c0.1-0.1,0.3-0.2,0.4-0.3c0.1-0.1,0.2-0.1,0.3-0.2c0,0,0,0,0.1-0.1c0.1-0.1,0.3-0.2,0.4-0.3c0.1-0.1,0.3-0.2,0.4-0.3
                                    c0.1-0.1,0.3-0.3,0.4-0.4c0,0,0,0,0,0c0.1-0.2,0.3-0.4,0.4-0.5c0.1-0.2,0.3-0.5,0.4-0.7c0.2-0.3,0.3-0.7,0.4-1
                                    c0.3-0.9,0.4-1.9,0.4-3C42.9,30.4,42.8,28.7,42.5,27.2z"></path>
                                 <path class="st2" d="M27.1,16.1c-0.3-0.3-0.7-0.6-1-0.9c-0.2-0.1-0.3-0.2-0.5-0.3c-0.2-0.1-0.3-0.2-0.5-0.3
                                    c-0.2-0.1-0.3-0.2-0.5-0.3c-0.2-0.1-0.3-0.2-0.5-0.3c-0.2-0.1-0.3-0.1-0.5-0.2c-0.2-0.1-0.3-0.1-0.5-0.2c-0.2-0.1-0.3-0.1-0.5-0.2
                                    c-0.2-0.1-0.3-0.1-0.5-0.1c-0.2,0-0.3-0.1-0.5-0.1c-0.2,0-0.3-0.1-0.5-0.1c-0.2,0-0.3-0.1-0.5-0.1c0,0,0,0,0,0
                                    c-0.1,0-0.3,0.1-0.4,0.1c-0.2,0-0.3,0.1-0.5,0.1c-0.2,0-0.3,0.1-0.5,0.1c-0.2,0-0.3,0.1-0.5,0.1c-0.2,0-0.3,0.1-0.5,0.1
                                    s-0.3,0.1-0.5,0.1c0,0,0,0,0,0c-0.2,0-0.3,0.1-0.5,0.1c-0.2,0-0.3,0.1-0.5,0.1c-0.2,0-0.3,0.1-0.5,0.1s-0.3,0.1-0.5,0.1
                                    c-0.2,0-0.3,0.1-0.5,0.1c-0.2,0-0.3,0.1-0.5,0.2s-0.3,0.1-0.5,0.2c-0.2,0.1-0.3,0.1-0.5,0.2c-0.2,0.1-0.3,0.1-0.5,0.2
                                    c-0.2,0.1-0.3,0.1-0.5,0.2s-0.3,0.1-0.5,0.2c-0.2,0.1-0.3,0.1-0.5,0.2c-0.2,0.1-0.3,0.1-0.5,0.2c-0.2,0.1-0.3,0.1-0.5,0.2
                                    C11,15.9,10.8,16,10.7,16c-0.2,0.1-0.3,0.1-0.5,0.2c-0.2,0.1-0.3,0.1-0.5,0.2c-0.2,0.1-0.3,0.1-0.5,0.2c-0.2,0.1-0.3,0.1-0.5,0.2
                                    C8.6,17,8.4,17,8.3,17.1c0,0-0.1,0-0.1,0c0,0,0.1,0,0.1,0c0.2,0.1,0.3,0.2,0.5,0.2c0.2,0.1,0.3,0.2,0.5,0.2
                                    c0.2,0.1,0.3,0.2,0.5,0.3c0.2,0.1,0.3,0.2,0.5,0.3c0.2,0.1,0.3,0.2,0.5,0.3c0.2,0.1,0.3,0.2,0.5,0.4c0.2,0.1,0.3,0.2,0.5,0.4
                                    c0.2,0.1,0.3,0.3,0.5,0.4c0.1,0.1,0.2,0.1,0.2,0.2c0.1,0.1,0.2,0.2,0.2,0.3c0.2,0.2,0.3,0.3,0.5,0.5c0.2,0.2,0.3,0.4,0.5,0.6
                                    c0.2,0.2,0.3,0.4,0.5,0.6c0.2,0.2,0.3,0.5,0.5,0.7c0.2,0.2,0.3,0.5,0.5,0.7c0.2,0.3,0.3,0.5,0.5,0.8c0.2,0.3,0.3,0.6,0.5,0.9
                                    c0.2,0.3,0.3,0.6,0.5,0.9c0.2,0.3,0.3,0.6,0.5,1c0.2,0.4,0.3,0.7,0.5,1.1c0.2,0.4,0.3,0.8,0.5,1.2c0,0,0,0,0,0
                                    c0.2,0.4,0.3,0.9,0.5,1.3c0.2,0.5,0.3,0.9,0.5,1.4c0.2,0.5,0.3,1.1,0.5,1.6c0.2,0.6,0.3,1.3,0.5,1.9c0.2,0.8,0.3,1.6,0.5,2.5
                                    c0.2,1.2,0.4,2.5,0.5,3.7c0.1,1.3,0.2,2.6,0.2,4c0.1,0,0.2,0,0.3-0.1c0.2,0,0.3-0.1,0.5-0.1s0.3-0.1,0.5-0.1c0.2,0,0.3-0.1,0.5-0.1
                                    c0.2,0,0.3-0.1,0.5-0.1c0.2,0,0.3-0.1,0.5-0.1s0.3-0.1,0.5-0.1c0.2,0,0.3-0.1,0.5-0.1c0.2,0,0.3-0.1,0.5-0.1c0.2,0,0.3-0.1,0.5-0.1
                                    c0.2,0,0.3-0.1,0.5-0.1c2.7-0.7,5.1-1.6,7.1-2.5c0.7-1.5,1-3.2,1-4.9C34.2,27.7,30.5,19.5,27.1,16.1z"></path>
                                 <path class="st3" d="M21,45.6c0-0.2,0-0.4,0-0.5c0-0.2,0-0.4,0-0.5c0-0.2,0-0.4,0-0.6c0-0.2,0-0.4,0-0.6c0-0.2,0-0.4,0-0.6
                                    c0-0.1,0-0.2,0-0.2c0-0.1,0-0.3,0-0.4c0-0.2,0-0.4-0.1-0.7c0-0.2,0-0.4-0.1-0.7c0-0.2-0.1-0.5-0.1-0.7c0-0.2-0.1-0.5-0.1-0.7
                                    c0-0.3-0.1-0.5-0.1-0.8c0-0.3-0.1-0.6-0.1-0.9c-0.1-0.3-0.1-0.6-0.2-0.9c-0.1-0.4-0.1-0.7-0.2-1c-0.1-0.4-0.2-0.8-0.3-1.2
                                    c-0.1-0.4-0.2-0.9-0.3-1.3c-0.2-0.6-0.4-1.2-0.5-1.8C18,29.2,17,27.1,16,25.2c-0.3-0.5-0.6-1-0.8-1.5c-0.2-0.3-0.4-0.7-0.6-1
                                    c-0.2-0.3-0.4-0.5-0.5-0.8c-0.2-0.2-0.3-0.4-0.5-0.7c-0.1-0.2-0.3-0.4-0.4-0.6c-0.1-0.2-0.3-0.3-0.4-0.5c-0.1-0.1-0.2-0.2-0.3-0.3
                                    c0,0,0,0-0.1-0.1c-0.1-0.1-0.2-0.2-0.4-0.4c0,0,0,0-0.1-0.1c-0.1-0.1-0.2-0.1-0.3-0.2c-0.1-0.1-0.2-0.2-0.3-0.3
                                    c-0.1-0.1-0.2-0.2-0.3-0.3c-0.1-0.1-0.2-0.1-0.3-0.2c-0.1-0.1-0.2-0.1-0.3-0.2c-0.1-0.1-0.2-0.1-0.3-0.2C10,18.1,9.9,18,9.8,18
                                    c-0.1-0.1-0.2-0.1-0.3-0.2c-0.1-0.1-0.2-0.1-0.3-0.2c-0.1,0-0.2-0.1-0.3-0.1c-0.1,0-0.2-0.1-0.3-0.1c-0.1,0-0.2-0.1-0.3-0.1
                                    c-0.1,0-0.1-0.1-0.2-0.1c0,0,0,0,0,0c-0.1,0-0.1,0.1-0.2,0.1c-0.1,0-0.1,0.1-0.2,0.1c-0.1,0-0.1,0.1-0.2,0.1
                                    c-0.1,0-0.1,0.1-0.2,0.1c-0.1,0-0.1,0.1-0.2,0.1c-0.1,0-0.1,0.1-0.2,0.1c-0.1,0-0.1,0.1-0.2,0.1c-0.1,0-0.1,0.1-0.2,0.1
                                    c-0.1,0-0.1,0.1-0.2,0.1c-0.1,0-0.1,0.1-0.2,0.1c-0.1,0-0.1,0.1-0.2,0.1c-0.1,0-0.1,0.1-0.2,0.1c-0.1,0-0.1,0.1-0.2,0.1
                                    c-0.1,0-0.1,0.1-0.2,0.1c-0.1,0-0.1,0.1-0.2,0.1c-0.1,0-0.1,0.1-0.2,0.1c-0.1,0-0.1,0.1-0.2,0.1c-0.1,0-0.1,0.1-0.2,0.1
                                    C5,19,4.9,19,4.9,19c-0.1,0-0.1,0.1-0.2,0.1c-0.1,0-0.1,0.1-0.2,0.1c-0.1,0-0.1,0.1-0.2,0.1c-0.1,0-0.1,0.1-0.2,0.1
                                    c-0.1,0-0.1,0.1-0.2,0.1c-0.1,0-0.1,0.1-0.2,0.1c-0.1,0-0.1,0.1-0.2,0.1c-0.1,0-0.1,0.1-0.2,0.1c-0.1,0-0.1,0.1-0.2,0.1
                                    c-0.1,0-0.1,0.1-0.2,0.1c-0.1,0-0.1,0.1-0.2,0.1c-0.1,0-0.1,0.1-0.2,0.1c0,0-0.1,0-0.1,0.1c0,0-0.1,0.1-0.1,0.1
                                    c-0.1,0-0.1,0.1-0.1,0.1c-0.1,0-0.1,0.1-0.2,0.1c0,0-0.1,0.1-0.1,0.1c0,0.1-0.1,0.1-0.2,0.1c-0.1,0.1-0.1,0.1-0.1,0.2
                                    c0,0.1-0.1,0.1-0.1,0.2c0,0.1-0.1,0.1-0.1,0.2c0,0.1-0.1,0.1-0.1,0.2c0,0.1-0.1,0.1-0.1,0.2s-0.1,0.1-0.1,0.2
                                    c0,0.1-0.1,0.1-0.1,0.2c0,0.1-0.1,0.1-0.1,0.2c0,0.1-0.1,0.1-0.1,0.2c0,0.1-0.1,0.2-0.1,0.2c0,0.1-0.1,0.2-0.1,0.2
                                    c0,0.1-0.1,0.2-0.1,0.2c0,0.1-0.1,0.2-0.1,0.3c0,0.1-0.1,0.2-0.1,0.3c0,0.1-0.1,0.2-0.1,0.3c0,0.1-0.1,0.2-0.1,0.3
                                    c0,0.1-0.1,0.2-0.1,0.3c0,0.1-0.1,0.2-0.1,0.3c0,0.1,0,0.3,0,0.4c0,0.1,0,0.3,0,0.4c0,0.2,0,0.4,0,0.6c0,0.1,0,0.3,0,0.4
                                    c0,0.1,0,0.2,0.1,0.3c0,0.3,0.1,0.5,0.1,0.8c0,0.3,0.1,0.6,0.1,0.8c0.1,0.3,0.1,0.6,0.2,0.9C2.2,39.2,5,44.6,7.2,46
                                    c0,0,0.1,0.1,0.1,0.1c0.1,0,0.1,0.1,0.2,0.1c0.1,0.1,0.2,0.1,0.3,0.2c0.1,0,0.2,0.1,0.3,0.1c0.1,0,0.2,0.1,0.3,0.1
                                    c0.1,0,0.2,0.1,0.3,0.1c0.1,0,0.2,0,0.2,0.1c0.1,0,0.2,0.1,0.2,0.1c0.1,0,0.1,0,0.2,0c0.1,0,0.2,0,0.2,0c0.1,0,0.1,0,0.2,0
                                    c0,0,0,0,0.1,0c0.1,0,0.1,0,0.2,0c0.1,0,0.1,0,0.2,0c0.1,0,0.1,0,0.2,0c0.1,0,0.1,0,0.2,0c0.1,0,0.1,0,0.2,0c0.1,0,0.1,0,0.2,0
                                    c0.1,0,0.1,0,0.2,0c0.1,0,0.1,0,0.2,0c0.1,0,0.1,0,0.2,0c0.1,0,0.1,0,0.2,0c0.1,0,0.1,0,0.2,0c0.1,0,0.1,0,0.2,0c0.1,0,0.1,0,0.2,0
                                    c0.1,0,0.1,0,0.2,0c0.1,0,0.1,0,0.2,0c0.1,0,0.1,0,0.2,0c0.1,0,0.1,0,0.2,0c0.1,0,0.1,0,0.2,0c0.1,0,0.1,0,0.2,0c0.1,0,0.1,0,0.2,0
                                    c0.1,0,0.1,0,0.2,0c0.1,0,0.1,0,0.2,0c0.1,0,0.1,0,0.2,0c0.1,0,0.1,0,0.2,0c0.1,0,0.1,0,0.2,0c0.1,0,0.1,0,0.2,0c0.1,0,0.1,0,0.2,0
                                    c0.1,0,0.1,0,0.2,0c0.1,0,0.1,0,0.2,0c0.1,0,0.1,0,0.2,0c0.1,0,0.1,0,0.2,0c0.1,0,0.1,0,0.2,0c0.1,0,0.1,0,0.2,0c0.1,0,0.1,0,0.2,0
                                    c0.1,0,0.1,0,0.2,0c0.1,0,0.1,0,0.2,0c0.1,0,0.1,0,0.2,0c0.1,0,0.1,0,0.2,0c0.1,0,0.1,0,0.2,0c0.1,0,0.1,0,0.2,0c0.1,0,0.1,0,0.2,0
                                    c0.1,0,0.1,0,0.2,0c0.1,0,0.1,0,0.2,0c0.1,0,0.1,0,0.2,0c0.1,0,0.1,0,0.2,0c0.1,0,0.1,0,0.2,0c0.1,0,0.1,0,0.2,0c0.1,0,0.1,0,0.2,0
                                    c0.1,0,0.1,0,0.2,0c0.1,0,0.1,0,0.2,0c0.1,0,0.1,0,0.2,0c0.1,0,0.1,0,0.2,0c0.1,0,0.1,0,0.2,0c0.1,0,0.1,0,0.2,0c0,0,0.1,0,0.1,0
                                    C21,45.8,21,45.7,21,45.6z"></path>
                                 <g>
                                    <path class="st0" d="M6.8,22.4c3.5-3.1,12.2-6.6,22-7c1.6-0.1,3.7,0.9,4.9,2.1c2.2,2.3,4.6,7.7,4.6,14c0,2-0.6,3.7-2.2,5
                                       c-3.4,2.8-10.7,7.2-24,6.6c-0.7,0-1.3-0.3-1.8-0.6c-1.9-1.2-4.4-6.1-5.7-14.8C4.3,25.7,5.3,23.8,6.8,22.4z"></path>
                                 </g>
                                 <g>
                                    <circle class="st0" cx="22.4" cy="1.7" r="1.7"></circle>
                                 </g>
                                 <g>
                                    <circle class="st0" cx="8.2" cy="9.1" r="1.7"></circle>
                                 </g>
                                 <g>
                                    <g class="st4">
                                       <path class="st0" d="M64,35.2c0,0.1,0,0.3-0.1,0.6c-0.1,0.3-0.1,0.5-0.2,0.6c0,0.3-0.1,0.8-0.2,1.4c0,0.2-0.2,0.2-0.3,0.2
                                          c-0.1,0-0.3,0-0.5,0c-1.1-0.2-2.8-0.2-5.2-0.2c-0.7,0-1.7,0-3,0c-1.3,0-2.3,0-3,0c-0.3,0-0.5-0.1-0.5-0.4c0-1,0.1-2.5,0.2-4.6
                                          c0.1-2,0.2-3.5,0.2-4.6c0-1,0-3-0.2-5.8c0-0.7-0.1-1.8-0.2-3.2l0-0.2c0-0.2,0.1-0.3,0.3-0.3c0.6,0,1.6,0,2.8,0
                                          c1.3,0.1,2.2,0.1,2.8,0.1c0.6,0,1.5,0,2.7,0c1.2,0,2.1,0,2.7,0c0.4,0,0.7,0.1,0.7,0.4c0.2,1.2,0.3,1.9,0.3,2.2
                                          c0,0.2-0.1,0.3-0.3,0.3c-0.8,0-1.9,0-3.5-0.1s-2.7-0.1-3.5-0.1c-0.7,0-1.2,0.1-1.4,0.3c-0.2,0.2-0.3,0.7-0.3,1.5v2
                                          c0,0.5,0,0.9,0,1c0.1,0.3,0.3,0.4,0.7,0.4c0.1,0,0.3,0,0.5,0c0.2,0,0.4,0,0.5,0c1.8,0,3.4,0,4.8-0.1c0.5,0,1.2-0.1,2-0.1
                                          c0.2,0,0.3,0.1,0.3,0.2c0,0.3,0,0.7-0.1,1.3s-0.1,1-0.1,1.3c0,0.2-0.1,0.2-0.4,0.2c-0.1,0-0.5,0-1.2-0.1c-0.5,0-1.6-0.1-3.4-0.1
                                          c-1.5,0-2.4,0-2.8,0c-0.4,0-0.6,0.2-0.6,0.6c0-0.1,0,0.3,0,1v2c0,0.9,0.1,1.5,0.4,1.8c0.2,0.2,0.8,0.3,1.7,0.3l1.5,0
                                          c0.6,0,2.4-0.1,5.3-0.3l0.2,0C63.9,34.8,64,34.9,64,35.2z"></path>
                                       <path class="st0" d="M78,37.6c0,0.2-0.1,0.3-0.3,0.3c-0.3,0-0.7,0-1.3,0c-0.6,0-1,0-1.3,0c-0.2,0-0.2-0.1-0.2-0.4
                                          c0-0.4,0-1.1,0-2s0-1.6,0-2c0-0.4,0-0.9,0-1.6s0-1.3,0-1.6c0-1-0.2-1.8-0.4-2.2c-0.4-0.5-1-0.8-2-0.8c-0.4,0-1.1,0.2-1.9,0.8
                                          c-0.8,0.5-1.3,1-1.3,1.4v8.1c0,0.2-0.1,0.3-0.3,0.3c-0.3,0-0.7,0-1.3,0s-1,0-1.3,0c-0.2,0-0.3-0.1-0.3-0.3c0-0.7,0-1.8,0-3.2
                                          s0-2.5,0-3.2c0-1.9-0.2-3.7-0.6-5.3c0-0.1,0-0.1,0-0.2c0-0.1,0.1-0.1,0.2-0.2c0.1,0,0.6-0.1,1.6-0.2s1.5-0.2,1.5-0.2
                                          c0.1,0,0.1,0.1,0.1,0.2c0,0.6,0.2,1.1,0.3,1.7c0.5-0.4,1.1-0.8,1.8-1.4c0.9-0.6,1.9-0.9,2.8-0.9c1.7,0,2.9,0.5,3.5,1.5
                                          c0.5,0.7,0.7,1.9,0.7,3.5c0,0.3,0,0.7,0,1.3c0,0.6,0,1,0,1.3c0,0.6,0,1.5,0,2.7S78,37,78,37.6z"></path>
                                       <path class="st0" d="M88.1,35.4c0,0.2,0,0.5-0.1,0.8c0,0.4-0.1,0.7-0.1,0.8c0,0.2,0,0.3,0,0.3c0,0.1-0.1,0.1-0.3,0.2
                                          c-0.7,0.3-1.9,0.5-3.5,0.5c-2.1,0-3.2-1-3.2-2.9c0-0.8,0-1.9,0.1-3.5c0-1.5,0.1-2.7,0.1-3.5c0-0.4-0.2-0.6-0.6-0.6
                                          c-0.3,0-0.7,0-1,0c-0.2,0-0.3-0.4-0.3-1.1c0-0.3,0-0.5,0.1-0.8c0-0.2,0.2-0.4,0.4-0.4c0.2,0,0.4,0,0.7,0c0.4,0,0.6-0.2,0.6-0.6
                                          c0-0.4,0-0.9,0-1.6s0-1.3,0-1.7c0-0.7,0.1-1,0.4-1c0.1,0,0.9,0.1,2.6,0.4c0.2,0,0.3,0.1,0.3,0.3c0,0.4,0,1-0.1,1.8
                                          c-0.1,0.8-0.1,1.4-0.1,1.8c0,0.3,0.1,0.4,0.4,0.4h3.2c0.2,0,0.2,0,0.2,0.2s0,0.3-0.1,0.5s-0.1,0.4-0.1,0.5c0,0.1,0,0.3,0,0.6
                                          s0,0.5,0,0.6c0,0.2-0.1,0.3-0.4,0.3c-0.4,0-0.9,0-1.6-0.1c-0.7,0-1.3-0.1-1.6-0.1c-0.1,0-0.1,0.3-0.2,0.8c0,0.6-0.1,1.5-0.1,2.7
                                          v2c0,0.8,0.1,1.3,0.2,1.6c0.2,0.5,0.7,0.7,1.3,0.7c0.3,0,0.7-0.1,1.2-0.2s0.9-0.2,1.2-0.2C88.1,35.2,88.1,35.2,88.1,35.4z"></path>
                                       <path class="st0" d="M101.5,30.6c0,0.7-0.3,1.1-0.9,1.3c-0.4,0.1-3.2,0.4-8.4,0.8c0.1,0.8,0.6,1.5,1.3,2c0.7,0.5,1.5,0.8,2.3,0.8
                                          c1.4,0,2.7-0.5,3.9-1.4c0.1-0.1,0.3-0.2,0.4-0.3c0.1,0,0.1,0,0.1,0s0.2,0.3,0.5,0.9s0.5,0.9,0.5,1c0,0-0.1,0.2-0.2,0.3
                                          c-1.3,1.4-3,2.1-5.1,2.1c-2.1,0-3.8-0.6-5-1.9c-1.2-1.2-1.9-2.9-1.9-5c0-1.9,0.6-3.5,1.8-4.9c1.2-1.4,2.8-2.1,4.6-2.1
                                          c1.7,0,3.1,0.6,4.2,1.9C101,27.4,101.5,28.9,101.5,30.6z M98.3,29.8c0-0.8-0.3-1.4-0.9-2c-0.6-0.6-1.3-0.9-2-0.9
                                          c-0.8,0-1.6,0.3-2.2,1c-0.7,0.7-1,1.4-1,2.2c0,0.2,0.2,0.3,0.5,0.3c1.3,0,2.9-0.1,4.8-0.4C98,30,98.3,29.9,98.3,29.8z"></path>
                                       <path class="st0" d="M111.2,25.2c0,1-0.1,1.9-0.2,2.7c0,0.2-0.1,0.3-0.3,0.3c-0.2,0-0.5,0-0.9-0.1c-0.4,0-0.7-0.1-0.9-0.1
                                          c-1.1,0-1.9,0.3-2.2,1c-0.2,0.4-0.3,1.2-0.3,2.4v1.5c0,0.5,0,1.3,0.1,2.3s0.1,1.8,0.1,2.3c0,0.2-0.1,0.4-0.3,0.4
                                          c-0.3,0-0.7,0-1.3,0s-1,0-1.3,0c-0.2,0-0.3-0.1-0.3-0.3c0-0.7,0-1.8,0-3.3c0-1.5,0-2.6,0-3.3c0-2.2-0.2-4-0.6-5.4
                                          c0-0.1,0-0.1,0-0.2c0-0.1,0.1-0.2,0.2-0.2c0.3,0,0.8-0.1,1.4-0.2c1-0.2,1.4-0.3,1.3-0.3c0.1,0,0.2,0.3,0.3,0.8
                                          c0,0.5,0.1,0.8,0.2,0.8c0,0,0,0,0.1,0c0.4-0.2,0.7-0.5,1.1-0.7c0.4-0.2,0.8-0.4,1.2-0.5c0.3-0.1,0.8-0.2,1.3-0.2
                                          C110.8,24.9,111.2,25,111.2,25.2z"></path>
                                       <path class="st0" d="M120.2,35.4c0,0.2,0,0.5-0.1,0.8c0,0.4-0.1,0.7-0.1,0.8c0,0.2,0,0.3,0,0.3c0,0.1-0.1,0.1-0.3,0.2
                                          c-0.7,0.3-1.9,0.5-3.5,0.5c-2.1,0-3.2-1-3.2-2.9c0-0.8,0-1.9,0.1-3.5c0-1.5,0.1-2.7,0.1-3.5c0-0.4-0.2-0.6-0.6-0.6
                                          c-0.3,0-0.7,0-1,0c-0.2,0-0.3-0.4-0.3-1.1c0-0.3,0-0.5,0.1-0.8c0-0.2,0.2-0.4,0.4-0.4c0.2,0,0.4,0,0.7,0c0.4,0,0.6-0.2,0.6-0.6
                                          c0-0.4,0-0.9,0-1.6s0-1.3,0-1.7c0-0.7,0.1-1,0.4-1c0.1,0,0.9,0.1,2.5,0.4c0.2,0,0.3,0.1,0.3,0.3c0,0.4,0,1-0.1,1.8
                                          c-0.1,0.8-0.1,1.4-0.1,1.8c0,0.3,0.1,0.4,0.4,0.4h3.2c0.2,0,0.2,0,0.2,0.2s0,0.3-0.1,0.5c0,0.2-0.1,0.4-0.1,0.5
                                          c0,0.1,0,0.3,0,0.6s0,0.5,0,0.6c0,0.2-0.1,0.3-0.4,0.3c-0.4,0-0.9,0-1.6-0.1c-0.7,0-1.3-0.1-1.6-0.1c-0.1,0-0.1,0.3-0.2,0.8
                                          c0,0.6-0.1,1.5-0.1,2.7v2c0,0.8,0.1,1.3,0.2,1.6c0.2,0.5,0.7,0.7,1.3,0.7c0.3,0,0.7-0.1,1.2-0.2s0.9-0.2,1.2-0.2
                                          C120.1,35.2,120.2,35.2,120.2,35.4z"></path>
                                       <path class="st0" d="M135.3,25.6c0,0,0,0.1,0,0.2c-0.4,2.1-0.5,3.9-0.5,5.4c0,0.1,0.1,2.1,0.3,6.1l0,0.2c0,0.3-0.1,0.4-0.4,0.4
                                          s-0.6,0-1.1,0.1s-0.9,0.1-1.1,0.1c-0.2,0-0.3-0.3-0.4-1s-0.2-1-0.3-1c-0.1,0-0.3,0.2-0.7,0.5c-0.5,0.4-1,0.8-1.5,1
                                          c-0.7,0.4-1.4,0.5-2.1,0.5c-1.8,0-3.2-0.7-4.4-2.1c-1.1-1.3-1.7-2.9-1.7-4.7c0-2,0.6-3.7,1.7-5c1.2-1.4,2.8-2,4.7-2
                                          c1.4,0,2.6,0.5,3.6,1.4c0.1,0.2,0.4,0.4,0.7,0.8c0,0,0.1,0,0.1,0c0.1,0,0.1-0.3,0.2-0.9c0.1-0.6,0.2-0.9,0.4-0.9
                                          c0.2,0,0.7,0.1,1.4,0.2C134.9,25.4,135.3,25.5,135.3,25.6z M131.8,31.3c0-1.1-0.3-2.1-1-2.9c-0.7-0.9-1.6-1.3-2.7-1.3
                                          c-1.1,0-2,0.4-2.7,1.3c-0.7,0.8-1,1.8-1,2.9c0,1.1,0.3,2,1,2.8c0.7,0.8,1.6,1.3,2.7,1.3c1.1,0,1.9-0.4,2.6-1.3
                                          C131.5,33.3,131.8,32.4,131.8,31.3z"></path>
                                       <path class="st0" d="M141,21.7c0,1.1-0.6,1.7-1.9,1.7c-1.2,0-1.9-0.5-1.9-1.7c0-0.5,0.2-0.9,0.6-1.2c0.4-0.3,0.8-0.5,1.3-0.5
                                          c0.5,0,0.9,0.2,1.3,0.5C140.8,20.8,141,21.2,141,21.7z M140.9,25.4c0,0.7,0,1.8-0.1,3.1c-0.1,1.4-0.1,2.5-0.1,3.2
                                          c0,0.7,0,1.6,0,3s0,2.3,0,2.9c0,0.2-0.1,0.3-0.2,0.3h-2.6c-0.2,0-0.3-0.2-0.3-0.5c0-0.6,0-1.6,0.1-2.8s0.1-2.2,0.1-2.8
                                          c0-0.7-0.1-1.8-0.2-3.2c-0.1-1.4-0.2-2.5-0.2-3.2c0-0.1,0.1-0.2,0.3-0.2c0.2,0,0.4,0,0.8,0.1s0.6,0.1,0.8,0.1
                                          c0.2,0,0.4,0,0.8-0.1c0.3,0,0.6-0.1,0.8-0.1C140.9,25.1,140.9,25.2,140.9,25.4z"></path>
                                       <path class="st0" d="M155.5,37.6c0,0.2-0.1,0.3-0.3,0.3c-0.3,0-0.7,0-1.3,0s-1,0-1.3,0c-0.2,0-0.2-0.1-0.2-0.4c0-0.4,0-1.1,0-2
                                          s0-1.6,0-2c0-0.4,0-0.9,0-1.6s0-1.3,0-1.6c0-1-0.1-1.8-0.4-2.2c-0.4-0.5-1-0.8-2-0.8c-0.4,0-1.1,0.2-1.9,0.8
                                          c-0.9,0.5-1.3,1-1.3,1.4v8.1c0,0.2-0.1,0.3-0.3,0.3c-0.3,0-0.7,0-1.3,0s-1,0-1.3,0c-0.2,0-0.3-0.1-0.3-0.3c0-0.7,0-1.8,0-3.2
                                          s0-2.5,0-3.2c0-1.9-0.2-3.7-0.6-5.3c0-0.1,0-0.1,0-0.2c0-0.1,0.1-0.1,0.2-0.2c0.1,0,0.6-0.1,1.6-0.2s1.5-0.2,1.5-0.2
                                          c0.1,0,0.1,0.1,0.1,0.2c0,0.6,0.1,1.1,0.3,1.7c0.5-0.4,1.1-0.8,1.9-1.4c0.9-0.6,1.9-0.9,2.8-0.9c1.7,0,2.9,0.5,3.5,1.5
                                          c0.5,0.7,0.7,1.9,0.7,3.5c0,0.3,0,0.7,0,1.3c0,0.6,0,1,0,1.3c0,0.6,0,1.5,0,2.7C155.5,36.1,155.5,37,155.5,37.6z"></path>
                                       <path class="st0" d="M169.6,30.6c0,0.7-0.3,1.1-0.9,1.3c-0.4,0.1-3.2,0.4-8.4,0.8c0.1,0.8,0.6,1.5,1.3,2c0.7,0.5,1.5,0.8,2.3,0.8
                                          c1.4,0,2.7-0.5,3.9-1.4c0.1-0.1,0.3-0.2,0.4-0.3c0.1,0,0.1,0,0.1,0s0.2,0.3,0.5,0.9s0.5,0.9,0.5,1c0,0-0.1,0.2-0.2,0.3
                                          c-1.3,1.4-3,2.1-5.1,2.1c-2.1,0-3.8-0.6-5-1.9c-1.2-1.2-1.9-2.9-1.9-5c0-1.9,0.6-3.5,1.8-4.9c1.2-1.4,2.8-2.1,4.6-2.1
                                          c1.7,0,3.1,0.6,4.2,1.9C169,27.4,169.6,28.9,169.6,30.6z M166.4,29.8c0-0.8-0.3-1.4-0.9-2c-0.6-0.6-1.3-0.9-2-0.9
                                          c-0.8,0-1.6,0.3-2.2,1c-0.6,0.7-1,1.4-1,2.2c0,0.2,0.2,0.3,0.5,0.3c1.3,0,2.9-0.1,4.8-0.4C166.1,30,166.4,29.9,166.4,29.8z"></path>
                                       <path class="st0" d="M179.2,25.2c0,1-0.1,1.9-0.2,2.7c0,0.2-0.1,0.3-0.3,0.3c-0.2,0-0.5,0-0.9-0.1c-0.4,0-0.7-0.1-0.9-0.1
                                          c-1.1,0-1.9,0.3-2.2,1c-0.2,0.4-0.3,1.2-0.3,2.4v1.5c0,0.5,0,1.3,0.1,2.3s0.1,1.8,0.1,2.3c0,0.2-0.1,0.4-0.3,0.4
                                          c-0.3,0-0.7,0-1.3,0s-1,0-1.3,0c-0.2,0-0.3-0.1-0.3-0.3c0-0.7,0-1.8,0-3.3c0-1.5,0-2.6,0-3.3c0-2.2-0.2-4-0.6-5.4
                                          c0-0.1,0-0.1,0-0.2c0-0.1,0.1-0.2,0.2-0.2c0.3,0,0.8-0.1,1.4-0.2c1-0.2,1.4-0.3,1.4-0.3c0.1,0,0.2,0.3,0.3,0.8
                                          c0,0.5,0.1,0.8,0.2,0.8c0,0,0,0,0.1,0c0.4-0.2,0.7-0.5,1.1-0.7c0.4-0.2,0.8-0.4,1.2-0.5c0.4-0.1,0.8-0.2,1.4-0.2
                                          C178.9,24.9,179.2,25,179.2,25.2z"></path>
                                       <path class="st0" d="M190.8,35.3c0,0-0.1,0.4-0.2,1c-0.1,0.6-0.2,1-0.2,1.2c0,0.3-0.1,0.5-0.2,0.5c-0.3,0-0.7,0-1.3,0
                                          c-0.6,0-1-0.1-1.3-0.1c-0.6,0-1.5,0-2.6,0c-0.9,0-1.9,0-2.9,0.1c-1.3,0.1-1.9,0.1-1.7,0.1c-0.2,0-0.3-0.1-0.4-0.2
                                          c-0.1-0.4-0.2-0.9-0.2-1.5c0-0.1,0-0.2,0-0.3c0-0.2,0-0.3,0-0.3c0-0.1,0.1-0.3,0.3-0.5c0.2-0.2,1.2-1.2,2.9-3.1
                                          c0.9-0.9,2.1-2.3,3.7-4.2h-6c-0.1,0-0.2-0.1-0.2-0.3c0-0.3,0.1-0.6,0.2-1.2s0.2-0.9,0.2-1.2c0-0.1,0.1-0.2,0.4-0.2
                                          c0.5,0,1.3,0,2.3,0.1c1,0,1.8,0.1,2.3,0.1c2.5,0,3.9,0,4.2,0c0.2,0,0.3,0.1,0.3,0.3c0,1.4-0.1,2.1-0.2,2.3
                                          c-0.1,0.1-0.9,1-2.3,2.7c-0.9,1.1-2.4,2.7-4.3,4.8c3-0.1,5.3-0.1,7-0.1C190.6,35.2,190.8,35.2,190.8,35.3z"></path>
                                    </g>
                                 </g>
                                 <g>
                                    <path class="st5" d="M26.2,26.8l-5.6-1.6c-1.3-0.4-2.9,0.3-3.5,1.4c-0.2,0.4-0.3,0.9-0.2,1.3l1.5,5.7c0.3,1.1,1.7,1.7,3.1,1.4
                                       c0.5-0.1,1-0.4,1.4-0.8l4.1-4.1c1-1,0.9-2.3-0.2-3C26.7,26.9,26.5,26.8,26.2,26.8L26.2,26.8z"></path>
                                 </g>
                              </g>
                           </svg>
                        </li>
                        <li><img src="https://cdn.oppyo.com/launches/entertainerz/special/ptwhitetick.webp" class="img-fluid mx-auto d-block"></li>
                        <li><img src="https://cdn.oppyo.com/launches/entertainerz/special/ptwhitetick.webp" class="img-fluid mx-auto d-block"></li>
                        <li><img src="https://cdn.oppyo.com/launches/entertainerz/special/ptwhitetick.webp" class="img-fluid mx-auto d-block"></li>
                        <li><img src="https://cdn.oppyo.com/launches/entertainerz/special/ptwhitetick.webp" class="img-fluid mx-auto d-block"></li>
                        <li><img src="https://cdn.oppyo.com/launches/entertainerz/special/ptwhitetick.webp" class="img-fluid mx-auto d-block"></li>
                        <li><img src="https://cdn.oppyo.com/launches/entertainerz/special/ptwhitetick.webp" class="img-fluid mx-auto d-block"></li>
                        <li><img src="https://cdn.oppyo.com/launches/entertainerz/special/ptwhitetick.webp" class="img-fluid mx-auto d-block"></li>
                        <li><img src="https://cdn.oppyo.com/launches/entertainerz/special/ptwhitetick.webp" class="img-fluid mx-auto d-block"></li>
                        <li><img src="https://cdn.oppyo.com/launches/entertainerz/special/ptwhitetick.webp" class="img-fluid mx-auto d-block"></li>
                        <li><img src="https://cdn.oppyo.com/launches/entertainerz/special/ptwhitetick.webp" class="img-fluid mx-auto d-block"></li>
                        <li><img src="https://cdn.oppyo.com/launches/entertainerz/special/ptwhitetick.webp" class="img-fluid mx-auto d-block"></li>
                        <li><img src="https://cdn.oppyo.com/launches/entertainerz/special/ptwhitetick.webp" class="img-fluid mx-auto d-block"></li>
                        <li><img src="https://cdn.oppyo.com/launches/entertainerz/special/ptwhitetick.webp" class="img-fluid mx-auto d-block"></li>
                        <li><img src="https://cdn.oppyo.com/launches/entertainerz/special/ptwhitetick.webp" class="img-fluid mx-auto d-block"></li>
                        <li><img src="https://cdn.oppyo.com/launches/entertainerz/special/ptwhitetick.webp" class="img-fluid mx-auto d-block"></li>
                        <li><img src="https://cdn.oppyo.com/launches/entertainerz/special/ptwhitetick.webp" class="img-fluid mx-auto d-block"></li>
                        <li><img src="https://cdn.oppyo.com/launches/entertainerz/special/ptwhitetick.webp" class="img-fluid mx-auto d-block"></li>
                        <li><img src="https://cdn.oppyo.com/launches/entertainerz/special/ptwhitetick.webp" class="img-fluid mx-auto d-block"></li>
                        <li><img src="https://cdn.oppyo.com/launches/entertainerz/special/ptwhitetick.webp" class="img-fluid mx-auto d-block"></li>
                        <li><img src="https://cdn.oppyo.com/launches/entertainerz/special/ptwhitetick.webp" class="img-fluid mx-auto d-block"></li>
                     </ul>
                  </div>
               </div>
               <div class="col-md-3 col-3">
                  <div class="third-row">
                     <ul class="f-md-16 f-14">
                        <li class="f-md-20 f-16">Others</li>
                        <li><img src="https://cdn.oppyo.com/launches/entertainerz/special/ptcross.webp" class="img-fluid mx-auto d-block"></li>
                        <li><img src="https://cdn.oppyo.com/launches/entertainerz/special/ptcross.webp" class="img-fluid mx-auto d-block"></li>
                        <li><img src="https://cdn.oppyo.com/launches/entertainerz/special/ptcross.webp" class="img-fluid mx-auto d-block"></li>
                        <li><img src="https://cdn.oppyo.com/launches/entertainerz/special/ptcross.webp" class="img-fluid mx-auto d-block"></li>
                        <li><img src="https://cdn.oppyo.com/launches/entertainerz/special/ptcross.webp" class="img-fluid mx-auto d-block"></li>
                        <li><img src="https://cdn.oppyo.com/launches/entertainerz/special/ptcross.webp" class="img-fluid mx-auto d-block"></li>
                        <li><img src="https://cdn.oppyo.com/launches/entertainerz/special/ptcross.webp" class="img-fluid mx-auto d-block"></li>
                        <li><img src="https://cdn.oppyo.com/launches/entertainerz/special/ptcross.webp" class="img-fluid mx-auto d-block"></li>
                        <li><img src="https://cdn.oppyo.com/launches/entertainerz/special/ptcross.webp" class="img-fluid mx-auto d-block"></li>
                        <li><img src="https://cdn.oppyo.com/launches/entertainerz/special/ptcross.webp" class="img-fluid mx-auto d-block"></li>
                        <li><img src="https://cdn.oppyo.com/launches/entertainerz/special/ptcross.webp" class="img-fluid mx-auto d-block"></li>
                        <li><img src="https://cdn.oppyo.com/launches/entertainerz/special/ptcross.webp" class="img-fluid mx-auto d-block"></li>
                        <li><img src="https://cdn.oppyo.com/launches/entertainerz/special/ptcross.webp" class="img-fluid mx-auto d-block"></li>
                        <li><img src="https://cdn.oppyo.com/launches/entertainerz/special/ptcross.webp" class="img-fluid mx-auto d-block"></li>
                        <li><img src="https://cdn.oppyo.com/launches/entertainerz/special/ptcross.webp" class="img-fluid mx-auto d-block"></li>
                        <li><img src="https://cdn.oppyo.com/launches/entertainerz/special/ptcross.webp" class="img-fluid mx-auto d-block"></li>
                        <li><img src="https://cdn.oppyo.com/launches/entertainerz/special/ptcross.webp" class="img-fluid mx-auto d-block"></li>
                        <li><img src="https://cdn.oppyo.com/launches/entertainerz/special/ptcross.webp" class="img-fluid mx-auto d-block"></li>
                        <li><img src="https://cdn.oppyo.com/launches/entertainerz/special/ptcross.webp" class="img-fluid mx-auto d-block"></li>
                        <li><img src="https://cdn.oppyo.com/launches/entertainerz/special/ptcross.webp" class="img-fluid mx-auto d-block"></li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <div class="imagine-sec pb0">
         <div class="container">
            <div class="row">
               <div class="col-md-6 col-12">
                  <img src="https://cdn.oppyo.com/launches/entertainerz/special/imagine-box.webp" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 col-md-6 mt30 mt-md80 mb-md0 mb30">
                  <div class="f-18 f-md-20 w400 lh150 black-clr">
                     ... ! A new lightweight piece of technology that’s so incredibly sophisticated yet intuitive & easy to use that allows you to do it all, with literally ONE click!<br><br> <b>That’s EXACTLY what we’ve designed Entertainerz to do for you.</b><br><br>                        So, if you want to build super engaging websites packed with TRENDY content & videos at the push of a button, then get viral social traffic automatically and convert it into SALES, all from start to finish, then Entertainerz is made for you! 
                  </div>
               </div>
            </div>
         </div>
      </div>
     
      <div class="need-marketing">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-22 f-md-30 w700 lh150 purple-clr">
                     It Works Seamlessly for ANY NICHE… All at the push of a button. 
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50">
                  <div class="row align-items-center flex-wrap">
                     <div class="col-12 col-md-7">
                        <div class="f-20 f-md-28 w600 lh150 black-clr text-center text-md-start">
                           Entertainerz Is Designed to Meet Every Marketer’s Need: 
                        </div>
                        <div class="f-18 f-md-20 w400 lh150 black-clr mt20">
                           <ul class="noneed-listing pl0">
                              <li>Anyone who wants a nice PASSIVE income while focusing on bigger projects</li>
                              <li>Lazy people who want easy profits</li>
                              <li>Anyone who wants to value their business and money and is not ready to sacrifice them</li>
                              <li>People without products who want to kickstart online</li>
                           </ul>
                        </div>
                     </div>
                     <div class="col-12 col-md-5 mt20 mt-md0">
                        <img src="https://cdn.oppyo.com/launches/entertainerz/special/platfrom.webp" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md70 text-center">
                  <div class="look-shape">
                     <div class="f-22 f-md-30 w600 lh150 white-clr">
                        Look, it doesn't matter who you are or what you're doing.
                     </div>
                     <div class="f-18 f-md-20 w400 lh150 white-clr mt10">
                        If you want to finally drive laser targeted traffic to your offers, make the money that you want <br class="d-none d-md-block">  and live in a way you dream of, Entertainerz is for you.
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- CTA Section Start -->
      <div class="cta-section-white">
         <div class="container">
            <div class="row align-items-center">
               <!-- CTA Btn Section Start -->
               <div class="col-12 col-md-5">
                  <img src="https://cdn.oppyo.com/launches/entertainerz/special/limited-time-offer-banner.webp" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 col-md-7 mt30 mt-md0">
                  <div class="f-18 f-md-20 w500 lh150 text-center white-clr">
                     (Free Commercial License + Low 1-Time Price + 6 Special Bonuses worth $2255)
                  </div>
                  <div class="row">
                     <div class="col-12 mt20 mt-md20 text-center">
                        <a href="https://warriorplus.com/o2/buy/h5143v/y8157d/g63083" class="cta-link-btn">Get Instant Access To Entertainerz</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                     <img src="https://cdn.oppyo.com/launches/entertainerz/special/compaitable-with1.webp" class="img-responsive mx-xs-center md-img-right">
                     <div class="d-md-block d-none px-md30"><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/v-line.webp" class="img-fluid "></div>
                     <img src="https://cdn.oppyo.com/launches/entertainerz/special/days-gurantee1.webp" class="img-responsive mx-xs-auto mt15 mt-md0">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="offer-new">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="offer">
                     <div><img src="https://cdn.oppyo.com/launches/appzilo/special/error.webp"></div>
                     <div class="f-18 w400 white-clr lh140"><b>FAIR WARNING! After Every Hour The Price Goes Up,</b> So Act Now or Come Later & Pay More</div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- CTA Section End -->
      <!---Bonus Section Starts-->
      <div class="bonus-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center mx-auto">
                  <img src="https://cdn.oppyo.com/launches/entertainerz/special/bonus-shape.webp" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 col-md-12 mt20 mt-md40">
                  <div class="f-22 f-md-24 w500 lh150 text-center">
                     When You Grab Your Entertainerz Account Today, You’ll Also Get These <span class="w600">Fast Action Bonuses!</span>
                  </div>
               </div>
               <div class="col-12 col-md-12 mt20 mt-md40">
                  <div class="f-28 f-md-45 w700 lh150 text-center">
                     Limited Time Bonuses!
                  </div>
               </div>
               <div class="col-12 col-md-12 mt60 mt-md70">
                  <div class="bonus-section-shape text-center">
                     <div class="col-12 margin-t-60">
                        <div class="bonus-headline">
                           <div class="f-md-28 f-20 w700 white-clr text-nowrap text-center">
                              Limited Time Bonus #1
                           </div>
                        </div>
                     </div>
                     <div class="row d-flex align-items-center flex-wrap mt15 mt-md30">
                        <div class="col-12 col-md-7 order-md-2">
                           <div class="f-22 f-md-30 w700 lh150 text-start black-clr">
                              LIVE Training: See How You Can Launch, Sell & Market Any Product, Course or Service - Fast & Easy!! (First 1000 buyers only - $997 Value)
                           </div>
                           <div class="f-18 f-md-20 w400 lh150 mt10 text-start">
                              Launch Your Own Product In FEW Minutes.<br><br>
                              This Awesome LIVE Training will Help you to Start Online and Grow your Online Business for Marketing & Selling your Products and Services. And Free Access to a Private Community of Like-Minded Entrepreneurs to learn and grow together with each other.<br><br>
                              And There will be a Live Q & A Session at the end of the training.
                           </div>
                        </div>
                        <div class="col-12 col-md-5 order-md-1 mt20 mt-md0">
                           <div> <img src="https://cdn.oppyo.com/launches/entertainerz/special/bonus1.webp" class="img-fluid d-block mx-auto max-h450"> </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-12 mt60 mt-md70">
                  <div class="bonus-section-shape text-center">
                     <div class="col-12 margin-t-60">
                        <div class="bonus-headline">
                           <div class="f-md-28 f-20 w700 white-clr text-nowrap text-center">
                              Limited Time Bonus #2 
                           </div>
                        </div>
                     </div>
                     <div class="row d-flex align-items-center flex-wrap mt15 mt-md30">
                        <div class="col-12 col-md-7">
                           <div class="f-22 f-md-30 w700 lh150 text-start black-clr">Oppyo- All in One Growth Platform for Entrepreneurs (That's PRICELESS)</div>
                           <div class="f-18 f-md-20 w400 lh150 mt10 text-start"> All-in-one growth platform for Entrepreneurs & SMBs to generate more leads, bring more growth & sell more, more quickly. It’s got everything you need to convert more visitors and bring more growth at every customer touchpoint... without any designer, tech team, or the usual hassles.
                              <br><br>
                              This package is something that’s of huge worth for your business, but we’re offering it for free only with your investment in Cordova today. 
                           </div>
                        </div>
                        <div class="col-12 col-md-5 mt20 mt-md0">
                           <img src="https://cdn.oppyo.com/launches/entertainerz/special/bonus2.webp" class="img-fluid d-block mx-auto">
                        </div>
                     </div>
                  </div>
               </div>

               <div class="col-12 col-md-12 mt20 mt-md40">
                  <div class="f-28 f-md-45 w700 lh150 text-center">
                     Super Value Bonuses
                  </div>
               </div>
               <div class="col-12 col-md-12 mt60 mt-md70">
                  <div class="bonus-section-shape text-center">
                     <div class="col-12 margin-t-60">
                        <div class="bonus-headline">
                           <div class="f-md-28 f-20 w700 white-clr text-nowrap text-center">
                              Super Value Bonus #3 
                           </div>
                        </div>
                     </div>
                     <div class="row d-flex align-items-center flex-wrap mt15 mt-md30">
                        <div class="col-12 col-md-7">
                           <div class="f-22 f-md-30 w700 lh150 text-start black-clr">Done-For-You Setup Service (It’s Priceless)
                           </div>
                           <div class="f-20 f-md-22 w400 lh150 mt10 text-start">
                              Complete Setup Service so you can kickstart your online marketing business with Trenzy instantly.<br><br>
                              You don’t need to worry about planning and designing landing pages, newsletter templates, web forms, notification pop-up templates, A/B testing them, or even for low email opt-in rates and poor Inboxing. Just tweak these templates according to your brand or campaign's motive and target audience. There are no huge Ad Budgets, low-engaging designs, and almost everything that holds you back. Not even any additional costs to get started with the system.<br><br>
                              Just provide us with a few details, and our dedicated team will set up everything for you. We want to see you succeed in the shortest time possible.
                           </div>
                        </div>
                        <div class="col-12 col-md-5  mt20 mt-md0">
                           <div> <img src="https://cdn.oppyo.com/launches/entertainerz/special/bonus4.webp" class="img-fluid d-block mx-auto max-h450"> </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-12 mt60 mt-md70">
                  <div class="bonus-section-shape text-center">
                     <div class="col-12 margin-t-60">
                        <div class="bonus-headline">
                           <div class="f-md-28 f-20 w700 white-clr text-nowrap text-center">
                              Super Value Bonus #4 
                           </div>
                        </div>
                     </div>
                     <div class="row d-flex align-items-center flex-wrap mt15 mt-md30">
                        <div class="col-12 col-md-7 order-md-2">
                           <div class="f-22 f-md-30 w700 lh150 text-start black-clr">How To Monetizing Your Website
                           </div>
                           <div class="f-20 f-md-22 w400 lh150 mt10 text-start">
                              This Video Training Will Teach You Website and Blog Monetization Strategies. It will show you how to start earning money from your trending content website - <br><br>
                              You will learn a lot about:<br>
                              <div class="f-18 f-md-20 w400 lh150 black-clr mt20">
                                 <ul class="noneed-listing pl0">
                                    <li>Ad placement</li>
                                    <li>Advertisement</li>
                                    <li>Pre-Selling Strategies</li>
                                    <li>Finding Offers & Deals In Your Niche</li>
                                    <li>Building And Monetizing Your List</li>
                                    <li>List Automation</li>
                                    <li>Marketing Funnel</li>
                                    <li>And much more!</li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                        <div class="col-12 col-md-5 order-md-1 mt20 mt-md0">
                           <div> <img src="https://cdn.oppyo.com/launches/entertainerz/special/bonus5.webp" class="img-fluid d-block mx-auto max-h450"> </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-12 mt60 mt-md70">
                  <div class="bonus-section-shape text-center">
                     <div class="col-12 margin-t-60">
                        <div class="bonus-headline">
                           <div class="f-md-28 f-20 w700 white-clr text-nowrap text-center">
                              Super Value Bonus #5 
                           </div>
                        </div>
                     </div>
                     <div class="row d-flex align-items-center flex-wrap mt15 mt-md30">
                        <div class="col-12 col-md-7">
                           <div class="f-22 f-md-30 w700 lh150 text-start black-clr">How to Start Affiliate Marketing
                           </div>
                           <div class="f-20 f-md-22 w400 lh150 mt10 text-start">
                              When done properly, Affiliate Marketing can be a Very Lucrative Component of an Online Business. This course will teach you what you need to know so you can be a successful affiliate marketer.<br><br>
                              You'll Learn How To:<br>
                              <div class="f-18 f-md-20 w400 lh150 black-clr mt20">
                                 <ul class="noneed-listing pl0">
                                    <li>How to Pick a Niche Market</li>
                                    <li>Finding Quality Products that Fit</li>
                                    <li>Implementing the Bonus Strategy</li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                        <div class="col-12 col-md-5  mt20 mt-md0">
                           <div> <img src="https://cdn.oppyo.com/launches/entertainerz/special/bonus6.webp" class="img-fluid d-block mx-auto max-h450"> </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-12 mt60 mt-md70">
                  <div class="bonus-section-shape text-center">
                     <div class="col-12 margin-t-60">
                        <div class="bonus-headline">
                           <div class="f-md-28 f-20 w700 white-clr text-nowrap text-center">
                              Super Value Bonus #6 
                           </div>
                        </div>
                     </div>
                     <div class="row d-flex align-items-center flex-wrap mt15 mt-md30">
                        <div class="col-12 col-md-7 order-md-2">
                           <div class="f-22 f-md-30 w700 lh150 text-start black-clr">How To Encash The Power of Viral Marketing
                           </div>
                           <div class="f-20 f-md-22 w400 lh150 mt10 text-start">
                              Believe it or not, people interested in whatever it is you are promoting are already congregating online. With this Video Training, you will Discover a Shortcut to Online Viral Marketing Secrets.<br><br>
                              Topics covered:
                              <br>
                              <div class="f-18 f-md-20 w400 lh150 black-clr mt20">
                                 <ul class="noneed-listing pl0">
                                    <li>The Two-Step Trick to Effective Viral Marketing</li>
                                    <li>How Do You Find Hot Content?</li>
                                    <li>Maximize Niche Targeting for Your Curated Content</li>
                                    <li>Filter your content format to go viral on many platforms</li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                        <div class="col-12 col-md-5 order-md-1 mt20 mt-md0">
                           <div> <img src="https://cdn.oppyo.com/launches/entertainerz/special/bonus7.webp" class="img-fluid d-block mx-auto max-h450"> </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-12 mt60 mt-md70">
                  <div class="bonus-section-shape text-center">
                     <div class="col-12 margin-t-60">
                        <div class="bonus-headline">
                           <div class="f-md-28 f-20 w700 white-clr text-nowrap text-center">
                              Super Value Bonus #7 
                           </div>
                        </div>
                     </div>
                     <div class="row d-flex align-items-center flex-wrap mt15 mt-md30">
                        <div class="col-12 col-md-7">
                           <div class="f-22 f-md-30 w700 lh150 text-start black-clr">Video Marketing Profit Kit 
                           </div>
                           <div class="f-20 f-md-22 w400 lh150 mt10 text-start">
                              Video is able to put a face to your brand and make your brand talk to the needs, fears, hopes, and aspirations of your prospective customers. And with this Specialised Training, you will discover how to use video marketing to build a thriving online business!<br><br>
                              You'll Learn How To:<br>
                              <div class="f-18 f-md-20 w400 lh150 black-clr mt20">
                                 <ul class="noneed-listing pl0">
                                    <li>Find Winning Video Topics</li>
                                    <li>Save Money on Video Creation</li>
                                    <li>Make Money from Your Videos</li>
                                    <li>Includes ready sales materials!</li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                        <div class="col-12 col-md-5  mt20 mt-md0">
                           <div> <img src="https://cdn.oppyo.com/launches/entertainerz/special/bonus8.webp" class="img-fluid d-block mx-auto max-h450"> </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--Bonus Section ends -->
      <!--Bonus value Section start -->
      <div class="bonus-section-value">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-6 mx-auto">
                  <img src="https://cdn.oppyo.com/launches/entertainerz/special/thats-a-total.webp" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 mt-md100 mt20">
                  <img src="https://cdn.oppyo.com/launches/entertainerz/special/arrow.webp" alt="" class="img-fluid d-block mx-auto vert-move">
               </div>
            </div>
         </div>
      </div>
      <!--Bonus value Section ends -->
      <div class="space-section license-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-md-45 f-28 w700 lh150 black-clr text-center">
                     Also Get Our Free Commercial License <br class="d-none d-md-block"> When You Get Access To Entertainerz Today!
                  </div>
               </div>
               <div class="col-12 mt25 mt-md70">
                  <div class="row d-flex align-items-center flex-wrap">
                     <div class="col-12 col-md-5 ">
                        <img src="https://cdn.oppyo.com/launches/entertainerz/special/commercial-image.webp" class="img-fluid d-block mx-auto"> 
                     </div>
                     <div class="col-12 col-md-7 mt20 mt-md0">
                        <div class="f-md-20 f-18 lh150 w400">
                           As we have shown you, there are tons of businesses & marketers – yoga gurus, fitness centers, social media marketers or anybody else who want to start online & Drive FREE Traffic,<span class="w600"> NEED YOUR SERVICE & would love to pay you monthly for your services.</span>
                           <br><br>
                           Build their branded & traffic generating trendy websites. We’ve taken care of everything so that you can deliver those services easily & 100% profits in your pocket.
                           <br><br>
                           <span class="w600">Note: </span>This special commercial license is being included in the Advanced plan and ONLY during this launch. Take advantage of it now because it will never be offered again.
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- No Need Section -->
      <div class="noneed-section">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-12">
                  <div class="f-20 f-md-24 w400 lh150 text-center">
                     <span class="w600">Only 3 easy steps is all it takes </span>to publish trendy <br class="d-none d-md-block"> content & bring hordes of TRAFFIC & Sales… 
                  </div>
                  <div class="f-md-45 f-28 w700 black-clr lh150 text-center mt20 mt-md40">
                     With Entertainerz, Turn Your Worries <br class="d-none d-md-block"> Into A HUGE Profitable Opportunity 
                  </div>
                  <div class="noneed-shape mt20 mt-md40">
                     <div class="f-20 f-md-20 w400 lh150">
                        <ul class="noneed-list pl0">
                           <li><span class="w600">No more worrying</span> for researching and writing content daily </li>
                           <li><span class="w600">No need for learning</span> complex video & audio editing skills</li>
                           <li><span class="w600">No worries for buying</span> expensive equipment that can leave your back accounts dry </li>
                           <li><span class="w600">No more frustration</span> of not being able to face the camera & creating a mockery of yourself </li>
                           <li><span class="w600">No worries</span> for lack of technical, designing or marketing skills </li>
                           <li><span class="w600">Say no to wasting</span> any time on creating funnels, SEO, paid traffic, complex tools etc. </li>
                           <li class="happy"><span class="w700">And…say YES Now to living a LAPTOP lifestyle</span> that gives you an ability to spend more time with your family and have complete financial FREEDOM.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- No Need Section End-->
      <!-- Table Section Start -->
      <div class="table-section1" id="buynow">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-20 f-md-24 w600 text-center lh150 black-clr">
                     Take Advantage Of Our Limited Time Deal 
                  </div>
                  <div class="f-md-45 f-28 w700 lh150 text-center mt10">
                     Get Entertainerz For A Low One-Time- 
                     <br class="d-none d-md-block"> Price, No Monthly Fee.
                  </div>
                  <div class="f-18 f-md-20 w500 lh150 text-center mt20">
                     (Free Commercial License + Low 1-Time Price + 6 Special Bonuses worth $2255)
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
                  <div class="row">
                     <!-- <div class="col-12 col-md-6">
                        <div class="table-wrap">
                            <div class="table-head text-center">
                                <div>
                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                        viewBox="0 0 977.7 276.9" style="enable-background:new 0 0 977.7 276.9; max-height:45px;" xml:space="preserve">
                        <style type="text/css">
                        .stblue{fill:#0041B5;}
                        .storg{fill:#FF7E1D;}
                        </style>
                        <g>
                        <path class="stblue" d="M0,213.5V12.2h99.1c19.4,0,34.5,4.3,45.5,12.7c11,8.5,16.5,21.1,16.5,37.9c0,20.9-8.6,35.7-25.9,44.3
                        c14.1,2.2,24.9,7.6,32.4,16.2c7.5,8.6,11.3,19.8,11.3,33.7c0,11.8-2.6,21.9-7.9,30.3c-5.3,8.4-12.8,14.9-22.6,19.4
                        c-9.8,4.5-21.3,6.8-34.6,6.8H0z M39.4,93.7h50.5c10.7,0,18.9-2.2,24.6-6.6c5.7-4.4,8.5-10.8,8.5-19.1c0-8.4-2.8-14.7-8.4-18.8
                        c-5.6-4.2-13.8-6.3-24.7-6.3H39.4V93.7z M39.4,182.7h60.9c12.4,0,21.7-2.3,27.9-6.8c6.2-4.5,9.3-11.3,9.3-20.3
                        c0-8.9-3.2-15.8-9.6-20.7c-6.4-4.8-15.6-7.3-27.6-7.3H39.4V182.7z"/>
                        <path class="stblue" d="M269.3,217.4c-22.9,0-39.3-4.5-49.3-13.5c-10-9-15.1-23.7-15.1-44.2V88.6h37.4v62.2c0,13.2,2,22.5,6,27.9
                        c4,5.4,11,8.1,20.9,8.1c9.8,0,16.7-2.7,20.7-8.1c4.1-5.4,6.1-14.7,6.1-27.9V88.6h37.4v71.1c0,20.4-5,35.1-15,44.2
                        C308.6,212.9,292.2,217.4,269.3,217.4z"/>
                        <path class="stblue" d="M357.8,213.5l68.9-96.4h-58.9V88.6h122.1l-69,96.4h64.8v28.5H357.8z"/>
                        <path class="stblue" d="M502.6,213.5l68.9-96.4h-58.9V88.6h122.1l-69,96.4h64.8v28.5H502.6z"/>
                        <path class="storg" d="M682.9,62.6c-4.1,0-7.8-1-11.2-3c-3.4-2-6.1-4.7-8.1-8.1c-2-3.4-3-7.2-3-11.2c0-4.1,1-7.8,3.1-11.1
                        c2-3.3,4.7-6,8.1-8c3.3-2,7-3,11.1-3c4.1,0,7.8,1,11.1,3c3.3,2,6,4.7,8.1,8c2,3.3,3,7,3,11.1c0,4.1-1,7.8-3,11.2
                        c-2,3.4-4.7,6.1-8.1,8.1C690.6,61.6,686.9,62.6,682.9,62.6z M664.2,213.5V88.6h37.4v124.9H664.2z"/>
                        <path class="storg" d="M751.3,213.5V117h-16.8V88.6h16.8v-31c0-11.8,1.8-22,5.5-30.6c3.7-8.6,8.9-15.3,15.8-19.9
                        c6.9-4.7,15.1-7,24.7-7c5.9,0,11.8,0.9,17.6,2.7c5.8,1.8,10.7,4.4,14.9,7.6l-12.9,26.2c-3.8-2.4-8.1-3.5-12.9-3.5
                        c-4.2,0-7.4,1.1-9.6,3.2c-2.2,2.1-3.7,5.1-4.5,8.9c-0.8,3.8-1.2,8.2-1.2,13.2v30.3h29.5V117h-29.5v96.5H751.3z"/>
                        <path class="storg" d="M839.4,276.9l40.3-79L820.3,88.6h42.9l38.2,73.2l33.3-73.2h42.9l-95.4,188.3H839.4z"/>
                        </g>
                        </svg>
                        </div>
                                <div class="w600 f-md-22 f-22 lh120 text-center text-uppercase mt15 mt-md30">Basic Plan</div>
                                <div class="center-line"></div> 
                            </div>
                            <div class="">
                                <ul class="table-list pl0 f-18 f-md-18 w500 lh150 black-clr">
                        <li>Done for You Lead generation Funnels</li>
                        <li>Inbuilt Social Syndication system</li>
                        <li>One Click A.I Analyzed offers and Links</li>
                        <li>Monetize better and MAKE MORE PROFITS with stunning banner ads</li>
                        <li>Click and Point Ecom Commission Pages</li>
                        <li>FB Messenger Chat Integration</li>
                        <li>Ultra-Fast Funnel Creation</li>
                        <li>Add elegant & eye-catching sliders to lure visitors</li>
                        <li>Double the POWER Of Your Campaigns With Smart Animation Effects</li>
                        <li>Integration with over 70+ Channels</li>
                        <li>Safe, Secure & 100% GDPR Complaint</li>
                        <li>Attractive Ready-to-Use Themes That Add Spark To Your Sites</li>
                        <li>Add elegant & eye-catching sliders to lure visitors</li>
                        <li>Curate UNLIMITED Fascinating Viral Videos</li>
                        <li>Instantly Curate Trending and Viral Content</li>
                        <li>Packed with PROVEN Converting & Ready-To-Use Promotional Templates</li>
                        <li>Easy-to-use, Inbuilt Text & Inline Editor</li>
                        <li>Auto-Curation of Top & Related Videos</li>
                        <li>Share your content & video capsules on 3 major Social Networks</li>
                        <li>Inbuilt content-spinner to create unique description for your stories</li>
                        <li>Boost engagement levels of your sites</li>
                        <li>All-in-one cloud-based site builder</li>
                        <li>Personal Use License included</li>
                        <li class="cross-sign">Use For Your Clients</li>
                        <li class="cross-sign">Provide High In Demand Services</li>
                        <li class="headline">BONUSES WORTH $2255 FREE!!!</li>
                        <li class="cross-sign">Fast Action Bonus 1 - Oppyo- All in One Growth Platform</li>
                        <li class="cross-sign">Fast Action Bonus 2 - LIVE Training( $997 Value)</li>
                        <li class="cross-sign">Fast Action Bonus 3 - Auto Video Creator (Worth $297)</li>
                        <li class="cross-sign">Fast Action Bonus 4 - Advanced Video Marketing (Worth $397)	</li>
                                </ul>
                            </div>
                            <div class="table-btn">
                                <div class="hideme-button">
                                    <a href="https://warriorplus.com/o2/buy/sq4h8c/dtcw83/bntldv"><img src="https://warriorplus.com/o2/btn/fn200011000/sq4h8c/dtcw83/296609" alt="Entertainerz Standard" class="img-fluid d-block mx-auto" border="0" /></a>
                                </div>
                            </div>
                        </div>
                        </div> -->
                     <div class="col-12 col-md-10 mx-auto mt20 mt-md0">
                        <div class="table-wrap1 relative">
                           <div class="table-head1">
                              <div>
                                 <svg id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 189.77 50" style="max-height:80px">
                                    <defs>
                                       <style>.cls-1{fill:url(#linear-gradient-2);}.cls-2{fill:#fff;}.cls-3{fill:#001730;}.cls-4{fill:url(#linear-gradient-3);}.cls-5{fill:url(#linear-gradient);}</style>
                                       <linearGradient id="linear-gradient" x1="20.65" y1="26.83" x2="42.69" y2="26.83" gradientUnits="userSpaceOnUse">
                                          <stop offset="0" stop-color="#bf003b"/>
                                          <stop offset="1" stop-color="#e23940"/>
                                       </linearGradient>
                                       <linearGradient id="linear-gradient-2" x1=".12" y1="29.26" x2="25.92" y2="29.26" gradientUnits="userSpaceOnUse">
                                          <stop offset="0" stop-color="#f38227"/>
                                          <stop offset="1" stop-color="#fbb826"/>
                                       </linearGradient>
                                       <linearGradient id="linear-gradient-3" x1="3.09" y1="36.12" x2="22.33" y2="28.06" gradientUnits="userSpaceOnUse">
                                          <stop offset="0" stop-color="#3369e9"/>
                                          <stop offset="1" stop-color="#002b7f"/>
                                       </linearGradient>
                                    </defs>
                                    <path class="cls-2" d="M22.29,0c-.94,0-1.69,.76-1.69,1.69,0,.75,.48,1.39,1.16,1.61l-2.95,10.01-.61,2.09,2.13,.26,.41-2.75v-.03s1.44-9.5,1.44-9.5c.03,0,.07,0,.11,0,.93,0,1.69-.76,1.69-1.7s-.76-1.69-1.69-1.69Z"/>
                                    <path class="cls-2" d="M16.2,15.76l-1.74,.96-5.3-6.31c-.29,.23-.64,.36-1.03,.36-.94,0-1.7-.75-1.7-1.69s.76-1.69,1.7-1.69,1.69,.76,1.69,1.69c0,.41-.15,.79-.4,1.08l6.78,5.6Z"/>
                                    <g>
                                       <path class="cls-2" d="M63.68,34.98c0,.14-.04,.35-.11,.62-.08,.34-.13,.55-.15,.62-.03,.32-.09,.79-.19,1.41-.05,.16-.16,.24-.35,.24-.09,0-.26-.02-.51-.05-1.1-.16-2.82-.24-5.13-.24-.66,0-1.64,.02-2.96,.05s-2.31,.05-2.96,.05c-.3,0-.44-.13-.44-.39,0-1.01,.06-2.52,.17-4.53,.11-2.02,.17-3.53,.17-4.53s-.05-2.93-.16-5.72c-.02-.72-.07-1.79-.15-3.22v-.19c-.02-.21,.09-.31,.34-.31,.63,0,1.57,.02,2.83,.05,1.26,.04,2.2,.05,2.83,.05s1.5-.02,2.7-.05c1.2-.04,2.1-.05,2.7-.05,.42,0,.65,.13,.69,.39,.19,1.19,.28,1.92,.28,2.21,0,.21-.1,.31-.3,.31-.76,0-1.92-.04-3.47-.13-1.55-.09-2.71-.13-3.5-.13-.74,0-1.2,.1-1.39,.3-.19,.2-.29,.69-.29,1.47v2.01c0,.55,.01,.87,.04,.96,.08,.28,.31,.42,.7,.42,.11,0,.26,0,.46-.01h.46c1.76,0,3.34-.03,4.74-.09,.55-.02,1.19-.06,1.94-.13,.19-.02,.28,.06,.28,.24,0,.29-.03,.73-.1,1.32-.07,.59-.1,1.04-.1,1.33,0,.16-.13,.24-.4,.24-.11,0-.51-.03-1.2-.09-.5-.05-1.63-.08-3.39-.08-1.48,0-2.42,.02-2.82,.05-.36,.03-.57,.22-.62,.57,0-.06,.01,.28,.01,1.04v1.99c0,.93,.14,1.52,.43,1.75,.22,.19,.79,.28,1.68,.28h1.48c.63-.01,2.39-.12,5.29-.34h.2c.18-.03,.27,.08,.27,.32Z"/>
                                       <path class="cls-2" d="M77.61,37.42c0,.2-.09,.3-.27,.3-.3,0-.74,0-1.33-.01-.59,0-1.03-.01-1.33-.01-.16,0-.24-.13-.24-.39,0-.44,.01-1.11,.04-2.01,.03-.9,.04-1.57,.04-2.02,0-.36,0-.9-.01-1.61,0-.71-.01-1.25-.01-1.61,0-1.02-.15-1.75-.44-2.17-.36-.5-1.02-.75-1.99-.75-.45,0-1.07,.25-1.87,.75-.84,.53-1.27,1-1.27,1.41v8.08c0,.22-.09,.34-.26,.34-.29,0-.72,0-1.29-.01-.57,0-1.01-.01-1.29-.01-.19,0-.28-.1-.28-.31,0-.71,.01-1.77,.03-3.19,.02-1.42,.03-2.49,.03-3.21,0-1.91-.2-3.68-.61-5.29-.03-.08-.04-.14-.04-.18,0-.09,.06-.15,.18-.19,.06,0,.58-.09,1.54-.25,.97-.16,1.47-.24,1.52-.24,.05,0,.09,.07,.11,.22,.04,.56,.15,1.12,.31,1.68,.47-.37,1.08-.83,1.85-1.37,.94-.57,1.85-.86,2.73-.86,1.69,0,2.84,.49,3.46,1.47,.46,.72,.69,1.88,.69,3.49,0,.28,0,.7-.02,1.28-.01,.57-.02,1.01-.02,1.29,0,.6,0,1.5,.03,2.69,.02,1.19,.03,2.09,.03,2.69Z"/>
                                       <path class="cls-2" d="M87.67,35.21c0,.2-.02,.47-.05,.81-.05,.42-.07,.7-.08,.82,0,.16-.02,.26-.04,.3-.04,.06-.12,.13-.26,.19-.74,.35-1.89,.53-3.48,.53-2.1,0-3.15-.95-3.15-2.86,0-.76,.02-1.91,.05-3.44,.04-1.53,.05-2.68,.05-3.44,0-.37-.18-.57-.55-.59-.33,0-.66,0-.98-.01-.19-.04-.28-.41-.28-1.1,0-.26,.02-.53,.05-.82,.03-.22,.17-.35,.43-.39,.22,0,.44,0,.66-.01,.41-.02,.62-.2,.62-.55s-.01-.91-.03-1.64c-.02-.74-.03-1.29-.03-1.66,0-.66,.13-.98,.38-.98,.08,0,.92,.14,2.53,.42,.21,.03,.31,.13,.31,.3,0,.4-.03,.99-.09,1.78-.06,.79-.09,1.38-.09,1.78,0,.26,.12,.39,.36,.39h3.23c.15,0,.23,.05,.23,.16s-.02,.28-.05,.52c-.03,.24-.05,.42-.05,.53,0,.13,0,.34,.01,.62,0,.28,.01,.48,.01,.62,0,.17-.13,.26-.38,.26-.36,0-.9-.02-1.63-.07-.73-.05-1.27-.07-1.63-.07-.07,0-.13,.26-.16,.77-.05,.64-.07,1.53-.07,2.68v2.01c0,.77,.07,1.31,.22,1.62,.22,.47,.66,.7,1.33,.7,.28,0,.69-.06,1.23-.18s.94-.18,1.2-.18c.11,0,.16,.08,.16,.24Z"/>
                                       <path class="cls-2" d="M101.01,30.46c0,.66-.29,1.11-.86,1.33-.39,.14-3.17,.41-8.34,.79,.12,.79,.55,1.46,1.31,2.02,.72,.53,1.49,.79,2.32,.79,1.42,0,2.72-.46,3.91-1.37,.14-.12,.29-.23,.43-.35,.07,0,.12,0,.14,.03,.02,.02,.19,.33,.51,.94,.31,.61,.47,.93,.47,.98,0,.05-.07,.16-.22,.31-1.32,1.4-3.01,2.1-5.08,2.1s-3.74-.62-4.98-1.87c-1.24-1.24-1.86-2.9-1.86-4.98,0-1.85,.59-3.46,1.78-4.84,1.25-1.43,2.78-2.14,4.59-2.14,1.69,0,3.1,.62,4.23,1.86,1.1,1.2,1.66,2.67,1.66,4.39Zm-3.21-.82c0-.75-.29-1.41-.88-1.98s-1.25-.86-1.99-.86c-.83,0-1.56,.33-2.2,1-.64,.66-.96,1.41-.96,2.24,0,.19,.16,.28,.48,.28,1.33,0,2.92-.13,4.77-.4,.52-.07,.78-.16,.78-.27Z"/>
                                       <path class="cls-2" d="M110.6,25.04c0,.99-.07,1.89-.2,2.69-.03,.21-.12,.31-.27,.31-.19,0-.47-.02-.86-.06-.38-.04-.67-.06-.87-.06-1.13,0-1.85,.32-2.17,.96-.19,.39-.28,1.19-.28,2.41v1.48c0,.5,.02,1.27,.06,2.29,.04,1.02,.06,1.79,.06,2.29,0,.24-.1,.36-.31,.36-.28,0-.7,0-1.27-.01-.57,0-.99-.01-1.27-.01-.21,0-.31-.11-.31-.34,0-.73,.01-1.82,.03-3.29,.02-1.46,.03-2.56,.03-3.3,0-2.22-.21-4-.62-5.35-.02-.07-.03-.13-.03-.16,0-.11,.06-.18,.18-.23,.33-.04,.8-.09,1.4-.16,.97-.18,1.42-.27,1.35-.27,.14,0,.24,.26,.28,.79,.04,.53,.13,.79,.24,.79,.02,0,.04,0,.07-.03,.37-.23,.74-.47,1.1-.7,.4-.24,.82-.42,1.24-.54,.35-.11,.8-.16,1.35-.16,.7,0,1.05,.09,1.05,.28Z"/>
                                       <path class="cls-2" d="M119.58,35.21c0,.2-.02,.47-.05,.81-.05,.42-.07,.7-.08,.82,0,.16-.02,.26-.04,.3-.04,.06-.12,.13-.26,.19-.74,.35-1.89,.53-3.48,.53-2.1,0-3.15-.95-3.15-2.86,0-.76,.02-1.91,.05-3.44,.04-1.53,.05-2.68,.05-3.44,0-.37-.18-.57-.55-.59-.33,0-.66,0-.98-.01-.19-.04-.28-.41-.28-1.1,0-.26,.02-.53,.05-.82,.03-.22,.17-.35,.43-.39,.22,0,.44,0,.66-.01,.41-.02,.62-.2,.62-.55s-.01-.91-.03-1.64c-.02-.74-.03-1.29-.03-1.66,0-.66,.13-.98,.38-.98,.08,0,.92,.14,2.53,.42,.21,.03,.31,.13,.31,.3,0,.4-.03,.99-.09,1.78-.06,.79-.09,1.38-.09,1.78,0,.26,.12,.39,.36,.39h3.23c.15,0,.23,.05,.23,.16s-.02,.28-.05,.52c-.03,.24-.05,.42-.05,.53,0,.13,0,.34,.01,.62,0,.28,.01,.48,.01,.62,0,.17-.13,.26-.38,.26-.36,0-.9-.02-1.63-.07-.73-.05-1.27-.07-1.63-.07-.07,0-.13,.26-.16,.77-.05,.64-.07,1.53-.07,2.68v2.01c0,.77,.07,1.31,.22,1.62,.22,.47,.66,.7,1.33,.7,.28,0,.69-.06,1.23-.18s.94-.18,1.2-.18c.11,0,.16,.08,.16,.24Z"/>
                                       <path class="cls-2" d="M134.6,25.5s-.01,.12-.04,.24c-.35,2.06-.53,3.83-.53,5.32,0,.09,.11,2.11,.34,6.05v.19c.02,.26-.1,.39-.34,.39s-.62,.03-1.12,.09c-.5,.06-.87,.09-1.11,.09-.17,0-.3-.31-.39-.94-.09-.63-.18-.94-.26-.94-.05,0-.3,.18-.73,.53-.53,.43-1.02,.76-1.48,.98-.71,.36-1.42,.54-2.13,.54-1.74,0-3.21-.7-4.4-2.09-1.13-1.33-1.7-2.88-1.7-4.65,0-1.99,.57-3.64,1.71-4.94,1.19-1.36,2.77-2.03,4.71-2.03,1.37,0,2.57,.48,3.6,1.43,.15,.18,.39,.44,.71,.78,.03,.03,.05,.04,.08,.04,.05,0,.13-.28,.23-.85,.1-.57,.22-.85,.38-.85,.24,0,.7,.07,1.36,.2,.74,.16,1.1,.31,1.1,.43Zm-3.43,5.63c0-1.1-.33-2.05-.98-2.84-.69-.85-1.58-1.28-2.65-1.28s-1.98,.42-2.69,1.27c-.69,.81-1.04,1.76-1.04,2.86s.35,2.03,1.04,2.83c.72,.84,1.62,1.25,2.69,1.25s1.93-.43,2.64-1.28c.66-.8,1-1.73,1-2.8Z"/>
                                       <path class="cls-2" d="M140.3,21.58c0,1.1-.62,1.66-1.85,1.66s-1.87-.55-1.87-1.66c0-.49,.19-.9,.58-1.23,.36-.31,.79-.46,1.29-.46s.94,.16,1.3,.47c.36,.31,.55,.72,.55,1.21Zm-.09,3.65c0,.69-.04,1.74-.13,3.13-.09,1.4-.13,2.44-.13,3.15,0,.66,.01,1.64,.04,2.94,.03,1.31,.04,2.28,.04,2.93,0,.19-.08,.28-.24,.28h-2.63c-.19,0-.28-.18-.28-.55,0-.62,.02-1.55,.05-2.8,.03-1.24,.05-2.18,.05-2.81,0-.7-.05-1.75-.16-3.15s-.15-2.45-.15-3.15c0-.15,.09-.23,.28-.23,.16,0,.41,.02,.75,.07,.34,.04,.59,.07,.76,.07s.42-.02,.76-.07c.34-.04,.59-.07,.76-.07,.15,0,.23,.09,.23,.26Z"/>
                                       <path class="cls-2" d="M154.74,37.42c0,.2-.09,.3-.27,.3-.3,0-.74,0-1.33-.01-.59,0-1.03-.01-1.33-.01-.16,0-.24-.13-.24-.39,0-.44,.01-1.11,.04-2.01,.03-.9,.04-1.57,.04-2.02,0-.36,0-.9-.01-1.61,0-.71-.01-1.25-.01-1.61,0-1.02-.15-1.75-.44-2.17-.36-.5-1.02-.75-1.99-.75-.45,0-1.07,.25-1.87,.75-.84,.53-1.27,1-1.27,1.41v8.08c0,.22-.09,.34-.26,.34-.29,0-.72,0-1.29-.01-.57,0-1.01-.01-1.29-.01-.19,0-.28-.1-.28-.31,0-.71,.01-1.77,.03-3.19,.02-1.42,.03-2.49,.03-3.21,0-1.91-.2-3.68-.61-5.29-.03-.08-.04-.14-.04-.18,0-.09,.06-.15,.18-.19,.06,0,.58-.09,1.54-.25,.97-.16,1.47-.24,1.52-.24,.05,0,.09,.07,.11,.22,.04,.56,.15,1.12,.31,1.68,.47-.37,1.08-.83,1.85-1.37,.94-.57,1.85-.86,2.73-.86,1.69,0,2.84,.49,3.46,1.47,.46,.72,.69,1.88,.69,3.49,0,.28,0,.7-.02,1.28-.01,.57-.02,1.01-.02,1.29,0,.6,0,1.5,.03,2.69,.02,1.19,.03,2.09,.03,2.69Z"/>
                                       <path class="cls-2" d="M168.7,30.46c0,.66-.29,1.11-.86,1.33-.39,.14-3.17,.41-8.34,.79,.12,.79,.55,1.46,1.31,2.02,.72,.53,1.49,.79,2.32,.79,1.42,0,2.72-.46,3.91-1.37,.14-.12,.29-.23,.43-.35,.07,0,.12,0,.14,.03,.02,.02,.19,.33,.51,.94,.31,.61,.47,.93,.47,.98,0,.05-.07,.16-.22,.31-1.32,1.4-3.01,2.1-5.08,2.1s-3.74-.62-4.98-1.87c-1.24-1.24-1.86-2.9-1.86-4.98,0-1.85,.59-3.46,1.78-4.84,1.25-1.43,2.78-2.14,4.59-2.14,1.69,0,3.1,.62,4.23,1.86,1.1,1.2,1.66,2.67,1.66,4.39Zm-3.21-.82c0-.75-.29-1.41-.88-1.98s-1.25-.86-1.99-.86c-.83,0-1.56,.33-2.2,1-.64,.66-.96,1.41-.96,2.24,0,.19,.16,.28,.48,.28,1.33,0,2.92-.13,4.77-.4,.52-.07,.78-.16,.78-.27Z"/>
                                       <path class="cls-2" d="M178.29,25.04c0,.99-.07,1.89-.2,2.69-.03,.21-.12,.31-.27,.31-.19,0-.47-.02-.86-.06-.38-.04-.67-.06-.87-.06-1.13,0-1.85,.32-2.17,.96-.19,.39-.28,1.19-.28,2.41v1.48c0,.5,.02,1.27,.06,2.29s.06,1.79,.06,2.29c0,.24-.1,.36-.31,.36-.28,0-.7,0-1.27-.01-.57,0-.99-.01-1.27-.01-.21,0-.31-.11-.31-.34,0-.73,.01-1.82,.03-3.29,.02-1.46,.03-2.56,.03-3.3,0-2.22-.21-4-.62-5.35-.02-.07-.03-.13-.03-.16,0-.11,.06-.18,.18-.23,.33-.04,.8-.09,1.4-.16,.97-.18,1.42-.27,1.35-.27,.14,0,.24,.26,.28,.79,.04,.53,.13,.79,.24,.79,.02,0,.04,0,.07-.03,.37-.23,.74-.47,1.1-.7,.4-.24,.82-.42,1.24-.54,.35-.11,.8-.16,1.35-.16,.7,0,1.05,.09,1.05,.28Z"/>
                                       <path class="cls-2" d="M189.77,35.13s-.07,.36-.2,.96c-.14,.64-.22,1.03-.24,1.19-.04,.31-.12,.47-.24,.47-.31,0-.74-.02-1.27-.05-.6-.04-1.02-.07-1.25-.08-.6-.02-1.46-.03-2.59-.03-.87,0-1.84,.04-2.9,.11-1.33,.09-1.9,.13-1.71,.13-.19,0-.31-.08-.36-.24-.13-.43-.19-.93-.19-1.51,0-.08,0-.2,.01-.34s.01-.26,.01-.33c0-.13,.1-.28,.29-.45,.19-.18,1.15-1.19,2.88-3.05,.87-.91,2.11-2.3,3.7-4.19h-5.94c-.14,0-.22-.09-.22-.28,0-.26,.06-.65,.17-1.17,.11-.52,.17-.9,.17-1.15,0-.11,.13-.16,.38-.16,.5,0,1.25,.02,2.25,.07s1.75,.07,2.25,.07c2.46,0,3.85,.02,4.16,.05,.22,.03,.34,.12,.34,.28,0,1.36-.05,2.12-.16,2.29-.09,.13-.87,1.01-2.33,2.65-.94,1.07-2.37,2.66-4.27,4.77,2.99-.08,5.31-.12,6.95-.12,.21,0,.31,.04,.31,.12Z"/>
                                    </g>
                                    <path class="cls-2" d="M16.61,49.17c.96-1.64,6.11-6.32,13.48-6.77,2.78-.17,5.12,.34,6.8,.88,0,0,.84,.29,.15,.94s-11.41-1.5-19.36,5.17c-1.6,1.37-1.2,.01-1.07-.22Z"/>
                                    <path class="cls-5" d="M42.69,31.94c-.01,2.52-.67,4.93-2.77,6.32-1.62,1.06-3.89,2.45-6.95,3.79,.63-1.27,.93-2.75,.99-4.28,.01-.21,.01-.43,.01-.64,.01-9.53-3.63-17.69-6.99-21.14-.16-.16-.32-.32-.5-.47-1.51-1.33-3.74-2.37-5.74-2.61-.03,0-.06-.01-.09-.01,.03-.01,.07-.01,.1-.02,3.12-.66,6.47-1.1,9.93-1.26,2.07-.09,4.7,1.12,6.16,2.62,2.81,2.89,5.86,9.72,5.85,17.7Z"/>
                                    <path class="cls-1" d="M33.97,37.13c0,.21,0,.43-.01,.64-.06,1.53-.36,3.01-.99,4.28-.27,.12-.54,.23-.82,.35-3.01,1.24-6.73,2.41-11.29,3.21v-.21c0-.47-.01-.95-.03-1.41-.02-.5-.04-.99-.07-1.48-.01-.04-.01-.07-.01-.11-.03-.46-.07-.91-.12-1.36-.02-.25-.05-.49-.08-.74-.08-.69-.17-1.36-.28-2.03-.07-.45-.15-.89-.23-1.32-.01-.08-.03-.17-.05-.25-.1-.54-.22-1.08-.34-1.6-.03-.13-.06-.26-.09-.39-.11-.44-.22-.87-.34-1.3-.11-.44-.24-.87-.37-1.29-.06-.2-.12-.4-.19-.6-.14-.44-.28-.88-.44-1.3-.18-.51-.37-1.01-.56-1.5-.25-.62-.51-1.22-.77-1.8-.6-1.29-1.23-2.47-1.88-3.53-.12-.2-.25-.4-.38-.59-.12-.2-.25-.39-.38-.57-.63-.92-1.28-1.72-1.92-2.38-.16-.16-.33-.33-.51-.48-.02-.03-.05-.05-.08-.08-.06-.05-.12-.1-.18-.15-.45-.38-.95-.74-1.48-1.07-.14-.09-.28-.17-.42-.25-.48-.28-.98-.53-1.49-.75,1.46-.71,3.09-1.4,4.87-2.04,.53-.18,1.07-.37,1.62-.54,1.32-.43,2.71-.82,4.15-1.18,.6-.14,1.22-.28,1.84-.41,.03,0,.06,.01,.09,.01,2,.24,4.23,1.28,5.74,2.61,.18,.15,.34,.31,.5,.47,3.36,3.45,7,11.61,6.99,21.14Z"/>
                                    <path class="cls-4" d="M20.86,45.61c-3.31,.58-7.05,.95-11.28,1.01-.83,.01-1.62-.36-2.33-.79-2.42-1.46-5.57-7.74-7.18-18.65-.38-2.58,.81-5,2.76-6.72,1.27-1.12,3.09-2.28,5.33-3.39,1.36,.59,2.64,1.4,3.65,2.3,.18,.16,.34,.32,.5,.48,3.71,3.82,7.71,12.34,8.42,22.55,.08,1.05,.11,2.12,.11,3.21Z"/>
                                    <path class="cls-2" d="M6.78,22.31c3.46-3.05,12.1-6.51,21.9-6.95,1.63-.07,3.7,.88,4.84,2.06,2.21,2.28,4.61,7.65,4.6,13.93,0,1.98-.64,3.72-2.18,4.97-3.42,2.78-10.62,7.14-23.86,6.57-.65-.03-1.28-.28-1.84-.62-1.9-1.15-4.38-6.09-5.65-14.67-.3-2.03,.64-3.93,2.17-5.29Z"/>
                                    <path class="cls-3" d="M26.11,26.61l-5.61-1.56c-1.31-.36-2.85,.26-3.44,1.38-.23,.43-.29,.9-.17,1.33l1.51,5.69c.3,1.12,1.67,1.73,3.08,1.36,.54-.14,1.04-.42,1.4-.8l4.1-4.13c.96-.96,.86-2.3-.21-2.99-.2-.13-.42-.22-.66-.29h0Z"/>
                                 </svg>
                              </div>
                              <div class="w600 f-md-22 f-22 lh120 text-center text-uppercase mt15 mt-md30 white-clr">Premium</div>
                           </div>
                           <div class="">
                              <ul class="table-list1 pl0 f-18 f-md-18 w500 lh150 white-clr">
                                 <li><span class="w600">Set and Forget System </span>with Single Keyword </li>
                                 <li>Built-In Automated Traffic Generating System</li>
                                 <li>Create Self-Updating & Beautiful Trendy Sites on ANY TOPIC  </li>
                                 <li>Puts Most Profitable & A.I Analysed Affiliate Links  </li>
                                 <li>Curate UNLIMITED Fascinating Viral Videos  </li>
                                 <li>Instantly Curate Trending and Viral Content  </li>
                                 <li>Inbuilt Content-Spinner to Create Unique Stories  </li>
                                 <li>Monetize Better and MAKE MORE PROFITS With Stunning Banner Ads  </li>
                                 <li>Attractive Ready-To-Use Theme That Add Spark to Your Sites  </li>
                                 <li>Add Elegant & Eye-Catching Sliders To Lure Visitors  </li>
                                 <li>Double The POWER Of Your Campaigns with Smart Animation Effects  </li>
                                 <li>Integration With Over 20+ Channels  </li>
                                 <li>Safe, Secure & 100% GDPR Complaint  </li>
                                 <li>Packed With PROVEN Converting Promotional Templates  </li>
                                 <li>Done For You Lead Generation Templates  </li>
                                 <li>Easy-To-Use, Inbuilt Text & Inline Editor  </li>
                                 <li>100% SEO Friendly Website and Built-In Remarketing System  </li>
                                 <li>Complete Social Media Automation – People ONLY WANT TRENDY Topics These Days </li>
                                 <li>Automatically Translate Your Sites In 15+ Language According To Location  </li>
                                 <li>All-In-One Cloud-Based Trendy Site Builder  </li>
                                 <li>Complete Newbie Friendly - No Prior Experience and Technical Skills Needed  </li>
                                 <li>Use For Your Clients  </li>
                                 <li>Provide High in Demand Services  </li>
                                 <li>Make 5K-10K/M Extra with Commercial License </li>
                                 <li class="headline">BONUSES WORTH $2255 FREE!!!</li>
                                 <li>Limited Time Bonus #1 – LIVE Training</li>
                                 <li>Limited Time Bonus #2 – Oppyo - All in One Platform</li>
                                 <li>Super Value Bonus #3 – Done-For-You Setup Service</li>
                                 <li>Super Value Bonus #4 – Video Training to Monetizing Your Website</li>
                                 <li>Super Value Bonus #5 – Training to Start Affiliate Marketing</li>
                                 <li>Super Value Bonus #6 – Video Training on Viral Marketing</li>
                                 <li>Super Value Bonus #7 – Video Marketing Profit Kit</li>
                              </ul>
                           </div>
                           <div class="table-btn">
                              <div class="hideme-button">
                                 <a href="https://warriorplus.com/o2/buy/h5143v/y8157d/g63083"><img src="https://warriorplus.com/o2/btn/fn200011000/h5143v/y8157d/326061" class="img-fluid d-block mx-auto"></a> 
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Table Section End -->
      <!-- Guarantee Section Start -->
      <div class="guarantee-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-40 f-24 w500 lh150 white-clr text-center">
                  We’re Backing Everything Up with An Iron Clad...
                  <span class="w700 orange-clr f-md-45 f-28">"30-Day Risk-Free Money Back Guarantee"</span>
               </div>
            </div>
            <div class="row d-flex align-items-center flex-wrap mt20 mt-md60">
               <div class="col-md-8 col-12 order-md-2 offset-md-0">
                  <div class="f-md-20 f-18 lh150 w400 white-clr text-xs-center">
                     I'm 100% confident that Entertainerz will help you take your online business to the next level. I'm so confident on it that I'm making this offer a risk-free investment for you.<br><br> If you concluded that, HONESTLY nothing of this
                     has helped you in any way, you can take advantage of our "30-day Money Back Guarantee" and simply ask for a refund within 30 days!<br><br>
                     <span class="">Note:</span> For refund, we will require a valid reason along with the proof that you tried our system, but it didn't work for you!<br><br> I am considering your money to be kept safe on the table between us and
                     waiting for you to APPLY and successfully use this product, and get best results, so then you can feel it is a great investment.
                  </div>
               </div>
               <div class="col-md-4 order-md-1 col-12 mt20 mt-md0">
                  <img src="https://cdn.oppyo.com/launches/entertainerz/special/mbg.webp" class="img-fluid d-block mx-auto wh300">
               </div>
            </div>
         </div>
      </div>
      <!-- Guarantee Section End -->
      <!-- Awesome Section Start -->
      <div class="awesome-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-18 f-md-20 w400 lh150 black-clr text-xs-center">
                     <span class="w600">You can agree that the price we're asking is extremely low.</span> The price is rising with every few hours, so it won't be long until it's more than double what it is today!<br><br> We could easily charge hundreds of dollars for a revolutionary
                     tool like this, but we want to offer you an attractive and affordable price that will finally <span class="w600">help you build self-updating TRENDY websites - without wasting tons of money!</span> <br><br> <span class="w600">So, take action now... and I promise you won't be disappointed! </span>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md70">
                  <div class="row align-items-center">
                     <!-- CTA Btn Section Start -->
                     <div class="col-12 col-md-5">
                        <img src="https://cdn.oppyo.com/launches/entertainerz/special/limited-time-offer-banner1.webp" class="img-fluid d-block mx-auto">
                     </div>
                     <div class="col-12 col-md-7 mt30 mt-md0">
                        <div class="f-18 f-md-20 w500 lh150 text-center black-clr">
                           (Free Commercial License + Low 1-Time Price + 6 Special Bonuses worth $2255)
                        </div>
                        <div class="row">
                           <div class="col-12 mt20 mt-md20 text-center">
                              <a href="https://warriorplus.com/o2/buy/h5143v/y8157d/g63083" class="cta-link-btn">Get Instant Access To Entertainerz</a>
                           </div>
                        </div>
                        <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                           <img src="https://cdn.oppyo.com/launches/entertainerz/special/compaitable-with.webp" class="img-responsive mx-xs-center md-img-right">
                           <div class="d-md-block d-none px-md30"><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/v-line.webp" class="img-fluid "></div>
                           <img src="https://cdn.oppyo.com/launches/entertainerz/special/days-gurantee.webp" class="img-responsive mx-xs-auto mt15 mt-md0">
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 w600 f-md-28 f-22 text-start mt20 mt-md100">To your success</div>
               <div class="col-12  col-md-10 mx-auto mt20 mt-md50">
                  <div class="row">
                     <div class="col-md-6">
                        <img src="https://entertainerz.co/jv/assets/images/ayush-jain.webp" alt="Ayush Jain" class="mx-auto img-fluid d-block">
                        <img src="https://entertainerz.co/jv/assets/images/ayush-jain-bg.webp" alt="Ayush Jain" class="mx-auto img-fluid d-block mt15 mt-md20">
                        <div class="f-18 lh150 w600 text-center black-clr mt15">
                           (Entrepreneur &amp; Internet Marketer)
                        </div>
                     </div>
                     <div class="col-md-6 mt20 mt-md0">
                        <div class="">
                           <img src="https://entertainerz.co/jv/assets/images/pranshu-gupta.webp" alt="Pranshu Gupta" class="mx-auto img-fluid d-block">
                           <img src="https://entertainerz.co/jv/assets/images/pranshup-gupta-bg.webp" alt="Pranshu Gupta" class="mx-auto img-fluid d-block mt15 mt-md20">
                           <div class="f-18 lh150 w600 text-center black-clr mt15">
                              (Internet Marketer &amp; Product Creator)
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50">
                  <div class="f-18 f-md-20 w400 lh150 black-clr text-xs-center">
                     <span class="w600">P.S- You can try "Entertainerz" for 30 days without any worries.</span><br><br> Listen, we know there are a lot of crappy software tools out there that will get you nowhere. Most of the software is overpriced and an
                     absolute waste of money. So, if you're a bit sceptical, that's perfectly fine. I'm so sure you'll see the potential of this ground-breaking software that I'll let you try it out 100% risk-free.
                     <br><br> Just test it for 30 days and if you're not able to build jaw-dropping websites packed with TRENDY content & videos and we cannot help you in any way, you will be eligible for a refund - no tricks, no hassles. 
                     <br><br>
                     <span class="w600">P.S.S Don't Procrastinate - Get your copy of Entertainerz! </span><br><br> By now you should be really excited about all the wonderful benefits of such an amazing piece of software. You don't want to
                     miss out on the wonderful opportunity presented today... And then regret later when it costs more than double... or it's even completely off the market!
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Awesome Section End -->
      <!-- FAQ Section Start -->
      <div class="faq-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-45 f-28 w700 lh150 text-center black-clr">
                  Frequently Asked Questions
               </div>
               <div class="col-12 mt20 mt-md30">
                  <div class="row">
                     <div class="col-md-6 col-12">
                        <div class="faq-list">
                           <div class="f-md-22 f-20 w600 black-clr lh150">
                              What exactly Entertainerz is all about? 
                           </div>
                           <div class="f-18 w400 lh150 mt10 text-start">
                              Entertainerz is the ultimate push-button software that creates self-updating websites packed with awesome & TRENDY content, drives FREE viral traffic & makes handsfree commissions, ad profits & sales. 
                           </div>
                        </div>
                        <div class="faq-list">
                           <div class="f-md-22 f-20 w600 black-clr lh150">
                              Is my investment risk free?
                           </div>
                           <div class="f-18 w400 lh150 mt10 text-start">
                              Entertainerz is the ultimate push-button software that creates self-updating websites packed with awesome & TRENDY content, drives FREE viral traffic & makes handsfree commissions, ad profits & sales. 
                           </div>
                        </div>
                        <div class="faq-list">
                           <div class="f-md-22 f-20 w600 black-clr lh150">
                              Do I have to install Entertainerz?
                           </div>
                           <div class="f-18 w400 lh150 mt10 text-start">
                              NO! Entertainerz is fully cloud based. Just create an account and you can get started immediately online. It is 100% web-based platform hosted on the cloud. This means you never have to download anything ever. You can access it at any time from any device that
                              has an internet connection.
                           </div>
                        </div>
                        <div class="faq-list">
                           <div class="f-md-22 f-20 w600 black-clr lh150">
                              Is it ‘Newbie Friendly’?
                           </div>
                           <div class="f-18 w400 lh150 mt10 text-start">
                              Yep, my friend, Entertainerz is 100% newbie friendly. We know that there are a lot of technical hassles that most software has, but our software is a cut above the rest, and everyone can use it with complete ease.
                           </div>
                        </div>
                        <div class="faq-list">
                           <div class="f-md-22 f-20 w600 black-clr lh150">
                              Do you charge any monthly fees?
                           </div>
                           <div class="f-18 w400 lh150 mt10 text-start">
                              NOT AT ALL. There are NO monthly fees to use Entertainerz during the launch period. During this period, you pay once and never again. We always believe in providing complete value for your money.
                           </div>
                        </div>
                     </div>
                     <div class="col-md-6 col-12">
                        <div class="faq-list">
                           <div class="f-md-22 f-20 w600 black-clr lh150">
                              Is Entertainerz easy to use?
                           </div>
                           <div class="f-18 w400 lh150 mt10 text-start">
                              I bet you it’s the easiest tool you might have seen so far. Our #1 priority during the development of the software was to make it simple and easy for everyone. There is nothing to install, just create your account and login to take a plunge into hugely
                              profitable video marketing arena.
                           </div>
                        </div>
                        <div class="faq-list">
                           <div class="f-md-22 f-20 w600 black-clr lh150">
                              How do I know how well my campaigns are working?
                           </div>
                           <div class="f-18 w400 lh150 mt10 text-start">
                              You can see real-time reports for your campaigns in your account dashboard and make changes accordingly. But there’s a small catch, you need to upgrade your purchase to use these benefits.
                           </div>
                        </div>
                        <div class="faq-list">
                           <div class="f-md-22 f-20 w600 black-clr lh150">
                              Will I able to drive hordes of viral traffic from day one?
                           </div>
                           <div class="f-18 w400 lh150 mt10 text-start">
                              Well, that depends on how well you make the use of this ultimate software. We’ve created this from grounds up to make everything simple and easy and ensure that you move ahead without any hassles.
                           </div>
                        </div>
                        <div class="faq-list">
                           <div class="f-md-22 f-20 w600 black-clr lh150">
                              Will I get any training or support?
                           </div>
                           <div class="f-18 w400 lh150 mt10 text-start">
                              YES. We made detailed and step-by-step training videos that show you every step of how to get setup and you can access them in the member’s area.
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50">
                  <div class="f-22 f-md-28 w600 black-clr px0 text-xs-center lh150">If you have any query, simply reach to us at <a href="mailto:support@bizomart.com" class="blue-clr">support@bizomart.com</a></div>
               </div>
               <div class="col-12 mt20 mt-md70">
                  <div class="row align-items-center">
                     <!-- CTA Btn Section Start -->
                     <div class="col-12 col-md-5">
                        <img src="https://cdn.oppyo.com/launches/entertainerz/special/limited-time-offer-banner1.webp" class="img-fluid d-block mx-auto">
                     </div>
                     <div class="col-12 col-md-7 mt30 mt-md0">
                        <div class="f-18 f-md-20 w500 lh150 text-center black-clr">
                           (Free Commercial License + Low 1-Time Price + 6 Special Bonuses worth $2255)
                        </div>
                        <div class="row">
                           <div class="col-12 mt20 mt-md20 text-center">
                              <a href="https://warriorplus.com/o2/buy/h5143v/y8157d/g63083" class="cta-link-btn">Get Instant Access To Entertainerz</a>
                           </div>
                        </div>
                        <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                           <img src="https://cdn.oppyo.com/launches/entertainerz/special/compaitable-with.webp" class="img-responsive mx-xs-center md-img-right">
                           <div class="d-md-block d-none px-md30"><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/v-line.webp" class="img-fluid "></div>
                           <img src="https://cdn.oppyo.com/launches/entertainerz/special/days-gurantee.webp" class="img-responsive mx-xs-auto mt15 mt-md0">
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- FAQ Section End -->
      <div class="offer-new">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="offer">
                     <div><img src="https://cdn.oppyo.com/launches/appzilo/special/error.webp"></div>
                     <div class="f-18 w400 white-clr lh140"><b>FAIR WARNING! After Every Hour The Price Goes Up,</b> So Act Now or Come Later & Pay More</div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="footer-section">
         <div class="container ">
            <div class="row">
               <div class="col-12 text-center">
                  <svg id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 189.77 50" style="max-height:80px">
                     <defs>
                        <style>.cls-1{fill:url(#linear-gradient-2);}.cls-2{fill:#fff;}.cls-3{fill:#001730;}.cls-4{fill:url(#linear-gradient-3);}.cls-5{fill:url(#linear-gradient);}</style>
                        <linearGradient id="linear-gradient" x1="20.65" y1="26.83" x2="42.69" y2="26.83" gradientUnits="userSpaceOnUse">
                           <stop offset="0" stop-color="#bf003b"/>
                           <stop offset="1" stop-color="#e23940"/>
                        </linearGradient>
                        <linearGradient id="linear-gradient-2" x1=".12" y1="29.26" x2="25.92" y2="29.26" gradientUnits="userSpaceOnUse">
                           <stop offset="0" stop-color="#f38227"/>
                           <stop offset="1" stop-color="#fbb826"/>
                        </linearGradient>
                        <linearGradient id="linear-gradient-3" x1="3.09" y1="36.12" x2="22.33" y2="28.06" gradientUnits="userSpaceOnUse">
                           <stop offset="0" stop-color="#3369e9"/>
                           <stop offset="1" stop-color="#002b7f"/>
                        </linearGradient>
                     </defs>
                     <path class="cls-2" d="M22.29,0c-.94,0-1.69,.76-1.69,1.69,0,.75,.48,1.39,1.16,1.61l-2.95,10.01-.61,2.09,2.13,.26,.41-2.75v-.03s1.44-9.5,1.44-9.5c.03,0,.07,0,.11,0,.93,0,1.69-.76,1.69-1.7s-.76-1.69-1.69-1.69Z"/>
                     <path class="cls-2" d="M16.2,15.76l-1.74,.96-5.3-6.31c-.29,.23-.64,.36-1.03,.36-.94,0-1.7-.75-1.7-1.69s.76-1.69,1.7-1.69,1.69,.76,1.69,1.69c0,.41-.15,.79-.4,1.08l6.78,5.6Z"/>
                     <g>
                        <path class="cls-2" d="M63.68,34.98c0,.14-.04,.35-.11,.62-.08,.34-.13,.55-.15,.62-.03,.32-.09,.79-.19,1.41-.05,.16-.16,.24-.35,.24-.09,0-.26-.02-.51-.05-1.1-.16-2.82-.24-5.13-.24-.66,0-1.64,.02-2.96,.05s-2.31,.05-2.96,.05c-.3,0-.44-.13-.44-.39,0-1.01,.06-2.52,.17-4.53,.11-2.02,.17-3.53,.17-4.53s-.05-2.93-.16-5.72c-.02-.72-.07-1.79-.15-3.22v-.19c-.02-.21,.09-.31,.34-.31,.63,0,1.57,.02,2.83,.05,1.26,.04,2.2,.05,2.83,.05s1.5-.02,2.7-.05c1.2-.04,2.1-.05,2.7-.05,.42,0,.65,.13,.69,.39,.19,1.19,.28,1.92,.28,2.21,0,.21-.1,.31-.3,.31-.76,0-1.92-.04-3.47-.13-1.55-.09-2.71-.13-3.5-.13-.74,0-1.2,.1-1.39,.3-.19,.2-.29,.69-.29,1.47v2.01c0,.55,.01,.87,.04,.96,.08,.28,.31,.42,.7,.42,.11,0,.26,0,.46-.01h.46c1.76,0,3.34-.03,4.74-.09,.55-.02,1.19-.06,1.94-.13,.19-.02,.28,.06,.28,.24,0,.29-.03,.73-.1,1.32-.07,.59-.1,1.04-.1,1.33,0,.16-.13,.24-.4,.24-.11,0-.51-.03-1.2-.09-.5-.05-1.63-.08-3.39-.08-1.48,0-2.42,.02-2.82,.05-.36,.03-.57,.22-.62,.57,0-.06,.01,.28,.01,1.04v1.99c0,.93,.14,1.52,.43,1.75,.22,.19,.79,.28,1.68,.28h1.48c.63-.01,2.39-.12,5.29-.34h.2c.18-.03,.27,.08,.27,.32Z"/>
                        <path class="cls-2" d="M77.61,37.42c0,.2-.09,.3-.27,.3-.3,0-.74,0-1.33-.01-.59,0-1.03-.01-1.33-.01-.16,0-.24-.13-.24-.39,0-.44,.01-1.11,.04-2.01,.03-.9,.04-1.57,.04-2.02,0-.36,0-.9-.01-1.61,0-.71-.01-1.25-.01-1.61,0-1.02-.15-1.75-.44-2.17-.36-.5-1.02-.75-1.99-.75-.45,0-1.07,.25-1.87,.75-.84,.53-1.27,1-1.27,1.41v8.08c0,.22-.09,.34-.26,.34-.29,0-.72,0-1.29-.01-.57,0-1.01-.01-1.29-.01-.19,0-.28-.1-.28-.31,0-.71,.01-1.77,.03-3.19,.02-1.42,.03-2.49,.03-3.21,0-1.91-.2-3.68-.61-5.29-.03-.08-.04-.14-.04-.18,0-.09,.06-.15,.18-.19,.06,0,.58-.09,1.54-.25,.97-.16,1.47-.24,1.52-.24,.05,0,.09,.07,.11,.22,.04,.56,.15,1.12,.31,1.68,.47-.37,1.08-.83,1.85-1.37,.94-.57,1.85-.86,2.73-.86,1.69,0,2.84,.49,3.46,1.47,.46,.72,.69,1.88,.69,3.49,0,.28,0,.7-.02,1.28-.01,.57-.02,1.01-.02,1.29,0,.6,0,1.5,.03,2.69,.02,1.19,.03,2.09,.03,2.69Z"/>
                        <path class="cls-2" d="M87.67,35.21c0,.2-.02,.47-.05,.81-.05,.42-.07,.7-.08,.82,0,.16-.02,.26-.04,.3-.04,.06-.12,.13-.26,.19-.74,.35-1.89,.53-3.48,.53-2.1,0-3.15-.95-3.15-2.86,0-.76,.02-1.91,.05-3.44,.04-1.53,.05-2.68,.05-3.44,0-.37-.18-.57-.55-.59-.33,0-.66,0-.98-.01-.19-.04-.28-.41-.28-1.1,0-.26,.02-.53,.05-.82,.03-.22,.17-.35,.43-.39,.22,0,.44,0,.66-.01,.41-.02,.62-.2,.62-.55s-.01-.91-.03-1.64c-.02-.74-.03-1.29-.03-1.66,0-.66,.13-.98,.38-.98,.08,0,.92,.14,2.53,.42,.21,.03,.31,.13,.31,.3,0,.4-.03,.99-.09,1.78-.06,.79-.09,1.38-.09,1.78,0,.26,.12,.39,.36,.39h3.23c.15,0,.23,.05,.23,.16s-.02,.28-.05,.52c-.03,.24-.05,.42-.05,.53,0,.13,0,.34,.01,.62,0,.28,.01,.48,.01,.62,0,.17-.13,.26-.38,.26-.36,0-.9-.02-1.63-.07-.73-.05-1.27-.07-1.63-.07-.07,0-.13,.26-.16,.77-.05,.64-.07,1.53-.07,2.68v2.01c0,.77,.07,1.31,.22,1.62,.22,.47,.66,.7,1.33,.7,.28,0,.69-.06,1.23-.18s.94-.18,1.2-.18c.11,0,.16,.08,.16,.24Z"/>
                        <path class="cls-2" d="M101.01,30.46c0,.66-.29,1.11-.86,1.33-.39,.14-3.17,.41-8.34,.79,.12,.79,.55,1.46,1.31,2.02,.72,.53,1.49,.79,2.32,.79,1.42,0,2.72-.46,3.91-1.37,.14-.12,.29-.23,.43-.35,.07,0,.12,0,.14,.03,.02,.02,.19,.33,.51,.94,.31,.61,.47,.93,.47,.98,0,.05-.07,.16-.22,.31-1.32,1.4-3.01,2.1-5.08,2.1s-3.74-.62-4.98-1.87c-1.24-1.24-1.86-2.9-1.86-4.98,0-1.85,.59-3.46,1.78-4.84,1.25-1.43,2.78-2.14,4.59-2.14,1.69,0,3.1,.62,4.23,1.86,1.1,1.2,1.66,2.67,1.66,4.39Zm-3.21-.82c0-.75-.29-1.41-.88-1.98s-1.25-.86-1.99-.86c-.83,0-1.56,.33-2.2,1-.64,.66-.96,1.41-.96,2.24,0,.19,.16,.28,.48,.28,1.33,0,2.92-.13,4.77-.4,.52-.07,.78-.16,.78-.27Z"/>
                        <path class="cls-2" d="M110.6,25.04c0,.99-.07,1.89-.2,2.69-.03,.21-.12,.31-.27,.31-.19,0-.47-.02-.86-.06-.38-.04-.67-.06-.87-.06-1.13,0-1.85,.32-2.17,.96-.19,.39-.28,1.19-.28,2.41v1.48c0,.5,.02,1.27,.06,2.29,.04,1.02,.06,1.79,.06,2.29,0,.24-.1,.36-.31,.36-.28,0-.7,0-1.27-.01-.57,0-.99-.01-1.27-.01-.21,0-.31-.11-.31-.34,0-.73,.01-1.82,.03-3.29,.02-1.46,.03-2.56,.03-3.3,0-2.22-.21-4-.62-5.35-.02-.07-.03-.13-.03-.16,0-.11,.06-.18,.18-.23,.33-.04,.8-.09,1.4-.16,.97-.18,1.42-.27,1.35-.27,.14,0,.24,.26,.28,.79,.04,.53,.13,.79,.24,.79,.02,0,.04,0,.07-.03,.37-.23,.74-.47,1.1-.7,.4-.24,.82-.42,1.24-.54,.35-.11,.8-.16,1.35-.16,.7,0,1.05,.09,1.05,.28Z"/>
                        <path class="cls-2" d="M119.58,35.21c0,.2-.02,.47-.05,.81-.05,.42-.07,.7-.08,.82,0,.16-.02,.26-.04,.3-.04,.06-.12,.13-.26,.19-.74,.35-1.89,.53-3.48,.53-2.1,0-3.15-.95-3.15-2.86,0-.76,.02-1.91,.05-3.44,.04-1.53,.05-2.68,.05-3.44,0-.37-.18-.57-.55-.59-.33,0-.66,0-.98-.01-.19-.04-.28-.41-.28-1.1,0-.26,.02-.53,.05-.82,.03-.22,.17-.35,.43-.39,.22,0,.44,0,.66-.01,.41-.02,.62-.2,.62-.55s-.01-.91-.03-1.64c-.02-.74-.03-1.29-.03-1.66,0-.66,.13-.98,.38-.98,.08,0,.92,.14,2.53,.42,.21,.03,.31,.13,.31,.3,0,.4-.03,.99-.09,1.78-.06,.79-.09,1.38-.09,1.78,0,.26,.12,.39,.36,.39h3.23c.15,0,.23,.05,.23,.16s-.02,.28-.05,.52c-.03,.24-.05,.42-.05,.53,0,.13,0,.34,.01,.62,0,.28,.01,.48,.01,.62,0,.17-.13,.26-.38,.26-.36,0-.9-.02-1.63-.07-.73-.05-1.27-.07-1.63-.07-.07,0-.13,.26-.16,.77-.05,.64-.07,1.53-.07,2.68v2.01c0,.77,.07,1.31,.22,1.62,.22,.47,.66,.7,1.33,.7,.28,0,.69-.06,1.23-.18s.94-.18,1.2-.18c.11,0,.16,.08,.16,.24Z"/>
                        <path class="cls-2" d="M134.6,25.5s-.01,.12-.04,.24c-.35,2.06-.53,3.83-.53,5.32,0,.09,.11,2.11,.34,6.05v.19c.02,.26-.1,.39-.34,.39s-.62,.03-1.12,.09c-.5,.06-.87,.09-1.11,.09-.17,0-.3-.31-.39-.94-.09-.63-.18-.94-.26-.94-.05,0-.3,.18-.73,.53-.53,.43-1.02,.76-1.48,.98-.71,.36-1.42,.54-2.13,.54-1.74,0-3.21-.7-4.4-2.09-1.13-1.33-1.7-2.88-1.7-4.65,0-1.99,.57-3.64,1.71-4.94,1.19-1.36,2.77-2.03,4.71-2.03,1.37,0,2.57,.48,3.6,1.43,.15,.18,.39,.44,.71,.78,.03,.03,.05,.04,.08,.04,.05,0,.13-.28,.23-.85,.1-.57,.22-.85,.38-.85,.24,0,.7,.07,1.36,.2,.74,.16,1.1,.31,1.1,.43Zm-3.43,5.63c0-1.1-.33-2.05-.98-2.84-.69-.85-1.58-1.28-2.65-1.28s-1.98,.42-2.69,1.27c-.69,.81-1.04,1.76-1.04,2.86s.35,2.03,1.04,2.83c.72,.84,1.62,1.25,2.69,1.25s1.93-.43,2.64-1.28c.66-.8,1-1.73,1-2.8Z"/>
                        <path class="cls-2" d="M140.3,21.58c0,1.1-.62,1.66-1.85,1.66s-1.87-.55-1.87-1.66c0-.49,.19-.9,.58-1.23,.36-.31,.79-.46,1.29-.46s.94,.16,1.3,.47c.36,.31,.55,.72,.55,1.21Zm-.09,3.65c0,.69-.04,1.74-.13,3.13-.09,1.4-.13,2.44-.13,3.15,0,.66,.01,1.64,.04,2.94,.03,1.31,.04,2.28,.04,2.93,0,.19-.08,.28-.24,.28h-2.63c-.19,0-.28-.18-.28-.55,0-.62,.02-1.55,.05-2.8,.03-1.24,.05-2.18,.05-2.81,0-.7-.05-1.75-.16-3.15s-.15-2.45-.15-3.15c0-.15,.09-.23,.28-.23,.16,0,.41,.02,.75,.07,.34,.04,.59,.07,.76,.07s.42-.02,.76-.07c.34-.04,.59-.07,.76-.07,.15,0,.23,.09,.23,.26Z"/>
                        <path class="cls-2" d="M154.74,37.42c0,.2-.09,.3-.27,.3-.3,0-.74,0-1.33-.01-.59,0-1.03-.01-1.33-.01-.16,0-.24-.13-.24-.39,0-.44,.01-1.11,.04-2.01,.03-.9,.04-1.57,.04-2.02,0-.36,0-.9-.01-1.61,0-.71-.01-1.25-.01-1.61,0-1.02-.15-1.75-.44-2.17-.36-.5-1.02-.75-1.99-.75-.45,0-1.07,.25-1.87,.75-.84,.53-1.27,1-1.27,1.41v8.08c0,.22-.09,.34-.26,.34-.29,0-.72,0-1.29-.01-.57,0-1.01-.01-1.29-.01-.19,0-.28-.1-.28-.31,0-.71,.01-1.77,.03-3.19,.02-1.42,.03-2.49,.03-3.21,0-1.91-.2-3.68-.61-5.29-.03-.08-.04-.14-.04-.18,0-.09,.06-.15,.18-.19,.06,0,.58-.09,1.54-.25,.97-.16,1.47-.24,1.52-.24,.05,0,.09,.07,.11,.22,.04,.56,.15,1.12,.31,1.68,.47-.37,1.08-.83,1.85-1.37,.94-.57,1.85-.86,2.73-.86,1.69,0,2.84,.49,3.46,1.47,.46,.72,.69,1.88,.69,3.49,0,.28,0,.7-.02,1.28-.01,.57-.02,1.01-.02,1.29,0,.6,0,1.5,.03,2.69,.02,1.19,.03,2.09,.03,2.69Z"/>
                        <path class="cls-2" d="M168.7,30.46c0,.66-.29,1.11-.86,1.33-.39,.14-3.17,.41-8.34,.79,.12,.79,.55,1.46,1.31,2.02,.72,.53,1.49,.79,2.32,.79,1.42,0,2.72-.46,3.91-1.37,.14-.12,.29-.23,.43-.35,.07,0,.12,0,.14,.03,.02,.02,.19,.33,.51,.94,.31,.61,.47,.93,.47,.98,0,.05-.07,.16-.22,.31-1.32,1.4-3.01,2.1-5.08,2.1s-3.74-.62-4.98-1.87c-1.24-1.24-1.86-2.9-1.86-4.98,0-1.85,.59-3.46,1.78-4.84,1.25-1.43,2.78-2.14,4.59-2.14,1.69,0,3.1,.62,4.23,1.86,1.1,1.2,1.66,2.67,1.66,4.39Zm-3.21-.82c0-.75-.29-1.41-.88-1.98s-1.25-.86-1.99-.86c-.83,0-1.56,.33-2.2,1-.64,.66-.96,1.41-.96,2.24,0,.19,.16,.28,.48,.28,1.33,0,2.92-.13,4.77-.4,.52-.07,.78-.16,.78-.27Z"/>
                        <path class="cls-2" d="M178.29,25.04c0,.99-.07,1.89-.2,2.69-.03,.21-.12,.31-.27,.31-.19,0-.47-.02-.86-.06-.38-.04-.67-.06-.87-.06-1.13,0-1.85,.32-2.17,.96-.19,.39-.28,1.19-.28,2.41v1.48c0,.5,.02,1.27,.06,2.29s.06,1.79,.06,2.29c0,.24-.1,.36-.31,.36-.28,0-.7,0-1.27-.01-.57,0-.99-.01-1.27-.01-.21,0-.31-.11-.31-.34,0-.73,.01-1.82,.03-3.29,.02-1.46,.03-2.56,.03-3.3,0-2.22-.21-4-.62-5.35-.02-.07-.03-.13-.03-.16,0-.11,.06-.18,.18-.23,.33-.04,.8-.09,1.4-.16,.97-.18,1.42-.27,1.35-.27,.14,0,.24,.26,.28,.79,.04,.53,.13,.79,.24,.79,.02,0,.04,0,.07-.03,.37-.23,.74-.47,1.1-.7,.4-.24,.82-.42,1.24-.54,.35-.11,.8-.16,1.35-.16,.7,0,1.05,.09,1.05,.28Z"/>
                        <path class="cls-2" d="M189.77,35.13s-.07,.36-.2,.96c-.14,.64-.22,1.03-.24,1.19-.04,.31-.12,.47-.24,.47-.31,0-.74-.02-1.27-.05-.6-.04-1.02-.07-1.25-.08-.6-.02-1.46-.03-2.59-.03-.87,0-1.84,.04-2.9,.11-1.33,.09-1.9,.13-1.71,.13-.19,0-.31-.08-.36-.24-.13-.43-.19-.93-.19-1.51,0-.08,0-.2,.01-.34s.01-.26,.01-.33c0-.13,.1-.28,.29-.45,.19-.18,1.15-1.19,2.88-3.05,.87-.91,2.11-2.3,3.7-4.19h-5.94c-.14,0-.22-.09-.22-.28,0-.26,.06-.65,.17-1.17,.11-.52,.17-.9,.17-1.15,0-.11,.13-.16,.38-.16,.5,0,1.25,.02,2.25,.07s1.75,.07,2.25,.07c2.46,0,3.85,.02,4.16,.05,.22,.03,.34,.12,.34,.28,0,1.36-.05,2.12-.16,2.29-.09,.13-.87,1.01-2.33,2.65-.94,1.07-2.37,2.66-4.27,4.77,2.99-.08,5.31-.12,6.95-.12,.21,0,.31,.04,.31,.12Z"/>
                     </g>
                     <path class="cls-2" d="M16.61,49.17c.96-1.64,6.11-6.32,13.48-6.77,2.78-.17,5.12,.34,6.8,.88,0,0,.84,.29,.15,.94s-11.41-1.5-19.36,5.17c-1.6,1.37-1.2,.01-1.07-.22Z"/>
                     <path class="cls-5" d="M42.69,31.94c-.01,2.52-.67,4.93-2.77,6.32-1.62,1.06-3.89,2.45-6.95,3.79,.63-1.27,.93-2.75,.99-4.28,.01-.21,.01-.43,.01-.64,.01-9.53-3.63-17.69-6.99-21.14-.16-.16-.32-.32-.5-.47-1.51-1.33-3.74-2.37-5.74-2.61-.03,0-.06-.01-.09-.01,.03-.01,.07-.01,.1-.02,3.12-.66,6.47-1.1,9.93-1.26,2.07-.09,4.7,1.12,6.16,2.62,2.81,2.89,5.86,9.72,5.85,17.7Z"/>
                     <path class="cls-1" d="M33.97,37.13c0,.21,0,.43-.01,.64-.06,1.53-.36,3.01-.99,4.28-.27,.12-.54,.23-.82,.35-3.01,1.24-6.73,2.41-11.29,3.21v-.21c0-.47-.01-.95-.03-1.41-.02-.5-.04-.99-.07-1.48-.01-.04-.01-.07-.01-.11-.03-.46-.07-.91-.12-1.36-.02-.25-.05-.49-.08-.74-.08-.69-.17-1.36-.28-2.03-.07-.45-.15-.89-.23-1.32-.01-.08-.03-.17-.05-.25-.1-.54-.22-1.08-.34-1.6-.03-.13-.06-.26-.09-.39-.11-.44-.22-.87-.34-1.3-.11-.44-.24-.87-.37-1.29-.06-.2-.12-.4-.19-.6-.14-.44-.28-.88-.44-1.3-.18-.51-.37-1.01-.56-1.5-.25-.62-.51-1.22-.77-1.8-.6-1.29-1.23-2.47-1.88-3.53-.12-.2-.25-.4-.38-.59-.12-.2-.25-.39-.38-.57-.63-.92-1.28-1.72-1.92-2.38-.16-.16-.33-.33-.51-.48-.02-.03-.05-.05-.08-.08-.06-.05-.12-.1-.18-.15-.45-.38-.95-.74-1.48-1.07-.14-.09-.28-.17-.42-.25-.48-.28-.98-.53-1.49-.75,1.46-.71,3.09-1.4,4.87-2.04,.53-.18,1.07-.37,1.62-.54,1.32-.43,2.71-.82,4.15-1.18,.6-.14,1.22-.28,1.84-.41,.03,0,.06,.01,.09,.01,2,.24,4.23,1.28,5.74,2.61,.18,.15,.34,.31,.5,.47,3.36,3.45,7,11.61,6.99,21.14Z"/>
                     <path class="cls-4" d="M20.86,45.61c-3.31,.58-7.05,.95-11.28,1.01-.83,.01-1.62-.36-2.33-.79-2.42-1.46-5.57-7.74-7.18-18.65-.38-2.58,.81-5,2.76-6.72,1.27-1.12,3.09-2.28,5.33-3.39,1.36,.59,2.64,1.4,3.65,2.3,.18,.16,.34,.32,.5,.48,3.71,3.82,7.71,12.34,8.42,22.55,.08,1.05,.11,2.12,.11,3.21Z"/>
                     <path class="cls-2" d="M6.78,22.31c3.46-3.05,12.1-6.51,21.9-6.95,1.63-.07,3.7,.88,4.84,2.06,2.21,2.28,4.61,7.65,4.6,13.93,0,1.98-.64,3.72-2.18,4.97-3.42,2.78-10.62,7.14-23.86,6.57-.65-.03-1.28-.28-1.84-.62-1.9-1.15-4.38-6.09-5.65-14.67-.3-2.03,.64-3.93,2.17-5.29Z"/>
                     <path class="cls-3" d="M26.11,26.61l-5.61-1.56c-1.31-.36-2.85,.26-3.44,1.38-.23,.43-.29,.9-.17,1.33l1.51,5.69c.3,1.12,1.67,1.73,3.08,1.36,.54-.14,1.04-.42,1.4-.8l4.1-4.13c.96-.96,.86-2.3-.21-2.99-.2-.13-.42-.22-.66-.29h0Z"/>
                  </svg>
                  <div class="f-14 f-md-16 w400 mt20 lh150 white-clr text-start" style="color:#7d8398;"><span class="w600">Note:</span> This site is not a part of the Facebook website or Facebook Inc. Additionally, this site is NOT endorsed by Facebook in any way. FACEBOOK & INSTAGRAM are the trademarks of FACEBOOK, Inc.<br><br>
                     <span class="w600">Earning Disclaimer:</span> Please make your purchase decision wisely. There is no promise or representation that you will make a certain amount of money, or any money, or not lose money, as a result of using
                     our products and services. Any stats, conversion, earnings, or income statements are strictly estimates. There is no guarantee that you will make these levels for yourself. As with any business, your results will vary and will
                     be based on your personal abilities, experience, knowledge, capabilities, level of desire, and an infinite number of variables beyond our control, including variables we or you have not anticipated. There are no guarantees concerning
                     the level of success you may experience. Each person’s results will vary. There are unknown risks in any business, particularly with the Internet where advances and changes can happen quickly. The use of the plug-in and the given
                     information should be based on your own due diligence, and you agree that we are not liable for your success or failure.
                  </div>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-14 f-md-16 w400 lh150 white-clr text-xs-center">Copyright © Entertainerz 2022</div>
                  <ul class="footer-ul w400 f-14 f-md-16 white-clr text-center text-md-right">
                     <li><a href="https://support.bizomart.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://entertainerz.co/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://entertainerz.co/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://entertainerz.co/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://entertainerz.co/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://entertainerz.co/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://entertainerz.co/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!-- Exit Popup and Timer -->
      <?php include('timer.php'); ?>	  
      <!-- Exit Popup and Timer End -->
   </body>
</html>
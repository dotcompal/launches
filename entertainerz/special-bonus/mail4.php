<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <!-- Device & IE Compatibility Meta -->
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=9">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=9">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
      <meta name="title" content="Entertainerz Special Bonuses">
      <meta name="description" content="Entertainerz Special Bonuses">
      <meta name="keywords" content="Entertainerz Special Bonuses">
      <meta property="og:image" content="https://www.entertainerz.co/special-bonus/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Pranshu Gupta">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="Entertainerz Special Bonuses">
      <meta property="og:description" content="Entertainerz Special Bonuses">
      <meta property="og:image" content="https://www.entertainerz.co/special-bonus/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="Entertainerz Special Bonuses">
      <meta property="twitter:description" content="Entertainerz Special Bonuses">
      <meta property="twitter:image" content="https://www.entertainerz.co/special-bonus/thumbnail.png">
      <title>Entertainerz  Bonuses</title>
      <!-- Shortcut Icon  -->
      <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
      <!-- Css CDN Load Link -->
      <link rel="stylesheet" href="assets/css/font-awesome.min.css" type="text/css" />
      <link rel="stylesheet" href="../common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" href="../common_assets/css/general.css">
      <link rel="stylesheet" href="assets/css/timer.css" type="text/css" />
      <link rel="stylesheet" type="text/css" href="assets/css/bonus-style.css">
      <link rel="stylesheet" type="text/css" href="assets/css/style.css">
      <link rel="stylesheet" type="text/css" href="assets/css/bonus.css">
      <link rel="stylesheet" href="assets/css/style-feature.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style-bottom.css" type="text/css">
      <!-- Font Family CDN Load Links -->
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Spartan:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
      <!-- Javascript File Load -->
      <script src="../common_assets/js/jquery.min.js"></script>
      <script src="../common_assets/js/popper.min.js"></script>
      <script src="../common_assets/js/bootstrap.min.js"></script>   
      <!-- Buy Button Lazy load Script -->
      <script>
         $(document).ready(function() {
          /* Every time the window is scrolled ... */
          $(window).scroll(function() {
          /* Check the location of each desired element */
          $('.hideme').each(function(i) {
         	 var bottom_of_object = $(this).offset().top + $(this).outerHeight();
         	 var bottom_of_window = $(window).scrollTop() + $(window).height();
         	 /* If the object is completely visible in the window, fade it it */
         	 if ((bottom_of_window - bottom_of_object) > -200) {
         		 $(this).animate({
         			 'opacity': '1'
         		 }, 300);
         	 }
          });
          });
         });
      </script>
      <!-- Smooth Scrolling Script -->
      <script>
         $(document).ready(function() {
          // Add smooth scrolling to all links
          $("a").on('click', function(event) {
         
          // Make sure this.hash has a value before overriding default behavior
          if (this.hash !== "") {
         	 // Prevent default anchor click behavior
         	 event.preventDefault();
         
         	 // Store hash
         	 var hash = this.hash;
         
         	 // Using jQuery's animate() method to add smooth page scroll
         	 // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
         	 $('html, body').animate({
         		 scrollTop: $(hash).offset().top
         	 }, 800, function() {
         
         		 // Add hash (#) to URL when done scrolling (default click behavior)
         		 window.location.hash = hash;
         	 });
          } // End if
          });
         });
      </script>
   </head>
   <body>
      <!-- New Timer  Start-->
      <?php
         $date = 'November 2 2022 11:59 PM EST';
         $exp_date = strtotime($date);
         $now = time();  
         /*
         
         $date = date('F d Y g:i:s A eO');
         $rand_time_add = rand(700, 1200);
         $exp_date = strtotime($date) + $rand_time_add;
         $now = time();*/
         
         if ($now < $exp_date) {
         ?>
      <?php
         } else {
          echo "Times Up";
         }
         ?>
      <!-- New Timer End -->
      <?php
         if(!isset($_GET['afflink'])){
         $_GET['afflink'] = 'https://warriorplus.com/o2/a/xn4w8p/0';
         $_GET['name'] = 'Pranshu Gupta';      
         }
         ?>
      <div class="main-header">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <!-- <img src="assets/images/demo-vid1.png" class="img-fluid d-block mx-auto"> -->
                  <div class="f-md-32 f-20 lh160 w400 text-center white-clr">
                     <span class="w600"><?php echo $_GET['name'];?>'s</span> special bonus for &nbsp; 	 
                     <svg id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 189.77 50" style="max-height:100px">
                        <defs>
                           <style>.cls-1{fill:url(#linear-gradient-2);}.cls-2{fill:#fff;}.cls-3{fill:#001730;}.cls-4{fill:url(#linear-gradient-3);}.cls-5{fill:url(#linear-gradient);}</style>
                           <linearGradient id="linear-gradient" x1="20.65" y1="26.83" x2="42.69" y2="26.83" gradientUnits="userSpaceOnUse">
                              <stop offset="0" stop-color="#bf003b"/>
                              <stop offset="1" stop-color="#e23940"/>
                           </linearGradient>
                           <linearGradient id="linear-gradient-2" x1=".12" y1="29.26" x2="25.92" y2="29.26" gradientUnits="userSpaceOnUse">
                              <stop offset="0" stop-color="#f38227"/>
                              <stop offset="1" stop-color="#fbb826"/>
                           </linearGradient>
                           <linearGradient id="linear-gradient-3" x1="3.09" y1="36.12" x2="22.33" y2="28.06" gradientUnits="userSpaceOnUse">
                              <stop offset="0" stop-color="#3369e9"/>
                              <stop offset="1" stop-color="#002b7f"/>
                           </linearGradient>
                        </defs>
                        <path class="cls-2" d="M22.29,0c-.94,0-1.69,.76-1.69,1.69,0,.75,.48,1.39,1.16,1.61l-2.95,10.01-.61,2.09,2.13,.26,.41-2.75v-.03s1.44-9.5,1.44-9.5c.03,0,.07,0,.11,0,.93,0,1.69-.76,1.69-1.7s-.76-1.69-1.69-1.69Z"/>
                        <path class="cls-2" d="M16.2,15.76l-1.74,.96-5.3-6.31c-.29,.23-.64,.36-1.03,.36-.94,0-1.7-.75-1.7-1.69s.76-1.69,1.7-1.69,1.69,.76,1.69,1.69c0,.41-.15,.79-.4,1.08l6.78,5.6Z"/>
                        <g>
                           <path class="cls-2" d="M63.68,34.98c0,.14-.04,.35-.11,.62-.08,.34-.13,.55-.15,.62-.03,.32-.09,.79-.19,1.41-.05,.16-.16,.24-.35,.24-.09,0-.26-.02-.51-.05-1.1-.16-2.82-.24-5.13-.24-.66,0-1.64,.02-2.96,.05s-2.31,.05-2.96,.05c-.3,0-.44-.13-.44-.39,0-1.01,.06-2.52,.17-4.53,.11-2.02,.17-3.53,.17-4.53s-.05-2.93-.16-5.72c-.02-.72-.07-1.79-.15-3.22v-.19c-.02-.21,.09-.31,.34-.31,.63,0,1.57,.02,2.83,.05,1.26,.04,2.2,.05,2.83,.05s1.5-.02,2.7-.05c1.2-.04,2.1-.05,2.7-.05,.42,0,.65,.13,.69,.39,.19,1.19,.28,1.92,.28,2.21,0,.21-.1,.31-.3,.31-.76,0-1.92-.04-3.47-.13-1.55-.09-2.71-.13-3.5-.13-.74,0-1.2,.1-1.39,.3-.19,.2-.29,.69-.29,1.47v2.01c0,.55,.01,.87,.04,.96,.08,.28,.31,.42,.7,.42,.11,0,.26,0,.46-.01h.46c1.76,0,3.34-.03,4.74-.09,.55-.02,1.19-.06,1.94-.13,.19-.02,.28,.06,.28,.24,0,.29-.03,.73-.1,1.32-.07,.59-.1,1.04-.1,1.33,0,.16-.13,.24-.4,.24-.11,0-.51-.03-1.2-.09-.5-.05-1.63-.08-3.39-.08-1.48,0-2.42,.02-2.82,.05-.36,.03-.57,.22-.62,.57,0-.06,.01,.28,.01,1.04v1.99c0,.93,.14,1.52,.43,1.75,.22,.19,.79,.28,1.68,.28h1.48c.63-.01,2.39-.12,5.29-.34h.2c.18-.03,.27,.08,.27,.32Z"/>
                           <path class="cls-2" d="M77.61,37.42c0,.2-.09,.3-.27,.3-.3,0-.74,0-1.33-.01-.59,0-1.03-.01-1.33-.01-.16,0-.24-.13-.24-.39,0-.44,.01-1.11,.04-2.01,.03-.9,.04-1.57,.04-2.02,0-.36,0-.9-.01-1.61,0-.71-.01-1.25-.01-1.61,0-1.02-.15-1.75-.44-2.17-.36-.5-1.02-.75-1.99-.75-.45,0-1.07,.25-1.87,.75-.84,.53-1.27,1-1.27,1.41v8.08c0,.22-.09,.34-.26,.34-.29,0-.72,0-1.29-.01-.57,0-1.01-.01-1.29-.01-.19,0-.28-.1-.28-.31,0-.71,.01-1.77,.03-3.19,.02-1.42,.03-2.49,.03-3.21,0-1.91-.2-3.68-.61-5.29-.03-.08-.04-.14-.04-.18,0-.09,.06-.15,.18-.19,.06,0,.58-.09,1.54-.25,.97-.16,1.47-.24,1.52-.24,.05,0,.09,.07,.11,.22,.04,.56,.15,1.12,.31,1.68,.47-.37,1.08-.83,1.85-1.37,.94-.57,1.85-.86,2.73-.86,1.69,0,2.84,.49,3.46,1.47,.46,.72,.69,1.88,.69,3.49,0,.28,0,.7-.02,1.28-.01,.57-.02,1.01-.02,1.29,0,.6,0,1.5,.03,2.69,.02,1.19,.03,2.09,.03,2.69Z"/>
                           <path class="cls-2" d="M87.67,35.21c0,.2-.02,.47-.05,.81-.05,.42-.07,.7-.08,.82,0,.16-.02,.26-.04,.3-.04,.06-.12,.13-.26,.19-.74,.35-1.89,.53-3.48,.53-2.1,0-3.15-.95-3.15-2.86,0-.76,.02-1.91,.05-3.44,.04-1.53,.05-2.68,.05-3.44,0-.37-.18-.57-.55-.59-.33,0-.66,0-.98-.01-.19-.04-.28-.41-.28-1.1,0-.26,.02-.53,.05-.82,.03-.22,.17-.35,.43-.39,.22,0,.44,0,.66-.01,.41-.02,.62-.2,.62-.55s-.01-.91-.03-1.64c-.02-.74-.03-1.29-.03-1.66,0-.66,.13-.98,.38-.98,.08,0,.92,.14,2.53,.42,.21,.03,.31,.13,.31,.3,0,.4-.03,.99-.09,1.78-.06,.79-.09,1.38-.09,1.78,0,.26,.12,.39,.36,.39h3.23c.15,0,.23,.05,.23,.16s-.02,.28-.05,.52c-.03,.24-.05,.42-.05,.53,0,.13,0,.34,.01,.62,0,.28,.01,.48,.01,.62,0,.17-.13,.26-.38,.26-.36,0-.9-.02-1.63-.07-.73-.05-1.27-.07-1.63-.07-.07,0-.13,.26-.16,.77-.05,.64-.07,1.53-.07,2.68v2.01c0,.77,.07,1.31,.22,1.62,.22,.47,.66,.7,1.33,.7,.28,0,.69-.06,1.23-.18s.94-.18,1.2-.18c.11,0,.16,.08,.16,.24Z"/>
                           <path class="cls-2" d="M101.01,30.46c0,.66-.29,1.11-.86,1.33-.39,.14-3.17,.41-8.34,.79,.12,.79,.55,1.46,1.31,2.02,.72,.53,1.49,.79,2.32,.79,1.42,0,2.72-.46,3.91-1.37,.14-.12,.29-.23,.43-.35,.07,0,.12,0,.14,.03,.02,.02,.19,.33,.51,.94,.31,.61,.47,.93,.47,.98,0,.05-.07,.16-.22,.31-1.32,1.4-3.01,2.1-5.08,2.1s-3.74-.62-4.98-1.87c-1.24-1.24-1.86-2.9-1.86-4.98,0-1.85,.59-3.46,1.78-4.84,1.25-1.43,2.78-2.14,4.59-2.14,1.69,0,3.1,.62,4.23,1.86,1.1,1.2,1.66,2.67,1.66,4.39Zm-3.21-.82c0-.75-.29-1.41-.88-1.98s-1.25-.86-1.99-.86c-.83,0-1.56,.33-2.2,1-.64,.66-.96,1.41-.96,2.24,0,.19,.16,.28,.48,.28,1.33,0,2.92-.13,4.77-.4,.52-.07,.78-.16,.78-.27Z"/>
                           <path class="cls-2" d="M110.6,25.04c0,.99-.07,1.89-.2,2.69-.03,.21-.12,.31-.27,.31-.19,0-.47-.02-.86-.06-.38-.04-.67-.06-.87-.06-1.13,0-1.85,.32-2.17,.96-.19,.39-.28,1.19-.28,2.41v1.48c0,.5,.02,1.27,.06,2.29,.04,1.02,.06,1.79,.06,2.29,0,.24-.1,.36-.31,.36-.28,0-.7,0-1.27-.01-.57,0-.99-.01-1.27-.01-.21,0-.31-.11-.31-.34,0-.73,.01-1.82,.03-3.29,.02-1.46,.03-2.56,.03-3.3,0-2.22-.21-4-.62-5.35-.02-.07-.03-.13-.03-.16,0-.11,.06-.18,.18-.23,.33-.04,.8-.09,1.4-.16,.97-.18,1.42-.27,1.35-.27,.14,0,.24,.26,.28,.79,.04,.53,.13,.79,.24,.79,.02,0,.04,0,.07-.03,.37-.23,.74-.47,1.1-.7,.4-.24,.82-.42,1.24-.54,.35-.11,.8-.16,1.35-.16,.7,0,1.05,.09,1.05,.28Z"/>
                           <path class="cls-2" d="M119.58,35.21c0,.2-.02,.47-.05,.81-.05,.42-.07,.7-.08,.82,0,.16-.02,.26-.04,.3-.04,.06-.12,.13-.26,.19-.74,.35-1.89,.53-3.48,.53-2.1,0-3.15-.95-3.15-2.86,0-.76,.02-1.91,.05-3.44,.04-1.53,.05-2.68,.05-3.44,0-.37-.18-.57-.55-.59-.33,0-.66,0-.98-.01-.19-.04-.28-.41-.28-1.1,0-.26,.02-.53,.05-.82,.03-.22,.17-.35,.43-.39,.22,0,.44,0,.66-.01,.41-.02,.62-.2,.62-.55s-.01-.91-.03-1.64c-.02-.74-.03-1.29-.03-1.66,0-.66,.13-.98,.38-.98,.08,0,.92,.14,2.53,.42,.21,.03,.31,.13,.31,.3,0,.4-.03,.99-.09,1.78-.06,.79-.09,1.38-.09,1.78,0,.26,.12,.39,.36,.39h3.23c.15,0,.23,.05,.23,.16s-.02,.28-.05,.52c-.03,.24-.05,.42-.05,.53,0,.13,0,.34,.01,.62,0,.28,.01,.48,.01,.62,0,.17-.13,.26-.38,.26-.36,0-.9-.02-1.63-.07-.73-.05-1.27-.07-1.63-.07-.07,0-.13,.26-.16,.77-.05,.64-.07,1.53-.07,2.68v2.01c0,.77,.07,1.31,.22,1.62,.22,.47,.66,.7,1.33,.7,.28,0,.69-.06,1.23-.18s.94-.18,1.2-.18c.11,0,.16,.08,.16,.24Z"/>
                           <path class="cls-2" d="M134.6,25.5s-.01,.12-.04,.24c-.35,2.06-.53,3.83-.53,5.32,0,.09,.11,2.11,.34,6.05v.19c.02,.26-.1,.39-.34,.39s-.62,.03-1.12,.09c-.5,.06-.87,.09-1.11,.09-.17,0-.3-.31-.39-.94-.09-.63-.18-.94-.26-.94-.05,0-.3,.18-.73,.53-.53,.43-1.02,.76-1.48,.98-.71,.36-1.42,.54-2.13,.54-1.74,0-3.21-.7-4.4-2.09-1.13-1.33-1.7-2.88-1.7-4.65,0-1.99,.57-3.64,1.71-4.94,1.19-1.36,2.77-2.03,4.71-2.03,1.37,0,2.57,.48,3.6,1.43,.15,.18,.39,.44,.71,.78,.03,.03,.05,.04,.08,.04,.05,0,.13-.28,.23-.85,.1-.57,.22-.85,.38-.85,.24,0,.7,.07,1.36,.2,.74,.16,1.1,.31,1.1,.43Zm-3.43,5.63c0-1.1-.33-2.05-.98-2.84-.69-.85-1.58-1.28-2.65-1.28s-1.98,.42-2.69,1.27c-.69,.81-1.04,1.76-1.04,2.86s.35,2.03,1.04,2.83c.72,.84,1.62,1.25,2.69,1.25s1.93-.43,2.64-1.28c.66-.8,1-1.73,1-2.8Z"/>
                           <path class="cls-2" d="M140.3,21.58c0,1.1-.62,1.66-1.85,1.66s-1.87-.55-1.87-1.66c0-.49,.19-.9,.58-1.23,.36-.31,.79-.46,1.29-.46s.94,.16,1.3,.47c.36,.31,.55,.72,.55,1.21Zm-.09,3.65c0,.69-.04,1.74-.13,3.13-.09,1.4-.13,2.44-.13,3.15,0,.66,.01,1.64,.04,2.94,.03,1.31,.04,2.28,.04,2.93,0,.19-.08,.28-.24,.28h-2.63c-.19,0-.28-.18-.28-.55,0-.62,.02-1.55,.05-2.8,.03-1.24,.05-2.18,.05-2.81,0-.7-.05-1.75-.16-3.15s-.15-2.45-.15-3.15c0-.15,.09-.23,.28-.23,.16,0,.41,.02,.75,.07,.34,.04,.59,.07,.76,.07s.42-.02,.76-.07c.34-.04,.59-.07,.76-.07,.15,0,.23,.09,.23,.26Z"/>
                           <path class="cls-2" d="M154.74,37.42c0,.2-.09,.3-.27,.3-.3,0-.74,0-1.33-.01-.59,0-1.03-.01-1.33-.01-.16,0-.24-.13-.24-.39,0-.44,.01-1.11,.04-2.01,.03-.9,.04-1.57,.04-2.02,0-.36,0-.9-.01-1.61,0-.71-.01-1.25-.01-1.61,0-1.02-.15-1.75-.44-2.17-.36-.5-1.02-.75-1.99-.75-.45,0-1.07,.25-1.87,.75-.84,.53-1.27,1-1.27,1.41v8.08c0,.22-.09,.34-.26,.34-.29,0-.72,0-1.29-.01-.57,0-1.01-.01-1.29-.01-.19,0-.28-.1-.28-.31,0-.71,.01-1.77,.03-3.19,.02-1.42,.03-2.49,.03-3.21,0-1.91-.2-3.68-.61-5.29-.03-.08-.04-.14-.04-.18,0-.09,.06-.15,.18-.19,.06,0,.58-.09,1.54-.25,.97-.16,1.47-.24,1.52-.24,.05,0,.09,.07,.11,.22,.04,.56,.15,1.12,.31,1.68,.47-.37,1.08-.83,1.85-1.37,.94-.57,1.85-.86,2.73-.86,1.69,0,2.84,.49,3.46,1.47,.46,.72,.69,1.88,.69,3.49,0,.28,0,.7-.02,1.28-.01,.57-.02,1.01-.02,1.29,0,.6,0,1.5,.03,2.69,.02,1.19,.03,2.09,.03,2.69Z"/>
                           <path class="cls-2" d="M168.7,30.46c0,.66-.29,1.11-.86,1.33-.39,.14-3.17,.41-8.34,.79,.12,.79,.55,1.46,1.31,2.02,.72,.53,1.49,.79,2.32,.79,1.42,0,2.72-.46,3.91-1.37,.14-.12,.29-.23,.43-.35,.07,0,.12,0,.14,.03,.02,.02,.19,.33,.51,.94,.31,.61,.47,.93,.47,.98,0,.05-.07,.16-.22,.31-1.32,1.4-3.01,2.1-5.08,2.1s-3.74-.62-4.98-1.87c-1.24-1.24-1.86-2.9-1.86-4.98,0-1.85,.59-3.46,1.78-4.84,1.25-1.43,2.78-2.14,4.59-2.14,1.69,0,3.1,.62,4.23,1.86,1.1,1.2,1.66,2.67,1.66,4.39Zm-3.21-.82c0-.75-.29-1.41-.88-1.98s-1.25-.86-1.99-.86c-.83,0-1.56,.33-2.2,1-.64,.66-.96,1.41-.96,2.24,0,.19,.16,.28,.48,.28,1.33,0,2.92-.13,4.77-.4,.52-.07,.78-.16,.78-.27Z"/>
                           <path class="cls-2" d="M178.29,25.04c0,.99-.07,1.89-.2,2.69-.03,.21-.12,.31-.27,.31-.19,0-.47-.02-.86-.06-.38-.04-.67-.06-.87-.06-1.13,0-1.85,.32-2.17,.96-.19,.39-.28,1.19-.28,2.41v1.48c0,.5,.02,1.27,.06,2.29s.06,1.79,.06,2.29c0,.24-.1,.36-.31,.36-.28,0-.7,0-1.27-.01-.57,0-.99-.01-1.27-.01-.21,0-.31-.11-.31-.34,0-.73,.01-1.82,.03-3.29,.02-1.46,.03-2.56,.03-3.3,0-2.22-.21-4-.62-5.35-.02-.07-.03-.13-.03-.16,0-.11,.06-.18,.18-.23,.33-.04,.8-.09,1.4-.16,.97-.18,1.42-.27,1.35-.27,.14,0,.24,.26,.28,.79,.04,.53,.13,.79,.24,.79,.02,0,.04,0,.07-.03,.37-.23,.74-.47,1.1-.7,.4-.24,.82-.42,1.24-.54,.35-.11,.8-.16,1.35-.16,.7,0,1.05,.09,1.05,.28Z"/>
                           <path class="cls-2" d="M189.77,35.13s-.07,.36-.2,.96c-.14,.64-.22,1.03-.24,1.19-.04,.31-.12,.47-.24,.47-.31,0-.74-.02-1.27-.05-.6-.04-1.02-.07-1.25-.08-.6-.02-1.46-.03-2.59-.03-.87,0-1.84,.04-2.9,.11-1.33,.09-1.9,.13-1.71,.13-.19,0-.31-.08-.36-.24-.13-.43-.19-.93-.19-1.51,0-.08,0-.2,.01-.34s.01-.26,.01-.33c0-.13,.1-.28,.29-.45,.19-.18,1.15-1.19,2.88-3.05,.87-.91,2.11-2.3,3.7-4.19h-5.94c-.14,0-.22-.09-.22-.28,0-.26,.06-.65,.17-1.17,.11-.52,.17-.9,.17-1.15,0-.11,.13-.16,.38-.16,.5,0,1.25,.02,2.25,.07s1.75,.07,2.25,.07c2.46,0,3.85,.02,4.16,.05,.22,.03,.34,.12,.34,.28,0,1.36-.05,2.12-.16,2.29-.09,.13-.87,1.01-2.33,2.65-.94,1.07-2.37,2.66-4.27,4.77,2.99-.08,5.31-.12,6.95-.12,.21,0,.31,.04,.31,.12Z"/>
                        </g>
                        <path class="cls-2" d="M16.61,49.17c.96-1.64,6.11-6.32,13.48-6.77,2.78-.17,5.12,.34,6.8,.88,0,0,.84,.29,.15,.94s-11.41-1.5-19.36,5.17c-1.6,1.37-1.2,.01-1.07-.22Z"/>
                        <path class="cls-5" d="M42.69,31.94c-.01,2.52-.67,4.93-2.77,6.32-1.62,1.06-3.89,2.45-6.95,3.79,.63-1.27,.93-2.75,.99-4.28,.01-.21,.01-.43,.01-.64,.01-9.53-3.63-17.69-6.99-21.14-.16-.16-.32-.32-.5-.47-1.51-1.33-3.74-2.37-5.74-2.61-.03,0-.06-.01-.09-.01,.03-.01,.07-.01,.1-.02,3.12-.66,6.47-1.1,9.93-1.26,2.07-.09,4.7,1.12,6.16,2.62,2.81,2.89,5.86,9.72,5.85,17.7Z"/>
                        <path class="cls-1" d="M33.97,37.13c0,.21,0,.43-.01,.64-.06,1.53-.36,3.01-.99,4.28-.27,.12-.54,.23-.82,.35-3.01,1.24-6.73,2.41-11.29,3.21v-.21c0-.47-.01-.95-.03-1.41-.02-.5-.04-.99-.07-1.48-.01-.04-.01-.07-.01-.11-.03-.46-.07-.91-.12-1.36-.02-.25-.05-.49-.08-.74-.08-.69-.17-1.36-.28-2.03-.07-.45-.15-.89-.23-1.32-.01-.08-.03-.17-.05-.25-.1-.54-.22-1.08-.34-1.6-.03-.13-.06-.26-.09-.39-.11-.44-.22-.87-.34-1.3-.11-.44-.24-.87-.37-1.29-.06-.2-.12-.4-.19-.6-.14-.44-.28-.88-.44-1.3-.18-.51-.37-1.01-.56-1.5-.25-.62-.51-1.22-.77-1.8-.6-1.29-1.23-2.47-1.88-3.53-.12-.2-.25-.4-.38-.59-.12-.2-.25-.39-.38-.57-.63-.92-1.28-1.72-1.92-2.38-.16-.16-.33-.33-.51-.48-.02-.03-.05-.05-.08-.08-.06-.05-.12-.1-.18-.15-.45-.38-.95-.74-1.48-1.07-.14-.09-.28-.17-.42-.25-.48-.28-.98-.53-1.49-.75,1.46-.71,3.09-1.4,4.87-2.04,.53-.18,1.07-.37,1.62-.54,1.32-.43,2.71-.82,4.15-1.18,.6-.14,1.22-.28,1.84-.41,.03,0,.06,.01,.09,.01,2,.24,4.23,1.28,5.74,2.61,.18,.15,.34,.31,.5,.47,3.36,3.45,7,11.61,6.99,21.14Z"/>
                        <path class="cls-4" d="M20.86,45.61c-3.31,.58-7.05,.95-11.28,1.01-.83,.01-1.62-.36-2.33-.79-2.42-1.46-5.57-7.74-7.18-18.65-.38-2.58,.81-5,2.76-6.72,1.27-1.12,3.09-2.28,5.33-3.39,1.36,.59,2.64,1.4,3.65,2.3,.18,.16,.34,.32,.5,.48,3.71,3.82,7.71,12.34,8.42,22.55,.08,1.05,.11,2.12,.11,3.21Z"/>
                        <path class="cls-2" d="M6.78,22.31c3.46-3.05,12.1-6.51,21.9-6.95,1.63-.07,3.7,.88,4.84,2.06,2.21,2.28,4.61,7.65,4.6,13.93,0,1.98-.64,3.72-2.18,4.97-3.42,2.78-10.62,7.14-23.86,6.57-.65-.03-1.28-.28-1.84-.62-1.9-1.15-4.38-6.09-5.65-14.67-.3-2.03,.64-3.93,2.17-5.29Z"/>
                        <path class="cls-3" d="M26.11,26.61l-5.61-1.56c-1.31-.36-2.85,.26-3.44,1.38-.23,.43-.29,.9-.17,1.33l1.51,5.69c.3,1.12,1.67,1.73,3.08,1.36,.54-.14,1.04-.42,1.4-.8l4.1-4.13c.96-.96,.86-2.3-.21-2.99-.2-.13-.42-.22-.66-.29h0Z"/>
                     </svg>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md40 text-center">
                  <div class="f-md-20 f-18 w600 lh160 white-clr"><span>Grab My 15 Exclusive Bonuses Before the Deal Ends…</span></div>
               </div>
               <div class="col-md-12 col-12 mt20 mt-md25 p-md0 px15">
                  <div class="">
                     <div class="f-md-40 f-28 w700 text-center white-clr lh160">Revealing A Breakthrough Software That <span class="under yellow-clr"> Creates Stunning & Self-Updating Entertainment News Site </span> Packaged with Trendy Content & Videos </div>
                  </div>
               </div>
               <div class="col-md-12 col-12 text-center mt20 mt-md30">
                  <div class="f-18 w400 text-center lh160 white-clr">
                     <span class="w600">A Secret Method That Makes $528/ Day Over And Over Again</span><br><br>
                     Watch My Quick Review Video
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md30">
               <div class="video-frame col-12 col-md-10 mx-auto">
                  <img src="assets/images/productbox.webp" class="img-fluid d-block mx-auto">
                  <!-- <div style="padding-bottom: 56.25%;position: relative;"><iframe src="https://buzzify.dotcompal.com/video/embed/nikbjn6qnu" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                     box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div> -->
               </div>
               <!-- <img src="assets/images/video-shadow.png" class="img-fluid hidden-xs"> -->
            </div>
         </div>
      </div>
      <div class="second-section">
         <div class="container">
            <div class="row">
               <div class="col-12 p-md0">
                  <div class="col-12 key-features-bg d-flex align-items-center flex-wrap">
                     <div class="row">
                        <div class="col-12 col-md-6">
                           <ul class="list-head-enter pl0 m0 f-16 f-md-18 lh160 w600 black-clr text-capitalize">
                              <li>Legally Use from 800 million+ Trending Content Created by Others</li>
                              <li>Set And Forget System with Single Keyword</li>
                              <li>AI-Powered Software Puts Most Profitable Links on Your Websites</li>
                              <li>Built-In 1-Click Traffic Generating System</li>
                              <li>1-Click Social Media Automation</li>
                              <li>100% SEO Friendly Website And Built-In Remarketing System </li>
                              
                           </ul>
                        </div>
                        <div class="col-12 col-md-6">
                           <ul class="list-head-enter pl0 m0 f-16 f-md-17 lh160 w600 black-clr text-capitalize">
                              <li>Automatically Translate Your Sites In 15+ Language According For More Traffic</li>
                              <li>Integration With Major Autoresponders And Social Media Apps </li>
                              <li>In-Built Content Spinner To Make Your Content Fresh</li>
                              <li>Make 5K-10K With Commercial License</li>
                              <li>A-Z Complete Video Training Is Included </li>
                              <li>Limited-Time Special Bonuses Worth $2285 If You Buy Today </li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md70">
               <!-- CTA Btn Section Start -->
               <!-- CTA Btn Section Start -->
               <div class="col-md-12 col-md-12 col-12 text-center">
                  <div class="f-md-20 f-18 text-center mt3 black lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-22 f-22 text-center purple lh120 w700 mt15 mt-md20 lite-black-clr">TAKE ACTION NOW!</div>
                  <div class="f-md-22 f-17 lh120 w600 mt15 mt-md20">Use Coupon Code <span class="w800 lite-black-clr">"ENTERBIG"</span> with <span class="w800 lite-black-clr">$5 discount</span> </div>
               </div>
               <div class="col-md-12 col-12 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab Entertainerz + My 15 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               <div class="col-12 mt15 mt-md20 text-center">
                  <img src="assets/images/payment.png" class="img-fluid">
               </div>
               <div class="col-12 mt15 mt-md20" align="center">
                  <h3 class="f-md-28 f-20 w500 text-center black">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-12 text-center">
                  <div class="countdown counter-black">
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Days</span> </div>
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                     <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                     <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
  <!-- Step Section End -->
  <div class="no-installation">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-md-36 f-28 w700 lh150 text-capitalize black-clr text-center">
                     Drive Massive Traffic &amp; Sales To Any Offer, <br class="d-none d-md-block">Page, Or Link In Just <span class="w800 purple-clr step-line">3 Simple Steps…</span>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md60">
                  <div class="row">
                     <div class="col-12 col-md-4">
                        <div class="steps-block">
                           <div class="step-bg">
                              <img src="assets/images/step-1.webp">
                              <div class="f-22 f-md-26 w700 black-clr">
                                 Step #1
                              </div>
                           </div>
                           <div class="f-18 f-md-20 w700 black-clr lh150 mt20 mt-md30 text-center">
                              Login into our <br class="d-none d-md-block"> cloud-based software.
                           </div>
                        </div>
                     </div>
                     <div class="col-12 col-md-4 mt20 mt-md0">
                        <div class="steps-block">
                           <div class="step-bg">
                              <img src="assets/images/step-2.webp">
                              <div class="f-22 f-md-26 w700 black-clr">
                                 Step #2
                              </div>
                           </div>
                           <div class="f-18 f-md-20 w700 black-clr lh150 mt20 mt-md30 text-center">
                              Just enter a keyword.
                           </div>
                        </div>
                     </div>
                     <div class="col-12 col-md-4 mt20 mt-md0">
                        <div class="steps-block">
                           <div class="step-bg">
                              <img src="assets/images/step-3.webp">
                              <div class="f-22 f-md-26 w700 black-clr">
                                 Step #3
                              </div>
                           </div>
                           <div class="f-18 f-md-20 w700 black-clr lh150 mt20 mt-md30 text-center">
                              Select the trending content from YouTube &amp; top authority
                              sites
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md80">
                  <div class="f-20 f-md-22 w600 text-center black-clr lh150">
                     That's it. All hard work is done. Now just sit back, relax, and watch your TRAFFIC &amp; profits grow leaps and bounds.
                  </div>
               </div>
               <div class="col-12 col-md-12 mx-auto mt40 mt-md50">
                  <div class="row">
                     <div class="col-12 col-md-4 col-md-4">
                        <!-- <img src="assets/images/no1.webp" class="img-fluid d-block mx-auto"> -->
                        <div class="w800 f-18 f-md-24 purple-clr text-center lh150">
                           No Download/Installation 
                        </div>
                     </div>
                     <div class="col-12 col-md-4 mt20 mt-md0">
                        <!--  <img src="assets/images/no2.webp" class="img-fluid d-block mx-auto"> -->
                        <div class="w800 f-18 f-md-24 purple-clr text-center lh150">
                           No Prior Knowledge 	
                        </div>
                     </div>
                     <div class="col-12 col-md-4 mt20 mt-md0">
                        <!-- <img src="assets/images/no3.webp" class="img-fluid d-block mx-auto"> -->
                        <div class="w800 f-18 f-md-24 purple-clr text-center lh150">
                           100% Beginners Friendly
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="proof-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-45 f-28 w700 lh150 text-center black-clr">
                  We Are Getting Thousands of FREE TRAFFIC Monthly 
                  for Each Site - 100% Hands-Free
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
                  <img src="assets/images/proof1.webp" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 mt20 mt-md80">
                  <div class="f-md-45 f-28 w700 lh150 text-center black-clr">
                     Which Made Us $16,029 Last Month - That's an 
                     Average <u>$528 In Profits Each &amp; Every Day</u>
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
                  <img src="assets/images/proof2.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>
         </div>
      </div>
	  <div class="testimonial-section">
         <div class="container ">
            <div class="row ">
               <div class="col-12">
                  <div class="f-md-45 f-28 lh150 w700 text-center black-clr">Checkout What Entertainerz Early <br class="d-none d-md-block"> Users Have to SAY</div>
               </div>
            </div>
            <div class="row row-cols-md-2 row-cols-1 gx4">
               <div class="col mt20 mt-md50">
                  <img src="https://cdn.oppyo.com/launches/entertainerz/special/t1.webp" class="img-fluid d-block mx-auto">
               </div>
               <div class="col mt20 mt-md50">
               <img src="https://cdn.oppyo.com/launches/entertainerz/special/t2.webp" class="img-fluid d-block mx-auto">
               </div>
               <div class="col mt20 mt-md20">
               <img src="https://cdn.oppyo.com/launches/entertainerz/special/t3.webp" class="img-fluid d-block mx-auto">
               </div>
               <div class="col mt20 mt-md20">
               <img src="https://cdn.oppyo.com/launches/entertainerz/special/t4.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>
         </div>
      </div>
      <!-- CTA Button Section Start -->
      <div class="cta-btn-section dark-cta-bg">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center">
                  <div class="f-md-20 f-18 text-center mt3 white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-22 f-22 text-center purple lh120 w700 mt15 mt-md20 orange-clr">TAKE ACTION NOW!</div>
                  <div class="f-md-22 f-17 lh120 w600 mt15 mt-md20 white-clr">Use Coupon Code <span class="w800 orange-clr">"ENTERBIG"</span> with <span class="w800 orange-clr">$5 discount</span> </div>
               </div>
               <div class="col-md-12 col-12 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab Entertainerz + My 15 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               <div class="col-12 mt15 mt-md20 text-center">
                  <img src="assets/images/payment.png" class="img-fluid">
               </div>
               <div class="col-12 mt15 mt-md20" align="center">
                  <h3 class="f-md-28 f-20 w500 text-center white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Days</span> </div>
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                     <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                     <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->
	  <div class="thatsall-section">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-10 mx-auto text-center">
                  <img src="assets/images/line.webp">
                  <div class="f-40 f-md-70 w800 purple-clr mt10">Did You Know?</div>
               </div>
               <div class="col-12 f-md-38 f-28 w700 lh150 text-center mt20">
                  Entertainment is No. 1 Watched <br class="d-none d-md-block">
Category on YouTube

               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-12 col-md-6 f-md-24 f-20 w600 lh150">
               And Global Entertainment News Media 
is A Huge $2.93 Trillion Market

               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                  <img src="assets/images/growth.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-12 text-center">
                  <div class="thatsall-heading f-24 f-md-38 w700 white-clr lh150">                  
                    
Now, Let Me Ask You a Question! 

                  </div>
               </div>
               <div class="col-12 text-center mt20">
                  <div class="f-22 f-md-28 w700 lh150">                  
                  How many times you have read or watched headlines like?
                  </div>
               </div>
            </div>
            <div class="row mt-md50 mt20 align-items-center">
               <div class="col-12 col-md-6">
                  <div class="avengame-bg">
                     <div class="f-24 f-md-32 w800 black-clr lh150">“10 reasons why G.O.T season 8 is going to change the series forever”</div>
                  </div>
               </div>
               <div class="col-12 col-md-6 mx-auto mt20 mt-md0">
                  <div class="avengame-bg1">
                     <div class="f-24 f-md-32 w800 black-clr lh150">“We finally understand<br class="d-none d-md-block"> why Marvel started with<br class="d-none d-md-block"> Iron Man” 
                  </div>
                  </div>
               </div>
               <div class="col-12 text-center f-md-20 f-18 w500 lh150 mt20 mt-md70">               
				Let's Admit, we all have clicked them, read that content, and watched that video.  
				<br> 
				These are Entertainment topics that almost everyone is into and wants to know more and more about that.
               </div>
            </div>
         </div>
      </div>
      <div class="proudly-section" id="product">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-36 f-24 w700 text-center white-clr lh150 text-uppercase">
                  Introducing…
               </div>
               <div class="col-12">
                  <img src="http://localhost/launches/entertainerz/jv/assets/images/h-line.webp" class="d-block mx-auto img-fluid">
               </div>
               <div class="col-12 mt-md30 mt20 text-center">
                  <svg id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 189.77 50" style="max-height:140px">
                     <defs>
                        <style>.cls-1{fill:url(#linear-gradient-2);}.cls-2{fill:#fff;}.cls-3{fill:#001730;}.cls-4{fill:url(#linear-gradient-3);}.cls-5{fill:url(#linear-gradient);}</style>
                        <linearGradient id="linear-gradient" x1="20.65" y1="26.83" x2="42.69" y2="26.83" gradientUnits="userSpaceOnUse">
                           <stop offset="0" stop-color="#bf003b"></stop>
                           <stop offset="1" stop-color="#e23940"></stop>
                        </linearGradient>
                        <linearGradient id="linear-gradient-2" x1=".12" y1="29.26" x2="25.92" y2="29.26" gradientUnits="userSpaceOnUse">
                           <stop offset="0" stop-color="#f38227"></stop>
                           <stop offset="1" stop-color="#fbb826"></stop>
                        </linearGradient>
                        <linearGradient id="linear-gradient-3" x1="3.09" y1="36.12" x2="22.33" y2="28.06" gradientUnits="userSpaceOnUse">
                           <stop offset="0" stop-color="#3369e9"></stop>
                           <stop offset="1" stop-color="#002b7f"></stop>
                        </linearGradient>
                     </defs>
                     <path class="cls-2" d="M22.29,0c-.94,0-1.69,.76-1.69,1.69,0,.75,.48,1.39,1.16,1.61l-2.95,10.01-.61,2.09,2.13,.26,.41-2.75v-.03s1.44-9.5,1.44-9.5c.03,0,.07,0,.11,0,.93,0,1.69-.76,1.69-1.7s-.76-1.69-1.69-1.69Z"></path>
                     <path class="cls-2" d="M16.2,15.76l-1.74,.96-5.3-6.31c-.29,.23-.64,.36-1.03,.36-.94,0-1.7-.75-1.7-1.69s.76-1.69,1.7-1.69,1.69,.76,1.69,1.69c0,.41-.15,.79-.4,1.08l6.78,5.6Z"></path>
                     <g>
                        <path class="cls-2" d="M63.68,34.98c0,.14-.04,.35-.11,.62-.08,.34-.13,.55-.15,.62-.03,.32-.09,.79-.19,1.41-.05,.16-.16,.24-.35,.24-.09,0-.26-.02-.51-.05-1.1-.16-2.82-.24-5.13-.24-.66,0-1.64,.02-2.96,.05s-2.31,.05-2.96,.05c-.3,0-.44-.13-.44-.39,0-1.01,.06-2.52,.17-4.53,.11-2.02,.17-3.53,.17-4.53s-.05-2.93-.16-5.72c-.02-.72-.07-1.79-.15-3.22v-.19c-.02-.21,.09-.31,.34-.31,.63,0,1.57,.02,2.83,.05,1.26,.04,2.2,.05,2.83,.05s1.5-.02,2.7-.05c1.2-.04,2.1-.05,2.7-.05,.42,0,.65,.13,.69,.39,.19,1.19,.28,1.92,.28,2.21,0,.21-.1,.31-.3,.31-.76,0-1.92-.04-3.47-.13-1.55-.09-2.71-.13-3.5-.13-.74,0-1.2,.1-1.39,.3-.19,.2-.29,.69-.29,1.47v2.01c0,.55,.01,.87,.04,.96,.08,.28,.31,.42,.7,.42,.11,0,.26,0,.46-.01h.46c1.76,0,3.34-.03,4.74-.09,.55-.02,1.19-.06,1.94-.13,.19-.02,.28,.06,.28,.24,0,.29-.03,.73-.1,1.32-.07,.59-.1,1.04-.1,1.33,0,.16-.13,.24-.4,.24-.11,0-.51-.03-1.2-.09-.5-.05-1.63-.08-3.39-.08-1.48,0-2.42,.02-2.82,.05-.36,.03-.57,.22-.62,.57,0-.06,.01,.28,.01,1.04v1.99c0,.93,.14,1.52,.43,1.75,.22,.19,.79,.28,1.68,.28h1.48c.63-.01,2.39-.12,5.29-.34h.2c.18-.03,.27,.08,.27,.32Z"></path>
                        <path class="cls-2" d="M77.61,37.42c0,.2-.09,.3-.27,.3-.3,0-.74,0-1.33-.01-.59,0-1.03-.01-1.33-.01-.16,0-.24-.13-.24-.39,0-.44,.01-1.11,.04-2.01,.03-.9,.04-1.57,.04-2.02,0-.36,0-.9-.01-1.61,0-.71-.01-1.25-.01-1.61,0-1.02-.15-1.75-.44-2.17-.36-.5-1.02-.75-1.99-.75-.45,0-1.07,.25-1.87,.75-.84,.53-1.27,1-1.27,1.41v8.08c0,.22-.09,.34-.26,.34-.29,0-.72,0-1.29-.01-.57,0-1.01-.01-1.29-.01-.19,0-.28-.1-.28-.31,0-.71,.01-1.77,.03-3.19,.02-1.42,.03-2.49,.03-3.21,0-1.91-.2-3.68-.61-5.29-.03-.08-.04-.14-.04-.18,0-.09,.06-.15,.18-.19,.06,0,.58-.09,1.54-.25,.97-.16,1.47-.24,1.52-.24,.05,0,.09,.07,.11,.22,.04,.56,.15,1.12,.31,1.68,.47-.37,1.08-.83,1.85-1.37,.94-.57,1.85-.86,2.73-.86,1.69,0,2.84,.49,3.46,1.47,.46,.72,.69,1.88,.69,3.49,0,.28,0,.7-.02,1.28-.01,.57-.02,1.01-.02,1.29,0,.6,0,1.5,.03,2.69,.02,1.19,.03,2.09,.03,2.69Z"></path>
                        <path class="cls-2" d="M87.67,35.21c0,.2-.02,.47-.05,.81-.05,.42-.07,.7-.08,.82,0,.16-.02,.26-.04,.3-.04,.06-.12,.13-.26,.19-.74,.35-1.89,.53-3.48,.53-2.1,0-3.15-.95-3.15-2.86,0-.76,.02-1.91,.05-3.44,.04-1.53,.05-2.68,.05-3.44,0-.37-.18-.57-.55-.59-.33,0-.66,0-.98-.01-.19-.04-.28-.41-.28-1.1,0-.26,.02-.53,.05-.82,.03-.22,.17-.35,.43-.39,.22,0,.44,0,.66-.01,.41-.02,.62-.2,.62-.55s-.01-.91-.03-1.64c-.02-.74-.03-1.29-.03-1.66,0-.66,.13-.98,.38-.98,.08,0,.92,.14,2.53,.42,.21,.03,.31,.13,.31,.3,0,.4-.03,.99-.09,1.78-.06,.79-.09,1.38-.09,1.78,0,.26,.12,.39,.36,.39h3.23c.15,0,.23,.05,.23,.16s-.02,.28-.05,.52c-.03,.24-.05,.42-.05,.53,0,.13,0,.34,.01,.62,0,.28,.01,.48,.01,.62,0,.17-.13,.26-.38,.26-.36,0-.9-.02-1.63-.07-.73-.05-1.27-.07-1.63-.07-.07,0-.13,.26-.16,.77-.05,.64-.07,1.53-.07,2.68v2.01c0,.77,.07,1.31,.22,1.62,.22,.47,.66,.7,1.33,.7,.28,0,.69-.06,1.23-.18s.94-.18,1.2-.18c.11,0,.16,.08,.16,.24Z"></path>
                        <path class="cls-2" d="M101.01,30.46c0,.66-.29,1.11-.86,1.33-.39,.14-3.17,.41-8.34,.79,.12,.79,.55,1.46,1.31,2.02,.72,.53,1.49,.79,2.32,.79,1.42,0,2.72-.46,3.91-1.37,.14-.12,.29-.23,.43-.35,.07,0,.12,0,.14,.03,.02,.02,.19,.33,.51,.94,.31,.61,.47,.93,.47,.98,0,.05-.07,.16-.22,.31-1.32,1.4-3.01,2.1-5.08,2.1s-3.74-.62-4.98-1.87c-1.24-1.24-1.86-2.9-1.86-4.98,0-1.85,.59-3.46,1.78-4.84,1.25-1.43,2.78-2.14,4.59-2.14,1.69,0,3.1,.62,4.23,1.86,1.1,1.2,1.66,2.67,1.66,4.39Zm-3.21-.82c0-.75-.29-1.41-.88-1.98s-1.25-.86-1.99-.86c-.83,0-1.56,.33-2.2,1-.64,.66-.96,1.41-.96,2.24,0,.19,.16,.28,.48,.28,1.33,0,2.92-.13,4.77-.4,.52-.07,.78-.16,.78-.27Z"></path>
                        <path class="cls-2" d="M110.6,25.04c0,.99-.07,1.89-.2,2.69-.03,.21-.12,.31-.27,.31-.19,0-.47-.02-.86-.06-.38-.04-.67-.06-.87-.06-1.13,0-1.85,.32-2.17,.96-.19,.39-.28,1.19-.28,2.41v1.48c0,.5,.02,1.27,.06,2.29,.04,1.02,.06,1.79,.06,2.29,0,.24-.1,.36-.31,.36-.28,0-.7,0-1.27-.01-.57,0-.99-.01-1.27-.01-.21,0-.31-.11-.31-.34,0-.73,.01-1.82,.03-3.29,.02-1.46,.03-2.56,.03-3.3,0-2.22-.21-4-.62-5.35-.02-.07-.03-.13-.03-.16,0-.11,.06-.18,.18-.23,.33-.04,.8-.09,1.4-.16,.97-.18,1.42-.27,1.35-.27,.14,0,.24,.26,.28,.79,.04,.53,.13,.79,.24,.79,.02,0,.04,0,.07-.03,.37-.23,.74-.47,1.1-.7,.4-.24,.82-.42,1.24-.54,.35-.11,.8-.16,1.35-.16,.7,0,1.05,.09,1.05,.28Z"></path>
                        <path class="cls-2" d="M119.58,35.21c0,.2-.02,.47-.05,.81-.05,.42-.07,.7-.08,.82,0,.16-.02,.26-.04,.3-.04,.06-.12,.13-.26,.19-.74,.35-1.89,.53-3.48,.53-2.1,0-3.15-.95-3.15-2.86,0-.76,.02-1.91,.05-3.44,.04-1.53,.05-2.68,.05-3.44,0-.37-.18-.57-.55-.59-.33,0-.66,0-.98-.01-.19-.04-.28-.41-.28-1.1,0-.26,.02-.53,.05-.82,.03-.22,.17-.35,.43-.39,.22,0,.44,0,.66-.01,.41-.02,.62-.2,.62-.55s-.01-.91-.03-1.64c-.02-.74-.03-1.29-.03-1.66,0-.66,.13-.98,.38-.98,.08,0,.92,.14,2.53,.42,.21,.03,.31,.13,.31,.3,0,.4-.03,.99-.09,1.78-.06,.79-.09,1.38-.09,1.78,0,.26,.12,.39,.36,.39h3.23c.15,0,.23,.05,.23,.16s-.02,.28-.05,.52c-.03,.24-.05,.42-.05,.53,0,.13,0,.34,.01,.62,0,.28,.01,.48,.01,.62,0,.17-.13,.26-.38,.26-.36,0-.9-.02-1.63-.07-.73-.05-1.27-.07-1.63-.07-.07,0-.13,.26-.16,.77-.05,.64-.07,1.53-.07,2.68v2.01c0,.77,.07,1.31,.22,1.62,.22,.47,.66,.7,1.33,.7,.28,0,.69-.06,1.23-.18s.94-.18,1.2-.18c.11,0,.16,.08,.16,.24Z"></path>
                        <path class="cls-2" d="M134.6,25.5s-.01,.12-.04,.24c-.35,2.06-.53,3.83-.53,5.32,0,.09,.11,2.11,.34,6.05v.19c.02,.26-.1,.39-.34,.39s-.62,.03-1.12,.09c-.5,.06-.87,.09-1.11,.09-.17,0-.3-.31-.39-.94-.09-.63-.18-.94-.26-.94-.05,0-.3,.18-.73,.53-.53,.43-1.02,.76-1.48,.98-.71,.36-1.42,.54-2.13,.54-1.74,0-3.21-.7-4.4-2.09-1.13-1.33-1.7-2.88-1.7-4.65,0-1.99,.57-3.64,1.71-4.94,1.19-1.36,2.77-2.03,4.71-2.03,1.37,0,2.57,.48,3.6,1.43,.15,.18,.39,.44,.71,.78,.03,.03,.05,.04,.08,.04,.05,0,.13-.28,.23-.85,.1-.57,.22-.85,.38-.85,.24,0,.7,.07,1.36,.2,.74,.16,1.1,.31,1.1,.43Zm-3.43,5.63c0-1.1-.33-2.05-.98-2.84-.69-.85-1.58-1.28-2.65-1.28s-1.98,.42-2.69,1.27c-.69,.81-1.04,1.76-1.04,2.86s.35,2.03,1.04,2.83c.72,.84,1.62,1.25,2.69,1.25s1.93-.43,2.64-1.28c.66-.8,1-1.73,1-2.8Z"></path>
                        <path class="cls-2" d="M140.3,21.58c0,1.1-.62,1.66-1.85,1.66s-1.87-.55-1.87-1.66c0-.49,.19-.9,.58-1.23,.36-.31,.79-.46,1.29-.46s.94,.16,1.3,.47c.36,.31,.55,.72,.55,1.21Zm-.09,3.65c0,.69-.04,1.74-.13,3.13-.09,1.4-.13,2.44-.13,3.15,0,.66,.01,1.64,.04,2.94,.03,1.31,.04,2.28,.04,2.93,0,.19-.08,.28-.24,.28h-2.63c-.19,0-.28-.18-.28-.55,0-.62,.02-1.55,.05-2.8,.03-1.24,.05-2.18,.05-2.81,0-.7-.05-1.75-.16-3.15s-.15-2.45-.15-3.15c0-.15,.09-.23,.28-.23,.16,0,.41,.02,.75,.07,.34,.04,.59,.07,.76,.07s.42-.02,.76-.07c.34-.04,.59-.07,.76-.07,.15,0,.23,.09,.23,.26Z"></path>
                        <path class="cls-2" d="M154.74,37.42c0,.2-.09,.3-.27,.3-.3,0-.74,0-1.33-.01-.59,0-1.03-.01-1.33-.01-.16,0-.24-.13-.24-.39,0-.44,.01-1.11,.04-2.01,.03-.9,.04-1.57,.04-2.02,0-.36,0-.9-.01-1.61,0-.71-.01-1.25-.01-1.61,0-1.02-.15-1.75-.44-2.17-.36-.5-1.02-.75-1.99-.75-.45,0-1.07,.25-1.87,.75-.84,.53-1.27,1-1.27,1.41v8.08c0,.22-.09,.34-.26,.34-.29,0-.72,0-1.29-.01-.57,0-1.01-.01-1.29-.01-.19,0-.28-.1-.28-.31,0-.71,.01-1.77,.03-3.19,.02-1.42,.03-2.49,.03-3.21,0-1.91-.2-3.68-.61-5.29-.03-.08-.04-.14-.04-.18,0-.09,.06-.15,.18-.19,.06,0,.58-.09,1.54-.25,.97-.16,1.47-.24,1.52-.24,.05,0,.09,.07,.11,.22,.04,.56,.15,1.12,.31,1.68,.47-.37,1.08-.83,1.85-1.37,.94-.57,1.85-.86,2.73-.86,1.69,0,2.84,.49,3.46,1.47,.46,.72,.69,1.88,.69,3.49,0,.28,0,.7-.02,1.28-.01,.57-.02,1.01-.02,1.29,0,.6,0,1.5,.03,2.69,.02,1.19,.03,2.09,.03,2.69Z"></path>
                        <path class="cls-2" d="M168.7,30.46c0,.66-.29,1.11-.86,1.33-.39,.14-3.17,.41-8.34,.79,.12,.79,.55,1.46,1.31,2.02,.72,.53,1.49,.79,2.32,.79,1.42,0,2.72-.46,3.91-1.37,.14-.12,.29-.23,.43-.35,.07,0,.12,0,.14,.03,.02,.02,.19,.33,.51,.94,.31,.61,.47,.93,.47,.98,0,.05-.07,.16-.22,.31-1.32,1.4-3.01,2.1-5.08,2.1s-3.74-.62-4.98-1.87c-1.24-1.24-1.86-2.9-1.86-4.98,0-1.85,.59-3.46,1.78-4.84,1.25-1.43,2.78-2.14,4.59-2.14,1.69,0,3.1,.62,4.23,1.86,1.1,1.2,1.66,2.67,1.66,4.39Zm-3.21-.82c0-.75-.29-1.41-.88-1.98s-1.25-.86-1.99-.86c-.83,0-1.56,.33-2.2,1-.64,.66-.96,1.41-.96,2.24,0,.19,.16,.28,.48,.28,1.33,0,2.92-.13,4.77-.4,.52-.07,.78-.16,.78-.27Z"></path>
                        <path class="cls-2" d="M178.29,25.04c0,.99-.07,1.89-.2,2.69-.03,.21-.12,.31-.27,.31-.19,0-.47-.02-.86-.06-.38-.04-.67-.06-.87-.06-1.13,0-1.85,.32-2.17,.96-.19,.39-.28,1.19-.28,2.41v1.48c0,.5,.02,1.27,.06,2.29s.06,1.79,.06,2.29c0,.24-.1,.36-.31,.36-.28,0-.7,0-1.27-.01-.57,0-.99-.01-1.27-.01-.21,0-.31-.11-.31-.34,0-.73,.01-1.82,.03-3.29,.02-1.46,.03-2.56,.03-3.3,0-2.22-.21-4-.62-5.35-.02-.07-.03-.13-.03-.16,0-.11,.06-.18,.18-.23,.33-.04,.8-.09,1.4-.16,.97-.18,1.42-.27,1.35-.27,.14,0,.24,.26,.28,.79,.04,.53,.13,.79,.24,.79,.02,0,.04,0,.07-.03,.37-.23,.74-.47,1.1-.7,.4-.24,.82-.42,1.24-.54,.35-.11,.8-.16,1.35-.16,.7,0,1.05,.09,1.05,.28Z"></path>
                        <path class="cls-2" d="M189.77,35.13s-.07,.36-.2,.96c-.14,.64-.22,1.03-.24,1.19-.04,.31-.12,.47-.24,.47-.31,0-.74-.02-1.27-.05-.6-.04-1.02-.07-1.25-.08-.6-.02-1.46-.03-2.59-.03-.87,0-1.84,.04-2.9,.11-1.33,.09-1.9,.13-1.71,.13-.19,0-.31-.08-.36-.24-.13-.43-.19-.93-.19-1.51,0-.08,0-.2,.01-.34s.01-.26,.01-.33c0-.13,.1-.28,.29-.45,.19-.18,1.15-1.19,2.88-3.05,.87-.91,2.11-2.3,3.7-4.19h-5.94c-.14,0-.22-.09-.22-.28,0-.26,.06-.65,.17-1.17,.11-.52,.17-.9,.17-1.15,0-.11,.13-.16,.38-.16,.5,0,1.25,.02,2.25,.07s1.75,.07,2.25,.07c2.46,0,3.85,.02,4.16,.05,.22,.03,.34,.12,.34,.28,0,1.36-.05,2.12-.16,2.29-.09,.13-.87,1.01-2.33,2.65-.94,1.07-2.37,2.66-4.27,4.77,2.99-.08,5.31-.12,6.95-.12,.21,0,.31,.04,.31,.12Z"></path>
                     </g>
                     <path class="cls-2" d="M16.61,49.17c.96-1.64,6.11-6.32,13.48-6.77,2.78-.17,5.12,.34,6.8,.88,0,0,.84,.29,.15,.94s-11.41-1.5-19.36,5.17c-1.6,1.37-1.2,.01-1.07-.22Z"></path>
                     <path class="cls-5" d="M42.69,31.94c-.01,2.52-.67,4.93-2.77,6.32-1.62,1.06-3.89,2.45-6.95,3.79,.63-1.27,.93-2.75,.99-4.28,.01-.21,.01-.43,.01-.64,.01-9.53-3.63-17.69-6.99-21.14-.16-.16-.32-.32-.5-.47-1.51-1.33-3.74-2.37-5.74-2.61-.03,0-.06-.01-.09-.01,.03-.01,.07-.01,.1-.02,3.12-.66,6.47-1.1,9.93-1.26,2.07-.09,4.7,1.12,6.16,2.62,2.81,2.89,5.86,9.72,5.85,17.7Z"></path>
                     <path class="cls-1" d="M33.97,37.13c0,.21,0,.43-.01,.64-.06,1.53-.36,3.01-.99,4.28-.27,.12-.54,.23-.82,.35-3.01,1.24-6.73,2.41-11.29,3.21v-.21c0-.47-.01-.95-.03-1.41-.02-.5-.04-.99-.07-1.48-.01-.04-.01-.07-.01-.11-.03-.46-.07-.91-.12-1.36-.02-.25-.05-.49-.08-.74-.08-.69-.17-1.36-.28-2.03-.07-.45-.15-.89-.23-1.32-.01-.08-.03-.17-.05-.25-.1-.54-.22-1.08-.34-1.6-.03-.13-.06-.26-.09-.39-.11-.44-.22-.87-.34-1.3-.11-.44-.24-.87-.37-1.29-.06-.2-.12-.4-.19-.6-.14-.44-.28-.88-.44-1.3-.18-.51-.37-1.01-.56-1.5-.25-.62-.51-1.22-.77-1.8-.6-1.29-1.23-2.47-1.88-3.53-.12-.2-.25-.4-.38-.59-.12-.2-.25-.39-.38-.57-.63-.92-1.28-1.72-1.92-2.38-.16-.16-.33-.33-.51-.48-.02-.03-.05-.05-.08-.08-.06-.05-.12-.1-.18-.15-.45-.38-.95-.74-1.48-1.07-.14-.09-.28-.17-.42-.25-.48-.28-.98-.53-1.49-.75,1.46-.71,3.09-1.4,4.87-2.04,.53-.18,1.07-.37,1.62-.54,1.32-.43,2.71-.82,4.15-1.18,.6-.14,1.22-.28,1.84-.41,.03,0,.06,.01,.09,.01,2,.24,4.23,1.28,5.74,2.61,.18,.15,.34,.31,.5,.47,3.36,3.45,7,11.61,6.99,21.14Z"></path>
                     <path class="cls-4" d="M20.86,45.61c-3.31,.58-7.05,.95-11.28,1.01-.83,.01-1.62-.36-2.33-.79-2.42-1.46-5.57-7.74-7.18-18.65-.38-2.58,.81-5,2.76-6.72,1.27-1.12,3.09-2.28,5.33-3.39,1.36,.59,2.64,1.4,3.65,2.3,.18,.16,.34,.32,.5,.48,3.71,3.82,7.71,12.34,8.42,22.55,.08,1.05,.11,2.12,.11,3.21Z"></path>
                     <path class="cls-2" d="M6.78,22.31c3.46-3.05,12.1-6.51,21.9-6.95,1.63-.07,3.7,.88,4.84,2.06,2.21,2.28,4.61,7.65,4.6,13.93,0,1.98-.64,3.72-2.18,4.97-3.42,2.78-10.62,7.14-23.86,6.57-.65-.03-1.28-.28-1.84-.62-1.9-1.15-4.38-6.09-5.65-14.67-.3-2.03,.64-3.93,2.17-5.29Z"></path>
                     <path class="cls-3" d="M26.11,26.61l-5.61-1.56c-1.31-.36-2.85,.26-3.44,1.38-.23,.43-.29,.9-.17,1.33l1.51,5.69c.3,1.12,1.67,1.73,3.08,1.36,.54-.14,1.04-.42,1.4-.8l4.1-4.13c.96-.96,.86-2.3-.21-2.99-.2-.13-.42-.22-.66-.29h0Z"></path>
                  </svg>
               </div>
               <div class="f-md-32 f-22 w700 white-clr lh150 col-12 mt-md30 mt20 text-center"> 
                  1-Click Software That Uses A Secret Method To Make Us $528/Day Over And Over Again By Creating Self-Updating Entertainment News Site Packaged with Trendy Content &amp; Videos 
               </div>
               <div class="col-12 mt-md30 mt20">
                  <img src="assets/images/proudly.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>
         </div>
      </div>
      <!-- grand bonus update  -->
      <div class="grand">
         <div class="container">
            <div class="col-12 text-center">
               <div class="f-24 f-md-40 lh160 w700 white-clr">Grand Bonus 1 -  Coursova (FE + OTO 3) with reseller license </div>
            </div>
         </div>
      </div>
      <!-- start coursova -->
      <div class="coursova-bg">
         <div class="container">
            <div class="row inner-content">
               <div class="col-12">
                  <div class="col-12 text-center">
                     <img src="assets/images/logo-co-white.png" class="img-responsive mx-xs-center logo-height">
                  </div>
               </div>
               <div class="col-12 text-center px-sm15">
                  <div class="mt20 mt-md50 mb20 mb-md50">
                     <div class="f-md-21 f-20 w500 lh140 text-center white-clr">
                        Turn your passion, hobby, or skill into <u>a profitable e-learning business</u> right from your home…
                     </div>
                  </div>
               </div>
               <div class="col-12">
                  <div class="col-12 heading-co-bg">
                     <div class="f-24 f-md-42 w500 text-center black-clr lh140  line-center rotate-5">								
                        <span class="w700"> Create and Sell Courses on Your Own Branded <br class="d-md-block d-none">E-Learning Marketplace</span>  without Any <br class="visible-lg"> Tech Hassles or Prior Skills
                     </div>
                  </div>
                  <div class="col-12 p0 f-18 f-md-24 w400 text-center  white-clr lh140 mt-md40 mt20">
                     Sell your own courses or <u>choose from 10 done-for-you RED-HOT HD <br class="d-md-block d-none">video courses to start earning right away</u>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-md-7 col-12 mt-sm100 mt20 px-sm15 min-sm-video-width-left">
                  <div class="col-12 p0 mt-md15">
                     <div class="col-12 responsive-video" editabletype="video" style="z-index: 10;">
                        <iframe src="https://coursova.dotcompal.com/video/embed/n56ppjcyr4" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                           box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                     </div>
                  </div>
               </div>
               <div class="col-md-5 col-12 f-20 f-md-20 worksans lh140 w400 white-clr mt-md37 mt20 pl15 min-sm-video-width-right">
                  <ul class="list-head list-head-co pl0 m0">
                     <li>Tap Into Fast-Growing $398 Billion Industry Today</li>
                     <li>Build Beautiful E-Learning Sites with Marketplace, Blog &amp; Members Area in Minutes</li>
                     <li>Easily Create Courses – Add Unlimited Lessons (Video, E-Book &amp; Reports)</li>
                     <li>Accept Payments with PayPal, JVZoo, ClickBank, W+ Etc.</li>
                     <li>No Leads or Profit Sharing With 3rd Party Marketplaces</li>
                     <li>Get Commercial License and Charge Clients Monthly (Yoga Classes, Gym Etc.)</li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      </div>
      <!-- grand bonus update end  -->
      <div class="gr-1">
         <div class="container-fluid p0">
            <picture>
               <img src="assets/images/gr1.png" alt="Flowers" style="width:100%;">
            </picture>
            <picture>
               <img src="assets/images/gr2.png" alt="Flowers" style="width:100%;">
            </picture>
            <picture>
               <img src="assets/images/gr5.png" alt="Flowers" style="width:100%;">
            </picture>
         </div>
      </div>
      </div>
      <!-----ninja kash---->
      <div class="grand">
         <div class="container">
            <div class="col-12 text-center">
               <div class="f-24 f-md-40 lh160 w700 white-clr">Grand Bonus 2 - Ninjashoppe (FE + OTO 3) with reseller license</div>
            </div>
         </div>
      </div>
      <div class="grand-logo-bg">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <img src="assets/images/logo-ninj.png" class="img-fluid mx-auto d-block">
               </div>
            </div>
         </div>
      </div>
      <div class="banner">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class=" col-12 banner-block">
                     <div class="text-center f-md-22  f-16 w400 lh140 mtprehl white-clr">Attention! ANY marketer/blogger who is sick &amp; tired of wasting tons of time in creating content &amp; affiliate sites with ZERO Traffic &amp; Sales?</div>
                     <div class="col-md-12 col-12 mt20 mt-md25 p-md0 px15">
                        <div class="">
                           <div class="f-md-40 f-28 w700 text-center white-clr lh160">Revealing A Breakthrough Software That <span class="under yellow-clr"> Creates Stunning & Self-Updating Entertainment News Site </span> Packaged with Trendy Content & Videos </div>
                        </div>
                     </div>
                     <div class="col-md-12 col-12 text-center mt20 mt-md30">
                        <div class="f-18 w400 text-center lh160 white-clr">
                           <span class="w600">A Secret Method That Makes $528/ Day Over And Over Again</span><br><br>
                           Watch My Quick Review Video
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-8 col-12 mx-auto mt0 mt-md2">
                  <div class="responsive-video">  
                     <object data="https://www.youtube.com/embed/GIoWXaNT5eQ?autoplay=1&amp;controls=0&amp;rel=0&amp;autohide=1&amp;nologo=1&amp;showinfo=0&amp;frameborder=0&amp;theme=light&amp;modestbranding=1"></object>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="gr-1">
         <div class="container-fluid p0">
            <picture>
               <img src="assets/images/gr3.png" alt="Flowers" style="width:100%;">
               <img src="assets/images/gr4.png" alt="Flowers" style="width:100%;">
            </picture>
         </div>
      </div>
      <div class="grand">
         <div class="container">
            <div class="col-12 text-center">
               <div class="f-24 f-md-40 lh160 w700 white-clr">Grand Bonus 3 - AppZilo (FE + OTO 5) with reseller license</div>
            </div>
         </div>
      </div>
      <!--Appzilo Header Section Start -->
      <div class="header-section-appzilo">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-md-left text-center">
                  <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 650.9 161.1" style="enable-background:new 0 0 650.9 161.1; max-height:50px;" xml:space="preserve">
                     <style type="text/css">
                        .st0a{fill:#FFFFFF;}
                        .st1b{fill-rule:evenodd;clip-rule:evenodd;fill:#2089EA;}
                        .st2c{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_1_);}
                        .st3d{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_00000001627455377407541100000004222602826157794230_);}
                     </style>
                     <g>
                        <path class="st0a" d="M221,39.2c4.4-2.4,9.6-3.6,15.5-3.6c6.9,0,13.2,1.8,18.9,5.3c5.6,3.5,10.1,8.5,13.4,15.1
                           c3.3,6.5,4.9,14.1,4.9,22.8c0,8.6-1.6,16.3-4.9,22.9c-3.3,6.6-7.7,11.7-13.3,15.2c-5.6,3.6-11.9,5.4-18.9,5.4
                           c-5.8,0-11-1.2-15.5-3.6c-4.5-2.4-8-5.5-10.5-9.4l0,51.9l-25.8,0l0-124.3l25.8,0l0,11.9C213.1,44.7,216.5,41.6,221,39.2z M242,63.5
                           c-3.6-3.7-8-5.5-13.2-5.5c-5.1,0-9.5,1.9-13,5.6c-3.6,3.7-5.4,8.8-5.4,15.2c0,6.4,1.8,11.5,5.4,15.2c3.6,3.7,7.9,5.6,13,5.6
                           s9.5-1.9,13.1-5.7c3.6-3.8,5.4-8.9,5.4-15.3C247.4,72.2,245.6,67.2,242,63.5z"/>
                        <path class="st0a" d="M323.4,39.2c4.4-2.4,9.6-3.6,15.5-3.6c6.9,0,13.2,1.8,18.9,5.3c5.6,3.5,10.1,8.5,13.4,15.1
                           c3.3,6.5,4.9,14.1,4.9,22.8c0,8.6-1.6,16.3-4.9,22.9c-3.3,6.6-7.7,11.7-13.3,15.2c-5.6,3.6-11.9,5.4-18.9,5.4
                           c-5.8,0-11-1.2-15.5-3.6c-4.5-2.4-8-5.5-10.5-9.4l0,51.9l-25.8,0l0-124.3l25.8,0l0,11.9C315.5,44.7,319,41.6,323.4,39.2z
                           M344.4,63.5c-3.6-3.7-8-5.5-13.2-5.5c-5.1,0-9.5,1.9-13,5.6c-3.6,3.7-5.4,8.8-5.4,15.2c0,6.4,1.8,11.5,5.4,15.2
                           c3.6,3.7,7.9,5.6,13,5.6c5.1,0,9.5-1.9,13.1-5.7c3.6-3.8,5.4-8.9,5.4-15.3C349.8,72.2,348,67.2,344.4,63.5z"/>
                        <path class="st0a" d="M417.1,99.8l45.6,0l0,21.1l-74.8,0l0-19.6l45.2-65.2l-45.3,0l0-21.1l74.8,0l0,19.6L417.1,99.8z"/>
                        <path class="st0a" d="M481.4,24c-2.9-2.7-4.3-6-4.3-9.9c0-4,1.4-7.4,4.3-10c2.9-2.7,6.6-4,11.1-4c4.4,0,8.1,1.3,10.9,4
                           c2.9,2.7,4.3,6,4.3,10c0,3.9-1.4,7.2-4.3,9.9c-2.9,2.7-6.5,4-10.9,4C488,28,484.3,26.6,481.4,24z M505.3,36.7l0,84.2l-25.8,0
                           l0-84.2L505.3,36.7z"/>
                        <path class="st0a" d="M549.8,9.3l0,111.6l-25.8,0l0-111.6L549.8,9.3z"/>
                        <path class="st0a" d="M584.6,116.8c-6.6-3.5-11.8-8.5-15.5-15.1c-3.8-6.5-5.7-14.2-5.7-22.9c0-8.6,1.9-16.3,5.7-22.9
                           c3.8-6.6,9-11.6,15.7-15.2c6.6-3.5,14.1-5.3,22.3-5.3c8.2,0,15.7,1.8,22.3,5.3c6.6,3.5,11.9,8.6,15.7,15.2
                           c3.8,6.6,5.7,14.2,5.7,22.9c0,8.6-1.9,16.3-5.8,22.9c-3.9,6.6-9.2,11.6-15.8,15.2c-6.7,3.5-14.2,5.3-22.4,5.3
                           C598.6,122.1,591.2,120.3,584.6,116.8z M619.4,94.3c3.5-3.6,5.2-8.8,5.2-15.5c0-6.7-1.7-11.9-5.1-15.5c-3.4-3.6-7.5-5.4-12.4-5.4
                           c-5,0-9.2,1.8-12.5,5.4c-3.3,3.6-5,8.8-5,15.6c0,6.7,1.6,11.9,4.9,15.5c3.3,3.6,7.4,5.4,12.3,5.4C611.8,99.8,616,98,619.4,94.3z"/>
                     </g>
                     <g>
                        <path class="st1b" d="M94.8,75.9l-4.6,9.3l-13.3,26.9l-17.1,34.6c-4.4,8.9-12.5,14.4-21.4,14.4H0l41.5-83.6l20-40.3
                           c4.4-0.2,8.8,1.4,12.8,4.6c3.7,2.9,7,7.1,9.7,12.3l0.8,1.7c0,0.1,0.1,0.1,0.1,0.2l3.1,6.2L94.8,75.9z"/>
                        <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="28.8665" y1="152.9736" x2="86.1242" y2="43.0589">
                           <stop  offset="0.5251" style="stop-color:#2089EA"/>
                           <stop  offset="0.6138" style="stop-color:#1D7ADE"/>
                           <stop  offset="0.7855" style="stop-color:#1751C0"/>
                           <stop  offset="1" style="stop-color:#0D1793"/>
                        </linearGradient>
                        <path class="st2c" d="M94.8,75.9l-4.6,9.3l-13.3,26.9l-17.1,34.6c-4.4,8.9-12.5,14.4-21.4,14.4H0l41.5-83.6l20-40.3
                           c4.4-0.2,8.8,1.4,12.8,4.6c3.7,2.9,7,7.1,9.7,12.3l0.8,1.7c0,0.1,0.1,0.1,0.1,0.2l3.1,6.2L94.8,75.9z"/>
                        <linearGradient id="SVGID_00000110437950689616847500000012451418664872649149_" gradientUnits="userSpaceOnUse" x1="116.6752" y1="141.5592" x2="116.6752" y2="0">
                           <stop  offset="0.3237" style="stop-color:#F4833F"/>
                           <stop  offset="0.6981" style="stop-color:#F39242"/>
                           <stop  offset="1" style="stop-color:#F2A246"/>
                        </linearGradient>
                        <path style="fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_00000110437950689616847500000012451418664872649149_);" d="
                           M171.9,141.6h-28.8c-10.9,0-20.9-6.8-26.4-17.7L90.8,71.4l-6-12.2l-2.7-5.4c0-0.1,0-0.1-0.1-0.2c-0.2-0.5-0.5-1-0.7-1.5
                           c-2.3-4.5-5.3-8.2-8.5-10.8c-3.5-2.8-7.4-4.3-11.3-4.1l6.2-12.5l5.9-11.8l1.7-3.4c0,0,0,0,0,0c2.7-4.6,6.5-7.9,11.1-9.1
                           c8.2-2.1,16.6,3.3,21.8,12.8c0,0,0,0,0,0l1.4,2.8c0,0,0,0,0,0l4.3,8.6L171.9,141.6z"/>
                     </g>
                  </svg>
               </div>
               <div class="col-12 text-center mt20 mt-md50">
                  <div class="pre-heading-appzilo f-md-22 f-16 w500 lh140 white-clr">
                     World's #1 & Fastest 1-Click iOS/Android Mobile App Builder At A Low One Time Price!
                  </div>
               </div>
               <div class="col-12 mt-md30 mt20 f-md-40 f-28 w600 text-center white-clr lh140">
                  Brand New 1-Click Cloud App LETS YOU CONVERT YOUR EXISTING WEBSITE INTO AN iOS & Android APP + UNLIMITED MOBILE APPS FOR IOS & ANDROID FROM <u class="stats-headline1">SCRATCH</u>
               </div>
               <div class="col-12 mt-md30 mt20 text-center">
                  <div class="post-heading f-16 f-md-24 w500 text-center lh140 white-clr text-capitalize">
                     Create & Sell Mobile Apps for BIG Profits to Affiliates, Coaches, Attorney, Dentists,<br class="d-none d-md-block"> Gyms, & 10,000+ Other Niches | No Coding or Prior Experience Needed...
                  </div>
               </div>
               <div class="col-12 mt20 mt-md20">
                  <div class="row">
                     <div class="col-md-6 col-12 min-md-video-width-left mt-md20">
                        <!-- <img src="https://cdn.oppyo.com/launches/appzilo/special/product-box.webp" class="img-fluid d-block mx-auto"> -->
                        <div class="col-12 responsive-video">
                           <iframe src="https://appzilo.dotcompal.com/video/embed/7fu418e5f4" style="width:100%; height:100%" frameborder="0" allow="fullscreen" allowfullscreen></iframe>
                        </div>
                     </div>
                     <div class="col-md-6 col-12 mt20 mt-md0 min-md-video-width-right">
                        <div class="key-features-bg-appzilo">
                           <ul class="list-head pl0 m0 f-18 lh150 w400 white-clr">
                              <li>Create Unlimited iOS & Android Apps </li>
                              <li>Comes With Simple Drag & Drop Editor </li>
                              <li>Sell Limitless Apps to Online & Local Businesses </li>
                              <li>Fastest and bug-free delivery.</li>
                              <li>Turn Any Website Into Fully Fledged Apps in Seconds </li>
                              <li>No App Store & Play Store Approval </li>
                              <li>Comes With Pre-Built Templates </li>
                              <li>No Technical Skills Required </li>
                              <li>Google Ads Monetization </li>
                              <li>100% Newbie Friendly </li>
                              <li>One Time Price and Use Forever </li>
                              <li>Work For ANY Business in ANY Niche </li>
                              <li>Comes With Unlimited Commercial License </li>
                              <li>Ultra-Fast Customer Support </li>
                              <li>$200 Refund If Doesn't Work for You </li>
                           </ul>
                        </div>
                        <div class="col-12 mt20 mt-md60 d-md-none">
                           <div class="f-20 f-md-24 w500 lh140 text-center white-clr">
                              (Get Free 30 Reseller License + Commercial License + Special Bonuses)
                           </div>
                           <div class="row">
                              <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center ">
                                 <a href="https://warriorplus.com/o2/buy/lbptdw/h6yjdz/dnp22z" class="cta-link-btn px0 d-block">Get Instant Access To AppZilo</a>
                              </div>
                           </div>
                           <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30 ">
                              <img src="https://cdn.oppyo.com/launches/appzilo/special/compaitable-with1.webp " class="img-fluid d-block mx-xs-center md-img-right ">
                              <div class="d-md-block d-none px-md30 "><img src="https://cdn.oppyo.com/launches/appzilo/special/v-line.webp " class="img-fluid "></div>
                              <img src="https://cdn.oppyo.com/launches/appzilo/special/days-gurantee1.webp " class="img-fluid d-block mx-xs-auto mt15 mt-md0 ">
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="gr-1">
         <div class="container-fluid p0">
            <picture>
               <source media="(min-width:768px)" srcset="assets/images/appzilo-ss.png">
               <source media="(min-width:320px)" srcset="assets/images/appzilo-mview.png">
               <img src="assets/images/appzilo-ss.png" alt="Steps" style="width:100%;">
            </picture>
         </div>
      </div>
      <div class="grand">
         <div class="container">
            <div class="col-12 text-center">
               <div class="f-24 f-md-40 lh160 w700 white-clr">Grand Bonus 4 - MailZilo (FE + OTO 5) with reseller license</div>
            </div>
         </div>
      </div>
      <div class="header-section-mailzilo">
         <div class="container">
            <div class="row">
               <div class="col-md-12  text-center">
                  <svg version="1.1" x="0px" y="0px" viewBox="0 0 1501.16 276.9" style="enable-background:new 0 0 1501.16 276.9; max-height:40px;" xml:space="preserve">
                     <style type="text/css">
                        .st0z{fill:#FFFFFF;}
                     </style>
                     <g>
                        <path class="st0z" d="M122.38,159.77l82.88-67.64h39.5v182.43h-45.44V160.68l-73.53,57.2h-6.82l-73.52-57.2v113.87H0V92.13h39.46
                           L122.38,159.77z"></path>
                        <path class="st0z" d="M391.87,92.13l81.03,182.43h-48.87L409,238.51h-81.7l-13.62,36.04h-48.16l73.69-182.43H391.87z M395.71,206.65
                           l-29.57-70.91l-26.8,70.91H395.71z"></path>
                        <path class="st0z" d="M540.93,92.13v182.43h-45.44V92.13H540.93z"></path>
                        <path class="st0z" d="M722.06,241.12v33.43H582.98V92.13h45.44v149H722.06z"></path>
                        <path class="st0z" d="M913.75,92.13l-99.48,149h99.48v33.43H736.68l100.26-149H743.6V92.13H913.75z"></path>
                        <path class="st0z" d="M989.75,92.13v182.43h-45.44V92.13H989.75z"></path>
                        <path class="st0z" d="M1170.88,241.12v33.43H1031.8V92.13h45.44v149H1170.88z"></path>
                        <path class="st0z" d="M1332.43,209.97c-2.33,5.08-5.42,9.64-9.28,13.71c-9.7,10.23-22.48,15.35-38.35,15.35
                           c-15.94,0-28.8-5.12-38.53-15.35c-9.74-10.22-14.61-23.66-14.61-40.28c0-16.72,4.87-30.17,14.61-40.35
                           c5.49-5.75,11.97-9.87,19.44-12.37l-26.14-31.06c-9.05,4.52-17.19,10.6-24.43,18.22c-17.75,18.72-26.63,40.57-26.63,65.56
                           c0,25.41,8.94,47.36,26.81,65.81c17.87,18.45,41.03,27.68,69.49,27.68c28.13,0,51.18-9.26,69.13-27.81
                           c2.07-2.15,4.04-4.34,5.87-6.58L1332.43,209.97z"></path>
                        <path class="st0z" d="M1501.16,0l-114.63,241.95l-71.11-84.51l-31.99,29.36l2.17-64.79c67.33-7.59,111.49-65.77,112.17-66.67
                           c-41.92,31-125.97,49.4-125.97,49.4l-28.6-33.1L1501.16,0z"></path>
                     </g>
                  </svg>
               </div>
               <div class="col-12 text-center mt20 mt-md50">
                  <div class="pre-heading-mailzilo f-md-22 f-16 w500 lh140 white-clr">
                     Are you Sick &amp; Tired of Paying 100s of Dollars To Old School Autoresponders That Never Deliver?
                  </div>
               </div>
               <div class="col-12 mt-md30 mt20 f-md-45 f-28 w600 text-center white-clr lh140">
                  Get 4X Email Opens &amp; Traffic Using <span class="yellow-clr">the Powerful Tagging Based Email Marketing Platform &amp; 90,000+ Done-For-You Proven Swipes</span> with No Monthly Fee Ever!
               </div>
               <div class="col-12 mt-md30 mt20 text-center">
                  <div class="post-heading-mailzilo f-16 f-md-24 w500 text-center lh140 white-clr text-capitalize">
                     100% Newbie Friendly Platform to Collect &amp; Manage Leads | Send Unlimited Emails <br class="d-none d-md-block">to Get Tons of Traffic, Affiliate Commissions, &amp; Sales on Autopilot
                  </div>
               </div>
               <div class="col-12 mt20 mt-md40">
                  <div class="row">
                     <div class="col-md-7 col-12 min-md-video-width-left">
                        <!-- <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/product-box.webp" class="img-fluid d-block mx-auto"> -->
                        <div class="col-12 responsive-video">
                           <iframe src="https://mailzilo.dotcompal.com/video/embed/caepa779qt" style="width:100%; height:100%" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                        </div>
                     </div>
                     <div class="col-md-5 col-12 mt20 mt-md0 min-md-video-width-right">
                        <div class="key-features-bg-mailzilo">
                           <ul class="list-head pl0 m0 f-18 lh150 w400 white-clr">
                              <li><span class="list-highlight">Send Unlimited Emails</span> to Unlimited Subscribers</li>
                              <li><span class="list-highlight">Collect Unlimited Leads</span> with Built-In Lead Form</li>
                              <li><span class="list-highlight">Free SMTP</span> for unrestricted Mailing</li>
                              <li><span class="list-highlight">Upload Unlimited List</span> with Zero Restrictions</li>
                              <li><span class="list-highlight">Get 4X More ROI</span> and Profits than Ever</li>
                              <li><span class="list-highlight">Smart Tagging</span> for Lead Personalisation</li>
                              <li><span class="list-highlight">100% Control</span> on your online business </li>
                              <li><span class="list-highlight">Built-in Inline</span> Editor to Craft Beautiful Emails</li>
                              <li><span class="list-highlight">Hassle-Free</span> Subscribers Management</li>
                              <li><span class="list-highlight">100+ High Converting</span> Templates for Webforms &amp; Email </li>
                              <li><span class="list-highlight">GDPR &amp; Can-Spam Compliant</span></li>
                              <li><span class="list-highlight">100% Newbie Friendly</span> &amp; Easy to Use</li>
                              <li><span class="list-highlight">Commercial License Included</span></li>
                           </ul>
                        </div>
                        <div class="col-12 mt20 mt-md60 d-md-none">
                           <div class="f-20 f-md-24 w500 lh140 text-center white-clr">
                              Get Free 30 Reseller License + Commercial License <br class="d-none d-md-block"> If You Buy Today!! 
                           </div>
                           <div class="row">
                              <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center ">
                                 <a href="https://warriorplus.com/o2/buy/xgwy6m/y4pjnq/nh90r1" class="cta-link-btn px0 d-block">Get Instant Access To MailZilo</a>
                              </div>
                           </div>
                           <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30 ">
                              <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/compaitable-with1.webp " class="img-fluid d-block mx-xs-center md-img-right ">
                              <div class="d-md-block d-none px-md30 "><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/v-line.webp " class="img-fluid "></div>
                              <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/days-gurantee1.webp " class="img-fluid d-block mx-xs-auto mt15 mt-md0 ">
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="gr-1">
         <div class="container-fluid p0">
            <picture>
               <source media="(min-width:768px)" srcset="assets/images/mailzilo-ss.png">
               <source media="(min-width:320px)" srcset="assets/images/mailzilo-mview.png">
               <img src="assets/images/mailzilo-ss.png" alt="Steps" style="width:100%;">
            </picture>
         </div>
      </div>
      <!-- Bonus Section Header Start -->
      <div class="bonus-header">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-10 mx-auto heading-bg text-center">
                  <div class="f-24 f-md-36 lh160 w700">When You Purchase Entertainerz, You Also Get <br class="d-lg-block d-none"> Instant Access To These 15 Exclusive Bonuses</div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus Section Header End -->
      <!-- Bonus #1 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
               </div>
               <div class="col-12 mt20 mt-md30">
                  <div class="row d-flex align-items-center flex-wrap">
                     <div class="col-12 col-md-5 order-3 order-md-0 text-center">
                        <img src="assets/images/bonus1.webp" class="img-fluid">
                     </div>
                     <div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
                        <div class="text-md-start text-center">
                           <div class="bonus-title-bg mb-4">
                              <div class="f-22 f-md-28 lh120 w700">Bonus 1</div>
                           </div>
                        </div>
                        <div class="f-22 f-md-33 w600 lh160 bonus-title-color">
                           Content Marketing Formula
                        </div>
                        <ul class="bonus-list f-18 f-md-19 lh160 w400 mt20 mt-md30 p0">
                           <li>Content marketing is currently one of the biggest trends in digital marketing as a whole and is an area that many website owners and brands are investing in heavily right now thanks to the impressive returns that they are seeing. </li>
                           <li>But content marketing is a complex and broad term that encompasses a number of different strategies and activities. </li>
                           <li>In order for it to be successful, you need to have a good understanding of what it is, how it works and how you can best adapt it to work for your particular brand.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #1 Section End -->
      <!-- Bonus #2 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="col-12 mt20 mt-md30">
               <div class="row align-items-center flex-wrap">
                  <div class="col-12 col-md-5 order-3 order-md-3 text-center">
                     <img src="assets/images/bonus2.webp" class="img-fluid">
                  </div>
                  <div class="col-12 col-md-7 mt-md0 mt20">
                     <div class=" text-md-start text-center">
                        <div class="bonus-title-bg mb-4">
                           <div class="f-22 f-md-28 lh120 w700">Bonus 2</div>
                        </div>
                     </div>
                     <div class="f-22 f-md-33 w600 lh160 bonus-title-color">
                        Personal Branding Blueprint
                     </div>
                     <ul class="bonus-list f-18 f-md-19 lh160 w400 mt20 mt-md30 p0">
                        <li>If your digital presence is chaotic, your audience will be confused about your product and what you stand for.  </li>
                        <li>To prevent this, you need to have a cohesive, streamlined and high quality social media presence.  </li>
                        <li>To stand out you need to understand your target audience.</li>
                        <li>The Personal Branding Blueprint is the one-stop shop for everything you will need to know to own a successful personal brand.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
      </div>
      <!-- Bonus #2 Section End -->
      <!-- Bonus #3 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row ">
               <div class="col-12 mt20 mt-md30">
                  <div class="row align-items-center flex-wrap">
                     <div class="col-12 col-md-5 order-3 order-md-0 text-center">
                        <img src="assets/images/bonus3.webp" class="img-fluid">
                     </div>
                     <div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
                        <div class="text-center text-md-start">
                           <div class="bonus-title-bg mb-4">
                              <div class="f-22 f-md-28 lh120 w700">Bonus 3</div>
                           </div>
                        </div>
                        <div class="f-22 f-md-33 w600 lh160 bonus-title-color">
                           Affiliate Marketing Mastery
                        </div>
                        <ul class="bonus-list f-18 f-md-19 lh160 w400 mt20 mt-md30 p0">
                           <li>Affiliate marketing is considered to be a part of a billion-dollar online industry and one that will only continue to grow in the upcoming years. </li>
                           <li>No doubt starting a new job is a risky opportunity to take; either you make it, or you break it. But with the new age of modern technology, never underestimate the power you hold! </li>
                           <li>You will learn strategies to generating a life-changing income online & unlock the secrets' to complete affiliate marketing mastery.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #3 Section End -->
      <!-- Bonus #4 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row ">
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center flex-wrap">
                     <div class="col-12 col-md-5 order-3 order-md-3 text-center">
                        <img src="assets/images/bonus4.webp" class="img-fluid">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 ">
                        <div class="text-md-start text-center">
                           <div class="bonus-title-bg mb-4">
                              <div class="f-22 f-md-28 lh120 w700">Bonus 4</div>
                           </div>
                        </div>
                        <div class="f-22 f-md-33 w600 lh160 bonus-title-color">
                           Social Pop-Ups Plugin
                        </div>
                        <ul class="bonus-list f-18 f-md-19 lh160 w400 mt20 mt-md30 p0">
                           <li>With this plugin you can create your own social pop up widget for your WordPress blog! Take advantage of this technique to improve your social conversions.  </li>
                           <li>This product allows your people to conveniently follow,like or subscribe to your social media page to keep informed about updates and new releases. </li>
                           <li>You will be confident when you make changes to your like pop-up box. This is super fun to use and fun for your readers.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #4 End -->
      <!-- Bonus #5 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row ">
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center flex-wrap">
                     <div class="col-12 col-md-5 order-3 order-md-0 text-center ">
                        <img src="assets/images/bonus5.webp" class="img-fluid">
                     </div>
                     <div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
                        <div class="text-md-start text-center">
                           <div class="bonus-title-bg mb-4">
                              <div class="f-22 f-md-28 lh120 w700">Bonus 5</div>
                           </div>
                        </div>
                        <div class="f-22 f-md-33 w600 lh160 bonus-title-color">
                           Graphics Images Creation
                        </div>
                        <ul class="bonus-list f-18 f-md-19 lh160 w400 mt20 mt-md30 p0">
                           In this course you will see exactly what tools to use for you social media activity to create high converting images for different purposes and platforms without losing tons of time or money on outsourcing or expensive designers and freelancers.<br><br>
                           <b>You will learn:</b>
                           <li>Why images are so important for your social media activity </li>
                           <li>How to enhance and transform your photos for your social media accounts. </li>
                           <li>What tools to use for free to enhance and resize your photos for social media.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #5 End -->
      <!-- CTA Button Section Start -->
      <div class="cta-btn-section dark-cta-bg">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center">
                  <div class="f-md-20 f-18 text-center mt3 white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-22 f-22 text-center purple lh120 w700 mt15 mt-md20 orange-clr">TAKE ACTION NOW!</div>
                  <div class="f-md-22 f-17 lh120 w600 mt15 mt-md20 white-clr">Use Coupon Code <span class="w800 orange-clr">"ENTERBIG"</span> with <span class="w800 orange-clr">$5 discount</span> </div>
               </div>
               <div class="col-md-12 col-12 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab Entertainerz + My 15 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               <div class="col-12 mt15 mt-md20 text-center">
                  <img src="assets/images/payment.png" class="img-fluid">
               </div>
               <div class="col-12 mt15 mt-md20" align="center">
                  <h3 class="f-md-28 f-20 w500 text-center white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Days</span> </div>
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                     <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                     <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->
      <!-- Bonus #6 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row align-items-center flex-wrap">
                  <div class="col-12 col-md-5 order-3 order-md-3 text-center">
                     <img src="assets/images/bonus6.webp" class="img-fluid">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="text-center text-md-start">
                        <div class="bonus-title-bg mb-4">
                           <div class="f-22 f-md-28 lh120 w700">Bonus 6</div>
                        </div>
                        <div class="f-22 f-md-33 w600 lh160 bonus-title-color">
                           Auto Video Creator 
                        </div>
                        <ul class="bonus-list f-18 f-md-19 lh160 w400 mt20 mt-md30 p0">
                           <li><b>Discover how to create your own professional videos in a snap! You don't even have to speak ... the software will do it for you!'</b> </li>
                           <li>If you want to build your brand, chances are you need to have a video to show your expertise. </li>
                           <li>But the if you are not good at creating video or you don't have the necessary tools yet to shoot your own video, this cool software will do the stuffs for you.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #6 End -->
      <!-- Bonus #7 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center flex-wrap">
                     <div class="col-12 col-md-5 order-3 order-md-0 text-center">
                        <img src="assets/images/bonus7.webp" class="img-fluid">
                     </div>
                     <div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
                        <div class="text-md-start text-center">
                           <div class="bonus-title-bg mb-4">
                              <div class="f-22 f-md-28 lh120 w700">Bonus 7</div>
                           </div>
                        </div>
                        <div class="f-22 f-md-33 w600 lh160 bonus-title-color">
                           Webloggerz
                        </div>
                        <ul class="bonus-list f-18 f-md-19 lh160 w400 mt20 mt-md30 p0">
                           <li><b>Now you can easily encourage more social users to your blog. This easy to use plugin lets you add your social media accounts on the sidebar of your blog.</b> </li>
                           <li>Almost all premium WordPress Themes comw with inbuilt social media widgets... </li>
                           <li>But is your theme's widgets really fancy enough to encourage more social user values to your blog?</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #7 End -->
      <!-- Bonus #8 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center flex-wrap">
                     <div class="col-12 col-md-5 order-3 order-md-3 text-center">
                        <img src="assets/images/bonus8.webp" class="img-fluid">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="text-center text-md-start">
                           <div class="bonus-title-bg mb-4">
                              <div class="f-22 f-md-28 lh120 w700">Bonus 8</div>
                           </div>
                        </div>
                        <div class="f-22 f-md-33 w600 lh160 bonus-title-color">
                           Goal Setting Video Site Builder Software
                        </div>
                        <ul class="bonus-list f-18 f-md-19 lh160 w400 mt20 mt-md30 p0">
                           <li><b>Instantly Create Your Own Complete Moneymaking Video Site Featuring Adsense and Amazon Ads, Unique Web Pages, SEO Solutions and Much More ...Built Automatically in 2 Minutes Flat!</b> </li>
                           <li>You can't deny the fact that making money online is a really good business model because you may have more time for yourself and to your family as you will be mostly working at the comfort of your own home.</li>
                           <li>The thing is that, there are so many online business model that you can try but one thing that you can also try and work on is having a video-based website that you can monetize.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #8 End -->
      <!-- Bonus #9 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row align-items-center flex-wrap">
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-12 col-md-5 order-3 order-md-0 text-center">
                        <img src="assets/images/bonus9.webp" class="img-fluid">
                     </div>
                     <div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
                        <div class="text-md-start text-center">
                           <div class="bonus-title-bg mb-4">
                              <div class="f-22 f-md-28 lh120 w700">Bonus 9</div>
                           </div>
                        </div>
                        <div class="f-22 f-md-33 w600 lh160 bonus-title-color">					 
                           X-Treme List Build Plugin
                        </div>
                        <ul class="bonus-list f-18 f-md-19 lh160 w400 mt20 mt-md30 p0">
                           <li>Edit every detail on the fly with the simple options panel for each page </li>
                           <li>Customize all of the content areas that are designed to be readable </li>
                           <li>Choose from a variety of colors for the 'call to action' buttons on your page </li>
                           <li>Load up the form code from any service like Aweber, MailChimp, and more</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #9 End -->
      <!-- Bonus #10 start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row align-items-center flex-wrap">
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-12 col-md-5 order-3 order-md-3 text-center">
                        <img src="assets/images/bonus10.webp" class="img-fluid">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="text-md-start text-center">
                           <div class="bonus-title-bg mb-4">
                              <div class="f-22 f-md-28 lh120 w700">Bonus 10</div>
                           </div>
                        </div>
                        <div class="f-22 f-md-33 w600 lh160 bonus-title-color">
                           Lead Book Generator
                        </div>
                        <ul class="bonus-list f-18 f-md-19 lh160 w400 mt20 mt-md30 p0">
                           <li>With this powerful plugin, you can easily integrate Facebook Lead Ads with your autoresponder and have your leads added to your mailing list automatically. </li>
                           <li>This Bonus is for aThis is a premium WP plugin that will open the doors to a highly effective way of building your list and making money quickly with Facebook.This software is for anyone who wants to take their lead generation efforts to the next level and build a profitable business.  </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #10 End -->
      <!-- CTA Button Section Start -->
      <div class="cta-btn-section dark-cta-bg">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center">
                  <div class="f-md-20 f-18 text-center mt3 white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-22 f-22 text-center purple lh120 w700 mt15 mt-md20 orange-clr">TAKE ACTION NOW!</div>
                  <div class="f-md-22 f-17 lh120 w600 mt15 mt-md20 white-clr">Use Coupon Code <span class="w800 orange-clr">"ENTERBIG"</span> with <span class="w800 orange-clr">$5 discount</span> </div>
               </div>
               <div class="col-md-12 col-12 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab Entertainerz + My 15 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               <div class="col-12 mt15 mt-md20 text-center">
                  <img src="assets/images/payment.png" class="img-fluid">
               </div>
               <div class="col-12 mt15 mt-md20" align="center">
                  <h3 class="f-md-28 f-20 w500 text-center white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Days</span> </div>
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                     <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                     <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->
      <!-- Bonus #11 start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center flex-wrap">
                     <div class="col-12 col-md-5 order-3 order-md-0 text-center">
                        <img src="assets/images/bonus11.webp" class="img-fluid">
                     </div>
                     <div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
                        <div class="text-md-start text-center">
                           <div class="bonus-title-bg mb-4">
                              <div class="f-22 f-md-28 lh120 w700">Bonus 11</div>
                           </div>
                        </div>
                        <div class="f-22 f-md-34 w600 lh160 bonus-title-color">
                           Quick Cash Traffic System
                        </div>
                        <ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
                           <li>Discover How to Get Instant Traffic and Leads For Your Business with No Complicated, Confusing and Expensive Strategies! </li>
                           <li>Have you ever come across a sales page, decided up front you didn't need the product, but then decided to read the letter anyway? And then not five minutes later you were shocked to find a Paypal receipt sitting in your email inbox... and you're sitting there with a somewhat hazy recollection of what just happened. </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #11 End -->
      <!-- Bonus #12 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center flex-wrap">
                     <div class="col-12 col-md-5 order-3 order-md-3 text-center">
                        <img src="assets/images/bonus12.webp" class="img-fluid">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="text-md-start text-center">
                           <div class="bonus-title-bg mb-4">
                              <div class="f-22 f-md-28 lh120 w700">Bonus 12</div>
                           </div>
                        </div>
                        <div class="f-22 f-md-32 w600 lh160 bonus-title-color">
                           Sales Factory
                        </div>
                        <ul class="bonus-list f-18 f-md-19 lh160 w400 mt20 mt-md30 p0">
                           <li>Creating Stunning Sales Videos Has Just Become So Easy That You Will Jump Off The Chair After You Figure Out How It Works Without Any Technical or Artistic Knowledge Whatsoever </li>
                           <li>Creating good looking "Presentation Style" sales videos USED to require you to have PowerPoint </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #12 End -->
      <!-- Bonus #13 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center flex-wrap">
                     <div class="col-12 col-md-5 order-3 order-md-0 text-center">
                        <img src="assets/images/bonus13.webp" class="img-fluid">
                     </div>
                     <div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
                        <div class="text-md-start text-center">
                           <div class="bonus-title-bg mb-4">
                              <div class="f-22 f-md-28 lh120 w700">Bonus 13</div>
                           </div>
                        </div>
                        <div class="f-22 f-md-32 w600 lh160 bonus-title-color">
                           Conversion Booster
                        </div>
                        <ul class="bonus-list f-18 f-md-19 lh160 w400 mt20 mt-md30 p0">
                           <li>Uncover the real truth about testing and tweaking! There are only a handful of things worth focusing on. The rest is a complete waste of time. Find out more...</li>
                           <li>Discover the different type of opt-in boxes and how each one can be used as powerful 'lead bait'! All you really need to know about squeeze pages. How to construct them, how to maximize your opt-in rates and what to avoid!</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #13 End -->
      <!-- Bonus #14 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center flex-wrap">
                     <div class="col-12 col-md-5 order-3 order-md-3 text-center">
                        <img src="assets/images/bonus14.webp" class="img-fluid">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="text-md-start text-center">
                           <div class="bonus-title-bg mb-4">
                              <div class="f-22 f-md-28 lh120 w700">Bonus 14</div>
                           </div>
                        </div>
                        <div class="f-22 f-md-32 w600 lh160 bonus-title-color">
                           The Copywriter's Handbook
                        </div>
                        <ul class="bonus-list f-18 f-md-19 lh160 w400 mt20 mt-md30 p0">
                           <li>The ability to turn words into gold is probably the most important aspect of any marketer. If you can do this, it does not matter where you are in the world, you can make money from anywhere, anytime just from your words. </li>
                           <li>Today, modern entrepreneurs are making a killing using copywriting techniques in their businesses.</li>
                           <li>With this ebook you will learn everything you need to know about selling effectively on the internet.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #14 End -->
      <!-- Bonus #15 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center flex-wrap">
                     <div class="col-12 col-md-5 order-3 order-md-0 text-center">
                        <img src="assets/images/bonus15.webp" class="img-fluid">
                     </div>
                     <div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
                        <div class="text-md-start text-center">
                           <div class="bonus-title-bg mb-4">
                              <div class="f-22 f-md-28 lh120 w700">Bonus 15</div>
                           </div>
                        </div>
                        <div class="f-22 f-md-32 w600 lh160 bonus-title-color">
                           WP Youtube Leads Plugin
                        </div>
                        <ul class="bonus-list f-18 f-md-19 lh160 w400 mt20 mt-md30 p0">
                           <li>With this plugin you can enhance the user engagement of your Youtube videos and increase your mailing list. </li>
                           <li>Integrate any YouTube video and start converting right away. Use the time-stamps with the video to maximize interest and action. </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #15 End -->
      <!-- Huge Woth Section Start -->
      <div class="huge-area mt30 mt-md10">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="f-md-60 f-30 lh120 w700 white-clr">That's Huge Worth of</div>
                  <br>
                  <div class="f-md-60 f-30 lh120 w800 purple">$2255!</div>
               </div>
            </div>
         </div>
      </div>
      <!-- Huge Worth Section End -->
      <!-- text Area Start -->
      <div class="white-section pb-0">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center p0">
                  <div class="f-md-36 f-25 lh160 w400">So what are you waiting for? You have a great opportunity <br class="d-lg-block d-none"> ahead + <span class="w600">My 15 Bonus Products</span> are making it a <br class="d-lg-block d-none"> <b>completely NO Brainer!!</b></div>
               </div>
            </div>
         </div>
      </div>
      <!-- text Area End -->
      <!-- CTA Button Section Start -->
      <div class="cta-btn-section pt-3 ">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab Entertainerz + My 15 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               <div class="col-12 mt15 mt-md20" align="center">
                  <h3 class="f-md-30 f-20 w500 text-center black">My Exclusive Bonuses Are Expiring in...</h3>
               </div>
               <!-- Timer -->
               <div class="col-12 text-center">
                  <div class="countdown counter-black">
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                     <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                     <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->
      <!--Footer Section Start -->
      <div class="footer-section">
         <div class="container ">
            <div class="row">
               <div class="col-12 text-center">
                  <svg id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 189.77 50" style="max-height:100px">
                     <defs>
                        <style>.cls-1{fill:url(#linear-gradient-2);}.cls-2{fill:#fff;}.cls-3{fill:#001730;}.cls-4{fill:url(#linear-gradient-3);}.cls-5{fill:url(#linear-gradient);}</style>
                        <linearGradient id="linear-gradient" x1="20.65" y1="26.83" x2="42.69" y2="26.83" gradientUnits="userSpaceOnUse">
                           <stop offset="0" stop-color="#bf003b"/>
                           <stop offset="1" stop-color="#e23940"/>
                        </linearGradient>
                        <linearGradient id="linear-gradient-2" x1=".12" y1="29.26" x2="25.92" y2="29.26" gradientUnits="userSpaceOnUse">
                           <stop offset="0" stop-color="#f38227"/>
                           <stop offset="1" stop-color="#fbb826"/>
                        </linearGradient>
                        <linearGradient id="linear-gradient-3" x1="3.09" y1="36.12" x2="22.33" y2="28.06" gradientUnits="userSpaceOnUse">
                           <stop offset="0" stop-color="#3369e9"/>
                           <stop offset="1" stop-color="#002b7f"/>
                        </linearGradient>
                     </defs>
                     <path class="cls-2" d="M22.29,0c-.94,0-1.69,.76-1.69,1.69,0,.75,.48,1.39,1.16,1.61l-2.95,10.01-.61,2.09,2.13,.26,.41-2.75v-.03s1.44-9.5,1.44-9.5c.03,0,.07,0,.11,0,.93,0,1.69-.76,1.69-1.7s-.76-1.69-1.69-1.69Z"/>
                     <path class="cls-2" d="M16.2,15.76l-1.74,.96-5.3-6.31c-.29,.23-.64,.36-1.03,.36-.94,0-1.7-.75-1.7-1.69s.76-1.69,1.7-1.69,1.69,.76,1.69,1.69c0,.41-.15,.79-.4,1.08l6.78,5.6Z"/>
                     <g>
                        <path class="cls-2" d="M63.68,34.98c0,.14-.04,.35-.11,.62-.08,.34-.13,.55-.15,.62-.03,.32-.09,.79-.19,1.41-.05,.16-.16,.24-.35,.24-.09,0-.26-.02-.51-.05-1.1-.16-2.82-.24-5.13-.24-.66,0-1.64,.02-2.96,.05s-2.31,.05-2.96,.05c-.3,0-.44-.13-.44-.39,0-1.01,.06-2.52,.17-4.53,.11-2.02,.17-3.53,.17-4.53s-.05-2.93-.16-5.72c-.02-.72-.07-1.79-.15-3.22v-.19c-.02-.21,.09-.31,.34-.31,.63,0,1.57,.02,2.83,.05,1.26,.04,2.2,.05,2.83,.05s1.5-.02,2.7-.05c1.2-.04,2.1-.05,2.7-.05,.42,0,.65,.13,.69,.39,.19,1.19,.28,1.92,.28,2.21,0,.21-.1,.31-.3,.31-.76,0-1.92-.04-3.47-.13-1.55-.09-2.71-.13-3.5-.13-.74,0-1.2,.1-1.39,.3-.19,.2-.29,.69-.29,1.47v2.01c0,.55,.01,.87,.04,.96,.08,.28,.31,.42,.7,.42,.11,0,.26,0,.46-.01h.46c1.76,0,3.34-.03,4.74-.09,.55-.02,1.19-.06,1.94-.13,.19-.02,.28,.06,.28,.24,0,.29-.03,.73-.1,1.32-.07,.59-.1,1.04-.1,1.33,0,.16-.13,.24-.4,.24-.11,0-.51-.03-1.2-.09-.5-.05-1.63-.08-3.39-.08-1.48,0-2.42,.02-2.82,.05-.36,.03-.57,.22-.62,.57,0-.06,.01,.28,.01,1.04v1.99c0,.93,.14,1.52,.43,1.75,.22,.19,.79,.28,1.68,.28h1.48c.63-.01,2.39-.12,5.29-.34h.2c.18-.03,.27,.08,.27,.32Z"/>
                        <path class="cls-2" d="M77.61,37.42c0,.2-.09,.3-.27,.3-.3,0-.74,0-1.33-.01-.59,0-1.03-.01-1.33-.01-.16,0-.24-.13-.24-.39,0-.44,.01-1.11,.04-2.01,.03-.9,.04-1.57,.04-2.02,0-.36,0-.9-.01-1.61,0-.71-.01-1.25-.01-1.61,0-1.02-.15-1.75-.44-2.17-.36-.5-1.02-.75-1.99-.75-.45,0-1.07,.25-1.87,.75-.84,.53-1.27,1-1.27,1.41v8.08c0,.22-.09,.34-.26,.34-.29,0-.72,0-1.29-.01-.57,0-1.01-.01-1.29-.01-.19,0-.28-.1-.28-.31,0-.71,.01-1.77,.03-3.19,.02-1.42,.03-2.49,.03-3.21,0-1.91-.2-3.68-.61-5.29-.03-.08-.04-.14-.04-.18,0-.09,.06-.15,.18-.19,.06,0,.58-.09,1.54-.25,.97-.16,1.47-.24,1.52-.24,.05,0,.09,.07,.11,.22,.04,.56,.15,1.12,.31,1.68,.47-.37,1.08-.83,1.85-1.37,.94-.57,1.85-.86,2.73-.86,1.69,0,2.84,.49,3.46,1.47,.46,.72,.69,1.88,.69,3.49,0,.28,0,.7-.02,1.28-.01,.57-.02,1.01-.02,1.29,0,.6,0,1.5,.03,2.69,.02,1.19,.03,2.09,.03,2.69Z"/>
                        <path class="cls-2" d="M87.67,35.21c0,.2-.02,.47-.05,.81-.05,.42-.07,.7-.08,.82,0,.16-.02,.26-.04,.3-.04,.06-.12,.13-.26,.19-.74,.35-1.89,.53-3.48,.53-2.1,0-3.15-.95-3.15-2.86,0-.76,.02-1.91,.05-3.44,.04-1.53,.05-2.68,.05-3.44,0-.37-.18-.57-.55-.59-.33,0-.66,0-.98-.01-.19-.04-.28-.41-.28-1.1,0-.26,.02-.53,.05-.82,.03-.22,.17-.35,.43-.39,.22,0,.44,0,.66-.01,.41-.02,.62-.2,.62-.55s-.01-.91-.03-1.64c-.02-.74-.03-1.29-.03-1.66,0-.66,.13-.98,.38-.98,.08,0,.92,.14,2.53,.42,.21,.03,.31,.13,.31,.3,0,.4-.03,.99-.09,1.78-.06,.79-.09,1.38-.09,1.78,0,.26,.12,.39,.36,.39h3.23c.15,0,.23,.05,.23,.16s-.02,.28-.05,.52c-.03,.24-.05,.42-.05,.53,0,.13,0,.34,.01,.62,0,.28,.01,.48,.01,.62,0,.17-.13,.26-.38,.26-.36,0-.9-.02-1.63-.07-.73-.05-1.27-.07-1.63-.07-.07,0-.13,.26-.16,.77-.05,.64-.07,1.53-.07,2.68v2.01c0,.77,.07,1.31,.22,1.62,.22,.47,.66,.7,1.33,.7,.28,0,.69-.06,1.23-.18s.94-.18,1.2-.18c.11,0,.16,.08,.16,.24Z"/>
                        <path class="cls-2" d="M101.01,30.46c0,.66-.29,1.11-.86,1.33-.39,.14-3.17,.41-8.34,.79,.12,.79,.55,1.46,1.31,2.02,.72,.53,1.49,.79,2.32,.79,1.42,0,2.72-.46,3.91-1.37,.14-.12,.29-.23,.43-.35,.07,0,.12,0,.14,.03,.02,.02,.19,.33,.51,.94,.31,.61,.47,.93,.47,.98,0,.05-.07,.16-.22,.31-1.32,1.4-3.01,2.1-5.08,2.1s-3.74-.62-4.98-1.87c-1.24-1.24-1.86-2.9-1.86-4.98,0-1.85,.59-3.46,1.78-4.84,1.25-1.43,2.78-2.14,4.59-2.14,1.69,0,3.1,.62,4.23,1.86,1.1,1.2,1.66,2.67,1.66,4.39Zm-3.21-.82c0-.75-.29-1.41-.88-1.98s-1.25-.86-1.99-.86c-.83,0-1.56,.33-2.2,1-.64,.66-.96,1.41-.96,2.24,0,.19,.16,.28,.48,.28,1.33,0,2.92-.13,4.77-.4,.52-.07,.78-.16,.78-.27Z"/>
                        <path class="cls-2" d="M110.6,25.04c0,.99-.07,1.89-.2,2.69-.03,.21-.12,.31-.27,.31-.19,0-.47-.02-.86-.06-.38-.04-.67-.06-.87-.06-1.13,0-1.85,.32-2.17,.96-.19,.39-.28,1.19-.28,2.41v1.48c0,.5,.02,1.27,.06,2.29,.04,1.02,.06,1.79,.06,2.29,0,.24-.1,.36-.31,.36-.28,0-.7,0-1.27-.01-.57,0-.99-.01-1.27-.01-.21,0-.31-.11-.31-.34,0-.73,.01-1.82,.03-3.29,.02-1.46,.03-2.56,.03-3.3,0-2.22-.21-4-.62-5.35-.02-.07-.03-.13-.03-.16,0-.11,.06-.18,.18-.23,.33-.04,.8-.09,1.4-.16,.97-.18,1.42-.27,1.35-.27,.14,0,.24,.26,.28,.79,.04,.53,.13,.79,.24,.79,.02,0,.04,0,.07-.03,.37-.23,.74-.47,1.1-.7,.4-.24,.82-.42,1.24-.54,.35-.11,.8-.16,1.35-.16,.7,0,1.05,.09,1.05,.28Z"/>
                        <path class="cls-2" d="M119.58,35.21c0,.2-.02,.47-.05,.81-.05,.42-.07,.7-.08,.82,0,.16-.02,.26-.04,.3-.04,.06-.12,.13-.26,.19-.74,.35-1.89,.53-3.48,.53-2.1,0-3.15-.95-3.15-2.86,0-.76,.02-1.91,.05-3.44,.04-1.53,.05-2.68,.05-3.44,0-.37-.18-.57-.55-.59-.33,0-.66,0-.98-.01-.19-.04-.28-.41-.28-1.1,0-.26,.02-.53,.05-.82,.03-.22,.17-.35,.43-.39,.22,0,.44,0,.66-.01,.41-.02,.62-.2,.62-.55s-.01-.91-.03-1.64c-.02-.74-.03-1.29-.03-1.66,0-.66,.13-.98,.38-.98,.08,0,.92,.14,2.53,.42,.21,.03,.31,.13,.31,.3,0,.4-.03,.99-.09,1.78-.06,.79-.09,1.38-.09,1.78,0,.26,.12,.39,.36,.39h3.23c.15,0,.23,.05,.23,.16s-.02,.28-.05,.52c-.03,.24-.05,.42-.05,.53,0,.13,0,.34,.01,.62,0,.28,.01,.48,.01,.62,0,.17-.13,.26-.38,.26-.36,0-.9-.02-1.63-.07-.73-.05-1.27-.07-1.63-.07-.07,0-.13,.26-.16,.77-.05,.64-.07,1.53-.07,2.68v2.01c0,.77,.07,1.31,.22,1.62,.22,.47,.66,.7,1.33,.7,.28,0,.69-.06,1.23-.18s.94-.18,1.2-.18c.11,0,.16,.08,.16,.24Z"/>
                        <path class="cls-2" d="M134.6,25.5s-.01,.12-.04,.24c-.35,2.06-.53,3.83-.53,5.32,0,.09,.11,2.11,.34,6.05v.19c.02,.26-.1,.39-.34,.39s-.62,.03-1.12,.09c-.5,.06-.87,.09-1.11,.09-.17,0-.3-.31-.39-.94-.09-.63-.18-.94-.26-.94-.05,0-.3,.18-.73,.53-.53,.43-1.02,.76-1.48,.98-.71,.36-1.42,.54-2.13,.54-1.74,0-3.21-.7-4.4-2.09-1.13-1.33-1.7-2.88-1.7-4.65,0-1.99,.57-3.64,1.71-4.94,1.19-1.36,2.77-2.03,4.71-2.03,1.37,0,2.57,.48,3.6,1.43,.15,.18,.39,.44,.71,.78,.03,.03,.05,.04,.08,.04,.05,0,.13-.28,.23-.85,.1-.57,.22-.85,.38-.85,.24,0,.7,.07,1.36,.2,.74,.16,1.1,.31,1.1,.43Zm-3.43,5.63c0-1.1-.33-2.05-.98-2.84-.69-.85-1.58-1.28-2.65-1.28s-1.98,.42-2.69,1.27c-.69,.81-1.04,1.76-1.04,2.86s.35,2.03,1.04,2.83c.72,.84,1.62,1.25,2.69,1.25s1.93-.43,2.64-1.28c.66-.8,1-1.73,1-2.8Z"/>
                        <path class="cls-2" d="M140.3,21.58c0,1.1-.62,1.66-1.85,1.66s-1.87-.55-1.87-1.66c0-.49,.19-.9,.58-1.23,.36-.31,.79-.46,1.29-.46s.94,.16,1.3,.47c.36,.31,.55,.72,.55,1.21Zm-.09,3.65c0,.69-.04,1.74-.13,3.13-.09,1.4-.13,2.44-.13,3.15,0,.66,.01,1.64,.04,2.94,.03,1.31,.04,2.28,.04,2.93,0,.19-.08,.28-.24,.28h-2.63c-.19,0-.28-.18-.28-.55,0-.62,.02-1.55,.05-2.8,.03-1.24,.05-2.18,.05-2.81,0-.7-.05-1.75-.16-3.15s-.15-2.45-.15-3.15c0-.15,.09-.23,.28-.23,.16,0,.41,.02,.75,.07,.34,.04,.59,.07,.76,.07s.42-.02,.76-.07c.34-.04,.59-.07,.76-.07,.15,0,.23,.09,.23,.26Z"/>
                        <path class="cls-2" d="M154.74,37.42c0,.2-.09,.3-.27,.3-.3,0-.74,0-1.33-.01-.59,0-1.03-.01-1.33-.01-.16,0-.24-.13-.24-.39,0-.44,.01-1.11,.04-2.01,.03-.9,.04-1.57,.04-2.02,0-.36,0-.9-.01-1.61,0-.71-.01-1.25-.01-1.61,0-1.02-.15-1.75-.44-2.17-.36-.5-1.02-.75-1.99-.75-.45,0-1.07,.25-1.87,.75-.84,.53-1.27,1-1.27,1.41v8.08c0,.22-.09,.34-.26,.34-.29,0-.72,0-1.29-.01-.57,0-1.01-.01-1.29-.01-.19,0-.28-.1-.28-.31,0-.71,.01-1.77,.03-3.19,.02-1.42,.03-2.49,.03-3.21,0-1.91-.2-3.68-.61-5.29-.03-.08-.04-.14-.04-.18,0-.09,.06-.15,.18-.19,.06,0,.58-.09,1.54-.25,.97-.16,1.47-.24,1.52-.24,.05,0,.09,.07,.11,.22,.04,.56,.15,1.12,.31,1.68,.47-.37,1.08-.83,1.85-1.37,.94-.57,1.85-.86,2.73-.86,1.69,0,2.84,.49,3.46,1.47,.46,.72,.69,1.88,.69,3.49,0,.28,0,.7-.02,1.28-.01,.57-.02,1.01-.02,1.29,0,.6,0,1.5,.03,2.69,.02,1.19,.03,2.09,.03,2.69Z"/>
                        <path class="cls-2" d="M168.7,30.46c0,.66-.29,1.11-.86,1.33-.39,.14-3.17,.41-8.34,.79,.12,.79,.55,1.46,1.31,2.02,.72,.53,1.49,.79,2.32,.79,1.42,0,2.72-.46,3.91-1.37,.14-.12,.29-.23,.43-.35,.07,0,.12,0,.14,.03,.02,.02,.19,.33,.51,.94,.31,.61,.47,.93,.47,.98,0,.05-.07,.16-.22,.31-1.32,1.4-3.01,2.1-5.08,2.1s-3.74-.62-4.98-1.87c-1.24-1.24-1.86-2.9-1.86-4.98,0-1.85,.59-3.46,1.78-4.84,1.25-1.43,2.78-2.14,4.59-2.14,1.69,0,3.1,.62,4.23,1.86,1.1,1.2,1.66,2.67,1.66,4.39Zm-3.21-.82c0-.75-.29-1.41-.88-1.98s-1.25-.86-1.99-.86c-.83,0-1.56,.33-2.2,1-.64,.66-.96,1.41-.96,2.24,0,.19,.16,.28,.48,.28,1.33,0,2.92-.13,4.77-.4,.52-.07,.78-.16,.78-.27Z"/>
                        <path class="cls-2" d="M178.29,25.04c0,.99-.07,1.89-.2,2.69-.03,.21-.12,.31-.27,.31-.19,0-.47-.02-.86-.06-.38-.04-.67-.06-.87-.06-1.13,0-1.85,.32-2.17,.96-.19,.39-.28,1.19-.28,2.41v1.48c0,.5,.02,1.27,.06,2.29s.06,1.79,.06,2.29c0,.24-.1,.36-.31,.36-.28,0-.7,0-1.27-.01-.57,0-.99-.01-1.27-.01-.21,0-.31-.11-.31-.34,0-.73,.01-1.82,.03-3.29,.02-1.46,.03-2.56,.03-3.3,0-2.22-.21-4-.62-5.35-.02-.07-.03-.13-.03-.16,0-.11,.06-.18,.18-.23,.33-.04,.8-.09,1.4-.16,.97-.18,1.42-.27,1.35-.27,.14,0,.24,.26,.28,.79,.04,.53,.13,.79,.24,.79,.02,0,.04,0,.07-.03,.37-.23,.74-.47,1.1-.7,.4-.24,.82-.42,1.24-.54,.35-.11,.8-.16,1.35-.16,.7,0,1.05,.09,1.05,.28Z"/>
                        <path class="cls-2" d="M189.77,35.13s-.07,.36-.2,.96c-.14,.64-.22,1.03-.24,1.19-.04,.31-.12,.47-.24,.47-.31,0-.74-.02-1.27-.05-.6-.04-1.02-.07-1.25-.08-.6-.02-1.46-.03-2.59-.03-.87,0-1.84,.04-2.9,.11-1.33,.09-1.9,.13-1.71,.13-.19,0-.31-.08-.36-.24-.13-.43-.19-.93-.19-1.51,0-.08,0-.2,.01-.34s.01-.26,.01-.33c0-.13,.1-.28,.29-.45,.19-.18,1.15-1.19,2.88-3.05,.87-.91,2.11-2.3,3.7-4.19h-5.94c-.14,0-.22-.09-.22-.28,0-.26,.06-.65,.17-1.17,.11-.52,.17-.9,.17-1.15,0-.11,.13-.16,.38-.16,.5,0,1.25,.02,2.25,.07s1.75,.07,2.25,.07c2.46,0,3.85,.02,4.16,.05,.22,.03,.34,.12,.34,.28,0,1.36-.05,2.12-.16,2.29-.09,.13-.87,1.01-2.33,2.65-.94,1.07-2.37,2.66-4.27,4.77,2.99-.08,5.31-.12,6.95-.12,.21,0,.31,.04,.31,.12Z"/>
                     </g>
                     <path class="cls-2" d="M16.61,49.17c.96-1.64,6.11-6.32,13.48-6.77,2.78-.17,5.12,.34,6.8,.88,0,0,.84,.29,.15,.94s-11.41-1.5-19.36,5.17c-1.6,1.37-1.2,.01-1.07-.22Z"/>
                     <path class="cls-5" d="M42.69,31.94c-.01,2.52-.67,4.93-2.77,6.32-1.62,1.06-3.89,2.45-6.95,3.79,.63-1.27,.93-2.75,.99-4.28,.01-.21,.01-.43,.01-.64,.01-9.53-3.63-17.69-6.99-21.14-.16-.16-.32-.32-.5-.47-1.51-1.33-3.74-2.37-5.74-2.61-.03,0-.06-.01-.09-.01,.03-.01,.07-.01,.1-.02,3.12-.66,6.47-1.1,9.93-1.26,2.07-.09,4.7,1.12,6.16,2.62,2.81,2.89,5.86,9.72,5.85,17.7Z"/>
                     <path class="cls-1" d="M33.97,37.13c0,.21,0,.43-.01,.64-.06,1.53-.36,3.01-.99,4.28-.27,.12-.54,.23-.82,.35-3.01,1.24-6.73,2.41-11.29,3.21v-.21c0-.47-.01-.95-.03-1.41-.02-.5-.04-.99-.07-1.48-.01-.04-.01-.07-.01-.11-.03-.46-.07-.91-.12-1.36-.02-.25-.05-.49-.08-.74-.08-.69-.17-1.36-.28-2.03-.07-.45-.15-.89-.23-1.32-.01-.08-.03-.17-.05-.25-.1-.54-.22-1.08-.34-1.6-.03-.13-.06-.26-.09-.39-.11-.44-.22-.87-.34-1.3-.11-.44-.24-.87-.37-1.29-.06-.2-.12-.4-.19-.6-.14-.44-.28-.88-.44-1.3-.18-.51-.37-1.01-.56-1.5-.25-.62-.51-1.22-.77-1.8-.6-1.29-1.23-2.47-1.88-3.53-.12-.2-.25-.4-.38-.59-.12-.2-.25-.39-.38-.57-.63-.92-1.28-1.72-1.92-2.38-.16-.16-.33-.33-.51-.48-.02-.03-.05-.05-.08-.08-.06-.05-.12-.1-.18-.15-.45-.38-.95-.74-1.48-1.07-.14-.09-.28-.17-.42-.25-.48-.28-.98-.53-1.49-.75,1.46-.71,3.09-1.4,4.87-2.04,.53-.18,1.07-.37,1.62-.54,1.32-.43,2.71-.82,4.15-1.18,.6-.14,1.22-.28,1.84-.41,.03,0,.06,.01,.09,.01,2,.24,4.23,1.28,5.74,2.61,.18,.15,.34,.31,.5,.47,3.36,3.45,7,11.61,6.99,21.14Z"/>
                     <path class="cls-4" d="M20.86,45.61c-3.31,.58-7.05,.95-11.28,1.01-.83,.01-1.62-.36-2.33-.79-2.42-1.46-5.57-7.74-7.18-18.65-.38-2.58,.81-5,2.76-6.72,1.27-1.12,3.09-2.28,5.33-3.39,1.36,.59,2.64,1.4,3.65,2.3,.18,.16,.34,.32,.5,.48,3.71,3.82,7.71,12.34,8.42,22.55,.08,1.05,.11,2.12,.11,3.21Z"/>
                     <path class="cls-2" d="M6.78,22.31c3.46-3.05,12.1-6.51,21.9-6.95,1.63-.07,3.7,.88,4.84,2.06,2.21,2.28,4.61,7.65,4.6,13.93,0,1.98-.64,3.72-2.18,4.97-3.42,2.78-10.62,7.14-23.86,6.57-.65-.03-1.28-.28-1.84-.62-1.9-1.15-4.38-6.09-5.65-14.67-.3-2.03,.64-3.93,2.17-5.29Z"/>
                     <path class="cls-3" d="M26.11,26.61l-5.61-1.56c-1.31-.36-2.85,.26-3.44,1.38-.23,.43-.29,.9-.17,1.33l1.51,5.69c.3,1.12,1.67,1.73,3.08,1.36,.54-.14,1.04-.42,1.4-.8l4.1-4.13c.96-.96,.86-2.3-.21-2.99-.2-.13-.42-.22-.66-.29h0Z"/>
                  </svg>
                  <div class="f-14 f-md-16 w400 mt20 lh160 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-14 f-md-16 w400 lh160 white-clr text-xs-center">Copyright © Entertainerz 2022</div>
                  <ul class="footer-ul w400 f-14 f-md-16 white-clr text-center text-md-right">
                     <li><a href="https://support.bizomart.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://entertainerz.co/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://entertainerz.co/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://entertainerz.co/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://entertainerz.co/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://entertainerz.co/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://entertainerz.co/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!--Footer Section End -->
      <!-- Exit Pop-Box Start -->
      <!-- Load UC Bounce Modal CDN Start -->
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/ouibounce/0.0.11/ouibounce.min.js"></script>
      <!-- <div id="ouibounce-modal" style="z-index:77777; display:none;">
         <div class="underlay"></div>
         	<div class="modal popupbg" style="display:block; ">
             <div class="modal-header" style="border:0; padding:0;">
                 <button type="button" class="close" onclick="document.getElementById('ouibounce-modal').style.display = 'none';">&times;</button>
            	</div>
         
             <div class="modal-body">
                 <div class="innerbody">
                     <div class="col-lg-12 col-md-12 col-md-12 col-md-offset-0 col-12 col-offset-0 mt2 xsmt2">
                         <img src="assets/images/waitimg.png" class="img-fluid mt2 xsmt2">
                         <h2 class="text-center mt2 w500 sm25 xs20 lh160">I HAVE SOMETHING SPECIAL<br /> FOR YOU</h2>
                         <a href="<?php echo $_GET['afflink']; ?>" class="createfree md20 sm18 xs16" style="color:#ffffff;">Stay</a>
                     </div>
                 </div>
             </div>
         </div>
         </div> -->
      <div class="modal fade in" id="ouibounce-modal" role="dialog" style="display:none; overflow-y:auto;">
         <div class="modal-dialog modal-dg">
            <button type="button" class="close" onclick="document.getElementById('ouibounce-modal').style.display = 'none';">&times;</button>	 
            <div class="col-12 modal-content pop-bg pop-padding">
               <div class="col-12 modal-body text-center border-pop px0">
                  <div class="col-12 px0">
                     <img src="assets/images/waitimg.png" class="img-fluid">
                  </div>
                  <div class="col-12 text-center mt20 mt-md25 px0">
                     <div class="f-20 f-md-28 lh160 w600">I HAVE SOMETHING SPECIAL <br class="d-lg-block d-none"> FOR YOU</div>
                  </div>
                  <div class="col-12 mt20 mt-md20">
                     <div class="link-btn">
                        <a href="<?php echo $_GET['afflink'];?>">STAY</a>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- <script type="text/javascript">
         var _ouibounce = ouibounce(document.getElementById('ouibounce-modal'),{
         aggressive: true, //Making this true makes ouibounce not to obey "once per visitor" rule
         });
         </script> -->
      <!-- Load UC Bounce Modal CDN End -->
      <!-- Exit Pop-Box End -->
      <!-- Google Tag Manager (noscript) -->
      <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K95FCV8"
         height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
      <!-- End Google Tag Manager (noscript) -->
      <!-- Facebook Pixel Code -->
      <script>
         ! function(f, b, e, v, n, t, s) {
          if (f.fbq) return;
          n = f.fbq = function() {
          n.callMethod ?
         	 n.callMethod.apply(n, arguments) : n.queue.push(arguments)
          };
          if (!f._fbq) f._fbq = n;
          n.push = n;
          n.loaded = !0;
          n.version = '2.0';
          n.queue = [];
          t = b.createElement(e);
          t.async = !0;
          t.src = v;
          s = b.getElementsByTagName(e)[0];
          s.parentNode.insertBefore(t, s)
         }(window,
          document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');
         fbq('init', '1777584378755780');
         fbq('track', 'PageView');
      </script>
      <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=1777584378755780&ev=PageView&noscript=1" /></noscript>
      <!-- DO NOT MODIFY -->
      <!-- End Facebook Pixel Code -->
      <!-- Google Code for Remarketing Tag -->
      <!-------------------------------------------------- Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup --------------------------------------------------->
      <div style="display:none;">
         <script type="text/javascript">
            /* <![CDATA[ */
            var google_conversion_id = 748114601;
            var google_custom_params = window.google_tag_params;
            var google_remarketing_only = true;
            var google_user_id = '<unique user id>';
            /* ]]> */
         </script>
         <script type="text/javascript" src="https://www.googleadservices.com/pagead/conversion.js"></script>
         <noscript>
            <div style="display:inline;">
               <img height="1" width="1" style="border-style:none;" alt="" src="https://googleads.g.doubleclick.net/pagead/viewthroughconversion/748114601/?guid=ON&amp;script=0&amp;userId=<unique user id>" />
            </div>
         </noscript>
      </div>
      <!-- Google Code for Remarketing Tag -->
      <!-- timer --->
      <?php
         if ($now < $exp_date) {
         
         ?>
      <script type="text/javascript">
         // Count down milliseconds = server_end - server_now = client_end - client_now
         var server_end = <?php echo $exp_date; ?> * 1000;
         var server_now = <?php echo time(); ?> * 1000;
         var client_now = new Date().getTime();
         var end = server_end - server_now + client_now; // this is the real end time
         
         var noob = $('.countdown').length;
         
         var _second = 1000;
         var _minute = _second * 60;
         var _hour = _minute * 60;
         var _day = _hour * 24
         var timer;
         
         function showRemaining() {
          var now = new Date();
          var distance = end - now;
          if (distance < 0) {
          clearInterval(timer);
          document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
          return;
          }
         
          var days = Math.floor(distance / _day);
          var hours = Math.floor((distance % _day) / _hour);
          var minutes = Math.floor((distance % _hour) / _minute);
          var seconds = Math.floor((distance % _minute) / _second);
          if (days < 10) {
          days = "0" + days;
          }
          if (hours < 10) {
          hours = "0" + hours;
          }
          if (minutes < 10) {
          minutes = "0" + minutes;
          }
          if (seconds < 10) {
          seconds = "0" + seconds;
          }
          var i;
          var countdown = document.getElementsByClassName('countdown');
          for (i = 0; i < noob; i++) {
          countdown[i].innerHTML = '';
         
          if (days) {
         	 countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + days + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 smmltd">Days</span> </div>';
          }
         
          countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + hours + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>';
         
          countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">' + minutes + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>';
         
          countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">' + seconds + '</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>';
          }
         
         }
         timer = setInterval(showRemaining, 1000);
      </script>
      <?php
         } else {
         echo "Times Up";
         }
         ?>
      <!--- timer end-->
   </body>
</html>
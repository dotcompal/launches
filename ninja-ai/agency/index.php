<!Doctype html>
<html>
   <head>
      <title>Ninja Ai Agency</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=9">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
      <meta name="title" content="Ninja Ai | Agency">
      <meta name="description" content="Ninja Ai">
      <meta name="keywords" content="Ninja Ai">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta property="og:image" content="https://www.getninjaai.com/agency/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Pranshu Gupta">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="Ninja Ai | Agency">
      <meta property="og:description" content="Ninja Ai">
      <meta property="og:image" content="https://www.getninjaai.com/agency/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="Ninja Ai | Reseller">
      <meta property="twitter:description" content="Ninja Ai">
      <meta property="twitter:image" content="https://getninjaai.com/reseller/thumbnail.png">
      <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&family=Poppins:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
      <!-- Start Editor required -->
      <link rel="stylesheet" href="https://cdn.oppyotest.com/launches/sellero/common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style.css" type="text/css">
      <link rel="stylesheet" href="assets/css/timer.css" type="text/css">
      <script src="https://cdn.oppyotest.com/launches/sellero/common_assets/js/bootstrap.min.js"></script>
      <script src="https://cdn.oppyotest.com/launches/sellero/common_assets/js/jquery.min.js"></script>
      <!-- End -->
   </head>
   <body>
      <!--1. Header Section Start -->
      <div class="warning-section">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <div class="offer">
                        <div>
                            <img src="https://cdn.oppyo.com/launches/appzilo/special/error.webp">
                        </div>
                        <div class="f-22 f-md-30 w500 lh140 white-clr text-center">
                            <span class="w600">WAIT!</span>  YOUR ORDER IS NOT YET COMPLETED... *DO NOT CLOSE*
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
      <div class="header-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div>
                  <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 645.02 151.26" style="max-height:70px">
                              <defs>
                                <style>
                                  .cls-1 {
                                    fill: url(#linear-gradient-2);
                                  }
                            
                                  .cls-2 {
                                    fill: #fff;
                                  }
                            
                                  .cls-3 {
                                    fill-rule: evenodd;
                                  }
                            
                                  .cls-3, .cls-4 {
                                    fill: #ff813b;
                                  }
                            
                                  .cls-5 {
                                    fill: url(#linear-gradient-3);
                                  }
                            
                                  .cls-6 {
                                    fill: url(#linear-gradient);
                                  }
                            
                                  .cls-7 {
                                    fill: url(#linear-gradient-4);
                                  }
                                </style>
                                <linearGradient id="linear-gradient" x1="47.81" y1="75" x2="185.48" y2="75" gradientUnits="userSpaceOnUse">
                                  <stop offset="0" stop-color="#b956ff"></stop>
                                  <stop offset="1" stop-color="#6e33ff"></stop>
                                </linearGradient>
                                <linearGradient id="linear-gradient-2" x1="0" y1="75" x2="85.18" y2="75" gradientUnits="userSpaceOnUse">
                                  <stop offset="0" stop-color="#b956ff"></stop>
                                  <stop offset="1" stop-color="#a356fa"></stop>
                                </linearGradient>
                                <linearGradient id="linear-gradient-3" x1="55.61" y1="62.54" x2="62.86" y2="62.54" gradientUnits="userSpaceOnUse">
                                  <stop offset="0" stop-color="#b956ff"></stop>
                                  <stop offset="1" stop-color="#ac59fa"></stop>
                                </linearGradient>
                                <linearGradient id="linear-gradient-4" x1="54.62" y1="79" x2="61.87" y2="79" gradientTransform="translate(19.64 -11.24) rotate(13.24)" xlink:href="#linear-gradient-3"></linearGradient>
                              </defs>
                              <g id="Layer_1-2" data-name="Layer 1">
                                <g>
                                  <g>
                                    <g>
                                      <path class="cls-6" d="m181.49,58.53h-10.72c-1.4-5.13-3.43-9.99-6.01-14.51l7.58-7.58c1.56-1.56,1.56-4.09,0-5.65l-17.65-17.65c-1.56-1.56-4.09-1.56-5.65,0l-7.58,7.58c-4.51-2.58-9.38-4.61-14.51-6.01V4c0-2.21-1.79-4-4-4h-24.96c-2.21,0-4,1.79-4,4v10.72c-.16.04-.31.09-.47.14-.27.08-.53.15-.8.23-.27.08-.55.16-.82.24-.55.17-1.09.35-1.63.53-.11.04-.22.08-.33.12-.48.17-.96.34-1.44.52-.12.04-.24.09-.36.14-.52.2-1.04.4-1.56.62-.04.02-.08.03-.12.05-2.41,1-4.74,2.15-6.99,3.43l-7.57-7.57c-1.56-1.56-4.09-1.56-5.65,0l-17.65,17.65c-.36.36-.63.76-.82,1.2h61.99v.02c.23,0,.45-.02.68-.02,23.76,0,43.01,19.26,43.01,43.01s-19.26,43.01-43.01,43.01c-.5,0-1-.02-1.5-.04v.04h-61.17c.19.43.46.84.82,1.2l17.65,17.65c1.56,1.56,4.09,1.56,5.65,0l7.57-7.57c2.25,1.28,4.58,2.43,6.99,3.43.04.02.08.03.12.05.51.21,1.03.42,1.55.62.12.05.24.09.36.14.47.18.95.35,1.43.52.11.04.23.08.34.12.54.18,1.08.36,1.63.53.27.08.55.16.83.25.26.08.53.16.79.23.16.04.31.09.47.14v10.72c0,2.21,1.79,4,4,4h24.96c2.21,0,4-1.79,4-4v-10.72c5.13-1.4,9.99-3.43,14.51-6.01l7.58,7.58c1.56,1.56,4.09,1.56,5.65,0l17.65-17.65c1.56-1.56,1.56-4.09,0-5.65l-7.58-7.58c2.58-4.51,4.61-9.38,6.01-14.51h10.72c2.21,0,4-1.79,4-4v-24.96c0-2.21-1.79-4-4-4Z"></path>
                                      <path class="cls-1" d="m76.64,101.54c-5.74-7.31-9.18-16.52-9.18-26.54s3.44-19.23,9.18-26.54c2.45-3.13,5.33-5.9,8.53-8.24h-48.39c-1.31-1.89-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.13h17.07c-1.51,3.21-2.76,6.58-3.71,10.07h3.1c1.31-1.89,3.49-3.13,5.97-3.13,4.01,0,7.25,3.25,7.25,7.25s-3.22,7.23-7.21,7.25h-.04c-2.48,0-4.66-1.24-5.97-3.13H13.22c-.44-.64-.98-1.19-1.59-1.65-1.21-.92-2.73-1.48-4.38-1.48-4,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.14h38.82c1.31-1.89,3.49-3.13,5.97-3.13.43,0,.84.05,1.25.12,3.41.59,6,3.56,6,7.14,0,2.78-1.56,5.19-3.86,6.41-1.01.53-2.16.84-3.39.84-2.48,0-4.66-1.24-5.97-3.13h-17.85c-1.31-1.9-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.13h15.96c.95,3.48,2.2,6.85,3.71,10.07h-5.63c-1.31-1.89-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25,0,1.53.48,2.95,1.28,4.12,1.31,1.9,3.49,3.14,5.97,3.14s4.66-1.24,5.97-3.14h36.95c-3.21-2.34-6.08-5.11-8.53-8.24ZM30.64,47.97c-2,0-3.62-1.62-3.62-3.62s1.62-3.62,3.62-3.62,3.63,1.62,3.63,3.62-1.63,3.62-3.63,3.62Zm-23.39,26.47c-2,0-3.62-1.63-3.62-3.62s1.63-3.63,3.62-3.63,3.63,1.63,3.63,3.63-1.63,3.62-3.63,3.62Zm20.97,16.45c-2,0-3.63-1.63-3.63-3.63s1.63-3.62,3.63-3.62,3.62,1.63,3.62,3.62-1.63,3.63-3.62,3.63Zm14.08,18.39c-2,0-3.62-1.62-3.62-3.62s1.62-3.62,3.62-3.62,3.62,1.62,3.62,3.62-1.62,3.62-3.62,3.62Z"></path>
                                      <circle class="cls-5" cx="59.23" cy="62.54" r="3.63"></circle>
                                      <circle class="cls-7" cx="58.24" cy="79" r="3.63" transform="translate(-16.54 15.44) rotate(-13.24)"></circle>
                                    </g>
                                    <g>
                                      <g>
                                        <path class="cls-3" d="m104.63,78.16c-9.44,6-14.52,2.86-15.25-9.4,4.93,3.38,10.02,6.52,15.25,9.4Z"></path>
                                        <path class="cls-3" d="m131.01,68.76c-.72,12.26-5.81,15.4-15.25,9.4,5.24-2.88,10.32-6.02,15.25-9.4Z"></path>
                                      </g>
                                      <path class="cls-4" d="m110.16,40.13c-.89,0-1.78.03-2.66.1-.08,0-.16,0-.24.02-9.59.79-18.07,5.45-23.89,12.42-.25.3-.49.59-.73.89-.14.19-.29.38-.43.57-4.02,5.37-6.52,11.93-6.88,19.07,0,.01,0,.03,0,.04-.01.22-.02.45-.03.68-.01.3-.02.6-.02.9v.18c0,.28,0,.55,0,.83.44,18.64,15.5,33.66,34.15,34.04.24,0,.48,0,.72,0,.3,0,.6,0,.9,0,18.85-.48,33.98-15.9,33.98-34.87s-15.61-34.88-34.88-34.88Zm.19,39.35c-14.41,14.37-29.15,1.23-29.15-9.45s13.05,0,29.15,0,29.15-10.67,29.15,0-14.74,23.81-29.15,9.45Z"></path>
                                    </g>
                                  </g>
                                  <g>
                                    <path class="cls-2" d="m286.65,118.51h-17.37l-39.32-59.42v59.42h-17.37V31.8h17.37l39.32,59.54V31.8h17.37v86.71Z"></path>
                                    <path class="cls-2" d="m304.95,38.69c-2.03-1.94-3.04-4.36-3.04-7.26s1.01-5.31,3.04-7.26c2.03-1.94,4.57-2.92,7.63-2.92s5.6.97,7.63,2.92c2.02,1.94,3.04,4.36,3.04,7.26s-1.02,5.31-3.04,7.26c-2.03,1.94-4.57,2.91-7.63,2.91s-5.6-.97-7.63-2.91Zm16.19,11.1v68.72h-17.37V49.79h17.37Z"></path>
                                    <path class="cls-2" d="m396.18,56.55c5.04,5.17,7.57,12.38,7.57,21.65v40.31h-17.36v-37.96c0-5.46-1.37-9.65-4.1-12.59-2.73-2.94-6.45-4.4-11.16-4.4s-8.58,1.47-11.35,4.4c-2.77,2.94-4.15,7.13-4.15,12.59v37.96h-17.37V49.79h17.37v8.56c2.31-2.98,5.27-5.31,8.87-7.01,3.6-1.69,7.55-2.54,11.85-2.54,8.19,0,14.8,2.59,19.85,7.75Z"></path>
                                    <path class="cls-2" d="m437.61,129.8c0,7.61-1.88,13.09-5.64,16.44-3.77,3.35-9.16,5.02-16.19,5.02h-7.69v-14.76h4.96c2.64,0,4.51-.52,5.58-1.55,1.07-1.03,1.61-2.71,1.61-5.02V49.79h17.37v80.01Zm-16.31-91.11c-2.03-1.94-3.04-4.36-3.04-7.26s1.01-5.31,3.04-7.26c2.02-1.94,4.61-2.92,7.75-2.92s5.58.97,7.57,2.92c1.98,1.94,2.98,4.36,2.98,7.26s-.99,5.31-2.98,7.26c-1.98,1.94-4.51,2.91-7.57,2.91s-5.73-.97-7.75-2.91Z"></path>
                                    <path class="cls-2" d="m454.42,65.42c2.77-5.37,6.53-9.51,11.29-12.4,4.76-2.89,10.07-4.34,15.94-4.34,5.13,0,9.61,1.04,13.46,3.1,3.85,2.07,6.92,4.67,9.24,7.82v-9.8h17.49v68.72h-17.49v-10.05c-2.23,3.23-5.31,5.89-9.24,8-3.93,2.11-8.46,3.16-13.58,3.16-5.79,0-11.06-1.49-15.82-4.47-4.76-2.98-8.52-7.17-11.29-12.59-2.77-5.41-4.16-11.64-4.16-18.67s1.38-13.11,4.16-18.48Zm47.45,7.88c-1.65-3.02-3.89-5.33-6.7-6.95-2.81-1.61-5.83-2.42-9.06-2.42s-6.2.79-8.93,2.36c-2.73,1.57-4.94,3.87-6.64,6.88-1.69,3.02-2.54,6.6-2.54,10.73s.85,7.75,2.54,10.85c1.69,3.1,3.93,5.48,6.7,7.13,2.77,1.66,5.72,2.48,8.87,2.48s6.24-.81,9.06-2.42c2.81-1.61,5.04-3.93,6.7-6.95,1.65-3.02,2.48-6.64,2.48-10.85s-.83-7.83-2.48-10.85Z"></path>
                                    <path class="cls-4" d="m591.93,102.01h-34.48l-5.71,16.5h-18.24l31.14-86.71h20.22l31.13,86.71h-18.36l-5.71-16.5Zm-4.71-13.89l-12.53-36.22-12.53,36.22h25.06Z"></path>
                                    <path class="cls-4" d="m645.02,31.93v86.58h-17.37V31.93h17.37Z"></path>
                                  </g>
                                </g>
                              </g>
                            </svg>
                  </div>
               </div>
               <div class="col-12 text-center lh150 mt20 mt-md50">
                  <div class="pre-heading f-18 f-md-22 w500 lh140 white-clr">
                  Special One-Time Deal - Upgrade To Ninja Ai Agency Today
                  </div>
               </div>
               <div class="col-12 mt-md30 mt20 f-md-46 f-28 w400 text-center white-clr lh140">
               Start Your <span class="yellow-clr w700">Own Profitable Pro-Agency And Make 5 Figure Income</span> By Charging $500- $1000 Per Client Without Any Hard Work
               </div>
               <div class="col-12 mt-md30 mt20">
                  <div class="f-md-22 f-20 lh140 w400 text-center white-clr">
                  We Did All the Work… Sell High In Demand DFY Services To Hungry Clients & Keep 100% Profits
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
                  <img src="assets/images/product-image.webp" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 mt30 mt-md50 f-md-22 f-20 lh140 w400 text-center white-clr">
                  This is An Exclusive Deal for New<span class="w600 yellow-clr"> "Ninja Ai"</span> Users Only...
               </div>
               <div class="col-md-9 mx-auto col-12 mt20 f-20 f-md-35 text-center probtn1">
                  <a href="#buynow" class="text-center">Upgrade to Ninja Ai Agency Now</a>
               </div>
            </div>
         </div>
      </div>
      <!--1. Header Section End -->
      <!--2. Second Section Start -->
      <div class="second-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-md-45 f-28 w500 lh140 text-capitalize text-center black-clr">
                  The Secret Weapon To 2X Your Profits & Start Making $5000 - $10000 <span class="w700"> From Your Ninja Ai Account </span>
                  </div>
               </div>
               <div class="col-12 text-center">
                  <div class="f-20 f-md-22 text-center w500 lh140 text-left mt15 mt-md40">
                     
                     This Agency upgrade puts you on whole another level with a touch of a button and <span class="w700"> enables you to enter in $413 Billion marketing industry.</span><br><br> From breakthrough funnel sites creation technology,
                     complete business panel to manage your client’s biz from single dashboard, adding your team members or freelancing staff to manage everything easily, <span class="w600">the agency upgrade has it all.</span>
                  </div>
               </div>
               <div class="col-md-6 col-12 mt30 mt-md70">
                  <div class="row">
                     <div class="col-md-2 col-12 px5">
                        <div>
                           <img src="assets/images/fe1.webp" class="img-fluid mx-auto d-block" width="70">
                        </div>
                     </div>
                     <div class="col-md-10 col-12 mt20 mt-md0">
                        <div class="f-18 f-md-20 lh150 w400 d-left-m-center">
                           <span class="w600">Sell high-in-demand Funnel site creation services To Your Clients</span> with Funnels & DFY Support for Our Platform.
                        </div>
                     </div>
                  </div>
                  <!---3-->
                  <div class="row mt20 mt-md20 align-items-center">
                     <div class="col-md-2 col-12 px5">
                        <div>
                           <img src="assets/images/fe3.webp" class="img-fluid mx-auto d-block" width="70">
                        </div>
                     </div>
                     <div class="col-md-10 col-12 mt20 mt-md0">
                        <div class="f-18 f-md-20 lh150 w400 d-left-m-center">
                           <span class="w600">Serve Unlimited Clients with Agency License</span>
                        </div>
                     </div>
                  </div>
                  <!--4-->
                  <div class="row mt20 mt-md50">
                     <div class="col-md-2 col-12 px5">
                        <div><img src="assets/images/fe5.webp" class="img-fluid mx-auto d-block" width="70"></div>
                     </div>
                     <div class="col-md-10 col-12 mt20 mt-md0">
                        <div class="f-18 f-md-20 lh150 w400 d-left-m-center mb0 mb-md20">
                           <span class="w600">Add Unlimited Team Members – Inhouse & freelancers </span>
                        </div>
                     </div>
                  </div>
                  <div class="row mt20 mt-md50">
                     <div class="col-md-2 col-12 px5">
                        <div><img src="assets/images/fe2.webp" class="img-fluid mx-auto d-block" width="70"></div>
                     </div>
                     <div class="col-md-10 col-12 mt20 mt-md0">
                        <div class="f-18 f-md-20 lh150 w400 d-left-m-center">
                           <span class="w600">Done-For-Your Business Management Panel -</span>Manage all your client’s businesses from a single dashboard to have full control.
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-6 col-12 mt30 mt-md70">
                  <!--11-->
                  <div class="row mt20 mt-md0">
                     <div class="col-md-2 col-12 px5">
                        <div><img src="assets/images/fe4.webp" class="img-fluid mx-auto d-block" width="70"></div>
                     </div>
                     <div class="col-md-10  col-12 mt20 mt-md15">
                        <div class="f-18 f-md-20 lh150 w400 d-left-m-center">
                           <span class="w600">Charge Monthly or One time at your own pricing</span> 
                        </div>
                     </div>
                  </div>
                  <div class="row mt20 mt-md50">
                     <div class="col-md-2 col-12 px5">
                        <div><img src="assets/images/fe5.webp" class="img-fluid mx-auto d-block" width="70"></div>
                     </div>
                     <div class="col-md-10  col-12 mt20 mt-md15">
                        <div class="f-18 f-md-20 lh150 w400 d-left-m-center">
                           <span class="w600">Start making profits Instantly!</span> 
                        </div>
                     </div>
                  </div>
                  <div class="row mt20 mt-md50">
                     <div class="col-md-2 col-12 px5">
                        <div><img src="assets/images/fe6.webp" class="img-fluid mx-auto d-block" width="70"></div>
                     </div>
                     <div class="col-md-10  col-12 mt20 mt-md15">
                        <div class="f-18 f-md-20 lh150 w400 d-left-m-center">
                           <span class="w600">Get All These Benefits</span> For One Time Price
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-9 mx-auto col-12 mt10 mt-md60 f-22 f-md-36 text-center probtn1">
                  <a href="#buynow" class="text-center">Upgrade to Ninja Ai Agency Now</a>
               </div>
            </div>
         </div>
      </div>
      <!--2. Second Section End -->
      <!--4. Fourth Section Starts -->
      <div class="fifth-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="prdly-pres f-28 f-md-38 w700 lh140 text-center white-clr text-uppercase">Introducing</div>
               </div>
               <div class="col-12 mt-md30 mt20 text-center">
               <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 645.02 151.26" style="max-height:135px">
                              <defs>
                                <style>
                                  .cls-1 {
                                    fill: url(#linear-gradient-2);
                                  }
                            
                                  .cls-2 {
                                    fill: #fff;
                                  }
                            
                                  .cls-3 {
                                    fill-rule: evenodd;
                                  }
                            
                                  .cls-3, .cls-4 {
                                    fill: #ff813b;
                                  }
                            
                                  .cls-5 {
                                    fill: url(#linear-gradient-3);
                                  }
                            
                                  .cls-6 {
                                    fill: url(#linear-gradient);
                                  }
                            
                                  .cls-7 {
                                    fill: url(#linear-gradient-4);
                                  }
                                </style>
                                <linearGradient id="linear-gradient" x1="47.81" y1="75" x2="185.48" y2="75" gradientUnits="userSpaceOnUse">
                                  <stop offset="0" stop-color="#b956ff"></stop>
                                  <stop offset="1" stop-color="#6e33ff"></stop>
                                </linearGradient>
                                <linearGradient id="linear-gradient-2" x1="0" y1="75" x2="85.18" y2="75" gradientUnits="userSpaceOnUse">
                                  <stop offset="0" stop-color="#b956ff"></stop>
                                  <stop offset="1" stop-color="#a356fa"></stop>
                                </linearGradient>
                                <linearGradient id="linear-gradient-3" x1="55.61" y1="62.54" x2="62.86" y2="62.54" gradientUnits="userSpaceOnUse">
                                  <stop offset="0" stop-color="#b956ff"></stop>
                                  <stop offset="1" stop-color="#ac59fa"></stop>
                                </linearGradient>
                                <linearGradient id="linear-gradient-4" x1="54.62" y1="79" x2="61.87" y2="79" gradientTransform="translate(19.64 -11.24) rotate(13.24)" xlink:href="#linear-gradient-3"></linearGradient>
                              </defs>
                              <g id="Layer_1-2" data-name="Layer 1">
                                <g>
                                  <g>
                                    <g>
                                      <path class="cls-6" d="m181.49,58.53h-10.72c-1.4-5.13-3.43-9.99-6.01-14.51l7.58-7.58c1.56-1.56,1.56-4.09,0-5.65l-17.65-17.65c-1.56-1.56-4.09-1.56-5.65,0l-7.58,7.58c-4.51-2.58-9.38-4.61-14.51-6.01V4c0-2.21-1.79-4-4-4h-24.96c-2.21,0-4,1.79-4,4v10.72c-.16.04-.31.09-.47.14-.27.08-.53.15-.8.23-.27.08-.55.16-.82.24-.55.17-1.09.35-1.63.53-.11.04-.22.08-.33.12-.48.17-.96.34-1.44.52-.12.04-.24.09-.36.14-.52.2-1.04.4-1.56.62-.04.02-.08.03-.12.05-2.41,1-4.74,2.15-6.99,3.43l-7.57-7.57c-1.56-1.56-4.09-1.56-5.65,0l-17.65,17.65c-.36.36-.63.76-.82,1.2h61.99v.02c.23,0,.45-.02.68-.02,23.76,0,43.01,19.26,43.01,43.01s-19.26,43.01-43.01,43.01c-.5,0-1-.02-1.5-.04v.04h-61.17c.19.43.46.84.82,1.2l17.65,17.65c1.56,1.56,4.09,1.56,5.65,0l7.57-7.57c2.25,1.28,4.58,2.43,6.99,3.43.04.02.08.03.12.05.51.21,1.03.42,1.55.62.12.05.24.09.36.14.47.18.95.35,1.43.52.11.04.23.08.34.12.54.18,1.08.36,1.63.53.27.08.55.16.83.25.26.08.53.16.79.23.16.04.31.09.47.14v10.72c0,2.21,1.79,4,4,4h24.96c2.21,0,4-1.79,4-4v-10.72c5.13-1.4,9.99-3.43,14.51-6.01l7.58,7.58c1.56,1.56,4.09,1.56,5.65,0l17.65-17.65c1.56-1.56,1.56-4.09,0-5.65l-7.58-7.58c2.58-4.51,4.61-9.38,6.01-14.51h10.72c2.21,0,4-1.79,4-4v-24.96c0-2.21-1.79-4-4-4Z"></path>
                                      <path class="cls-1" d="m76.64,101.54c-5.74-7.31-9.18-16.52-9.18-26.54s3.44-19.23,9.18-26.54c2.45-3.13,5.33-5.9,8.53-8.24h-48.39c-1.31-1.89-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.13h17.07c-1.51,3.21-2.76,6.58-3.71,10.07h3.1c1.31-1.89,3.49-3.13,5.97-3.13,4.01,0,7.25,3.25,7.25,7.25s-3.22,7.23-7.21,7.25h-.04c-2.48,0-4.66-1.24-5.97-3.13H13.22c-.44-.64-.98-1.19-1.59-1.65-1.21-.92-2.73-1.48-4.38-1.48-4,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.14h38.82c1.31-1.89,3.49-3.13,5.97-3.13.43,0,.84.05,1.25.12,3.41.59,6,3.56,6,7.14,0,2.78-1.56,5.19-3.86,6.41-1.01.53-2.16.84-3.39.84-2.48,0-4.66-1.24-5.97-3.13h-17.85c-1.31-1.9-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.13h15.96c.95,3.48,2.2,6.85,3.71,10.07h-5.63c-1.31-1.89-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25,0,1.53.48,2.95,1.28,4.12,1.31,1.9,3.49,3.14,5.97,3.14s4.66-1.24,5.97-3.14h36.95c-3.21-2.34-6.08-5.11-8.53-8.24ZM30.64,47.97c-2,0-3.62-1.62-3.62-3.62s1.62-3.62,3.62-3.62,3.63,1.62,3.63,3.62-1.63,3.62-3.63,3.62Zm-23.39,26.47c-2,0-3.62-1.63-3.62-3.62s1.63-3.63,3.62-3.63,3.63,1.63,3.63,3.63-1.63,3.62-3.63,3.62Zm20.97,16.45c-2,0-3.63-1.63-3.63-3.63s1.63-3.62,3.63-3.62,3.62,1.63,3.62,3.62-1.63,3.63-3.62,3.63Zm14.08,18.39c-2,0-3.62-1.62-3.62-3.62s1.62-3.62,3.62-3.62,3.62,1.62,3.62,3.62-1.62,3.62-3.62,3.62Z"></path>
                                      <circle class="cls-5" cx="59.23" cy="62.54" r="3.63"></circle>
                                      <circle class="cls-7" cx="58.24" cy="79" r="3.63" transform="translate(-16.54 15.44) rotate(-13.24)"></circle>
                                    </g>
                                    <g>
                                      <g>
                                        <path class="cls-3" d="m104.63,78.16c-9.44,6-14.52,2.86-15.25-9.4,4.93,3.38,10.02,6.52,15.25,9.4Z"></path>
                                        <path class="cls-3" d="m131.01,68.76c-.72,12.26-5.81,15.4-15.25,9.4,5.24-2.88,10.32-6.02,15.25-9.4Z"></path>
                                      </g>
                                      <path class="cls-4" d="m110.16,40.13c-.89,0-1.78.03-2.66.1-.08,0-.16,0-.24.02-9.59.79-18.07,5.45-23.89,12.42-.25.3-.49.59-.73.89-.14.19-.29.38-.43.57-4.02,5.37-6.52,11.93-6.88,19.07,0,.01,0,.03,0,.04-.01.22-.02.45-.03.68-.01.3-.02.6-.02.9v.18c0,.28,0,.55,0,.83.44,18.64,15.5,33.66,34.15,34.04.24,0,.48,0,.72,0,.3,0,.6,0,.9,0,18.85-.48,33.98-15.9,33.98-34.87s-15.61-34.88-34.88-34.88Zm.19,39.35c-14.41,14.37-29.15,1.23-29.15-9.45s13.05,0,29.15,0,29.15-10.67,29.15,0-14.74,23.81-29.15,9.45Z"></path>
                                    </g>
                                  </g>
                                  <g>
                                    <path class="cls-2" d="m286.65,118.51h-17.37l-39.32-59.42v59.42h-17.37V31.8h17.37l39.32,59.54V31.8h17.37v86.71Z"></path>
                                    <path class="cls-2" d="m304.95,38.69c-2.03-1.94-3.04-4.36-3.04-7.26s1.01-5.31,3.04-7.26c2.03-1.94,4.57-2.92,7.63-2.92s5.6.97,7.63,2.92c2.02,1.94,3.04,4.36,3.04,7.26s-1.02,5.31-3.04,7.26c-2.03,1.94-4.57,2.91-7.63,2.91s-5.6-.97-7.63-2.91Zm16.19,11.1v68.72h-17.37V49.79h17.37Z"></path>
                                    <path class="cls-2" d="m396.18,56.55c5.04,5.17,7.57,12.38,7.57,21.65v40.31h-17.36v-37.96c0-5.46-1.37-9.65-4.1-12.59-2.73-2.94-6.45-4.4-11.16-4.4s-8.58,1.47-11.35,4.4c-2.77,2.94-4.15,7.13-4.15,12.59v37.96h-17.37V49.79h17.37v8.56c2.31-2.98,5.27-5.31,8.87-7.01,3.6-1.69,7.55-2.54,11.85-2.54,8.19,0,14.8,2.59,19.85,7.75Z"></path>
                                    <path class="cls-2" d="m437.61,129.8c0,7.61-1.88,13.09-5.64,16.44-3.77,3.35-9.16,5.02-16.19,5.02h-7.69v-14.76h4.96c2.64,0,4.51-.52,5.58-1.55,1.07-1.03,1.61-2.71,1.61-5.02V49.79h17.37v80.01Zm-16.31-91.11c-2.03-1.94-3.04-4.36-3.04-7.26s1.01-5.31,3.04-7.26c2.02-1.94,4.61-2.92,7.75-2.92s5.58.97,7.57,2.92c1.98,1.94,2.98,4.36,2.98,7.26s-.99,5.31-2.98,7.26c-1.98,1.94-4.51,2.91-7.57,2.91s-5.73-.97-7.75-2.91Z"></path>
                                    <path class="cls-2" d="m454.42,65.42c2.77-5.37,6.53-9.51,11.29-12.4,4.76-2.89,10.07-4.34,15.94-4.34,5.13,0,9.61,1.04,13.46,3.1,3.85,2.07,6.92,4.67,9.24,7.82v-9.8h17.49v68.72h-17.49v-10.05c-2.23,3.23-5.31,5.89-9.24,8-3.93,2.11-8.46,3.16-13.58,3.16-5.79,0-11.06-1.49-15.82-4.47-4.76-2.98-8.52-7.17-11.29-12.59-2.77-5.41-4.16-11.64-4.16-18.67s1.38-13.11,4.16-18.48Zm47.45,7.88c-1.65-3.02-3.89-5.33-6.7-6.95-2.81-1.61-5.83-2.42-9.06-2.42s-6.2.79-8.93,2.36c-2.73,1.57-4.94,3.87-6.64,6.88-1.69,3.02-2.54,6.6-2.54,10.73s.85,7.75,2.54,10.85c1.69,3.1,3.93,5.48,6.7,7.13,2.77,1.66,5.72,2.48,8.87,2.48s6.24-.81,9.06-2.42c2.81-1.61,5.04-3.93,6.7-6.95,1.65-3.02,2.48-6.64,2.48-10.85s-.83-7.83-2.48-10.85Z"></path>
                                    <path class="cls-4" d="m591.93,102.01h-34.48l-5.71,16.5h-18.24l31.14-86.71h20.22l31.13,86.71h-18.36l-5.71-16.5Zm-4.71-13.89l-12.53-36.22-12.53,36.22h25.06Z"></path>
                                    <path class="cls-4" d="m645.02,31.93v86.58h-17.37V31.93h17.37Z"></path>
                                  </g>
                                </g>
                              </g>
                            </svg>
               </div>
               <div class="f-md-50 f-28 w700 white-clr lh150 col-12 mt-md30 mt20 text-center">
                   <span class="yellow-clr">Agency Upgrade</span>
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md70">
                  <img src="assets/images/product-image.webp" class="img-fluid d-block mx-auto margin-bottom-15">
               </div>
               <!-- <div class="col-md-5 col-12 mt20 mt-md70">
                  <div class="proudly-list-bg">
                     <ul class="proudly-tick pl0 m0 f-18 w500 black-clr lh150 text-capitalize">
                        <li><span class="w600">Build your Pro Academy with an In-built</span> Marketplace, Courses, Members Area, Lead Management, and Help Desk</li>
                        <li><span class="w600">Create Beautiful and Proven Converting</span> E-Learning Sites 
                        </li>
                        <li><span class="w600">Preloaded with 400 HD Video Courses and 30 DFY Sales Funnels </span>
                           to Start Selling Instantly and Keep 100% of Profit</li>
                        <li><span class="w600">Don’t Lose Traffic, Leads, and Profit </span>with Any 3rd Party Marketplace</li>
                        <li><span class="w600">Commercial License Included</span> </li>
                        <li><span class="w600">50+ More Features</span></li>
                     </ul>
                  </div>
               </div> -->
               <div class="col-md-9 mx-auto col-12 mt30 mt-md65">
                  <div class="f-18 f-md-36 text-center probtn1" editabletype="button" style="z-index: 10;">
                     <a href="#buynow" class="text-center">Upgrade to Ninja Ai Agency Now</a>
                  </div>
                  <br class="visible-md">
               </div>
            </div>
         </div>
      </div>
      <!--4. Fourth Section End -->
      <div class="thats-not-all-section">
         <div class="container">
            <div class="row">
               <div class="f-md-45 f-28 w700 lh140 text-center black-clr">
                  With Ninja Ai Agency Upgrade, You Can Tap Into The Very <span class="w700 purple-clr">Fast-Growing Freelancing Industry.</span>
               </div>
               <div class="col-12 col-md-12  mt20 mt-md60">
                  <div class="row d-flex align-items-center flex-wrap">
                     <div class="col-12 col-md-7">
                        <div class="w400 f-20 f-md-22 black-clr text-xs-center lh140 mt20 mt-md0">
                           <span class="w700">Start Your Own Profitable Freelancing services or Agency From Your Home</span> and Tap into 250 Billion Industry.
                        </div>
                     </div>
                     <div class="col-12 col-md-5 mt20 mt-md0">
                        <div editabletype="image" style="z-index:10;">
                           <img src="assets/images/freelancing-img.webp" class="img-fluid mx-auto d-block">
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12">
                  <div class="f-md-45 f-28 w700 lh150 text-center black-clr mt20 mt-md50 text-capitalize">
                     See how much freelancers are charging just to create basic academy websites
                  </div>
                  <div class="mt10 mt-md15" editabletype="image" style="z-index:10;">
                     <img src="assets/images/sales-funnels-img.webp" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-18 f-md-20 w400 lh150 black-clr text-xs-center mt15">
                     <span class="w500">With Ninja Ai agency not only you can find clients in your local area and sell using a proven funnel using Ninja Ai, but also you can find them on websites like Fiverr, Freelancer, Odesk, Upwork where tons of businesses are looking for a legitimate service provider. 
                  </div>
               </div>
               <div class="col-12 text-center mt30 mt-md80">
                  <div class="one-time-shape" editabletype="shape" style="z-index: 8;">
                     <div class="f-24 f-md-36 w600 text-center lh120 white-clr text-center">And it’s not a 1-Time Income</div>
                  </div>
               </div>
               <div class="col-12  f-30 f-md-70 w600 lh120 black-clr text-center mt30 mt-md5">534,000 </div>
               <div class="col-12 f-18 f-md-20 w400 lh150 text-center black-clr mt30 mt-md30">
                  New Businesses Starts Every Month. You can do this for life and Charge Your New as well as Existing Clients for Your Services again, and again and again forever
               </div>
               <div class="col-12 f-18 f-md-20 w400 lh150 text-center black-clr mt10">
                  <span class="w700">Yes, Tons of Businesses Are Looking for A Solution...</span>
                  <br class="visible-md">
                  <span class="w500">Absolutely No Limitations!</span>
               </div>
            </div>
         </div>
      </div>
      <div class="amazing-software-section1">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-12">
                  <div class="f-md-40 f-28 w700 lh150 text-capitalize text-center black-clr">
                     Nothing to Host, Build or Manage Support... Setup A Complete <span class="w700 purple-clr">'Done For You' System in 3 Simple Steps</span>
                  </div>
               </div>
               <!-- feature 1 -->
               <div class="col-12 col-md-12  mt20 mt-md60 d-flex align-items-center flex-wrap">
                  <div class="col-12 col-md-6 order-md-1">
                     <div>
                        <img src="../common_assets/images/1.webp" class="img-responsive">
                     </div>
                     <div class="f-24 f-md-36 w600 lh150 text-left mt10">
                        Add a new client’s business
                     </div>
                     <div class="f-18 f-md-20 w400 lh150 mt10">
                        All you need to do is add a new client’s business, insert details from where you want to fetch trendy content & videos, & you're all set.
                        <br><br> ZERO Marketing Experience Needed! We did All the HARD Part!
                     </div>
                  </div>
                  <div class="col-12 col-md-6  order-md-6 mt20 mt-md50">
                     <img src="assets/images/step1.webp" class="img-fluid mx-auto d-block imahe-h-250">
                  </div>
               </div>
               <!-- feature 2 -->
               <div class="col-12 col-md-12  mt20 mt-md60 d-flex align-items-center flex-wrap">
                  <div class="col-12 col-md-6 px0">
                     <div class="mt-md30">
                        <img src="../common_assets/images/2.webp" class="img-responsive">
                     </div>
                     <div class="f-24 f-md-36 w600 lh150 text-left mt10">
                        Accept the Monthly Payments & Keep 100% Profits
                     </div>
                     <div class="f-18 f-md-20 w400 lh150 mt10">
                        Charge them monthly, yearly or one-time high fee for providing them high-in-demand services & Keep 100% profits with you.
                     </div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md50 p-md0">
                     <img src="assets/images/step2.webp" class="img-fluid mx-auto d-block imahe-h-250">
                  </div>
               </div>
               <!-- feature 3 -->
               <div class="col-12 col-md-12  mt20 mt-md60 d-flex align-items-center flex-wrap">
                  <div class="col-12 col-md-6 order-md-1">
                     <div class="mt-md30">
                        <img src="../common_assets/images/3.webp" class="img-responsive">
                     </div>
                     <div class="f-24 f-md-36 w600 lh150 text-left mt10">
                        We did all the Hard work.
                     </div>
                     <div class="f-18 f-md-20 w400 lh150 w400 mt10">
                        We invested a lot of money to get this revolutionary technology ready. And you know what the best part is, we will take care of all the customer support about your and your client’s Ninja Ai account queries. You don’t even need to lift a finger…
                     </div>
                  </div>
                  <div class="col-12 col-md-6 order-md-6 mt20 mt-md0">
                     <img src="assets/images/step3.webp" class="img-fluid mx-auto d-block imahe-h-300">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- feature1 -->
      <div class="white-section">
         <div class="container">
            <div class="row">
               <div class="f-md-45 f-28 w700 lh150 text-center black-clr mb-md0 mb20">
                  Here’ What You are Getting with <br class="d-lg-block d-none"><span class="w700">This Agency Upgrade Today</span>
               </div>
               <div class="col-12 col-md-12 mt30 mt-md50">
                  <div>
                     <img src="../common_assets/images/1.webp" class="img-responsive">
                  </div>
                  <div class="col-12 col-md-12 f-24 f-md-36 w500 lh140 mt10 mt-md10 text-left black-clr">
                     A Top-Notch Software, Ninja Ai With Agency License To Provide Affilate Funnel Sites Creation Services To Your Clients.
                  </div>
                  <div class="col-12 col-md-6 mx-auto mt20 mt-md30">
                     <img src="assets/images/feature1.webp" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt10 mt-md30 text-left col-12">
                     Providing high-in-demand funnel sites creation services couldn’t get easier than this. You’re getting the never offered before Ninja Ai Agency License to provide these services to your clients with DFY support for our software. This is something that
                     will get you way above your competitors.
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--feature1 -->
      <!--feature3 -->
      <div class="grey-section">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-12">
                  <div>
                     <img src="../common_assets/images/2.webp" class="img-responsive">
                  </div>
                  <div class="col-md-12 col-12 f-24 f-md-36 w500 lh140 mt10 mt-md10 text-left black-clr pl0">
                     Done-For-Your Business Management Panel
                  </div>
                  <div class="col-12 col-md-10 mx-auto mt20 mt-md30">
                     <img src="assets/images/bussiness-manage.webp" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt15 mt-md30 mt10 mt-md60 text-left col-12">
                     Oh yeah! You can manage all your client’s businesses from a single dashboard to have full control. Simply switch between the businesses or copy-paste a proven funnel or high converting landing page within few clicks.
                     <span class="w600">It’s EASY & FAST.</span><br><br> You also can add your client to their business as an admin & Impress them by giving them a separate business dashboard with their personal login details to let them check
                     their business performance live
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--feature7 -->
      <div class="white-section">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-12">
                  <div>
                     <img src="../common_assets/images/3.webp" class="img-responsive">
                  </div>
                  <div class="col-md-12 col-12 f-24 f-md-36 w500 lh140 mt10 mt-md10 text-left black-clr ">
                     Add Unlimited team members – Inhouse & freelancers
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto">
                  <div class="mt20 mt-md50">
                     <img src="assets/images/unlimited-clients.webp" class="img-fluid mx-auto d-block">
                  </div>
               </div>
               <div class="col-12 col-md-12">
                  <div class="f-18 f-md-20 w400 lh140 mt10 mt-md40 text-left col-12 ">
                     You can assign them limited or full access to features according to their role with separate login details to outsource your manual work.
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="grey-section">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-12">
                  <div>
                     <img src="../common_assets/images/4.webp" class="img-responsive">
                  </div>
                  <div class="col-md-12 col-12 f-24 f-md-36 w500 lh140 mt10 mt-md10 text-left black-clr ">
                     Serve Unlimited Clients with Agency License
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto">
                  <div class="mt20 mt-md50">
                     <img src="assets/images/team-manage.webp" class="img-fluid mx-auto d-block">
                  </div>
               </div>
               <div class="col-12 col-md-12">
                  <div class="f-18 f-md-20 w400 lh140 mt10 mt-md40 text-left">
                     <span class="w600">Ninja Ai Agency</span> gives you the complete power to go beyond the normal and have a passive income source by serving unlimited clients in a hands down manner
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="white-section">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-12">
                  <div>
                     <img src="../common_assets/images/5.webp" class="img-responsive">
                  </div>
                  <div class="col-md-12 col-12 f-24 f-md-36 w500 lh140 mt10 mt-md10 text-left black-clr ">
                     Get All These Benefits For One Time Price
                  </div>
               </div>
               <div class="col-12 col-md-5 order-md-1">
                  <div class="mt20 mt-md20">
                     <img src="assets/images/benefit-bg.webp" class="img-fluid mx-auto d-block">
                  </div>
               </div>
               <div class="col-12 col-md-7 order-md-6 mt-md10 mt0">
                  <div class="f-18 f-md-20 w400 lh140 mt20 mt-md80">
                     And here's the best part. When you get access to <span class="w600">Ninja Ai Agency,</span> you can get all these benefits by paying just a small one time price. Now that's something you can't afford to miss out on
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="grey-section">
         <div class="container">
            <div class="row">
               <div class="col-12 mt10 mt-md10">
                  <div class="f-24 f-md-65 w700 lh140 text-center red-clr">
                     THINK
                  </div>
                  <div class="f-20 f-md-36 w600 lh140 mt10 mt-md10 text-center black-clr">
                     This Much Money You Would Make BY Selling Agency Version
                  </div>
               </div>
               <div class="col-12 mx-auto">
                  <div class="mt20 mt-md50">
                     <img src="assets/images/think-img.png" class="img-fluid mx-auto d-block">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--money back Start -->
      <div class="moneyback-section">
         <div class="container">
            <div class="row align-items-center d-flex flex-wrap">
               <div class="col-12 mb-md30">
                  <div class="f-28 f-md-45 lh140 w700 text-center white-clr">
                     30 Days Money Back Guarantee
                  </div>
               </div>
               <div class="col-md-4 col-12 mt30 mt-md40">
                  <img src="assets/images/moneyback.webp" class="img-fluid mx-auto d-block">
               </div>
               <div class="col-md-8 col-12 mt30">
                  <div class="f-18 f-md-20 w400 lh140 white-clr">
                     We have absolutely no doubt that you'll love the extra benefits, training and Ninja Ai Agency upgraded features.
                     <br>
                     <br> You can try it out without any risk. If, for any reason, you’re not satisfied with your Ninja Ai Agency upgrade you can simply ask for a refund. Your investment is covered by my 30-Days money back guarantee.
                     <br>
                     <br> You have absolutely nothing to lose and everything to gain with this fantastic offer and guarantee.
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--money back End-->
      <!---Bonus Section Starts-->
      <div class="bonus-section">
         <div class="container">
            <div class="row">
            <div class="col-12 text-center">
                                <div class="bonus-head f-28 f-md-48 w600 lh140 text-center white-clr">
                                    But That's Not All
                                 </div>
                        </div>
               <div class="col-12 mt20 f-20 f-md-24 w400 text-center lh140">
                  In addition, we have a number of bonuses for those who want to take action today and start profiting from this opportunity
               </div>
               <!-- bonus1 -->
               <div class="col-12 col-md-12  mt20 mt-md60 d-flex align-items-center flex-wrap">
                  <div class="col-12 col-md-6 px-md15">
                  <img src="assets/images/bonus1.webp" alt="Bonus 1" class="img-fluid d-block ">
                     <div class="f-24 f-md-30 w500 lh140 mt20 mt-md20 text-left black-clr">
                     Key Strategies To Build Trust Instantly
                     </div>
                     <div class="f-18 f-md-20 w400 lh140 mt10 mt-md10 text-left">
                     This useful package that Utilize active listening, maintain eye contact, and display genuine empathy. Establish credibility through honesty and consistency. Respect boundaries and deliver on promises promptly. These strategies foster instant trust and connections.. Use it without fail with Ninja AI Agency upgrade, and see results like you always wanted.
                     </div>
                  </div>
                  <div class="col-12 col-md-6">
                     <div class="mt20 mt-md0">
                        <img src="assets/images/bonusbox.webp" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>
               <!-- bonus2 -->
               <div class="col-12 col-md-12  mt20 mt-md60 d-flex align-items-center flex-wrap">
                  <div class="col-12 col-md-6 order-md-1">
                  <img src="assets/images/bonus2.webp" alt="Bonus 2" class="img-fluid d-block ">
                     <div class="f-24 f-md-30 w500 lh140 mt20 mt-md20 text-left black-clr">
                     Outsourcing Fundamentals Development and Strategy
                     </div>
                     <div class="f-18 f-md-20 w400 lh140 mt10 mt-md10 text-left">
                     This internet marketing report based on outsourcing fundamentals development and strategy includes blog posts, forums, YouTube videos and other related stuff for your business growth. Use it with the immense powers of NinjaAIAgency Upgrade to get results that your competitors envy.
                     </div>
                  </div>
                  <div class="col-12 col-md-6 order-md-6">
                     <div class="mt20 mt-md50">
                        <img src="assets/images/bonusbox.webp" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>
               <!-- bonus3 -->
               <div class="col-12 col-md-12  mt20 mt-md60 d-flex align-items-center flex-wrap">
                  <div class="col-12 col-md-6 px-md15">
                  <img src="assets/images/bonus3.webp" alt="Bonus 4" class="img-fluid d-block ">
                     <div class="f-24 f-md-30 w500 lh140 mt20 mt-md20 text-left black-clr">
                     The Copywriter's Handbook
                     </div>
                     <div class="f-18 f-md-20 w400 lh140 mt10 mt-md10 text-left">
                     Get your hands on proven tips and tricks used by world’s top marketers and learn why they became such envious success stories. Use this information with Ninja AI Agency Upgrade, and see yourself on the road to online marketing success.
                     </div>
                  </div>
                  <div class="col-12 col-md-6">
                     <div class="mt20 mt-md0">
                        <img src="assets/images/bonusbox.webp" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>
               <!-- bonus4 -->
               <div class="col-12 col-md-12  mt40 mt-md60 d-flex align-items-center flex-wrap">
                  <div class="col-12 col-md-6 order-md-1">
                  <img src="assets/images/bonus4.webp" alt="Bonus 4" class="img-fluid d-block ">
                     <div class="f-24 f-md-30 w500 lh140 mt20 mt-md20 text-left black-clr">
                     Recognizing Target Markets
                     </div>
                     <div class="f-18 f-md-20 w400 lh140 mt10 mt-md10 text-left">
                     Venture into the right market and tap into tons of untapped clients that are right there to be grabbed. When combined with Ninja AI Agency Upgrade powers, this package becomes a sure shot business booster.
                     </div>
                  </div>
                  <div class="col-12 col-md-6 order-md-6 mt-md30">
                     <div class="mt20 mt-md0">
                        <img src="assets/images/bonusbox.webp" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>
               <!-- bonus5 -->
               <div class="col-12 col-md-12  mt20 mt-md60 d-flex align-items-center flex-wrap">
                  <div class="col-12 col-md-6 px-md15">
                  <img src="assets/images/bonus5.png" alt="Bonus 5" class="img-fluid d-block ">
                     <div class="f-24 f-md-30 w500 lh140 mt20 mt-md20 text-left black-clr">
                        Recognizing Target Markets
                     </div>
                     <div class="f-18 f-md-20 w400 lh140 mt10 mt-md10 text-left">
                        Venture into the right market and tap into tons of untapped clients that are right there to be grabbed. When combined with Ninja Ai Agency Upgrade powers, this package becomes a sure shot business booster.
                     </div>
                  </div>
                  <div class="col-12 col-md-6">
                     <div class="mt20 mt-md0">
                        <img src="assets/images/bonusbox.webp" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--Bonus Section ends -->
      <!--10. Table Section Starts -->
      <div class="table-section padding10" id="buynow">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-12">
                  <div class="f-md-45 f-28 w500 lh140 mt20 mt-md30 text-center">
                     Today You Can Get Unrestricted Access To <span class="w700">Ninja Ai Agency Suite For <span class="">LESS Than The Price Of Just One Month’s</span> Membership.</span>
                  </div>
               </div>
               <div class="col-md-6 col-12  mt30 mt-md-90">
                  <div class="tablebox2" editabletype="shape" style="z-index: 8;">
                     <div class="tbbg2 text-center">
                        <div>
                        <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 645.02 151.26" style="max-height:70px">
                                       <defs>
                                         <style>
                                           .cls-1a {
                                             fill: #06101f;
                                           }
                                     
                                           .cls-2a {
                                             fill: url(#linear-gradient-2);
                                           }
                                     
                                           .cls-3a {
                                             fill-rule: evenodd;
                                           }
                                     
                                           .cls-3a, .cls-4a {
                                             fill: #ff813b;
                                           }
                                     
                                           .cls-5a {
                                             fill: url(#linear-gradient-3);
                                           }
                                     
                                           .cls-6a {
                                             fill: url(#linear-gradient);
                                           }
                                     
                                           .cls-7a {
                                             fill: url(#linear-gradient-4);
                                           }
                                         </style>
                                         <linearGradient id="linear-gradient" x1="47.81" y1="75" x2="185.48" y2="75" gradientUnits="userSpaceOnUse">
                                           <stop offset="0" stop-color="#b956ff"></stop>
                                           <stop offset="1" stop-color="#6e33ff"></stop>
                                         </linearGradient>
                                         <linearGradient id="linear-gradient-2" x1="0" y1="75" x2="85.18" y2="75" gradientUnits="userSpaceOnUse">
                                           <stop offset="0" stop-color="#b956ff"></stop>
                                           <stop offset="1" stop-color="#a356fa"></stop>
                                         </linearGradient>
                                         <linearGradient id="linear-gradient-3" x1="55.61" y1="62.54" x2="62.86" y2="62.54" gradientUnits="userSpaceOnUse">
                                           <stop offset="0" stop-color="#b956ff"></stop>
                                           <stop offset="1" stop-color="#ac59fa"></stop>
                                         </linearGradient>
                                         <linearGradient id="linear-gradient-4" x1="54.62" y1="79" x2="61.87" y2="79" gradientTransform="translate(19.64 -11.24) rotate(13.24)" xlink:href="#linear-gradient-3"></linearGradient>
                                       </defs>
                                       <g id="Layer_1-2" data-name="Layer 1">
                                         <g>
                                           <g>
                                             <g>
                                               <path class="cls-6a" d="m181.49,58.53h-10.72c-1.4-5.13-3.43-9.99-6.01-14.51l7.58-7.58c1.56-1.56,1.56-4.09,0-5.65l-17.65-17.65c-1.56-1.56-4.09-1.56-5.65,0l-7.58,7.58c-4.51-2.58-9.38-4.61-14.51-6.01V4c0-2.21-1.79-4-4-4h-24.96c-2.21,0-4,1.79-4,4v10.72c-.16.04-.31.09-.47.14-.27.08-.53.15-.8.23-.27.08-.55.16-.82.24-.55.17-1.09.35-1.63.53-.11.04-.22.08-.33.12-.48.17-.96.34-1.44.52-.12.04-.24.09-.36.14-.52.2-1.04.4-1.56.62-.04.02-.08.03-.12.05-2.41,1-4.74,2.15-6.99,3.43l-7.57-7.57c-1.56-1.56-4.09-1.56-5.65,0l-17.65,17.65c-.36.36-.63.76-.82,1.2h61.99v.02c.23,0,.45-.02.68-.02,23.76,0,43.01,19.26,43.01,43.01s-19.26,43.01-43.01,43.01c-.5,0-1-.02-1.5-.04v.04h-61.17c.19.43.46.84.82,1.2l17.65,17.65c1.56,1.56,4.09,1.56,5.65,0l7.57-7.57c2.25,1.28,4.58,2.43,6.99,3.43.04.02.08.03.12.05.51.21,1.03.42,1.55.62.12.05.24.09.36.14.47.18.95.35,1.43.52.11.04.23.08.34.12.54.18,1.08.36,1.63.53.27.08.55.16.83.25.26.08.53.16.79.23.16.04.31.09.47.14v10.72c0,2.21,1.79,4,4,4h24.96c2.21,0,4-1.79,4-4v-10.72c5.13-1.4,9.99-3.43,14.51-6.01l7.58,7.58c1.56,1.56,4.09,1.56,5.65,0l17.65-17.65c1.56-1.56,1.56-4.09,0-5.65l-7.58-7.58c2.58-4.51,4.61-9.38,6.01-14.51h10.72c2.21,0,4-1.79,4-4v-24.96c0-2.21-1.79-4-4-4Z"></path>
                                               <path class="cls-2a" d="m76.64,101.54c-5.74-7.31-9.18-16.52-9.18-26.54s3.44-19.23,9.18-26.54c2.45-3.13,5.33-5.9,8.53-8.24h-48.39c-1.31-1.89-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.13h17.07c-1.51,3.21-2.76,6.58-3.71,10.07h3.1c1.31-1.89,3.49-3.13,5.97-3.13,4.01,0,7.25,3.25,7.25,7.25s-3.22,7.23-7.21,7.25h-.04c-2.48,0-4.66-1.24-5.97-3.13H13.22c-.44-.64-.98-1.19-1.59-1.65-1.21-.92-2.73-1.48-4.38-1.48-4,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.14h38.82c1.31-1.89,3.49-3.13,5.97-3.13.43,0,.84.05,1.25.12,3.41.59,6,3.56,6,7.14,0,2.78-1.56,5.19-3.86,6.41-1.01.53-2.16.84-3.39.84-2.48,0-4.66-1.24-5.97-3.13h-17.85c-1.31-1.9-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.13h15.96c.95,3.48,2.2,6.85,3.71,10.07h-5.63c-1.31-1.89-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25,0,1.53.48,2.95,1.28,4.12,1.31,1.9,3.49,3.14,5.97,3.14s4.66-1.24,5.97-3.14h36.95c-3.21-2.34-6.08-5.11-8.53-8.24ZM30.64,47.97c-2,0-3.62-1.62-3.62-3.62s1.62-3.62,3.62-3.62,3.63,1.62,3.63,3.62-1.63,3.62-3.63,3.62Zm-23.39,26.47c-2,0-3.62-1.63-3.62-3.62s1.63-3.63,3.62-3.63,3.63,1.63,3.63,3.63-1.63,3.62-3.63,3.62Zm20.97,16.45c-2,0-3.63-1.63-3.63-3.63s1.63-3.62,3.63-3.62,3.62,1.63,3.62,3.62-1.63,3.63-3.62,3.63Zm14.08,18.39c-2,0-3.62-1.62-3.62-3.62s1.62-3.62,3.62-3.62,3.62,1.62,3.62,3.62-1.62,3.62-3.62,3.62Z"></path>
                                               <circle class="cls-5a" cx="59.23" cy="62.54" r="3.63"></circle>
                                               <circle class="cls-7a" cx="58.24" cy="79" r="3.63" transform="translate(-16.54 15.44) rotate(-13.24)"></circle>
                                             </g>
                                             <g>
                                               <g>
                                                 <path class="cls-3a" d="m104.63,78.16c-9.44,6-14.52,2.86-15.25-9.4,4.93,3.38,10.02,6.52,15.25,9.4Z"></path>
                                                 <path class="cls-3a" d="m131.01,68.76c-.72,12.26-5.81,15.4-15.25,9.4,5.24-2.88,10.32-6.02,15.25-9.4Z"></path>
                                               </g>
                                               <path class="cls-4" d="m110.16,40.13c-.89,0-1.78.03-2.66.1-.08,0-.16,0-.24.02-9.59.79-18.07,5.45-23.89,12.42-.25.3-.49.59-.73.89-.14.19-.29.38-.43.57-4.02,5.37-6.52,11.93-6.88,19.07,0,.01,0,.03,0,.04-.01.22-.02.45-.03.68-.01.3-.02.6-.02.9v.18c0,.28,0,.55,0,.83.44,18.64,15.5,33.66,34.15,34.04.24,0,.48,0,.72,0,.3,0,.6,0,.9,0,18.85-.48,33.98-15.9,33.98-34.87s-15.61-34.88-34.88-34.88Zm.19,39.35c-14.41,14.37-29.15,1.23-29.15-9.45s13.05,0,29.15,0,29.15-10.67,29.15,0-14.74,23.81-29.15,9.45Z"></path>
                                             </g>
                                           </g>
                                           <g>
                                             <path class="cls-1a" d="m286.65,118.51h-17.37l-39.32-59.42v59.42h-17.37V31.8h17.37l39.32,59.54V31.8h17.37v86.71Z"></path>
                                             <path class="cls-1a" d="m304.95,38.69c-2.03-1.94-3.04-4.36-3.04-7.26s1.01-5.31,3.04-7.26c2.03-1.94,4.57-2.92,7.63-2.92s5.6.97,7.63,2.92c2.02,1.94,3.04,4.36,3.04,7.26s-1.02,5.31-3.04,7.26c-2.03,1.94-4.57,2.91-7.63,2.91s-5.6-.97-7.63-2.91Zm16.19,11.1v68.72h-17.37V49.79h17.37Z"></path>
                                             <path class="cls-1a" d="m396.18,56.55c5.04,5.17,7.57,12.38,7.57,21.65v40.31h-17.36v-37.96c0-5.46-1.37-9.65-4.1-12.59-2.73-2.94-6.45-4.4-11.16-4.4s-8.58,1.47-11.35,4.4c-2.77,2.94-4.15,7.13-4.15,12.59v37.96h-17.37V49.79h17.37v8.56c2.31-2.98,5.27-5.31,8.87-7.01,3.6-1.69,7.55-2.54,11.85-2.54,8.19,0,14.8,2.59,19.85,7.75Z"></path>
                                             <path class="cls-1a" d="m437.61,129.8c0,7.61-1.88,13.09-5.64,16.44-3.77,3.35-9.16,5.02-16.19,5.02h-7.69v-14.76h4.96c2.64,0,4.51-.52,5.58-1.55,1.07-1.03,1.61-2.71,1.61-5.02V49.79h17.37v80.01Zm-16.31-91.11c-2.03-1.94-3.04-4.36-3.04-7.26s1.01-5.31,3.04-7.26c2.02-1.94,4.61-2.92,7.75-2.92s5.58.97,7.57,2.92c1.98,1.94,2.98,4.36,2.98,7.26s-.99,5.31-2.98,7.26c-1.98,1.94-4.51,2.91-7.57,2.91s-5.73-.97-7.75-2.91Z"></path>
                                             <path class="cls-1a" d="m454.42,65.42c2.77-5.37,6.53-9.51,11.29-12.4,4.76-2.89,10.07-4.34,15.94-4.34,5.13,0,9.61,1.04,13.46,3.1,3.85,2.07,6.92,4.67,9.24,7.82v-9.8h17.49v68.72h-17.49v-10.05c-2.23,3.23-5.31,5.89-9.24,8-3.93,2.11-8.46,3.16-13.58,3.16-5.79,0-11.06-1.49-15.82-4.47-4.76-2.98-8.52-7.17-11.29-12.59-2.77-5.41-4.16-11.64-4.16-18.67s1.38-13.11,4.16-18.48Zm47.45,7.88c-1.65-3.02-3.89-5.33-6.7-6.95-2.81-1.61-5.83-2.42-9.06-2.42s-6.2.79-8.93,2.36c-2.73,1.57-4.94,3.87-6.64,6.88-1.69,3.02-2.54,6.6-2.54,10.73s.85,7.75,2.54,10.85c1.69,3.1,3.93,5.48,6.7,7.13,2.77,1.66,5.72,2.48,8.87,2.48s6.24-.81,9.06-2.42c2.81-1.61,5.04-3.93,6.7-6.95,1.65-3.02,2.48-6.64,2.48-10.85s-.83-7.83-2.48-10.85Z"></path>
                                             <path class="cls-4a" d="m591.93,102.01h-34.48l-5.71,16.5h-18.24l31.14-86.71h20.22l31.13,86.71h-18.36l-5.71-16.5Zm-4.71-13.89l-12.53-36.22-12.53,36.22h25.06Z"></path>
                                             <path class="cls-4a" d="m645.02,31.93v86.58h-17.37V31.93h17.37Z"></path>
                                           </g>
                                         </g>
                                       </g>
                                     </svg>
                        </div>
                        <div class="text-uppercase mt15 mt-md15 w600 f-md-30 f-22 text-center black-clr lh120">Agency 100 Client Plan</div>
                     </div>
                     <div>
                        <ul class="f-18 w400 lh140 text-center vgreytick mb0">
                           <li>Directly Sell Ninja Ai Services and Charge Monthly or Recurring Amount For 100% Profits</li>
                           <li>Comes with Dedicated Dashboard to Create Accounts for the Customers in 3 Simple Clicks</li>
                           <li>Completely Cloud-Based Tool, so no additional domain or hosting required</li>
                           <li>Serve Up to 100 Clients with Agency License </li>
                           <li>Unparallel Price with No Recurring Fee</li>
                        </ul>
                     </div>
                     <div class="myfeatureslast f-md-25 f-16 w400 text-center lh150 hideme" editabletype="shape" style="opacity: 1; z-index: 9;">
                       
                        <div class="" editabletype="button" style="z-index: 10;">
                           <a href="https://warriorplus.com/o2/buy/zh5gzv/stszb4/vqkh07"><img src="https://warriorplus.com/o2/btn/fn200011000/zh5gzv/stszb4/357338" class="img-fluid mx-auto d-block"></a>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-6 col-12  mt30 mt-md-90">
                  <div class="tablebox3" editabletype="shape" style="z-index: 8;">
                     <div class="tbbg3 text-center">
                        <div>
                        <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 645.02 151.26" style="max-height:70px">
                              <defs>
                                <style>
                                  .cls-1 {
                                    fill: url(#linear-gradient-2);
                                  }
                            
                                  .cls-2 {
                                    fill: #fff;
                                  }
                            
                                  .cls-3 {
                                    fill-rule: evenodd;
                                  }
                            
                                  .cls-3, .cls-4 {
                                    fill: #ff813b;
                                  }
                            
                                  .cls-5 {
                                    fill: url(#linear-gradient-3);
                                  }
                            
                                  .cls-6 {
                                    fill: url(#linear-gradient);
                                  }
                            
                                  .cls-7 {
                                    fill: url(#linear-gradient-4);
                                  }
                                </style>
                                <linearGradient id="linear-gradient" x1="47.81" y1="75" x2="185.48" y2="75" gradientUnits="userSpaceOnUse">
                                  <stop offset="0" stop-color="#b956ff"></stop>
                                  <stop offset="1" stop-color="#6e33ff"></stop>
                                </linearGradient>
                                <linearGradient id="linear-gradient-2" x1="0" y1="75" x2="85.18" y2="75" gradientUnits="userSpaceOnUse">
                                  <stop offset="0" stop-color="#b956ff"></stop>
                                  <stop offset="1" stop-color="#a356fa"></stop>
                                </linearGradient>
                                <linearGradient id="linear-gradient-3" x1="55.61" y1="62.54" x2="62.86" y2="62.54" gradientUnits="userSpaceOnUse">
                                  <stop offset="0" stop-color="#b956ff"></stop>
                                  <stop offset="1" stop-color="#ac59fa"></stop>
                                </linearGradient>
                                <linearGradient id="linear-gradient-4" x1="54.62" y1="79" x2="61.87" y2="79" gradientTransform="translate(19.64 -11.24) rotate(13.24)" xlink:href="#linear-gradient-3"></linearGradient>
                              </defs>
                              <g id="Layer_1-2" data-name="Layer 1">
                                <g>
                                  <g>
                                    <g>
                                      <path class="cls-6" d="m181.49,58.53h-10.72c-1.4-5.13-3.43-9.99-6.01-14.51l7.58-7.58c1.56-1.56,1.56-4.09,0-5.65l-17.65-17.65c-1.56-1.56-4.09-1.56-5.65,0l-7.58,7.58c-4.51-2.58-9.38-4.61-14.51-6.01V4c0-2.21-1.79-4-4-4h-24.96c-2.21,0-4,1.79-4,4v10.72c-.16.04-.31.09-.47.14-.27.08-.53.15-.8.23-.27.08-.55.16-.82.24-.55.17-1.09.35-1.63.53-.11.04-.22.08-.33.12-.48.17-.96.34-1.44.52-.12.04-.24.09-.36.14-.52.2-1.04.4-1.56.62-.04.02-.08.03-.12.05-2.41,1-4.74,2.15-6.99,3.43l-7.57-7.57c-1.56-1.56-4.09-1.56-5.65,0l-17.65,17.65c-.36.36-.63.76-.82,1.2h61.99v.02c.23,0,.45-.02.68-.02,23.76,0,43.01,19.26,43.01,43.01s-19.26,43.01-43.01,43.01c-.5,0-1-.02-1.5-.04v.04h-61.17c.19.43.46.84.82,1.2l17.65,17.65c1.56,1.56,4.09,1.56,5.65,0l7.57-7.57c2.25,1.28,4.58,2.43,6.99,3.43.04.02.08.03.12.05.51.21,1.03.42,1.55.62.12.05.24.09.36.14.47.18.95.35,1.43.52.11.04.23.08.34.12.54.18,1.08.36,1.63.53.27.08.55.16.83.25.26.08.53.16.79.23.16.04.31.09.47.14v10.72c0,2.21,1.79,4,4,4h24.96c2.21,0,4-1.79,4-4v-10.72c5.13-1.4,9.99-3.43,14.51-6.01l7.58,7.58c1.56,1.56,4.09,1.56,5.65,0l17.65-17.65c1.56-1.56,1.56-4.09,0-5.65l-7.58-7.58c2.58-4.51,4.61-9.38,6.01-14.51h10.72c2.21,0,4-1.79,4-4v-24.96c0-2.21-1.79-4-4-4Z"></path>
                                      <path class="cls-1" d="m76.64,101.54c-5.74-7.31-9.18-16.52-9.18-26.54s3.44-19.23,9.18-26.54c2.45-3.13,5.33-5.9,8.53-8.24h-48.39c-1.31-1.89-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.13h17.07c-1.51,3.21-2.76,6.58-3.71,10.07h3.1c1.31-1.89,3.49-3.13,5.97-3.13,4.01,0,7.25,3.25,7.25,7.25s-3.22,7.23-7.21,7.25h-.04c-2.48,0-4.66-1.24-5.97-3.13H13.22c-.44-.64-.98-1.19-1.59-1.65-1.21-.92-2.73-1.48-4.38-1.48-4,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.14h38.82c1.31-1.89,3.49-3.13,5.97-3.13.43,0,.84.05,1.25.12,3.41.59,6,3.56,6,7.14,0,2.78-1.56,5.19-3.86,6.41-1.01.53-2.16.84-3.39.84-2.48,0-4.66-1.24-5.97-3.13h-17.85c-1.31-1.9-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.13h15.96c.95,3.48,2.2,6.85,3.71,10.07h-5.63c-1.31-1.89-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25,0,1.53.48,2.95,1.28,4.12,1.31,1.9,3.49,3.14,5.97,3.14s4.66-1.24,5.97-3.14h36.95c-3.21-2.34-6.08-5.11-8.53-8.24ZM30.64,47.97c-2,0-3.62-1.62-3.62-3.62s1.62-3.62,3.62-3.62,3.63,1.62,3.63,3.62-1.63,3.62-3.63,3.62Zm-23.39,26.47c-2,0-3.62-1.63-3.62-3.62s1.63-3.63,3.62-3.63,3.63,1.63,3.63,3.63-1.63,3.62-3.63,3.62Zm20.97,16.45c-2,0-3.63-1.63-3.63-3.63s1.63-3.62,3.63-3.62,3.62,1.63,3.62,3.62-1.63,3.63-3.62,3.63Zm14.08,18.39c-2,0-3.62-1.62-3.62-3.62s1.62-3.62,3.62-3.62,3.62,1.62,3.62,3.62-1.62,3.62-3.62,3.62Z"></path>
                                      <circle class="cls-5" cx="59.23" cy="62.54" r="3.63"></circle>
                                      <circle class="cls-7" cx="58.24" cy="79" r="3.63" transform="translate(-16.54 15.44) rotate(-13.24)"></circle>
                                    </g>
                                    <g>
                                      <g>
                                        <path class="cls-3" d="m104.63,78.16c-9.44,6-14.52,2.86-15.25-9.4,4.93,3.38,10.02,6.52,15.25,9.4Z"></path>
                                        <path class="cls-3" d="m131.01,68.76c-.72,12.26-5.81,15.4-15.25,9.4,5.24-2.88,10.32-6.02,15.25-9.4Z"></path>
                                      </g>
                                      <path class="cls-4" d="m110.16,40.13c-.89,0-1.78.03-2.66.1-.08,0-.16,0-.24.02-9.59.79-18.07,5.45-23.89,12.42-.25.3-.49.59-.73.89-.14.19-.29.38-.43.57-4.02,5.37-6.52,11.93-6.88,19.07,0,.01,0,.03,0,.04-.01.22-.02.45-.03.68-.01.3-.02.6-.02.9v.18c0,.28,0,.55,0,.83.44,18.64,15.5,33.66,34.15,34.04.24,0,.48,0,.72,0,.3,0,.6,0,.9,0,18.85-.48,33.98-15.9,33.98-34.87s-15.61-34.88-34.88-34.88Zm.19,39.35c-14.41,14.37-29.15,1.23-29.15-9.45s13.05,0,29.15,0,29.15-10.67,29.15,0-14.74,23.81-29.15,9.45Z"></path>
                                    </g>
                                  </g>
                                  <g>
                                    <path class="cls-2" d="m286.65,118.51h-17.37l-39.32-59.42v59.42h-17.37V31.8h17.37l39.32,59.54V31.8h17.37v86.71Z"></path>
                                    <path class="cls-2" d="m304.95,38.69c-2.03-1.94-3.04-4.36-3.04-7.26s1.01-5.31,3.04-7.26c2.03-1.94,4.57-2.92,7.63-2.92s5.6.97,7.63,2.92c2.02,1.94,3.04,4.36,3.04,7.26s-1.02,5.31-3.04,7.26c-2.03,1.94-4.57,2.91-7.63,2.91s-5.6-.97-7.63-2.91Zm16.19,11.1v68.72h-17.37V49.79h17.37Z"></path>
                                    <path class="cls-2" d="m396.18,56.55c5.04,5.17,7.57,12.38,7.57,21.65v40.31h-17.36v-37.96c0-5.46-1.37-9.65-4.1-12.59-2.73-2.94-6.45-4.4-11.16-4.4s-8.58,1.47-11.35,4.4c-2.77,2.94-4.15,7.13-4.15,12.59v37.96h-17.37V49.79h17.37v8.56c2.31-2.98,5.27-5.31,8.87-7.01,3.6-1.69,7.55-2.54,11.85-2.54,8.19,0,14.8,2.59,19.85,7.75Z"></path>
                                    <path class="cls-2" d="m437.61,129.8c0,7.61-1.88,13.09-5.64,16.44-3.77,3.35-9.16,5.02-16.19,5.02h-7.69v-14.76h4.96c2.64,0,4.51-.52,5.58-1.55,1.07-1.03,1.61-2.71,1.61-5.02V49.79h17.37v80.01Zm-16.31-91.11c-2.03-1.94-3.04-4.36-3.04-7.26s1.01-5.31,3.04-7.26c2.02-1.94,4.61-2.92,7.75-2.92s5.58.97,7.57,2.92c1.98,1.94,2.98,4.36,2.98,7.26s-.99,5.31-2.98,7.26c-1.98,1.94-4.51,2.91-7.57,2.91s-5.73-.97-7.75-2.91Z"></path>
                                    <path class="cls-2" d="m454.42,65.42c2.77-5.37,6.53-9.51,11.29-12.4,4.76-2.89,10.07-4.34,15.94-4.34,5.13,0,9.61,1.04,13.46,3.1,3.85,2.07,6.92,4.67,9.24,7.82v-9.8h17.49v68.72h-17.49v-10.05c-2.23,3.23-5.31,5.89-9.24,8-3.93,2.11-8.46,3.16-13.58,3.16-5.79,0-11.06-1.49-15.82-4.47-4.76-2.98-8.52-7.17-11.29-12.59-2.77-5.41-4.16-11.64-4.16-18.67s1.38-13.11,4.16-18.48Zm47.45,7.88c-1.65-3.02-3.89-5.33-6.7-6.95-2.81-1.61-5.83-2.42-9.06-2.42s-6.2.79-8.93,2.36c-2.73,1.57-4.94,3.87-6.64,6.88-1.69,3.02-2.54,6.6-2.54,10.73s.85,7.75,2.54,10.85c1.69,3.1,3.93,5.48,6.7,7.13,2.77,1.66,5.72,2.48,8.87,2.48s6.24-.81,9.06-2.42c2.81-1.61,5.04-3.93,6.7-6.95,1.65-3.02,2.48-6.64,2.48-10.85s-.83-7.83-2.48-10.85Z"></path>
                                    <path class="cls-4" d="m591.93,102.01h-34.48l-5.71,16.5h-18.24l31.14-86.71h20.22l31.13,86.71h-18.36l-5.71-16.5Zm-4.71-13.89l-12.53-36.22-12.53,36.22h25.06Z"></path>
                                    <path class="cls-4" d="m645.02,31.93v86.58h-17.37V31.93h17.37Z"></path>
                                  </g>
                                </g>
                              </g>
                            </svg>
                        </div>
                        <div class="text-uppercase mt15 mt-md15 w600 f-md-30 f-22 text-center white-clr lh120">Agency Unlimited Client Plan</div>
                     </div>
                     <div>
                        <ul class="f-18 w400 lh140 text-center grey-tick-last mb0 white-clr">
                           <li>Directly Sell Ninja Ai Services and Charge Monthly or Recurring Amount For 100% Profits</li>
                           <li>Comes with Dedicated Dashboard to Create Accounts for the Customers in 3 Simple Clicks</li>
                           <li>Completely Cloud-Based Tool, so no additional domain or hosting required</li>
                           <li>Serve Unlimited Clients with Agency License </li>
                           <li>Unparallel Price with No Recurring Fee</li>
                        </ul>
                     </div>
                     <div class="myfeatureslast myfeatureslastborder f-md-25 f-16 w400 text-center lh150 hideme" editabletype="shape" style="opacity: 1; z-index: 9;">
                       
                        <div class="" editabletype="button" style="z-index: 10;">
                           <a href="https://warriorplus.com/o2/buy/zh5gzv/stszb4/qr34pz"><img src="https://warriorplus.com/o2/btn/fn200011000/zh5gzv/stszb4/357339" class="img-fluid mx-auto d-block"></a>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt40 mt-md50 text-left thanks-button text-center">
                  <a href="https://warriorplus.com/o/nothanks/stszb4" target="_blank" class="kapblue f-18 f-md-22 lh140 w400">
                  No thanks - I don't want to use the untapped POWER to setup my own pro agency without doing any extra work. I know that Ninja Ai Agency Upgrade can increase my profits & I duly realize that I won't get this offer again. Please take me to the next step to get access to my purchase. 
                  </a>
               </div>
            </div>
         </div>
      </div>
      <!--10. Table Section End -->
      <!--Footer Section Start -->
      <div class="footer-section">
         <div class="container ">
            <div class="row">
               <div class="col-12 text-center">
               <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 645.02 151.26" style="max-height:70px">
                              <defs>
                                <style>
                                  .cls-1 {
                                    fill: url(#linear-gradient-2);
                                  }
                            
                                  .cls-2 {
                                    fill: #fff;
                                  }
                            
                                  .cls-3 {
                                    fill-rule: evenodd;
                                  }
                            
                                  .cls-3, .cls-4 {
                                    fill: #ff813b;
                                  }
                            
                                  .cls-5 {
                                    fill: url(#linear-gradient-3);
                                  }
                            
                                  .cls-6 {
                                    fill: url(#linear-gradient);
                                  }
                            
                                  .cls-7 {
                                    fill: url(#linear-gradient-4);
                                  }
                                </style>
                                <linearGradient id="linear-gradient" x1="47.81" y1="75" x2="185.48" y2="75" gradientUnits="userSpaceOnUse">
                                  <stop offset="0" stop-color="#b956ff"></stop>
                                  <stop offset="1" stop-color="#6e33ff"></stop>
                                </linearGradient>
                                <linearGradient id="linear-gradient-2" x1="0" y1="75" x2="85.18" y2="75" gradientUnits="userSpaceOnUse">
                                  <stop offset="0" stop-color="#b956ff"></stop>
                                  <stop offset="1" stop-color="#a356fa"></stop>
                                </linearGradient>
                                <linearGradient id="linear-gradient-3" x1="55.61" y1="62.54" x2="62.86" y2="62.54" gradientUnits="userSpaceOnUse">
                                  <stop offset="0" stop-color="#b956ff"></stop>
                                  <stop offset="1" stop-color="#ac59fa"></stop>
                                </linearGradient>
                                <linearGradient id="linear-gradient-4" x1="54.62" y1="79" x2="61.87" y2="79" gradientTransform="translate(19.64 -11.24) rotate(13.24)" xlink:href="#linear-gradient-3"></linearGradient>
                              </defs>
                              <g id="Layer_1-2" data-name="Layer 1">
                                <g>
                                  <g>
                                    <g>
                                      <path class="cls-6" d="m181.49,58.53h-10.72c-1.4-5.13-3.43-9.99-6.01-14.51l7.58-7.58c1.56-1.56,1.56-4.09,0-5.65l-17.65-17.65c-1.56-1.56-4.09-1.56-5.65,0l-7.58,7.58c-4.51-2.58-9.38-4.61-14.51-6.01V4c0-2.21-1.79-4-4-4h-24.96c-2.21,0-4,1.79-4,4v10.72c-.16.04-.31.09-.47.14-.27.08-.53.15-.8.23-.27.08-.55.16-.82.24-.55.17-1.09.35-1.63.53-.11.04-.22.08-.33.12-.48.17-.96.34-1.44.52-.12.04-.24.09-.36.14-.52.2-1.04.4-1.56.62-.04.02-.08.03-.12.05-2.41,1-4.74,2.15-6.99,3.43l-7.57-7.57c-1.56-1.56-4.09-1.56-5.65,0l-17.65,17.65c-.36.36-.63.76-.82,1.2h61.99v.02c.23,0,.45-.02.68-.02,23.76,0,43.01,19.26,43.01,43.01s-19.26,43.01-43.01,43.01c-.5,0-1-.02-1.5-.04v.04h-61.17c.19.43.46.84.82,1.2l17.65,17.65c1.56,1.56,4.09,1.56,5.65,0l7.57-7.57c2.25,1.28,4.58,2.43,6.99,3.43.04.02.08.03.12.05.51.21,1.03.42,1.55.62.12.05.24.09.36.14.47.18.95.35,1.43.52.11.04.23.08.34.12.54.18,1.08.36,1.63.53.27.08.55.16.83.25.26.08.53.16.79.23.16.04.31.09.47.14v10.72c0,2.21,1.79,4,4,4h24.96c2.21,0,4-1.79,4-4v-10.72c5.13-1.4,9.99-3.43,14.51-6.01l7.58,7.58c1.56,1.56,4.09,1.56,5.65,0l17.65-17.65c1.56-1.56,1.56-4.09,0-5.65l-7.58-7.58c2.58-4.51,4.61-9.38,6.01-14.51h10.72c2.21,0,4-1.79,4-4v-24.96c0-2.21-1.79-4-4-4Z"></path>
                                      <path class="cls-1" d="m76.64,101.54c-5.74-7.31-9.18-16.52-9.18-26.54s3.44-19.23,9.18-26.54c2.45-3.13,5.33-5.9,8.53-8.24h-48.39c-1.31-1.89-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.13h17.07c-1.51,3.21-2.76,6.58-3.71,10.07h3.1c1.31-1.89,3.49-3.13,5.97-3.13,4.01,0,7.25,3.25,7.25,7.25s-3.22,7.23-7.21,7.25h-.04c-2.48,0-4.66-1.24-5.97-3.13H13.22c-.44-.64-.98-1.19-1.59-1.65-1.21-.92-2.73-1.48-4.38-1.48-4,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.14h38.82c1.31-1.89,3.49-3.13,5.97-3.13.43,0,.84.05,1.25.12,3.41.59,6,3.56,6,7.14,0,2.78-1.56,5.19-3.86,6.41-1.01.53-2.16.84-3.39.84-2.48,0-4.66-1.24-5.97-3.13h-17.85c-1.31-1.9-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.13h15.96c.95,3.48,2.2,6.85,3.71,10.07h-5.63c-1.31-1.89-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25,0,1.53.48,2.95,1.28,4.12,1.31,1.9,3.49,3.14,5.97,3.14s4.66-1.24,5.97-3.14h36.95c-3.21-2.34-6.08-5.11-8.53-8.24ZM30.64,47.97c-2,0-3.62-1.62-3.62-3.62s1.62-3.62,3.62-3.62,3.63,1.62,3.63,3.62-1.63,3.62-3.63,3.62Zm-23.39,26.47c-2,0-3.62-1.63-3.62-3.62s1.63-3.63,3.62-3.63,3.63,1.63,3.63,3.63-1.63,3.62-3.63,3.62Zm20.97,16.45c-2,0-3.63-1.63-3.63-3.63s1.63-3.62,3.63-3.62,3.62,1.63,3.62,3.62-1.63,3.63-3.62,3.63Zm14.08,18.39c-2,0-3.62-1.62-3.62-3.62s1.62-3.62,3.62-3.62,3.62,1.62,3.62,3.62-1.62,3.62-3.62,3.62Z"></path>
                                      <circle class="cls-5" cx="59.23" cy="62.54" r="3.63"></circle>
                                      <circle class="cls-7" cx="58.24" cy="79" r="3.63" transform="translate(-16.54 15.44) rotate(-13.24)"></circle>
                                    </g>
                                    <g>
                                      <g>
                                        <path class="cls-3" d="m104.63,78.16c-9.44,6-14.52,2.86-15.25-9.4,4.93,3.38,10.02,6.52,15.25,9.4Z"></path>
                                        <path class="cls-3" d="m131.01,68.76c-.72,12.26-5.81,15.4-15.25,9.4,5.24-2.88,10.32-6.02,15.25-9.4Z"></path>
                                      </g>
                                      <path class="cls-4" d="m110.16,40.13c-.89,0-1.78.03-2.66.1-.08,0-.16,0-.24.02-9.59.79-18.07,5.45-23.89,12.42-.25.3-.49.59-.73.89-.14.19-.29.38-.43.57-4.02,5.37-6.52,11.93-6.88,19.07,0,.01,0,.03,0,.04-.01.22-.02.45-.03.68-.01.3-.02.6-.02.9v.18c0,.28,0,.55,0,.83.44,18.64,15.5,33.66,34.15,34.04.24,0,.48,0,.72,0,.3,0,.6,0,.9,0,18.85-.48,33.98-15.9,33.98-34.87s-15.61-34.88-34.88-34.88Zm.19,39.35c-14.41,14.37-29.15,1.23-29.15-9.45s13.05,0,29.15,0,29.15-10.67,29.15,0-14.74,23.81-29.15,9.45Z"></path>
                                    </g>
                                  </g>
                                  <g>
                                    <path class="cls-2" d="m286.65,118.51h-17.37l-39.32-59.42v59.42h-17.37V31.8h17.37l39.32,59.54V31.8h17.37v86.71Z"></path>
                                    <path class="cls-2" d="m304.95,38.69c-2.03-1.94-3.04-4.36-3.04-7.26s1.01-5.31,3.04-7.26c2.03-1.94,4.57-2.92,7.63-2.92s5.6.97,7.63,2.92c2.02,1.94,3.04,4.36,3.04,7.26s-1.02,5.31-3.04,7.26c-2.03,1.94-4.57,2.91-7.63,2.91s-5.6-.97-7.63-2.91Zm16.19,11.1v68.72h-17.37V49.79h17.37Z"></path>
                                    <path class="cls-2" d="m396.18,56.55c5.04,5.17,7.57,12.38,7.57,21.65v40.31h-17.36v-37.96c0-5.46-1.37-9.65-4.1-12.59-2.73-2.94-6.45-4.4-11.16-4.4s-8.58,1.47-11.35,4.4c-2.77,2.94-4.15,7.13-4.15,12.59v37.96h-17.37V49.79h17.37v8.56c2.31-2.98,5.27-5.31,8.87-7.01,3.6-1.69,7.55-2.54,11.85-2.54,8.19,0,14.8,2.59,19.85,7.75Z"></path>
                                    <path class="cls-2" d="m437.61,129.8c0,7.61-1.88,13.09-5.64,16.44-3.77,3.35-9.16,5.02-16.19,5.02h-7.69v-14.76h4.96c2.64,0,4.51-.52,5.58-1.55,1.07-1.03,1.61-2.71,1.61-5.02V49.79h17.37v80.01Zm-16.31-91.11c-2.03-1.94-3.04-4.36-3.04-7.26s1.01-5.31,3.04-7.26c2.02-1.94,4.61-2.92,7.75-2.92s5.58.97,7.57,2.92c1.98,1.94,2.98,4.36,2.98,7.26s-.99,5.31-2.98,7.26c-1.98,1.94-4.51,2.91-7.57,2.91s-5.73-.97-7.75-2.91Z"></path>
                                    <path class="cls-2" d="m454.42,65.42c2.77-5.37,6.53-9.51,11.29-12.4,4.76-2.89,10.07-4.34,15.94-4.34,5.13,0,9.61,1.04,13.46,3.1,3.85,2.07,6.92,4.67,9.24,7.82v-9.8h17.49v68.72h-17.49v-10.05c-2.23,3.23-5.31,5.89-9.24,8-3.93,2.11-8.46,3.16-13.58,3.16-5.79,0-11.06-1.49-15.82-4.47-4.76-2.98-8.52-7.17-11.29-12.59-2.77-5.41-4.16-11.64-4.16-18.67s1.38-13.11,4.16-18.48Zm47.45,7.88c-1.65-3.02-3.89-5.33-6.7-6.95-2.81-1.61-5.83-2.42-9.06-2.42s-6.2.79-8.93,2.36c-2.73,1.57-4.94,3.87-6.64,6.88-1.69,3.02-2.54,6.6-2.54,10.73s.85,7.75,2.54,10.85c1.69,3.1,3.93,5.48,6.7,7.13,2.77,1.66,5.72,2.48,8.87,2.48s6.24-.81,9.06-2.42c2.81-1.61,5.04-3.93,6.7-6.95,1.65-3.02,2.48-6.64,2.48-10.85s-.83-7.83-2.48-10.85Z"></path>
                                    <path class="cls-4" d="m591.93,102.01h-34.48l-5.71,16.5h-18.24l31.14-86.71h20.22l31.13,86.71h-18.36l-5.71-16.5Zm-4.71-13.89l-12.53-36.22-12.53,36.22h25.06Z"></path>
                                    <path class="cls-4" d="m645.02,31.93v86.58h-17.37V31.93h17.37Z"></path>
                                  </g>
                                </g>
                              </g>
                            </svg>
                  <div editabletype="text" class="f-14 f-md-16 w400 mt20 lh160 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
                  <div class=" mt20 mt-md40 white-clr"> <script type="text/javascript" src="https://warriorplus.com/o2/disclaimer/zh5gzv" defer=""></script><div class="wplus_spdisclaimer f-17 w300 mt20 lh140 white-clr text-center"><strong>Disclaimer:</strong> <em>WarriorPlus is used to help manage the sale of products on this site. While WarriorPlus helps facilitate the sale, all payments are made directly to the product vendor and NOT WarriorPlus. Thus, all product questions, support inquiries and/or refund requests must be sent to the vendor. WarriorPlus's role should not be construed as an endorsement, approval or review of these products or any claim, statement or opinion used in the marketing of these products.</em></div></div>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-14 f-md-16 w400 lh160 white-clr text-xs-center">Copyright © Ninja Ai</div>
                  <ul class="footer-ul w400 f-14 f-md-16 white-clr text-center text-md-right">
                     <li><a href="https://support.bizomart.com/hc/en-us" class="white-clr  t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://getninjaai.com/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://getninjaai.com/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://getninjaai.com/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://getninjaai.com/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://getninjaai.com/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://getninjaai.com/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!--Footer Section End -->
   <!-- New Timer  Start-->
<!-- timer --->
<!-- Exit Popup -->	
<style>
   </style>
<script type="text/javascript" src="assets/js/ouibounce.min.js"></script>
<div id="ouibounce-modal" style="z-index:9999999; display:none;">
   <div class="underlay"></div>
   <div class="modal-wrapper" style="display:block;">
      <div class="modal-bg">
		<button type="button" class="close" onclick="document.getElementById('ouibounce-modal').style.display = 'none';">
			X 
		</button>
		<div class="model-header">
      <div class="d-flex justify-content-center align-items-center gap-3">
            <img src="assets/images/hold.png" alt="" class="img-fluid d-block m131">
            <div>
               <div class="f-md-56 f-24 w700 lh130 white-clr">
                  WAIT! HOLD ON!!
               </div>
               <div class="f-md-28 f-18 w700 lh130 black-clr mt10 mt-md0" style="background-color:yellow; padding:5px 10px; display:inline-block">
                  Don't Leave Empty Handed
               </div>
            </div>
         </div>
		</div>
         <div class="col-12 for-padding">
			<div class="copun-line f-md-45 f-16 w500 lh130 text-center black-clr mt10">
         You've Qualified For An <span class="w700 red-clr"><br> INSTANT $200 DISCOUNT</span>  
			</div>
         <div class="copun-line f-md-30 f-16 w700 lh130 text-center black-clr mt10" style="background-color:yellow; padding:5px 10px; display:inline-block">
         Regular Price <strike>$267</strike>,
         Now Only $67	
			</div>
			 <div class="mt-md20 mt10 text-center">
				<a href="https://warriorplus.com/o2/buy/zh5gzv/stszb4/qr34pz" class="cta-link-btn1">Grab SPECIAL Discount Now!
				<br>
				<span class="f-12 black-clr w600 text-uppercase">Act Now! Limited to First 100 Buyers Only.</span> </a>
			 </div>
			<!-- <div class="mt-md20 mt10 text-center f-md-26 f-16 w900 lh150 text-center red-clr text-uppercase">
				Coupon Code EXPIRES IN:
			</div>
			<div class="timer-new">
            <div class="days" class="timer-label">
               <span id="days"></span> <span class="text">Days</span>
            </div> 
            <div class="hours" class="timer-label">
               <span id="hours"></span> <span  class="text">Hours</span>
            </div>
            <div class="days" class="timer-label">
               <span id="mins" ></span> <span class="text">Min</span>
            </div>
            <div class="secs" class="timer-label">
               <span id="secs" ></span> <span class="text">Sec</span>
            </div>
         </div> -->
      </div>
   </div>
</div>
<script type="text/javascript">
      var timeInSecs;
      var ticker;
      function startTimer(secs) {
      timeInSecs = parseInt(secs);
	  tick();
      //ticker = setInterval("tick()", 1000);
      }
      function tick() {
      var secs = timeInSecs;
      if (secs > 0) {
      timeInSecs--;
      } else {
      clearInterval(ticker);
      //startTimer(20 * 60); // 4 minutes in seconds
      }
      var days = Math.floor(secs / 86400);
      secs %= 86400;
      var hours = Math.floor(secs / 3600);
      secs %= 3600;
      var mins = Math.floor(secs / 60);
      secs %= 60;
      var pretty = ((days < 10) ? "0" : "") + days + ":" + ((hours < 10) ? "0" : "") + hours + ":" + ((mins < 10) ? "0" : "") + mins + ":" + ((secs < 10) ? "0" : "") + secs;
      document.getElementById("days").innerHTML = days
      document.getElementById("hours").innerHTML = hours
      document.getElementById("mins").innerHTML = mins
      document.getElementById("secs").innerHTML = secs;
      }
      
	  //startTimer(60 * 60); 
      // 4 minutes in seconds
	  
	  function getCookie(cname) {
            var name = cname + "=";
            var decodedCookie = decodeURIComponent(document.cookie);
            var ca = decodedCookie.split(';');
            for(var i = 0; i <ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        }


      var cnt = 60*60;

      function counter(){
       
         if(getCookie("cnt") > 0){
            cnt = getCookie("cnt");
         }

         cnt -= 1;
         document.cookie = "cnt="+ cnt;

         startTimer(getCookie("cnt"));
         //jQuery("#counter").val(getCookie("cnt"));

         if(cnt>0){
            setTimeout(counter,1000);
         }
      
      }
      
      counter();
	  
	  
      </script>-
      <script type="text/javascript">
         var _ouibounce = ouibounce(document.getElementById('ouibounce-modal'),{
         aggressive: true, //Making this true makes ouibounce not to obey "once per visitor" rule
         });
      </script>
      <!-- Exit Popup Ends-->
            <!--- timer end-->
   <!-- Exit Popup and Timer End -->
</body>
</html>
<script> 
function myFunction() {
  var checkBox = document.getElementById("check");
  var buybutton = document.getElementById("buyButton");
  if (checkBox.checked == true){
 
	    buybutton.getAttribute("href");
	    buybutton.setAttribute("href","https://warriorplus.com/o2/buy/zh5gzv/stszb4/qr34pz");
	} else {
		 buybutton.getAttribute("href");
		 buybutton.setAttribute("href","https://warriorplus.com/o2/buy/zh5gzv/stszb4/qr34pz");
	  }
}
</script>
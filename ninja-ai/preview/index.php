<!Doctype html>
<html>
   <head>
      <title>NinjaAi Special</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=9">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
      <meta name="title" content="3 Steps AI-App Create Online-Learning Platform For Us, Prefill Them With AI Courses">
      <meta name="description" content="Its World first A.I App Creates Online-Learning Platform Prefilled With Smoking Hot AI Courses & E-Books…And Then Market Them To over 3.5 Millions Customers & earn $872.36 daily">
      <meta name="keywords" content="NinjaAi">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta property="og:image" content="https://www.getninjaai.com/special/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Pranshu Gupta">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="3 Steps AI-App Create Online-Learning Platform For Us, Prefill Them With AI Courses">
      <meta property="og:description" content="Its World first A.I App Creates Online-Learning Platform Prefilled With Smoking Hot AI Courses & E-Books…And Then Market Them To over 3.5 Millions Customers & earn $872.36 daily">
      <meta property="og:image" content="https://www.getninjaai.com/special/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="3 Steps AI-App Create Online-Learning Platform For Us, Prefill Them With AI Courses">
      <meta property="twitter:description" content="Its World first A.I App Creates Online-Learning Platform Prefilled With Smoking Hot AI Courses & E-Books…And Then Market Them To over 3.5 Millions Customers & earn $872.36 daily">
      <meta property="twitter:image" content="https://www.getninjaai.com/special/thumbnail.png">
      <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&family=Poppins:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
      <!-- Start Editor required -->
      <link rel="stylesheet" href="https://cdn.oppyotest.com/launches/sellero/common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style-feature.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style-pawan.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style-bottom.css" type="text/css">
      <link rel="stylesheet" href="assets/css/timer.css" type="text/css">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <script src="https://cdn.oppyotest.com/launches/sellero/common_assets/js/bootstrap.min.js"></script>
      <script src="https://cdn.oppyotest.com/launches/sellero/common_assets/js/jquery.min.js"></script>
	  
	  <script>
(function(w, i, d, g, e, t, s) {
if(window.businessDomain != undefined){
console.log("Your page have duplicate embed code. Please check it.");
return false;
}
businessDomain = 'aicademy';
allowedDomain = 'getninjaai.com';
if(!window.location.hostname.includes(allowedDomain)){
console.log("Your page have not authorized. Please check it.");
return false;
}
console.log("Your script is ready...");
w[d] = w[d] || [];
t = i.createElement(g);
t.async = 1;
t.src = e;
s = i.getElementsByTagName(g)[0];
s.parentNode.insertBefore(t, s);
})(window, document, '_gscq', 'script', 'https://cdn.staticdcp.com/apps/engage/smart_engage/js/config-loader.js');
</script>

   </head>
   <body>

      <div class="timer-header-top fixed-top">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-12 col-md-3">
                  <div class="tht-left f-14 f-md-14 text-md-start text-center white-clr">
                     Use Coupon <span class="orange-clr">"NinjaAi"</span>  for <br class="d-block d-md-none">Extra <span class="orange-clr"> $3 Discount </span>
                  </div>
               </div>
               <div class="col-8 col-md-6">
                  <div class="countdown counter-white text-center">
                     <div class="timer-label text-center"><span class="f-24 f-md-28 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-16 w500 smmltd">Days</span> </div>
                     <div class="timer-label text-center"><span class="f-24 f-md-28 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-16 w500 smmltd">Hours</span> </div>
                     <div class="timer-label text-center timer-mrgn"><span class="f-24 f-md-28 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-16 w500 smmltd">Mins</span> </div>
                     <div class="timer-label text-center "><span class="f-24 f-md-28 timerbg oswald">00</span><br><span class="f-14 f-md-16 w500 smmltd">Sec</span> </div>
                  </div>
               </div>
               <div class="col-4 col-md-3 text-center text-md-right">
                  <div class="affiliate-link-btn"><a href="#buynow" class="t-decoration-none">Buy Now</a></div>
               </div>
            </div>
         </div>
      </div>
      <!--1. Header Section Start -->
      <div class="header-section">
         <div class="container">
            <div class="row">
               <div class="col-md-3 text-md-start text-center">
               <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 645.02 151.26" style="max-height:70px">
                                    <defs>
                                      <style>
                                        .cls-1 {
                                          fill: url(#linear-gradient-2);
                                        }
                                  
                                        .cls-2 {
                                          fill: #fff;
                                        }
                                  
                                        .cls-3 {
                                          fill-rule: evenodd;
                                        }
                                  
                                        .cls-3, .cls-4 {
                                          fill: #ff813b;
                                        }
                                  
                                        .cls-5 {
                                          fill: url(#linear-gradient-3);
                                        }
                                  
                                        .cls-6 {
                                          fill: url(#linear-gradient);
                                        }
                                  
                                        .cls-7 {
                                          fill: url(#linear-gradient-4);
                                        }
                                      </style>
                                      <linearGradient id="linear-gradient" x1="47.81" y1="75" x2="185.48" y2="75" gradientUnits="userSpaceOnUse">
                                        <stop offset="0" stop-color="#b956ff"></stop>
                                        <stop offset="1" stop-color="#6e33ff"></stop>
                                      </linearGradient>
                                      <linearGradient id="linear-gradient-2" x1="0" y1="75" x2="85.18" y2="75" gradientUnits="userSpaceOnUse">
                                        <stop offset="0" stop-color="#b956ff"></stop>
                                        <stop offset="1" stop-color="#a356fa"></stop>
                                      </linearGradient>
                                      <linearGradient id="linear-gradient-3" x1="55.61" y1="62.54" x2="62.86" y2="62.54" gradientUnits="userSpaceOnUse">
                                        <stop offset="0" stop-color="#b956ff"></stop>
                                        <stop offset="1" stop-color="#ac59fa"></stop>
                                      </linearGradient>
                                      <linearGradient id="linear-gradient-4" x1="54.62" y1="79" x2="61.87" y2="79" gradientTransform="translate(19.64 -11.24) rotate(13.24)" xlink:href="#linear-gradient-3"></linearGradient>
                                    </defs>
                                    <g id="Layer_1-2" data-name="Layer 1">
                                      <g>
                                        <g>
                                          <g>
                                            <path class="cls-6" d="m181.49,58.53h-10.72c-1.4-5.13-3.43-9.99-6.01-14.51l7.58-7.58c1.56-1.56,1.56-4.09,0-5.65l-17.65-17.65c-1.56-1.56-4.09-1.56-5.65,0l-7.58,7.58c-4.51-2.58-9.38-4.61-14.51-6.01V4c0-2.21-1.79-4-4-4h-24.96c-2.21,0-4,1.79-4,4v10.72c-.16.04-.31.09-.47.14-.27.08-.53.15-.8.23-.27.08-.55.16-.82.24-.55.17-1.09.35-1.63.53-.11.04-.22.08-.33.12-.48.17-.96.34-1.44.52-.12.04-.24.09-.36.14-.52.2-1.04.4-1.56.62-.04.02-.08.03-.12.05-2.41,1-4.74,2.15-6.99,3.43l-7.57-7.57c-1.56-1.56-4.09-1.56-5.65,0l-17.65,17.65c-.36.36-.63.76-.82,1.2h61.99v.02c.23,0,.45-.02.68-.02,23.76,0,43.01,19.26,43.01,43.01s-19.26,43.01-43.01,43.01c-.5,0-1-.02-1.5-.04v.04h-61.17c.19.43.46.84.82,1.2l17.65,17.65c1.56,1.56,4.09,1.56,5.65,0l7.57-7.57c2.25,1.28,4.58,2.43,6.99,3.43.04.02.08.03.12.05.51.21,1.03.42,1.55.62.12.05.24.09.36.14.47.18.95.35,1.43.52.11.04.23.08.34.12.54.18,1.08.36,1.63.53.27.08.55.16.83.25.26.08.53.16.79.23.16.04.31.09.47.14v10.72c0,2.21,1.79,4,4,4h24.96c2.21,0,4-1.79,4-4v-10.72c5.13-1.4,9.99-3.43,14.51-6.01l7.58,7.58c1.56,1.56,4.09,1.56,5.65,0l17.65-17.65c1.56-1.56,1.56-4.09,0-5.65l-7.58-7.58c2.58-4.51,4.61-9.38,6.01-14.51h10.72c2.21,0,4-1.79,4-4v-24.96c0-2.21-1.79-4-4-4Z"></path>
                                            <path class="cls-1" d="m76.64,101.54c-5.74-7.31-9.18-16.52-9.18-26.54s3.44-19.23,9.18-26.54c2.45-3.13,5.33-5.9,8.53-8.24h-48.39c-1.31-1.89-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.13h17.07c-1.51,3.21-2.76,6.58-3.71,10.07h3.1c1.31-1.89,3.49-3.13,5.97-3.13,4.01,0,7.25,3.25,7.25,7.25s-3.22,7.23-7.21,7.25h-.04c-2.48,0-4.66-1.24-5.97-3.13H13.22c-.44-.64-.98-1.19-1.59-1.65-1.21-.92-2.73-1.48-4.38-1.48-4,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.14h38.82c1.31-1.89,3.49-3.13,5.97-3.13.43,0,.84.05,1.25.12,3.41.59,6,3.56,6,7.14,0,2.78-1.56,5.19-3.86,6.41-1.01.53-2.16.84-3.39.84-2.48,0-4.66-1.24-5.97-3.13h-17.85c-1.31-1.9-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.13h15.96c.95,3.48,2.2,6.85,3.71,10.07h-5.63c-1.31-1.89-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25,0,1.53.48,2.95,1.28,4.12,1.31,1.9,3.49,3.14,5.97,3.14s4.66-1.24,5.97-3.14h36.95c-3.21-2.34-6.08-5.11-8.53-8.24ZM30.64,47.97c-2,0-3.62-1.62-3.62-3.62s1.62-3.62,3.62-3.62,3.63,1.62,3.63,3.62-1.63,3.62-3.63,3.62Zm-23.39,26.47c-2,0-3.62-1.63-3.62-3.62s1.63-3.63,3.62-3.63,3.63,1.63,3.63,3.63-1.63,3.62-3.63,3.62Zm20.97,16.45c-2,0-3.63-1.63-3.63-3.63s1.63-3.62,3.63-3.62,3.62,1.63,3.62,3.62-1.63,3.63-3.62,3.63Zm14.08,18.39c-2,0-3.62-1.62-3.62-3.62s1.62-3.62,3.62-3.62,3.62,1.62,3.62,3.62-1.62,3.62-3.62,3.62Z"></path>
                                            <circle class="cls-5" cx="59.23" cy="62.54" r="3.63"></circle>
                                            <circle class="cls-7" cx="58.24" cy="79" r="3.63" transform="translate(-16.54 15.44) rotate(-13.24)"></circle>
                                          </g>
                                          <g>
                                            <g>
                                              <path class="cls-3" d="m104.63,78.16c-9.44,6-14.52,2.86-15.25-9.4,4.93,3.38,10.02,6.52,15.25,9.4Z"></path>
                                              <path class="cls-3" d="m131.01,68.76c-.72,12.26-5.81,15.4-15.25,9.4,5.24-2.88,10.32-6.02,15.25-9.4Z"></path>
                                            </g>
                                            <path class="cls-4" d="m110.16,40.13c-.89,0-1.78.03-2.66.1-.08,0-.16,0-.24.02-9.59.79-18.07,5.45-23.89,12.42-.25.3-.49.59-.73.89-.14.19-.29.38-.43.57-4.02,5.37-6.52,11.93-6.88,19.07,0,.01,0,.03,0,.04-.01.22-.02.45-.03.68-.01.3-.02.6-.02.9v.18c0,.28,0,.55,0,.83.44,18.64,15.5,33.66,34.15,34.04.24,0,.48,0,.72,0,.3,0,.6,0,.9,0,18.85-.48,33.98-15.9,33.98-34.87s-15.61-34.88-34.88-34.88Zm.19,39.35c-14.41,14.37-29.15,1.23-29.15-9.45s13.05,0,29.15,0,29.15-10.67,29.15,0-14.74,23.81-29.15,9.45Z"></path>
                                          </g>
                                        </g>
                                        <g>
                                          <path class="cls-2" d="m286.65,118.51h-17.37l-39.32-59.42v59.42h-17.37V31.8h17.37l39.32,59.54V31.8h17.37v86.71Z"></path>
                                          <path class="cls-2" d="m304.95,38.69c-2.03-1.94-3.04-4.36-3.04-7.26s1.01-5.31,3.04-7.26c2.03-1.94,4.57-2.92,7.63-2.92s5.6.97,7.63,2.92c2.02,1.94,3.04,4.36,3.04,7.26s-1.02,5.31-3.04,7.26c-2.03,1.94-4.57,2.91-7.63,2.91s-5.6-.97-7.63-2.91Zm16.19,11.1v68.72h-17.37V49.79h17.37Z"></path>
                                          <path class="cls-2" d="m396.18,56.55c5.04,5.17,7.57,12.38,7.57,21.65v40.31h-17.36v-37.96c0-5.46-1.37-9.65-4.1-12.59-2.73-2.94-6.45-4.4-11.16-4.4s-8.58,1.47-11.35,4.4c-2.77,2.94-4.15,7.13-4.15,12.59v37.96h-17.37V49.79h17.37v8.56c2.31-2.98,5.27-5.31,8.87-7.01,3.6-1.69,7.55-2.54,11.85-2.54,8.19,0,14.8,2.59,19.85,7.75Z"></path>
                                          <path class="cls-2" d="m437.61,129.8c0,7.61-1.88,13.09-5.64,16.44-3.77,3.35-9.16,5.02-16.19,5.02h-7.69v-14.76h4.96c2.64,0,4.51-.52,5.58-1.55,1.07-1.03,1.61-2.71,1.61-5.02V49.79h17.37v80.01Zm-16.31-91.11c-2.03-1.94-3.04-4.36-3.04-7.26s1.01-5.31,3.04-7.26c2.02-1.94,4.61-2.92,7.75-2.92s5.58.97,7.57,2.92c1.98,1.94,2.98,4.36,2.98,7.26s-.99,5.31-2.98,7.26c-1.98,1.94-4.51,2.91-7.57,2.91s-5.73-.97-7.75-2.91Z"></path>
                                          <path class="cls-2" d="m454.42,65.42c2.77-5.37,6.53-9.51,11.29-12.4,4.76-2.89,10.07-4.34,15.94-4.34,5.13,0,9.61,1.04,13.46,3.1,3.85,2.07,6.92,4.67,9.24,7.82v-9.8h17.49v68.72h-17.49v-10.05c-2.23,3.23-5.31,5.89-9.24,8-3.93,2.11-8.46,3.16-13.58,3.16-5.79,0-11.06-1.49-15.82-4.47-4.76-2.98-8.52-7.17-11.29-12.59-2.77-5.41-4.16-11.64-4.16-18.67s1.38-13.11,4.16-18.48Zm47.45,7.88c-1.65-3.02-3.89-5.33-6.7-6.95-2.81-1.61-5.83-2.42-9.06-2.42s-6.2.79-8.93,2.36c-2.73,1.57-4.94,3.87-6.64,6.88-1.69,3.02-2.54,6.6-2.54,10.73s.85,7.75,2.54,10.85c1.69,3.1,3.93,5.48,6.7,7.13,2.77,1.66,5.72,2.48,8.87,2.48s6.24-.81,9.06-2.42c2.81-1.61,5.04-3.93,6.7-6.95,1.65-3.02,2.48-6.64,2.48-10.85s-.83-7.83-2.48-10.85Z"></path>
                                          <path class="cls-4" d="m591.93,102.01h-34.48l-5.71,16.5h-18.24l31.14-86.71h20.22l31.13,86.71h-18.36l-5.71-16.5Zm-4.71-13.89l-12.53-36.22-12.53,36.22h25.06Z"></path>
                                          <path class="cls-4" d="m645.02,31.93v86.58h-17.37V31.93h17.37Z"></path>
                                        </g>
                                      </g>
                                    </g>
                                  </svg>
               </div>
               <div class="col-md-9  mt-md0 mt15">
                  <ul class="leader-ul f-20 w400 white-clr text-md-end text-center">
                     <li>
                        <a href="#features" class="white-clr t-decoration-none">Features</a><span class="pl15">|</span>
                     </li>
                     <li>
                        <a href="#productdemo" class="white-clr t-decoration-none">Demo</a>
                     </li>
                     <li class="affiliate-link-btn">
                        <a href="#buynow">Buy Now</a>
                     </li>
                  </ul>
               </div>
               <div class="col-12 text-center lh150 mt20 mt-md70">
                  <div class="pre-heading f-18 f-md-22 w600 lh140 white-clr">
                     Legally... Swipe Guru's Profitable Funnels with Our “ChatGPT4 Secret Agent”
                  </div>
               </div>
               <div class="col-12 mt-md30 mt20 f-md-42 f-28 w400 text-center white-clr lh140">
                  World's First Google-Killer Ai App Builds Us <br class="d-none d-md-block">
                  <span class="white-clr w700">DFY Profitable Affiliate Funnel Sites, Prefilled with Content, Reviews & Lead Magnet in Just 60 Seconds.</span>
               </div>
               <div class="col-12 mt-md15 mt15 text-center">
                  <div class="f-22 f-md-28 w700 lh140 black-clr yellow-brush">
                     Then Promote Them to Our Secret Buyers Network of 496 million Users...
                  </div>
               </div>
               <div class="col-12 mt20 text-center">
                  <div class="f-22 f-md-28 w400 lh140 white-clr">
                     & Making Us <span class="w600 underline">$955.45 Commissions Daily</span>  with Literally Zero Work                 
                  </div>
               </div>
               <div class="col-12 mt20 text-center">
                  <div class="f-20 f-md-22 w400 lh140 red-bg white-clr">
                     No Designing, No Copywriting, & No Hosting…. Even A 100% Beginner Can Do It.
                  </div>
               </div>
            </div>

            <div class="row mt20 mt-md40 gx-md-5 align-items-center">
               <div class="col-md-7 col-12">
                  <div class="col-12">
                     <div class="f-18 f-md-19 w500 lh140 white-clr shape-head">
                     Watch How We Swiped A Profitable Funnel With Just One Click And Made $30,345.34 Per Month On Complete Autopilot…
                     </div>
                     
                     <div class="video-box mt20 mt-md30">
                        <!-- <img src="assets/images/video-thumb.webp" alt="Video" class="mx-auto d-block img-fluid"> -->
                        <div style="padding-bottom: 56.25%;position: relative;"><iframe src="https://ninjaai.dotcompal.com/video/embed/q5429rqlji" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div>
                     </div>
                  </div>
                  <div class="col-12 mt20 mt-md50">
                     <div class="border-dashed">
                        <div class="f-18 f-md-20 w500 lh140 text-center grey-clr">
                        First 99 Action Takers Get Instant Access To Our Top 5 Profitable DFY AI-Funnels For
                        <span class="orange-clr"><u>FREE</u><br class="d-none d-md-block"> (Average User Saw 475% Increase In Profit worth $1,997)</span>
                        </div>
                     </div>
                     <div class="f-22 f-md-28 w700 lh150 text-center mt20 mt-md30 white-clr">
                     Get NinjaAI For Low One Time Fee… <br class="d-none d-md-block"> Just Pay Once Instead Of <del class="red-clr"> Monthly</del>
                     </div>
                     <div class="row">
                        <div class="col-md-12 mx-auto col-12 mt20 mt-md20 text-center affiliate-link-btn1">
                           <a href="#buynow">>>> Get Instant Access To NinjaAi <<<</a>
                        </div>
                     </div>
                     <div class="col-12  mt20 mt-md30">
                        <img src="assets/images/compaitable-gurantee1.webp" alt="Compaitable Gurantee" class="mx-auto d-block img-fluid">
                     </div>

                     <div class="mt20 mt-md30">
                        <div class="countdown counter-white text-center">
                           <div class="timer-label text-center"><span class="f-24 f-md-42 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
                           <div class="timer-label text-center"><span class="f-24 f-md-42 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                           <div class="timer-label text-center timer-mrgn"><span class="f-24 f-md-42 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                           <div class="timer-label text-center "><span class="f-24 f-md-42 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-5 col-12 mt20 mt-md0">
                  <div class="key-features-bg">
                     <ul class="list-head pl0 m0 f-16 f-md-18 lh160 w400 white-clr">
                        <li><span class="w600">Eliminate All The Guesswork</span> And Jump Straight to Results</li>
                        <li><span class="w600">Let Ai Find</span> Hundreds Of The Guru's Profitable Funnels</li>
                        <li><span class="w600">Let Ai Create</span> Proven Money Making Funnels On Autopilot</li>
                        <li class="w600">All The Copywriting And Designing Is Done For You</li>
                        <li>Generate 100% <span class="w600">SEO & Mobile Optimized</span> Funnels</li>
                        <li><span class="w600">Instant High Converting Traffic For 100% Free…</span> No Need To Run Ads</li>
                        <li>98% Of Beta Testers Made At Least One Sale <span class="w600 underline">Within 24 Hours</span> Of Using Ninja AI.</li>
                        <li><span class="w600">A True Ai App</span> That Leverages ChatGPT4</li>
                        <li>Connect Your Favourite Autoresponders And <span class="w600">Build Your List FAST.</span></li>
                        <li>Seamless Integration With Any Payment Processor You Want</li>
                        <li class="w600">ZERO Upfront Cost</li>
                        <li>No Complicated Setup - Get Up And Running In 2 Minutes</li>
                        <li class="w600">So Easy, Anyone Can Do it.</li>
                        <li>30 Days Money-Back Guarantee</li>
                        <li><span class="w600">Free Commercial License Included -</span> That's Priceless</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--1. Header Section End -->

   <!-- Limited Section Start    -->
   <div class="limited-time-sec">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-20 f-md-24 w700 text-center white-clr align-items-center flex-column flex-md-row justify-content-center">
                  <img src="assets/images/caution-icon.webp" class="img-fluid mr-md5 mb5 mb-md0" alt="Caution Icon">
                  Your $2,475.34 Bonuses Package Expires When Timer Hits ZERO 
                  <img src="assets/images/caution-icon.webp" class="img-fluid ml-md5 mb5 mb-md0" alt="Caution Icon">
               </div>
            </div>
         </div>
      </div>
   </div> 
   <!-- Limited Section End -->

   <div class="step-section">
      <div class="container ">
         <div class="row ">
            <div class="col-12 f-28 f-md-50 w700 lh140 black-clr text-center">
               All It Takes Is 3 Fail-Proof Steps<br class="d-none d-md-block">
            </div>
            <div class="col-12 f-24 f-md-28 w600 lh140 white-clr text-center mt20">
               <div class="post-heading">
               Start Dominating ANY Niche With DFY AI Funnels…
               </div>
            </div>
            <div class="col-12 mt30 mt-md90">
               <div class="row align-items-center ">
                  <div class="col-12 col-md-6">
                     <div class="step-shapes f-20 f-md-22 w600 white-clr">
                        Step 1
                     </div>
                     <div class="f-34 f-md-65 w700 lh140 mt15 caveat">
                     Access
                     </div>
                     <div class="f-20 f-md-24 w400 lh140 mt5 mt-md10 text-capitalize">
                     click on any of the links below to get instant access to NinjaAI for a low 1-time fee
                     </div>
                  </div>
                  
                  <div class="col-12 col-md-6 mt20 mt-md0 ">
                  <!-- <img src="assets/images/video-thumb.webp" alt="Video" class="mx-auto d-block img-fluid"> -->
                     <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 1px solid #a94eff;">
                        <source src="assets/images/step-one.mp4" type="video/mp4">
                     </video>
                  </div>
               </div>
            </div>
            <div class="col-12">
               <img src="assets/images/left-arrow.webp" class="img-fluid d-none d-md-block mx-auto " alt="Left Arrow">
            </div>
            <div class="col-12 mt30 mt-md30">
               <div class="row align-items-center ">
                  <div class="col-12 col-md-6 order-md-2">
                     <div class="step-shapes f-20 f-md-22 w600 white-clr">
                        Step 2
                     </div>
                     <div class="f-34 f-md-65 w700 lh140 mt15 caveat">
                     Choose
                     </div>
                     <div class="f-20 f-md-24 w400 lh140 mt5 mt-md10 text-capitalize">
                     Choose your niche, and let the AI take care of everything for you…
                     </div>
                  </div>
                  <div class="col-md-6 col-md-pull-6 mt20 mt-md0 order-md-1 ">
                  <!-- <img src="assets/images/video-thumb.webp" alt="Video" class="mx-auto d-block img-fluid"> -->
                     <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 1px solid #a94eff;">
                        <source src="assets/images/step-two.mp4" type="video/mp4">
                     </video>
                  </div>
               </div>
            </div>
            <div class="col-12">
               <img src="assets/images/right-arrow.webp" class="img-fluid d-none d-md-block mx-auto" alt="Right Arrow">
            </div>
            <div class="col-12 mt30 mt-md30">
               <div class="row align-items-center ">
                  <div class="col-12 col-md-6">
                     <div class="step-shapes f-20 f-md-22 w600 white-clr">
                        Step 3
                     </div>
                     <div class="f-34 f-md-65 w700 lh140 mt15 caveat">
                     Profit
                     </div>
                     <div class="f-20 f-md-24 w400 lh140 mt5 mt-md10 text-capitalize">
                     That's it, there is no technical setup, there is no learning curve…
                     <br>
                     And we daily make money like this.
                     </div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 ">
                  <!-- <img src="assets/images/video-thumb.webp" alt="Video" class="mx-auto d-block img-fluid"> -->
                  <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 1px solid #a94eff;">
                        <source src="assets/images/step-three.mp4" type="video/mp4">
                     </video>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>


   <div class="profitwall-sec1">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="col-12 f-28 f-md-38 w400 lh140 white-clr text-center">
               Ninja AI Leveraged ChatGPT4 To Eliminate ANY Guesswork…
               </div>
               <div class="col-12 f-22 f-md-50 w700 lh140 white-clr mt10 text-center">
                  And Allowed Us To (Legally)
               </div>
               <div class="col-12 f-22 f-md-50 w400 lh140 white-clr mt10 text-center">
                  <div class="orange-bg">
                     Steal <u>PROFITABLE</u> Funnels
                  </div>
               </div>
            </div>
         </div>
         <div class="row row-cols-1 row-cols-sm-2 row-cols-md-2 gap30 mt20 mt-md10">
            <div class="col">
               <div class="text-center">
                  <img src="assets/images/profitwall-img1.png" alt="No Coding" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-34 lh140 w600 text-center white-clr orange-brush mt20 mx-auto">
                  Just Select A Niche
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                  You don’t need to write a single line of code with WebGenie… You won’t even see any codes…  
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                  AI does it all for you… 
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                  And there is NO need for any coding or programming of any sort… 
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="assets/images/profitwall-img2.png" alt="No Server Configurations" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-34 lh140 w600 text-center white-clr orange-brush mt20 mx-auto">
                  AI Finds All The Gurus In That Niche
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                  On the background. Our AI model will scour your niche…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                  And find the best gurus in that niche and what exactly are they doing…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                  To make money
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="assets/images/profitwall-img3.png" alt="No Content Writing" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-34 lh140 w600 text-center white-clr orange-brush mt20 mx-auto">
                  AI “LEGALLY” Steal Their Profitable Funnels
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                  Our AI model will swipe their funnels…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                  Improve on it…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                  And give it to you in a template that you can swipe with just 1 click.
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                  You don’t have to write or design anything.
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="assets/images/profitwall-img4.png" alt="No Optimizing" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-34 lh140 w600 text-center white-clr orange-brush mt20 mx-auto">
                  Let Ai Create Money Making Funnels On Autopilot
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                  Now we have a profitable funnel…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                  Our AI model will start sending massive amount of targeted clicks to it now…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                  On complete autopilot. Without paying a penny in ads.
                  </div>
               </div>
            </div>
            <!-- <div class="col">
               <div class="text-center">
                  <img src="assets/images/profitwall-img6.png" alt="No Optimizing" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-34 lh140 w600 text-center white-clr orange-brush mt20 mx-auto">
                  We Sit Back And Profit
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                  That’s it.
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                  The entire process takes less than 20 seconds.
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                  The entire process takes less than 20 seconds. Then, we just watch sales hammers our account
                  </div>
               </div>
            </div> -->
            <div class="col">
               <div class="text-center">
                  <img src="assets/images/profitwall-img5.png" alt="No Optimizing" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-34 lh140 w600 text-center white-clr orange-brush mt20 mx-auto">
                  Fast Result
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                  No need to wait months to see results…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                  Because NinjaAi is not relying on just SEO & No guess work. The results are BLAZING FAST…
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="assets/images/profitwall-img7.png" alt="No Coding" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-34 lh140 w600 text-center white-clr red-brush mt20 mx-auto">
                  No Designing
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                  NinjaAi eliminates the need to hire a designer…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                  Or even use an expensive app to design…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                  It comes with a built-in features that will allow you to turn any keyword… Into a stunning design…
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="assets/images/profitwall-img8.png" alt="No Server Configurations" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-34 lh140 w600 text-center white-clr red-brush mt20 mx-auto">
                  No Costly Hosting
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                  Server management is costly & also not an easy task.
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                  Luckily, you don't have to worry about it even a bit…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                  Cause NinjaAi will handle all of it for you in the background.
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="assets/images/profitwall-img9.png" alt="No Content Writing" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-34 lh140 w600 text-center white-clr red-brush mt20 mx-auto">
                  No Paid Ads
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                  Everyone says that you need to run ads to get traffic…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                  But that's not the case with NinjaAi… Because it will do that for you…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                  It will drive thousands of buyers clicks to you for 100% free…
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   
   <!-- CTA Section Start -->
   <div class="cta-section-white">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-38 w700 lh140 text-center white-clr">
                  Get <span class="step-shapes"> NinjaAi </span> For Low One Time Fee… <br class="d-none d-md-block"> 
               </div>
               <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                  <span class="w700">Just Pay Once</span> Instead Of <s class="red-clr">Monthly</s>
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md50">
            <div class="col-md-7">
               <a href="#buynow" class="cta-link-btn">>>> Get Instant Access To NinjaAi <<<</a>
               <img src="assets/images/compaitable-gurantee1.webp" alt="Compaitable Gurantee" class="mx-auto d-block img-fluid mt20 mt-md30">
            </div>
            <div class="col-md-5 mt20 mt-md0">
               <div class="countdown-container">
                  <div class="f-20 f-md-24 w400 lh140 text-center white-clr mb10">
                     <span class="w700 red-clr">Hurry up!</span>  Price Increases In
                  </div>
                  <div class="countdown counter-white text-center">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 smmltd">Days</span> 
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-45 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> 
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- CTA Section End -->

   <!--Limited Section Start -->
   <div class="limited-time-sec">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-20 f-md-24 w700 text-center white-clr align-items-center flex-column flex-md-row justify-content-center">
                  <img src="assets/images/caution-icon.webp" class="img-fluid mr-md5 mb5 mb-md0" alt="Caution Icon">
                  Your $2,475.34 Bonuses Package Expires When Timer Hits ZERO 
                  <img src="assets/images/caution-icon.webp" class="img-fluid ml-md5 mb5 mb-md0" alt="Caution Icon">
               </div>
            </div>
         </div>
      </div>
   </div>
   <!--Limited Section End -->
   
   <!-- Testimonial Section Start -->
   <div class="testimonial-sec">
      <div class="container ">
         <div class="row ">
            <div class="col-12 f-28 f-md-45 w700 lh140 black-clr text-center">
               NinjaAI <span class="orange-clr"> Works For Everyeone… </span>
            </div>
            <div class="col-12 f-22 f-md-28 w400 lh140 black-clr text-center">
            Here Is What Our Current Members Have To Say About It…
            </div>
            <div class="col-12 f-22 f-md-28 w400 lh140 black-clr text-center">
            (And You Can Join Them In 10 Seconds)
            </div>
         </div>
         <div class="row row-cols-md-2 row-cols-1 gx4 mt-md100 mt0">
            <div class="col mt20 mt-md50">
               <div class="single-testimonial">
                  <div class="st-img">
                     <img src="assets/images/craig-mcintosh.webp" class="img-fluid d-block mx-auto " alt="Craig McIntosh">
                  </div>
                  <div class="mt20 md-md50 f-24 f-md-28 w600 lh140 black-clr">Craig McIntosh </div>
                  <div class="mt10 f-20 f-md-22 w400 lh140 black-clr">
                     Digital Marketer and Internet Marketer.
                  </div>
                  <div class="stars mt10">
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                  </div>
                  <img src="assets/images/quote.webp" alt="Quotes" class="mx-auto d-block img-fluid mt20 mt-md50 ">
                  <p class="mt20 f-18 f-md-20 lh140 w400 text-center">
                  <span class="w600">"NinjaAi has transformed my affiliate marketing game with its AI-powered features.</span>
                  Creating stunning funnels in minutes and generating original, SEO-friendly content, including headlines, sales letters, and email swipes, has never been easier.<br><br>
                  NINJAAI's built-in targeted traffic, and the viral social traffic feature boosts my reach. <span class="w600">Plus, with no monthly charges, NinjaAi proves to be a cost-effective and user-friendly solution for my affiliate marketing success."</span>
                  </p>
               </div>
            </div>
            <div class="col mt20 mt-md50">
               <div class="single-testimonial">
                  <div class="st-img">
                     <img src="assets/images/edward-reid.webp" class="img-fluid d-block mx-auto " alt="Edward Reid">
                  </div>
                  <div class="mt20 md-md50 f-24 f-md-28 w600 lh140 black-clr"> James Milo  </div>
                  <div class="mt10 f-20 f-md-22 w400 lh140 black-clr">
                  Founder,  AI Marketing and Sales Software
                  </div>
                  <div class="stars mt10">
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                  </div>
                  <img src="assets/images/quote.webp" alt="Quotes" class="mx-auto d-block img-fluid mt20 mt-md50 ">
                  <p class="mt20 f-18 f-md-20 lh140 w400 text-center">
                  NinjaAi is a low-cost, simple and easy way to conquer the annoying technical aspects of your very own affiliate marketing empire.<span class="w600"> No copywriting. No designing. No coding.</span>
                  <br><br>
                  Just rapidly customize the many pre-built templates & proven funnels and I let the cutting-edge AI do all the heavy lifting for me so <span class="w600">I can enjoy new passive revenue streams!</span>

                  </p>
               </div>
            </div>
            <div class="col mt-md120 mx-auto">
               <div class="single-testimonial">
                  <div class="st-img">
                     <img src="assets/images/jaeden-downs.webp" class="img-fluid d-block mx-auto " alt="Jaeden Downs">
                  </div>
                  <div class="mt20 md-md50 f-24 f-md-28 w600 lh140 black-clr"> Jaeden Downs  </div>
                  <div class="stars mt10">
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                  </div>
                  <img src="assets/images/quote.webp" alt="Quotes" class="mx-auto d-block img-fluid mt20 mt-md50 ">
                  <p class="mt20 f-18 f-md-20 lh140 w400 text-center">
                     Ninja AI is an absolute game-changer! As an affiliate marketer, I've never seen anything so powerful and efficient. 
                     <br><br>
                     In just 60 seconds, it effortlessly crafts DFY affiliate funnel sites, complete with top-notch content, reviews, and lead magnets.
                     <span class="w600"> Ninja AI has truly slayed the competition and is undoubtedly the Google Killer of our time!</span>
                  </p>
               </div>
            </div>
            <div class="col mt-md120 mx-auto">
               <div class="single-testimonial">
                  <div class="st-img">
                     <img src="assets/images/stuart-johnson.webp" class="img-fluid d-block mx-auto " alt="Drew Warren">
                  </div>
                  <div class="mt20 md-md50 f-24 f-md-28 w600 lh140 black-clr"> Drew Warren  </div>
                  <div class="stars mt10">
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                  </div>
                  <img src="assets/images/quote.webp" alt="Quotes" class="mx-auto d-block img-fluid mt20 mt-md50 ">
                  <p class="mt20 f-18 f-md-20 lh140 w400 text-center">
                  Ninja AI has revolutionized my marketing game! With just one click, it crafts incredibly profitable DFY AI funnels that consistently drive conversions and boost our bottom line. 
                           <br><br>
                   Its unmatched AI capabilities have saved my time and resources while delivering exceptional results.<span class="w600"> I've witnessed an unprecedented surge in my affiliate commissions on just 1st day after my new Ai Funnel launch using NinjaAi.</span> Thanks to Ninja AI's unparalleled performance.
                  </p>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Testimonial Section End -->

   <section class="ruined-sec">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-md-6">
               <div class="f-28 f-md-50 w700 white-clr lh140 red-box">
                  ClickFunnel RUINED Our Lives…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20 mt-md22">
                  I know you might hate me for that…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  But that’s my opinion and I’m sticking to it…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  As a matter of fact…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  It didn’t just ruin my life…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  It ruined yours too… 
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  You just don’t realize it yet…
               </div>
            </div>
            <div class="col-md-6 mt20 mt-md0">
               <img src="assets/images/ruined-women.webp" alt="Ruined Women" class="mx-auto d-block img-fluid">
            </div>
         </div>
      </div>
   </section>

   <section class="brainwashed-sec">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-md-6 order-md-2">
               <div class="f-28 f-md-50 w700 black-clr lh140">
                  They Brainwashed Our Minds…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20 mt-md22">
                  “You are just one funnel away from becoming a millionaire..” 
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  How many times did you see this bullsh!t or something similar? 
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  They conditioned us that it’s all about the funnel…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  It doesn’t matter what you are doing… All you need is to create a new funnel… 
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  But what if it didn’t make any money?
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  Doesn’t matter, just create a new one… 
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  As long as you keep paying us that monthly payment… 
               </div>
            </div>
            <div class="col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/brainwashed-man.webp" alt="BrainWashed Men" class="mx-auto d-block img-fluid">
            </div>
         </div>
      </div>
   </section>

   <section class="grey-sec">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-md-6">
               <div class="f-28 f-md-42 w400 lh140 black-clr">
               But It Gets Worse… <br class="d-none d-md-block">
               <span class="w700">They</span> <span class="w700 red-clr underline">ALWAYS Blame YOU.</span>
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  It’s always your fault…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  They will always tell you
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  “Oh you did the funnel wrong, you need better copywriting or design”
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  It doesn’t matter, they will make shit up…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  But they will NEVER admit that maybe this thing doesn’t work…
               </div>
            </div>
            <div class="col-md-6 mt20 mt-md0">
               <img src="assets/images/blame-women.webp" alt="Blame Women" class="mx-auto d-block img-fluid">
            </div>
         </div>
      </div>
   </section>

   <section class="white-sec">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-md-6 order-md-2">
               <div class="f-28 f-md-38 w700 lh140 black-clr">
                  They Ever Dare To… <br class="d-none d-md-block">
                  Sell You More Useless Stuff…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  Can you believe it?
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  They have the audacity to sell you MORE stuff…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  And try to convenience you that you didn’t make money because you don’t have this funnel upgrade…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  There are horror stories all over the internet…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  Of people paying thousands of dollars for the “best funnel”
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  And as you might have guessed…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  It’s more useless shit that they sell you to extract more money from you…
               </div>
            </div>
            <div class="col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/dare-man.webp" alt="Dare Men" class="mx-auto d-block img-fluid">
            </div>
         </div>
      </div>
   </section>


   <section class="grey-sec">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-md-6">
               <div class="f-28 f-md-45 w400 lh140 black-clr">
                  Meanwhile… <br>
                  <span class="w700">They’re Making BANK With Their Funnels…</span>
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  Believe it or not…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  All gurus (including Russell Brunson) make most of their money from FUNNELS…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  Don’t get confused, just hear me out…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  They do make millions off of funnels, but not the funnels they show you…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  Not the funnel they show you how to create in clickfunnels…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  Nop, their business is very different than that…
               </div>
            </div>
            <div class="col-md-6 mt20 mt-md0">
               <img src="assets/images/bank-men.webp" alt="Bank Men" class="mx-auto d-block img-fluid">
            </div>
         </div>
      </div>
   </section>

   <section class="white-sec">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-md-6 order-md-2">
               <div class="f-28 f-md-45 w400 lh140 black-clr">
                  Does That Means… <br class="d-none d-md-block">
                 <span class="w700 red-clr">We Can’t Use Funnels??</span> 
               </div>
               <div class="f-20 f-md-22 w600 lh140 black-clr mt20">
                  The short answer is… wrong.
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  Let me explain… 
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  Funnels -if done right- is one of the most profitable things you can do online…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  The issue is not the funnels…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  The issue is that those gurus hide what’s really working…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  They keep their good stuff hidden…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  And try to sell you outdated funnels only…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  It’s disgusting I know…
               </div>
            </div>
            <div class="col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/confused-women.webp" alt="Confused Women" class="mx-auto d-block img-fluid">
            </div>
         </div>
      </div>
   </section>

   <section class="blue-sec">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <div class="f-28 f-md-50 w700 white-clr lh140">
                  Here Is The Issue…
               </div>
               <div class="f-28 f-md-50 w700 white-clr lh140 mt20 mt-md30 red-brush">
                  You Build Your Dreams On Guessword…
               </div>
            </div>
         </div>
         <div class="row mt20 mt-md80 align-items-center">
            <div class="col-md-6">
               <div class="f-20 f-md-22 w400 lh140 white-clr">
                  I’m sorry, but it’s true…
               </div>
               <div class="f-20 f-md-22 w400 lh140 white-clr mt20">
                  You go in, create a funnel and hope it works… 
               </div>
               <div class="f-20 f-md-22 w400 lh140 white-clr mt20">
                  Spend all your time and money on it. 
               </div>
               <div class="f-20 f-md-22 w400 lh140 white-clr mt20">
                  HOPING that it will work…
               </div>
               <div class="f-20 f-md-22 w400 lh140 white-clr mt20">
                  Some of us even don’t dare to try it…
               </div>
               <div class="f-20 f-md-22 w400 lh140 white-clr mt20">
                  We don’t know if it will work or not… 
               </div>
               <div class="f-20 f-md-22 w400 lh140 white-clr mt20">
                  So why should we risk spending money or time creating one…
               </div>
               <div class="f-20 f-md-22 w400 lh140 white-clr mt20">
                  However, there is some good news…
               </div>
            </div>
            <div class="col-md-6 mt20 mt-md0">
               <img src="assets/images/male-coworking-space.webp" alt="Male Coworking" class="mx-auto d-block img-fluid">
            </div>
         </div>
      </div>
   </section>


   <section class="white-sec">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-md-6 order-md-2">
               <div class="f-28 f-md-38 w700 lh140 black-clr">
                  <span class="red-clr">What If…?</span> <br class="d-none d-md-block">
                  There Is A Way To Eliminate ANY Guess Work… 
               </div>
               <div class="f-20 f-md-22 w600 lh140 black-clr mt20">
                  Imagine for a second…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  That there is a way that would allow you to GUARANTEE a funnel is profitable…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  Even before building it…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  Imagine that there is a way to remove any doubt… 
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  And be 100% sure, that this funnel will make you money…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  Before spending a minute working on it…
               </div>
               <div class="f-20 f-md-22 w600 lh140 white-clr mt20 mt-md30 thumsup-text">
                  That would be great, huh?
               </div>
               
            </div>
            <div class="col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/bulb.webp" alt="Bulb" class="mx-auto d-block img-fluid">
            </div>
         </div>
      </div>
   </section>


   <section class="grey-sec">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-md-6">
               <div class="f-28 f-md-38 w400 lh140 black-clr">
                  Well, You Don’t Have To Imagine <span class="orange-clr w700">Because AI Did Exactly That…</span> 
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  Unless you been living under a rock…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  You know that AI is taking the world by a storm…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  They are changing the way we do everything…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  And funnels is one of them…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  In fact I’d argue that funnels got the MOST impact from AI…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  It leveled the playing field…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  And gave us the power back to create profitable funnels…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  Just like the gurus, without any of the hard work, or guesswork
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  Let me explain how…
               </div>
            </div>
            <div class="col-md-6 mt20 mt-md0">
               <img src="assets/images/ai-hand.webp" alt="AI Hand" class="mx-auto d-block img-fluid">
            </div>
         </div>
      </div>
   </section>

   <section class="white-sec">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-md-6 order-md-2">
               <div class="f-28 f-md-38 w700 lh140 black-clr">
                  Our AI Model <span class="red-clr">EXPOSED</span>  All Top Guru’s Secret… 
               </div>
               <div class="f-20 f-md-22 w600 lh140 black-clr mt20">
                  What we did is… GENIUS…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  We created the world’s first AI model that spied on all of the top gurus…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  And got exactly what they are doing online, what funnel they have, and how much money they are making…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  Can you believe it?
               </div>
            </div>
            <div class="col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/exposed.webp" alt="Exposed" class="mx-auto d-block img-fluid">
            </div>
         </div>
      </div>
   </section>

   <section class="grey-sec">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-md-6">
               <div class="f-28 f-md-50 w400 lh140 black-clr">
                  Ninja AI Allowed Us To <br class="d-none d-md-block"> <span class="w700">Dominate ANY Niche… </span> 
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  This is never done before…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  All we need to do is just choose a niche…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  And Ninja AI will do everything else…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  It will find all the top gurus in that niche…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  And find exactly how they are making the money…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  Then, it will give you all of that on a silver platter…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  All you have to do is just click one button and swipe everything…
               </div>
               <div class="f-20 f-md-22 w600 lh140 black-clr mt20">
                  Isn’t that awesome?
               </div>
            </div>
            <div class="col-md-6 mt20 mt-md0">
               <img src="assets/images/dominate-ninjaai.webp" alt="Dominate NinjaAI" class="mx-auto d-block img-fluid">
            </div>
         </div>
      </div>
   </section>

   <section class="white-sec">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-md-6 order-md-2">
               <div class="f-28 f-md-45 w700 lh140 black-clr">
                  The World’s First AI “Secret Agent” 
               </div>
               <div class="f-20 f-md-22 w600 lh140 black-clr mt20">
                  It’s like having 007 working for you privately…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  How powerful would that be?
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  That’s exactly what NinjaAI would do for you…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  It will spy and steal ONLY the profitable campaigns for you… 
               </div>
            </div>
            <div class="col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/secret-agent.webp" alt="Secret Agent" class="mx-auto d-block img-fluid">
            </div>
         </div>
      </div>
   </section>

   <section class="blue-sec1">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-28 f-md-50 w700 lh140 white-clr">
                  Finally…
               </div>
               <div class="f-28 f-md-50 w700 lh140 white-clr purple-brush mt20">
                  ELIMINATE All The Guesswork…
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md50">
            <div class="col-md-6">
               <div class="f-20 f-md-22 w400 lh140 white-clr">
               With Ninja AI…
               </div>
               <div class="f-20 f-md-22 w600 lh140 white-clr mt20">
               You don’t have to guess or hope for a working funnel…
               </div>
               <div class="f-20 f-md-22 w400 lh140 white-clr mt20">
               Our AI model with Ninja AI will give you ONLY the funnels that we know for sure works…
               </div>
               <div class="f-20 f-md-22 w400 lh140 white-clr mt20">
               No more split-testing, stressing, and hoping for success…
               </div>
            </div>
            <div class="col-md-6 mt20 mt-md0">
               <img src="assets/images/guesswork-women.webp" alt="GuessWork Women" class="mx-auto d-block img-fluid">
            </div>
         </div>
      </div>
   </section>

   <section class="white-sec">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-md-6 order-md-2">
               <div class="f-28 f-md-50 w700 lh140 black-clr">
                  Ninja AI Blow Any Other Funnel Creator Out Of Water… 
               </div>
               <div class="f-20 f-md-22 w600 lh140 black-clr mt20">
                  Here is the big difference…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  Not one does Ninja AI create stunning funnels… 
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  It’s the only app on the market that will give you WORKING funnels… 
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  Funnels that are proven to work and convert NOW. not a year ago… 
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20 funeel-money">
                  Funnels that constantly makes us money like this: 
               </div>
            </div>
            <div class="col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/out-of-water.webp" alt="Men Pulling" class="mx-auto d-block img-fluid">
            </div>
           
         </div>
         <div class="col-12 col-md-12 mt20 mt-md80">
               <img src="assets/images/funnel-money.webp" alt="Funnel Money" class="mx-auto d-block img-fluid">
            </div>
      </div>
   </section>

   <section class="blue-sec1">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-24 f-md-45 w700 lh140 white-clr">
                  Just check yourself and see how the funnels looks in Ninja AI
               </div>
               <div class="mt20 mt-md80">
                  <img src="assets/images/templates.webp" alt="Templates" class="mx-auto d-block img-fluid">
               </div>
            </div>
         </div>
      </div>
   </section>

   <section class="grey-sec">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-md-6">
               <div class="f-28 f-md-50 w700 lh140 black-clr">
                  No Writing & No Designing 
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  We already did all the heavylifting for you…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  All the copy was done by our AI model. And designed it for you too…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  You just attach your affiliate link…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  And you are done.
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  No more work…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  Maybe just keep refreshing your account…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  To see how much you made…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  Us personall…
               </div>
               <div class="f-20 f-md-22 w600 lh140 red-clr mt20">
                  We make about $300-$500 per each funnel…
               </div>
               <div class="f-20 f-md-22 w600 lh140 black-clr mt20 daily-text">
                  DAILY
               </div>
            </div>
            <div class="col-md-6 mt20 mt-md0">
               <img src="assets/images/no-writing-no-designing.webp" alt="No Writing & No Designing" class="mx-auto d-block img-fluid">
            </div>
            <div class="col-md-12 mt20 mt-md50">
               <img src="assets/images/no-writing.webp" alt="No Writing" class="mx-auto d-block img-fluid">
            </div>
         </div>
      </div>
   </section>

   <section class="white-sec">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-md-6 order-md-2">
               <div class="f-28 f-md-45 w400 lh140 black-clr">
                  Unlike Clickfunnels, <br  class="d-none d-md-block">
                  Ninja AI Comes With <span class="w700"> ZERO Monthly Fees…</span> 
               </div>
               <div class="f-20 f-md-22 w600 lh140 black-clr mt20">
                  Yea, you read that right…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  All you need to do to access the most advanced piece of software on the market…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  Is to pay one, small fee…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  That’s it, now you own it forever…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  And can use it as much as you need
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  With zero restrictions…
               </div>
            </div>
            <div class="col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/zero-monthly.webp" alt="Zero Monthly Fee" class="mx-auto d-block img-fluid">
            </div>
         </div>
      </div>
   </section>

   <section class="blue-sec2">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-28 f-md-50 w700 lh140 white-clr">
                  Watch Ninja AI In Action… And See What It Can Do For You…
               </div>
               <img src="assets/images/floating-arrow.webp" alt="floating Arrow" class="mx-auto d-block img-fluid mt20 mt-md50">
            </div>
            <div class="col-12 col-md-9 mx-auto mt20 mt-md50">
               <!-- <img src="assets/images/watch-ninjaai.webp" alt="Watch Ninjaai" class="mx-auto d-block img-fluid "> -->
               <div class="video-box">
                  <div style="padding-bottom: 56.25%;position: relative;"><iframe src="https://ninjaai.dotcompal.com/video/embed/n4mhnex94f" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div>
               </div>
            </div>
         </div>
         <div class="row mt20 mt-md80">
            <div class="col-12">
               <div class="f-28 f-md-38 w700 lh140 text-center white-clr">
                  Get <span class="step-shapes"> NinjaAi </span> For Low One Time Fee… <br class="d-none d-md-block"> 
               </div>
               <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                  <span class="w700">Just Pay Once</span> Instead Of <s class="red-clr">Monthly</s>
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md50">
            <div class="col-md-7">
               <a href="#buynow" class="cta-link-btn">&gt;&gt;&gt; Get Instant Access To NinjaAi &lt;&lt;&lt;</a>
               <img src="assets/images/compaitable-gurantee1.webp" alt="Compaitable Gurantee" class="mx-auto d-block img-fluid mt20 mt-md30">
            </div>
            <div class="col-md-5 mt20 mt-md0">
               <div class="countdown-container">
                  <div class="f-20 f-md-24 w400 lh140 text-center white-clr mb10">
                     <span class="w700 red-clr">Hurry up!</span>  Price Increases In
                  </div>
                  <div class="countdown counter-white text-center"><div class="timer-label text-center"><span class="f-24 f-md-32 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-14 w500 smmltd">Days</span> </div><div class="timer-label text-center"><span class="f-24 f-md-32 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-14 w500 smmltd">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-24 f-md-32 timerbg oswald">32&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-14 w500 smmltd">Mins</span> </div><div class="timer-label text-center "><span class="f-24 f-md-32 timerbg oswald">41</span><br><span class="f-14 f-md-14 w500 smmltd">Sec</span> </div></div>
               </div>
            </div>
         </div>
      </div>
   </section>

   <section class="experienced-sec">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-28 f-md-50 w700 lh140 black-clr">
                  Ninja AI Is For Everyone! Regardless Of <br class="d-none d-md-block">
                  Your Experience…
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md80">
            <div class="col-md-6 order-md-2">
               <div class="f-20 f-md-22 lh140 black-clr w400">
                  It doesn’t matter if you have made money online before or not…
               </div>
               <div class="f-20 f-md-22 lh140 black-clr w400 mt20">
                  It doesn’t matter if you know how a funnel is made or not…
               </div>
               <div class="f-20 f-md-22 lh140 black-clr w400 mt20">
                  As long as you can follow fool-proof instructions…
               </div>
               <div class="f-20 f-md-22 lh140 black-clr w400 mt20">
                  And can click 3 clicks with your mouse…
               </div>
               <div class="f-20 f-md-22 lh140 black-clr w400 mt20">
                  You’re all set…
               </div>
            </div>
            <div class="col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/experience-women.webp" alt="Experienced Women" class="mx-auto d-block img-fluid">
            </div>
         </div>
      </div>
   </section>

   <section class="grey-sec">
      <div class="container hi-their-box">
         <div class="row">
            <div class="col-12 text-center">
               <div class="hi-their-shadow">
                  <div class="f-28 f-md-45 lh140 black-clr caveat w700 hi-their-head">
                     Hey There,
                  </div>
               </div>
            </div>
         </div>
         <div class="row mt20 mt-md80">
            <div class="col-md-3 text-center">
               <div class="figure mr15 mr-md0">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/pranshu-gupta.webp" alt="Pranshu Gupta" class="mx-auto d-block img-fluid  img-shadow">
                  <div class="figure-caption mt10">
                     <div class="f-24 f-md-28 w700 lh140 black-clr caveat text-center">
                        Pranshu Gupta
                     </div>
                  </div>
               </div>
               <div class="figure mt20 mt-md30">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/bizomart-team.webp" alt="Bizomart" class="mx-auto d-block img-fluid  img-shadow">
                  <div class="figure-caption mt10">
                     <div class="f-24 f-md-28 w700 lh140 black-clr caveat text-center">
                        Bizomart
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-md-9 mt20 mt-md0">
               <div class="f-28 f-md-38 lh140 black-clr w400">
                  It’s <span class="w600">Pranshu</span> along with <span class="w600">Bizomart.</span> 
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr mt20 mt-md30">
                  I’m a full time internet marketer…
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr mt20">
                  Over the years, I tried dozens (it could be hundreds even…) of methods to make money online…
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr mt20">
                  But the only thing that gave me the life I always wanted was…
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr mt20">
                  Creating websites…
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr mt20">
                  It’s the easiest, and fastest way to build wealth…
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr mt20">
                  Not just make some pocket change…&nbsp;
               </div>
               <div class="f-20 f-md-22 lh140 w600 black-clr mt20">
                  Thanks to that…
               </div>
            </div>
         </div>

         <div class="row mt20 mt-md50">
            <div class="col-12">
               <div class="blue-border"></div>
            </div>
         </div>

         <div class="row mt20 mt-md50">
            <div class="col-12 text-center">
               <div class="f-22 f-md-28 lh140 black-clr w600">
                  I can spend my time as I please
               </div>
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/red-underline.webp" alt="Quotes" class="mx-auto d-block img-fluid  mt10">
            </div>
         </div>

         <div class="row mt20 mt-md50">
            <div class="col-md-3">
               <figure>
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/family.webp" alt="Family" class="mx-auto d-block img-fluid ">
                  <figure-caption>
                     <div class="f-20 f-md-22 w600 lh140 black-clr mt10 text-center">
                        Spend it with my family
                     </div>
                  </figure-caption>
               </figure>
            </div>
            <div class="col-md-3">
               <figure>
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/travel.webp" alt="Travel" class="mx-auto d-block img-fluid ">
                  <figure-caption>
                     <div class="f-20 f-md-22 w600 lh140 black-clr mt10 text-center">
                        Travel
                     </div>
                  </figure-caption>
               </figure>
            </div>
            <div class="col-md-3">
               <figure>
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/sunset-beach.webp" alt="Enjoy a sunset on the beach" class="mx-auto d-block img-fluid ">
                  <figure-caption>
                     <div class="f-20 f-md-22 w600 lh140 black-clr mt10 text-center">
                        Enjoy a sunset on the beach
                     </div>
                  </figure-caption>
               </figure>
            </div>
            <div class="col-md-3">
               <figure>
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/video-games.webp" alt="Or even play video games on my PS5" class="mx-auto d-block img-fluid ">
                  <figure-caption>
                     <div class="f-20 f-md-22 w600 lh140 black-clr mt10 text-center">
                        Or even play video games on my PS5
                     </div>
                  </figure-caption>
               </figure>
            </div>
         </div>
         <div class="row mt20 mt-md80">
            <div class="col-12 text-center">
               <div class="f-20 f-md-24 w600 lh140 black-clr">
                  And the best part is… No matter what I do with my day… I still make a lot of money…
               </div>
               <div class="f-20 f-md-24 w400 lh140 black-clr mt20">
                  And I’m not telling you this to brag… Not at all… I’m telling you this to demonstrate why you should even listen to me…
               </div>
            </div>
         </div>
      </div>
   </section>

   <section class="white-sec">
      <div class="container">
         <div class="row">
         <div class="col-12 text-center">
               <div class="f-28 f-md-50 w700 lh140 black-clr">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/blub.webp" class="img-fluid mr-md5 mb5 mb-md0" alt="blub">
                  I have One Goal This Year…
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/blub.webp" class="img-fluid ml-md5 mb5 mb-md0" alt="blub">
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md80">
            <div class="col-md-6">
               <div class="f-20 f-md-22 w400 lh140 black-clr ">
                  You see…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  Making money online is good any everything…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  But what’s 10x better… Is helping someone else make it…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  This is why I decided that in 2023 I will help 100 new people…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  Quit their job, and have the financial freedom they always wanted…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  So far, it’s been going GREAT…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  I’ve managed to help countless people…
               </div>
            </div>
            <div class="col-md-6 mt20 mt-md0">
               <img src="assets/images/goal-man.webp" alt="Goal Man" class="mx-auto d-block img-fluid">
            </div>
         </div>
      </div>
   </section>

   <section class="success-sec">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-28 f-md-50 w700 lh140 black-clr">
                  And I’m Here To Help You My Next <br class="d-none d-md-block">
                  Success Story…
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md80">
            <div class="col-md-6 order-md-2">
               <div class="f-20 f-md-22 w400 lh140 black-clr ">
                  Listen, if you’re reading this page right now…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  It means one thing, you’re dedicated to making money online…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  And I have some good news for you…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  On this page, I will give you everything you need…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  To get started and join me and thousands of other profitable members…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  Who changed their lives forever…
               </div>
            </div>
            <div class="col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/success-man.webp" alt="Success Man" class="mx-auto d-block img-fluid">
            </div>
         </div>
      </div>
   </section>

   <section class="cool-sec">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-28 f-md-50 w700 black-clr lh140">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/cool.webp" class="img-fluid mr-md5 mb5 mb-md0" alt="Cool">
                  Let Me Show You Something Cool…
                  <img src="assets/images/orange-underline.webp" alt="Quotes" class="mx-auto d-block img-fluid ">
               </div>
            </div>
            <!-- <div class="col-12 col-md-8 mx-auto mt20 mt-md80">
               <img src="assets/images/cool-img.webp" alt="Cool Image" class="mx-auto d-block img-fluid">
            </div> -->
            <div class="col-12 col-md-10 mx-auto mt20 mt-md80">
               <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 1px solid #15c6fc;">
                  <source src="https://cdn.oppyotest.com/launches/webgenie/preview/images/cool.mp4" type="video/mp4">
               </video>
            </div>
            <div class="col-12 mt20 mt-md50 text-center">
               <div class="f-22 f-md-28 w600 lh140 black-clr">
                  You see this endless stream of payments?
               </div>
               <div class="f-22 f-md-28 w400 lh140 black-clr mt10">
                  We got all of those doing only one thing…
               </div>
               <div class="smile-text f-22 f-md-28 w600 lh140 white-clr mt20 mt-md50">Using NinjaAI</div>
               <div class="f-22 f-md-28 w400 lh140 black-clr mt20 mt-md49">
                  It allowed me to swipe all the top gurus funnels…
               </div>
               <div class="f-22 f-md-28 w400 lh140 black-clr mt20">
                  And guarantee a profitable funnel, before even creating it…
               </div>
               <div class="f-22 f-md-28 w400 lh140 black-clr mt20">
                  All i do is just turn it on… And enjoy my life…
               </div>
               <div class="f-22 f-md-28 w400 lh140 black-clr mt20">
                  This month alone…
               </div>
               <div class="f-28 f-md-38 w400 lh140 black-clr mt20 mt-md30">
                  We made <span class="red-clr w600 affliliate-underline"> $29,232.33 on one of our affiliate accounts…</span>
               </div>
               <div class="d-flex justify-content-md-end justify-content-center">
                  <img src="assets/images/out-water-arrow.webp" alt="Demo Proof" class="d-block img-fluid  mt20 ">
               </div>
               <img src="assets/images/demo-proof.webp" alt="Demo Proof" class="mx-auto d-block img-fluid  ">
               <div class="f-24 f-md-28 w400 lh140 black-clr mt20 mt-md30">
               And I’m gonna show you exactly how I do it…
               </div>
            </div>
         </div>
      </div>
   </section>

   <div class="proudly-section" id="product">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="prdly-pres f-28 f-md-38 w700 lh140 text-center white-clr text-uppercase">
                     Introducing
                  </div>
               </div>
               <div class="col-12 mt-md50 mt20 text-center">
                  <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 645.02 151.26" style="max-height:120px">
                                    <defs>
                                      <style>
                                        .cls-1 {
                                          fill: url(#linear-gradient-2);
                                        }
                                  
                                        .cls-2 {
                                          fill: #fff;
                                        }
                                  
                                        .cls-3 {
                                          fill-rule: evenodd;
                                        }
                                  
                                        .cls-3, .cls-4 {
                                          fill: #ff813b;
                                        }
                                  
                                        .cls-5 {
                                          fill: url(#linear-gradient-3);
                                        }
                                  
                                        .cls-6 {
                                          fill: url(#linear-gradient);
                                        }
                                  
                                        .cls-7 {
                                          fill: url(#linear-gradient-4);
                                        }
                                      </style>
                                      <linearGradient id="linear-gradient" x1="47.81" y1="75" x2="185.48" y2="75" gradientUnits="userSpaceOnUse">
                                        <stop offset="0" stop-color="#b956ff"></stop>
                                        <stop offset="1" stop-color="#6e33ff"></stop>
                                      </linearGradient>
                                      <linearGradient id="linear-gradient-2" x1="0" y1="75" x2="85.18" y2="75" gradientUnits="userSpaceOnUse">
                                        <stop offset="0" stop-color="#b956ff"></stop>
                                        <stop offset="1" stop-color="#a356fa"></stop>
                                      </linearGradient>
                                      <linearGradient id="linear-gradient-3" x1="55.61" y1="62.54" x2="62.86" y2="62.54" gradientUnits="userSpaceOnUse">
                                        <stop offset="0" stop-color="#b956ff"></stop>
                                        <stop offset="1" stop-color="#ac59fa"></stop>
                                      </linearGradient>
                                      <linearGradient id="linear-gradient-4" x1="54.62" y1="79" x2="61.87" y2="79" gradientTransform="translate(19.64 -11.24) rotate(13.24)" xlink:href="#linear-gradient-3"></linearGradient>
                                    </defs>
                                    <g id="Layer_1-2" data-name="Layer 1">
                                      <g>
                                        <g>
                                          <g>
                                            <path class="cls-6" d="m181.49,58.53h-10.72c-1.4-5.13-3.43-9.99-6.01-14.51l7.58-7.58c1.56-1.56,1.56-4.09,0-5.65l-17.65-17.65c-1.56-1.56-4.09-1.56-5.65,0l-7.58,7.58c-4.51-2.58-9.38-4.61-14.51-6.01V4c0-2.21-1.79-4-4-4h-24.96c-2.21,0-4,1.79-4,4v10.72c-.16.04-.31.09-.47.14-.27.08-.53.15-.8.23-.27.08-.55.16-.82.24-.55.17-1.09.35-1.63.53-.11.04-.22.08-.33.12-.48.17-.96.34-1.44.52-.12.04-.24.09-.36.14-.52.2-1.04.4-1.56.62-.04.02-.08.03-.12.05-2.41,1-4.74,2.15-6.99,3.43l-7.57-7.57c-1.56-1.56-4.09-1.56-5.65,0l-17.65,17.65c-.36.36-.63.76-.82,1.2h61.99v.02c.23,0,.45-.02.68-.02,23.76,0,43.01,19.26,43.01,43.01s-19.26,43.01-43.01,43.01c-.5,0-1-.02-1.5-.04v.04h-61.17c.19.43.46.84.82,1.2l17.65,17.65c1.56,1.56,4.09,1.56,5.65,0l7.57-7.57c2.25,1.28,4.58,2.43,6.99,3.43.04.02.08.03.12.05.51.21,1.03.42,1.55.62.12.05.24.09.36.14.47.18.95.35,1.43.52.11.04.23.08.34.12.54.18,1.08.36,1.63.53.27.08.55.16.83.25.26.08.53.16.79.23.16.04.31.09.47.14v10.72c0,2.21,1.79,4,4,4h24.96c2.21,0,4-1.79,4-4v-10.72c5.13-1.4,9.99-3.43,14.51-6.01l7.58,7.58c1.56,1.56,4.09,1.56,5.65,0l17.65-17.65c1.56-1.56,1.56-4.09,0-5.65l-7.58-7.58c2.58-4.51,4.61-9.38,6.01-14.51h10.72c2.21,0,4-1.79,4-4v-24.96c0-2.21-1.79-4-4-4Z"></path>
                                            <path class="cls-1" d="m76.64,101.54c-5.74-7.31-9.18-16.52-9.18-26.54s3.44-19.23,9.18-26.54c2.45-3.13,5.33-5.9,8.53-8.24h-48.39c-1.31-1.89-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.13h17.07c-1.51,3.21-2.76,6.58-3.71,10.07h3.1c1.31-1.89,3.49-3.13,5.97-3.13,4.01,0,7.25,3.25,7.25,7.25s-3.22,7.23-7.21,7.25h-.04c-2.48,0-4.66-1.24-5.97-3.13H13.22c-.44-.64-.98-1.19-1.59-1.65-1.21-.92-2.73-1.48-4.38-1.48-4,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.14h38.82c1.31-1.89,3.49-3.13,5.97-3.13.43,0,.84.05,1.25.12,3.41.59,6,3.56,6,7.14,0,2.78-1.56,5.19-3.86,6.41-1.01.53-2.16.84-3.39.84-2.48,0-4.66-1.24-5.97-3.13h-17.85c-1.31-1.9-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.13h15.96c.95,3.48,2.2,6.85,3.71,10.07h-5.63c-1.31-1.89-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25,0,1.53.48,2.95,1.28,4.12,1.31,1.9,3.49,3.14,5.97,3.14s4.66-1.24,5.97-3.14h36.95c-3.21-2.34-6.08-5.11-8.53-8.24ZM30.64,47.97c-2,0-3.62-1.62-3.62-3.62s1.62-3.62,3.62-3.62,3.63,1.62,3.63,3.62-1.63,3.62-3.63,3.62Zm-23.39,26.47c-2,0-3.62-1.63-3.62-3.62s1.63-3.63,3.62-3.63,3.63,1.63,3.63,3.63-1.63,3.62-3.63,3.62Zm20.97,16.45c-2,0-3.63-1.63-3.63-3.63s1.63-3.62,3.63-3.62,3.62,1.63,3.62,3.62-1.63,3.63-3.62,3.63Zm14.08,18.39c-2,0-3.62-1.62-3.62-3.62s1.62-3.62,3.62-3.62,3.62,1.62,3.62,3.62-1.62,3.62-3.62,3.62Z"></path>
                                            <circle class="cls-5" cx="59.23" cy="62.54" r="3.63"></circle>
                                            <circle class="cls-7" cx="58.24" cy="79" r="3.63" transform="translate(-16.54 15.44) rotate(-13.24)"></circle>
                                          </g>
                                          <g>
                                            <g>
                                              <path class="cls-3" d="m104.63,78.16c-9.44,6-14.52,2.86-15.25-9.4,4.93,3.38,10.02,6.52,15.25,9.4Z"></path>
                                              <path class="cls-3" d="m131.01,68.76c-.72,12.26-5.81,15.4-15.25,9.4,5.24-2.88,10.32-6.02,15.25-9.4Z"></path>
                                            </g>
                                            <path class="cls-4" d="m110.16,40.13c-.89,0-1.78.03-2.66.1-.08,0-.16,0-.24.02-9.59.79-18.07,5.45-23.89,12.42-.25.3-.49.59-.73.89-.14.19-.29.38-.43.57-4.02,5.37-6.52,11.93-6.88,19.07,0,.01,0,.03,0,.04-.01.22-.02.45-.03.68-.01.3-.02.6-.02.9v.18c0,.28,0,.55,0,.83.44,18.64,15.5,33.66,34.15,34.04.24,0,.48,0,.72,0,.3,0,.6,0,.9,0,18.85-.48,33.98-15.9,33.98-34.87s-15.61-34.88-34.88-34.88Zm.19,39.35c-14.41,14.37-29.15,1.23-29.15-9.45s13.05,0,29.15,0,29.15-10.67,29.15,0-14.74,23.81-29.15,9.45Z"></path>
                                          </g>
                                        </g>
                                        <g>
                                          <path class="cls-2" d="m286.65,118.51h-17.37l-39.32-59.42v59.42h-17.37V31.8h17.37l39.32,59.54V31.8h17.37v86.71Z"></path>
                                          <path class="cls-2" d="m304.95,38.69c-2.03-1.94-3.04-4.36-3.04-7.26s1.01-5.31,3.04-7.26c2.03-1.94,4.57-2.92,7.63-2.92s5.6.97,7.63,2.92c2.02,1.94,3.04,4.36,3.04,7.26s-1.02,5.31-3.04,7.26c-2.03,1.94-4.57,2.91-7.63,2.91s-5.6-.97-7.63-2.91Zm16.19,11.1v68.72h-17.37V49.79h17.37Z"></path>
                                          <path class="cls-2" d="m396.18,56.55c5.04,5.17,7.57,12.38,7.57,21.65v40.31h-17.36v-37.96c0-5.46-1.37-9.65-4.1-12.59-2.73-2.94-6.45-4.4-11.16-4.4s-8.58,1.47-11.35,4.4c-2.77,2.94-4.15,7.13-4.15,12.59v37.96h-17.37V49.79h17.37v8.56c2.31-2.98,5.27-5.31,8.87-7.01,3.6-1.69,7.55-2.54,11.85-2.54,8.19,0,14.8,2.59,19.85,7.75Z"></path>
                                          <path class="cls-2" d="m437.61,129.8c0,7.61-1.88,13.09-5.64,16.44-3.77,3.35-9.16,5.02-16.19,5.02h-7.69v-14.76h4.96c2.64,0,4.51-.52,5.58-1.55,1.07-1.03,1.61-2.71,1.61-5.02V49.79h17.37v80.01Zm-16.31-91.11c-2.03-1.94-3.04-4.36-3.04-7.26s1.01-5.31,3.04-7.26c2.02-1.94,4.61-2.92,7.75-2.92s5.58.97,7.57,2.92c1.98,1.94,2.98,4.36,2.98,7.26s-.99,5.31-2.98,7.26c-1.98,1.94-4.51,2.91-7.57,2.91s-5.73-.97-7.75-2.91Z"></path>
                                          <path class="cls-2" d="m454.42,65.42c2.77-5.37,6.53-9.51,11.29-12.4,4.76-2.89,10.07-4.34,15.94-4.34,5.13,0,9.61,1.04,13.46,3.1,3.85,2.07,6.92,4.67,9.24,7.82v-9.8h17.49v68.72h-17.49v-10.05c-2.23,3.23-5.31,5.89-9.24,8-3.93,2.11-8.46,3.16-13.58,3.16-5.79,0-11.06-1.49-15.82-4.47-4.76-2.98-8.52-7.17-11.29-12.59-2.77-5.41-4.16-11.64-4.16-18.67s1.38-13.11,4.16-18.48Zm47.45,7.88c-1.65-3.02-3.89-5.33-6.7-6.95-2.81-1.61-5.83-2.42-9.06-2.42s-6.2.79-8.93,2.36c-2.73,1.57-4.94,3.87-6.64,6.88-1.69,3.02-2.54,6.6-2.54,10.73s.85,7.75,2.54,10.85c1.69,3.1,3.93,5.48,6.7,7.13,2.77,1.66,5.72,2.48,8.87,2.48s6.24-.81,9.06-2.42c2.81-1.61,5.04-3.93,6.7-6.95,1.65-3.02,2.48-6.64,2.48-10.85s-.83-7.83-2.48-10.85Z"></path>
                                          <path class="cls-4" d="m591.93,102.01h-34.48l-5.71,16.5h-18.24l31.14-86.71h20.22l31.13,86.71h-18.36l-5.71-16.5Zm-4.71-13.89l-12.53-36.22-12.53,36.22h25.06Z"></path>
                                          <path class="cls-4" d="m645.02,31.93v86.58h-17.37V31.93h17.37Z"></path>
                                        </g>
                                      </g>
                                    </g>
                                  </svg>
                              </div>
                           <!-- <div class="text-center mt20 mt-md50">
                              <div class="pre-heading f-md-22 f-20 w600 lh140 white-clr">
                              Exploit The Emerging E-Learning Business With A Click…  
                              </div>
                           </div> -->
                           <div class="f-md-50 f-28 w700 white-clr lh140 mt-md30 mt20 text-center">
                           The Only AI App <span class="orange-clr orange-underline"><u> That Can Give Us PROFITABLE Funnels</u></span> With Just One Click…
                           </div>
                           <div class="mt-md30 mt20 f-28 f-md-50 w700 text-center lh140 text-capitalize white-clr">
                              <div class="post-heading">Make Us $955.45 Per Day...</div>
                           </div>
                           <div class="f-22 f-md-28 lh140 w600 text-center white-clr mt20 mt-md30">
                           No More Guesswork, Trial And Error, Or Blindly Trying A Funnel…<br class="d-none d-md-block"> We Jump Straight To Profit With Ninja AI
                           </div>
                        </div>
                        <div class="row d-flex flex-wrap align-items-center mt20 mt-md50">
                           <div class="col-12 col-md-10 mx-auto">
                              <img src="assets/images/productbox.png" class="img-fluid d-block mx-auto margin-bottom-15">
                           </div>
                        </div>
                        <div class="row mt30 mt-md80">
                           <div class="col-12">
                              <div class="f-28 f-md-38 w700 lh140 text-center white-clr">
                                 Get <span class="step-shapes"> NinjaAi </span> For Low One Time Fee… <br class="d-none d-md-block"> 
                              </div>
                              <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                                 <span class="w700">Just Pay Once</span> Instead Of <s class="red-clr">Monthly</s>
                              </div>
                           </div>
                        </div>
                        <div class="row align-items-center mt20">
                           <div class="col-md-7 mx-auto">
                              <a href="#buynow" class="cta-link-btn">&gt;&gt;&gt; Get Instant Access To NinjaAi &lt;&lt;&lt;</a>
                              <img src="assets/images/compaitable-gurantee1.webp" alt="Compaitable Gurantee" class="mx-auto d-block img-fluid mt20 mt-md30">
                           </div>
                        </div>
                     </div>
                </div>





   <div class="profitwall-sec">
       <div class="container">
         <div class="row">
            <div class="mt-md30 mt20 f-28 f-md-50 w700 text-center lh140 text-capitalize white-clr">
               <div class="post-heading">Let Me Show You The Power Of Ninja AI</div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md80 white-bg">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-22 f-md-34 w700 lh140 black-clr text-center text-md-start">
               Find The Perfect Funnel In Any Niche
               </div>
               <div class="f-18 f-md-20 w400 lh140 black-clr text-center text-md-start mt10">
               Just enter your niche. It's as easy as selecting from a menu. Or writing it yourself…
               </div>
               <div class="f-18 f-md-20 w400 lh140 black-clr text-center text-md-start mt10">
               Ninja AI covers over 990+ niches
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 1px solid #a94eff;">
                  <source src="assets/images/doitall-img1.mp4" type="video/mp4">
               </video>
            </div>
         </div>

         <div class="row align-items-center mt20 mt-md80 purple-bgg">
            <div class="col-12 col-md-6">
               <div class="f-22 f-md-36 w700 lh140 white-clr text-center text-md-start">
               Improve The Funnel Copywriting For 10x Conversions
               </div>
               <div class="f-18 f-md-20 w400 lh140 white-clr text-center text-md-start mt20">
               We don't want similar results like the guru, we want way better…
               </div>
               <div class="f-18 f-md-20 w400 lh140 white-clr text-center text-md-start mt20">
               That's why we let our AI model, rewrite their copy and improve it drasitcally.
               </div>
               <div class="f-18 f-md-20 w400 lh140 white-clr text-center text-md-start mt20">
               This alone, gave us 10x conversions…
               </div>
               <div class="f-18 f-md-20 w400 lh140 white-clr text-center text-md-start mt20">
               Which means 10x the sales and money.
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
            <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 1px solid #a94eff;">
                  <source src="assets/images/doitall-img2.mp4" type="video/mp4">
               </video>
            </div>
         </div>

         <div class="row align-items-center mt20 mt-md80 white-bg">
            <div class="col-12 col-md-7 order-md-2">
               <div class="f-22 f-md-38 w700 lh140 black-clr text-center text-md-start">
               Redesign The Funnel In HIGHER Quality
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               In order to make a funnel work nowadays…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               It must be unique, and that's why we always redesign every funnel…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               And improve it's quality. That's how we can make sure that we are outperforming all of our competitors
               </div>
            </div>
            <div class="col-12 col-md-5 mt20 mt-md0 order-md-1">
               <img src="assets/images/doitall-img3.png" alt="imagine-img" class="mx-auto d-block img-fluid">
            </div>
         </div>

         <div class="row align-items-center mt20 mt-md80 purple-bgg">
            <div class="col-12 col-md-6">
               <div class="f-22 f-md-36 w700 lh140 white-clr text-center text-md-start">
               Find The Perfect Product To Promote
               </div>
               <div class="f-18 f-md-20 w400 lh140 white-clr text-center text-md-start mt5">
               If you don't have a product already to promote…
               </div>
               <div class="f-18 f-md-20 w400 lh140 white-clr text-center text-md-start mt5">
               Don't worry…
               </div>
               <div class="f-18 f-md-20 w400 lh140 white-clr text-center text-md-start mt5">
               Because Ninja AI will do that for you too…
               </div>
               <div class="f-18 f-md-20 w400 lh140 white-clr text-center text-md-start mt5">
               It will give you a list of the affiliate products that the AI believe will sell
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/doitall-img4.webp" alt="imagine-img" class="mx-auto d-block img-fluid">
            </div>
         </div>

         <div class="row align-items-center mt20 mt-md80 white-bg">
            <div class="col-12 col-md-7 order-md-2">
               <div class="f-22 f-md-38 w700 lh140 black-clr text-center text-md-start">
               Completely Cloud Based Software
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               NinjaAi is fully cloud-based software that means there is no need of a domain name, hosting or downloading anything. It creates affiliate funnels ensuring they receive maximum viewership.
               </div>
            </div>
            <div class="col-12 col-md-5 mt20 mt-md0 order-md-1">
               <img src="assets/images/doitall-img6.png" alt="imagine-img" class="mx-auto d-block img-fluid">
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md80 purple-bgg">
            <div class="col-12 col-md-7">
               <div class="f-22 f-md-38 w700 lh140 white-clr text-center text-md-start">
               Creates Stunning Affiliate Funnels Within Minutes
               </div>
               <div class="f-18 f-md-22 w400 lh140 white-clr text-center text-md-start mt20">
               We've built NinjaAi from the ground up to be marketer-friendly, meaning our funnels are tweaked to perfection to help you get traffic and sales.
               </div>
            </div>
            <div class="col-12 col-md-5 mt20 mt-md0">
               <img src="assets/images/doitall-img7.png" alt="imagine-img" class="mx-auto d-block img-fluid">
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md80 white-bg">
            <div class="col-12 col-md-7 order-md-2">
               <div class="f-22 f-md-34 w700 lh140 black-clr text-center text-md-start">
               Inbuilt Text & Inline Editor To Create Hundreds of Unique Campaigns
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Just drag and drop the elements to the place on your page you want them to go. You can add, delete, modify text and images as per your brand identity and need and create stunning, high-converting & unique campaigns within minutes. 
               </div>
            </div>
            <div class="col-12 col-md-5 mt20 mt-md0 order-md-1">
               <img src="assets/images/doitall-img8.png" alt="imagine-img" class="mx-auto d-block img-fluid">
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md80 purple-bgg">
            <div class="col-12 col-md-7">
               <div class="f-22 f-md-38 w700 lh140 white-clr text-center text-md-start">
               Works Seamlessly with All Major Autoresponders
               </div>
               <div class="f-18 f-md-22 w400 lh140 white-clr text-center text-md-start mt20">
               Easily build your targeted email list with Ninja Ai AR integration…
               <br><br>Simply connect your autoresponder, and let Ninja Ai do the rest…
               </div>
            </div>
            <div class="col-12 col-md-5 mt20 mt-md0">
               <img src="https://aicademy.live/special/assets/images/ar-integration.webp" alt="imagine-img" class="mx-auto d-block img-fluid">
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md80 white-bg">
            <div class="col-12 col-md-7 order-md-2">
               <div class="f-22 f-md-38 w700 lh140 black-clr text-center text-md-start">
               Drive Thousands Of FREE Clicks To It
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               With Ninja AI you don't have to worry about traffic…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Or paying for ads in any way shape or form…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               It drives thousands of clicks for 100% free
               </div>
            </div>
            <div class="col-12 col-md-5 mt20 mt-md0 order-md-1">
               <img src="assets/images/doitall-img5.png" alt="imagine-img" class="mx-auto d-block img-fluid">
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md80 purple-bgg">
            <div class="col-12 col-md-7">
               <div class="f-22 f-md-38 w700 lh140 white-clr text-center text-md-start">
               Built-in SEO to Drive Targeted Traffic
               </div>
               <div class="f-18 f-md-22 w400 lh140 white-clr text-center text-md-start mt20">
               NinjaAi has been designed from the ground up with the best SEO practices in mind. So, our platform is completely SEO compatible to get your offers high rankings in Google and other search engines.
               </div>
            </div>
            <div class="col-12 col-md-5 mt20 mt-md0">
               <img src="assets/images/doitall-img9.png" alt="imagine-img" class="mx-auto d-block img-fluid">
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md80 white-bg">
            <div class="col-12 col-md-7 order-md-2">
               <div class="f-22 f-md-38 w700 lh140 black-clr text-center text-md-start">
               100% Mobile Friendly
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Each funnel you create is 100% mobile optimized
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               There are no glitches, no errors…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               AI will turn it into a fully functional mobile site in SECONDS…
               </div>
            </div>
            <div class="col-12 col-md-5 mt20 mt-md0 order-md-1">
               <img src="assets/images/doitall-img10.png" alt="imagine-img" class="mx-auto d-block img-fluid">
            </div>
         </div>
      </div>
   </div>

   <div class="cta-section-white">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-38 w700 lh140 text-center white-clr">
                  Get <span class="step-shapes"> NinjaAi </span> For Low One Time Fee… <br class="d-none d-md-block"> 
               </div>
               <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                  <span class="w700">Just Pay Once</span> Instead Of <s class="red-clr">Monthly</s>
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md50">
            <div class="col-md-7">
               <a href="#buynow" class="cta-link-btn">&gt;&gt;&gt; Get Instant Access To NinjaAi &lt;&lt;&lt;</a>
               <img src="assets/images/compaitable-gurantee1.webp" alt="Compaitable Gurantee" class="mx-auto d-block img-fluid mt20 mt-md30">
            </div>
            <div class="col-md-5 mt20 mt-md0">
               <div class="countdown-container">
                  <div class="f-20 f-md-24 w400 lh140 text-center white-clr mb10">
                     <span class="w700 red-clr">Hurry up!</span>  Price Increases In
                  </div>
                  <div class="countdown counter-white text-center"><div class="timer-label text-center"><span class="f-24 f-md-32 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-14 w500 smmltd">Days</span> </div><div class="timer-label text-center"><span class="f-24 f-md-32 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-14 w500 smmltd">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-24 f-md-32 timerbg oswald">18&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-14 w500 smmltd">Mins</span> </div><div class="timer-label text-center "><span class="f-24 f-md-32 timerbg oswald">38</span><br><span class="f-14 f-md-14 w500 smmltd">Sec</span> </div></div>
               </div>
            </div>
         </div>
      </div>
   </div>

   <div class="limited-time-sec">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-20 f-md-24 w700 text-center white-clr align-items-center flex-column flex-md-row justify-content-center">
                  <img src="assets/images/caution-icon.webp" class="img-fluid mr-md5 mb5 mb-md0" alt="Caution Icon">
                  Your $2,475.34 Bonuses Package Expires When Timer Hits ZERO 
                  <img src="assets/images/caution-icon.webp" class="img-fluid ml-md5 mb5 mb-md0" alt="Caution Icon">
               </div>
            </div>
         </div>
      </div>
   </div>
<!-- 
   <div class="areyouready-section">
      <div class="container">
         <div class="row">
            <div class="col-12 f-28 f-md-50 w700 lh140 black-clr text-center">
               Are You Ready For An A.I. Revolution?
            </div>
            <div class="col-12 col-md-7 mx-auto f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
            NinjaAi  And AI Pay Us Daily…
            </div>
            <div class="col-12 f-18 f-md-22 w400 lh140 black-clr text-center mt20 mt-md50">
               There is nothing that AI can't do in the digital world… It's the most powerful technology that exists now… And NinjaAi is the only app on the market…<br><br>
               <span class="w600 lh120">
               That harnessed that power, and created something that makes us money day after day
            </span>
            </div>
            <img src="assets/images/proof1.webp" alt="Quotes" class="mx-auto d-block img-fluid mt20 mt-md30 ">
            <div class="col-12 f-18 f-md-22 w400 lh140 black-clr text-center mt20 mt-md50">
               Every day we make money without doing any work… And not pocket change…<br><br>
               <span class="w600 lh120">
               Last month alone we made a bit over $17,000 with NinjaAi 
               </span>
            </div>
            <img src="assets/images/proof2.webp" alt="Quotes" class="mx-auto d-block img-fluid mt20 mt-md30 ">
            <div class="col-12 f-28 f-md-50 w700 lh140 black-clr text-center">
               Wanna know how?
               <img src="assets/images/red-underline.webp" alt="Quotes" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div> -->


   <!-- Imagine Section Start -->
   <div class="imagine-section">
      <div class="container">
         <!-- <div class="row">
            <div class="col-12 text-center">
               <div class="f-28 f-md-50 w400 lh140 black-clr text-center">
               Imagine If You Can Conquer Multi-Billion Industry… <span class="w700"> With ONE App…</span>
               </div>
            </div>
         </div> -->
         <div class="row align-items-center">
            <div class="col-12 col-md-6  order-md-2">
               <div class="f-22 f-md-50 w600 lh140 black-clr text-center text-md-start">
               No More <strike class="red-clr">“Luck”</strike>
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Luck is not in our dictionary…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               We don't test, and we don't try…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               We turn on Ninja AI and we immediately get what we want…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               We get profitable funnels that we can swipe with a click…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               This funnels is secretly used by all the big gurus you see..
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               And we finally made it available to anyone…
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/imagine-img.png" alt="imagine-img" class="mx-auto d-block img-fluid ">
            </div>
         </div>

         <!-- <div class="row mt20 mt-md50">
         <div class="col-12 f-28 f-md-50 w700 lh140 black-clr text-center">
               Wanna know how?
               <img src="assets/images/red-underline.webp" alt="Quotes" class="mx-auto d-block img-fluid ">
            </div>
         </div> -->
      </div>
   </div>
   <!-- Imagine Section End -->

   <div class="niche-section">
      <div class="container">
         <div class="row align-items-center mt20 mt-md30">
            <div class="col-12 col-md-6">
               <div class="f-26 f-md-50 w700 lh140 black-clr text-center text-md-start mt20">
                  Works In Any Niche…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
                  Not everyone is in the MMO niche…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
                  And if you are one of those people…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
                  Don't worry, we got you covered…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
                  Because we made sure that ninja AI works in any niche no matter what…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
                  In fact, it works for anyone no matter what…
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/niche-girl.png" alt="Matter" class="mx-auto d-block img-fluid ">
            </div>
         </div>
        
         <div class="regardless-sec">
            <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3">
               <div class="col">
                  <div class="figure">
                     <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/product-creator.webp" alt="Product Creators" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption">
                        <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                        Product Creators
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="figure">
                     <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/affiliate-marketers.webp" alt="Affiliate Marketers" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption">
                        <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                           Affiliate Marketers
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="figure">
                     <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/ecom-store-owners.webp" alt="eCom Store Owners" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption">
                        <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                           eCom Store Owners
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="figure">
                     <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/blog-owners.webp" alt="Blog Owners" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption">
                        <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                           Blog Owners
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="figure">
                     <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/marketers.webp" alt="CPA Marketers" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption">
                        <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                           CPA Marketers
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="figure">
                     <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/video-marketers.webp" alt="Video Marketers" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption">
                        <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                           Video Marketers
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="figure">
                     <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/content-creators.webp" alt="Artists/Content Creators" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption">
                        <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                           Artists/Content Creators
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="figure">
                     <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/personal-brands.webp" alt="Personal Brands" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption">
                        <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                           Personal Brands
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="figure">
                     <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/freelancers.webp" alt="Freelancers" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption">
                        <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                           Freelancers
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>

      </div>
   </div>
   
   <div class="justoneclick-section">
      <div class="container">
         <div class="row align-items-center mt20 mt-md50">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-26 f-md-50 w700 lh140 black-clr text-center text-md-start">
               All It Takes Is <br class="d-none d-md-block"><span class="orange-clr">Just One Click...</span>
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               No need for complicated setup…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               All you need is your finger to click a button…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               And immediately you will swipe any profitable funnel you want…
               </div>
               <div class="f-18 f-md-22 w600 lh140 black-clr text-center text-md-start mt20">
               No need for:
               </div>
               <ul class="cross-head pl0 m0 f-18 f-md-22 lh160 w400 black-clr">
                  <li>Hosting</li>
                  <li>Domain</li>
                  <li>Design</li>
                  <li>Copywriting</li>
                  <li>Vsl creation</li>
                  <li>Optimizing</li>
                  <li>Spending on ads</li>
               </ul>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
                  None of that, you just swipe and enjoy… 
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
                  At least that we been doing along all of our beta members…  
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
                  And daily, we make money like this:  
               </div>
               
               <!-- <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Within seconds, WebGenie will create you a unique, and stunning website in any niche, in any language.
               </div> -->
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/oneclick-img.png" alt="Profitable" class="mx-auto d-block img-fluid">
            </div>
         </div>
      </div>
   </div>


   <div class="profitale-section">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <!-- <div class="easy-text f-28 f-md-65 w700 lh140 white-clr mx-auto">Easy</div> -->
               <div class="f-28 f-md-50 w600 lh140 black-clr text-center mt20">
                The Only <span class="orange-clr">ChatGPT4 Driven App…</span>
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md50">
            <div class="col-12 col-md-6">
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start">
               Look I'm not gonna lie…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               We couldn't have created such a beast…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Without an AI model so robust that it can infltrate ANYTHING for us…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               That's why we 100% integrate with ChatGPT4 to do this magic…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               There is nothing on the market right now that can even come close to what we have here…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               But the good thing is…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               It all works in the background…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               You don't have to do anything, you don't have to see anything even… 
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               We just click swipe…
               </div>
               <div class="f-18 f-md-22 w600 lh140 black-clr text-center text-md-start mt20">
               And within a few days, our account went from this
               </div>
              
               <img src="assets/images/paypal.webp" alt="Proof SS" class="mx-auto d-block img-fluid mt20 mt-md24">
               <div class="text-center text-md-start">
                  <div class="smile-text f-20 f-md-22 w400 lh140 white-clr mt20 mt-md24">Sounds good, huh?</div>
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/chatgpt4-img.png" alt="Profitable" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <div class="buthowdoesit-section">
      <div class="container">
         <div class="row align-items-center mt20 mt-md50">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-22 f-md-48 w700 lh140 black-clr text-center text-md-start">
                  <span class="red-clr">But How Does It</span> Really Work?
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Im sure you are wondering now… 
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               I mean, just saying it works with ChatGPT4 doesn't explain all…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               So, allow me to explain how this piece-of-art works…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               First thing the app does is scan for all the gurus online… 
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Everyone who is making money in a niche…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Then through it's specific model of infiltrating “100% legal btw”
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               It extract exactly what this guru is doing to make his money…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               What kind of product is he selling, what type of funnel, and everything in between…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Then it works in replicating it within seconds…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               And offer it all to us on a silver platter…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Just to swipe, and enjoy.
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/reallywork-img.png" alt="Create Course" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <div class="already-section">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-6">
               <div class="f-26 f-md-50 w700 lh140 black-clr text-center text-md-start">
               Isn't Many App Doing That Already?
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               I can see why you would think that… 
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Every day there are tons of apps that launch that can “create funnels”
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Let's assume they even work “most of them don't btw”
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               At best, they will give you just random funnels…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Those funnels are not proven to work…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Basically, they turning you into their guinea pig…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               But that's completely different with Ninja AI…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               We only share what we REALLY know works 100%
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Eliminating all the guesswork for you…
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/already-girl.png" alt="Profitable" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>
   <div class="profitwall-sec">
      <div class="container">
         <div class="row">
            <div class="mt-md30 mt20 f-28 f-md-50 w700 text-center lh140 text-capitalize white-clr">
               <div class="post-heading">Ninja AI Do-It-All…</div>
            </div>
         </div>


         <div class="row align-items-center mt20 mt-md80 white-bg">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-22 f-md-34 w700 lh140 black-clr text-center text-md-start">
               Find The Perfect Funnel In Any Niche
               </div>
               <div class="f-18 f-md-20 w400 lh140 black-clr text-center text-md-start mt10">
               Just enter your niche. It's as easy as selecting from a menu. Or writing it yourself…
               </div>
               <div class="f-18 f-md-20 w400 lh140 black-clr text-center text-md-start mt10">
               Ninja AI covers over 990+ niches
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 1px solid #a94eff;">
                  <source src="assets/images/doitall-img1.mp4" type="video/mp4">
               </video>
            </div>
         </div>

         <div class="row align-items-center mt20 mt-md80 purple-bgg">
            <div class="col-12 col-md-6">
               <div class="f-22 f-md-36 w700 lh140 white-clr text-center text-md-start">
               Improve The Funnel Copywriting For 10x Conversions
               </div>
               <div class="f-18 f-md-20 w400 lh140 white-clr text-center text-md-start mt20">
               We don't want similar results like the guru, we want way better…
               </div>
               <div class="f-18 f-md-20 w400 lh140 white-clr text-center text-md-start mt20">
               That's why we let our AI model, rewrite their copy and improve it drasitcally.
               </div>
               <div class="f-18 f-md-20 w400 lh140 white-clr text-center text-md-start mt20">
               This alone, gave us 10x conversions…
               </div>
               <div class="f-18 f-md-20 w400 lh140 white-clr text-center text-md-start mt20">
               Which means 10x the sales and money.
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
            <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 1px solid #a94eff;">
                  <source src="assets/images/doitall-img2.mp4" type="video/mp4">
               </video>
            </div>
         </div>

         <div class="row align-items-center mt20 mt-md80 white-bg">
            <div class="col-12 col-md-7 order-md-2">
               <div class="f-22 f-md-38 w700 lh140 black-clr text-center text-md-start">
               Redesign The Funnel In HIGHER Quality
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               In order to make a funnel work nowadays…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               It must be unique, and that's why we always redesign every funnel…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               And improve it's quality. That's how we can make sure that we are outperforming all of our competitors
               </div>
            </div>
            <div class="col-12 col-md-5 mt20 mt-md0 order-md-1">
               <img src="assets/images/doitall-img3.png" alt="imagine-img" class="mx-auto d-block img-fluid">
            </div>
         </div>

         <div class="row align-items-center mt20 mt-md80 purple-bgg">
            <div class="col-12 col-md-6">
               <div class="f-22 f-md-36 w700 lh140 white-clr text-center text-md-start">
               Find The Perfect Product To Promote
               </div>
               <div class="f-18 f-md-20 w400 lh140 white-clr text-center text-md-start mt5">
               If you don't have a product already to promote…
               </div>
               <div class="f-18 f-md-20 w400 lh140 white-clr text-center text-md-start mt5">
               Don't worry…
               </div>
               <div class="f-18 f-md-20 w400 lh140 white-clr text-center text-md-start mt5">
               Because Ninja AI will do that for you too…
               </div>
               <div class="f-18 f-md-20 w400 lh140 white-clr text-center text-md-start mt5">
               It will give you a list of the affiliate products that the AI believe will sell
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/doitall-img4.webp" alt="imagine-img" class="mx-auto d-block img-fluid">
            </div>
         </div>

         <div class="row align-items-center mt20 mt-md80 white-bg">
            <div class="col-12 col-md-7 order-md-2">
               <div class="f-22 f-md-38 w700 lh140 black-clr text-center text-md-start">
               Completely Cloud Based Software
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               NinjaAi is fully cloud-based software that means there is no need of a domain name, hosting or downloading anything. It creates affiliate funnels ensuring they receive maximum viewership.
               </div>
            </div>
            <div class="col-12 col-md-5 mt20 mt-md0 order-md-1">
               <img src="assets/images/doitall-img6.png" alt="imagine-img" class="mx-auto d-block img-fluid">
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md80 purple-bgg">
            <div class="col-12 col-md-7">
               <div class="f-22 f-md-38 w700 lh140 white-clr text-center text-md-start">
               Creates Stunning Affiliate Funnels Within Minutes
               </div>
               <div class="f-18 f-md-22 w400 lh140 white-clr text-center text-md-start mt20">
               We've built NinjaAi from the ground up to be marketer-friendly, meaning our funnels are tweaked to perfection to help you get traffic and sales.
               </div>
            </div>
            <div class="col-12 col-md-5 mt20 mt-md0">
               <img src="assets/images/doitall-img7.png" alt="imagine-img" class="mx-auto d-block img-fluid">
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md80 white-bg">
            <div class="col-12 col-md-7 order-md-2">
               <div class="f-22 f-md-34 w700 lh140 black-clr text-center text-md-start">
               Inbuilt Text & Inline Editor To Create Hundreds of Unique Campaigns
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Just drag and drop the elements to the place on your page you want them to go. You can add, delete, modify text and images as per your brand identity and need and create stunning, high-converting & unique campaigns within minutes. 
               </div>
            </div>
            <div class="col-12 col-md-5 mt20 mt-md0 order-md-1">
               <img src="assets/images/doitall-img8.png" alt="imagine-img" class="mx-auto d-block img-fluid">
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md80 purple-bgg">
            <div class="col-12 col-md-7">
               <div class="f-22 f-md-38 w700 lh140 white-clr text-center text-md-start">
               Works Seamlessly with All Major Autoresponders
               </div>
               <div class="f-18 f-md-22 w400 lh140 white-clr text-center text-md-start mt20">
               Easily build your targeted email list with Ninja Ai AR integration…
               <br><br>Simply connect your autoresponder, and let Ninja Ai do the rest…
               </div>
            </div>
            <div class="col-12 col-md-5 mt20 mt-md0">
               <img src="https://aicademy.live/special/assets/images/ar-integration.webp" alt="imagine-img" class="mx-auto d-block img-fluid">
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md80 white-bg">
            <div class="col-12 col-md-7 order-md-2">
               <div class="f-22 f-md-38 w700 lh140 black-clr text-center text-md-start">
               Drive Thousands Of FREE Clicks To It
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               With Ninja AI you don't have to worry about traffic…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Or paying for ads in any way shape or form…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               It drives thousands of clicks for 100% free
               </div>
            </div>
            <div class="col-12 col-md-5 mt20 mt-md0 order-md-1">
               <img src="assets/images/doitall-img5.png" alt="imagine-img" class="mx-auto d-block img-fluid">
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md80 purple-bgg">
            <div class="col-12 col-md-7">
               <div class="f-22 f-md-38 w700 lh140 white-clr text-center text-md-start">
               Built-in SEO to Drive Targeted Traffic
               </div>
               <div class="f-18 f-md-22 w400 lh140 white-clr text-center text-md-start mt20">
               NinjaAi has been designed from the ground up with the best SEO practices in mind. So, our platform is completely SEO compatible to get your offers high rankings in Google and other search engines.
               </div>
            </div>
            <div class="col-12 col-md-5 mt20 mt-md0">
               <img src="assets/images/doitall-img9.png" alt="imagine-img" class="mx-auto d-block img-fluid">
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md80 white-bg">
            <div class="col-12 col-md-7 order-md-2">
               <div class="f-22 f-md-38 w700 lh140 black-clr text-center text-md-start">
               100% Mobile Friendly
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Each funnel you create is 100% mobile optimized
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               There are no glitches, no errors…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               AI will turn it into a fully functional mobile site in SECONDS…
               </div>
            </div>
            <div class="col-12 col-md-5 mt20 mt-md0 order-md-1">
               <img src="assets/images/doitall-img10.png" alt="imagine-img" class="mx-auto d-block img-fluid">
            </div>
         </div>
      </div>
   </div>
   <div class="cta-section-white">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-38 w700 lh140 text-center white-clr">
                  Get <span class="step-shapes"> NinjaAi </span> For Low One Time Fee… <br class="d-none d-md-block"> 
               </div>
               <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                  <span class="w700">Just Pay Once</span> Instead Of <s class="red-clr">Monthly</s>
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md50">
            <div class="col-md-7">
               <a href="#buynow" class="cta-link-btn">&gt;&gt;&gt; Get Instant Access To NinjaAi &lt;&lt;&lt;</a>
               <img src="assets/images/compaitable-gurantee1.webp" alt="Compaitable Gurantee" class="mx-auto d-block img-fluid mt20 mt-md30">
            </div>
            <div class="col-md-5 mt20 mt-md0">
               <div class="countdown-container">
                  <div class="f-20 f-md-24 w400 lh140 text-center white-clr mb10">
                     <span class="w700 red-clr">Hurry up!</span>  Price Increases In
                  </div>
                  <div class="countdown counter-white text-center"><div class="timer-label text-center"><span class="f-24 f-md-32 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-14 w500 smmltd">Days</span> </div><div class="timer-label text-center"><span class="f-24 f-md-32 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-14 w500 smmltd">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-24 f-md-32 timerbg oswald">02&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-14 w500 smmltd">Mins</span> </div><div class="timer-label text-center "><span class="f-24 f-md-32 timerbg oswald">15</span><br><span class="f-14 f-md-14 w500 smmltd">Sec</span> </div></div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="limited-time-sec">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-20 f-md-24 w700 text-center white-clr align-items-center flex-column flex-md-row justify-content-center">
                  <img src="assets/images/caution-icon.webp" class="img-fluid mr-md5 mb5 mb-md0" alt="Caution Icon">
                  Your $2,475.34 Bonuses Package Expires When Timer Hits ZERO 
                  <img src="assets/images/caution-icon.webp" class="img-fluid ml-md5 mb5 mb-md0" alt="Caution Icon">
               </div>
            </div>
         </div>
      </div>
   </div>

   
   <div class="step-section">
      <div class="container ">
         <div class="row ">
            <div class="col-12 f-28 f-md-50 w700 lh140 black-clr text-center">
               All It Takes Is 3 Fail-Proof Steps<br class="d-none d-md-block">
            </div>
            <div class="col-12 f-24 f-md-28 w600 lh140 white-clr text-center mt20">
               <div class="post-heading">
               Start Dominating ANY Niche With DFY AI Funnels…
               </div>
            </div>
            <div class="col-12 mt30 mt-md90">
               <div class="row align-items-center ">
                  <div class="col-12 col-md-6">
                     <div class="step-shapes f-20 f-md-22 w600 white-clr">
                        Step 1
                     </div>
                     <div class="f-34 f-md-65 w700 lh140 mt15 caveat">
                     Access
                     </div>
                     <div class="f-20 f-md-24 w400 lh140 mt5 mt-md10 text-capitalize">
                     click on any of the links below to get instant access to NinjaAI for a low 1-time fee
                     </div>
                  </div>
                  
                  <div class="col-12 col-md-6 mt20 mt-md0 ">
                  <!-- <img src="assets/images/video-thumb.webp" alt="Video" class="mx-auto d-block img-fluid"> -->
                     <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 1px solid #a94eff;">
                        <source src="assets/images/step-one.mp4" type="video/mp4">
                     </video>
                  </div>
               </div>
            </div>
            <div class="col-12">
               <img src="assets/images/left-arrow.webp" class="img-fluid d-none d-md-block mx-auto " alt="Left Arrow">
            </div>
            <div class="col-12 mt30 mt-md30">
               <div class="row align-items-center ">
                  <div class="col-12 col-md-6 order-md-2">
                     <div class="step-shapes f-20 f-md-22 w600 white-clr">
                        Step 2
                     </div>
                     <div class="f-34 f-md-65 w700 lh140 mt15 caveat">
                     Choose
                     </div>
                     <div class="f-20 f-md-24 w400 lh140 mt5 mt-md10 text-capitalize">
                     Choose your niche, and let the AI take care of everything for you…
                     </div>
                  </div>
                  <div class="col-md-6 col-md-pull-6 mt20 mt-md0 order-md-1 ">
                  <!-- <img src="assets/images/video-thumb.webp" alt="Video" class="mx-auto d-block img-fluid"> -->
                     <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 1px solid #a94eff;">
                        <source src="assets/images/step-two.mp4" type="video/mp4">
                     </video>
                  </div>
               </div>
            </div>
            <div class="col-12">
               <img src="assets/images/right-arrow.webp" class="img-fluid d-none d-md-block mx-auto" alt="Right Arrow">
            </div>
            <div class="col-12 mt30 mt-md30">
               <div class="row align-items-center ">
                  <div class="col-12 col-md-6">
                     <div class="step-shapes f-20 f-md-22 w600 white-clr">
                        Step 3
                     </div>
                     <div class="f-34 f-md-65 w700 lh140 mt15 caveat">
                     Profit
                     </div>
                     <div class="f-20 f-md-24 w400 lh140 mt5 mt-md10 text-capitalize">
                     That's it, there is no technical setup, there is no learning curve…
                     <br>
                     And we daily make money like this.
                     </div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 ">
                  <!-- <img src="assets/images/video-thumb.webp" alt="Video" class="mx-auto d-block img-fluid"> -->
                     <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 1px solid #a94eff;">
                        <source src="assets/images/step-three.mp4" type="video/mp4">
                     </video>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
 

   <div class="cta-section-white">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-38 w700 lh140 text-center white-clr">
                  Get <span class="step-shapes"> NinjaAi </span> For Low One Time Fee… <br class="d-none d-md-block"> 
               </div>
               <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                  <span class="w700">Just Pay Once</span> Instead Of <s class="red-clr">Monthly</s>
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md50">
            <div class="col-md-7">
               <a href="#buynow" class="cta-link-btn">&gt;&gt;&gt; Get Instant Access To NinjaAi &lt;&lt;&lt;</a>
               <img src="assets/images/compaitable-gurantee1.webp" alt="Compaitable Gurantee" class="mx-auto d-block img-fluid mt20 mt-md30">
            </div>
            <div class="col-md-5 mt20 mt-md0">
               <div class="countdown-container">
                  <div class="f-20 f-md-24 w400 lh140 text-center white-clr mb10">
                     <span class="w700 red-clr">Hurry up!</span>  Price Increases In
                  </div>
                  <div class="countdown counter-white text-center"><div class="timer-label text-center"><span class="f-24 f-md-32 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-14 w500 smmltd">Days</span> </div><div class="timer-label text-center"><span class="f-24 f-md-32 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-14 w500 smmltd">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-24 f-md-32 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-14 w500 smmltd">Mins</span> </div><div class="timer-label text-center "><span class="f-24 f-md-32 timerbg oswald">00</span><br><span class="f-14 f-md-14 w500 smmltd">Sec</span> </div></div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="limited-time-sec">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-20 f-md-24 w700 text-center white-clr align-items-center flex-column flex-md-row justify-content-center">
                  <img src="assets/images/caution-icon.webp" class="img-fluid mr-md5 mb5 mb-md0" alt="Caution Icon">
                  Your $2,475.34 Bonuses Package Expires When Timer Hits ZERO 
                  <img src="assets/images/caution-icon.webp" class="img-fluid ml-md5 mb5 mb-md0" alt="Caution Icon">
               </div>
            </div>
         </div>
      </div>
   </div>



   <section class="white-section">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-28 f-md-50 w700 lh140 black-clr text-center">
                 <span class="left-line">You Get Everything </span> You Need…
               </div>
            </div>
         </div>
         <div class="row mt20 mt-md80 align-items-center">
            <div class="col-12 col-md-6">
               <div class="f-28 f-md-38 lh140 w700 text-center white-clr need-head">
               Ninja AI App
               </div>
               <div class="f-20 f-md-22 w400 lh140 mt20 black-clr">
               The app that is responsible for everything…
               </div>
               <div class="f-20 f-md-22 w400 lh140 mt20 black-clr">
               Get 100% full access to our state-of-the-art AI app…
               </div>
               <div class="f-20 f-md-22 w700 lh140 mt10 red-clr">
               (Worth $997/mo)
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/everything-img1.png" alt="Aicademy App" class="mx-auto d-block img-fluid ">
            </div>
         </div>
         <div class="row mt20 mt-md80">
            <div class="col-12">
               <div class="border-line"></div>
            </div>
         </div>
         <div class="row mt20 mt-md80 align-items-center">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-28 f-md-38 lh140 w700 text-center white-clr need-head">
               AI DFY Funnels
               </div>
               <div class="f-20 f-md-22 w400 lh140 mt20 black-clr">
               Just choose a niche, and NInja AI will give you all the funnels you want…
               </div>
               <div class="f-20 f-md-22 w400 lh140 mt20 black-clr">
               Best of all, each funnel is 100% profitable for us
               </div>
               <div class="f-20 f-md-22 w700 lh140 mt10 red-clr">
                  (Worth $997)
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/everything-img2.png" alt="AI Content Generator" class="mx-auto d-block img-fluid ">
            </div>
         </div>
         <div class="row mt20 mt-md80">
            <div class="col-12">
               <div class="border-line"></div>
            </div>
         </div>
         <div class="row mt20 mt-md80 align-items-center">
            <div class="col-12 col-md-6">
               <div class="f-28 f-md-38 lh140 w700 text-center white-clr need-head">
               Ninja AI Built-In Traffic
               </div>
               <div class="f-20 f-md-22 w400 lh140 mt20 black-clr">
               You don't have to worry about traffic any more, let AI handle that for you
               </div>
               <div class="f-20 f-md-22 w700 lh140 mt10 red-clr">
                  (Worth $997)
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/everything-img3.png" alt="Aicademy App" class="mx-auto d-block img-fluid ">
            </div>
         </div>
         <div class="row mt20 mt-md80">
            <div class="col-12">
               <div class="border-line"></div>
            </div>
         </div>
         <div class="row mt20 mt-md80 align-items-center">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-28 f-md-38 lh140 w700 text-center white-clr need-head">
               Ninja AI Mobile EDITION
               </div>
               <div class="f-20 f-md-22 w400 lh140 mt20 black-clr">
               This will allow you to also operate Ninja AI, even from your mobile phone…
               </div>
               <div class="f-20 f-md-22 w400 lh140 mt10 black-clr">
               Whether it's an Android, iPhone, or tablet, it will work…
               </div>
               <div class="f-20 f-md-22 w700 lh140 mt10 red-clr">
                  (Worth $997)
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/everything-img4.png" alt="Aicademy Mobile Optimizer" class="mx-auto d-block img-fluid ">
            </div>
         </div>
         <div class="row mt20 mt-md80">
            <div class="col-12">
               <div class="border-line"></div>
            </div>
         </div>
         <div class="row mt20 mt-md80 align-items-center">
            <div class="col-12 col-md-6">
               <div class="f-28 f-md-38 lh140 w700 text-center white-clr need-head">
               Training videos
               </div>
               <div class="f-20 f-md-22 w400 lh140 mt20 black-clr">
               There is NOTHING missing in this training… 
               </div>
               <div class="f-20 f-md-22 w400 lh140 mt20 black-clr">
               Everything you need to know is explained in IMMENSE details 
               </div>
               <div class="f-20 f-md-22 w700 lh140 mt10 red-clr">
                  (Worth $997)
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/everything-img5.png" alt="Aicademy Mobile EDITION" class="mx-auto d-block img-fluid ">
            </div>
         </div>
         <div class="row mt20 mt-md80">
            <div class="col-12">
               <div class="border-line"></div>
            </div>
         </div>
         <div class="row mt20 mt-md80 align-items-center">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-28 f-md-38 lh140 w700 text-center white-clr need-head">
               World-class support
               </div>
               <div class="f-20 f-md-22 w400 lh140 mt20 black-clr">
               Have a question? Just reach out to us and our team will do their best to fix your problem in no time
               </div>
               <div class="f-20 f-md-22 w700 lh140 mt10 red-clr">
               (Worth A LOT)
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/everything-img6.png" alt="Training videos" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </section>



   <div class="cta-section-white">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-38 w700 lh140 text-center white-clr">
                  Get <span class="step-shapes"> NinjaAi </span> For Low One Time Fee… <br class="d-none d-md-block"> 
               </div>
               <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                  <span class="w700">Just Pay Once</span> Instead Of <s class="red-clr">Monthly</s>
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md50">
            <div class="col-md-7">
               <a href="#buynow" class="cta-link-btn">&gt;&gt;&gt; Get Instant Access To NinjaAi &lt;&lt;&lt;</a>
               <img src="assets/images/compaitable-gurantee1.webp" alt="Compaitable Gurantee" class="mx-auto d-block img-fluid mt20 mt-md30">
            </div>
            <div class="col-md-5 mt20 mt-md0">
               <div class="countdown-container">
                  <div class="f-20 f-md-24 w400 lh140 text-center white-clr mb10">
                     <span class="w700 red-clr">Hurry up!</span>  Price Increases In
                  </div>
                  <div class="countdown counter-white text-center"><div class="timer-label text-center"><span class="f-24 f-md-32 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-14 w500 smmltd">Days</span> </div><div class="timer-label text-center"><span class="f-24 f-md-32 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-14 w500 smmltd">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-24 f-md-32 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-14 w500 smmltd">Mins</span> </div><div class="timer-label text-center "><span class="f-24 f-md-32 timerbg oswald">00</span><br><span class="f-14 f-md-14 w500 smmltd">Sec</span> </div></div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="limited-time-sec">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-20 f-md-24 w700 text-center white-clr align-items-center flex-column flex-md-row justify-content-center">
                  <img src="assets/images/caution-icon.webp" class="img-fluid mr-md5 mb5 mb-md0" alt="Caution Icon">
                  Your $2,475.34 Bonuses Package Expires When Timer Hits ZERO 
                  <img src="assets/images/caution-icon.webp" class="img-fluid ml-md5 mb5 mb-md0" alt="Caution Icon">
               </div>
            </div>
         </div>
      </div>
   </div>


   <div class="butif-section">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-26 f-md-50 w700 lh140 black-clr text-center text-md-start">
               But It Gets Better…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               By getting access to Ninja AI
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               You will immediately unlock access to our custom-made bonuses…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               We created this pack exclusively for Ninja AI
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Our goal was simple…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Give fast action takers the advantage…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               By giving them whatever they need to achieve 10x the results… In half the time…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Here is exactly what you will get:
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/butit-img.png" alt="Profitable" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>
   <!-- <div class="take-look-sec">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-28 f-md-50 w600 lh140 white-clr text-center">
               Take A Look At What NinjaAi <br class="d-none d-md-block"> Created For Us… 
               </div>
            </div>
         </div>
         <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 gap30 mt20 mt-md10">
            <div class="col">
               <div class="video-box1">
                  <div style="padding-bottom: 56.25%;position: relative;"><iframe src="https://aicademy.oppyo.com/video/embed/xwgw1x48vd" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div>
               </div>
            </div>
            <div class="col">
               <div class="video-box1">
                  <div style="padding-bottom: 56.25%;position: relative;"><iframe src="https://aicademy.oppyo.com/video/embed/a7aofg8uzm" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div>
               </div>
            </div>
            <div class="col">
               <div class="video-box1">
                  <div style="padding-bottom: 56.25%;position: relative;"><iframe src="https://aicademy.oppyo.com/video/embed/6lsgozh9xc" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div>
               </div>
            </div>
            <div class="col">
               <div style="padding-bottom: 56.25%;position: relative;"><iframe src="https://aicademy.oppyo.com/video/embed/zhhocsqwcg" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div>
            </div>
            <div class="col">
               <div class="video-box1">
                  <div style="padding-bottom: 56.25%;position: relative;"><iframe src="https://aicademy.oppyo.com/video/embed/cpkcbm24pf" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div>
               </div>
            </div>
            <div class="col">
               <div class="video-box1">
                  <div style="padding-bottom: 56.25%;position: relative;"><iframe src="https://aicademy.oppyo.com/video/embed/ta2b2rhknp" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div>
               </div>
            </div>
         </div>
      </div>
   </div>

   <div class="not-just">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-50 w700 lh140  black-clr text-center">
                  But Not Just Courses…
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 text-center">
               <div class="f-24 f-md-36 w400 black-clr lh140">
                  NinjaAi Creates Stunning Websites for us that will work 24/7…. To sell and our courses…
               </div>
            </div>
            <div class="col-12 mt-md50 mt20">
               <img src="assets/images/f1.webp" class="img-fluid d-block mx-auto">
            </div>
         </div>
      </div>
   </div>

   <div class="traffic-section">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-6">
               <div class="f-28 f-md-45 w400 lh140 mt20 black-clr text-center text-md-start">
               Even Traffic… <br class="d-none d-md-block"><span class="w600">Is Done-For-Us</span>
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               NinjaAi does that for us automatically…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               It lines up hundreds of customers for us every single day…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Without us spending a penny on ads… or waiting…
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/traffic-img.webp" alt="Traffic" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <div class="online-section">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-28 f-md-45 w400 lh140 mt20 black-clr text-center text-md-start">
               This Is Your Chance…  <br class="d-none d-md-block"> <span class="w600">To Tap Into The Online-Learning Industry</span>
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               NinjaAi made it incredibly easy for us…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               To tap into the exploding business that is e-learning with just one click…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               The same business that even big tech giants like IBM, Google, And Microsoft got into…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               They spent billions of dollars creating their own e-learning portals…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Both for profit, and to train their staff…
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/online-img.webp" alt="Traffic" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <div class="capability-sec" id="features">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-45 w500 lh140 text-center black-clr">
                  Take A Look At What <span class="w700 orange-line"> NinjaAi Can Do For You </span>
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md90">
            <div class="col-md-6 col-12">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
                  Build A Udemy-Like Website  
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  With just one click, you will have a DFY Udemy-like website that well sell any course you want on your behalf…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Without you doing any of the work…
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0">
               <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 1px solid #15c6fc;">
                  <source src="assets/images/udemy-website.mp4" type="video/mp4">
               </video>
            </div>
         </div>

         <div class="row align-items-center mt-md120">
            <div class="col-md-6 col-12 order-md-2">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
                  Generate AI Courses Easily
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Sell high-quality courses that you did NOT create…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  But rather, AI created…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  It's as easy as it sounds, just enter a keyword…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  And let AI do its magic…
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0 order-md-1">
               <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 1px solid #15c6fc;">
                  <source src="assets/images/generate-course.mp4" type="video/mp4">
               </video>
            </div>
         </div>

         <div class="row align-items-center mt-md120">
            <div class="col-md-6 col-12">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
                  1-Click Course Details.
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Each course you create…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  AI will generate all the details, descriptions, tags for each one…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  This will make your life 10x easier…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  As it will do all the selling on your behalf.
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0">
               <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 1px solid #15c6fc;">
                  <source src="assets/images/course-details.mp4" type="video/mp4">
               </video>
            </div>
         </div>

         <div class="row align-items-center mt-md120">
            <div class="col-md-6 col-12 order-md-2">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
                  AI-ChatBots
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Forget all about manual customer support.
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Because now, you will have automated AI bots that will engage with your customers and offer them any kind of help they want…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Not only that, it will even upsell the customers for you.
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0 order-md-1">
               <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 1px solid #15c6fc;">
                  <source src="assets/images/ai-chatbot.mp4" type="video/mp4">
               </video>
            </div>
         </div>
         
         <div class="row align-items-center mt-md120">
            <div class="col-md-6 col-12 ">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
                  Generate Attention-Sucking Images
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Turn any keyword into a stunning graphic generated by AI… 
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  This alone will turn your website into a futuristic website with ease…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Never hire a graphic designer again. 
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0">
               <img src="assets/images/attention-sucking.webp" alt="Attention Sucking" class="mx-auto d-block img-fluid">
               <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted">
                  <source src="assets/images/attention-sucking.mp4" type="video/mp4">
               </video> 
            </div>
         </div>

      </div>
   </div> -->

   <!-- <div class="dfy-section">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-50 w700 text-center white-clr">
               DFY Themes
               </div>
               <div class="f-22 f-md-28 w400 text-center white-clr mt10">
               Choose from out stunning designs. Without writing any code… Or design anything… All of that is DFY on autopilot
               </div>
            </div>
            <div class="col-md-10 mx-auto mt20 mt-md50">
               <img src="assets/images/funnel-planner.webp" alt="DFY Themes" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <div class="capability-sec">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-md-6 col-12">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
                  100% Mobile Optimized Websites
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Each website you create is 100% mobile optimized
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  There are no glitches, no errors…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  AI will turn it into a fully functional mobile site in SECONDS…
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0">
               <img src="assets/images/optimized-websites.webp" alt="100% Mobile Optimized Websites" class="mx-auto d-block img-fluid ">
            </div>
         </div>

         <div class="row align-items-center mt-md120">
            <div class="col-md-6 col-12 order-md-2">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
                  Seamless AR Integration
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Easily build your targeted email list with NinjaAi AR integration…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Simply connect your autoresponder, and let NinjaAi do the rest…
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0 order-md-1">
               <img src="assets/images/ar-integration.webp" alt="Seamless AR Integration" class="mx-auto d-block img-fluid ">
            </div>
         </div> -->

         <!--<div class="row align-items-center mt-md120">-->
         <!--   <div class="col-md-6 col-12">-->
         <!--      <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">-->
         <!--         Built-In Appointment Booking-->
         <!--      </div>-->
         <!--      <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">-->
         <!--         Does your business requires setting appointments?-->
         <!--      </div>-->
         <!--      <div class="f-22 f-md-28 w700 text-center text-md-start orange-gradient mt20">-->
         <!--         Easy.-->
         <!--      </div>-->
         <!--      <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">-->
         <!--         Let NinjaAi does that for you on autopilot with its powerful appointment-setting features-->
         <!--      </div>-->
         <!--   </div>-->
         <!--   <div class="col-md-6 col-12 mt20 mt-md0">-->
         <!--      <img src="assets/images/appointment-booking.webp" alt="Appointment Booking" class="mx-auto d-block img-fluid ">-->
         <!--   </div>-->
         <!--</div>-->

         <!-- <div class="row align-items-center mt-md120">
            <div class="col-md-6 col-12">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
                  Built-In Social Media Integration
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  With one click, auto syndicate all your content into over 50+ social media platform…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  This will give you a surge of highly targeted traffic within seconds.
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0">
               <img src="assets/images/social-media-integration.webp" alt="Social Media Integration" class="mx-auto d-block img-fluid ">
            </div>
         </div>

         <div class="row align-items-center mt-md120">
            <div class="col-md-6 col-12 order-md-2">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
                  Commercial License Included
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Your copy of NinjaAi will come with a commercial license, so you can use it on your client website too
               </div>
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr mt20">
                  Valid only until the timer hits zero…
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0 order-md-1">
               <img src="assets/images/license.webp" alt=" Commercial License" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>
    -->

   <!-- CTA Section End -->

<!-- 
   <div class="limited-time-sec">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-20 f-md-24 w700 text-center white-clr align-items-center flex-column flex-md-row justify-content-center">
                  <img src="assets/images/caution-icon.webp" class="img-fluid mr-md5 mb5 mb-md0" alt="Caution Icon">
                  Your $2,475.34 Bonuses Package Expires When Timer Hits ZERO 
                  <img src="assets/images/caution-icon.webp" class="img-fluid ml-md5 mb5 mb-md0" alt="Caution Icon">
               </div>
            </div>
         </div>
      </div>
   </div>


   <div class="newbie-section">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-6">
               <div class="f-22 f-md-50 w400 lh140 mt20 black-clr text-center text-md-start">
                  Even If You Are A <br class="d-none d-md-block"> Complete Newbie…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Can you believe it?
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Even if you don't have ANY business right now…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               We still can make TONS of money using NinjaAi
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Simply by selling online courses
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Let NinjaAi do all the work…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               And we just collect money…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               As simple as that…
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/newbie-img.webp" alt="Newbie" class="mx-auto d-block img-fluid ">
            </div>
         </div>
         <div class="row mt20 mt-md95">
            <div class="col-12 text-center">
               <div class="f-28 f-md-50 w700 lh140 black-clr">
                  Ready To Transform Your Life?
               </div>
               <img src="assets/images/red-line.webp" alt="Red Line" class="mx-auto d-block img-fluid">
            </div>
         </div>
      </div>
   </div>

   <div class="hi-their-sec">
      <div class="container hi-their-box">
         <div class="row">
            <div class="col-12 text-center">
               <div class="hi-their-shadow">
                  <div class="f-28 f-md-45 lh140 black-clr caveat w700 hi-their-head">
                     Hey There,
                  </div>
               </div>
            </div>
         </div>
         <div class="row mt20 mt-md80">
            <div class="col-md-3 text-center">
               <div class="figure mr15 mr-md0">
                  <img src="assets/images/pranshu-gupta.webp" alt="Pranshu Gupta" class="mx-auto d-block img-fluid  img-shadow">
                  <div class="figure-caption mt10">
                     <div class="f-24 f-md-28 w700 lh140 black-clr caveat text-center">
                        Pranshu Gupta
                     </div>
                  </div>
               </div>
               <div class="figure mt20 mt-md30">
                  <img src="assets/images/bizomart-team.webp" alt="Bizomart" class="mx-auto d-block img-fluid  img-shadow">
                  <div class="figure-caption mt10">
                     <div class="f-24 f-md-28 w700 lh140 black-clr caveat text-center">
                        Bizomart
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-md-9 mt20 mt-md0">
               <div class="f-28 f-md-38 lh140 black-clr w400">
                  It's <span class="w600">Pranshu</span> along with <span class="w600">Bizomart.</span> 
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr mt20 mt-md30">
                  I'm a full time internet marketer…
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr mt20">
                  Over the years, I tried dozens (it could be hundreds even…) of methods to make money online…
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr mt20">
                  But the only thing that gave me the life I always wanted was…
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr mt20">
                  Creating websites…
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr mt20">
                  It's the easiest, and fastest way to build wealth…
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr mt20">
                  Not just make some pocket change…&nbsp;
               </div>
               <div class="f-20 f-md-22 lh140 w600 black-clr mt20">
                  Thanks to that…
               </div>
            </div>
         </div>

         <div class="row mt20 mt-md50">
            <div class="col-12">
               <div class="blue-border"></div>
            </div>
         </div>

         <div class="row mt20 mt-md50">
            <div class="col-12 text-center">
               <div class="f-22 f-md-28 lh140 black-clr w600">
                  I can spend my time as I please
               </div>
               <img src="assets/images/red-underline.webp" alt="Quotes" class="mx-auto d-block img-fluid  mt10">
            </div>
         </div>

         <div class="row mt20 mt-md50">
            <div class="col-md-3">
               <figure>
                  <img src="assets/images/family.webp" alt="Family" class="mx-auto d-block img-fluid ">
                  <figure-caption>
                     <div class="f-20 f-md-22 w600 lh140 black-clr mt10 text-center">
                        Spend it with my family
                     </div>
                  </figure-caption>
               </figure>
            </div>
            <div class="col-md-3">
               <figure>
                  <img src="assets/images/travel.webp" alt="Travel" class="mx-auto d-block img-fluid ">
                  <figure-caption>
                     <div class="f-20 f-md-22 w600 lh140 black-clr mt10 text-center">
                        Travel
                     </div>
                  </figure-caption>
               </figure>
            </div>
            <div class="col-md-3">
               <figure>
                  <img src="assets/images/sunset-beach.webp" alt="Enjoy a sunset on the beach" class="mx-auto d-block img-fluid ">
                  <figure-caption>
                     <div class="f-20 f-md-22 w600 lh140 black-clr mt10 text-center">
                        Enjoy a sunset on the beach
                     </div>
                  </figure-caption>
               </figure>
            </div>
            <div class="col-md-3">
               <figure>
                  <img src="assets/images/video-games.webp" alt="Or even play video games on my PS5" class="mx-auto d-block img-fluid ">
                  <figure-caption>
                     <div class="f-20 f-md-22 w600 lh140 black-clr mt10 text-center">
                        Or even play video games on my PS5
                     </div>
                  </figure-caption>
               </figure>
            </div>
         </div>

         <div class="row mt20 mt-md80">
            <div class="col-12 text-center">
               <div class="f-20 f-md-24 w600 lh140 black-clr">
                  And the best part is… No matter what I do with my day… I still make a lot of money…
               </div>
               <div class="f-20 f-md-24 w400 lh140 black-clr mt20">
                  And I'm not telling you this to brag… Not at all… I'm telling you this to demonstrate why you should even listen to me…
               </div>
            </div>
         </div>
      </div>
   </div>

   <div class="goal-section">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-28 f-md-50 w700 lh140 black-clr">
                  <img src="assets/images/blub.webp" class="img-fluid mr-md5 mb5 mb-md0" alt="blub">
                  I have One Goal This Year…
                  <img src="assets/images/blub.webp" class="img-fluid ml-md5 mb5 mb-md0" alt="blub">
               </div>
            </div>
         </div>
         <div class="row mt20 mt-md70 align-items-center">
            <div class="col-md-6">
               <div class="f-20 f-md22 w400 lh140 black-clr">
                  You see…
               </div>
               <div class="f-20 f-md22 w400 lh140 black-clr mt20">
                  Making money online is good any everything…&nbsp;
               </div>
               <div class="f-20 f-md22 w400 lh140 black-clr mt20">
                  But what's 10x better… Is helping someone else make it…
               </div>
               <div class="f-20 f-md22 w400 lh140 black-clr mt20">
                  This is why I decided that in 2023 <span class="w600">I will help 100 new people…</span> 
               </div>
               <div class="f-20 f-md22 w400 lh140 black-clr mt20">
                  Quit their job, and have the financial freedom they always wanted…
               </div>
               <div class="f-20 f-md22 w400 lh140 black-clr mt20">
                  So far, it's been going GREAT…
               </div>
               <div class="f-20 f-md22 w400 lh140 black-clr mt20">
                  I've managed to help countless people…
               </div>
            </div>
            <div class="col-md-6 mt20 mt-md0">
               <img src="assets/images/goal.webp" alt="Women" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <div class="success-story-section">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-28 f-md-50 w700 lh140 black-clr">
                  And I'm Here To Help You My <br class="d-none d-md-block">
                  Next Success Story…
               </div>
            </div>
         </div>
         <div class="row mt20 mt-md70 align-items-center">
            <div class="col-md-6 order-md-2">
               <div class="f-20 f-md22 w400 lh140 black-clr">
                  Listen, if you're reading this page right now…
               </div>
               <div class="f-20 f-md22 w400 lh140 black-clr mt20">
                  It means one thing, you're dedicated to making money online…
               </div>
               <div class="f-20 f-md22 w600 lh140 black-clr mt20">
                  And I have some good news for you…
               </div>
               <div class="f-20 f-md22 w400 lh140 black-clr mt20">
                  On this page, I will give you everything you need…
               </div>
               <div class="f-20 f-md22 w400 lh140 black-clr mt20">
                  To get started and join me and thousands of other profitable members…&nbsp;
               </div>
               <div class="f-20 f-md22 w400 lh140 black-clr mt20">
                  Who changed their lives forever…
               </div>
            </div>
            <div class="col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/successful-man.webp" alt="SuccessFull Man" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <div class="demo-sec">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-28 f-md-50 w700 black-clr lh140">
                  <img src="assets/images/cool.webp" class="img-fluid mr-md5 mb5 mb-md0" alt="Cool">
                  Let Me Show You Something Cool…
                  <img src="assets/images/takealook-brush.webp" alt="Quotes" class="mx-auto d-block img-fluid ">
               </div>
            </div>
            <div class="col-12 mt20 mt-md80">
               <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 1px solid #15c6fc;">
                  <source src="assets/images/cool.mp4" type="video/mp4">
               </video>
            </div>
            <div class="col-12 mt20 mt-md50 text-center">
               <div class="f-22 f-md-28 w600 lh140 black-clr">
                  You see this endless stream of payments?
               </div>
               <div class="f-22 f-md-28 w400 lh140 black-clr mt10">
                  We got all of those doing only one thing…
               </div>
               <div class="smile-text f-22 f-md-28 w600 lh140 white-clr mt20 mt-md50">Using NinjaAi</div>
               <div class="f-22 f-md-28 w400 lh140 black-clr mt20 mt-md49">
                  We turn it on, and that's pretty much it… As simple as that…
               </div>
               <div class="f-22 f-md-28 w400 lh140 black-clr mt20">
                  This month alone…
               </div>
               <div class="f-28 f-md-38 w400 lh140 black-clr mt20 mt-md30">
                  We made <span class="red-clr w600 affliliate-underline">  $24,124.65 on one of our affiliate accounts…</span>  
               </div>
               <img src="assets/images/arrow-down-3.webp" alt="Demo Proof" class="d-block img-fluid  mt20 ">
               <img src="assets/images/demo-proof.webp" alt="Demo Proof" class="mx-auto d-block img-fluid  ">
               <div class="f-28 f-md-38 w400 lh140 black-clr mt20 mt-md30">
               And I'm gonna show you exactly how I do it…
               </div>
            </div>
         </div>
      </div>
   </div>
       -->
   

      <!-- <div class="capability-sec" id="features">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-45 w500 lh140 text-center black-clr">
                  Take A Look At What <span class="w700 orange-line"> NinjaAi Can Do For You </span>
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md90">
            <div class="col-md-6 col-12">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
                  Build A Udemy-Like Website  
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  With just one click, you will have a DFY Udemy-like website that well sell any course you want on your behalf…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Without you doing any of the work…
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0">
               <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 1px solid #15c6fc;">
                  <source src="assets/images/udemy-website.mp4" type="video/mp4">
               </video>
            </div>
         </div>

         <div class="row align-items-center mt-md120">
            <div class="col-md-6 col-12 order-md-2">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
                  Generate AI Courses Easily
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Sell high-quality courses that you did NOT create…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  But rather, AI created…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  It's as easy as it sounds, just enter a keyword…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  And let AI do its magic…
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0 order-md-1">
               <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 1px solid #15c6fc;">
                  <source src="assets/images/generate-course.mp4" type="video/mp4">
               </video>
            </div>
         </div>

         <div class="row align-items-center mt-md120">
            <div class="col-md-6 col-12">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
                  1-Click Course Details.
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Each course you create…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  AI will generate all the details, descriptions, tags for each one…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  This will make your life 10x easier…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  As it will do all the selling on your behalf.
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0">
               <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 1px solid #15c6fc;">
                  <source src="assets/images/course-details.mp4" type="video/mp4">
               </video>
            </div>
         </div>

         <div class="row align-items-center mt-md120">
            <div class="col-md-6 col-12 order-md-2">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
                  AI-ChatBots
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Forget all about manual customer support.
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Because now, you will have automated AI bots that will engage with your customers and offer them any kind of help they want…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Not only that, it will even upsell the customers for you.
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0 order-md-1">
               <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 1px solid #15c6fc;">
                  <source src="assets/images/ai-chatbot.mp4" type="video/mp4">
               </video>
            </div>
         </div>
         
         <div class="row align-items-center mt-md120">
            <div class="col-md-6 col-12 ">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
                  Generate Attention-Sucking Images
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Turn any keyword into a stunning graphic generated by AI… 
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  This alone will turn your website into a futuristic website with ease…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Never hire a graphic designer again. 
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0 ">
               <img src="assets/images/attention-sucking.webp" alt="Attention Sucking" class="mx-auto d-block img-fluid">
               <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted">
                  <source src="assets/images/attention-sucking.mp4" type="video/mp4">
               </video> 
            </div>
         </div>

      </div>
   </div> -->
<!-- 
   <div class="dfy-section">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-50 w700 text-center white-clr">
               DFY Themes
               </div>
               <div class="f-22 f-md-28 w400 text-center white-clr mt10">
               Choose from out stunning designs. Without writing any code… Or design anything… All of that is DFY on autopilot
               </div>
            </div>
            <div class="col-md-10 mx-auto mt20 mt-md50">
               <img src="assets/images/funnel-planner.webp" alt="DFY Themes" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <div class="capability-sec">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-md-6 col-12">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
                  100% Mobile Optimized Websites
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Each website you create is 100% mobile optimized
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  There are no glitches, no errors…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  AI will turn it into a fully functional mobile site in SECONDS…
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0">
               <img src="assets/images/optimized-websites.webp" alt="100% Mobile Optimized Websites" class="mx-auto d-block img-fluid ">
            </div>
         </div>

         <div class="row align-items-center mt-md120">
            <div class="col-md-6 col-12 order-md-2">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
                  Seamless AR Integration
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Easily build your targeted email list with NinjaAi AR integration…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Simply connect your autoresponder, and let NinjaAi do the rest…
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0 order-md-1">
               <img src="assets/images/ar-integration.webp" alt="Seamless AR Integration" class="mx-auto d-block img-fluid ">
            </div>
         </div> -->

         <!--<div class="row align-items-center mt-md120">-->
         <!--   <div class="col-md-6 col-12">-->
         <!--      <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">-->
         <!--         Built-In Appointment Booking-->
         <!--      </div>-->
         <!--      <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">-->
         <!--         Does your business requires setting appointments?-->
         <!--      </div>-->
         <!--      <div class="f-22 f-md-28 w700 text-center text-md-start orange-gradient mt20">-->
         <!--         Easy.-->
         <!--      </div>-->
         <!--      <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">-->
         <!--         Let NinjaAi does that for you on autopilot with its powerful appointment-setting features-->
         <!--      </div>-->
         <!--   </div>-->
         <!--   <div class="col-md-6 col-12 mt20 mt-md0">-->
         <!--      <img src="assets/images/appointment-booking.webp" alt="Appointment Booking" class="mx-auto d-block img-fluid ">-->
         <!--   </div>-->
         <!--</div>-->

         <!-- <div class="row align-items-center mt-md120">
            <div class="col-md-6 col-12">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
                  Built-In Social Media Integration
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  With one click, auto syndicate all your content into over 50+ social media platform…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  This will give you a surge of highly targeted traffic within seconds.
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0">
               <img src="assets/images/social-media-integration.webp" alt="Social Media Integration" class="mx-auto d-block img-fluid ">
            </div>
         </div>

         <div class="row align-items-center mt-md120">
            <div class="col-md-6 col-12 order-md-2">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
                  Commercial License Included
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Your copy of NinjaAi will come with a commercial license, so you can use it on your client website too
               </div>
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr mt20">
                  Valid only until the timer hits zero…
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0 order-md-1">
               <img src="assets/images/license.webp" alt=" Commercial License" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>
   
    <div class="in-action-section" id="demo">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="f-28 f-md-50 w700 lh140 white-clr text-center">
                        Watch NinjaAi In Action, And See What It Can Do For You…
                    </div>
                    <div class="mt20 mt-md50">
                        <img src="assets/images/blue-arrow.webp" alt="Product Box" class="mx-auto d-block img-fluid ">
                    </div>
                </div>
                <div class="col-md-10 col-12 mx-auto mt20 mt-md50">
                    <div class="col-12">
                        <div class="video-box">
                           <div style="padding-bottom: 56.25%;position: relative;"><iframe src="https://aicademy.oppyo.com/video/embed/x6xnjhlvq3" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div>
                        </div>      
                    </div>
                </div>
            </div>
        </div>
        <div class="container mt20 mt-md30">
            <div class="row">
                <div class="col-12">
                    <div class="f-28 f-md-38 w700 lh140 text-center white-clr">
                        Get NinjaAi And Save $293 Now <br class="d-none d-md-block"> 
                    </div>
                    <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                        <span class="w700">Just Pay Once</span> Instead Of a <s>Monthly Fee</s>
                    </div>
                </div>
            </div>
            <div class="row align-items-center mt20 mt-md50">
                <div class="col-md-7">
                    <a href="#buynow" class="cta-link-btn">>>> Get Instant Access To NinjaAi <<<</a>
                    <img src="assets/images/compaitable-gurantee1.webp" alt="Compaitable Gurantee" class="mx-auto d-block img-fluid mt20 mt-md30">
                </div>
                <div class="col-md-5 mt20 mt-md0">
                    <div class="countdown-container">
                        <div class="f-20 f-md-24 w400 lh140 text-center white-clr mb10">
                            <span class="w700 orange-clr">Hurry up!</span>  Price Increases In
                        </div>
                        <div class="countdown counter-white text-center">
                            <div class="timer-label text-center">
                                <span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 smmltd">Days</span> 
                            </div>
                            <div class="timer-label text-center">
                                <span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                            </div>
                            <div class="timer-label text-center timer-mrgn">
                                <span class="f-31 f-md-45 timerbg oswald">46&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                            </div>
                            <div class="timer-label text-center ">
                                <span class="f-31 f-md-45 timerbg oswald">28</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> -->

   <!-- CTA Section Start -->
   <!--<div class="cta-section-white">-->
   <!--   <div class="container">-->
   <!--      <div class="row">-->
   <!--         <div class="col-12">-->
   <!--            <div class="f-28 f-md-38 w700 lh140 text-center white-clr">-->
   <!--               Get NinjaAi And Save $293 Now <br class="d-none d-md-block"> -->
   <!--            </div>-->
   <!--            <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">-->
   <!--               <span class="w700">Just Pay Once</span> Instead Of a <s>Monthly Fee</s>-->
   <!--            </div>-->
   <!--         </div>-->
   <!--      </div>-->
   <!--      <div class="row align-items-center mt20 mt-md50">-->
   <!--         <div class="col-md-7">-->
   <!--            <a href="#buynow" class="cta-link-btn">>>> Get Instant Access To NinjaAi <<<</a>-->
   <!--            <img src="assets/images/compaitable-gurantee1.webp" alt="Compaitable Gurantee" class="mx-auto d-block img-fluid mt20 mt-md30">-->
   <!--         </div>-->
   <!--         <div class="col-md-5 mt20 mt-md0">-->
   <!--            <div class="countdown-container">-->
   <!--               <div class="f-20 f-md-24 w400 lh140 text-center white-clr mb10">-->
   <!--                  <span class="w700 orange-clr">Hurry up!</span>  Price Increases In-->
   <!--               </div>-->
   <!--               <div class="countdown counter-white text-center">-->
   <!--                  <div class="timer-label text-center">-->
   <!--                     <span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 smmltd">Days</span> -->
   <!--                  </div>-->
   <!--                  <div class="timer-label text-center">-->
   <!--                     <span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> -->
   <!--                  </div>-->
   <!--                  <div class="timer-label text-center timer-mrgn">-->
   <!--                     <span class="f-31 f-md-45 timerbg oswald">46&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> -->
   <!--                  </div>-->
   <!--                  <div class="timer-label text-center ">-->
   <!--                     <span class="f-31 f-md-45 timerbg oswald">28</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> -->
   <!--                  </div>-->
   <!--               </div>-->
   <!--            </div>-->
   <!--         </div>-->
   <!--      </div>-->
   <!--   </div>-->
   <!--</div>-->
   <!-- CTA Section End -->

   <!--Limited Section Start -->
   <!--<div class="limited-time-sec">-->
   <!--   <div class="container">-->
   <!--      <div class="row">-->
   <!--         <div class="col-12 text-center">-->
   <!--            <div class="f-20 f-md-24 w700 text-center white-clr align-items-center flex-column flex-md-row justify-content-center">-->
   <!--               <img src="assets/images/caution-icon.webp" class="img-fluid mr-md5 mb5 mb-md0" alt="Caution Icon">-->
   <!--               Your $2,475.34 Bonuses Package Expires When Timer Hits ZERO -->
   <!--               <img src="assets/images/caution-icon.webp" class="img-fluid ml-md5 mb5 mb-md0" alt="Caution Icon">-->
   <!--            </div>-->
   <!--         </div>-->
   <!--      </div>-->
   <!--   </div>-->
   <!--</div>-->
   <!--Limited Section End -->

<!-- 
   <div class="white-section">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-6">
               <div class="f-22 f-md-45 w400 lh140 mt20 black-clr text-center text-md-start">
                  Finally… <br class="d-none d-md-block"> Dominate The E-Learning Niche
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
                  With AiCademy…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
                  It never been easier to dominate this niche…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
                  We literally made it fail-proof…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
                  There is no way for you to fail this. Even if you tried to…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
                  We give you the AI model…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
                  That made it all possible for us.  
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/e-learning.webp" alt="E-Leaning" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <div class="grey-section">
      <div class="container">
         <div class="row">
            <div class="col-12">
            <div class="f-28 f-md-45 w700 lh140 mt20 black-clr text-center">
               It Doesn't Matter What Experience You Have…
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md30">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Look, it's very easy and simple…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               This is the best side income you can have…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               You can continue doing your job or business as usual if you want…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               And just use NinjaAi to make a few thousands a month extra…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Or you can go all in…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               And use it to make fulltime income like us…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               And enjoy life…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               As long as you are trying to make money online…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               NinjaAi is for you…
               </div>
               <div class="f-18 f-md-22 w600 lh140 black-clr text-center text-md-start mt20">
               It work for…
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/matter.webp" alt="Matter" class="mx-auto d-block img-fluid ">
            </div>
         </div>
        <img src="assets/images/blue-arrow.webp" alt="arrow" class="mx-auto d-block img-fluid mt20 mt-md50">
         <div class="regardless-sec mt20 mt-md50">
            <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3">
               <div class="col">
                  <div class="figure">
                     <img src="assets/images/product-creator.webp" alt="Product Creators" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption">
                        <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                        Product Creators
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="figure">
                     <img src="assets/images/affiliate-marketers.webp" alt="Affiliate Marketers" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption">
                        <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                           Affiliate Marketers
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="figure">
                     <img src="assets/images/ecom-store-owners.webp" alt="eCom Store Owners" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption">
                        <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                           eCom Store Owners
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="figure">
                     <img src="assets/images/blog-owners.webp" alt="Blog Owners" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption">
                        <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                           Blog Owners
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="figure">
                     <img src="assets/images/marketers.webp" alt="CPA Marketers" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption">
                        <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                           CPA Marketers
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="figure">
                     <img src="assets/images/video-marketers.webp" alt="Video Marketers" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption">
                        <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                           Video Marketers
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="figure">
                     <img src="assets/images/content-creators.webp" alt="Artists/Content Creators" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption">
                        <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                           Artists/Content Creators
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="figure">
                     <img src="assets/images/personal-brands.webp" alt="Personal Brands" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption">
                        <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                           Personal Brands
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="figure">
                     <img src="assets/images/freelancers.webp" alt="Freelancers" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption">
                        <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                           Freelancers
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>

      </div>
   </div>

   <div class="white-section">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-6">
               <div class="f-28 f-md-50 w700 lh140 text-center text-md-start black-clr">
               There Is Nothing For You To Do…<span class="red-clr"><u> EXCEPT</u></span>
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               We did all the work for you…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               There is nothing for you to do literally…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               AI will take over, and create your website…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               AND sell it for you…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               The only thing you would need to do is…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Keep refreshing your account and see how much you've made…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Other than that…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               You're all set.
               </div>
               <ul class="cross-list pl0 m0 f-18 f-md-22 lh160 w600 black-clr">
                  <li>No Website Creation</li>
                  <li>No Programming Required</li>
                  <li>No Complicated Setup</li>
                  <li>No Hiring Anyone</li>
               </ul>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/no-setup.webp" alt="Nothing Except" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <div class="grey-section">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-28 f-md-50 w700 lh140 text-center text-md-start black-clr">
                  Allow ChatGPT To Run Your Business…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               We created the best AI model that integrate with ChatGPT…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               That will run everything for us…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Its the same model that we used on a brand new account and took it from this…
               </div>
               <div class="f-20 f-md-22 w600 lh140 red-clr text-center text-md-start mt20">
               [$0 bank account]
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr text-center text-md-start mt20 arrow-down">
               To this
               </div>
               <img src="assets/images/proof.webp" alt="Proof SS" class="mx-auto d-block img-fluid mt20 mt-md24 ">
               <div class="f-20 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               In just a few days…
               </div>
               <div class="text-center text-md-start">
                  <div class="smile-text f-20 f-md-22 w400 lh140 white-clr mt20 mt-md24">Sounds good, huh?</div>
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/chatgpt.webp" alt="ChatGPT" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>


   <div class="white-section"> 
      <div class="container">
         <div class="row">
            <div class="col-12 col-md-6">
               <div class="f-28 f-md-50 w700 lh140 text-center text-md-start black-clr">
                 <span class="red-clr">How</span>  Does It Work?
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               It's very easy actually…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               NinjaAi will create you a udemy-like website…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               And then generate AI courses for you in any niche you want…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               Giving you a stunning and fully functional courses website…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               Then it will start promoting it on your behalf  too…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               You don't have to run ads or do anything…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               It will do  that for you.
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               And with one click it will integrate with your favoritet payment processor and autoresponder
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               Now you have a fully functional courses website…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               That will start bringing in customers and sales <span class="w600">every single day…</span> 
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               Best part is, all it takes is just 30 seconds. If not less.
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/work-men.webp" alt="Work Men" class="d-block mx-auto img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <div class="grey-section">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-28 f-md-50 w700 lh140 text-center text-md-start black-clr">
                 <span class="red-clr">How</span> Is It Different?
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                 NinjaAi is the only app that does everything…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               It doesn't matter if  you  have experience or not…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               There is nothing technical that you need to do whatsoever….
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               Not even a line of code…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               And you don't need to design anything either…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               All you need is just give your website a name…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               And that pretty much it
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               Removing all the guesswork, and hard work from your end… 
               </div>
            </div>
            <div class="col-12 col-md-6 order-md-1 mt20 mt-md0">
               <img src="assets/images/different-girl.webp" alt="Different" class="d-block mx-auto img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <div class="white-section">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-24 f-md-38 w400 text-center black-clr dotted-border">
               With NinjaAi
               </div>
               <div class="f-28 f-md-50 w700 lh140 mt11 black-clr">
               Everything Is Done For You By The Most <br class="d-none d-md-block">Powerful AI Model Ever Created…
               </div>
            </div>
         </div>
         <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 gap30 mt20 mt-md10">
            <div class="col">
               <div class="figure">
                  <img src="assets/images/dfy-websites.webp" alt="Udemy-Like Websites" class="mx-auto d-block img-fluid ">
                  <div class="figure-caption text-center">
                     <div class="f-22 f-md-28 w400 lh140 mt20 black-clr">Udemy-Like Websites</div>
                  </div>
               </div>
            </div>

            <div class="col">
               <div class="figure">
                  <img src="assets/images/dfy-designs.webp" alt="DFY Design" class="mx-auto d-block img-fluid ">
                  <div class="figure-caption text-center">
                     <div class="f-22 f-md-28 w400 lh140 mt20 black-clr">DFY Design</div>
                  </div>
               </div>
            </div>

            <div class="col">
               <div class="figure">
                  <img src="assets/images/dfy-content.webp" alt="DFY Courses" class="mx-auto d-block img-fluid ">
                  <div class="figure-caption text-center">
                     <div class="f-22 f-md-28 w400 lh140 mt20 black-clr">DFY Courses</div>
                  </div>
               </div>
            </div>

            <div class="col">
               <div class="figure">
                  <img src="assets/images/dfy-training.webp" alt="DFY Training" class="mx-auto d-block img-fluid ">
                  <div class="figure-caption text-center">
                     <div class="f-22 f-md-28 w400 lh140 mt20 black-clr">DFY Training </div>
                  </div>
               </div>
            </div>

            <div class="col">
               <div class="figure">
                  <img src="assets/images/dfy-traffic.webp" alt="DFY Traffic" class="mx-auto d-block img-fluid ">
                  <div class="figure-caption text-center">
                     <div class="f-22 f-md-28 w400 lh140 mt20 black-clr">DFY Traffic</div>
                  </div>
               </div>
            </div>

            <div class="col">
               <div class="figure">
                  <img src="assets/images/dfy-mobile-optimization.webp" alt="DFY Mobile Optimization" class="mx-auto d-block img-fluid ">
                  <div class="figure-caption text-center">
                     <div class="f-22 f-md-28 w400 lh140 mt20 black-clr">DFY Mobile Optimization</div>
                  </div>
               </div>
            </div>

            <div class="col">
               <div class="figure">
                  <img src="assets/images/dfy-chatbot.webp" alt="DFY AI Chatbots" class="mx-auto d-block img-fluid ">
                  <div class="figure-caption text-center">
                     <div class="f-22 f-md-28 w400 lh140 mt20 black-clr">DFY AI Chatbots</div>
                  </div>
               </div>
            </div>

            <div class="col">
               <div class="figure">
                  <img src="assets/images/dfy-woocomerce-integration.webp" alt="DFY Integration" class="mx-auto d-block img-fluid ">
                  <div class="figure-caption text-center">
                     <div class="f-22 f-md-28 w400 lh140 mt20 black-clr">DFY Integration</div>
                  </div>
               </div>
            </div>
            
            <div class="col">
               <div class="figure">
                  <img src="assets/images/dfy-templates.webp" alt="DFY Templates" class="mx-auto d-block img-fluid ">
                  <div class="figure-caption text-center">
                     <div class="f-22 f-md-28 w400 lh140 mt20 black-clr">DFY Templates</div>
                  </div>
               </div>
            </div>

         </div>
      </div>
   </div> -->

   


   <!-- <section class="white-section">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-28 f-md-50 w700 lh140 black-clr text-center">
                 <span class="left-line">Everything You Need</span> Is Included…
               </div>
            </div>
         </div>
         <div class="row mt20 mt-md80 align-items-center">
            <div class="col-12 col-md-6">
               <div class="f-28 f-md-38 lh140 w700 text-center white-clr need-head">
                  NinjaAi App
               </div>
               <div class="f-20 f-md-22 w400 lh140 mt20 black-clr">
                  The app that is responsible for everything…
                  Start Your E-Learning Empire Today…
               </div>
               <div class="f-20 f-md-22 w700 lh140 mt10 red-clr">
                  (Worth $997/mo)
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/webgenie-app.webp" alt="NinjaAi App" class="mx-auto d-block img-fluid ">
            </div>
         </div>
         <div class="row mt20 mt-md80">
            <div class="col-12">
               <div class="border-line"></div>
            </div>
         </div>
         <div class="row mt20 mt-md80 align-items-center">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-28 f-md-38 lh140 w700 text-center white-clr need-head">
                  AI Course Generator
               </div>
               <div class="f-20 f-md-22 w400 lh140 mt20 black-clr">
                  With one click, Fill your new website with hundreds of AI courses in any niche you want. Without you doing any of the work
               </div>
               <div class="f-20 f-md-22 w700 lh140 mt10 red-clr">
                  (Worth $997)
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/ai-content-generator.webp" alt="AI Content Generator" class="mx-auto d-block img-fluid ">
            </div>
         </div>
         <div class="row mt20 mt-md80">
            <div class="col-12">
               <div class="border-line"></div>
            </div>
         </div>
         <div class="row mt20 mt-md80 align-items-center">
            <div class="col-12 col-md-6">
               <div class="f-28 f-md-38 lh140 w700 text-center white-clr need-head">
                  NinjaAi Built-In Traffic
               </div>
               <div class="f-20 f-md-22 w400 lh140 mt20 black-clr">
                  You don't have to worry about traffic any more, let AI handle that for you
               </div>
               <div class="f-20 f-md-22 w700 lh140 mt10 red-clr">
                  (Worth $997)
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/webgenie-built-in-traffic.webp" alt="NinjaAi App" class="mx-auto d-block img-fluid ">
            </div>
         </div>
         <div class="row mt20 mt-md80">
            <div class="col-12">
               <div class="border-line"></div>
            </div>
         </div>
         <div class="row mt20 mt-md80 align-items-center">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-28 f-md-38 lh140 w700 text-center white-clr need-head">
               NinjaAi Mobile Optimizer
               </div>
               <div class="f-20 f-md-22 w400 lh140 mt20 black-clr">
                  No need to optimize anything yourself.
               </div>
               <div class="f-20 f-md-22 w400 lh140 mt20 black-clr">
               Let AI take care of that too.
               </div>
               <div class="f-20 f-md-22 w700 lh140 mt10 red-clr">
                  (Worth $997)
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/webgenie-mobile-optimizer.webp" alt="NinjaAi Mobile Optimizer" class="mx-auto d-block img-fluid ">
            </div>
         </div>
         <div class="row mt20 mt-md80">
            <div class="col-12">
               <div class="border-line"></div>
            </div>
         </div>
         <div class="row mt20 mt-md80 align-items-center">
            <div class="col-12 col-md-6">
               <div class="f-28 f-md-38 lh140 w700 text-center white-clr need-head">
               NinjaAi Mobile EDITION
               </div>
               <div class="f-20 f-md-22 w400 lh140 mt20 black-clr">
               This will allow you to also operate NinjaAi, even from your mobile phone…<br>
               Whether it's an Android, iPhone, or tablet, it will work…
               </div>
               <div class="f-20 f-md-22 w700 lh140 mt10 red-clr">
                  (Worth $497)
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/webgenie-mobile-edition.webp" alt="NinjaAi Mobile EDITION" class="mx-auto d-block img-fluid ">
            </div>
         </div>
         <div class="row mt20 mt-md80">
            <div class="col-12">
               <div class="border-line"></div>
            </div>
         </div>
         <div class="row mt20 mt-md80 align-items-center">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-28 f-md-38 lh140 w700 text-center white-clr need-head">
               Training videos
               </div>
               <div class="f-20 f-md-22 w400 lh140 mt20 black-clr">
               There is NOTHING missing in this training… <br>
               Everything you need to know is explained in IMMENSE details
               </div>
               <div class="f-20 f-md-22 w700 lh140 mt10 red-clr">
                  (Worth $997)
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/training-videos.webp" alt="Training videos" class="mx-auto d-block img-fluid ">
            </div>
         </div>
         <div class="row mt20 mt-md80">
            <div class="col-12">
               <div class="border-line"></div>
            </div>
         </div>
         <div class="row mt20 mt-md80 align-items-center">
            <div class="col-12 col-md-6">
               <div class="f-28 f-md-38 lh140 w700 text-center white-clr need-head">
               World-class support
               </div>
               <div class="f-20 f-md-22 w400 lh140 mt20 black-clr">
               Have a question? Just reach out to us and our team will do their best to fix your problem in no time
               </div>
               <div class="f-20 f-md-22 w700 lh140 mt10 red-clr">
               (Worth A LOT)
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/world-class-support.webp" alt="World-class support" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </section>
 -->

     
   <!-- <div class="white-section">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-28 f-md-50 w700 lh140 text-center text-md-start black-clr red-line">
               But There Is More…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20 mt-md30">
               By getting access to NinjaAi
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  You will immediately unlock access to our custom-made bonuses…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               We created this pack exclusively for NinjaAi
               </div>
               <div class="f-20 f-md-22 w600 lh140 text-center text-md-start black-clr mt20">
                  Our goal was simple…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  Give fast action takers the advantage…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               By giving them whatever they need to achieve 10x the results…
               In half the time…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               Here is exactly what you will get:
               </div>
            </div>
            <div class="col-12 col-md-6 order-md-1 mt20 mt-md0">
               <img src="assets/images/better-man.webp" alt="Better Man" class="mx-auto d-block img-fluid">
            </div>
         </div>
      </div>
   </div>-->

   <div class="grey-section">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-head f-28 f-md-48 w600 lh140 text-center white-clr">
                  But That's Not All
               </div>
               <div class="f-20 f-md-22 w500 lh140 black-clr mt20">
                  In addition, we have several bonuses for those who want to take action <br class="d-none d-md-block">
                  today and start profiting from this opportunity.
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md80">
            <div class="col-12 col-md-6">
               <div class="">
                  <img src="assets/images/bonus1.webp" alt="Bonus 1" class="img-fluid d-block ">
                  <div class="mt20 f-20 f-md-24 w700 lh140 black-clr">
                  Selecting a Domain Name for Your Success
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 black-clr mt15">
                  Choose the best domain name for your business keeping in mind various criteria like SEO optimization, targeted niche, business profile etc. with this exclusive bonus. Now use the power of creating UNLIMITED subdomains with Ninja AI, and scale your business to the next level.
                  </div>
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/bonus.webp" alt="Bonus Box" class="mx-auto d-block img-fluid ">
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md80">
            <div class="col-12 col-md-6 order-md-2">
               <div class="">
                  <img src="assets/images/bonus2.webp" alt="Bonus 2" class="img-fluid d-block ">
                  <div class="mt20 f-20 f-md-24 w700 lh140 black-clr">
                  Inbound Marketing Development And Strategy
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 black-clr mt15">
                  Get the best results for your business using inbound marketing strategies used by pro marketers? Get blog posts, forum posts, keyword analysis and almost everything else you need. When used with NinjaAI, it gives scalable results in the long run.
                  </div>
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/bonus.webp" alt="Bonus Box" class="mx-auto d-block img-fluid ">
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md80">
            <div class="col-12 col-md-6">
               <div class="">
                  <img src="assets/images/bonus3.webp" alt="Bonus 3" class="img-fluid d-block ">
                  <div class="mt20 f-20 f-md-24 w700 lh140 black-clr">
                  Split Testing Development and Strategy
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 black-clr mt15">
                  Always a good idea to split test your marketing campaigns rather than investing in 1 thing and repenting later. So, when this useful bonus is used with UNLMITED A/B testing capacity of Ninja AI, and be on the right track for success.
                  </div>
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/bonus.webp" alt="Bonus Box" class="mx-auto d-block img-fluid ">
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md80">
            <div class="col-12 col-md-6 order-md-2">
               <div class="">
                  <img src="assets/images/bonus4.webp" alt="Bonus 4" class="img-fluid d-block ">
                  <div class="mt20 f-20 f-md-24 w700 lh140 black-clr">
                  Sales Funnel Mastery
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 black-clr mt15">
                  Unlock the secrets to creating highly optimized sales funnels that convert leads into loyal customers. Maximize sales and growth with this comprehensive, step-by-step training.
                  </div>
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/bonus.webp" alt="Bonus Box" class="mx-auto d-block img-fluid ">
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md80">
            <div class="col-12 col-md-6">
               <div class="">
                  <img src="assets/images/bonus5.png" alt="Bonus 3" class="img-fluid d-block ">
                  <div class="mt20 f-20 f-md-24 w700 lh140 black-clr">
                  Lead Generation On Demand
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 black-clr mt15">
                  It doesn't matter what kind of business you're in, if you aren't able to generate new leads and turn them into paying customers, your company will never succeed.
                  <br><br>
                  So, to help you to build your leads and make the max from them, this comprehensive guide lays down proven tips and tricks on how you can create lead generation on demand.
                  </div>
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/bonus.webp" alt="Bonus Box" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <div class="white-section">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-50 w700 lh140 text-center black-clr">
               Eliminate All The Guesswork With Ninja AI
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md50">
            <div class="col-12 col-md-6">
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20 mt-md30">
               Aren't you tired of wasting your money and time……on half-complete apps?
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               If you are…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               Then this is your chance to finally jump on something that has been proven to work…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               Not just for us…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               But for HUNDREDS of beta-testers…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               With %100 success rate…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               We're confident to say that Ninja AI is the BEST app on the market right now…
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/question-girl.png" alt="Future Start" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <div class="grey-section">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-50 w700 lh140 text-center black-clr">
               Wanna Know <span class="orange-clr"> How Much It Costs? </span>
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md80">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr">
               Look, I'm gonna be honest with you…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               We can charge anything we want for Ninja AI
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               Even $997 a month…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               And it will be a no-brainer…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               After all, our beta testers make so much more than that…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               And on top of that, there is no work for you to do.
               </div>
               <div class="f-20 f-md-22 w600 lh140 text-center text-md-start black-clr mt20">
               NinjaAI does it all…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               But I make enough money from using Ninja AI
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               So I don't need to charge you that
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               I use it on a daily basis…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               So I don't see why not I shouldn't give you access to it…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               So, for a limited time only…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               I'm willing to give you full access to Ninja AI
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               For a fraction of the price
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               Less than the price of a cheap dinner
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               Which is enough for me to cover the cost of the servers running Ninja AI
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/cost.png" alt="Costs" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <div class="quick-section">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-6 ">
               <div class="f-28 f-md-50 w700 lh140 text-center text-md-start white-clr">
               You Have To Be Quick Tho…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start white-clr mt20 mt-md30">
               The last thing on earth I want is to get Ninja AI saturated…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start white-clr mt20">
               So, sadly I'll have to limit the number of licenses I can give…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start white-clr mt20">
               After that, I'm raising the price to <span class="w600 orange-clr">$97 monthly.</span> 
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start white-clr mt20">
               And trust me, that day will come very…VERY SOON.
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start white-clr mt20">
               So you better act quickly and grab your copy while you can.
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/quick-img.png" alt="Money Scale" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div> 
   <div class="cta-section-white">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-38 w700 lh140 text-center white-clr">
                  Get <span class="step-shapes"> NinjaAi </span> For Low One Time Fee… <br class="d-none d-md-block"> 
               </div>
               <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                  <span class="w700">Just Pay Once</span> Instead Of <s class="red-clr">Monthly</s>
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md50">
            <div class="col-md-7">
               <a href="#buynow" class="cta-link-btn">&gt;&gt;&gt; Get Instant Access To NinjaAi &lt;&lt;&lt;</a>
               <img src="assets/images/compaitable-gurantee1.webp" alt="Compaitable Gurantee" class="mx-auto d-block img-fluid mt20 mt-md30">
            </div>
            <div class="col-md-5 mt20 mt-md0">
               <div class="countdown-container">
                  <div class="f-20 f-md-24 w400 lh140 text-center white-clr mb10">
                     <span class="w700 red-clr">Hurry up!</span>  Price Increases In
                  </div>
                  <div class="countdown counter-white text-center"><div class="timer-label text-center"><span class="f-24 f-md-32 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-14 w500 smmltd">Days</span> </div><div class="timer-label text-center"><span class="f-24 f-md-32 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-14 w500 smmltd">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-24 f-md-32 timerbg oswald">34&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-14 w500 smmltd">Mins</span> </div><div class="timer-label text-center "><span class="f-24 f-md-32 timerbg oswald">41</span><br><span class="f-14 f-md-14 w500 smmltd">Sec</span> </div></div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="limited-time-sec">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-20 f-md-24 w700 text-center white-clr align-items-center flex-column flex-md-row justify-content-center">
                  <img src="assets/images/caution-icon.webp" class="img-fluid mr-md5 mb5 mb-md0" alt="Caution Icon">
                  Your $2,475.34 Bonuses Package Expires When Timer Hits ZERO 
                  <img src="assets/images/caution-icon.webp" class="img-fluid ml-md5 mb5 mb-md0" alt="Caution Icon">
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="butif-section">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-26 f-md-50 w700 lh140 black-clr text-center text-md-start">
               Can't Decide?
               </div>
               <div class="f-18 f-md-22 w600 lh140 black-clr text-center text-md-start mt20">
               Listen…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               It's really simple…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               If you're sick of all the BS that is going on the market right now…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               If you're worried about inflation and the prices that are going up like there is no tomorrow
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Ninja AI IS for you <span class="w600"> PERIOD </span>
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               It was designed to be the life-changing system that you've been looking for…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Right now, you have the option to change your life
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               With just a few clicks
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               We created this for people exactly like you. To help them finally break through.
               </div>
               <div class="f-18 f-md-22 w600 lh140 red-clr text-center text-md-start mt20">
               Worried what will happen if it didn't work for you?
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/cantdecide-img.png" alt="Profitable" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>
   <div class="imagine-section">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-20 f-md-38 w400 lh140 black-clr text-center">
               We Will Pay You To Fail With Ninja AI
               </div>
               <div class="f-28 f-md-45 w700 lh140 black-clr text-center">
               Our 30 Days Iron Clad  Money Back Guarantee
               </div>
            </div>
         </div>
         <div class="row align-items-center mt30 mt-md50">
            <div class="col-12 col-md-6">
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start">
               We trust our app blindly…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               We know it works, after all we been using it for a year…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               And not just us…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               But hey, I know you probably don't know me… and you may be hesitant…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               And i understand that. A bit of skepticism is always healthy…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               But I can help…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Here is the deal, get access to Ninja AI now…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Use it, and enjoy its features to the fullest…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               And if for any reason you don't think Ninja AI is worth its weight in gold…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Just send us a message to our 24/7 customer support…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               And we will refund every single penny back to you…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               No questions asked… Not just that…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               We will send you $300 as a gift for wasting your time.
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Worst case scenario, you get Ninja AI and don't make any money
               </div>
               <div class="f-18 f-md-22 w600 lh140 red-clr text-center text-md-start mt20">
               You will still get paid $300 for trying it out.
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/money-back-guarantee.png" alt="imagine-img" class="mx-auto d-block img-fluid ">
            </div>
         </div>

         <!-- <div class="row mt20 mt-md50">
         <div class="col-12 f-28 f-md-50 w700 lh140 black-clr text-center">
               Wanna know how?
               <img src="assets/images/red-underline.webp" alt="Quotes" class="mx-auto d-block img-fluid ">
            </div>
         </div> -->
      </div>
   </div>


   <div class="cta-section-white">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-38 w700 lh140 text-center white-clr">
                  Get <span class="step-shapes"> NinjaAi </span> For Low One Time Fee… <br class="d-none d-md-block"> 
               </div>
               <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                  <span class="w700">Just Pay Once</span> Instead Of <s class="red-clr">Monthly</s>
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md50">
            <div class="col-md-7">
               <a href="#buynow" class="cta-link-btn">&gt;&gt;&gt; Get Instant Access To NinjaAi &lt;&lt;&lt;</a>
               <img src="assets/images/compaitable-gurantee1.webp" alt="Compaitable Gurantee" class="mx-auto d-block img-fluid mt20 mt-md30">
            </div>
            <div class="col-md-5 mt20 mt-md0">
               <div class="countdown-container">
                  <div class="f-20 f-md-24 w400 lh140 text-center white-clr mb10">
                     <span class="w700 red-clr">Hurry up!</span>  Price Increases In
                  </div>
                  <div class="countdown counter-white text-center"><div class="timer-label text-center"><span class="f-24 f-md-32 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-14 w500 smmltd">Days</span> </div><div class="timer-label text-center"><span class="f-24 f-md-32 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-14 w500 smmltd">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-24 f-md-32 timerbg oswald">34&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-14 w500 smmltd">Mins</span> </div><div class="timer-label text-center "><span class="f-24 f-md-32 timerbg oswald">41</span><br><span class="f-14 f-md-14 w500 smmltd">Sec</span> </div></div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="limited-time-sec">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-20 f-md-24 w700 text-center white-clr align-items-center flex-column flex-md-row justify-content-center">
                  <img src="assets/images/caution-icon.webp" class="img-fluid mr-md5 mb5 mb-md0" alt="Caution Icon">
                  Your $2,475.34 Bonuses Package Expires When Timer Hits ZERO 
                  <img src="assets/images/caution-icon.webp" class="img-fluid ml-md5 mb5 mb-md0" alt="Caution Icon">
               </div>
            </div>
         </div>
      </div>
   </div>



   <div class="white-section" id="buynow">
      <div class="container">
         <div class="row">
            <div class="col-12 col-md-9 mx-auto text-center">
               <div class="table-wrap">
                  <div class="table-head">
                     <div class="f-28 f-md-45 lh140 white-clr w700">
                        Today with NinjaAi, You're Getting
                     </div>
                  </div>
                  <div class=" ">
                     <ul class="table-list pl0 f-18 f-md-20 w400 text-center lh140 black-clr mt20 mt-md40">
                        <li>AI Technology Eliminate All The Guesswork So You Jump Straight to Results - <span class="w600">That’s PRICELESS</span> </li>
                        <li>Let Ai Find Hundreds Of The Guru's Profitable Funnels - <span class="w600">That’s PRICELESS</span> </li>
                        <li>Ninja Ai Creates Profitable DFY Affiliate Funnel Sites Instantly- <span class="w600">Worth $997</span> </li>
                        <li>All The Copywriting And Designing Is Done For You - <span class="w600">Worth $997</span> </li>
                        <li>Ninja Ai Built-In Traffic: Instant High Converting Traffic For 100% Free - <span class="w600"> Worth $997 </span></li>
                        <li>100% SEO & Mobile Optimizer - <span class="w600">Worth $697</span> </li>
                        <li>Generate & Manage Leads With Inbuilt Lead Management System - <span class="w600">Worth $257</span> </li>
                        <li>100% Newbie Friendly <span class="w600">(Beyond A Value)</span> </li>
                        <li>Step-By-Step Training Videos - <span class="w600">Worth $297</span> </li>
                        <li>Round-The-Clock World-Class Support <span class="w600">(Priceless – It’s Worth A LOT)</span> </li>
                        <li>FREE Commercial License - <span class="w600">That's Priceless</span> </li>
                        <li class="headline my15">BONUSES WORTH $2,475.34 FREE!!! </li>
                        <li>Selecting a Domain Name for Your Success</li>
                        <li>Inbound Marketing Development And Strategy</li>
                        <li>Split Testing Development and Strategy</li>
                        <li>Social Media Traffic Streams</li>
                        <li>Lead Generation On Demand</li>
                     </ul>
                  </div>
                  <div class="table-btn ">
                     <div class="f-22 f-md-28 w400 lh140 black-clr">
                        Total Value of Everything
                     </div>
                     <div class="f-26 f-md-36 w700 lh140 black-clr mt12">
                        YOU GET TODAY:
                     </div>
                     <div class="f-28 f-md-70 w700 red-clr mt12 l140">
                        $4,242.00
                     </div>
                     <div class="f-22 f-md-28 w400 lh140 black-clr mt20">
                        For Limited Time Only, Grab It For:
                     </div>
                     <div class="f-21 f-md-40 w700 lh140 red-clr mt20">
                       <del>$97 Monthly</del> 
                     </div>
                     <div class="f-24 f-md-34 w700 lh140 black-clr mt20">
                        Today, <span class="blue-clr">Just $17 One-Time</span> 
                     </div>
                     
                     <div class="col-12 mx-auto text-center">
                     <a href="https://warriorplus.com/o2/buy/zh5gzv/kxvwlz/vs2dks" id="buyButton" class="cta-link-btn mt20">&gt;&gt;&gt; Get Instant Access To NinjaAi &lt;&lt;&lt;</a>
                           </div>
                     <!-- <a href="https://warriorplus.com/o2/buy/zh5gzv/kxvwlz/vs2dks"><img src="https://warriorplus.com/o2/btn/fn200011000/zh5gzv/kxvwlz/357201" class="d-block img-fluid mx-auto mt20 mt-md30"></a>  -->
                  </div>
                  <div class="table-border-content">
                     <div class="tb-check d-flex align-items-center f-18">
                        <label class="checkbox inline">
                           <img src="https://assets.clickfunnels.com/templates/listhacking-sales/images/arrow-flash-small.gif" alt="">
                           <input type="checkbox" id="check" onclick="myFunction()">
                        </label>
                        <label class="checkbox inline">
                           <h5>Yes, Add Ninja Ai Funnel + Unlock Our $538/Day Secret AI Traffic System</h5>
                        </label>
                     </div>
                     <div class="p20">
                        <p class="f-18 text-center text-md-start"><b class="red-clr underline">LIMITED TIME OFFER :</b> Activate Ai Traffic Source To Your Order To Make Additional $538/Day with FREE Traffic. And Also Get 5 DFY Profitable High Ticket Funnels + Put Your Funnel Site On A Faster Server for More Conversions, Sales & Commissions. <br><b>(97% Of Customers Pick This Up And Get Immediate Results With It)</b></p>
                        <p class="f-18 text-center text-md-start">Just <span class="green-clr w600">  $9.97 One Time Only</span> </p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="imagine-section">
      <div class="container">
         <div class="row align-items-center mt30 mt-md50">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-28 f-md-50 w700 lh140 black-clr text-center text-md-start">
               Remember…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               You don’t have a lot of time. We will remove this huge discount once we hit our limit.
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               After that, you will have to <span class="w600"> pay $97/mo for it.</span>
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               While it will still be worth it.
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Why pay more, when you can just pay a small one-time fee today and get access to everything?
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/remember-img.png" alt="imagine-img" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>
   <div class="white-section">
      <div class="container">
         <div class="row align-items-center mt30 mt-md50">
            <div class="col-12 col-md-6">
               <div class="f-28 f-md-45 w700 lh140 black-clr text-center text-md-start">
               So, Are you ready yet?
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               You reading this far into the page means one thing.
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               You're interested.
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               The good news is, you're just minutes away from your breakthrough
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Just imagine waking up every day to thousands of dollars in your account Instead of ZERO.
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               The bad news is, it will sell out FAST
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               So you need to act now.
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Ready to join us?
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/are-you-ready-img.png" alt="imagine-img" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>


   <div class="cta-section-white">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-38 w700 lh140 text-center white-clr">
                  Get <span class="step-shapes"> NinjaAi </span> For Low One Time Fee… <br class="d-none d-md-block"> 
               </div>
               <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                  <span class="w700">Just Pay Once</span> Instead Of <s class="red-clr">Monthly</s>
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md50">
            <div class="col-md-7">
               <a href="#buynow" class="cta-link-btn">&gt;&gt;&gt; Get Instant Access To NinjaAi &lt;&lt;&lt;</a>
               <img src="assets/images/compaitable-gurantee1.webp" alt="Compaitable Gurantee" class="mx-auto d-block img-fluid mt20 mt-md30">
            </div>
            <div class="col-md-5 mt20 mt-md0">
               <div class="countdown-container">
                  <div class="f-20 f-md-24 w400 lh140 text-center white-clr mb10">
                     <span class="w700 red-clr">Hurry up!</span>  Price Increases In
                  </div>
                  <div class="countdown counter-white text-center"><div class="timer-label text-center"><span class="f-24 f-md-32 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-14 w500 smmltd">Days</span> </div><div class="timer-label text-center"><span class="f-24 f-md-32 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-14 w500 smmltd">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-24 f-md-32 timerbg oswald">20&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-14 w500 smmltd">Mins</span> </div><div class="timer-label text-center "><span class="f-24 f-md-32 timerbg oswald">14</span><br><span class="f-14 f-md-14 w500 smmltd">Sec</span> </div></div>
               </div>
            </div>
         </div>
      </div>
   </div>

   <div class="limited-time-sec">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-20 f-md-24 w700 text-center white-clr align-items-center flex-column flex-md-row justify-content-center">
                  <img src="assets/images/caution-icon.webp" class="img-fluid mr-md5 mb5 mb-md0" alt="Caution Icon">
                  Your $2,475.34 Bonuses Package Expires When Timer Hits ZERO 
                  <img src="assets/images/caution-icon.webp" class="img-fluid ml-md5 mb5 mb-md0" alt="Caution Icon">
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- <div class="grey-section">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-6">
               <div class="f-28 f-md-50 w700 lh140 text-center text-md-start black-clr decide-line">
               Hesitant?
               </div>
               <img src="assets/images/decide-line.webp" alt="Red Line" class="d-block img-fluid ">
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20 mt-md30">
                  Listen…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  It's really simple…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  If you're sick of all the BS that is going on the market right now…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  If you're worried about inflation and the prices that are going up like there is no tomorrow
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               NinjaAi IS for you  <span class="w600">PERIOD</span> 
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  It was designed to be the life-changing system that you've been looking for…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  Right now, you have the option to change your life With just a few clicks
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  We created this for people exactly like you. To help them finally break through.
               </div>
               <div class="f-20 f-md-22 w600 lh140 text-center text-md-start red-clr mt20">
                  Worried what will happen if it didn't work for you?
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/confused-girl.webp" alt="Confused Girl" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <div class="white-section">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-24 f-md-38 w400 lh140 text-center black-clr">
               We Will Pay You To Fail With NinjaAi
               </div>
               <div class="f-28 f-md-45 w700 lh140 text-center black-clr mt10">
                  Our 30 Days Iron Clad <span class="need-head white-clr">Money Back Guarantee</span> 
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md80">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr">
                  We trust our app blindly…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  We know it works, after all we been using it for a year… 
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  And not just us… 
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  But hey, I know you probably don't know me… and you may be hesitant… 
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  And I understand that. A bit of skepticism is always healthy… 
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  But I can help… 
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  Here is the deal, get access to NinjaAi now…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  Use it, and enjoy its features to the fullest… 
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  And if for any reason you don't think NinjaAi is worth its weight in gold…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  Just send us a message to our 24/7 customer support… 
               </div>
               <div class="f-20 f-md-22 w600 lh140 text-center text-md-start black-clr mt20">
                  And we will refund every single penny back to you… 
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  No questions asked… Not just that… 
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  We will send you a bundle of premium software as a gift for wasting your time.
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  Worst case scenario, you get NinjaAi and don't make any money
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                 <u> You will still get extra bundle of premium software for trying it out. </u>
               </div>
            </div>
            <div class="col-12 col-md-6 order-md-1 mt20 mt-md0">
               <img src="assets/images/money-back-guarantee.webp" alt="Money Back Guarantee" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div> -->


   <!-- <div class="white-section" id="buynow">
      <div class="container">
         <div class="row">
            <div class="col-12 col-md-9 mx-auto text-center">
               <div class="table-wrap">
                  <div class="table-head">
                     <div class="f-28 f-md-45 lh140 white-clr w700">
                        Today with NinjaAi, You're Getting :
                     </div>
                  </div>
                  <div class=" ">
                     <ul class="table-list pl0 f-18 f-md-20 w400 text-center lh140 black-clr mt20 mt-md40">
                        <li>AI Technology Creates High Quality Course For You - Just Enter A Keyword - <span class="w600">That's PRICELESS</span> </li>
                        <li>Create Auto Updating Websites with 100% Original, Search Engine Friendly Courses For Any Niche - <span class="w600">That's PRICELESS</span> </li>
                        <li>NinjaAi App To Start Your E-Learning Empire Having Marketplace, Blog & Members Area within Minutes - <span class="w600">Worth $997/Month</span> </li>
                        <li>AI Course Generator To Fill your New Website With Hundreds of AI Courses in Any Niche in Just 1 Click - <span class="w600">Worth $697</span> </li>
                        <li>Comes With 50,000+ SEO Optimized Done For You Smoking Hot AI Courses - <span class="w600"> Worth $997</span></li>
                        <li>NinjaAi Built-In Traffic - <span class="w600">Worth $997</span> </li>
                        <li>100%  Mobile Optimizer - <span class="w600">Worth $697</span> </li>
                        <li>Live AI ChatBots That Will Handle All Customer Support For You - <span class="w600">Worth $297</span> </li>
                        <li>Accept Payments With All Top Platforms – PayPal, Stripe, JVZoo, ClickBank, Warrior Plus, PayDotCom Etc - <span class="w600">Worth $297</span> </li>
                        <li>NinjaAi Mobile EDITION To Operate NinjaAi from Mobile Whether Android, iPhone, or Tablet - <span class="w600">Worth $497</span> </li>
                        <li>Manage Leads With Inbuilt Lead Management System - <span class="w600">Worth $257</span> </li>
                        <li>100% Newbie Friendly <span class="w600">(Beyond A Value)</span> </li>
                        <li>Step-By-Step Training Videos - <span class="w600">Worth $997</span> </li>
                        <li>Round-The-Clock World-Class Support <span class="w600">(Priceless – It's Worth A LOT)</span> </li>
                        <li>Commercial License - <span class="w600"> That's Priceless</span></li>
                        <li class="headline my15">BONUSES WORTH $2,475.34 FREE!!! </li>
                        <li>Bonus Course Package with Resell Rights</li>
                        <li>Niche Graphics Set</li>
                        <li>Video Creation Suite to Create Promo Videos and Courses</li>
                        <li>Social Media Traffic Streams</li>
                     </ul>
                  </div>
                  <div class="table-btn ">
                     <div class="f-22 f-md-28 w400 lh140 black-clr">
                        Total Value of Everything
                     </div>
                     <div class="f-26 f-md-36 w700 lh140 black-clr mt12">
                        YOU GET TODAY:
                     </div>
                     <div class="f-28 f-md-70 w700 red-clr mt12 l140">
                        $6,745.00
                     </div>
                     <div class="f-22 f-md-28 w400 lh140 black-clr mt20">
                        For Limited Time Only, Grab It For:
                     </div>
                     <div class="f-21 f-md-40 w700 lh140 red-clr mt20">
                       <del>$97 Monthly</del> 
                     </div>
                     <div class="f-24 f-md-34 w700 lh140 black-clr mt20">
                        Today, <span class="blue-clr">Just $17 One-Time</span> 
                     </div>
                     <a href="https://warriorplus.com/o2/buy/zh5gzv/kxvwlz/vs2dks" id="buyButton" class="cta-link-btn mt20">Grab NinjaAi Now</a>
                     <a href="https://warriorplus.com/o2/buy/zh5gzv/kxvwlz/vs2dks"><img src="https://warriorplus.com/o2/btn/fn200011000/zh5gzv/kxvwlz/357201" class="d-block img-fluid mx-auto mt20 mt-md30"></a> 
                  </div>
                  <div class="table-border-content">
                     <div class="tb-check d-flex align-items-center f-18">
                        <label class="checkbox inline">
                           <img src="https://assets.clickfunnels.com/templates/listhacking-sales/images/arrow-flash-small.gif" alt="">
                           <input type="checkbox" id="check" onclick="myFunction()">
                        </label>
                        <label class="checkbox inline">
                           <h5>Yes, Add NinjaAi + $2K/Day DFY AI Campaigns + AI Blogs!</h5>
                        </label>
                     </div>
                     <div class="p20">
                        <p class="f-18 text-center text-md-start"><b class="red-clr underline">LIMITED TIME OFFER :</b> Activate Our $2,067 Per Day Campaigns And Get 5 DFY Profitable Campaigns + Auto-Updating AI Blogs + Put Your NinjaAi Site On A Faster Server + Exclusive higher priority support! <br><b>(97% Of Customers Pick This Up And Get Immediate Results With It)</b></p>
                        <p class="f-18 text-center text-md-start">Just <span class="green-clr w600">  $9.97 One Time Only</span> </p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div> -->

   <!-- CTA Section Start -->
   <!--<div class="cta-section-white">-->
   <!--   <div class="container">-->
   <!--      <div class="row">-->
   <!--         <div class="col-12">-->
   <!--            <div class="f-28 f-md-38 w700 lh140 text-center white-clr">-->
   <!--               Get NinjaAi And Save $293 Now <br class="d-none d-md-block"> -->
   <!--            </div>-->
   <!--            <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">-->
   <!--               <span class="w700">Just Pay Once</span> Instead Of a <s>Monthly Fee</s>-->
   <!--            </div>-->
   <!--         </div>-->
   <!--      </div>-->
   <!--      <div class="row align-items-center mt20 mt-md50">-->
   <!--         <div class="col-md-7">-->
   <!--            <a href="#buynow" class="cta-link-btn">>>> Get Instant Access To NinjaAi <<<</a>-->
   <!--            <img src="assets/images/compaitable-gurantee1.webp" alt="Compaitable Gurantee" class="mx-auto d-block img-fluid mt20 mt-md30">-->
   <!--         </div>-->
   <!--         <div class="col-md-5 mt20 mt-md0">-->
   <!--            <div class="countdown-container">-->
   <!--               <div class="f-20 f-md-24 w400 lh140 text-center white-clr mb10">-->
   <!--                  <span class="w700 orange-clr">Hurry up!</span>  Price Increases In-->
   <!--               </div>-->
   <!--               <div class="countdown counter-white text-center">-->
   <!--                  <div class="timer-label text-center">-->
   <!--                     <span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 smmltd">Days</span> -->
   <!--                  </div>-->
   <!--                  <div class="timer-label text-center">-->
   <!--                     <span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> -->
   <!--                  </div>-->
   <!--                  <div class="timer-label text-center timer-mrgn">-->
   <!--                     <span class="f-31 f-md-45 timerbg oswald">46&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> -->
   <!--                  </div>-->
   <!--                  <div class="timer-label text-center ">-->
   <!--                     <span class="f-31 f-md-45 timerbg oswald">28</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> -->
   <!--                  </div>-->
   <!--               </div>-->
   <!--            </div>-->
   <!--         </div>-->
   <!--      </div>-->
   <!--   </div>-->
   <!--</div>-->
   <!-- CTA Section End -->

   <!--Limited Section Start -->
   <!--<div class="limited-time-sec">-->
   <!--   <div class="container">-->
   <!--      <div class="row">-->
   <!--         <div class="col-12 text-center">-->
   <!--            <div class="f-20 f-md-24 w700 text-center white-clr align-items-center flex-column flex-md-row justify-content-center">-->
   <!--               <img src="assets/images/caution-icon.webp" class="img-fluid mr-md5 mb5 mb-md0" alt="Caution Icon">-->
   <!--               Your $2,475.34 Bonuses Package Expires When Timer Hits ZERO -->
   <!--               <img src="assets/images/caution-icon.webp" class="img-fluid ml-md5 mb5 mb-md0" alt="Caution Icon">-->
   <!--            </div>-->
   <!--         </div>-->
   <!--      </div>-->
   <!--   </div>-->
   <!--</div>-->
   <!--Limited Section End -->

<!-- 
   <div class="grey-section">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-28 f-md-45 w700 lh140 text-center text-md-start black-clr">
                  Remember…
               </div>
               <img src="assets/images/remember-line.webp" alt="Red Line" class="d-block img-fluid">
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  You don't have a lot of time. We will remove this huge discount once we hit our limit.
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  After that, you will have to pay $997/mo for it.
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  While it will still be worth it.
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  Why pay more, when you can just pay a small one-time fee today and get access to everything?
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1 mt20 mt-md0">
               <img src="assets/images/remember.webp" alt="Remember" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <div class="white-section">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-6">
               <div class="f-28 f-md-45 w700 lh140 text-center text-md-start black-clr">
                  So, Are you ready yet?
               </div>
               <img src="assets/images/ready-line.webp" alt="Red Line" class="d-block mx-auto img-fluid mt5 ">
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  You reading this far into the page means one thing..
               </div>
               <div class="f-20 f-md-22 w600 lh140 text-center text-md-start black-clr mt20">
                  You're interested.
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  The good news is, you're just minutes away from your breakthrough
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  Just imagine waking up every day to thousands of dollars in your account Instead of ZERO.
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  The bad news is, it will sell out FAST So you need to act now.
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  Ready to join us?
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/ready.webp" alt="Ready" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div> -->

 

   <div class="inside-section">
      <div class="container">
         <div class="row">
            <div class="col-12 col-md-12">
               <div class="f-28 f-md-45 lh140 w700 text-center black-clr">
                  We'll see you inside,
               </div>
               <img src="assets/images/orange-line.webp" alt="Orange Line" class="mx-auto d-block img-fluid mt5 ">
            </div>
         </div>
         <div class="row mt20 mt-md70">
            <div class="col-12 col-md-8 mx-auto">
               <div class="row justify-content-between">
                  <div class="col-md-5">
                     <div class="inside-box">
                        <img src="assets/images/pranshu-gupta.webp" alt="Pranshu Gupta" class="mx-auto d-block mx-auto ">
                        <div class="f-22 f-md-28 lh140 w600 text-center black-clr mt20">
                           Pranshu Gupta
                        </div>
                     </div>
                  </div>
                  <div class="col-md-5 mt20 mt-md0">
                     <div class="inside-box">
                        <img src="assets/images/bizomart-team.webp" alt="Bizomart" class="mx-auto d-block mx-auto ">
                        <div class="f-22 f-md-28 lh140 w600 text-center black-clr mt20">
                           Bizomart
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="row mt20 mt-md70">
            <div class="col-12">
               <div class="f-20 f-md-22 lh140 w400 black-clr text-center text-md-start">
                  <span class="w600">PS:</span> If you act now, you will instantly receive [bonuses] worth over  <span class="w600">$2,475.34</span> This bonus is designed specifically to help you get 10x the results, in half the time required
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr text-center text-md-start mt20">
                 <span class="w600">PPS:</span>There is nothing else required for you to start earning with Ninja AINo hidden fees, no monthly costs, nothing.
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr text-center text-md-start mt20">
                 <span class="w600">PPPS:</span> Remember, you’re protected by our money-back guarantee If you fail, Not only will we refund your money.
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr text-center text-md-start mt20">
               We will send you <span class="red-clr">$300 out of our own pockets just for wasting your time</span> 
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr text-center text-md-start mt20">
                 <span class="w600">PPPPS:</span>   If you put off the decision till later, you will end up paying $997/mo Instead of just a one-time flat fee.
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr text-center text-md-start mt20">
               And why pay more, when you can pay less?
               </div>
            </div>
         </div>
      </div>
   </div>

   <div class="faq-section ">
      <div class="container ">
         <div class="row ">
            <div class="col-12 f-28 f-md-45 lh140 w600 text-center black-clr ">
               Frequently Asked <span class="orange-clr ">Questions</span>
            </div>
            <div class="col-12 mt20 mt-md30 ">
               <div class="row ">
                  <div class="col-md-9 mx-auto col-12 ">
                     <div class="faq-list ">
                        <div class="f-md-22 f-20 w600 black-clr lh140 ">
                           Do I need any experience to get started? 
                        </div>
                        <div class="f-18 w400 lh140 mt10 text-start ">
                           None, all you need is just an internet connection. And you're good to go
                        </div>
                     </div>
                     <div class="faq-list mt20">
                        <div class="f-md-22 f-20 w600 black-clr lh140 ">
                           Is there any monthly cost?
                        </div>
                        <div class="f-18 w400 lh140 mt10 text-start ">
                           Depends, If you act now, NONE. 
                           But if you wait, you might end up paying $997/month
                           <br><br>
                           It's up to you. 
                        </div>
                     </div>
                     <div class="faq-list mt20">
                        <div class="f-md-22 f-20 w600 black-clr lh140 ">
                           How long does it take to make money?
                        </div>
                        <div class="f-18 w400 lh140 mt10 text-start ">
                           Our average member made their first sale the same day they got access to NinjaAi.
                        </div>
                     </div>
                     <div class="faq-list mt20">
                        <div class="f-md-22 f-20 w600 black-clr lh140 ">
                           Do I need to purchase anything else for it to work?
                        </div>
                        <div class="f-18 w400 lh140 mt10 text-start ">
                           Nop, NinjaAi is the complete thing. 
                           <br><br>
                           You get everything you need to make it work. Nothing is left behind. 
                        </div>
                     </div>
                     <div class="faq-list mt20">
                        <div class="f-md-22 f-20 w600 black-clr lh140 ">
                           What if I failed?
                        </div>
                        <div class="f-18 w400 lh140 mt10 text-start ">
                           While that is unlikely, we removed all the risk for you.
                           <br><br>
                           If you tried NinjaAi and failed, we will refund you every cent you paid
                           <br><br>
                           And send you $300 on top of that just to apologize for wasting your time.
                        </div>
                     </div>
                     <div class="faq-list mt20">
                        <div class="f-md-22 f-20 w600 black-clr lh140 ">
                           How can I get started?
                        </div>
                        <div class="f-18 w400 lh140 mt10 text-start ">
                           Awesome, I like your excitement, All you have to do is click any of the buy buttons on the page, and secure your copy of NinjaAi at a one-time fee
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md50 ">
               <div class="f-22 f-md-28 w600 black-clr px0 text-center lh140 ">If you have any query, simply reach to us at <a href="mailto:support@bizomart.com " class="kapblue ">support@bizomart.com</a>
               </div>
            </div>
         </div>
      </div>
   </div>


   <!-- CTA Section Start -->
   <div class="cta-section-white">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-45 f-md-45 lh140 w700 white-clr final-shape">
                  FINAL CALL
               </div>
               <div class="f-22 f-md-36 lh140 w600 white-clr mt20 mt-md30">
                  You Still Have the Opportunity To Raise Your Game… <br> Remember, It's Your Make-or-Break Time!
               </div>
               <div class="f-28 f-md-38 w700 lh140 text-center white-clr mt20 mt-md30">
                  Get NinjaAi And Save $293 Now <br class="d-none d-md-block"> 
               </div>
               <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                  <span class="w700">Just Pay Once</span> Instead Of a <s>Monthly Fee</s>
               </div>
               <div class="row">
                  <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center">
                     <a href="#buynow" class="cta-link-btn">>>> Get Instant Access To NinjaAi <<<</a>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30">
                  <img src="assets/images/compaitable-gurantee1.webp" alt="Compaitable Gurantee" class="mx-auto d-block img-fluid">
               </div>
               <!--<div class="col-12 mt20 mt-md30">-->
               <!--    <a href="https://warriorplus.com/o2/buy/hb06pf/wmp7ql/pzsb6y"><img src="https://warriorplus.com/o2/btn/fn200011000/hb06pf/wmp7ql/352565" class="d-block img-fluid mx-auto mt20 mt-md30"></a>-->
               <!--</div>-->
            </div>
         </div>
      </div>
   </div>
   <!-- CTA Section End -->

   <!--Limited Section Start -->
   <div class="limited-time-sec">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-20 f-md-24 w700 text-center white-clr align-items-center flex-column flex-md-row justify-content-center">
                  <img src="assets/images/caution-icon.webp" class="img-fluid mr-md5 mb5 mb-md0" alt="Caution Icon">
                  Your $2,475.34 Bonuses Package Expires When Timer Hits ZERO 
                  <img src="assets/images/caution-icon.webp" class="img-fluid ml-md5 mb5 mb-md0" alt="Caution Icon">
               </div>
            </div>
         </div>
      </div>
   </div>
   <!--Limited Section End -->

      
      <div class="footer-section">
         <div class="container ">
            <div class="row">
               <div class="col-12 text-center">
               <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 645.02 151.26" style="max-height:70px">
                                    <defs>
                                      <style>
                                        .cls-1 {
                                          fill: url(#linear-gradient-2);
                                        }
                                  
                                        .cls-2 {
                                          fill: #fff;
                                        }
                                  
                                        .cls-3 {
                                          fill-rule: evenodd;
                                        }
                                  
                                        .cls-3, .cls-4 {
                                          fill: #ff813b;
                                        }
                                  
                                        .cls-5 {
                                          fill: url(#linear-gradient-3);
                                        }
                                  
                                        .cls-6 {
                                          fill: url(#linear-gradient);
                                        }
                                  
                                        .cls-7 {
                                          fill: url(#linear-gradient-4);
                                        }
                                      </style>
                                      <linearGradient id="linear-gradient" x1="47.81" y1="75" x2="185.48" y2="75" gradientUnits="userSpaceOnUse">
                                        <stop offset="0" stop-color="#b956ff"></stop>
                                        <stop offset="1" stop-color="#6e33ff"></stop>
                                      </linearGradient>
                                      <linearGradient id="linear-gradient-2" x1="0" y1="75" x2="85.18" y2="75" gradientUnits="userSpaceOnUse">
                                        <stop offset="0" stop-color="#b956ff"></stop>
                                        <stop offset="1" stop-color="#a356fa"></stop>
                                      </linearGradient>
                                      <linearGradient id="linear-gradient-3" x1="55.61" y1="62.54" x2="62.86" y2="62.54" gradientUnits="userSpaceOnUse">
                                        <stop offset="0" stop-color="#b956ff"></stop>
                                        <stop offset="1" stop-color="#ac59fa"></stop>
                                      </linearGradient>
                                      <linearGradient id="linear-gradient-4" x1="54.62" y1="79" x2="61.87" y2="79" gradientTransform="translate(19.64 -11.24) rotate(13.24)" xlink:href="#linear-gradient-3"></linearGradient>
                                    </defs>
                                    <g id="Layer_1-2" data-name="Layer 1">
                                      <g>
                                        <g>
                                          <g>
                                            <path class="cls-6" d="m181.49,58.53h-10.72c-1.4-5.13-3.43-9.99-6.01-14.51l7.58-7.58c1.56-1.56,1.56-4.09,0-5.65l-17.65-17.65c-1.56-1.56-4.09-1.56-5.65,0l-7.58,7.58c-4.51-2.58-9.38-4.61-14.51-6.01V4c0-2.21-1.79-4-4-4h-24.96c-2.21,0-4,1.79-4,4v10.72c-.16.04-.31.09-.47.14-.27.08-.53.15-.8.23-.27.08-.55.16-.82.24-.55.17-1.09.35-1.63.53-.11.04-.22.08-.33.12-.48.17-.96.34-1.44.52-.12.04-.24.09-.36.14-.52.2-1.04.4-1.56.62-.04.02-.08.03-.12.05-2.41,1-4.74,2.15-6.99,3.43l-7.57-7.57c-1.56-1.56-4.09-1.56-5.65,0l-17.65,17.65c-.36.36-.63.76-.82,1.2h61.99v.02c.23,0,.45-.02.68-.02,23.76,0,43.01,19.26,43.01,43.01s-19.26,43.01-43.01,43.01c-.5,0-1-.02-1.5-.04v.04h-61.17c.19.43.46.84.82,1.2l17.65,17.65c1.56,1.56,4.09,1.56,5.65,0l7.57-7.57c2.25,1.28,4.58,2.43,6.99,3.43.04.02.08.03.12.05.51.21,1.03.42,1.55.62.12.05.24.09.36.14.47.18.95.35,1.43.52.11.04.23.08.34.12.54.18,1.08.36,1.63.53.27.08.55.16.83.25.26.08.53.16.79.23.16.04.31.09.47.14v10.72c0,2.21,1.79,4,4,4h24.96c2.21,0,4-1.79,4-4v-10.72c5.13-1.4,9.99-3.43,14.51-6.01l7.58,7.58c1.56,1.56,4.09,1.56,5.65,0l17.65-17.65c1.56-1.56,1.56-4.09,0-5.65l-7.58-7.58c2.58-4.51,4.61-9.38,6.01-14.51h10.72c2.21,0,4-1.79,4-4v-24.96c0-2.21-1.79-4-4-4Z"></path>
                                            <path class="cls-1" d="m76.64,101.54c-5.74-7.31-9.18-16.52-9.18-26.54s3.44-19.23,9.18-26.54c2.45-3.13,5.33-5.9,8.53-8.24h-48.39c-1.31-1.89-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.13h17.07c-1.51,3.21-2.76,6.58-3.71,10.07h3.1c1.31-1.89,3.49-3.13,5.97-3.13,4.01,0,7.25,3.25,7.25,7.25s-3.22,7.23-7.21,7.25h-.04c-2.48,0-4.66-1.24-5.97-3.13H13.22c-.44-.64-.98-1.19-1.59-1.65-1.21-.92-2.73-1.48-4.38-1.48-4,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.14h38.82c1.31-1.89,3.49-3.13,5.97-3.13.43,0,.84.05,1.25.12,3.41.59,6,3.56,6,7.14,0,2.78-1.56,5.19-3.86,6.41-1.01.53-2.16.84-3.39.84-2.48,0-4.66-1.24-5.97-3.13h-17.85c-1.31-1.9-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.13h15.96c.95,3.48,2.2,6.85,3.71,10.07h-5.63c-1.31-1.89-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25,0,1.53.48,2.95,1.28,4.12,1.31,1.9,3.49,3.14,5.97,3.14s4.66-1.24,5.97-3.14h36.95c-3.21-2.34-6.08-5.11-8.53-8.24ZM30.64,47.97c-2,0-3.62-1.62-3.62-3.62s1.62-3.62,3.62-3.62,3.63,1.62,3.63,3.62-1.63,3.62-3.63,3.62Zm-23.39,26.47c-2,0-3.62-1.63-3.62-3.62s1.63-3.63,3.62-3.63,3.63,1.63,3.63,3.63-1.63,3.62-3.63,3.62Zm20.97,16.45c-2,0-3.63-1.63-3.63-3.63s1.63-3.62,3.63-3.62,3.62,1.63,3.62,3.62-1.63,3.63-3.62,3.63Zm14.08,18.39c-2,0-3.62-1.62-3.62-3.62s1.62-3.62,3.62-3.62,3.62,1.62,3.62,3.62-1.62,3.62-3.62,3.62Z"></path>
                                            <circle class="cls-5" cx="59.23" cy="62.54" r="3.63"></circle>
                                            <circle class="cls-7" cx="58.24" cy="79" r="3.63" transform="translate(-16.54 15.44) rotate(-13.24)"></circle>
                                          </g>
                                          <g>
                                            <g>
                                              <path class="cls-3" d="m104.63,78.16c-9.44,6-14.52,2.86-15.25-9.4,4.93,3.38,10.02,6.52,15.25,9.4Z"></path>
                                              <path class="cls-3" d="m131.01,68.76c-.72,12.26-5.81,15.4-15.25,9.4,5.24-2.88,10.32-6.02,15.25-9.4Z"></path>
                                            </g>
                                            <path class="cls-4" d="m110.16,40.13c-.89,0-1.78.03-2.66.1-.08,0-.16,0-.24.02-9.59.79-18.07,5.45-23.89,12.42-.25.3-.49.59-.73.89-.14.19-.29.38-.43.57-4.02,5.37-6.52,11.93-6.88,19.07,0,.01,0,.03,0,.04-.01.22-.02.45-.03.68-.01.3-.02.6-.02.9v.18c0,.28,0,.55,0,.83.44,18.64,15.5,33.66,34.15,34.04.24,0,.48,0,.72,0,.3,0,.6,0,.9,0,18.85-.48,33.98-15.9,33.98-34.87s-15.61-34.88-34.88-34.88Zm.19,39.35c-14.41,14.37-29.15,1.23-29.15-9.45s13.05,0,29.15,0,29.15-10.67,29.15,0-14.74,23.81-29.15,9.45Z"></path>
                                          </g>
                                        </g>
                                        <g>
                                          <path class="cls-2" d="m286.65,118.51h-17.37l-39.32-59.42v59.42h-17.37V31.8h17.37l39.32,59.54V31.8h17.37v86.71Z"></path>
                                          <path class="cls-2" d="m304.95,38.69c-2.03-1.94-3.04-4.36-3.04-7.26s1.01-5.31,3.04-7.26c2.03-1.94,4.57-2.92,7.63-2.92s5.6.97,7.63,2.92c2.02,1.94,3.04,4.36,3.04,7.26s-1.02,5.31-3.04,7.26c-2.03,1.94-4.57,2.91-7.63,2.91s-5.6-.97-7.63-2.91Zm16.19,11.1v68.72h-17.37V49.79h17.37Z"></path>
                                          <path class="cls-2" d="m396.18,56.55c5.04,5.17,7.57,12.38,7.57,21.65v40.31h-17.36v-37.96c0-5.46-1.37-9.65-4.1-12.59-2.73-2.94-6.45-4.4-11.16-4.4s-8.58,1.47-11.35,4.4c-2.77,2.94-4.15,7.13-4.15,12.59v37.96h-17.37V49.79h17.37v8.56c2.31-2.98,5.27-5.31,8.87-7.01,3.6-1.69,7.55-2.54,11.85-2.54,8.19,0,14.8,2.59,19.85,7.75Z"></path>
                                          <path class="cls-2" d="m437.61,129.8c0,7.61-1.88,13.09-5.64,16.44-3.77,3.35-9.16,5.02-16.19,5.02h-7.69v-14.76h4.96c2.64,0,4.51-.52,5.58-1.55,1.07-1.03,1.61-2.71,1.61-5.02V49.79h17.37v80.01Zm-16.31-91.11c-2.03-1.94-3.04-4.36-3.04-7.26s1.01-5.31,3.04-7.26c2.02-1.94,4.61-2.92,7.75-2.92s5.58.97,7.57,2.92c1.98,1.94,2.98,4.36,2.98,7.26s-.99,5.31-2.98,7.26c-1.98,1.94-4.51,2.91-7.57,2.91s-5.73-.97-7.75-2.91Z"></path>
                                          <path class="cls-2" d="m454.42,65.42c2.77-5.37,6.53-9.51,11.29-12.4,4.76-2.89,10.07-4.34,15.94-4.34,5.13,0,9.61,1.04,13.46,3.1,3.85,2.07,6.92,4.67,9.24,7.82v-9.8h17.49v68.72h-17.49v-10.05c-2.23,3.23-5.31,5.89-9.24,8-3.93,2.11-8.46,3.16-13.58,3.16-5.79,0-11.06-1.49-15.82-4.47-4.76-2.98-8.52-7.17-11.29-12.59-2.77-5.41-4.16-11.64-4.16-18.67s1.38-13.11,4.16-18.48Zm47.45,7.88c-1.65-3.02-3.89-5.33-6.7-6.95-2.81-1.61-5.83-2.42-9.06-2.42s-6.2.79-8.93,2.36c-2.73,1.57-4.94,3.87-6.64,6.88-1.69,3.02-2.54,6.6-2.54,10.73s.85,7.75,2.54,10.85c1.69,3.1,3.93,5.48,6.7,7.13,2.77,1.66,5.72,2.48,8.87,2.48s6.24-.81,9.06-2.42c2.81-1.61,5.04-3.93,6.7-6.95,1.65-3.02,2.48-6.64,2.48-10.85s-.83-7.83-2.48-10.85Z"></path>
                                          <path class="cls-4" d="m591.93,102.01h-34.48l-5.71,16.5h-18.24l31.14-86.71h20.22l31.13,86.71h-18.36l-5.71-16.5Zm-4.71-13.89l-12.53-36.22-12.53,36.22h25.06Z"></path>
                                          <path class="cls-4" d="m645.02,31.93v86.58h-17.37V31.93h17.37Z"></path>
                                        </g>
                                      </g>
                                    </g>
                                  </svg>
                  <div class="f-14 f-md-16 w400 mt20 lh150 white-clr text-start" style="color:#7d8398;"><span class="w600">Note:</span> This site is not a part of the Facebook website or Facebook Inc. Additionally, this site is NOT endorsed by Facebook in any way. FACEBOOK & INSTAGRAM are the trademarks of FACEBOOK, Inc.<br><br>
                     <span class="w600">Earning Disclaimer:</span> Please make your purchase decision wisely. There is no promise or representation that you will make a certain amount of money, or any money, or not lose money, as a result of using
                     our products and services. Any stats, conversion, earnings, or income statements are strictly estimates. There is no guarantee that you will make these levels for yourself. As with any business, your results will vary and will
                     be based on your personal abilities, experience, knowledge, capabilities, level of desire, and an infinite number of variables beyond our control, including variables we or you have not anticipated. There are no guarantees concerning
                     the level of success you may experience. Each person's results will vary. There are unknown risks in any business, particularly with the Internet where advances and changes can happen quickly. The use of the plug-in and the given
                     information should be based on your own due diligence, and you agree that we are not liable for your success or failure.
                  </div>
                  <!-- <div class=" mt20 mt-md40 white-clr"> <script type="text/javascript" src="https://warriorplus.com/o2/disclaimer/rbhnkl" defer></script><div class="wplus_spdisclaimer"></div></div> -->
                  <div class=" mt20 mt-md40 white-clr"> <script type="text/javascript" src="https://warriorplus.com/o2/disclaimer/hb06pf" defer=""></script><div class="wplus_spdisclaimer f-17 w300 mt20 lh140 white-clr text-center"></div></div>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-14 f-md-16 w400 lh150 white-clr text-xs-center">Copyright © NinjaAi 2023</div>
                  <ul class="footer-ul w400 f-14 f-md-16 white-clr text-center text-md-right">
                     <li><a href="https://support.bizomart.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://getninjaai.com/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://getninjaai.com/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://getninjaai.com/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://getninjaai.com/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://getninjaai.com/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://getninjaai.com/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                    
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!-- Exit Popup and Timer -->
      <?php include('timer.php'); ?>	  
      <!-- Exit Popup and Timer End -->
   </body>
</html>

<script> 
function myFunction() {
  var checkBox = document.getElementById("check");
  var buybutton = document.getElementById("buyButton");
  if (checkBox.checked == true){
 
	    buybutton.getAttribute("href");
	    buybutton.setAttribute("href","https://warriorplus.com/o2/buy/hb06pf/wmp7ql/b74wsw");
	} else {
		 buybutton.getAttribute("href");
		 buybutton.setAttribute("href","https://warriorplus.com/o2/buy/hb06pf/wmp7ql/pzsb6y");
	  }
}
</script>
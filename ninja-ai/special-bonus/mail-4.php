<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- Device & IE Compatibility Meta -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=9">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=9">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta name="title" content="Ninja Ai Special Bonuses">
    <meta name="description" content="Ninja Ai Special Bonuses">
    <meta name="keywords" content="Ninja Ai Special Bonuses">
    <meta property="og:image" content="https://www.getninjaai.com/special-bonus/thumbnail.png">
    <meta name="language" content="English">
    <meta name="revisit-after" content="1 days">
    <meta name="author" content="Dr. Amit Pareek">
    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:title" content="Ninja Ai Special Bonuses">
    <meta property="og:description" content="Ninja Ai Special Bonuses">
    <meta property="og:image" content="https://www.getninjaai.com/special-bonus/thumbnail.png">
    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:title" content="Ninja Ai Special Bonuses">
    <meta property="twitter:description" content="Ninja Ai Special Bonuses">
    <meta property="twitter:image" content="https://www.getninjaai.com/special-bonus/thumbnail.png">


<title>Ninja Ai  Bonuses</title>
<!-- Shortcut Icon  -->
<link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
<!-- Css CDN Load Link -->
<link rel="stylesheet" href="assets/css/font-awesome.min.css" type="text/css" />
<link rel="stylesheet" href="../common_assets/css/bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="../common_assets/css/general.css">
<link rel="stylesheet" href="assets/css/timer.css" type="text/css" />
<link rel="stylesheet" type="text/css" href="assets/css/bonus-style.css">
<link rel="stylesheet" type="text/css" href="assets/css/style.css">
<link rel="stylesheet" type="text/css" href="assets/css/bonus.css">
<link rel="stylesheet" href="assets/css/style-feature.css" type="text/css">
<link rel="stylesheet" href="assets/css/style-bottom.css" type="text/css">
<!-- Font Family CDN Load Links -->
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&family=Poppins:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<!-- Javascript File Load -->
<script src="../common_assets/js/jquery.min.js"></script>
<script src="../common_assets/js/popper.min.js"></script>
<script src="../common_assets/js/bootstrap.min.js"></script>   
<!-- Buy Button Lazy load Script -->


<script>
 $(document).ready(function() {
  /* Every time the window is scrolled ... */
  $(window).scroll(function() {
	 /* Check the location of each desired element */
	 $('.hideme').each(function(i) {
		 var bottom_of_object = $(this).offset().top + $(this).outerHeight();
		 var bottom_of_window = $(window).scrollTop() + $(window).height();
		 /* If the object is completely visible in the window, fade it it */
		 if ((bottom_of_window - bottom_of_object) > -200) {
			 $(this).animate({
				 'opacity': '1'
			 }, 300);
		 }
	 });
  });
 });
</script>
<!-- Smooth Scrolling Script -->
<script>
 $(document).ready(function() {
  // Add smooth scrolling to all links
  $("a").on('click', function(event) {
 
	 // Make sure this.hash has a value before overriding default behavior
	 if (this.hash !== "") {
		 // Prevent default anchor click behavior
		 event.preventDefault();
 
		 // Store hash
		 var hash = this.hash;
 
		 // Using jQuery's animate() method to add smooth page scroll
		 // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
		 $('html, body').animate({
			 scrollTop: $(hash).offset().top
		 }, 800, function() {
 
			 // Add hash (#) to URL when done scrolling (default click behavior)
			 window.location.hash = hash;
		 });
	 } // End if
  });
 });
</script>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-K6H5FJP');</script>
<!-- End Google Tag Manager -->
</head>
<body>


<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K6H5FJP"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

  <!-- New Timer  Start-->
  <?php
	 $date = 'July 31 2023 11:59 PM EST';
	 $exp_date = strtotime($date);
	 $now = time();  
	 /*
	 
	 $date = date('F d Y g:i:s A eO');
	 $rand_time_add = rand(700, 1200);
	 $exp_date = strtotime($date) + $rand_time_add;
	 $now = time();*/
	 
	 if ($now < $exp_date) {
	 ?>
  <?php
	 } else {
		 echo "Times Up";
	 }
	 ?>
  <!-- New Timer End -->
  <?php
	 if(!isset($_GET['afflink'])){
	 $_GET['afflink'] = 'https://warriorplus.com/o2/a/j9bx01/0';
	 $_GET['name'] = 'Dr. Amit Pareek';      
	 }
	 ?>

<div class="main-header">
        <div class="container">
            <div class="row">
				<div class="col-12">
					<div class="f-md-32 f-20 lh160 w400 text-center white-clr">
						<span class="orange-clr w600"><?php echo $_GET['name'];?>'s </span> special bonus for &nbsp; 	  
						<svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 645.02 151.26" style="max-height:70px">
                              <defs>
                                <style>
                                  .cls-1n {
                                    fill: url(#linear-gradient-2);
                                  }
                            
                                  .cls-2n {
                                    fill: #fff;
                                  }
                            
                                  .cls-3n {
                                    fill-rule: evenodd;
                                  }
                            
                                  .cls-3n, .cls-4n {
                                    fill: #ff813b;
                                  }
                            
                                  .cls-5n {
                                    fill: url(#linear-gradient-3);
                                  }
                            
                                  .cls-6n {
                                    fill: url(#linear-gradient);
                                  }
                            
                                  .cls-7n {
                                    fill: url(#linear-gradient-4);
                                  }
                                </style>
                                <linearGradient id="linear-gradient" x1="47.81" y1="75" x2="185.48" y2="75" gradientUnits="userSpaceOnUse">
                                  <stop offset="0" stop-color="#b956ff"></stop>
                                  <stop offset="1" stop-color="#6e33ff"></stop>
                                </linearGradient>
                                <linearGradient id="linear-gradient-2" x1="0" y1="75" x2="85.18" y2="75" gradientUnits="userSpaceOnUse">
                                  <stop offset="0" stop-color="#b956ff"></stop>
                                  <stop offset="1" stop-color="#a356fa"></stop>
                                </linearGradient>
                                <linearGradient id="linear-gradient-3" x1="55.61" y1="62.54" x2="62.86" y2="62.54" gradientUnits="userSpaceOnUse">
                                  <stop offset="0" stop-color="#b956ff"></stop>
                                  <stop offset="1" stop-color="#ac59fa"></stop>
                                </linearGradient>
                                <linearGradient id="linear-gradient-4" x1="54.62" y1="79" x2="61.87" y2="79" gradientTransform="translate(19.64 -11.24) rotate(13.24)" xlink:href="#linear-gradient-3"></linearGradient>
                              </defs>
                              <g id="Layer_1-2" data-name="Layer 1">
                                <g>
                                  <g>
                                    <g>
                                      <path class="cls-6n" d="m181.49,58.53h-10.72c-1.4-5.13-3.43-9.99-6.01-14.51l7.58-7.58c1.56-1.56,1.56-4.09,0-5.65l-17.65-17.65c-1.56-1.56-4.09-1.56-5.65,0l-7.58,7.58c-4.51-2.58-9.38-4.61-14.51-6.01V4c0-2.21-1.79-4-4-4h-24.96c-2.21,0-4,1.79-4,4v10.72c-.16.04-.31.09-.47.14-.27.08-.53.15-.8.23-.27.08-.55.16-.82.24-.55.17-1.09.35-1.63.53-.11.04-.22.08-.33.12-.48.17-.96.34-1.44.52-.12.04-.24.09-.36.14-.52.2-1.04.4-1.56.62-.04.02-.08.03-.12.05-2.41,1-4.74,2.15-6.99,3.43l-7.57-7.57c-1.56-1.56-4.09-1.56-5.65,0l-17.65,17.65c-.36.36-.63.76-.82,1.2h61.99v.02c.23,0,.45-.02.68-.02,23.76,0,43.01,19.26,43.01,43.01s-19.26,43.01-43.01,43.01c-.5,0-1-.02-1.5-.04v.04h-61.17c.19.43.46.84.82,1.2l17.65,17.65c1.56,1.56,4.09,1.56,5.65,0l7.57-7.57c2.25,1.28,4.58,2.43,6.99,3.43.04.02.08.03.12.05.51.21,1.03.42,1.55.62.12.05.24.09.36.14.47.18.95.35,1.43.52.11.04.23.08.34.12.54.18,1.08.36,1.63.53.27.08.55.16.83.25.26.08.53.16.79.23.16.04.31.09.47.14v10.72c0,2.21,1.79,4,4,4h24.96c2.21,0,4-1.79,4-4v-10.72c5.13-1.4,9.99-3.43,14.51-6.01l7.58,7.58c1.56,1.56,4.09,1.56,5.65,0l17.65-17.65c1.56-1.56,1.56-4.09,0-5.65l-7.58-7.58c2.58-4.51,4.61-9.38,6.01-14.51h10.72c2.21,0,4-1.79,4-4v-24.96c0-2.21-1.79-4-4-4Z"></path>
                                      <path class="cls-1n" d="m76.64,101.54c-5.74-7.31-9.18-16.52-9.18-26.54s3.44-19.23,9.18-26.54c2.45-3.13,5.33-5.9,8.53-8.24h-48.39c-1.31-1.89-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.13h17.07c-1.51,3.21-2.76,6.58-3.71,10.07h3.1c1.31-1.89,3.49-3.13,5.97-3.13,4.01,0,7.25,3.25,7.25,7.25s-3.22,7.23-7.21,7.25h-.04c-2.48,0-4.66-1.24-5.97-3.13H13.22c-.44-.64-.98-1.19-1.59-1.65-1.21-.92-2.73-1.48-4.38-1.48-4,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.14h38.82c1.31-1.89,3.49-3.13,5.97-3.13.43,0,.84.05,1.25.12,3.41.59,6,3.56,6,7.14,0,2.78-1.56,5.19-3.86,6.41-1.01.53-2.16.84-3.39.84-2.48,0-4.66-1.24-5.97-3.13h-17.85c-1.31-1.9-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.13h15.96c.95,3.48,2.2,6.85,3.71,10.07h-5.63c-1.31-1.89-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25,0,1.53.48,2.95,1.28,4.12,1.31,1.9,3.49,3.14,5.97,3.14s4.66-1.24,5.97-3.14h36.95c-3.21-2.34-6.08-5.11-8.53-8.24ZM30.64,47.97c-2,0-3.62-1.62-3.62-3.62s1.62-3.62,3.62-3.62,3.63,1.62,3.63,3.62-1.63,3.62-3.63,3.62Zm-23.39,26.47c-2,0-3.62-1.63-3.62-3.62s1.63-3.63,3.62-3.63,3.63,1.63,3.63,3.63-1.63,3.62-3.63,3.62Zm20.97,16.45c-2,0-3.63-1.63-3.63-3.63s1.63-3.62,3.63-3.62,3.62,1.63,3.62,3.62-1.63,3.63-3.62,3.63Zm14.08,18.39c-2,0-3.62-1.62-3.62-3.62s1.62-3.62,3.62-3.62,3.62,1.62,3.62,3.62-1.62,3.62-3.62,3.62Z"></path>
                                      <circle class="cls-5n" cx="59.23" cy="62.54" r="3.63"></circle>
                                      <circle class="cls-7n" cx="58.24" cy="79" r="3.63" transform="translate(-16.54 15.44) rotate(-13.24)"></circle>
                                    </g>
                                    <g>
                                      <g>
                                        <path class="cls-3n" d="m104.63,78.16c-9.44,6-14.52,2.86-15.25-9.4,4.93,3.38,10.02,6.52,15.25,9.4Z"></path>
                                        <path class="cls-3n" d="m131.01,68.76c-.72,12.26-5.81,15.4-15.25,9.4,5.24-2.88,10.32-6.02,15.25-9.4Z"></path>
                                      </g>
                                      <path class="cls-4n" d="m110.16,40.13c-.89,0-1.78.03-2.66.1-.08,0-.16,0-.24.02-9.59.79-18.07,5.45-23.89,12.42-.25.3-.49.59-.73.89-.14.19-.29.38-.43.57-4.02,5.37-6.52,11.93-6.88,19.07,0,.01,0,.03,0,.04-.01.22-.02.45-.03.68-.01.3-.02.6-.02.9v.18c0,.28,0,.55,0,.83.44,18.64,15.5,33.66,34.15,34.04.24,0,.48,0,.72,0,.3,0,.6,0,.9,0,18.85-.48,33.98-15.9,33.98-34.87s-15.61-34.88-34.88-34.88Zm.19,39.35c-14.41,14.37-29.15,1.23-29.15-9.45s13.05,0,29.15,0,29.15-10.67,29.15,0-14.74,23.81-29.15,9.45Z"></path>
                                    </g>
                                  </g>
                                  <g>
                                    <path class="cls-2n" d="m286.65,118.51h-17.37l-39.32-59.42v59.42h-17.37V31.8h17.37l39.32,59.54V31.8h17.37v86.71Z"></path>
                                    <path class="cls-2n" d="m304.95,38.69c-2.03-1.94-3.04-4.36-3.04-7.26s1.01-5.31,3.04-7.26c2.03-1.94,4.57-2.92,7.63-2.92s5.6.97,7.63,2.92c2.02,1.94,3.04,4.36,3.04,7.26s-1.02,5.31-3.04,7.26c-2.03,1.94-4.57,2.91-7.63,2.91s-5.6-.97-7.63-2.91Zm16.19,11.1v68.72h-17.37V49.79h17.37Z"></path>
                                    <path class="cls-2n" d="m396.18,56.55c5.04,5.17,7.57,12.38,7.57,21.65v40.31h-17.36v-37.96c0-5.46-1.37-9.65-4.1-12.59-2.73-2.94-6.45-4.4-11.16-4.4s-8.58,1.47-11.35,4.4c-2.77,2.94-4.15,7.13-4.15,12.59v37.96h-17.37V49.79h17.37v8.56c2.31-2.98,5.27-5.31,8.87-7.01,3.6-1.69,7.55-2.54,11.85-2.54,8.19,0,14.8,2.59,19.85,7.75Z"></path>
                                    <path class="cls-2n" d="m437.61,129.8c0,7.61-1.88,13.09-5.64,16.44-3.77,3.35-9.16,5.02-16.19,5.02h-7.69v-14.76h4.96c2.64,0,4.51-.52,5.58-1.55,1.07-1.03,1.61-2.71,1.61-5.02V49.79h17.37v80.01Zm-16.31-91.11c-2.03-1.94-3.04-4.36-3.04-7.26s1.01-5.31,3.04-7.26c2.02-1.94,4.61-2.92,7.75-2.92s5.58.97,7.57,2.92c1.98,1.94,2.98,4.36,2.98,7.26s-.99,5.31-2.98,7.26c-1.98,1.94-4.51,2.91-7.57,2.91s-5.73-.97-7.75-2.91Z"></path>
                                    <path class="cls-2n" d="m454.42,65.42c2.77-5.37,6.53-9.51,11.29-12.4,4.76-2.89,10.07-4.34,15.94-4.34,5.13,0,9.61,1.04,13.46,3.1,3.85,2.07,6.92,4.67,9.24,7.82v-9.8h17.49v68.72h-17.49v-10.05c-2.23,3.23-5.31,5.89-9.24,8-3.93,2.11-8.46,3.16-13.58,3.16-5.79,0-11.06-1.49-15.82-4.47-4.76-2.98-8.52-7.17-11.29-12.59-2.77-5.41-4.16-11.64-4.16-18.67s1.38-13.11,4.16-18.48Zm47.45,7.88c-1.65-3.02-3.89-5.33-6.7-6.95-2.81-1.61-5.83-2.42-9.06-2.42s-6.2.79-8.93,2.36c-2.73,1.57-4.94,3.87-6.64,6.88-1.69,3.02-2.54,6.6-2.54,10.73s.85,7.75,2.54,10.85c1.69,3.1,3.93,5.48,6.7,7.13,2.77,1.66,5.72,2.48,8.87,2.48s6.24-.81,9.06-2.42c2.81-1.61,5.04-3.93,6.7-6.95,1.65-3.02,2.48-6.64,2.48-10.85s-.83-7.83-2.48-10.85Z"></path>
                                    <path class="cls-4n" d="m591.93,102.01h-34.48l-5.71,16.5h-18.24l31.14-86.71h20.22l31.13,86.71h-18.36l-5.71-16.5Zm-4.71-13.89l-12.53-36.22-12.53,36.22h25.06Z"></path>
                                    <path class="cls-4n" d="m645.02,31.93v86.58h-17.37V31.93h17.37Z"></path>
                                  </g>
                                </g>
                              </g>
                            </svg>
					</div>
				</div>

				<!-- <div class="col-12 mt20 mt-md40 text-center">
					<div class="f-16 f-md-18 w700 lh160 white-clr"><span>Grab My 20 Exclusive Bonuses Before the Deal Ends…</span></div>
				</div> -->

				<div class="col-12 text-center lh150 mt20 mt-md40">
                    <div class="pre-heading-box f-18 f-md-22 w600 lh140 white-clr">
					Legally... Swipe Big Guru's Secret Funnels with Our “ChatGPT4 Secret Agent”.
					</div>
                </div>

				<div class="col-12 mt-md30 mt20 f-md-42 f-28 w400 text-center white-clr lh140">
                  World's First Google-Killer Ai App Builds Us <br class="d-none d-md-block">
                  <span class="orange-clr w700">DFY Profitable Affiliate Funnel Sites, Prefilled with Content, Reviews &amp; Lead Magnet in Just 60 Seconds.</span>
               </div>
			  
			   <div class="col-12 mt20 text-center">
                  <div class="f-22 f-md-28 w400 lh140 white-clr">
                     &amp; Making Us <span class="w600 underline">$955.45 Commissions Daily</span>  with Literally Zero Work                 
                  </div>
               </div>
			   <div class="col-12 mt15 text-center">
			  		<div class="f-20 f-md-22 w400 lh140 red-bg white-clr">
					  No Designing, No Copywriting, &amp; No Hosting…. Even A 100% Beginner Can Do It.
                  	</div>
               </div>
				
            </div>
            <div class="row mt20 mt-md30">
                    <div class="video-frame col-12 col-md-10 mx-auto">
					<img src="assets/images/productbox.png" class="img-fluid mx-auto d-block">
					   <!-- <div class="responsive-video">
						   <iframe src="https://coursesify.dotcompal.com/video/embed/o4eartift7" style="width:100%; height:100%" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
					   </div> -->
                </div>
            </div>
        </div>
    </div>
	
	<div class="second-section">
         <div class="container">
            <div class="row">
			<div class="col-12 col-md-6 ">
				<div class="f-16 f-md-20 lh160 w600">
					<ul class="kaptick pl0">
					<li>Eliminate All The Guesswork and Testing. And jump straight to results </li>
						<li>Let Ai Find Hundreds Of The Guru's Profitable Funnels</li>
						<li>Let Ai Create Proven Money Making Funnels On Autopilot</li>
						<li>All The Copywriting And Designing Is Done For You</li>
						<li>Generate 100% SEO &amp; Mobile Optimized Funnels</li>
						<li>Instant High Converting Traffic For 100% Free… No Need To Run Ads</li>
						<li>98% Of Beta Testers Made At Least One Sale <span class="underline">Within 24 Hours</span> Of Using Ninja AI. </li>
                    </ul>
				</div>
            </div>
			<div class="col-12 col-md-6">
				<div class="f-16 f-md-20 lh160 w600">
					<ul class="kaptick pl0">
					<li>A True Ai App That Leverages ChatGPT4</li>
						<li>Connect Your Favourite Autoresponders And Build Your List FAST.</li>
                        <li>Seamless Integration With Any Payment Processor You Want</li>
						<li>ZERO Upfront Cost</li>
						<li>No Complicated Setup - Get Up And Running In 2 Minutes</li>
						
						<li>So Easy, Anyone Can Do it.</li>
						<li>30 Days Money-Back Guarantee</li>
						<li>Free Commercial License Included - That's Priceless</li>
					</ul>
				</div>
			</div>
        </div>
            <div class="row mt20 mt-md30">
               <!-- CTA Btn Section Start -->
                <!-- CTA Btn Section Start -->
				<div class="col-md-12 col-md-12 col-12 text-center">
					<div class="f-md-26 f-18 text-center black lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
					<div class="f-md-24 f-22 text-center lh120 w700 mt15 mt-md20 purple-clr">TAKE ACTION NOW!</div>
					<div class="f-md-22 f-17 lh120 w600 mt15 mt-md20 black-clr">Use Special Coupon <span class=" purple-clr">"ADMININJA"</span> For <span class="purple-clr">30% Off</span> On Full Funnel </div>
				</div>
				<div class="col-md-12 col-12 text-center mt15 mt-md20">
					<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
						<span class="text-center">Grab Ninja Ai + My 20 Exclusive Bonuses</span> <br>
					</a>
				</div>
				<div class="col-12 mt15 mt-md20 text-center">
					<img src="assets/images/payment.png" class="img-fluid">
				</div>
				<div class="col-12 mt15 mt-md20" align="center">
					<h3 class="f-md-28 f-20 w500 text-center black">Coupon Is Expiring In... </h3>
				</div>
				<!-- Timer -->
				<div class="col-12 text-center">
					<div class="countdown counter-black">
						<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Days</span> </div>
						<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
						<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
						<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
					</div>
				</div>
				<!-- Timer End -->
            </div>
         </div>
	</div>
  <!-- Step Section End -->
  <div class="step-section">
      <div class="container ">
         <div class="row ">
            <div class="col-12 f-28 f-md-50 w700 lh140 black-clr text-center">
               All It Takes Is 3 Fail-Proof Steps<br class="d-none d-md-block">
            </div>
            <div class="col-12 f-24 f-md-28 w600 lh140 white-clr text-center mt20">
               <div class="post-heading">
			   Start Dominating ANY Niche With DFY AI Funnels…
               </div>
            </div>
            <div class="col-12 mt30 mt-md90">
               <div class="row align-items-center ">
                  <div class="col-12 col-md-6">
                     <div class="step-shapes f-20 f-md-22 w600 white-clr">
                        Step 1
                     </div>
                     <div class="f-34 f-md-65 w700 lh140 mt15 caveat">
                        Access
                     </div>
                     <div class="f-20 f-md-24 w400 lh140 mt5 mt-md10">
					 Click On Any Of The Links Below To Get Instant Access To NinjaAI For A Low 1-Time Fee
                     </div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 ">
                  <img src="assets/images/video-thumb.webp" alt="Video" class="mx-auto d-block img-fluid">
                     <!-- <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 1px solid #a94eff;">
                        <source src="assets/images/step1.mp4" type="video/mp4">
                     </video> -->
                  </div>
               </div>
            </div>
            <div class="col-12">
               <img src="assets/images/left-arrow.webp" class="img-fluid d-none d-md-block mx-auto " alt="Left Arrow">
            </div>
            <div class="col-12 mt30 mt-md30">
               <div class="row align-items-center ">
                  <div class="col-12 col-md-6 order-md-2">
                     <div class="step-shapes f-20 f-md-22 w600 white-clr">
                        Step 2
                     </div>
                     <div class="f-34 f-md-65 w700 lh140 mt15 caveat">
					 Choose
                     </div>
                     <div class="f-20 f-md-24 w400 lh140 mt5 mt-md10">
					 Choose Your Niche, And Let The AI Take Care Of Everything For You…
                     </div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 ">
                 	 <img src="assets/images/video-thumb.webp" alt="Video" class="mx-auto d-block img-fluid">
                     <!-- <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 1px solid #a94eff;">
                        <source src="assets/images/step1.mp4" type="video/mp4">
                     </video> -->
                  </div>
               </div>
            </div>
            <div class="col-12">
               <img src="assets/images/right-arrow.webp" class="img-fluid d-none d-md-block mx-auto" alt="Right Arrow">
            </div>
            <div class="col-12 mt30 mt-md30">
               <div class="row align-items-center ">
                  <div class="col-12 col-md-6">
                     <div class="step-shapes f-20 f-md-22 w600 white-clr">
                        Step 3
                     </div>
                     <div class="f-34 f-md-65 w700 lh140 mt15 caveat">
                        Profit
                     </div>
                     <div class="f-20 f-md-24 w400 lh140 mt5 mt-md10">
					 That's It, There Is No Technical Setup, There Is No Learning Curve…
							<br>
							And We Daily Make Money Like This.
                     </div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 ">
                  <img src="assets/images/video-thumb.webp" alt="Video" class="mx-auto d-block img-fluid">
                     <!-- <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 1px solid #a94eff;">
                        <source src="assets/images/step1.mp4" type="video/mp4">
                     </video> -->
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
	<!-----step section end---------->
	<!-----proof section---------->
	<div class="cta-btn-section dark-cta-bg">
	   <div class="container">
		  <div class="row">
			 <div class="col-md-12 col-md-12 col-12 text-center">
				<div class="f-md-30 f-18 text-center mt3 white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
				<div class="f-md-22 f-22 text-center purple lh120 w700 mt15 mt-md20 orange-clr">TAKE ACTION NOW!</div>
				<div class="f-md-22 f-17 lh120 w600 mt15 mt-md20 white-clr">Use Special Coupon <span class="orange-clr">"ADMININJA"</span> For <span class="orange-clr">30% Off</span> On Full Funnel </div>
			 </div>
			 <div class="col-md-12 col-12 text-center mt15 mt-md20">
			 <a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
				<span class="text-center">Grab Ninja Ai + My 20 Exclusive Bonuses</span> <br>
				</a>
			 </div>
			 <div class="col-12 mt15 mt-md20 text-center">
				<img src="assets/images/payment.png" class="img-fluid">
			 </div>
			 <div class="col-12 mt15 mt-md20" align="center">
				<h3 class="f-md-28 f-20 w500 text-center white-clr">Coupon Is Expiring In... </h3>
			 </div>
			 <!-- Timer -->
			 <div class="col-12 text-center">
				<div class="countdown counter-white white-clr">
				   <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Days</span> </div>
				   <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
				   <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
				   <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
				</div>
			 </div>
			 <!-- Timer End -->
		  </div>
	   </div>
	</div>
	<!---proof section end---->
	  <!-- Video Testimonial -->
	  <div class="profitwall-sec">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="col-12 f-28 f-md-38 w400 lh140 black-clr text-center">
               Ninja AI Leveraged ChatGPT4 To Eliminate ANY Guesswork… 
               </div>
               <div class="col-12 f-22 f-md-50 w700 lh140 black-clr mt10 text-center">
               And Allowed Us To (Legally)
               </div>
               <div class="col-12 f-22 f-md-50 w400 lh140 white-clr mt10 text-center">
                  <div class="orange-bg">
                     Steal <u>PROFITABLE</u> Funnels
                  </div>
               </div>
            </div>
         </div>


         <div class="row align-items-center mt20 mt-md80 white-bg">
            <div class="col-12 col-md-7 order-md-2">
               <div class="f-22 f-md-38 w700 lh140 black-clr text-center text-md-start">
                  Select A Niche
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
                  Choose any niche you want inside NinjaAI easy-to-use dashboard
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
                  NinjaAI supports ANY niche you can imagine
               </div>
            </div>
            <div class="col-12 col-md-5 mt20 mt-md0 order-md-1">
               <img src="assets/images/profitable-img1.png" alt="imagine-img" class="mx-auto d-block img-fluid">
            </div>
         </div>

         <div class="row align-items-center mt20 mt-md80 purple-bgg">
            <div class="col-12 col-md-7">
               <div class="f-22 f-md-38 w700 lh140 white-clr text-center text-md-start">
                  AI Finds All The Gurus In That Niche
               </div>
               <div class="f-18 f-md-22 w400 lh140 white-clr text-center text-md-start mt20">
                  On the background. Our AI model will scour your niche…
               </div>
               <div class="f-18 f-md-22 w400 lh140 white-clr text-center text-md-start mt20">
                  And find the best gurus in that niche and what exactly are they doing…
               </div>
               <div class="f-18 f-md-22 w400 lh140 white-clr text-center text-md-start mt20">
                  To make money
               </div>
            </div>
            <div class="col-12 col-md-5 mt20 mt-md0">
               <img src="assets/images/profitable-img2.png" alt="imagine-img" class="mx-auto d-block img-fluid">
            </div>
         </div>

         <div class="row align-items-center mt20 mt-md80 white-bg">
            <div class="col-12 col-md-7 order-md-2">
               <div class="f-22 f-md-38 w700 lh140 black-clr text-center text-md-start">
                  AI “LEGALLY” Steal Their Profitable Funnels
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
                  Our AI model will swipe their funnels…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
                  Improve on it And give it to you in a template that you can swipe with just 1 click.
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
                  You don't have to write or design anything.
               </div>
            </div>
            <div class="col-12 col-md-5 mt20 mt-md0 order-md-1">
               <img src="assets/images/profitable-img3.png" alt="imagine-img" class="mx-auto d-block img-fluid">
            </div>
         </div>

         <div class="row align-items-center mt20 mt-md80 purple-bgg">
            <div class="col-12 col-md-7">
               <div class="f-22 f-md-38 w700 lh140 white-clr text-center text-md-start">
                  AI Gives You The Profitable Funnel
               </div>
               <div class="f-18 f-md-22 w400 lh140 white-clr text-center text-md-start mt20">
                  Now we have a profitable funnel…
               </div>
               <div class="f-18 f-md-22 w400 lh140 white-clr text-center text-md-start mt20">
                  Our AI model will start sending massive amount of targeted clicks to it now…
               </div>
               <div class="f-18 f-md-22 w400 lh140 white-clr text-center text-md-start mt20">
                  On complete autopilot. Without paying a penny in ads.
               </div>
            </div>
            <div class="col-12 col-md-5 mt20 mt-md0">
               <img src="assets/images/profitable-img4.png" alt="imagine-img" class="mx-auto d-block img-fluid">
            </div>
         </div>

         <div class="row align-items-center mt20 mt-md80 white-bg">
            <div class="col-12 col-md-7 order-md-2">
               <div class="f-22 f-md-38 w700 lh140 black-clr text-center text-md-start">
                  We Sit Back And Profit
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
                  That's it.
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
                  The entire process takes less than 20 seconds.
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
                  Then, we just watch sales hammers our account
               </div>
            </div>
            <div class="col-12 col-md-5 mt20 mt-md0 order-md-1">
               <img src="assets/images/profitable-img5.png" alt="imagine-img" class="mx-auto d-block img-fluid">
            </div>
         </div>
      </div>
   </div>
    
<!-- Video Testimonial -->

<div class="exclusive-bonus">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <div class="white-clr f-24 f-md-36 lh140 w600">
                  Exclusive Bonus #1 : Aicademy
               </div>
               <div class="f-18 f-md-20 w500 mt20 white-clr">
                  20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-------Exclusive Bonus----------->
 <div class="header-section-aicademy">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                  <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 644.49 120" style="max-height:55px">
                     <defs>
                        <style>
                           .cls-1a {
                           fill: #fff;
                           }
                     
                           .cls-2a {
                           fill: #0af;
                           }
                        </style>
                     </defs>
                     <g id="Layer_1-2" data-name="Layer 1">
                        <g>
                           <g>
                              <circle class="cls-2a" cx="79.02" cy="43.5" r="7.25"></circle>
                              <circle class="cls-2a" cx="70.47" cy="78.46" r="5.46"></circle>
                              <path class="cls-2a" d="m158.03,45.28l-33.74,13.31v36.4l-45.27,16.86-27.9-10.4c-4.17,3.37-7.04,6.74-8.15,10.11-1.01,3.22-.07,6.21,2.46,8.42-5.44-1.46-8-5.25-6.96-9.76,1.24-3.62,4.79-7.68,10-12.08h.01c3.95-3.31,8.84-6.84,14.46-10.5,2.04,1.67,4.68,2.68,7.53,2.68,6.56,0,11.88-5.32,11.88-11.88s-5.32-11.88-11.88-11.88-11.88,5.32-11.88,11.88c0,1.13.15,2.21.46,3.25-8.01,5.01-14.83,10.36-19.57,15.44l-5.75-2.14v-7.66c-7.84,4.92-11.84,11.06-5.99,16.99-5.17-1.64-7.69-4.49-7.69-8.05,0-4.83,4.67-10.99,13.69-17.26l35.41-23.23c2.71,2.18,6.13,3.48,9.87,3.48,8.7,0,15.76-7.06,15.76-15.76s-7.06-15.76-15.76-15.76-15.76,7.06-15.76,15.76c0,1.59.23,3.12.68,4.57l-30.2,19.53v-9L0,45.28,79.02,0l79.01,45.28Z"></path>
                              <path class="cls-2" d="m149.38,68.32v-19.62l-1.51.6v19.03c-1.66.37-2.9,1.96-2.9,3.85v11.69h7.31v-11.69c0-1.9-1.24-3.48-2.9-3.85Z"></path>
                           </g>
                           <g>
                              <g>
                                <path class="cls-2a" d="m178.03,98.08l22.25-71.1h19.3l22.25,71.1h-17.01l-4.36-16.9h-21.7l-4.36,16.9h-16.36Zm25.74-35.99l-1.74,6.54h15.16l-1.64-6.54c-.95-3.56-1.91-7.34-2.89-11.34-.98-4-1.95-7.85-2.89-11.56h-.44c-.87,3.78-1.76,7.65-2.67,11.61-.91,3.96-1.87,7.73-2.89,11.29Z"></path>
                                <path class="cls-2a" d="m256.22,36.03c-2.69,0-4.87-.76-6.54-2.29-1.67-1.53-2.51-3.56-2.51-6.11s.83-4.58,2.51-6.11c1.67-1.53,3.85-2.29,6.54-2.29s4.87.76,6.54,2.29c1.67,1.53,2.51,3.56,2.51,6.11s-.84,4.58-2.51,6.11c-1.67,1.53-3.85,2.29-6.54,2.29Zm-7.96,62.05v-54.09h16.03v54.09h-16.03Z"></path>
                              </g>
                              <g>
                                <path class="cls-1a" d="m299.89,99.38c-5.02,0-9.54-1.11-13.58-3.33-4.03-2.22-7.23-5.45-9.6-9.71-2.36-4.25-3.54-9.36-3.54-15.32s1.31-11.16,3.93-15.38c2.62-4.22,6.07-7.43,10.36-9.65,4.29-2.22,8.9-3.33,13.85-3.33,3.34,0,6.31.55,8.89,1.64,2.58,1.09,4.89,2.47,6.92,4.14l-7.52,10.36c-2.55-2.11-4.98-3.16-7.31-3.16-3.85,0-6.92,1.38-9.21,4.14-2.29,2.76-3.44,6.51-3.44,11.23s1.15,8.38,3.44,11.18c2.29,2.8,5.18,4.2,8.67,4.2,1.74,0,3.45-.38,5.13-1.15,1.67-.76,3.2-1.69,4.58-2.78l6.33,10.47c-2.69,2.33-5.6,3.98-8.72,4.96-3.13.98-6.18,1.47-9.16,1.47Z"></path>
                                <path class="cls-1a" d="m339.81,99.38c-4.94,0-8.87-1.58-11.78-4.74-2.91-3.16-4.36-7.03-4.36-11.61,0-5.67,2.4-10.1,7.2-13.3,4.8-3.2,12.54-5.34,23.23-6.43-.15-2.4-.86-4.31-2.13-5.73-1.27-1.42-3.4-2.13-6.38-2.13-2.25,0-4.54.44-6.87,1.31-2.33.87-4.8,2.07-7.42,3.6l-5.78-10.58c3.42-2.11,7.07-3.82,10.96-5.13,3.89-1.31,7.94-1.96,12.16-1.96,6.91,0,12.21,2,15.92,6,3.71,4,5.56,10.14,5.56,18.43v30.97h-13.09l-1.09-5.56h-.44c-2.25,2.04-4.67,3.69-7.25,4.96-2.58,1.27-5.4,1.91-8.45,1.91Zm5.45-12.43c1.82,0,3.4-.42,4.74-1.25,1.34-.83,2.71-1.94,4.09-3.33v-9.49c-5.67.73-9.6,1.87-11.78,3.44-2.18,1.56-3.27,3.4-3.27,5.51,0,1.74.56,3.04,1.69,3.87,1.13.84,2.63,1.25,4.53,1.25Z"></path>
                                <path class="cls-1a" d="m402.95,99.38c-6.69,0-12.05-2.53-16.09-7.58-4.03-5.05-6.05-11.98-6.05-20.77,0-5.89,1.07-10.96,3.22-15.21,2.14-4.25,4.94-7.51,8.4-9.76,3.45-2.25,7.07-3.38,10.85-3.38,2.98,0,5.49.51,7.52,1.53,2.04,1.02,3.96,2.4,5.78,4.14l-.66-8.29v-18.43h16.03v76.45h-13.09l-1.09-5.34h-.44c-1.89,1.89-4.11,3.47-6.65,4.74-2.55,1.27-5.13,1.91-7.74,1.91Zm4.14-13.09c1.74,0,3.33-.36,4.74-1.09,1.42-.73,2.78-2,4.09-3.82v-22.14c-1.38-1.31-2.85-2.22-4.42-2.73-1.56-.51-3.07-.76-4.53-.76-2.55,0-4.8,1.22-6.76,3.65-1.96,2.44-2.94,6.23-2.94,11.4s.85,9.21,2.56,11.72c1.71,2.51,4.13,3.76,7.25,3.76Z"></path>
                                <path class="cls-1a" d="m470.34,99.38c-5.16,0-9.81-1.13-13.96-3.38-4.14-2.25-7.42-5.49-9.81-9.71-2.4-4.22-3.6-9.31-3.6-15.27s1.22-10.94,3.65-15.16c2.43-4.22,5.62-7.47,9.54-9.76,3.93-2.29,8.03-3.44,12.32-3.44,5.16,0,9.43,1.15,12.81,3.44,3.38,2.29,5.92,5.38,7.63,9.27,1.71,3.89,2.56,8.31,2.56,13.25,0,1.38-.07,2.74-.22,4.09-.15,1.35-.29,2.34-.44,3h-32.39c.73,3.93,2.36,6.82,4.91,8.67,2.54,1.85,5.6,2.78,9.16,2.78,3.85,0,7.74-1.2,11.67-3.6l5.34,9.71c-2.76,1.89-5.85,3.38-9.27,4.47-3.42,1.09-6.73,1.64-9.92,1.64Zm-12-34.24h19.52c0-2.98-.71-5.43-2.13-7.36-1.42-1.93-3.73-2.89-6.92-2.89-2.47,0-4.69.86-6.65,2.56-1.96,1.71-3.24,4.27-3.82,7.69Z"></path>
                                <path class="cls-1a" d="m502.62,98.08v-54.09h13.09l1.09,6.98h.44c2.25-2.25,4.65-4.2,7.2-5.83,2.54-1.64,5.6-2.45,9.16-2.45,3.85,0,6.96.78,9.32,2.34,2.36,1.56,4.23,3.8,5.62,6.71,2.4-2.47,4.94-4.6,7.63-6.38,2.69-1.78,5.82-2.67,9.38-2.67,5.82,0,10.09,1.95,12.81,5.83,2.73,3.89,4.09,9.21,4.09,15.98v33.59h-16.03v-31.52c0-3.93-.53-6.61-1.58-8.07-1.05-1.45-2.74-2.18-5.07-2.18-2.69,0-5.78,1.74-9.27,5.23v36.53h-16.03v-31.52c0-3.93-.53-6.61-1.58-8.07-1.05-1.45-2.74-2.18-5.07-2.18-2.69,0-5.74,1.74-9.16,5.23v36.53h-16.03Z"></path>
                                <path class="cls-1a" d="m602.07,119.23c-1.6,0-3-.11-4.2-.33-1.2-.22-2.34-.47-3.44-.76l2.84-12.21c.51.07,1.09.2,1.74.38.65.18,1.27.27,1.85.27,2.69,0,4.76-.65,6.22-1.96,1.45-1.31,2.54-3.02,3.27-5.13l.76-2.84-20.83-52.67h16.14l7.74,23.23c.8,2.47,1.53,4.98,2.18,7.52.65,2.55,1.34,5.16,2.07,7.85h.44c.58-2.54,1.18-5.11,1.8-7.69.62-2.58,1.25-5.14,1.91-7.69l6.54-23.23h15.38l-18.76,54.63c-1.67,4.51-3.53,8.31-5.56,11.4-2.04,3.09-4.49,5.4-7.36,6.92-2.87,1.53-6.45,2.29-10.74,2.29Z"></path>
                              </g>
                           </g>
                        </g>
                     </g>
                  </svg>
               </div>
               
               <div class="col-12 text-center lh150 mt20 mt-md70">
                  <div class="pre-heading f-18 f-md-22 w600 lh140 white-clr">
                     We Exploited ChatGPT’s AI To Get A Share Of A $300 Billion Industry…
                  </div>
               </div>
               <div class="col-12 mt-md30 mt20 f-md-46 f-28 w400 text-center white-clr lh140">
                  World's First App <span class="w700 yellow-clr"> Builds Us DFY Auto-Updating Academy Sites, Prefill Them With 50,000+ Smoking Hot AI Courses...</span>
               </div>
               <div class="col-12 mt-md15 mt15 text-center ">
                  <div class="f-22 f-md-28 w700 lh140 white-clr">
                  Then Promote Them To 495 MILLION Buyers For 100% Free…
                  </div>
               </div>
               <div class="col-12 mt20 text-center">
                  <div class="f-22 f-md-28 w700 lh140 white-clr post-heading-aicademy">
                  Resulting In $872.36 Daily With Zero Work
                  </div>
               </div>
               <div class="col-12 mt20 text-center">
                  <div class="f-20 f-md-22 w500 lh140 white-clr">
                  Without Us Creating Anything…. Even A 100% Beginner Or Camera SHY Can Do It.
                  </div>
               </div>
               <div class="col-12 mt15 text-center">
                  <div class="f-20 f-md-22 w600 lh140 yellow-clr">
                  No Tech Skills Needed - No Experience Required - No Hidden Fees - No BS
                  </div>
               </div>
            </div>

            <div class="row mt20 mt-md40 gx-md-5">
               <div class="col-md-7 col-12">
                  <div class="col-12">
                     <div class="f-18 f-md-19 w600 lh140 white-clr shape-head-aicademy">
                        Watch As We Build udemy-like website, And Sell $13,454.23 Worth Of Courses In 10 Days…    
                     </div>
                     <!-- <img src="assets/images/video-thumb.webp" alt="Video" class="mx-auto d-block img-fluid mt20 mt-md30"> -->
                     <div class="video-box mt20 mt-md30">
                        <div style="padding-bottom: 56.25%;position: relative;">
                        <iframe src="https://aicademy.oppyo.com/video/embed/6bfo4m9lu4" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                        </div>
                     </div>
                  </div>
                  
               </div>
               <div class="col-md-5 col-12 mt20 mt-md0">
                  <div class="key-features-bg">
                     <ul class="list-head-aicademy pl0 m0 f-16 f-md-18 lh160 w400 white-clr">
                        <li>Tap Into $300 Billion Industry With Just 3 Steps</li>
                        <li>Let AI Create Hundreds Of Courses For You On Autopilot</li>
                        <li>Ai Finds Best Profitable Niches Ideas for You</li>
                        <li>Ai Creates Our Websites Too, No Tech Setup Whatsoever</li>
                        <li>50,000+ SEO Optimized Done For You Smoking Hot AI Courses</li>
                        <li class="w600">Even Traffic Is DFY. Without Spending A Dime</li>
                        <li>Integrate Any Payment Processor Your Want</li>
                        <li>No Complicated Setup - Get Up And Running In 2 Minutes</li>
                        <li>Build Your List With Autoresponder Integration</li>
                        <li>AI ChatBots That Will Handle All Customer Support For You</li>
                        <li>So Easy, Anyone Can Do it.</li>
                        <li>Cancel All Your Costly Subscriptions</li>
                        <li>ZERO Upfront Cost</li>
                        <li>30 Days Money-Back Guarantee</li>
                        <li>Free Commercial License Included - That's Priceless</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
	<div class="gr-1">
      <div class="container-fluid p0">
         <picture>
            <source media="(min-width:768px)" srcset="assets/images/aicademy.webp">
            <source media="(min-width:320px)" srcset="assets/images/aicademy-mview.webp">
            <img src="assets/images/aicademy.webp" alt="Steps" style="width:100%;">
         </picture>
      </div>
   </div>
   
	<!-------Exclusive Bonus----------->
<div class="exclusive-bonus">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <div class="white-clr f-24 f-md-36 lh140 w600">
                  Exclusive Bonus #2 : WebGenie
               </div>
               <div class="f-18 f-md-20 w500 mt20 white-clr">
                  20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-------Exclusive Bonus----------->
   <div class="header-section-webgenie">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
            <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 705.07 130" style="max-height: 60px;">
                     <defs>
                       <style>
                         .cls-1w {
                           fill: #fff;
                         }
                   
                         .cls-2w {
                           fill: #fc8548;
                         }
                   
                         .cls-3w {
                           fill: #15c6fc;
                         }
                       </style>
                     </defs>
                     <g id="Layer_1-2" data-name="Layer 1">
                       <g>
                         <g>
                           <path class="cls-3w" d="m106.3,101.8c-15.16,10.74-31.47,19.22-46.87,24.66,5.84,1.82,12.05,2.8,18.49,2.8,32.56,0,59.25-25.09,61.82-56.98-9.69,10.65-21.17,20.83-33.44,29.52Z"></path>
                           <path class="cls-3w" d="m131.29,50.75c-1.46-2.75-5.89-11.12-4.97-15.56.43-2.08,2.28-1.79,4.61.7.51.54.91.95,1.23,1.26.76,1.37,1.48,2.77,2.14,4.21-.7-.7-1.36-.95-1.91-.49-1.58,2.28,1.02,10.37,1.05,10.45.19.56.35,1.09.49,1.6-.4.47-.81.94-1.22,1.41-.34-1.22-.8-2.42-1.41-3.58Zm-113.89,30.07c-.97-4.37-1.49-8.91-1.49-13.57,0-10.04,2.39-19.53,6.63-27.92-.53,1.52-.72,2.51-.72,2.51h0c-.07.75-.1,1.15-.04,1.61.42,3.43,4.17,1.59,7.87-3.89.06-.09,6.17-9.63,4.38-12.75-.49-.3-1.06-.32-1.82,0-1.37.86-2.56,1.83-3.6,2.85,5.59-7.32,12.77-13.36,21.03-17.6-2.43,1.69-3.77,3.22-3.77,3.22h0c-.44.61-.71,1.01-.9,1.44-1.38,3.17,2.76,3.53,8.74.69.1-.05,10.22-5.18,10.26-8.78-.27-.51-.73-.83-1.55-.94-.86.03-1.69.1-2.5.22,3.93-1.19,8.03-2,12.25-2.39-1.04.21-1.7.51-1.7.85,0,.62,2.17,1.12,4.86,1.12s4.86-.5,4.86-1.12c0-.54-1.67-1-3.89-1.1.55-.01,1.1-.02,1.65-.02,17.08,0,32.55,6.91,43.77,18.08.37.64.9,1.48,1.65,2.59,3.36,5.03,1.75,6.47-.79,5.21-3.97-1.96-8.77-7.73-10.34-9.62-1.61-1.94-4.03-3.88-6.47-5.38-.42-.13-.85-.26-1.26-.43-.75-.3-1.5-.6-2.27-.85-.24,0-.48-.02-.72-.01-.46.02-1.3.03-1.41.62-.08.44.15.97.32,1.36.42.95,1.02,1.82,1.58,2.69.51.8.99,1.67,1.64,2.36.59.62,1.29,1.11,1.84,1.77.06.07.09.15.11.23,2.46,2.16,5.15,3.79,7.43,3.8,3.84.02,17.78,10.55,9.91,19.31-3.06,3.41-2.18,11.92.78,16.83-4.75,4.65-9.96,9.26-15.54,13.76-.71-.62-1.24-1.49-1.64-2.65-.28-1.05-.55-2.1-.82-3.17-.47-2.74-.15-2.56,2.9-8.5,3.7-7.2-4.29-13.57-11.24-11.68-7.7,2.08-10.61,13.04-2.47,16.92.04,0,3.93.97,5.47,5.74.22.88.44,1.75.67,2.62.51,2.64.17,4.62-1.3,7.21-1.23.9-2.47,1.8-3.72,2.69-7.5,5.31-15.16,10.09-22.71,14.23-.91-3.2.33-6.08.85-6.84,2.11-3.06,3.7-4.14,7.39-5.01,1.14-.27,3.26-.77,5.09-2.42,6.2-5.58,4.7-18.32-6.72-19.11-7.22.35-11.57,8.15-8.61,15.47,2.2,5.45.52,9.12-.68,10.82-1.03,1.46-4.01,4.69-9.6,3.2-4.95-1.32-6.61-6.92-11.18-8.73-7.77-3.09-11.6,5.37-7.27,11.76,3.24,4.79,6.49,6.02,8.64,6.21,2.23.2,3.34-.51,4.08-.97,1.51-.95,1.99-2.85,3.5-3.8,2.73-1.73,7.54-1.65,11.19,1.22-2.99,1.57-5.95,3.04-8.86,4.39-.05,0-.09,0-.14.01-1.76.17-3.05.83-3.86,1.79-3.1,1.35-6.15,2.56-9.11,3.62-.05-.13-.09-.27-.14-.4-2.09-5.4-7.96-8.45-9.6-14.13-.31-1.27-.58-2.58-.81-3.91-.19-1.49-.58-4.58,1.8-7.18,4.3-4.72,3.76-16.42-4.35-19.62-8.03-.92-10.21,9.81-5.52,17.26.02,0,2.56,3.13,3.92,8.96.21,1.02.44,2.02.69,3.01.34,1.51.97,4.33-.15,6.41,0,0-.02,0-.03,0-1.57,3.43,3.31,8.91,7.67,11.78-.87.26-1.74.51-2.59.74-7.91-6.41-14.2-14.73-18.19-24.25.11.17.22.33.33.51,0,0,.06.1.17.27.05.08.1.16.16.24.59.87,1.9,2.67,3.43,3.83,3.14,2.36,5.07.48,4.49-4.38-.01-.1-1.53-10.46-8.2-14.5-1.63-.57-2.56-.27-3.23,1.12-.01.03-.39.92-.46,2.52Zm59.78-22.16c0-5.16-4.18-9.34-9.34-9.34s-9.34,4.18-9.34,9.34,4.18,9.34,9.34,9.34,9.34-4.18,9.34-9.34Zm20.01-20.24c-.99.12-1.99.23-3,.33q-2.56.07-7.37-3.76c-6.07-4.83-11.86,2.62-11.06,9.21.89,7.3,9.43,10.91,14.03,3.99,0-.03,1.4-3.46,5.95-4.24.83-.08,1.66-.17,2.48-.27,2.75-.13,4.63.58,7.16,2.74.02.05,1.12,1.06,2.31,1.67,5.4,2.74,9.26-1.79,7-8.21-1.03-2.91-5.1-9.19-10.54-8.84-1.45.47-2.36,1.51-2.71,3.1-.55,2.56-1.79,3.8-4.27,4.28Zm-2.86-28.9c3.81,1.68,2.69.64,8.36,3.46,5.67,2.82,5.71,1.59,1.86-1.26-3.85-2.86-12.22-4.75-12.22-4.75-3.3-.41-1.81.87,2,2.55Zm-7.76.64s1.78,3.84,6.7,7.38c4.91,3.54,7.42,1.79,3.73-3.84-3.69-5.63-12.28-7.25-10.42-3.54Zm3.83,22.99c5.69,3.02,10.81-.48,8.6-6.63-2.21-6.15-11.64-11.3-13.31-3.27,0,0-.98,6.87,4.72,9.89Zm-29.35-12.88c1.14,3.13,7.38-3.8,10.18-3.93,2.8-.14,3.62.55,5.18,3.69,1.56,3.14,7.83,2.91,6.91-3.13-.93-6.04-4.76-7.26-7.14-5.14-2.38,2.12-4.01.96-4.94-.67s-4.12-.93-8.44,3.49c0,0-2.89,2.57-1.75,5.69Zm-21.21,15.26c.95,4.71,7.81,3.19,12.26-4.31,1.08-1.82,4.46-8.89,1.25-10.18-4.96-.88-14.73,8.46-13.51,14.49Zm-12.46,24.36c.42,1.17,3.89,6.17,9.04.12,3.77-4.42,7.76-2.55,8.53-2.14,3.4,1.87,6.59,8.49,3.46,15.56-1.35,3.04-1.15,8.01,1.84,11.62,5.21,6.29,13.94,3.93,15.33-4.15.07-.4,1.57-9.85-7.82-12.86-5.42-1.74-7.7-6.56-6.1-12.92.28-1.13,1.66-4.03,4.12-5.78,2.18-1.56,4.99-1.19,7.29-2.38,5.97-3.09,7.53-15.39.59-17.07-3.95-.31-9.49,5.43-9.53,11.51-.05,7.82-4.68,11.09-6.1,11.91-2.27,1.31-7.56,1.9-8.52-4.86-.66-4.71-3.02-5.36-4.37-5.33-4.27.1-10.81,8.28-7.75,16.77Zm-7.62,5.64c.03-.1,2.79-10.55,1.54-15.37-.44-.77-.9-.58-1.6.71-.04.08-2.4,4.62-3.06,13.03-.01.22-.03.42-.04.65,0,0,0,.11-.02.29,0,.09-.01.17-.02.26-.06.94-.14,2.88.08,4.21.44,2.73,1.79,1.1,3.13-3.78Z"></path>
                           <path class="cls-2w" d="m162.67,10.07c-7.36-10.39-23-12.38-42.41-7.63-.97-.64-2.26-.86-3.55-.51-2.23.61-3.59,2.71-3.05,4.69.54,1.98,2.78,3.09,5.01,2.48,1.5-.41,2.61-1.5,3.01-2.77,14.79-3.45,26.12-2.24,30.9,4.5,10.38,14.65-14.1,49.83-54.68,78.57-40.57,28.74-81.88,40.16-92.26,25.51-4.43-6.25-2.49-16.25,4.32-27.89-.5-1.18-.98-2.38-1.42-3.59C-.57,98.79-2.88,112.77,3.99,122.47c10.65,15.03,56.61,7.71,100.43-23.33,43.82-31.04,68.79-74.19,58.25-89.06Z"></path>
                         </g>
                         <g>
                           <g>
                             <path class="cls-1w" d="m284.28,35.17l-18.1,69.44h-20.48l-11.08-45.7-11.47,45.7h-20.48l-17.61-69.44h18.1l9.99,50.55,12.36-50.55h18.6l11.87,50.55,10.09-50.55h18.2Z"></path>
                             <path class="cls-1w" d="m344.52,81.07h-38.28c.26,3.43,1.37,6.05,3.31,7.86,1.94,1.81,4.34,2.72,7.17,2.72,4.22,0,7.15-1.78,8.8-5.34h18c-.92,3.63-2.59,6.89-5,9.79-2.41,2.9-5.43,5.18-9.05,6.83-3.63,1.65-7.68,2.47-12.17,2.47-5.41,0-10.22-1.15-14.44-3.46-4.22-2.31-7.52-5.6-9.89-9.89-2.37-4.29-3.56-9.3-3.56-15.04s1.17-10.75,3.51-15.04c2.34-4.29,5.62-7.58,9.84-9.89,4.22-2.31,9.07-3.46,14.54-3.46s10.09,1.12,14.24,3.36c4.15,2.24,7.4,5.44,9.74,9.6,2.34,4.15,3.51,9,3.51,14.54,0,1.58-.1,3.23-.3,4.95Zm-17.01-9.4c0-2.9-.99-5.21-2.97-6.92-1.98-1.71-4.45-2.57-7.42-2.57s-5.23.83-7.17,2.47c-1.95,1.65-3.15,3.99-3.61,7.02h21.17Z"></path>
                             <path class="cls-1w" d="m377.46,51c2.97-1.58,6.36-2.37,10.19-2.37,4.55,0,8.67,1.15,12.36,3.46,3.69,2.31,6.61,5.61,8.75,9.89,2.14,4.29,3.21,9.27,3.21,14.94s-1.07,10.67-3.21,14.99c-2.14,4.32-5.06,7.65-8.75,9.99-3.69,2.34-7.81,3.51-12.36,3.51-3.89,0-7.29-.78-10.19-2.32-2.9-1.55-5.18-3.61-6.83-6.18v7.72h-16.91V31.41h16.91v25.82c1.58-2.57,3.86-4.65,6.83-6.23Zm13.8,15.98c-2.34-2.41-5.23-3.61-8.66-3.61s-6.22,1.22-8.56,3.66c-2.34,2.44-3.51,5.77-3.51,9.99s1.17,7.55,3.51,9.99c2.34,2.44,5.19,3.66,8.56,3.66s6.23-1.24,8.61-3.71c2.37-2.47,3.56-5.82,3.56-10.04s-1.17-7.53-3.51-9.94Z"></path>
                             <path class="cls-3w" d="m466.98,57.13c-1.25-2.31-3.05-4.07-5.39-5.29-2.34-1.22-5.09-1.83-8.26-1.83-5.47,0-9.86,1.8-13.16,5.39-3.3,3.59-4.95,8.39-4.95,14.39,0,6.4,1.73,11.39,5.19,14.99,3.46,3.6,8.23,5.39,14.29,5.39,4.15,0,7.67-1.05,10.53-3.17,2.87-2.11,4.96-5.14,6.28-9.1h-21.47v-12.46h36.8v15.73c-1.25,4.22-3.38,8.15-6.38,11.77-3,3.63-6.81,6.56-11.42,8.8-4.62,2.24-9.83,3.36-15.63,3.36-6.86,0-12.98-1.5-18.35-4.5-5.38-3-9.56-7.17-12.56-12.51-3-5.34-4.5-11.44-4.5-18.3s1.5-12.97,4.5-18.35c3-5.37,7.17-9.56,12.51-12.56,5.34-3,11.44-4.5,18.3-4.5,8.31,0,15.32,2.01,21.02,6.03,5.7,4.02,9.48,9.59,11.33,16.72h-18.7Z"></path>
                             <path class="cls-3w" d="m547.99,81.07h-38.28c.26,3.43,1.37,6.05,3.31,7.86,1.94,1.81,4.34,2.72,7.17,2.72,4.22,0,7.15-1.78,8.8-5.34h18c-.92,3.63-2.59,6.89-5,9.79-2.41,2.9-5.43,5.18-9.05,6.83-3.63,1.65-7.68,2.47-12.17,2.47-5.41,0-10.22-1.15-14.44-3.46-4.22-2.31-7.52-5.6-9.89-9.89-2.37-4.29-3.56-9.3-3.56-15.04s1.17-10.75,3.51-15.04c2.34-4.29,5.62-7.58,9.84-9.89,4.22-2.31,9.07-3.46,14.54-3.46s10.09,1.12,14.24,3.36c4.15,2.24,7.4,5.44,9.74,9.6,2.34,4.15,3.51,9,3.51,14.54,0,1.58-.1,3.23-.3,4.95Zm-17.01-9.4c0-2.9-.99-5.21-2.97-6.92-1.98-1.71-4.45-2.57-7.42-2.57s-5.23.83-7.17,2.47c-1.95,1.65-3.15,3.99-3.61,7.02h21.17Z"></path>
                             <path class="cls-3w" d="m606.3,55.1c3.86,4.19,5.79,9.94,5.79,17.26v32.25h-16.82v-29.97c0-3.69-.96-6.56-2.87-8.61-1.91-2.04-4.49-3.07-7.72-3.07s-5.8,1.02-7.72,3.07c-1.91,2.04-2.87,4.91-2.87,8.61v29.97h-16.91v-55.2h16.91v7.32c1.71-2.44,4.02-4.37,6.92-5.79,2.9-1.42,6.17-2.13,9.79-2.13,6.46,0,11.62,2.09,15.48,6.28Z"></path>
                             <path class="cls-3w" d="m640.77,59.74v44.87h-16.91v-44.87h16.91Z"></path>
                             <path class="cls-3w" d="m704.77,81.07h-38.28c.26,3.43,1.37,6.05,3.31,7.86,1.94,1.81,4.34,2.72,7.17,2.72,4.22,0,7.15-1.78,8.8-5.34h18c-.92,3.63-2.59,6.89-5,9.79-2.41,2.9-5.42,5.18-9.05,6.83-3.63,1.65-7.68,2.47-12.17,2.47-5.41,0-10.22-1.15-14.44-3.46-4.22-2.31-7.52-5.6-9.89-9.89-2.37-4.29-3.56-9.3-3.56-15.04s1.17-10.75,3.51-15.04c2.34-4.29,5.62-7.58,9.84-9.89,4.22-2.31,9.07-3.46,14.54-3.46s10.09,1.12,14.24,3.36c4.15,2.24,7.4,5.44,9.74,9.6,2.34,4.15,3.51,9,3.51,14.54,0,1.58-.1,3.23-.3,4.95Zm-17.01-9.4c0-2.9-.99-5.21-2.97-6.92-1.98-1.71-4.45-2.57-7.42-2.57s-5.23.83-7.17,2.47c-1.95,1.65-3.15,3.99-3.61,7.02h21.17Z"></path>
                           </g>
                           <g>
                             <path class="cls-3w" d="m613.45,33.05c-1.76.24-3.17,1.63-3.5,3.45-.48,2.66,1.41,4.37,1.49,4.45,1.5,1.45,3.85,1.42,6.61-.15.52-.3,1.07-.44,1.65-.42l.67.02c.24.29.45.55.63.8,2.9,3.88,5.97,5.37,8.03,5.94-.44.92-1.18,2.04-2.12,2.31h-3.15c0,3.09,0,5.06,0,5.06h16.98q0-5.06,0-5.06h-3.36c-.3-.11-1.38-.59-2.23-2.33,4.79-1.07,7.98-4.21,8.43-4.68,6.25-5.05,11.29-5.51,11.34-5.51.37-.03.69-.28.82-.62.12-.35.04-.75-.22-1.01l-2.04-2.03c-.21-.21-.52-.32-.81-.27-11.25,1.69-13.55.72-13.85.55-.18-.22-.45-.34-.74-.34h-11.72c-2.4,1.09-8.38.39-10.55.03-.16-.06-.32-.1-.48-.12-.65-.12-1.28-.14-1.87-.07h0Zm16.42,16.4c.53-.7.89-1.44,1.11-1.98.31,0,.5,0,.54,0,.14,0,.29,0,.42,0,.42,0,.83-.02,1.24-.06.34.84.76,1.51,1.18,2.03h-4.49Zm-17.13-9.91c-.08-.07-1.2-1.12-.91-2.7.18-.99.95-1.78,1.87-1.9.38-.05.8-.03,1.25.06.08,0,.15.03.23.06.02,0,.04,0,.06.02.24.09.46.24.67.43.64.62,1.77,1.81,2.85,3.02-.57.11-1.14.31-1.66.6-1.13.64-3.19,1.55-4.36.41h0Z"></path>
                             <path class="cls-3w" d="m638.11,30.84c.13,0,.27,0,.4.04-.7-1.53-2.06-2.74-3.73-3.33.22-.39.34-.82.34-1.29,0-1.48-1.2-2.7-2.7-2.7s-2.7,1.21-2.7,2.7c0,.48.14.92.36,1.3-1.73.61-3.1,1.89-3.79,3.48.31-.13.64-.19.98-.19h10.83Zm-5.68-5.49c.5,0,.9.4.9.9,0,.49-.39.88-.87.89h-.04c-.49,0-.88-.4-.88-.89,0-.49.4-.9.9-.9h0Z"></path>
                           </g>
                         </g>
                       </g>
                     </g>
                   </svg>
            </div>
            
            <div class="col-12 text-center mt20 mt-md50">
               <div class="pre-heading f-md-22 f-20 w600 lh140 white-clr">
                  We Exploited ChatGPT4 To Create A Better And Smarter AI Model...
               </div>
            </div>
            <div class="col-12 mt-md25 mt20 f-md-50 f-28 w700 text-center white-clr lh140">
               World's First A.I Bot <span class="orange-gradient"> Builds Us DFY Websites Prefilled With Smoking Hot Content…</span> 
            </div>
            <div class="col-12 mt-md15 mt20 f-22 f-md-28 w700 text-center lh140 white-clr text-capitalize">
               Then Promote It To 495 MILLION Buyers For 100% Free…
            </div>
            <div class="col-12 mt-md20 mt20 f-22 f-md-28 w700 text-center lh140 black-clr text-capitalize">
               <div class="head-yellow">
                  Banking Us $578.34 Daily With Zero Work
               </div> 
            </div>
            <div class="col-12 mt-md15 mt20 f-20 f-md-22 w600 text-center lh140 orange-gradient text-capitalize">
               Without Creating Anything | Without Writing Content | Without Promoting | Without Paid Ads
            </div>
            <div class="col-12 mt-md15 mt20 f-20 f-md-22 w500 text-center lh140 white-clr text-capitalize">
               No Tech Skills Needed - No Experience Required - No Hidden Fees - No BS
            </div>
            <div class="col-12 mt20 mt-md40">
               <div class="row">
                  <div class="col-md-7 col-12 min-md-video-width-left">
                     <div class="f-18 f-md-20 w600 lh140 text-center white-clr blue-design">
                     Watch How We Deploy Profitable AI-Managed Websites That Makes Us Over $17,349 Monthly In Less Than 30 Seconds…
                     </div>
                     <div class="col-12 mt20 mt-md30">
                        <div class="video-box">
                           <div style="padding-bottom: 56.25%;position: relative;">
                              <iframe src="https://webgenie.oppyo.com/video/embed/9xupab3svh" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                           </div>
                        </div>      
                     </div>
                     <div class="col-12 mt20 mt-md40">
                        <div class="border-dashed">
                           <div class="f-18 f-md-20 w500 lh140 text-center grey-clr">
                              First 99 Action Taker Get Instant Access To Our Top 5 <br class="d-none d-md-block"> Profitable DFY Websites For <span class="orange-gradient underline w700">FREE</span> 
                           </div>
                           <div class="f-18 f-md-20 w500 lh140 text-center yellow-clr">
                              (Average User Saw 475% Increase In Profit <span class="w700">worth $1,997</span>)
                           </div>
                        </div>
                     </div>

                     
                  </div>
                  <div class="col-md-5 col-12 mt20 mt-md0 min-md-video-width-right">
                     <div class="key-features-bg">
                        <ul class="list-head pl0 m0 f-18 f-md-20 lh150 w400 white-clr">
                           <li>Deploy Stunning Websites By Leveraging ChatGPT…</li>
                           <li><span class="w600">All Websites Are Prefilled With Smoking Hot, AI, Human-Like Content</span></li>
                           <li>Instant High Convertring Traffic For 100% Free</li>
                           <li><span class="w600">100% Of Beta Testers Made At Least $100 Within 24 Hours Of Using WebGenie.</span></li>
                           <li>HighTicket Offers For DFY Monetization</li>
                           <li><span class="w600">No Complicated Setup - Get Up And Running In 2 Minutes</span></li>
                           <li><span class="w600">We Let AI Do All The Work For Us.</span></li>
                           <li>Get Up And Running In 30 Seconds Or Less </li>
                           <li><span class="w600">Instantly Tap Into $1.3 Trillion Platform</span> </li>
                           <li>Leverage A Better And Smarter AI Model Than ChatGPT4</li>
                           <li><span class="w600">99.99% Up Time Guaranteed</span> </li>
                           <li>ZERO Upfront Cost</li>
                           <li><span class="w600">30 Days Money-Back Guarantee</span></li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
	<div class="gr-1">
      <div class="container-fluid p0">
         <picture>
            <source media="(min-width:768px)" srcset="assets/images/webgenie.webp">
            <source media="(min-width:320px)" srcset="assets/images/webgenie-mview.webp">
            <img src="assets/images/webgenie.webp" alt="Steps" style="width:100%;">
         </picture>
      </div>
   </div>
   <!-------Exclusive Bonus----------->


 <!-------Exclusive Bonus----------->
 <div class="exclusive-bonus">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <div class="white-clr f-24 f-md-36 lh140 w600">
                  Exclusive Bonus #3 : Vidmaster
               </div>
               <div class="f-18 f-md-20 w500 mt20 white-clr">
                  20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-------Exclusive Bonus----------->

   <div class="header-section-vidmaster">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
			   <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 858.2 150.9" style="enable-background:new 0 0 858.2 150.9; max-height: 60px;" xml:space="preserve">
                     <style type="text/css">
                        .st0{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_1_);}
                        .st1{fill-rule:evenodd;clip-rule:evenodd;fill:#3C4650;}
                        .st2{fill-rule:evenodd;clip-rule:evenodd;fill:#1C262D;}
                        .st3{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_2_);}
                        .st4{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_3_);}
                        .st5{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_4_);}
                        .st6{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_5_);}
                        .st7{fill:#FFFFFF;}
                     </style>
                     <g id="Layer_1-2">
                        <g>
                           <g>
                              <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="25.2183" y1="-779.9527" x2="12.9283" y2="-954.9427" gradientTransform="matrix(1 0 0 -1 0 -776)">
                                 <stop offset="0" style="stop-color:#CCCCCB"></stop>
                                 <stop offset="0.32" style="stop-color:#3C4650"></stop>
                                 <stop offset="0.7" style="stop-color:#252D33"></stop>
                                 <stop offset="1" style="stop-color:#181D20"></stop>
                              </linearGradient>
                              <path class="st0" d="M40.1,3.1c-14.4-6-22.8-3.9-29.9,10.8C-0.2,35.8-2.6,80.4,3,112.9c5.4,20.7,22.1,25.1,32.2,15
                              c0.7-0.3,1.5-0.6,2.1-0.9c-4.3,1.9-9.1-0.3-10.5-4.8c-7.4-23.6-6.2-70,0.3-91.8C30.9,17.5,39.4,4,40.1,3.1L40.1,3.1z"></path>
                              <path class="st1" d="M31.6,0.4C1.2,18.4,7.9,102.5,18.1,129.5c5.3,1.4,11.3,0.9,17-1.6c0.7-0.3,1.5-0.6,2.1-0.9
                              c-4.3,1.9-9.1-0.3-10.5-4.8c-7.4-23.6-4.8-64.4,1.9-88.3C32.9,18.4,39.3,4.6,40.1,3.1c-3.2-1.3-3.4-1.4-6.2-2.2L31.6,0.4
                              L31.6,0.4z"></path>
                              <path class="st2" d="M2.4,109.5c0.2,1.2,0.3,2.3,0.6,3.4c3.4,15.5,18.4,20.7,32.2,15c0.7-0.3,1.5-0.6,2.1-0.9
                              c-4.3,1.9-9.1-0.3-10.5-4.8C13.8,125.2,5.6,121.7,2.4,109.5L2.4,109.5z"></path>
                              <linearGradient id="SVGID_2_" gradientUnits="userSpaceOnUse" x1="-5.0837" y1="-921.3506" x2="166.8963" y2="-831.7707" gradientTransform="matrix(1 0 0 -1 0 -776)">
                                 <stop offset="0" style="stop-color:#CCCCCB"></stop>
                                 <stop offset="0.32" style="stop-color:#3C4650"></stop>
                                 <stop offset="0.7" style="stop-color:#252D33"></stop>
                                 <stop offset="1" style="stop-color:#181D20"></stop>
                              </linearGradient>
                              <path class="st3" d="M118.5,101.8c15-13.2,4.7-28.9-11.3-33.6c6.2,5.9,5.8,10.5-1.3,16.6c-10.3,8.8-22.8,17-34.3,24.1
                              c-11.4,6.9-24,13.9-36.3,19c-13.8,5.7-28.8,0.5-32.2-15c1.7,9.8,4.1,18.4,7.2,25c7.1,14.8,18.7,15.5,33.1,9.6
                              C65.7,138.2,99.7,117.5,118.5,101.8L118.5,101.8L118.5,101.8z"></path>
                              <path class="st1" d="M120.4,83.8c-14.2,18.1-62,46.8-80.9,55.3c-11.8,5.2-25.1,7.1-31.1-5.5c0.6,1.6,1.1,2.9,1.8,4.3
                              c7.1,14.8,18.7,15.5,33.1,9.6c22.4-9.3,56.5-30,75.2-45.6C124.2,96.5,123.7,90.1,120.4,83.8L120.4,83.8L120.4,83.8z"></path>
                              <linearGradient id="SVGID_3_" gradientUnits="userSpaceOnUse" x1="-9.377" y1="-913.095" x2="162.593" y2="-823.515" gradientTransform="matrix(1 0 0 -1 0 -776)">
                                 <stop offset="0" style="stop-color:#CCCCCB"></stop>
                                 <stop offset="0.32" style="stop-color:#4E4F4F"></stop>
                                 <stop offset="0.7" style="stop-color:#383838"></stop>
                                 <stop offset="1" style="stop-color:#231F20"></stop>
                              </linearGradient>
                              <path class="st4" d="M35.2,127.9c-13.8,5.7-28.8,0.5-32.2-15c0.2,1.2,0.4,2.5,0.7,3.7c4.3,15.7,18.5,20.2,33,13.8
                              c24.1-10.6,47-27.5,57.7-36.5c-7.4,5.4-15.3,10.4-22.8,15C60.1,115.8,47.5,122.8,35.2,127.9L35.2,127.9L35.2,127.9z"></path>
                              <path class="st2" d="M115.3,104.5c1.1-0.9,2.1-1.8,3.2-2.6c11-10.1-0.8-24.2-11.3-33.6c4.9,4.8,5.7,8.7,2.1,13.1
                              C115.6,85.6,123.2,97.3,115.3,104.5L115.3,104.5L115.3,104.5z"></path>
                              <linearGradient id="SVGID_4_" gradientUnits="userSpaceOnUse" x1="39.6241" y1="-837.5" x2="136.5" y2="-837.5" gradientTransform="matrix(1 0 0 -1 0 -776)">
                                 <stop offset="0" style="stop-color:#ED2024"></stop>
                                 <stop offset="1" style="stop-color:#F05757"></stop>
                              </linearGradient>
                              <path class="st5" d="M60,105.5c-3.4-18.2-3.5-36.9-0.7-55.1c2.6,15.8,8,31.4,15.4,46.2c8.1-5.2,16.1-10.7,23.5-16.8
                              c0.2-6.1,0.6-12,1.2-17.9c10.3,7.9,33,27.2,19.1,40c0.4-0.3,0.7-0.7,1.2-1c22.4-19.2,22.4-30.6,0-49.9
                              c-9.9-8.4-21.3-16.1-32.4-23c-3.1,12.2-5.1,24.7-6.1,37.3c-5-16.1-5.8-28-5.8-44.5c-7.5-4.3-15.2-8.5-23-12.3
                              c-13.2,33.6-16.3,70.8-8.7,106C49.2,111.7,54.6,108.7,60,105.5L60,105.5L60,105.5z"></path>
                              <linearGradient id="SVGID_5_" gradientUnits="userSpaceOnUse" x1="144.1735" y1="-864.6838" x2="14.8335" y2="-839.7738" gradientTransform="matrix(1 0 0 -1 0 -776)">
                                 <stop offset="0" style="stop-color:#ED2024"></stop>
                                 <stop offset="1" style="stop-color:#F05757"></stop>
                              </linearGradient>
                              <path class="st6" d="M99.4,61.9c10.3,8,33,27.2,19.1,40l1.2-1c6.8-5.8,6.8-13,1.9-20.5C116.3,72.4,106.1,65.4,99.4,61.9
                              L99.4,61.9L99.4,61.9z"></path>
                           </g>
                           <g>
                              <path class="st7" d="M834,63.8V50h-15.5h-8.2c3.2,0,5.4,2.2,5.7,4.5c0,4.4,0.1,9.4,0.1,15.6v54H834V90.6
                              c0-19.8,9.2-26.9,24.2-26.8V50.1C845.7,50.3,837.5,54.8,834,63.8L834,63.8z"></path>
                              <path class="st7" d="M573.3,106.1c0,6.9,0.6,16.3,1,18.2h-17.1c-0.6-1.5-1-5.3-1.1-8.1c-2.7,4.4-8,9.8-21.5,9.8
                              c-17.7,0-25.3-11.6-25.3-23.1c0-16.8,13.4-24.5,35.2-24.5h11.3v-5.1c0-5.7-2-11.8-12.9-11.8c-9.9,0-11.9,4.5-13,10h-17.1
                              c1.1-12.2,8.6-23.2,30.7-23.1c19.3,0.1,29.8,7.7,29.8,25.1L573.3,106.1L573.3,106.1z M555.8,89.6h-9.6
                              c-13.2,0-18.9,3.9-18.9,12.1c0,6.2,4,11.1,11.9,11.1c14.7,0,16.5-10.1,16.5-21.1L555.8,89.6L555.8,89.6z"></path>
                              <path class="st7" d="M745.2,90.8c0,11.2,5.7,20.8,16.7,20.8c9.6,0,12.5-4.3,14.6-9.1h18c-2.7,9.2-10.8,23.4-33.1,23.4
                              c-24,0-34.3-18.5-34.3-37.8c0-22.8,11.7-39.8,35-39.8c24.9,0,33.3,18.7,33.3,36.3c0,2.4,0,4.1-0.3,6.2H745.2L745.2,90.8z
                              M777.4,79.4c-0.2-9.8-4.5-18-15.4-18s-15.5,7.6-16.5,18H777.4L777.4,79.4z"></path>
                              <path class="st7" d="M606.8,102.3c1.8,6.6,7,10.4,15.4,10.4s12-3.4,12-8.6s-3.3-7.8-15.1-10.7c-23.2-5.7-27.3-12.8-27.3-23.2
                              s7.7-21.9,29-21.9s29,11.9,29.8,22h-17.1c-0.8-3.4-3.2-9.1-13.5-9.1c-8,0-10.5,3.7-10.5,7.5c0,4.3,2.5,6.4,15.1,9.4
                              c24,5.6,27.8,13.8,27.8,24.7c0,12.5-9.7,23.1-30.9,23.1s-30.7-10.5-32.5-23.7L606.8,102.3L606.8,102.3z"></path>
                              <path class="st7" d="M471,48.3c-12.3,0-18.9,5.6-23.1,12c-2.7-6.5-9-12-19.6-12c-11.2,0-17.3,5.5-20.9,11.4l0.1-9.7h-23.6
                              c3.4,0,5.8,2.5,5.8,5l0,0c0.1,4.7,0.1,9.4,0.1,14.1v55h17.7V82.9c0-13.2,4.5-19.8,14.1-19.8s11.9,7,11.9,15.3v45.8h17.6V81.7
                              c0-12,4-18.8,13.9-18.8s12.1,7.4,12.1,14.7v46.5h17.5V75.5C494.4,56,483.6,48.3,471,48.3L471,48.3z"></path>
                              <path class="st7" d="M368.8,103.6V26.4h-17.9h-5.8c3.4,0,5.8,2.5,5.8,5l0,0v24.8c-1.8-3.6-7.5-7.9-19-7.9
                              c-20.8,0-33.5,16.7-33.5,39.5s11.9,38.1,30.7,38.1c11.4,0,18.2-3.9,21.8-10.4l0.1,8.7h18C368.8,117.3,368.8,110.5,368.8,103.6
                              L368.8,103.6z M334,111.4c-10.6,0-17.2-8.4-17.2-24.1s6.3-24.4,17.9-24.4c14.5,0,16.9,9.9,16.9,23.8
                              C351.5,99.2,349.2,111.3,334,111.4L334,111.4z"></path>
                              <g>
                                 <path class="st7" d="M265.6,50h-5.8c3.4,0,5.8,2.5,5.8,5v69.2h17.9V50H265.6z"></path>
                                 <path class="st7" d="M283.5,29.1v-2.7h-17.9V47l0,0C275.5,46.9,283.5,38.9,283.5,29.1L283.5,29.1z"></path>
                              </g>
                              <g>
                                 <path class="st7" d="M712.3,63.8V50.1h-15.1l0,0h-17.9l0,0h-11.9v13.8h11.9v41.5c0,12.8,4.9,19.9,18.4,19.9
                                    c3.9,0,9.1-0.1,12.9-1.4v-12.6c-1.7,0.3-3.9,0.4-5.3,0.4c-6.3,0-8-2.7-8-8.8V63.8H712.3L712.3,63.8z"></path>
                                 <path class="st7" d="M697,29.1v-2.7h-17.9V47l0,0C689,46.9,697,38.9,697,29.1L697,29.1z"></path>
                              </g>
                              <path class="st7" d="M231.3,25.9l-16.3,47c-4.8,14-9.3,27.1-11.3,35.9h-0.3c-2.2-9.7-6.2-22.3-10.8-36.3L177,25.9h-25.7
                              c5.1,0,7.6,4.6,8.6,7.4l32.4,90.9h21.8l36.1-98.3L231.3,25.9L231.3,25.9z"></path>
                           </g>
                        </g>
                     </g>
                  </svg>
				
               </div>
			   <div class="col-12 col-md-10 mx-auto">

					<div class="pre-heading-vidmaster f-18 f-md-18 w400 lh140 white-clr text-center">
						Make BIG Profits in 2023 by Copying a Proven System That Make Us $545/Day Over and Over Again…
					</div>
				</div>
				<div class="col-12 mt-md50 mt30 f-md-45 f-28 w700 text-center white-clr lh140 text-capitalize">
                  Brand New AI APP <span class="red-clr-vid">Creates 100s  Reels, Boomerang, &amp; Short Videos with Just One Keyword…</span> for Floods of FREE Traffic and Sales
               </div>
               <div class="col-12 mt-md25 mt20 f-16 f-md-20 w400 text-center lh140 white-clr text-capitalize">
					Even A Complete Beginner Can Drive Thousands of Visitors to Any Offer or Link  Instantly <br> |  No Camera | No Writing &amp; Editing | No Paid Traffic Ever…
               </div>
               <!--<div class="col-12 mt-md30 mt20 text-center">-->
               <!--   <div class="f-16 f-md-24 w500 text-center lh140 white-clr text-capitalize">-->
               <!--   Say No To Expensive Email Marketing Tools / Autoresponders, Email Writers & Designers-->
               <!--   </div>-->
               <!--</div>-->
			   <div class="row mt20 mt-md40 gx-md-5">
               <div class="col-md-7 col-12">
                  <div class="col-12">
                    
                     <!-- <img src="assets/images/video-thumb.webp" alt="Video" class="mx-auto d-block img-fluid mt20 mt-md30"> -->
                     <div class="video-box mt20 mt-md30">
                        <div style="padding-bottom: 56.25%;position: relative;">
                        <iframe src="https://vidmaster.dotcompal.com/video/embed/8x6aokyyl7" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                        </div>
                     </div>
                  </div>
                  
               </div>
               <div class="col-md-5 col-12 mt20 mt-md0">
			   <div class="calendar-wrap-vidmaster mb20 mb-md0">
                     <ul class="list-head-vidmaster pl0 m0 f-18 lh140 w400 white-clr">
                     <li>Drive Tsunami of FREE Traffic in Any Niche</li>
                     <li>Create Reels &amp; Shorts Using Just One Keyword </li>
                     <li>Create Boomerang Video to Engage Audience</li>
                     <li>Get Thousands of Visitors &amp; Sales to Any Offer or Page</li>
                     <li>Make Tons of Affiliate Commissions or Ad Profits</li> 
                     <li>Create Short Videos from Any Video, Text, or Just a Keyword</li>
                     <li>Add Background Music and/or Voiceover to Any Video</li>
                     <li>Pre-designed 50+ Shorts Video templates</li>
                     <li>25+ Vector Images to Choose From</li>
                     <li>100+ Social Sharing Platforms ForFREE Viral Traffic</li>
                     <li>100% Newbie Friendly, No Prior Experience Or Tech Skills Needed</li>
                     <li>No Camera Recording, No Voice, Or Complex Editing Required</li>
                     <li>Free Commercial License To Sell Video Services For High Profits</li>
                     </ul>
                  </div>
               </div>
            </div>
               </div>
            </div>
         </div>
      </div>
	  <div class="gr-1">
      <div class="container-fluid p0">
         <picture>
            <source media="(min-width:768px)" srcset="assets/images/vidmaster.png">
            <source media="(min-width:320px)" srcset="assets/images/vidmaster-mview.png">
            <img src="assets/images/vidmasterpt.png" alt="Steps" style="width:100%;">
         </picture>
      </div>
   </div>

  	<!-- Bonus Section Header Start -->
	<div class="bonus-header">
		<div class="container">
			<div class="row">
				<div class="col-12 col-md-10 mx-auto heading-bg text-center">
					<div class="f-24 f-md-36 lh160 w700">When You Purchase Ninja Ai, You Also Get <br class="d-lg-block d-none"> Instant Access To These 20 Exclusive Bonuses</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Bonus Section Header End -->

	
	<!-- Bonus #1 Section Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
			 <div class="col-12 text-center">
			 </div>
			 <div class="col-12 mt20 mt-md30">
				<div class="row d-flex align-items-center flex-wrap">
				   <div class="col-12 col-md-5 order-3 order-md-0 text-center">
					  <img src="assets/images/bonus1.webp" class="img-fluid">
				   </div>
				   <div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				  <div class="text-md-start text-center">
				  <div class="bonus-title-bg mb-4">
				   <div class="f-22 f-md-28 lh120 w700 white-clr">Bonus 1</div>
				</div>
				  </div>
					  <div class="f-22 f-md-34 w600 lh160 bonus-title-color mt20 mt-md30">
					  Boost Your Online Sales 
					  </div>
					  <ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
						<li class="w600">When it comes to online sales, there are many strategies you could implement to boost sales.</li>
						<li>From tweaking your sales copy to wowing your customers with exceptional customer support, all these things can boost sales.</li>
						<li>With this ebook you will discover 101 practical strategies and methods to increase online sales for your product or service.</li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #1 Section End -->

	<!-- Bonus #2 Section Start -->
	<div class="section-bonus">
	   <div class="container">
		
			 <div class="col-12 mt20 mt-md30">
				<div class="row align-items-center flex-wrap">
				   <div class="col-12 col-md-5 order-3 order-md-3 text-center">
					  <img src="assets/images/bonus2.webp" class="img-fluid">
				   </div>
				   <div class="col-12 col-md-7 mt-md0 mt20">
				   <div class=" text-md-start text-center">
				<div class="bonus-title-bg mb-4">
				   <div class="f-22 f-md-28 lh120 w700 white-clr">Bonus 2</div>
				</div></div>
			
					  <div class="f-22 f-md-34 w600 lh160 bonus-title-color mt20 mt-md30">
					  Sales Funnel Optimization 

					  </div>
					  <ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
					  	<li class="w600">Whether you make your money through ad clicks using the Adsense monetization platform or you sell affiliate products or your own services or you run your own online drop shipping store, you're trying to convert people from simple clickers of links and readers of your content to cold hard cash.</li> 
						<li>The sales funnel model helps you craft together working strategies that would help you turn your content and traffic into cash.</li>
						<li>This video course teaches you how to optimize your sales funnel. It will show you key strategies that will help you maximize conversions and thereby maximize your profits.</li>
					</ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #2 Section End -->

	<!-- Bonus #3 Section Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row ">
		
			 <div class="col-12 mt20 mt-md30">
				<div class="row align-items-center flex-wrap">
				   <div class="col-12 col-md-5 order-3 order-md-0 text-center">
					  <img src="assets/images/bonus3.webp" class="img-fluid">
				   </div>
				   <div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				   <div class="text-center text-md-start">
				<div class="bonus-title-bg mb-4">
				   <div class="f-22 f-md-28 lh120 w700 white-clr">Bonus 3</div>
				</div>
			 </div>
					  <div class="f-22 f-md-34 w600 lh160 bonus-title-color mt20 mt-md30">
					  Traffic Secrets Unleashed 

					  </div>
					  <ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
					  	<li class="w600">This is a short training that gets straight to the core and reveals super-effective traffic sources for getting huge amount of traffic. </li>
						<li>You will learn to use the largest, most responsive, supportive, and the best traffic generating tribe on the internet to reach your goals!</li>
					
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #3 Section End -->

	<!-- Bonus #4 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row ">
		
			 <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
				<div class="row align-items-center flex-wrap">
				   <div class="col-12 col-md-5 order-3 order-md-3 text-center">
					  <img src="assets/images/bonus4.webp" class="img-fluid">
				   </div>
				   <div class="col-md-7 col-12 mt20 mt-md0 ">
				   <div class="text-md-start text-center">
				<div class="bonus-title-bg mb-4">
				   <div class="f-22 f-md-28 lh120 w700 white-clr">Bonus 4</div>
				</div>
			 </div>
					  <div class="f-22 f-md-34 w600 lh160 bonus-title-color mt20 mt-md30">
					  Digital Empire 

					  </div>
					  <ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
					  <li class="w600">If you’ve been looking for a fast and easy way to build a profitable online business, look no further than digital products.</li>
					  <li>Digital products are not only easy to create, but they can be easily replicated and turned into multiple product lines that fuel an unlimited number of sales funnels.</li>
					  <li>They’re also consistently in demand, easy to deliver, and if you never want to write a line of content yourself, you can affordably outsource everything!</li>
					</ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #4 End -->

	<!-- Bonus #5 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row ">
		
			 <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
				<div class="row align-items-center flex-wrap">
				   <div class="col-12 col-md-5 order-3 order-md-0 text-center ">
					  <img src="assets/images/bonus5.webp" class="img-fluid">
				   </div>
				   <div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				   <div class="text-md-start text-center">
				<div class="bonus-title-bg mb-4">
				   <div class="f-22 f-md-28 lh120 w700 white-clr">Bonus 5</div>
				</div>
			 </div>
					  <div class="f-22 f-md-34 w600 lh160 bonus-title-color mt20 mt-md30">
					  Smart OTO Blueprint 

					  </div>
					  <ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
					  	<li class="w600">When it comes to selling your products and services online, you need to ensure that your whole funnel converts - everything from your front-end offer to your one-time offers.</li>
						<li>If you've been purchasing products online or selling them, you have likelyexperienced one-time offers (OTO). To set up a successful and high-converting one-time offer, you need to plan it out carefully</li>
						<!--<li>This video course will take you behind the scenes to help you understand how to achieve more sales without spending days or weeks banging your head against the wall trying to figure things out.</li>-->
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #5 End -->

	<!-- CTA Button Section Start -->
	<div class="cta-btn-section dark-cta-bg">
	   <div class="container">
		  <div class="row">
			 <div class="col-md-12 col-md-12 col-12 text-center">
				<div class="f-md-20 f-18 text-center mt3 white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
				<div class="f-md-22 f-22 text-center purple lh120 w700 mt15 mt-md20 orange-clr">TAKE ACTION NOW!</div>
				<div class="f-md-22 f-17 lh120 w600 mt15 mt-md20 white-clr">Use Special Coupon <span class=" orange-clr">"ADMININJA"</span> For <span class=" orange-clr">30% Off</span> On Full Funnel </div>
			 </div>
			 <div class="col-md-12 col-12 text-center mt15 mt-md20">
				<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
				<span class="text-center">Grab Ninja Ai + My 20 Exclusive Bonuses</span> <br>
				</a>
			 </div>
			 <div class="col-12 mt15 mt-md20 text-center">
				<img src="assets/images/payment.png" class="img-fluid">
			 </div>
			 <div class="col-12 mt15 mt-md20" align="center">
				<h3 class="f-md-28 f-20 w500 text-center white-clr">Coupon Is Expiring In... </h3>
			 </div>
			 <!-- Timer -->
			 <div class="col-12 text-center">
				<div class="countdown counter-white white-clr">
				   <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Days</span> </div>
				   <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
				   <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
				   <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
				</div>
			 </div>
			 <!-- Timer End -->
		  </div>
	   </div>
	</div>
	<!-- CTA Button Section End -->

	<!-- Bonus #6 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
		
			 </div>
			 <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
				<div class="row align-items-center flex-wrap">
				   <div class="col-12 col-md-5 order-3 order-md-3 text-center">
					  <img src="assets/images/bonus6.webp" class="img-fluid">
				   </div>
				   <div class="col-md-7 col-12 mt20 mt-md0">
				   <div class="text-center text-md-start">
				<div class="bonus-title-bg mb-4">
				   <div class="f-22 f-md-28 lh120 w700 white-clr">Bonus 6</div>
				</div>
					  <div class="f-22 f-md-34 w600 lh160 bonus-title-color mt20 mt-md30">
					  Big Traffic Firesale 

					  </div>
					  <ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
					  	<li><span class="w600">If you own a business, traffic it is the main driver for sales.</span> You can have the best sales funnel on the planet but if no one is visiting your website, it doesn’t mean a thing.</li>
						<li>The theory is simple, the higher your traffic is, the higher probability it converts to sales.</li>
						<li>This course will show you how to use multiple social network and other websites to link the desired site that needs traffic.</li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #6 End -->

	<!-- Bonus #7 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
		
			 <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
				<div class="row align-items-center flex-wrap">
				   <div class="col-12 col-md-5 order-3 order-md-0 text-center">
					  <img src="assets/images/bonus7.webp" class="img-fluid">
				   </div>
				   <div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				   <div class="text-md-start text-center">
				<div class="bonus-title-bg mb-4">
				   <div class="f-22 f-md-28 lh120 w700 white-clr">Bonus 7</div>
				</div>
			 </div>
					  <div class="f-22 f-md-34 w600 lh160 bonus-title-color mt20 mt-md30">
					  Sales Funnel Mastery

					  </div>
					  <ul class="bonus-list f-18 f-md-20 lh160 w400">
					  	<li class="w600">The most profits from Internet marketing come to those who own their own digital products and control the sales funnel.</li>
						<li>There are two basic rules regarding sales funnels and corresponding upsells and downsells. You will learn everything you need to know in this quick step by step guide. </li>
					</ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #7 End -->

	<!-- Bonus #8 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
		
			 <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
				<div class="row align-items-center flex-wrap">
				   <div class="col-12 col-md-5 order-3 order-md-3 text-center">
					  <img src="assets/images/bonus8.webp" class="img-fluid">
				   </div>
				   <div class="col-md-7 col-12 mt20 mt-md0">
				   <div class="text-center text-md-start">
				<div class="bonus-title-bg mb-4">
				   <div class="f-22 f-md-28 lh120 w700 white-clr">Bonus 8</div>
				</div>
			 </div>
					  <div class="f-22 f-md-34 w600 lh160 bonus-title-color mt20 mt-md30">
					  Hashtag Traffic Secrets 

					  </div>
					  <ul class="bonus-list f-18 f-md-20 lh160 w400">
					  	<li class="w600">Get Laser-Targeted Social Media Traffic Using The Power Of Hashtags!</li>
						<li>There are plenty of different ways to drive traffic to your website today, but some are definitely more effective – and attractive – than others. </li>
						<li>Social media marketing, more specifically “hashtag marketing”, is definitely one of the hottest drivers of organic traffic today and you have to make sure that you are making the most of the hashtags you create online right now to build your business from the ground up.</li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #8 End -->

	<!-- Bonus #9 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row align-items-center flex-wrap">
		
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-12 col-md-5 order-3 order-md-0 text-center">
					  		<img src="assets/images/bonus9.webp" class="img-fluid">
				   		</div>
				   		<div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				   			<div class="text-md-start text-center">
								<div class="bonus-title-bg mb-4">
				   					<div class="f-22 f-md-28 lh120 w700 white-clr">Bonus 9</div>
								</div>
			 				</div>
					  		<div class="f-22 f-md-34 w600 lh160 bonus-title-color  mt20 mt-md30">					 
							  WP Funnel Profit 

					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 lh160 w400">
					  			<li class="w600">Get Instant Access to the 30 Brand New WordPress 'How To' Videos!</li>
								<li>If you are a blogger, niche marketer or an Internet Marketing influencer, learning and teaching how to use WordPress is a huge help to grow your business!</li>
								<li>The thing is that, many people would really want to learn how to use WordPress for their blog and business. Still they are looking for an answer on how you can maximize wordpress in this latest version. Learn more inside!</li>
							</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #9 End -->

	<!-- Bonus #10 start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row align-items-center flex-wrap">
		
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-12 col-md-5 order-3 order-md-3 text-center">
					  		<img src="assets/images/bonus10.webp" class="img-fluid">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
				   			<div class="text-md-start text-center">
								<div class="bonus-title-bg mb-4">
				   					<div class="f-22 f-md-28 lh120 w700 white-clr">Bonus 10</div>
								</div>
			 				</div>
					  		<div class="f-22 f-md-34 w600 lh160 bonus-title-color mt20 mt-md30">
							  The Funnel Hacker 

					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 lh160 w400">
								<span class="w600">With this 4-part video course you will learn:</span>
							  	<li>What is funnel hacking</li>
								<li>Tips for funnel hacking</li>
								<li>Tools for funnel hacking</li>
							</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #10 End -->

<!-- CTA Button Section Start -->
<div class="cta-btn-section dark-cta-bg">
	   <div class="container">
		  <div class="row">
			 <div class="col-md-12 col-md-12 col-12 text-center">
				<div class="f-md-20 f-18 text-center mt3 white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
				<div class="f-md-22 f-22 text-center purple lh120 w700 mt15 mt-md20 orange-clr">TAKE ACTION NOW!</div>
				<div class="f-md-22 f-17 lh120 w600 mt15 mt-md20 white-clr">Use Special Coupon <span class=" orange-clr">"ADMININJA"</span> For <span class=" orange-clr">30% Off</span> On Full Funnel </div>
			 </div>
			 <div class="col-md-12 col-12 text-center mt15 mt-md20">
				<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
				<span class="text-center">Grab Ninja Ai + My 20 Exclusive Bonuses</span> <br>
				</a>
			 </div>
			 <div class="col-12 mt15 mt-md20 text-center">
				<img src="assets/images/payment.png" class="img-fluid">
			 </div>
			 <div class="col-12 mt15 mt-md20" align="center">
				<h3 class="f-md-28 f-20 w500 text-center white-clr">Coupon Is Expiring In... </h3>
			 </div>
			 <!-- Timer -->
			 <div class="col-12 text-center">
				<div class="countdown counter-white white-clr">
				   <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Days</span> </div>
				   <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
				   <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
				   <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
				</div>
			 </div>
			 <!-- Timer End -->
		  </div>
	   </div>
	</div>
	<!-- CTA Button Section End -->

<!-- Bonus #11 start -->
<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
	
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row align-items-center flex-wrap">
				   		<div class="col-12 col-md-5 order-3 order-md-0 text-center">
					  		<img src="assets/images/bonus11.webp" class="img-fluid">
				   		</div>
				   		<div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				   			<div class="text-md-start text-center">
								<div class="bonus-title-bg mb-4">
				   					<div class="f-22 f-md-28 lh120 w700 white-clr">Bonus 11</div>
								</div>
			 				</div>
					  		<div class="f-22 f-md-34 w600 lh160 bonus-title-color mt20 mt-md30">
							  WP Mini Funnels 

					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 lh160 w400">
							  	<li classs="w600">Create Mini Sales Funnels (Front End Product, OTO and their Download Pages) in Minutes!</li>
								<li>Traffic is the blood to every online business website. But the thing is that not all traffic are equal. That's sales funnel are created to sort out those leads being generated.</li>
								<li>Now, the next challenge is how are you going to build those sales funnels? The good news is that inside this product is a piece of amazing software that will help you build a stunning sales funnel in just a matter of minutes.</li>		
							</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #11 End -->


	<!-- Bonus #12 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
		
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row align-items-center flex-wrap">
				   		<div class="col-12 col-md-5 order-3 order-md-3 text-center">
					  		<img src="assets/images/bonus12.webp" class="img-fluid">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
				   			<div class="text-md-start text-center">
								<div class="bonus-title-bg mb-4">
				   					<div class="f-22 f-md-28 lh120 w700 white-clr">Bonus 12</div>
								</div>
			 				</div>
					  		<div class="f-22 f-md-34 w600 lh160 bonus-title-color mt20 mt-md30">
							  Traffic Babylon 

					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 lh160 w400">
					  			<li class="w600">If You Are Looking To Send Your Website Traffic Even Further Through The Roof, then here's a short, 12 part video series that reveals to you 6 paid and 6 free traffic sources to SKYROCKET your traffic!</li>
 								<li>YES! Traffic is indeed the life-blood of your website. But the question is that, how many traffic generating ideas you have for you to perform on your campaign?</li>	
							</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #12 End -->

	<!-- Bonus #13 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">

			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row align-items-center flex-wrap">
				   		<div class="col-12 col-md-5 order-3 order-md-0 text-center">
					  		<img src="assets/images/bonus13.webp" class="img-fluid">
				   		</div>
				   		<div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				   			<div class="text-md-start text-center">
								<div class="bonus-title-bg mb-4">
				   					<div class="f-22 f-md-28 lh120 w700 white-clr">Bonus 13</div>
								</div>
			 				</div>
					  		<div class="f-22 f-md-34 w600 lh160 bonus-title-color mt20 mt-md30">
							  List Building Excellence 

					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 lh160 w400">
					  			<li class="w600">Are You Ready to Finally Discover the REAL Secrets to Building a Responsive List? Even if Your Are a Complete Newbie?</li>
								<li>If your goal is to build a long-term business that is profitable, lean and provides steady income for years to come, you’ve come to the right place.</li>
								<li>List building is the life blood of business success for anyone who is building a business online. It allows you to build relationships with your clients, learn about the problems they’re experiencing, and accumulate repeat sales over time.</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #13 End -->

	<!-- Bonus #14 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">

			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row align-items-center flex-wrap">
				   		<div class="col-12 col-md-5 order-3 order-md-3 text-center">
					  		<img src="assets/images/bonus14.webp" class="img-fluid">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
				   			<div class="text-md-start text-center">
								<div class="bonus-title-bg mb-4">
				   					<div class="f-22 f-md-28 lh120 w700 white-clr">Bonus 14</div>
								</div>
			 				</div>	  
				   			<div class="f-22 f-md-34 w600 lh160 bonus-title-color mt20 mt-md30">
							   Innovative Lead Generation

					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 lh160 w400">
								<li class="w600">Are You In The Business of Making Money? Attract High Quality Leads - And You Will Be!</li>
								<li>Don't let anyone tell you different. There are two types of business... Ones that make money, and ones that don't. Which one are you? Is your business making money? That could be a loaded question...</li>
								<li>There could be a whole list of reasons you aren't making money, but from my experience there is one issue that will stunt the growth of even the most impressive business. </li>
								
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #14 End -->


	<!-- Bonus #15 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">

			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row align-items-center flex-wrap">
				   		<div class="col-12 col-md-5 order-3 order-md-0 text-center">
					  		<img src="assets/images/bonus15.webp" class="img-fluid">
				   		</div>
				   		<div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				   			<div class="text-md-start text-center">
								<div class="bonus-title-bg mb-4">
				   					<div class="f-22 f-md-28 lh120 w700 white-clr">Bonus 15</div>
								</div>
			 				</div>
				   			<div class="f-22 f-md-34 w600 lh160 bonus-title-color mt20 mt-md30">
							   First Sales Funnel 

					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 lh160 w400">
					  			<li class="w600">Learn How to Make Your First Sales Funnel!</li>
								<li>If you are an affiliate marketer or digital product owner who aims to have a huge profitable product launch, having an effective sales funnel will help you close more sales to your product.</li>
								<li>Sales Funnel has been proven and tested to many successful internet marketers and if you want to become successful too, learn how to build yours is essential.</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #15 End -->



	<!-- CTA Button Section Start -->
	<div class="cta-btn-section dark-cta-bg">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-md-12 col-md-12 col-12 text-center">
					<div class="f-md-20 f-18 text-center mt3 white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
					<div class="f-md-22 f-22 text-center purple lh120 w700 mt15 mt-md20 orange-clr">TAKE ACTION NOW!</div>
					<div class="f-md-22 f-17 lh120 w600 mt15 mt-md20 white-clr">Use Special Coupon <span class=" orange-clr">"ADMININJA"</span> For <span class="orange-clr">30% Off</span> On Full Funnel </div>
			 	</div>
			 	<div class="col-md-12 col-12 text-center mt15 mt-md20">
					<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
						<span class="text-center">Grab Ninja Ai + My 20 Exclusive Bonuses</span> <br>
					</a>
			 	</div>
			 	<div class="col-12 mt15 mt-md20 text-center">
					<img src="assets/images/payment.png" class="img-fluid">
			 	</div>
			 	<div class="col-12 mt15 mt-md20" align="center">
					<h3 class="f-md-28 f-20 w500 text-center white-clr">Coupon Is Expiring In... </h3>
			 	</div>
			 	<!-- Timer -->
			 	<div class="col-12 text-center">
					<div class="countdown counter-white white-clr">
				   		<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Days</span> </div>
				   		<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
				   		<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
				   		<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
					</div>
			 	</div>
			 	<!-- Timer End -->
		  	</div>
	   	</div>
	</div>
	<!-- CTA Button Section End -->
	
	<!-- Bonus #16 start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
	
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row align-items-center flex-wrap">
				   		<div class="col-12 col-md-5 order-3 order-md-0 text-center">
					  		<img src="assets/images/bonus16.webp" class="img-fluid">
				   		</div>
				   		<div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				   			<div class="text-md-start text-center">
								<div class="bonus-title-bg mb-4">
				   					<div class="f-22 f-md-28 lh120 w700 white-clr">Bonus 16</div>
								</div>
			 				</div>
					  		<div class="f-22 f-md-34 w600 lh160 bonus-title-color  mt20 mt-md30">
							  Niche Profit Plan 

					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 lh160 w400">
							    <li>Tap Into Profitable Niche Markets In 7 Days Or Less Even If You Have NEVER Tried This Before!</li>
								<li>They have tried a few different approaches in Internet Marketing and have not made a single dollar, so how are they going to make $10,000 in 3 short months?</li>
								<li>Fewer of us know exactly how to get started with niche marketing so that every hour we spend, is a productive and profitable one.. after all, niche marketing can be a confusing process to get started in, especially if you just don't know how to figure out how profitable a potential market might be.</li>
							</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #16 End -->


	<!-- Bonus #17 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
		
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row align-items-center flex-wrap">
				   		<div class="col-12 col-md-5 order-3 order-md-3 text-center">
					  		<img src="assets/images/bonus17.webp" class="img-fluid">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
				   			<div class="text-md-start text-center">
								<div class="bonus-title-bg mb-4">
				   					<div class="f-22 f-md-28 lh120 w700 white-clr">Bonus 17</div>
								</div>
			 				</div>
					  		<div class="f-22 f-md-34 w600 lh160 bonus-title-color mt20 mt-md30">
							  List Explosion 

					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 lh160 w400">
								<span>List Explosion Learn...</span>
							  <li>How to quickly set up a powerful list building campaign that will funnel in qualified leads!</li>
							  <li>The easiest way to create a laser-targeted squeeze page that will boost opt-in rates by up to 200%, instantly!</li>
							  <li>The #1 component that will maximize exposure while solidifying your place in your market!</li>
			   				</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #17 End -->

	<!-- Bonus #18 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">

			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row align-items-center flex-wrap">
				   		<div class="col-12 col-md-5 order-3 order-md-0 text-center">
					  		<img src="assets/images/bonus18.webp" class="img-fluid">
				   		</div>
				   		<div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				   			<div class="text-md-start text-center">
								<div class="bonus-title-bg mb-4">
				   					<div class="f-22 f-md-28 lh120 w700 white-clr">Bonus 18</div>
								</div>
			 				</div>
					  		<div class="f-22 f-md-34 w600 lh160 bonus-title-color mt20 mt-md30">
							  Traffic Heist

					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 lh160 w400">
							  	<li>Follow A Step By Step Blueprint To Legally Steal, Swipe And Funnel As Much Targeted FREE Traffic As Your Server Can Handle! Activate A Traffic Generation System That Will Sweep Through Your Niche Markets And Pull In Thousands Of Highly Targeted Customers, Instantly!</li>
								<li>While you're busy trying "plan a", "plan b" and some of "plan c", savvy marketers are focusing on ONE solid action plan that drives endless streams of traffic to their websites, effortlessly. These guys don't sit around waiting for things to happen.. they MAKE things happen by weeding out the time-wasters and spend a couple of hours a week fine tuning their websites and keeping the traffic machine running... full speed ahead.</li>
							
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #18 End -->

	<!-- Bonus #19 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">

			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row align-items-center flex-wrap">
				   		<div class="col-12 col-md-5 order-3 order-md-3 text-center">
					  		<img src="assets/images/bonus19.webp" class="img-fluid">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
				   			<div class="text-md-start text-center">
								<div class="bonus-title-bg mb-4">
				   					<div class="f-22 f-md-28 lh120 w700 white-clr">Bonus 19</div>
								</div>
			 				</div>	  
				   			<div class="f-22 f-md-34 w600 lh160 bonus-title-color mt20 mt-md30">
							   Perfect Sales Funnel

					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 lh160 w400">
							  	<li class="w600">Learn how to make your business more successful. Have You Ever Wondered How the Big Online Players Create so Much Revenue and Online Subscribers?</li>
								<li>You make use of the metaphor of a sales funnel (wide at the very top, narrow at the end) to monitor the sales process. Towards the top of this funnel you've got 'unqualified prospects' - the people who you believe may need your service or product, but to whom you've never spoken. </li>
								
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #19 End -->


	<!-- Bonus #20 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row align-items-center flex-wrap">
				   		<div class="col-12 col-md-5 order-3 order-md-0 text-center">
					  		<img src="assets/images/bonus20.webp" class="img-fluid">
				   		</div>
				   		<div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				   			<div class="text-md-start text-center">
								<div class="bonus-title-bg mb-4">
				   					<div class="f-22 f-md-28 lh120 w700 white-clr">Bonus 20</div>
								</div>
			 				</div>
				   			<div class="f-22 f-md-34 w600 lh160 bonus-title-color mt20 mt-md30">
							   Exit Prophet Pro 

					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 lh160 w400">
							 	<li>Exit Prophet Pro is a small php script you can use to generate little 'exit traffic salesmen'. As soon as they get a sense someone is about to abandon your site they jump into action, funneling the traffic where you want it to go before its gone forever.</li>
								<li>Here are just some of the ways you will benefitwhen you start using 'Exit Prophet Pro' today:</li>
								<li>-Build Your Opt-In List-Promoting Affiliate Programs-Pre-built Salesmen-Audio / Video MessagesAnd so much more!</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>

	<!-- Huge Woth Section Start -->
	<div class="huge-area mt30 mt-md10">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-12 text-center">
                    <div class="f-md-60 f-30 lh120 w700 white-clr">That’s Huge Worth of</div>
                    <br>
                    <div class="f-md-60 f-30 lh120 w800 orange-clr">$2465!</div>
                </div>
            </div>
        </div>
    </div>
	<!-- Huge Worth Section End -->

	<!-- text Area Start -->
	<div class="white-section pb-0">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-12 text-center p0">
                	<div class="f-md-40 f-25 lh160 w400">So what are you waiting for? You have a great opportunity <br class="d-lg-block d-none"> ahead + <span class="w600">My 20 Bonus Products</span> are making it a <br class="d-lg-block d-none"> <b>completely NO Brainer!!</b></div>
            	</div>
				<div class="col-md-12 col-md-12 col-12 text-center mt20 mt-md50">
					<div class="f-md-26 f-18 text-center black lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
					<div class="f-md-24 f-22 text-center purple lh120 w700 mt15 mt-md20 purple-clr">TAKE ACTION NOW!</div>
					<div class="f-md-22 f-17 lh120 w600 mt15 mt-md20 black-clr">Use Special Coupon <span class="w800 purple-clr">"ADMININJA"</span> For <span class="w800 purple-clr">30% Off</span> On Full Funnel </div>
				</div>
			
			</div>
			
		</div>
	</div>
	<!-- text Area End -->

	<!-- CTA Button Section Start -->
	<div class="cta-btn-section pt-3 ">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-md-12 col-12 text-center">
					<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
						<span class="text-center">Grab Ninja Ai + My 20 Exclusive Bonuses</span> <br>
					</a>
			 	</div>
			 
			 	<div class="col-12 mt15 mt-md20" align="center">
					<h3 class="f-md-30 f-20 w500 text-center black">My Exclusive Bonuses Are Expiring in...</h3>
			 	</div>
			 	<!-- Timer -->
			 	<div class="col-12 text-center">
					<div class="countdown counter-black">
						<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
						<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
						<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
						<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
					</div>
			 	</div>
			 	<!-- Timer End -->
		  	</div>
	   	</div>
	</div>
	<!-- CTA Button Section End -->

	
	<!--Footer Section Start -->
    <div class="footer-section">
        <div class="container ">
            <div class="row">
                <div class="col-12 text-center">
				<svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 645.02 151.26" style="max-height:70px">
                              <defs>
                                <style>
                                  .cls-1n {
                                    fill: url(#linear-gradient-2);
                                  }
                            
                                  .cls-2n {
                                    fill: #fff;
                                  }
                            
                                  .cls-3n {
                                    fill-rule: evenodd;
                                  }
                            
                                  .cls-3n, .cls-4n {
                                    fill: #ff813b;
                                  }
                            
                                  .cls-5n {
                                    fill: url(#linear-gradient-3);
                                  }
                            
                                  .cls-6n {
                                    fill: url(#linear-gradient);
                                  }
                            
                                  .cls-7n {
                                    fill: url(#linear-gradient-4);
                                  }
                                </style>
                                <linearGradient id="linear-gradient" x1="47.81" y1="75" x2="185.48" y2="75" gradientUnits="userSpaceOnUse">
                                  <stop offset="0" stop-color="#b956ff"></stop>
                                  <stop offset="1" stop-color="#6e33ff"></stop>
                                </linearGradient>
                                <linearGradient id="linear-gradient-2" x1="0" y1="75" x2="85.18" y2="75" gradientUnits="userSpaceOnUse">
                                  <stop offset="0" stop-color="#b956ff"></stop>
                                  <stop offset="1" stop-color="#a356fa"></stop>
                                </linearGradient>
                                <linearGradient id="linear-gradient-3" x1="55.61" y1="62.54" x2="62.86" y2="62.54" gradientUnits="userSpaceOnUse">
                                  <stop offset="0" stop-color="#b956ff"></stop>
                                  <stop offset="1" stop-color="#ac59fa"></stop>
                                </linearGradient>
                                <linearGradient id="linear-gradient-4" x1="54.62" y1="79" x2="61.87" y2="79" gradientTransform="translate(19.64 -11.24) rotate(13.24)" xlink:href="#linear-gradient-3"></linearGradient>
                              </defs>
                              <g id="Layer_1-2" data-name="Layer 1">
                                <g>
                                  <g>
                                    <g>
                                      <path class="cls-6n" d="m181.49,58.53h-10.72c-1.4-5.13-3.43-9.99-6.01-14.51l7.58-7.58c1.56-1.56,1.56-4.09,0-5.65l-17.65-17.65c-1.56-1.56-4.09-1.56-5.65,0l-7.58,7.58c-4.51-2.58-9.38-4.61-14.51-6.01V4c0-2.21-1.79-4-4-4h-24.96c-2.21,0-4,1.79-4,4v10.72c-.16.04-.31.09-.47.14-.27.08-.53.15-.8.23-.27.08-.55.16-.82.24-.55.17-1.09.35-1.63.53-.11.04-.22.08-.33.12-.48.17-.96.34-1.44.52-.12.04-.24.09-.36.14-.52.2-1.04.4-1.56.62-.04.02-.08.03-.12.05-2.41,1-4.74,2.15-6.99,3.43l-7.57-7.57c-1.56-1.56-4.09-1.56-5.65,0l-17.65,17.65c-.36.36-.63.76-.82,1.2h61.99v.02c.23,0,.45-.02.68-.02,23.76,0,43.01,19.26,43.01,43.01s-19.26,43.01-43.01,43.01c-.5,0-1-.02-1.5-.04v.04h-61.17c.19.43.46.84.82,1.2l17.65,17.65c1.56,1.56,4.09,1.56,5.65,0l7.57-7.57c2.25,1.28,4.58,2.43,6.99,3.43.04.02.08.03.12.05.51.21,1.03.42,1.55.62.12.05.24.09.36.14.47.18.95.35,1.43.52.11.04.23.08.34.12.54.18,1.08.36,1.63.53.27.08.55.16.83.25.26.08.53.16.79.23.16.04.31.09.47.14v10.72c0,2.21,1.79,4,4,4h24.96c2.21,0,4-1.79,4-4v-10.72c5.13-1.4,9.99-3.43,14.51-6.01l7.58,7.58c1.56,1.56,4.09,1.56,5.65,0l17.65-17.65c1.56-1.56,1.56-4.09,0-5.65l-7.58-7.58c2.58-4.51,4.61-9.38,6.01-14.51h10.72c2.21,0,4-1.79,4-4v-24.96c0-2.21-1.79-4-4-4Z"></path>
                                      <path class="cls-1n" d="m76.64,101.54c-5.74-7.31-9.18-16.52-9.18-26.54s3.44-19.23,9.18-26.54c2.45-3.13,5.33-5.9,8.53-8.24h-48.39c-1.31-1.89-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.13h17.07c-1.51,3.21-2.76,6.58-3.71,10.07h3.1c1.31-1.89,3.49-3.13,5.97-3.13,4.01,0,7.25,3.25,7.25,7.25s-3.22,7.23-7.21,7.25h-.04c-2.48,0-4.66-1.24-5.97-3.13H13.22c-.44-.64-.98-1.19-1.59-1.65-1.21-.92-2.73-1.48-4.38-1.48-4,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.14h38.82c1.31-1.89,3.49-3.13,5.97-3.13.43,0,.84.05,1.25.12,3.41.59,6,3.56,6,7.14,0,2.78-1.56,5.19-3.86,6.41-1.01.53-2.16.84-3.39.84-2.48,0-4.66-1.24-5.97-3.13h-17.85c-1.31-1.9-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.13h15.96c.95,3.48,2.2,6.85,3.71,10.07h-5.63c-1.31-1.89-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25,0,1.53.48,2.95,1.28,4.12,1.31,1.9,3.49,3.14,5.97,3.14s4.66-1.24,5.97-3.14h36.95c-3.21-2.34-6.08-5.11-8.53-8.24ZM30.64,47.97c-2,0-3.62-1.62-3.62-3.62s1.62-3.62,3.62-3.62,3.63,1.62,3.63,3.62-1.63,3.62-3.63,3.62Zm-23.39,26.47c-2,0-3.62-1.63-3.62-3.62s1.63-3.63,3.62-3.63,3.63,1.63,3.63,3.63-1.63,3.62-3.63,3.62Zm20.97,16.45c-2,0-3.63-1.63-3.63-3.63s1.63-3.62,3.63-3.62,3.62,1.63,3.62,3.62-1.63,3.63-3.62,3.63Zm14.08,18.39c-2,0-3.62-1.62-3.62-3.62s1.62-3.62,3.62-3.62,3.62,1.62,3.62,3.62-1.62,3.62-3.62,3.62Z"></path>
                                      <circle class="cls-5n" cx="59.23" cy="62.54" r="3.63"></circle>
                                      <circle class="cls-7n" cx="58.24" cy="79" r="3.63" transform="translate(-16.54 15.44) rotate(-13.24)"></circle>
                                    </g>
                                    <g>
                                      <g>
                                        <path class="cls-3n" d="m104.63,78.16c-9.44,6-14.52,2.86-15.25-9.4,4.93,3.38,10.02,6.52,15.25,9.4Z"></path>
                                        <path class="cls-3n" d="m131.01,68.76c-.72,12.26-5.81,15.4-15.25,9.4,5.24-2.88,10.32-6.02,15.25-9.4Z"></path>
                                      </g>
                                      <path class="cls-4n" d="m110.16,40.13c-.89,0-1.78.03-2.66.1-.08,0-.16,0-.24.02-9.59.79-18.07,5.45-23.89,12.42-.25.3-.49.59-.73.89-.14.19-.29.38-.43.57-4.02,5.37-6.52,11.93-6.88,19.07,0,.01,0,.03,0,.04-.01.22-.02.45-.03.68-.01.3-.02.6-.02.9v.18c0,.28,0,.55,0,.83.44,18.64,15.5,33.66,34.15,34.04.24,0,.48,0,.72,0,.3,0,.6,0,.9,0,18.85-.48,33.98-15.9,33.98-34.87s-15.61-34.88-34.88-34.88Zm.19,39.35c-14.41,14.37-29.15,1.23-29.15-9.45s13.05,0,29.15,0,29.15-10.67,29.15,0-14.74,23.81-29.15,9.45Z"></path>
                                    </g>
                                  </g>
                                  <g>
                                    <path class="cls-2n" d="m286.65,118.51h-17.37l-39.32-59.42v59.42h-17.37V31.8h17.37l39.32,59.54V31.8h17.37v86.71Z"></path>
                                    <path class="cls-2n" d="m304.95,38.69c-2.03-1.94-3.04-4.36-3.04-7.26s1.01-5.31,3.04-7.26c2.03-1.94,4.57-2.92,7.63-2.92s5.6.97,7.63,2.92c2.02,1.94,3.04,4.36,3.04,7.26s-1.02,5.31-3.04,7.26c-2.03,1.94-4.57,2.91-7.63,2.91s-5.6-.97-7.63-2.91Zm16.19,11.1v68.72h-17.37V49.79h17.37Z"></path>
                                    <path class="cls-2n" d="m396.18,56.55c5.04,5.17,7.57,12.38,7.57,21.65v40.31h-17.36v-37.96c0-5.46-1.37-9.65-4.1-12.59-2.73-2.94-6.45-4.4-11.16-4.4s-8.58,1.47-11.35,4.4c-2.77,2.94-4.15,7.13-4.15,12.59v37.96h-17.37V49.79h17.37v8.56c2.31-2.98,5.27-5.31,8.87-7.01,3.6-1.69,7.55-2.54,11.85-2.54,8.19,0,14.8,2.59,19.85,7.75Z"></path>
                                    <path class="cls-2n" d="m437.61,129.8c0,7.61-1.88,13.09-5.64,16.44-3.77,3.35-9.16,5.02-16.19,5.02h-7.69v-14.76h4.96c2.64,0,4.51-.52,5.58-1.55,1.07-1.03,1.61-2.71,1.61-5.02V49.79h17.37v80.01Zm-16.31-91.11c-2.03-1.94-3.04-4.36-3.04-7.26s1.01-5.31,3.04-7.26c2.02-1.94,4.61-2.92,7.75-2.92s5.58.97,7.57,2.92c1.98,1.94,2.98,4.36,2.98,7.26s-.99,5.31-2.98,7.26c-1.98,1.94-4.51,2.91-7.57,2.91s-5.73-.97-7.75-2.91Z"></path>
                                    <path class="cls-2n" d="m454.42,65.42c2.77-5.37,6.53-9.51,11.29-12.4,4.76-2.89,10.07-4.34,15.94-4.34,5.13,0,9.61,1.04,13.46,3.1,3.85,2.07,6.92,4.67,9.24,7.82v-9.8h17.49v68.72h-17.49v-10.05c-2.23,3.23-5.31,5.89-9.24,8-3.93,2.11-8.46,3.16-13.58,3.16-5.79,0-11.06-1.49-15.82-4.47-4.76-2.98-8.52-7.17-11.29-12.59-2.77-5.41-4.16-11.64-4.16-18.67s1.38-13.11,4.16-18.48Zm47.45,7.88c-1.65-3.02-3.89-5.33-6.7-6.95-2.81-1.61-5.83-2.42-9.06-2.42s-6.2.79-8.93,2.36c-2.73,1.57-4.94,3.87-6.64,6.88-1.69,3.02-2.54,6.6-2.54,10.73s.85,7.75,2.54,10.85c1.69,3.1,3.93,5.48,6.7,7.13,2.77,1.66,5.72,2.48,8.87,2.48s6.24-.81,9.06-2.42c2.81-1.61,5.04-3.93,6.7-6.95,1.65-3.02,2.48-6.64,2.48-10.85s-.83-7.83-2.48-10.85Z"></path>
                                    <path class="cls-4n" d="m591.93,102.01h-34.48l-5.71,16.5h-18.24l31.14-86.71h20.22l31.13,86.71h-18.36l-5.71-16.5Zm-4.71-13.89l-12.53-36.22-12.53,36.22h25.06Z"></path>
                                    <path class="cls-4n" d="m645.02,31.93v86.58h-17.37V31.93h17.37Z"></path>
                                  </g>
                                </g>
                              </g>
                            </svg>
                <div class="f-14 f-md-16 w400 mt20 lh160 white-clr text-center">
					Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. 
				</div>
				<div class=" mt20 mt-md40 white-clr"> <script type="text/javascript" src="https://warriorplus.com/o2/disclaimer/rbhnkl" defer=""></script><div class="wplus_spdisclaimer"><strong>Disclaimer:</strong> <em>WarriorPlus is used to help manage the sale of products on this site. While WarriorPlus helps facilitate the sale, all payments are made directly to the product vendor and NOT WarriorPlus. Thus, all product questions, support inquiries and/or refund requests must be sent to the vendor. WarriorPlus's role should not be construed as an endorsement, approval or review of these products or any claim, statement or opinion used in the marketing of these products.</em></div></div>
			</div>
            <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                <div class="f-14 f-md-16 w400 lh160 white-clr text-center">Copyright © Ninja Ai</div>
                    <ul class="footer-ul w400 f-14 f-md-16 white-clr text-center text-md-right">
                        <li><a href="https://support.bizomart.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                        <li><a href="http://getninjaai.com/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                        <li><a href="http://getninjaai.com/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                        <li><a href="http://getninjaai.com/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                        <li><a href="http://getninjaai.com/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                        <li><a href="http://getninjaai.com/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                        <li><a href="http://getninjaai.com/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!--Footer Section End -->


	<!-- Exit Pop-Box Start -->
	<!-- Load UC Bounce Modal CDN Start -->
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/ouibounce/0.0.11/ouibounce.min.js"></script>

	<!-- <div id="ouibounce-modal" style="z-index:77777; display:none;">
	    <div class="underlay"></div>
	    	<div class="modal popupbg" style="display:block; ">
	        <div class="modal-header" style="border:0; padding:0;">
	            <button type="button" class="close" onclick="document.getElementById('ouibounce-modal').style.display = 'none';">&times;</button>
	       	</div>

	        <div class="modal-body">
	            <div class="innerbody">
	                <div class="col-lg-12 col-md-12 col-md-12 col-md-offset-0 col-12 col-offset-0 mt2 xsmt2">
	                    <img src="assets/images/waitimg.png" class="img-fluid mt2 xsmt2">
	                    <h2 class="text-center mt2 w500 sm25 xs20 lh160">I HAVE SOMETHING SPECIAL<br /> FOR YOU</h2>
	                    <a href="<?php echo $_GET['afflink']; ?>" class="createfree md20 sm18 xs16" style="color:#ffffff;">Stay</a>
	                </div>
	            </div>
	        </div>
	    </div>
	</div> -->

	<div class="modal fade in" id="ouibounce-modal" role="dialog" style="display:none; overflow-y:auto;">
		<div class="modal-dialog modal-dg">
			<button type="button" class="close" onclick="document.getElementById('ouibounce-modal').style.display = 'none';">&times;</button>	 
			<div class="col-12 modal-content pop-bg pop-padding">			
				<div class="col-12 modal-body text-center border-pop ">	
					<div class="col-12 ">
						<img src="assets/images/waitimg.png" class="img-fluid">
					</div>
					
					<div class="col-12 text-center mt20 mt-md25 ">
						<div class="f-20 f-md-28 lh160 w600">I HAVE SOMETHING SPECIAL <br class="d-lg-block d-none"> FOR YOU</div>
					</div>

					<div class="col-12 mt20 mt-md20">
						<div class="link-btn">
							<a href="<?php echo $_GET['afflink'];?>">STAY</a>
						</div>
					</div>
				</div>		
			</div>
		</div>
	</div>

	<!-- <script type="text/javascript">
        var _ouibounce = ouibounce(document.getElementById('ouibounce-modal'),{
        aggressive: true, //Making this true makes ouibounce not to obey "once per visitor" rule
        });
    </script> -->
	<!-- Load UC Bounce Modal CDN End -->
	<!-- Exit Pop-Box End -->


	
  <!-- timer --->
  <?php
	 if ($now < $exp_date) {
	 
	 ?>
  <script type="text/javascript">
	 // Count down milliseconds = server_end - server_now = client_end - client_now
	 var server_end = <?php echo $exp_date; ?> * 1000;
	 var server_now = <?php echo time(); ?> * 1000;
	 var client_now = new Date().getTime();
	 var end = server_end - server_now + client_now; // this is the real end time
	 
	 var noob = $('.countdown').length;
	 
	 var _second = 1000;
	 var _minute = _second * 60;
	 var _hour = _minute * 60;
	 var _day = _hour * 24
	 var timer;
	 
	 function showRemaining() {
	  var now = new Date();
	  var distance = end - now;
	  if (distance < 0) {
		 clearInterval(timer);
		 document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
		 return;
	  }
	 
	  var days = Math.floor(distance / _day);
	  var hours = Math.floor((distance % _day) / _hour);
	  var minutes = Math.floor((distance % _hour) / _minute);
	  var seconds = Math.floor((distance % _minute) / _second);
	  if (days < 10) {
		 days = "0" + days;
	  }
	  if (hours < 10) {
		 hours = "0" + hours;
	  }
	  if (minutes < 10) {
		 minutes = "0" + minutes;
	  }
	  if (seconds < 10) {
		 seconds = "0" + seconds;
	  }
	  var i;
	  var countdown = document.getElementsByClassName('countdown');
	  for (i = 0; i < noob; i++) {
		 countdown[i].innerHTML = '';
	 
		 if (days) {
			 countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + days + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 smmltd">Days</span> </div>';
		 }
	 
		 countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + hours + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>';
	 
		 countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">' + minutes + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>';
	 
		 countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">' + seconds + '</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>';
	  }
	 
	 }
	 timer = setInterval(showRemaining, 1000);
  </script>
  <?php
	 } else {
	 echo "Times Up";
	 }
	 ?>
  <!--- timer end-->
</body>
</html>

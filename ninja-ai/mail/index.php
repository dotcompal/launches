<!DOCTYPE html>
<html>
   <head>
      <title>NinjaAi | Mail</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=9">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
      <meta name="title" content="NinjaAi | Mail">
      <meta name="description" content="Most Special and Powerful Tag & Segment Based Email Software That Gets Us 4X More Opening & Click without Any Monthly Fee Ever!">
      <meta name="keywords" content="NinjaAi | Mail">
      <meta property="og:image" content="https://www.getninjaai.com/mail/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Pranshu Gupta">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="NinjaAi | Mail">
      <meta property="og:description" content="Most Special and Powerful Tag & Segment Based Email Software That Gets Us 4X More Opening & Click without Any Monthly Fee Ever!">
      <meta property="og:image" content="https://www.getninjaai.com/mail/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="NinjaAi | Mail">
      <meta property="twitter:description" content="Most Special and Powerful Tag & Segment Based Email Software That Gets Us 4X More Opening & Click without Any Monthly Fee Ever!">
      <meta property="twitter:image" content="https://www.getninjaai.com/mail/thumbnail.png">
      <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
      <link rel="preconnect" href="https://fonts.googleapis.com">
       <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&family=Inter:wght@300;400;500;600;700;800;900&display=swap" rel="stylesheet">
      <link rel="stylesheet" href="../common_assets/css/bootstrap2.min.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style.css" type="text/css" />
      <link rel="stylesheet" href="assets/css/timer.css" type="text/css" />
      <script src="https://cdn.oppyo.com/launches/webpull/common_assets/js/jquery.min.js"></script>   


   </head>
   <body>
   <div class="warning-section">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <div class="offer d-flex justify-content-center align-items-center">
                        <div>
                            <img src="https://cdn.oppyo.com/launches/appzilo/special/error.webp">
                        </div>
                        <div class="f-22 f-md-28 w600 lh140 white-clr text-center">
                        ATTENTION: Avoid This Offer & Be Ready To Pay A Fortune...
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
      <!-- Header Section Start -->
      <div class="header-section">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                  <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 645.02 151.26" style="max-height:70px">
                     <defs>
                        <style>
                           .cls-1 {
                           fill: url(#linear-gradient-2);
                           }
                           .cls-2 {
                           fill: #fff;
                           }
                           .cls-3 {
                           fill-rule: evenodd;
                           }
                           .cls-3, .cls-4 {
                           fill: #ff813b;
                           }
                           .cls-5 {
                           fill: url(#linear-gradient-3);
                           }
                           .cls-6 {
                           fill: url(#linear-gradient);
                           }
                           .cls-7 {
                           fill: url(#linear-gradient-4);
                           }
                        </style>
                        <linearGradient id="linear-gradient" x1="47.81" y1="75" x2="185.48" y2="75" gradientUnits="userSpaceOnUse">
                           <stop offset="0" stop-color="#b956ff"></stop>
                           <stop offset="1" stop-color="#6e33ff"></stop>
                        </linearGradient>
                        <linearGradient id="linear-gradient-2" x1="0" y1="75" x2="85.18" y2="75" gradientUnits="userSpaceOnUse">
                           <stop offset="0" stop-color="#b956ff"></stop>
                           <stop offset="1" stop-color="#a356fa"></stop>
                        </linearGradient>
                        <linearGradient id="linear-gradient-3" x1="55.61" y1="62.54" x2="62.86" y2="62.54" gradientUnits="userSpaceOnUse">
                           <stop offset="0" stop-color="#b956ff"></stop>
                           <stop offset="1" stop-color="#ac59fa"></stop>
                        </linearGradient>
                        <linearGradient id="linear-gradient-4" x1="54.62" y1="79" x2="61.87" y2="79" gradientTransform="translate(19.64 -11.24) rotate(13.24)" xlink:href="#linear-gradient-3"></linearGradient>
                     </defs>
                     <g id="Layer_1-2" data-name="Layer 1">
                        <g>
                           <g>
                           <g>
                              <path class="cls-6" d="m181.49,58.53h-10.72c-1.4-5.13-3.43-9.99-6.01-14.51l7.58-7.58c1.56-1.56,1.56-4.09,0-5.65l-17.65-17.65c-1.56-1.56-4.09-1.56-5.65,0l-7.58,7.58c-4.51-2.58-9.38-4.61-14.51-6.01V4c0-2.21-1.79-4-4-4h-24.96c-2.21,0-4,1.79-4,4v10.72c-.16.04-.31.09-.47.14-.27.08-.53.15-.8.23-.27.08-.55.16-.82.24-.55.17-1.09.35-1.63.53-.11.04-.22.08-.33.12-.48.17-.96.34-1.44.52-.12.04-.24.09-.36.14-.52.2-1.04.4-1.56.62-.04.02-.08.03-.12.05-2.41,1-4.74,2.15-6.99,3.43l-7.57-7.57c-1.56-1.56-4.09-1.56-5.65,0l-17.65,17.65c-.36.36-.63.76-.82,1.2h61.99v.02c.23,0,.45-.02.68-.02,23.76,0,43.01,19.26,43.01,43.01s-19.26,43.01-43.01,43.01c-.5,0-1-.02-1.5-.04v.04h-61.17c.19.43.46.84.82,1.2l17.65,17.65c1.56,1.56,4.09,1.56,5.65,0l7.57-7.57c2.25,1.28,4.58,2.43,6.99,3.43.04.02.08.03.12.05.51.21,1.03.42,1.55.62.12.05.24.09.36.14.47.18.95.35,1.43.52.11.04.23.08.34.12.54.18,1.08.36,1.63.53.27.08.55.16.83.25.26.08.53.16.79.23.16.04.31.09.47.14v10.72c0,2.21,1.79,4,4,4h24.96c2.21,0,4-1.79,4-4v-10.72c5.13-1.4,9.99-3.43,14.51-6.01l7.58,7.58c1.56,1.56,4.09,1.56,5.65,0l17.65-17.65c1.56-1.56,1.56-4.09,0-5.65l-7.58-7.58c2.58-4.51,4.61-9.38,6.01-14.51h10.72c2.21,0,4-1.79,4-4v-24.96c0-2.21-1.79-4-4-4Z"></path>
                              <path class="cls-1" d="m76.64,101.54c-5.74-7.31-9.18-16.52-9.18-26.54s3.44-19.23,9.18-26.54c2.45-3.13,5.33-5.9,8.53-8.24h-48.39c-1.31-1.89-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.13h17.07c-1.51,3.21-2.76,6.58-3.71,10.07h3.1c1.31-1.89,3.49-3.13,5.97-3.13,4.01,0,7.25,3.25,7.25,7.25s-3.22,7.23-7.21,7.25h-.04c-2.48,0-4.66-1.24-5.97-3.13H13.22c-.44-.64-.98-1.19-1.59-1.65-1.21-.92-2.73-1.48-4.38-1.48-4,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.14h38.82c1.31-1.89,3.49-3.13,5.97-3.13.43,0,.84.05,1.25.12,3.41.59,6,3.56,6,7.14,0,2.78-1.56,5.19-3.86,6.41-1.01.53-2.16.84-3.39.84-2.48,0-4.66-1.24-5.97-3.13h-17.85c-1.31-1.9-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.13h15.96c.95,3.48,2.2,6.85,3.71,10.07h-5.63c-1.31-1.89-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25,0,1.53.48,2.95,1.28,4.12,1.31,1.9,3.49,3.14,5.97,3.14s4.66-1.24,5.97-3.14h36.95c-3.21-2.34-6.08-5.11-8.53-8.24ZM30.64,47.97c-2,0-3.62-1.62-3.62-3.62s1.62-3.62,3.62-3.62,3.63,1.62,3.63,3.62-1.63,3.62-3.63,3.62Zm-23.39,26.47c-2,0-3.62-1.63-3.62-3.62s1.63-3.63,3.62-3.63,3.63,1.63,3.63,3.63-1.63,3.62-3.63,3.62Zm20.97,16.45c-2,0-3.63-1.63-3.63-3.63s1.63-3.62,3.63-3.62,3.62,1.63,3.62,3.62-1.63,3.63-3.62,3.63Zm14.08,18.39c-2,0-3.62-1.62-3.62-3.62s1.62-3.62,3.62-3.62,3.62,1.62,3.62,3.62-1.62,3.62-3.62,3.62Z"></path>
                              <circle class="cls-5" cx="59.23" cy="62.54" r="3.63"></circle>
                              <circle class="cls-7" cx="58.24" cy="79" r="3.63" transform="translate(-16.54 15.44) rotate(-13.24)"></circle>
                           </g>
                           <g>
                              <g>
                                 <path class="cls-3" d="m104.63,78.16c-9.44,6-14.52,2.86-15.25-9.4,4.93,3.38,10.02,6.52,15.25,9.4Z"></path>
                                 <path class="cls-3" d="m131.01,68.76c-.72,12.26-5.81,15.4-15.25,9.4,5.24-2.88,10.32-6.02,15.25-9.4Z"></path>
                              </g>
                              <path class="cls-4" d="m110.16,40.13c-.89,0-1.78.03-2.66.1-.08,0-.16,0-.24.02-9.59.79-18.07,5.45-23.89,12.42-.25.3-.49.59-.73.89-.14.19-.29.38-.43.57-4.02,5.37-6.52,11.93-6.88,19.07,0,.01,0,.03,0,.04-.01.22-.02.45-.03.68-.01.3-.02.6-.02.9v.18c0,.28,0,.55,0,.83.44,18.64,15.5,33.66,34.15,34.04.24,0,.48,0,.72,0,.3,0,.6,0,.9,0,18.85-.48,33.98-15.9,33.98-34.87s-15.61-34.88-34.88-34.88Zm.19,39.35c-14.41,14.37-29.15,1.23-29.15-9.45s13.05,0,29.15,0,29.15-10.67,29.15,0-14.74,23.81-29.15,9.45Z"></path>
                           </g>
                           </g>
                           <g>
                           <path class="cls-2" d="m286.65,118.51h-17.37l-39.32-59.42v59.42h-17.37V31.8h17.37l39.32,59.54V31.8h17.37v86.71Z"></path>
                           <path class="cls-2" d="m304.95,38.69c-2.03-1.94-3.04-4.36-3.04-7.26s1.01-5.31,3.04-7.26c2.03-1.94,4.57-2.92,7.63-2.92s5.6.97,7.63,2.92c2.02,1.94,3.04,4.36,3.04,7.26s-1.02,5.31-3.04,7.26c-2.03,1.94-4.57,2.91-7.63,2.91s-5.6-.97-7.63-2.91Zm16.19,11.1v68.72h-17.37V49.79h17.37Z"></path>
                           <path class="cls-2" d="m396.18,56.55c5.04,5.17,7.57,12.38,7.57,21.65v40.31h-17.36v-37.96c0-5.46-1.37-9.65-4.1-12.59-2.73-2.94-6.45-4.4-11.16-4.4s-8.58,1.47-11.35,4.4c-2.77,2.94-4.15,7.13-4.15,12.59v37.96h-17.37V49.79h17.37v8.56c2.31-2.98,5.27-5.31,8.87-7.01,3.6-1.69,7.55-2.54,11.85-2.54,8.19,0,14.8,2.59,19.85,7.75Z"></path>
                           <path class="cls-2" d="m437.61,129.8c0,7.61-1.88,13.09-5.64,16.44-3.77,3.35-9.16,5.02-16.19,5.02h-7.69v-14.76h4.96c2.64,0,4.51-.52,5.58-1.55,1.07-1.03,1.61-2.71,1.61-5.02V49.79h17.37v80.01Zm-16.31-91.11c-2.03-1.94-3.04-4.36-3.04-7.26s1.01-5.31,3.04-7.26c2.02-1.94,4.61-2.92,7.75-2.92s5.58.97,7.57,2.92c1.98,1.94,2.98,4.36,2.98,7.26s-.99,5.31-2.98,7.26c-1.98,1.94-4.51,2.91-7.57,2.91s-5.73-.97-7.75-2.91Z"></path>
                           <path class="cls-2" d="m454.42,65.42c2.77-5.37,6.53-9.51,11.29-12.4,4.76-2.89,10.07-4.34,15.94-4.34,5.13,0,9.61,1.04,13.46,3.1,3.85,2.07,6.92,4.67,9.24,7.82v-9.8h17.49v68.72h-17.49v-10.05c-2.23,3.23-5.31,5.89-9.24,8-3.93,2.11-8.46,3.16-13.58,3.16-5.79,0-11.06-1.49-15.82-4.47-4.76-2.98-8.52-7.17-11.29-12.59-2.77-5.41-4.16-11.64-4.16-18.67s1.38-13.11,4.16-18.48Zm47.45,7.88c-1.65-3.02-3.89-5.33-6.7-6.95-2.81-1.61-5.83-2.42-9.06-2.42s-6.2.79-8.93,2.36c-2.73,1.57-4.94,3.87-6.64,6.88-1.69,3.02-2.54,6.6-2.54,10.73s.85,7.75,2.54,10.85c1.69,3.1,3.93,5.48,6.7,7.13,2.77,1.66,5.72,2.48,8.87,2.48s6.24-.81,9.06-2.42c2.81-1.61,5.04-3.93,6.7-6.95,1.65-3.02,2.48-6.64,2.48-10.85s-.83-7.83-2.48-10.85Z"></path>
                           <path class="cls-4" d="m591.93,102.01h-34.48l-5.71,16.5h-18.24l31.14-86.71h20.22l31.13,86.71h-18.36l-5.71-16.5Zm-4.71-13.89l-12.53-36.22-12.53,36.22h25.06Z"></path>
                           <path class="cls-4" d="m645.02,31.93v86.58h-17.37V31.93h17.37Z"></path>
                           </g>
                        </g>
                     </g>
                     </svg>
               </div>
               <!--<div class="col-md-7 mt20 mt-md0">-->
               <!--   <ul class="leader-ul f-16 f-md-18 w500 white-clr text-md-end text-center">-->
               <!--      <li><a href="#features" class="white-clr">Features</a></li>-->
               <!--      <li><a href="#demo" class="white-clr">Demo</a></li>-->
               <!--      <li class="d-md-none affiliate-link-btn"><a href="https://warriorplus.com/o2/buy/d8j629/hy9rz2/d410t1" class="white-clr">Buy Now</a></li>-->
               <!--   </ul>-->
               <!--</div>-->
               <!--<div class="col-md-2 affiliate-link-btn text-md-end text-center mt15 mt-md0 d-none d-md-block f-18">-->
               <!--   <a href="https://warriorplus.com/o2/buy/d8j629/hy9rz2/d410t1">Buy Now</a>-->
               <!--</div>-->
               <div class="col-12 text-center mt20 mt-md60">
                  <div class="f-md-24 f-18 w500 text-center white-clr pre-heading lh150">
                  Finally Aweber, GetResponse & MailChimp Killer Is Here ...
                  </div>
               </div>
               <div class="col-12 mt-md50 mt20 f-md-44 f-28 w700 text-center white-clr lh150">
                  <span class="orange-clr"> World’s First ChatGPT AI  -  Powered Email Marketing App</span> Writes, Design & Send  Unlimited Profit Pulling  Emails <span class="orange-underline"> with Just One Keyword </span> for Tons of Traffic, Commissions, & Sales on Autopilot                
                  
               </div>
               <div class="col-12 mt-md50 mt20 text-center">
                  <div class="post-heading f-16 f-md-24 w500 text-center lh140 white-clr text-capitalize">
                  Say Goodbye to Expensive Email Marketing Apps, Writers, & Freelancers. Even A Kid Can Start & Make 6 Figure Email Marketing Income with NinjaAi Mail. No Monthly Fee Ever…
                  </div>
               </div>
               <!--<div class="col-12 mt-md30 mt20 text-center">-->
               <!--   <div class="f-16 f-md-24 w500 text-center lh140 white-clr text-capitalize">-->
               <!--   Say No To Expensive Email Marketing Tools / Autoresponders, Email Writers & Designers-->
               <!--   </div>-->
               <!--</div>-->
               <div class="col-12 mt20 mt-md80">
                  <div class="row align-items-center">
                     <div class="col-md-7 col-12">
                        <img src="assets/images/product-box.webp" class="img-fluid d-block mx-auto">
                         <!-- <div class="col-12 responsive-video">
                           <iframe src="https://mailgpt.dotcompal.com/video/embed/xajnf33ejg" style="width:100%; height:100%" frameborder="0" allow="fullscreen" allowfullscreen ></iframe>
                        </div>  -->
                        <div class="col-12 mt20 mt-md40 d-none d-md-block">
                           <div class="row">
                              <div class="col-md-10 mx-auto col-12 mt20 mt-md30 text-center ">
                                 <a href="#buynow" class="cta-link-btn px0 d-block">Get Instant Access To NinjaAi Mail</a>
                              </div>
                           </div>
                           <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30 ">
                              <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/compaitable-with1.webp " class="img-fluid d-block mx-xs-center md-img-right ">
                              <div class="d-md-block d-none px-md30 "><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/v-line.webp " class="img-fluid "></div>
                              <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/days-gurantee1.webp " class="img-fluid d-block mx-xs-auto mt15 mt-md0 ">
                           </div>
                        </div>
                     </div>
                     <div class="col-md-5 col-12 mt20 mt-md0">
                        <div class="key-features-bg">
                           <ul class="list-head pl0 m0 f-20 lh150 w400 white-clr">
                              <li>Create 1000s Of High-Converting Emails With Just One Keyword</li>
                              <li>Just Enter Keyword & NinjaAi Mail AI Plans And Writes Email Content For You</li> 
                              <li>Send Unlimited Emails To Unlimited Subscribers. </li>
                              <li>AI Enabled Smart Tagging For Lead Personalization & Traffic</li> 
                              <li>Collect Unlimited Leads  With Inbuilt Forms </li>
                              <li>Design Newsletter & Autoresponder Mails Using The Power Of AI</li>
                              <li>Free SMTP For Unrestricted Mailing </li>
                              <li>Upload Unlimited List with Zero Restrictions </li>
                              <li>100% Control on your online business</li> 
                              <li>100+ High Converting Templates For Webforms & Email</li>
                              <li>No Monthly Fee Forever. Pay One Time</li>
                              <li>GDPR & Can-Spam Compliant</li>
                              <li>Commercial License Included</li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
     
   <!--<div class="step-section">-->
   <!--   <div class="container ">-->
   <!--      <div class="row ">-->
   <!--         <div class="col-12 f-28 f-md-45 w600 lh140 black-clr text-center">-->
   <!--         Create & Send Profit-Pulling Emails with  <br class="d-none d-md-block"><span class="blue-gradient d-inline-grid">NinjaAi Mail in Just 3 Simple Clicks-->
   <!--            <img src="assets/images/let-line.webp" class="img-fluid mx-auto"></span>-->
   <!--         </div>-->
   <!--         <div class="col-12 mt30 mt-md80">-->
   <!--            <div class="row align-items-center">-->
   <!--               <div class="col-12 col-md-5 relative">-->
   <!--                  <img src="assets/images/step1.webp" class="img-fluid">-->
   <!--                  <div class=" step1 f-28 f-md-65 w600 mt15">-->
   <!--                  Login & Enter a <br class="d-none d-md-block"> Keyword-->
   <!--                  </div>-->
   <!--                  <div class="f-18 f-md-20 w400 lh140 mt15">-->
   <!--                  Login to your Mail GPT account & just enter a desired keyword for which you want to send an Email-->
   <!--                  </div>-->
   <!--                  <img src="assets/images/left-arrow.webp" class="img-fluid d-none d-md-block ms-auto step-arrow1">-->
   <!--               </div>-->
   <!--               <div class="col-12 col-md-7 mt20 mt-md0 ">-->
   <!--                  <img src="assets/images/step1.gif" class="img-fluid d-block mx-auto gif-bg">-->
   <!--               </div>-->
   <!--            </div>-->
   <!--         </div>-->
   <!--         <div class="col-12 mt-md150">-->
   <!--            <div class="row align-items-center ">-->
   <!--               <div class="col-12 col-md-5 order-md-2 relative">-->
   <!--                  <img src="assets/images/step2.webp" class="img-fluid">-->
   <!--                  <div class=" step2 f-28 f-md-65 w600 mt15">-->
   <!--                  Generate-->
   <!--                  </div>-->
   <!--                  <div class="f-18 f-md-20 w400 lh140 mt15">-->
   <!--                  NinjaAi Mail AI generates profitable, engaging & high-quality email for you in seconds.-->
   <!--                  </div>-->
   <!--                  <img src="assets/images/right-arrow.webp" class="img-fluid d-none d-md-block step-arrow2">-->
   <!--               </div>-->
   <!--               <div class="col-md-7 mt20 mt-md0 order-md-1 ">-->
   <!--                  <img src="assets/images/step2.gif" class="img-fluid d-block mx-auto gif-bg">-->
   <!--               </div>-->
   <!--            </div>-->
   <!--         </div>-->
   <!--         <div class="col-12 mt-md150">-->
   <!--            <div class="row align-items-center">-->
   <!--               <div class="col-12 col-md-5">-->
   <!--                  <img src="assets/images/step3.webp" class="img-fluid">-->
   <!--                  <div class=" step3 f-28 f-md-65 w600 mt15">-->
   <!--                     Profit-->
   <!--                  </div>-->
   <!--                  <div class="f-18 f-md-20 w400 lh140 mt15">-->
   <!--                    Send unlimited emails directly into inbox for tons of autopilot profits with just push of a button.-->
   <!--                  </div>-->
   <!--               </div>-->
   <!--               <div class="col-12 col-md-7 mt20 mt-md0">-->
   <!--                  <video width="100%" height="auto" loop="" autoplay="" muted="muted" class="gif-bg" >-->
   <!--                <source src="assets/images/step3.mp4" type="video/mp4" >-->
   <!--            </video>-->
   <!--               </div>-->
   <!--            </div>-->
   <!--         </div>-->
   <!--          <div class="col-12 f-md-24 f-20 w700 lh140 text-center mt20 mt-md40 aos-init aos-animate" data-aos="fade-up">-->
   <!--            <div class="smile-text">It Just Takes Less than 60 Seconds...</div>-->
   <!--         </div>-->
            
   <!--         <div class="col-12 text-center">-->
               
   <!--            <div class="d-flex gap-2 gap-md-1 justify-content-center flex-wrap mt20 mt-md30">-->
   <!--               <div class="d-flex align-items-center gap-2 justify-content-center">-->
   <!--                  <img src="assets/images/red-cross.webp" alt="Cross" class="img-fluid d-block">-->
   <!--                  <div class="f-md-28 w700 lh140 f-20 red-clr"> No Email Writing &nbsp; </div>-->
   <!--               </div>-->
   <!--               <div class="d-flex align-items-center gap-2 justify-content-center">-->
   <!--                  <img src="assets/images/red-cross.webp" alt="Cross" class="img-fluid d-block">-->
   <!--                  <div class="f-md-28 w700 lh140 f-20 red-clr"> No Designer &nbsp;</div>-->
   <!--               </div>-->
   <!--               <div class="d-flex align-items-center gap-2 justify-content-center">-->
   <!--                  <img src="assets/images/red-cross.webp" alt="Cross" class="img-fluid d-block">-->
   <!--                  <div class="f-md-28 w700 lh140 f-20 red-clr">No Freelancer &nbsp;</div>-->
   <!--               </div>-->
   <!--               <div class="d-flex align-items-center gap-2 justify-content-center">-->
   <!--                  <img src="assets/images/red-cross.webp" alt="Cross" class="img-fluid d-block">-->
   <!--                  <div class="f-md-28 w700 lh140 f-20 red-clr">No 3rd Party Apps &nbsp;</div>-->
   <!--               </div>-->
   <!--               <div class="d-flex align-items-center gap-2 justify-content-center">-->
   <!--                  <img src="assets/images/red-cross.webp" alt="Cross" class="img-fluid d-block">-->
   <!--                  <div class="f-md-28 w700 lh140 f-20 red-clr">No SMTP </div>-->
   <!--               </div>-->
   <!--            </div>-->
   <!--         </div>-->
   <!--        <div class="col-12 f-18 f-md-24 w400 lh140 black-clr text-center mt20 mt-md30">-->
   <!--               Plus, with included free commercial license, this is the easiest & fastest way to start 6 figure business and help desperate businesses in no time! -->
   <!--            </div>-->
   <!--      </div>-->
   <!--      <div class="row">-->
   <!--         <div class="col-md-10 mx-auto col-12 mt20 mt-md30 text-center ">-->
   <!--            <a href="#buynow" class="cta-link-btn px0 d-block">Get Instant Access To NinjaAi Mail</a>-->
   <!--         </div>-->
   <!--      </div>-->
   <!--   </div>-->
   <!--</div>-->

   
   <section class="future-section">
      <div class="container">
         <div class="row">
               <div class="col-12 f-28 f-md-45 w700 lh130 black-clr text-center thunder">
                  NinjaAi Mail Is FUTURE Of Email Marketing
                  <img src="assets/images/double-blue-line.png" alt="line" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 f-18 f-md-28 w400 lh140 black-clr text-center mt20 mt-md30">
                  Here you Can See What you Can Do With This Fully ChatGPT Cutting<br class="d-none d-md-block"> Edge Software.
               </div>
               <div class="col-md-12 mt-md80 mt20">
               <img src="assets/images/future-img.webp" alt="" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-md-12 mt-md80 mt20">
                  <img src="assets/images/yes-you-can.png" alt="" class="img-fluid d-block mx-auto">
               </div>
               <div class="f-18 f-md-28 w400 lh150 black-clr text-center mt20 mt-md60">
                 <span class="blue-gradient1 under w600"> The Best Thing is You Just Need to Enter a Keyword </span> And NinjaAi Mail Will Create, <br class="d-none d-md-block">Design & Send Emails For You In Just 60 Seconds Flat.
               </div>
         </div>
      </div>
   </section> 

      
      <!-- Proof Section -->
      <div class="proof-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-42 w700 lh140 black-clr">
                     Checkout How We’ve Got 20,000+ Clicks & Made Over $25,233 Income by Using the Power of NinjaAi Mail
                  </div>
                 
               </div>
               <div class="col-12 mx-auto mt20 mt-md50">
                  <img src="assets/images/proof.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>
            <div class="row mt-md70 mt30 align-items-center">
               <div class="col-md-6 f-md-32 f-22 w600 text-center text-md-start">
                  Got 20K+ Visitors in Just 15 Days on My Affiliate Offers Using the Power of Emails  
               </div>
               <div class="col-md-6 mt-md0 mt20">
                  <img src="assets/images/proof1.webp" alt="" class="img-fluid d-block mx-auto">
               </div>
            </div>
            <div class="row mt-md70 mt30 align-items-center">
               <div class="col-md-6 f-md-32 f-22 w600 order-md-2 text-center text-md-start">
                  And Let’s Check Out the Crazy Open And Click Rates 
                  We’ve Got for A Simple Email I Sent Using NinjaAi Mail.
               </div>
               <div class="col-md-6 mt-md0 mt20 order-md-1">
                  <img src="assets/images/proof2.webp" alt="" class="img-fluid d-block mx-auto">
               </div>
            </div>
         </div>
      </div>
      <!-- Proof Section End --> 
     
    
      <div class="unstoppable-section">
         <div class="container">
            <div class="row">
               <div class=" col-12 text-center mx-auto">
                  <div class="f-22 f-md-55 w700 lh140 text-center white-clr unstoppable ">
                     ChatGPT is UNSTOPPABLE 
                  </div>
                  <div class="f-28 f-md-45 w500 lh140 black-clr mt20 mt-md30">
                  And It’s Bigger After Acquisition & Investment of 10 Billion Dollars By Microsoft 
                  </div>
                  <div class="mt20 mt-md30 chatgpt-head-design">
                     <div class="f-32 f-md-42 w700 black-clr lh140">
                     ChatGPT Is The Game Changer in 2023 & Beyond
                     </div>
                  </div>
                  <div class="f-28 f-md-45 w700 lh160 mt20 mt-md30 black-clr">
                  <span class="w400 black-clr">And It Instantly Became The </span> <span class="blue-gradient1 under">Undisputed Ruler Of The Digital World... </span> 
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md50 align-items-center">
               <div class="col-12 col-md-7">
                  <div class="f-24 f-md-32 w600 black-clr lh140">
                 It solves all your tedious writing & designing problem like you have now as 
                  </div>
                  <ul class="problem-list pl0 m0 f-20 f-md-22 lh150 w500 black-clr mt20 mt-md30">
                     <li>	You want to write content for Emails</li>
                     <li>	Want to translate Language.</li>
                     <li>	You want to generate graphics & images.</li>
                     <li>	You want to create videos</li>
                     <li>	Want to write high quality content for Websites & blogs</li>
                     <li>	Want to write marketing material</li>
                  </ul>
               </div>
               <div class="col-12 col-md-5 relative mt20 mt-md0">
                  <img src="assets/images/game-changer-robot.webp" alt="Robot Question" class="img-fluid mx-auto d-block robot-ques">
               </div>
            </div>
            <div class="row mt30 mt-md50">
            <div class="col-md-10 mx-auto col-12 mt20 mt-md30 text-center ">
               <a href="#buynow" class="cta-link-btn px0 d-block">Get Instant Access To NinjaAi Mail</a>
            </div>
         </div>
         </div>
      </div>

<!--research section starts-->
<div class="research-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="reason-box">
                     <div class="f-28 f-md-45 w700 lh140 black-clr">
                        And, MONEY IS IN THE LIST
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50">
                  <div class="row align-items-center">
                     <div class="col-12 col-md-6">
                        <div class="f-20 f-md-24 w500 lh140 text-center text-md-start">
                           Hope you have heard that quote already.<br><br>
                           Yes, Email marketing yields an average 4300% return on investment for businesses worldwide. <span class="w700">Email gets 3 times higher conversions & ROI than social media or any other mode of marketing.</span>
                        </div>
                     </div>
                     <div class="col-12 col-md-6 mt20 mt-md0">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/investment.webp" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md70">
                  <div class="row">
                     <div class="col-12 col-md-7 order-md-2">
                        <div class="f-28 f-md-45 w700 lh140 text-center text-md-start green-clr">
                           That’s Right…
                        </div>
                        <div class="f-20 f-md-24 w500 lh140 text-center text-md-start mt10">
                           Email marketing is the key to getting new customers and creating deeper relationships with your existing customers at a fraction of the cost.
                        </div>
                     </div>
                     <div class="col-12 col-md-5 order-md-1 mt20 mt-md0 relative d-none d-md-block">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/girl.webp" class="img-fluid d-block mx-auto girl-img">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      
      <div class="stats-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-45 w700 lh140 text-center white-clr stats-headline">If You’re Not Using Emails, You’re <br class="d-none d-md-block">Actually Losing Money! </div>
               </div>
            </div>
            <div class="row row-cols-1 row-cols-md-2 gap30 mt0 t-md20">
               <div class="col">
                  <div class="stats-block">
                     <div><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/mail.webp" class="img-fluid d-block mx-auto"></div>
                     <div class="f-20 f-md-24 w400 lh140 white-clr">There are <span class="orange-clr w700">4.5+ Billion</span> Daily Email Users</div>
                  </div>
               </div>
               <div class="col">
                  <div class="stats-block">
                     <div><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/mail.webp" class="img-fluid d-block mx-auto"></div>
                     <div class="f-20 f-md-24 w400 lh140 white-clr"><span class="orange-clr w700">350+ Billion Emails</span> are Circulated
                        Every Single Day
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="stats-block">
                     <div><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/mail.webp" class="img-fluid d-block mx-auto"></div>
                     <div class="f-20 f-md-24 w400 lh140 white-clr"><span class="orange-clr w700">$400 Million</span> are Spent Every Year
                        on Email Marketing in US Alone
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="stats-block">
                     <div><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/mail.webp" class="img-fluid d-block mx-auto"></div>
                     <div class="f-20 f-md-24 w400 lh140 white-clr"><span class="orange-clr w700">95% of Email</span> Users Check
                        Their Emails Daily
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="stats-block">
                     <div><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/mail.webp" class="img-fluid d-block mx-auto"></div>
                     <div class="f-20 f-md-24 w400 lh140 white-clr"><span class="orange-clr w700">85% of Businesses</span> Believe that
                        Email Marketing Improves
                        Customer Retention
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="stats-block">
                     <div><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/mail.webp" class="img-fluid d-block mx-auto"></div>
                     <div class="f-20 f-md-24 w400 lh140 white-clr"><span class="orange-clr w700">80% of Youth</span> Prefer Business
                        Communications via Email
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="stats-block">
                     <div><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/mail.webp" class="img-fluid d-block mx-auto"></div>
                     <div class="f-20 f-md-24 w400 lh140 white-clr"><span class="orange-clr w700">90% of Marketers</span> Use Email
                        as the Primary Channel
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="stats-block">
                     <div><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/mail.webp" class="img-fluid d-block mx-auto"></div>
                     <div class="f-20 f-md-24 w400 lh140 white-clr"><span class="orange-clr w700">Since 10+ Years,</span> Email Marketing
                        Generates the Highest ROI for
                        Marketers.
                     </div>
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md80">
               <div class="col-12 f-24 f-md-36 w600 white-clr lh140 text-center">  This Is Why 1000s Of Small & Big Businesses/Marketers  
               <br class="d-none d-md-block"> Are Capitalizing On Email Marketing
               </div>
            </div>
         </div>
      </div>
<!--research section ends-->

      <!-- Unique Twist Section -->
      <section class="unique-sec"> 
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-45 w800 lh140 white-clr">
                     Here’s The Unique TWIST Comes
                  </div>
                  <div class="mt10">
                     <img src="assets/images/hline.webp" alt="" class="mx-auto d-block img-fluid">
                  </div>
                  <div class="f-20 f-md-24 w500 lh140 white-clr mt20 mt-md50">
                  Now we all know that ChatGPT is the future of online world & emails are the most <br class="d-none d-md-block"> powerful and low-cost marketing method. 
                  </div>
                  <div class="f-20 f-md-28 w500 lh140 white-clr mt20 mt-md50">
                  When we combine these tow two powerful tools, it brings tons of Traffic,<br class="d-none d-md-block"> commission, and sales on 100% autopilot mode. 
                  </div>
                  <!--<div class="mt20 mt-md30 lh140 w500 white-clr f-20 f-md-24 w500 unique-text">-->
                  <!--   So Here we comes with-->
                  <!--</div>-->
                  <div class="col-md-10 col-12 combile-mailgpt mt20 mt-md70">
                     <img src="assets/images/mailgpt.webp" alt="NinjaAi Mail" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-24 f-md-38 w500 lh140 white-clr mt20 mt-md90">
                   With NinjaAi Mail, You Need Just One Keyword To Write, Design, And Send Profitable Emails for...
                  </div>
                  <div class="mt20 mt-md30">
                     <img src="assets/images/arrow.png" class="img-fluid mx-auto d-block downarrow">
                  </div>
               </div>
            </div>
            <div class="row row-cols-1 row-cols-md-3 gap30  justify-content-center mt0 mt-md50">
               <div class="col">
                  <img src="assets/images/u1.webp" class="img-fluid mx-auto d-block">
               </div>
               <div class="col">
                  <img src="assets/images/u2.webp" class="img-fluid mx-auto d-block">
               </div>
               <div class="col">
                  <img src="assets/images/u3.webp" class="img-fluid mx-auto d-block">
               </div>
               <div class="col">
                  <img src="assets/images/u4.webp" class="img-fluid mx-auto d-block">
               </div>
               <div class="col">
                  <img src="assets/images/u5.webp" class="img-fluid mx-auto d-block">
               </div>
               <div class="col">
                  <img src="assets/images/u6.webp" class="img-fluid mx-auto d-block">
               </div>
               <div class="col">
                  <img src="assets/images/u7.webp" class="img-fluid mx-auto d-block">
               </div>
               <div class="col">
                  <img src="assets/images/u8.webp" class="img-fluid mx-auto d-block">    
               </div>
               <div class="col">
                  <img src="assets/images/u9.webp" class="img-fluid mx-auto d-block">
               </div>
              
            </div>
         </div>
      </section>

  
      <!--Proudly Introducing Start -->
     <div class="next-gen-sec" id="product">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="proudly-sec f-26 w700 text-center white-clr">
                     Proudly <br> Presenting...
                  </div>
               </div>
               <div class="col-12 mt-md20 mt20  mt-md50 text-center">
               <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 645.02 151.26" style="max-height:70px">
                                    <defs>
                                      <style>
                                        .cls-1 {
                                          fill: url(#linear-gradient-2);
                                        }
                                  
                                        .cls-2 {
                                          fill: #fff;
                                        }
                                  
                                        .cls-3 {
                                          fill-rule: evenodd;
                                        }
                                  
                                        .cls-3, .cls-4 {
                                          fill: #ff813b;
                                        }
                                  
                                        .cls-5 {
                                          fill: url(#linear-gradient-3);
                                        }
                                  
                                        .cls-6 {
                                          fill: url(#linear-gradient);
                                        }
                                  
                                        .cls-7 {
                                          fill: url(#linear-gradient-4);
                                        }
                                      </style>
                                      <linearGradient id="linear-gradient" x1="47.81" y1="75" x2="185.48" y2="75" gradientUnits="userSpaceOnUse">
                                        <stop offset="0" stop-color="#b956ff"></stop>
                                        <stop offset="1" stop-color="#6e33ff"></stop>
                                      </linearGradient>
                                      <linearGradient id="linear-gradient-2" x1="0" y1="75" x2="85.18" y2="75" gradientUnits="userSpaceOnUse">
                                        <stop offset="0" stop-color="#b956ff"></stop>
                                        <stop offset="1" stop-color="#a356fa"></stop>
                                      </linearGradient>
                                      <linearGradient id="linear-gradient-3" x1="55.61" y1="62.54" x2="62.86" y2="62.54" gradientUnits="userSpaceOnUse">
                                        <stop offset="0" stop-color="#b956ff"></stop>
                                        <stop offset="1" stop-color="#ac59fa"></stop>
                                      </linearGradient>
                                      <linearGradient id="linear-gradient-4" x1="54.62" y1="79" x2="61.87" y2="79" gradientTransform="translate(19.64 -11.24) rotate(13.24)" xlink:href="#linear-gradient-3"></linearGradient>
                                    </defs>
                                    <g id="Layer_1-2" data-name="Layer 1">
                                      <g>
                                        <g>
                                          <g>
                                            <path class="cls-6" d="m181.49,58.53h-10.72c-1.4-5.13-3.43-9.99-6.01-14.51l7.58-7.58c1.56-1.56,1.56-4.09,0-5.65l-17.65-17.65c-1.56-1.56-4.09-1.56-5.65,0l-7.58,7.58c-4.51-2.58-9.38-4.61-14.51-6.01V4c0-2.21-1.79-4-4-4h-24.96c-2.21,0-4,1.79-4,4v10.72c-.16.04-.31.09-.47.14-.27.08-.53.15-.8.23-.27.08-.55.16-.82.24-.55.17-1.09.35-1.63.53-.11.04-.22.08-.33.12-.48.17-.96.34-1.44.52-.12.04-.24.09-.36.14-.52.2-1.04.4-1.56.62-.04.02-.08.03-.12.05-2.41,1-4.74,2.15-6.99,3.43l-7.57-7.57c-1.56-1.56-4.09-1.56-5.65,0l-17.65,17.65c-.36.36-.63.76-.82,1.2h61.99v.02c.23,0,.45-.02.68-.02,23.76,0,43.01,19.26,43.01,43.01s-19.26,43.01-43.01,43.01c-.5,0-1-.02-1.5-.04v.04h-61.17c.19.43.46.84.82,1.2l17.65,17.65c1.56,1.56,4.09,1.56,5.65,0l7.57-7.57c2.25,1.28,4.58,2.43,6.99,3.43.04.02.08.03.12.05.51.21,1.03.42,1.55.62.12.05.24.09.36.14.47.18.95.35,1.43.52.11.04.23.08.34.12.54.18,1.08.36,1.63.53.27.08.55.16.83.25.26.08.53.16.79.23.16.04.31.09.47.14v10.72c0,2.21,1.79,4,4,4h24.96c2.21,0,4-1.79,4-4v-10.72c5.13-1.4,9.99-3.43,14.51-6.01l7.58,7.58c1.56,1.56,4.09,1.56,5.65,0l17.65-17.65c1.56-1.56,1.56-4.09,0-5.65l-7.58-7.58c2.58-4.51,4.61-9.38,6.01-14.51h10.72c2.21,0,4-1.79,4-4v-24.96c0-2.21-1.79-4-4-4Z"></path>
                                            <path class="cls-1" d="m76.64,101.54c-5.74-7.31-9.18-16.52-9.18-26.54s3.44-19.23,9.18-26.54c2.45-3.13,5.33-5.9,8.53-8.24h-48.39c-1.31-1.89-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.13h17.07c-1.51,3.21-2.76,6.58-3.71,10.07h3.1c1.31-1.89,3.49-3.13,5.97-3.13,4.01,0,7.25,3.25,7.25,7.25s-3.22,7.23-7.21,7.25h-.04c-2.48,0-4.66-1.24-5.97-3.13H13.22c-.44-.64-.98-1.19-1.59-1.65-1.21-.92-2.73-1.48-4.38-1.48-4,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.14h38.82c1.31-1.89,3.49-3.13,5.97-3.13.43,0,.84.05,1.25.12,3.41.59,6,3.56,6,7.14,0,2.78-1.56,5.19-3.86,6.41-1.01.53-2.16.84-3.39.84-2.48,0-4.66-1.24-5.97-3.13h-17.85c-1.31-1.9-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.13h15.96c.95,3.48,2.2,6.85,3.71,10.07h-5.63c-1.31-1.89-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25,0,1.53.48,2.95,1.28,4.12,1.31,1.9,3.49,3.14,5.97,3.14s4.66-1.24,5.97-3.14h36.95c-3.21-2.34-6.08-5.11-8.53-8.24ZM30.64,47.97c-2,0-3.62-1.62-3.62-3.62s1.62-3.62,3.62-3.62,3.63,1.62,3.63,3.62-1.63,3.62-3.63,3.62Zm-23.39,26.47c-2,0-3.62-1.63-3.62-3.62s1.63-3.63,3.62-3.63,3.63,1.63,3.63,3.63-1.63,3.62-3.63,3.62Zm20.97,16.45c-2,0-3.63-1.63-3.63-3.63s1.63-3.62,3.63-3.62,3.62,1.63,3.62,3.62-1.63,3.63-3.62,3.63Zm14.08,18.39c-2,0-3.62-1.62-3.62-3.62s1.62-3.62,3.62-3.62,3.62,1.62,3.62,3.62-1.62,3.62-3.62,3.62Z"></path>
                                            <circle class="cls-5" cx="59.23" cy="62.54" r="3.63"></circle>
                                            <circle class="cls-7" cx="58.24" cy="79" r="3.63" transform="translate(-16.54 15.44) rotate(-13.24)"></circle>
                                          </g>
                                          <g>
                                            <g>
                                              <path class="cls-3" d="m104.63,78.16c-9.44,6-14.52,2.86-15.25-9.4,4.93,3.38,10.02,6.52,15.25,9.4Z"></path>
                                              <path class="cls-3" d="m131.01,68.76c-.72,12.26-5.81,15.4-15.25,9.4,5.24-2.88,10.32-6.02,15.25-9.4Z"></path>
                                            </g>
                                            <path class="cls-4" d="m110.16,40.13c-.89,0-1.78.03-2.66.1-.08,0-.16,0-.24.02-9.59.79-18.07,5.45-23.89,12.42-.25.3-.49.59-.73.89-.14.19-.29.38-.43.57-4.02,5.37-6.52,11.93-6.88,19.07,0,.01,0,.03,0,.04-.01.22-.02.45-.03.68-.01.3-.02.6-.02.9v.18c0,.28,0,.55,0,.83.44,18.64,15.5,33.66,34.15,34.04.24,0,.48,0,.72,0,.3,0,.6,0,.9,0,18.85-.48,33.98-15.9,33.98-34.87s-15.61-34.88-34.88-34.88Zm.19,39.35c-14.41,14.37-29.15,1.23-29.15-9.45s13.05,0,29.15,0,29.15-10.67,29.15,0-14.74,23.81-29.15,9.45Z"></path>
                                          </g>
                                        </g>
                                        <g>
                                          <path class="cls-2" d="m286.65,118.51h-17.37l-39.32-59.42v59.42h-17.37V31.8h17.37l39.32,59.54V31.8h17.37v86.71Z"></path>
                                          <path class="cls-2" d="m304.95,38.69c-2.03-1.94-3.04-4.36-3.04-7.26s1.01-5.31,3.04-7.26c2.03-1.94,4.57-2.92,7.63-2.92s5.6.97,7.63,2.92c2.02,1.94,3.04,4.36,3.04,7.26s-1.02,5.31-3.04,7.26c-2.03,1.94-4.57,2.91-7.63,2.91s-5.6-.97-7.63-2.91Zm16.19,11.1v68.72h-17.37V49.79h17.37Z"></path>
                                          <path class="cls-2" d="m396.18,56.55c5.04,5.17,7.57,12.38,7.57,21.65v40.31h-17.36v-37.96c0-5.46-1.37-9.65-4.1-12.59-2.73-2.94-6.45-4.4-11.16-4.4s-8.58,1.47-11.35,4.4c-2.77,2.94-4.15,7.13-4.15,12.59v37.96h-17.37V49.79h17.37v8.56c2.31-2.98,5.27-5.31,8.87-7.01,3.6-1.69,7.55-2.54,11.85-2.54,8.19,0,14.8,2.59,19.85,7.75Z"></path>
                                          <path class="cls-2" d="m437.61,129.8c0,7.61-1.88,13.09-5.64,16.44-3.77,3.35-9.16,5.02-16.19,5.02h-7.69v-14.76h4.96c2.64,0,4.51-.52,5.58-1.55,1.07-1.03,1.61-2.71,1.61-5.02V49.79h17.37v80.01Zm-16.31-91.11c-2.03-1.94-3.04-4.36-3.04-7.26s1.01-5.31,3.04-7.26c2.02-1.94,4.61-2.92,7.75-2.92s5.58.97,7.57,2.92c1.98,1.94,2.98,4.36,2.98,7.26s-.99,5.31-2.98,7.26c-1.98,1.94-4.51,2.91-7.57,2.91s-5.73-.97-7.75-2.91Z"></path>
                                          <path class="cls-2" d="m454.42,65.42c2.77-5.37,6.53-9.51,11.29-12.4,4.76-2.89,10.07-4.34,15.94-4.34,5.13,0,9.61,1.04,13.46,3.1,3.85,2.07,6.92,4.67,9.24,7.82v-9.8h17.49v68.72h-17.49v-10.05c-2.23,3.23-5.31,5.89-9.24,8-3.93,2.11-8.46,3.16-13.58,3.16-5.79,0-11.06-1.49-15.82-4.47-4.76-2.98-8.52-7.17-11.29-12.59-2.77-5.41-4.16-11.64-4.16-18.67s1.38-13.11,4.16-18.48Zm47.45,7.88c-1.65-3.02-3.89-5.33-6.7-6.95-2.81-1.61-5.83-2.42-9.06-2.42s-6.2.79-8.93,2.36c-2.73,1.57-4.94,3.87-6.64,6.88-1.69,3.02-2.54,6.6-2.54,10.73s.85,7.75,2.54,10.85c1.69,3.1,3.93,5.48,6.7,7.13,2.77,1.66,5.72,2.48,8.87,2.48s6.24-.81,9.06-2.42c2.81-1.61,5.04-3.93,6.7-6.95,1.65-3.02,2.48-6.64,2.48-10.85s-.83-7.83-2.48-10.85Z"></path>
                                          <path class="cls-4" d="m591.93,102.01h-34.48l-5.71,16.5h-18.24l31.14-86.71h20.22l31.13,86.71h-18.36l-5.71-16.5Zm-4.71-13.89l-12.53-36.22-12.53,36.22h25.06Z"></path>
                                          <path class="cls-4" d="m645.02,31.93v86.58h-17.37V31.93h17.37Z"></path>
                                        </g>
                                      </g>
                                    </g>
                                  </svg>
               </div>
              <div class="col-12 mt-md50 mt20 f-md-34 f-28 w700 text-center white-clr lh150">
                  <span class="orange-clr orange-underline"> World’s First ChatGPT AI </span> -  Powered Email Marketing App Writes, Design & Send  Unlimited Profit Pulling  Emails And Messages with Just One Keyword for Tons of Traffic, Commissions, & Sales on Autopilot                
                   </div>
               <div class="f-20 f-md-24 lh140 w400 text-center white-clr mt20">              
                  It is a must-have tool in your marketing arsenal that comes with a ONE-TIME FEE, and that will send all the existing money-sucking autoresponders back to their nest.
               </div>
                <div class="row align-items-center mt20 mt-md50">
               <div class="col-12 col-md-10 mx-auto">
                  <img src="assets/images/product-box.webp" class="img-fluid d-block mx-auto" alt="Product">
               </div>
            </div>
            </div>
            <div class="row">
            <div class="col-md-10 mx-auto col-12 mt20 mt-md30 text-center ">
               <a href="#buynow" class="cta-link-btn px0 d-block">Get Instant Access To NinjaAi Mail</a>
            </div>
         </div>
         </div>
      </div> 
      <!--Proudly Introducing End -->

    


      <section class="ground-breaking-features">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-45 w700 lh140 white-clr still-headline">
                     Checkout The Ground-Breaking Features
                  </div>
                  <div class="f-28 f-md-45 w700 lh140 black-clr mt10">
                     That Make NinjaAi Mail A Cut Above The Rest 
                  </div>
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md30">
               <div class="col-md-6 relative">
                  <div class="f-24 f-md-30 lh140 black-clr w600">
                     	A Fully ChatGPT AI Powered Cloud Based Platform
                  </div>
                  <img src="assets/images/arrow-left.webp" class="img-fluid d-none ms-md-auto d-md-block" style="position: absolute; right: -160px; top: 210px;">
               </div>
               <div class="col-md-6 mt20 mt-md0">
                  <img src="assets/images/gb-feat1.webp" alt="Build Huge" class="mx-auto d-block img-fluid">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md90">
               <div class="col-md-6 order-md-2 relative">
                  <div class="f-24 f-md-30 lh140 black-clr w600">
                  Create High-Quality Emails & Messages With Just One Keyword
                  </div>
                  <img src="assets/images/arrow-right.webp" class="img-fluid d-none me-md-auto d-md-block" style="position: absolute; top: 280px; left: -240px;">
               </div>
               <div class="col-md-6 mt20 mt-md0 order-md-1">
                  <img src="assets/images/gb-feat2.webp" alt="Sell" class="mx-auto d-block img-fluid">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md90">
               <div class="col-md-6 relative">
                  <div class="f-24 f-md-30 lh140 black-clr w600">
                     	Send Unlimited Mails Instantly Or Schedule Them For Later Date/Time
                  </div>
                  <img src="assets/images/arrow-left.webp" class="img-fluid d-none ms-md-auto d-md-block" style="position: absolute; right: -160px; top: 190px;">
               </div>
               <div class="col-md-6 mt20 mt-md0">
                  <img src="assets/images/gb-feat3.webp" alt="Build Huge" class="mx-auto d-block img-fluid">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md90">
               <div class="col-md-6 order-md-2 relative">
                  <div class="f-24 f-md-30 lh140 black-clr w600">
                  	AI Enabled Smart Tagging For Lead Personalization & Traffic
                  </div>
                  <img src="assets/images/arrow-right.webp" class="img-fluid d-none me-md-auto d-md-block" style="position: absolute; top: 280px; left: -240px;">
               </div>
               <div class="col-md-6 mt20 mt-md0 order-md-1">
                  <img src="assets/images/gb-feat4.webp" alt="Sell" class="mx-auto d-block img-fluid">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md90">
               <div class="col-md-6 relative">
                  <div class="f-24 f-md-30 lh140 black-clr w600">
                     	Boost Email Delivery, Click And Open Rate with Engaging Emails
                  </div>
                  <img src="assets/images/arrow-left.webp" class="img-fluid d-none ms-md-auto d-md-block" style="position: absolute; right: -160px; top: 240px;">
               </div>
               <div class="col-md-6 mt20 mt-md0">
                  <img src="assets/images/gb-feat5.webp" alt="Build Huge" class="mx-auto d-block img-fluid">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md90">
               <div class="col-md-6 order-md-2 relative">
                  <div class="f-24 f-md-30 lh140 black-clr w600">
                     	Design Beautiful Newsletters & Autoresponders Using AI 
                  </div>
               </div>
               <div class="col-md-6 mt20 mt-md0 order-md-1">
                  <img src="assets/images/gb-feat6.webp" alt="Sell" class="mx-auto d-block img-fluid">
               </div>
            </div>
         </div>  
      </section>

        <section class="ef-sec">
         <div class="container">
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-md-6 relative">
                  <div class="f-24 f-md-30 lh140 black-clr w600">
                     	Import Unlimited Subscribers with Just 1 Click
                  </div>
                  <img src="assets/images/arrow-left.webp" class="img-fluid d-none ms-md-auto d-md-block" style="position: absolute; right: -160px; top: 210px;">
               </div>
               <div class="col-md-6 mt20 mt-md0">
                  <img src="assets/images/ef1.webp" alt="Build Huge" class="mx-auto d-block img-fluid">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md90">
               <div class="col-md-6 order-md-2 relative">
                  <div class="f-24 f-md-30 lh140 black-clr w600">
                 	Generate Tons of Subscribers With Our Attractive In-Built Lead Form
                  </div>
                  <img src="assets/images/arrow-right.webp" class="img-fluid d-none me-md-auto d-md-block" style="position: absolute; top: 250px; left: -180px;">
               </div>
               <div class="col-md-6 mt20 mt-md0 order-md-1">
                  <img src="assets/images/ef2.webp" alt="Sell" class="mx-auto d-block img-fluid">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md90">
               <div class="col-md-6 relative">
                  <div class="f-24 f-md-30 lh140 black-clr w600">
                     	Inbuilt AI Text & Inline Editor To Craft Best Emails
                  </div>
                  <img src="assets/images/arrow-left.webp" class="img-fluid d-none ms-md-auto d-md-block" style="position: absolute; right: -180px; top: 220px;">
               </div>
               <div class="col-md-6 mt20 mt-md0">
                  <img src="assets/images/ef3.webp" alt="Build Huge" class="mx-auto d-block img-fluid">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md90">
               <div class="col-md-6 order-md-2 relative">
                  <div class="f-24 f-md-30 lh140 black-clr w600">
                  	NinjaAi Mail is 100% CAN SPAM Compliant
                  </div>
                  <img src="assets/images/arrow-right.webp" class="img-fluid d-none me-md-auto d-md-block" style="position: absolute; top: 280px; left: -240px;">
               </div>
               <div class="col-md-6 mt20 mt-md0 order-md-1">
                  <img src="assets/images/ef4.webp" alt="Sell" class="mx-auto d-block img-fluid">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md90">
               <div class="col-md-6 relative">
                  <div class="f-24 f-md-30 lh140 black-clr w600">
                  Boost Your Traffic, Sales And Increase Revenue 
                  </div>
                  <img src="assets/images/arrow-left.webp" class="img-fluid d-none ms-md-auto d-md-block" style="position: absolute; right: -160px; top: 210px;">
               </div>
               <div class="col-md-6 mt20 mt-md0">
                  <img src="assets/images/ef5.webp" alt="Build Huge" class="mx-auto d-block img-fluid">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md90">
               <div class="col-md-6 order-md-2 relative">
                  <div class="f-24 f-md-30 lh140 black-clr w600">
                 	Personalize Your Mails To Get High Opening Rates
                  </div>
                  <img src="assets/images/arrow-right.webp" class="img-fluid d-none me-md-auto d-md-block" style="position: absolute; top: 250px; left: -240px;">
               </div>
               <div class="col-md-6 mt20 mt-md0 order-md-1">
                  <img src="assets/images/ef6.webp" alt="Sell" class="mx-auto d-block img-fluid">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md90">
               <div class="col-md-6 relative">
                  <div class="f-24 f-md-30 lh140 black-clr w600">
                     	Create Long-Term Relationships With Your Subscribers With Beautiful Newsletters
                  </div>
                  <img src="assets/images/arrow-left.webp" class="img-fluid d-none ms-md-auto d-md-block" style="position: absolute; right: -160px; top: 260px;">
               </div>
               <div class="col-md-6 mt20 mt-md0">
                  <img src="assets/images/ef7.webp" alt="Build Huge" class="mx-auto d-block img-fluid">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md90">
               <div class="col-md-6 order-md-2 relative">
                  <div class="f-24 f-md-30 lh140 black-clr w600">
                    	100% Newbie Friendly And Fully Automated
                  </div>
                  <img src="assets/images/arrow-right.webp" class="img-fluid d-none me-md-auto d-md-block" style="position: absolute; top: 250px; left: -240px;">
               </div>
               <div class="col-md-6 mt20 mt-md0 order-md-1">
                  <img src="assets/images/ef8.webp" alt="Sell" class="mx-auto d-block img-fluid">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md90">
               <div class="col-md-6 relative">
                  <div class="f-24 f-md-30 lh140 black-clr w600">
                     	No Monthly Fees Or Additional Charges
                  </div>
                  <img src="assets/images/arrow-left.webp" class="img-fluid d-none ms-md-auto d-md-block" style="position: absolute; right: -160px; top: 220px;">
               </div>
               <div class="col-md-6 mt20 mt-md0">
                  <img src="assets/images/ef9.webp" alt="Build Huge" class="mx-auto d-block img-fluid">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md90">
               <div class="col-md-6 order-md-2 relative">
                  <div class="f-24 f-md-30 lh140 black-clr w600">
                  Brand New System- Absolutely NO Rehashes
                  </div>
                  <img src="assets/images/arrow-right.webp" class="img-fluid d-none me-md-auto d-md-block" style="position: absolute; top: 240px; left: -240px;">
               </div>
               <div class="col-md-6 mt20 mt-md0 order-md-1">
                  <img src="assets/images/ef10.webp" alt="Sell" class="mx-auto d-block img-fluid">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md90">
               <div class="col-md-6 relative">
                  <div class="f-24 f-md-30 lh140 black-clr w600">
                  	Designed by Marketers for Marketers
                  </div>
                  <img src="assets/images/arrow-left.webp" class="img-fluid d-none ms-md-auto d-md-block" style="position: absolute; right: -160px; top: 230px;">
               </div>
               <div class="col-md-6 mt20 mt-md0">
                  <img src="assets/images/ef11.webp" alt="Build Huge" class="mx-auto d-block img-fluid">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md90">
               <div class="col-md-6 order-md-2 relative">
                  <div class="f-24 f-md-30 lh140 black-clr w600">
                  	Step-By-Step Training to Make Everything Easy For You
                  </div>
               </div>
               <div class="col-md-6 mt20 mt-md0 order-md-1">
                  <img src="assets/images/ef12.webp" alt="Sell" class="mx-auto d-block img-fluid">
               </div>
            </div>
         </div>  
      </section>

  
      <section class="revolution-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-45 w700 lh140 white-clr">
                  NinjaAi Mail Is The Revolution In Email Marketing World
                  </div>
                  
                  <div class="mt20 mt-md30 white-clr f-20 f-md-24 w400 lh140">
                    That is going to change the future of email marketing, writing & designing for “Better”.
                  </div>
                  <div class="mt20 mt-md80">
                     <img src="assets/images/mailgpt-img.webp" alt="" class="img-fluid mx-auto d-block">
                  </div>
               </div>
            </div>
            <div class="row">
            <div class="col-md-10 mx-auto col-12 mt20 mt-md30 text-center ">
               <a href="#buynow" class="cta-link-btn px0 d-block">Get Instant Access To NinjaAi Mail</a>
            </div>
         </div>
         </div>
      </section>

      
     

      <section class="unlock-opportunities-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="black-clr w700 f-28 f-md-35 unlimited-head">
                     <div class="unlimited-blub">
                        With NinjaAi Mail, Tap into Endless Earning Opportunities...
                     </div>
                  </div>
               </div>
            </div>
            <div class="row row-cols-1 row-cols-md-3 gap30  justify-content-center mt20 mt-md50">
               <div class="col">
                  <img src="assets/images/op3.webp" class="img-fluid mx-auto d-block">
                  <div class="balck-clr f-18 f-md-20 w700 lh140 mt20 text-center">
                     	Promote Any Product as An Affiliate from Warrior+, JVZoo, ClickBank
                  </div>
               </div>
               <div class="col">
                  <img src="assets/images/op5.webp" class="img-fluid mx-auto d-block">
                  <div class="balck-clr f-18 f-md-20 w700 lh140 mt20 text-center">
                     Create And Sell Your Own Courses, E-Book, or Digital Products
                  </div>
               </div>
               <div class="col">
                  <img src="assets/images/op9.webp" class="img-fluid mx-auto d-block">
                  <div class="balck-clr f-18 f-md-20 w700 lh140 mt20 text-center">
                     Earn More with Cross-Selling Similar Product/Services with Emails
                  </div>
               </div>
               <div class="col">
                  <img src="assets/images/op2.webp" class="img-fluid mx-auto d-block">
                  <div class="balck-clr f-18 f-md-20 w700 lh140 mt20 text-center">
                     Start Email Marketing Agency & Charge Customers for Your Services
                  </div>
               </div>
               <div class="col">
                  <img src="assets/images/op8.webp" class="img-fluid mx-auto d-block">
                  <div class="balck-clr f-18 f-md-20 w700 lh140 mt20 text-center">
                    	Offer Paid Newsletter Services by Charging Subscription Fee 
                  </div>
               </div>
               <div class="col">
                  <img src="assets/images/op7.webp" class="img-fluid mx-auto d-block">
                  <div class="balck-clr f-18 f-md-20 w700 lh140 mt20 text-center">
                     	Craft Personalized, Engaging, High Converting Email Swipes and Sell Them
                  </div>
               </div>
               
            </div>
         </div>
      </section>

    
      <div class="benefit-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-28 f-md-45 w700 lh140 text-center white-clr">So, Who Can Benefit From NinjaAi Mail? </div>
               </div>
            </div>
            <div class="row row-cols-1 gap30 mt0 mt-md20">
               <div class="col">
                  
               </div>
            </div>
            <div class="row row-cols-1 row-cols-md-3 gap30 mt0">
               <div class="col">
                  <div class="fea-white">
                     <img src="assets/images/b1.webp" class="img-fluid mx-auto d-block" alt="">
                     <div class="f-18 f-md-20 w600 lh140 grey-clr mt10">
                        	Any Internet Marketer, Regardless of The Niche. NinjaAi Mail Helps Them Boost Inboxing, Clicks & Sales.
                     </div>
                  </div>
               </div>
               <div class="col">
                   <div class="fea-white">
                     <img src="assets/images/b2.webp" class="img-fluid mx-auto d-block" alt="">
                     <div class="f-18 f-md-20 w600 lh140 grey-clr mt10">
                        		List Builders Skyrocket Their Subscribers’ Numbers... All At The Push Of A Button.
                     </div>
                  </div>
               </div>
               <div class="col">
                   <div class="fea-white">
                     <img src="assets/images/b3.webp" class="img-fluid mx-auto d-block" alt="">
                     <div class="f-18 f-md-20 w600 lh140 grey-clr mt10">
                        	Anyone Who Wants To Streamline Their Business While Focusing On Bigger Projects.
                     </div>
                  </div>
               </div>
            </div>
            
            
            <div class="row row-cols-1 row-cols-md-3 gap30 mt0">
               <div class="col">
                  <div class="fea-white">
                     <img src="assets/images/b4.webp" class="img-fluid mx-auto d-block" alt="">
                     <div class="f-18 f-md-20 w600 lh140 grey-clr mt10">
                        	Affiliate Marketers Who Don't Have Any Product Or Service To Sell
                     </div>
                  </div>
               </div>
               <div class="col">
                   <div class="fea-white">
                     <img src="assets/images/b5.webp" class="img-fluid mx-auto d-block" alt="">
                     <div class="f-18 f-md-20 w600 lh140 grey-clr mt10">
                        	Product/Service/Course Sellers To Boost Their Sales & Customer Satisfaction
                     </div>
                  </div>
               </div>
               <div class="col">
                   <div class="fea-white">
                     <img src="assets/images/b6.webp" class="img-fluid mx-auto d-block" alt="">
                     <div class="f-18 f-md-20 w600 lh140 grey-clr mt10">
                    	Online Marketing Agency To Provide Services To Local Businesses & Build Income
                     </div>
                  </div>
               </div>
            </div>
            
            <div class="row row-cols-1 row-cols-md-3 gap30 mt0">
               <div class="col">
                  <div class="fea-white">
                     <img src="assets/images/b7.webp" class="img-fluid mx-auto d-block" alt="">
                     <div class="f-18 f-md-20 w600 lh140 grey-clr mt10">
                    	Local Businesses To Automate Their Client Communication.
                     </div>
                  </div>
               </div>
               <div class="col">
                   <div class="fea-white">
                     <img src="assets/images/b8.webp" class="img-fluid mx-auto d-block" alt="">
                     <div class="f-18 f-md-20 w600 lh140 grey-clr mt10">
                    	Lazy People Who Are Reluctant To Work
                     </div>
                  </div>
               </div>
               <div class="col">
                   <div class="fea-white">
                     <img src="assets/images/b9.webp" class="img-fluid mx-auto d-block" alt="">
                     <div class="f-18 f-md-20 w600 lh140 grey-clr mt10">
                        Anyone Who Wants To Value Their Business And Money 
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

 
     
      <section class="still-think">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-43 w700 lh140 black-clr">
                  Say GoodBye to Expensive & Complex Software
                  </div>
                  <div class="mt10">
                     <img src="assets/images/hline.webp" alt="" class="mx-auto d-block img-fluid">
                  </div>
                  <div class="f-24 f-md-28 w400 lh140 black-clr mt20">
                  Save Thousands of Dollars Per Month Easily with NinjaAi Mail rather than spending on expensive 3rd party tools
                  </div>
                  <div class="f-24 f-md-28 w700 lh140 white-clr still-headline mt20 mt-md50">
                     Content Creation Platform&nbsp;
                  </div>
               </div>
            </div>
            <div class="row row-cols-1 row-cols-md-3 gap30  justify-content-center mt20 mt-md20">
               <div class="col">
                  <img src="assets/images/email1.webp" class="img-fluid mx-auto d-block">
               </div>
               <div class="col">
                  <img src="assets/images/email2.webp" class="img-fluid mx-auto d-block">
               </div>
               <div class="col">
                  <img src="assets/images/email3.webp" class="img-fluid mx-auto d-block">
               </div>
            </div>
            <div class="row mt20 mt-md80">
               <div class="col-12 text-center">
                  <div class="f-24 f-md-28 w700 lh140 white-clr still-headline">
                  Email Marketing Platform
                  </div>
               </div>
            </div>
            <div class="row row-cols-1 row-cols-md-3 gap30  justify-content-center mt20 mt-md50">
               <div class="col">
                  <img src="assets/images/email-marketing1.webp" class="img-fluid mx-auto d-block">
               </div>
               <div class="col">
                  <img src="assets/images/email-marketing2.webp" class="img-fluid mx-auto d-block">
               </div>
               <div class="col">
                  <img src="assets/images/email-marketing3.webp" class="img-fluid mx-auto d-block">
               </div>
               <div class="col">
                  <img src="assets/images/email-marketing4.webp" class="img-fluid mx-auto d-block">
               </div>
               <div class="col">
                  <img src="assets/images/email-marketing5.webp" class="img-fluid mx-auto d-block">
               </div>
               <div class="col">
                  <img src="assets/images/email-marketing6.webp" class="img-fluid mx-auto d-block">
               </div>
            </div>
            <div class="row mt20 mt-md80">
               <div class="col-12 text-center">
                  <div class="f-24 f-md-28 w700 lh140 white-clr still-headline">
                  SMTP Provider Platforms
                  </div>
               </div>
            </div>
            <div class="row row-cols-1 row-cols-md-3 gap30  justify-content-center mt20 mt-md50">
               <div class="col">
                  <img src="assets/images/smtp1.webp" class="img-fluid mx-auto d-block">
               </div>
               <div class="col">
                  <img src="assets/images/smtp2.webp" class="img-fluid mx-auto d-block">
               </div>
               <div class="col">
                  <img src="assets/images/smtp3.webp" class="img-fluid mx-auto d-block">
               </div>
               <div class="col">
                  <img src="assets/images/smtp4.webp" class="img-fluid mx-auto d-block">
               </div>
               <div class="col">
                  <img src="assets/images/smtp5.webp" class="img-fluid mx-auto d-block">
               </div>
               <div class="col">
                  <img src="assets/images/smtp6.webp" class="img-fluid mx-auto d-block">
               </div>
            </div>
         </div>
      </section>
   
      <div class="bonus-section">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-12">
                  <img src="assets/images/bonus-shape.webp" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 col-md-12 mt15 mt-md20">
                  <div class="f-18 f-md-32 w500 lh140 text-center">
                      
                     When You Grab Your NinjaAi Mail Account Today, <br class="d-none d-md-block">You'll Also Get These <span class="w600">Fast Action Bonuses!</span>
                  </div>
               </div>
               <div class="col-12 col-md-12 mt60 mt-md100">
                  <div class="bonus-section-shape text-center">
                     <div class="col-12 margin-t-60">
                        <div class="bonus-headline">
                           <div class="f-md-24 f-20 w700 white-clr text-nowrap text-center">
                              Bonus #1
                           </div>
                        </div>
                     </div>
                     <div class="row d-flex align-items-center flex-wrap mt15 mt-md30">
                        <div class="col-12 col-md-7 order-md-2">
                           <div class="w700 f-22 f-md-24 black-clr lh140 text-start">
                              Email Marketing DFY Business (with PLR Rights)
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-start">
                              Both online and offline marketers can make a killing using this up-to-date Email Marketing DFY Business. Our step-by-step Email Marketing exclusive training is going to take you and your customers by the hand and show you how to get some amazing marketing results in the shortest time ever. </div>
                        <ul class="revo-list1 pl0">
                                          <li>Top quality training to sell under your name!</li>
                                          <li>Hottest &amp; proven-seller topic on the web!</li>
                                          <li>Ready-to-go sales material to start selling today!</li>
                                          <li>Sell unlimited copies!</li>
                                       </ul>
                        </div>  
                        
                        <div class="col-12 col-md-5 order-md-1 mt20 mt-md0">
                           <img src="assets/images/bonus-box.webp" class="img-fluid d-block mx-auto max-h450" alt="Bonus">
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-12 mt60 mt-md100">
                  <div class="bonus-section-shape text-center">
                     <div class="col-12 margin-t-60">
                        <div class="bonus-headline">
                           <div class="f-md-24 f-20 w700 white-clr text-nowrap text-center">
                              Bonus #2
                           </div>
                        </div>
                     </div>
                     <div class="row d-flex align-items-center flex-wrap mt15 mt-md30">
                        <div class="col-12 col-md-7">
                           <div class="w700 f-22 f-md-24 black-clr lh140 text-start">
                              Progressive List Building DFY Business (with PLR Rights)
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-start">
                             Both online and offline marketers can make a killing using this up-to-date Progressive List Building DFY Business training. Our step-by-step Progressive List Building DFY Business exclusive training is going to take you and your customers by the hand and show you how to get some amazing marketing results in the shortest time ever.
                            <ul class="revo-list1 pl0">
                                          <li>Top quality training to sell under your name!</li>
                                          <li>Hottest &amp; proven-seller topic on the web!</li>
                                          <li>Ready-to-go sales material to start selling today!</li>
                                          <li>Sell unlimited copies!</li>
                                       </ul>
                           </div>
                        </div>
                        <div class="col-12 col-md-5 mt20 mt-md0">
                           <img src="assets/images/bonus-box.webp" class="img-fluid d-block mx-auto max-h450" alt="Bonus">
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-12 mt60 mt-md100">
                  <div class="bonus-section-shape text-center">
                     <div class="col-12 margin-t-60">
                        <div class="bonus-headline">
                           <div class="f-md-24 f-20 w700 white-clr text-nowrap text-center">
                              Bonus #3
                           </div>
                        </div>
                     </div>
                     <div class="row d-flex align-items-center flex-wrap mt15 mt-md30">
                        <div class="col-12 col-md-7 order-md-2">
                           <div class="w700 f-22 f-md-24 black-clr lh140 text-start">
                              Affiliate Marketing Made Easy
                           </div>
                           <div class="f-18 f-md-20 w400 lh140 mt10 text-start">
                              Marketers do not want to waste their money, time and effort just for not doing, and even worse, for not knowing how to do Affiliate Marketing!
                              <br><br>
                              Our step-by-step Affiliate Marketing Training System is going to take you and your customers by the hand and show you how to make as much money as possible, in the shortest time ever with Affiliate Marketing online. </div>
                        </div>
                        <div class="col-12 col-md-5 mt20 mt-md0 order-md-1">
                           <img src="assets/images/bonus-box.webp" class="img-fluid d-block mx-auto max-h450" alt="Bonus">
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-12 mt60 mt-md100">
                  <div class="bonus-section-shape text-center">
                     <div class="col-12 margin-t-60">
                        <div class="bonus-headline">
                           <div class="f-md-24 f-20 w700 white-clr text-nowrap text-center">
                              Bonus #4
                           </div>
                        </div>
                     </div>
                     <div class="row d-flex align-items-center flex-wrap mt15 mt-md30">
                        <div class="col-12 col-md-7">
                           <div class="w700 f-22 f-md-24 black-clr lh140 text-start">
                            Auto Video Creator - Create your own professional videos!
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-start">
                              Uncover the secrets to create your own professional videos in minutes with this easy to use software. You don't even have to speak ... the software will do it for you!
                              Auto Video Creator - Create your own professional videos in a snap! </div>
                        </div>
                        <div class="col-12 col-md-5 mt20 mt-md0">
                           <img src="assets/images/bonus-box.webp" class="img-fluid d-block mx-auto max-h450" alt="Bonus">
                        </div>
                     </div>
                  </div>
               </div>
              
               <div class="col-12 text-center">
                  <div class="bonus-gradient">
                     <div class="f-22 f-md-36 w700 white-clr lh240 text-center">
                        That's A Total Value of <br class="d-none d-md-block"><span class="f-60 f-md-100 w700">$3300</span>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--Bonus Section ends -->
     
      <!-- License Section Start -->
      <div class="license-section">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-12 col-md-6">
                  <div class="f-28 f-md-45 w700 lh140 black-clr text-center text-md-start">
                     Also, Get Free Commercial License 
                  </div>
                  <div class="f-md-20 f-18 w400 lh140 black-clr mt20 text-center text-md-start">
                     NinjaAi Mail comes with Commercial use license so that you can use NinjaAi Mail for commercial purposes and sell services to your clients.
                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                  <img src="assets/images/commercial-license.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>
         </div>
      </div>
       
    
      <!-- Guarantee Section Start -->
     <div class="riskfree-section ">
         <div class="container ">
            <div class="row align-items-center ">
               <div class="f-28 f-md-45 w400 lh140 white-clr text-center col-12 mb-md40">
                  We’re Backing Everything Up with An Iron Clad... <br class="d-none d-md-block"><span class="w600 orange-clr">"30-Day Risk-Free Money Back Guarantee"</span>
               </div>
               <div class="col-md-7 col-12 order-md-2">
                  <div class="f-md-20 f-18 w400 lh140 white-clr mt15 ">
                     I'm 100% confident that NinjaAi Mail will help you take your online business to the next level. I'm so confident on it that I'm making this offer a risk-free investment for you.
                     <br><br>
                     If you concluded that, HONESTLY nothing of this has helped you in any way, you can take advantage of our "30-day Money Back Guarantee" and simply ask for a refund within 30 days!
                     <br><br>
                     Note: For refund, we will require a valid reason along with the proof that you tried our system, but it didn't work for you!
                     <br><br>
                     I am considering your money to be kept safe on the table between us and waiting for you to APPLY and successfully use this product, and get best results, so then you can feel it is a great investment.
                  </div>
               </div>
               <div class="col-md-5 order-md-1 col-12 mt20 mt-md0 ">
                  <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/riskfree-img.webp " class="img-fluid d-block mx-auto ">
               </div>
            </div>
         </div>
      </div>
      <!-- Guarantee Section End -->
      <!-- Discounted Price Section Start -->
     <div class="table-section" id="buynow">
         <div class="container">
            <div class="row ">
               <div class="col-12 ">
                  <div class="f-20 f-md-24 w400 text-center lh140">
                     Take Advantage of Our Limited Time Deal
                  </div>
                  <div class="f-md-45 f-28 w600 lh140 text-center mt10 ">
                     Get NinjaAi Mail for A Low One-Time-
                     <br class="d-none d-md-block "> Price, No Monthly Fee.
                  </div>
               </div>
               <div class="col-12 col-md-8 mx-auto mt20 mt-md50 ">
                  <div class="row ">
                     <div class="col-12 col-md-10 mx-auto mt20 mt-md0">
                        <div class="table-wrap1 ">
                           <div class="table-head1 ">
                           <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 645.02 151.26" style="max-height:120px">
                                       <defs>
                                         <style>
                                           .cls-1a {
                                             fill: #06101f;
                                           }
                                     
                                           .cls-2a {
                                             fill: url(#linear-gradient-2);
                                           }
                                     
                                           .cls-3a {
                                             fill-rule: evenodd;
                                           }
                                     
                                           .cls-3a, .cls-4a {
                                             fill: #ff813b;
                                           }
                                     
                                           .cls-5a {
                                             fill: url(#linear-gradient-3);
                                           }
                                     
                                           .cls-6a {
                                             fill: url(#linear-gradient);
                                           }
                                     
                                           .cls-7a {
                                             fill: url(#linear-gradient-4);
                                           }
                                         </style>
                                         <linearGradient id="linear-gradient" x1="47.81" y1="75" x2="185.48" y2="75" gradientUnits="userSpaceOnUse">
                                           <stop offset="0" stop-color="#b956ff"></stop>
                                           <stop offset="1" stop-color="#6e33ff"></stop>
                                         </linearGradient>
                                         <linearGradient id="linear-gradient-2" x1="0" y1="75" x2="85.18" y2="75" gradientUnits="userSpaceOnUse">
                                           <stop offset="0" stop-color="#b956ff"></stop>
                                           <stop offset="1" stop-color="#a356fa"></stop>
                                         </linearGradient>
                                         <linearGradient id="linear-gradient-3" x1="55.61" y1="62.54" x2="62.86" y2="62.54" gradientUnits="userSpaceOnUse">
                                           <stop offset="0" stop-color="#b956ff"></stop>
                                           <stop offset="1" stop-color="#ac59fa"></stop>
                                         </linearGradient>
                                         <linearGradient id="linear-gradient-4" x1="54.62" y1="79" x2="61.87" y2="79" gradientTransform="translate(19.64 -11.24) rotate(13.24)" xlink:href="#linear-gradient-3"></linearGradient>
                                       </defs>
                                       <g id="Layer_1-2" data-name="Layer 1">
                                         <g>
                                           <g>
                                             <g>
                                               <path class="cls-6a" d="m181.49,58.53h-10.72c-1.4-5.13-3.43-9.99-6.01-14.51l7.58-7.58c1.56-1.56,1.56-4.09,0-5.65l-17.65-17.65c-1.56-1.56-4.09-1.56-5.65,0l-7.58,7.58c-4.51-2.58-9.38-4.61-14.51-6.01V4c0-2.21-1.79-4-4-4h-24.96c-2.21,0-4,1.79-4,4v10.72c-.16.04-.31.09-.47.14-.27.08-.53.15-.8.23-.27.08-.55.16-.82.24-.55.17-1.09.35-1.63.53-.11.04-.22.08-.33.12-.48.17-.96.34-1.44.52-.12.04-.24.09-.36.14-.52.2-1.04.4-1.56.62-.04.02-.08.03-.12.05-2.41,1-4.74,2.15-6.99,3.43l-7.57-7.57c-1.56-1.56-4.09-1.56-5.65,0l-17.65,17.65c-.36.36-.63.76-.82,1.2h61.99v.02c.23,0,.45-.02.68-.02,23.76,0,43.01,19.26,43.01,43.01s-19.26,43.01-43.01,43.01c-.5,0-1-.02-1.5-.04v.04h-61.17c.19.43.46.84.82,1.2l17.65,17.65c1.56,1.56,4.09,1.56,5.65,0l7.57-7.57c2.25,1.28,4.58,2.43,6.99,3.43.04.02.08.03.12.05.51.21,1.03.42,1.55.62.12.05.24.09.36.14.47.18.95.35,1.43.52.11.04.23.08.34.12.54.18,1.08.36,1.63.53.27.08.55.16.83.25.26.08.53.16.79.23.16.04.31.09.47.14v10.72c0,2.21,1.79,4,4,4h24.96c2.21,0,4-1.79,4-4v-10.72c5.13-1.4,9.99-3.43,14.51-6.01l7.58,7.58c1.56,1.56,4.09,1.56,5.65,0l17.65-17.65c1.56-1.56,1.56-4.09,0-5.65l-7.58-7.58c2.58-4.51,4.61-9.38,6.01-14.51h10.72c2.21,0,4-1.79,4-4v-24.96c0-2.21-1.79-4-4-4Z"></path>
                                               <path class="cls-2a" d="m76.64,101.54c-5.74-7.31-9.18-16.52-9.18-26.54s3.44-19.23,9.18-26.54c2.45-3.13,5.33-5.9,8.53-8.24h-48.39c-1.31-1.89-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.13h17.07c-1.51,3.21-2.76,6.58-3.71,10.07h3.1c1.31-1.89,3.49-3.13,5.97-3.13,4.01,0,7.25,3.25,7.25,7.25s-3.22,7.23-7.21,7.25h-.04c-2.48,0-4.66-1.24-5.97-3.13H13.22c-.44-.64-.98-1.19-1.59-1.65-1.21-.92-2.73-1.48-4.38-1.48-4,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.14h38.82c1.31-1.89,3.49-3.13,5.97-3.13.43,0,.84.05,1.25.12,3.41.59,6,3.56,6,7.14,0,2.78-1.56,5.19-3.86,6.41-1.01.53-2.16.84-3.39.84-2.48,0-4.66-1.24-5.97-3.13h-17.85c-1.31-1.9-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.13h15.96c.95,3.48,2.2,6.85,3.71,10.07h-5.63c-1.31-1.89-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25,0,1.53.48,2.95,1.28,4.12,1.31,1.9,3.49,3.14,5.97,3.14s4.66-1.24,5.97-3.14h36.95c-3.21-2.34-6.08-5.11-8.53-8.24ZM30.64,47.97c-2,0-3.62-1.62-3.62-3.62s1.62-3.62,3.62-3.62,3.63,1.62,3.63,3.62-1.63,3.62-3.63,3.62Zm-23.39,26.47c-2,0-3.62-1.63-3.62-3.62s1.63-3.63,3.62-3.63,3.63,1.63,3.63,3.63-1.63,3.62-3.63,3.62Zm20.97,16.45c-2,0-3.63-1.63-3.63-3.63s1.63-3.62,3.63-3.62,3.62,1.63,3.62,3.62-1.63,3.63-3.62,3.63Zm14.08,18.39c-2,0-3.62-1.62-3.62-3.62s1.62-3.62,3.62-3.62,3.62,1.62,3.62,3.62-1.62,3.62-3.62,3.62Z"></path>
                                               <circle class="cls-5a" cx="59.23" cy="62.54" r="3.63"></circle>
                                               <circle class="cls-7a" cx="58.24" cy="79" r="3.63" transform="translate(-16.54 15.44) rotate(-13.24)"></circle>
                                             </g>
                                             <g>
                                               <g>
                                                 <path class="cls-3a" d="m104.63,78.16c-9.44,6-14.52,2.86-15.25-9.4,4.93,3.38,10.02,6.52,15.25,9.4Z"></path>
                                                 <path class="cls-3a" d="m131.01,68.76c-.72,12.26-5.81,15.4-15.25,9.4,5.24-2.88,10.32-6.02,15.25-9.4Z"></path>
                                               </g>
                                               <path class="cls-4" d="m110.16,40.13c-.89,0-1.78.03-2.66.1-.08,0-.16,0-.24.02-9.59.79-18.07,5.45-23.89,12.42-.25.3-.49.59-.73.89-.14.19-.29.38-.43.57-4.02,5.37-6.52,11.93-6.88,19.07,0,.01,0,.03,0,.04-.01.22-.02.45-.03.68-.01.3-.02.6-.02.9v.18c0,.28,0,.55,0,.83.44,18.64,15.5,33.66,34.15,34.04.24,0,.48,0,.72,0,.3,0,.6,0,.9,0,18.85-.48,33.98-15.9,33.98-34.87s-15.61-34.88-34.88-34.88Zm.19,39.35c-14.41,14.37-29.15,1.23-29.15-9.45s13.05,0,29.15,0,29.15-10.67,29.15,0-14.74,23.81-29.15,9.45Z"></path>
                                             </g>
                                           </g>
                                           <g>
                                             <path class="cls-1a" d="m286.65,118.51h-17.37l-39.32-59.42v59.42h-17.37V31.8h17.37l39.32,59.54V31.8h17.37v86.71Z"></path>
                                             <path class="cls-1a" d="m304.95,38.69c-2.03-1.94-3.04-4.36-3.04-7.26s1.01-5.31,3.04-7.26c2.03-1.94,4.57-2.92,7.63-2.92s5.6.97,7.63,2.92c2.02,1.94,3.04,4.36,3.04,7.26s-1.02,5.31-3.04,7.26c-2.03,1.94-4.57,2.91-7.63,2.91s-5.6-.97-7.63-2.91Zm16.19,11.1v68.72h-17.37V49.79h17.37Z"></path>
                                             <path class="cls-1a" d="m396.18,56.55c5.04,5.17,7.57,12.38,7.57,21.65v40.31h-17.36v-37.96c0-5.46-1.37-9.65-4.1-12.59-2.73-2.94-6.45-4.4-11.16-4.4s-8.58,1.47-11.35,4.4c-2.77,2.94-4.15,7.13-4.15,12.59v37.96h-17.37V49.79h17.37v8.56c2.31-2.98,5.27-5.31,8.87-7.01,3.6-1.69,7.55-2.54,11.85-2.54,8.19,0,14.8,2.59,19.85,7.75Z"></path>
                                             <path class="cls-1a" d="m437.61,129.8c0,7.61-1.88,13.09-5.64,16.44-3.77,3.35-9.16,5.02-16.19,5.02h-7.69v-14.76h4.96c2.64,0,4.51-.52,5.58-1.55,1.07-1.03,1.61-2.71,1.61-5.02V49.79h17.37v80.01Zm-16.31-91.11c-2.03-1.94-3.04-4.36-3.04-7.26s1.01-5.31,3.04-7.26c2.02-1.94,4.61-2.92,7.75-2.92s5.58.97,7.57,2.92c1.98,1.94,2.98,4.36,2.98,7.26s-.99,5.31-2.98,7.26c-1.98,1.94-4.51,2.91-7.57,2.91s-5.73-.97-7.75-2.91Z"></path>
                                             <path class="cls-1a" d="m454.42,65.42c2.77-5.37,6.53-9.51,11.29-12.4,4.76-2.89,10.07-4.34,15.94-4.34,5.13,0,9.61,1.04,13.46,3.1,3.85,2.07,6.92,4.67,9.24,7.82v-9.8h17.49v68.72h-17.49v-10.05c-2.23,3.23-5.31,5.89-9.24,8-3.93,2.11-8.46,3.16-13.58,3.16-5.79,0-11.06-1.49-15.82-4.47-4.76-2.98-8.52-7.17-11.29-12.59-2.77-5.41-4.16-11.64-4.16-18.67s1.38-13.11,4.16-18.48Zm47.45,7.88c-1.65-3.02-3.89-5.33-6.7-6.95-2.81-1.61-5.83-2.42-9.06-2.42s-6.2.79-8.93,2.36c-2.73,1.57-4.94,3.87-6.64,6.88-1.69,3.02-2.54,6.6-2.54,10.73s.85,7.75,2.54,10.85c1.69,3.1,3.93,5.48,6.7,7.13,2.77,1.66,5.72,2.48,8.87,2.48s6.24-.81,9.06-2.42c2.81-1.61,5.04-3.93,6.7-6.95,1.65-3.02,2.48-6.64,2.48-10.85s-.83-7.83-2.48-10.85Z"></path>
                                             <path class="cls-4a" d="m591.93,102.01h-34.48l-5.71,16.5h-18.24l31.14-86.71h20.22l31.13,86.71h-18.36l-5.71-16.5Zm-4.71-13.89l-12.53-36.22-12.53,36.22h25.06Z"></path>
                                             <path class="cls-4a" d="m645.02,31.93v86.58h-17.37V31.93h17.37Z"></path>
                                           </g>
                                         </g>
                                       </g>
                                     </svg>
                           </div>
                           <div class=" ">
                              <ul class="table-list1 pl0 f-18 f-md-20 w500 text-center lh140 black-clr mt15">
                                 <li>World First ChatGPT AI Powered Email Marketing APP</li>
                                 <li>Create 1000s of High-Converting Emails With Just One Keyword</li>
                                 <li>Send & Scheduled Unlimited Emails To Unlimited Subscribers.</li>
                                 <li>Boost Email Delivery, Click And Open Rate with Engaging Emails</li>
                                 <li>Send Beautiful Emails For Sales, Promotion, Product Delivery And Many More ...</li>
                                 <li>AI Enabled Smart Tagging For Lead Personalization & Traffic</li>
                                 <li>Collect Unlimited Leads With Built-In Lead Form</li>
                                 <li>Design Newsletter & Autoresponder Mails Using The Power Of AI </li>
                                 <li>Unlimited Email with Free SMTP </li>
                                 <li>Upload Unlimited List with Zero Restrictions</li>
                                 <li>Inbuilt AI Text & Inline Editor To Craft Best Emails </li>
                                 <li>Personalize Your Mails to Get High Opening Rates </li>
                                 <li>Create Long-Term Relationships with Your Subscribers With Beautiful Newsletters</li>
                                 <li>100% Control on your online business</li>
                                 <li>100+ High Converting Templates For Webforms & Email</li>
                                 <li>No Monthly Fee Forever. Pay One Time Only...</li>
                                 <li>NinjaAi Mail is 100% GDPR & Can-Spam Compliant</li>
                                 <li>Brand New System- Absolutely NO Rehashes</li>
                                 <li>100% Newbie Friendly And Fully Automated</li>
                                 <li>Step-By-Step Training to Make Everything Easy For You</li>
                                 <li>Commercial License Included</li>
                                 <li class="headline my15">Fast Action Bonuses!</li>
                                 <li>Fast Action Bonus #1 – Email Marketing DFY Business (with PLR Rights)</li>
                                 <li>Fast Action Bonus #2 – Progressive List Building DFY Business (with PLR Rights)</li>
                                 <li>Fast Action Bonus #3 – Affiliate Marketing Made Easy</li>
                                 <li>Fast Action Bonus #4 – Auto Video Creator - Create your own professional videos!</li>
                              </ul>
                           </div>
                           <div class="table-btn ">
                              <div class="hideme-button">
                                 <a href="https://warriorplus.com/o2/buy/zh5gzv/h65s1l/y9w9jv"><img src="https://warriorplus.com/o2/btn/fn200011000/zh5gzv/h65s1l/357336" class="img-fluid d-block mx-auto"></a>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt30 ">
                  <div class="f-md-22 f-20 w400 lh140 text-center ">
                     *Commercial License Is ONLY Available With The Purchase Of Commercial Plan
                  </div>
               </div>
               <div class="row">
                  <div class="col-md-6 col-12 mt20 mt-md30 text-center mx-auto">
                     <a href="https://warriorplus.com/o2/buy/zh5gzv/h65s1l/y9w9jv" class="yes-btn px0 d-block">Yes, Upgrade To Mail Edition</a>
                  </div>
                  <div class="col-md-6 col-12 mt20 mt-md30 text-center mx-auto">
                     <a href="https://warriorplus.com/o/nothanks/h65s1l" class="no-btn px0 d-block">No Thanks</a>
                  </div>
               </div>
            </div>
         </div>
      </div> 
      <!-- Discounted Price Section End -->

      <!--Footer Section Start -->
      <div class="footer-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
               <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 645.02 151.26" style="max-height:70px">
                                    <defs>
                                      <style>
                                        .cls-1 {
                                          fill: url(#linear-gradient-2);
                                        }
                                  
                                        .cls-2 {
                                          fill: #fff;
                                        }
                                  
                                        .cls-3 {
                                          fill-rule: evenodd;
                                        }
                                  
                                        .cls-3, .cls-4 {
                                          fill: #ff813b;
                                        }
                                  
                                        .cls-5 {
                                          fill: url(#linear-gradient-3);
                                        }
                                  
                                        .cls-6 {
                                          fill: url(#linear-gradient);
                                        }
                                  
                                        .cls-7 {
                                          fill: url(#linear-gradient-4);
                                        }
                                      </style>
                                      <linearGradient id="linear-gradient" x1="47.81" y1="75" x2="185.48" y2="75" gradientUnits="userSpaceOnUse">
                                        <stop offset="0" stop-color="#b956ff"></stop>
                                        <stop offset="1" stop-color="#6e33ff"></stop>
                                      </linearGradient>
                                      <linearGradient id="linear-gradient-2" x1="0" y1="75" x2="85.18" y2="75" gradientUnits="userSpaceOnUse">
                                        <stop offset="0" stop-color="#b956ff"></stop>
                                        <stop offset="1" stop-color="#a356fa"></stop>
                                      </linearGradient>
                                      <linearGradient id="linear-gradient-3" x1="55.61" y1="62.54" x2="62.86" y2="62.54" gradientUnits="userSpaceOnUse">
                                        <stop offset="0" stop-color="#b956ff"></stop>
                                        <stop offset="1" stop-color="#ac59fa"></stop>
                                      </linearGradient>
                                      <linearGradient id="linear-gradient-4" x1="54.62" y1="79" x2="61.87" y2="79" gradientTransform="translate(19.64 -11.24) rotate(13.24)" xlink:href="#linear-gradient-3"></linearGradient>
                                    </defs>
                                    <g id="Layer_1-2" data-name="Layer 1">
                                      <g>
                                        <g>
                                          <g>
                                            <path class="cls-6" d="m181.49,58.53h-10.72c-1.4-5.13-3.43-9.99-6.01-14.51l7.58-7.58c1.56-1.56,1.56-4.09,0-5.65l-17.65-17.65c-1.56-1.56-4.09-1.56-5.65,0l-7.58,7.58c-4.51-2.58-9.38-4.61-14.51-6.01V4c0-2.21-1.79-4-4-4h-24.96c-2.21,0-4,1.79-4,4v10.72c-.16.04-.31.09-.47.14-.27.08-.53.15-.8.23-.27.08-.55.16-.82.24-.55.17-1.09.35-1.63.53-.11.04-.22.08-.33.12-.48.17-.96.34-1.44.52-.12.04-.24.09-.36.14-.52.2-1.04.4-1.56.62-.04.02-.08.03-.12.05-2.41,1-4.74,2.15-6.99,3.43l-7.57-7.57c-1.56-1.56-4.09-1.56-5.65,0l-17.65,17.65c-.36.36-.63.76-.82,1.2h61.99v.02c.23,0,.45-.02.68-.02,23.76,0,43.01,19.26,43.01,43.01s-19.26,43.01-43.01,43.01c-.5,0-1-.02-1.5-.04v.04h-61.17c.19.43.46.84.82,1.2l17.65,17.65c1.56,1.56,4.09,1.56,5.65,0l7.57-7.57c2.25,1.28,4.58,2.43,6.99,3.43.04.02.08.03.12.05.51.21,1.03.42,1.55.62.12.05.24.09.36.14.47.18.95.35,1.43.52.11.04.23.08.34.12.54.18,1.08.36,1.63.53.27.08.55.16.83.25.26.08.53.16.79.23.16.04.31.09.47.14v10.72c0,2.21,1.79,4,4,4h24.96c2.21,0,4-1.79,4-4v-10.72c5.13-1.4,9.99-3.43,14.51-6.01l7.58,7.58c1.56,1.56,4.09,1.56,5.65,0l17.65-17.65c1.56-1.56,1.56-4.09,0-5.65l-7.58-7.58c2.58-4.51,4.61-9.38,6.01-14.51h10.72c2.21,0,4-1.79,4-4v-24.96c0-2.21-1.79-4-4-4Z"></path>
                                            <path class="cls-1" d="m76.64,101.54c-5.74-7.31-9.18-16.52-9.18-26.54s3.44-19.23,9.18-26.54c2.45-3.13,5.33-5.9,8.53-8.24h-48.39c-1.31-1.89-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.13h17.07c-1.51,3.21-2.76,6.58-3.71,10.07h3.1c1.31-1.89,3.49-3.13,5.97-3.13,4.01,0,7.25,3.25,7.25,7.25s-3.22,7.23-7.21,7.25h-.04c-2.48,0-4.66-1.24-5.97-3.13H13.22c-.44-.64-.98-1.19-1.59-1.65-1.21-.92-2.73-1.48-4.38-1.48-4,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.14h38.82c1.31-1.89,3.49-3.13,5.97-3.13.43,0,.84.05,1.25.12,3.41.59,6,3.56,6,7.14,0,2.78-1.56,5.19-3.86,6.41-1.01.53-2.16.84-3.39.84-2.48,0-4.66-1.24-5.97-3.13h-17.85c-1.31-1.9-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.13h15.96c.95,3.48,2.2,6.85,3.71,10.07h-5.63c-1.31-1.89-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25,0,1.53.48,2.95,1.28,4.12,1.31,1.9,3.49,3.14,5.97,3.14s4.66-1.24,5.97-3.14h36.95c-3.21-2.34-6.08-5.11-8.53-8.24ZM30.64,47.97c-2,0-3.62-1.62-3.62-3.62s1.62-3.62,3.62-3.62,3.63,1.62,3.63,3.62-1.63,3.62-3.63,3.62Zm-23.39,26.47c-2,0-3.62-1.63-3.62-3.62s1.63-3.63,3.62-3.63,3.63,1.63,3.63,3.63-1.63,3.62-3.63,3.62Zm20.97,16.45c-2,0-3.63-1.63-3.63-3.63s1.63-3.62,3.63-3.62,3.62,1.63,3.62,3.62-1.63,3.63-3.62,3.63Zm14.08,18.39c-2,0-3.62-1.62-3.62-3.62s1.62-3.62,3.62-3.62,3.62,1.62,3.62,3.62-1.62,3.62-3.62,3.62Z"></path>
                                            <circle class="cls-5" cx="59.23" cy="62.54" r="3.63"></circle>
                                            <circle class="cls-7" cx="58.24" cy="79" r="3.63" transform="translate(-16.54 15.44) rotate(-13.24)"></circle>
                                          </g>
                                          <g>
                                            <g>
                                              <path class="cls-3" d="m104.63,78.16c-9.44,6-14.52,2.86-15.25-9.4,4.93,3.38,10.02,6.52,15.25,9.4Z"></path>
                                              <path class="cls-3" d="m131.01,68.76c-.72,12.26-5.81,15.4-15.25,9.4,5.24-2.88,10.32-6.02,15.25-9.4Z"></path>
                                            </g>
                                            <path class="cls-4" d="m110.16,40.13c-.89,0-1.78.03-2.66.1-.08,0-.16,0-.24.02-9.59.79-18.07,5.45-23.89,12.42-.25.3-.49.59-.73.89-.14.19-.29.38-.43.57-4.02,5.37-6.52,11.93-6.88,19.07,0,.01,0,.03,0,.04-.01.22-.02.45-.03.68-.01.3-.02.6-.02.9v.18c0,.28,0,.55,0,.83.44,18.64,15.5,33.66,34.15,34.04.24,0,.48,0,.72,0,.3,0,.6,0,.9,0,18.85-.48,33.98-15.9,33.98-34.87s-15.61-34.88-34.88-34.88Zm.19,39.35c-14.41,14.37-29.15,1.23-29.15-9.45s13.05,0,29.15,0,29.15-10.67,29.15,0-14.74,23.81-29.15,9.45Z"></path>
                                          </g>
                                        </g>
                                        <g>
                                          <path class="cls-2" d="m286.65,118.51h-17.37l-39.32-59.42v59.42h-17.37V31.8h17.37l39.32,59.54V31.8h17.37v86.71Z"></path>
                                          <path class="cls-2" d="m304.95,38.69c-2.03-1.94-3.04-4.36-3.04-7.26s1.01-5.31,3.04-7.26c2.03-1.94,4.57-2.92,7.63-2.92s5.6.97,7.63,2.92c2.02,1.94,3.04,4.36,3.04,7.26s-1.02,5.31-3.04,7.26c-2.03,1.94-4.57,2.91-7.63,2.91s-5.6-.97-7.63-2.91Zm16.19,11.1v68.72h-17.37V49.79h17.37Z"></path>
                                          <path class="cls-2" d="m396.18,56.55c5.04,5.17,7.57,12.38,7.57,21.65v40.31h-17.36v-37.96c0-5.46-1.37-9.65-4.1-12.59-2.73-2.94-6.45-4.4-11.16-4.4s-8.58,1.47-11.35,4.4c-2.77,2.94-4.15,7.13-4.15,12.59v37.96h-17.37V49.79h17.37v8.56c2.31-2.98,5.27-5.31,8.87-7.01,3.6-1.69,7.55-2.54,11.85-2.54,8.19,0,14.8,2.59,19.85,7.75Z"></path>
                                          <path class="cls-2" d="m437.61,129.8c0,7.61-1.88,13.09-5.64,16.44-3.77,3.35-9.16,5.02-16.19,5.02h-7.69v-14.76h4.96c2.64,0,4.51-.52,5.58-1.55,1.07-1.03,1.61-2.71,1.61-5.02V49.79h17.37v80.01Zm-16.31-91.11c-2.03-1.94-3.04-4.36-3.04-7.26s1.01-5.31,3.04-7.26c2.02-1.94,4.61-2.92,7.75-2.92s5.58.97,7.57,2.92c1.98,1.94,2.98,4.36,2.98,7.26s-.99,5.31-2.98,7.26c-1.98,1.94-4.51,2.91-7.57,2.91s-5.73-.97-7.75-2.91Z"></path>
                                          <path class="cls-2" d="m454.42,65.42c2.77-5.37,6.53-9.51,11.29-12.4,4.76-2.89,10.07-4.34,15.94-4.34,5.13,0,9.61,1.04,13.46,3.1,3.85,2.07,6.92,4.67,9.24,7.82v-9.8h17.49v68.72h-17.49v-10.05c-2.23,3.23-5.31,5.89-9.24,8-3.93,2.11-8.46,3.16-13.58,3.16-5.79,0-11.06-1.49-15.82-4.47-4.76-2.98-8.52-7.17-11.29-12.59-2.77-5.41-4.16-11.64-4.16-18.67s1.38-13.11,4.16-18.48Zm47.45,7.88c-1.65-3.02-3.89-5.33-6.7-6.95-2.81-1.61-5.83-2.42-9.06-2.42s-6.2.79-8.93,2.36c-2.73,1.57-4.94,3.87-6.64,6.88-1.69,3.02-2.54,6.6-2.54,10.73s.85,7.75,2.54,10.85c1.69,3.1,3.93,5.48,6.7,7.13,2.77,1.66,5.72,2.48,8.87,2.48s6.24-.81,9.06-2.42c2.81-1.61,5.04-3.93,6.7-6.95,1.65-3.02,2.48-6.64,2.48-10.85s-.83-7.83-2.48-10.85Z"></path>
                                          <path class="cls-4" d="m591.93,102.01h-34.48l-5.71,16.5h-18.24l31.14-86.71h20.22l31.13,86.71h-18.36l-5.71-16.5Zm-4.71-13.89l-12.53-36.22-12.53,36.22h25.06Z"></path>
                                          <path class="cls-4" d="m645.02,31.93v86.58h-17.37V31.93h17.37Z"></path>
                                        </g>
                                      </g>
                                    </g>
                                  </svg>
                  <div class="f-14 f-md-16 w400 mt20 lh150 white-clr text-center" style="color:#7d8398;"><span class="w600">Note:</span> This site is not a part of the Facebook website or Facebook Inc. Additionally, this site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc.<br><br>
                  <div class=" mt20 mt-md40 white-clr text-center"> <script type="text/javascript" src="https://warriorplus.com/o2/disclaimer/zh5gzv" defer=""></script><div class="wplus_spdisclaimer"><strong>Disclaimer:</strong> <em>WarriorPlus is used to help manage the sale of products on this site. While WarriorPlus helps facilitate the sale, all payments are made directly to the product vendor and NOT WarriorPlus. Thus, all product questions, support inquiries and/or refund requests must be sent to the vendor. WarriorPlus's role should not be construed as an endorsement, approval or review of these products or any claim, statement or opinion used in the marketing of these products.</em></div></div>
                  </div>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-md-16 f-16 w400 lh140 white-clr text-xs-center">Copyright © NinjaAi Mail 2023</div>
                  <ul class="footer-ul w400 f-md-16 f-16 white-clr text-center text-md-right">
                     <li><a href="https://support.bizomart.com/hc/en-us " class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://www.getninjaai.com/legal/privacy-policy.html " class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://www.getninjaai.com/legal/terms-of-service.html " class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://www.getninjaai.com/legal/disclaimer.html " class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://www.getninjaai.com/legal/gdpr.html " class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://www.getninjaai.com/legal/dmca.html " class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://www.getninjaai.com/legal/anti-spam.html " class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!--Footer Section End -->

   <!-- New Timer  Start-->
<!-- timer --->
<!-- Exit Popup -->	
<style>
   </style>
<script type="text/javascript" src="assets/js/ouibounce.min.js"></script>
<div id="ouibounce-modal" style="z-index:9999999; display:none;">
   <div class="underlay"></div>
   <div class="modal-wrapper" style="display:block;">
      <div class="modal-bg">
		<button type="button" class="close" onclick="document.getElementById('ouibounce-modal').style.display = 'none';">
			X 
		</button>
		<div class="model-header">
      <div class="d-flex justify-content-center align-items-center gap-3">
            <img src="assets/images/hold.png" alt="" class="img-fluid d-block m131">
            <div>
               <div class="f-md-56 f-24 w700 lh130 white-clr">
                  WAIT! HOLD ON!!
               </div>
               <div class="f-md-28 f-18 w700 lh130 black-clr mt10 mt-md0" style="background-color:yellow; padding:5px 10px; display:inline-block">
                  Don't Leave Empty Handed
               </div>
            </div>
         </div>
		</div>
         <div class="col-12 for-padding">
			<div class="copun-line f-md-45 f-16 w500 lh130 text-center black-clr mt10">
         You've Qualified For An <span class="w700 red-clr"><br> INSTANT $60 DISCOUNT</span>  
			</div>
         <div class="copun-line f-md-30 f-16 w700 lh130 text-center black-clr mt10" style="background-color:yellow; padding:5px 10px; display:inline-block">
         Regular Price <strike>$97</strike>,
         Now Only $37
			</div>
			 <div class="mt-md20 mt10 text-center">
				<a href="https://warriorplus.com/o2/buy/zh5gzv/q2gdg8/dq92vx" class="cta-link-btn1">Grab SPECIAL Discount Now!
				<br>
				<span class="f-12 black-clr w600 text-uppercase">Act Now! Limited to First 100 Buyers Only.</span> </a>
			 </div>
			<!-- <div class="mt-md20 mt10 text-center f-md-26 f-16 w900 lh150 text-center red-clr text-uppercase">
				Coupon Code EXPIRES IN:
			</div>
			<div class="timer-new">
            <div class="days" class="timer-label">
               <span id="days"></span> <span class="text">Days</span>
            </div> 
            <div class="hours" class="timer-label">
               <span id="hours"></span> <span  class="text">Hours</span>
            </div>
            <div class="days" class="timer-label">
               <span id="mins" ></span> <span class="text">Min</span>
            </div>
            <div class="secs" class="timer-label">
               <span id="secs" ></span> <span class="text">Sec</span>
            </div>
         </div> -->
      </div>
   </div>
</div>
<script type="text/javascript">
      var timeInSecs;
      var ticker;
      function startTimer(secs) {
      timeInSecs = parseInt(secs);
	  tick();
      //ticker = setInterval("tick()", 1000);
      }
      function tick() {
      var secs = timeInSecs;
      if (secs > 0) {
      timeInSecs--;
      } else {
      clearInterval(ticker);
      //startTimer(20 * 60); // 4 minutes in seconds
      }
      var days = Math.floor(secs / 86400);
      secs %= 86400;
      var hours = Math.floor(secs / 3600);
      secs %= 3600;
      var mins = Math.floor(secs / 60);
      secs %= 60;
      var pretty = ((days < 10) ? "0" : "") + days + ":" + ((hours < 10) ? "0" : "") + hours + ":" + ((mins < 10) ? "0" : "") + mins + ":" + ((secs < 10) ? "0" : "") + secs;
      document.getElementById("days").innerHTML = days
      document.getElementById("hours").innerHTML = hours
      document.getElementById("mins").innerHTML = mins
      document.getElementById("secs").innerHTML = secs;
      }
      
	  //startTimer(60 * 60); 
      // 4 minutes in seconds
	  
	  function getCookie(cname) {
            var name = cname + "=";
            var decodedCookie = decodeURIComponent(document.cookie);
            var ca = decodedCookie.split(';');
            for(var i = 0; i <ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        }


      var cnt = 60*60;

      function counter(){
       
         if(getCookie("cnt") > 0){
            cnt = getCookie("cnt");
         }

         cnt -= 1;
         document.cookie = "cnt="+ cnt;

         startTimer(getCookie("cnt"));
         //jQuery("#counter").val(getCookie("cnt"));

         if(cnt>0){
            setTimeout(counter,1000);
         }
      
      }
      
      counter();
	  
	  
      </script>-
      <script type="text/javascript">
         var _ouibounce = ouibounce(document.getElementById('ouibounce-modal'),{
         aggressive: true, //Making this true makes ouibounce not to obey "once per visitor" rule
         });
      </script>
      <!-- Exit Popup Ends-->
            <!--- timer end-->
   <!-- Exit Popup and Timer End -->
</body>
</html>
<script> 
function myFunction() {
  var checkBox = document.getElementById("check");
  var buybutton = document.getElementById("buyButton");
  if (checkBox.checked == true){
 
	    buybutton.getAttribute("href");
	    buybutton.setAttribute("href","https://warriorplus.com/o2/buy/hb06pf/f0znfl/rzp51k");
	} else {
		 buybutton.getAttribute("href");
		 buybutton.setAttribute("href","https://warriorplus.com/o2/buy/hb06pf/f0znfl/kpcb3m");
	  }
}
</script>
<!Doctype html>
<html>

<head>
    <title>NinjaAi Reseller</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=9">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta name="title" content="NinjaAi | Reseller">
    <meta name="description" content="NinjaAi">
    <meta name="keywords" content="NinjaAi">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Spartan:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">

    <meta property="og:image" content="https://getninjaai.com/reseller/thumbnail.png">
    <meta name="language" content="English">
    <meta name="revisit-after" content="1 days">
    <meta name="author" content="Pranshu Gupta">
    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:title" content="NinjaAi | Reseller">
    <meta property="og:description" content="NinjaAi">
    <meta property="og:image" content="https://getninjaai.com/reseller/thumbnail.png">
    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:title" content="NinjaAi | Reseller">
    <meta property="twitter:description" content="NinjaAi">
    <meta property="twitter:image" content="https://getninjaai.com/reseller/thumbnail.png">

    <!-- Start Editor required -->
    <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
    <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    <!-- Start Editor required -->
    <link rel="stylesheet" href="https://cdn.oppyotest.com/launches/sellero/common_assets/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="assets/css/style.css" type="text/css">
    <link rel="stylesheet" href="assets/css/timer.css" type="text/css">
    <script src="https://cdn.oppyotest.com/launches/sellero/common_assets/js/bootstrap.min.js"></script>
    <script src="https://cdn.oppyotest.com/launches/sellero/common_assets/js/jquery.min.js"></script>
    <!-- End -->

</head>

<body>

<div class="warning-section">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <div class="offer">
                        <div>
                            <img src="https://cdn.oppyo.com/launches/appzilo/special/error.webp">
                        </div>
                        <div class="f-22 f-md-30 w600 lh140 white-clr text-center">
                                WAIT! WAIT! YOUR ORDER IS NOT YET COMPLETED... *DO NOT CLOSE*
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--1. Header Section Start -->
    <div class="header-section">
        <div class="container">
            <div class="row ">
                <div class="col-12 px-md15">
                    <div class="text-center">
                    <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 645.02 151.26" style="max-height:70px">
                                    <defs>
                                      <style>
                                        .cls-1 {
                                          fill: url(#linear-gradient-2);
                                        }
                                  
                                        .cls-2 {
                                          fill: #fff;
                                        }
                                  
                                        .cls-3 {
                                          fill-rule: evenodd;
                                        }
                                  
                                        .cls-3, .cls-4 {
                                          fill: #ff813b;
                                        }
                                  
                                        .cls-5 {
                                          fill: url(#linear-gradient-3);
                                        }
                                  
                                        .cls-6 {
                                          fill: url(#linear-gradient);
                                        }
                                  
                                        .cls-7 {
                                          fill: url(#linear-gradient-4);
                                        }
                                      </style>
                                      <linearGradient id="linear-gradient" x1="47.81" y1="75" x2="185.48" y2="75" gradientUnits="userSpaceOnUse">
                                        <stop offset="0" stop-color="#b956ff"></stop>
                                        <stop offset="1" stop-color="#6e33ff"></stop>
                                      </linearGradient>
                                      <linearGradient id="linear-gradient-2" x1="0" y1="75" x2="85.18" y2="75" gradientUnits="userSpaceOnUse">
                                        <stop offset="0" stop-color="#b956ff"></stop>
                                        <stop offset="1" stop-color="#a356fa"></stop>
                                      </linearGradient>
                                      <linearGradient id="linear-gradient-3" x1="55.61" y1="62.54" x2="62.86" y2="62.54" gradientUnits="userSpaceOnUse">
                                        <stop offset="0" stop-color="#b956ff"></stop>
                                        <stop offset="1" stop-color="#ac59fa"></stop>
                                      </linearGradient>
                                      <linearGradient id="linear-gradient-4" x1="54.62" y1="79" x2="61.87" y2="79" gradientTransform="translate(19.64 -11.24) rotate(13.24)" xlink:href="#linear-gradient-3"></linearGradient>
                                    </defs>
                                    <g id="Layer_1-2" data-name="Layer 1">
                                      <g>
                                        <g>
                                          <g>
                                            <path class="cls-6" d="m181.49,58.53h-10.72c-1.4-5.13-3.43-9.99-6.01-14.51l7.58-7.58c1.56-1.56,1.56-4.09,0-5.65l-17.65-17.65c-1.56-1.56-4.09-1.56-5.65,0l-7.58,7.58c-4.51-2.58-9.38-4.61-14.51-6.01V4c0-2.21-1.79-4-4-4h-24.96c-2.21,0-4,1.79-4,4v10.72c-.16.04-.31.09-.47.14-.27.08-.53.15-.8.23-.27.08-.55.16-.82.24-.55.17-1.09.35-1.63.53-.11.04-.22.08-.33.12-.48.17-.96.34-1.44.52-.12.04-.24.09-.36.14-.52.2-1.04.4-1.56.62-.04.02-.08.03-.12.05-2.41,1-4.74,2.15-6.99,3.43l-7.57-7.57c-1.56-1.56-4.09-1.56-5.65,0l-17.65,17.65c-.36.36-.63.76-.82,1.2h61.99v.02c.23,0,.45-.02.68-.02,23.76,0,43.01,19.26,43.01,43.01s-19.26,43.01-43.01,43.01c-.5,0-1-.02-1.5-.04v.04h-61.17c.19.43.46.84.82,1.2l17.65,17.65c1.56,1.56,4.09,1.56,5.65,0l7.57-7.57c2.25,1.28,4.58,2.43,6.99,3.43.04.02.08.03.12.05.51.21,1.03.42,1.55.62.12.05.24.09.36.14.47.18.95.35,1.43.52.11.04.23.08.34.12.54.18,1.08.36,1.63.53.27.08.55.16.83.25.26.08.53.16.79.23.16.04.31.09.47.14v10.72c0,2.21,1.79,4,4,4h24.96c2.21,0,4-1.79,4-4v-10.72c5.13-1.4,9.99-3.43,14.51-6.01l7.58,7.58c1.56,1.56,4.09,1.56,5.65,0l17.65-17.65c1.56-1.56,1.56-4.09,0-5.65l-7.58-7.58c2.58-4.51,4.61-9.38,6.01-14.51h10.72c2.21,0,4-1.79,4-4v-24.96c0-2.21-1.79-4-4-4Z"></path>
                                            <path class="cls-1" d="m76.64,101.54c-5.74-7.31-9.18-16.52-9.18-26.54s3.44-19.23,9.18-26.54c2.45-3.13,5.33-5.9,8.53-8.24h-48.39c-1.31-1.89-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.13h17.07c-1.51,3.21-2.76,6.58-3.71,10.07h3.1c1.31-1.89,3.49-3.13,5.97-3.13,4.01,0,7.25,3.25,7.25,7.25s-3.22,7.23-7.21,7.25h-.04c-2.48,0-4.66-1.24-5.97-3.13H13.22c-.44-.64-.98-1.19-1.59-1.65-1.21-.92-2.73-1.48-4.38-1.48-4,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.14h38.82c1.31-1.89,3.49-3.13,5.97-3.13.43,0,.84.05,1.25.12,3.41.59,6,3.56,6,7.14,0,2.78-1.56,5.19-3.86,6.41-1.01.53-2.16.84-3.39.84-2.48,0-4.66-1.24-5.97-3.13h-17.85c-1.31-1.9-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.13h15.96c.95,3.48,2.2,6.85,3.71,10.07h-5.63c-1.31-1.89-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25,0,1.53.48,2.95,1.28,4.12,1.31,1.9,3.49,3.14,5.97,3.14s4.66-1.24,5.97-3.14h36.95c-3.21-2.34-6.08-5.11-8.53-8.24ZM30.64,47.97c-2,0-3.62-1.62-3.62-3.62s1.62-3.62,3.62-3.62,3.63,1.62,3.63,3.62-1.63,3.62-3.63,3.62Zm-23.39,26.47c-2,0-3.62-1.63-3.62-3.62s1.63-3.63,3.62-3.63,3.63,1.63,3.63,3.63-1.63,3.62-3.63,3.62Zm20.97,16.45c-2,0-3.63-1.63-3.63-3.63s1.63-3.62,3.63-3.62,3.62,1.63,3.62,3.62-1.63,3.63-3.62,3.63Zm14.08,18.39c-2,0-3.62-1.62-3.62-3.62s1.62-3.62,3.62-3.62,3.62,1.62,3.62,3.62-1.62,3.62-3.62,3.62Z"></path>
                                            <circle class="cls-5" cx="59.23" cy="62.54" r="3.63"></circle>
                                            <circle class="cls-7" cx="58.24" cy="79" r="3.63" transform="translate(-16.54 15.44) rotate(-13.24)"></circle>
                                          </g>
                                          <g>
                                            <g>
                                              <path class="cls-3" d="m104.63,78.16c-9.44,6-14.52,2.86-15.25-9.4,4.93,3.38,10.02,6.52,15.25,9.4Z"></path>
                                              <path class="cls-3" d="m131.01,68.76c-.72,12.26-5.81,15.4-15.25,9.4,5.24-2.88,10.32-6.02,15.25-9.4Z"></path>
                                            </g>
                                            <path class="cls-4" d="m110.16,40.13c-.89,0-1.78.03-2.66.1-.08,0-.16,0-.24.02-9.59.79-18.07,5.45-23.89,12.42-.25.3-.49.59-.73.89-.14.19-.29.38-.43.57-4.02,5.37-6.52,11.93-6.88,19.07,0,.01,0,.03,0,.04-.01.22-.02.45-.03.68-.01.3-.02.6-.02.9v.18c0,.28,0,.55,0,.83.44,18.64,15.5,33.66,34.15,34.04.24,0,.48,0,.72,0,.3,0,.6,0,.9,0,18.85-.48,33.98-15.9,33.98-34.87s-15.61-34.88-34.88-34.88Zm.19,39.35c-14.41,14.37-29.15,1.23-29.15-9.45s13.05,0,29.15,0,29.15-10.67,29.15,0-14.74,23.81-29.15,9.45Z"></path>
                                          </g>
                                        </g>
                                        <g>
                                          <path class="cls-2" d="m286.65,118.51h-17.37l-39.32-59.42v59.42h-17.37V31.8h17.37l39.32,59.54V31.8h17.37v86.71Z"></path>
                                          <path class="cls-2" d="m304.95,38.69c-2.03-1.94-3.04-4.36-3.04-7.26s1.01-5.31,3.04-7.26c2.03-1.94,4.57-2.92,7.63-2.92s5.6.97,7.63,2.92c2.02,1.94,3.04,4.36,3.04,7.26s-1.02,5.31-3.04,7.26c-2.03,1.94-4.57,2.91-7.63,2.91s-5.6-.97-7.63-2.91Zm16.19,11.1v68.72h-17.37V49.79h17.37Z"></path>
                                          <path class="cls-2" d="m396.18,56.55c5.04,5.17,7.57,12.38,7.57,21.65v40.31h-17.36v-37.96c0-5.46-1.37-9.65-4.1-12.59-2.73-2.94-6.45-4.4-11.16-4.4s-8.58,1.47-11.35,4.4c-2.77,2.94-4.15,7.13-4.15,12.59v37.96h-17.37V49.79h17.37v8.56c2.31-2.98,5.27-5.31,8.87-7.01,3.6-1.69,7.55-2.54,11.85-2.54,8.19,0,14.8,2.59,19.85,7.75Z"></path>
                                          <path class="cls-2" d="m437.61,129.8c0,7.61-1.88,13.09-5.64,16.44-3.77,3.35-9.16,5.02-16.19,5.02h-7.69v-14.76h4.96c2.64,0,4.51-.52,5.58-1.55,1.07-1.03,1.61-2.71,1.61-5.02V49.79h17.37v80.01Zm-16.31-91.11c-2.03-1.94-3.04-4.36-3.04-7.26s1.01-5.31,3.04-7.26c2.02-1.94,4.61-2.92,7.75-2.92s5.58.97,7.57,2.92c1.98,1.94,2.98,4.36,2.98,7.26s-.99,5.31-2.98,7.26c-1.98,1.94-4.51,2.91-7.57,2.91s-5.73-.97-7.75-2.91Z"></path>
                                          <path class="cls-2" d="m454.42,65.42c2.77-5.37,6.53-9.51,11.29-12.4,4.76-2.89,10.07-4.34,15.94-4.34,5.13,0,9.61,1.04,13.46,3.1,3.85,2.07,6.92,4.67,9.24,7.82v-9.8h17.49v68.72h-17.49v-10.05c-2.23,3.23-5.31,5.89-9.24,8-3.93,2.11-8.46,3.16-13.58,3.16-5.79,0-11.06-1.49-15.82-4.47-4.76-2.98-8.52-7.17-11.29-12.59-2.77-5.41-4.16-11.64-4.16-18.67s1.38-13.11,4.16-18.48Zm47.45,7.88c-1.65-3.02-3.89-5.33-6.7-6.95-2.81-1.61-5.83-2.42-9.06-2.42s-6.2.79-8.93,2.36c-2.73,1.57-4.94,3.87-6.64,6.88-1.69,3.02-2.54,6.6-2.54,10.73s.85,7.75,2.54,10.85c1.69,3.1,3.93,5.48,6.7,7.13,2.77,1.66,5.72,2.48,8.87,2.48s6.24-.81,9.06-2.42c2.81-1.61,5.04-3.93,6.7-6.95,1.65-3.02,2.48-6.64,2.48-10.85s-.83-7.83-2.48-10.85Z"></path>
                                          <path class="cls-4" d="m591.93,102.01h-34.48l-5.71,16.5h-18.24l31.14-86.71h20.22l31.13,86.71h-18.36l-5.71-16.5Zm-4.71-13.89l-12.53-36.22-12.53,36.22h25.06Z"></path>
                                          <path class="cls-4" d="m645.02,31.93v86.58h-17.37V31.93h17.37Z"></path>
                                        </g>
                                      </g>
                                    </g>
                                  </svg>
                    </div>
                    <!-- <div class="col-12 text-center mt20 mt-md50 f-16 f-md-18 w700 lh160 white-clr">
                        We've NEVER done this before...
                    </div> -->
                    <div class="col-12 text-center lh150 mt20 mt-md50">
                        <div class="pre-heading f-18 f-md-22 w600 lh140 white-clr">
                            We've NEVER done this before...
                          </div>
                     </div>
                     <div class="col-12 mt-md30 mt20 f-md-50 f-28 w400 text-center white-clr lh140">
                        How Would You Like to BANK BIG By Selling <span class="w800 under orange-clr"> NinjaAi Software As Your Own And Keep 100% Profits </span> in Your Pocket?
                    </div>
                    <div class="col-12 mt-md25 mt20">
                        <div class="f-20 f-md-22 w500 lh140 white-clr text-center">
                        NO Investment, No Profit Sharing, & No Sales Pages & Video Creation - Everything is<br class="d-none d-md-block"> Done for You on A Silver Platter to Get Started Right Away!
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto mt-md30 mt20 mx-auto">
                    <!-- <div class="responsive-video" editabletype="video">
                        <iframe src="https://coursesify.dotcompal.com/video/embed/97z38agmqx" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                     box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                    </div> -->
                    <div>
                        <img src="assets/images/product-box.webp" alt="" class="img-fluid d-block mx-auto">
                    </div>
                </div>
                <div class="col-12 mt30 mt-md50 f-md-24 f-20 lh140 w400 text-center white-clr">
                    Start your own software business to get results like Gurus starting Today
                </div>
                <div class="col-md-10 mx-auto col-12 mt20 f-20 f-md-35 text-center up-btn-header">
                    <a href="#buynow" class="text-center"> Get Instant Access to NinjaAi Reseller </a>
                </div>
            </div>
        </div>
    </div></div>
    <!--1. Header Section End -->

    <!--2. Second Section Start -->
    <div class="second-sec">
        <div class="container">
            <div class="row m0">
                <div class="col-12 text-center text-capitalize p0">
                    <div class="f-md-45 f-28 w700 lh160 text-capitalize text-center black-clr">
                        Here's One Last And Very Important Thing <br class="d-none d-lg-block"> Before You Go Inside The Member's Area!
                    </div>
                    <div class="f-18 f-md-20 w500 lh160 text-capitalize text-center black-clr mt15">
                        We've Decided To Let You Sell NinjaAi To Anyone And <br class="d-none d-lg-block"><span class="blue-clr"> Keep All Benefits For Yourself.</span> It Clearly Means
                    </div>
                </div>
                <div class="col-12 col-md-10 boxes mt20 mt-md30 mx-auto">
                    <div class="row align-items-center flex-wrap">
                        <div class="col-md-7 col-12 ">
                            <ul class="bigboxes">
                                <li class="f-24 f-md-36"><span class="w700 black-clr lh160">No Profit Sharing-</span> <br>
                                    <span class="f-18 f-md-20 w400 mt5 lh160 pt0 pt-md-20 d-block">You will be authorized to sell NinjaAi to anyone you want on our website and the whole benefit will be only yours.
                              </span>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-5 col-12 mt20 mt-md0">
                            <img src="assets/images/profit.png" class="img-fluid mx-auto d-block">
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-10 boxes mt20 mt-md30 mx-auto">
                    <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-md-7 col-12 order-md-2">
                            <ul class="bigboxes">
                                <li class="f-24 f-md-36"><span class="w700 black-clr lh160">No Investment-</span> <br>
                                    <span class="f-18 f-md-20 w400 mt5 lh160 pt0 pt-md-20 d-block">Today you start selling a winning software to succeed like gurus do.<br>
                              And even better thing is, you can do it all without an initial investment of $5000-10,000 in building it. We already covered it for you.
                              </span>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-5 col-12 mt20 mt-md0 order-md-1">
                            <img src="assets/images/invest.png" class="img-fluid mx-auto d-block">
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-10 boxes mt20 mt-md30 mx-auto">
                    <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-md-7 col-12">
                            <ul class="bigboxes">
                                <li class="f-24 f-md-36"><span class="w700 black-clr lh160">Nothing to Upload, Host or Configure -</span> <br>
                                    <span class="f-18 f-md-20 w400 mt5 lh160 pt0 pt-md-20 d-block">We've Already Done the Hard Work For YOU.<br><br>
                              Take my word, you won’t even need a website. Yeah, you don’t need to bother for doing even a single thing yourself.
                              </span>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-5 col-12 mt20 mt-md0">
                            <img src="assets/images/skills.png" class="img-fluid mx-auto d-block">
                        </div>
                    </div>
                </div>
                <div class="col-12 f-md-45 f-28 w700 lh160 text-capitalize text-center black-clr mt20 mt-md50 px-md15 px0">
                    YES- You'll get to use all our...
                </div>
                <div class="col-12 col-md-8 text-center mt20 mt-md30 mx-auto">
                    <div class="row">
                        <div class="col-md-6 col-12">
                            <ul class="bigboxes1 f-18 f-md-20 w500">
                                <li>Marketing Pages</li>
                                <li>Sales Videos</li>
                            </ul>
                        </div>
                        <div class="col-md-6 col-12">
                            <ul class="bigboxes1 f-18 f-md-20 w500">
                                <li>Members Area</li>
                                <li>Sales funnels etc.</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-12 f-18 f-md-21 w400 lh160 text-center mt20 mt-md30">
                    It’s extremely simple and easy. This means you don’t need to invest your time and money in any grunt work. All you need to do is use everything we’ve provided and sit back and see your benefits pouring in.
                </div>
            </div>
        </div>
    </div>
    <!--2. Second Section End -->

    <!--3. Third Section Start -->
    <div class="grey-section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-md-7 col-12">
                            <div class="f-24 f-md-35 lh160 w600">And You Know What The Best Part Is?
                            </div>
                            <div class="f-18 f-md-20 w400 lh160 mt20 mt-md30 text-left">
                                We will take care of all the customer support
                            </div>
                            <div class="f-18 f-md-20 w400 lh160 mt20 mt-md30 text-left">
                                We have a dedicated help-desk available to handle support, product delivery and training to make it Hassle FREE for you. You don’t even need to do anything…
                            </div>
                        </div>
                        <div class="col-md-5 col-12 mt20 mt-md0">
                            <img src="assets/images/telephone.png" class="img-fluid mx-auto d-block">
                        </div>
                    </div>
                </div>
                <div class="col-12 mt20 mt-md70">
                    <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-md-7 col-12 order-md-2">
                            <div class="f-24 f-md-36 lh160 w600">And YES- You Keep 100% Profits. We have SOME MORE for you.</div>
                            <div class="f-18 f-md-22 w400 lh160 mt20 mt-md30 text-left">
                                You’ll be able to start getting sales, right after the 4-day launch period gets over & keep 100% profits.
                                <br><br> Along with this, <span class="w600">you’ll be delighted to know that, we’re also giving 50% commissions</span> over the entire funnel as a complementary gesture.
                                <br><br> So, you can make up for your investment only by having one sale itself.
                            </div>
                        </div>
                        <div class="col-md-5 col-12 mt20 mt-md0 order-md-1">
                            <img src="assets/images/commision.png" class="img-fluid mx-auto d-block">
                        </div>
                    </div>
                </div>
                <div class="col-12 mt20 mt-md70">
                    <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-12">
                            <div class="f-18 f-md-20 w400 lh160 text-left">
                                And, NinjaAi removes all the hassles and streamlines academy site creation that skyrockets commissions and profits. SO, isn’t that a BIG SOLUTION for marketers.<br><br>
                            </div>
                            <div class="f-24 f-md-36 w700 lh160 text-center">
                                There's a HUGE DEMAND<br><br>
                            </div>
                            <div class="f-18 f-md-20 w400 lh160 text-left">
                                Website building is HOT… marketers whether newbie or experienced, can make tons of money & list by creating stunning funnel sites
                                <br><br>
                                <span class="w600">Marketers & businesses of all size - whether newbie or experienced need this.</span> There are OVER 50Mn businesses & marketers out there - Hungry & looking for a solution like this.
                                <br><br> They can drive FREE traffic & make tons of money by creating beautiful funnel sites. And, NinjaAi removes all the hassles and streamlines this process that skyrockets commissions and profits.
                                <br><br> What if you could give those hungry buyers a feature-packed software that creates stunning funnel sites pre loaded with done for you courses that gets targeted traffic and sales?
                                <br><br>
                                <span class="w600">They will bite your hands to get NinjaAi from you for any amount and you keep all benefits in your pocket with this, opportunity.</span>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--3. Third Section End -->

    <!--4. Fourth Section Starts -->
    <div class="fifth-section">
        <div class="container">
            <div class="row">
                <div class="col-12 f-28 f-md-45 w700 text-center white-clr lh160">
                    EXCLUSIVE FOR OUR CUSTOMERS ONLY
                </div>
                <div class="col-12 mt20 mt-md30">
                    <div class="f-18 f-md-20 w400 lh160 text-left white-clr">
                        We don't want every other person on this planet to have access to this exclusive opportunity. So, we are offering this only for our privileged customers. Hurry, the price is crazily low right now and it will shoot up very soon.<br><br>                        This is possibly your only chance to become a reseller so click the button below and start your own software business. You may never see this offer again...
                    </div>
                </div>
                <div class="col-12 mt30 mt-md50 ">
                    <div class="f-md-22 f-20 lh160 w400 text-center white-clr">
                        GET Exclusive Access to Become a VIP NinjaAi RESELLER TODAY...
                    </div>
                </div>

                <!-- <div class="col-12 col-md-11 mx-auto mt20 mt-md20">
                    <div class="f-20 f-md-35 text-center up-btn-header">
                        <a href="#buynow" class="text-center">
                        GET YOUR RESELLER LICENSE - RIGHT NOW
                     </a>
                    </div>
                </div> -->
                <div class="col-md-10 mx-auto col-12 mt20 mt-md40 f-20 f-md-35 text-center up-btn-header">
                    <a href="#buynow" class="text-center">  Get Your Reseller License - Right Now  </a>
                </div>
            </div>
        </div>
    </div>
    <!--4. Fourth Section End -->

    <!--10. Table Section Starts -->
    <div class="table-section" id="buynow">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="f-20 f-md-22 w500 lh160 text-center">
                        I know this is a mind-blowing deal, something that will make you lot of money. <br class="d-md-block d-none">So, take action now.
                    </div>
                    <div class="f-28 f-md-45 w700 lh160 mt20 mt-md30 text-center black-clr text-capitalize">
                        Go ahead and get this crazy deal and grab your exclusive Reseller license today.
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 mt30 mt-md-90">
                    <div class="tablebox2">
                        <div class="tbbg2 text-center">
                            <div>
                            <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 645.02 151.26" style="max-height:70px">
                                       <defs>
                                         <style>
                                           .cls-1a {
                                             fill: #06101f;
                                           }
                                     
                                           .cls-2a {
                                             fill: url(#linear-gradient-2);
                                           }
                                     
                                           .cls-3a {
                                             fill-rule: evenodd;
                                           }
                                     
                                           .cls-3a, .cls-4a {
                                             fill: #ff813b;
                                           }
                                     
                                           .cls-5a {
                                             fill: url(#linear-gradient-3);
                                           }
                                     
                                           .cls-6a {
                                             fill: url(#linear-gradient);
                                           }
                                     
                                           .cls-7a {
                                             fill: url(#linear-gradient-4);
                                           }
                                         </style>
                                         <linearGradient id="linear-gradient" x1="47.81" y1="75" x2="185.48" y2="75" gradientUnits="userSpaceOnUse">
                                           <stop offset="0" stop-color="#b956ff"></stop>
                                           <stop offset="1" stop-color="#6e33ff"></stop>
                                         </linearGradient>
                                         <linearGradient id="linear-gradient-2" x1="0" y1="75" x2="85.18" y2="75" gradientUnits="userSpaceOnUse">
                                           <stop offset="0" stop-color="#b956ff"></stop>
                                           <stop offset="1" stop-color="#a356fa"></stop>
                                         </linearGradient>
                                         <linearGradient id="linear-gradient-3" x1="55.61" y1="62.54" x2="62.86" y2="62.54" gradientUnits="userSpaceOnUse">
                                           <stop offset="0" stop-color="#b956ff"></stop>
                                           <stop offset="1" stop-color="#ac59fa"></stop>
                                         </linearGradient>
                                         <linearGradient id="linear-gradient-4" x1="54.62" y1="79" x2="61.87" y2="79" gradientTransform="translate(19.64 -11.24) rotate(13.24)" xlink:href="#linear-gradient-3"></linearGradient>
                                       </defs>
                                       <g id="Layer_1-2" data-name="Layer 1">
                                         <g>
                                           <g>
                                             <g>
                                               <path class="cls-6a" d="m181.49,58.53h-10.72c-1.4-5.13-3.43-9.99-6.01-14.51l7.58-7.58c1.56-1.56,1.56-4.09,0-5.65l-17.65-17.65c-1.56-1.56-4.09-1.56-5.65,0l-7.58,7.58c-4.51-2.58-9.38-4.61-14.51-6.01V4c0-2.21-1.79-4-4-4h-24.96c-2.21,0-4,1.79-4,4v10.72c-.16.04-.31.09-.47.14-.27.08-.53.15-.8.23-.27.08-.55.16-.82.24-.55.17-1.09.35-1.63.53-.11.04-.22.08-.33.12-.48.17-.96.34-1.44.52-.12.04-.24.09-.36.14-.52.2-1.04.4-1.56.62-.04.02-.08.03-.12.05-2.41,1-4.74,2.15-6.99,3.43l-7.57-7.57c-1.56-1.56-4.09-1.56-5.65,0l-17.65,17.65c-.36.36-.63.76-.82,1.2h61.99v.02c.23,0,.45-.02.68-.02,23.76,0,43.01,19.26,43.01,43.01s-19.26,43.01-43.01,43.01c-.5,0-1-.02-1.5-.04v.04h-61.17c.19.43.46.84.82,1.2l17.65,17.65c1.56,1.56,4.09,1.56,5.65,0l7.57-7.57c2.25,1.28,4.58,2.43,6.99,3.43.04.02.08.03.12.05.51.21,1.03.42,1.55.62.12.05.24.09.36.14.47.18.95.35,1.43.52.11.04.23.08.34.12.54.18,1.08.36,1.63.53.27.08.55.16.83.25.26.08.53.16.79.23.16.04.31.09.47.14v10.72c0,2.21,1.79,4,4,4h24.96c2.21,0,4-1.79,4-4v-10.72c5.13-1.4,9.99-3.43,14.51-6.01l7.58,7.58c1.56,1.56,4.09,1.56,5.65,0l17.65-17.65c1.56-1.56,1.56-4.09,0-5.65l-7.58-7.58c2.58-4.51,4.61-9.38,6.01-14.51h10.72c2.21,0,4-1.79,4-4v-24.96c0-2.21-1.79-4-4-4Z"></path>
                                               <path class="cls-2a" d="m76.64,101.54c-5.74-7.31-9.18-16.52-9.18-26.54s3.44-19.23,9.18-26.54c2.45-3.13,5.33-5.9,8.53-8.24h-48.39c-1.31-1.89-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.13h17.07c-1.51,3.21-2.76,6.58-3.71,10.07h3.1c1.31-1.89,3.49-3.13,5.97-3.13,4.01,0,7.25,3.25,7.25,7.25s-3.22,7.23-7.21,7.25h-.04c-2.48,0-4.66-1.24-5.97-3.13H13.22c-.44-.64-.98-1.19-1.59-1.65-1.21-.92-2.73-1.48-4.38-1.48-4,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.14h38.82c1.31-1.89,3.49-3.13,5.97-3.13.43,0,.84.05,1.25.12,3.41.59,6,3.56,6,7.14,0,2.78-1.56,5.19-3.86,6.41-1.01.53-2.16.84-3.39.84-2.48,0-4.66-1.24-5.97-3.13h-17.85c-1.31-1.9-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.13h15.96c.95,3.48,2.2,6.85,3.71,10.07h-5.63c-1.31-1.89-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25,0,1.53.48,2.95,1.28,4.12,1.31,1.9,3.49,3.14,5.97,3.14s4.66-1.24,5.97-3.14h36.95c-3.21-2.34-6.08-5.11-8.53-8.24ZM30.64,47.97c-2,0-3.62-1.62-3.62-3.62s1.62-3.62,3.62-3.62,3.63,1.62,3.63,3.62-1.63,3.62-3.63,3.62Zm-23.39,26.47c-2,0-3.62-1.63-3.62-3.62s1.63-3.63,3.62-3.63,3.63,1.63,3.63,3.63-1.63,3.62-3.63,3.62Zm20.97,16.45c-2,0-3.63-1.63-3.63-3.63s1.63-3.62,3.63-3.62,3.62,1.63,3.62,3.62-1.63,3.63-3.62,3.63Zm14.08,18.39c-2,0-3.62-1.62-3.62-3.62s1.62-3.62,3.62-3.62,3.62,1.62,3.62,3.62-1.62,3.62-3.62,3.62Z"></path>
                                               <circle class="cls-5a" cx="59.23" cy="62.54" r="3.63"></circle>
                                               <circle class="cls-7a" cx="58.24" cy="79" r="3.63" transform="translate(-16.54 15.44) rotate(-13.24)"></circle>
                                             </g>
                                             <g>
                                               <g>
                                                 <path class="cls-3a" d="m104.63,78.16c-9.44,6-14.52,2.86-15.25-9.4,4.93,3.38,10.02,6.52,15.25,9.4Z"></path>
                                                 <path class="cls-3a" d="m131.01,68.76c-.72,12.26-5.81,15.4-15.25,9.4,5.24-2.88,10.32-6.02,15.25-9.4Z"></path>
                                               </g>
                                               <path class="cls-4" d="m110.16,40.13c-.89,0-1.78.03-2.66.1-.08,0-.16,0-.24.02-9.59.79-18.07,5.45-23.89,12.42-.25.3-.49.59-.73.89-.14.19-.29.38-.43.57-4.02,5.37-6.52,11.93-6.88,19.07,0,.01,0,.03,0,.04-.01.22-.02.45-.03.68-.01.3-.02.6-.02.9v.18c0,.28,0,.55,0,.83.44,18.64,15.5,33.66,34.15,34.04.24,0,.48,0,.72,0,.3,0,.6,0,.9,0,18.85-.48,33.98-15.9,33.98-34.87s-15.61-34.88-34.88-34.88Zm.19,39.35c-14.41,14.37-29.15,1.23-29.15-9.45s13.05,0,29.15,0,29.15-10.67,29.15,0-14.74,23.81-29.15,9.45Z"></path>
                                             </g>
                                           </g>
                                           <g>
                                             <path class="cls-1a" d="m286.65,118.51h-17.37l-39.32-59.42v59.42h-17.37V31.8h17.37l39.32,59.54V31.8h17.37v86.71Z"></path>
                                             <path class="cls-1a" d="m304.95,38.69c-2.03-1.94-3.04-4.36-3.04-7.26s1.01-5.31,3.04-7.26c2.03-1.94,4.57-2.92,7.63-2.92s5.6.97,7.63,2.92c2.02,1.94,3.04,4.36,3.04,7.26s-1.02,5.31-3.04,7.26c-2.03,1.94-4.57,2.91-7.63,2.91s-5.6-.97-7.63-2.91Zm16.19,11.1v68.72h-17.37V49.79h17.37Z"></path>
                                             <path class="cls-1a" d="m396.18,56.55c5.04,5.17,7.57,12.38,7.57,21.65v40.31h-17.36v-37.96c0-5.46-1.37-9.65-4.1-12.59-2.73-2.94-6.45-4.4-11.16-4.4s-8.58,1.47-11.35,4.4c-2.77,2.94-4.15,7.13-4.15,12.59v37.96h-17.37V49.79h17.37v8.56c2.31-2.98,5.27-5.31,8.87-7.01,3.6-1.69,7.55-2.54,11.85-2.54,8.19,0,14.8,2.59,19.85,7.75Z"></path>
                                             <path class="cls-1a" d="m437.61,129.8c0,7.61-1.88,13.09-5.64,16.44-3.77,3.35-9.16,5.02-16.19,5.02h-7.69v-14.76h4.96c2.64,0,4.51-.52,5.58-1.55,1.07-1.03,1.61-2.71,1.61-5.02V49.79h17.37v80.01Zm-16.31-91.11c-2.03-1.94-3.04-4.36-3.04-7.26s1.01-5.31,3.04-7.26c2.02-1.94,4.61-2.92,7.75-2.92s5.58.97,7.57,2.92c1.98,1.94,2.98,4.36,2.98,7.26s-.99,5.31-2.98,7.26c-1.98,1.94-4.51,2.91-7.57,2.91s-5.73-.97-7.75-2.91Z"></path>
                                             <path class="cls-1a" d="m454.42,65.42c2.77-5.37,6.53-9.51,11.29-12.4,4.76-2.89,10.07-4.34,15.94-4.34,5.13,0,9.61,1.04,13.46,3.1,3.85,2.07,6.92,4.67,9.24,7.82v-9.8h17.49v68.72h-17.49v-10.05c-2.23,3.23-5.31,5.89-9.24,8-3.93,2.11-8.46,3.16-13.58,3.16-5.79,0-11.06-1.49-15.82-4.47-4.76-2.98-8.52-7.17-11.29-12.59-2.77-5.41-4.16-11.64-4.16-18.67s1.38-13.11,4.16-18.48Zm47.45,7.88c-1.65-3.02-3.89-5.33-6.7-6.95-2.81-1.61-5.83-2.42-9.06-2.42s-6.2.79-8.93,2.36c-2.73,1.57-4.94,3.87-6.64,6.88-1.69,3.02-2.54,6.6-2.54,10.73s.85,7.75,2.54,10.85c1.69,3.1,3.93,5.48,6.7,7.13,2.77,1.66,5.72,2.48,8.87,2.48s6.24-.81,9.06-2.42c2.81-1.61,5.04-3.93,6.7-6.95,1.65-3.02,2.48-6.64,2.48-10.85s-.83-7.83-2.48-10.85Z"></path>
                                             <path class="cls-4a" d="m591.93,102.01h-34.48l-5.71,16.5h-18.24l31.14-86.71h20.22l31.13,86.71h-18.36l-5.71-16.5Zm-4.71-13.89l-12.53-36.22-12.53,36.22h25.06Z"></path>
                                             <path class="cls-4a" d="m645.02,31.93v86.58h-17.37V31.93h17.37Z"></path>
                                           </g>
                                         </g>
                                       </g>
                                     </svg>
                            </div>
                            <div class="text-uppercase mt15 mt-md30 w600 f-md-28 f-22 text-center black-clr lh120">
                                Reseller 100 License
                            </div>
                        </div>
                        <div class="myfeatureslast f-md-25 f-16 w400 text-center lh160 hideme">
                            <a href="https://warriorplus.com/o2/buy/zh5gzv/pbsfwc/zfdqxh"><img src="https://warriorplus.com/o2/btn/fn200011000/zh5gzv/pbsfwc/357343" class="img-fluid mx-auto d-block"></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 mx-auto mt30 mt-md-90">
                    <div class="tablebox3">
                        <div class="tbbg3 text-center">
                            <div class="relative">
                                <!--<div class="most-popular">
                                    <div class="text-center f-md-22 f-18 w600 black-clr" editabletype="text" style="z-index:11"> Most Popular</div>
                                </div>-->
                                <div class="">
                                <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 645.02 151.26" style="max-height:70px">
                        <defs>
                          <style>
                            .cls-1 {
                              fill: url(#linear-gradient-2);
                            }
                      
                            .cls-2 {
                              fill: #fff;
                            }
                      
                            .cls-3 {
                              fill-rule: evenodd;
                            }
                      
                            .cls-3, .cls-4 {
                              fill: #ff813b;
                            }
                      
                            .cls-5 {
                              fill: url(#linear-gradient-3);
                            }
                      
                            .cls-6 {
                              fill: url(#linear-gradient);
                            }
                      
                            .cls-7 {
                              fill: url(#linear-gradient-4);
                            }
                          </style>
                          <linearGradient id="linear-gradient" x1="47.81" y1="75" x2="185.48" y2="75" gradientUnits="userSpaceOnUse">
                            <stop offset="0" stop-color="#b956ff"></stop>
                            <stop offset="1" stop-color="#6e33ff"></stop>
                          </linearGradient>
                          <linearGradient id="linear-gradient-2" x1="0" y1="75" x2="85.18" y2="75" gradientUnits="userSpaceOnUse">
                            <stop offset="0" stop-color="#b956ff"></stop>
                            <stop offset="1" stop-color="#a356fa"></stop>
                          </linearGradient>
                          <linearGradient id="linear-gradient-3" x1="55.61" y1="62.54" x2="62.86" y2="62.54" gradientUnits="userSpaceOnUse">
                            <stop offset="0" stop-color="#b956ff"></stop>
                            <stop offset="1" stop-color="#ac59fa"></stop>
                          </linearGradient>
                          <linearGradient id="linear-gradient-4" x1="54.62" y1="79" x2="61.87" y2="79" gradientTransform="translate(19.64 -11.24) rotate(13.24)" xlink:href="#linear-gradient-3"></linearGradient>
                        </defs>
                        <g id="Layer_1-2" data-name="Layer 1">
                          <g>
                            <g>
                              <g>
                                <path class="cls-6" d="m181.49,58.53h-10.72c-1.4-5.13-3.43-9.99-6.01-14.51l7.58-7.58c1.56-1.56,1.56-4.09,0-5.65l-17.65-17.65c-1.56-1.56-4.09-1.56-5.65,0l-7.58,7.58c-4.51-2.58-9.38-4.61-14.51-6.01V4c0-2.21-1.79-4-4-4h-24.96c-2.21,0-4,1.79-4,4v10.72c-.16.04-.31.09-.47.14-.27.08-.53.15-.8.23-.27.08-.55.16-.82.24-.55.17-1.09.35-1.63.53-.11.04-.22.08-.33.12-.48.17-.96.34-1.44.52-.12.04-.24.09-.36.14-.52.2-1.04.4-1.56.62-.04.02-.08.03-.12.05-2.41,1-4.74,2.15-6.99,3.43l-7.57-7.57c-1.56-1.56-4.09-1.56-5.65,0l-17.65,17.65c-.36.36-.63.76-.82,1.2h61.99v.02c.23,0,.45-.02.68-.02,23.76,0,43.01,19.26,43.01,43.01s-19.26,43.01-43.01,43.01c-.5,0-1-.02-1.5-.04v.04h-61.17c.19.43.46.84.82,1.2l17.65,17.65c1.56,1.56,4.09,1.56,5.65,0l7.57-7.57c2.25,1.28,4.58,2.43,6.99,3.43.04.02.08.03.12.05.51.21,1.03.42,1.55.62.12.05.24.09.36.14.47.18.95.35,1.43.52.11.04.23.08.34.12.54.18,1.08.36,1.63.53.27.08.55.16.83.25.26.08.53.16.79.23.16.04.31.09.47.14v10.72c0,2.21,1.79,4,4,4h24.96c2.21,0,4-1.79,4-4v-10.72c5.13-1.4,9.99-3.43,14.51-6.01l7.58,7.58c1.56,1.56,4.09,1.56,5.65,0l17.65-17.65c1.56-1.56,1.56-4.09,0-5.65l-7.58-7.58c2.58-4.51,4.61-9.38,6.01-14.51h10.72c2.21,0,4-1.79,4-4v-24.96c0-2.21-1.79-4-4-4Z"></path>
                                <path class="cls-1" d="m76.64,101.54c-5.74-7.31-9.18-16.52-9.18-26.54s3.44-19.23,9.18-26.54c2.45-3.13,5.33-5.9,8.53-8.24h-48.39c-1.31-1.89-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.13h17.07c-1.51,3.21-2.76,6.58-3.71,10.07h3.1c1.31-1.89,3.49-3.13,5.97-3.13,4.01,0,7.25,3.25,7.25,7.25s-3.22,7.23-7.21,7.25h-.04c-2.48,0-4.66-1.24-5.97-3.13H13.22c-.44-.64-.98-1.19-1.59-1.65-1.21-.92-2.73-1.48-4.38-1.48-4,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.14h38.82c1.31-1.89,3.49-3.13,5.97-3.13.43,0,.84.05,1.25.12,3.41.59,6,3.56,6,7.14,0,2.78-1.56,5.19-3.86,6.41-1.01.53-2.16.84-3.39.84-2.48,0-4.66-1.24-5.97-3.13h-17.85c-1.31-1.9-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.13h15.96c.95,3.48,2.2,6.85,3.71,10.07h-5.63c-1.31-1.89-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25,0,1.53.48,2.95,1.28,4.12,1.31,1.9,3.49,3.14,5.97,3.14s4.66-1.24,5.97-3.14h36.95c-3.21-2.34-6.08-5.11-8.53-8.24ZM30.64,47.97c-2,0-3.62-1.62-3.62-3.62s1.62-3.62,3.62-3.62,3.63,1.62,3.63,3.62-1.63,3.62-3.63,3.62Zm-23.39,26.47c-2,0-3.62-1.63-3.62-3.62s1.63-3.63,3.62-3.63,3.63,1.63,3.63,3.63-1.63,3.62-3.63,3.62Zm20.97,16.45c-2,0-3.63-1.63-3.63-3.63s1.63-3.62,3.63-3.62,3.62,1.63,3.62,3.62-1.63,3.63-3.62,3.63Zm14.08,18.39c-2,0-3.62-1.62-3.62-3.62s1.62-3.62,3.62-3.62,3.62,1.62,3.62,3.62-1.62,3.62-3.62,3.62Z"></path>
                                <circle class="cls-5" cx="59.23" cy="62.54" r="3.63"></circle>
                                <circle class="cls-7" cx="58.24" cy="79" r="3.63" transform="translate(-16.54 15.44) rotate(-13.24)"></circle>
                              </g>
                              <g>
                                <g>
                                  <path class="cls-3" d="m104.63,78.16c-9.44,6-14.52,2.86-15.25-9.4,4.93,3.38,10.02,6.52,15.25,9.4Z"></path>
                                  <path class="cls-3" d="m131.01,68.76c-.72,12.26-5.81,15.4-15.25,9.4,5.24-2.88,10.32-6.02,15.25-9.4Z"></path>
                                </g>
                                <path class="cls-4" d="m110.16,40.13c-.89,0-1.78.03-2.66.1-.08,0-.16,0-.24.02-9.59.79-18.07,5.45-23.89,12.42-.25.3-.49.59-.73.89-.14.19-.29.38-.43.57-4.02,5.37-6.52,11.93-6.88,19.07,0,.01,0,.03,0,.04-.01.22-.02.45-.03.68-.01.3-.02.6-.02.9v.18c0,.28,0,.55,0,.83.44,18.64,15.5,33.66,34.15,34.04.24,0,.48,0,.72,0,.3,0,.6,0,.9,0,18.85-.48,33.98-15.9,33.98-34.87s-15.61-34.88-34.88-34.88Zm.19,39.35c-14.41,14.37-29.15,1.23-29.15-9.45s13.05,0,29.15,0,29.15-10.67,29.15,0-14.74,23.81-29.15,9.45Z"></path>
                              </g>
                            </g>
                            <g>
                              <path class="cls-2" d="m286.65,118.51h-17.37l-39.32-59.42v59.42h-17.37V31.8h17.37l39.32,59.54V31.8h17.37v86.71Z"></path>
                              <path class="cls-2" d="m304.95,38.69c-2.03-1.94-3.04-4.36-3.04-7.26s1.01-5.31,3.04-7.26c2.03-1.94,4.57-2.92,7.63-2.92s5.6.97,7.63,2.92c2.02,1.94,3.04,4.36,3.04,7.26s-1.02,5.31-3.04,7.26c-2.03,1.94-4.57,2.91-7.63,2.91s-5.6-.97-7.63-2.91Zm16.19,11.1v68.72h-17.37V49.79h17.37Z"></path>
                              <path class="cls-2" d="m396.18,56.55c5.04,5.17,7.57,12.38,7.57,21.65v40.31h-17.36v-37.96c0-5.46-1.37-9.65-4.1-12.59-2.73-2.94-6.45-4.4-11.16-4.4s-8.58,1.47-11.35,4.4c-2.77,2.94-4.15,7.13-4.15,12.59v37.96h-17.37V49.79h17.37v8.56c2.31-2.98,5.27-5.31,8.87-7.01,3.6-1.69,7.55-2.54,11.85-2.54,8.19,0,14.8,2.59,19.85,7.75Z"></path>
                              <path class="cls-2" d="m437.61,129.8c0,7.61-1.88,13.09-5.64,16.44-3.77,3.35-9.16,5.02-16.19,5.02h-7.69v-14.76h4.96c2.64,0,4.51-.52,5.58-1.55,1.07-1.03,1.61-2.71,1.61-5.02V49.79h17.37v80.01Zm-16.31-91.11c-2.03-1.94-3.04-4.36-3.04-7.26s1.01-5.31,3.04-7.26c2.02-1.94,4.61-2.92,7.75-2.92s5.58.97,7.57,2.92c1.98,1.94,2.98,4.36,2.98,7.26s-.99,5.31-2.98,7.26c-1.98,1.94-4.51,2.91-7.57,2.91s-5.73-.97-7.75-2.91Z"></path>
                              <path class="cls-2" d="m454.42,65.42c2.77-5.37,6.53-9.51,11.29-12.4,4.76-2.89,10.07-4.34,15.94-4.34,5.13,0,9.61,1.04,13.46,3.1,3.85,2.07,6.92,4.67,9.24,7.82v-9.8h17.49v68.72h-17.49v-10.05c-2.23,3.23-5.31,5.89-9.24,8-3.93,2.11-8.46,3.16-13.58,3.16-5.79,0-11.06-1.49-15.82-4.47-4.76-2.98-8.52-7.17-11.29-12.59-2.77-5.41-4.16-11.64-4.16-18.67s1.38-13.11,4.16-18.48Zm47.45,7.88c-1.65-3.02-3.89-5.33-6.7-6.95-2.81-1.61-5.83-2.42-9.06-2.42s-6.2.79-8.93,2.36c-2.73,1.57-4.94,3.87-6.64,6.88-1.69,3.02-2.54,6.6-2.54,10.73s.85,7.75,2.54,10.85c1.69,3.1,3.93,5.48,6.7,7.13,2.77,1.66,5.72,2.48,8.87,2.48s6.24-.81,9.06-2.42c2.81-1.61,5.04-3.93,6.7-6.95,1.65-3.02,2.48-6.64,2.48-10.85s-.83-7.83-2.48-10.85Z"></path>
                              <path class="cls-4" d="m591.93,102.01h-34.48l-5.71,16.5h-18.24l31.14-86.71h20.22l31.13,86.71h-18.36l-5.71-16.5Zm-4.71-13.89l-12.53-36.22-12.53,36.22h25.06Z"></path>
                              <path class="cls-4" d="m645.02,31.93v86.58h-17.37V31.93h17.37Z"></path>
                            </g>
                          </g>
                        </g>
                      </svg>        <path class="cls-2" d="m286.65,118.51h-17.37l-39.32-59.42v59.42h-17.37V31.8h17.37l39.32,59.54V31.8h17.37v86.71Z"></path>
                              <path class="cls-2" d="m304.95,38.69c-2.03-1.94-3.04-4.36-3.04-7.26s1.01-5.31,3.04-7.26c2.03-1.94,4.57-2.92,7.63-2.92s5.6.97,7.63,2.92c2.02,1.94,3.04,4.36,3.04,7.26s-1.02,5.31-3.04,7.26c-2.03,1.94-4.57,2.91-7.63,2.91s-5.6-.97-7.63-2.91Zm16.19,11.1v68.72h-17.37V49.79h17.37Z"></path>
                              <path class="cls-2" d="m396.18,56.55c5.04,5.17,7.57,12.38,7.57,21.65v40.31h-17.36v-37.96c0-5.46-1.37-9.65-4.1-12.59-2.73-2.94-6.45-4.4-11.16-4.4s-8.58,1.47-11.35,4.4c-2.77,2.94-4.15,7.13-4.15,12.59v37.96h-17.37V49.79h17.37v8.56c2.31-2.98,5.27-5.31,8.87-7.01,3.6-1.69,7.55-2.54,11.85-2.54,8.19,0,14.8,2.59,19.85,7.75Z"></path>
                              <path class="cls-2" d="m437.61,129.8c0,7.61-1.88,13.09-5.64,16.44-3.77,3.35-9.16,5.02-16.19,5.02h-7.69v-14.76h4.96c2.64,0,4.51-.52,5.58-1.55,1.07-1.03,1.61-2.71,1.61-5.02V49.79h17.37v80.01Zm-16.31-91.11c-2.03-1.94-3.04-4.36-3.04-7.26s1.01-5.31,3.04-7.26c2.02-1.94,4.61-2.92,7.75-2.92s5.58.97,7.57,2.92c1.98,1.94,2.98,4.36,2.98,7.26s-.99,5.31-2.98,7.26c-1.98,1.94-4.51,2.91-7.57,2.91s-5.73-.97-7.75-2.91Z"></path>
                              <path class="cls-2" d="m454.42,65.42c2.77-5.37,6.53-9.51,11.29-12.4,4.76-2.89,10.07-4.34,15.94-4.34,5.13,0,9.61,1.04,13.46,3.1,3.85,2.07,6.92,4.67,9.24,7.82v-9.8h17.49v68.72h-17.49v-10.05c-2.23,3.23-5.31,5.89-9.24,8-3.93,2.11-8.46,3.16-13.58,3.16-5.79,0-11.06-1.49-15.82-4.47-4.76-2.98-8.52-7.17-11.29-12.59-2.77-5.41-4.16-11.64-4.16-18.67s1.38-13.11,4.16-18.48Zm47.45,7.88c-1.65-3.02-3.89-5.33-6.7-6.95-2.81-1.61-5.83-2.42-9.06-2.42s-6.2.79-8.93,2.36c-2.73,1.57-4.94,3.87-6.64,6.88-1.69,3.02-2.54,6.6-2.54,10.73s.85,7.75,2.54,10.85c1.69,3.1,3.93,5.48,6.7,7.13,2.77,1.66,5.72,2.48,8.87,2.48s6.24-.81,9.06-2.42c2.81-1.61,5.04-3.93,6.7-6.95,1.65-3.02,2.48-6.64,2.48-10.85s-.83-7.83-2.48-10.85Z"></path>
                              <path class="cls-4" d="m591.93,102.01h-34.48l-5.71,16.5h-18.24l31.14-86.71h20.22l31.13,86.71h-18.36l-5.71-16.5Zm-4.71-13.89l-12.53-36.22-12.53,36.22h25.06Z"></path>
                              <path class="cls-4" d="m645.02,31.93v86.58h-17.37V31.93h17.37Z"></path>
                            </g>
                          </g>
                        </g>
                      </svg>
                                </div>
                            </div>
                            <div class="text-uppercase mt15 mt-md30 w600 f-md-28 f-22 text-center white-clr lh120">
                            Reseller Unlimited License
                            </div>
                        </div>
                        <div class="myfeatureslast-com f-md-25 f-16 w400 text-center lh160 hideme">
                        <a href="https://warriorplus.com/o2/buy/zh5gzv/pbsfwc/jmpsmv"><img src="https://warriorplus.com/o2/btn/fn200011000/zh5gzv/pbsfwc/357344" class="img-fluid d-block mx-auto"></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 mt40 mt-md50 text-left thanks-button text-center">
                    <a class="kapblue f-18 f-md-22 lh140 w400 text-decoration-none" href="https://warriorplus.com/o/nothanks/pbsfwc" target="_blank">
                        No thanks - I don't want to sell NinjaAi and keep all benefits in my pocket without any grunt work. I know that NinjaAi Reseller Edition is a unique opportunity. Please take me to the next step to get access to my purchase.
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!--10. Table Section End -->

    <!--Footer Section Start -->
    <div class="footer-section">
         <div class="container ">
            <div class="row">
               <div class="col-12 text-center">
               <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 645.02 151.26" style="max-height:70px">
                              <defs>
                                <style>
                                  .cls-1 {
                                    fill: url(#linear-gradient-2);
                                  }
                            
                                  .cls-2 {
                                    fill: #fff;
                                  }
                            
                                  .cls-3 {
                                    fill-rule: evenodd;
                                  }
                            
                                  .cls-3, .cls-4 {
                                    fill: #ff813b;
                                  }
                            
                                  .cls-5 {
                                    fill: url(#linear-gradient-3);
                                  }
                            
                                  .cls-6 {
                                    fill: url(#linear-gradient);
                                  }
                            
                                  .cls-7 {
                                    fill: url(#linear-gradient-4);
                                  }
                                </style>
                                <linearGradient id="linear-gradient" x1="47.81" y1="75" x2="185.48" y2="75" gradientUnits="userSpaceOnUse">
                                  <stop offset="0" stop-color="#b956ff"></stop>
                                  <stop offset="1" stop-color="#6e33ff"></stop>
                                </linearGradient>
                                <linearGradient id="linear-gradient-2" x1="0" y1="75" x2="85.18" y2="75" gradientUnits="userSpaceOnUse">
                                  <stop offset="0" stop-color="#b956ff"></stop>
                                  <stop offset="1" stop-color="#a356fa"></stop>
                                </linearGradient>
                                <linearGradient id="linear-gradient-3" x1="55.61" y1="62.54" x2="62.86" y2="62.54" gradientUnits="userSpaceOnUse">
                                  <stop offset="0" stop-color="#b956ff"></stop>
                                  <stop offset="1" stop-color="#ac59fa"></stop>
                                </linearGradient>
                                <linearGradient id="linear-gradient-4" x1="54.62" y1="79" x2="61.87" y2="79" gradientTransform="translate(19.64 -11.24) rotate(13.24)" xlink:href="#linear-gradient-3"></linearGradient>
                              </defs>
                              <g id="Layer_1-2" data-name="Layer 1">
                                <g>
                                  <g>
                                    <g>
                                      <path class="cls-6" d="m181.49,58.53h-10.72c-1.4-5.13-3.43-9.99-6.01-14.51l7.58-7.58c1.56-1.56,1.56-4.09,0-5.65l-17.65-17.65c-1.56-1.56-4.09-1.56-5.65,0l-7.58,7.58c-4.51-2.58-9.38-4.61-14.51-6.01V4c0-2.21-1.79-4-4-4h-24.96c-2.21,0-4,1.79-4,4v10.72c-.16.04-.31.09-.47.14-.27.08-.53.15-.8.23-.27.08-.55.16-.82.24-.55.17-1.09.35-1.63.53-.11.04-.22.08-.33.12-.48.17-.96.34-1.44.52-.12.04-.24.09-.36.14-.52.2-1.04.4-1.56.62-.04.02-.08.03-.12.05-2.41,1-4.74,2.15-6.99,3.43l-7.57-7.57c-1.56-1.56-4.09-1.56-5.65,0l-17.65,17.65c-.36.36-.63.76-.82,1.2h61.99v.02c.23,0,.45-.02.68-.02,23.76,0,43.01,19.26,43.01,43.01s-19.26,43.01-43.01,43.01c-.5,0-1-.02-1.5-.04v.04h-61.17c.19.43.46.84.82,1.2l17.65,17.65c1.56,1.56,4.09,1.56,5.65,0l7.57-7.57c2.25,1.28,4.58,2.43,6.99,3.43.04.02.08.03.12.05.51.21,1.03.42,1.55.62.12.05.24.09.36.14.47.18.95.35,1.43.52.11.04.23.08.34.12.54.18,1.08.36,1.63.53.27.08.55.16.83.25.26.08.53.16.79.23.16.04.31.09.47.14v10.72c0,2.21,1.79,4,4,4h24.96c2.21,0,4-1.79,4-4v-10.72c5.13-1.4,9.99-3.43,14.51-6.01l7.58,7.58c1.56,1.56,4.09,1.56,5.65,0l17.65-17.65c1.56-1.56,1.56-4.09,0-5.65l-7.58-7.58c2.58-4.51,4.61-9.38,6.01-14.51h10.72c2.21,0,4-1.79,4-4v-24.96c0-2.21-1.79-4-4-4Z"></path>
                                      <path class="cls-1" d="m76.64,101.54c-5.74-7.31-9.18-16.52-9.18-26.54s3.44-19.23,9.18-26.54c2.45-3.13,5.33-5.9,8.53-8.24h-48.39c-1.31-1.89-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.13h17.07c-1.51,3.21-2.76,6.58-3.71,10.07h3.1c1.31-1.89,3.49-3.13,5.97-3.13,4.01,0,7.25,3.25,7.25,7.25s-3.22,7.23-7.21,7.25h-.04c-2.48,0-4.66-1.24-5.97-3.13H13.22c-.44-.64-.98-1.19-1.59-1.65-1.21-.92-2.73-1.48-4.38-1.48-4,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.14h38.82c1.31-1.89,3.49-3.13,5.97-3.13.43,0,.84.05,1.25.12,3.41.59,6,3.56,6,7.14,0,2.78-1.56,5.19-3.86,6.41-1.01.53-2.16.84-3.39.84-2.48,0-4.66-1.24-5.97-3.13h-17.85c-1.31-1.9-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.13h15.96c.95,3.48,2.2,6.85,3.71,10.07h-5.63c-1.31-1.89-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25,0,1.53.48,2.95,1.28,4.12,1.31,1.9,3.49,3.14,5.97,3.14s4.66-1.24,5.97-3.14h36.95c-3.21-2.34-6.08-5.11-8.53-8.24ZM30.64,47.97c-2,0-3.62-1.62-3.62-3.62s1.62-3.62,3.62-3.62,3.63,1.62,3.63,3.62-1.63,3.62-3.63,3.62Zm-23.39,26.47c-2,0-3.62-1.63-3.62-3.62s1.63-3.63,3.62-3.63,3.63,1.63,3.63,3.63-1.63,3.62-3.63,3.62Zm20.97,16.45c-2,0-3.63-1.63-3.63-3.63s1.63-3.62,3.63-3.62,3.62,1.63,3.62,3.62-1.63,3.63-3.62,3.63Zm14.08,18.39c-2,0-3.62-1.62-3.62-3.62s1.62-3.62,3.62-3.62,3.62,1.62,3.62,3.62-1.62,3.62-3.62,3.62Z"></path>
                                      <circle class="cls-5" cx="59.23" cy="62.54" r="3.63"></circle>
                                      <circle class="cls-7" cx="58.24" cy="79" r="3.63" transform="translate(-16.54 15.44) rotate(-13.24)"></circle>
                                    </g>
                                    <g>
                                      <g>
                                        <path class="cls-3" d="m104.63,78.16c-9.44,6-14.52,2.86-15.25-9.4,4.93,3.38,10.02,6.52,15.25,9.4Z"></path>
                                        <path class="cls-3" d="m131.01,68.76c-.72,12.26-5.81,15.4-15.25,9.4,5.24-2.88,10.32-6.02,15.25-9.4Z"></path>
                                      </g>
                                      <path class="cls-4" d="m110.16,40.13c-.89,0-1.78.03-2.66.1-.08,0-.16,0-.24.02-9.59.79-18.07,5.45-23.89,12.42-.25.3-.49.59-.73.89-.14.19-.29.38-.43.57-4.02,5.37-6.52,11.93-6.88,19.07,0,.01,0,.03,0,.04-.01.22-.02.45-.03.68-.01.3-.02.6-.02.9v.18c0,.28,0,.55,0,.83.44,18.64,15.5,33.66,34.15,34.04.24,0,.48,0,.72,0,.3,0,.6,0,.9,0,18.85-.48,33.98-15.9,33.98-34.87s-15.61-34.88-34.88-34.88Zm.19,39.35c-14.41,14.37-29.15,1.23-29.15-9.45s13.05,0,29.15,0,29.15-10.67,29.15,0-14.74,23.81-29.15,9.45Z"></path>
                                    </g>
                                  </g>
                                  <g>
                                    <path class="cls-2" d="m286.65,118.51h-17.37l-39.32-59.42v59.42h-17.37V31.8h17.37l39.32,59.54V31.8h17.37v86.71Z"></path>
                                    <path class="cls-2" d="m304.95,38.69c-2.03-1.94-3.04-4.36-3.04-7.26s1.01-5.31,3.04-7.26c2.03-1.94,4.57-2.92,7.63-2.92s5.6.97,7.63,2.92c2.02,1.94,3.04,4.36,3.04,7.26s-1.02,5.31-3.04,7.26c-2.03,1.94-4.57,2.91-7.63,2.91s-5.6-.97-7.63-2.91Zm16.19,11.1v68.72h-17.37V49.79h17.37Z"></path>
                                    <path class="cls-2" d="m396.18,56.55c5.04,5.17,7.57,12.38,7.57,21.65v40.31h-17.36v-37.96c0-5.46-1.37-9.65-4.1-12.59-2.73-2.94-6.45-4.4-11.16-4.4s-8.58,1.47-11.35,4.4c-2.77,2.94-4.15,7.13-4.15,12.59v37.96h-17.37V49.79h17.37v8.56c2.31-2.98,5.27-5.31,8.87-7.01,3.6-1.69,7.55-2.54,11.85-2.54,8.19,0,14.8,2.59,19.85,7.75Z"></path>
                                    <path class="cls-2" d="m437.61,129.8c0,7.61-1.88,13.09-5.64,16.44-3.77,3.35-9.16,5.02-16.19,5.02h-7.69v-14.76h4.96c2.64,0,4.51-.52,5.58-1.55,1.07-1.03,1.61-2.71,1.61-5.02V49.79h17.37v80.01Zm-16.31-91.11c-2.03-1.94-3.04-4.36-3.04-7.26s1.01-5.31,3.04-7.26c2.02-1.94,4.61-2.92,7.75-2.92s5.58.97,7.57,2.92c1.98,1.94,2.98,4.36,2.98,7.26s-.99,5.31-2.98,7.26c-1.98,1.94-4.51,2.91-7.57,2.91s-5.73-.97-7.75-2.91Z"></path>
                                    <path class="cls-2" d="m454.42,65.42c2.77-5.37,6.53-9.51,11.29-12.4,4.76-2.89,10.07-4.34,15.94-4.34,5.13,0,9.61,1.04,13.46,3.1,3.85,2.07,6.92,4.67,9.24,7.82v-9.8h17.49v68.72h-17.49v-10.05c-2.23,3.23-5.31,5.89-9.24,8-3.93,2.11-8.46,3.16-13.58,3.16-5.79,0-11.06-1.49-15.82-4.47-4.76-2.98-8.52-7.17-11.29-12.59-2.77-5.41-4.16-11.64-4.16-18.67s1.38-13.11,4.16-18.48Zm47.45,7.88c-1.65-3.02-3.89-5.33-6.7-6.95-2.81-1.61-5.83-2.42-9.06-2.42s-6.2.79-8.93,2.36c-2.73,1.57-4.94,3.87-6.64,6.88-1.69,3.02-2.54,6.6-2.54,10.73s.85,7.75,2.54,10.85c1.69,3.1,3.93,5.48,6.7,7.13,2.77,1.66,5.72,2.48,8.87,2.48s6.24-.81,9.06-2.42c2.81-1.61,5.04-3.93,6.7-6.95,1.65-3.02,2.48-6.64,2.48-10.85s-.83-7.83-2.48-10.85Z"></path>
                                    <path class="cls-4" d="m591.93,102.01h-34.48l-5.71,16.5h-18.24l31.14-86.71h20.22l31.13,86.71h-18.36l-5.71-16.5Zm-4.71-13.89l-12.53-36.22-12.53,36.22h25.06Z"></path>
                                    <path class="cls-4" d="m645.02,31.93v86.58h-17.37V31.93h17.37Z"></path>
                                  </g>
                                </g>
                              </g>
                            </svg>
                  <div editabletype="text" class="f-14 f-md-16 w400 mt20 lh160 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
                  <div class=" mt20 mt-md40 white-clr"> <script type="text/javascript" src="https://warriorplus.com/o2/disclaimer/zh5gzv" defer=""></script><div class="wplus_spdisclaimer f-17 w300 mt20 lh140 white-clr text-center"><strong>Disclaimer:</strong> <em>WarriorPlus is used to help manage the sale of products on this site. While WarriorPlus helps facilitate the sale, all payments are made directly to the product vendor and NOT WarriorPlus. Thus, all product questions, support inquiries and/or refund requests must be sent to the vendor. WarriorPlus's role should not be construed as an endorsement, approval or review of these products or any claim, statement or opinion used in the marketing of these products.</em></div></div>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-14 f-md-16 w400 lh160 white-clr text-xs-center">Copyright © Ninja Ai</div>
                  <ul class="footer-ul w400 f-14 f-md-16 white-clr text-center text-md-right">
                     <li><a href="https://support.bizomart.com/hc/en-us" class="white-clr  t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://getninjaai.com/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://getninjaai.com/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://getninjaai.com/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://getninjaai.com/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://getninjaai.com/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://getninjaai.com/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
    <!--Footer Section End -->
  <!-- New Timer  Start-->
<!-- timer --->
<!-- Exit Popup -->	
<style>
   </style>
<script type="text/javascript" src="assets/js/ouibounce.min.js"></script>
<div id="ouibounce-modal" style="z-index:9999999; display:none;">
   <div class="underlay"></div>
   <div class="modal-wrapper" style="display:block;">
      <div class="modal-bg">
		<button type="button" class="close" onclick="document.getElementById('ouibounce-modal').style.display = 'none';">
			X 
		</button>
		<div class="model-header">
      <div class="d-flex justify-content-center align-items-center gap-3">
            <img src="assets/images/hold.png" alt="" class="img-fluid d-block m131">
            <div>
               <div class="f-md-56 f-24 w700 lh130 white-clr">
                  WAIT! HOLD ON!!
               </div>
               <div class="f-md-28 f-18 w700 lh130 black-clr mt10 mt-md0" style="background-color:yellow; padding:5px 10px; display:inline-block">
                  Don't Leave Empty Handed
               </div>
            </div>
         </div>
		</div>
         <div class="col-12 for-padding">
			<div class="copun-line f-md-45 f-16 w500 lh130 text-center black-clr mt10">
         You've Qualified For An <span class="w700 red-clr"><br> INSTANT $200 DISCOUNT</span>  
			</div>
         <div class="copun-line f-md-30 f-16 w700 lh130 text-center black-clr mt10" style="background-color:yellow; padding:5px 10px; display:inline-block">
         Regular Price <strike>$397</strike>,
         Now Only $197	
			</div>
			 <div class="mt-md20 mt10 text-center">
				<a href="https://warriorplus.com/o2/buy/zh5gzv/pbsfwc/m8vq9f" class="cta-link-btn1">Grab SPECIAL Discount Now!
				<br>
				<span class="f-12 black-clr w600 text-uppercase">Act Now! Limited to First 100 Buyers Only.</span> </a>
			 </div>
			<!-- <div class="mt-md20 mt10 text-center f-md-26 f-16 w900 lh150 text-center red-clr text-uppercase">
				Coupon Code EXPIRES IN:
			</div>
			<div class="timer-new">
            <div class="days" class="timer-label">
               <span id="days"></span> <span class="text">Days</span>
            </div> 
            <div class="hours" class="timer-label">
               <span id="hours"></span> <span  class="text">Hours</span>
            </div>
            <div class="days" class="timer-label">
               <span id="mins" ></span> <span class="text">Min</span>
            </div>
            <div class="secs" class="timer-label">
               <span id="secs" ></span> <span class="text">Sec</span>
            </div>
         </div> -->
      </div>
   </div>
</div>
<script type="text/javascript">
      var timeInSecs;
      var ticker;
      function startTimer(secs) {
      timeInSecs = parseInt(secs);
	  tick();
      //ticker = setInterval("tick()", 1000);
      }
      function tick() {
      var secs = timeInSecs;
      if (secs > 0) {
      timeInSecs--;
      } else {
      clearInterval(ticker);
      //startTimer(20 * 60); // 4 minutes in seconds
      }
      var days = Math.floor(secs / 86400);
      secs %= 86400;
      var hours = Math.floor(secs / 3600);
      secs %= 3600;
      var mins = Math.floor(secs / 60);
      secs %= 60;
      var pretty = ((days < 10) ? "0" : "") + days + ":" + ((hours < 10) ? "0" : "") + hours + ":" + ((mins < 10) ? "0" : "") + mins + ":" + ((secs < 10) ? "0" : "") + secs;
      document.getElementById("days").innerHTML = days
      document.getElementById("hours").innerHTML = hours
      document.getElementById("mins").innerHTML = mins
      document.getElementById("secs").innerHTML = secs;
      }
      
	  //startTimer(60 * 60); 
      // 4 minutes in seconds
	  
	  function getCookie(cname) {
            var name = cname + "=";
            var decodedCookie = decodeURIComponent(document.cookie);
            var ca = decodedCookie.split(';');
            for(var i = 0; i <ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        }


      var cnt = 60*60;

      function counter(){
       
         if(getCookie("cnt") > 0){
            cnt = getCookie("cnt");
         }

         cnt -= 1;
         document.cookie = "cnt="+ cnt;

         startTimer(getCookie("cnt"));
         //jQuery("#counter").val(getCookie("cnt"));

         if(cnt>0){
            setTimeout(counter,1000);
         }
      
      }
      
      counter();
	  
	  
      </script>-
      <script type="text/javascript">
         var _ouibounce = ouibounce(document.getElementById('ouibounce-modal'),{
         aggressive: true, //Making this true makes ouibounce not to obey "once per visitor" rule
         });
      </script>
      <!-- Exit Popup Ends-->
            <!--- timer end-->
   <!-- Exit Popup and Timer End -->
</body>
</html>
<script> 
function myFunction() {
  var checkBox = document.getElementById("check");
  var buybutton = document.getElementById("buyButton");
  if (checkBox.checked == true){
 
	    buybutton.getAttribute("href");
	    buybutton.setAttribute("href","https://warriorplus.com/o2/buy/zh5gzv/pbsfwc/m8vq9f");
	} else {
		 buybutton.getAttribute("href");
		 buybutton.setAttribute("href","https://warriorplus.com/o2/buy/zh5gzv/pbsfwc/m8vq9f");
	  }
}
</script>
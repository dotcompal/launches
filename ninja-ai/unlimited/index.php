<!Doctype html>
<html>
   <head>
      <title>NinjaAi | Unlimited</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=9">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
      <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&family=Poppins:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
      <meta name="description" content="World's First Google Killer Ninja AI App Creates DFY Affiliate Funnel Sites with DFY Content, Reviews & Lead Magnet in Just 60 seconds...">
      <meta property="og:image" content="https://www.getninjaai.com/unlimited/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Pranshu Gupta">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="NinjaAi | Unlimited">
      <meta property="og:description" content="World's First Google Killer Ninja AI App Creates DFY Affiliate Funnel Sites with DFY Content, Reviews & Lead Magnet in Just 60 seconds...">
      <meta property="og:image" content="https://www.getninjaai.com/unlimited/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="NinjaAi | Unlimited">
      <meta property="twitter:description" content="World's First Google Killer Ninja AI App Creates DFY Affiliate Funnel Sites with DFY Content, Reviews & Lead Magnet in Just 60 seconds...">
      <meta property="twitter:image" content="https://www.getninjaai.com/unlimited/thumbnail.png">
      <!-- Start Editor required -->
      <link rel="stylesheet" href="../common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" href="../common_assets/css/general.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style.css" type="text/css">
      <link rel="stylesheet" href="assets/css/timer.css" type="text/css">
      <script src="https://cdn.oppyotest.com/launches/sellero/common_assets/js/bootstrap.min.js"></script>
      <script src="https://cdn.oppyotest.com/launches/sellero/common_assets/js/jquery.min.js"></script>
   </head>
   <body>
      <div class="warning-section">
         <div class="container">
             <div class="row">
                 <div class="col-12 text-center">
                     <div class="offer">
                         <div>
                             <img src="https://cdn.oppyo.com/launches/appzilo/special/error.webp">
                         </div>
                         <div class="f-22 f-md-30 w500 lh140 white-clr text-center">
                             <span class="w600">Warning:</span> This offer will expire automatically once you leave the page
                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </div>
      
            <!---Legal Practice Area---->
            <!--1. Header Section Start -->
            <div class="space-section header-section">
               <div class="container">
                  <div class="row inner-content">
                     <div class="col-12 text-center">
                           <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 645.02 151.26" style="max-height:70px">
                              <defs>
                                <style>
                                  .cls-1 {
                                    fill: url(#linear-gradient-2);
                                  }
                            
                                  .cls-2 {
                                    fill: #fff;
                                  }
                            
                                  .cls-3 {
                                    fill-rule: evenodd;
                                  }
                            
                                  .cls-3, .cls-4 {
                                    fill: #ff813b;
                                  }
                            
                                  .cls-5 {
                                    fill: url(#linear-gradient-3);
                                  }
                            
                                  .cls-6 {
                                    fill: url(#linear-gradient);
                                  }
                            
                                  .cls-7 {
                                    fill: url(#linear-gradient-4);
                                  }
                                </style>
                                <linearGradient id="linear-gradient" x1="47.81" y1="75" x2="185.48" y2="75" gradientUnits="userSpaceOnUse">
                                  <stop offset="0" stop-color="#b956ff"></stop>
                                  <stop offset="1" stop-color="#6e33ff"></stop>
                                </linearGradient>
                                <linearGradient id="linear-gradient-2" x1="0" y1="75" x2="85.18" y2="75" gradientUnits="userSpaceOnUse">
                                  <stop offset="0" stop-color="#b956ff"></stop>
                                  <stop offset="1" stop-color="#a356fa"></stop>
                                </linearGradient>
                                <linearGradient id="linear-gradient-3" x1="55.61" y1="62.54" x2="62.86" y2="62.54" gradientUnits="userSpaceOnUse">
                                  <stop offset="0" stop-color="#b956ff"></stop>
                                  <stop offset="1" stop-color="#ac59fa"></stop>
                                </linearGradient>
                                <linearGradient id="linear-gradient-4" x1="54.62" y1="79" x2="61.87" y2="79" gradientTransform="translate(19.64 -11.24) rotate(13.24)" xlink:href="#linear-gradient-3"></linearGradient>
                              </defs>
                              <g id="Layer_1-2" data-name="Layer 1">
                                <g>
                                  <g>
                                    <g>
                                      <path class="cls-6" d="m181.49,58.53h-10.72c-1.4-5.13-3.43-9.99-6.01-14.51l7.58-7.58c1.56-1.56,1.56-4.09,0-5.65l-17.65-17.65c-1.56-1.56-4.09-1.56-5.65,0l-7.58,7.58c-4.51-2.58-9.38-4.61-14.51-6.01V4c0-2.21-1.79-4-4-4h-24.96c-2.21,0-4,1.79-4,4v10.72c-.16.04-.31.09-.47.14-.27.08-.53.15-.8.23-.27.08-.55.16-.82.24-.55.17-1.09.35-1.63.53-.11.04-.22.08-.33.12-.48.17-.96.34-1.44.52-.12.04-.24.09-.36.14-.52.2-1.04.4-1.56.62-.04.02-.08.03-.12.05-2.41,1-4.74,2.15-6.99,3.43l-7.57-7.57c-1.56-1.56-4.09-1.56-5.65,0l-17.65,17.65c-.36.36-.63.76-.82,1.2h61.99v.02c.23,0,.45-.02.68-.02,23.76,0,43.01,19.26,43.01,43.01s-19.26,43.01-43.01,43.01c-.5,0-1-.02-1.5-.04v.04h-61.17c.19.43.46.84.82,1.2l17.65,17.65c1.56,1.56,4.09,1.56,5.65,0l7.57-7.57c2.25,1.28,4.58,2.43,6.99,3.43.04.02.08.03.12.05.51.21,1.03.42,1.55.62.12.05.24.09.36.14.47.18.95.35,1.43.52.11.04.23.08.34.12.54.18,1.08.36,1.63.53.27.08.55.16.83.25.26.08.53.16.79.23.16.04.31.09.47.14v10.72c0,2.21,1.79,4,4,4h24.96c2.21,0,4-1.79,4-4v-10.72c5.13-1.4,9.99-3.43,14.51-6.01l7.58,7.58c1.56,1.56,4.09,1.56,5.65,0l17.65-17.65c1.56-1.56,1.56-4.09,0-5.65l-7.58-7.58c2.58-4.51,4.61-9.38,6.01-14.51h10.72c2.21,0,4-1.79,4-4v-24.96c0-2.21-1.79-4-4-4Z"></path>
                                      <path class="cls-1" d="m76.64,101.54c-5.74-7.31-9.18-16.52-9.18-26.54s3.44-19.23,9.18-26.54c2.45-3.13,5.33-5.9,8.53-8.24h-48.39c-1.31-1.89-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.13h17.07c-1.51,3.21-2.76,6.58-3.71,10.07h3.1c1.31-1.89,3.49-3.13,5.97-3.13,4.01,0,7.25,3.25,7.25,7.25s-3.22,7.23-7.21,7.25h-.04c-2.48,0-4.66-1.24-5.97-3.13H13.22c-.44-.64-.98-1.19-1.59-1.65-1.21-.92-2.73-1.48-4.38-1.48-4,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.14h38.82c1.31-1.89,3.49-3.13,5.97-3.13.43,0,.84.05,1.25.12,3.41.59,6,3.56,6,7.14,0,2.78-1.56,5.19-3.86,6.41-1.01.53-2.16.84-3.39.84-2.48,0-4.66-1.24-5.97-3.13h-17.85c-1.31-1.9-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.13h15.96c.95,3.48,2.2,6.85,3.71,10.07h-5.63c-1.31-1.89-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25,0,1.53.48,2.95,1.28,4.12,1.31,1.9,3.49,3.14,5.97,3.14s4.66-1.24,5.97-3.14h36.95c-3.21-2.34-6.08-5.11-8.53-8.24ZM30.64,47.97c-2,0-3.62-1.62-3.62-3.62s1.62-3.62,3.62-3.62,3.63,1.62,3.63,3.62-1.63,3.62-3.63,3.62Zm-23.39,26.47c-2,0-3.62-1.63-3.62-3.62s1.63-3.63,3.62-3.63,3.63,1.63,3.63,3.63-1.63,3.62-3.63,3.62Zm20.97,16.45c-2,0-3.63-1.63-3.63-3.63s1.63-3.62,3.63-3.62,3.62,1.63,3.62,3.62-1.63,3.63-3.62,3.63Zm14.08,18.39c-2,0-3.62-1.62-3.62-3.62s1.62-3.62,3.62-3.62,3.62,1.62,3.62,3.62-1.62,3.62-3.62,3.62Z"></path>
                                      <circle class="cls-5" cx="59.23" cy="62.54" r="3.63"></circle>
                                      <circle class="cls-7" cx="58.24" cy="79" r="3.63" transform="translate(-16.54 15.44) rotate(-13.24)"></circle>
                                    </g>
                                    <g>
                                      <g>
                                        <path class="cls-3" d="m104.63,78.16c-9.44,6-14.52,2.86-15.25-9.4,4.93,3.38,10.02,6.52,15.25,9.4Z"></path>
                                        <path class="cls-3" d="m131.01,68.76c-.72,12.26-5.81,15.4-15.25,9.4,5.24-2.88,10.32-6.02,15.25-9.4Z"></path>
                                      </g>
                                      <path class="cls-4" d="m110.16,40.13c-.89,0-1.78.03-2.66.1-.08,0-.16,0-.24.02-9.59.79-18.07,5.45-23.89,12.42-.25.3-.49.59-.73.89-.14.19-.29.38-.43.57-4.02,5.37-6.52,11.93-6.88,19.07,0,.01,0,.03,0,.04-.01.22-.02.45-.03.68-.01.3-.02.6-.02.9v.18c0,.28,0,.55,0,.83.44,18.64,15.5,33.66,34.15,34.04.24,0,.48,0,.72,0,.3,0,.6,0,.9,0,18.85-.48,33.98-15.9,33.98-34.87s-15.61-34.88-34.88-34.88Zm.19,39.35c-14.41,14.37-29.15,1.23-29.15-9.45s13.05,0,29.15,0,29.15-10.67,29.15,0-14.74,23.81-29.15,9.45Z"></path>
                                    </g>
                                  </g>
                                  <g>
                                    <path class="cls-2" d="m286.65,118.51h-17.37l-39.32-59.42v59.42h-17.37V31.8h17.37l39.32,59.54V31.8h17.37v86.71Z"></path>
                                    <path class="cls-2" d="m304.95,38.69c-2.03-1.94-3.04-4.36-3.04-7.26s1.01-5.31,3.04-7.26c2.03-1.94,4.57-2.92,7.63-2.92s5.6.97,7.63,2.92c2.02,1.94,3.04,4.36,3.04,7.26s-1.02,5.31-3.04,7.26c-2.03,1.94-4.57,2.91-7.63,2.91s-5.6-.97-7.63-2.91Zm16.19,11.1v68.72h-17.37V49.79h17.37Z"></path>
                                    <path class="cls-2" d="m396.18,56.55c5.04,5.17,7.57,12.38,7.57,21.65v40.31h-17.36v-37.96c0-5.46-1.37-9.65-4.1-12.59-2.73-2.94-6.45-4.4-11.16-4.4s-8.58,1.47-11.35,4.4c-2.77,2.94-4.15,7.13-4.15,12.59v37.96h-17.37V49.79h17.37v8.56c2.31-2.98,5.27-5.31,8.87-7.01,3.6-1.69,7.55-2.54,11.85-2.54,8.19,0,14.8,2.59,19.85,7.75Z"></path>
                                    <path class="cls-2" d="m437.61,129.8c0,7.61-1.88,13.09-5.64,16.44-3.77,3.35-9.16,5.02-16.19,5.02h-7.69v-14.76h4.96c2.64,0,4.51-.52,5.58-1.55,1.07-1.03,1.61-2.71,1.61-5.02V49.79h17.37v80.01Zm-16.31-91.11c-2.03-1.94-3.04-4.36-3.04-7.26s1.01-5.31,3.04-7.26c2.02-1.94,4.61-2.92,7.75-2.92s5.58.97,7.57,2.92c1.98,1.94,2.98,4.36,2.98,7.26s-.99,5.31-2.98,7.26c-1.98,1.94-4.51,2.91-7.57,2.91s-5.73-.97-7.75-2.91Z"></path>
                                    <path class="cls-2" d="m454.42,65.42c2.77-5.37,6.53-9.51,11.29-12.4,4.76-2.89,10.07-4.34,15.94-4.34,5.13,0,9.61,1.04,13.46,3.1,3.85,2.07,6.92,4.67,9.24,7.82v-9.8h17.49v68.72h-17.49v-10.05c-2.23,3.23-5.31,5.89-9.24,8-3.93,2.11-8.46,3.16-13.58,3.16-5.79,0-11.06-1.49-15.82-4.47-4.76-2.98-8.52-7.17-11.29-12.59-2.77-5.41-4.16-11.64-4.16-18.67s1.38-13.11,4.16-18.48Zm47.45,7.88c-1.65-3.02-3.89-5.33-6.7-6.95-2.81-1.61-5.83-2.42-9.06-2.42s-6.2.79-8.93,2.36c-2.73,1.57-4.94,3.87-6.64,6.88-1.69,3.02-2.54,6.6-2.54,10.73s.85,7.75,2.54,10.85c1.69,3.1,3.93,5.48,6.7,7.13,2.77,1.66,5.72,2.48,8.87,2.48s6.24-.81,9.06-2.42c2.81-1.61,5.04-3.93,6.7-6.95,1.65-3.02,2.48-6.64,2.48-10.85s-.83-7.83-2.48-10.85Z"></path>
                                    <path class="cls-4" d="m591.93,102.01h-34.48l-5.71,16.5h-18.24l31.14-86.71h20.22l31.13,86.71h-18.36l-5.71-16.5Zm-4.71-13.89l-12.53-36.22-12.53,36.22h25.06Z"></path>
                                    <path class="cls-4" d="m645.02,31.93v86.58h-17.37V31.93h17.37Z"></path>
                                  </g>
                                </g>
                              </g>
                            </svg>
                        </div>
                        <div class="col-12 mt20 mt-md50 text-center">
                           <div class="pre-heading f-18 f-md-22 w500 lh140 white-clr">
                              Congratulations! Your order is almost complete. But before you get started... 
                           </div>
                        </div>
                        <div class="col-12 mt-md30 mt15" >
                           <div class="f-md-45 f-28 w400 text-center white-clr lh140">
                              Unlock Unlimited Potential to Get <span class="w700 orange-clr">5X More Traffic, Leads & Commissions Faster & Easier</span>  Using This Ninja Ai Unlimited Upgrade
                           </div>
                        </div>
                        <div class="col-12">
                           <div class="f-md-24 f-20 lh140 w400 text-center white-clr mt-md30 mt20">					  
                              Unlock All Features | Go Unlimited I 5X Your Profits Instantly
                           </div>
                        </div>
                     </div>
                     <div class="col-12 col-md-10 mx-auto mt-md30 mt20 ">
                        <img src="assets/images/productbox.webp" class="mx-auto d-block img-fluid" width="850">
                        <!-- <div class="responsive-video" editabletype="video" style="z-index: 10;">
                          <iframe src="https://ninjakash.dotcompal.com/video/embed/k1618v15v2" style=" position: absolute;top: 0;left: 0;width: 100%;    height: 100%; background: transparent !important;
                           box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                        </div> -->
                     </div>
                     <div class="col-md-10 mx-auto col-12  mt30 mt-md50">
                        <div class="f-20 f-md-35 text-center probtn1" editabletype="button" style="z-index: 10;">
                           <a href="#buynow" class="text-center">Get Instant Access to Ninja Ai Unlimited</a>
                        </div>
                     </div>
                     <div class="col-12 mt15 mt-md20 ">
                        <div class="f-md-22 f-18 lh140 w400 text-center white-clr">
                           This is An Exclusive Deal for <span class="w500">"Ninja Ai"</span> Users Only...
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!--1. Header Section End -->
            <!--2. Second Section Start -->
            <div class="space-section second-sec">
               <div class="container ">
                  <div class="row inner-content">
                     <div class="col-12 text-center text-capitalize">
                        <div class="f-20 f-md-28 text-center w400 mb-md20 mb0 lh140">
                           You Are About To Witness The RAW Power Of Ninja Ai In A Way <br class="visible-lg"> You Have Never Seen It.
                        </div>
                     </div>
                     <div class="col-12 mt15 mt-md15">
                        <div class="f-md-50 f-28 w500 lh140 text-capitalize text-center black-clr">
                           A Whopping 92% Of The <span class="w700 orange-clr">Ninja Ai Members </span> Upgraded To Ninja Ai Unlimited... Here's Why
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-6 col-12 mt30 mt-sm70 px0 px-md15">
                           <div class="row">
                              <div class="col-md-2 col-12 px5">
                                 <div>
                                    <img src="assets/images/fe1.webp" class="mx-auto d-block img-fluid" width="70">
                                 </div>
                              </div>
                              <div class="col-md-10 col-12 mt20 mt-sm0">
                                 <div class="f-20 f-md-22 lh140 w500 xs-text-center">
                                    Create Unlimited Businesses/Sub-Domains For Promoting Unlimited Products to 5X Profits
                                 </div>
                              </div>
                           </div>
                           <div class="row mt25 mt-md50">
                              <div class="col-md-2 col-12 px5">
                                 <div><img src="assets/images/fe2.webp" class="mx-auto d-block img-fluid" width="70"></div>
                              </div>
                              <div class="col-md-10 col-12 mt20 mt-sm0">
                                 <div class="f-20 f-md-22 lh140 w500 xs-text-center">
                                    Get Done-for-You And Premium Lead Funnel Templates to DOUBLE Your Leads 
                                 </div>
                              </div>
                           </div>
                           <!--14-->
                           <div class="row mt15 mt-sm45">
                              <div class="col-md-2 col-12 px5 mt0 mt-md15">
                                 <div><img src="assets/images/fe3.webp" class="mx-auto d-block img-fluid" width="70"></div>
                              </div>
                              <div class="col-md-10 col-12 mt0 mt-md15">
                                 <div class="f-20 f-md-22 lh140 w500 xs-text-center">
                                    Get 100 Researched Affiliates Products To Make Recurring Affiliate Commissions And Sales
                                 </div>
                              </div>
                           </div>
                           <!--14-->
                           <!--14-->
                           <div class="row mt15 mt-sm40">
                              <div class="col-md-2 col-12 px5 mt0 mt-md15 mx-auto">
                                 <div><img src="assets/images/fe4.webp" class="mx-auto d-block img-fluid" width="70"></div>
                              </div>
                              <div class="col-md-10 col-12 mt0 mt-md15">
                                 <div class="f-20 f-md-22 lh140 w500 xs-text-center">
                                    Get 20+ Extra Attractive Promotion Funnel Templates
                                 </div>
                              </div>
                           </div>
                           
                           <div class="row mt15 mt-sm40">
                              <div class="col-md-2 col-12 px5 mt0 mt-md15 mx-auto">
                                 <div><img src="assets/images/chatbot.webp" class="mx-auto d-block img-fluid" width="70"></div>
                              </div>
                              <div class="col-md-10 col-12 mt0 mt-md15">
                                 <div class="f-20 f-md-22 lh140 w500 xs-text-center">
                                    AI ChatBot to Convert More Visitors for You - More Commissions on Your Way
                                 </div>
                              </div>
                           </div>
                           <!--14-->
                           <!--14-->
                           <!--14-->
                        </div>
                        <div class="col-md-6 col-12 mt30 mt-sm70 px0 px-md15">
                           <div class="row ">
                              <div class="col-md-2 col-12 px5 mx-auto">
                                 <div><img src="assets/images/fe5.webp" class="mx-auto d-block img-fluid" width="70"></div>
                              </div>
                              <div class="col-md-10 col-12 mt0 mt-md15">
                                 <div class="f-20 f-md-22 lh140 w500 xs-text-center">
                                    Maximize ROI & Sales From Your Leads With Automated Webinar Integration
                                 </div>
                              </div>
                           </div>
                           <div class="row mt15 mt-sm40">
                              <div class="col-md-2  col-12 px5">
                                 <div><img src="assets/images/fe6.webp" class="mx-auto d-block img-fluid" width="70"></div>
                              </div>
                              <div class="col-md-10  col-12 mt20 mt-sm0">
                                 <div class="f-20 f-md-22 lh140 w500 xs-text-center">
                                    Make Funnels More Attractive By Adding Images From <span class="w600"> AI Media Library </span>
                                 </div>
                              </div>
                           </div>
                           <!--14-->
                           <!--11-->
                           <div class="row mt25 mt-md65">
                              <div class="col-md-2 col-12 px5 mx-auto">
                                 <div><img src="assets/images/fe8.webp" class="mx-auto d-block img-fluid" width="70"></div>
                              </div>
                              <div class="col-md-10  col-12 mt20 mt-sm0">
                                 <div class="f-20 f-md-22 lh140 w500 xs-text-center pr0">
                                    Share Control Of Your Dashboard To Up To 5 Team Members
                                 </div>
                              </div>
                           </div>
                           <!--14-->
                           <div class="row mt15 mt-sm45">
                              <div class="col-md-2 col-12 px5 mt0 mt-md15 mx-auto">
                                 <div><img src="assets/images/fe10.webp" class="mx-auto d-block img-fluid" width="70"></div>
                              </div>
                              <div class="col-md-10 col-12 mt0 mt-md15">
                                 <div class="f-20 f-md-22 lh140 w500 xs-text-center">
                                    Get All These Benefits At An Unparalleled Price
                                 </div>
                              </div>
                           </div>
                           <!--14-->
                        </div>
                     </div>
                     <div class="col-md-10 col-12 mt10 mt-md60 mx-auto">
                        <div class="f-22 f-md-36 text-center probtn" editabletype="button" style="z-index: 10;"><a href="#buynow" class="text-center">Get Instant Access to Ninja Ai Unlimited</a></div>
                        <br>
                     </div>
                  </div>
               </div>
            </div>
            <!--2. Second Section End -->
            <!--3. Third Section Start -->
            <div class="space-section third-section">
               <div class="container ">
                  <div class="row inner-content">
                     <div class="col-12 col-md-12">
                        <div class="f-27 f-md-45 lh140 w500 text-center mb20 black-clr">
                           This Is Your Chance to <span class="w700">Get Unfair Advantage </span> Over Your Competition
                        </div>
                        <div class="f-20 f-md-22 w400 mt20 mt-sm35 lh160 text-center">
                           If you are on this page, you have already realized the power of this revolutionary technology and secured a leap from the competitors. If a basic feature that everyone is getting can bring such massive results… <span class="w600">what more this unlimited-upgrade can bring on the table with the power to Create UNLIMITED Businesses, Get More Products To Promote, get Extra Lead & Funnel Templates and Other Unlimited Features</span><br><br>
                           I know you're probably very eager to get to your members area and <span class="w600">start using Ninja Ai and start getting more sales and commissions from your funnels.</span> However, if you can give me a few minutes I'll show you how to take this system beyond basic features using it to skyrocket your profit.
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!--3. Third Section End -->
            <!--4. Fourth Section Starts -->
            <div class="space-section fifth-section">
               <div class="container ">
                  <div class="row inner-content">
                     <div class="col-12 col-md-12 text-center">
                        <div class="prdly-pres f-28 f-md-38 w700 lh140 text-center white-clr text-uppercase">Introducing</div>
                     </div>
                     <div class="col-12 mt-md50 mt20 text-center">
                        <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 645.02 151.26" style="max-height:70px">
                                             <defs>
                                               <style>
                                                 .cls-1 {
                                                   fill: url(#linear-gradient-2);
                                                 }
                                           
                                                 .cls-2 {
                                                   fill: #fff;
                                                 }
                                           
                                                 .cls-3 {
                                                   fill-rule: evenodd;
                                                 }
                                           
                                                 .cls-3, .cls-4 {
                                                   fill: #ff813b;
                                                 }
                                           
                                                 .cls-5 {
                                                   fill: url(#linear-gradient-3);
                                                 }
                                           
                                                 .cls-6 {
                                                   fill: url(#linear-gradient);
                                                 }
                                           
                                                 .cls-7 {
                                                   fill: url(#linear-gradient-4);
                                                 }
                                               </style>
                                               <linearGradient id="linear-gradient" x1="47.81" y1="75" x2="185.48" y2="75" gradientUnits="userSpaceOnUse">
                                                 <stop offset="0" stop-color="#b956ff"></stop>
                                                 <stop offset="1" stop-color="#6e33ff"></stop>
                                               </linearGradient>
                                               <linearGradient id="linear-gradient-2" x1="0" y1="75" x2="85.18" y2="75" gradientUnits="userSpaceOnUse">
                                                 <stop offset="0" stop-color="#b956ff"></stop>
                                                 <stop offset="1" stop-color="#a356fa"></stop>
                                               </linearGradient>
                                               <linearGradient id="linear-gradient-3" x1="55.61" y1="62.54" x2="62.86" y2="62.54" gradientUnits="userSpaceOnUse">
                                                 <stop offset="0" stop-color="#b956ff"></stop>
                                                 <stop offset="1" stop-color="#ac59fa"></stop>
                                               </linearGradient>
                                               <linearGradient id="linear-gradient-4" x1="54.62" y1="79" x2="61.87" y2="79" gradientTransform="translate(19.64 -11.24) rotate(13.24)" xlink:href="#linear-gradient-3"></linearGradient>
                                             </defs>
                                             <g id="Layer_1-2" data-name="Layer 1">
                                               <g>
                                                 <g>
                                                   <g>
                                                     <path class="cls-6" d="m181.49,58.53h-10.72c-1.4-5.13-3.43-9.99-6.01-14.51l7.58-7.58c1.56-1.56,1.56-4.09,0-5.65l-17.65-17.65c-1.56-1.56-4.09-1.56-5.65,0l-7.58,7.58c-4.51-2.58-9.38-4.61-14.51-6.01V4c0-2.21-1.79-4-4-4h-24.96c-2.21,0-4,1.79-4,4v10.72c-.16.04-.31.09-.47.14-.27.08-.53.15-.8.23-.27.08-.55.16-.82.24-.55.17-1.09.35-1.63.53-.11.04-.22.08-.33.12-.48.17-.96.34-1.44.52-.12.04-.24.09-.36.14-.52.2-1.04.4-1.56.62-.04.02-.08.03-.12.05-2.41,1-4.74,2.15-6.99,3.43l-7.57-7.57c-1.56-1.56-4.09-1.56-5.65,0l-17.65,17.65c-.36.36-.63.76-.82,1.2h61.99v.02c.23,0,.45-.02.68-.02,23.76,0,43.01,19.26,43.01,43.01s-19.26,43.01-43.01,43.01c-.5,0-1-.02-1.5-.04v.04h-61.17c.19.43.46.84.82,1.2l17.65,17.65c1.56,1.56,4.09,1.56,5.65,0l7.57-7.57c2.25,1.28,4.58,2.43,6.99,3.43.04.02.08.03.12.05.51.21,1.03.42,1.55.62.12.05.24.09.36.14.47.18.95.35,1.43.52.11.04.23.08.34.12.54.18,1.08.36,1.63.53.27.08.55.16.83.25.26.08.53.16.79.23.16.04.31.09.47.14v10.72c0,2.21,1.79,4,4,4h24.96c2.21,0,4-1.79,4-4v-10.72c5.13-1.4,9.99-3.43,14.51-6.01l7.58,7.58c1.56,1.56,4.09,1.56,5.65,0l17.65-17.65c1.56-1.56,1.56-4.09,0-5.65l-7.58-7.58c2.58-4.51,4.61-9.38,6.01-14.51h10.72c2.21,0,4-1.79,4-4v-24.96c0-2.21-1.79-4-4-4Z"></path>
                                                     <path class="cls-1" d="m76.64,101.54c-5.74-7.31-9.18-16.52-9.18-26.54s3.44-19.23,9.18-26.54c2.45-3.13,5.33-5.9,8.53-8.24h-48.39c-1.31-1.89-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.13h17.07c-1.51,3.21-2.76,6.58-3.71,10.07h3.1c1.31-1.89,3.49-3.13,5.97-3.13,4.01,0,7.25,3.25,7.25,7.25s-3.22,7.23-7.21,7.25h-.04c-2.48,0-4.66-1.24-5.97-3.13H13.22c-.44-.64-.98-1.19-1.59-1.65-1.21-.92-2.73-1.48-4.38-1.48-4,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.14h38.82c1.31-1.89,3.49-3.13,5.97-3.13.43,0,.84.05,1.25.12,3.41.59,6,3.56,6,7.14,0,2.78-1.56,5.19-3.86,6.41-1.01.53-2.16.84-3.39.84-2.48,0-4.66-1.24-5.97-3.13h-17.85c-1.31-1.9-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.13h15.96c.95,3.48,2.2,6.85,3.71,10.07h-5.63c-1.31-1.89-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25,0,1.53.48,2.95,1.28,4.12,1.31,1.9,3.49,3.14,5.97,3.14s4.66-1.24,5.97-3.14h36.95c-3.21-2.34-6.08-5.11-8.53-8.24ZM30.64,47.97c-2,0-3.62-1.62-3.62-3.62s1.62-3.62,3.62-3.62,3.63,1.62,3.63,3.62-1.63,3.62-3.63,3.62Zm-23.39,26.47c-2,0-3.62-1.63-3.62-3.62s1.63-3.63,3.62-3.63,3.63,1.63,3.63,3.63-1.63,3.62-3.63,3.62Zm20.97,16.45c-2,0-3.63-1.63-3.63-3.63s1.63-3.62,3.63-3.62,3.62,1.63,3.62,3.62-1.63,3.63-3.62,3.63Zm14.08,18.39c-2,0-3.62-1.62-3.62-3.62s1.62-3.62,3.62-3.62,3.62,1.62,3.62,3.62-1.62,3.62-3.62,3.62Z"></path>
                                                     <circle class="cls-5" cx="59.23" cy="62.54" r="3.63"></circle>
                                                     <circle class="cls-7" cx="58.24" cy="79" r="3.63" transform="translate(-16.54 15.44) rotate(-13.24)"></circle>
                                                   </g>
                                                   <g>
                                                     <g>
                                                       <path class="cls-3" d="m104.63,78.16c-9.44,6-14.52,2.86-15.25-9.4,4.93,3.38,10.02,6.52,15.25,9.4Z"></path>
                                                       <path class="cls-3" d="m131.01,68.76c-.72,12.26-5.81,15.4-15.25,9.4,5.24-2.88,10.32-6.02,15.25-9.4Z"></path>
                                                     </g>
                                                     <path class="cls-4" d="m110.16,40.13c-.89,0-1.78.03-2.66.1-.08,0-.16,0-.24.02-9.59.79-18.07,5.45-23.89,12.42-.25.3-.49.59-.73.89-.14.19-.29.38-.43.57-4.02,5.37-6.52,11.93-6.88,19.07,0,.01,0,.03,0,.04-.01.22-.02.45-.03.68-.01.3-.02.6-.02.9v.18c0,.28,0,.55,0,.83.44,18.64,15.5,33.66,34.15,34.04.24,0,.48,0,.72,0,.3,0,.6,0,.9,0,18.85-.48,33.98-15.9,33.98-34.87s-15.61-34.88-34.88-34.88Zm.19,39.35c-14.41,14.37-29.15,1.23-29.15-9.45s13.05,0,29.15,0,29.15-10.67,29.15,0-14.74,23.81-29.15,9.45Z"></path>
                                                   </g>
                                                 </g>
                                                 <g>
                                                   <path class="cls-2" d="m286.65,118.51h-17.37l-39.32-59.42v59.42h-17.37V31.8h17.37l39.32,59.54V31.8h17.37v86.71Z"></path>
                                                   <path class="cls-2" d="m304.95,38.69c-2.03-1.94-3.04-4.36-3.04-7.26s1.01-5.31,3.04-7.26c2.03-1.94,4.57-2.92,7.63-2.92s5.6.97,7.63,2.92c2.02,1.94,3.04,4.36,3.04,7.26s-1.02,5.31-3.04,7.26c-2.03,1.94-4.57,2.91-7.63,2.91s-5.6-.97-7.63-2.91Zm16.19,11.1v68.72h-17.37V49.79h17.37Z"></path>
                                                   <path class="cls-2" d="m396.18,56.55c5.04,5.17,7.57,12.38,7.57,21.65v40.31h-17.36v-37.96c0-5.46-1.37-9.65-4.1-12.59-2.73-2.94-6.45-4.4-11.16-4.4s-8.58,1.47-11.35,4.4c-2.77,2.94-4.15,7.13-4.15,12.59v37.96h-17.37V49.79h17.37v8.56c2.31-2.98,5.27-5.31,8.87-7.01,3.6-1.69,7.55-2.54,11.85-2.54,8.19,0,14.8,2.59,19.85,7.75Z"></path>
                                                   <path class="cls-2" d="m437.61,129.8c0,7.61-1.88,13.09-5.64,16.44-3.77,3.35-9.16,5.02-16.19,5.02h-7.69v-14.76h4.96c2.64,0,4.51-.52,5.58-1.55,1.07-1.03,1.61-2.71,1.61-5.02V49.79h17.37v80.01Zm-16.31-91.11c-2.03-1.94-3.04-4.36-3.04-7.26s1.01-5.31,3.04-7.26c2.02-1.94,4.61-2.92,7.75-2.92s5.58.97,7.57,2.92c1.98,1.94,2.98,4.36,2.98,7.26s-.99,5.31-2.98,7.26c-1.98,1.94-4.51,2.91-7.57,2.91s-5.73-.97-7.75-2.91Z"></path>
                                                   <path class="cls-2" d="m454.42,65.42c2.77-5.37,6.53-9.51,11.29-12.4,4.76-2.89,10.07-4.34,15.94-4.34,5.13,0,9.61,1.04,13.46,3.1,3.85,2.07,6.92,4.67,9.24,7.82v-9.8h17.49v68.72h-17.49v-10.05c-2.23,3.23-5.31,5.89-9.24,8-3.93,2.11-8.46,3.16-13.58,3.16-5.79,0-11.06-1.49-15.82-4.47-4.76-2.98-8.52-7.17-11.29-12.59-2.77-5.41-4.16-11.64-4.16-18.67s1.38-13.11,4.16-18.48Zm47.45,7.88c-1.65-3.02-3.89-5.33-6.7-6.95-2.81-1.61-5.83-2.42-9.06-2.42s-6.2.79-8.93,2.36c-2.73,1.57-4.94,3.87-6.64,6.88-1.69,3.02-2.54,6.6-2.54,10.73s.85,7.75,2.54,10.85c1.69,3.1,3.93,5.48,6.7,7.13,2.77,1.66,5.72,2.48,8.87,2.48s6.24-.81,9.06-2.42c2.81-1.61,5.04-3.93,6.7-6.95,1.65-3.02,2.48-6.64,2.48-10.85s-.83-7.83-2.48-10.85Z"></path>
                                                   <path class="cls-4" d="m591.93,102.01h-34.48l-5.71,16.5h-18.24l31.14-86.71h20.22l31.13,86.71h-18.36l-5.71-16.5Zm-4.71-13.89l-12.53-36.22-12.53,36.22h25.06Z"></path>
                                                   <path class="cls-4" d="m645.02,31.93v86.58h-17.37V31.93h17.37Z"></path>
                                                 </g>
                                               </g>
                                             </g>
                                           </svg>
                                <div class="f-22 f-md-36 text-center white-clr lh140 w400 mt-md30 mt20">
                                 The Unlimited Features Top Marketers Demand & Need Are Now Available At A Fraction Of The Cost
                            </div>
                        </div>
                     <div class="col-12 mt5 mt-sm5">
                        <div class="mt20 mt-sm40">
                           <img src="assets/images/productbox.webp" class="mx-auto d-block img-fluid" width="850">
                        </div>
                     </div>
                     <div class="col-md-10 col-12 mt30 mt-md65 mx-auto">
                        <div class="f-18 f-md-36 text-center probtn1" editabletype="button" style="z-index: 10;">
                           <a href="#buynow" class="text-center">Get Instant Access to Ninja Ai Unlimited</a>
                        </div>
                        <br class="visible-md">
                     </div>
                  </div>
               </div>
            </div>
            <!--4. Fourth Section End -->
            <!--5. FiFth Section Starts
               <div class="space-section white-section">
               	<div class="container">
               		<div class="row inner-content">
               
               				<div class="col-12 col-md-12">
               
               				<div class="f-16 f-md-22 w600 lh140">
               				<span class="f-20 f-md-25">Hey Folks,</span><br><br>
               
               Dr. Amit and Ashu Kumar here again and I would like to thank you for picking up MaxDrive. <br><br>
               
               And I would like to tell you that Not every marketer needs the features and tools you’re about to discover. <br>
               Yes! We are going to hand you over 40 extra PROVEN converting and ready-to-use lead, promo and social templates that we have used for our lead generation, selling products and webinars generating over 7 figures in revenue that you can simply swipe and making profit. It's your time to get an unfair advantage over your competition with these templates and 10+ pro features.
               				</div>
               
               				</div>
               		</div>
               	</div>
               </div>
                FiFth Section End -->
            <!--6. Six Section Starts (features)-->
            <!-- <div class="space-section fourth-section">
               <div class="container">
                   <div class="row inner-content">
                       <div class="col-12 col-md-12">
                           <div class="promo-icon mt20 mt-sm40" editabletype="image" style="z-index: 9;">
                               <img src="assets/images/promo-icon.png" class="mx-auto d-block img-fluid">
                           </div>
                           <div class="f-25 f-md-42 lh140 w600 text-center mt20 mt-sm40 mb20" editabletype="text"
                               style="z-index: 10;">
                               Here’s What You’re Getting with <span class="kapblue">This Unfair Advantage
                                   Upgrade</span>
                           </div>
                       </div>
                   </div>
               </div>
               </div> -->
            <!--6. Six Section Starts -->
            <!-- feature1 -->
            <div class="space-section white-section">
               <div class="container ">
                  <div class="row inner-content">
                     <div class="col-12 col-md-12">
                        <div class="f-28 f-md-50 lh140 w500 text-center mb-sm40 mb20 black-clr">
                           Here’s What You’re Getting with <br class="hidden-xs"><span class="w700">This Unfair Advantage
                           Upgrade</span>
                        </div>
                     </div>
                     <div class="col-12 col-md-12">
                        <div>
                           <img src="../common_assets/images/1.webp" class="img-responsive">
                        </div>
                        <div class="f-24 f-md-36 w500 lh140 mt10 mt-sm10 text-left black-clr">
                           Create Unlimited Businesses (Sub-Domains) For Promoting Products For Yourself Or Clients
                        </div>
                        <div class="col-12 col-md-10 mt20 mt-md30 mx-auto">
                           <img src="assets/images/f1.webp" class="mx-auto d-block img-fluid">
                        </div>
                        <div class="col-12 f-20 px0 f-md-22 w400 lh140 mt10 mt-md50 text-left">
                           Today, thousands of businesses need high-class marketing services, but don’t know where to get them from. Or even if they know, they are charged HUGE MONEY that’s completely beyond their reach.<br><br>
                           Now, you too can establish yourself as an authority in the market by creating unlimited businesses and <span class="w600">build affiliate marketing funnels to get maximum affiliate</span> commissions.
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!--feature2 -->
            <div class="space-section grey-section">
               <div class="container ">
                  <div class="row inner-content">
                     <div class="col-12 col-md-12">
                        <div class="pr0">
                           <img src="../common_assets/images/2.webp" class="img-responsive">
                        </div>
                        <div class="f-24 f-md-36 w500 lh140 mt10 mt-sm10 text-left black-clr">
                           Get EXTRA, Done-for-You And Proven Converting Lead Funnel Templates
                        </div>
                        <div class="col-12 col-md-10  mx-auto px0 mt20 mt-md30 p-sm0">
                           <img src="assets/images/f2.png" class="mx-auto d-block img-fluid">
                        </div>
                        <div class="col-12 px0 f-18 f-md-22 w400 lh140 mt20 mt-md50">
                           Designing premium templates that entice audience attention is a critical task for marketers. To bail you out of this menace, we’re providing an enticing assortment of <span class="w600">25 MORE AWESOME, Attention-Grabbing lead funnel templates</span> to help you capture more fresh leads by simply delivering the BEST of your offers in the most striking way.<br><br> 
                           These templates are custom made for every marketer and every niche, and the best part is, they’re SUPER-EASY to edit. All you need to do is just insert a CTA and see a massive increase in your conversions.
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            
            <!--feature3 -->
            <div class="space-section white-section">
               <div class="container ">
                  <div class="row inner-content">
                     <div class="col-12 col-md-12">
                        <div>
                           <img src="../common_assets/images/3.webp" class="img-responsive">
                        </div>
                        <div class="f-24 f-md-36 w500 lh140 mt10 mt-sm10 text-left black-clr">
                           AI ChatBot to Convert More Visitors for You - More Commissions on Your Way
                        </div>
                        <div class="col-12 col-md-10 mx-auto px0 mt20 mt-md30 p-sm0">
                           <!--<img src="assets/images/f3.webp" class="mx-auto d-block img-fluid">-->
                            <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 1px solid #15c6fc;">
                                <source src="assets/images/ai-chatbot.mp4" type="video/mp4">
                            </video>
                        </div>
                        <div class="col-12 px0 f-20 f-md-22 w400 lh140 mt20 mt-md50 text-left">
                           Forget all about manual customer support.
                           <br><br>
                           Because now, you will have automated AI bots that will engage with your customers and offer them any kind of help they want…
                            <br><br>
                            Not only that, it will even upsell the customers for you.
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!--feature3 -->
            
            <!--feature3 -->
            <div class="space-section grey-section">
               <div class="container ">
                  <div class="row inner-content">
                     <div class="col-12 col-md-12">
                        <div>
                           <img src="../common_assets/images/4.webp" class="img-responsive">
                        </div>
                        <div class="f-24 f-md-36 w500 lh140 mt10 mt-sm10 text-left black-clr">
                           Get 100 MORE Researched Products To Have An Endless Supply Of Affiliate Commissions And Sales
                        </div>
                        <div class="col-12 col-md-10 mx-auto px0 mt20 mt-md30 p-sm0">
                           <img src="assets/images/f3.webp" class="mx-auto d-block img-fluid">
                        </div>
                        <div class="col-12 px0 f-20 f-md-22 w400 lh140 mt20 mt-md50 text-left">
                           In addition to the 50 PROVEN products you got with Ninja Ai Professional Edition, we’re providing you with <span class="w600">50 MORE researched products</span> to promote. We’re sure they’ll convert crazily for you and help you to maximize your affiliate commissions hands down.
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!--feature3 -->
            <!--feature4 -->
            <div class="space-section white-section">
               <div class="container ">
                  <div class="row inner-content">
                     <div class="col-12 col-md-12">
                        <div>
                           <img src="../common_assets/images/5.webp" class="img-responsive">
                        </div>
                        <div class="f-24 f-md-36 w500 lh140 mt10 mt-sm10 text-left black-clr">
                           Get 20 Extra Attractive Promo Funnel Templates
                        </div>
                        <div class="col-12 col-md-10 mx-auto px0 mt20 mt-md30 p-sm0">
                           <img src="assets/images/f4.png" class="mx-auto d-block img-fluid" style="">
                        </div>
                        <div class="col-12 px0 f-20 f-md-22 w400 lh140 mt20 mt-md30 text-left">
                           Now, you can <span class="w600">easily capture visitor’s attention.</span> No imagining what looks good and what works, we already did the hard work for you. Just choose a template, edit it and you’re ready to rock in few seconds.<br><br>
                           These templates are <span class="w600">beautifully designed</span> and can be customized as per your brand and need.
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!--feature5 -->
            <div class="space-section grey-section">
               <div class="container ">
                  <div class="row inner-content">
                     <div class="col-12 col-md-12">
                        <div>
                           <img src="../common_assets/images/6.webp" class="img-responsive">
                        </div>
                        <div class="f-24 f-md-36 w500 lh140 mt10 mt-sm10 text-left black-clr">
                           Maximize ROI From Your Leads With Webinar Integration
                        </div>
                     </div>
                     <div class="col-12 col-md-12">
                        <div class="mt20 mt-sm40">
                           <img src="assets/images/f5.webp" class="mx-auto d-block img-fluid">
                        </div>
                     </div>
                     <div class="col-12 col-md-12">
                        <div class="f-20 f-md-22 w400 lh140 mt20 mt-md30 text-left">
                           With Unlimited edition, we are making it easier for you to integrate a webinar with your affiliate funnel. So, when someone fill the form, they <span class="w600">automatically will get registered for webinar & you can get maximum ROI.</span><br><br>
                           For webinar integration, just put API key inside the box and you are ready to get more registrants without losing a single lead.
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!--feature6 -->
            <div class="space-section white-section">
               <div class="container ">
                  <div class="row inner-content">
                     <div class="col-12 col-md-12">
                        <div>
                           <img src="../common_assets/images/7.webp" class="img-responsive">
                        </div>
                        <div class="f-24 f-md-36 w500 lh140 mt10 mt-sm10 text-left black-clr">
                           Make Sites More Attractive By Adding Images From <span class="w600">AI Media Library</span> 
                           <!-- make the most of your knowledge & get your students completely amazed -->
                        </div>
                     </div>
                     <div class="col-12 col-md-10 mt20 mt-sm40 mx-auto">
                           <img src="assets/images/f6.webp" class="mx-auto d-block img-fluid">
                     </div>
                     <div class="col-12 col-md-12">
                        <div class="f-20 f-md-22 w400 lh140 mt20 mt-md50 text-left">
                           Eye-catchy images are the best way to attract attention of audience and get them hooked to your affiliate marketing funnel. Yep, <span class="w600">Ninja Ai Unlimited Edition</span> gives you the power to upload attractive images from library and grab their attention by the neck to boost sales and commissions like you always wanted.<br><br>
                           <span class="w600"> Isn’t that SUPER Cool?</span>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!--feature7 -->
            <div class="space-section grey-section">
               <div class="container ">
                  <div class="row inner-content">
                     <div class="col-12 col-md-12">
                        <div>
                           <img src="../common_assets/images/8.webp" class="img-responsive">
                        </div>
                        <div class="f-24 f-md-36 w500 lh140 mt10 mt-sm10 text-left black-clr">
                           Share Control Of Your Dashboard To Up To 5 Team Members
                        </div>
                     </div>
                     <div class="col-12 col-md-10 mt20 mt-sm40 mx-auto">
                           <img src="assets/images/f7.webp" class="mx-auto d-block img-fluid">
                     </div>
                     <div class="col-12 col-md-12">
                        <div class="f-20 f-md-22 w400 lh140 mt20 mt-md60 text-left">
                           You can add up-to 5 team members to manage your campaigns according to their roles and what action they can perform in dashboard. You can also <span class="w600">create team and assign roles and permissions to them.</span><br><br>
                           All you need to do is click on the add new user tab and fill all the information including the name of the person, their email address and assign them specific roles and give privileges that you want to give to that person.
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- feature10 -->
            <div class="space-section white-section">
               <div class="container ">
                  <div class="row inner-content">
                     <div class="col-12 col-md-12 mb-md20">
                        <div class="mt20 mt-sm0">
                           <img src="../common_assets/images/9.webp" class="img-responsive">
                        </div>
                        <div class="f-24 f-md-36 w500 lh140 mt10 mt-sm10 text-left black-clr">
                           Get all these benefits at an unparallel price
                        </div>
                     </div>
                     <div class="col-12 col-md-5 mt20 mt-md20">
                           <img src="assets/images/f8.png" class="mx-auto d-block img-fluid">
                     </div>
                     <div class="col-12 col-md-7 col-md-pull-5 mt-sm10 mt0">
                        <div class="f-20 f-md-22 w400 lh140 mt20 mt-sm80">
                           Ultimately you are saving thousands of dollars monthly with us, &amp; every dollar that you save, adds to your profits. By multiplying engagement &amp; opt-ins, <span class="w600">you’re not only saving money, you’re also taking your business to the next level.</span>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!--7. Seventh Section Starts -->
            <div class="space-section grey-section">
               <div class="container ">
                  <div class="row inner-content">
                     <div class="col-12 col-md-12">
                        <div class="">
                           <div class="f-28 f-md-50 w500 lh140 text-center">
                              Act Now For <span class="w700">2 Very Good Reasons:</span>
                           </div>
                        </div>
                        <div class="row align-items-center mt20">
                           <div class="col-2 col-md-1 pl0 pr10">
                              <img src="assets/images/one.png" class="mx-auto d-block img-fluid">
                           </div>
                           <div class="col-10 col-md-11">
                              <div class="f-20 f-md-22 w400 lh140">
                                 We’re offering Ninja Ai Unlimited at a CUSTOMER’S ONLY discount, so you can grab this <span class="w600">game-changing upgrade for a low ONE-TIME fee.</span>
                              </div>
                           </div>
                        </div>
                        <div class="row align-items-center mt20">
                           <div class="col-2 col-md-1 pl0 pr10">
                              <div>
                                 <img src="assets/images/two.png" class="mx-auto d-block img-fluid">
                              </div>
                           </div>
                           <div class="col-10 col-md-11">
                              <div class="f-20 f-md-22 w400 lh140">
                                 Your only chance to <span class="w600">get access is right now,</span> on this page. When this page closes, so does this exclusive offer.
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!--7. Seventh Section End -->
            <!--Commercial License Start -->
            <!-- <div class="space-section license-section" id="funnel">
               <div class="container ">
                   <div class="row inner-content">
                       <div class="col-12">
                           <div class="f-25 f-md-42 lh140 w400 text-center black-clr">
                               Commercial License to Serve Clients with <br class="visible-lg"><span class="kapblue w600">Unlimited-Services</span>
                           </div>
                       </div>
                       <div class="col-12 col-md-4 mt10 mt-md20">
                           <div class="mt20 mt-md15"><img src="assets/images/license.png" class="mx-auto d-block img-fluid"></div>
                       </div>
                       <div class="col-12 col-md-8 mt10 mt-sm40">
                           <div class="f-20 f-md-22 w400 mt20 text-left black-clr lh140">
                               MaxDrive 2.0 Unlimited Commercial Edition leverages you with the limited time offer for the Commercial License. Start providing these pro quality services to your clients and build yourself an additional monthly income.
                               <br>
                               <br> Hurry Up! 36% of the users have already claimed the MaxDrive 2.0 Unlimited Edition with the Commercial License.
               
                           </div>
                       </div>
                   </div>
               </div>
               </div> -->
            <!--Commercial License End-->
            <!--money back Start -->
            <div class="space-section moneyback-section">
               <div class="container ">
                  <div class="row inner-content">
                     <div class="col-12 px-md15">
                        <div class="f-28 f-md-50 lh140 w500 text-center white-clr">
                           Let’s Remove All The Risks For You And Secure It With <span class="w700">30 Days Money Back Guarantee</span>
                        </div>
                     </div>
                     <div class="col-12 col-md-4 mt10 mt-md30 px-md15">
                        <div class="mt20 mt-md30">
                           <img src="assets/images/moneyback.png" class="mx-auto d-block img-fluid">
                        </div>
                     </div>
                     <div class="col-12 col-md-8 mt10 mt-md30 px-md15">
                        <div class="f-md-22 f-20 lh140 w400 mt-md20 text-left white-clr">
                           You can buy with confidence because if you face any technical issue which we don’t resolve for you, <span class="w600">just raise a ticket within 30 days and we'll refund you everything,</span> down to the last penny.
                           <br><br> <span class="w600">Our team has a 99.21% proven record of solving customer problems and helping them through any issues. </span>
                           <br><br> Guarantees like that don't come along every day, and neither do golden chances like this.
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!--money back End-->
            <!---Bonus Section Starts-->
            <div class="space-section bonus-section">
               <div class="container">
                  <div class="row inner-content">
                     <div class="col-12 col-md-12 ">
                        <div class="col-12 col-md-12 text-center">
                           <div class="bonus-head f-28 f-md-48 w600 lh140 text-center white-clr">
                              But That's Not All
                           </div>
                        </div>
                        <div class="col-12 p0 f-22 f-md-30 w400 mt10 mt-sm0 mt-md30 text-center mb20 lh140">
                           In addition, we have a number of bonuses for those who want to take action today and start profiting from this opportunity
                        </div>
                     </div>
                     <!-- bonus1 -->
                     <div class="row p0 mt10 mt-md60 align-items-center">
                        <div class="col-12 col-md-6 ">
                           <img src="assets/images/bonus1.webp" alt="Bonus 1" class="img-fluid d-block ">
                           <div class="f-24 f-md-30 w500 lh140 mt20 mt-md20 text-left black-clr">
                              Virtual Summit Secrets
                           </div>
                           <div class="f-20 f-md-22 w400 lh140 mt10 mt-sm10 text-left">
                              Discover A virtual summit is an online conference that allows attendees from around the world to watch and learn from a live event. It's usually organized by one person, the host, and has a variety of 5 or more guest speakers, each discussing topics within a particular niche and/or industry. When combined with Ninja AI unlimited, this package proves to be of immense worth for every marketer.
                           </div>
                        </div>
                        <div class="col-12 col-md-6 ">
                           <div class="mt20 mt-sm0" editabletype="image" style="z-index: 10;">
                              <img src="assets/images/bonusbox.webp" class="mx-auto d-block img-fluid">
                           </div>
                        </div>
                     </div>
                     <!-- bonus2 -->
                     <div class="row p0 mt40 mt-md60 align-items-center">
                        <div class="col-12 col-md-6 order-md-2">
                           <!-- <div editabletype="image" style="z-index: 10;">
                              <img src="assets/images/bonus-heading2.png" class="img-responsive">
                              </div> -->
                          <img src="assets/images/bonus2.webp" alt="Bonus 2" class="img-fluid d-block ">
                           <div class="f-24 f-md-30 w500 lh140 mt20 mt-md20 text-left black-clr">
                              30 Days Content Marketing Plan
                           </div>
                           <div class="f-20 f-md-22 w400 lh140 mt10 mt-sm10 text-left">
                              Discover how you can finally create a content marketing strategy that will help you grow your business! 30-Day Content Marketing Plan is a is a step-by-step blueprint to creating fresh content that converts. Don’t spend time thinking, just use this with Ninja AI Unlimited Upgrade and scale your business to new heights.
                           </div>
                        </div>
                        <div class="col-12 col-md-6  order-md-1">
                           <div class="mt20 mt-md30" editabletype="image" style="z-index: 10;">
                              <img src="assets/images/bonusbox.webp" class="mx-auto d-block img-fluid">
                           </div>
                        </div>
                     </div>
                     <!-- bonus3 -->
                     <div class="row p0 mt40 mt-md60 align-items-center">
                        <div class="col-12 col-md-6   mt-md50 mt0">
                           <!-- <div editabletype="image" style="z-index: 10;">
                              <img src="assets/images/bonus-heading3.png" class="img-responsive">
                              </div> -->
                              <img src="assets/images/bonus3.webp" alt="Bonus 3" class="img-fluid d-block ">
                           <div class="f-24 f-md-30 w500 lh140 mt20 mt-md20 text-left black-clr">
                              Traffic Secrets Unleashed
                           </div>
                           <div class="f-20 f-md-22 w400 lh140 mt10 mt-sm10 text-left">
                              This is a short training that gets straight to the core and reveals super-effective traffic sources for getting huge amount of traffic. You will learn to use the largest, most responsive, supportive, and the best traffic generating tribe on the internet to reach your goals!
                           </div>
                        </div>
                        <div class="col-12 col-md-6  mt20 mt-md50">
                           <div class="" editabletype="image" style="z-index: 10;">
                              <img src="assets/images/bonusbox.webp" class="mx-auto d-block img-fluid">
                           </div>
                        </div>
                     </div>
                     <!-- bonus4 -->
                     <div class="row p0 mt40 mt-md60 align-items-center">
                        <div class="col-12 col-md-6 order-md-2">
                           <!-- <div editabletype="image" style="z-index: 10;">
                              <img src="assets/images/bonus-heading4.png" class="img-responsive">
                              </div> -->
                              <img src="assets/images/bonus4.webp" alt="Bonus 4" class="img-fluid d-block ">
                           <div class="f-24 f-md-30 w500 lh140 mt20 mt-md20 text-left black-clr">
                              Sales Funnel Explosion 
                           </div>
                           <div class="f-20 f-md-22 w400 lh140 mt10 mt-sm10 text-left">
                              Learn How To Build A Profitable Online Sales Funnel! How to build a list very fast by buying solo ad. Solo Ad is when you pay someone to mail their email list, on your behalf, with a recommendation and link to your free optin offer squeeze page. When combined with Ninja AI Unlimited, this package proves to build a list for every marketer.
                           </div>
                        </div>
                        <div class="col-12 col-md-6  mt20 mt-md30 order-md-1">
                           <div editabletype="image" style="z-index: 10;">
                              <img src="assets/images/bonusbox.webp" class="mx-auto d-block img-fluid">
                           </div>
                        </div>
                     </div>
                     <!-- bonus5 -->
                     <!--  <div class="col-12 col-md-12 p0 mt40 mt-md60">
                        <div class="col-12 col-md-6  mt-md50 mt0">							
                        <div class="f-md-36 f-22 w600 black-clr black-clr ">
                        Bonus 5
                        </div>
                            <div class="f-22 f-md-36 w400 lh140 mt20 mt-md30 text-left">
                        Miscellaneous Stock Photos
                            </div>
                            <div class="f-20 f-md-22 w400 lh140 mt10 mt-sm10 text-left">
                        Get high quality stock images to use in your projects and your clients projects. And the best part, you can resell them. Now use these eye-catchy stock images on your landing pages built with Ninja Ai, and pack a lasting punch at your competitors.
                            </div>
                        </div>
                        <div class="col-12 col-md-6 ">
                            <div class="mt20 mt-md50" editabletype="image" style="z-index: 10;">
                                <img src="assets/images/bonusbox.webp" class="img-responsive">
                            </div>
                        </div>
                        </div> -->
                  </div>
               </div>
            </div>
            <!--Bonus Section Starts -->
            <!--10. Table Section Starts -->
            <div class="space-section table-section" id="buynow">
               <div class="container ">
                  <div class="row inner-content">
                     <div class="col-12 col-md-12">
                        <div class="f-20 f-md-22 w400 lh140 text-center">
                           Yup..! Take action on this while you can as you won’t be seeing this offer again.
                        </div>
                        <div class="f-28 f-md-50 w500 lh140 mt20 mt-md30 text-center black-clr">
                           Today You Can <span class="w700">Get Unrestricted Access to Ninja Ai</span> For LESS Than the Price of Just One Month’s Membership.
                        </div>
                     </div>
                     <div class="row mt30 mt-md60">
                        <div class="col-md-8 col-12 mx-auto">
                           <div class="tablebox3">
                              <div class="tbbg3 text-center">
                                 <div class="relative">
                                    <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 645.02 151.26" style="max-height:70px">
                                       <defs>
                                         <style>
                                           .cls-1 {
                                             fill: url(#linear-gradient-2);
                                           }
                                     
                                           .cls-2 {
                                             fill: #fff;
                                           }
                                     
                                           .cls-3 {
                                             fill-rule: evenodd;
                                           }
                                     
                                           .cls-3, .cls-4 {
                                             fill: #ff813b;
                                           }
                                     
                                           .cls-5 {
                                             fill: url(#linear-gradient-3);
                                           }
                                     
                                           .cls-6 {
                                             fill: url(#linear-gradient);
                                           }
                                     
                                           .cls-7 {
                                             fill: url(#linear-gradient-4);
                                           }
                                         </style>
                                         <linearGradient id="linear-gradient" x1="47.81" y1="75" x2="185.48" y2="75" gradientUnits="userSpaceOnUse">
                                           <stop offset="0" stop-color="#b956ff"></stop>
                                           <stop offset="1" stop-color="#6e33ff"></stop>
                                         </linearGradient>
                                         <linearGradient id="linear-gradient-2" x1="0" y1="75" x2="85.18" y2="75" gradientUnits="userSpaceOnUse">
                                           <stop offset="0" stop-color="#b956ff"></stop>
                                           <stop offset="1" stop-color="#a356fa"></stop>
                                         </linearGradient>
                                         <linearGradient id="linear-gradient-3" x1="55.61" y1="62.54" x2="62.86" y2="62.54" gradientUnits="userSpaceOnUse">
                                           <stop offset="0" stop-color="#b956ff"></stop>
                                           <stop offset="1" stop-color="#ac59fa"></stop>
                                         </linearGradient>
                                         <linearGradient id="linear-gradient-4" x1="54.62" y1="79" x2="61.87" y2="79" gradientTransform="translate(19.64 -11.24) rotate(13.24)" xlink:href="#linear-gradient-3"></linearGradient>
                                       </defs>
                                       <g id="Layer_1-2" data-name="Layer 1">
                                         <g>
                                           <g>
                                             <g>
                                               <path class="cls-6" d="m181.49,58.53h-10.72c-1.4-5.13-3.43-9.99-6.01-14.51l7.58-7.58c1.56-1.56,1.56-4.09,0-5.65l-17.65-17.65c-1.56-1.56-4.09-1.56-5.65,0l-7.58,7.58c-4.51-2.58-9.38-4.61-14.51-6.01V4c0-2.21-1.79-4-4-4h-24.96c-2.21,0-4,1.79-4,4v10.72c-.16.04-.31.09-.47.14-.27.08-.53.15-.8.23-.27.08-.55.16-.82.24-.55.17-1.09.35-1.63.53-.11.04-.22.08-.33.12-.48.17-.96.34-1.44.52-.12.04-.24.09-.36.14-.52.2-1.04.4-1.56.62-.04.02-.08.03-.12.05-2.41,1-4.74,2.15-6.99,3.43l-7.57-7.57c-1.56-1.56-4.09-1.56-5.65,0l-17.65,17.65c-.36.36-.63.76-.82,1.2h61.99v.02c.23,0,.45-.02.68-.02,23.76,0,43.01,19.26,43.01,43.01s-19.26,43.01-43.01,43.01c-.5,0-1-.02-1.5-.04v.04h-61.17c.19.43.46.84.82,1.2l17.65,17.65c1.56,1.56,4.09,1.56,5.65,0l7.57-7.57c2.25,1.28,4.58,2.43,6.99,3.43.04.02.08.03.12.05.51.21,1.03.42,1.55.62.12.05.24.09.36.14.47.18.95.35,1.43.52.11.04.23.08.34.12.54.18,1.08.36,1.63.53.27.08.55.16.83.25.26.08.53.16.79.23.16.04.31.09.47.14v10.72c0,2.21,1.79,4,4,4h24.96c2.21,0,4-1.79,4-4v-10.72c5.13-1.4,9.99-3.43,14.51-6.01l7.58,7.58c1.56,1.56,4.09,1.56,5.65,0l17.65-17.65c1.56-1.56,1.56-4.09,0-5.65l-7.58-7.58c2.58-4.51,4.61-9.38,6.01-14.51h10.72c2.21,0,4-1.79,4-4v-24.96c0-2.21-1.79-4-4-4Z"></path>
                                               <path class="cls-1" d="m76.64,101.54c-5.74-7.31-9.18-16.52-9.18-26.54s3.44-19.23,9.18-26.54c2.45-3.13,5.33-5.9,8.53-8.24h-48.39c-1.31-1.89-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.13h17.07c-1.51,3.21-2.76,6.58-3.71,10.07h3.1c1.31-1.89,3.49-3.13,5.97-3.13,4.01,0,7.25,3.25,7.25,7.25s-3.22,7.23-7.21,7.25h-.04c-2.48,0-4.66-1.24-5.97-3.13H13.22c-.44-.64-.98-1.19-1.59-1.65-1.21-.92-2.73-1.48-4.38-1.48-4,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.14h38.82c1.31-1.89,3.49-3.13,5.97-3.13.43,0,.84.05,1.25.12,3.41.59,6,3.56,6,7.14,0,2.78-1.56,5.19-3.86,6.41-1.01.53-2.16.84-3.39.84-2.48,0-4.66-1.24-5.97-3.13h-17.85c-1.31-1.9-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.13h15.96c.95,3.48,2.2,6.85,3.71,10.07h-5.63c-1.31-1.89-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25,0,1.53.48,2.95,1.28,4.12,1.31,1.9,3.49,3.14,5.97,3.14s4.66-1.24,5.97-3.14h36.95c-3.21-2.34-6.08-5.11-8.53-8.24ZM30.64,47.97c-2,0-3.62-1.62-3.62-3.62s1.62-3.62,3.62-3.62,3.63,1.62,3.63,3.62-1.63,3.62-3.63,3.62Zm-23.39,26.47c-2,0-3.62-1.63-3.62-3.62s1.63-3.63,3.62-3.63,3.63,1.63,3.63,3.63-1.63,3.62-3.63,3.62Zm20.97,16.45c-2,0-3.63-1.63-3.63-3.63s1.63-3.62,3.63-3.62,3.62,1.63,3.62,3.62-1.63,3.63-3.62,3.63Zm14.08,18.39c-2,0-3.62-1.62-3.62-3.62s1.62-3.62,3.62-3.62,3.62,1.62,3.62,3.62-1.62,3.62-3.62,3.62Z"></path>
                                               <circle class="cls-5" cx="59.23" cy="62.54" r="3.63"></circle>
                                               <circle class="cls-7" cx="58.24" cy="79" r="3.63" transform="translate(-16.54 15.44) rotate(-13.24)"></circle>
                                             </g>
                                             <g>
                                               <g>
                                                 <path class="cls-3" d="m104.63,78.16c-9.44,6-14.52,2.86-15.25-9.4,4.93,3.38,10.02,6.52,15.25,9.4Z"></path>
                                                 <path class="cls-3" d="m131.01,68.76c-.72,12.26-5.81,15.4-15.25,9.4,5.24-2.88,10.32-6.02,15.25-9.4Z"></path>
                                               </g>
                                               <path class="cls-4" d="m110.16,40.13c-.89,0-1.78.03-2.66.1-.08,0-.16,0-.24.02-9.59.79-18.07,5.45-23.89,12.42-.25.3-.49.59-.73.89-.14.19-.29.38-.43.57-4.02,5.37-6.52,11.93-6.88,19.07,0,.01,0,.03,0,.04-.01.22-.02.45-.03.68-.01.3-.02.6-.02.9v.18c0,.28,0,.55,0,.83.44,18.64,15.5,33.66,34.15,34.04.24,0,.48,0,.72,0,.3,0,.6,0,.9,0,18.85-.48,33.98-15.9,33.98-34.87s-15.61-34.88-34.88-34.88Zm.19,39.35c-14.41,14.37-29.15,1.23-29.15-9.45s13.05,0,29.15,0,29.15-10.67,29.15,0-14.74,23.81-29.15,9.45Z"></path>
                                             </g>
                                           </g>
                                           <g>
                                             <path class="cls-2" d="m286.65,118.51h-17.37l-39.32-59.42v59.42h-17.37V31.8h17.37l39.32,59.54V31.8h17.37v86.71Z"></path>
                                             <path class="cls-2" d="m304.95,38.69c-2.03-1.94-3.04-4.36-3.04-7.26s1.01-5.31,3.04-7.26c2.03-1.94,4.57-2.92,7.63-2.92s5.6.97,7.63,2.92c2.02,1.94,3.04,4.36,3.04,7.26s-1.02,5.31-3.04,7.26c-2.03,1.94-4.57,2.91-7.63,2.91s-5.6-.97-7.63-2.91Zm16.19,11.1v68.72h-17.37V49.79h17.37Z"></path>
                                             <path class="cls-2" d="m396.18,56.55c5.04,5.17,7.57,12.38,7.57,21.65v40.31h-17.36v-37.96c0-5.46-1.37-9.65-4.1-12.59-2.73-2.94-6.45-4.4-11.16-4.4s-8.58,1.47-11.35,4.4c-2.77,2.94-4.15,7.13-4.15,12.59v37.96h-17.37V49.79h17.37v8.56c2.31-2.98,5.27-5.31,8.87-7.01,3.6-1.69,7.55-2.54,11.85-2.54,8.19,0,14.8,2.59,19.85,7.75Z"></path>
                                             <path class="cls-2" d="m437.61,129.8c0,7.61-1.88,13.09-5.64,16.44-3.77,3.35-9.16,5.02-16.19,5.02h-7.69v-14.76h4.96c2.64,0,4.51-.52,5.58-1.55,1.07-1.03,1.61-2.71,1.61-5.02V49.79h17.37v80.01Zm-16.31-91.11c-2.03-1.94-3.04-4.36-3.04-7.26s1.01-5.31,3.04-7.26c2.02-1.94,4.61-2.92,7.75-2.92s5.58.97,7.57,2.92c1.98,1.94,2.98,4.36,2.98,7.26s-.99,5.31-2.98,7.26c-1.98,1.94-4.51,2.91-7.57,2.91s-5.73-.97-7.75-2.91Z"></path>
                                             <path class="cls-2" d="m454.42,65.42c2.77-5.37,6.53-9.51,11.29-12.4,4.76-2.89,10.07-4.34,15.94-4.34,5.13,0,9.61,1.04,13.46,3.1,3.85,2.07,6.92,4.67,9.24,7.82v-9.8h17.49v68.72h-17.49v-10.05c-2.23,3.23-5.31,5.89-9.24,8-3.93,2.11-8.46,3.16-13.58,3.16-5.79,0-11.06-1.49-15.82-4.47-4.76-2.98-8.52-7.17-11.29-12.59-2.77-5.41-4.16-11.64-4.16-18.67s1.38-13.11,4.16-18.48Zm47.45,7.88c-1.65-3.02-3.89-5.33-6.7-6.95-2.81-1.61-5.83-2.42-9.06-2.42s-6.2.79-8.93,2.36c-2.73,1.57-4.94,3.87-6.64,6.88-1.69,3.02-2.54,6.6-2.54,10.73s.85,7.75,2.54,10.85c1.69,3.1,3.93,5.48,6.7,7.13,2.77,1.66,5.72,2.48,8.87,2.48s6.24-.81,9.06-2.42c2.81-1.61,5.04-3.93,6.7-6.95,1.65-3.02,2.48-6.64,2.48-10.85s-.83-7.83-2.48-10.85Z"></path>
                                             <path class="cls-4" d="m591.93,102.01h-34.48l-5.71,16.5h-18.24l31.14-86.71h20.22l31.13,86.71h-18.36l-5.71-16.5Zm-4.71-13.89l-12.53-36.22-12.53,36.22h25.06Z"></path>
                                             <path class="cls-4" d="m645.02,31.93v86.58h-17.37V31.93h17.37Z"></path>
                                           </g>
                                         </g>
                                       </g>
                                     </svg>
                                 </div>
                                 <div class="text-uppercase mt15 mt-md15 w600 f-md-32 f-22 text-center white-clr lh120">Commercial</div>
                              </div>
                              <div>
                                 <ul class="f-20 f-md-20 w400 text-center lh130 grey-tick-last mb0 white-clr">
                                    <li>Create Unlimited Businesses/Sub-Domains For Promoting Unlimited Products to 5X Profits</li>
                                    <li>Get Done-for-You And Premium Lead Funnel Templates to DOUBLE Your Leads</li>
                                    <li>Get 100 Researched Affiliates Products To Make Recurring Affiliate Commissions And Sales</li>
                                    <li>Get 20+ Extra Attractive Promotion Funnel Templates</li>
                                    <li>AI ChatBot to Convert More Visitors for You - More Commissions on Your Way</li>
                                    <li>Maximize ROI & Sales From Your Leads With Automated Webinar Integration</li>
                                    <li>Make Funnels More Attractive By Adding Images From <span class="w600">AI Media Library</span> </li>
                                    <li>Share Control Of Your Dashboard To Up To 5 Team Members</li>
                                    <li class="w600">Get All These Benefits At An Unparalleled Price</li>
                                 </ul>
                                 <div class="myfeatureslast-com f-md-25 f-16 w400 text-center lh160 hideme" editabletype="shape" style="opacity: 1; z-index: 9;">
                                    
                                    <a href="https://warriorplus.com/o2/buy/zh5gzv/zwv1bj/j2hdbw" id="buyButton"><img src="https://warriorplus.com/o2/btn/fn200011000/zh5gzv/zwv1bj/357324" class="mx-auto d-block img-fluid"></a>

                                    <div class="table-border-content">
                                       <div class="tb-check d-flex align-items-center f-18">
                                       <label class="checkbox inline">
                                           <img src="https://assets.clickfunnels.com/templates/listhacking-sales/images/arrow-flash-small.gif" alt="">
                                           <input type="checkbox" id="check" onclick="myFunction()">
                                       </label>
                                       <label class="checkbox inline">
                                           <h5 class="f-14 w600"> &nbsp; &nbsp;Yes, Add CDN Fast Unlimited Hosting &amp; 300+DFY Lead Popups</h5>
                                       </label>
                                       </div>
                                       <div class="p20">
                                       <p class="f-18 text-center text-md-start"><b class="red-clr underline">LIMITED TIME OFFER :</b> Active CDN Fast DOUBLE Unlimited Hosting (Video &amp; Web Hosting) + One Click Lead Popups with 300+ Ready To Use Templates + Commercial License to Serve Your Clients   <br><b>(92% Of Customers Pick Up This Add-on &amp; Save $1,000s Per Year!)</b></p>
                                       <p class="f-18 text-center text-md-start"> <span class="green-clr w600"> Just $9.93 One Time  (Regular $397)</span> </p>
                                       </div>
                                   </div>
                                 </div>
                                 
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-12 mt40 mt-md50 thanks-button text-center">
                        <a class="kapblue f-18 f-md-20 lh140 w400" href="https://warriorplus.com/o/nothanks/zwv1bj" target="_blank">
                           No Thanks, I Don't Want To Remove All Limitations To Generate 5X MORE Traffic, Leads and Passive Commissions With Virtually No Extra Efforts. Please take me to the next step to get access to my purchase.
                        </a>
                     </div>
                  </div>
               </div>
            </div>
            <!--10. Table Section End -->
            <!--Footer Section Start -->
            <div class="footer-section">
               <div class="container ">
                  <div class="row">
                     <div class="col-12 text-center">
                     <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 645.02 151.26" style="max-height:70px">
                                    <defs>
                                      <style>
                                        .cls-1 {
                                          fill: url(#linear-gradient-2);
                                        }
                                  
                                        .cls-2 {
                                          fill: #fff;
                                        }
                                  
                                        .cls-3 {
                                          fill-rule: evenodd;
                                        }
                                  
                                        .cls-3, .cls-4 {
                                          fill: #ff813b;
                                        }
                                  
                                        .cls-5 {
                                          fill: url(#linear-gradient-3);
                                        }
                                  
                                        .cls-6 {
                                          fill: url(#linear-gradient);
                                        }
                                  
                                        .cls-7 {
                                          fill: url(#linear-gradient-4);
                                        }
                                      </style>
                                      <linearGradient id="linear-gradient" x1="47.81" y1="75" x2="185.48" y2="75" gradientUnits="userSpaceOnUse">
                                        <stop offset="0" stop-color="#b956ff"></stop>
                                        <stop offset="1" stop-color="#6e33ff"></stop>
                                      </linearGradient>
                                      <linearGradient id="linear-gradient-2" x1="0" y1="75" x2="85.18" y2="75" gradientUnits="userSpaceOnUse">
                                        <stop offset="0" stop-color="#b956ff"></stop>
                                        <stop offset="1" stop-color="#a356fa"></stop>
                                      </linearGradient>
                                      <linearGradient id="linear-gradient-3" x1="55.61" y1="62.54" x2="62.86" y2="62.54" gradientUnits="userSpaceOnUse">
                                        <stop offset="0" stop-color="#b956ff"></stop>
                                        <stop offset="1" stop-color="#ac59fa"></stop>
                                      </linearGradient>
                                      <linearGradient id="linear-gradient-4" x1="54.62" y1="79" x2="61.87" y2="79" gradientTransform="translate(19.64 -11.24) rotate(13.24)" xlink:href="#linear-gradient-3"></linearGradient>
                                    </defs>
                                    <g id="Layer_1-2" data-name="Layer 1">
                                      <g>
                                        <g>
                                          <g>
                                            <path class="cls-6" d="m181.49,58.53h-10.72c-1.4-5.13-3.43-9.99-6.01-14.51l7.58-7.58c1.56-1.56,1.56-4.09,0-5.65l-17.65-17.65c-1.56-1.56-4.09-1.56-5.65,0l-7.58,7.58c-4.51-2.58-9.38-4.61-14.51-6.01V4c0-2.21-1.79-4-4-4h-24.96c-2.21,0-4,1.79-4,4v10.72c-.16.04-.31.09-.47.14-.27.08-.53.15-.8.23-.27.08-.55.16-.82.24-.55.17-1.09.35-1.63.53-.11.04-.22.08-.33.12-.48.17-.96.34-1.44.52-.12.04-.24.09-.36.14-.52.2-1.04.4-1.56.62-.04.02-.08.03-.12.05-2.41,1-4.74,2.15-6.99,3.43l-7.57-7.57c-1.56-1.56-4.09-1.56-5.65,0l-17.65,17.65c-.36.36-.63.76-.82,1.2h61.99v.02c.23,0,.45-.02.68-.02,23.76,0,43.01,19.26,43.01,43.01s-19.26,43.01-43.01,43.01c-.5,0-1-.02-1.5-.04v.04h-61.17c.19.43.46.84.82,1.2l17.65,17.65c1.56,1.56,4.09,1.56,5.65,0l7.57-7.57c2.25,1.28,4.58,2.43,6.99,3.43.04.02.08.03.12.05.51.21,1.03.42,1.55.62.12.05.24.09.36.14.47.18.95.35,1.43.52.11.04.23.08.34.12.54.18,1.08.36,1.63.53.27.08.55.16.83.25.26.08.53.16.79.23.16.04.31.09.47.14v10.72c0,2.21,1.79,4,4,4h24.96c2.21,0,4-1.79,4-4v-10.72c5.13-1.4,9.99-3.43,14.51-6.01l7.58,7.58c1.56,1.56,4.09,1.56,5.65,0l17.65-17.65c1.56-1.56,1.56-4.09,0-5.65l-7.58-7.58c2.58-4.51,4.61-9.38,6.01-14.51h10.72c2.21,0,4-1.79,4-4v-24.96c0-2.21-1.79-4-4-4Z"></path>
                                            <path class="cls-1" d="m76.64,101.54c-5.74-7.31-9.18-16.52-9.18-26.54s3.44-19.23,9.18-26.54c2.45-3.13,5.33-5.9,8.53-8.24h-48.39c-1.31-1.89-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.13h17.07c-1.51,3.21-2.76,6.58-3.71,10.07h3.1c1.31-1.89,3.49-3.13,5.97-3.13,4.01,0,7.25,3.25,7.25,7.25s-3.22,7.23-7.21,7.25h-.04c-2.48,0-4.66-1.24-5.97-3.13H13.22c-.44-.64-.98-1.19-1.59-1.65-1.21-.92-2.73-1.48-4.38-1.48-4,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.14h38.82c1.31-1.89,3.49-3.13,5.97-3.13.43,0,.84.05,1.25.12,3.41.59,6,3.56,6,7.14,0,2.78-1.56,5.19-3.86,6.41-1.01.53-2.16.84-3.39.84-2.48,0-4.66-1.24-5.97-3.13h-17.85c-1.31-1.9-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.13h15.96c.95,3.48,2.2,6.85,3.71,10.07h-5.63c-1.31-1.89-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25,0,1.53.48,2.95,1.28,4.12,1.31,1.9,3.49,3.14,5.97,3.14s4.66-1.24,5.97-3.14h36.95c-3.21-2.34-6.08-5.11-8.53-8.24ZM30.64,47.97c-2,0-3.62-1.62-3.62-3.62s1.62-3.62,3.62-3.62,3.63,1.62,3.63,3.62-1.63,3.62-3.63,3.62Zm-23.39,26.47c-2,0-3.62-1.63-3.62-3.62s1.63-3.63,3.62-3.63,3.63,1.63,3.63,3.63-1.63,3.62-3.63,3.62Zm20.97,16.45c-2,0-3.63-1.63-3.63-3.63s1.63-3.62,3.63-3.62,3.62,1.63,3.62,3.62-1.63,3.63-3.62,3.63Zm14.08,18.39c-2,0-3.62-1.62-3.62-3.62s1.62-3.62,3.62-3.62,3.62,1.62,3.62,3.62-1.62,3.62-3.62,3.62Z"></path>
                                            <circle class="cls-5" cx="59.23" cy="62.54" r="3.63"></circle>
                                            <circle class="cls-7" cx="58.24" cy="79" r="3.63" transform="translate(-16.54 15.44) rotate(-13.24)"></circle>
                                          </g>
                                          <g>
                                            <g>
                                              <path class="cls-3" d="m104.63,78.16c-9.44,6-14.52,2.86-15.25-9.4,4.93,3.38,10.02,6.52,15.25,9.4Z"></path>
                                              <path class="cls-3" d="m131.01,68.76c-.72,12.26-5.81,15.4-15.25,9.4,5.24-2.88,10.32-6.02,15.25-9.4Z"></path>
                                            </g>
                                            <path class="cls-4" d="m110.16,40.13c-.89,0-1.78.03-2.66.1-.08,0-.16,0-.24.02-9.59.79-18.07,5.45-23.89,12.42-.25.3-.49.59-.73.89-.14.19-.29.38-.43.57-4.02,5.37-6.52,11.93-6.88,19.07,0,.01,0,.03,0,.04-.01.22-.02.45-.03.68-.01.3-.02.6-.02.9v.18c0,.28,0,.55,0,.83.44,18.64,15.5,33.66,34.15,34.04.24,0,.48,0,.72,0,.3,0,.6,0,.9,0,18.85-.48,33.98-15.9,33.98-34.87s-15.61-34.88-34.88-34.88Zm.19,39.35c-14.41,14.37-29.15,1.23-29.15-9.45s13.05,0,29.15,0,29.15-10.67,29.15,0-14.74,23.81-29.15,9.45Z"></path>
                                          </g>
                                        </g>
                                        <g>
                                          <path class="cls-2" d="m286.65,118.51h-17.37l-39.32-59.42v59.42h-17.37V31.8h17.37l39.32,59.54V31.8h17.37v86.71Z"></path>
                                          <path class="cls-2" d="m304.95,38.69c-2.03-1.94-3.04-4.36-3.04-7.26s1.01-5.31,3.04-7.26c2.03-1.94,4.57-2.92,7.63-2.92s5.6.97,7.63,2.92c2.02,1.94,3.04,4.36,3.04,7.26s-1.02,5.31-3.04,7.26c-2.03,1.94-4.57,2.91-7.63,2.91s-5.6-.97-7.63-2.91Zm16.19,11.1v68.72h-17.37V49.79h17.37Z"></path>
                                          <path class="cls-2" d="m396.18,56.55c5.04,5.17,7.57,12.38,7.57,21.65v40.31h-17.36v-37.96c0-5.46-1.37-9.65-4.1-12.59-2.73-2.94-6.45-4.4-11.16-4.4s-8.58,1.47-11.35,4.4c-2.77,2.94-4.15,7.13-4.15,12.59v37.96h-17.37V49.79h17.37v8.56c2.31-2.98,5.27-5.31,8.87-7.01,3.6-1.69,7.55-2.54,11.85-2.54,8.19,0,14.8,2.59,19.85,7.75Z"></path>
                                          <path class="cls-2" d="m437.61,129.8c0,7.61-1.88,13.09-5.64,16.44-3.77,3.35-9.16,5.02-16.19,5.02h-7.69v-14.76h4.96c2.64,0,4.51-.52,5.58-1.55,1.07-1.03,1.61-2.71,1.61-5.02V49.79h17.37v80.01Zm-16.31-91.11c-2.03-1.94-3.04-4.36-3.04-7.26s1.01-5.31,3.04-7.26c2.02-1.94,4.61-2.92,7.75-2.92s5.58.97,7.57,2.92c1.98,1.94,2.98,4.36,2.98,7.26s-.99,5.31-2.98,7.26c-1.98,1.94-4.51,2.91-7.57,2.91s-5.73-.97-7.75-2.91Z"></path>
                                          <path class="cls-2" d="m454.42,65.42c2.77-5.37,6.53-9.51,11.29-12.4,4.76-2.89,10.07-4.34,15.94-4.34,5.13,0,9.61,1.04,13.46,3.1,3.85,2.07,6.92,4.67,9.24,7.82v-9.8h17.49v68.72h-17.49v-10.05c-2.23,3.23-5.31,5.89-9.24,8-3.93,2.11-8.46,3.16-13.58,3.16-5.79,0-11.06-1.49-15.82-4.47-4.76-2.98-8.52-7.17-11.29-12.59-2.77-5.41-4.16-11.64-4.16-18.67s1.38-13.11,4.16-18.48Zm47.45,7.88c-1.65-3.02-3.89-5.33-6.7-6.95-2.81-1.61-5.83-2.42-9.06-2.42s-6.2.79-8.93,2.36c-2.73,1.57-4.94,3.87-6.64,6.88-1.69,3.02-2.54,6.6-2.54,10.73s.85,7.75,2.54,10.85c1.69,3.1,3.93,5.48,6.7,7.13,2.77,1.66,5.72,2.48,8.87,2.48s6.24-.81,9.06-2.42c2.81-1.61,5.04-3.93,6.7-6.95,1.65-3.02,2.48-6.64,2.48-10.85s-.83-7.83-2.48-10.85Z"></path>
                                          <path class="cls-4" d="m591.93,102.01h-34.48l-5.71,16.5h-18.24l31.14-86.71h20.22l31.13,86.71h-18.36l-5.71-16.5Zm-4.71-13.89l-12.53-36.22-12.53,36.22h25.06Z"></path>
                                          <path class="cls-4" d="m645.02,31.93v86.58h-17.37V31.93h17.37Z"></path>
                                        </g>
                                      </g>
                                    </g>
                                  </svg>
                        <div editabletype="text" class="f-14 f-md-16 w400 mt20 lh160 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
                        <div class=" mt20 mt-md40 white-clr"> <script type="text/javascript" src="https://warriorplus.com/o2/disclaimer/zh5gzv" defer=""></script><div class="wplus_spdisclaimer f-17 w300 mt20 lh140 white-clr text-center"><strong>Disclaimer:</strong> <em>WarriorPlus is used to help manage the sale of products on this site. While WarriorPlus helps facilitate the sale, all payments are made directly to the product vendor and NOT WarriorPlus. Thus, all product questions, support inquiries and/or refund requests must be sent to the vendor. WarriorPlus's role should not be construed as an endorsement, approval or review of these products or any claim, statement or opinion used in the marketing of these products.</em></div></div>
                     </div>
                     <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                        <div class="f-14 f-md-16 w400 lh160 white-clr text-xs-center">Copyright © Ninja Ai</div>
                        <ul class="footer-ul w400 f-14 f-md-16 white-clr text-center text-md-right">
                           <li><a href="https://support.bizomart.com/hc/en-us" class="white-clr  t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                           <li><a href="http://getninjaai.com/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                           <li><a href="http://getninjaai.com/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                           <li><a href="http://getninjaai.com/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                           <li><a href="http://getninjaai.com/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                           <li><a href="http://getninjaai.com/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                           <li><a href="http://getninjaai.com/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
            <!--Footer Section End -->
         
        
<!-- Exit Popup -->	
<style>
</style>
<script type="text/javascript" src="assets/js/ouibounce.min.js"></script>
<div id="ouibounce-modal" style="z-index:9999999; display:none;">
<div class="underlay"></div>
<div class="modal-wrapper" style="display:block;">
   <div class="modal-bg">
   <button type="button" class="close" onclick="document.getElementById('ouibounce-modal').style.display = 'none';">
      X 
   </button>
   <div class="model-header">
   <div class="d-flex justify-content-center align-items-center gap-3">
         <img src="assets/images/hold.png" alt="" class="img-fluid d-block m131">
         <div>
            <div class="f-md-56 f-24 w700 lh130 white-clr">
               WAIT! HOLD ON!!
            </div>
            <div class="f-md-28 f-18 w700 lh130 black-clr mt10 mt-md0" style="background-color:yellow; padding:5px 10px; display:inline-block">
               Don't Leave Empty Handed
            </div>
         </div>
      </div>
   </div>
      <div class="col-12 for-padding">
      <div class="copun-line f-md-45 f-16 w500 lh130 text-center black-clr mt10">
      You've Qualified For An <span class="w700 red-clr"><br> INSTANT $100 DISCOUNT</span>  
      </div>
      <div class="copun-line f-md-30 f-16 w700 lh130 text-center black-clr mt10" style="background-color:yellow; padding:5px 10px; display:inline-block">
      Regular Price <strike>$147</strike>,
      Now Only $47	
      </div>
       <div class="mt-md20 mt10 text-center">
         <a href="https://warriorplus.com/o2/buy/zh5gzv/zwv1bj/j2hdbw" class="cta-link-btn1">Grab SPECIAL Discount Now!
         <br>
         <span class="f-12 black-clr w600 text-uppercase">Act Now! Limited to First 100 Buyers Only.</span> </a>
       </div>
      <!-- <div class="mt-md20 mt10 text-center f-md-26 f-16 w900 lh150 text-center red-clr text-uppercase">
         Coupon Code EXPIRES IN:
      </div>
      <div class="timer-new">
         <div class="days" class="timer-label">
            <span id="days"></span> <span class="text">Days</span>
         </div> 
         <div class="hours" class="timer-label">
            <span id="hours"></span> <span  class="text">Hours</span>
         </div>
         <div class="days" class="timer-label">
            <span id="mins" ></span> <span class="text">Min</span>
         </div>
         <div class="secs" class="timer-label">
            <span id="secs" ></span> <span class="text">Sec</span>
         </div>
      </div> -->
   </div>
</div>
</div>
<script type="text/javascript">
   var timeInSecs;
   var ticker;
   function startTimer(secs) {
   timeInSecs = parseInt(secs);
  tick();
   //ticker = setInterval("tick()", 1000);
   }
   function tick() {
   var secs = timeInSecs;
   if (secs > 0) {
   timeInSecs--;
   } else {
   clearInterval(ticker);
   //startTimer(20 * 60); // 4 minutes in seconds
   }
   var days = Math.floor(secs / 86400);
   secs %= 86400;
   var hours = Math.floor(secs / 3600);
   secs %= 3600;
   var mins = Math.floor(secs / 60);
   secs %= 60;
   var pretty = ((days < 10) ? "0" : "") + days + ":" + ((hours < 10) ? "0" : "") + hours + ":" + ((mins < 10) ? "0" : "") + mins + ":" + ((secs < 10) ? "0" : "") + secs;
   document.getElementById("days").innerHTML = days
   document.getElementById("hours").innerHTML = hours
   document.getElementById("mins").innerHTML = mins
   document.getElementById("secs").innerHTML = secs;
   }
   
  //startTimer(60 * 60); 
   // 4 minutes in seconds
  
  function getCookie(cname) {
         var name = cname + "=";
         var decodedCookie = decodeURIComponent(document.cookie);
         var ca = decodedCookie.split(';');
         for(var i = 0; i <ca.length; i++) {
             var c = ca[i];
             while (c.charAt(0) == ' ') {
                 c = c.substring(1);
             }
             if (c.indexOf(name) == 0) {
                 return c.substring(name.length, c.length);
             }
         }
         return "";
     }


   var cnt = 60*60;

   function counter(){
    
      if(getCookie("cnt") > 0){
         cnt = getCookie("cnt");
      }

      cnt -= 1;
      document.cookie = "cnt="+ cnt;

      startTimer(getCookie("cnt"));
      //jQuery("#counter").val(getCookie("cnt"));

      if(cnt>0){
         setTimeout(counter,1000);
      }
   
   }
   
   counter();
  
  
   </script>-
   <script type="text/javascript">
      var _ouibounce = ouibounce(document.getElementById('ouibounce-modal'),{
      aggressive: true, //Making this true makes ouibounce not to obey "once per visitor" rule
      });
   </script>
   <!-- Exit Popup Ends-->
         <!--- timer end-->
<!-- Exit Popup and Timer End -->
</body>
</html>
<script> 
function myFunction() {
var checkBox = document.getElementById("check");
var buybutton = document.getElementById("buyButton");
if (checkBox.checked == true){

    buybutton.getAttribute("href");
    buybutton.setAttribute("href","https://warriorplus.com/o2/buy/zh5gzv/zwv1bj/hd3rgz");
} else {
    buybutton.getAttribute("href");
    buybutton.setAttribute("href","https://warriorplus.com/o2/buy/zh5gzv/zwv1bj/j2hdbw");
  }
}
</script>
<!Doctype html>
<html lang="en">
   <head>
   <head>
    <title>Ninja Ai | Free Gift</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=9">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">

    <meta name="title" content="Ninja Ai | Free Gift">
    <meta name="description" content="Download Your Free Gifts and Enjoy Your Extra Bonus.">
    <meta name="keywords" content="Ninja Ai | Free Gift">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta property="og:image" content="https://www.getninjaai.com/free-gift/thumbnail.png">
    <meta name="language" content="English">
    <meta name="revisit-after" content="1 days">
    <meta name="author" content="Ashu Kumar">

    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:title" content="Ninja Ai | Free Gift">
    <meta property="og:description" content="Download Your Free Gifts and Enjoy Your Extra Bonus.">
    <meta property="og:image" content="https://www.getninjaai.com/free-gift/thumbnail.png">

    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:title" content="Ninja Ai | Free Gift">
    <meta property="twitter:description" content="Download Your Free Gifts and Enjoy Your Extra Bonus.">
    <meta property="twitter:image" content="https://www.getninjaai.com/free-gift/thumbnail.png">

   <!-- Font-Family -->
    <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
    <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    <!-- Start Editor required -->
    <link rel="stylesheet" href="https://cdn.oppyotest.com/launches/sellero/common_assets/css/bootstrap.min.css" type="text/css">
	<!-- <link rel="stylesheet" type="text/css" href="assets/css/bonus-style.css"> -->
    <link rel="stylesheet" href="assets/css/style.css" type="text/css">
    <link rel="stylesheet" href="assets/css/aweberform.css" type="text/css">
    <script src="https://cdn.oppyotest.com/launches/sellero/common_assets/js/jquery.min.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- End -->
</head>
<body>
	<a href="#" id="scroll" style="display: block;"><span></span></a>
	<?php
		if(!isset($_GET['afflink'])){
		$_GET['afflink'] = 'https://getninjaai.com/special/';
		$_GET['name'] = 'Ashu Kumar';      
		}
	?>

	<div class="header-sec" id="product">
        <div class="container">
            <div class="row">
				<div class="col-12 text-center">
                    <div class="white-clr f-20 f-md-32">
                        Free Gifts by <span class="w600 orange-clr">Ashu Kumar's</span> for SuperVIP Customers
                    </div>
                    <div class="uy mt20 mt-md40">
                        <div class="f-20 f-md-24 lh160 w500 black-clr">
                            <span class="f-20 f-md-24 purple-clr w600">Congratulations!</span> you have also <span class="purple-clr w600">unlocked Special Exclusive coupon <br class="d-none d-md-block">  "NINJAAIMAX" </span> for my Lightning Fast Software <span class="purple-clr w600">"NinjaAi"</span> launch on <br class="d-none d-md-block"> <span class="purple-clr w600">26th June 2023  @ 11:00 AM EST</span>
                        </div>
                        <div class="f-20 f-md-24 lh130 w600 mt20 gn-box">Good News :</div>
                        <div class="f-20 f-md-24 lh160 w400 mt20 mt-md30 black-clr">
                            If you want any other <b>BONUSES</b> that are relevant for your business <br class="d-none d-md-block"> or <b>any niche</b> to increase more of your profits then, <b>please feel free <br class="d-none d-md-block"> to email</b> us at <a href="https://support.bizomart.com/hc/en-us" target="_blank" class="purple-clr">https://support.bizomart.com/hc/en-us</a>
                        </div>
                    </div>
                </div>
               	<div class="col-12 mt-md50 mt20 text-center">
				   <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 645.02 151.26" style="max-height:70px">
                              <defs>
                                <style>
                                  .cls-1 {
                                    fill: url(#linear-gradient-2);
                                  }
                            
                                  .cls-2 {
                                    fill: #fff;
                                  }
                            
                                  .cls-3 {
                                    fill-rule: evenodd;
                                  }
                            
                                  .cls-3, .cls-4 {
                                    fill: #ff813b;
                                  }
                            
                                  .cls-5 {
                                    fill: url(#linear-gradient-3);
                                  }
                            
                                  .cls-6 {
                                    fill: url(#linear-gradient);
                                  }
                            
                                  .cls-7 {
                                    fill: url(#linear-gradient-4);
                                  }
                                </style>
                                <linearGradient id="linear-gradient" x1="47.81" y1="75" x2="185.48" y2="75" gradientUnits="userSpaceOnUse">
                                  <stop offset="0" stop-color="#b956ff"></stop>
                                  <stop offset="1" stop-color="#6e33ff"></stop>
                                </linearGradient>
                                <linearGradient id="linear-gradient-2" x1="0" y1="75" x2="85.18" y2="75" gradientUnits="userSpaceOnUse">
                                  <stop offset="0" stop-color="#b956ff"></stop>
                                  <stop offset="1" stop-color="#a356fa"></stop>
                                </linearGradient>
                                <linearGradient id="linear-gradient-3" x1="55.61" y1="62.54" x2="62.86" y2="62.54" gradientUnits="userSpaceOnUse">
                                  <stop offset="0" stop-color="#b956ff"></stop>
                                  <stop offset="1" stop-color="#ac59fa"></stop>
                                </linearGradient>
                                <linearGradient id="linear-gradient-4" x1="54.62" y1="79" x2="61.87" y2="79" gradientTransform="translate(19.64 -11.24) rotate(13.24)" xlink:href="#linear-gradient-3"></linearGradient>
                              </defs>
                              <g id="Layer_1-2" data-name="Layer 1">
                                <g>
                                  <g>
                                    <g>
                                      <path class="cls-6" d="m181.49,58.53h-10.72c-1.4-5.13-3.43-9.99-6.01-14.51l7.58-7.58c1.56-1.56,1.56-4.09,0-5.65l-17.65-17.65c-1.56-1.56-4.09-1.56-5.65,0l-7.58,7.58c-4.51-2.58-9.38-4.61-14.51-6.01V4c0-2.21-1.79-4-4-4h-24.96c-2.21,0-4,1.79-4,4v10.72c-.16.04-.31.09-.47.14-.27.08-.53.15-.8.23-.27.08-.55.16-.82.24-.55.17-1.09.35-1.63.53-.11.04-.22.08-.33.12-.48.17-.96.34-1.44.52-.12.04-.24.09-.36.14-.52.2-1.04.4-1.56.62-.04.02-.08.03-.12.05-2.41,1-4.74,2.15-6.99,3.43l-7.57-7.57c-1.56-1.56-4.09-1.56-5.65,0l-17.65,17.65c-.36.36-.63.76-.82,1.2h61.99v.02c.23,0,.45-.02.68-.02,23.76,0,43.01,19.26,43.01,43.01s-19.26,43.01-43.01,43.01c-.5,0-1-.02-1.5-.04v.04h-61.17c.19.43.46.84.82,1.2l17.65,17.65c1.56,1.56,4.09,1.56,5.65,0l7.57-7.57c2.25,1.28,4.58,2.43,6.99,3.43.04.02.08.03.12.05.51.21,1.03.42,1.55.62.12.05.24.09.36.14.47.18.95.35,1.43.52.11.04.23.08.34.12.54.18,1.08.36,1.63.53.27.08.55.16.83.25.26.08.53.16.79.23.16.04.31.09.47.14v10.72c0,2.21,1.79,4,4,4h24.96c2.21,0,4-1.79,4-4v-10.72c5.13-1.4,9.99-3.43,14.51-6.01l7.58,7.58c1.56,1.56,4.09,1.56,5.65,0l17.65-17.65c1.56-1.56,1.56-4.09,0-5.65l-7.58-7.58c2.58-4.51,4.61-9.38,6.01-14.51h10.72c2.21,0,4-1.79,4-4v-24.96c0-2.21-1.79-4-4-4Z"></path>
                                      <path class="cls-1" d="m76.64,101.54c-5.74-7.31-9.18-16.52-9.18-26.54s3.44-19.23,9.18-26.54c2.45-3.13,5.33-5.9,8.53-8.24h-48.39c-1.31-1.89-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.13h17.07c-1.51,3.21-2.76,6.58-3.71,10.07h3.1c1.31-1.89,3.49-3.13,5.97-3.13,4.01,0,7.25,3.25,7.25,7.25s-3.22,7.23-7.21,7.25h-.04c-2.48,0-4.66-1.24-5.97-3.13H13.22c-.44-.64-.98-1.19-1.59-1.65-1.21-.92-2.73-1.48-4.38-1.48-4,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.14h38.82c1.31-1.89,3.49-3.13,5.97-3.13.43,0,.84.05,1.25.12,3.41.59,6,3.56,6,7.14,0,2.78-1.56,5.19-3.86,6.41-1.01.53-2.16.84-3.39.84-2.48,0-4.66-1.24-5.97-3.13h-17.85c-1.31-1.9-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.13h15.96c.95,3.48,2.2,6.85,3.71,10.07h-5.63c-1.31-1.89-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25,0,1.53.48,2.95,1.28,4.12,1.31,1.9,3.49,3.14,5.97,3.14s4.66-1.24,5.97-3.14h36.95c-3.21-2.34-6.08-5.11-8.53-8.24ZM30.64,47.97c-2,0-3.62-1.62-3.62-3.62s1.62-3.62,3.62-3.62,3.63,1.62,3.63,3.62-1.63,3.62-3.63,3.62Zm-23.39,26.47c-2,0-3.62-1.63-3.62-3.62s1.63-3.63,3.62-3.63,3.63,1.63,3.63,3.63-1.63,3.62-3.63,3.62Zm20.97,16.45c-2,0-3.63-1.63-3.63-3.63s1.63-3.62,3.63-3.62,3.62,1.63,3.62,3.62-1.63,3.63-3.62,3.63Zm14.08,18.39c-2,0-3.62-1.62-3.62-3.62s1.62-3.62,3.62-3.62,3.62,1.62,3.62,3.62-1.62,3.62-3.62,3.62Z"></path>
                                      <circle class="cls-5" cx="59.23" cy="62.54" r="3.63"></circle>
                                      <circle class="cls-7" cx="58.24" cy="79" r="3.63" transform="translate(-16.54 15.44) rotate(-13.24)"></circle>
                                    </g>
                                    <g>
                                      <g>
                                        <path class="cls-3" d="m104.63,78.16c-9.44,6-14.52,2.86-15.25-9.4,4.93,3.38,10.02,6.52,15.25,9.4Z"></path>
                                        <path class="cls-3" d="m131.01,68.76c-.72,12.26-5.81,15.4-15.25,9.4,5.24-2.88,10.32-6.02,15.25-9.4Z"></path>
                                      </g>
                                      <path class="cls-4" d="m110.16,40.13c-.89,0-1.78.03-2.66.1-.08,0-.16,0-.24.02-9.59.79-18.07,5.45-23.89,12.42-.25.3-.49.59-.73.89-.14.19-.29.38-.43.57-4.02,5.37-6.52,11.93-6.88,19.07,0,.01,0,.03,0,.04-.01.22-.02.45-.03.68-.01.3-.02.6-.02.9v.18c0,.28,0,.55,0,.83.44,18.64,15.5,33.66,34.15,34.04.24,0,.48,0,.72,0,.3,0,.6,0,.9,0,18.85-.48,33.98-15.9,33.98-34.87s-15.61-34.88-34.88-34.88Zm.19,39.35c-14.41,14.37-29.15,1.23-29.15-9.45s13.05,0,29.15,0,29.15-10.67,29.15,0-14.74,23.81-29.15,9.45Z"></path>
                                    </g>
                                  </g>
                                  <g>
                                    <path class="cls-2" d="m286.65,118.51h-17.37l-39.32-59.42v59.42h-17.37V31.8h17.37l39.32,59.54V31.8h17.37v86.71Z"></path>
                                    <path class="cls-2" d="m304.95,38.69c-2.03-1.94-3.04-4.36-3.04-7.26s1.01-5.31,3.04-7.26c2.03-1.94,4.57-2.92,7.63-2.92s5.6.97,7.63,2.92c2.02,1.94,3.04,4.36,3.04,7.26s-1.02,5.31-3.04,7.26c-2.03,1.94-4.57,2.91-7.63,2.91s-5.6-.97-7.63-2.91Zm16.19,11.1v68.72h-17.37V49.79h17.37Z"></path>
                                    <path class="cls-2" d="m396.18,56.55c5.04,5.17,7.57,12.38,7.57,21.65v40.31h-17.36v-37.96c0-5.46-1.37-9.65-4.1-12.59-2.73-2.94-6.45-4.4-11.16-4.4s-8.58,1.47-11.35,4.4c-2.77,2.94-4.15,7.13-4.15,12.59v37.96h-17.37V49.79h17.37v8.56c2.31-2.98,5.27-5.31,8.87-7.01,3.6-1.69,7.55-2.54,11.85-2.54,8.19,0,14.8,2.59,19.85,7.75Z"></path>
                                    <path class="cls-2" d="m437.61,129.8c0,7.61-1.88,13.09-5.64,16.44-3.77,3.35-9.16,5.02-16.19,5.02h-7.69v-14.76h4.96c2.64,0,4.51-.52,5.58-1.55,1.07-1.03,1.61-2.71,1.61-5.02V49.79h17.37v80.01Zm-16.31-91.11c-2.03-1.94-3.04-4.36-3.04-7.26s1.01-5.31,3.04-7.26c2.02-1.94,4.61-2.92,7.75-2.92s5.58.97,7.57,2.92c1.98,1.94,2.98,4.36,2.98,7.26s-.99,5.31-2.98,7.26c-1.98,1.94-4.51,2.91-7.57,2.91s-5.73-.97-7.75-2.91Z"></path>
                                    <path class="cls-2" d="m454.42,65.42c2.77-5.37,6.53-9.51,11.29-12.4,4.76-2.89,10.07-4.34,15.94-4.34,5.13,0,9.61,1.04,13.46,3.1,3.85,2.07,6.92,4.67,9.24,7.82v-9.8h17.49v68.72h-17.49v-10.05c-2.23,3.23-5.31,5.89-9.24,8-3.93,2.11-8.46,3.16-13.58,3.16-5.79,0-11.06-1.49-15.82-4.47-4.76-2.98-8.52-7.17-11.29-12.59-2.77-5.41-4.16-11.64-4.16-18.67s1.38-13.11,4.16-18.48Zm47.45,7.88c-1.65-3.02-3.89-5.33-6.7-6.95-2.81-1.61-5.83-2.42-9.06-2.42s-6.2.79-8.93,2.36c-2.73,1.57-4.94,3.87-6.64,6.88-1.69,3.02-2.54,6.6-2.54,10.73s.85,7.75,2.54,10.85c1.69,3.1,3.93,5.48,6.7,7.13,2.77,1.66,5.72,2.48,8.87,2.48s6.24-.81,9.06-2.42c2.81-1.61,5.04-3.93,6.7-6.95,1.65-3.02,2.48-6.64,2.48-10.85s-.83-7.83-2.48-10.85Z"></path>
                                    <path class="cls-4" d="m591.93,102.01h-34.48l-5.71,16.5h-18.24l31.14-86.71h20.22l31.13,86.71h-18.36l-5.71-16.5Zm-4.71-13.89l-12.53-36.22-12.53,36.22h25.06Z"></path>
                                    <path class="cls-4" d="m645.02,31.93v86.58h-17.37V31.93h17.37Z"></path>
                                  </g>
                                </g>
                              </g>
                            </svg>
               	</div>
                 <div class="col-12 mt-md30 mt20 f-md-50 f-28 w600 text-center white-clr lh140">
                  <span class="w700 orange-clr"> “ChatGPT Plugin” </span> Infiltrates Big <br class="d-none d-md-block">Guru’s Secret Funnels…
               </div>
               <div class="col-12 mt-md15 mt15 text-center">
                  <div class="f-22 f-md-28 w600 lh140 white-clr">
                  And Allowed Us To Swipe ONLY Profitable Funnels With One Click…
                  </div>
               </div>
               <div class="col-12 mt20 text-center">
                  <div class="f-22 f-md-28 w700 lh140 white-clr post-heading">
                  Banking Us $955.45 Daily In 27 Seconds                 
                  </div>
               </div>
               <div class="col-12 mt20 text-center">
                  <div class="f-18 f-md-20 w400 lh140 white-clr">
                  (Best Of All, It Comes With Built In FREE Traffic Generator That Sends Thousands Of<br class="d-none d-md-block"> Clicks Daily To Our AI-Funnels)
                  </div>
               </div>
               <div class="col-12 mt15 text-center">
                  <div class="f-18 f-md-20 w400 lh140 purple-bg white-clr">
                  Without Designing | Without Copywriting | Without Hosting | Without Ads
                  </div>
               </div>
				<div class="col-12 white-clr text-center">
					<div class="f-md-36 f-28 lh160 w700 mt30 white-clr">
						Download Your Free Gifts Below
					</div>
					<div class="mt10 f-20 f-md-32 lh160 ">
						<span class="tde">Enjoy Your Extra Bonus Below</span>
					</div>
				</div>
				<div class="col-12 mt20 mt-md50">
					<img src="assets/images/productbox.png" class="img-fluid d-block mx-auto img-animation" alt="Proudly Presenting...">
				</div>
			</div>

			<!-- Bonus-1 -->
			<div class="row mt20 mt-md100 align-items-center">
               	<div class="col-md-6">
                  	<div class="f-22 f-md-28 lh140 w700 white-clr">
                     	<div class="option-design mr10 mr-md20">Bonus 1</div>
                        <div class="f-20 f-md-32 w600 lh140 white-clr mt20 mt-md30">
                          Web Security Graphics 
                  	    </div>
                  	  </div>
                  	<div class="f-18 f-md-20 w600 lh140 white-clr mt20 mt-md30">
					             Many people process information better when it is presented in a visual manner. Use these web security graphics to add appeal to your blog posts, marketing, PLR content and so much more.
                  	</div>
                  	<div class="f-18 f-md-20 w400 lh140 white-clr mt20">
					             According to many Neuro-Linguistic Programming practitioners, human minds works visually in which our imagination, calculations and memories takes place.
                  	</div>
                 	<div class="f-18 f-md-20 w400 lh140 white-clr mt20">
				          	 The good thing to this is that, you can utilize this human attributes to your marketing efforts by simply providing people a good graphics presentation of your products or services.
                  	</div>
					<div class="f-md-22 w700 lh160 d-btn mt20 mt-md30">
                       <a href="https://tinyurl.com/3326hmkb" target="_blank">Download Now 🎁</a>
                    </div>
               	</div>
               	<div class="col-md-6 mt20 mt-md0">
                  	<img src="assets/images/bonus1.webp" alt="Bonus 1" class="mx-auto d-block img-fluid">
               	</div>
            </div>

			<!-- Bonus-2 -->
			<div class="row mt20 mt-md100 align-items-center">
               	<div class="col-md-6 order-md-2">
                  	<div class="f-22 f-md-28 lh140 w700 white-clr">
                     	<div class="option-design mr10 mr-md20">Bonus 2</div>
                        <div class="f-20 f-md-32 w600 lh140 white-clr mt20 mt-md30">
                            SVG Galaxy Extended 
                  	    </div>
                  	  </div>
                  	<div class="f-18 f-md-20 w600 lh140 white-clr mt20 mt-md30">
					              Expand Your SVG Galaxy Library And Create More Elegant Videos With The Extended Package! Get 1000’s more of SVGs in 20 modules to use in Your & your clients’ projects!
                  	</div>
                  	<div class="f-18 f-md-20 w400 lh140 white-clr mt20">
					              If you are already satisfied with the first SVG Galaxy package, well it's not the end of the offer as inside this new product package, you will receive another set of cool and professional video.
                  	</div>
                 	  <div class="f-18 f-md-20 w400 lh140 white-clr mt20">
					              Assuming that you already knew how powerful video is and how this media can be a huge help to your online business, this tool is really useful that can drive more attention and of course more sales.
                  	</div>
					          <div class="f-md-22 w700 lh160 d-btn mt20 mt-md30">
                       <a href="https://tinyurl.com/488534vp" target="_blank">Download Now 🎁</a>
                    </div>
               	</div>
               	<div class="col-md-6 mt20 mt-md0 order-md-1">
                  	<img src="assets/images/bonus2.webp" alt="Bonus 2" class="mx-auto d-block img-fluid">
               	</div>
            </div>

			<!-- Bonus-3 -->
			<div class="row mt20 mt-md100 align-items-center">
               	<div class="col-md-6">
                  	<div class="f-22 f-md-28 lh140 w700 white-clr">
                     	<div class="option-design mr10 mr-md20">Bonus 3</div>
                       <div class="f-20 f-md-32 w600 lh140 white-clr mt20 mt-md30">
                          Money Graphics Pack 
                  	    </div>
                  	</div>
                  	<div class="f-18 f-md-20 w500 lh140 white-clr mt20 mt-md30">
					             This graphics pack has 150 money related images. The images will be delivered in PNG and Vector PDF format!
                  	</div>
                  	<div class="f-18 f-md-20 w400 lh140 white-clr mt20">
					             If you are an online entrepreneur selling digital products, a blogger, affiliate marketer, freelance web graphics designer having a pre-made graphics that are relevant to your needs or your client's needs will give you more time to other things that will make your more productive.
                  	</div>
					          <div class="f-18 f-md-20 w400 lh140 white-clr mt20">
					              Images are indeed powerful to persuade your reader's understanding and attention for you to make them turn into a customer to the products that you promote or sell online.
                  	</div>
				          	<div class="f-md-22 w700 lh160 d-btn mt20 mt-md30">
                       <a href="https://tinyurl.com/mvkf5fy4" target="_blank">Download Now 🎁</a>
                    </div>
               	</div>
               	<div class="col-md-6 mt20 mt-md0">
                  	<img src="assets/images/bonus3.webp" alt="Bonus 3" class="mx-auto d-block img-fluid">
               	</div>
            </div>

			<!-- Bonus-4 -->
			<div class="row mt20 mt-md100 align-items-center">
               	<div class="col-md-6 order-md-2">
                  	<div class="f-22 f-md-28 lh140 w700 white-clr">
                     	<div class="option-design mr10 mr-md20">Bonus 4</div>
                       <div class="f-20 f-md-32 w600 lh140 white-clr mt20 mt-md30">
                       50 Adsense Ready Wordpress Templates 
                  	    </div>
                  	</div>
                  	<div class="f-18 f-md-20 w500 lh140 white-clr mt20 mt-md30">
					             Finally, All Your Niche Wordpress Blogs Can Look Cool & Different - You Can Easily Spice Up Your Niche Wordpress Blogs, Giving Them A Unique & More Responsive Look!
                  	</div>
                  	<div class="f-18 f-md-20 w500 lh140 white-clr mt20">
					             The look and feel of your blog matters to your blog vistors. Not only are they interested in valuable information you provide, but the presentation and overall look of your blog will also help to make them repeat visitors as well as more responsive to any offers or ads you may have on your blogs.
                  	</div>
					           <div class="f-18 f-md-20 w500 lh140 white-clr mt20">
					              Presenting 50 Adsense Ready Professional Niche Wordpress Templates - simple easy to follow step-by-step instructions are included with this package!
                  	</div>
				          	<div class="f-md-22 w700 lh160 d-btn mt20 mt-md30">
                       <a href="https://tinyurl.com/4yaz9as9" target="_blank">Download Now 🎁</a>
                    </div>
               	</div>
               	<div class="col-md-6 mt20 mt-md0 order-md-1">
                  	<img src="assets/images/bonus4.webp" alt="Bonus 4" class="mx-auto d-block img-fluid">
               	</div>
            </div>

			<!-- Bonus-5 -->
			<div class="row mt20 mt-md100 align-items-center">
               	<div class="col-md-6">
                  	<div class="f-22 f-md-28 lh140 w700 white-clr">
                     	<div class="option-design mr10 mr-md20">Bonus 5</div>
                        <div class="f-20 f-md-32 w600 lh140 white-clr mt20 mt-md30">
                          Premium Presentation Template 
                  	    </div>
                  	</div>
                  	<div class="f-18 f-md-20 w500 lh140 white-clr mt20 mt-md30">
					               Are Tired Of Your Presentation Not Converting Or Getting Attention? No Designs Skills, Photoshop Or Expensive Complicated Software Needed!
                  	</div>
                  	<div class="f-18 f-md-20 w400 lh140 white-clr mt20">
					               Having a lot of traffic or leads to your business is nothing if it won't convert into paying customers. The thing is that, these people tend to judge your business model, services or opportunity depending how you present to them.
                  	</div>
                    <div class="f-18 f-md-20 w400 lh140 white-clr mt20">
                       One of the biggest factors to make these leads to convert is by having a professional presentation. And this starts in the looks of your professional presentation slides.
                  	</div>
			          		<div class="f-md-22 w700 lh160 d-btn mt20 mt-md30">
                       <a href="https://tinyurl.com/4dm4h5xs" target="_blank">Download Now 🎁</a>
                    </div>
               	</div>
               	<div class="col-md-6 mt20 mt-md0">
                  	<img src="assets/images/bonus5.webp" alt="Bonus 5" class="mx-auto d-block img-fluid">
               	</div>
            </div>

			<!-- Bonus-6 -->
			<div class="row mt20 mt-md100 align-items-center">
               	<div class="col-md-6 order-md-2">
                  	<div class="f-22 f-md-28 lh140 w700 white-clr">
                     	<div class="option-design mr10 mr-md20">Bonus 6</div>
                       <div class="f-20 f-md-32 w600 lh140 white-clr mt20 mt-md30">
                       Beyond The Limit 
                  	    </div>
                  	</div>
                  	<div class="f-18 f-md-20 w500 lh140 white-clr mt20 mt-md30">
					             Finding perseverance in your daily life can be hard without external resources. You need to make sure you have materials to help you get you started on your journey. 
                  	</div>
                  	<div class="f-18 f-md-20 w400 lh140 white-clr mt20">
					             Whether you are experiencing a major setback now or just preparing yourself for the future, you can never start working on the power of perseverance too early. Emotional resilience is also key when it comes to persevering. 
                  	</div>
					          <div class="f-18 f-md-20 w400 lh140 white-clr mt20">
					              Your relationship with your mind is the most important thing to keep in mind. You need to develop a spirit of mental toughness and resiliency to be able to preserve and push yourself beyond the limit.
                  	</div>
				          	<div class="f-md-22 w700 lh160 d-btn mt20 mt-md30">
                       <a href="https://tinyurl.com/6xr82a8u" target="_blank">Download Now 🎁</a>
                    </div>
               	</div>
               	<div class="col-md-6 mt20 mt-md0 order-md-1">
                  	<img src="assets/images/bonus6.webp" alt="Bonus 6" class="mx-auto d-block img-fluid">
               	</div>
            </div>

			<!-- Bonus-7 -->
			        <div class="row mt20 mt-md100 align-items-center">
               	<div class="col-md-6">
                  	<div class="f-22 f-md-28 lh140 w700 white-clr">
                     	<div class="option-design mr10 mr-md20">Bonus 7</div>
                        <div class="f-20 f-md-32 w600 lh140 white-clr mt20 mt-md30">
                          Insta Niche Headers 
                  	    </div>
                  	</div>
                  	<div class="f-18 f-md-20 w500 lh140 white-clr mt20 mt-md30">
					             Over 100 niche headers for you to choose from - use with your blog, sales letter, squeeze page, landing page, etc!
                  	</div>
                  	<div class="f-18 f-md-20 w400 lh140 white-clr mt20">
					            Images are often use to attract online audience. This is because of the fluid and amazing designs that graphic artists do. If you own a squeeze page, landing page, salespage or a blog, header is one of the most important component of your website.
                  	</div>
					          <div class="f-18 f-md-20 w400 lh140 white-clr mt20">
					             And because of that, successful internet marketers used to hire someone to create a custom header that will suit their needs and of course can capture the attention of their audience.
                  	</div>
				          	<div class="f-md-22 w700 lh160 d-btn mt20 mt-md30">
                       <a href="https://tinyurl.com/mtrh6hh" target="_blank">Download Now 🎁</a>
                    </div>
               	</div>
               	<div class="col-md-6 mt20 mt-md0">
                  	<img src="assets/images/bonus7.webp" alt="Bonus 7" class="mx-auto d-block img-fluid">
               	</div>
            </div>

			<!-- Bonus-8 -->
			        <div class="row mt20 mt-md100 align-items-center">
               	  <div class="col-md-6 order-md-2">
                  	<div class="f-22 f-md-28 lh140 w700 white-clr">
                     	<div class="option-design mr10 mr-md20">Bonus 8</div>
                       <div class="f-20 f-md-32 w600 lh140 white-clr mt20 mt-md30">
                          3D Man Characters 
                  	    </div>
                  	</div>
                  	<div class="f-18 f-md-20 w400 lh140 white-clr mt20 mt-md30">
					              <span class="w500">Wanna Make Your Presentation More Appealing?</span> Then! You Need 3D Male Character with Real, Transparent PNG, Vector Image and High Resolution with 3000 pixels x 3000 pixels
                  	</div>
					          <div class="f-md-22 w700 lh160 d-btn mt20 mt-md30">
                       <a href="https://tinyurl.com/jph9288z" target="_blank">Download Now 🎁</a>
                    </div>
               	  </div>
               	  <div class="col-md-6 mt20 mt-md0 order-md-1">
                  	<img src="assets/images/bonus8.webp" alt="Bonus 8" class="mx-auto d-block img-fluid">
               	  </div>
              </div>

			<!-- Bonus-9 -->
			        <div class="row mt20 mt-md100 align-items-center">
               	<div class="col-md-6">
                  <div class="f-22 f-md-28 lh140 w700 white-clr">     
                    <div class="option-design mr10 mr-md20">Bonus 9</div>
                      <div class="f-20 f-md-32 w600 lh140 white-clr mt20 mt-md30">
                        Power Of Visualization 
                  	  </div>
                  	</div>
                  	<div class="f-18 f-md-20 w600 lh140 white-clr mt20 mt-md30">
					              Visualization is the key to creating the life of your dreams that is full of happiness and accomplishment.
                  	</div>
                  	<div class="f-18 f-md-20 w500 lh140 white-clr mt20">
					              Visualization’ is the ultimate guide for those who want to use the unique power of their imagination to change the course of their life and design their future.
                  	</div>
					          <div class="f-18 f-md-20 w500 lh140 white-clr mt20">
					               Most importantly, this blueprint will help you overcome the common obstacles to successful visualization and teach you how to manifest your dreams into reality!
                  	</div>
					          <div class="f-md-22 w700 lh160 d-btn mt20 mt-md30">
                       <a href="https://tinyurl.com/3y3h2pca" target="_blank">Download Now 🎁</a>
                    </div>
               	</div>
               	<div class="col-md-6 mt20 mt-md0">
                  	<img src="assets/images/bonus9.webp" alt="Bonus 9" class="mx-auto d-block img-fluid">
               	</div>
            </div>

			<!-- Bonus-10 -->
			<div class="row mt20 mt-md100 align-items-center">
               	<div class="col-md-6 order-md-2">
                  	<div class="f-22 f-md-28 lh140 w700 white-clr">
                     	<div class="option-design mr10 mr-md20">Bonus 10</div>
                       <div class="f-20 f-md-32 w600 lh140 white-clr mt20 mt-md30">
                       3D Box Templates 
                  	  </div>
                  	</div>
                  	<div class="f-18 f-md-20 w400 lh140 white-clr mt20 mt-md30">
					              3D Box Templates allow you to create great looking eBox covers in just a few simple steps; <span class="w600">Upload template, Enter text, Set color and you're done!</span>
                  	</div>
                  	<div class="f-18 f-md-20 w400 lh140 white-clr mt20">
					             This package comes with 5 eBox templates.
                  	</div>
                    <div class="f-18 f-md-20 w400 lh140 white-clr mt20">
                       Each template comes in 3 versions; <span class="w600">with text, without text and with .psd source you you can make changes!</span>
                  	</div>
					          <div class="f-md-22 w700 lh160 d-btn mt20 mt-md30">
                       <a href="https://tinyurl.com/3md2vhz9" target="_blank">Download Now 🎁</a>
                    </div>
               	</div>
               	<div class="col-md-6 mt20 mt-md0 order-md-1">
                  	<img src="assets/images/bonus10.webp" alt="Bonus 10" class="mx-auto d-block img-fluid">
               	</div>
            </div>
		</div>
	</div>

	<!--Footer Section Start -->
	<div class="footer-section">
         <div class="container ">
            <div class="row">
               <div class="col-12 text-center">
			   <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 645.02 151.26" style="max-height:70px">
                              <defs>
                                <style>
                                  .cls-1 {
                                    fill: url(#linear-gradient-2);
                                  }
                            
                                  .cls-2 {
                                    fill: #fff;
                                  }
                            
                                  .cls-3 {
                                    fill-rule: evenodd;
                                  }
                            
                                  .cls-3, .cls-4 {
                                    fill: #ff813b;
                                  }
                            
                                  .cls-5 {
                                    fill: url(#linear-gradient-3);
                                  }
                            
                                  .cls-6 {
                                    fill: url(#linear-gradient);
                                  }
                            
                                  .cls-7 {
                                    fill: url(#linear-gradient-4);
                                  }
                                </style>
                                <linearGradient id="linear-gradient" x1="47.81" y1="75" x2="185.48" y2="75" gradientUnits="userSpaceOnUse">
                                  <stop offset="0" stop-color="#b956ff"></stop>
                                  <stop offset="1" stop-color="#6e33ff"></stop>
                                </linearGradient>
                                <linearGradient id="linear-gradient-2" x1="0" y1="75" x2="85.18" y2="75" gradientUnits="userSpaceOnUse">
                                  <stop offset="0" stop-color="#b956ff"></stop>
                                  <stop offset="1" stop-color="#a356fa"></stop>
                                </linearGradient>
                                <linearGradient id="linear-gradient-3" x1="55.61" y1="62.54" x2="62.86" y2="62.54" gradientUnits="userSpaceOnUse">
                                  <stop offset="0" stop-color="#b956ff"></stop>
                                  <stop offset="1" stop-color="#ac59fa"></stop>
                                </linearGradient>
                                <linearGradient id="linear-gradient-4" x1="54.62" y1="79" x2="61.87" y2="79" gradientTransform="translate(19.64 -11.24) rotate(13.24)" xlink:href="#linear-gradient-3"></linearGradient>
                              </defs>
                              <g id="Layer_1-2" data-name="Layer 1">
                                <g>
                                  <g>
                                    <g>
                                      <path class="cls-6" d="m181.49,58.53h-10.72c-1.4-5.13-3.43-9.99-6.01-14.51l7.58-7.58c1.56-1.56,1.56-4.09,0-5.65l-17.65-17.65c-1.56-1.56-4.09-1.56-5.65,0l-7.58,7.58c-4.51-2.58-9.38-4.61-14.51-6.01V4c0-2.21-1.79-4-4-4h-24.96c-2.21,0-4,1.79-4,4v10.72c-.16.04-.31.09-.47.14-.27.08-.53.15-.8.23-.27.08-.55.16-.82.24-.55.17-1.09.35-1.63.53-.11.04-.22.08-.33.12-.48.17-.96.34-1.44.52-.12.04-.24.09-.36.14-.52.2-1.04.4-1.56.62-.04.02-.08.03-.12.05-2.41,1-4.74,2.15-6.99,3.43l-7.57-7.57c-1.56-1.56-4.09-1.56-5.65,0l-17.65,17.65c-.36.36-.63.76-.82,1.2h61.99v.02c.23,0,.45-.02.68-.02,23.76,0,43.01,19.26,43.01,43.01s-19.26,43.01-43.01,43.01c-.5,0-1-.02-1.5-.04v.04h-61.17c.19.43.46.84.82,1.2l17.65,17.65c1.56,1.56,4.09,1.56,5.65,0l7.57-7.57c2.25,1.28,4.58,2.43,6.99,3.43.04.02.08.03.12.05.51.21,1.03.42,1.55.62.12.05.24.09.36.14.47.18.95.35,1.43.52.11.04.23.08.34.12.54.18,1.08.36,1.63.53.27.08.55.16.83.25.26.08.53.16.79.23.16.04.31.09.47.14v10.72c0,2.21,1.79,4,4,4h24.96c2.21,0,4-1.79,4-4v-10.72c5.13-1.4,9.99-3.43,14.51-6.01l7.58,7.58c1.56,1.56,4.09,1.56,5.65,0l17.65-17.65c1.56-1.56,1.56-4.09,0-5.65l-7.58-7.58c2.58-4.51,4.61-9.38,6.01-14.51h10.72c2.21,0,4-1.79,4-4v-24.96c0-2.21-1.79-4-4-4Z"></path>
                                      <path class="cls-1" d="m76.64,101.54c-5.74-7.31-9.18-16.52-9.18-26.54s3.44-19.23,9.18-26.54c2.45-3.13,5.33-5.9,8.53-8.24h-48.39c-1.31-1.89-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.13h17.07c-1.51,3.21-2.76,6.58-3.71,10.07h3.1c1.31-1.89,3.49-3.13,5.97-3.13,4.01,0,7.25,3.25,7.25,7.25s-3.22,7.23-7.21,7.25h-.04c-2.48,0-4.66-1.24-5.97-3.13H13.22c-.44-.64-.98-1.19-1.59-1.65-1.21-.92-2.73-1.48-4.38-1.48-4,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.14h38.82c1.31-1.89,3.49-3.13,5.97-3.13.43,0,.84.05,1.25.12,3.41.59,6,3.56,6,7.14,0,2.78-1.56,5.19-3.86,6.41-1.01.53-2.16.84-3.39.84-2.48,0-4.66-1.24-5.97-3.13h-17.85c-1.31-1.9-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.13h15.96c.95,3.48,2.2,6.85,3.71,10.07h-5.63c-1.31-1.89-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25,0,1.53.48,2.95,1.28,4.12,1.31,1.9,3.49,3.14,5.97,3.14s4.66-1.24,5.97-3.14h36.95c-3.21-2.34-6.08-5.11-8.53-8.24ZM30.64,47.97c-2,0-3.62-1.62-3.62-3.62s1.62-3.62,3.62-3.62,3.63,1.62,3.63,3.62-1.63,3.62-3.63,3.62Zm-23.39,26.47c-2,0-3.62-1.63-3.62-3.62s1.63-3.63,3.62-3.63,3.63,1.63,3.63,3.63-1.63,3.62-3.63,3.62Zm20.97,16.45c-2,0-3.63-1.63-3.63-3.63s1.63-3.62,3.63-3.62,3.62,1.63,3.62,3.62-1.63,3.63-3.62,3.63Zm14.08,18.39c-2,0-3.62-1.62-3.62-3.62s1.62-3.62,3.62-3.62,3.62,1.62,3.62,3.62-1.62,3.62-3.62,3.62Z"></path>
                                      <circle class="cls-5" cx="59.23" cy="62.54" r="3.63"></circle>
                                      <circle class="cls-7" cx="58.24" cy="79" r="3.63" transform="translate(-16.54 15.44) rotate(-13.24)"></circle>
                                    </g>
                                    <g>
                                      <g>
                                        <path class="cls-3" d="m104.63,78.16c-9.44,6-14.52,2.86-15.25-9.4,4.93,3.38,10.02,6.52,15.25,9.4Z"></path>
                                        <path class="cls-3" d="m131.01,68.76c-.72,12.26-5.81,15.4-15.25,9.4,5.24-2.88,10.32-6.02,15.25-9.4Z"></path>
                                      </g>
                                      <path class="cls-4" d="m110.16,40.13c-.89,0-1.78.03-2.66.1-.08,0-.16,0-.24.02-9.59.79-18.07,5.45-23.89,12.42-.25.3-.49.59-.73.89-.14.19-.29.38-.43.57-4.02,5.37-6.52,11.93-6.88,19.07,0,.01,0,.03,0,.04-.01.22-.02.45-.03.68-.01.3-.02.6-.02.9v.18c0,.28,0,.55,0,.83.44,18.64,15.5,33.66,34.15,34.04.24,0,.48,0,.72,0,.3,0,.6,0,.9,0,18.85-.48,33.98-15.9,33.98-34.87s-15.61-34.88-34.88-34.88Zm.19,39.35c-14.41,14.37-29.15,1.23-29.15-9.45s13.05,0,29.15,0,29.15-10.67,29.15,0-14.74,23.81-29.15,9.45Z"></path>
                                    </g>
                                  </g>
                                  <g>
                                    <path class="cls-2" d="m286.65,118.51h-17.37l-39.32-59.42v59.42h-17.37V31.8h17.37l39.32,59.54V31.8h17.37v86.71Z"></path>
                                    <path class="cls-2" d="m304.95,38.69c-2.03-1.94-3.04-4.36-3.04-7.26s1.01-5.31,3.04-7.26c2.03-1.94,4.57-2.92,7.63-2.92s5.6.97,7.63,2.92c2.02,1.94,3.04,4.36,3.04,7.26s-1.02,5.31-3.04,7.26c-2.03,1.94-4.57,2.91-7.63,2.91s-5.6-.97-7.63-2.91Zm16.19,11.1v68.72h-17.37V49.79h17.37Z"></path>
                                    <path class="cls-2" d="m396.18,56.55c5.04,5.17,7.57,12.38,7.57,21.65v40.31h-17.36v-37.96c0-5.46-1.37-9.65-4.1-12.59-2.73-2.94-6.45-4.4-11.16-4.4s-8.58,1.47-11.35,4.4c-2.77,2.94-4.15,7.13-4.15,12.59v37.96h-17.37V49.79h17.37v8.56c2.31-2.98,5.27-5.31,8.87-7.01,3.6-1.69,7.55-2.54,11.85-2.54,8.19,0,14.8,2.59,19.85,7.75Z"></path>
                                    <path class="cls-2" d="m437.61,129.8c0,7.61-1.88,13.09-5.64,16.44-3.77,3.35-9.16,5.02-16.19,5.02h-7.69v-14.76h4.96c2.64,0,4.51-.52,5.58-1.55,1.07-1.03,1.61-2.71,1.61-5.02V49.79h17.37v80.01Zm-16.31-91.11c-2.03-1.94-3.04-4.36-3.04-7.26s1.01-5.31,3.04-7.26c2.02-1.94,4.61-2.92,7.75-2.92s5.58.97,7.57,2.92c1.98,1.94,2.98,4.36,2.98,7.26s-.99,5.31-2.98,7.26c-1.98,1.94-4.51,2.91-7.57,2.91s-5.73-.97-7.75-2.91Z"></path>
                                    <path class="cls-2" d="m454.42,65.42c2.77-5.37,6.53-9.51,11.29-12.4,4.76-2.89,10.07-4.34,15.94-4.34,5.13,0,9.61,1.04,13.46,3.1,3.85,2.07,6.92,4.67,9.24,7.82v-9.8h17.49v68.72h-17.49v-10.05c-2.23,3.23-5.31,5.89-9.24,8-3.93,2.11-8.46,3.16-13.58,3.16-5.79,0-11.06-1.49-15.82-4.47-4.76-2.98-8.52-7.17-11.29-12.59-2.77-5.41-4.16-11.64-4.16-18.67s1.38-13.11,4.16-18.48Zm47.45,7.88c-1.65-3.02-3.89-5.33-6.7-6.95-2.81-1.61-5.83-2.42-9.06-2.42s-6.2.79-8.93,2.36c-2.73,1.57-4.94,3.87-6.64,6.88-1.69,3.02-2.54,6.6-2.54,10.73s.85,7.75,2.54,10.85c1.69,3.1,3.93,5.48,6.7,7.13,2.77,1.66,5.72,2.48,8.87,2.48s6.24-.81,9.06-2.42c2.81-1.61,5.04-3.93,6.7-6.95,1.65-3.02,2.48-6.64,2.48-10.85s-.83-7.83-2.48-10.85Z"></path>
                                    <path class="cls-4" d="m591.93,102.01h-34.48l-5.71,16.5h-18.24l31.14-86.71h20.22l31.13,86.71h-18.36l-5.71-16.5Zm-4.71-13.89l-12.53-36.22-12.53,36.22h25.06Z"></path>
                                    <path class="cls-4" d="m645.02,31.93v86.58h-17.37V31.93h17.37Z"></path>
                                  </g>
                                </g>
                              </g>
                            </svg>
                  <div editabletype="text" class="f-14 f-md-16 w400 mt20 lh160 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
                  <div class=" mt20 mt-md40 white-clr">
                     <script type="text/javascript" src="https://warriorplus.com/o2/disclaimer/rbhnkl" defer=""></script>
                     <div class="wplus_spdisclaimer"><strong>Disclaimer:</strong> <em>WarriorPlus is used to help manage the sale of products on this site. While WarriorPlus helps facilitate the sale, all payments are made directly to the product vendor and NOT WarriorPlus. Thus, all product questions, support inquiries and/or refund requests must be sent to the vendor. WarriorPlus's role should not be construed as an endorsement, approval or review of these products or any claim, statement or opinion used in the marketing of these products.</em></div>
                  </div>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-14 f-md-16 w400 lh160 white-clr text-xs-center">Copyright © NinjaAi</div>
                  <ul class="footer-ul w400 f-14 f-md-16 white-clr text-center text-md-right">
                     <li><a href="https://support.bizomart.com/hc/en-us" class="white-clr  t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://getninjaai.com/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://getninjaai.com/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://getninjaai.com/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://getninjaai.com/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://getninjaai.com/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://getninjaai.com/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
    <!--Footer Section End -->


</body>
</html>

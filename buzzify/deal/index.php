<!Doctype html>
<html>
   <head>
      <title>Buzzify Deal</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=9">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
      <meta name="title" content="Buzzify | Deal">
      <meta name="description" content="Buzzify">
      <meta name="keywords" content="Buzzify">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta property="og:image" content="https://www.buzzify.biz/deal/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Ayush Jain">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="Buzzify | Deal">
      <meta property="og:description" content="Buzzify">
      <meta property="og:image" content="https://www.buzzify.biz/deal/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="Buzzify | Deal">
      <meta property="twitter:description" content="Buzzify">
      <meta property="twitter:image" content="https://www.buzzify.biz/deal/thumbnail.png">
      <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Spartan:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
      <!-- Start Editor required -->
      <link rel="stylesheet" href="../common_assets/css/font-awesome.min.css" type="text/css">
      <link rel="stylesheet" href="../common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" href="../common_assets/css/general.css">
      <link rel="stylesheet" href="assets/css/style.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style-feature.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style-bottom.css" type="text/css">
      <link rel="stylesheet" href="assets/css/timer.css" type="text/css">
      <script src="../common_assets/js/jquery.min.js"></script>
      <script src="../common_assets/js/popper.min.js"></script>
      <script src="../common_assets/js/bootstrap.min.js"></script>
      <script>
         (function(w, i, d, g, e, t, s) {
             if (window.businessDomain != undefined) {
                 console.log("Your page have duplicate embed code. Please check it.");
                 return false;
             }
             businessDomain = 'buzzify';
             allowedDomain = 'buzzify.biz';
             if (!window.location.hostname.includes(allowedDomain)) {
                 console.log("Your page have not authorized. Please check it.");
                 return false;
             }
             console.log("Your script is ready...");
             w[d] = w[d] || [];
             t = i.createElement(g);
             t.async = 1;
             t.src = e;
             s = i.getElementsByTagName(g)[0];
             s.parentNode.insertBefore(t, s);
         })(window, document, '_gscq', 'script', 'https://cdn.staticdcp.com/apps/engage/smart_engage/js/config-loader.js');
      </script>
   </head>
   <body>
      <!-- Header Section Start -->
      <div class="header-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row">
                     <div class="col-md-3 text-center text-md-start">
                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 973.3 276.9" style="enable-background:new 0 0 973.3 276.9; max-height:40px;" xml:space="preserve">
                           <style type="text/css">
                              .st0{fill:#FFFFFF;}
                           </style>
                           <g>
                              <path class="st0" d="M0,212.5V12.1h98.6c19.3,0,34.4,4.2,45.3,12.7c10.9,8.5,16.4,21,16.4,37.7c0,20.8-8.6,35.5-25.7,44.1
                                 c14,2.2,24.8,7.6,32.2,16.1c7.5,8.5,11.2,19.7,11.2,33.6c0,11.7-2.6,21.8-7.9,30.2c-5.3,8.4-12.8,14.8-22.5,19.3
                                 c-9.7,4.5-21.2,6.7-34.5,6.7H0z M39.2,93.3h50.3c10.7,0,18.8-2.2,24.5-6.6c5.6-4.4,8.4-10.7,8.4-19c0-8.3-2.8-14.6-8.3-18.8
                                 c-5.5-4.2-13.7-6.3-24.6-6.3H39.2V93.3z M39.2,181.9h60.6c12.3,0,21.6-2.2,27.7-6.7c6.2-4.5,9.3-11.2,9.3-20.2
                                 c0-8.9-3.2-15.8-9.5-20.6c-6.4-4.8-15.5-7.2-27.5-7.2H39.2V181.9z"/>
                              <path class="st0" d="M268.1,216.4c-22.8,0-39.1-4.5-49.1-13.5c-10-9-15-23.6-15-44V88.2h37.3v61.9c0,13.1,2,22.4,6,27.7
                                 c4,5.4,10.9,8.1,20.8,8.1c9.7,0,16.6-2.7,20.6-8.1c4-5.4,6.1-14.6,6.1-27.7V88.2h37.3V159c0,20.3-5,35-14.9,44
                                 C307.2,211.9,290.9,216.4,268.1,216.4z"/>
                              <path class="st0" d="M356.2,212.5l68.6-95.9h-58.7V88.2h121.6L419,184.1h64.5v28.4H356.2z"/>
                              <path class="st0" d="M500.3,212.5l68.6-95.9h-58.7V88.2h121.6l-68.7,95.9h64.5v28.4H500.3z"/>
                              <path class="st0" d="M679.8,62.3c-4.1,0-7.8-1-11.1-3c-3.4-2-6-4.7-8-8.1s-3-7.1-3-11.2c0-4,1-7.7,3-11c2-3.3,4.7-6,8-8
                                 c3.3-2,7-3,11.1-3c4,0,7.7,1,11.1,3c3.3,2,6,4.6,8,8c2,3.3,3,7,3,11c0,4.1-1,7.8-3,11.2c-2,3.4-4.7,6.1-8,8.1
                                 C687.5,61.3,683.8,62.3,679.8,62.3z M661.2,212.5V88.2h37.3v124.4H661.2z"/>
                              <path class="st0" d="M747.9,212.5v-96.1h-16.8V88.2h16.8V57.3c0-11.7,1.8-21.9,5.5-30.5c3.6-8.6,8.9-15.2,15.7-19.9
                                 c6.8-4.7,15-7,24.6-7c5.9,0,11.7,0.9,17.5,2.7c5.7,1.8,10.7,4.4,14.8,7.6l-12.9,26.1c-3.8-2.3-8.1-3.5-12.9-3.5
                                 c-4.2,0-7.4,1.1-9.6,3.2c-2.2,2.1-3.7,5.1-4.4,8.9c-0.8,3.8-1.2,8.2-1.2,13.1v30.1h29.4v28.3h-29.4v96.1H747.9z"/>
                              <path class="st0" d="M835.6,275.7l40.1-78.7l-59-108.8h42.7l38,72.9l33.2-72.9h42.7l-95,187.5H835.6z"/>
                           </g>
                        </svg>
                     </div>
                     <div class="col-md-7 mt20 mt-md5">
                        <ul class="leader-ul f-md-18 f-16 w500 white-clr text-md-end text-center">
                           <li><a href="#features" class="white-clr t-decoration-none">Features</a><span class="pl9 white-clr">|</span></li>
                           <li><a href="#demo" class="white-clr t-decoration-none">Demo</a></li>
                           <li class="d-md-none affiliate-link-btn"><a href="https://warriorplus.com/o2/buy/fmtj72/gk3xc1/x4080g" class="white-clr t-decoration-none ">Buy Now</a></li>
                        </ul>
                     </div>
                     <div class="col-md-2 affiliate-link-btn text-md-end text-center mt15 mt-md0 d-none d-md-block">
                        <a href="https://warriorplus.com/o2/buy/fmtj72/gk3xc1/x4080g">Buy Now</a>
                     </div>
                  </div>
               </div>
			   	<div class="col-12 mt20 mt-md50"> <img src="assets/images/demo-vid1.png" class="img-fluid d-block mx-auto"></div>
               <div class="col-12 text-center lh150">
                  <div class="pre-heading f-md-20 f-18 w600 lh150">
                     <div class="skew-con d-flex gap20 align-items-center ">
                        It’s Time to Cash In BIG Using The Power Of Trending Videos & Content For 2022
                     </div>
                  </div>
               </div>
               <div class="col-12 mt-md30 mt20 f-md-40 f-28 w600 text-center white-clr lh150">
				  EXPOSED- Breakthrough 3-Click Software That Uses The Power of Trending Videos & Content  <span class="under yellow-clr w800"> To Make $528/Day Over and Over</span>  Again 
               </div>
               <div class="col-12 mt-md25 mt20 f-18 f-md-21 w600 text-center lh150 yellow-clr">
                No Content Creation, No Editing, No Camera & No Tech Hassles Ever! Even Newbies Can Get Unlimited FREE Traffic and Steady PASSIVE Income…Every Day
               </div>
            </div>
            <div class="row d-flex align-items-center flex-wrap mt20 mt-md40">
               <div class="col-md-10 mx-auto col-12 mt20 mt-md0">
                  <div class="col-12 responsive-video">
                     <iframe src="https://buzzify.dotcompal.com/video/embed/satfj9nvaq" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                        box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Header Section End -->
      <!-- Header Section Start -->
      <div class="second-section">
         <div class="container">
            <div class="row">
               <div class="col-12 p-md0">
                  <div class="col-12 key-features-bg d-flex align-items-center flex-wrap">
                     <div class="row">
                        <div class="col-12 col-md-6">
                           <ul class="list-head pl0 m0 f-16 f-md-18 lh160 w400 white-clr text-capitalize">
								<li><span class="w600">Creates Beautiful & Self-Updating Sites </span>with Hot Trending Videos, Content, News & Articles </li>
								<li class="w600">Built-In 1-Click Traffic Generating System </li>
								<li class="w600">Puts Most Profitable Links on Your Websites using AI </li>
								<li>Legally Use Other’s Trending Content To Generate Automated Profits- <span class="w600">Works in Any Niche or TOPIC </span></li>
								<li>1-Click Social Media Automation – <span class="w600">People ONLY WANT TRENDY Topics These Days To Click.</span> </li>
								<li><span class="w600">100% SEO Friendly </span> Website and Built-In Remarketing System </li>
								<li>Complete Newbie Friendly And <span class="w600">Works Even If You Have No Prior Experience And Technical Skills</span> </li>
                           </ul>
                        </div>
                        <div class="col-12 col-md-6">
                           <ul class="list-head pl0 m0 f-16 f-md-18 lh160 w400 white-clr text-capitalize">                            							  
							<li><span class="w600">Set and Forget System</span> With Single Keyword – Set Rules To Find & Publish Trending Posts</li>
							<li><span class="w600">Automatically Translate Your Sites In 15+ Language</span> According To Location For More Traffic</li>
							<li><span class="w600">Make 5K-10K With Commercial License </span>That Allows You To Serve Your Clients & Charge Them</li>
							<li><span class="w600">Integration with Major Platforms</span> Including Autoresponders And Social Media Apps </li>
							<li><span class="w600">In-Built Content Spinner</span> to Make your Content Fresh and More Engaging </li>
							<li><span class="w600">A-Z Complete Video Training </span>Is Included To Make It Easier For You </li>
							<li><span class="w600">Limited-Time Special</span> Bonuses Worth $2285 If You Buy Today </li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md70">
               <!-- CTA Btn Section Start -->
               <div class="col-12">
                  <div class="d-flex align-items-center justify-content-center">
                     <img src="assets/images/limited-time-offer-banner.png" class="img-fluid d-block mx-auto">
                  </div>
                  <div class="f-20 f-md-26 w800 lh150 text-center mt15 black-clr">
                     Grab Buzzify Now at 80% Launch Special Discount!
                  </div>
                  <div class="f-18 f-md-20 w500 lh150 text-center mt20 black-clr">
                     (Free Commercial License + Low 1-Time Price + 8 Special Bonuses worth $2285)
                  </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center">
                        <a href="https://warriorplus.com/o2/buy/fmtj72/gk3xc1/x4080g" class="cta-link-btn">Get Instant Access To Buzzify</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                     <img src="assets/images/compaitable-with.png" class="img-responsive mx-xs-center md-img-right">
                     <div class="d-lg-block d-none visible-md px-md30"><img src="assets/images/v-line.png" class="img-fluid"></div>
                     <img src="assets/images/days-gurantee.png" class="img-responsive mx-xs-auto mt15 mt-md0">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Header Section End -->
      <!-- Step Section Start -->
      <!-- <div class="cta-section-white">
         <div class="container">
             <div class="row">
                 <div class="col-12">
                     <div class="d-flex align-items-center justify-content-center">
                         <img src="assets/images/limited-time-offer-banner.png" class="img-fluid d-block mx-auto">
                     </div>
                     <div class="f-20 f-md-26 w800 lh150 text-center mt15 white-clr">
                         Grab Buzzify Now at 80% Launch Special Discount!
                     </div>
                     <div class="f-18 f-md-20 w500 lh150 text-center mt20 white-clr">
                         (Free Commercial License + Low 1-Time Price + 8 Special Bonuses worth $2285)
                     </div>
                     <div class="row">
                         <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center">
                             <a href="https://warriorplus.com/o2/buy/fmtj72/gk3xc1/x4080g" class="cta-link-btn">Get Instant Access To Buzzify</a>
                         </div>
                     </div>
                     <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                         <img src="assets/images/compaitable-with1.png" class="img-responsive mx-xs-center md-img-right">
                         <div class="d-lg-block d-none visible-md px-md30"><img src="assets/images/v-line.png" class="img-fluid"></div>
                         <img src="assets/images/days-gurantee1.png" class="img-responsive mx-xs-auto mt15 mt-md0">
                     </div>
                 </div>
             </div>
         </div>
         </div> -->
      <!-- Step Section End -->
      <div class="no-installation">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-md-45 f-28 w700 lh150 text-capitalize text-center black-clr">
                     Drive MASSIVE Traffic & Sales to Any Offer, <br class="d-none d-md-block"> Page or Link <span class="w700 blue-clr">In Just 3 SIMPLE Steps…</span>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md60">
                  <div class="row">
                     <div class="col-12 col-md-4">
                        <div class="steps-block">
                           <div class="step-bg">
                              <div class="f-22 f-md-26 w700 white-clr">
                                 Step #1
                              </div>
                           </div>
                           <div class="p15 p-md30">
                              <img src="assets/images/step-1.png" alt="" class="img-fluid mx-auto d-block">
                              <div class="f-18 f-md-20 w700 black-clr lh150 mt20 mt-md30 text-center">
                               Insert Keyword
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-12 col-md-4 mt20 mt-md0">
                        <div class="steps-block">
                           <div class="step-bg">
                              <div class="f-22 f-md-26 w700 white-clr">
                                 Step #2
                              </div>
                           </div>
                           <div class="p15 p-md30">
                              <img src="assets/images/step-2.png" alt="" class="img-fluid mx-auto d-block">
                              <div class="f-18 f-md-20 w700 black-clr lh150 mt20 mt-md30 text-center">
                                Find Trending Videos & Content
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-12 col-md-4 mt20 mt-md0">
                        <div class="steps-block">
                           <div class="step-bg">
                              <div class="f-22 f-md-26 w700 white-clr">
                                 Step #3
                              </div>
                           </div>
                           <div class="p15 p-md30">
                              <img src="assets/images/step-3.png" alt="" class="img-fluid mx-auto d-block">
                              <div class="f-18 f-md-20 w700 black-clr lh150 mt20 mt-md30 text-center">
                                Sit Back, Relax & Profit 
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-12 mx-auto mt40 mt-md50">
                  <div class="row">
                     <div class="col-12 col-md-4 col-md-4">
                        <!-- <img src="assets/images/no1.png" class="img-fluid d-block mx-auto"> -->
                        <div class="w800 f-18 f-md-24 black-clr text-center lh150">
                           No Download/Installation
                        </div>
                     </div>
                     <div class="col-12 col-md-4 mt20 mt-md0">
                        <!--  <img src="assets/images/no2.png" class="img-fluid d-block mx-auto"> -->
                        <div class="w800 f-18 f-md-24 black-clr text-center lh150">
                           No Prior Knowledge
                        </div>
                     </div>
                     <div class="col-12 col-md-4 mt20 mt-md0">
                        <!-- <img src="assets/images/no3.png" class="img-fluid d-block mx-auto"> -->
                        <div class="w800 f-18 f-md-24 black-clr text-center lh150">
                           100% Beginners Friendly
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md40">
                  <div class="f-18 f-md-20 w500 text-center black-clr lh150">
                     In just a few clicks, you can drive endless free traffic and convert it into passive commissions, ad profits & sales for you on autopilot
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="proof-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-40 f-28 w700 lh150 text-center black-clr">
				  We Are Getting 6100+ Visitors As<br class="d-none d-md-block"> FREE TRAFFIC Monthly 100% Hands-Free
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
                  <img src="assets/images/proof.png" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 mt20 mt-md80">
                  <div class="f-md-40 f-28 w700 lh150 text-center black-clr">
                  & That Made Us $16,029 Last Month - That’s <span class="under"> $528 In Profits With This Traffic</span> Each & Every Day
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
                  <img src="assets/images/proof-03.png" class="img-fluid d-block mx-auto">
               </div>
            </div>
         </div>
      </div>
	  <div class="testimonial-section2">
        <div class="container ">
            <div class="row ">
               <div class="col-12">
                  <div class="f-md-45 f-28 lh150 w700 text-center black-clr">Checkout What Buzzify Early <br class="d-none d-md-block"> Users Have to SAY</div>
               </div>
            </div>
            <div class="row align-items-center">
                <div class="col-md-6 p-md0">
                    <img src="./assets/images/t1.png" alt="" class="img-fluid d-block mx-auto mt70 mt-md50">
                    <img src="./assets/images/t2.png" alt="" class="img-fluid d-block mx-auto mt30 mt-md50">
                </div>
                <div class="col-md-6 p-md0">
                    <img src="./assets/images/t3.png" alt="" class="img-fluid d-block mx-auto mt30 mt-md70">
                    <img src="./assets/images/t4.png" alt="" class="img-fluid d-block mx-auto mt30 mt-md50">
                </div>
            </div>
        </div>
    </div>
	  
      <!-- CTA Section Start -->
      <div class="cta-section-white">
         <div class="container">
            <div class="row">
               <!-- CTA Btn Section Start -->
               <div class="col-12">
                  <div class="d-flex align-items-center justify-content-center">
                     <img src="assets/images/limited-time-offer-banner.png" class="img-fluid d-block mx-auto">
                  </div>
                  <div class="f-20 f-md-26 w800 lh150 text-center mt15 white-clr">
                     Grab Buzzify Now at 80% Launch Special Discount!
                  </div>
                  <div class="f-18 f-md-20 w500 lh150 text-center mt20 white-clr">
                     (Free Commercial License + Low 1-Time Price + 8 Special Bonuses worth $2285)
                  </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center">
                        <a href="https://warriorplus.com/o2/buy/fmtj72/gk3xc1/x4080g" class="cta-link-btn">Get Instant Access To Buzzify</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                     <img src="assets/images/compaitable-with1.png" class="img-responsive mx-xs-center md-img-right">
                     <div class="d-lg-block d-none visible-md px-md30"><img src="assets/images/v-line.png" class="img-fluid"></div>
                     <img src="assets/images/days-gurantee1.png" class="img-responsive mx-xs-auto mt15 mt-md0">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- CTA Section End -->
      <div class="thatsall-section">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-10 mx-auto text-center">
                  <div class="f-30 f-md-50 w700 shape-testi1 purple-hd">Do You Know?</div>
               </div>              
			   
			   <div class="col-12 f-md-80 f-28 w700 yellow-clr lh140 text-center mt20 mt-md70">
                 Trending Videos & Content Is The WINNER In 2022 & Beyond... 
               </div>
			   <div class="col-12 text-center f-md-36 f-24 w700 white-clr lh150 mt20 mt-md10">
                 How many times have you watched these trendy videos?
               </div>
            </div>	
			<div class="row align-items-center mt20 mt-md30">
                <div class="col-md-4">
                    <img src="./assets/images/v1.png" alt="" class="img-fluid d-block mx-auto">
                </div>
				 <div class="col-md-4 mt20 mt-md0">
                    <img src="./assets/images/v2.png" alt="" class="img-fluid d-block mx-auto">
                </div>
				 <div class="col-md-4 mt20 mt-md0">
                    <img src="./assets/images/v3.png" alt="" class="img-fluid d-block mx-auto">
                </div>
				 <div class="col-md-4 mt20 mt-md50">
                    <img src="./assets/images/v4.png" alt="" class="img-fluid d-block mx-auto">
                </div>
				 <div class="col-md-4 mt20 mt-md50">
                    <img src="./assets/images/v5.png" alt="" class="img-fluid d-block mx-auto">
                </div>
				 <div class="col-md-4 mt20 mt-md50">
                    <img src="./assets/images/v6.png" alt="" class="img-fluid d-block mx-auto">
                </div>
            </div>
			
			 <div class="col-12 text-center f-md-36 f-24 w700 white-clr lh150 mt40 mt-md80">
                 How many times have you clicked & read these <br class="d-none d-md-block"> amazing headlines? 
               </div>
			   
			   <div class="row align-items-center mt20 mt-md30">
                <div class="col-md-4">
                    <img src="./assets/images/g1.png" alt="" class="img-fluid d-block mx-auto">
                </div>
				 <div class="col-md-4 mt20 mt-md0">
                    <img src="./assets/images/g2.png" alt="" class="img-fluid d-block mx-auto">
                </div>
				 <div class="col-md-4 mt20 mt-md0">
                    <img src="./assets/images/g3.png" alt="" class="img-fluid d-block mx-auto">
                </div>
            </div>
			
			
            <div class="row mt-md50 mt20 align-items-center">
              <!-- <div class="col-12 col-md-10 mx-auto">
                  <div class="avengame-bg">
                     <p class="f-20 f-md-30 w700 black-clr lh150">10 reasons why G.O.T season 8</p>
                     <div class="f-24 f-md-32 w500 black-clr lh150"> is going to change the series forever</div>
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
                  <div class="avengame-bg">
                     <p class="f-20 f-md-30 w700 black-clr lh150">We finally understand why Marvel</p>
                     <div class="f-24 f-md-32 w500 black-clr lh150">  started with iron man</div>
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
                  <div class="avengame-bg">
                     <p class="f-20 f-md-30 w700 black-clr lh150">One team who shouldn’t win</p>
                     <div class="f-24 f-md-32 w500 black-clr lh150">the Fortnite cup for sure</div>
                  </div>
               </div>-->
               <div class="col-12 text-center f-md-32 f-22 w600 white-clr lh150 white-clr lh150 mt20 mt-md50">
                 We all have clicked & read such viral post while surfing online regularly.<span class="under"> People only want trending videos, content, news & articles.</span> This is working like a miracle 
               </div>
			   
			   <div class="col-12 f-md-38 f-28 w700 yellow-clr lh150 text-center mt20 mt-md70">
                 Trending Content Is The WINNER In 2022 & Beyond... 
               </div>
               <div class="col-12 text-center f-md-20 f-18 w400 white-clr lh150 mt10">
                 The idea is simple, the more trending videos, content, news & articles you have, more traffic you get, more leads and commissions you generate. 
               </div>
            </div>
         </div>
      </div>
	   <div class="still-there-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-28 f-md-42 w700 lh150 text-center  black-clr">
                     And Do You Know How Much People Are Making Using The POWER of Trending Videos & Content? 
                  </div>
               </div>
            </div>
            <div class="row d-flex align-items-center flex-wrap mt-md50 mt30">
               <div class="col-8 mx-auto col-md-4">
                  <img src="assets/images/ryan.png" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 col-md-8 mt30 mt-md0">
                  <div class="f-md-24 f-20 lh150 w400 pl-md130 text-center text-md-start">
                     <span class="w600">Here’s “Ryan’s World” – </span> A 10-Year-Old Kid that has over 31.9M Subscribers with whooping <span class="w600">Net Worth of $32M... </span>
                  </div>
                  <div class="f-md-24 f-20 lh150 w400 pl-md130 text-center text-md-start mt20">
                     Yes! You heard me right... He is just 10-year-kid but making BIG.
                  </div>
                  <img src="assets/images/left-arrow.png" class="img-fluid d-none d-md-block mt10">
               </div>
            </div>
            <div class="row d-flex align-items-center flex-wrap mt-md50 mt30">
               <div class="col-8 mx-auto col-md-5 order-md-2">
                  <img src="assets/images/gary-vaynerchuk.png" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 col-md-7 mt30 mt-md0 order-md-1">
                  <div class="f-md-24 f-20 lh150 w400 text-center text-md-start">
                     Even Pro marketers are bidding heavily on trendy content:
                  </div>
                  <div class="f-md-28 f-24 lh150 w400 text-center text-md-start mt20">
                     <span class="w600">Mr. Gary Vaynerchuk,</span> an entrepreneur and business Guru who’s also known as the first wine guru has <span class="w600">Net Worth of $200M+</span>
                  </div>
                  <img src="assets/images/right-arrow.png" class="img-fluid d-none d-md-block mt10 ms-md-auto">
               </div>
            </div>
         </div>
      </div>
	   <!-- Suport Section Start -->
      <div class="support-section">
         <div class="container ">		 
            <div class="row">
               <div class="col-12 ">
                  <div class="f-md-32 f-24 w500 lh150 black-clr text-center ">
                     And Now You Will Be Shocked to Know How Much These 
                     <br><span class="blue-clr f-md-45 f-28 w700 d-block mt10 mt-md15 ">Viral Content Sites Make Online!</span>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50 ">
                  <div class="row no-gutters ">
                     <!-- first -->
                     <div class="col-12 col-md-4 ">
                        <div class="boxbg boxborder-rb boxborder top-first ">
                           <img src="assets/images/buzzfeed.png " class="img-fluid d-block mx-auto ">
                           <div class="f-20 f-md-22 w600 lh150 mt15 mt-md40 text-center black-clr ">
                              5 Billion Traffic<br>
                              <span class="green-clr w900">$300M </span>Revenue
                           </div>
                        </div>
                     </div>
                     <!-- second -->
                     <div class="col-12 col-md-4 ">
                        <div class="boxbg boxborder-rb boxborder ">
                           <img src="assets/images/upworthy.png " class="img-fluid d-block mx-auto ">
                           <div class="f-20 f-md-22 w600 lh150 mt15 mt-md40 text-center black-clr ">
                              5 Million Traffic<br>
                              <span class="green-clr w900">$10M </span>Revenue
                           </div>
                        </div>
                     </div>
                     <!-- third -->
                     <div class="col-12 col-md-4 ">
                        <div class="boxbg boxborder-b boxborder ">
                           <img src="assets/images/wittyfeed.png " class="img-fluid d-block mx-auto ">
                           <div class="f-20 f-md-22 w600 lh150 mt15 mt-md40 text-center black-clr ">
                              1 Million Traffic<br>
                              <span class="green-clr w900">$10M</span> Revenue
                           </div>
                        </div>
                     </div>
                     <!-- fourth -->
                     <div class="col-12 col-md-4 ">
                        <div class="boxbg boxborder-r boxborder ">
                           <img src="assets/images/viralnova.png " class="img-fluid d-block mx-auto ">
                           <div class="f-20 f-md-22 w600 lh150 mt15 mt-md40 text-center black-clr ">
                              1 Million Traffic<br>
                              <span class="green-clr w900">$100M</span> Revenue
                           </div>
                        </div>
                     </div>
                     <!-- fifth -->
                     <div class="col-12 col-md-4 ">
                        <div class="boxbg boxborder-r boxborder ">
                           <img src="assets/images/techcrunch.png " class="img-fluid d-block mx-auto ">
                           <div class="f-20 f-md-22 w600 lh150 mt15 mt-md40 text-center black-clr ">
                              25 Million Traffic<br>
                              <span class="green-clr w900">$25M</span> Revenue
                           </div>
                        </div>
                     </div>
                     <!-- sixth -->
                     <div class="col-12 col-md-4 ">
                        <div class="boxbg boxborder ">
                           <img src="assets/images/wp.png " class="img-fluid d-block mx-auto ">
                           <div class="f-20 f-md-22 w600 lh150 mt15 mt-md40 text-center black-clr ">
                              200 Million Traffic<br>
                              <span class="green-clr w900">$100M</span> Revenue
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Suport Section End -->
	  
      <!-- Suport Section End -->
      <div class="playbuzz-bg">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="black-design1">
                     <div class="f-30 f-md-50 w700 lh150 text-center white-clr skew-normal">
                        Impressive, right?
                     </div>
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md20">
               <div class="col-12  f-22 f-md-28 lh160 w500 text-center black-clr lh150">
                So, it can be safely stated that… <br>
                  <span class="f-24 f-md-36 w600 mt25 d-block">
				  Trending Videos, Content, News & Articles Is The BIGGEST, Traffic And Income Opportunity Right Now!</span>
               </div>
            </div>
         </div>
      </div>
      
      <div class="new-dcp-team-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-md-24 f-20 w600 lh150 black-clr">
                     Dear Struggling Marketer,
                  </div>
                  <div class="f-md-32 f-24 w700 lh150 blue-clr1 mt10">
                     From the desk of Ayush Jain and Pranshu Gupta
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50">
                  <div class="row d-flex align-items-center flex-wrap">
                     <div class="col-12 col-md-7">
                        <div class="f-md-20 f-18 w400 lh150 black-clr ">
                           I’m Ayush Jain along with my friend Pranshu Gupta.
                           <br><br>
                           We are happily living the internet lifestyle since last 10 years with full flexibility of working from anywhere at any time.<br><br>
                          Over time, we’ve learnt that to be successful online, you must get your hands on a fool proof system that works again and again and again.  
                        </div>
                        <div class="mt20 f-md-26 w600 f-22 lh150 black-clr">
                           And without a doubt that’s Trending Content.  
                        </div>
                        <div class="mt20">
                           <div class="f-md-20 f-18 w400 lh150 black-clr ">
                              Till now, you might already have discovered that <span class="w600">  trending content is an untapped goldmine to tap into oceans</span> of viral traffic that’s ready to be pounced upon to boost leads, sales & profits for your offers.
                             
                           </div>
                        </div>
                     </div>
                     <div class="col-md-5 col-12 text-center mt20 mt-md0">
                        <img src="assets/images/ayush-pranshu.png" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
	  
	  <div class="traffic-section">
	  <div class="container">
	   <div class="row">
	  
	  

				 <div class="col-12">
                    <div class="f-md-40 f-28 w700 lh130 text-capitalize text-center black-clr">
					And Getting 1000s of Visitors, Leads & Commissions Every Day Following This Proven System… 
                    </div>
                </div>
							    <div class="col-12 col-md-10 mx-auto mt20 mt-md30">
                  <img src="assets/images/proof2.png" class="img-fluid d-block mx-auto">
               </div>
			   
			    <div class="col-12 mt20 mt-md70">
                    <div class="f-md-36 f-28 w700 lh130 text-capitalize text-center black-clr">
Imagine Getting Results Like This Everyday On Autopilot
                    </div>
                </div>
				
				<div class="col-11 col-md-7 mt20 mt-md50 mx-auto">
				<video loop autoplay muted id="vid" class="w-100">
				<source type="video/mp4" src="assets/images/traffic.mp4"></source>
				<source type="video/ogg" src="assets/images/traffic.ogg"></source>
				</video>
				</div>
<div class="col-12 f-md-24 f-20 w400 lh150 black-clr">
 When it’s used wisely to reach out to your globally scattered audience, <span class="w600">the power it provides to your business IS TRULY MAGICAL.</span>
</div>				
            </div>
	  </div>
	  </div>
    
    <div class="problem-bg-section">
         <div class="container">
            <div class="row">
               <div class="col-12 px40 text-center">
                  <div class="f-28 f-md-45 w500 lh150 text-center black-clr skew-normal">
                     That’s A REAL Deal but
                  </div>
                  <div class="f-30 f-md-60 w700 lh150 text-center red-clr skew-normal">
                     But Here’s the Problem…
                  </div>
               </div>
            </div>
            <div class="row d-flex align-items-center flex-wrap mt20 mt-md70">
               <div class="col-12 problme-shape">
                  <div class="f-24 f-md-36 w700 lh150 yellow-clr text-center">
                     Creating Engaging Content Daily Is PAINFUL & Time-Consuming
                  </div>
                  <div class="f-20 f-md-22 w400 lh150 white-clr mt20 mt-md30">
                     <ul class="noneed-listing5 pl0">
                        <li><span class="w600">You need to research, plan, and write content daily.</span> And staying up-to-date with latest niche trends needs lots of effort </li>
                        <li><span class="w600">You Need To Be On Camera.</span> This can be a major blocker in case you are shy or introvert and have a fear that people would judge and laugh at your videos. </li>
                        <li><span class="w600">You need to learn COMPLEX video & audio editing skills.</span> Most software are complex and difficult to learn, especially if you are a non-technical guy like us. </li>
                     </ul>
                  </div>
               </div>
            </div>
            <div class="row d-flex align-items-center flex-wrap mt20 mt-md70">
               <div class="col-12 col-md-7 mt-md50">
                  <div class="f-24 f-md-36 w700 lh150 black-clr">
                     Buying Expensive Equipment Can Leave Your Back Accounts Dry
                  </div>
                  <div class="f-20 f-md-21 w400 lh150 black-clr mt10">
                     To even get started with first video, you need expensive equipment, like a
                  </div>
                  <div class="f-20 f-md-22 w400 lh150 black-clr mt20 mt-md30">
                     <ul class="noneed-listing3 pl0">
                        <li class="w600">Nice camera, </li>
                        <li class="w600">Microphone, and </li>
                        <li><span class="w600">Video-audio editing software</span> That would cost you THOUSANDS of dollars. </li>
                     </ul>
                  </div>
               </div>
               <div class="col-12 col-md-5">
                  <img src="assets/images/need-img.png" class="img-fluid d-block mx-auto">
               </div>
            </div>
            <div class="row d-flex align-items-center flex-wrap mt20 mt-md40">
               <div class="col-12 col-md-7 order-md-2">
                  <div class="f-24 f-md-36 w700 lh150 black-clr">
                     Driving TRAFFIC to your content is the BIGGEST challenge and costs hundreds of dollars
                  </div>
                  <div class="f-20 f-md-22 w400 lh150 black-clr mt20 mt-md30">
                     <ul class="noneed-listing3 pl0">
                        <li><span class="w600">You need to be consistent</span> with uploading videos every day for months until you see good visitors. </li>
                        <li><span class="w600">If you only have channels on YouTube, you’re 100% DEPENDING on them.</span> Many times, they Shut down accounts without any prior notice. That’s a scary stuff so you need FULL control on your business by
                           having self-growing video channels on Your own domains. 
                        </li>
                     </ul>
                  </div>
               </div>
               <div class="col-12 col-md-5 order-md-1">
                  <img src="assets/images/sad.png" class="img-fluid d-block mx-auto">
               </div>
            </div>
            <!-- <div class="row d-flex align-items-center flex-wrap mt20 mt-md70">
               <div class="col-12 col-md-7 mt-md50">
                  <div class="f-24 f-md-36 w700 lh150 black-clr">
                     Even If You Outsource Any Of Above Tasks!
                  </div>
                  <div class="f-20 f-md-22 w400 lh150 black-clr mt20 mt-md30">
                     <ul class="noneed-listing3 pl0">
                        <li class="w600">Firstly, it can cost $100-200/video to be created and </li>
                        <li><span class="w600">Then You Still Need to Work countless hours/week</span> - Finding Ideas, Recording Voiceovers, Editing Videos, Promoting Them </li>
                     </ul>
                  </div>
               </div>
               <div class="col-12 col-md-5">
                  <img src="assets/images/need-img1.png" class="img-fluid d-block mx-auto">
               </div>
               </div> -->
            <!--<div class="row d-flex align-items-center flex-wrap mt70 mt-md70">
               <div class="col-12 problme-shape1">
                  <div class="col-12 p0 text-center">
                     <div class="shape-testi5">
                        <div class="f-24 f-md-36 w600 lh150 text-center white-clr skew-normal">
                           Problems do not end here…
                        </div>
                     </div>
                  </div>
                  <div class="row d-flex align-items-center flex-wrap">
                     <div class="col-12 col-md-8 order-md-2">
                        <div class="f-20 f-md-22 w400 lh150 black-clr">
                           <ul class="noneed-listing4 pl0">
                              <li>There’s NO Surety Whether Your Videos Will Go VIRAL </li>
                              <li>You need to do everything manually and wait for things to happen </li>
                              <li>Devoting time equally to all tasks becomes almost IMPOSSIBLE </li>
                           </ul>
                        </div>
                     </div>
                     <div class="col-12 col-md-4 order-md-1">
                        <img src="assets/images/need-img2.png" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
            </div>-->
            <div class="row">
               <div class="col-12 f-24 f-md-34 w600 lh150 text-center black-clr mt20 mt-md50">
                  That’s why over 80% entrepreneurs fail during their first year & only a handful of people are successful at making it big.  
               </div>
			     <div class="col-12 f-20 f-md-22 w400 lh150 text-center black-clr mt20">
                  There’re tons of areas where marketers can face challenges which we had faced in our own success journey. 
               </div>
            </div>
            <div class="row mt20 mt-md50">
               <div class="col-12 text-center">
                  <div class="black-design11">
                     <div class="f-28 f-md-40 w700 lh150 text-center white-clr skew-normal">
                        BUT NOT ANYMORE!
                     </div>
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md20">
               <div class="col-12  f-20 f-md-24 lh160 w400 text-center black-clr lh150">
                 That is why we have created a software that allows you to bring massive traffic in just a couple of clicks, and you don’t have to waste any time on creating content, funnels, SEO, paid traffic, complex tools etc. once & for all. 
               </div>
            </div>
         </div>
      </div>
      <div class="proudly-section" id="product">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-36 f-24 w700 text-center white-clr lh150 px-md0">
                  Proudly Presenting...
               </div>
               <div class="col-12 mt-md30 mt20 text-center">
                  <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 973.3 276.9" style="enable-background:new 0 0 973.3 276.9; max-height:80px;" xml:space="preserve">
                     <style type="text/css">
                        .st0{fill:#FFFFFF;}
                     </style>
                     <g>
                        <path class="st0" d="M0,212.5V12.1h98.6c19.3,0,34.4,4.2,45.3,12.7c10.9,8.5,16.4,21,16.4,37.7c0,20.8-8.6,35.5-25.7,44.1
                           c14,2.2,24.8,7.6,32.2,16.1c7.5,8.5,11.2,19.7,11.2,33.6c0,11.7-2.6,21.8-7.9,30.2c-5.3,8.4-12.8,14.8-22.5,19.3
                           c-9.7,4.5-21.2,6.7-34.5,6.7H0z M39.2,93.3h50.3c10.7,0,18.8-2.2,24.5-6.6c5.6-4.4,8.4-10.7,8.4-19c0-8.3-2.8-14.6-8.3-18.8
                           c-5.5-4.2-13.7-6.3-24.6-6.3H39.2V93.3z M39.2,181.9h60.6c12.3,0,21.6-2.2,27.7-6.7c6.2-4.5,9.3-11.2,9.3-20.2
                           c0-8.9-3.2-15.8-9.5-20.6c-6.4-4.8-15.5-7.2-27.5-7.2H39.2V181.9z"/>
                        <path class="st0" d="M268.1,216.4c-22.8,0-39.1-4.5-49.1-13.5c-10-9-15-23.6-15-44V88.2h37.3v61.9c0,13.1,2,22.4,6,27.7
                           c4,5.4,10.9,8.1,20.8,8.1c9.7,0,16.6-2.7,20.6-8.1c4-5.4,6.1-14.6,6.1-27.7V88.2h37.3V159c0,20.3-5,35-14.9,44
                           C307.2,211.9,290.9,216.4,268.1,216.4z"/>
                        <path class="st0" d="M356.2,212.5l68.6-95.9h-58.7V88.2h121.6L419,184.1h64.5v28.4H356.2z"/>
                        <path class="st0" d="M500.3,212.5l68.6-95.9h-58.7V88.2h121.6l-68.7,95.9h64.5v28.4H500.3z"/>
                        <path class="st0" d="M679.8,62.3c-4.1,0-7.8-1-11.1-3c-3.4-2-6-4.7-8-8.1s-3-7.1-3-11.2c0-4,1-7.7,3-11c2-3.3,4.7-6,8-8
                           c3.3-2,7-3,11.1-3c4,0,7.7,1,11.1,3c3.3,2,6,4.6,8,8c2,3.3,3,7,3,11c0,4.1-1,7.8-3,11.2c-2,3.4-4.7,6.1-8,8.1
                           C687.5,61.3,683.8,62.3,679.8,62.3z M661.2,212.5V88.2h37.3v124.4H661.2z"/>
                        <path class="st0" d="M747.9,212.5v-96.1h-16.8V88.2h16.8V57.3c0-11.7,1.8-21.9,5.5-30.5c3.6-8.6,8.9-15.2,15.7-19.9
                           c6.8-4.7,15-7,24.6-7c5.9,0,11.7,0.9,17.5,2.7c5.7,1.8,10.7,4.4,14.8,7.6l-12.9,26.1c-3.8-2.3-8.1-3.5-12.9-3.5
                           c-4.2,0-7.4,1.1-9.6,3.2c-2.2,2.1-3.7,5.1-4.4,8.9c-0.8,3.8-1.2,8.2-1.2,13.1v30.1h29.4v28.3h-29.4v96.1H747.9z"/>
                        <path class="st0" d="M835.6,275.7l40.1-78.7l-59-108.8h42.7l38,72.9l33.2-72.9h42.7l-95,187.5H835.6z"/>
                     </g>
                  </svg>
               </div>
               <div class="f-md-32 f-22 w500 white-clr lh150 col-12 mt-md50 mt20 text-center">
                  A Breakthrough Software That Uses A Secret Method To Make Us $528/ Day Over And Over Again Using The Power Of Trending Videos & Content 
               </div>
               <div class="col-12 mt-md30 mt20">
                  <img src="assets/images/proudly.png" class="img-fluid d-block mx-auto">
               </div>
            </div>
         </div>
      </div>
    
      <div class="new-steps-bg">
         <div class="container">
            <div class="row">
               <div class="col-lg-12">
                  <div class="f-md-45 f-28 w700 lh150 text-capitalize text-center black-clr">
					 
					 Drive MASSIVE Traffic & Sales To Any Offer,<br class="d-none d-md-block">  Page Or Link <span class="w700 blue-clr">In Just 3 SIMPLE Steps… </span>
                  </div>
               </div>
               <div class="col-12 mt-md70 mt20">
                  <div class="row align-items-center">
                     <div class="col-12 col-md-6 shutacc-li vline-yellow align-self-center step-pl15px relative">
                        <img src="assets/images/step-left-border.png" class="step-left-boredr-image d-lg-block d-none">
                        <span class="f-md-26 f-20 w600 step-btn-style">Step #1</span>
                        <div class="mt-md40 mt20 blue-clr f-md-30 f-22 w700 lh150">Insert Keyword</div>
                        <div class="f-md-20 f-18 w400 lh150 mt10">
                          Just enter your keywords, Buzzify will find and post super engaging videos and articles (legally) and share on social media platforms automatically to bring targeted traffic using special tag management. 
                        </div>
                     </div>
                     <div class="col-12 col-md-6 align-self-center mt-md0 mt20">
                        <div class="step-image-back">
                           <img  src="assets/images/sm-steps1.png" class="img-fluid d-block mx-auto"/>
                        </div>
                     </div>
                  </div>
                  <div class="row align-items-center mt-md50 mt20">
                     <div class="col-12 col-md-6 order-md-2 shutacc-li vline-blue step-plr5px relative">
                        <img src="assets/images/step-right-border.png" class="step-right-boredr-image d-lg-block d-none">
                        <span class="f-md-26 f-20 w600 step-btn-style">Step #2</span>
                        <div class="mt-md40 mt20 blue-clr f-md-30 f-22 w700 lh150">Find Trending Videos & Content</div>
                        <div class="f-md-20 f-18 w400 lh150 mt10 pr-md50">
                          This system uses artificial intelligence to decide the best offer or lead grabber on your site (totally hosted on our servers) to make sure that you make profit out of every niche 
                        </div>
                     </div>
                     <div class="col-12 col-md-6 order-md-1 align-self-center mt-md0 mt20">
                        <div class="step-image-back1">
                           <img  src="assets/images/sm-steps2.png" class="img-fluid d-block mx-auto"/>
                        </div>
                     </div>
                  </div>
                  <div class="row align-items-center mt-md50 mt20">
                     <div class="col-12 col-md-6 shutacc-li vline-yellow align-self-center step-pl15px relative">
                        <img src="assets/images/step-left-border.png" class="step-left-boredr-image d-lg-block d-none">
                        <span class="f-md-26 f-20 w600 step-btn-style">Step #3</span>
                        <div class="mt-md40 mt20 blue-clr f-md-30 f-22 w700 lh150">Sit Back, Relax & Profit</div>
                        <div class="f-md-20 f-18 w400 lh150 mt10">
                          You can directly drive the free traffic generated to your affiliate offer, encash it using Amazon Ads or simply collect leads. Set rules once & forget it to work 24*7 for you. And you can create as many sites as you want 
                        </div>
                     </div>
                     <div class="col-12 col-md-6 align-self-center mt-md0 mt20">
                        <div class="step-image-back3">
                           <img  src="assets/images/sm-steps3.png" class="img-fluid d-block mx-auto"/>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Demo Section Start -->
      <div class="demo-section" id="demo">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-45 f-28 w700 lh150 text-center">
                  Watch Buzzify In Action
               </div>
               <div class="col-12 col-md-9 mx-auto mt15 mt-md50 relative">
                  <div class="col-12 responsive-video">
                     <iframe src="https://buzzify.dotcompal.com/video/embed/9skbs4inur" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                        box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe> 
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--!Demo Section End--->
      <!-- CTA Section Start -->
      <div class="cta-section-white">
         <div class="container">
            <div class="row">
               <!-- CTA Btn Section Start -->
               <div class="col-12">
                  <div class="d-flex align-items-center justify-content-center">
                     <img src="assets/images/limited-time-offer-banner.png" class="img-fluid d-block mx-auto">
                  </div>
                  <div class="f-20 f-md-26 w800 lh150 text-center mt15 white-clr">
                     Grab Buzzify Now at 80% Launch Special Discount!
                  </div>
                  <div class="f-18 f-md-20 w500 lh150 text-center mt20 white-clr">
                     (Free Commercial License + Low 1-Time Price + 8 Special Bonuses worth $2285)
                  </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center">
                        <a href="https://warriorplus.com/o2/buy/fmtj72/gk3xc1/x4080g" class="cta-link-btn">Get Instant Access To Buzzify</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                     <img src="assets/images/compaitable-with1.png" class="img-responsive mx-xs-center md-img-right">
                     <div class="d-lg-block d-none visible-md px-md30"><img src="assets/images/v-line.png" class="img-fluid"></div>
                     <img src="assets/images/days-gurantee1.png" class="img-responsive mx-xs-auto mt15 mt-md0">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- CTA Section End -->
	  <div class="testimonial-section">
        <div class="container ">
            <div class="row ">
                <div class="col-12 f-md-45 f-28 lh150 w700 text-center black-clr">
                  Here's What REAL Users Have To <br class="d-none d-md-block">Say About Buzzify
               </div>
            </div>
            <div class="row align-items-center">
                <div class="col-md-6 p-md0">
                    <img src="./assets/images/t5.png" alt="" class="img-fluid d-block mx-auto mt70 mt-md70">
                    <img src="./assets/images/t6.png" alt="" class="img-fluid d-block mx-auto mt70 mt-md50">
                </div>
                <div class="col-md-6 p-md0">
                    <img src="./assets/images/t7.png" alt="" class="img-fluid d-block mx-auto mt70 mt-md70">
                    <img src="./assets/images/t8.png" alt="" class="img-fluid d-block mx-auto mt70 mt-md50">
                </div>
            </div>
        </div>
    </div>
   
      <!-- Features 1 Section Start -->
      <div class="features-one" id="features">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="features-headline f-28 f-md-45 w700 lh150 white-clr">
                     Here Are The GROUND - BREAKING Features
                  </div>
                  <div class="f-28 f-md-45 w700 lh150 white-clr mt-md15 mt15">
                     That Makes Buzzify A CUT ABOVE The Rest
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50">
                  <div class="row d-flex align-items-center flex-wrap">
                     <div class="col-12 f-18 mt20 mt-md0 ">
                        <div class="f-22 f-md-32 w700 lh150 text-capitalize white-clr text-center">
                           Create unlimited self-updating sites with DFY hot trending content
                        </div>
                        <div class="f-18 f-md-20 w400 lh150 white-clr mt20">
                           Eye-catchy Sites packed with trendy content and engaging videos are the best way to bring visitors and convert them into customers. Buzzify gives you the power to create unlimited, beautifully designed & self-updating sites with hot trending content that’s fully ready to monetise the traffic with your own products or affiliate offers.
                        </div>
                     </div>
                     <div class="col-10 mx-auto mt-md40 mt20">
                        <img src="assets/images/feature1.png" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Features 1 Section End -->
      <!-- Features 1 Section Start -->
      <div class="features-two">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row d-flex align-items-center flex-wrap">
                     <div class="col-12 col-md-6 order-md-2 f-18 mt20 mt-md0 ">
                        <div class="f-22 f-md-32 w700 lh150 text-capitalize black-clr text-md-start text-center">
                           Find hot & trendy content in seconds for any niche and spin it to make it UNIQUE.
                        </div>
                        <div class="f-18 f-md-20 w400 lh150 black-clr mt15 text-md-start text-center">
                           Trendy content is the KING, and Buzzify helps you to fill your sites with trendy content that grabs eyeballs. Just enter your niche related keywords and BANG!!!
                           <br><br><span class="w500">You’ll be amazed to see fresh, popular and trending content & articles that gets visitors glued right on your desktops in less than 60 seconds.</span> Even modify the content using our Ultra-Fast & Super Easy Spinner and create engagement boosters sipping your HOT Cuppa!
                        </div>
                     </div>
                     <div class="col-12 col-md-6 order-md-1 mt20 mt-md0">
                        <img src="assets/images/feature2.png" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Features 1 Section End -->
      <!-- Features 1 Section Start -->
      <div class="features-three">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row d-flex align-items-center flex-wrap">
                     <div class="col-12 col-md-6 f-18 mt20 mt-md0 ">
                        <div class="f-22 f-md-32 w700 lh150 text-capitalize black-clr text-md-start text-center">
                           Legally use other people’s trendy videos and profit from it
                        </div>
                        <div class="f-18 f-md-20 w400 lh150 black-clr mt15 text-md-start text-center">
                           Videos are the best way to share more in few seconds. But creating them is not everyone’s cup of tea. With Buzzify, that’ll be an issue of the past.
                           <br><br> Just enter few keywords for your niche and you have all the HOT & Popular videos from the video sharing Giant, YouTube. The secret formula is 100% Legal and you can start PROFITING from Today!
                        </div>
                     </div>
                     <div class="col-12 col-md-6 mt20 mt-md0">
                        <img src="assets/images/feature3.png" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Features 1 Section End -->
      <!-- Features 2 Section Start -->
      <div class="features-two">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row d-flex align-items-center flex-wrap">
                     <div class="col-12 col-md-6 f-18 mt20 mt-md0 order-md-2">
                        <div class="f-22 f-md-32 w700 lh150 text-capitalize black-clr text-md-start text-center">
                           Make trendy content viral to drive tons of targeted traffic
                        </div>
                        <div class="f-18 f-md-20 w400 lh150 black-clr mt15">
                           It’s no secret that social media is the ULTIMATE way to give push to your trendy content for driving targeted traffic.
                           <br><br><span class="w500"> So, with Buzzify, you can easily share your articles & videos on TOP social platforms</span> without investing time & money on creating and optimizing posts. As you already have the Power of Viral Content added with engagement boosters.
                        </div>
                     </div>
                     <div class="col-12 col-md-6 mt20 mt-md0 order-md-1 fe-box-img">
                        <img src="assets/images/feature4.png" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Features 2 Section End -->
      <!-- Features 3 Section Start -->
      <div class="features-three">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row d-flex align-items-center flex-wrap">
                     <div class="col-12 col-md-6 f-18 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w700 lh150 text-capitalize black-clr text-md-start text-center">
                           Set and forget system with just one keyword
                        </div>
                        <div class="f-18 f-md-20 w400 lh150 black-clr mt15">
                           Gone are the days of doing stuff manually and wasting time and energy.
                           <br><br>Simply by settings rules once, you have the POWER to get TOP of the content each time flowing right to you as soon as they are published. Buzzify has built-in settings for automating content curation from Top & trusted web publishers.
                           <br><br>Syndicate your selected social accounts and be the first one to share viral content to your subscribers and make them loyal to your business.
                        </div>
                     </div>
                     <div class="col-12 col-md-6 mt20 mt-md0 order-md-2 fe-box-img">
                        <img src="assets/images/feature5.png" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Features 3 Section End -->
      <!-- Features 4 Section Start -->
      <div class="features-four">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row d-flex align-items-center flex-wrap">
                     <div class="col-12 col-md-6 order-md-5 f-18 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh150 text-capitalize black-clr text-md-start text-center">
                           Get unstoppable leads in your favourite Autoresponder or the dashboard
                        </div>
                        <div class="f-18 f-md-20 w400 lh150 black-clr mt15 text-md-start text-center">
                           Buzzify has been custom created to enable you get fast and easy integration with TOP autoresponders. Import your leads directly to your favorite autoresponder and you’re all set to rock.
                        </div>
                     </div>
                     <div class="col-12 col-md-6 order-md-0 mt20 mt-md0 fe-box-img">
                        <img src="assets/images/feature6.png" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Features 4 Section End -->
      <div class="features-three">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row d-flex align-items-center flex-wrap">
                     <div class="col-12 col-md-6 f-18 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w700 lh150 text-capitalize black-clr text-md-start text-center">
                           Done-For-You, Colour Themes to Create Your Beautiful & Trendy Content Rich Viral Sites
                        </div>
                        <div class="f-18 f-md-20 w400 lh150 black-clr mt15">
                           These attractive, eye-catchy and premium themes have been created keeping every marketer’s needs in mind. They will prove to be a great add-on to your purchase.
                           <br><br>All you need to do is just use them, and see a huge boost in conversions, leads and sales hands down.
                        </div>
                     </div>
                     <div class="col-12 col-md-6 mt20 mt-md0 order-md-2 fe-box-img">
                        <img src="assets/images/feature7.png" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="features-four">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row d-flex align-items-center flex-wrap">
                     <div class="col-12 col-md-6 order-md-5 f-18 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh150 text-capitalize black-clr text-md-start text-center">
                           Loaded with PROVEN Converting & Ready-To-Use Lead & Promo Templates & Easy & FAST Editor
                        </div>
                        <div class="f-18 f-md-20 w400 lh150 black-clr mt15">
                           Just tweak them within minutes using our editor to create a high converting promo to maximise profits.
                           <br><br>No matter what industry you’re in, our enticing templates offers your business a way to truly stand out and skyrocket conversions.
                        </div>
                     </div>
                     <div class="col-12 col-md-6 order-md-0 mt20 mt-md0 fe-box-img">
                        <img src="assets/images/feature8.png" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="features-three">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row d-flex align-items-center flex-wrap">
                     <div class="col-12 col-md-6 f-18 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh150 text-capitalize black-clr text-md-start text-center">
                           Market your content in 15+ Languages in one click
                        </div>
                        <div class="f-18 f-md-20 w400 lh150 black-clr mt15">
                           When it comes to gathering audience attention, adding a <span class="w500">personalized</span> touch to your audience is of immense importance for every marketer’s success. So, Buzzify comes pre-loaded with <span class="w500">15+ languages</span> that gets every visitor glued to your sites.
                        </div>
                     </div>
                     <div class="col-12 col-md-6 mt20 mt-md0 fe-box-img">
                        <img src="assets/images/feature9.png" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="features-four">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row d-flex align-items-center flex-wrap">
                     <div class="col-12 col-md-6 f-18 mt20 mt-md0 order-md-2">
                        <div class="f-22 f-md-32 w700 lh150 text-capitalize black-clr text-md-start text-center">
                           Create SEO friendly pages automatically
                        </div>
                        <div class="f-18 f-md-20 w400 lh150 black-clr mt15">
                           Worried whether Buzzify follows SEO guidelines to staying compliant? Yes, you have built-in settings for making all your sites completely SEO friendly and all that comes extremely fast and easy.
                        </div>
                     </div>
                     <div class="col-12 col-md-6 mt20 mt-md0 order-md-1 fe-box-img">
                        <img src="assets/images/feature11.png" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="features-three">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row d-flex align-items-center flex-wrap">
                     <div class="col-12 col-md-6  f-18 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh150 text-capitalize black-clr text-md-start text-center">
                           No Domain or hosting required
                        </div>
                        <div class="f-18 f-md-20 w400 lh150 black-clr mt15">
                           With Buzzify, you <span class="w500">save hundreds of dollars as yearly charges for hosting services.</span> Now, when we have everything in place for you, you just sit and relax and save your money.
                        </div>
                     </div>
                     <div class="col-12 col-md-6  mt20 mt-md0 fe-box-img">
                        <img src="assets/images/feature12.png" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="features-four">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row d-flex align-items-center flex-wrap">
                     <div class="col-12 col-md-6 f-18 mt20 mt-md0 order-md-2">
                        <div class="f-22 f-md-32 w700 lh150 text-capitalize black-clr text-md-start text-center">
                           Precise analytics included
                        </div>
                        <div class="f-18 f-md-20 w400 lh150 black-clr mt15">
                           Get your viral site analytics with smart metrics on articles and videos count, leads captured, views along with quick-to-understand graphs for articles and videos. So you can see how successful your campaigns are and scale them accordingly.
                        </div>
                     </div>
                     <div class="col-12 col-md-6 mt20 mt-md0 order-md-1 fe-box-img">
                        <img src="assets/images/feature13.png" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="features-three">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row d-flex align-items-center flex-wrap">
                     <div class="col-12 col-md-6 f-18 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh150 text-capitalize black-clr text-md-start text-center">
                           Complete A-Z Training
                        </div>
                        <div class="f-18 f-md-20 w400 lh150 black-clr mt15">
                           When you are diving into such a large pool of features, we are not leaving you alone there. You will see complete step-by-step training in PDF and Video format to take you by the hand and make everything fast and easy.
                        </div>
                     </div>
                     <div class="col-12 col-md-6 mt20 mt-md0 fe-box-img">
                        <img src="assets/images/feature14.png" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>    
      <!-- CTA Section Start -->
      <div class="cta-section-white">
         <div class="container">
            <div class="row">
               <!-- CTA Btn Section Start -->
               <div class="col-12">
                  <div class="d-flex align-items-center justify-content-center">
                     <img src="assets/images/limited-time-offer-banner.png" class="img-fluid d-block mx-auto">
                  </div>
                  <div class="f-20 f-md-26 w800 lh150 text-center mt15 white-clr">
                     Grab Buzzify Now at 80% Launch Special Discount!
                  </div>
                  <div class="f-18 f-md-20 w500 lh150 text-center mt20 white-clr">
                     (Free Commercial License + Low 1-Time Price + 8 Special Bonuses worth $2285)
                  </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center">
                        <a href="https://warriorplus.com/o2/buy/fmtj72/gk3xc1/x4080g" class="cta-link-btn">Get Instant Access To Buzzify</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                     <img src="assets/images/compaitable-with1.png" class="img-responsive mx-xs-center md-img-right">
                     <div class="d-lg-block d-none visible-md px-md30"><img src="assets/images/v-line.png" class="img-fluid"></div>
                     <img src="assets/images/days-gurantee1.png" class="img-responsive mx-xs-auto mt15 mt-md0">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- CTA Section End -->
      <section class="table-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-45 w700 lh150 black-clr">
                     We Literally Have no Competition
                  </div>
               </div>
            </div>
            <div class="row g-0 mt70 mt-md100">
               <div class="col-md-6 col-6">
                  <div class="fist-row">
                     <ul class="f-md-16 f-14 w500">
                        <li class="f-md-20 f-16">Features</li>
						<li><span class="w600">Set and Forget System </span> with Single Keyword </li>
						<li>Self-Updating & Beautiful Trendy Sites </li>
						<li>1-Click Traffic Generating System  </li>
						<li>Auto-Curate Trendy Videos </li>
						<li>Auto-Curate Trendy Articles </li>
						<li>UNLIMITED Campaigns </li>
						<li>Puts Most Profitable Affiliate Links using AI </li>
						<li>Done-For-You Promo & Lead Gen Templates </li>
						<li>Excellent, Fast & Easy Template Editor </li>
						<li>Campaign Scheduling (pop ups effect)</li> 
						<li>Built-in Content Spinner </li>
						<li>100% SEO Friendly Website and Built-In Remarketing System </li>
						<li>Accelerated Mobile Pages </li>
						<li>Built-in Slider Management </li>
						<li>Amazon Ads Integration </li>
						<li>Monetization with Banner Ads </li>
						<li>Precise Site Analytics </li>
						<li>SSl Certification & GDPR Compliance </li>
						<li>One-Time Payment </li>
						<li>FREE Commercial License Included </li>
                     </ul>
                  </div>
               </div>
               <div class="col-md-3 col-3">
                  <div class="second-row">
                     <ul class="f-md-16 f-14">
                        <li>
                           <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 973.3 276.9" style="enable-background:new 0 0 973.3 276.9; max-height:40px;" xml:space="preserve">
                              <style type="text/css">
                                 .st0{fill:#FFFFFF;}
                              </style>
                              <g>
                                 <path class="st0" d="M0,212.5V12.1h98.6c19.3,0,34.4,4.2,45.3,12.7c10.9,8.5,16.4,21,16.4,37.7c0,20.8-8.6,35.5-25.7,44.1
                                    c14,2.2,24.8,7.6,32.2,16.1c7.5,8.5,11.2,19.7,11.2,33.6c0,11.7-2.6,21.8-7.9,30.2c-5.3,8.4-12.8,14.8-22.5,19.3
                                    c-9.7,4.5-21.2,6.7-34.5,6.7H0z M39.2,93.3h50.3c10.7,0,18.8-2.2,24.5-6.6c5.6-4.4,8.4-10.7,8.4-19c0-8.3-2.8-14.6-8.3-18.8
                                    c-5.5-4.2-13.7-6.3-24.6-6.3H39.2V93.3z M39.2,181.9h60.6c12.3,0,21.6-2.2,27.7-6.7c6.2-4.5,9.3-11.2,9.3-20.2
                                    c0-8.9-3.2-15.8-9.5-20.6c-6.4-4.8-15.5-7.2-27.5-7.2H39.2V181.9z"/>
                                 <path class="st0" d="M268.1,216.4c-22.8,0-39.1-4.5-49.1-13.5c-10-9-15-23.6-15-44V88.2h37.3v61.9c0,13.1,2,22.4,6,27.7
                                    c4,5.4,10.9,8.1,20.8,8.1c9.7,0,16.6-2.7,20.6-8.1c4-5.4,6.1-14.6,6.1-27.7V88.2h37.3V159c0,20.3-5,35-14.9,44
                                    C307.2,211.9,290.9,216.4,268.1,216.4z"/>
                                 <path class="st0" d="M356.2,212.5l68.6-95.9h-58.7V88.2h121.6L419,184.1h64.5v28.4H356.2z"/>
                                 <path class="st0" d="M500.3,212.5l68.6-95.9h-58.7V88.2h121.6l-68.7,95.9h64.5v28.4H500.3z"/>
                                 <path class="st0" d="M679.8,62.3c-4.1,0-7.8-1-11.1-3c-3.4-2-6-4.7-8-8.1s-3-7.1-3-11.2c0-4,1-7.7,3-11c2-3.3,4.7-6,8-8
                                    c3.3-2,7-3,11.1-3c4,0,7.7,1,11.1,3c3.3,2,6,4.6,8,8c2,3.3,3,7,3,11c0,4.1-1,7.8-3,11.2c-2,3.4-4.7,6.1-8,8.1
                                    C687.5,61.3,683.8,62.3,679.8,62.3z M661.2,212.5V88.2h37.3v124.4H661.2z"/>
                                 <path class="st0" d="M747.9,212.5v-96.1h-16.8V88.2h16.8V57.3c0-11.7,1.8-21.9,5.5-30.5c3.6-8.6,8.9-15.2,15.7-19.9
                                    c6.8-4.7,15-7,24.6-7c5.9,0,11.7,0.9,17.5,2.7c5.7,1.8,10.7,4.4,14.8,7.6l-12.9,26.1c-3.8-2.3-8.1-3.5-12.9-3.5
                                    c-4.2,0-7.4,1.1-9.6,3.2c-2.2,2.1-3.7,5.1-4.4,8.9c-0.8,3.8-1.2,8.2-1.2,13.1v30.1h29.4v28.3h-29.4v96.1H747.9z"/>
                                 <path class="st0" d="M835.6,275.7l40.1-78.7l-59-108.8h42.7l38,72.9l33.2-72.9h42.7l-95,187.5H835.6z"/>
                              </g>
                           </svg>
                        </li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptwhitetick.png" class="img-fluid mx-auto d-block"></li>
                     </ul>
                  </div>
               </div>
               <div class="col-md-3 col-3">
                  <div class="third-row">
                     <ul class="f-md-16 f-14">
                        <li class="f-md-20 f-16">Others</li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                        <li><img src="assets/images/ptcross.png" class="img-fluid mx-auto d-block"></li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <div class="imagine-sec pb0">
         <div class="container">
            <div class="row">
               <div class="col-md-6 col-12">
                  <img src="assets/images/imagine-box.png" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 col-md-6 mt30 mt-md80 mb-md0 mb30">
                  <div class="f-18 f-md-20 w400 lh150 black-clr">
                     ... ! A new lightweight piece of technology that’s so incredibly sophisticated yet intuitive & easy to use that allows you to do it all, with literally ONE click!<br><br> <b>That’s EXACTLY what we’ve designed Buzzify to do for you.</b><br><br>                        So, if you want to build super engaging websites packed with Trending Videos, Content, News & Articles  at the push of a button, then get viral social traffic automatically and convert it into SALES, all from start to finish, then Buzzify is made for you! 
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- <div class="targeted-section">
         <div class="container">
             <div class="row">
                 <div class="col-12">
                     <div class="row d-flex align-items-center flex-wrap">
                         <div class="col-12 col-md-7">
                             <div class="d-flex">
                                 <div class="mr15 mt5">
                                     <img src="assets/images/tick-icon.png">
                                 </div>
                                 <div class="f-18 f-md-20 w500 lh150 black-clr">
                                     Create instant 1 click video channels packed with trending videos
                                 </div>
                             </div>
                             <div class="d-flex mt15">
                                 <div class="mr15 mt5">
                                     <img src="assets/images/tick-icon.png">
                                 </div>
                                 <div class="f-18 f-md-20 w500 lh150 black-clr">
                                     Drive tons of laser targeted traffic from RED-HOT social media giants
                                 </div>
                             </div>
                             <div class="d-flex  mt15">
                                 <div class="mr15 mt5">
                                     <img src="assets/images/tick-icon.png">
                                 </div>
                                 <div class="f-18 f-md-20 w500 lh150 black-clr">
                                     Get best results without getting into video creation & editing hassles
                                 </div>
                             </div>
                             <div class="d-flex  mt15">
                                 <div class="mr15 mt5">
                                     <img src="assets/images/tick-icon.png">
                                 </div>
                                 <div class="f-18 f-md-20 w500 lh150 black-clr">
                                     Get more exposure for your offers in a hands down manner
                                 </div>
                             </div>
                             <div class="d-flex  mt15">
                                 <div class="mr15 mt5">
                                     <img src="assets/images/tick-icon.png">
                                 </div>
                                 <div class="f-18 f-md-20 w500 lh150 black-clr">
                                     Boost sales & profits without spending much
                                 </div>
                             </div>
                             <div class="d-flex mt15">
                                 <div class="mr15 mt5">
                                     <img src="assets/images/tick-icon.png">
                                 </div>
                                 <div class="f-18 f-md-20 w500 lh150 black-clr">
                                     Have complete control on your video marketing campaigns
                                 </div>
                             </div>
                         </div>
                         <div class="col-12 col-md-5 mt20 mt-md0">
                             <img src="assets/images/target-arrow.png" class="img-fluid d-block mx-auto">
         
                         </div>
                     </div>
                 </div>
                 <div class="col-12 mt-md30 mt20">
                     <div class="f-18 f-md-20 w400 lh150 black-clr">
                         <span class="w600">Seriously, having to do all that manually is expensive,</span> time consuming and put frankly downright irritating, especially once you want to build more than 1 channel that’s packed with gold content.<br><br> 
         That's exactly the reason so many people are afraid to even get started in video marketing and the same <span class="w600"> reason why out of all that DO get started, very few truly "make it".</span> <br><br> We have done all the grunt work for you. So, you don’t need to worry at
                         all
         
                     </div>
                 </div>
             </div>
         </div>
         </div> -->
      <div class="potential-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-10 mx-auto text-center">
                  <div class="f-28 f-md-45 w700 shape-testi1 blue-clr">Buzzify Is Perfect For</div>
               </div>
            </div>
            <div class="row">
               <div class="col-12">
                  <div class="row">
                     <div class="col-12 col-md-4 mt25 mt-md50 ">
                        <div class="never-ending" >
                           <div class="mb23" >
                              <img src="assets/images/n2.png" class="img-fluid d-block mx-auto">
                           </div>
                           <div class="hr-shape" ></div>
                           <div class="f-22 f-md-24 w500 1h140 black  text-center" >
                              Affiliate Marketers
                           </div>
                        </div>
                     </div>
                     <div class="col-12 col-md-4 mt25 mt-md50 ">
                        <div class="never-ending" >
                           <div class="mt5" >
                              <img src="assets/images/video-marketers.png" class="img-fluid d-block mx-auto">
                           </div>
                           <div class="hr-shape" ></div>
                           <div class="f-22 f-md-24 w500 1h140 black mt14  text-center" >
                              Video Marketers
                           </div>
                        </div>
                     </div>
                     <div class="col-12 col-md-4 mt25 mt-md50 ">
                        <div class="never-ending" >
                           <div >
                              <img src="assets/images/n3.png" class="img-fluid d-block mx-auto">
                           </div>
                           <div class="hr-shape" ></div>
                           <div class="f-22 f-md-24 w500 1h140 black  text-center" >
                              E-Com Sellers
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12">
                  <div class="row">
                     <div class="col-12 col-md-4 mt25 mt-md30 ">
                        <div class="never-ending" >
                           <div class="mt5" >
                              <img src="assets/images/n1.png" class="img-fluid d-block mx-auto">
                           </div>
                           <div class="hr-shape" ></div>
                           <div class="f-22 f-md-24 w500 1h140 black mt14  text-center" >
                              Business Coaches
                           </div>
                        </div>
                     </div>
                     <div class="col-12 col-md-4 mt25 mt-md30 ">
                        <div class="never-ending" >
                           <div class="mb23" >
                              <img src="assets/images/n23.png" class="img-fluid d-block mx-auto">
                           </div>
                           <div class="hr-shape" ></div>
                           <div class="f-22 f-md-24 w500 1h140 black  text-center" >
                              Digital Product Sellers
                           </div>
                        </div>
                     </div>
                     <div class="col-12 col-md-4 mt25 mt-md30 ">
                        <div class="never-ending" >
                           <div >
                              <img src="assets/images/youtubers.png" class="img-fluid d-block mx-auto">
                           </div>
                           <div class="hr-shape" ></div>
                           <div class="f-22 f-md-24 w500 1h140 black  text-center" >
                              YouTubers
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12">
                  <div class="row">
                     <div class="col-12 col-md-4 mt25 mt-md30 ">
                        <div class="never-ending" >
                           <div >
                              <img src="assets/images/n4.png" class="img-fluid d-block mx-auto">
                           </div>
                           <div class="hr-shape" ></div>
                           <div class="f-22 f-md-24 w500 1h140 black  text-center" >
                              All Info Sellers
                           </div>
                        </div>
                     </div>
                     <div class="col-12 col-md-4 mt25 mt-md30 ">
                        <div class="never-ending" >
                           <div >
                              <img src="assets/images/n22.png" class="img-fluid d-block mx-auto">
                           </div>
                           <div class="hr-shape" ></div>
                           <div class="f-22 f-md-24 w500 1h140 black  text-center" >
                              Lead Generation Companies
                           </div>
                        </div>
                     </div>
                     <div class="col-12 col-md-4 mt25 mt-md30 ">
                        <div class="never-ending" >
                           <div class="" >
                              <img src="assets/images/n6.png" class="img-fluid d-block mx-auto">
                           </div>
                           <div class="hr-shape" ></div>
                           <div class="f-22 f-md-24 w500 1h140 black  text-center" >
                              Music Classes
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12">
                  <div class="row">
                     <div class="col-12 col-md-4 mt25 mt-md30 ">
                        <div class="never-ending" >
                           <div >
                              <img src="assets/images/n7.png" class="img-fluid d-block mx-auto">
                           </div>
                           <div class="hr-shape" ></div>
                           <div class="f-22 f-md-24 w500 1h140 black  text-center" >
                              Sports Clubs
                           </div>
                        </div>
                     </div>
                     <div class="col-12 col-md-4 mt25 mt-md30 ">
                        <div class="never-ending" >
                           <div >
                              <img src="assets/images/n8.png" class="img-fluid d-block mx-auto">
                           </div>
                           <div class="hr-shape" ></div>
                           <div class="f-22 f-md-24 w500 1h140 black  text-center" >
                              Bars
                           </div>
                        </div>
                     </div>
                     <div class="col-12 col-md-4 mt25 mt-md30 ">
                        <div class="never-ending" >
                           <div class="" >
                              <img src="assets/images/n9.png" class="img-fluid d-block mx-auto">
                           </div>
                           <div class="hr-shape" ></div>
                           <div class="f-22 f-md-24 w500 1h140 black  text-center" >
                              Restaurants
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12">
                  <div class="row">
                     <div class="col-12 col-md-4 mt25 mt-md30 ">
                        <div class="never-ending" >
                           <div >
                              <img src="assets/images/n10.png" class="img-fluid d-block mx-auto">
                           </div>
                           <div class="hr-shape" ></div>
                           <div class="f-22 f-md-24 w500 1h140 black  text-center" >
                              Hotels
                           </div>
                        </div>
                     </div>
                     <div class="col-12 col-md-4 mt25 mt-md30 ">
                        <div class="never-ending" >
                           <div >
                              <img src="assets/images/n11.png" class="img-fluid d-block mx-auto">
                           </div>
                           <div class="hr-shape" ></div>
                           <div class="f-22 f-md-24 w500 1h140 black  text-center" >
                              Schools
                           </div>
                        </div>
                     </div>
                     <div class="col-12 col-md-4 mt25 mt-md30 ">
                        <div class="never-ending" >
                           <div class="" >
                              <img src="assets/images/n12.png" class="img-fluid d-block mx-auto">
                           </div>
                           <div class="hr-shape" ></div>
                           <div class="f-22 f-md-24 w500 1h140 black  text-center" >
                              Churches
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12">
                  <div class="row">
                     <div class="col-12 col-md-4 mt25 mt-md30 ">
                        <div class="never-ending" >
                           <div >
                              <img src="assets/images/n13.png" class="img-fluid d-block mx-auto">
                           </div>
                           <div class="hr-shape" ></div>
                           <div class="f-22 f-md-24 w500 1h140 black  text-center" >
                              Taxi Services
                           </div>
                        </div>
                     </div>
                     <div class="col-12 col-md-4 mt25 mt-md30 ">
                        <div class="never-ending" >
                           <div >
                              <img src="assets/images/n14.png" class="img-fluid d-block mx-auto">
                           </div>
                           <div class="hr-shape" ></div>
                           <div class="f-22 f-md-24 w500 1h140 black  text-center" >
                              Garage Owners
                           </div>
                        </div>
                     </div>
                     <div class="col-12 col-md-4 mt25 mt-md30 ">
                        <div class="never-ending" >
                           <div class="" >
                              <img src="assets/images/n15.png" class="img-fluid d-block mx-auto">
                           </div>
                           <div class="hr-shape" ></div>
                           <div class="f-22 f-md-24 w500 1h140 black  text-center" >
                              Dentists
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="need-marketing">
         <div class="container">
            <div class="row">             
               <div class="col-12 text-center">
                  <div class="f-22 f-md-30 w700 lh150 orange-clr">
                     It Works Seamlessly for ANY NICHE… All at the push of a button. 
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50">
                  <div class="row align-items-center flex-wrap">
                     <div class="col-12 col-md-7">
                        <div class="f-20 f-md-28 w600 lh150 black-clr text-center text-md-start">
                           Buzzify Is Designed to Meet Every Marketer’s Need: 
                        </div>
                        <div class="f-18 f-md-20 w400 lh150 black-clr mt20">
                           <ul class="noneed-listing pl0">
                              <li>Anyone who wants a nice PASSIVE income while focusing on bigger projects</li>
                              <li>Lazy people who want easy profits</li>
                              <li>Anyone who wants to value their business and money and is not ready to sacrifice them</li>
                              <li>People without products who want to kickstart online</li>
                           </ul>
                        </div>
                     </div>
                     <div class="col-12 col-md-5 mt20 mt-md0">
                        <img src="assets/images/platfrom.png" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md70 text-center">
                  <div class="look-shape">
                     <div class="f-22 f-md-30 w600 lh150 white-clr">
                        Look, it doesn't matter who you are or what you're doing.
                     </div>
                     <div class="f-18 f-md-20 w400 lh150 white-clr mt10">
                        If you want to finally drive laser targeted traffic to your offers, make the money that you want <br class="d-none d-md-block">  and live in a way you dream of, Buzzify is for you.
                     </div>
                  </div>
               </div>            
            </div>
         </div>
      </div>
      <!-- CTA Section Start -->
      <div class="cta-section-white">
         <div class="container">
            <div class="row">
               <!-- CTA Btn Section Start -->
               <div class="col-12">
                  <div class="d-flex align-items-center justify-content-center">
                     <img src="assets/images/limited-time-offer-banner.png" class="img-fluid d-block mx-auto">
                  </div>
                  <div class="f-20 f-md-26 w800 lh150 text-center mt15 white-clr">
                     Grab Buzzify Now at 80% Launch Special Discount!
                  </div>
                  <div class="f-18 f-md-20 w500 lh150 text-center mt20 white-clr">
                     (Free Commercial License + Low 1-Time Price + 8 Special Bonuses worth $2285)
                  </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center">
                        <a href="https://warriorplus.com/o2/buy/fmtj72/gk3xc1/x4080g" class="cta-link-btn">Get Instant Access To Buzzify</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                     <img src="assets/images/compaitable-with1.png" class="img-responsive mx-xs-center md-img-right">
                     <div class="d-lg-block d-none visible-md px-md30"><img src="assets/images/v-line.png" class="img-fluid"></div>
                     <img src="assets/images/days-gurantee1.png" class="img-responsive mx-xs-auto mt15 mt-md0">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- CTA Section End -->
      <!---Bonus Section Starts-->
      <div class="bonus-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center mx-auto">
                  <img src="assets/images/bonus-shape.png" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 col-md-12 mt20 mt-md40">
                  <div class="f-22 f-md-24 w500 lh150 text-center">
                     When You Grab Your Buzzify Account Today, You’ll Also Get These <span class="w600">Fast Action Bonuses!</span>
                  </div>
               </div>
               <div class="col-12 col-md-12 mt20 mt-md40">
                  <div class="f-28 f-md-45 w700 lh150 text-center">
                     Limited Time Bonuses!
                  </div>
               </div>
               <div class="col-12 col-md-12 mt60 mt-md70">
                  <div class="bonus-section-shape text-center">
                     <div class="col-12 margin-t-60">
                        <div class="bonus-headline">
                           <div class="f-md-28 f-20 w700 white-clr text-nowrap text-center">
                              Limited Time Bonus #1
                           </div>
                        </div>
                     </div>
                     <div class="row d-flex align-items-center flex-wrap mt15 mt-md30">
                        <div class="col-12 col-md-7 order-md-2">
                           <div class="f-22 f-md-30 w700 lh150 text-start black-clr">
                              LIVE Training: See How You Can Launch, Sell & Market Any Product, Course or Service - Fast & Easy!! (First 1000 buyers only - $997 Value)
                           </div>
                           <div class="f-18 f-md-20 w400 lh150 mt10 text-start">
                              Launch Your Own Product In FEW Minutes.<br><br>
                              This Awesome LIVE Training will Help you to Start Online and Grow your Online Business for Marketing & Selling your Products and Services. And Free Access to a Private Community of Like-Minded Entrepreneurs to learn and grow together with each other.<br><br>
                              And There will be a Live Q & A Session at the end of the training.
                           </div>
                        </div>
                        <div class="col-12 col-md-5 order-md-1 mt20 mt-md0">
                           <div> <img src="assets/images/bonus1.png" class="img-fluid d-block mx-auto max-h450"> </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-12 mt60 mt-md70">
                  <div class="bonus-section-shape text-center">
                     <div class="col-12 margin-t-60">
                        <div class="bonus-headline">
                           <div class="f-md-28 f-20 w700 white-clr text-nowrap text-center">
                              Limited Time Bonus #2 
                           </div>
                        </div>
                     </div>
                     <div class="row d-flex align-items-center flex-wrap mt15 mt-md30">
                        <div class="col-12 col-md-7">
                           <div class="f-22 f-md-30 w700 lh150 text-start black-clr">DotcomPal- All in One Growth Platform for Entrepreneurs (That's PRICELESS)</div>
                           <div class="f-18 f-md-20 w400 lh150 mt10 text-start"> All-in-one growth platform for Entrepreneurs & SMBs to generate more leads, bring more growth & sell more, more quickly. It’s got everything you need to convert more visitors and bring more growth at every customer touchpoint... without any designer, tech team, or the usual hassles.
                              <br><br>
                              This package is something that’s of huge worth for your business, but we’re offering it for free only with your investment in Cordova today. 
                           </div>
                        </div>
                        <div class="col-12 col-md-5 mt20 mt-md0">
                           <img src="assets/images/bonus2.png" class="img-fluid d-block mx-auto">
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-12 mt60 mt-md70">
                  <div class="bonus-section-shape text-center">
                     <div class="col-12 margin-t-60">
                        <div class="bonus-headline">
                           <div class="f-md-28 f-20 w700 white-clr text-nowrap text-center">
                              Limited Time Bonus #3
                           </div>
                        </div>
                     </div>
                     <div class="row d-flex align-items-center flex-wrap mt15 mt-md30">
                        <div class="col-12 col-md-7 order-md-2">
                           <div class="f-22 f-md-30 w700 lh150 text-start black-clr">Ultimate Notification System That Shows Your Perfect Offer to Visitors According to What They NEED & Generate You More Leads Sales and Commissions.</div>
                           <div class="f-18 f-md-20 w400 lh150 mt10 text-start">
                              Target your audience precisely on the basis of geo-location, behavior, keywords in URL & much more…
                              <div class="f-18 f-md-20 w400 lh150 black-clr mt20">
                                 <ul class="noneed-listing pl0">
                                    <li>Anyone who wants a nice PASSIVE income while focusing on bigger projects</li>
                                    <li>Show your visitors exactly what they WANT & maximize your affiliate commissions & leads</li>
                                    <li>Grab visitors from the neck and literally force them to take the desired action</li>
                                    <li>Comes with ready-to-use and proven lead & promo notification templates</li>
                                    <li>Use Inbuilt Inline Editor to change text, images, CTA buttons, or anything in templates & it’s very easy</li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                        <div class="col-12 col-md-5 order-md-1 mt20 mt-md0">
                           <img src="assets/images/bonus3.png" class="img-fluid d-block mx-auto">
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-12 mt20 mt-md40">
                  <div class="f-28 f-md-45 w700 lh150 text-center">
                     Super Value Bonuses
                  </div>
               </div>
               <div class="col-12 col-md-12 mt60 mt-md70">
                  <div class="bonus-section-shape text-center">
                     <div class="col-12 margin-t-60">
                        <div class="bonus-headline">
                           <div class="f-md-28 f-20 w700 white-clr text-nowrap text-center">
                              Super Value Bonus #4 
                           </div>
                        </div>
                     </div>
                     <div class="row d-flex align-items-center flex-wrap mt15 mt-md30">
                        <div class="col-12 col-md-7">
                           <div class="f-22 f-md-30 w700 lh150 text-start black-clr">Done-For-You Setup Service (It’s Priceless)
                           </div>
                           <div class="f-20 f-md-22 w400 lh150 mt10 text-start">
                              Complete Setup Service so you can kickstart your online marketing business with Trenzy instantly.<br><br>
                              You don’t need to worry about planning and designing landing pages, newsletter templates, web forms, notification pop-up templates, A/B testing them, or even for low email opt-in rates and poor Inboxing. Just tweak these templates according to your brand or campaign's motive and target audience. There are no huge Ad Budgets, low-engaging designs, and almost everything that holds you back. Not even any additional costs to get started with the system.<br><br>
                              Just provide us with a few details, and our dedicated team will set up everything for you. We want to see you succeed in the shortest time possible.
                           </div>
                        </div>
                        <div class="col-12 col-md-5  mt20 mt-md0">
                           <div> <img src="assets/images/bonus4.png" class="img-fluid d-block mx-auto max-h450"> </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-12 mt60 mt-md70">
                  <div class="bonus-section-shape text-center">
                     <div class="col-12 margin-t-60">
                        <div class="bonus-headline">
                           <div class="f-md-28 f-20 w700 white-clr text-nowrap text-center">
                              Super Value Bonus #5 
                           </div>
                        </div>
                     </div>
                     <div class="row d-flex align-items-center flex-wrap mt15 mt-md30">
                        <div class="col-12 col-md-7 order-md-2">
                           <div class="f-22 f-md-30 w700 lh150 text-start black-clr">How To Monetizing Your Website
                           </div>
                           <div class="f-20 f-md-22 w400 lh150 mt10 text-start">
                              This Video Training Will Teach You Website and Blog Monetization Strategies. It will show you how to start earning money from your trending content website - <br><br>
                              You will learn a lot about:<br>
                              <div class="f-18 f-md-20 w400 lh150 black-clr mt20">
                                 <ul class="noneed-listing pl0">
                                    <li>Ad placement</li>
                                    <li>Advertisement</li>
                                    <li>Pre-Selling Strategies</li>
                                    <li>Finding Offers & Deals In Your Niche</li>
                                    <li>Building And Monetizing Your List</li>
                                    <li>List Automation</li>
                                    <li>Marketing Funnel</li>
                                    <li>And much more!</li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                        <div class="col-12 col-md-5 order-md-1 mt20 mt-md0">
                           <div> <img src="assets/images/bonus5.png" class="img-fluid d-block mx-auto max-h450"> </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-12 mt60 mt-md70">
                  <div class="bonus-section-shape text-center">
                     <div class="col-12 margin-t-60">
                        <div class="bonus-headline">
                           <div class="f-md-28 f-20 w700 white-clr text-nowrap text-center">
                              Super Value Bonus #6 
                           </div>
                        </div>
                     </div>
                     <div class="row d-flex align-items-center flex-wrap mt15 mt-md30">
                        <div class="col-12 col-md-7">
                           <div class="f-22 f-md-30 w700 lh150 text-start black-clr">How to Start Affiliate Marketing
                           </div>
                           <div class="f-20 f-md-22 w400 lh150 mt10 text-start">
                              When done properly, Affiliate Marketing can be a Very Lucrative Component of an Online Business. This course will teach you what you need to know so you can be a successful affiliate marketer.<br><br>
                              You'll Learn How To:<br>
                              <div class="f-18 f-md-20 w400 lh150 black-clr mt20">
                                 <ul class="noneed-listing pl0">
                                    <li>How to Pick a Niche Market</li>
                                    <li>Finding Quality Products that Fit</li>
                                    <li>Implementing the Bonus Strategy</li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                        <div class="col-12 col-md-5  mt20 mt-md0">
                           <div> <img src="assets/images/bonus6.png" class="img-fluid d-block mx-auto max-h450"> </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-12 mt60 mt-md70">
                  <div class="bonus-section-shape text-center">
                     <div class="col-12 margin-t-60">
                        <div class="bonus-headline">
                           <div class="f-md-28 f-20 w700 white-clr text-nowrap text-center">
                              Super Value Bonus #7 
                           </div>
                        </div>
                     </div>
                     <div class="row d-flex align-items-center flex-wrap mt15 mt-md30">
                        <div class="col-12 col-md-7 order-md-2">
                           <div class="f-22 f-md-30 w700 lh150 text-start black-clr">How To Encash The Power of Viral Marketing
                           </div>
                           <div class="f-20 f-md-22 w400 lh150 mt10 text-start">
                              Believe it or not, people interested in whatever it is you are promoting are already congregating online. With this Video Training, you will Discover a Shortcut to Online Viral Marketing Secrets.<br><br>
                              Topics covered:
                              <br>
                              <div class="f-18 f-md-20 w400 lh150 black-clr mt20">
                                 <ul class="noneed-listing pl0">
                                    <li>The Two-Step Trick to Effective Viral Marketing</li>
                                    <li>How Do You Find Hot Content?</li>
                                    <li>Maximize Niche Targeting for Your Curated Content</li>
                                    <li>Filter your content format to go viral on many platforms</li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                        <div class="col-12 col-md-5 order-md-1 mt20 mt-md0">
                           <div> <img src="assets/images/bonus7.png" class="img-fluid d-block mx-auto max-h450"> </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-12 mt60 mt-md70">
                  <div class="bonus-section-shape text-center">
                     <div class="col-12 margin-t-60">
                        <div class="bonus-headline">
                           <div class="f-md-28 f-20 w700 white-clr text-nowrap text-center">
                              Super Value Bonus #8 
                           </div>
                        </div>
                     </div>
                     <div class="row d-flex align-items-center flex-wrap mt15 mt-md30">
                        <div class="col-12 col-md-7">
                           <div class="f-22 f-md-30 w700 lh150 text-start black-clr">Video Marketing Profit Kit 
                           </div>
                           <div class="f-20 f-md-22 w400 lh150 mt10 text-start">
                              Video is able to put a face to your brand and make your brand talk to the needs, fears, hopes, and aspirations of your prospective customers. And with this Specialised Training, you will discover how to use video marketing to build a thriving online business!<br><br>
                              You'll Learn How To:<br>
                              <div class="f-18 f-md-20 w400 lh150 black-clr mt20">
                                 <ul class="noneed-listing pl0">
                                    <li>Find Winning Video Topics</li>
                                    <li>Save Money on Video Creation</li>
                                    <li>Make Money from Your Videos</li>
                                    <li>Includes ready sales materials!</li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                        <div class="col-12 col-md-5  mt20 mt-md0">
                           <div> <img src="assets/images/bonus8.png" class="img-fluid d-block mx-auto max-h450"> </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--Bonus Section ends -->
      <!--Bonus value Section start -->
      <div class="bonus-section-value">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-6 mx-auto">
                  <img src="assets/images/thats-a-total.png" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 mt-md100 mt20">
                  <img src="assets/images/arrow.png" alt="" class="img-fluid d-block mx-auto vert-move">
               </div>
            </div>
         </div>
      </div>
      <!--Bonus value Section ends -->
      <div class="space-section license-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-md-45 f-28 w700 lh150 black-clr text-center">
                     Also Get Our Free Commercial License <br class="d-none d-md-block"> When You Get Access To Buzzify Today!
                  </div>
               </div>
               <div class="col-12 mt25 mt-md70">
                  <div class="row d-flex align-items-center flex-wrap">
                     <div class="col-12 col-md-5 ">
                        <img src="assets/images/commercial-image.png" class="img-fluid d-block mx-auto"> 
                     </div>
                     <div class="col-12 col-md-7 mt20 mt-md0">
                        <div class="f-md-20 f-18 lh150 w400">
                           As we have shown you, there are tons of businesses & marketers – yoga gurus, fitness centers, social media marketers or anybody else who want to start online & Drive FREE Traffic,<span class="w600"> NEED YOUR SERVICE & would love to pay you monthly for your services.</span>
                           <br><br>
                           Build their branded & traffic generating trendy websites. We’ve taken care of everything so that you can deliver those services easily & 100% profits in your pocket.
                           <br><br>
                           <span class="w600">Note: </span>This special commercial license is being included in the Advanced plan and ONLY during this launch. Take advantage of it now because it will never be offered again.
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
	  <!-- No Need Section -->
		<div class="noneed-section">
			<div class="container">
				<div class="row">
					<div class="col-12 col-md-12">
						<div class="f-20 f-md-24 w400 lh150 text-center">
							<span class="w600">Only 3 easy steps is all it takes </span>to publish trendy <br class="d-none d-md-block"> content & bring hordes of TRAFFIC & Sales… 
						</div>
						<div class="f-md-45 f-28 w700 black-clr lh150 text-center mt20 mt-md40">
							With Buzzify, Turn Your Worries <br class="d-none d-md-block"> Into A HUGE Profitable Opportunity 
							
						</div>
						<div class="noneed-shape mt20 mt-md40">
							<div class="f-20 f-md-20 w400 lh150">
							<ul class="noneed-list pl0">
							<li><span class="w600">No more worrying</span> for researching and writing content daily </li>
							<li><span class="w600">No need for learning</span> complex video & audio editing skills</li> 
							<li><span class="w600">No worries for buying</span> expensive equipment that can leave your back accounts dry </li>
							<li><span class="w600">No more frustration</span> of not being able to face the camera & creating a mockery of yourself </li>
							<li><span class="w600">No worries</span> for lack of technical, designing or marketing skills </li>
							<li><span class="w600">Say no to wasting</span> any time on creating funnels, SEO, paid traffic, complex tools etc. </li>
							<li class="happy"><span class="w700">And…say YES Now to living a LAPTOP lifestyle</span> that gives you an ability to spend more time with your family and have complete financial FREEDOM.</li>
							</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- No Need Section End-->
	  
      <!-- Table Section Start -->
      <div class="table-section" id="buynow">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-20 f-md-24 w600 text-center lh150 black-clr">
                     Take Advantage Of Our Limited Time Deal 
                  </div>
                  <div class="f-md-45 f-28 w700 lh150 text-center mt10">
                     Get Buzzify For A Low One-Time- 
                     <br class="d-none d-md-block"> Price, No Monthly Fee.
                  </div>
                  <div class="f-18 f-md-20 w500 lh150 text-center mt20">
                     (Free Commercial License + Low 1-Time Price + 8 Special Bonuses worth $2285)
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
                  <div class="row">
                     <!-- <div class="col-12 col-md-6">
                        <div class="table-wrap">
                            <div class="table-head text-center">
                                <div>
                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                        viewBox="0 0 977.7 276.9" style="enable-background:new 0 0 977.7 276.9; max-height:45px;" xml:space="preserve">
                        <style type="text/css">
                        .stblue{fill:#0041B5;}
                        .storg{fill:#FF7E1D;}
                        </style>
                        <g>
                        <path class="stblue" d="M0,213.5V12.2h99.1c19.4,0,34.5,4.3,45.5,12.7c11,8.5,16.5,21.1,16.5,37.9c0,20.9-8.6,35.7-25.9,44.3
                        c14.1,2.2,24.9,7.6,32.4,16.2c7.5,8.6,11.3,19.8,11.3,33.7c0,11.8-2.6,21.9-7.9,30.3c-5.3,8.4-12.8,14.9-22.6,19.4
                        c-9.8,4.5-21.3,6.8-34.6,6.8H0z M39.4,93.7h50.5c10.7,0,18.9-2.2,24.6-6.6c5.7-4.4,8.5-10.8,8.5-19.1c0-8.4-2.8-14.7-8.4-18.8
                        c-5.6-4.2-13.8-6.3-24.7-6.3H39.4V93.7z M39.4,182.7h60.9c12.4,0,21.7-2.3,27.9-6.8c6.2-4.5,9.3-11.3,9.3-20.3
                        c0-8.9-3.2-15.8-9.6-20.7c-6.4-4.8-15.6-7.3-27.6-7.3H39.4V182.7z"/>
                        <path class="stblue" d="M269.3,217.4c-22.9,0-39.3-4.5-49.3-13.5c-10-9-15.1-23.7-15.1-44.2V88.6h37.4v62.2c0,13.2,2,22.5,6,27.9
                        c4,5.4,11,8.1,20.9,8.1c9.8,0,16.7-2.7,20.7-8.1c4.1-5.4,6.1-14.7,6.1-27.9V88.6h37.4v71.1c0,20.4-5,35.1-15,44.2
                        C308.6,212.9,292.2,217.4,269.3,217.4z"/>
                        <path class="stblue" d="M357.8,213.5l68.9-96.4h-58.9V88.6h122.1l-69,96.4h64.8v28.5H357.8z"/>
                        <path class="stblue" d="M502.6,213.5l68.9-96.4h-58.9V88.6h122.1l-69,96.4h64.8v28.5H502.6z"/>
                        <path class="storg" d="M682.9,62.6c-4.1,0-7.8-1-11.2-3c-3.4-2-6.1-4.7-8.1-8.1c-2-3.4-3-7.2-3-11.2c0-4.1,1-7.8,3.1-11.1
                        c2-3.3,4.7-6,8.1-8c3.3-2,7-3,11.1-3c4.1,0,7.8,1,11.1,3c3.3,2,6,4.7,8.1,8c2,3.3,3,7,3,11.1c0,4.1-1,7.8-3,11.2
                        c-2,3.4-4.7,6.1-8.1,8.1C690.6,61.6,686.9,62.6,682.9,62.6z M664.2,213.5V88.6h37.4v124.9H664.2z"/>
                        <path class="storg" d="M751.3,213.5V117h-16.8V88.6h16.8v-31c0-11.8,1.8-22,5.5-30.6c3.7-8.6,8.9-15.3,15.8-19.9
                        c6.9-4.7,15.1-7,24.7-7c5.9,0,11.8,0.9,17.6,2.7c5.8,1.8,10.7,4.4,14.9,7.6l-12.9,26.2c-3.8-2.4-8.1-3.5-12.9-3.5
                        c-4.2,0-7.4,1.1-9.6,3.2c-2.2,2.1-3.7,5.1-4.5,8.9c-0.8,3.8-1.2,8.2-1.2,13.2v30.3h29.5V117h-29.5v96.5H751.3z"/>
                        <path class="storg" d="M839.4,276.9l40.3-79L820.3,88.6h42.9l38.2,73.2l33.3-73.2h42.9l-95.4,188.3H839.4z"/>
                        </g>
                        </svg>
                        </div>
                                <div class="w600 f-md-22 f-22 lh120 text-center text-uppercase mt15 mt-md30">Basic Plan</div>
                                <div class="center-line"></div> 
                            </div>
                            <div class="">
                                <ul class="table-list pl0 f-18 f-md-18 w500 lh150 black-clr">
                        <li>Done for You Lead generation Funnels</li>
                        <li>Inbuilt Social Syndication system</li>
                        <li>One Click A.I Analyzed offers and Links</li>
                        <li>Monetize better and MAKE MORE PROFITS with stunning banner ads</li>
                        <li>Click and Point Ecom Commission Pages</li>
                        <li>FB Messenger Chat Integration</li>
                        <li>Ultra-Fast Funnel Creation</li>
                        <li>Add elegant & eye-catching sliders to lure visitors</li>
                        <li>Double the POWER Of Your Campaigns With Smart Animation Effects</li>
                        <li>Integration with over 70+ Channels</li>
                        <li>Safe, Secure & 100% GDPR Complaint</li>
                        <li>Attractive Ready-to-Use Themes That Add Spark To Your Sites</li>
                        <li>Add elegant & eye-catching sliders to lure visitors</li>
                        <li>Curate UNLIMITED Fascinating Viral Videos</li>
                        <li>Instantly Curate Trending and Viral Content</li>
                        <li>Packed with PROVEN Converting & Ready-To-Use Promotional Templates</li>
                        <li>Easy-to-use, Inbuilt Text & Inline Editor</li>
                        <li>Auto-Curation of Top & Related Videos</li>
                        <li>Share your content & video capsules on 3 major Social Networks</li>
                        <li>Inbuilt content-spinner to create unique description for your stories</li>
                        <li>Boost engagement levels of your sites</li>
                        <li>All-in-one cloud-based site builder</li>
                        <li>Personal Use License included</li>
                        <li class="cross-sign">Use For Your Clients</li>
                        <li class="cross-sign">Provide High In Demand Services</li>
                        <li class="headline">BONUSES WORTH $2285 FREE!!!</li>
                        <li class="cross-sign">Fast Action Bonus 1 - DotcomPal- All in One Growth Platform</li>
                        <li class="cross-sign">Fast Action Bonus 2 - LIVE Training( $997 Value)</li>
                        <li class="cross-sign">Fast Action Bonus 3 - Auto Video Creator (Worth $297)</li>
                        <li class="cross-sign">Fast Action Bonus 4 - Advanced Video Marketing (Worth $397)	</li>
                                </ul>
                            </div>
                            <div class="table-btn">
                                <div class="hideme-button">
                                    <a href="https://warriorplus.com/o2/buy/sq4h8c/dtcw83/bntldv"><img src="https://warriorplus.com/o2/btn/fn200011000/sq4h8c/dtcw83/296609" alt="Buzzify Standard" class="img-fluid d-block mx-auto" border="0" /></a>
                                </div>
                            </div>
                        </div>
                        </div> -->
                     <div class="col-12 col-md-10 mx-auto mt20 mt-md0">
                        <div class="table-wrap1 relative">
                           <div class="table-head1">
                              <div>
                                 <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                    viewBox="0 0 973.3 276.9" style="enable-background:new 0 0 973.3 276.9; max-height:45px;" xml:space="preserve">
                                    <style type="text/css">
                                       .st0{fill:#FFFFFF;}
                                    </style>
                                    <g>
                                       <path class="st0" d="M0,212.5V12.1h98.6c19.3,0,34.4,4.2,45.3,12.7c10.9,8.5,16.4,21,16.4,37.7c0,20.8-8.6,35.5-25.7,44.1
                                          c14,2.2,24.8,7.6,32.2,16.1c7.5,8.5,11.2,19.7,11.2,33.6c0,11.7-2.6,21.8-7.9,30.2c-5.3,8.4-12.8,14.8-22.5,19.3
                                          c-9.7,4.5-21.2,6.7-34.5,6.7H0z M39.2,93.3h50.3c10.7,0,18.8-2.2,24.5-6.6c5.6-4.4,8.4-10.7,8.4-19c0-8.3-2.8-14.6-8.3-18.8
                                          c-5.5-4.2-13.7-6.3-24.6-6.3H39.2V93.3z M39.2,181.9h60.6c12.3,0,21.6-2.2,27.7-6.7c6.2-4.5,9.3-11.2,9.3-20.2
                                          c0-8.9-3.2-15.8-9.5-20.6c-6.4-4.8-15.5-7.2-27.5-7.2H39.2V181.9z"/>
                                       <path class="st0" d="M268.1,216.4c-22.8,0-39.1-4.5-49.1-13.5c-10-9-15-23.6-15-44V88.2h37.3v61.9c0,13.1,2,22.4,6,27.7
                                          c4,5.4,10.9,8.1,20.8,8.1c9.7,0,16.6-2.7,20.6-8.1c4-5.4,6.1-14.6,6.1-27.7V88.2h37.3V159c0,20.3-5,35-14.9,44
                                          C307.2,211.9,290.9,216.4,268.1,216.4z"/>
                                       <path class="st0" d="M356.2,212.5l68.6-95.9h-58.7V88.2h121.6L419,184.1h64.5v28.4H356.2z"/>
                                       <path class="st0" d="M500.3,212.5l68.6-95.9h-58.7V88.2h121.6l-68.7,95.9h64.5v28.4H500.3z"/>
                                       <path class="st0" d="M679.8,62.3c-4.1,0-7.8-1-11.1-3c-3.4-2-6-4.7-8-8.1s-3-7.1-3-11.2c0-4,1-7.7,3-11c2-3.3,4.7-6,8-8
                                          c3.3-2,7-3,11.1-3c4,0,7.7,1,11.1,3c3.3,2,6,4.6,8,8c2,3.3,3,7,3,11c0,4.1-1,7.8-3,11.2c-2,3.4-4.7,6.1-8,8.1
                                          C687.5,61.3,683.8,62.3,679.8,62.3z M661.2,212.5V88.2h37.3v124.4H661.2z"/>
                                       <path class="st0" d="M747.9,212.5v-96.1h-16.8V88.2h16.8V57.3c0-11.7,1.8-21.9,5.5-30.5c3.6-8.6,8.9-15.2,15.7-19.9
                                          c6.8-4.7,15-7,24.6-7c5.9,0,11.7,0.9,17.5,2.7c5.7,1.8,10.7,4.4,14.8,7.6l-12.9,26.1c-3.8-2.3-8.1-3.5-12.9-3.5
                                          c-4.2,0-7.4,1.1-9.6,3.2c-2.2,2.1-3.7,5.1-4.4,8.9c-0.8,3.8-1.2,8.2-1.2,13.1v30.1h29.4v28.3h-29.4v96.1H747.9z"/>
                                       <path class="st0" d="M835.6,275.7l40.1-78.7l-59-108.8h42.7l38,72.9l33.2-72.9h42.7l-95,187.5H835.6z"/>
                                    </g>
                                 </svg>
                              </div>
                              <div class="w600 f-md-22 f-22 lh120 text-center text-uppercase mt15 mt-md30 white-clr">Premium</div>
                           </div>
                           <div class="">
                              <ul class="table-list1 pl0 f-18 f-md-18 w500 lh150 white-clr">
								<li><span class="w600">Set and Forget System </span>with Single Keyword </li>
								<li>Inbuilt 1-Click Traffic Generating System  </li>
								<li>Create Self-Updating & Beautiful Trendy Sites on ANY TOPIC  </li>
								<li>Puts Most Profitable & A.I Analysed Affiliate Links  </li>
								<li>Curate UNLIMITED Fascinating Viral Videos  </li>
								<li>Instantly Curate Trending and Viral Content  </li>
								<li>Inbuilt Content-Spinner to Create Unique Stories  </li>
								<li>Monetize Better and MAKE MORE PROFITS With Stunning Banner Ads  </li>
								<li>Attractive Ready-To-Use Theme That Add Spark to Your Sites  </li>
								<li>Add Elegant & Eye-Catching Sliders To Lure Visitors  </li>
								<li>Double The POWER Of Your Campaigns with Smart Animation Effects  </li>
								<li>Integration With Over 20+ Channels  </li>
								<li>Safe, Secure & 100% GDPR Complaint  </li>
								<li>Packed With PROVEN Converting Promotional Templates  </li>
								<li>Done For You Lead Generation Templates  </li>
								<li>Easy-To-Use, Inbuilt Text & Inline Editor  </li>
								<li>100% SEO Friendly Website and Built-In Remarketing System  </li>
								<li>1-Click Social Media Automation – People ONLY WANT TRENDY Topics These Days  </li>
								<li>Automatically Translate Your Sites In 15+ Language According To Location  </li> 
								<li>All-In-One Cloud-Based Trendy Site Builder  </li>
								<li>Complete Newbie Friendly - No Prior Experience and Technical Skills Needed  </li>
								<li>Use For Your Clients  </li>
								<li>Provide High in Demand Services  </li>
								<li>Make 5K-10K/M Extra with Commercial License </li>
                                 <li class="headline">BONUSES WORTH $2285 FREE!!!</li>
								<li>Limited Time Bonus #1 – LIVE Training</li>
								<li>Limited Time Bonus #2 – DotcomPal - All in One Platform</li>
								<li>Limited Time Bonus #3 – ProfitFox - Ultimate Notification System</li>
								<li>Super Value Bonus #4 – Done-For-You Setup Service</li>
								<li>Super Value Bonus #5 – Video Training to Monetizing Your Website</li>
								<li>Super Value Bonus #6 – Training to Start Affiliate Marketing</li>
								<li>Super Value Bonus #7 – Video Training on Viral Marketing</li>
								<li>Super Value Bonus #8 – Video Marketing Profit Kit</li>
                              </ul>
                           </div>
                           <div class="table-btn">
                              <div class="hideme-button">
                                 <a href="https://warriorplus.com/o2/buy/fmtj72/gk3xc1/x4080g"><img src="https://warriorplus.com/o2/btn/fn200011000/fmtj72/gk3xc1/303690" class="img-fluid d-block mx-auto" border="0"></a>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!--<div class="col-12 mt30">
                  <div class="f-md-22 f-20 w500 lh150 text-center">
                     *Commercial License Is ONLY Available with The Purchase Of Premium Plan
                  </div>
               </div>-->
            </div>
         </div>
      </div>
      <!-- Table Section End -->
      <!-- Guarantee Section Start -->
      <div class="guarantee-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-40 f-24 w500 lh150 white-clr text-center">
                  We’re Backing Everything Up with An Iron Clad...
                  <span class="w700 orange-clr f-md-45 f-28">"30-Day Risk-Free Money Back Guarantee"</span>
               </div>
            </div>
            <div class="row d-flex align-items-center flex-wrap mt20 mt-md60">
               <div class="col-md-8 col-12 order-md-2 offset-md-0">
                  <div class="f-md-20 f-18 lh150 w400 white-clr text-xs-center">
                     I'm 100% confident that Buzzify will help you take your online business to the next level. I'm so confident on it that I'm making this offer a risk-free investment for you.<br><br> If you concluded that, HONESTLY nothing of this
                     has helped you in any way, you can take advantage of our "30-day Money Back Guarantee" and simply ask for a refund within 30 days!<br><br>
                     <span class="">Note:</span> For refund, we will require a valid reason along with the proof that you tried our system, but it didn't work for you!<br><br> I am considering your money to be kept safe on the table between us and
                     waiting for you to APPLY and successfully use this product, and get best results, so then you can feel it is a great investment.
                  </div>
               </div>
               <div class="col-md-4 order-md-1 col-12 mt20 mt-md0">
                  <img src="assets/images/mbg.png" class="img-fluid d-block mx-auto wh300">
               </div>
            </div>
         </div>
      </div>
      <!-- Guarantee Section End -->
      <!-- Awesome Section Start -->
      <div class="awesome-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-18 f-md-20 w400 lh150 black-clr text-xs-center">
                     <span class="w600">You can agree that the price we're asking is extremely low.</span> The price is rising with every few hours, so it won't be long until it's more than double what it is today!<br><br> We could easily charge hundreds of dollars for a revolutionary
                     tool like this, but we want to offer you an attractive and affordable price that will finally <span class="w600">help you build self-updating TRENDY websites - without wasting tons of money!</span> <br><br> <span class="w600">So, take action now... and I promise you won't be disappointed! </span>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md70">
                  <div class="d-flex align-items-center justify-content-center">
                     <img src="assets/images/limited-time-offer-banner.png" class="img-fluid d-block mx-auto">
                  </div>
                  <div class="f-20 f-md-26 w800 lh150 text-center mt15 black-clr">
                     Grab Buzzify Now at 80% Launch Special Discount!
                  </div>
                  <div class="f-18 f-md-20 w500 lh150 text-center mt20 black-clr">
                     (Free Commercial License + Low 1-Time Price + 8 Special Bonuses worth $2285)
                  </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center">
                        <a href="https://warriorplus.com/o2/buy/fmtj72/gk3xc1/x4080g" class="cta-link-btn">Get Instant Access To Buzzify</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                     <img src="assets/images/compaitable-with.png" class="img-responsive mx-xs-center md-img-right">
                     <div class="d-lg-block d-none visible-md px-md30"><img src="assets/images/v-line.png" class="img-fluid"></div>
                     <img src="assets/images/days-gurantee.png" class="img-responsive mx-xs-auto mt15 mt-md0">
                  </div>
               </div>
               <div class="col-12 w600 f-md-28 f-22 text-start mt20 mt-md100">To your success</div>
               <div class="col-12  col-md-8 mx-auto mt20 mt-md50">
                  <div class="row">
                     <div class="col-md-6 col-12 text-center mt20 mt-md0">
                        <img src="assets/images/ayush-jain.png" class="img-fluid d-block mx-auto">
                     </div>
                     <div class="col-md-6 col-12 text-center mt20 mt-md0">
                        <img src="assets/images/pranshu-gupta.png" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50">
                  <div class="f-18 f-md-20 w400 lh150 black-clr text-xs-center">
                     <span class="w600">P.S- You can try "Buzzify" for 30 days without any worries.</span><br><br> Listen, we know there are a lot of crappy software tools out there that will get you nowhere. Most of the software is overpriced and an
                     absolute waste of money. So, if you're a bit sceptical, that's perfectly fine. I'm so sure you'll see the potential of this ground-breaking software that I'll let you try it out 100% risk-free.
                     <br><br> Just test it for 30 days and if you're not able to build jaw-dropping websites packed with Trending Videos, Content, News & Articles  and we cannot help you in any way, you will be eligible for a refund - no tricks, no hassles. 
                     <br><br>
                     <span class="w600">P.S.S Don't Procrastinate - Get your copy of Buzzify! </span><br><br> By now you should be really excited about all the wonderful benefits of such an amazing piece of software. You don't want to
                     miss out on the wonderful opportunity presented today... And then regret later when it costs more than double... or it's even completely off the market!
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Awesome Section End -->
      <!-- FAQ Section Start -->
      <div class="faq-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-45 f-28 w700 lh150 text-center black-clr">
                  Frequently Asked <span class="orange-clr">Questions</span>
               </div>
               <div class="col-12 mt20 mt-md30">
                  <div class="row">
                     <div class="col-md-6 col-12">
                        <div class="faq-list">
                           <div class="f-md-22 f-20 w600 black-clr lh150">
                              What exactly Buzzify is all about? 
                           </div>
                           <div class="f-18 w400 lh150 mt10 text-start">
							  Buzzify is the ultimate push-button software that creates self-updating websites packed with awesome & TRENDY content, drives FREE viral traffic & makes handsfree commissions, ad profits & sales. 
                           </div>
                        </div>
                        <div class="faq-list">
                           <div class="f-md-22 f-20 w600 black-clr lh150">
                              Is my investment risk free?
                           </div>
                           <div class="f-18 w400 lh150 mt10 text-start">
                             Buzzify is the ultimate push-button software that creates self-updating websites packed with awesome & TRENDY content, drives FREE viral traffic & makes handsfree commissions, ad profits & sales. 
                           </div>
                        </div>
                        <div class="faq-list">
                           <div class="f-md-22 f-20 w600 black-clr lh150">
                              Do I have to install Buzzify?
                           </div>
                           <div class="f-18 w400 lh150 mt10 text-start">
                              NO! Buzzify is fully cloud based. Just create an account and you can get started immediately online. It is 100% web-based platform hosted on the cloud. This means you never have to download anything ever. You can access it at any time from any device that
                              has an internet connection.
                           </div>
                        </div>
                        <div class="faq-list">
                           <div class="f-md-22 f-20 w600 black-clr lh150">
                              Is it ‘Newbie Friendly’?
                           </div>
                           <div class="f-18 w400 lh150 mt10 text-start">
                              Yep, my friend, Buzzify is 100% newbie friendly. We know that there are a lot of technical hassles that most software has, but our software is a cut above the rest, and everyone can use it with complete ease.
                           </div>
                        </div>
                        <div class="faq-list">
                           <div class="f-md-22 f-20 w600 black-clr lh150">
                              Do you charge any monthly fees?
                           </div>
                           <div class="f-18 w400 lh150 mt10 text-start">
                              NOT AT ALL. There are NO monthly fees to use Buzzify during the launch period. During this period, you pay once and never again. We always believe in providing complete value for your money.
                           </div>
                        </div>
                     </div>
                     <div class="col-md-6 col-12">
                        <div class="faq-list">
                           <div class="f-md-22 f-20 w600 black-clr lh150">
                              Is Buzzify easy to use?
                           </div>
                           <div class="f-18 w400 lh150 mt10 text-start">
                              I bet you it’s the easiest tool you might have seen so far. Our #1 priority during the development of the software was to make it simple and easy for everyone. There is nothing to install, just create your account and login to take a plunge into hugely
                              profitable video marketing arena.
                           </div>
                        </div>
                        <div class="faq-list">
                           <div class="f-md-22 f-20 w600 black-clr lh150">
                              How do I know how well my campaigns are working?
                           </div>
                           <div class="f-18 w400 lh150 mt10 text-start">
                              You can see real-time reports for your campaigns in your account dashboard and make changes accordingly. But there’s a small catch, you need to upgrade your purchase to use these benefits.
                           </div>
                        </div>
                        <div class="faq-list">
                           <div class="f-md-22 f-20 w600 black-clr lh150">
                              Will I able to drive hordes of viral traffic from day one?
                           </div>
                           <div class="f-18 w400 lh150 mt10 text-start">
                              Well, that depends on how well you make the use of this ultimate software. We’ve created this from grounds up to make everything simple and easy and ensure that you move ahead without any hassles.
                           </div>
                        </div>
                        <div class="faq-list">
                           <div class="f-md-22 f-20 w600 black-clr lh150">
                              Will I get any training or support?
                           </div>
                           <div class="f-18 w400 lh150 mt10 text-start">
                              YES. We made detailed and step-by-step training videos that show you every step of how to get setup and you can access them in the member’s area.
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50">
                  <div class="f-22 f-md-28 w600 black-clr px0 text-xs-center lh150">If you have any query, simply reach to us at <a href="mailto:support@bizomart.com" class="blue-clr">support@bizomart.com</a></div>
               </div>
               <div class="col-12 mt20 mt-md70">
                  <div class="d-flex align-items-center justify-content-center">
                     <img src="assets/images/limited-time-offer-banner.png" class="img-fluid d-block mx-auto">
                  </div>
                  <div class="f-20 f-md-26 w800 lh150 text-center mt15 black-clr">
                     Grab Buzzify Now at 80% Launch Special Discount!
                  </div>
                  <div class="f-18 f-md-20 w500 lh150 text-center mt20 black-clr">
                     (Free Commercial License + Low 1-Time Price + 8 Special Bonuses worth $2285)
                  </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center">
                        <a href="https://warriorplus.com/o2/buy/fmtj72/gk3xc1/x4080g" class="cta-link-btn">Get Instant Access To Buzzify</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                     <img src="assets/images/compaitable-with.png" class="img-responsive mx-xs-center md-img-right">
                     <div class="d-lg-block d-none visible-md px-md30"><img src="assets/images/v-line.png" class="img-fluid"></div>
                     <img src="assets/images/days-gurantee.png" class="img-responsive mx-xs-auto mt15 mt-md0">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- FAQ Section End -->
      <div class="footer-section">
         <div class="container ">
            <div class="row">
               <div class="col-12 text-center">
                  <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 973.3 276.9" style="enable-background:new 0 0 973.3 276.9; max-height:40px;" xml:space="preserve">
                     <style type="text/css">
                        .st0{fill:#FFFFFF;}
                     </style>
                     <g>
                        <path class="st0" d="M0,212.5V12.1h98.6c19.3,0,34.4,4.2,45.3,12.7c10.9,8.5,16.4,21,16.4,37.7c0,20.8-8.6,35.5-25.7,44.1
                           c14,2.2,24.8,7.6,32.2,16.1c7.5,8.5,11.2,19.7,11.2,33.6c0,11.7-2.6,21.8-7.9,30.2c-5.3,8.4-12.8,14.8-22.5,19.3
                           c-9.7,4.5-21.2,6.7-34.5,6.7H0z M39.2,93.3h50.3c10.7,0,18.8-2.2,24.5-6.6c5.6-4.4,8.4-10.7,8.4-19c0-8.3-2.8-14.6-8.3-18.8
                           c-5.5-4.2-13.7-6.3-24.6-6.3H39.2V93.3z M39.2,181.9h60.6c12.3,0,21.6-2.2,27.7-6.7c6.2-4.5,9.3-11.2,9.3-20.2
                           c0-8.9-3.2-15.8-9.5-20.6c-6.4-4.8-15.5-7.2-27.5-7.2H39.2V181.9z"/>
                        <path class="st0" d="M268.1,216.4c-22.8,0-39.1-4.5-49.1-13.5c-10-9-15-23.6-15-44V88.2h37.3v61.9c0,13.1,2,22.4,6,27.7
                           c4,5.4,10.9,8.1,20.8,8.1c9.7,0,16.6-2.7,20.6-8.1c4-5.4,6.1-14.6,6.1-27.7V88.2h37.3V159c0,20.3-5,35-14.9,44
                           C307.2,211.9,290.9,216.4,268.1,216.4z"/>
                        <path class="st0" d="M356.2,212.5l68.6-95.9h-58.7V88.2h121.6L419,184.1h64.5v28.4H356.2z"/>
                        <path class="st0" d="M500.3,212.5l68.6-95.9h-58.7V88.2h121.6l-68.7,95.9h64.5v28.4H500.3z"/>
                        <path class="st0" d="M679.8,62.3c-4.1,0-7.8-1-11.1-3c-3.4-2-6-4.7-8-8.1s-3-7.1-3-11.2c0-4,1-7.7,3-11c2-3.3,4.7-6,8-8
                           c3.3-2,7-3,11.1-3c4,0,7.7,1,11.1,3c3.3,2,6,4.6,8,8c2,3.3,3,7,3,11c0,4.1-1,7.8-3,11.2c-2,3.4-4.7,6.1-8,8.1
                           C687.5,61.3,683.8,62.3,679.8,62.3z M661.2,212.5V88.2h37.3v124.4H661.2z"/>
                        <path class="st0" d="M747.9,212.5v-96.1h-16.8V88.2h16.8V57.3c0-11.7,1.8-21.9,5.5-30.5c3.6-8.6,8.9-15.2,15.7-19.9
                           c6.8-4.7,15-7,24.6-7c5.9,0,11.7,0.9,17.5,2.7c5.7,1.8,10.7,4.4,14.8,7.6l-12.9,26.1c-3.8-2.3-8.1-3.5-12.9-3.5
                           c-4.2,0-7.4,1.1-9.6,3.2c-2.2,2.1-3.7,5.1-4.4,8.9c-0.8,3.8-1.2,8.2-1.2,13.1v30.1h29.4v28.3h-29.4v96.1H747.9z"/>
                        <path class="st0" d="M835.6,275.7l40.1-78.7l-59-108.8h42.7l38,72.9l33.2-72.9h42.7l-95,187.5H835.6z"/>
                     </g>
                  </svg>
                  <div class="f-14 f-md-16 w400 mt20 lh150 white-clr text-start" style="color:#7d8398;"><span class="w600">Note:</span> This site is not a part of the Facebook website or Facebook Inc. Additionally, this site is NOT endorsed by Facebook in any way. FACEBOOK & INSTAGRAM are the trademarks of FACEBOOK, Inc.<br><br>
                     <span class="w600">Earning Disclaimer:</span> Please make your purchase decision wisely. There is no promise or representation that you will make a certain amount of money, or any money, or not lose money, as a result of using
                     our products and services. Any stats, conversion, earnings, or income statements are strictly estimates. There is no guarantee that you will make these levels for yourself. As with any business, your results will vary and will
                     be based on your personal abilities, experience, knowledge, capabilities, level of desire, and an infinite number of variables beyond our control, including variables we or you have not anticipated. There are no guarantees concerning
                     the level of success you may experience. Each person’s results will vary. There are unknown risks in any business, particularly with the Internet where advances and changes can happen quickly. The use of the plug-in and the given
                     information should be based on your own due diligence, and you agree that we are not liable for your success or failure.
                  </div>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-14 f-md-16 w400 lh150 white-clr text-xs-center">Copyright © Buzzify 2022</div>
                  <ul class="footer-ul w400 f-14 f-md-16 white-clr text-center text-md-right">
                     <li><a href="https://support.bizomart.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://buzzify.biz/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://buzzify.biz/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://buzzify.biz/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://buzzify.biz/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://buzzify.biz/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://buzzify.biz/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!-- Exit Popup and Timer -->
      <?php include('timer.php'); ?>	  
      <!-- Exit Popup and Timer End -->
   </body>
</html>
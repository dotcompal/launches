<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- Device & IE Compatibility Meta -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=9">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=9">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta name="title" content="Buzzify Special Bonuses">
    <meta name="description" content="Buzzify Special Bonuses">
    <meta name="keywords" content="Buzzify Special Bonuses">
    <meta property="og:image" content="https://www.buzzify.biz/special-bonus/thumbnail.png">
    <meta name="language" content="English">
    <meta name="revisit-after" content="1 days">
    <meta name="author" content="Ayush Jain">
    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:title" content="Buzzify Special Bonuses">
    <meta property="og:description" content="Buzzify Special Bonuses">
    <meta property="og:image" content="https://www.buzzify.biz/special-bonus/thumbnail.png">
    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:title" content="Buzzify Special Bonuses">
    <meta property="twitter:description" content="Buzzify Special Bonuses">
    <meta property="twitter:image" content="https://www.buzzify.biz/special-bonus/thumbnail.png">


<title>Buzzify  Bonuses</title>
<!-- Shortcut Icon  -->
<link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
<!-- Css CDN Load Link -->
<link rel="stylesheet" href="assets/css/font-awesome.min.css" type="text/css" />
<link rel="stylesheet" href="../common_assets/css/bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="../common_assets/css/general.css">
<link rel="stylesheet" href="assets/css/timer.css" type="text/css" />
<link rel="stylesheet" type="text/css" href="assets/css/bonus-style.css">
<link rel="stylesheet" type="text/css" href="assets/css/style.css">
<link rel="stylesheet" type="text/css" href="assets/css/bonus.css">
<link rel="stylesheet" href="assets/css/style-feature.css" type="text/css">
<link rel="stylesheet" href="assets/css/style-bottom.css" type="text/css">
<!-- Font Family CDN Load Links -->
<link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Spartan:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
    
<!-- Javascript File Load -->
<script src="../common_assets/js/jquery.min.js"></script>
<script src="../common_assets/js/popper.min.js"></script>
<script src="../common_assets/js/bootstrap.min.js"></script>   
<!-- Buy Button Lazy load Script -->


<script>
 $(document).ready(function() {
  /* Every time the window is scrolled ... */
  $(window).scroll(function() {
	 /* Check the location of each desired element */
	 $('.hideme').each(function(i) {
		 var bottom_of_object = $(this).offset().top + $(this).outerHeight();
		 var bottom_of_window = $(window).scrollTop() + $(window).height();
		 /* If the object is completely visible in the window, fade it it */
		 if ((bottom_of_window - bottom_of_object) > -200) {
			 $(this).animate({
				 'opacity': '1'
			 }, 300);
		 }
	 });
  });
 });
</script>
<!-- Smooth Scrolling Script -->
<script>
 $(document).ready(function() {
  // Add smooth scrolling to all links
  $("a").on('click', function(event) {
 
	 // Make sure this.hash has a value before overriding default behavior
	 if (this.hash !== "") {
		 // Prevent default anchor click behavior
		 event.preventDefault();
 
		 // Store hash
		 var hash = this.hash;
 
		 // Using jQuery's animate() method to add smooth page scroll
		 // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
		 $('html, body').animate({
			 scrollTop: $(hash).offset().top
		 }, 800, function() {
 
			 // Add hash (#) to URL when done scrolling (default click behavior)
			 window.location.hash = hash;
		 });
	 } // End if
  });
 });
</script>


</head>
<body>


  <!-- New Timer  Start-->
  <?php
	 $date = 'April 15 2022 11:59 PM EST';
	 $exp_date = strtotime($date);
	 $now = time();  
	 /*
	 
	 $date = date('F d Y g:i:s A eO');
	 $rand_time_add = rand(700, 1200);
	 $exp_date = strtotime($date) + $rand_time_add;
	 $now = time();*/
	 
	 if ($now < $exp_date) {
	 ?>
  <?php
	 } else {
		 echo "Times Up";
	 }
	 ?>
  <!-- New Timer End -->
  <?php
	 if(!isset($_GET['afflink'])){
	 $_GET['afflink'] = 'https://warriorplus.com/o2/a/m3tyzb/0';
	 $_GET['name'] = 'Achal & Atul';      
	 }
	 ?>


	<div class="main-header">
        <div class="container">
            <div class="row">
				<div class="col-12">
					<img src="assets/images/demo-vid1.png" class="img-fluid d-block mx-auto">
					<div class="f-md-32 f-20 lh160 w400 text-center white-clr">
						<span class="w600"><?php echo $_GET['name'];?>'s</span> special bonus for &nbsp; 	  <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
viewBox="0 0 973.3 276.9" style="enable-background:new 0 0 973.3 276.9; max-height:40px;" xml:space="preserve">
<style type="text/css">
.st0{fill:#FFFFFF;}
</style>
<g>
<path class="st0" d="M0,212.5V12.1h98.6c19.3,0,34.4,4.2,45.3,12.7c10.9,8.5,16.4,21,16.4,37.7c0,20.8-8.6,35.5-25.7,44.1
c14,2.2,24.8,7.6,32.2,16.1c7.5,8.5,11.2,19.7,11.2,33.6c0,11.7-2.6,21.8-7.9,30.2c-5.3,8.4-12.8,14.8-22.5,19.3
c-9.7,4.5-21.2,6.7-34.5,6.7H0z M39.2,93.3h50.3c10.7,0,18.8-2.2,24.5-6.6c5.6-4.4,8.4-10.7,8.4-19c0-8.3-2.8-14.6-8.3-18.8
c-5.5-4.2-13.7-6.3-24.6-6.3H39.2V93.3z M39.2,181.9h60.6c12.3,0,21.6-2.2,27.7-6.7c6.2-4.5,9.3-11.2,9.3-20.2
c0-8.9-3.2-15.8-9.5-20.6c-6.4-4.8-15.5-7.2-27.5-7.2H39.2V181.9z"/>
<path class="st0" d="M268.1,216.4c-22.8,0-39.1-4.5-49.1-13.5c-10-9-15-23.6-15-44V88.2h37.3v61.9c0,13.1,2,22.4,6,27.7
c4,5.4,10.9,8.1,20.8,8.1c9.7,0,16.6-2.7,20.6-8.1c4-5.4,6.1-14.6,6.1-27.7V88.2h37.3V159c0,20.3-5,35-14.9,44
C307.2,211.9,290.9,216.4,268.1,216.4z"/>
<path class="st0" d="M356.2,212.5l68.6-95.9h-58.7V88.2h121.6L419,184.1h64.5v28.4H356.2z"/>
<path class="st0" d="M500.3,212.5l68.6-95.9h-58.7V88.2h121.6l-68.7,95.9h64.5v28.4H500.3z"/>
<path class="st0" d="M679.8,62.3c-4.1,0-7.8-1-11.1-3c-3.4-2-6-4.7-8-8.1s-3-7.1-3-11.2c0-4,1-7.7,3-11c2-3.3,4.7-6,8-8
c3.3-2,7-3,11.1-3c4,0,7.7,1,11.1,3c3.3,2,6,4.6,8,8c2,3.3,3,7,3,11c0,4.1-1,7.8-3,11.2c-2,3.4-4.7,6.1-8,8.1
C687.5,61.3,683.8,62.3,679.8,62.3z M661.2,212.5V88.2h37.3v124.4H661.2z"/>
<path class="st0" d="M747.9,212.5v-96.1h-16.8V88.2h16.8V57.3c0-11.7,1.8-21.9,5.5-30.5c3.6-8.6,8.9-15.2,15.7-19.9
c6.8-4.7,15-7,24.6-7c5.9,0,11.7,0.9,17.5,2.7c5.7,1.8,10.7,4.4,14.8,7.6l-12.9,26.1c-3.8-2.3-8.1-3.5-12.9-3.5
c-4.2,0-7.4,1.1-9.6,3.2c-2.2,2.1-3.7,5.1-4.4,8.9c-0.8,3.8-1.2,8.2-1.2,13.1v30.1h29.4v28.3h-29.4v96.1H747.9z"/>
<path class="st0" d="M835.6,275.7l40.1-78.7l-59-108.8h42.7l38,72.9l33.2-72.9h42.7l-95,187.5H835.6z"/>
</g>
</svg>
					</div>
				</div>

				<div class="col-12 mt20 mt-md40 text-center">
					<div class="f-md-20 f-18 w600 lh160 white-clr"><span>Grab My 15 Exclusive Bonuses Before the Deal Ends…</span></div>
				</div>

				<div class="col-md-12 col-12 mt20 mt-md25 p-md0 px15">
					<div class="">
						<div class="f-md-40 f-28 w700 text-center white-clr lh160">Revealing A Breakthrough Software That Uses A Secret Method That <span class="under yellow-clr"> Makes $528/ Day Over And Over Again Using</span> The Power Of Trending Content & Videos</div>
					</div>
				</div>
				<div class="col-md-12 col-12 text-center mt20 mt-md30">
					<div class="f-18 w400 text-center lh160 white-clr">Watch My Quick Review Video</div>
				</div>
            </div>
            <div class="row mt20 mt-md30">
                    <div class="video-frame col-12 col-md-10 mx-auto">
                       <!-- <img src="assets/images/productbox.png" class="img-fluid">
                        -->
						<div style="padding-bottom: 56.25%;position: relative;"><iframe src="https://buzzify.dotcompal.com/video/embed/nikbjn6qnu" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div>
                       
                    </div>
                    <!-- <img src="assets/images/video-shadow.png" class="img-fluid hidden-xs"> -->
            </div>
        </div>
    </div>
	
	<div class="second-section">
         <div class="container">
            <div class="row">
               <div class="col-12 p-md0">
                  <div class="col-12 key-features-bg d-flex align-items-center flex-wrap">
                     <div class="row">
                        <div class="col-12 col-md-6">
                           <ul class="list-head pl0 m0 f-16 f-md-18 lh160 w400 white-clr text-capitalize">
								<li><span class="w600">Creates Beautiful & Self-Updating Sites </span>with Hot Trending Content & Videos </li>
								<li class="w600">Built-In 1-Click Traffic Generating System </li>
								<li class="w600">Puts Most Profitable Links on Your Websites using AI </li>
								<li>Legally Use Other’s Trending Content To Generate Automated Profits- <span class="w600">Works in Any Niche or TOPIC </span></li>
								<li>1-Click Social Media Automation – <span class="w600">People ONLY WANT TRENDY Topics These Days To Click.</span> </li>
								<li><span class="w600">100% SEO Friendly </span> Website and Built-In Remarketing System </li>
								<li>Complete Newbie Friendly And <span class="w600">Works Even If You Have No Prior Experience And Technical Skills</span> </li>
                           </ul>
                        </div>
                        <div class="col-12 col-md-6">
                           <ul class="list-head pl0 m0 f-16 f-md-18 lh160 w400 white-clr text-capitalize">                            							  
							<li><span class="w600">Set and Forget System</span> With Single Keyword – Set Rules To Find & Publish Trending Posts</li>
							<li><span class="w600">Automatically Translate Your Sites In 15+ Language</span> According To Location For More Traffic</li>
							<li><span class="w600">Make 5K-10K With Commercial License </span>That Allows You To Serve Your Clients & Charge Them</li>
							<li><span class="w600">Integration with Major Platforms</span> Including Autoresponders And Social Media Apps </li>
							<li><span class="w600">In-Built Content Spinner</span> to Make your Content Fresh and More Engaging </li>
							<li><span class="w600">A-Z Complete Video Training </span>Is Included To Make It Easier For You </li>
							<li><span class="w600">Limited-Time Special</span> Bonuses Worth $2285 If You Buy Today </li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md70">
               <!-- CTA Btn Section Start -->
                <!-- CTA Btn Section Start -->
				<div class="col-md-12 col-md-12 col-12 text-center">
					<div class="f-md-20 f-18 text-center mt3 black lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
					<div class="f-md-22 f-22 text-center purple lh120 w700 mt15 mt-md20 lite-black-clr">TAKE ACTION NOW!</div>
					<div class="f-md-22 f-17 lh120 w600 mt15 mt-md20">Use Coupon Code <span class="w800 lite-black-clr">"buzzearly"</span> with <span class="w800 lite-black-clr">$4 discount</span> </div>
				</div>
				<div class="col-md-12 col-12 text-center mt15 mt-md20">
					<a href="https://warriorplus.com/o2/a/m3tyzb/0" class="text-center bonusbuy-btn">
						<span class="text-center">Grab Buzzify + My 15 Exclusive Bonuses</span> <br>
					</a>
				</div>
				<div class="col-12 mt15 mt-md20 text-center">
					<img src="assets/images/payment.png" class="img-fluid">
				</div>
				<div class="col-12 mt15 mt-md20" align="center">
					<h3 class="f-md-28 f-20 w500 text-center black">Coupon Is Expiring In... </h3>
				</div>
				<!-- Timer -->
				<div class="col-12 text-center">
					<div class="countdown counter-black">
						<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Days</span> </div>
						<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
						<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
						<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
					</div>
				</div>
				<!-- Timer End -->
            </div>
         </div>
      </div>
	  <!-- Step Section End -->
		<div class="no-installation">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-md-45 f-28 w700 lh150 text-capitalize text-center black-clr">
                     Drive MASSIVE Traffic & Sales to Any Offer, <br class="d-none d-md-block"> Page or Link <span class="w700 blue-clr">In Just 3 SIMPLE Steps…</span>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md60">
                  <div class="row">
                     <div class="col-12 col-md-4">
                        <div class="steps-block">
                           <div class="step-bg">
                              <div class="f-22 f-md-26 w700 white-clr">
                                 Step #1
                              </div>
                           </div>
                           <div class="p15 p-md30">
                              <img src="assets/images/step-1.png" alt="" class="img-fluid mx-auto d-block">
                              <div class="f-18 f-md-20 w700 black-clr lh150 mt20 mt-md30 text-center">
                               Just Enter a Keyword to Publish Trending Content in Seconds 
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-12 col-md-4 mt20 mt-md0">
                        <div class="steps-block">
                           <div class="step-bg">
                              <div class="f-22 f-md-26 w700 white-clr">
                                 Step #2
                              </div>
                           </div>
                           <div class="p15 p-md30">
                              <img src="assets/images/step-2.png" alt="" class="img-fluid mx-auto d-block">
                              <div class="f-18 f-md-20 w700 black-clr lh150 mt20 mt-md30 text-center">
                                Monetize the Trend with Best Offer or Affiliate Links 
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-12 col-md-4 mt20 mt-md0">
                        <div class="steps-block">
                           <div class="step-bg">
                              <div class="f-22 f-md-26 w700 white-clr">
                                 Step #3
                              </div>
                           </div>
                           <div class="p15 p-md30">
                              <img src="assets/images/step-3.png" alt="" class="img-fluid mx-auto d-block">
                              <div class="f-18 f-md-20 w700 black-clr lh150 mt20 mt-md30 text-center">
                                Sit Back and Relax While Buzzify Works for You 24x7 
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-12 mx-auto mt40 mt-md50">
                  <div class="row">
                     <div class="col-12 col-md-4 col-md-4">
                        <!-- <img src="assets/images/no1.png" class="img-fluid d-block mx-auto"> -->
                        <div class="w800 f-18 f-md-24 black-clr text-center lh150">
                           No Download/Installation
                        </div>
                     </div>
                     <div class="col-12 col-md-4 mt20 mt-md0">
                        <!--  <img src="assets/images/no2.png" class="img-fluid d-block mx-auto"> -->
                        <div class="w800 f-18 f-md-24 black-clr text-center lh150">
                           No Prior Knowledge
                        </div>
                     </div>
                     <div class="col-12 col-md-4 mt20 mt-md0">
                        <!-- <img src="assets/images/no3.png" class="img-fluid d-block mx-auto"> -->
                        <div class="w800 f-18 f-md-24 black-clr text-center lh150">
                           100% Beginners Friendly
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md40">
                  <div class="f-18 f-md-20 w500 text-center black-clr lh150">
                     In just a few clicks, you can drive endless free traffic and convert it into passive commissions, ad profits & sales for you on autopilot
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="proof-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-45 f-28 w700 lh150 text-center black-clr">
			   We Are Getting Thousands of FREE TRAFFIC Monthly for Each Site - 100% Hands-Free
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
                  <img src="assets/images/proof.png" class="img-fluid d-block mx-auto">
               </div>
			    <div class="col-12 col-md-10 mx-auto mt20 mt-md30">
                  <img src="assets/images/proof2.png" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 mt20 mt-md80">
                  <div class="f-md-45 f-28 w700 lh150 text-center black-clr">
				  Which Made Us $16,029 Last Month - That’s an Average <u>$528 In Profits Each & Every Day</u>
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
                  <img src="assets/images/proof-03.png" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
                  <img src="assets/images/proof-04.png" class="img-fluid d-block mx-auto">
               </div>
            </div>
         </div>
      </div>
      <div class="testimonial-section2 ">
        <div class="container ">
            <div class="row ">
               <div class="col-12">
                  <div class="f-md-45 f-28 lh150 w700 text-center black-clr">Checkout What Buzzify Early <br class="d-none d-md-block"> Users Have to SAY</div>
               </div>
            </div>
            <div class="row  align-items-center">
                <div class="col-md-6 p-md0">
                    <img src="./assets/images/t1.png" alt="" class="img-fluid d-block mx-auto mt30 mt-md50">
                    <img src="./assets/images/t2.png" alt="" class="img-fluid d-block mx-auto mt30 mt-md50">
                </div>
                <div class="col-md-6 p-md0">
                    <img src="./assets/images/t3.png" alt="" class="img-fluid d-block mx-auto mt30 mt-md50">
                    <img src="./assets/images/t4.png" alt="" class="img-fluid d-block mx-auto mt30 mt-md50">
                </div>
            </div>
        </div>
    </div>

	<!-- CTA Button Section Start -->
	<div class="cta-btn-section dark-cta-bg">
	   <div class="container">
		  <div class="row">
			 <div class="col-md-12 col-md-12 col-12 text-center">
				<div class="f-md-20 f-18 text-center mt3 white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
				<div class="f-md-22 f-22 text-center purple lh120 w700 mt15 mt-md20 orange-clr">TAKE ACTION NOW!</div>
				<div class="f-md-22 f-17 lh120 w600 mt15 mt-md20 white-clr">Use Coupon Code <span class="w800 orange-clr">"buzzearly"</span> with <span class="w800 orange-clr">$4 discount</span> </div>
			 </div>
			 <div class="col-md-12 col-12 text-center mt15 mt-md20">
				<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
				<span class="text-center">Grab Buzzify + My 15 Exclusive Bonuses</span> <br>
				</a>
			 </div>
			 <div class="col-12 mt15 mt-md20 text-center">
				<img src="assets/images/payment.png" class="img-fluid">
			 </div>
			 <div class="col-12 mt15 mt-md20" align="center">
				<h3 class="f-md-28 f-20 w500 text-center white-clr">Coupon Is Expiring In... </h3>
			 </div>
			 <!-- Timer -->
			 <div class="col-12 text-center">
				<div class="countdown counter-white white-clr">
				   <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Days</span> </div>
				   <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
				   <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
				   <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
				</div>
			 </div>
			 <!-- Timer End -->
		  </div>
	   </div>
	</div>
	<!-- CTA Button Section End -->
	<div class="thatsall-section">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-10 mx-auto text-center">
                  <div class="f-30 f-md-50 w700 shape-testi1 purple-hd">No Doubt</div>
               </div>
               <div class="col-12 f-md-38 f-28 w700 white-clr lh150 text-center mt20">
                  Old & Boring Content Is NOT Working Today... 
               </div>
               <div class="col-12 text-center f-md-20 f-18 w400 white-clr lh150 mt10">
                 Those are not working on social media, not on YouTube and not even worth publishing on your own website. And that’s the reason why you don’t get FREE traffic, loose sales & commission and must think of PAID traffic methods my friend. 
               </div>
			    <div class="col-12 text-center f-md-20 f-18 w400 white-clr lh150 mt20 mt-md50">
                But before revealing our secret. Let me ask you a question! 
               </div>
               <div class="col-12 text-center f-md-30 f-22 w600 white-clr lh150 mt20 mt-md10">
                 How many times you have read or watched about headlines like? 
               </div>
            </div>
            <div class="row mt-md50 mt20 align-items-center">
               <div class="col-12 col-md-10 mx-auto">
                  <div class="avengame-bg">
                     <p class="f-20 f-md-30 w700 black-clr lh150">10 reasons why G.O.T season 8</p>
                     <div class="f-24 f-md-32 w500 black-clr lh150"> is going to change the series forever</div>
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
                  <div class="avengame-bg">
                     <p class="f-20 f-md-30 w700 black-clr lh150">We finally understand why Marvel</p>
                     <div class="f-24 f-md-32 w500 black-clr lh150">  started with iron man</div>
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
                  <div class="avengame-bg">
                     <p class="f-20 f-md-30 w700 black-clr lh150">One team who shouldn’t win</p>
                     <div class="f-24 f-md-32 w500 black-clr lh150">the Fortnite cup for sure</div>
                  </div>
               </div>
               <div class="col-12 text-center f-md-30 f-22 w600 white-clr lh150 white-clr lh150 mt20 mt-md50">
                 We all have clicked & read such viral post while surfing online regularly.<u> People only want trendy articles and videos.</u> This is working like a miracle 
               </div>
			   
			   <div class="col-12 f-md-38 f-28 w700 yellow-clr lh150 text-center mt20 mt-md70">
                 Trending Content Is The WINNER In 2022 & Beyond... 
               </div>
               <div class="col-12 text-center f-md-20 f-18 w400 white-clr lh150 mt10">
                 The idea is simple, trending content you have, more traffic you get, more leads and commission you generate. 
               </div>
            </div>
         </div>
      </div>
      <div class="still-there-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-28 f-md-42 w700 lh150 text-center  black-clr">
                     And Do You Know How Much People Are Making Using The POWER of Trending Content & Videos? 
                  </div>
               </div>
            </div>
            <div class="row d-flex align-items-center flex-wrap mt-md50 mt30">
               <div class="col-8 mx-auto col-md-4">
                  <img src="assets/images/ryan.png" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 col-md-8 mt30 mt-md0">
                  <div class="f-md-24 f-20 lh150 w400 pl-md130 text-center text-md-start">
                     <span class="w600">Here’s “Ryan’s World” – </span> A 10-Year-Old Kid that has over 31.9M Subscribers with whooping <span class="w600">Net Worth of $32M... </span>
                  </div>
                  <div class="f-md-24 f-20 lh150 w400 pl-md130 text-center text-md-start mt20">
                     Yes! You heard me right... He is just 10-year-kid but making BIG.
                  </div>
                  <img src="assets/images/left-arrow.png" class="img-fluid d-none d-md-block mt10">
               </div>
            </div>
            <div class="row d-flex align-items-center flex-wrap mt-md50 mt30">
               <div class="col-8 mx-auto col-md-5 order-md-2">
                  <img src="assets/images/gary-vaynerchuk.png" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 col-md-7 mt30 mt-md0 order-md-1">
                  <div class="f-md-24 f-20 lh150 w400 text-center text-md-start">
                     Even Pro marketers are bidding heavily on trendy content:
                  </div>
                  <div class="f-md-28 f-24 lh150 w400 text-center text-md-start mt20">
                     <span class="w600">Mr. Gary Vaynerchuk,</span> an entrepreneur and business Guru who’s also known as the first wine guru has <span class="w600">Net Worth of $200M+</span>
                  </div>
                  <img src="assets/images/right-arrow.png" class="img-fluid d-none d-md-block mt10 ms-md-auto">
               </div>
            </div>
         </div>
      </div>
      <!-- Suport Section Start -->
      <div class="support-section">
         <div class="container ">
            <div class="row">
               <div class="col-12 ">
                  <div class="f-md-32 f-24 w500 lh150 black-clr text-center ">
                     And Now You Will Be Shocked to Know How Much These 
                     <br><span class="blue-clr f-md-45 f-28 w700 d-block mt10 mt-md15 ">Viral Content Sites Make Online!</span>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50 ">
                  <div class="row no-gutters ">
                     <!-- first -->
                     <div class="col-12 col-md-4 ">
                        <div class="boxbg boxborder-rb boxborder top-first ">
                           <img src="assets/images/buzzfeed.png " class="img-fluid d-block mx-auto ">
                           <div class="f-20 f-md-22 w600 lh150 mt15 mt-md40 text-center black-clr ">
                              5 Billion Traffic<br>
                              <span class="green-clr w900">$300M </span>Revenue
                           </div>
                        </div>
                     </div>
                     <!-- second -->
                     <div class="col-12 col-md-4 ">
                        <div class="boxbg boxborder-rb boxborder ">
                           <img src="assets/images/upworthy.png " class="img-fluid d-block mx-auto ">
                           <div class="f-20 f-md-22 w600 lh150 mt15 mt-md40 text-center black-clr ">
                              5 Million Traffic<br>
                              <span class="green-clr w900">$10M </span>Revenue
                           </div>
                        </div>
                     </div>
                     <!-- third -->
                     <div class="col-12 col-md-4 ">
                        <div class="boxbg boxborder-b boxborder ">
                           <img src="assets/images/wittyfeed.png " class="img-fluid d-block mx-auto ">
                           <div class="f-20 f-md-22 w600 lh150 mt15 mt-md40 text-center black-clr ">
                              1 Million Traffic<br>
                              <span class="green-clr w900">$10M</span> Revenue
                           </div>
                        </div>
                     </div>
                     <!-- fourth -->
                     <div class="col-12 col-md-4 ">
                        <div class="boxbg boxborder-r boxborder ">
                           <img src="assets/images/viralnova.png " class="img-fluid d-block mx-auto ">
                           <div class="f-20 f-md-22 w600 lh150 mt15 mt-md40 text-center black-clr ">
                              1 Million Traffic<br>
                              <span class="green-clr w900">$100M</span> Revenue
                           </div>
                        </div>
                     </div>
                     <!-- fifth -->
                     <div class="col-12 col-md-4 ">
                        <div class="boxbg boxborder-r boxborder ">
                           <img src="assets/images/techcrunch.png " class="img-fluid d-block mx-auto ">
                           <div class="f-20 f-md-22 w600 lh150 mt15 mt-md40 text-center black-clr ">
                              25 Million Traffic<br>
                              <span class="green-clr w900">$25M</span> Revenue
                           </div>
                        </div>
                     </div>
                     <!-- sixth -->
                     <div class="col-12 col-md-4 ">
                        <div class="boxbg boxborder ">
                           <img src="assets/images/wp.png " class="img-fluid d-block mx-auto ">
                           <div class="f-20 f-md-22 w600 lh150 mt15 mt-md40 text-center black-clr ">
                              200 Million Traffic<br>
                              <span class="green-clr w900">$100M</span> Revenue
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Suport Section End -->
      <div class="playbuzz-bg">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-36 f-28 w700 lh150 black-clr text-center">
                    The Huffington Post, 9GAG.TV, Liveleak, .MIC, Playbuzz Are All Making In Hundreds Of Millions Every Year… 
               </div>
               <div class="col-12 mt-md70">
                  <img src="assets/images/playfuzz.png" class="img-fluid d-block mx-auto">
               </div>
            </div>
            <div class="row mt20 mt-md70">
			 <div class="col-12 f-md-32 f-28 w700 lh150 black-clr text-center">
                    And all of that just by using other people’s trending content 
               </div>
               <div class="col-12 text-center mt20 mt-md50">
                  <div class="black-design1">
                     <div class="f-30 f-md-50 w700 lh150 text-center white-clr skew-normal">
                        Impressive, right?
                     </div>
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md20">
               <div class="col-12  f-22 f-md-28 lh160 w500 text-center black-clr lh150">
                So, it can be safely stated that… <br>
                  <span class="f-24 f-md-36 w600 mt25 d-block">Trending Content & Videos Is The BIGGEST, Traffic And Income Opportunity Right Now! </span>
               </div>
            </div>
         </div>
      </div>
	  <div class="proudly-section" id="product">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-36 f-24 w700 text-center white-clr lh150 px-md0">
                  Proudly Presenting...
               </div>
               <div class="col-12 mt-md30 mt20 text-center">
                  <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 973.3 276.9" style="enable-background:new 0 0 973.3 276.9; max-height:80px;" xml:space="preserve">
                     <style type="text/css">
                        .st0{fill:#FFFFFF;}
                     </style>
                     <g>
                        <path class="st0" d="M0,212.5V12.1h98.6c19.3,0,34.4,4.2,45.3,12.7c10.9,8.5,16.4,21,16.4,37.7c0,20.8-8.6,35.5-25.7,44.1
                           c14,2.2,24.8,7.6,32.2,16.1c7.5,8.5,11.2,19.7,11.2,33.6c0,11.7-2.6,21.8-7.9,30.2c-5.3,8.4-12.8,14.8-22.5,19.3
                           c-9.7,4.5-21.2,6.7-34.5,6.7H0z M39.2,93.3h50.3c10.7,0,18.8-2.2,24.5-6.6c5.6-4.4,8.4-10.7,8.4-19c0-8.3-2.8-14.6-8.3-18.8
                           c-5.5-4.2-13.7-6.3-24.6-6.3H39.2V93.3z M39.2,181.9h60.6c12.3,0,21.6-2.2,27.7-6.7c6.2-4.5,9.3-11.2,9.3-20.2
                           c0-8.9-3.2-15.8-9.5-20.6c-6.4-4.8-15.5-7.2-27.5-7.2H39.2V181.9z"/>
                        <path class="st0" d="M268.1,216.4c-22.8,0-39.1-4.5-49.1-13.5c-10-9-15-23.6-15-44V88.2h37.3v61.9c0,13.1,2,22.4,6,27.7
                           c4,5.4,10.9,8.1,20.8,8.1c9.7,0,16.6-2.7,20.6-8.1c4-5.4,6.1-14.6,6.1-27.7V88.2h37.3V159c0,20.3-5,35-14.9,44
                           C307.2,211.9,290.9,216.4,268.1,216.4z"/>
                        <path class="st0" d="M356.2,212.5l68.6-95.9h-58.7V88.2h121.6L419,184.1h64.5v28.4H356.2z"/>
                        <path class="st0" d="M500.3,212.5l68.6-95.9h-58.7V88.2h121.6l-68.7,95.9h64.5v28.4H500.3z"/>
                        <path class="st0" d="M679.8,62.3c-4.1,0-7.8-1-11.1-3c-3.4-2-6-4.7-8-8.1s-3-7.1-3-11.2c0-4,1-7.7,3-11c2-3.3,4.7-6,8-8
                           c3.3-2,7-3,11.1-3c4,0,7.7,1,11.1,3c3.3,2,6,4.6,8,8c2,3.3,3,7,3,11c0,4.1-1,7.8-3,11.2c-2,3.4-4.7,6.1-8,8.1
                           C687.5,61.3,683.8,62.3,679.8,62.3z M661.2,212.5V88.2h37.3v124.4H661.2z"/>
                        <path class="st0" d="M747.9,212.5v-96.1h-16.8V88.2h16.8V57.3c0-11.7,1.8-21.9,5.5-30.5c3.6-8.6,8.9-15.2,15.7-19.9
                           c6.8-4.7,15-7,24.6-7c5.9,0,11.7,0.9,17.5,2.7c5.7,1.8,10.7,4.4,14.8,7.6l-12.9,26.1c-3.8-2.3-8.1-3.5-12.9-3.5
                           c-4.2,0-7.4,1.1-9.6,3.2c-2.2,2.1-3.7,5.1-4.4,8.9c-0.8,3.8-1.2,8.2-1.2,13.1v30.1h29.4v28.3h-29.4v96.1H747.9z"/>
                        <path class="st0" d="M835.6,275.7l40.1-78.7l-59-108.8h42.7l38,72.9l33.2-72.9h42.7l-95,187.5H835.6z"/>
                     </g>
                  </svg>
               </div>
               <div class="f-md-32 f-22 w500 white-clr lh150 col-12 mt-md50 mt20 text-center">
                  A Breakthrough Software That Uses A Secret Method To Make Us $528/ Day Over And Over Again Using The Power Of Trending Content & Videos
               </div>
               <div class="col-12 mt-md30 mt20">
                  <img src="assets/images/proudly.png" class="img-fluid d-block mx-auto">
               </div>
            </div>
         </div>
      </div>
								
<!-- grand bonus update  -->

	<div class="grand">
		<div class="container">
			<div class="col-12 text-center">
				<div class="f-30 f-md-40 lh160 w700 white-clr">Grand Bonus 1 -  Coursova (FE + OTO 3) with reseller license</div>
			</div>
		</div>
	</div>

<!-- start coursova -->
<div class="coursova-bg">
                <div class="container">
                    <div class="row inner-content">
                        <div class="col-12">
                            <div class="col-12 text-center">
                                <img src="assets/images/logo-co-white.png" class="img-responsive mx-xs-center logo-height">
                            </div>
                         
                        </div>
                        <div class="col-12 text-center px-sm15">
                            <div class="mt20 mt-md50 mb20 mb-md50">
                                <div class="f-md-21 f-20 w500 lh140 text-center white-clr">
                                   Turn your passion, hobby, or skill into <u>a profitable e-learning business</u> right from your home…
                                </div>
                            </div>
                        </div>
           
                        <div class="col-12">
                     
                            <div class="col-12 heading-co-bg">
                                <div class="f-24 f-md-42 w500 text-center black-clr lh140  line-center rotate-5">								
								  <span class="w700"> Create and Sell Courses on Your Own Branded <br class="d-md-block d-none">E-Learning Marketplace</span>  without Any <br class="visible-lg"> Tech Hassles or Prior Skills
                                </div>
                            </div>
                            <div class="col-12 p0 f-18 f-md-24 w400 text-center  white-clr lh140 mt-md40 mt20">
								
								Sell your own courses or <u>choose from 10 done-for-you RED-HOT HD <br class="d-md-block d-none">video courses to start earning right away</u>

								</div>
						</div>
							  </div>
                        <div class="row">
                            <div class="col-md-7 col-12 mt-sm100 mt20 px-sm15 min-sm-video-width-left">
                          
                               <div class="col-12 p0 mt-md15">
                                    <div class="col-12 responsive-video" editabletype="video" style="z-index: 10;">
                                        <iframe src="https://coursova.dotcompal.com/video/embed/n56ppjcyr4" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5 col-12 f-20 f-md-20 worksans lh140 w400 white-clr mt-md37 mt20 pl15 min-sm-video-width-right">
                                <ul class="list-head list-head-co pl0 m0">
                                    <li>Tap Into Fast-Growing $398 Billion Industry Today</li>
                                    <li>Build Beautiful E-Learning Sites with Marketplace, Blog &amp; Members Area in Minutes</li>
                                    <li>Easily Create Courses – Add Unlimited Lessons (Video, E-Book &amp; Reports)</li>
                                    <li>Accept Payments with PayPal, JVZoo, ClickBank, W+ Etc.</li>
                                    <li>No Leads or Profit Sharing With 3rd Party Marketplaces</li>
                                    <li>Get Commercial License and Charge Clients Monthly (Yoga Classes, Gym Etc.)</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>











<!-- grand bonus update end  -->

	<div class="gr-1">
		<div class="container-fluid p0">			
			<picture>
			  <img src="assets/images/gr1.png" alt="Flowers" style="width:100%;">
			</picture>	
			<picture>
			  <img src="assets/images/gr2.png" alt="Flowers" style="width:100%;">
			</picture>		
			<picture>
			  <img src="assets/images/gr5.png" alt="Flowers" style="width:100%;">
			</picture>			
		</div>
		</div> 
	</div>
	
	<!-----ninja kash---->
		<div class="grand">
		<div class="container">
			<div class="col-12 text-center">
				<div class="f-30 f-md-40 lh160 w700 white-clr">Grand Bonus 2 - Ninjashoppe (FE + OTO 3) with reseller license </div>
			</div>
		</div>
	</div>
	<div class="grand-logo-bg">
		<div class="container">
			<div class="row">
				<div class="col-12 text-center">
					<img src="assets/images/logo-ninj.png" class="img-fluid mx-auto d-block">
				</div>
			</div>
		</div>
	</div>
	






<div class="banner">
<div class="container">
<div class="row">

<div class="col-12">
<div class=" col-12 banner-block">
<div class="text-center f-md-22  f-16 w400 lh140 mtprehl white-clr">Attention! ANY marketer/blogger who is sick &amp; tired of wasting tons of time in creating content &amp; affiliate sites with ZERO Traffic &amp; Sales?</div>

<div class="text-center f-md-39  f-25 lh140 mt30 mt-md30 w600 white-clr">
Revealing: The MOST POWERFUL App on Planet That <span class="lightyellow yellowline">Builds a Fully Functional Affiliate Shoppe in ANY Language or Niche in 60 Seconds Flat</span> That Gets You Hot Products from E-com Giants, Unique Content, Video Reviews, TRAFFIC &amp; COMMISSIONS… All Hands FREE.
</div>



<div class="text-center f-md-22 f-18 lh140 w400 mt30 mt-md30 white-clr mb30">Create Your Own Beautiful Shoppe Today with Zero Technical &amp; Marketing Skills…Watch the Video
</div>



</div>
</div>




<div class="col-md-8 col-12 mx-auto mt0 mt-md2">
<div class="responsive-video">  
<object data="https://www.youtube.com/embed/GIoWXaNT5eQ?autoplay=1&amp;controls=0&amp;rel=0&amp;autohide=1&amp;nologo=1&amp;showinfo=0&amp;frameborder=0&amp;theme=light&amp;modestbranding=1"></object>
</div>

</div>


</div>
</div>
</div>


	<div class="gr-1">
		<div class="container-fluid p0">			
			<picture>
		
			  <img src="assets/images/gr3.png" alt="Flowers" style="width:100%;">
			  <img src="assets/images/gr4.png" alt="Flowers" style="width:100%;">
			</picture>				
		</div>
		</div>
	
	
  	<!-- Bonus Section Header Start -->
	<div class="bonus-header">
		<div class="container">
			<div class="row">
				<div class="col-12 col-md-10 mx-auto heading-bg text-center">
					<div class="f-24 f-md-36 lh160 w700">When You Purchase Buzzify, You Also Get <br class="d-lg-block d-none"> Instant Access To These 15 Exclusive Bonuses</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Bonus Section Header End -->

	<!-- Bonus #1 Section Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
			 <div class="col-12 text-center">
			
			 </div>
			 <div class="col-12 mt20 mt-md30">
				<div class="row d-flex align-items-center flex-wrap">
				   <div class="col-12 col-md-5 order-3 order-md-0 text-center">
					  <img src="assets/images/bonus1.png" class="img-fluid">
				   </div>
				   <div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				  <div class="text-md-start text-center">
				  <div class="bonus-title-bg mb-4">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 1</div>
				</div>
				  </div>
					  <div class="f-22 f-md-34 w600 lh160 bonus-title-color">
						 Content Marketing Formula
					  </div>
					  <ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
						<li>In this guide you will learn how content marketing works, why it is crucial for your business and how to harness it in a way that completely transforms your success. </li>
						<li>For the content our software Buzzify will do the magic but this Bonus will help you to build your own brand without spending a single penny. </li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #1 Section End -->
	<!-- Bonus #2 Section Start -->
	<div class="section-bonus">
	   <div class="container">
		
			 <div class="col-12 mt20 mt-md30">
				<div class="row align-items-center flex-wrap">
				   <div class="col-12 col-md-5 order-3 order-md-3 text-center">
					  <img src="assets/images/bonus2.png" class="img-fluid">
				   </div>
				   <div class="col-12 col-md-7 mt-md0 mt20">
				   <div class=" text-md-start text-center">
				<div class="bonus-title-bg mb-4">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 2</div>
				</div></div>
			
					  <div class="f-22 f-md-34 w600 lh160 bonus-title-color">
						Personal Branding Blueprint
					  </div>
					  <ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
						<li>The Personal Branding Blueprint (eBook) is the one-stop shop for everything you will need to know to own a successful personal brand. </li>
					<li>This Bonus will enhance your overall Content Marketing by leveraging your Personal Engagement with your Audience. </li>  
					</ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #2 Section End -->

	<!-- Bonus #3 Section Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row ">
		
			 <div class="col-12 mt20 mt-md30">
				<div class="row align-items-center flex-wrap">
				   <div class="col-12 col-md-5 order-3 order-md-0 text-center">
					  <img src="assets/images/bonus3.png" class="img-fluid">
				   </div>
				   <div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				   <div class="text-center text-md-start">
				<div class="bonus-title-bg mb-4">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 3</div>
				</div>
			 </div>
					  <div class="f-22 f-md-34 w600 lh160 bonus-title-color">
						 Affiliate Marketing Mastery
					  </div>
					  <ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
						 <li>In this guide you will learn strategies to generating a life-changing income online & unlock the secrets to complete affiliate marketing mastery. </li>
						 <li>This Bonus when used with Buzzify will help you build a Profitable 6-Figure Affiliate Marketing Business. </li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #3 Section End -->

	<!-- Bonus #4 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row ">
		
			 <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
				<div class="row align-items-center flex-wrap">
				   <div class="col-12 col-md-5 order-3 order-md-3 text-center">
					  <img src="assets/images/bonus4.png" class="img-fluid">
				   </div>
				   <div class="col-md-7 col-12 mt20 mt-md0 ">
				   <div class="text-md-start text-center">
				<div class="bonus-title-bg mb-4">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 4</div>
				</div>
			 </div>
					  <div class="f-22 f-md-34 w600 lh160 bonus-title-color">
						Make It Happen
					  </div>
					  <ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
						<li>This is a proven powerful goal setting guide with step by step instructions and practical examples. </li>
						<li>This Bonus when combined with Buzzify or any business, will help you to achieve your goal in just few days of consistent execution. </li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #4 End -->

	<!-- Bonus #5 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row ">
		
			 <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
				<div class="row align-items-center flex-wrap">
				   <div class="col-12 col-md-5 order-3 order-md-0 text-center ">
					  <img src="assets/images/bonus5.png" class="img-fluid">
				   </div>
				   <div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				   <div class="text-md-start text-center">
				<div class="bonus-title-bg mb-4">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 5</div>
				</div>
			 </div>
					  <div class="f-22 f-md-34 w600 lh160 bonus-title-color">
						Get More Eyes On Your Content
					  </div>
					  <ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
							<li>This is a complete video guide that will help you boost your content marketing efforts best suited for 2022. </li>	
							<li>This Bonus with Buzzify will get your content viral by 10x and ultimately build you an everlasting profitable passive income business. </li>	
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #5 End -->

	<!-- CTA Button Section Start -->
	<div class="cta-btn-section dark-cta-bg">
	   <div class="container">
		  <div class="row">
			 <div class="col-md-12 col-md-12 col-12 text-center">
				<div class="f-md-20 f-18 text-center mt3 white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
				<div class="f-md-22 f-22 text-center purple lh120 w700 mt15 mt-md20 orange-clr">TAKE ACTION NOW!</div>
				<div class="f-md-22 f-17 lh120 w600 mt15 mt-md20 white-clr">Use Coupon Code <span class="w800 orange-clr">"buzzearly"</span> with <span class="w800 orange-clr">$4 discount</span> </div>
			 </div>
			 <div class="col-md-12 col-12 text-center mt15 mt-md20">
				<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
				<span class="text-center">Grab Buzzify + My 15 Exclusive Bonuses</span> <br>
				</a>
			 </div>
			 <div class="col-12 mt15 mt-md20 text-center">
				<img src="assets/images/payment.png" class="img-fluid">
			 </div>
			 <div class="col-12 mt15 mt-md20" align="center">
				<h3 class="f-md-28 f-20 w500 text-center white-clr">Coupon Is Expiring In... </h3>
			 </div>
			 <!-- Timer -->
			 <div class="col-12 text-center">
				<div class="countdown counter-white white-clr">
				   <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Days</span> </div>
				   <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
				   <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
				   <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
				</div>
			 </div>
			 <!-- Timer End -->
		  </div>
	   </div>
	</div>
	<!-- CTA Button Section End -->

	<!-- Bonus #6 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
		
			 </div>
			 <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
				<div class="row align-items-center flex-wrap">
				   <div class="col-12 col-md-5 order-3 order-md-3 text-center">
					  <img src="assets/images/bonus6.png" class="img-fluid">
				   </div>
				   <div class="col-md-7 col-12 mt20 mt-md0">
				   <div class="text-center text-md-start">
				<div class="bonus-title-bg mb-4">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 6</div>
				</div>
					  <div class="f-22 f-md-34 w600 lh160 bonus-title-color">
						List Cleaner V2
					  </div>
					  <ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
						<li>It’s a powerful tool for SEO that removes duplicate keywords, blank spaces or any keyword containing . ' , - / www, and long phrase keywords. </li>
						<li>This is an extremely fast utility that can 'clean' your keyword list in one second and ultimately save your time and manual effort. </li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #6 End -->

	<!-- Bonus #7 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
		
			 <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
				<div class="row align-items-center flex-wrap">
				   <div class="col-12 col-md-5 order-3 order-md-0 text-center">
					  <img src="assets/images/bonus7.png" class="img-fluid">
				   </div>
				   <div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				   <div class="text-md-start text-center">
				<div class="bonus-title-bg mb-4">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 7</div>
				</div>
			 </div>
					  <div class="f-22 f-md-34 w600 lh160 bonus-title-color">
						IM Guide to Website Design And Development
					  </div>
					  <ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
						<li>Discover the Time Saving, Profit Boosting Magic of Website Design And Development! </li>
						<li>This Bonus will help you create your own Blog apart from the Invaluable features of Buzzify. </li>
					</ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #7 End -->

	<!-- Bonus #8 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
		
			 <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
				<div class="row align-items-center flex-wrap">
				   <div class="col-12 col-md-5 order-3 order-md-3 text-center">
					  <img src="assets/images/bonus8.png" class="img-fluid">
				   </div>
				   <div class="col-md-7 col-12 mt20 mt-md0">
				   <div class="text-center text-md-start">
				<div class="bonus-title-bg mb-4">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 8</div>
				</div>
			 </div>
					  <div class="f-22 f-md-34 w600 lh160 bonus-title-color">
						 Goal Setting Video Site Builder Software
					  </div>
					  <ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
						 <li>A Powerful Software that instantly creates your own complete Moneymaking Video Site featuring AdSense and Amazon Ads, unique web pages, SEO solutions and much more... Built Automatically in 2 Minutes Flat! </li>
						 <li>This Powerful Bonus with Buzzify is the perfect combination to make a 6 figure business with the minimum manual effort. 

</li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #8 End -->

	<!-- Bonus #9 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row align-items-center flex-wrap">
		
			 <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
				<div class="row">
				   <div class="col-12 col-md-5 order-3 order-md-0 text-center">
					  <img src="assets/images/bonus9.png" class="img-fluid">
				   </div>
				   <div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				   <div class="text-md-start text-center">
				<div class="bonus-title-bg mb-4">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 9</div>
				</div>
			 </div>
					  <div class="f-22 f-md-34 w600 lh160 bonus-title-color">					 
						X-Treme List Build Plugin
					  </div>
					  <ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
					<li>Use this Plugin to create unlimited awesome pages in just 2 Clicks. </li>	
					<li>Get ready sales materials with the plugin. </li>	
					<li>Use this Bonus to create beautiful Landing Pages, Sales Pages, Thank-You Pages etc. to increase your traffic and conversions. </li>	
					</ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #9 End -->

	<!-- Bonus #10 start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row align-items-center flex-wrap">
		
			 <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
				<div class="row">
				   <div class="col-12 col-md-5 order-3 order-md-3 text-center">
					  <img src="assets/images/bonus10.png" class="img-fluid">
				   </div>
				   <div class="col-md-7 col-12 mt20 mt-md0">
				   <div class="text-md-start text-center">
				<div class="bonus-title-bg mb-4">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 10</div>
				</div>
			 </div>
					  <div class="f-22 f-md-34 w600 lh160 bonus-title-color">
							Lead Book Generator
					  </div>
					  <ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
							<li>With this powerful plugin, you can easily integrate Facebook Lead Ads with your autoresponder and have your leads added to your mailing list automatically. </li>
							<li>This Bonus is for anyone who wants to take their lead generation efforts to the next level and build a profitable business. </li>

					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #10 End -->

<!-- CTA Button Section Start -->
<div class="cta-btn-section dark-cta-bg">
	   <div class="container">
		  <div class="row">
			 <div class="col-md-12 col-md-12 col-12 text-center">
				<div class="f-md-20 f-18 text-center mt3 white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
				<div class="f-md-22 f-22 text-center purple lh120 w700 mt15 mt-md20 orange-clr">TAKE ACTION NOW!</div>
				<div class="f-md-22 f-17 lh120 w600 mt15 mt-md20 white-clr">Use Coupon Code <span class="w800 orange-clr">"buzzearly"</span> with <span class="w800 orange-clr">$4 discount</span> </div>
			 </div>
			 <div class="col-md-12 col-12 text-center mt15 mt-md20">
				<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
				<span class="text-center">Grab Buzzify + My 15 Exclusive Bonuses</span> <br>
				</a>
			 </div>
			 <div class="col-12 mt15 mt-md20 text-center">
				<img src="assets/images/payment.png" class="img-fluid">
			 </div>
			 <div class="col-12 mt15 mt-md20" align="center">
				<h3 class="f-md-28 f-20 w500 text-center white-clr">Coupon Is Expiring In... </h3>
			 </div>
			 <!-- Timer -->
			 <div class="col-12 text-center">
				<div class="countdown counter-white white-clr">
				   <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Days</span> </div>
				   <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
				   <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
				   <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
				</div>
			 </div>
			 <!-- Timer End -->
		  </div>
	   </div>
	</div>
	<!-- CTA Button Section End -->

	<!-- Bonus #11 start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
	
			 <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
				<div class="row align-items-center flex-wrap">
				   <div class="col-12 col-md-5 order-3 order-md-0 text-center">
					  <img src="assets/images/bonus11.png" class="img-fluid">
				   </div>
				   <div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				   <div class="text-md-start text-center">
				<div class="bonus-title-bg mb-4">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 11</div>
				</div>
			 </div>
					  <div class="f-22 f-md-34 w600 lh160 bonus-title-color">
						Quick Cash Traffic System
					  </div>
					  <ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
					<li>Discover How to Get Instant Traffic and Leads For Your Business with No Complicated, Confusing and Expensive Strategies. </li>		
					<li>This Bonus is a plus with Buzzify that will help you generate hot traffic and leads for your business. </li>		
					</ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #11 End -->


	<!-- Bonus #12 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
		
			 <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
				<div class="row align-items-center flex-wrap">
				   <div class="col-12 col-md-5 order-3 order-md-3 text-center">
					  <img src="assets/images/bonus12.png" class="img-fluid">
				   </div>
				   <div class="col-md-7 col-12 mt20 mt-md0">
				   <div class="text-md-start text-center">
				<div class="bonus-title-bg mb-4">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 12</div>
				</div>
			 </div>
					  <div class="f-22 f-md-34 w600 lh160 bonus-title-color">
						 Exit Profiter Software
					  </div>
					  <ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
					<li>Get Your Exit Pop On Any Web Page That You Share </li>		
					<li>This bonus will increase your conversions by showing the exit popup with the attractive offer and make your lead your permanent customer. </li>		
					</ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #12 End -->

	<!-- Bonus #13 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">

			 <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
				<div class="row align-items-center flex-wrap">
				   <div class="col-12 col-md-5 order-3 order-md-0 text-center">
					  <img src="assets/images/bonus13.png" class="img-fluid">
				   </div>
				   <div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				   <div class="text-md-start text-center">
				<div class="bonus-title-bg mb-4">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 13</div>
				</div>
			 </div>
					  <div class="f-22 f-md-34 w600 lh160 bonus-title-color">
						Conversion Booster
					  </div>
					  <ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
						<li>Discover the real truth about testing and tweaking with various secrets of conversions. </li>
						<li>This Bonus is very beneficial with Buzzify to give a huge boost on your conversions and make a big profit. </li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #13 End -->

	<!-- Bonus #14 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">

			 <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
				<div class="row align-items-center flex-wrap">
				   <div class="col-12 col-md-5 order-3 order-md-3 text-center">
					  <img src="assets/images/bonus14.png" class="img-fluid">
				   </div>
				   <div class="col-md-7 col-12 mt20 mt-md0">
				   <div class="text-md-start text-center">
				<div class="bonus-title-bg mb-4">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 14</div>
				</div>
			 </div>	  
				   <div class="f-22 f-md-34 w600 lh160 bonus-title-color">
						The Copywriter's Handbook
					  </div>
					  <ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
							
							<li>With this ebook you will learn everything you need to know about successful copywriting techniques. </li>
							<li>This Bonus with Buzzify will help you sell through words and also help you create quality articles on your blogs. </li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #14 End -->


	<!-- Bonus #15 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">

			 <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
				<div class="row align-items-center flex-wrap">
				   <div class="col-12 col-md-5 order-3 order-md-0 text-center">
					  <img src="assets/images/bonus15.png" class="img-fluid">
				   </div>
				   <div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				   <div class="text-md-start text-center">
				<div class="bonus-title-bg mb-4">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 15</div>
				</div>
			 </div>
				   <div class="f-22 f-md-34 w600 lh160 bonus-title-color">
						 Cross Link Randomizer
					  </div>
					  <ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
							<li>This cross-linking software can also be a huge help in boosting your search engine traffic as this will your site do on-page optimization. </li>
							<li>This Bonus when used with Buzzify will boost your SEO ranking on Google and ultimately give huge traffic and sales. </li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #15 End -->
	
	

	<!-- Huge Woth Section Start -->
	<div class="huge-area mt30 mt-md10">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-12 text-center">
                    <div class="f-md-60 f-30 lh120 w700 white-clr">That’s Huge Worth of</div>
                    <br>
                    <div class="f-md-60 f-30 lh120 w800 purple">$2255!</div>
                </div>
            </div>
        </div>
    </div>
	<!-- Huge Worth Section End -->

	<!-- text Area Start -->
	<div class="white-section pb-0">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-12 text-center p0">
                	<div class="f-md-36 f-25 lh160 w400">So what are you waiting for? You have a great opportunity <br class="d-lg-block d-none"> ahead + <span class="w600">My 15 Bonus Products</span> are making it a <br class="d-lg-block d-none"> <b>completely NO Brainer!!</b></div>
            	</div>
			</div>
		</div>
	</div>
	<!-- text Area End -->

	<!-- CTA Button Section Start -->
	<div class="cta-btn-section pt-3 ">
	   <div class="container">
		  <div class="row">
			 <div class="col-md-12 col-12 text-center">
				<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
				<span class="text-center">Grab Buzzify + My 15 Exclusive Bonuses</span> <br>
				</a>
			 </div>
			 
			 <div class="col-12 mt15 mt-md20" align="center">
				<h3 class="f-md-30 f-20 w500 text-center black">My Exclusive Bonuses Are Expiring in...</h3>
			 </div>
			 <!-- Timer -->
			 <div class="col-12 text-center">
				<div class="countdown counter-black">
				   <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
				   <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
				   <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
				   <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
				</div>
			 </div>
			 <!-- Timer End -->
		  </div>
	   </div>
	</div>
	<!-- CTA Button Section End -->

	
	  <!--Footer Section Start -->
	  <div class="footer-section">
        <div class="container ">
            <div class="row">
                <div class="col-12 text-center">
                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 973.3 276.9" style="enable-background:new 0 0 973.3 276.9; max-height:40px;" xml:space="preserve">
<style type="text/css">
.st0{fill:#FFFFFF;}
</style>
<g>
<path class="st0" d="M0,212.5V12.1h98.6c19.3,0,34.4,4.2,45.3,12.7c10.9,8.5,16.4,21,16.4,37.7c0,20.8-8.6,35.5-25.7,44.1
    c14,2.2,24.8,7.6,32.2,16.1c7.5,8.5,11.2,19.7,11.2,33.6c0,11.7-2.6,21.8-7.9,30.2c-5.3,8.4-12.8,14.8-22.5,19.3
    c-9.7,4.5-21.2,6.7-34.5,6.7H0z M39.2,93.3h50.3c10.7,0,18.8-2.2,24.5-6.6c5.6-4.4,8.4-10.7,8.4-19c0-8.3-2.8-14.6-8.3-18.8
    c-5.5-4.2-13.7-6.3-24.6-6.3H39.2V93.3z M39.2,181.9h60.6c12.3,0,21.6-2.2,27.7-6.7c6.2-4.5,9.3-11.2,9.3-20.2
    c0-8.9-3.2-15.8-9.5-20.6c-6.4-4.8-15.5-7.2-27.5-7.2H39.2V181.9z"/>
<path class="st0" d="M268.1,216.4c-22.8,0-39.1-4.5-49.1-13.5c-10-9-15-23.6-15-44V88.2h37.3v61.9c0,13.1,2,22.4,6,27.7
    c4,5.4,10.9,8.1,20.8,8.1c9.7,0,16.6-2.7,20.6-8.1c4-5.4,6.1-14.6,6.1-27.7V88.2h37.3V159c0,20.3-5,35-14.9,44
    C307.2,211.9,290.9,216.4,268.1,216.4z"/>
<path class="st0" d="M356.2,212.5l68.6-95.9h-58.7V88.2h121.6L419,184.1h64.5v28.4H356.2z"/>
<path class="st0" d="M500.3,212.5l68.6-95.9h-58.7V88.2h121.6l-68.7,95.9h64.5v28.4H500.3z"/>
<path class="st0" d="M679.8,62.3c-4.1,0-7.8-1-11.1-3c-3.4-2-6-4.7-8-8.1s-3-7.1-3-11.2c0-4,1-7.7,3-11c2-3.3,4.7-6,8-8
    c3.3-2,7-3,11.1-3c4,0,7.7,1,11.1,3c3.3,2,6,4.6,8,8c2,3.3,3,7,3,11c0,4.1-1,7.8-3,11.2c-2,3.4-4.7,6.1-8,8.1
    C687.5,61.3,683.8,62.3,679.8,62.3z M661.2,212.5V88.2h37.3v124.4H661.2z"/>
<path class="st0" d="M747.9,212.5v-96.1h-16.8V88.2h16.8V57.3c0-11.7,1.8-21.9,5.5-30.5c3.6-8.6,8.9-15.2,15.7-19.9
    c6.8-4.7,15-7,24.6-7c5.9,0,11.7,0.9,17.5,2.7c5.7,1.8,10.7,4.4,14.8,7.6l-12.9,26.1c-3.8-2.3-8.1-3.5-12.9-3.5
    c-4.2,0-7.4,1.1-9.6,3.2c-2.2,2.1-3.7,5.1-4.4,8.9c-0.8,3.8-1.2,8.2-1.2,13.1v30.1h29.4v28.3h-29.4v96.1H747.9z"/>
<path class="st0" d="M835.6,275.7l40.1-78.7l-59-108.8h42.7l38,72.9l33.2-72.9h42.7l-95,187.5H835.6z"/>
</g>
</svg>
                    <div class="f-14 f-md-16 w400 mt20 lh160 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
                </div>
                <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                    <div class="f-14 f-md-16 w400 lh160 white-clr text-xs-center">Copyright © Buzzify 2022</div>
                    <ul class="footer-ul w400 f-14 f-md-16 white-clr text-center text-md-right">
                        <li><a href="https://support.bizomart.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                        <li><a href="http://buzzify.biz/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                        <li><a href="http://buzzify.biz/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                        <li><a href="http://buzzify.biz/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                        <li><a href="http://buzzify.biz/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                        <li><a href="http://buzzify.biz/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                        <li><a href="http://buzzify.biz/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!--Footer Section End -->


	<!-- Exit Pop-Box Start -->
	<!-- Load UC Bounce Modal CDN Start -->
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/ouibounce/0.0.11/ouibounce.min.js"></script>

	<!-- <div id="ouibounce-modal" style="z-index:77777; display:none;">
	    <div class="underlay"></div>
	    	<div class="modal popupbg" style="display:block; ">
	        <div class="modal-header" style="border:0; padding:0;">
	            <button type="button" class="close" onclick="document.getElementById('ouibounce-modal').style.display = 'none';">&times;</button>
	       	</div>

	        <div class="modal-body">
	            <div class="innerbody">
	                <div class="col-lg-12 col-md-12 col-md-12 col-md-offset-0 col-12 col-offset-0 mt2 xsmt2">
	                    <img src="assets/images/waitimg.png" class="img-fluid mt2 xsmt2">
	                    <h2 class="text-center mt2 w500 sm25 xs20 lh160">I HAVE SOMETHING SPECIAL<br /> FOR YOU</h2>
	                    <a href="<?php echo $_GET['afflink']; ?>" class="createfree md20 sm18 xs16" style="color:#ffffff;">Stay</a>
	                </div>
	            </div>
	        </div>
	    </div>
	</div> -->

	<div class="modal fade in" id="ouibounce-modal" role="dialog" style="display:none; overflow-y:auto;">
		<div class="modal-dialog modal-dg">
			<button type="button" class="close" onclick="document.getElementById('ouibounce-modal').style.display = 'none';">&times;</button>	 
			<div class="col-12 modal-content pop-bg pop-padding">			
				<div class="col-12 modal-body text-center border-pop px0">	
					<div class="col-12 px0">
						<img src="assets/images/waitimg.png" class="img-fluid">
					</div>
					
					<div class="col-12 text-center mt20 mt-md25 px0">
						<div class="f-20 f-md-28 lh160 w600">I HAVE SOMETHING SPECIAL <br class="d-lg-block d-none"> FOR YOU</div>
					</div>

					<div class="col-12 mt20 mt-md20">
						<div class="link-btn">
							<a href="<?php echo $_GET['afflink'];?>">STAY</a>
						</div>
					</div>
				</div>		
			</div>
		</div>
	</div>

	<!-- <script type="text/javascript">
        var _ouibounce = ouibounce(document.getElementById('ouibounce-modal'),{
        aggressive: true, //Making this true makes ouibounce not to obey "once per visitor" rule
        });
    </script> -->
	<!-- Load UC Bounce Modal CDN End -->
	<!-- Exit Pop-Box End -->


	<!-- Google Tag Manager (noscript) -->

<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K95FCV8"

height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

<!-- End Google Tag Manager (noscript) -->
  <!-- Facebook Pixel Code -->
  <script>
	 ! function(f, b, e, v, n, t, s) {
	  if (f.fbq) return;
	  n = f.fbq = function() {
		 n.callMethod ?
			 n.callMethod.apply(n, arguments) : n.queue.push(arguments)
	  };
	  if (!f._fbq) f._fbq = n;
	  n.push = n;
	  n.loaded = !0;
	  n.version = '2.0';
	  n.queue = [];
	  t = b.createElement(e);
	  t.async = !0;
	  t.src = v;
	  s = b.getElementsByTagName(e)[0];
	  s.parentNode.insertBefore(t, s)
	 }(window,
	  document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');
	 fbq('init', '1777584378755780');
	 fbq('track', 'PageView');
  </script>
  <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=1777584378755780&ev=PageView&noscript=1" /></noscript>
  <!-- DO NOT MODIFY -->
  <!-- End Facebook Pixel Code -->
  <!-- Google Code for Remarketing Tag -->
  <!-------------------------------------------------- Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup --------------------------------------------------->
  <div style="display:none;">
	 <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 748114601;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		var google_user_id = '<unique user id>';
		/* ]]> */
	 </script>
	 <script type="text/javascript" src="https://www.googleadservices.com/pagead/conversion.js"></script>
	 <noscript>
		<div style="display:inline;">
		   <img height="1" width="1" style="border-style:none;" alt="" src="https://googleads.g.doubleclick.net/pagead/viewthroughconversion/748114601/?guid=ON&amp;script=0&amp;userId=<unique user id>" />
		</div>
	 </noscript>
  </div>
  <!-- Google Code for Remarketing Tag -->
  <!-- timer --->
  <?php
	 if ($now < $exp_date) {
	 
	 ?>
  <script type="text/javascript">
	 // Count down milliseconds = server_end - server_now = client_end - client_now
	 var server_end = <?php echo $exp_date; ?> * 1000;
	 var server_now = <?php echo time(); ?> * 1000;
	 var client_now = new Date().getTime();
	 var end = server_end - server_now + client_now; // this is the real end time
	 
	 var noob = $('.countdown').length;
	 
	 var _second = 1000;
	 var _minute = _second * 60;
	 var _hour = _minute * 60;
	 var _day = _hour * 24
	 var timer;
	 
	 function showRemaining() {
	  var now = new Date();
	  var distance = end - now;
	  if (distance < 0) {
		 clearInterval(timer);
		 document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
		 return;
	  }
	 
	  var days = Math.floor(distance / _day);
	  var hours = Math.floor((distance % _day) / _hour);
	  var minutes = Math.floor((distance % _hour) / _minute);
	  var seconds = Math.floor((distance % _minute) / _second);
	  if (days < 10) {
		 days = "0" + days;
	  }
	  if (hours < 10) {
		 hours = "0" + hours;
	  }
	  if (minutes < 10) {
		 minutes = "0" + minutes;
	  }
	  if (seconds < 10) {
		 seconds = "0" + seconds;
	  }
	  var i;
	  var countdown = document.getElementsByClassName('countdown');
	  for (i = 0; i < noob; i++) {
		 countdown[i].innerHTML = '';
	 
		 if (days) {
			 countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + days + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 smmltd">Days</span> </div>';
		 }
	 
		 countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + hours + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>';
	 
		 countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">' + minutes + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>';
	 
		 countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">' + seconds + '</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>';
	  }
	 
	 }
	 timer = setInterval(showRemaining, 1000);
  </script>
  <?php
	 } else {
	 echo "Times Up";
	 }
	 ?>
  <!--- timer end-->
</body>
</html>

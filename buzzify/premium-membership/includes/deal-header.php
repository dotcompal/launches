<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>DotcomPal Founder Special  One Time Price</title>
<meta name="title" content="DotcomPal Founder Special  One Time Price">
<meta name="description" content="Get lifetime access to all 20+ premium DotcomPal apps for a low one-time price and save upto $16,000/yr. Replace 10+ apps - start online and grow in one place">
<meta name="keywords" content="DotcomPal">
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="language" content="English">
<meta name="revisit-after" content="1 days">
<meta name="author" content="Dr. Amit Pareek">

<!-- Open Graph / Facebook -->
<meta property="og:type" content="website">
<meta property="og:url" content="https://www.dotcompal.com/customers-only-deal">
<meta property="og:title" content="DotcomPal Founder Special  One Time Price">
<meta property="og:description" content="Get lifetime access to all 20+ premium DotcomPal apps for a low one-time price and save upto $16,000/yr. Replace 10+ apps - start online and grow in one place">
<meta property="og:image" content="https://cdn.staticdcp.com/assets/images/whatnew1.jpg">

<!-- Twitter -->
<meta property="twitter:card" content="summary_large_image">
<meta property="twitter:url" content="https://www.dotcompal.com/customers-only-deal">
<meta property="twitter:title" content="DotcomPal Founder Special  One Time Price">
<meta property="twitter:description" content="Get lifetime access to all 20+ premium DotcomPal apps for a low one-time price and save upto $16,000/yr. Replace 10+ apps - start online and grow in one place">
<meta property="twitter:image" content="https://cdn.staticdcp.com/assets/images/whatnew1.jpg">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta name="google" content="notranslate"/>

<!-- Tell the browser to be responsive to screen width -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- Bootstrap 4 Stylesheet ----->
<?php $basedir = "https://".$_SERVER['SERVER_NAME'] . '/premium-membership/'; ?>
<?php $basedir = "http://".$_SERVER['SERVER_NAME'] . '/launches/buzzify/premium-membership/'; ?>
<!-- Favicon Icon -->
<link rel="icon" href="<?php echo $basedir; ?>assets/images/favicon.png" type="image/png">
<link rel="stylesheet" href="<?php echo $basedir; ?>assets/vendors/bootstrap/bootstrap.min.css">
<!-- Common Css Stylesheet ----->
<link rel="stylesheet" href="<?php echo $basedir; ?>assets/css/general.css">
<link rel="stylesheet" href="<?php echo $basedir; ?>assets/css/elements.css">
<!-- Bootstrap Select Dropdown Stylesheet ----->
<link rel="stylesheet" href="<?php echo $basedir; ?>assets/vendors/bootstrap/bootstrap-select.css">
<!-- DotcomPal Header Css Stylesheet ----->
<link rel="stylesheet" href="<?php echo $basedir; ?>assets/css/header.css">
<!-- DotcomPal Layout Css Stylesheet ----->	
<link rel="stylesheet" href="<?php echo $basedir; ?>assets/css/layout.css">
<link rel="stylesheet" href="<?php echo $basedir; ?>assets/css/affiliate-panel.css">
<!-- Animation Css Stylesheet ----->	
<link rel="stylesheet" href="<?php echo $basedir; ?>assets/css/custom-animation.css">
<!-- Custom Scrollbar Stylesheet ----->
<link rel="stylesheet" href="<?php echo $basedir; ?>assets/vendors/scrollbar/jquery.mCustomScrollbar.css">
<!---- Country Code Plugin Stylesheet ------>
<link rel="stylesheet" href="<?php echo $basedir; ?>assets/vendors/tel-input/intlTelInput.css">
 <!-- boostrap select dropdwon for mobile & tablet js--->
<script type="text/javascript">
	$(function () {
		$('.selectpicker').selectpicker({
			container: 'body'   
		});

		if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
			$('.selectpicker').selectpicker('mobile');
		}
	});
</script>
<!-- boostrap select dropdwon  for mobile & tablet js end--->	
</head>
<body>
<!---------- DotcomPal Login Header ---------->
<!---------- DotcomPal Login Header ---------->
<div class="trans-header pricing-header">
	<header class="relative">
		<div class="saglus-header affiliate-panel-header align-self-center d-flex">
			<nav class="navbar navbar-expand w-100 px20 px-md30 saglus-nav">
			   	<a class="navbar-brand m0" href="https://www.dotcompal.com/">
				   <img src="<?php echo $basedir; ?>assets/images/logo-white.png" class="img-fluid logo-size dcp-white-logo" alt="DotComPal">
				   <img src="<?php echo $basedir; ?>assets/images/logo.png" class="img-fluid logo-size dcp-blue-logo" alt="DotComPal">
			   	</a>
				<div class="seprate-line"></div>
		   		<!-- For Mobile -->				
				<!--- Mobile end -->
			    <div class="collapse navbar-collapse new-custom-megamenu" id="navbarSupportedContent">
					<ul class="navbar-nav w-100">
						<li class="nav-item mr-auto">
							<a class="nav-link" href="#" style="pointer-events: none;">
								Customers Only - LIMITED TIME Offer              
							</a>				
						</li>
						<li class="nav-item">
							<a class="nav-link" href="https://www.dotcompal.com/login">
								Login
							</a>
						</li>
						<li class="nav-item d-none d-md-block">
							<a class="base-btn blue-btn" title="Upgrade Now" href="#grabBtn">
								Upgrade Now
							</a>
						</li>
						<!--<li class=" nav-item dropdown caret-icon-none profile-dropdown"> 
							<a class="nav-link dropdown-toggle" data-toggle="dropdown"> 
								<img src="<?php echo $basedir; ?>assets/images/profile-pic.png" class="img-fluid profile-pic"> 
							</a>
							<div class="dropdown-menu dropdown-menu-right mCustomScrollbar h-auto" data-mcs-theme="inset-3">
				
			
						<div class="profile-header p15 px-md30 py-md20">
							<div class="media">
								 <img class="rounded-circle img-circle img-fluid" src="<?php echo $basedir; ?>assets/images/profile-pic.png" alt="">
								<div class="media-body align-self-center ml20">
									<h5 class="vd-gblue-clr w700 f-14 f-md-16">Amit Pareek</h5>
									<p class="d-gblue-clr w400 f-12 my5 break-all">dr.amitparik@gmail.com</p>
									<a href="#" class="p-blue-clr1 f-14 w400 mt2" type="button">Account Settings</a>
									
									<div class="d-flex align-items-center mt7">
										<div class="rounded-progress profile-progress-bar">
											<div class="rounded-progress-bar s-green-bg" style="width: 43%;"></div>
										</div>
										<p class="f-12 pl10 d-gblue-clr">43%</p>
									</div>
								</div>
							</div>
						</div>
						<div class="mt15">
							<a class="dropdown-item" href="#"><i class="icon-my-businesses f-16"></i>
							<div>
								<h6 class="link-title">Businesses</h6>
								<p class="sub-text">Manage all your businesses or add new to get online and grow</p>
							</div>
							</a>
							<a class="dropdown-item" href="#"><i class="icon-all-accounts f-16"></i>
							<div>
								<h6 class="link-title">Team</h6>
								<p class="sub-text">Manage all your team members or add new</p>
							</div>
							</a>
							<div class="subscription-list">
								<a class="dropdown-item" href="#"><i class="icon-subscription-other f-16"></i>
									<span class="mr100">
										<h6 class="link-title">Subscription</h6>
										<p class="sub-text">You’re using <strong class="vd-gblue-clr">DotcomPal Free Plan </strong></p>
									</span>	
								</a>
								<a href="#" class="base-btn blue-btn1 upgrdae-btn">Upgrade</a>
							</div>
							<a class="dropdown-item" href="#"><i class="icon-affiliate-program f-16"></i>
							<div>
								<h6 class="link-title">Affiliate Program</h6>
								<p class="sub-text">Refer and earn with industry’s best affiliate program</p>
							</div>
							</a>					
							<div class="link-sepration-border"></div>
							<a class="dropdown-item" href="#"><i class="icon-logout f-16"></i>
							<div>
								<h6 class="link-title">Logout</h6>						
							</div>
							</a>							
						</div>
			</div>
						</li>-->
					</ul>
			   </div>
			</nav>
		<!----- Header Right Side Links---------->
		</div>
	</header>
</div>

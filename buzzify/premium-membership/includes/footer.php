<!----------- Footer Section-------------->
<footer class="footer-section">
	<div class="container container-1170">
		<div class="row">
		
			<!--- Footer Logo & Links ----->
			<div class="col-12 col-md-5 text-center">
				<div class="col-12 p0 clearfix">
				<img src="https://cdn.staticdcp.com/apps/website/assets/images/footer-logo.png" class="img-fluid d-block mx-auto float-md-left">
				</div>	
				<p class="f-14 f-lg-16 w400 d-flex align-items-center justify-content-center justify-content-md-start mt15 mt-md25">Copyright <span class="f-20">&nbsp;&copy;&nbsp;</span> 2022, Saglus Info Pvt Ltd. </p>
			</div>
			<div class="col-12 col-md-7 d-flex align-items-end justify-content-center justify-content-md-end mt10 mt-md0 text-center pl-md0">
				<ul class="footer-bottom-links f-14 f-lg-16 w400">
					<li><a target="_blank" href="https://www.dotcompal.com/terms-condition" title="Terms & Conditions">Terms & Conditions</a></li>
					<li>|</li>
					<li><a target="_blank" href="https://www.dotcompal.com/privacy-policy" title="Privacy Policy">Privacy Policy</a></li>
					<li>|</li>
					<li><a target="_blank" href="https://www.dotcompal.com/dmca" title="DMCA Policy">DMCA Policy</a></li>
					<li>|</li>
					<li><a target="_blank" href="https://www.dotcompal.com/disclaimer" title="Cookies">Disclaimer</a></li>
				</ul>
			</div>
		</div>
	</div>
</footer>
<!----------- Footer Section end ------------->

<!-- Top Scroll Button start--->
	<a href="#" id="back-to-top" title="Back to top">
	<i class="icon-upper-dropdown" aria-hidden="true"></i>
	</a>
<!-- Top Scroll Button end --->

<!----------- Loader Section start ------------->
<div class="d-jhnone d-none" style="position: fixed; height: 100%; background: rgba(0,0,0,0.7); z-index: 99999; left: 0; right: 0; bottom: 0; right: 0;">
	<i class="icon-marketingsolution icon-loader"></i>
	<div class="page-loader"></div>
</div>
<!----------- Loader Section end ------------->

<!------------- Alerts Div start------->
<div class="alert-positions d-none">
<div class="alert alert-success-box alert-dismissible">
        <button type="button" class="close" data-dismiss="alert"><i class="icon-cross"></i></button>
        <div class="d-flex">
          <div class="d-flex flex-wrap mr15"><span class="d-table"> <i class="icon-dot-tick f-18"></i></span> </div>
          <div class="d-flex flex-wrap f-14">Well Done ! You Successfully read this alert message. </div>
        </div>
      </div>
		
      <div class="alert alert-primary-box alert-dismissible f-14">
         <button type="button" class="close" data-dismiss="alert"><i class="icon-cross"></i></button>
        <div class="d-flex">
          <div class="d-flex flex-wrap mr15"><span class="d-table"> <i class="icon icon-notification f-10"></i></span></div>
          <div class="d-flex flex-wrap">Heads Up ! This alert needs your attention, but it’s not super
            important. This could be an Informatial alert.</div>
        </div>
      </div>
		
      <div class="alert alert-primary-box alert-dismissible f-14 ">
         <button type="button" class="close" data-dismiss="alert"><i class="icon-cross"></i></button>
        <div class="d-flex">
          <div class="d-flex flex-wrap mr15"><span class="d-table"> <i class="icon icon-notification f-10"></i></span></div>
          <div class="d-flex flex-wrap">You Got a new message.. <a href="#" class="white-clr t-decoration-none"><u>Click here</u></a>&nbsp; to read </div>
        </div>
      </div>
		
      <div class="alert alert-primary-box1 f-14">
	    <div class="d-flex">
          <div class="d-flex flex-wrap mr15"><span class="d-table"><i class="icon-dot f-6 opacity20"></i> <i class="icon-dot f-6 opacity60"></i> <i class="icon-dot f-6"></i></span></div>
          <div class="d-flex flex-wrap">Just Finishing things ! Please wait...</div>
        </div>
      </div>
		
      <div class="alert alert-warning-box alert-dismissible f-14">
        <button type="button" class="close" data-dismiss="alert"><i class="icon-cross"></i></button>
        <div class="d-flex">
          <div class="d-flex flex-wrap mr15"><span class="d-table"> <i class="fa fa-exclamation-triangle f-10"></i></span></div>
          <div class="d-flex flex-wrap">Warning ! Better check yourself, you’re not looking too good.</div>
        </div>
      </div>
		
      <div class="alert alert-danger-box alert-dismissible f-14">
        <button type="button" class="close" data-dismiss="alert"><i class="icon-cross"></i></button>
        <div class="d-flex">
          <div class="d-flex flex-wrap mr15"><span class="d-table"> <i class="fa fa-exclamation-circle"></i></span></div>
          <div class="d-flex flex-wrap">Oh Snap ! Change a few things up and try submitting again.</div>
        </div>
      </div>
 </div>
<!------------- Alerts Div end--------->
<!----  Cookie consent popup section start --->
<div class="cookie-consent d-none">
<div class="container container-1170">
<div class="row align-items-center">
	<div class="col-12 col-md-8 col-lg-9 text-md-left text-center">
		<p class="f-14 w300 lh150 l-gblue-clr">DotcomPal uses cookies to provide you best online experience and making interactions with our website easy and meaningful.<br class="d-none d-xl-block">You can know more about our cookie policy <a href="privacy-policy.php" class="m-blue-clr w400">here.</a>
		</p>
	</div>	
	<div class="col-12 col-md-4 col-lg-3 text-md-right text-center mt15 mt-md0">
		<a href="javascript:" class="base-btn blue-btn mr7">Accept</a>		
		<a href="javascript:" class="base-btn white-outline-btn">Decline</a>		
	</div>
</div>	
</div>
</div>
<!----  Cookie consent popup section end ---->

<!---- Common Jquery Files ------> 
<script type='text/javascript' src="<?php echo $basedir; ?>assets/vendors/jquery/jquery.min.js"></script>
<script type='text/javascript' src="<?php echo $basedir; ?>assets/vendors/bootstrap/popper.min.js"></script> 
<script type='text/javascript' src="<?php echo $basedir; ?>assets/vendors/bootstrap/bootstrap.min.js"></script> 
<script type='text/javascript' src="<?php echo $basedir; ?>assets/vendors/bootstrap/bootstrap-select.js"></script>

<link rel="stylesheet" href="<?php echo $basedir; ?>assets/css/owl.carousel.min.css"/>
<script type='text/javascript' src="<?php echo $basedir; ?>assets/js/owl.carousel.min.js"></script>
<script type='text/javascript' src="<?php echo $basedir; ?>assets/js/carousel-slider.js"></script>

<script type='text/javascript' src="<?php echo $basedir; ?>assets/js/step-carousel.js"></script>
<!---- Add Custom Javacscript File ------> 
<script type='text/javascript' src="<?php echo $basedir; ?>assets/js/m-header-menu.js"></script>
<script type='text/javascript' src="<?php echo $basedir; ?>assets/js/script.js"></script>
<!---- Custom Scrollbar Jquery File ------>
<script type='text/javascript' src="<?php echo $basedir; ?>assets/vendors/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<!----- Country Code Plugins ---------> 
<script src="<?php echo $basedir; ?>assets/vendors/tel-input/intlTelInput.js"></script>
<script>
	// Country Code Plugins js //
	$("#phone").intlTelInput({
		autoHideDialCode: false,
		nationalMode: false,
		preferredCountries: ['in', 'us'],
		utilsScript: "<?php echo $basedir; ?>assets/vendors/tel-input/utils.js"
	});
</script>
<!----- AOS Animation ---------> 
<link rel="stylesheet" href="<?php echo $basedir; ?>assets/vendors/aos-animation/aos.css" />
<script src="<?php echo $basedir; ?>assets/vendors/aos-animation/aos.js"></script>
<script>
  AOS.init();
</script>
</body>
</html>

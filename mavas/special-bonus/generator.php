<!DOCTYPE html>
<html>
   <head>
      <title>MAVAS Generator</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=9">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
      <meta name="title" content="MAVAS Generator">
      <meta name="description" content="MAVAS Generator">
      <meta name="keywords" content="MAVAS Generator">
      <meta property="og:image" content="https://www.getmavas.com/special-bonus/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Dr. Amit Pareek">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="MAVAS Generator">
      <meta property="og:description" content="MAVAS Generator">
      <meta property="og:image" content="https://www.getmavas.com/special-bonus/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="MAVAS Generator">
      <meta property="twitter:description" content="MAVAS Generator">
      <meta property="twitter:image" content="https://www.getmavas.com/special-bonus/thumbnail.png">
      <!-- Shortcut Icon  -->
      <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
      <link rel="stylesheet" href="../common_assets/css/bootstrap.min.css" type="text/css">
      <script src="https://cdn.oppyotest.com/launches/sellero/common_assets/js/jquery.min.js"></script>
      <!--Load External CSS -->
      <link rel="stylesheet" href="assets/css/bonus-style.css" type="text/css" />
   </head>
   <body>
      <div class="whitesection">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
               <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 685 120" style="max-height:55px;">
                    <defs>
                      <style>
                        .cls-1m {
                          fill: url(#linear-gradient-2);
                        }
                  
                        .cls-2m {
                          fill: #21e5f4;
                        }
                  
                        .cls-3m {
                          fill: #0d1021;
                        }
                  
                        .cls-4m {
                          fill: url(#linear-gradient);
                        }
                      </style>
                      <linearGradient id="linear-gradient" x1="0" y1="60" x2="25.31" y2="60" gradientUnits="userSpaceOnUse">
                        <stop offset="0" stop-color="#b956ff"></stop>
                        <stop offset="1" stop-color="#8d4ef9"></stop>
                      </linearGradient>
                      <linearGradient id="linear-gradient-2" x1="99.25" y1="59.94" x2="124.57" y2="59.94" gradientUnits="userSpaceOnUse">
                        <stop offset="0" stop-color="#8d57f4"></stop>
                        <stop offset="1" stop-color="#6e33ff"></stop>
                      </linearGradient>
                    </defs>
                    <g id="Layer_1-2" data-name="Layer 1">
                      <g>
                        <g>
                          <g>
                            <polygon class="cls-3m" points="343.95 66.06 343.95 66.06 300 .53 273.46 .53 343.96 99.29 355.45 83.2 343.95 66.06"></polygon>
                            <polygon class="cls-3m" points="367.54 66.26 414.45 .53 388.11 .53 355.71 48.61 367.54 66.26"></polygon>
                          </g>
                          <polygon class="cls-3m" points="218.06 0 147.57 99.82 172.51 99.82 182.71 84.58 198.02 61.87 218.07 32.36 237.91 61.88 253.21 84.59 263.4 99.82 288.55 99.82 218.06 0"></polygon>
                          <polygon class="cls-3m" points="469.86 0 399.36 99.82 424.31 99.82 434.5 84.58 449.81 61.87 469.87 32.36 489.7 61.88 505.01 84.59 515.2 99.82 540.35 99.82 469.86 0"></polygon>
                          <g>
                            <path class="cls-3m" d="m662.66,46.82l-96.97-14.26,16.04,21.03,5.2.9,69.12,11.61c3.34.56,5.78,3.45,5.78,6.83,0,3.83-3.1,6.93-6.93,6.93h-75.79l-15.37,20.13,94.46.02c14.79,0,26.78-11.99,26.78-26.78,0-13.07-9.43-24.22-22.31-26.4Z"></path>
                            <polygon class="cls-3m" points="685 1.08 589.71 1.08 589.71 1.06 581.89 1.06 566.5 21.21 669.63 21.21 685 1.08"></polygon>
                          </g>
                        </g>
                        <g>
                          <polygon class="cls-4m" points="25.31 20.82 25.31 120 0 99.11 0 0 25.31 20.82"></polygon>
                          <polygon class="cls-1m" points="124.57 0 124.57 99.13 99.25 119.89 99.25 20.76 124.57 0"></polygon>
                          <polygon class="cls-2m" points="92.74 25.17 92.74 62.88 62.24 100.89 31.82 62.89 31.82 25.17 62.24 63.17 92.74 25.17"></polygon>
                        </g>
                      </g>
                    </g>
                  </svg>
               </div>
               <div class="col-12 f-md-45 f-28 w700 text-center black-clr lh150 mt20">
                  Here's How You Can Generate <br class="d-none d-md-block"> Your Own Bonus Page Easily
               </div>
            </div>
         </div>
      </div>
      <div class="formsection">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-7 col-12">
                  <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/product-box.webp" class="img-fluid mx-auto d-block" alt="ProductBox">
               </div>
               <div class="col-md-5 col-12 mt20 mt-md0">
                  <?php //echo "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI']; ?>
                  <?php //echo "http://" . $_SERVER['SERVER_NAME'] ."/". basename(__DIR__)."/index.php?name=".$name."&affid=".$affid."&pic=".$filename;  ?>
                  <?php
                     if(isset($_POST['submit'])) 
                     {
                        $name=$_REQUEST['name'];
                        $afflink=$_REQUEST['afflink'];
                        //$picname=$_REQUEST['pic'];
                        /*$tmpname=$_FILES['pic']['tmp_name'];
                        $type=$_FILES['pic']['type'];
                        $size=$_FILES['pic']['size']/1024;
                        $rand=rand(1111111,9999999999);*/
                        if($afflink=="")
                        {
                           echo 'Please fill the details.';
                        }
                        else
                        {
                           /*if(($type!="image/jpg" && $type!="image/jpeg" && $type!="image/png" && $type!="image/gif" && $type!="image/bmp") or $size>1024)
                           {
                              echo "Please upload image file (size must be less than 1 MB)";	
                           }
                           else
                           {*/
                              //$filename=$rand."-".$picname;
                              //move_uploaded_file($tmpname,"images/".$filename);
                              $url="https://www.getmavas.com/special-bonus/?afflink=".trim($afflink)."&name=".urlencode(trim($name));
                              echo "<a target='_blank' href=".$url.">".$url."</a><br>";
                              //header("Location:$url");
                           //}	
                        }
                     }
                     ?>
                  <form class="row" action="" method="post" enctype="multipart/form-data">
                     <div class="col-12 form_area ">
                        <div class="col-12 clear">
                           <input type="text" name="name" placeholder="Your Name..." required>
                        </div>
                        <div class="col-12 mt20 clear">
                           <input type="text" name="afflink" placeholder="Your Affiliate Link..." required>
                        </div>
                        <div class="col-12 f-24 f-md-30 white-clr center-block mt10 mt-md20 w500 clear">
                           <input type="submit" value="Generate Page" name="submit" class="f-md-30 f-24" />
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
     <!--Footer Section Start -->
<div class="footer-section">
   <div class="container">
      <div class="row">
         <div class="col-12 text-center">
         <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 685 120" style="max-height:55px">
  <defs>
    <style>
      .cls-1 {
        fill: url(#linear-gradient-2);
      }

      .cls-2 {
        fill: #fff;
      }

      .cls-3 {
        fill: #21e5f4;
      }

      .cls-4 {
        fill: url(#linear-gradient);
      }
    </style>
    <linearGradient id="linear-gradient" x1="0" y1="60" x2="25.31" y2="60" gradientUnits="userSpaceOnUse">
      <stop offset="0" stop-color="#b956ff"></stop>
      <stop offset="1" stop-color="#8d4ef9"></stop>
    </linearGradient>
    <linearGradient id="linear-gradient-2" x1="99.25" y1="59.94" x2="124.57" y2="59.94" gradientUnits="userSpaceOnUse">
      <stop offset="0" stop-color="#8d57f4"></stop>
      <stop offset="1" stop-color="#6e33ff"></stop>
    </linearGradient>
  </defs>
  <g id="Layer_1-2" data-name="Layer 1">
    <g>
      <g>
        <g>
          <polygon class="cls-2" points="343.95 66.06 343.95 66.06 300 .53 273.46 .53 343.96 99.29 355.45 83.2 343.95 66.06"></polygon>
          <polygon class="cls-2" points="367.54 66.26 414.45 .53 388.11 .53 355.71 48.61 367.54 66.26"></polygon>
        </g>
        <polygon class="cls-2" points="218.06 0 147.57 99.82 172.51 99.82 182.71 84.58 198.02 61.87 218.07 32.36 237.91 61.88 253.21 84.59 263.4 99.82 288.55 99.82 218.06 0"></polygon>
        <polygon class="cls-2" points="469.86 0 399.36 99.82 424.31 99.82 434.5 84.58 449.81 61.87 469.87 32.36 489.7 61.88 505.01 84.59 515.2 99.82 540.35 99.82 469.86 0"></polygon>
        <g>
          <path class="cls-2" d="m662.66,46.82l-96.97-14.26,16.04,21.03,5.2.9,69.12,11.61c3.34.56,5.78,3.45,5.78,6.83,0,3.83-3.1,6.93-6.93,6.93h-75.79l-15.37,20.13,94.46.02c14.79,0,26.78-11.99,26.78-26.78,0-13.07-9.43-24.22-22.31-26.4Z"></path>
          <polygon class="cls-2" points="685 1.08 589.71 1.08 589.71 1.06 581.89 1.06 566.5 21.21 669.63 21.21 685 1.08"></polygon>
        </g>
      </g>
      <g>
        <polygon class="cls-4" points="25.31 20.82 25.31 120 0 99.11 0 0 25.31 20.82"></polygon>
        <polygon class="cls-1" points="124.57 0 124.57 99.13 99.25 119.89 99.25 20.76 124.57 0"></polygon>
        <polygon class="cls-3" points="92.74 25.17 92.74 62.88 62.24 100.89 31.82 62.89 31.82 25.17 62.24 63.17 92.74 25.17"></polygon>
      </g>
    </g>
  </g>
</svg>
         </div>
         <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md60">
            <div class="f-md-18 f-16 w400 lh140 white-clr text-xs-center">Copyright © MAVAS 2023</div>
            <ul class="footer-ul w400 f-md-18 f-16 white-clr text-center text-md-right">
               <li><a href="https://support.oppyo.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
               <li><a href="http://getmavas.com/legal/privacy-policy.html " class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
               <li><a href="http://getmavas.com/legal/terms-of-service.html " class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
               <li><a href="http://getmavas.com/legal/disclaimer.html " class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
               <li><a href="http://getmavas.com/legal/gdpr.html " class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
               <li><a href="http://getmavas.com/legal/dmca.html " class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
               <li><a href="http://getmavas.com/legal/anti-spam.html " class="white-clr t-decoration-none">Anti-Spam</a></li>
            </ul>
         </div>
      </div>
   </div>
</div>
<!--Footer Section End -->
   </body>
</html>
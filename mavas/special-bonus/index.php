<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <!-- Tell the browser to be responsive to screen width -->
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
      <meta name="title" content="MAVAS Bonuses">
      <meta name="description" content="Grab My 20 Exclusive Bonuses Before the Deal Ends...">
      <meta name="keywords" content="MAVAS Bonuses">
      <meta property="og:image" content="https://www.getmavas.com/special-bonus/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Dr. Amit Pareek">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="MAVAS Bonuses">
      <meta property="og:description" content="Grab My 20 Exclusive Bonuses Before the Deal Ends...">
      <meta property="og:image" content="https://www.getmavas.com/special-bonus/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="MAVAS Bonuses">
      <meta property="twitter:description" content="Grab My 20 Exclusive Bonuses Before the Deal Ends...">
      <meta property="twitter:image" content="https://www.getmavas.com/special-bonus/thumbnail.png">
      <title>MAVAS Bonuses</title>
      <!-- Shortcut Icon  -->
      <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
      <!-- Start Editor required -->
      <link rel="stylesheet" href="../common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" type="text/css" href="assets/css/bonus-style.css">
      <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
      <script type='text/javascript' src="https://cdn.oppyotest.com/launches/sellero/common_assets/js/jquery.min.js"></script>
      <script src="../common_assets/js/jquery.min.js"></script>
      <link rel="stylesheet" href="assets/css/jquery.bxslider.min.css" type="text/css">
      <script src="assets/css/jquery.bxslider.min.js"></script>
   </head>
   <body>
       <!-- New Timer  Start-->
       <?php
         $date = 'October 25 2023 11:59 AM EST';
         $exp_date = strtotime($date);
         $now = time();  

         if ($now < $exp_date) {
         ?>
      <?php
         } else {
         	echo "";
         }
         ?>
      <!-- New Timer End -->
      <?php
         if(!isset($_GET['afflink'])){
         $_GET['afflink'] = 'https://getmavas.com/special/';
         $_GET['name'] = 'Dr. Amit Pareek';      
         }
         else {
             $_GET['afflinkbundle'] = 'https://getmavas.com/bundle/'.$_GET['afflink'];
             $fe_id_array=explode("/",$_GET['afflink']); 
             $bundle_id="400759";
             $_GET['afflinkbundle']=str_replace($fe_id_array[5],$bundle_id,$_GET['afflink']);
         }
         ?>
          <?php
         if(!isset($_GET['afflinkbundle'])){
         $_GET['afflinkbundle'] = 'https://getmavas.com/bundle/';
         $_GET['name'] = 'Dr. Amit Pareek';      
         }
         ?>
         <style>
            .tht-left{color:#fff;  font-weight:700}
         </style>  
      <!-- Header Section Start -->   
      <div class="main-header">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="text-center">
                     <div class="f-md-32 f-20 lh140 w400 white-clr d-block d-md-flex align-items-center justify-content-center">
                        <span class="w600 purple-clr"><?php echo $_GET['name'];?>'s</span> &nbsp;special bonus for &nbsp;
                        <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 685 120" style="max-height:55px">
  <defs>
    <style>
      .cls-1 {
        fill: url(#linear-gradient-2);
      }

      .cls-2 {
        fill: #fff;
      }

      .cls-3 {
        fill: #21e5f4;
      }

      .cls-4 {
        fill: url(#linear-gradient);
      }
    </style>
    <linearGradient id="linear-gradient" x1="0" y1="60" x2="25.31" y2="60" gradientUnits="userSpaceOnUse">
      <stop offset="0" stop-color="#b956ff"></stop>
      <stop offset="1" stop-color="#8d4ef9"></stop>
    </linearGradient>
    <linearGradient id="linear-gradient-2" x1="99.25" y1="59.94" x2="124.57" y2="59.94" gradientUnits="userSpaceOnUse">
      <stop offset="0" stop-color="#8d57f4"></stop>
      <stop offset="1" stop-color="#6e33ff"></stop>
    </linearGradient>
  </defs>
  <g id="Layer_1-2" data-name="Layer 1">
    <g>
      <g>
        <g>
          <polygon class="cls-2" points="343.95 66.06 343.95 66.06 300 .53 273.46 .53 343.96 99.29 355.45 83.2 343.95 66.06"></polygon>
          <polygon class="cls-2" points="367.54 66.26 414.45 .53 388.11 .53 355.71 48.61 367.54 66.26"></polygon>
        </g>
        <polygon class="cls-2" points="218.06 0 147.57 99.82 172.51 99.82 182.71 84.58 198.02 61.87 218.07 32.36 237.91 61.88 253.21 84.59 263.4 99.82 288.55 99.82 218.06 0"></polygon>
        <polygon class="cls-2" points="469.86 0 399.36 99.82 424.31 99.82 434.5 84.58 449.81 61.87 469.87 32.36 489.7 61.88 505.01 84.59 515.2 99.82 540.35 99.82 469.86 0"></polygon>
        <g>
          <path class="cls-2" d="m662.66,46.82l-96.97-14.26,16.04,21.03,5.2.9,69.12,11.61c3.34.56,5.78,3.45,5.78,6.83,0,3.83-3.1,6.93-6.93,6.93h-75.79l-15.37,20.13,94.46.02c14.79,0,26.78-11.99,26.78-26.78,0-13.07-9.43-24.22-22.31-26.4Z"></path>
          <polygon class="cls-2" points="685 1.08 589.71 1.08 589.71 1.06 581.89 1.06 566.5 21.21 669.63 21.21 685 1.08"></polygon>
        </g>
      </g>
      <g>
        <polygon class="cls-4" points="25.31 20.82 25.31 120 0 99.11 0 0 25.31 20.82"></polygon>
        <polygon class="cls-1" points="124.57 0 124.57 99.13 99.25 119.89 99.25 20.76 124.57 0"></polygon>
        <polygon class="cls-3" points="92.74 25.17 92.74 62.88 62.24 100.89 31.82 62.89 31.82 25.17 62.24 63.17 92.74 25.17"></polygon>
      </g>
    </g>
  </g>
</svg>
                     </div>
                  </div>
                  <div class="col-12 mt20 mt-md50 text-center">
                     <div class="preheadline f-20 f-md-22 w500 lh140">
                        <span class="preheadline-content white-clr">Revealing... M.A.V.A.S (Multifunctional Automated Virtual Assistant System)</span>
                     </div>
                  </div>
                  <div class="col-12 mt30 mt-md50 relative p-md0">
                  <div class="col-12 mt30 mt-md50 relative p-md0 ">
                  <div class="mainheadline f-md-42 f-25 w400 text-center white-clr lh140">
                     <div class="headline-text">
                     <!-- First-To-Market <span class="w700 neon-clr">#1 Super VA That Finish Your 100s Type of Marketing Tasks <span class="caveat">(Better &amp; Faster Than Any CMO Out There...)</span> </span> AND Trim Your Workload, Hiring/Firing Headaches, &amp; Monthly Bills </div> -->

                        <span class="w700 neon-clr">#1 Super VA That Finish 100s of Marketing Tasks <span class="caveat">(Better &amp; Faster Than Any CMO Out There...)</span> </span> &amp; Trim Your Workload, Hiring/Firing Headaches, &amp; Monthly Bills
                     </div>   
                  </div>
               </div>
               </div>
                  <div class="col-12 mt20 mt-md60 f-md-28 f-22 w700 white-clr text-center">
                  <div class="d-flex justify-content-center align-items-center flex-wrap flex-md-nowrap" style="gap: 10px;">
                     <div class="mavas-desktop">
                        MAVAS Follows Siri-like Voice Commands to
                     </div>
                     <div class="slider-wall">
                        <div class="bx-wrapper" style="max-width: 100%;"><div class="bx-viewport" aria-live="polite" style="width: 100%; overflow: hidden; position: relative; height: 75.9896px;"><ul class="bxslider text" style="width: auto; position: relative; transition-duration: 0.3s; transform: translate3d(0px, -455.938px, 0px);"><li class="text-clr3 bx-clone" style="float: none; list-style: none; position: relative; width: 500.347px;" aria-hidden="true">
                              <div class="text">Create Compelling Content</div>
                           </li>
                           <li class="text-clr1" style="float: none; list-style: none; position: relative; width: 500.347px;" aria-hidden="true">
                              <div class="text">Create Beautiful Emails… </div>
                           </li>
                           <li class="text-clr1" aria-hidden="true" style="float: none; list-style: none; position: relative; width: 500.347px;">
                              <div class="text">Create Your Course</div>
                           </li>
                           <li class="text-clr2" aria-hidden="true" style="float: none; list-style: none; position: relative; width: 500.347px;">
                              <div class="text">Dominate Social Media</div>
                           </li>
                           <li class="text-clr3" aria-hidden="true" style="float: none; list-style: none; position: relative; width: 500.347px;">
                              <div class="text">Drive Website Traffic</div>
                           </li>
                           <li class="text-clr4" aria-hidden="true" style="float: none; list-style: none; position: relative; width: 500.347px;">
                              <div class="text">Create Ads</div>
                           </li>
                           <li class="text-clr3" aria-hidden="false" style="float: none; list-style: none; position: relative; width: 500.347px;">
                              <div class="text">Optimize SEO</div>
                           </li>
                           <li class="text-clr4" aria-hidden="true" style="float: none; list-style: none; position: relative; width: 500.347px;">
                              <div class="text">Generate Quality Leads</div>
                           </li>
                           <li class="text-clr3" style="float: none; list-style: none; position: relative; width: 500.347px;" aria-hidden="true">
                              <div class="text">Create Compelling Content</div>
                           </li>
                           
                        <li class="text-clr1 bx-clone" style="float: none; list-style: none; position: relative; width: 500.347px;" aria-hidden="true">
                              <div class="text">Create Beautiful Emails… </div>
                           </li></ul></div></div>
                     </div>
                  </div>
               </div>
               <div class="row mt20 mt-md50 align-items-center">
               <div class="col-12 col-md-8 mx-auto">
                  <div class="video-box">
                     <div style="padding-bottom: 56.25%;position: relative;">
                        <iframe src="https://mavas.dotcompal.com/video/embed/knw5m7qvhx" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen="" title="JV Video"></iframe>
                     </div>
                  </div>
               </div>
               <div class="col-md-4 col-12 mt20 mt-md0">
                  <div class="calendar-wrap">
                     <ul class="list-head pl0 m0 f-16 f-md-18 lh140 w400 white-clr">
                        <li>AI Finishes 100s of Marketing Tasks in Seconds</li>
                        <li>Eliminate Monthly Salaries and High Project Costs</li>
                        <li>Easily Plan and Create Courses and E-Books Hands-Free</li>
                        <li>Boost Profits with Landing Pages, and Funnels Done Right</li>
                        <li>Profit by Selling Marketing Assets to Local Businesses</li>
                        <li>Enhance Affiliate Marketing, Email Marketing, SEO, and Paid Ads</li>
                        <li>Sell High-Demand Services with the Included Commercial License</li>
                     </ul>
                  </div>
               </div>
            </div>
               </div>
            </div>
            <!-- <div class="row mt20 mt-md40">
               <div class="col-md-10 col-12 mx-auto">
                  <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/product-box.webp" class="img-fluid mx-auto d-block" alt="ProductBox">
               </div>
            </div> -->
         </div>
      </div>
      <!-- Header Section End -->

      <!-- List Section Start -->
      <div class="second-section">
         <div class="container">
            <div class="row">
               <div class="col-12 p-md0">
               <div class="col-12 key-features-bg d-flex align-items-center flex-wrap">
                     <div class="row">
                        <div class="col-12 col-md-6">
                           <ul class="list-head1 pl0 m0 f-18 f-md-20 lh160 w400 text-capitalize">
                              <li><span class="w600">Access Predefined Tasks or Create your Own: </span>Use MAVAS to quickly carry out 100+ marketing & business operation tasks</li>
                              <li><span class="w600">Embed MAVAS on any website or Page by Just one line of code: </span>Add super-smart support to your site with a single line of code, making your website work harder for you</li>
                              <li><span class="w600">Generate Leads using MAVAS: </span>Let MAVAS bring in potential customers, so you can focus on what you do best—running your business</li>
                              <li><span class="w600">Create and/or Delete Tasks: </span>In just 1-click create NEW tasks and/or delete EXISTING tasks to be done by MAVAS</li>
                              <li><span class="w600">Fully Encrypted AI Chats: </span>Keep your chats under lock & key, so you can chat with confidence.</li>
                              <li><span class="w600">100% Chat/Talk Privacy: </span>Rest easy knowing your conversations are just that – yours.</li>
                              <li><span class="w600">Beginner Friendly: </span>No Tech or any AI skills or experience required.</li>
                           </ul>
                        </div>
                        <div class="col-12 col-md-6">
                           <ul class="list-head1 pl0 m0 f-18 f-md-20 lh160 w400 text-capitalize">
                              <li><span class="w600">Create A New Task For MAVAS: </span>Direct MAVAS to complete any new task that’s NOT predefined</li>
                              <li><span class="w600">150,000+ Niche Specific Prompts: </span>Never stress about what to say. We've got prompts for every situation, so you can just focus on being awesome</li>
                              <li><span class="w600">Access in Different Languages: </span>Connect with clients from all over the world in their language</li>
                              <li><span class="w600">Siri/Alexa Like Conversation: </span>MAVAS keeps it simple & easy, just like chatting with a virtual assistant.</li>
                              <li><span class="w600">Built-In Smart Learning: </span>MAVAS gets smarter with every chat, delivering expert-level responses</li>
                              <li><span class="w600">Authentic Conversations, Every Time: </span>Enjoy dynamic, lifelike interactions. No rehearsed answers. MAVAS keeps it fresh, like a real conversation</li>
                              <li><span class="w600">FREE Commercial License: </span>Use MAVAS for clients to create an extra income stream</li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- List Section End -->
      <!-- CTA Btn Start-->
      <!-- <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center">
                  <div class="f-md-24 f
                  
                  text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700  purple-clr">"MAVAS3"</span> for an Additional <span class="w700  purple-clr">$3 Discount</span> on MAVAS
                  </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn px-md80">
                  <span class="text-center">Grab MAVAS + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                  Use Coupon Code <span class="w700  purple-clr">"MAVAS"</span> for an Additional <span class="w700  purple-clr">$50 Discount</span> on Bundle Deal
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflinkbundle']; ?>" class="text-center bonusbuy-btn px-md80">
                  <span class="text-center">Grab MAVAS Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               
                <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 smmltd">Days</span>
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Sec</span>
                     </div>
                  </div>
               </div>
               
            </div>
         </div>
      </div> -->
      <!-- CTA Btn End -->

      <!-- <section class="step-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-50 w700 lh140 ">
                  We Have Automated Everything & Reduced Running A Business  In ANY Niche To <br> <span class="step-headline white-clr">Just 2 Steps…</span> 
                  </div>
                 
               </div>
            </div>
            <div class="row row-cols-1 row-cols-sm-2 row-cols-md-2 gap30 mt20 mt-md50">
               <div class="col">
                  <div class="step-wall">
                     <div class="step-wall-inner">
                        <div class="step-heading f-20 f-md-22 w700 lh140 white-clr">
                           Step1
                        </div>
                        <div class="">
                           <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/web-design.webp" alt="Web Design" class="mx-auto d-block img-fluid">
                        </div>
                        <div class="f-18 f-md-20 lh140 mt20 w700 text-center">
                           Embed MAVAS On Any Website/Landing <br class="d-none d-md-block"> Page With A Single Line Of Code
                        </div>
                        <div class="f-18 f-md-20 lh140 mt20 w400 text-center text-capitalize">
                           Choose from a range of specialized tasks <br class="d-none d-md-block"> or customize your own, tailored to <br class="d-none d-md-block"> your business needs.
                        </div>
                        <div class="f-18 f-md-20 lh140 mt20 w400 black-clr1 text-center text-capitalize">
                           (With the FREE Commercial License Included <br class="d-none d-md-block"> during this special launch… you can also use <br class="d-none d-md-block"> the MAVAS for your clients' projects)
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="step-wall">
                     <div class="step-wall-inner">
                        <div class="step-heading f-20 f-md-22 w700 lh140 white-clr">
                           Step2
                        </div>
                        <div class="">
                           <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/robot-assistant.webp" alt="Robot Assistant" class="mx-auto d-block img-fluid">
                        </div>
                        <div class="f-18 f-md-20 lh140 mt20 w700 text-center">
                           Command MAVAS to quickly carry out 100+ <br class="d-none d-md-block"> marketing & business operation tasks
                        </div>
                        <div class="f-18 f-md-20 lh140 mt20 w400 text-center text-capitalize">
                           Simply instruct MAVAS using simple Siri-like <br class="d-none d-md-block"> voice commands or chat, & watch it get <br class="d-none d-md-block"> to work.
                        </div>
                        <div class="f-18 f-md-20 lh140 mt20 w400 black-clr1 text-center text-capitalize">
                           (Get Paid a PREMIUM Fee for services rendered <br class="d-none d-md-block"> by MAVAS without incurring any expenses in <br class="d-none d-md-block"> salaries. Keep 100% of the profits)
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section> -->
       

      
      <!-- Bonus Section Header Start -->
      <div class="bonus-header">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-10 mx-auto heading-bg text-center">
                  <div class="f-24 f-md-36 lh140 w700"> When You Purchase MAVAS, You Also Get <br class="hidden-xs"> Instant Access To These 20 Exclusive Bonuses</div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus Section Header End -->
       <!-- Bonus #1 Section Start -->
       <div class="section-bonus">
         <div class="container">
            <div class="row">
               
               <div class="col-12 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                  
                     <div class="col-md-5 col-12">
                        <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/bonus1.webp" class="img-fluid mx-auto d-block" alt="Bonus1">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="bonus-title-bg">
                        <div class="f-22 f-md-28 lh120 w700">BONUS 1</div>
                      </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md30">
                        250 WordPress Plugins

                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                              An Amazing Collection Of 250+ Hand-Picked, Profit Oozing, Easy To Implement, Web 2.0 Compliant, Free Wordpress Plugins To Skyrocket Your Wordpress Profits Up Above The Eiffel Towers In Paris!
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #1 Section End -->
      <!-- Bonus #2 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               
               <div class="col-12">
                  <div class="row align-items-center">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/bonus2.webp" class="img-fluid mx-auto d-block" alt="Bonus2">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                     <div class="bonus-title-bg">
                         <div class="f-22 f-md-28 lh120 w700">BONUS 2</div>
                      </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md30">
                        100 Mobile Web Templates 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           Make Thousands of Dollars Online Offering Mobile Friendly Websites to Your Local Business Clients!
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #2 Section End -->
      <!-- Bonus #3 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                     <div class="col-md-5 col-12">
                        <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/bonus3.webp" class="img-fluid mx-auto d-block " alt="Bonus3">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="bonus-title-bg">
                           <div class="f-22 f-md-28 lh120 w700">BONUS 3</div>
                         </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md30">
                        Modern Affiliate Marketing Video Upgrade
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           Picking the Right Affiliate Program to Promote all Boils Down to ROI.<br>
                           Make no mistake, if you want to succeed with your affiliate marketing business, you have to focus on ROI. If you have a fuzzy idea of what return on investment means, you're playing the game wrong.
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #3 Section End -->
      <!-- Bonus #4 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/bonus4.webp" class="img-fluid mx-auto d-block " alt="Bonus4">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                     <div class="bonus-title-bg">
                         <div class="f-22 f-md-28 lh120 w700">BONUS 4</div>
                     </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md30">
                           SEO And Tracking
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           In this 6-part video course you will learn how to turn long or affiliate links into short and pretty links. Also it will teach you how to use 'related posts' to help visitors find related content.
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #4 End -->
      <!-- Bonus #5 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               
               <div class="col-12 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                     <div class="col-md-5 col-12">
                        <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/bonus5.webp" class="img-fluid mx-auto d-block " alt="Bonus5">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md30">
                     <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 5</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-colo mt20 mt-md30">
                        Social Media Marketing Boost
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           Discover 100 Powerful Social Media Marketing Tips That Will Boost Your Following, Gain Authority And Increase Engagement On Social Media!   
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #5 End -->
      <!-- CTA Button Section Start -->
      <!-- CTA Btn Start-->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 purple-clr">"MAVAS3"</span> for an Additional <span class="w700 purple-clr">$3 Discount</span> on MAVAS
                  </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn px-md80">
                  <span class="text-center">Grab MAVAS + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                  Use Coupon Code <span class="w700 purple-clr">"MAVAS"</span> for an Additional <span class="w700 purple-clr">$50 Discount</span> on Bundle Deal
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflinkbundle']; ?>" class="text-center bonusbuy-btn px-md80">
                  <span class="text-center">Grab MAVAS Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr"><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00</span><span class="f-14 f-md-18 smmltd">Days</span> </div><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00</span><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">00</span><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div><div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">00</span><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div></div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->
       <!-- Bonus #6 Start -->
       <div class="section-bonus">
         <div class="container">
            <div class="row">
               
               <div class="col-12 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/bonus6.webp" class="img-fluid mx-auto d-block " alt="Bonus6">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                     <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 6</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md30">
                        Mobile eCommerce Simplified

                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           Brand New High Quality Lead Magnet Targets Ecommerce and Online Store Owners<br><br>
                           This mobile eCommerce video course explains how to create user-friendly, mobile optimized web stores in less than 24 hours.
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #6 End -->
      <!-- Bonus #7 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               
               <div class="col-12 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                     <div class="col-md-5 col-12">
                        <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/bonus7.webp" class="img-fluid mx-auto d-block " alt="Bonus7">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="bonus-title-bg">
                         <div class="f-22 f-md-28 lh120 w700">BONUS 7</div>
                     </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md30">
                           Web Traffic Excellence
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           Discover How to Build Hundreds of Content Rich, Dynamically Changing, Keyword Covered Web Pages in Mere Minutes. Start Building the High-Quality Content Sites You Need to Succeed on the Internet Today!
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #7 End -->
      <!-- Bonus #8 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               
               <div class="col-12  d-flex align-content-center flex-wrap">
                     <div class="row align-items-center">
                        <div class="col-md-5 order-md-2 col-12">
                           <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/bonus8.webp" class="img-fluid mx-auto d-block " alt="Bonus8">
                        </div>
                        <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="bonus-title-bg">
                        <div class="f-22 f-md-28 lh120 w700">BONUS 8</div>
                     </div>
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt2- mt-md30">
                        SEO and PPC Ninja Calculator
                     </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           Quickly Make And Brand Your Own Iframes!<br><br>
                           This iFrame generator helps you to instantly create customized iFrames for your web site/blog. Fill in the fields below and within seconds you will receive the HTML code for your custom iFrame. 
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #8 End -->
      <!-- Bonus #9 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
              
               <div class="col-12 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                     <div class="col-md-5 col-12">
                        <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/bonus9.webp" class="img-fluid mx-auto d-block " alt="Bonus9">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 9</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md30">
                           Sell Grow with Content Writing Tips
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           Learn How You Can Sell More with Content Marketing!   
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #9 End -->

      <!-- Bonus #10 start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
              
               <div class="col-12 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/bonus10.webp" class="img-fluid mx-auto d-block " alt="Bonus10">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="bonus-title-bg">
                           <div class="f-22 f-md-28 lh120 w700">BONUS 10</div>
                        </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md30">
                        Doubling Your Sales With These Tricks  

                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           Learn How to Double your Sales with These Tricks!<br><br>
                           Many successful digital entrepreneurs have likely already stated this at one point or another, but this is very important, which is why this report will going to say it one more time.
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #10 End -->
     <!-- CTA Btn Start-->
     <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 purple-clr">"MAVAS3"</span> for an Additional <span class="w700 purple-clr">$3 Discount</span> on MAVAS
                  </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn px-md80">
                  <span class="text-center">Grab MAVAS + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                  Use Coupon Code <span class="w700 purple-clr">"MAVAS"</span> for an Additional <span class="w700 purple-clr">$50 Discount</span> on Bundle Deal
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflinkbundle']; ?>" class="text-center bonusbuy-btn px-md80">
                  <span class="text-center">Grab MAVAS Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr"><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00</span><span class="f-14 f-md-18 smmltd">Days</span> </div><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00</span><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">00</span><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div><div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">00</span><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div></div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Btn End -->
       <!-- Bonus #11 start -->
       <div class="section-bonus">
         <div class="container">
            <div class="row">
               
               <div class="col-12 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                     <div class="col-md-5 col-12">
                        <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/bonus11.webp" class="img-fluid mx-auto d-block " alt="Bonus11">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 11</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md30">
                        WP In-Content Popup Pro
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           WP In-Content Popup Pro is a new plugin that lets you create attention grabbing popups within your content.  You can trigger in-content video popups, image popups, text popups, or content popups which you can use to showcase your product, article or even your profile.
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #11 End -->

      <!-- Bonus #12 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
              
               <div class="col-12 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/bonus12.webp" class="img-fluid mx-auto d-block " alt="Bonus12">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 12</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md30">
                        Sales Funnel Optimization Strategy Video
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           Whether you make your money through ad clicks using the Adsense monetization platform or you sell affiliate products or your own services or you run your own online drop shipping store, you're trying to convert people from simple clickers of links and readers of your content to cold hard cash.
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #12 End -->

      <!-- Bonus #13 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
              
               <div class="col-12 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                     <div class="col-md-5 col-12">
                        <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/bonus13.webp" class="img-fluid mx-auto d-block " alt="Bonus13">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 13</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md30">
                           Introduction to Postcard Marketing
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           Postcard Marketing Video Course will provide you with a basic overview of Postcard marketing. You will receive three straight to the point lessons that will teach you exactly what it is, how it works and how you can use to effectively use it to market your own profitable business.
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #13 End -->

      <!-- Bonus #14 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               
               <div class="col-12 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/bonus14.webp" class="img-fluid mx-auto d-block " alt="Bonus14">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 14</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md30">
                        Buying Traffic to Generate Massive Website Visitors

                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        An intrinsic understanding of your competition and how to better them is the most important component of any digital marketing strategy. Today, there is such small distances between you and your competition - it's unlikely that you're offering anything that cannot be bought on some other website.
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #14 End -->

      <!-- Bonus #15 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               
               <div class="col-12 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                     <div class="col-md-5 col-12">
                        <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/bonus15.webp" class="img-fluid mx-auto d-block " alt="Bonus15">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 15</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md30">
                        Easy Survey Generator
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        It’s the software your competitors don’t want you to know! Who Else Wants To Discover The Ultimate Secret For Getting Into Your Prospect’s Heads And Boosting Your Chances For Riches!                        
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #15 End -->
     <!-- CTA Btn Start-->
     <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 purple-clr">"MAVAS3"</span> for an Additional <span class="w700 purple-clr">$3 Discount</span> on MAVAS
                  </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn px-md80">
                  <span class="text-center">Grab MAVAS + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                  Use Coupon Code <span class="w700 purple-clr">"MAVAS"</span> for an Additional <span class="w700 purple-clr">$50 Discount</span> on Bundle Deal
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflinkbundle']; ?>" class="text-center bonusbuy-btn px-md80">
                  <span class="text-center">Grab MAVAS Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr"><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00</span><span class="f-14 f-md-18 smmltd">Days</span> </div><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00</span><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">00</span><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div><div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">00</span><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div></div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Btn End -->
      <!-- Bonus #16 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/bonus16.webp" class="img-fluid mx-auto d-block " alt="Bonus16">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 16</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md30">
                        Youtube Success Step By Step

                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           YouTube Success Step By Step is a new powerful report that explains to the reader how they can create a successful YouTube channel in the fastest possible time.
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #16 End -->
      <!-- Bonus #17 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
              
               <div class="col-12 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                     <div class="col-md-5 col-12">
                        <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/bonus17.webp" class="img-fluid mx-auto d-block " alt="Bonus17">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 17</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md30">
                        Virtual Networking Success
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           Networking when you are not in the office can be hard, but it may be one of the best ways to grow your business and make sure that you meet new people.
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #17 End -->

      <!-- Bonus #18 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               
               <div class="col-12 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/bonus18.webp" class="img-fluid mx-auto d-block " alt="Bonus18">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 18</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md30">
                        Power Of Visualization Video Upgrade

                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           Attracting your best life can be challenging. You are bound to face obstacles such as fear, failure, and disappointments that will make you feel like a hopeless failure.
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #18 End -->

      <!-- Bonus #19 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               
               <div class="col-12 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                     <div class="col-md-5 col-12">
                        <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/bonus19.webp" class="img-fluid mx-auto d-block " alt="Bonus19">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 19</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md30">
                        Blogging Traffic Mantra
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        Making a living as a blogger has to be one of the sweetest gigs out there. As a blogger, you’ll be able to earn passive income which means that your money will flow in even as you’re sleeping, travelling or relaxing with friends.
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #19 End -->

      <!-- Bonus #20 start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
              
               <div class="col-12 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/bonus20.webp" class="img-fluid mx-auto d-block " alt="Bonus20">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 20</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md30">
                        Content Marketing Formula

                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           Content marketing is currently one of the biggest trends in digital marketing as a whole and is an area that many website owners and brands are investing in heavily right now thanks to the impressive returns that they are seeing.
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #20 End -->
      <!-- Huge Woth Section Start -->
      <div class="huge-area mt30 mt-md10">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="f-md-65 f-40 lh120 w700 white-clr">That's Huge Worth of</div>
                  <br>
                  <div class="f-md-60 f-40 lh120 w800 purple-clr">$3300!</div>
               </div>
            </div>
         </div>
      </div>
      <!-- Huge Worth Section End -->
      <!-- text Area Start -->
      <div class="white-section pb0">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="f-md-36 f-25 lh140 w500">So what are you waiting for? You have a great opportunity <br class="hidden-xs"> ahead + <span class="w700 ">My 20 Bonus Products</span> are making it a <br class="hidden-xs"> <span class="w700 ">completely NO Brainer!!</span></div>
               </div>
            </div>
         </div>
      </div>
      <!-- text Area End -->
      <!-- CTA Button Section Start -->
      <div class="cta-btn-section">
         <div class="container">
            <div class="row">
            <div class="f-16 f-md-20 lh150 w400 text-center mt20 black-clr">
                     Use Coupon Code <span class="w700 purple-clr">"MAVAS3"</span> for an Additional <span class="w700 purple-clr">$3 Discount</span> on MAVAS
                  </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn px-md80">
                  <span class="text-center">Grab MAVAS + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 black-clr mt-md30">
                  Use Coupon Code <span class="w700 purple-clr">"MAVAS"</span> for an Additional <span class="w700 purple-clr">$50 Discount</span> on Bundle Deal
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflinkbundle']; ?>" class="text-center bonusbuy-btn px-md80">
                  <span class="text-center">Grab MAVAS Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-12 mt15 mt-md20" align="center">
                  <h3 class="f-md-28 f-20 w500 text-center black">My Exclusive Bonuses Are Expiring in...</h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-black black-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <span class="f-14 f-md-18 smmltd">Days</span>
                      </div>
                      <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <span class="f-14 f-md-18 w500 smmltd">Hours</span>
                      </div>
                      <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <span class="f-14 f-md-18 w500 smmltd">Sec</span> 
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->
<!--Footer Section Start -->
<div class="footer-section">
   <div class="container">
      <div class="row">
         <div class="col-12 text-center">
         <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 685 120" style="max-height:55px">
  <defs>
    <style>
      .cls-1 {
        fill: url(#linear-gradient-2);
      }

      .cls-2 {
        fill: #fff;
      }

      .cls-3 {
        fill: #21e5f4;
      }

      .cls-4 {
        fill: url(#linear-gradient);
      }
    </style>
    <linearGradient id="linear-gradient" x1="0" y1="60" x2="25.31" y2="60" gradientUnits="userSpaceOnUse">
      <stop offset="0" stop-color="#b956ff"></stop>
      <stop offset="1" stop-color="#8d4ef9"></stop>
    </linearGradient>
    <linearGradient id="linear-gradient-2" x1="99.25" y1="59.94" x2="124.57" y2="59.94" gradientUnits="userSpaceOnUse">
      <stop offset="0" stop-color="#8d57f4"></stop>
      <stop offset="1" stop-color="#6e33ff"></stop>
    </linearGradient>
  </defs>
  <g id="Layer_1-2" data-name="Layer 1">
    <g>
      <g>
        <g>
          <polygon class="cls-2" points="343.95 66.06 343.95 66.06 300 .53 273.46 .53 343.96 99.29 355.45 83.2 343.95 66.06"></polygon>
          <polygon class="cls-2" points="367.54 66.26 414.45 .53 388.11 .53 355.71 48.61 367.54 66.26"></polygon>
        </g>
        <polygon class="cls-2" points="218.06 0 147.57 99.82 172.51 99.82 182.71 84.58 198.02 61.87 218.07 32.36 237.91 61.88 253.21 84.59 263.4 99.82 288.55 99.82 218.06 0"></polygon>
        <polygon class="cls-2" points="469.86 0 399.36 99.82 424.31 99.82 434.5 84.58 449.81 61.87 469.87 32.36 489.7 61.88 505.01 84.59 515.2 99.82 540.35 99.82 469.86 0"></polygon>
        <g>
          <path class="cls-2" d="m662.66,46.82l-96.97-14.26,16.04,21.03,5.2.9,69.12,11.61c3.34.56,5.78,3.45,5.78,6.83,0,3.83-3.1,6.93-6.93,6.93h-75.79l-15.37,20.13,94.46.02c14.79,0,26.78-11.99,26.78-26.78,0-13.07-9.43-24.22-22.31-26.4Z"></path>
          <polygon class="cls-2" points="685 1.08 589.71 1.08 589.71 1.06 581.89 1.06 566.5 21.21 669.63 21.21 685 1.08"></polygon>
        </g>
      </g>
      <g>
        <polygon class="cls-4" points="25.31 20.82 25.31 120 0 99.11 0 0 25.31 20.82"></polygon>
        <polygon class="cls-1" points="124.57 0 124.57 99.13 99.25 119.89 99.25 20.76 124.57 0"></polygon>
        <polygon class="cls-3" points="92.74 25.17 92.74 62.88 62.24 100.89 31.82 62.89 31.82 25.17 62.24 63.17 92.74 25.17"></polygon>
      </g>
    </g>
  </g>
</svg>
         </div>
         <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md60">
            <div class="f-md-18 f-16 w400 lh140 white-clr text-xs-center">Copyright © MAVAS 2023</div>
            <ul class="footer-ul w400 f-md-18 f-16 white-clr text-center text-md-right">
               <li><a href="https://support.oppyo.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
               <li><a href="http://getmavas.com/legal/privacy-policy.html " class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
               <li><a href="http://getmavas.com/legal/terms-of-service.html " class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
               <li><a href="http://getmavas.com/legal/disclaimer.html " class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
               <li><a href="http://getmavas.com/legal/gdpr.html " class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
               <li><a href="http://getmavas.com/legal/dmca.html " class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
               <li><a href="http://getmavas.com/legal/anti-spam.html " class="white-clr t-decoration-none">Anti-Spam</a></li>
            </ul>
         </div>
      </div>
   </div>
</div>
<!--Footer Section End -->
       <!-- timer --->
       <?php
         if ($now < $exp_date) {
         ?>
      <script type="text/javascript">
         // Count down milliseconds = server_end - server_now = client_end - client_now
         var server_end = <?php echo $exp_date; ?> * 1000;
         var server_now = <?php echo time(); ?> * 1000;
         var client_now = new Date().getTime();
         var end = server_end - server_now + client_now; // this is the real end time
         
         var noob = $('.countdown').length;
         
         var _second = 1000;
         var _minute = _second * 60;
         var _hour = _minute * 60;
         var _day = _hour * 24
         var timer;
         
         function showRemaining() {
         var now = new Date();
         var distance = end - now;
         if (distance < 0) {
         	clearInterval(timer);
         	document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
         	return;
         }
         
         var days = Math.floor(distance / _day);
         var hours = Math.floor((distance % _day) / _hour);
         var minutes = Math.floor((distance % _hour) / _minute);
         var seconds = Math.floor((distance % _minute) / _second);
         if (days < 10) {
         	days = "0" + days;
         }
         if (hours < 10) {
         	hours = "0" + hours;
         }
         if (minutes < 10) {
         	minutes = "0" + minutes;
         }
         if (seconds < 10) {
         	seconds = "0" + seconds;
         }
         var i;
         var countdown = document.getElementsByClassName('countdown');
         for (i = 0; i < noob; i++) {
         	countdown[i].innerHTML = '';
         
         	if (days) {
         		countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + days + '</span><span class="f-14 f-md-18 smmltd">Days</span> </div>';
         	}
         
         	countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + hours + '</span><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>';
         
         	countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">' + minutes + '</span><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>';
         
         	countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">' + seconds + '</span><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>';
         }
         
         }
         timer = setInterval(showRemaining, 1000);
         	
      </script>
      <?php
         } else {
         echo "Times Up";
         }
         ?>
      <!--- timer end-->

      <!-- <script>
const greeting = ['Dominate Social Media, Drive Website Traffic, Create Ads,', 'Provide Legal Guidance, Optimize SEO, Manage Finances,', 'Generate Quality Leads, Create Compelling Content,', '& Much More... All Hands-Free!'];
let currentGreetingIndex = 0;
let currentCharacterIndex = 0;
let isDeleting = false;
let isPaused = false;
let pauseEnd = 0;

function typeWriterEffect() {
  const greetingElement = document.getElementById('typing');

  if (isPaused && Date.now() > pauseEnd) {
    isPaused = false;
    if (isDeleting) {
      currentGreetingIndex = (currentGreetingIndex + 1) % greeting.length;
      isDeleting = false;
    } else {
      isDeleting = true;
    }
  }

  if (!isPaused && !isDeleting && currentCharacterIndex === greeting[currentGreetingIndex].length) {
    isPaused = true;
    pauseEnd = Date.now() + 800; 
    return setTimeout(typeWriterEffect, 50);
  }

  if (!isPaused && isDeleting && currentCharacterIndex === 0) {
    isPaused = true;
    pauseEnd = Date.now() + 200; 
    return setTimeout(typeWriterEffect, 50);
  }

  const timeout = isDeleting ? 100 : 200;
  greetingElement.innerText = greeting[currentGreetingIndex].substring(0, currentCharacterIndex);
  currentCharacterIndex = isDeleting ? currentCharacterIndex - 1 : currentCharacterIndex + 1;
  setTimeout(typeWriterEffect, timeout);
}
typeWriterEffect();
</script> -->
<script type="text/javascript">
   var noob = $('.countdown').length;
   var stimeInSecs;
   var tickers;

   function timerBegin(seconds) {     
      stimeInSecs = parseInt(seconds);           
      showRemaining();
      //tickers = setInterval("showRemaining()", 1000);
   }

   function showRemaining() {

      var seconds = stimeInSecs;
         
      if (seconds > 0) {         
         stimeInSecs--;       
      } else {        
         clearInterval(tickers);       
         //timerBegin(59 * 60); 
      }

      var days = Math.floor(seconds / 86400);

      seconds %= 86400;
      
      var hours = Math.floor(seconds / 3600);
      
      seconds %= 3600;
      
      var minutes = Math.floor(seconds / 60);
      
      seconds %= 60;


      if (days < 10) {
         days = "0" + days;
      }
      if (hours < 10) {
         hours = "0" + hours;
      }
      if (minutes < 10) {
         minutes = "0" + minutes;
      }
      if (seconds < 10) {
         seconds = "0" + seconds;
      }
      var i;
      var countdown = document.getElementsByClassName('countdown');

      for (i = 0; i < noob; i++) {
         countdown[i].innerHTML = '';
      
         if (days) {
            countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-24 f-md-32 timerbg oswald">' + days + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-14 w400 smmltd">Days</span> </div>';
         }
      
         countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-24 f-md-32 timerbg oswald">' + hours + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-14 w400 smmltd">Hours</span> </div>';
      
         countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"><span class="f-24 f-md-32 timerbg oswald">' + minutes + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-14 w400 smmltd">Mins</span> </div>';
      
         countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-24 f-md-32 timerbg oswald">' + seconds + '</span><br><span class="f-14 f-md-14 w400 smmltd">Sec</span> </div>';
      }
   
   }


//timerBegin(59 * 60); 
function getCookie(cname) {
      var name = cname + "=";
      var decodedCookie = decodeURIComponent(document.cookie);
      var ca = decodedCookie.split(';');
      for(var i = 0; i <ca.length; i++) {
          var c = ca[i];
          while (c.charAt(0) == ' ') {
              c = c.substring(1);
          }
          if (c.indexOf(name) == 0) {
              return c.substring(name.length, c.length);
          }
      }
      return "";
  }
var cnt = 60*60;
function counter(){
   if(getCookie("cnt") > 0){
      cnt = getCookie("cnt");
   }
   cnt -= 1;
   document.cookie = "cnt="+ cnt;
   timerBegin(getCookie("cnt"));
   //jQuery("#counter").val(getCookie("cnt"));
   if(cnt>0){
      setTimeout(counter,1000);
  }
}
counter();
</script>
<script>
         const typedTextSpan = document.querySelector(".typed-text");
const cursorSpan = document.querySelector(".cursor");
const textArray = ["Create Your Course", "Dominate Social Media", "Drive Website Traffic", "Create Ads", "Optimize SEO", "Generate Quality Leads", "Create Compelling Content"];
const typingDelay = 100;
const erasingDelay = 50;
const newTextDelay = 200; // Delay between current and next text
let textArrayIndex = 0;
let charIndex = 0;

function type() {
  if (charIndex < textArray[textArrayIndex].length) {
    if(!cursorSpan.classList.contains("typing")) cursorSpan.classList.add("typing");
    typedTextSpan.textContent += textArray[textArrayIndex].charAt(charIndex);
    charIndex++;
    setTimeout(type, typingDelay);
  } 
  else {
    cursorSpan.classList.remove("typing");
  	setTimeout(erase, newTextDelay);
  }
}

function erase() {
	if (charIndex > 0) {
    if(!cursorSpan.classList.contains("typing")) cursorSpan.classList.add("typing");
    typedTextSpan.textContent = textArray[textArrayIndex].substring(0, charIndex-1);
    charIndex--;
    setTimeout(erase, erasingDelay);
  } 
  else {
    cursorSpan.classList.remove("typing");
    textArrayIndex++;
    if(textArrayIndex>=textArray.length) textArrayIndex=0;
    setTimeout(type, typingDelay + 1100);
  }
}

document.addEventListener("DOMContentLoaded", function() { // On DOM Load initiate the effect
  if(textArray.length) setTimeout(type, newTextDelay + 250);
});
      </script>

<script id="rendered-js" >
   $('.bxslider.text').bxSlider({
     mode: 'vertical',
     pager: false,
     controls: false,
     infiniteLoop: true,
     auto: true,
     speed: 300,
     pause: 2000 });
          
</script>
   </body>
</html>
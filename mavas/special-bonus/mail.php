<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <!-- Tell the browser to be responsive to screen width -->
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
      <meta name="title" content="Mavas Bonuses">
      <meta name="description" content="Grab My 20 Exclusive Bonuses Before the Deal Ends...">
      <meta name="keywords" content="Mavas Bonuses">
      <meta property="og:image" content="https://www.getmavas.com/special-bonus/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Dr. Amit Pareek">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="Mavas Bonuses">
      <meta property="og:description" content="Grab My 20 Exclusive Bonuses Before the Deal Ends...">
      <meta property="og:image" content="https://www.getmavas.com/special-bonus/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="Mavas Bonuses">
      <meta property="twitter:description" content="Grab My 20 Exclusive Bonuses Before the Deal Ends...">
      <meta property="twitter:image" content="https://www.getmavas.com/special-bonus/thumbnail.png">
      <title>Mavas Bonuses</title>
      <!-- Shortcut Icon  -->
      <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
      <link rel="stylesheet" href="../common_assets/css/bootstrap.min.css" type="text/css">
      <script src="../common_assets/js/jquery.min.js"></script>
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <!-- Start Editor required -->
      <link rel="stylesheet" href="assets/css/style.css" type="text/css">
      <link rel="stylesheet" type="text/css" href="assets/css/bonus-style.css">
   </head>
   <body>
      <!-- New Timer  Start-->
      <?php
         $date = 'September 27 2023 11:59 AM EST';
         $exp_date = strtotime($date);
         $now = time();  
         if ($now < $exp_date) {
         ?>
      <?php
         } else {
         	echo "Times Up";
         }
         ?>
      <!-- New Timer End -->
      <?php
         if(!isset($_GET['afflink'])){
         $_GET['afflink'] = 'https://jvz6.com/c/10103/398674/';
         $_GET['name'] = 'Dr. Amit Pareek';      
         }
         ?>
          <?php
         if(!isset($_GET['afflinkbundle'])){
         $_GET['afflinkbundle'] = 'https://jvz2.com/c/10103/398672/';
         $_GET['name'] = 'Dr. Amit Pareek';      
         }
         ?>
      <!-- Header Section Start -->   
      <div class="main-header">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="text-center">
                     <div class="f-md-32 f-20 lh140 w400 white-clr d-block d-md-flex align-items-center justify-content-center">
                        <span class="w600 purple-clr"><?php echo $_GET['name'];?>'s</span> &nbsp;special bonus for &nbsp;
                        <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 685 120" style="max-height:55px">
  <defs>
    <style>
      .cls-1 {
        fill: url(#linear-gradient-2);
      }

      .cls-2 {
        fill: #fff;
      }

      .cls-3 {
        fill: #21e5f4;
      }

      .cls-4 {
        fill: url(#linear-gradient);
      }
    </style>
    <linearGradient id="linear-gradient" x1="0" y1="60" x2="25.31" y2="60" gradientUnits="userSpaceOnUse">
      <stop offset="0" stop-color="#b956ff"></stop>
      <stop offset="1" stop-color="#8d4ef9"></stop>
    </linearGradient>
    <linearGradient id="linear-gradient-2" x1="99.25" y1="59.94" x2="124.57" y2="59.94" gradientUnits="userSpaceOnUse">
      <stop offset="0" stop-color="#8d57f4"></stop>
      <stop offset="1" stop-color="#6e33ff"></stop>
    </linearGradient>
  </defs>
  <g id="Layer_1-2" data-name="Layer 1">
    <g>
      <g>
        <g>
          <polygon class="cls-2" points="343.95 66.06 343.95 66.06 300 .53 273.46 .53 343.96 99.29 355.45 83.2 343.95 66.06"></polygon>
          <polygon class="cls-2" points="367.54 66.26 414.45 .53 388.11 .53 355.71 48.61 367.54 66.26"></polygon>
        </g>
        <polygon class="cls-2" points="218.06 0 147.57 99.82 172.51 99.82 182.71 84.58 198.02 61.87 218.07 32.36 237.91 61.88 253.21 84.59 263.4 99.82 288.55 99.82 218.06 0"></polygon>
        <polygon class="cls-2" points="469.86 0 399.36 99.82 424.31 99.82 434.5 84.58 449.81 61.87 469.87 32.36 489.7 61.88 505.01 84.59 515.2 99.82 540.35 99.82 469.86 0"></polygon>
        <g>
          <path class="cls-2" d="m662.66,46.82l-96.97-14.26,16.04,21.03,5.2.9,69.12,11.61c3.34.56,5.78,3.45,5.78,6.83,0,3.83-3.1,6.93-6.93,6.93h-75.79l-15.37,20.13,94.46.02c14.79,0,26.78-11.99,26.78-26.78,0-13.07-9.43-24.22-22.31-26.4Z"></path>
          <polygon class="cls-2" points="685 1.08 589.71 1.08 589.71 1.06 581.89 1.06 566.5 21.21 669.63 21.21 685 1.08"></polygon>
        </g>
      </g>
      <g>
        <polygon class="cls-4" points="25.31 20.82 25.31 120 0 99.11 0 0 25.31 20.82"></polygon>
        <polygon class="cls-1" points="124.57 0 124.57 99.13 99.25 119.89 99.25 20.76 124.57 0"></polygon>
        <polygon class="cls-3" points="92.74 25.17 92.74 62.88 62.24 100.89 31.82 62.89 31.82 25.17 62.24 63.17 92.74 25.17"></polygon>
      </g>
    </g>
  </g>
</svg>
                     </div>
                  </div>
                  <div class="col-12 mt20 mt-md50 text-center">
                     <div class="preheadline f-20 f-md-22 w500 lh140">
                        <span class="preheadline-content white-clr">Grab My 20 Exclusive Bonuses Before the Deal Ends...</span>
                     </div>
                  </div>
                  <div class="col-12 mt30 mt-md50 relative p-md0">
                     <div class="mainheadline f-md-40 f-28 w700 text-center white-clr lh140">
                        <div class="headline-text">
                           Revolutionary AI App Uses 100+ <span class="neon-clr caveat">(Near Human Intelligence)</span> <br class="d-none d-md-block">
                           AI Experts That Follow Siri-like Voice Commands To <span class="header-underline">Run Email Campaigns</span>  
                           <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/header-underline.webp" alt="" class="mx-auto d-block img-fluid">
                        </div>
                     </div>
                  </div>
                  <div class="col-12 mt20 mt-md30 col-md-10 mx-auto">
                     <div class="text-center typing-text">
                        <h1 class="typing" id="typing"></h1>
                     </div>
                  </div>
                  <div class="col-12 mt-md40 mt20 f-18 f-md-24 w400 text-center lh150 white-clr text-capitalize">
                  Embed These 100+ AI Experts On Any Website/Landing Page With A Single Line Of Code. Use For Your Business Or For Your Clients & Charge Them PREMIUM Fee Per AI Expert
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md40">
               <div class="col-md-10 col-12 mx-auto">
                  <!-- <div class="col-12 responsive-video border-video">
                     <iframe src="https://cloudfusion.oppyo.com/video/embed/ypzjqhcfxg" style="width:100%; height:80%" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                  </div> -->
                  <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/product-box.webp" class="img-fluid mx-auto d-block" alt="ProductBox">
               </div>
            </div>
         </div>
      </div>

      <!-- List Section Start -->
      <div class="second-section">
         <div class="container">
            <div class="row">
               <div class="col-12 p-md0">
                  <div class="col-12 key-features-bg d-flex align-items-center flex-wrap">
                     <div class="row">
                        <div class="col-12 col-md-6">
                           <ul class="list-head1 pl0 m0 f-18 f-md-20 lh160 w400 text-capitalize">
                              <li><span class="w600">Access to Predefined AI Employees or Create your Own:</span> Customize your AI team to get things done your way, leaving you 100% in control</li>
                              <li><span class="w600">Embed AI Employees on any website or Page by Just one line of code:</span> Add super-smart support to your site with a single line of code, making your website work harder for you</li>
                              <li><span class="w600">Generate Leads using AI Employees:</span> Let AI Experts bring in potential customers, so you can focus on what you do best—running your business</li>
                              <li><span class="w600">Appoint & Dismiss AI Employees Any time:</span> Build your dream team on your terms, so you can run your business just the way you want.</li>
                              <li><span class="w600">Fully Encrypted AI Employees Chat:</span> Keep your chats under lock & key, so you can chat with confidence.</li>
                              <li><span class="w600">100% Chat/Talk Privacy:</span> Rest easy knowing your conversations are just that – yours.</li>
                              <li><span class="w600">Beginner Friendly:</span> No Tech or any AI skills or experience required.</li>
                           </ul>
                        </div>
                        <div class="col-12 col-md-6">
                           <ul class="list-head1 pl0 m0 f-18 f-md-20 lh160 w400 text-capitalize">
                              <li><span class="w600">Chat with AI Employees: </span>Talk with your AI team like you would with a friend </li>
                              <li><span class="w600">150,000+ Niche Specific Prompts for Chat:</span> Never stress about what to say. We've got prompts for every situation, so you can just focus on being awesome</li>
                              <li><span class="w600">Access in Different Languages:</span> Connect with clients from all over the world in their language</li>
                              <li><span class="w600">Siri/Alexa Like Conversation:</span> Your AI team keeps it simple & easy, just like chatting with a virtual assistant</li>
                              <li><span class="w600">Smart Learning AI Team:</span> Your AI team gets smarter with every chat, delivering expert-level responses</li>
                              <li><span class="w600">Authentic Conversations, Every Time:</span> Enjoy dynamic, lifelike interactions. No rehearsed answers. Your AI team keeps it fresh, like a real conversation</li>
                              <li><span class="w600">FREE Commercial License:</span> Lease Your AI Employees & create an extra income stream, making you a savvy business owner.</li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- List Section End -->

      <!-- Header Section End -->
      
      <!-- CTA Btn Start-->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700  purple-clr">"MAVAS3"</span> for an Additional <span class="w700  purple-clr">$3 Discount</span> on Mavas
                  </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn px-md80">
                  <span class="text-center">Grab Mavas + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                  Use Coupon Code <span class="w700  purple-clr">"Mavas" </span> for an Additional <span class="w700  purple-clr">$50 Discount</span> on Bundle Deal
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflinkbundle']; ?>" class="text-center bonusbuy-btn px-md80">
                  <span class="text-center">Grab Mavas Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
                <!-- Timer -->
                <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 smmltd">Days</span>
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Sec</span>
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Btn End -->

      <!-- Header Section End -->
      <section class="step-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-50 w700 lh140 ">
                  We Have Automated Everything & Reduced Running A Business  In ANY Niche To <br> <span class="step-headline white-clr">Just 2 Steps…</span> 
                  </div>
                  <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/steps-arrow.png" class="img-fluid mx-auto d-none d-md-block mt20">
               </div>
            </div>
            <div class="row mt20 mt-md30">
               <div class="col-md-6 col-12 px-md50">
                  <div class="step-wall">
                     <div class="f-20 f-md-22 w700 lh140 white-clr step-head mx-auto">
                        Step 1
                     </div>
                     <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/step1.webp" alt="Step One" class="mx-auto img-fluid d-block mt20 mt-md30">
                     <div class="f-22 f-md-26 w700 lh140 mt20 mt-md30 text-center">
                        SELECT AN AI EXPERT OR <br class="d-none d-md-block"> CREATE YOUR OWN
                     </div>
                     <div class="f-18 f-md-20 w400 lh140 mt10 text-center">
                        Choose from a range of specialized AI  experts or customize your own, tailored  to your business needs.
                     </div>
                     <div class="f-18 f-md-20 w400 lh140 mt10 black-clr1 text-center">
                        (With the FREE Commercial License  Included during this special launch… you  can also use the AI Experts for your  clients' projects)
                     </div>
                  </div>
               </div>
               <div class="col-md-6 col-12 mt20 mt-md0 px-md50">
                  <div class="step-wall">
                     <div class="f-20 f-md-22 w700 lh140 white-clr step-head mx-auto">
                        Step 2
                     </div>
                     <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/step2.webp" alt="Step Two" class="mx-auto img-fluid d-block mt20 mt-md30">
                     <div class="f-22 f-md-26 w700 lh140 mt20 mt-md30 text-center">
                        COMMAND THEM TO <br class="d-none d-md-block"> COMPLETE THE TASK
                     </div>
                     <div class="f-18 f-md-20 w400 lh140 mt10 text-center">
                        Simply instruct your AI expert using simple  Siri-like voice commands or chat, &  watch them get to work.
                     </div>
                     <div class="f-18 f-md-20 w400 lh140 mt10 black-clr1 text-center">
                        (Get Paid a PREMIUM Fee for services <br class="d-none d-md-block"> rendered by your AI Experts without  incurring any expenses in salaries. Keep  100% of the profits)
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      

      
      <div class="exclusive-bonus">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                  <div class="white-clr f-24 f-md-36 lh140 w600">
                     Exclusive Bonus #1 : NEXUSGPT
                  </div>
                  <div class="f-18 f-md-20 w500 mt20 white-clr">
                     20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="nexusgpt-header-section">
      <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row align-items-center">
                     <div class="col-md-12 text-center">
                        <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1073.21 250" style="max-height:65px;">
                           <defs>
                              <style>
                                 .cls-1ng {
                                 fill: #fff;
                                 }

                                 .cls-2ng {
                                 fill-rule: evenodd;
                                 }

                                 .cls-2ng, .cls-3ng {
                                 fill: #00a4ff;
                                 }

                                 .cls-4ng {
                                 fill: #f19b10;
                                 }
                              </style>
                           </defs>
                           <g id="Layer_1-2" data-name="Layer 1">
                              <g>
                                 <g>
                                    <g>
                                       <path class="cls-1ng" d="m872.38,109.17c-3.33-7.67-8.42-13.69-15.25-18.08-6.83-4.39-14.86-6.58-24.08-6.58-8.67,0-16.45,2-23.33,6-6.89,4-12.33,9.7-16.33,17.08-4,7.39-6,15.97-6,25.75s2,18.39,6,25.83c4,7.45,9.44,13.17,16.33,17.17,6.89,4,14.67,6,23.33,6,8.11,0,15.42-1.75,21.92-5.25s11.72-8.5,15.67-15c3.94-6.5,6.19-14.08,6.75-22.75h-49v-9.5h61.33v8.5c-.56,10.22-3.33,19.47-8.33,27.75-5,8.28-11.7,14.81-20.08,19.58-8.39,4.78-17.81,7.17-28.25,7.17s-20.56-2.53-29.33-7.58c-8.78-5.05-15.69-12.11-20.75-21.17-5.06-9.05-7.58-19.3-7.58-30.75s2.53-21.69,7.58-30.75c5.05-9.05,11.97-16.11,20.75-21.17,8.78-5.05,18.55-7.58,29.33-7.58,12.44,0,23.33,3.11,32.67,9.33,9.33,6.22,16.11,14.89,20.33,26h-13.67Z"></path>
                                       <path class="cls-1ng" d="m975.71,132.83c-6.56,6.11-16.39,9.17-29.5,9.17h-23.5v49.67h-11.67v-116.5h35.17c13,0,22.8,3.06,29.42,9.17,6.61,6.11,9.92,14.22,9.92,24.33s-3.28,18.06-9.83,24.17Zm-2.17-24.17c0-7.78-2.17-13.67-6.5-17.67-4.33-4-11.28-6-20.83-6h-23.5v47h23.5c18.22,0,27.33-7.78,27.33-23.33Z"></path>
                                       <path class="cls-1ng" d="m1073.21,75.17v9.67h-32.33v106.83h-11.67v-106.83h-32.5v-9.67h76.5Z"></path>
                                    </g>
                                    <g>
                                       <path class="cls-1ng" d="m322.77,192.08h-28.5l-47.67-72.17v72.17h-28.5v-117h28.5l47.67,72.5v-72.5h28.5v117Z"></path>
                                       <path class="cls-1ng" d="m505.27,192.08l-23.83-35.83-21,35.83h-32.33l37.5-59.5-38.33-57.5h33.17l23.5,35.33,20.67-35.33h32.33l-37.17,59,38.67,58h-33.17Z"></path>
                                       <path class="cls-1ng" d="m580.77,75.08v70c0,7,1.72,12.39,5.17,16.17,3.44,3.78,8.5,5.67,15.17,5.67s11.78-1.89,15.33-5.67c3.55-3.78,5.33-9.17,5.33-16.17v-70h28.5v69.83c0,10.45-2.22,19.28-6.67,26.5-4.45,7.22-10.42,12.67-17.92,16.33-7.5,3.67-15.86,5.5-25.08,5.5s-17.47-1.8-24.75-5.42c-7.28-3.61-13.03-9.05-17.25-16.33-4.22-7.28-6.33-16.14-6.33-26.58v-69.83h28.5Z"></path>
                                       <path class="cls-1ng" d="m689.6,189.08c-6.78-2.78-12.2-6.89-16.25-12.33-4.06-5.44-6.2-12-6.42-19.67h30.33c.44,4.33,1.94,7.64,4.5,9.92,2.55,2.28,5.89,3.42,10,3.42s7.55-.97,10-2.92c2.44-1.94,3.67-4.64,3.67-8.08,0-2.89-.97-5.28-2.92-7.17-1.95-1.89-4.33-3.44-7.17-4.67-2.83-1.22-6.86-2.61-12.08-4.17-7.56-2.33-13.72-4.67-18.5-7-4.78-2.33-8.89-5.78-12.33-10.33-3.45-4.55-5.17-10.5-5.17-17.83,0-10.89,3.94-19.42,11.83-25.58,7.89-6.17,18.17-9.25,30.83-9.25s23.28,3.08,31.17,9.25c7.89,6.17,12.11,14.75,12.67,25.75h-30.83c-.22-3.78-1.61-6.75-4.17-8.92-2.56-2.17-5.83-3.25-9.83-3.25-3.45,0-6.22.92-8.33,2.75-2.11,1.83-3.17,4.47-3.17,7.92,0,3.78,1.78,6.72,5.33,8.83,3.55,2.11,9.11,4.39,16.67,6.83,7.55,2.56,13.69,5,18.42,7.33,4.72,2.33,8.8,5.72,12.25,10.17,3.44,4.45,5.17,10.17,5.17,17.17s-1.7,12.72-5.08,18.17c-3.39,5.45-8.31,9.78-14.75,13-6.45,3.22-14.06,4.83-22.83,4.83s-16.22-1.39-23-4.17Z"></path>
                                       <rect class="cls-1ng" x="343.41" y="75.29" width="71.69" height="22.79"></rect>
                                       <rect class="cls-1ng" x="343.41" y="121.79" width="71.69" height="22.79"></rect>
                                       <rect class="cls-1ng" x="343.41" y="169.5" width="71.69" height="22.79"></rect>
                                    </g>
                                 </g>
                                 <g>
                                    <path class="cls-3ng" d="m76.03.47L13.55,16.04C5.96,17.93,0,28.08,0,38.78v147.74c0,.3,0,.61,0,.91v23.78c0,10.7,5.96,20.86,13.55,22.75l62.48,15.56c10.52,2.63,19.39-5.93,19.39-19.22v-112.1c0-.25,0-.5,0-.75V19.7c0-13.29-8.88-21.85-19.4-19.22Zm-34.9,227.64c-2.77-1.31-4.37-4.32-4.37-7.13s1.61-5.83,4.37-7.13c4.4-2.08,10.41.47,10.41,7.13s-6.01,9.22-10.41,7.13ZM54.19,23.79l-19.12,4.39c-1.93.44-3.86-.76-4.3-2.69-.06-.27-.09-.54-.09-.81,0-1.63,1.12-3.11,2.78-3.49l19.13-4.39c1.92-.45,3.85.76,4.29,2.69.07.27.09.54.09.81,0,1.63-1.12,3.11-2.78,3.49Z"></path>
                                    <path class="cls-4ng" d="m166.8,83.52l-44.46-11.07c-7.48-1.87-13.8,4.23-13.8,13.68v149.86c0,9.46,6.32,15.55,13.8,13.68l44.46-11.07c5.41-1.35,9.64-8.57,9.64-16.18v-122.7c0-7.61-4.23-14.84-9.64-16.18Zm-19.62,150.91c-3.13,1.47-7.41-.34-7.41-5.08s4.27-6.56,7.41-5.08c1.97.93,3.11,3.08,3.11,5.08s-1.14,4.14-3.11,5.08Zm6.79-144.13c-.33,1.31-1.65,2.11-2.96,1.79l-12.66-2.64c-1.5-.42-2.2-1.68-2.2-2.85,0-.23.03-.46.08-.68.32-1.32,1.65-2.12,2.96-1.79l12.98,3.21c1.12.28,1.87,1.27,1.87,2.37,0,.2-.03.39-.08.59Z"></path>
                                    <g>
                                       <path class="cls-2ng" d="m163.94,67.4h0c-.95,1.77-2.97,2.67-4.92,2.18-1.95-.48-3.32-2.22-3.34-4.23.96-3.86,1.37-7.85,1.22-11.85-.25-7.01-2.24-14.05-6.12-20.47-.17-.27-.33-.54-.51-.81-.25-.41-.52-.81-.79-1.2-.38-.57-.78-1.12-1.19-1.66-.09-.12-.18-.23-.27-.35-.22-.28-.43-.55-.66-.82-2.05-2.55-4.37-4.81-6.87-6.76-.3-.24-.6-.48-.91-.69-.43-.32-.85-.62-1.28-.92-1.09-.75-2.21-1.44-3.35-2.07-3.35-1.87-6.9-3.26-10.53-4.15-3.93-.97-7.96-1.37-11.97-1.21-1.96-.42-3.39-2.12-3.47-4.12v-.18c0-1.93,1.26-3.64,3.12-4.21,9.06-.36,18.22,1.67,26.45,6.12.54.29,1.08.59,1.62.91.36.21.71.43,1.06.64.35.22.7.44,1.05.67,2.31,1.53,4.53,3.25,6.63,5.19,1.68,1.56,3.25,3.2,4.68,4.92.22.27.43.54.65.81.2.25.4.5.59.75.33.43.65.86.96,1.29.25.34.49.69.73,1.04.24.35.47.71.7,1.06.23.36.46.72.67,1.07.16.25.31.51.46.77.36.61.71,1.22,1.03,1.84.32.59.62,1.19.92,1.8.18.38.36.75.53,1.13.17.38.34.76.5,1.14.17.38.32.77.48,1.15.27.68.52,1.35.76,2.03,3.37,9.57,3.71,19.71,1.38,29.18Z"></path>
                                       <g>
                                          <path class="cls-2ng" d="m116.46,56.27c-.03-.25-.08-.5-.16-.74,0-.02,0-.02,0-.04-.02-.07-.04-.13-.07-.18,0-.05-.03-.1-.05-.14,0,0,0-.03-.02-.03,0,0,0,0,0-.02-.04-.11-.09-.2-.15-.3,0-.03-.02-.04-.03-.07-.05-.09-.12-.19-.18-.28,0-.02-.02-.05-.04-.07-.03-.04-.06-.09-.09-.13-.04-.04-.08-.08-.12-.12,0,0,0-.02-.03-.03,0,0-.02-.03-.03-.04-.08-.08-.15-.16-.23-.23-.02-.03-.05-.05-.07-.08-.06-.05-.12-.1-.18-.15,0,0-.02-.02-.03-.03l-.3-.2s-.04-.03-.06-.04c-.04-.03-.09-.05-.13-.08-.04-.02-.08-.03-.13-.05h0s0-.02,0-.02c-.02,0-.05-.03-.07-.03-.08-.04-.18-.08-.27-.12-.02,0-.03,0-.04,0-.04-.02-.09-.03-.13-.04-.06-.02-.11-.02-.17-.04s-.12-.03-.19-.05c-.03-.02-.08-.03-.12-.03h-.02l-.18-.03s-.1-.02-.15-.02c-.07,0-.13,0-.19,0h-.54c-.13.02-.25.03-.38.07-.07,0-.16.03-.23.05-.03,0-.05.02-.08.03-.02,0-.06.02-.09.02h-.02c-.11.04-.21.08-.32.13-.02,0-.04.02-.06.03,0,0-.02,0-.02,0-.09.04-.18.09-.28.14h0c-.11.07-.21.13-.31.2-.02,0-.02,0-.03.03-.04.02-.08.05-.13.08,0,0-.02.02-.03.03-.04.03-.08.06-.13.11,0,0-.02,0-.03.03-.05.03-.09.07-.13.12-.03.03-.07.06-.09.09-.02.02-.04.04-.07.07-.03.02-.05.05-.08.08-.07.08-.13.17-.2.25-.05.07-.1.15-.15.23-.02.02-.03.03-.03.05-.02.02-.03.06-.05.08-.03.05-.06.11-.08.17-.03.03-.03.04-.03.07-.04.08-.08.16-.11.23,0,0,0,.02,0,.03-.03.07-.07.16-.08.23-.02.04-.03.07-.04.12-.04.12-.07.25-.09.38,0,.07-.02.13-.02.21,0,0,0,.03,0,.04,0,.02,0,.03,0,.04,0,.05-.02.11,0,.16-.03.2-.02.4,0,.6,0,.05,0,.09,0,.13,0,.03,0,.08.02.11,0,.03.02.07.03.1,0,.1.02.19.05.28,0,.03.02.04.02.07.03.12.08.25.12.37.04.09.08.17.13.27,0,.02,0,.03.02.04.02.04.04.09.07.13,0,.02.02.03.03.06,0,.02.03.05.05.07,0,0,.02.03.03.04s.02.04.04.06c.03.07.07.12.12.18.02.03.03.05.06.07,0,0,0,.02,0,.02.04.06.08.11.13.16.03.05.08.09.12.13.02.03.04.04.06.07,0,0,.02.02.03.03.03.03.05.05.08.07.06.06.11.11.17.16.03.03.06.05.08.07.02.03.05.04.07.06.08.06.15.11.23.15.06.04.13.08.19.12,0,0,0,0,0,0,.06.03.11.07.17.09l.18.08s.06.03.08.04c.09.03.18.07.28.09.05.03.1.04.15.05.05.02.1.03.16.03.02.02.04.02.07.02.03,0,.08.02.11.02.08.02.17.03.25.04.03,0,.07,0,.1,0,.12.02.24.02.37.02.09,0,.19,0,.29,0,.08,0,.17-.02.26-.03.1-.02.2-.04.3-.07.09-.03.19-.06.29-.09,0,0,.02,0,.04,0h0c.12-.04.22-.09.33-.14.08-.03.15-.07.22-.1.11-.05.21-.11.3-.17,0,0,.02,0,.03-.02.02,0,.02-.02.04-.02.03-.03.08-.05.11-.08,0,0,0,0,.02,0,.02-.02.04-.03.06-.05,0,0,.02,0,.03-.03.1-.07.19-.15.28-.23.02-.02.03-.03.04-.04.09-.09.18-.18.26-.28.1-.12.19-.24.28-.37.02-.03.05-.08.07-.11.04-.05.07-.11.1-.17.02-.03.04-.07.06-.11.13-.26.23-.53.3-.82,0,0,0-.03,0-.04.08-.34.11-.7.08-1.06,0-.07,0-.13,0-.2Z"></path>
                                          <path class="cls-2ng" d="m147.39,63.31c-.95,1.77-2.97,2.67-4.93,2.18-1.95-.48-3.32-2.22-3.34-4.23.57-2.32.83-4.71.73-7.12,0-.2-.02-.39-.03-.59-.23-4.02-1.43-8.03-3.64-11.72-.96-1.58-2.05-3.03-3.27-4.31-.5-.55-1.03-1.07-1.58-1.54-.1-.1-.21-.19-.31-.28-.3-.27-.61-.53-.92-.77-.14-.11-.28-.22-.42-.31,0-.02,0-.02-.02-.02,0-.02-.02-.02-.03-.02-.16-.13-.32-.25-.48-.36,0,0,0,0,0,0-.16-.12-.32-.23-.48-.33-.12-.11-.26-.19-.39-.28-2.51-1.67-5.23-2.83-8.04-3.52-2.37-.58-4.82-.82-7.24-.71-.19,0-.39,0-.58.02-1.97-.37-3.45-2.02-3.58-4.02-.13-2.01,1.11-3.84,3.01-4.48,5.91-.4,11.91.74,17.38,3.45.04.02.08.03.11.05,0,0,.02,0,.02.02.25.13.49.25.74.38.34.17.68.37,1.02.56.34.2.67.4,1.02.61.23.14.45.28.68.43.44.29.89.59,1.32.9,0,0,.02,0,.02.02.21.15.41.3.61.45l.02.02c.62.48,1.24.98,1.85,1.5.24.21.48.43.72.65.22.2.43.4.64.61.93.89,1.79,1.83,2.59,2.81.27.33.53.66.78.99.17.23.34.46.5.68.17.23.33.46.49.69.21.3.41.6.61.91,0,.03.02.04.03.06.38.59.74,1.19,1.08,1.8.18.32.34.63.51.95.14.28.28.56.42.84.14.28.27.57.4.85.13.28.25.57.37.86,2.9,6.91,3.33,14.38,1.63,21.32Z"></path>
                                       </g>
                                       <path class="cls-2ng" d="m130.85,59.22c-.95,1.77-2.98,2.67-4.93,2.18-1.94-.48-3.32-2.22-3.34-4.23.19-.75.28-1.53.25-2.32v-.14c0-.15-.02-.3-.03-.45-.12-1.24-.52-2.48-1.2-3.62-.54-.9-1.22-1.65-1.98-2.27-.03-.02-.07-.05-.1-.07-.05-.04-.09-.08-.13-.1-.07-.06-.14-.11-.21-.16,0,0-.02,0-.03-.02-.08-.07-.17-.12-.26-.17-.23-.17-.48-.33-.72-.44-.71-.42-1.46-.7-2.22-.88-.87-.21-1.75-.28-2.62-.22-.06,0-.12.02-.17.03-.14,0-.28.03-.42.04-2-.22-3.59-1.76-3.88-3.74-.28-1.99.81-3.92,2.66-4.69,4.02-.58,8.25.27,11.86,2.65.24.15.48.32.72.49-.08-.06-.17-.12-.25-.17.17.12.35.24.53.38.2.14.38.29.57.45.07.05.15.11.22.18.18.15.36.31.53.47-.1-.1-.19-.19-.29-.28.15.13.3.28.44.41,0,0,.01,0,.01.01,0,0,.01,0,.02.01,1.14,1.05,2.16,2.28,3,3.68,2.45,4.06,3.02,8.72,1.97,12.98Z"></path>
                                    </g>
                                 </g>
                              </g>
                           </g>
                        </svg>
                     </div>
                    
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50 text-center">
                  <div class="nexusgpt-pre-heading f-md-22 f-18 w500 lh140">
                  Exploit The NFC &amp; Ai Tech to <u>Make Multiple Recurring Income Streams </u>  
                  </div>
               </div>
               <div class="col-12 mt80 nexusgpt-head-design relative">
                  <div class="nexusgpt-gametext">
                     First-to-JVZoo Technology 
                  </div>
                  <div class=" f-md-40 f-28 w400 text-center black-clr lh140">
                     <span class="w600">Super-Easy NFC App Creates Contactless Digital Business Cards with Ai-Assistant, </span> Generates Leads, Followers, Reviews &amp; Sales  <span class="w600 underline-text2">with Just One Touch.</span>
                  </div>
               </div>
               <div class="col-12 mt-md40 mt20 f-20 f-md-24 w400 text-center lh140 white-clr text-capitalize">
               Easily Create &amp; Sell Contactless NFC Digital Cards &amp; GPT-Ai Assistants for Big Profits to Dentists, Attorney, Gyms, Spas, Restaurants, Cafes, Coaches, Affiliates &amp; 100+ Other Niches... 
               </div>
               <div class="col-12 mt20 f-20 f-md-24 w500 text-center lh140 orange-clr2 text-capitalize">
               No Tech Skills of Any Kind Needed. No Monthly Fee Ever. 
               </div>
            </div>
            <div class="row mt20 mt-md50 align-items-center">
               <div class="col-md-8 col-12">
                  <div class="col-12">
                     <div class="video-box">
                        <div style="padding-bottom: 56.25%;position: relative;"><iframe src="https://nexusgpt.oppyo.com/video/embed/e786pt8zf4" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe></div>
                     </div>
                  </div>
               </div>
               <div class="col-md-4 col-12 mt20 mt-md0">
                  <div class="">
                     <ul class="nexusgpt-list-head pl0 m0 f-18 lh140 w400 white-clr">
                        <li><span class="w600">Nexus Of NFC Tech &amp; GPT AI</span> Revolutionise Your Marketing</li>
                        <li><span class="w600">Help Desperate Local Businesses In Any Niche</span> within Minutes</li>
                        <li><span class="w600">GPT AI Assistant Closes Leads &amp; Sales 24x7</span> on Automation</li>
                        <li><span class="w600">Impress Your Colleagues &amp; Client’s</span> with Contactless NFC card</li>
                        <li><span class="w600">Tons of Ready-To-Go Templates</span> with Free-Flow Editor</li>
                        <li>Sell High In-Demand Services with <span class="w600">Included Commercial License.</span> </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="gr-1">
         <div class="container-fluid p0">
            <picture>
               <source media="(min-width:768px)" srcset="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/nexusgpt.webp">
               <source media="(min-width:320px)" srcset="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/nexusgpt-mview.webp" style="width:100%" class="vidvee-mview">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/nexusgpt.webp" alt="" class="img-fluid" style="width: 100%;">
            </picture>
            
         </div>
      </div>
      <!-------Exclusive Bonus----------->
      <div class="exclusive-bonus">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                  <div class="white-clr f-24 f-md-36 lh140 w600">
                     Exclusive Bonus #2 : LinkPro
                  </div>
                  <div class="f-18 f-md-20 w500 mt20 white-clr">
                     20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!------LinkPro Section------>
      <div class="linkpro-header-section">
      <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" style="max-height: 55px; width: 214px;" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1500 380" enable-background="new 0 0 1500 380" xml:space="preserve">
              <g>
                 <g id="Layer_1-2">
                    
                       <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="-689.5011" y1="10559.4189" x2="-790.6911" y2="10663.2998" gradientTransform="matrix(-0.7071 0.7071 0.7071 0.7071 -7815.8701 -6918.3799)">
                       <stop offset="0" style="stop-color:#3BB6FF"></stop>
                       <stop offset="1" style="stop-color:#006DE3"></stop>
                    </linearGradient>
                    <path fill="url(#SVGID_1_)" d="M137.2,100.2l53.2-8.5c13.1-28.5,42.8-45.7,74-42.9c-20.2-24.4-53.7-33.3-83.3-21.9
                       c-3.5,2.8-6.8,5.8-9.9,8.9C153.6,53.4,141.8,75.9,137.2,100.2z"></path>
                    
                       <linearGradient id="SVGID_2_" gradientUnits="userSpaceOnUse" x1="-526.0667" y1="10729.4053" x2="-433.6907" y2="10729.4053" gradientTransform="matrix(-0.7071 0.7071 0.7071 0.7071 -7815.8701 -6918.3799)">
                       <stop offset="0" style="stop-color:#EFA222"></stop>
                       <stop offset="1" style="stop-color:#F0592D"></stop>
                    </linearGradient>
                    <path fill="url(#SVGID_2_)" d="M167.9,371.5c11.4-4.5,22.1-10.7,31.7-18.4c-29.6,11.3-63.1,2.5-83.3-21.9
                       c-31.6-2.6-58-25.2-65.5-56c-2,27.5,5.4,55,21,77.7c5.8,8.6,12.8,16.4,20.6,23.2C117.4,382.5,143.8,380.8,167.9,371.5L167.9,371.5
                       z"></path>
                    
                       <linearGradient id="SVGID_3_" gradientUnits="userSpaceOnUse" x1="-789.4672" y1="10592.5967" x2="-794.1872" y2="10698.4463" gradientTransform="matrix(-0.7071 0.7071 0.7071 0.7071 -7815.8701 -6918.3799)">
                       <stop offset="0" style="stop-color:#3BB6FF"></stop>
                       <stop offset="1" style="stop-color:#006DE3"></stop>
                    </linearGradient>
                    <path fill="url(#SVGID_3_)" d="M181.1,27c29.6-11.4,63.2-2.6,83.4,21.9c31.6,2.6,57.9,25.1,65.5,55.9c0.9-12.2-0.1-24.6-3-36.5
                       c-6-25.1-19.6-47.7-39.1-64.6c-10.1-2.5-20.4-3.7-30.8-3.7C229.4,0.1,202.6,9.7,181.1,27z"></path>
                    
                       <linearGradient id="SVGID_4_" gradientUnits="userSpaceOnUse" x1="-498.1561" y1="10793.4258" x2="-595.8061" y2="10605.5264" gradientTransform="matrix(-0.7071 0.7071 0.7071 0.7071 -7815.8701 -6918.3799)">
                       <stop offset="0" style="stop-color:#EFA222"></stop>
                       <stop offset="1" style="stop-color:#F0592D"></stop>
                    </linearGradient>
                    <path fill="url(#SVGID_4_)" d="M75.1,370.4c5.6,2.4,11.5,4.3,17.4,5.8c-7.8-6.8-14.7-14.6-20.6-23.2c-15.6-22.8-23-50.2-21-77.7
                       c-6.2-25.2,1.2-51.8,19.5-70.1c9.8-9.9,22.3-16.8,35.9-19.8l76.6-11l58-8.3l7.1-1l-2.9-2.9l-23.6-23.6l-2.6-2.6l-14.1-14.1
                       l-31.9,4.7l-3.3,0.5l-9,1.4l-54.8,8.3l-11.5,1.7l-2,0.5C71,144.3,51.6,155.3,36,170.8c-47.9,47.8-48,125.3-0.2,173.2
                       C47.1,355.3,60.4,364.3,75.1,370.4L75.1,370.4z"></path>
                    
                       <linearGradient id="SVGID_5_" gradientUnits="userSpaceOnUse" x1="-608.9395" y1="10772.7686" x2="-482.3742" y2="10772.7686" gradientTransform="matrix(-0.7071 0.7071 0.7071 0.7071 -7815.8701 -6918.3799)">
                       <stop offset="0" style="stop-color:#EFA222"></stop>
                       <stop offset="1" style="stop-color:#F0592D"></stop>
                    </linearGradient>
                    <path fill="url(#SVGID_5_)" d="M174.9,310.3c-14.1,13.9-33.1,21.5-52.9,21.2c-1.9,0-3.8-0.1-5.6-0.3
                       c20.2,24.4,53.7,33.2,83.3,21.9c3.2-2.6,6.4-5.4,9.3-8.3l0.6-0.6c17.5-17.5,29.4-40,33.9-64.3l-53.2,8.5
                       C186.6,296.6,181.3,304,174.9,310.3z"></path>
                    
                       <linearGradient id="SVGID_6_" gradientUnits="userSpaceOnUse" x1="-810.9832" y1="10699.4775" x2="-631.0933" y2="10745.2881" gradientTransform="matrix(-0.7071 0.7071 0.7071 0.7071 -7815.8701 -6918.3799)">
                       <stop offset="0" style="stop-color:#3BB6FF"></stop>
                       <stop offset="1" style="stop-color:#006DE3"></stop>
                    </linearGradient>
                    <path fill="url(#SVGID_6_)" d="M208,253.7l3.2-0.5l9-1.4l54.8-8.3l11.4-1.7l2-0.5c21.6-5.5,41.3-16.9,57-32.7
                       c11.6-11.7,20.7-25.7,26.7-41c24.9-63-6-134.2-69-159.1c-5-2-10-3.6-15.2-4.9C307.3,20.5,321,43.2,327,68.3
                       c2.9,11.9,3.9,24.2,3,36.5c1.4,5.6,2.1,11.3,2.1,17c0.3,19.7-7.3,38.8-21.2,52.8c-0.1,0.1-0.3,0.3-0.4,0.4
                       c-9.8,9.9-22.3,16.8-35.8,19.8l-76.7,11l-58,8.3l-7.1,1l2.9,2.9l23.6,23.6l2.6,2.6l13.9,14.1L208,253.7z"></path>
                    <rect x="613.5" y="166.6" fill="#FFFFFF" width="44.4" height="159.8"></rect>
                    <path fill="#FFFFFF" d="M652.8,107.8c8.7,7.9,9.4,21.4,1.5,30.1c-0.5,0.5-1,1.1-1.5,1.5c-9.6,8.8-24.3,8.8-33.9,0
                       c-8.7-7.9-9.4-21.4-1.5-30.1c0.5-0.6,1-1.1,1.6-1.6C628.5,99,643.2,99,652.8,107.8z"></path>
                    <path fill="#FFFFFF" d="M834,192.6c4.6,9,6.9,19.7,6.9,32.1v101.7h-44.4v-93.8c0.1-9.8-2.4-17.4-7.5-22.9s-12.1-8.3-21-8.3
                       c-5.5-0.1-11,1.2-15.8,3.9c-4.6,2.6-8.3,6.5-10.6,11.2c-2.5,4.9-3.8,10.8-3.9,17.6v92.4h-44.4V166.6h42.3v28.2h1.9
                       c3.4-9.1,9.6-16.9,17.8-22.1c8.3-5.4,18.4-8.2,30.3-8.2c11.1,0,20.8,2.4,29.1,7.3C823,176.7,829.7,183.9,834,192.6z"></path>
                    <polygon fill="#FFFFFF" points="493,289.3 584.5,289.3 584.5,326.4 447.9,326.4 447.9,113.4 493,113.4 		"></polygon>
                    <polygon fill="#FFFFFF" points="1027.8,326.4 975.8,326.4 932.2,262 920.2,275.7 920.2,326.4 875.8,326.4 875.8,113.4 
                       920.2,113.4 920.2,227.3 922.6,227.3 973.8,166.6 1024.8,166.6 965.4,235.8 		"></polygon>
                    <path fill="#FFFFFF" d="M1198.9,148.1c-6-10.9-15.1-19.7-26.1-25.5c-11.4-6.1-25.2-9.2-41.4-9.2h-84.2v213h45.1v-69h37.9
                       c16.4,0,30.4-3,41.9-9c11.1-5.6,20.4-14.5,26.5-25.3c6.1-10.8,9.2-23.3,9.2-37.4S1204.9,158.9,1198.9,148.1z M1157.3,204.1
                       c-2.8,5.4-7.3,9.8-12.7,12.6c-5.7,3-12.8,4.5-21.5,4.5h-30.6v-71h30.4c8.8,0,16,1.5,21.7,4.4c5.4,2.7,9.9,7,12.8,12.3
                       c2.9,5.7,4.4,12.1,4.2,18.6C1161.6,192,1160.2,198.4,1157.3,204.1z"></path>
                    <path fill="#FFFFFF" d="M1333.6,165.9v39.3c-2.8-0.8-5.7-1.3-8.6-1.7c-3.4-0.5-6.7-0.7-10.1-0.7c-6.1-0.1-12.1,1.3-17.4,4.2
                       c-5,2.7-9.2,6.8-12.1,11.7c-3,5.2-4.5,11.2-4.4,17.3v90.4h-44.4V166.6h43v27.9h1.7c2.9-9.9,7.8-17.4,14.7-22.5
                       c6.9-5.1,15.2-7.8,23.8-7.6c2.4,0,4.8,0.2,7.2,0.4C1329.1,165,1331.4,165.4,1333.6,165.9z"></path>
                    <path fill="#FFFFFF" d="M1421.2,329.5c-16.2,0-30.2-3.5-41.9-10.4c-11.7-6.8-21.2-16.8-27.3-28.9c-6.4-12.4-9.6-26.8-9.6-43.1
                       c0-16.5,3.2-31,9.6-43.3c6.1-12.1,15.6-22.1,27.3-28.9c11.8-6.9,25.7-10.4,41.9-10.4s30.2,3.4,41.9,10.4
                       c11.7,6.8,21.1,16.8,27.2,28.9c6.4,12.4,9.6,26.8,9.6,43.3c0,16.4-3.2,30.7-9.6,43.1c-6.1,12.1-15.6,22.1-27.2,28.9
                       C1451.4,326.1,1437.4,329.5,1421.2,329.5z M1421.4,295.2c7.3,0,13.5-2.1,18.4-6.3c5-4.2,8.7-9.9,11.2-17.2
                       c5.1-16.2,5.1-33.5,0-49.7c-2.5-7.3-6.3-13-11.2-17.3c-4.9-4.2-11.1-6.3-18.4-6.4c-7.4,0-13.7,2.1-18.7,6.4s-8.8,10-11.3,17.3
                       c-5.1,16.2-5.1,33.5,0,49.7c2.5,7.3,6.3,13,11.3,17.2S1414,295.2,1421.4,295.2z"></path>
                 </g>
              </g>
              </svg>
                  
               </div>
            </div>
            <div class="row mt20 mt-md50">
               <div class="col-12 text-center">
                  <div class="f-20 f-md-24 w700 black-clr lh140 preheadline-lp">
                    <span class="orange-gradient"> Are you sick & tired of losing clicks </span> with long, broken, or <br class="d-none d-md-block"> suspicious links in your emails, website, etc.?
                     <!-- First to JVZoo & <span class="under">MUST HAVE Solution</span> for All Marketers…   -->
                  </div>
               </div>
               <div class="col-12 mt20 mt-md80 relative">
                  <div class="linkpro-gametext d-none d-md-block">
                     Next-Gen Technology
                  </div>
                  <div class="linkpro-main-heading">
                      <div class=" f-md-45 f-28 w800 text-center white-clr lh140 ">
                           <span class="orange-gradient"> Convert ANY Long & Ugly Marketing URL into Profit-Pulling SMART Link </span> (Clean, Trackable, & QR Code Ready) <u>in Just 30 Seconds</u>... 
                      </div>
                    
                  </div>
               </div>

               <div class="col-12 mt20 mt-md50 text-center">
                  <div class="f-20 f-md-28 w500 text-center lh140 white-clr text-capitalize">               
                     Introducing The Industry-First, 5-In-One Builder – <span class="orange-gradient w700">QR Code, Link Cloaker, Shortener, Checkout Links & Bio-Pages Creator - </span> All Rolled into One Easy to Use App!
                  </div>
               </div>
            </div>

            <div class="row mt20 mt-md70 align-items-center">
               <div class="col-md-8 col-12 mx-auto">
                  <div class="video-box">
                       <!--<video width="100%" height="auto" controls autoplay muted="muted">-->
                       <!--       <source src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/coming-soon.mp4" type="video/mp4">-->
                       <!--    </video> -->
                      <div style="padding-bottom: 56.25%;position: relative;">
                         <iframe src="https://linkpro.dotcompal.com/video/embed/x8ipwmt2vf" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
   box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe> No Matter What You Do Online for Living
                     </div>
                  </div>
               </div>
               <div class="col-md-4 col-12 mx-auto">
                  <div class="">
                     <ul class="linkpro-list-head pl0 m0 f-16 f-md-18 lh150 w400 white-clr">
                        <li><span class="orange-gradient w700">	Boost Your Click Through Rates</span> By Cloaking Long & Suspicious Links</li>
                        <li><span class="orange-gradient w700">	Create QR Codes</span> For Touch-Less Payments & Branding For Local Businesses</li>
                        <li><span class="orange-gradient w700">	Double Your Email Clicks & Profits</span></li>
                        <li><span class="orange-gradient w700">	Boost Followership</span> Using Ready-To-Use Bio Pages.</li>
                        <li><span class="orange-gradient w700">	Fix All Broken Links on Any Website</span> In Just A Few Clicks </li>
                        <li><span class="orange-gradient w700">	Track Even A Single Click</span> So Never Lose Affiliate Commissions Again</li>
                        <li>	Sell High In-Demand Services With <span class="orange-gradient w700">Included Commercial License.</span></li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="tr-all">
         <picture>
         <source media="(min-width:768px)" srcset="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/linkpro.webp">
               <source media="(min-width:320px)" srcset="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/linkpro-mview.webp" style="width:100%" class="vidvee-mview">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/linkpro.webp" alt="" class="img-fluid" style="width: 100%;">
         </picture>
      </div>
      <!------Trendio Section Ends------>
      <!-------Exclusive Bonus----------->
      <div class="exclusive-bonus">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                  <div class="white-clr f-24 f-md-36 lh140 w600">
                     Exclusive Bonus #3 : LegalSuites
                  </div>
                  <div class="f-18 f-md-20 w500 mt20 white-clr">
                     20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="legalsuite-header-section">
      <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 495.7 104.2" style="enable-background:new 0 0 495.7 104.2; max-height:60px;" xml:space="preserve">
                     <style type="text/css">
                        .st0{fill:#FFFFFF;}
                        .st1{fill:#FFA829;}
                        .st2{fill:url(#SVGID_1_);}
                        .st3{fill:url(#SVGID_00000132794433514585357610000014266780508499585408_);}
                     </style>
                     <g>
                        <path class="st0" d="M96.9,82.6c-7.5,2.8-15.7,4.4-24.2,4.8c0.2,0.7,0,1.4-0.5,1.9c-3.2,3.4-6.4,6.1-9.9,8.2
                           c-5.4,3.4-11.5,5.6-17.9,6.5l-0.3,0.1c-0.2,0-0.3,0.1-0.5,0.1c-0.2,0-0.3,0-0.5-0.1l-0.3-0.1c-6.4-0.9-12.4-3.1-17.9-6.6
                           C11.6,89,3.4,74.5,3.2,58.7c0-0.9,0.6-1.7,1.4-2c0,0,0.1,0,0.1,0C1.2,50.6-0.5,43.9,0.1,37C1.8,19.4,18.3,4.9,42.3,0
                           c1-0.2,2,0.4,2.3,1.4c0.3,1-0.2,2.1-1.2,2.4c-5.4,2-10.2,4.6-14.2,7.7l14-3.6c0.3-0.1,0.7-0.1,1,0l38.4,9.9c0.9,0.2,1.5,1,1.5,1.9
                           v38.1c0,7.9-2,15.6-5.7,22.4c5.9,0.1,11.7-0.4,17.4-1.5c1-0.2,2,0.4,2.3,1.4C98.4,81.2,97.9,82.2,96.9,82.6z"/>
                        <g>
                           <path class="st0" d="M291.8,67.6l2.5-5.7c3.2,2.5,8.2,4.3,13.1,4.3c6.2,0,8.8-2.2,8.8-5.1c0-8.6-23.6-3-23.6-17.7
                              c0-6.4,5.1-11.8,16-11.8c4.8,0,9.7,1.3,13.2,3.5l-2.3,5.7c-3.6-2.1-7.5-3.1-10.9-3.1c-6.1,0-8.6,2.4-8.6,5.4
                              c0,8.4,23.5,3,23.5,17.5c0,6.3-5.1,11.8-16,11.8C301.2,72.4,295,70.4,291.8,67.6z"/>
                           <path class="st0" d="M464,67.6l2.5-5.7c3.2,2.5,8.2,4.3,13.1,4.3c6.2,0,8.8-2.2,8.8-5.1c0-8.6-23.6-3-23.6-17.7
                              c0-6.4,5.1-11.8,16-11.8c4.8,0,9.7,1.3,13.2,3.5l-2.3,5.7c-3.6-2.1-7.5-3.1-10.9-3.1c-6.1,0-8.6,2.4-8.6,5.4
                              c0,8.4,23.5,3,23.5,17.5c0,6.3-5.1,11.8-16,11.8C473.4,72.4,467.2,70.4,464,67.6z"/>
                           <path class="st0" d="M330.3,54.5V32.2h7.3v22.1c0,8.1,3.7,11.7,10.1,11.7s10-3.5,10-11.7V32.2h7.2v22.3c0,11.6-6.5,17.8-17.3,17.8
                              C336.9,72.4,330.3,66.1,330.3,54.5z"/>
                           <path class="st0" d="M375.3,32.2h7.3v39.6h-7.3V32.2z"/>
                           <path class="st0" d="M401.4,38.4h-13.1v-6.2h33.6v6.2h-13.1v33.4h-7.3V38.4H401.4z"/>
                           <path class="st0" d="M457.2,65.6v6.2h-29.7V32.2h28.9v6.2h-21.6v10.3H454v6.1h-19.1v10.9L457.2,65.6L457.2,65.6z"/>
                        </g>
                        <g>
                           <path class="st1" d="M96.2,80.7c-10.5,3.9-23,5.6-36.1,4.4C25.9,81.9,0,60.5,2.1,37.2C3.7,20,20.3,6.5,42.7,2
                              C26.1,8.2,14.6,19.8,13.2,34.1C11.1,57.4,37,78.8,71.1,81.9C79.9,82.7,88.4,82.2,96.2,80.7z"/>
                           <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="20.0931" y1="1069.2449" x2="47.1737" y2="1042.1643" gradientTransform="matrix(1 0 0 1 0 -978)">
                              <stop  offset="0" style="stop-color:#0055FF"/>
                              <stop  offset="1" style="stop-color:#008CFF"/>
                           </linearGradient>
                           <path class="st2" d="M68.3,87.8c-2,0-3.9-0.1-5.9-0.2c-1.3,1.1-2.7,2.1-4.2,3.1c-4.5,2.8-9.3,4.6-14.5,5.4
                              c-5.1-0.8-10-2.6-14.5-5.4c-8.7-5.4-14.6-13.8-17-23.4c-2.8-2.7-5.1-5.6-7-8.6c0.2,15.2,8,29,20.9,37.1c5.3,3.3,11,5.4,17.2,6.3
                              l0.4,0.1l0.4-0.1c6.1-0.9,11.9-3,17.2-6.3c3.6-2.2,6.7-4.9,9.5-7.9C70,87.8,69.1,87.8,68.3,87.8z"/>
                           <linearGradient id="SVGID_00000129182319601907477880000006569859738938224028_" gradientUnits="userSpaceOnUse" x1="33.5187" y1="1042.8949" x2="80.3658" y2="996.0477" gradientTransform="matrix(1 0 0 1 0 -978)">
                              <stop  offset="0" style="stop-color:#0055FF"/>
                              <stop  offset="1" style="stop-color:#008CFF"/>
                           </linearGradient>
                           <path style="fill:url(#SVGID_00000129182319601907477880000006569859738938224028_);" d="M43.7,9.9l-17.5,4.5
                              c-2,1.9-21.6,31.5,10.2,51.6l2.2-13c-2.2-1.6-3.6-4.2-3.6-7c0-4.8,3.9-8.7,8.7-8.7s8.7,3.9,8.7,8.7c0,2.9-1.4,5.4-3.6,7l3.4,20.5
                              C59,76,67,78.1,76.4,79.8c3.7-6.5,5.7-14,5.7-21.9V19.8L43.7,9.9z M40.3,17.5l-0.2,2.2l-1.6-1.4l-2.1,0.5l0.9-2l-1.1-1.9l2.2,0.2
                              l1.4-1.6l0.4,2.1l2,0.8L40.3,17.5z M59.9,22.7l-0.2,2.5l-1.8-1.7l-2.4,0.6l1-2.2l-1.3-2.1l2.5,0.3l1.6-1.9l0.5,2.4l2.3,1
                              L59.9,22.7z M74.2,71.3L74,74.6l-2.5-2.3l-3.3,0.8l1.4-3.1l-1.8-2.9l3.4,0.4l2.2-2.5l0.7,3.3l3.1,1.3L74.2,71.3z M74.8,51.1
                              l-0.2,3.1l-2.3-2.1l-3,0.7l1.3-2.8L69,47.4l3.1,0.3l2-2.4l0.6,3l2.9,1.2L74.8,51.1z M75.4,31l-0.2,2.8l-2.1-1.9l-2.7,0.6l1.1-2.5
                              l-1.4-2.4l2.7,0.3l1.8-2.1l0.6,2.7l2.6,1.1L75.4,31z"/>
                        </g>
                        <g>
                           <path class="st0" d="M105.1,32.2h11.2v30.7h18.9v8.9h-30.1V32.2z"/>
                           <path class="st0" d="M171.5,63.1v8.7h-31.8V32.2h31.1v8.7h-20v6.7h17.6V56h-17.6v7.2L171.5,63.1L171.5,63.1z"/>
                           <path class="st0" d="M204.2,51.2h9.9v16.4c-4.6,3.3-10.9,5-16.6,5c-12.6,0-21.8-8.6-21.8-20.6s9.2-20.6,22.1-20.6
                              c7.4,0,13.4,2.5,17.3,7.2L208,45c-2.7-3-5.8-4.4-9.6-4.4c-6.8,0-11.3,4.5-11.3,11.3c0,6.7,4.5,11.3,11.2,11.3
                              c2.1,0,4.1-0.4,6.1-1.3L204.2,51.2L204.2,51.2z"/>
                           <path class="st0" d="M253,63.1v8.7h-31.8V32.2h31.1v8.7h-20v6.7h17.6V56h-17.6v7.2L253,63.1L253,63.1z"/>
                           <path class="st0" d="M259.2,32.2h11.2v30.7h18.9v8.9h-30.1V32.2z"/>
                        </g>
                     </g>
                  </svg>
                  
               </div>
            </div>
         </div>
         <div class="legalsuite-header-wrap">
            <div class="container">
               <div class="row">
                  <div class="col-12 text-center">
                     <div class="legalsuite-preheadline f-16 f-md-22 w400 white-clr lh140">
                        Kick Start Simple 7-Minute Agency Services to Website Owners… 
                     </div>
                  </div>
                  <div class="col-12 mt-md30 mt20 f-md-45 f-26 w700 text-center white-clr lh140">
                     Analyse, Detect, & Fix Deadly Legal Flaws<br class="d-none d-md-block"> on Your Website & For Clients Just by<br class="d-none d-md-block"> <span class="cyan-clr w900">Copy-Pasting a Single Line of Code </span>
                  </div>
                  <div class="col-12 mt20 mt-md30 text-center">
                     <div class="f-18 f-md-26 w500 text-center lh130 white-clr text-capitalize">               
                        Charge BIG for Fixing Deadly Law Issues in Minutes Like ADA, GDPR, Policies,<br class="d-none d-md-block"> T&C & More | No Coding | 100% Newbie Friendly 
                     </div>
                     <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/legalsuites-h-line.webp" alt="H-Line" class="mx-auto img-fluid d-none d-md-block mt15">
                  </div>
               </div>
               <div class="row mt20 mt-md40">
                  <div class="col-md-10 col-12 mx-auto">
                     <!-- <img src="https://cdn.oppyo.com/launches/legelsuites/special/product-image.webp" class="img-fluid d-block mx-auto"> -->
                     <div class="col-12 responsive-video">
                        <iframe src="https://legelsuites.dotcompal.com/video/embed/t9ev45pyjl" style="width:100%; height:100%" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                        </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="legalsuite-feature-wrap">
                     <div class="row">
                        <div class="col-12 col-md-6">
                           <div class="legalsuite-feature-list">
                             
                           <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/legalsuites-list-check.webp">
                              <div class="f-18 lh140 w400"><span class="w600">Quick-Start with Built-in Agency Website Builder</span> with Drag & Drop Editor to Start Selling Services Starting Today </div>
                           </div>
                           <div class="legalsuite-feature-list">
                           <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/legalsuites-list-check.webp">
                              <div class="f-18 lh140 w400"><span class="w600">Scans Any Website with LegelSuites Analyzer</span> to detect Legal Flaws on your Website like GDPR, ADA, Privacy Policy, T&C, etc</div>
                           </div>
                           <div class="legalsuite-feature-list">
                           <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/legalsuites-list-check.webp">
                              <div class="f-18 lh140 w400"><span class="w600">Help You Make Your Website ADA Compliant</span> by Copy-Pasting a Single Line of Code </div>
                           </div>
                           <div class="legalsuite-feature-list">
                           <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/legalsuites-list-check.webp">
                              <div class="f-18 lh140 w400">
                                 <span class="w600">Get Your Website GDPR & Privacy Laws Comply</span> as per International Laws 
                              </div>
                           </div>
                           <div class="legalsuite-feature-list">
                           <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/legalsuites-list-check.webp">
                              <div class="f-18 lh140 w400"><span class="w600">Generate Cookies Policies & Cookies Widgets</span> with an inbuilt GDPR Tool </div>
                           </div>
                           <div class="legalsuite-feature-list">
                           <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/legalsuites-list-check.webp">
                              <div class="f-18 lh140 w400"><span class="w600">Copy Paste a Single Line Of Code</span> To Make Any Of Your Business Websites 100% Compliance </div>
                           </div>
                        </div>
                        <div class="col-12 col-md-6">
                           <div class="f-18 lh140 w400">                             
                              <div class="legalsuite-feature-list">
                              <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/legalsuites-list-check.webp">
                                 <div class="f-18 lh140 w400"><span class="w600">Get Tailor-Made Legal Policies</span> and Other Legal Documents For Your's And Your Client's Websites </div>
                              </div>
                              <div class="legalsuite-feature-list">
                              <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/legalsuites-list-check.webp">
                                 <div class="f-18 lh140 w600">Tap into Hot Website Agency Services to Make Huge Profits </div>
                              </div>
                              <div class="legalsuite-feature-list">
                              <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/legalsuites-list-check.webp">
                                 <div class="f-18 lh140 w400"><span class="w600">Website Widget to add on your Agency Website</span> to Offer Free Legal & Compliance Tests to quickly generate leads </div>
                              </div>
                              <div class="legalsuite-feature-list">
                              <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/legalsuites-list-check.webp">
                                 <div class="f-18 lh140 w400"><span class="w600">Step-by-Step Training</span> is Included to get you Started Today </div>
                              </div>
                              <div class="legalsuite-feature-list">
                              <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/legalsuites-list-check.webp">
                                 <div class="f-18 lh140 w400"><span class="w600">Very Limited Agency License Included</span> to Serve Your Clients and Make Huge Profits </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="gr-1">
         <div class="container-fluid p0">
         <picture>
         <source media="(min-width:768px)" srcset="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/legelsuites.webp">
               <source media="(min-width:320px)" srcset="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/legelsuites-mview.webp" style="width:100%" class="vidvee-mview">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/legelsuites.webp" alt="" class="img-fluid" style="width: 100%;">
         </picture>
         </div>
      </div>
      <!-------Exclusive Bonus----------->
      <div class="exclusive-bonus">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                  <div class="white-clr f-24 f-md-36 lh140 w600">
                     Exclusive Bonus #4 : RestroSuite
                  </div>
                  <div class="f-18 f-md-20 w500 mt20 white-clr">
                     20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!------LinkPro Section------>
     
      <!-- Bonus Section Header Start -->
      <div class="bonus-header">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-10 mx-auto heading-bg text-center">
                  <div class="f-24 f-md-36 lh140 w700"> When You Purchase Mavas, You Also Get <br class="hidden-xs"> Instant Access To These 20 Exclusive Bonuses</div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus Section Header End -->
      <!-- Bonus #1 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               
               <div class="col-12 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                  
                     <div class="col-md-5 col-12">
                        <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/bonus1.webp" class="img-fluid mx-auto d-block" alt="Bonus1">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="bonus-title-bg">
                        <div class="f-22 f-md-28 lh120 w700">BONUS 1</div>
                      </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md30">
                        250 WordPress Plugins

                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                              An Amazing Collection Of 250+ Hand-Picked, Profit Oozing, Easy To Implement, Web 2.0 Compliant, Free Wordpress Plugins To Skyrocket Your Wordpress Profits Up Above The Eiffel Towers In Paris!
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #1 Section End -->
      <!-- Bonus #2 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               
               <div class="col-12">
                  <div class="row align-items-center">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/bonus2.webp" class="img-fluid mx-auto d-block" alt="Bonus2">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                     <div class="bonus-title-bg">
                         <div class="f-22 f-md-28 lh120 w700">BONUS 2</div>
                      </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md30">
                        100 Mobile Web Templates 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           Make Thousands of Dollars Online Offering Mobile Friendly Websites to Your Local Business Clients!
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #2 Section End -->
      <!-- Bonus #3 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                     <div class="col-md-5 col-12">
                        <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/bonus3.webp" class="img-fluid mx-auto d-block " alt="Bonus3">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="bonus-title-bg">
                           <div class="f-22 f-md-28 lh120 w700">BONUS 3</div>
                         </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md30">
                        Modern Affiliate Marketing Video Upgrade
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           Picking the Right Affiliate Program to Promote all Boils Down to ROI.<br>
                           Make no mistake, if you want to succeed with your affiliate marketing business, you have to focus on ROI. If you have a fuzzy idea of what return on investment means, you're playing the game wrong.
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #3 Section End -->
      <!-- Bonus #4 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/bonus4.webp" class="img-fluid mx-auto d-block " alt="Bonus4">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                     <div class="bonus-title-bg">
                         <div class="f-22 f-md-28 lh120 w700">BONUS 4</div>
                     </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md30">
                           SEO And Tracking
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           In this 6-part video course you will learn how to turn long or affiliate links into short and pretty links. Also it will teach you how to use 'related posts' to help visitors find related content.
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #4 End -->
      <!-- Bonus #5 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               
               <div class="col-12 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                     <div class="col-md-5 col-12">
                        <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/bonus5.webp" class="img-fluid mx-auto d-block " alt="Bonus5">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md30">
                     <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 5</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-colo mt20 mt-md30">
                        Social Media Marketing Boost
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           Discover 100 Powerful Social Media Marketing Tips That Will Boost Your Following, Gain Authority And Increase Engagement On Social Media!   
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #5 End -->
      <!-- CTA Btn Start-->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center mt20 mt-md20">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 purple-clr">"MAVAS3"</span> for an Additional <span class="w700 purple-clr">$3 Discount</span> on Mavas
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn px-md80">
                  <span class="text-center">Grab Mavas + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                  Use Coupon Code <span class="w700 purple-clr">"Mavas"</span> for an Additional <span class="w700 purple-clr">$50 Discount</span> on Bundle Deal
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflinkbundle']; ?>" class="text-center bonusbuy-btn px-md80">
                  <span class="text-center">Grab Mavas Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 smmltd">Days</span>
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Sec</span>
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Btn End -->
     <!-- Bonus #6 Start -->
     <div class="section-bonus">
         <div class="container">
            <div class="row">
               
               <div class="col-12 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/bonus6.webp" class="img-fluid mx-auto d-block " alt="Bonus6">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                     <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 6</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md30">
                        Mobile eCommerce Simplified

                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           Brand New High Quality Lead Magnet Targets Ecommerce and Online Store Owners<br><br>
                           This mobile eCommerce video course explains how to create user-friendly, mobile optimized web stores in less than 24 hours.
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #6 End -->
      <!-- Bonus #7 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               
               <div class="col-12 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                     <div class="col-md-5 col-12">
                        <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/bonus7.webp" class="img-fluid mx-auto d-block " alt="Bonus7">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="bonus-title-bg">
                         <div class="f-22 f-md-28 lh120 w700">BONUS 7</div>
                     </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md30">
                           Web Traffic Excellence
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           Discover How to Build Hundreds of Content Rich, Dynamically Changing, Keyword Covered Web Pages in Mere Minutes. Start Building the High-Quality Content Sites You Need to Succeed on the Internet Today!
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #7 End -->
      <!-- Bonus #8 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               
               <div class="col-12  d-flex align-content-center flex-wrap">
                     <div class="row align-items-center">
                        <div class="col-md-5 order-md-2 col-12">
                           <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/bonus8.webp" class="img-fluid mx-auto d-block " alt="Bonus8">
                        </div>
                        <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="bonus-title-bg">
                        <div class="f-22 f-md-28 lh120 w700">BONUS 8</div>
                     </div>
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt2- mt-md30">
                        SEO and PPC Ninja Calculator
                     </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           Quickly Make And Brand Your Own Iframes!<br><br>
                           This iFrame generator helps you to instantly create customized iFrames for your web site/blog. Fill in the fields below and within seconds you will receive the HTML code for your custom iFrame. 
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #8 End -->
      <!-- Bonus #9 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
              
               <div class="col-12 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                     <div class="col-md-5 col-12">
                        <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/bonus9.webp" class="img-fluid mx-auto d-block " alt="Bonus9">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 9</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md30">
                           Sell Grow with Content Writing Tips
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           Learn How You Can Sell More with Content Marketing!   
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #9 End -->

      <!-- Bonus #10 start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
              
               <div class="col-12 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/bonus10.webp" class="img-fluid mx-auto d-block " alt="Bonus10">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="bonus-title-bg">
                           <div class="f-22 f-md-28 lh120 w700">BONUS 10</div>
                        </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md30">
                        Doubling Your Sales With These Tricks  

                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           Learn How to Double your Sales with These Tricks!<br><br>
                           Many successful digital entrepreneurs have likely already stated this at one point or another, but this is very important, which is why this report will going to say it one more time.
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #10 End -->
      <!-- CTA Btn Start-->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center mt20 mt-md20">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 purple-clr">"MAVAS"</span> for an Additional <span class="w700 purple-clr">$3 Discount</span> on Mavas
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn px-md80">
                  <span class="text-center">Grab Mavas + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                  Use Coupon Code <span class="w700 purple-clr">"Mavas"</span> for an Additional <span class="w700 purple-clr">$50 Discount</span> on Bundle Deal
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflinkbundle']; ?>" class="text-center bonusbuy-btn px-md80">
                  <span class="text-center">Grab Mavas Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 smmltd">Days</span>
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Sec</span>
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Btn End -->
        <!-- Bonus #11 start -->
        <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                     <div class="col-md-5 col-12">
                        <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/bonus11.webp" class="img-fluid mx-auto d-block " alt="Bonus11">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 11</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md30">
                        WP In-Content Popup Pro
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           WP In-Content Popup Pro is a new plugin that lets you create attention grabbing popups within your content.  You can trigger in-content video popups, image popups, text popups, or content popups which you can use to showcase your product, article or even your profile.
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #11 End -->
      <!-- Bonus #12 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/bonus12.webp" class="img-fluid mx-auto d-block " alt="Bonus12">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 12</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md30">
                        Sales Funnel Optimization Strategy Video
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           Whether you make your money through ad clicks using the Adsense monetization platform or you sell affiliate products or your own services or you run your own online drop shipping store, you're trying to convert people from simple clickers of links and readers of your content to cold hard cash.
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #12 End -->
      <!-- Bonus #13 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
              
               <div class="col-12 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                     <div class="col-md-5 col-12">
                        <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/bonus13.webp" class="img-fluid mx-auto d-block " alt="Bonus13">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 13</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md30">
                           Introduction to Postcard Marketing
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           Postcard Marketing Video Course will provide you with a basic overview of Postcard marketing. You will receive three straight to the point lessons that will teach you exactly what it is, how it works and how you can use to effectively use it to market your own profitable business.
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #13 End -->
      <!-- Bonus #14 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               
               <div class="col-12 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/bonus14.webp" class="img-fluid mx-auto d-block " alt="Bonus14">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 14</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md30">
                        Buying Traffic to Generate Massive Website Visitors

                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        An intrinsic understanding of your competition and how to better them is the most important component of any digital marketing strategy. Today, there is such small distances between you and your competition - it's unlikely that you're offering anything that cannot be bought on some other website.
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #14 End -->
      <!-- Bonus #15 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               
               <div class="col-12 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                     <div class="col-md-5 col-12">
                        <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/bonus15.webp" class="img-fluid mx-auto d-block " alt="Bonus15">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 15</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md30">
                        Easy Survey Generator
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        It’s the software your competitors don’t want you to know! Who Else Wants To Discover The Ultimate Secret For Getting Into Your Prospect’s Heads And Boosting Your Chances For Riches!                        
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #15 End -->
      <!-- CTA Btn Start-->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center mt20 mt-md20">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 purple-clr">"MAVAS3"</span> for an Additional <span class="w700 purple-clr">$3 Discount</span> on Mavas
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn px-md80">
                  <span class="text-center">Grab Mavas + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                  Use Coupon Code <span class="w700 purple-clr">"Mavas"</span> for an Additional <span class="w700 purple-clr">$50 Discount</span> on Bundle Deal
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflinkbundle']; ?>" class="text-center bonusbuy-btn px-md80">
                  <span class="text-center">Grab Mavas Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 smmltd">Days</span>
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Sec</span>
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Btn End -->
      <!-- Bonus #16 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/bonus16.webp" class="img-fluid mx-auto d-block " alt="Bonus16">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 16</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md30">
                        Youtube Success Step By Step

                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           YouTube Success Step By Step is a new powerful report that explains to the reader how they can create a successful YouTube channel in the fastest possible time.
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #16 End -->
      <!-- Bonus #17 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
              
               <div class="col-12 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                     <div class="col-md-5 col-12">
                        <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/bonus17.webp" class="img-fluid mx-auto d-block " alt="Bonus17">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 17</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md30">
                        Virtual Networking Success
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           Networking when you are not in the office can be hard, but it may be one of the best ways to grow your business and make sure that you meet new people.
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #17 End -->

      <!-- Bonus #18 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               
               <div class="col-12 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/bonus18.webp" class="img-fluid mx-auto d-block " alt="Bonus18">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 18</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md30">
                        Power Of Visualization Video Upgrade

                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           Attracting your best life can be challenging. You are bound to face obstacles such as fear, failure, and disappointments that will make you feel like a hopeless failure.
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #18 End -->

      <!-- Bonus #19 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               
               <div class="col-12 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                     <div class="col-md-5 col-12">
                        <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/bonus19.webp" class="img-fluid mx-auto d-block " alt="Bonus19">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 19</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md30">
                        Blogging Traffic Mantra
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        Making a living as a blogger has to be one of the sweetest gigs out there. As a blogger, you’ll be able to earn passive income which means that your money will flow in even as you’re sleeping, travelling or relaxing with friends.
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #19 End -->

      <!-- Bonus #20 start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
              
               <div class="col-12 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/special-bonus/bonus20.webp" class="img-fluid mx-auto d-block " alt="Bonus20">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 20</div>
                  </div>
                  <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md30">
                  Content Marketing Formula
                  </div>
                  <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                     Content marketing is currently one of the biggest trends in digital marketing as a whole and is an area that many website owners and brands are investing in heavily right now thanks to the impressive returns that they are seeing.
                  </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #20 End -->
      <!-- Huge Woth Section Start -->
      <div class="huge-area mt30 mt-md10">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="f-md-65 f-40 lh120 w700 white-clr">That's Huge Worth of</div>
                  <br>
                  <div class="f-md-60 f-40 lh120 w800 purple-clr">$3300!</div>
               </div>
            </div>
         </div>
      </div>
      <!-- Huge Worth Section End -->
      <!-- text Area Start -->
      <div class="white-section pb0">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="f-md-36 f-25 lh140 w500">So what are you waiting for? You have a great opportunity <br class="hidden-xs"> ahead + <span class="w700">My 20 Bonus Products</span> are making it a <br class="hidden-xs"> <span class="w700">completely NO Brainer!!</span></div>
               </div>
            </div>
         </div>
      </div>
      <!-- text Area End -->
      <!-- CTA Button Section Start -->
      <div class="cta-btn-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-16 f-md-20 lh150 w500 text-center black-clr">
                  Use Coupon Code <span class="w700 purple-clr">"MAVAS3"</span> for an Additional <span class="w700 purple-clr">$3 Discount</span> on Mavas
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn px-md80">
                  Grab Mavas + My 20 Exclusive Bonuses
                  </a>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 black-clr mt-md30">
                  Use Coupon Code <span class="w700 purple-clr">"Mavas"</span> for an Additional <span class="w700 purple-clr">$50 Discount</span> on Bundle Deal
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflinkbundle']; ?>" class="text-center bonusbuy-btn px-md80">
                  <span class="text-center">Grab Mavas Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-12 mt15 mt-md20" align="center">
                  <h3 class="f-md-28 f-20 w500 text-center black">My Exclusive Bonuses Are Expiring in...</h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 col-md-10 mx-auto col-12 text-center">
                  <div class="countdown counter-black">
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                     <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                     <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->
   <!--Footer Section Start -->
   <div class="footer-section">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
            <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 685 120" style="max-height:55px">
  <defs>
    <style>
      .cls-1 {
        fill: url(#linear-gradient-2);
      }

      .cls-2 {
        fill: #fff;
      }

      .cls-3 {
        fill: #21e5f4;
      }

      .cls-4 {
        fill: url(#linear-gradient);
      }
    </style>
    <linearGradient id="linear-gradient" x1="0" y1="60" x2="25.31" y2="60" gradientUnits="userSpaceOnUse">
      <stop offset="0" stop-color="#b956ff"></stop>
      <stop offset="1" stop-color="#8d4ef9"></stop>
    </linearGradient>
    <linearGradient id="linear-gradient-2" x1="99.25" y1="59.94" x2="124.57" y2="59.94" gradientUnits="userSpaceOnUse">
      <stop offset="0" stop-color="#8d57f4"></stop>
      <stop offset="1" stop-color="#6e33ff"></stop>
    </linearGradient>
  </defs>
  <g id="Layer_1-2" data-name="Layer 1">
    <g>
      <g>
        <g>
          <polygon class="cls-2" points="343.95 66.06 343.95 66.06 300 .53 273.46 .53 343.96 99.29 355.45 83.2 343.95 66.06"></polygon>
          <polygon class="cls-2" points="367.54 66.26 414.45 .53 388.11 .53 355.71 48.61 367.54 66.26"></polygon>
        </g>
        <polygon class="cls-2" points="218.06 0 147.57 99.82 172.51 99.82 182.71 84.58 198.02 61.87 218.07 32.36 237.91 61.88 253.21 84.59 263.4 99.82 288.55 99.82 218.06 0"></polygon>
        <polygon class="cls-2" points="469.86 0 399.36 99.82 424.31 99.82 434.5 84.58 449.81 61.87 469.87 32.36 489.7 61.88 505.01 84.59 515.2 99.82 540.35 99.82 469.86 0"></polygon>
        <g>
          <path class="cls-2" d="m662.66,46.82l-96.97-14.26,16.04,21.03,5.2.9,69.12,11.61c3.34.56,5.78,3.45,5.78,6.83,0,3.83-3.1,6.93-6.93,6.93h-75.79l-15.37,20.13,94.46.02c14.79,0,26.78-11.99,26.78-26.78,0-13.07-9.43-24.22-22.31-26.4Z"></path>
          <polygon class="cls-2" points="685 1.08 589.71 1.08 589.71 1.06 581.89 1.06 566.5 21.21 669.63 21.21 685 1.08"></polygon>
        </g>
      </g>
      <g>
        <polygon class="cls-4" points="25.31 20.82 25.31 120 0 99.11 0 0 25.31 20.82"></polygon>
        <polygon class="cls-1" points="124.57 0 124.57 99.13 99.25 119.89 99.25 20.76 124.57 0"></polygon>
        <polygon class="cls-3" points="92.74 25.17 92.74 62.88 62.24 100.89 31.82 62.89 31.82 25.17 62.24 63.17 92.74 25.17"></polygon>
      </g>
    </g>
  </g>
</svg>
            </div>
            <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md60">
               <div class="f-md-18 f-16 w400 lh140 white-clr text-xs-center">Copyright © Mavas 2023</div>
               <ul class="footer-ul w400 f-md-18 f-16 white-clr text-center text-md-right">
                  <li><a href="https://support.oppyo.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                  <li><a href="http://getmavas.com/legal/privacy-policy.html " class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                  <li><a href="http://getmavas.com/legal/terms-of-service.html " class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                  <li><a href="http://getmavas.com/legal/disclaimer.html " class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                  <li><a href="http://getmavas.com/legal/gdpr.html " class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                  <li><a href="http://getmavas.com/legal/dmca.html " class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                  <li><a href="http://getmavas.com/legal/anti-spam.html " class="white-clr t-decoration-none">Anti-Spam</a></li>
               </ul>
            </div>
         </div>
      </div>
   </div>
   <!--Footer Section End -->
      <!-- timer --->
      <?php
         if ($now < $exp_date) {
         ?>
      <script type="text/javascript">
         // Count down milliseconds = server_end - server_now = client_end - client_now
         var server_end = <?php echo $exp_date; ?> * 1000;
         var server_now = <?php echo time(); ?> * 1000;
         var client_now = new Date().getTime();
         var end = server_end - server_now + client_now; // this is the real end time
         
         var noob = $('.countdown').length;
         
         var _second = 1000;
         var _minute = _second * 60;
         var _hour = _minute * 60;
         var _day = _hour * 24
         var timer;
         
         function showRemaining() {
         var now = new Date();
         var distance = end - now;
         if (distance < 0) {
         	clearInterval(timer);
         	document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
         	return;
         }  
         var days = Math.floor(distance / _day);
         var hours = Math.floor((distance % _day) / _hour);
         var minutes = Math.floor((distance % _hour) / _minute);
         var seconds = Math.floor((distance % _minute) / _second);
         if (days < 10) {
         	days = "0" + days;
         }
         if (hours < 10) {
         	hours = "0" + hours;
         }
         if (minutes < 10) {
         	minutes = "0" + minutes;
         }
         if (seconds < 10) {
         	seconds = "0" + seconds;
         }
         var i;
         var countdown = document.getElementsByClassName('countdown');
         for (i = 0; i < noob; i++) {
         	countdown[i].innerHTML = '';
         
         	if (days) {
         		countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + days + '</span><span class="f-14 f-md-18 smmltd">Days</span> </div>';
         	}
         
         	countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + hours + '</span><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>';
         
         	countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">' + minutes + '</span><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>';
         
         	countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">' + seconds + '</span><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>';
         }
         
         }
         timer = setInterval(showRemaining, 1000);
         	
      </script>
      <?php
         } else {
         echo "Times Up";
         }
         ?>
      <!--- timer end-->
   </body>
   <script>
const greeting = ['Dominate Social Media, Drive Website Traffic, Create Ads,', 'Provide Legal Guidance, Optimize SEO, Manage Finances,', 'Generate Quality Leads, Create Compelling Content,', '& Much More... All Hands-Free!'];
let currentGreetingIndex = 0;
let currentCharacterIndex = 0;
let isDeleting = false;
let isPaused = false;
let pauseEnd = 0;

function typeWriterEffect() {
  const greetingElement = document.getElementById('typing');

  if (isPaused && Date.now() > pauseEnd) {
    isPaused = false;
    if (isDeleting) {
      currentGreetingIndex = (currentGreetingIndex + 1) % greeting.length;
      isDeleting = false;
    } else {
      isDeleting = true;
    }
  }

  if (!isPaused && !isDeleting && currentCharacterIndex === greeting[currentGreetingIndex].length) {
    isPaused = true;
    pauseEnd = Date.now() + 800; 
    return setTimeout(typeWriterEffect, 50);
  }

  if (!isPaused && isDeleting && currentCharacterIndex === 0) {
    isPaused = true;
    pauseEnd = Date.now() + 200; 
    return setTimeout(typeWriterEffect, 50);
  }

  const timeout = isDeleting ? 100 : 200;
  greetingElement.innerText = greeting[currentGreetingIndex].substring(0, currentCharacterIndex);
  currentCharacterIndex = isDeleting ? currentCharacterIndex - 1 : currentCharacterIndex + 1;
  setTimeout(typeWriterEffect, timeout);
}

typeWriterEffect();
</script>
</html>
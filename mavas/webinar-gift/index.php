<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<!-- Tell the browser to be responsive to screen width -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="title" content="MAVAS Webinar Gift">
    <meta name="description" content="MAVAS Webinar Gift">
    <meta name="keywords" content="First-To-Market #1 Super VA That Finish Your 100s Type of Marketing Tasks (Better & Faster Than Any CMO Out There...) AND Trim Your Workload, Hiring/Firing Headaches, & Monthly Bills">
    <meta property="og:image" content="https://www.getmavas.com/webinar-gift/thumbnail.png">
    <meta name="language" content="English">
    <meta name="revisit-after" content="1 days">
    <meta name="author" content="Dr. Amit Pareek">
    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:title" content="MAVAS Webinar Gift">
    <meta property="og:description" content="First-To-Market #1 Super VA That Finish Your 100s Type of Marketing Tasks (Better & Faster Than Any CMO Out There...) AND Trim Your Workload, Hiring/Firing Headaches, & Monthly Bills">
    <meta property="og:image" content="https://www.getmavas.com/webinar-gift/thumbnail.png">
    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:title" content="MAVAS Webinar Gift">
    <meta property="twitter:description" content="First-To-Market #1 Super VA That Finish Your 100s Type of Marketing Tasks (Better & Faster Than Any CMO Out There...) AND Trim Your Workload, Hiring/Firing Headaches, & Monthly Bills">
    <meta property="twitter:image" content="https://www.getmavas.com/webinar-gift/thumbnail.png">

	<title>MAVAS | Webinar Gift</title>
	<!-- Shortcut Icon  -->
	<link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Kalam:wght@300;400;700&family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
	<!-- Css CDN Load Link -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="../common_assets/css/bootstrap.min.css" type="text/css">
   	<link rel="stylesheet" type="text/css" href="assets/css/style.css">
   	<script src="https://cdn.oppyotest.com/launches/MAVAS/common_assets/js/jquery.min.js"></script>

		
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-NMK2MBB');</script>
	<!-- End Google Tag Manager -->


</head>
<body>
	<a href="#" id="scroll" style="display: block;"><span></span></a>
	
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NMK2MBB"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) --> 

	<!-- New Timer  Start-->
	<?php
		$date = 'August 15 2023 11:00 AM EST';
		$exp_date = strtotime($date);
		$now = time();  
		/*
		
		$date = date('F d Y g:i:s A eO');
		$rand_time_add = rand(700, 1200);
		$exp_date = strtotime($date) + $rand_time_add;
		$now = time();*/
		
		if ($now < $exp_date) {
	?>
	<?php
		} else {
			echo "";
		}
	?>
	<!-- New Timer End -->

	<?php
		if(!isset($_GET['afflink'])){
		$_GET['afflink'] = 'https://getmavas.com/special/';
		$_GET['name'] = 'Dr. Amit Pareek';      
		}
	?>

	<div class="header-sec" id="product">
        <div class="container">
            <div class="row">
               	<div class="col-12 text-center">
				   <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 685 120" style="max-height:55px">
  <defs>
    <style>
      .cls-1 {
        fill: url(#linear-gradient-2);
      }

      .cls-2 {
        fill: #fff;
      }

      .cls-3 {
        fill: #21e5f4;
      }

      .cls-4 {
        fill: url(#linear-gradient);
      }
    </style>
    <linearGradient id="linear-gradient" x1="0" y1="60" x2="25.31" y2="60" gradientUnits="userSpaceOnUse">
      <stop offset="0" stop-color="#b956ff"></stop>
      <stop offset="1" stop-color="#8d4ef9"></stop>
    </linearGradient>
    <linearGradient id="linear-gradient-2" x1="99.25" y1="59.94" x2="124.57" y2="59.94" gradientUnits="userSpaceOnUse">
      <stop offset="0" stop-color="#8d57f4"></stop>
      <stop offset="1" stop-color="#6e33ff"></stop>
    </linearGradient>
  </defs>
  <g id="Layer_1-2" data-name="Layer 1">
    <g>
      <g>
        <g>
          <polygon class="cls-2" points="343.95 66.06 343.95 66.06 300 .53 273.46 .53 343.96 99.29 355.45 83.2 343.95 66.06"></polygon>
          <polygon class="cls-2" points="367.54 66.26 414.45 .53 388.11 .53 355.71 48.61 367.54 66.26"></polygon>
        </g>
        <polygon class="cls-2" points="218.06 0 147.57 99.82 172.51 99.82 182.71 84.58 198.02 61.87 218.07 32.36 237.91 61.88 253.21 84.59 263.4 99.82 288.55 99.82 218.06 0"></polygon>
        <polygon class="cls-2" points="469.86 0 399.36 99.82 424.31 99.82 434.5 84.58 449.81 61.87 469.87 32.36 489.7 61.88 505.01 84.59 515.2 99.82 540.35 99.82 469.86 0"></polygon>
        <g>
          <path class="cls-2" d="m662.66,46.82l-96.97-14.26,16.04,21.03,5.2.9,69.12,11.61c3.34.56,5.78,3.45,5.78,6.83,0,3.83-3.1,6.93-6.93,6.93h-75.79l-15.37,20.13,94.46.02c14.79,0,26.78-11.99,26.78-26.78,0-13.07-9.43-24.22-22.31-26.4Z"></path>
          <polygon class="cls-2" points="685 1.08 589.71 1.08 589.71 1.06 581.89 1.06 566.5 21.21 669.63 21.21 685 1.08"></polygon>
        </g>
      </g>
      <g>
        <polygon class="cls-4" points="25.31 20.82 25.31 120 0 99.11 0 0 25.31 20.82"></polygon>
        <polygon class="cls-1" points="124.57 0 124.57 99.13 99.25 119.89 99.25 20.76 124.57 0"></polygon>
        <polygon class="cls-3" points="92.74 25.17 92.74 62.88 62.24 100.89 31.82 62.89 31.82 25.17 62.24 63.17 92.74 25.17"></polygon>
      </g>
    </g>
  </g>
</svg>
               	</div>
               	<div class="col-12 mt30 mt-md50 relative p-md0 ">
            <div class="mainheadline f-md-42 f-25 w400 text-center white-clr lh140">
               <div class="headline-text">
               First-To-Market #<span class="w700 sky-blue">1 Super VA That Finish Your 100s Type of Marketing Tasks (Better &amp; Faster Than Any CMO Out There...) </span> AND Trim Your Workload, Hiring/Firing Headaches, &amp; Monthly Bills </div>
            </div>
         </div>
               <div class="col-12 mt-md25 mt20 p-md0">
			   		<div class="f-20 f-md-28 w500 text-center lh140 white-clr text-capitalize">
					   Get All Your/Client's Marketing Work Done I Loaded With 150,000+ Prompts I Trained In 100s Type Of Marketing Tasks
                 	 </div>
               </div>
               <div class="col-12 mt-md30 mt20 text-center white-clr">
                   	<div class="f-md-36 f-28 lh160 w700 mt20 mt-md30">
				   		You've Earned It - <br class="d-none d-md-block">
						Enjoy This Assure Gift For Attending Our Webinar!
                   	</div>
                   	<div class="mt10 f-20 f-md-32 lh160">
                       <span class="tde">Download Your Assure Gifts Below</span>
                   	</div>
               	</div>
               	<div class="col-12 col-md-10 mx-auto mt20 mt-md30">
			   		<img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/webinar-gift/product-image.png" alt="Product Box" class="mx-auto d-block img-fluid mt20 mt-md50">
               	</div>
            </div>
            </div>
	      </div>	
        <div class="nexusgpt-header-section">
      <div class="container">
            <div class="row">
               <div class="col-12 w600 lh140 text-center purple-clr f-md-40 f-28">
          <u>Premium free gift</u>
               </div>
               <div class="col-12 mt-md30 mt20">
                  <div class="row align-items-center">
                     <div class="col-md-12 text-center">
                        <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1073.21 250" style="max-height:65px;">
                           <defs>
                              <style>
                                 .cls-1ng {
                                 fill: #fff;
                                 }

                                 .cls-2ng {
                                 fill-rule: evenodd;
                                 }

                                 .cls-2ng, .cls-3ng {
                                 fill: #00a4ff;
                                 }

                                 .cls-4ng {
                                 fill: #f19b10;
                                 }
                              </style>
                           </defs>
                           <g id="Layer_1-2" data-name="Layer 1">
                              <g>
                                 <g>
                                    <g>
                                       <path class="cls-1ng" d="m872.38,109.17c-3.33-7.67-8.42-13.69-15.25-18.08-6.83-4.39-14.86-6.58-24.08-6.58-8.67,0-16.45,2-23.33,6-6.89,4-12.33,9.7-16.33,17.08-4,7.39-6,15.97-6,25.75s2,18.39,6,25.83c4,7.45,9.44,13.17,16.33,17.17,6.89,4,14.67,6,23.33,6,8.11,0,15.42-1.75,21.92-5.25s11.72-8.5,15.67-15c3.94-6.5,6.19-14.08,6.75-22.75h-49v-9.5h61.33v8.5c-.56,10.22-3.33,19.47-8.33,27.75-5,8.28-11.7,14.81-20.08,19.58-8.39,4.78-17.81,7.17-28.25,7.17s-20.56-2.53-29.33-7.58c-8.78-5.05-15.69-12.11-20.75-21.17-5.06-9.05-7.58-19.3-7.58-30.75s2.53-21.69,7.58-30.75c5.05-9.05,11.97-16.11,20.75-21.17,8.78-5.05,18.55-7.58,29.33-7.58,12.44,0,23.33,3.11,32.67,9.33,9.33,6.22,16.11,14.89,20.33,26h-13.67Z"></path>
                                       <path class="cls-1ng" d="m975.71,132.83c-6.56,6.11-16.39,9.17-29.5,9.17h-23.5v49.67h-11.67v-116.5h35.17c13,0,22.8,3.06,29.42,9.17,6.61,6.11,9.92,14.22,9.92,24.33s-3.28,18.06-9.83,24.17Zm-2.17-24.17c0-7.78-2.17-13.67-6.5-17.67-4.33-4-11.28-6-20.83-6h-23.5v47h23.5c18.22,0,27.33-7.78,27.33-23.33Z"></path>
                                       <path class="cls-1ng" d="m1073.21,75.17v9.67h-32.33v106.83h-11.67v-106.83h-32.5v-9.67h76.5Z"></path>
                                    </g>
                                    <g>
                                       <path class="cls-1ng" d="m322.77,192.08h-28.5l-47.67-72.17v72.17h-28.5v-117h28.5l47.67,72.5v-72.5h28.5v117Z"></path>
                                       <path class="cls-1ng" d="m505.27,192.08l-23.83-35.83-21,35.83h-32.33l37.5-59.5-38.33-57.5h33.17l23.5,35.33,20.67-35.33h32.33l-37.17,59,38.67,58h-33.17Z"></path>
                                       <path class="cls-1ng" d="m580.77,75.08v70c0,7,1.72,12.39,5.17,16.17,3.44,3.78,8.5,5.67,15.17,5.67s11.78-1.89,15.33-5.67c3.55-3.78,5.33-9.17,5.33-16.17v-70h28.5v69.83c0,10.45-2.22,19.28-6.67,26.5-4.45,7.22-10.42,12.67-17.92,16.33-7.5,3.67-15.86,5.5-25.08,5.5s-17.47-1.8-24.75-5.42c-7.28-3.61-13.03-9.05-17.25-16.33-4.22-7.28-6.33-16.14-6.33-26.58v-69.83h28.5Z"></path>
                                       <path class="cls-1ng" d="m689.6,189.08c-6.78-2.78-12.2-6.89-16.25-12.33-4.06-5.44-6.2-12-6.42-19.67h30.33c.44,4.33,1.94,7.64,4.5,9.92,2.55,2.28,5.89,3.42,10,3.42s7.55-.97,10-2.92c2.44-1.94,3.67-4.64,3.67-8.08,0-2.89-.97-5.28-2.92-7.17-1.95-1.89-4.33-3.44-7.17-4.67-2.83-1.22-6.86-2.61-12.08-4.17-7.56-2.33-13.72-4.67-18.5-7-4.78-2.33-8.89-5.78-12.33-10.33-3.45-4.55-5.17-10.5-5.17-17.83,0-10.89,3.94-19.42,11.83-25.58,7.89-6.17,18.17-9.25,30.83-9.25s23.28,3.08,31.17,9.25c7.89,6.17,12.11,14.75,12.67,25.75h-30.83c-.22-3.78-1.61-6.75-4.17-8.92-2.56-2.17-5.83-3.25-9.83-3.25-3.45,0-6.22.92-8.33,2.75-2.11,1.83-3.17,4.47-3.17,7.92,0,3.78,1.78,6.72,5.33,8.83,3.55,2.11,9.11,4.39,16.67,6.83,7.55,2.56,13.69,5,18.42,7.33,4.72,2.33,8.8,5.72,12.25,10.17,3.44,4.45,5.17,10.17,5.17,17.17s-1.7,12.72-5.08,18.17c-3.39,5.45-8.31,9.78-14.75,13-6.45,3.22-14.06,4.83-22.83,4.83s-16.22-1.39-23-4.17Z"></path>
                                       <rect class="cls-1ng" x="343.41" y="75.29" width="71.69" height="22.79"></rect>
                                       <rect class="cls-1ng" x="343.41" y="121.79" width="71.69" height="22.79"></rect>
                                       <rect class="cls-1ng" x="343.41" y="169.5" width="71.69" height="22.79"></rect>
                                    </g>
                                 </g>
                                 <g>
                                    <path class="cls-3ng" d="m76.03.47L13.55,16.04C5.96,17.93,0,28.08,0,38.78v147.74c0,.3,0,.61,0,.91v23.78c0,10.7,5.96,20.86,13.55,22.75l62.48,15.56c10.52,2.63,19.39-5.93,19.39-19.22v-112.1c0-.25,0-.5,0-.75V19.7c0-13.29-8.88-21.85-19.4-19.22Zm-34.9,227.64c-2.77-1.31-4.37-4.32-4.37-7.13s1.61-5.83,4.37-7.13c4.4-2.08,10.41.47,10.41,7.13s-6.01,9.22-10.41,7.13ZM54.19,23.79l-19.12,4.39c-1.93.44-3.86-.76-4.3-2.69-.06-.27-.09-.54-.09-.81,0-1.63,1.12-3.11,2.78-3.49l19.13-4.39c1.92-.45,3.85.76,4.29,2.69.07.27.09.54.09.81,0,1.63-1.12,3.11-2.78,3.49Z"></path>
                                    <path class="cls-4ng" d="m166.8,83.52l-44.46-11.07c-7.48-1.87-13.8,4.23-13.8,13.68v149.86c0,9.46,6.32,15.55,13.8,13.68l44.46-11.07c5.41-1.35,9.64-8.57,9.64-16.18v-122.7c0-7.61-4.23-14.84-9.64-16.18Zm-19.62,150.91c-3.13,1.47-7.41-.34-7.41-5.08s4.27-6.56,7.41-5.08c1.97.93,3.11,3.08,3.11,5.08s-1.14,4.14-3.11,5.08Zm6.79-144.13c-.33,1.31-1.65,2.11-2.96,1.79l-12.66-2.64c-1.5-.42-2.2-1.68-2.2-2.85,0-.23.03-.46.08-.68.32-1.32,1.65-2.12,2.96-1.79l12.98,3.21c1.12.28,1.87,1.27,1.87,2.37,0,.2-.03.39-.08.59Z"></path>
                                    <g>
                                       <path class="cls-2ng" d="m163.94,67.4h0c-.95,1.77-2.97,2.67-4.92,2.18-1.95-.48-3.32-2.22-3.34-4.23.96-3.86,1.37-7.85,1.22-11.85-.25-7.01-2.24-14.05-6.12-20.47-.17-.27-.33-.54-.51-.81-.25-.41-.52-.81-.79-1.2-.38-.57-.78-1.12-1.19-1.66-.09-.12-.18-.23-.27-.35-.22-.28-.43-.55-.66-.82-2.05-2.55-4.37-4.81-6.87-6.76-.3-.24-.6-.48-.91-.69-.43-.32-.85-.62-1.28-.92-1.09-.75-2.21-1.44-3.35-2.07-3.35-1.87-6.9-3.26-10.53-4.15-3.93-.97-7.96-1.37-11.97-1.21-1.96-.42-3.39-2.12-3.47-4.12v-.18c0-1.93,1.26-3.64,3.12-4.21,9.06-.36,18.22,1.67,26.45,6.12.54.29,1.08.59,1.62.91.36.21.71.43,1.06.64.35.22.7.44,1.05.67,2.31,1.53,4.53,3.25,6.63,5.19,1.68,1.56,3.25,3.2,4.68,4.92.22.27.43.54.65.81.2.25.4.5.59.75.33.43.65.86.96,1.29.25.34.49.69.73,1.04.24.35.47.71.7,1.06.23.36.46.72.67,1.07.16.25.31.51.46.77.36.61.71,1.22,1.03,1.84.32.59.62,1.19.92,1.8.18.38.36.75.53,1.13.17.38.34.76.5,1.14.17.38.32.77.48,1.15.27.68.52,1.35.76,2.03,3.37,9.57,3.71,19.71,1.38,29.18Z"></path>
                                       <g>
                                          <path class="cls-2ng" d="m116.46,56.27c-.03-.25-.08-.5-.16-.74,0-.02,0-.02,0-.04-.02-.07-.04-.13-.07-.18,0-.05-.03-.1-.05-.14,0,0,0-.03-.02-.03,0,0,0,0,0-.02-.04-.11-.09-.2-.15-.3,0-.03-.02-.04-.03-.07-.05-.09-.12-.19-.18-.28,0-.02-.02-.05-.04-.07-.03-.04-.06-.09-.09-.13-.04-.04-.08-.08-.12-.12,0,0,0-.02-.03-.03,0,0-.02-.03-.03-.04-.08-.08-.15-.16-.23-.23-.02-.03-.05-.05-.07-.08-.06-.05-.12-.1-.18-.15,0,0-.02-.02-.03-.03l-.3-.2s-.04-.03-.06-.04c-.04-.03-.09-.05-.13-.08-.04-.02-.08-.03-.13-.05h0s0-.02,0-.02c-.02,0-.05-.03-.07-.03-.08-.04-.18-.08-.27-.12-.02,0-.03,0-.04,0-.04-.02-.09-.03-.13-.04-.06-.02-.11-.02-.17-.04s-.12-.03-.19-.05c-.03-.02-.08-.03-.12-.03h-.02l-.18-.03s-.1-.02-.15-.02c-.07,0-.13,0-.19,0h-.54c-.13.02-.25.03-.38.07-.07,0-.16.03-.23.05-.03,0-.05.02-.08.03-.02,0-.06.02-.09.02h-.02c-.11.04-.21.08-.32.13-.02,0-.04.02-.06.03,0,0-.02,0-.02,0-.09.04-.18.09-.28.14h0c-.11.07-.21.13-.31.2-.02,0-.02,0-.03.03-.04.02-.08.05-.13.08,0,0-.02.02-.03.03-.04.03-.08.06-.13.11,0,0-.02,0-.03.03-.05.03-.09.07-.13.12-.03.03-.07.06-.09.09-.02.02-.04.04-.07.07-.03.02-.05.05-.08.08-.07.08-.13.17-.2.25-.05.07-.1.15-.15.23-.02.02-.03.03-.03.05-.02.02-.03.06-.05.08-.03.05-.06.11-.08.17-.03.03-.03.04-.03.07-.04.08-.08.16-.11.23,0,0,0,.02,0,.03-.03.07-.07.16-.08.23-.02.04-.03.07-.04.12-.04.12-.07.25-.09.38,0,.07-.02.13-.02.21,0,0,0,.03,0,.04,0,.02,0,.03,0,.04,0,.05-.02.11,0,.16-.03.2-.02.4,0,.6,0,.05,0,.09,0,.13,0,.03,0,.08.02.11,0,.03.02.07.03.1,0,.1.02.19.05.28,0,.03.02.04.02.07.03.12.08.25.12.37.04.09.08.17.13.27,0,.02,0,.03.02.04.02.04.04.09.07.13,0,.02.02.03.03.06,0,.02.03.05.05.07,0,0,.02.03.03.04s.02.04.04.06c.03.07.07.12.12.18.02.03.03.05.06.07,0,0,0,.02,0,.02.04.06.08.11.13.16.03.05.08.09.12.13.02.03.04.04.06.07,0,0,.02.02.03.03.03.03.05.05.08.07.06.06.11.11.17.16.03.03.06.05.08.07.02.03.05.04.07.06.08.06.15.11.23.15.06.04.13.08.19.12,0,0,0,0,0,0,.06.03.11.07.17.09l.18.08s.06.03.08.04c.09.03.18.07.28.09.05.03.1.04.15.05.05.02.1.03.16.03.02.02.04.02.07.02.03,0,.08.02.11.02.08.02.17.03.25.04.03,0,.07,0,.1,0,.12.02.24.02.37.02.09,0,.19,0,.29,0,.08,0,.17-.02.26-.03.1-.02.2-.04.3-.07.09-.03.19-.06.29-.09,0,0,.02,0,.04,0h0c.12-.04.22-.09.33-.14.08-.03.15-.07.22-.1.11-.05.21-.11.3-.17,0,0,.02,0,.03-.02.02,0,.02-.02.04-.02.03-.03.08-.05.11-.08,0,0,0,0,.02,0,.02-.02.04-.03.06-.05,0,0,.02,0,.03-.03.1-.07.19-.15.28-.23.02-.02.03-.03.04-.04.09-.09.18-.18.26-.28.1-.12.19-.24.28-.37.02-.03.05-.08.07-.11.04-.05.07-.11.1-.17.02-.03.04-.07.06-.11.13-.26.23-.53.3-.82,0,0,0-.03,0-.04.08-.34.11-.7.08-1.06,0-.07,0-.13,0-.2Z"></path>
                                          <path class="cls-2ng" d="m147.39,63.31c-.95,1.77-2.97,2.67-4.93,2.18-1.95-.48-3.32-2.22-3.34-4.23.57-2.32.83-4.71.73-7.12,0-.2-.02-.39-.03-.59-.23-4.02-1.43-8.03-3.64-11.72-.96-1.58-2.05-3.03-3.27-4.31-.5-.55-1.03-1.07-1.58-1.54-.1-.1-.21-.19-.31-.28-.3-.27-.61-.53-.92-.77-.14-.11-.28-.22-.42-.31,0-.02,0-.02-.02-.02,0-.02-.02-.02-.03-.02-.16-.13-.32-.25-.48-.36,0,0,0,0,0,0-.16-.12-.32-.23-.48-.33-.12-.11-.26-.19-.39-.28-2.51-1.67-5.23-2.83-8.04-3.52-2.37-.58-4.82-.82-7.24-.71-.19,0-.39,0-.58.02-1.97-.37-3.45-2.02-3.58-4.02-.13-2.01,1.11-3.84,3.01-4.48,5.91-.4,11.91.74,17.38,3.45.04.02.08.03.11.05,0,0,.02,0,.02.02.25.13.49.25.74.38.34.17.68.37,1.02.56.34.2.67.4,1.02.61.23.14.45.28.68.43.44.29.89.59,1.32.9,0,0,.02,0,.02.02.21.15.41.3.61.45l.02.02c.62.48,1.24.98,1.85,1.5.24.21.48.43.72.65.22.2.43.4.64.61.93.89,1.79,1.83,2.59,2.81.27.33.53.66.78.99.17.23.34.46.5.68.17.23.33.46.49.69.21.3.41.6.61.91,0,.03.02.04.03.06.38.59.74,1.19,1.08,1.8.18.32.34.63.51.95.14.28.28.56.42.84.14.28.27.57.4.85.13.28.25.57.37.86,2.9,6.91,3.33,14.38,1.63,21.32Z"></path>
                                       </g>
                                       <path class="cls-2ng" d="m130.85,59.22c-.95,1.77-2.98,2.67-4.93,2.18-1.94-.48-3.32-2.22-3.34-4.23.19-.75.28-1.53.25-2.32v-.14c0-.15-.02-.3-.03-.45-.12-1.24-.52-2.48-1.2-3.62-.54-.9-1.22-1.65-1.98-2.27-.03-.02-.07-.05-.1-.07-.05-.04-.09-.08-.13-.1-.07-.06-.14-.11-.21-.16,0,0-.02,0-.03-.02-.08-.07-.17-.12-.26-.17-.23-.17-.48-.33-.72-.44-.71-.42-1.46-.7-2.22-.88-.87-.21-1.75-.28-2.62-.22-.06,0-.12.02-.17.03-.14,0-.28.03-.42.04-2-.22-3.59-1.76-3.88-3.74-.28-1.99.81-3.92,2.66-4.69,4.02-.58,8.25.27,11.86,2.65.24.15.48.32.72.49-.08-.06-.17-.12-.25-.17.17.12.35.24.53.38.2.14.38.29.57.45.07.05.15.11.22.18.18.15.36.31.53.47-.1-.1-.19-.19-.29-.28.15.13.3.28.44.41,0,0,.01,0,.01.01,0,0,.01,0,.02.01,1.14,1.05,2.16,2.28,3,3.68,2.45,4.06,3.02,8.72,1.97,12.98Z"></path>
                                    </g>
                                 </g>
                              </g>
                           </g>
                        </svg>
                     </div>
                    
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50 text-center">
                  <div class="nexusgpt-pre-heading f-md-22 f-18 w500 lh140">
                  Exploit The NFC &amp; Ai Tech to <u>Make Multiple Recurring Income Streams </u>  
                  </div>
               </div>
               <div class="col-12 mt80 nexusgpt-head-design relative">
                  <div class="nexusgpt-gametext">
                     First-to-JVZoo Technology 
                  </div>
                  <div class=" f-md-40 f-28 w400 text-center black-clr lh140">
                     <span class="w600">Super-Easy NFC App Creates Contactless Digital Business Cards with Ai-Assistant, </span> Generates Leads, Followers, Reviews &amp; Sales  <span class="w600 underline-text2">with Just One Touch.</span>
                  </div>
               </div>
               <div class="col-12 mt-md40 mt20 f-20 f-md-24 w400 text-center lh140 white-clr text-capitalize">
               Easily Create &amp; Sell Contactless NFC Digital Cards &amp; GPT-Ai Assistants for Big Profits to Dentists, Attorney, Gyms, Spas, Restaurants, Cafes, Coaches, Affiliates &amp; 100+ Other Niches... 
               </div>
               <div class="col-12 mt20 f-20 f-md-24 w500 text-center lh140 orange-clr text-capitalize">
               No Tech Skills of Any Kind Needed. No Monthly Fee Ever. 
               </div>
            </div>
            <div class="row mt20 mt-md50 align-items-center">
               <div class="col-md-8 col-12">
                  <div class="col-12">
                     <div class="video-box">
                        <div style="padding-bottom: 56.25%;position: relative;"><iframe src="https://nexusgpt.oppyo.com/video/embed/e786pt8zf4" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe></div>
                     </div>
                  </div>
               </div>
               <div class="col-md-4 col-12 mt20 mt-md0">
                  <div class="">
                     <ul class="nexusgpt-list-head pl0 m0 f-18 lh140 w400 white-clr">
                        <li><span class="w600">Nexus Of NFC Tech &amp; GPT AI</span> Revolutionise Your Marketing</li>
                        <li><span class="w600">Help Desperate Local Businesses In Any Niche</span> within Minutes</li>
                        <li><span class="w600">GPT AI Assistant Closes Leads &amp; Sales 24x7</span> on Automation</li>
                        <li><span class="w600">Impress Your Colleagues &amp; Client’s</span> with Contactless NFC card</li>
                        <li><span class="w600">Tons of Ready-To-Go Templates</span> with Free-Flow Editor</li>
                        <li>Sell High In-Demand Services with <span class="w600">Included Commercial License.</span> </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="gr-1">
         <div class="container-fluid p0">
            <picture>
               <source media="(min-width:768px)" srcset="https://cdn.dotcompaltest.com/uploads/launches/mavas/webinar-gift/nexusgpt.webp">
               <source media="(min-width:320px)" srcset="https://cdn.dotcompaltest.com/uploads/launches/mavas/webinar-gift/nexusgpt-mview.webp" style="width:100%" class="vidvee-mview">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/webinar-gift/nexusgpt.webp" alt="" class="img-fluid" style="width: 100%;">
            </picture>
            <div class="f-md-35 f-15 w700 lh140 text-center mb-md30 ">
            Just submit a ticket through our support desk to claim <br><a href="https://support.oppyo.com/hc/en-us" class="a-link"> Nexus GPT as a complimentary webinar gift.</a>
            </div>
         </div>
      </div>
      <div class="blue-section">
        <div class="container">
			<div class="row mt20 mt-md100 align-items-center">
               	<div class="col-md-6">
                  	<div class="f-22 f-md-28 lh140 w700 white-clr">
                     	<div class="option-design mr10 mr-md20">Bonus 1</div> 
                     	
                  	</div>
                     <div class="f-20 f-md-32 w600 lh140 white-clr mt20 mt-md30">
					 Web Security Graphics 

                  	</div>
                  	
                  	<div class="f-20 f-md-22 lh140 white-clr mt20 w400">
					  <span>Many people process information better when it is presented in a visual manner. Use these web security graphics to add appeal to your blog posts, marketing, PLR content and so much more.</span>
					  <br><br>
					  According to many Neuro-Linguistic Programming practitioners, human minds works visually in which our imagination, calculations and memories takes place.
                  	</div>
                 	
					<div class="f-md-22 w700 lh160 d-btn mt20 mt-md30">
                       <a href="https://tinyurl.com/3326hmkb" target="_blank">Download Now 🎁</a>
                    </div>
               	</div>
               	<div class="col-md-6 mt20 mt-md0">
                  	<img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/webinar-gift/b4.webp" alt="Bonus 1" class="mx-auto d-block img-fluid">
               	</div>
            </div>

			<div class="row mt20 mt-md100 align-items-center">
               	<div class="col-md-6 order-md-2">
                  	<div class="f-22 f-md-28 lh140 w700 white-clr">
                     	<div class="option-design mr10 mr-md20">Bonus 2</div> 
                     
                  	</div>
                  	<div class="f-20 f-md-32 w600 lh140 white-clr mt20 mt-md30">
                     100 Video Transition Backgrounds
                                    	</div>
                 	<div class="f-20 f-md-22 w400 lh140 white-clr mt20">
                   Nice PRO Looking Transition Backgrounds For Your Videos
				   <br><br>
				   Are you looking for video that could be a transition backgrounds that are easy to use? Transition background that you could could use for your video edition? Then, download this one!
                  	</div>
					<div class="f-md-22 w700 lh160 d-btn mt20 mt-md30">
                       <a href="https://tinyurl.com/488534vp" target="_blank">Download Now 🎁</a>
                    </div>
               	</div>
               	<div class="col-md-6 mt20 mt-md0 order-md-1">
                  	<img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/webinar-gift/video-gift.webp" alt="Bonus 2" class="mx-auto d-block img-fluid">
               	</div>
            </div>

			<div class="row mt20 mt-md100 align-items-center">
               	<div class="col-md-6">
                  	<div class="f-22 f-md-28 lh140 w700 white-clr">
                     	<div class="option-design mr10 mr-md20">Bonus 3</div> <br>
                        
                  	</div>
                     <div class="f-20 f-md-32 w600 lh140 white-clr mt20 mt-md30">
					 Money Graphics Pack 

                  	</div>
                  	
                 	<div class="f-20 f-md-22 w400 lh140 white-clr mt20">
                    <span class="w400">This graphics pack has 150 money related images. The images will be delivered in PNG and Vector PDF format! 
</span>
					<br><br>
					If you are an online entrepreneur selling digital products, a blogger, affiliate marketer, freelance web graphics designer having a pre-made graphics that are relevant to your needs or your client's needs will give you more time to other things that will make your more productive.
                           </div>
					<div class="f-md-22 w700 lh160 d-btn mt20 mt-md30">
                       <a href="https://tinyurl.com/mvkf5fy4" target="_blank">Download Now 🎁</a>
                    </div>
               	</div>
               	<div class="col-md-6 mt20 mt-md0">
                  	<img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/webinar-gift/b3.webp" alt="Bonus 3" class="mx-auto d-block img-fluid">
               	</div>
            </div>

			<div class="row mt20 mt-md100 align-items-center">
               	<div class="col-md-6 order-md-2">
                  	<div class="f-22 f-md-28 lh140 w700 white-clr">
                     	<div class="option-design mr10 mr-md20">Bonus 4</div> <br>
                     	
                  	</div>
                  	<div class="f-20 f-md-32 w600 lh140 white-clr mt20 mt-md30">
					  50 Adsense Ready Wordpress Templates 

                  	</div>
                 	<div class="f-20 f-md-22 w400 lh140 white-clr mt20">
					 Finally, All Your Niche Wordpress Blogs Can Look Cool & Different - You Can Easily Spice Up Your Niche Wordpress Blogs, Giving Them A Unique & More Responsive Look!
<br><br>
The look and feel of your blog matters to your blog vistors. Not only are they interested in valuable information you provide, but the presentation and overall look of your blog will also help to make them repeat visitors as well as more responsive to any offers or ads you may have on your blogs.
                  	</div>
					<div class="f-md-22 w700 lh160 d-btn mt20 mt-md30">
                       <a href="https://tinyurl.com/4yaz9as9" target="_blank">Download Now 🎁</a>
                    </div>
               	</div>
               	<div class="col-md-6 mt20 mt-md0 order-md-1">
                  	<img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/webinar-gift/b4.webp" alt="Bonus 4" class="mx-auto d-block img-fluid">
               	</div>
            </div>

			<div class="row mt20 mt-md100 align-items-center">
               	<div class="col-md-6">
                  	<div class="f-22 f-md-28 lh140 w700 white-clr">
                     	<div class="option-design mr10 mr-md20">Bonus 5</div> <br>
                     	
                  	</div>
                     <div class="f-20 f-md-32 w600 lh140 white-clr mt20 mt-md30">
                     Premium Presentation Template 

                  	</div>
                  	
                 	<div class="f-20 f-md-22 w400 lh140 white-clr mt20">
					 <span class="w400">Are Tired Of Your Presentation Not Converting Or Getting Attention? No Designs Skills, Photoshop Or Expensive Complicated Software Needed!</span><br><br>

					 Having a lot of traffic or leads to your business is nothing if it won't convert into paying customers. The thing is that, these people tend to judge your business model, services or opportunity depending how you present to them.
                  	</div>
					<div class="f-md-22 w700 lh160 d-btn mt20 mt-md30">
                       <a href="https://tinyurl.com/4dm4h5xs" target="_blank">Download Now 🎁</a>
                    </div>
               	</div>
               	<div class="col-md-6 mt20 mt-md0">
                  	<img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/webinar-gift/b5.webp" alt="Bonus 5" class="mx-auto d-block img-fluid">
               	</div>
            </div>
			<div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center mt20 mt-md70">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 purple-clr">"MAVAS3"</span> for an Additional <span class="w700 purple-clr">$3 Discount</span> on MAVAS
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn px-md80">
                  <span class="text-center">Grab MAVAS + 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                  Use Coupon Code <span class="w700 purple-clr">"MAVAS"</span> for an Additional <span class="w700 purple-clr">$50 Discount</span> on Bundle Deal
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="https://getmavas.com/bundle/" class="text-center bonusbuy-btn px-md80">
                  <span class="text-center">Grab MAVAS Bundle + 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/webinar-gift/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
            </div>
            </div>
      </div>
	<!-- Proudly Section End -->
 <!-- CTA Btn Start-->

      <!-- CTA Btn End -->


	<!--Footer Section Start -->
	<div class="footer-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
			   <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 685 120" style="max-height:55px">
  <defs>
    <style>
      .cls-1 {
        fill: url(#linear-gradient-2);
      }

      .cls-2 {
        fill: #fff;
      }

      .cls-3 {
        fill: #21e5f4;
      }

      .cls-4 {
        fill: url(#linear-gradient);
      }
    </style>
    <linearGradient id="linear-gradient" x1="0" y1="60" x2="25.31" y2="60" gradientUnits="userSpaceOnUse">
      <stop offset="0" stop-color="#b956ff"></stop>
      <stop offset="1" stop-color="#8d4ef9"></stop>
    </linearGradient>
    <linearGradient id="linear-gradient-2" x1="99.25" y1="59.94" x2="124.57" y2="59.94" gradientUnits="userSpaceOnUse">
      <stop offset="0" stop-color="#8d57f4"></stop>
      <stop offset="1" stop-color="#6e33ff"></stop>
    </linearGradient>
  </defs>
  <g id="Layer_1-2" data-name="Layer 1">
    <g>
      <g>
        <g>
          <polygon class="cls-2" points="343.95 66.06 343.95 66.06 300 .53 273.46 .53 343.96 99.29 355.45 83.2 343.95 66.06"></polygon>
          <polygon class="cls-2" points="367.54 66.26 414.45 .53 388.11 .53 355.71 48.61 367.54 66.26"></polygon>
        </g>
        <polygon class="cls-2" points="218.06 0 147.57 99.82 172.51 99.82 182.71 84.58 198.02 61.87 218.07 32.36 237.91 61.88 253.21 84.59 263.4 99.82 288.55 99.82 218.06 0"></polygon>
        <polygon class="cls-2" points="469.86 0 399.36 99.82 424.31 99.82 434.5 84.58 449.81 61.87 469.87 32.36 489.7 61.88 505.01 84.59 515.2 99.82 540.35 99.82 469.86 0"></polygon>
        <g>
          <path class="cls-2" d="m662.66,46.82l-96.97-14.26,16.04,21.03,5.2.9,69.12,11.61c3.34.56,5.78,3.45,5.78,6.83,0,3.83-3.1,6.93-6.93,6.93h-75.79l-15.37,20.13,94.46.02c14.79,0,26.78-11.99,26.78-26.78,0-13.07-9.43-24.22-22.31-26.4Z"></path>
          <polygon class="cls-2" points="685 1.08 589.71 1.08 589.71 1.06 581.89 1.06 566.5 21.21 669.63 21.21 685 1.08"></polygon>
        </g>
      </g>
      <g>
        <polygon class="cls-4" points="25.31 20.82 25.31 120 0 99.11 0 0 25.31 20.82"></polygon>
        <polygon class="cls-1" points="124.57 0 124.57 99.13 99.25 119.89 99.25 20.76 124.57 0"></polygon>
        <polygon class="cls-3" points="92.74 25.17 92.74 62.88 62.24 100.89 31.82 62.89 31.82 25.17 62.24 63.17 92.74 25.17"></polygon>
      </g>
    </g>
  </g>
</svg>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-md-18 f-16 w400 lh140 white-clr text-xs-center">Copyright © MAVAS 2023</div>
                  <ul class="footer-ul w400 f-md-18 f-16 white-clr text-center text-md-right">
                     <li><a href="https://support.oppyo.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://getmavas.com/legal/privacy-policy.html " class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://getmavas.com/legal/terms-of-service.html " class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://getmavas.com/legal/disclaimer.html " class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://getmavas.com/legal/gdpr.html " class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://getmavas.com/legal/dmca.html " class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://getmavas.com/legal/anti-spam.html " class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
    <!--Footer Section End -->


</body>
</html>

<!Doctype html>
<html>

<head>
   <title>Mavas Prelaunch</title>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=9">
   <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
   <meta name="title" content="Mavas | Prelaunch">
   <meta name="description" content="Supercharge Your Website and Boost Engagement, Conversions, and Sales to Make All-Time Big Profits in Your Business ">
   <meta name="keywords" content="Mavas">
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   <meta property="og:image" content="https://www.getmavas.com/prelaunch/thumbnail.png">
   <meta name="language" content="English">
   <meta name="revisit-after" content="1 days">
   <meta name="author" content="Dr. Amit Pareek">
   <!-- Open Graph / Facebook -->
   <meta property="og:type" content="website">
   <meta property="og:title" content="Mavas | Prelaunch">
   <meta property="og:description" content="Supercharge Your Website and Boost Engagement, Conversions, and Sales to Make All-Time Big Profits in Your Business ">
   <meta property="og:image" content="https://www.getmavas.com/prelaunch/thumbnail.png">
   <!-- Twitter -->
   <meta property="twitter:card" content="summary_large_image">
   <meta property="twitter:title" content="Mavas | Prelaunch">
   <meta property="twitter:description" content="Supercharge Your Website and Boost Engagement, Conversions, and Sales to Make All-Time Big Profits in Your Business ">
   <meta property="twitter:image" content="https://www.getmavas.com/prelaunch/thumbnail.png">
   <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
   <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
   <!-- Start Editor required -->
   <link rel="stylesheet" href="../common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style.css" type="text/css">
      <link rel="stylesheet" href="assets/css/aweberform.css" type="text/css">
   
</head>

<body> 
   <!-- Header Section Start -->
   <div class="header-section">
      <div class="container">
         <div class="row">
            <div class="col-12 col-12 d-flex align-items-center justify-content-center justify-content-md-between flex-wrap flex-md-nowrap">           
            <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 685 120" style="max-height:55px">
                     <defs>
                       <style>
                         .cls-1t {
                           fill: url(#linear-gradient-2);
                         }
                   
                         .cls-2t {
                           fill: #fff;
                         }
                   
                         .cls-3t {
                           fill: #21e5f4;
                         }
                   
                         .cls-4t {
                           fill: url(#linear-gradient);
                         }
                       </style>
                       <linearGradient id="linear-gradient" x1="0" y1="60" x2="25.31" y2="60" gradientUnits="userSpaceOnUse">
                         <stop offset="0" stop-color="#b956ff"></stop>
                         <stop offset="1" stop-color="#8d4ef9"></stop>
                       </linearGradient>
                       <linearGradient id="linear-gradient-2" x1="99.25" y1="59.94" x2="124.57" y2="59.94" gradientUnits="userSpaceOnUse">
                         <stop offset="0" stop-color="#8d57f4"></stop>
                         <stop offset="1" stop-color="#6e33ff"></stop>
                       </linearGradient>
                     </defs>
                     <g id="Layer_1-2" data-name="Layer 1">
                       <g>
                         <g>
                           <g>
                             <polygon class="cls-2t" points="343.95 66.06 343.95 66.06 300 .53 273.46 .53 343.96 99.29 355.45 83.2 343.95 66.06"></polygon>
                             <polygon class="cls-2t" points="367.54 66.26 414.45 .53 388.11 .53 355.71 48.61 367.54 66.26"></polygon>
                           </g>
                           <polygon class="cls-2t" points="218.06 0 147.57 99.82 172.51 99.82 182.71 84.58 198.02 61.87 218.07 32.36 237.91 61.88 253.21 84.59 263.4 99.82 288.55 99.82 218.06 0"></polygon>
                           <polygon class="cls-2t" points="469.86 0 399.36 99.82 424.31 99.82 434.5 84.58 449.81 61.87 469.87 32.36 489.7 61.88 505.01 84.59 515.2 99.82 540.35 99.82 469.86 0"></polygon>
                           <g>
                             <path class="cls-2t" d="m662.66,46.82l-96.97-14.26,16.04,21.03,5.2.9,69.12,11.61c3.34.56,5.78,3.45,5.78,6.83,0,3.83-3.1,6.93-6.93,6.93h-75.79l-15.37,20.13,94.46.02c14.79,0,26.78-11.99,26.78-26.78,0-13.07-9.43-24.22-22.31-26.4Z"></path>
                             <polygon class="cls-2t" points="685 1.08 589.71 1.08 589.71 1.06 581.89 1.06 566.5 21.21 669.63 21.21 685 1.08"></polygon>
                           </g>
                         </g>
                         <g>
                           <polygon class="cls-4t" points="25.31 20.82 25.31 120 0 99.11 0 0 25.31 20.82"></polygon>
                           <polygon class="cls-1t" points="124.57 0 124.57 99.13 99.25 119.89 99.25 20.76 124.57 0"></polygon>
                           <polygon class="cls-3t" points="92.74 25.17 92.74 62.88 62.24 100.89 31.82 62.89 31.82 25.17 62.24 63.17 92.74 25.17"></polygon>
                               </g>
                                </g>
                                 </g>
                                  </svg>
               <div class=" mt20 mt-md5">
                  <ul class="leader-ul f-18 f-md-20 white-clr text-md-end text-center">
                     <li class="affiliate-link-btn"><a href="#book-seat" class="t-decoration-none ">Book Your Free Seat</a></li>
                  </ul>
               </div>
            </div>
            
            <div class="col-12 mt30 mt-md50 text-center">
               <div class="preheadline f-18 f-md-22 w600 white-clr lh140">
                  <div class="preheadline-content white-clr ">
                  Register For This Value Packed Training; <span class="neon-clr"> All Attendees Receive FREE Gifts</span>
                  </div>
               </div>
            </div>
            <!-- <div class="col-12 mt20 mt-md30">
               <div class="f-md-22 f-18 w700 text-center lh140 white-clr">
               [🎯Unlock Your TRUE Income Potential] Learn How to Make $2K Weekly Using Amazon S3's Rental Strategy!"
               </div>
            </div> -->
            <div class="col-12 mt20 mt-md50 relative">
                  <div class="mainheadline f-md-40 f-28 w700 text-center white-clr lh140">
                  <div class="headline-text">
                     First-To-Market <span class="w700 sky-blue neon-clr">#1 Super VA That Finish Your 100s Type of Marketing Tasks <span class="caveat">(Better &amp; Faster Than Any CMO Out There...)</span> </span> AND Trim Your Workload, Hiring/Firing Headaches, &amp; Monthly Bills </div>
                  </div>
               </div>
            <div class="col-12 mt-md50 mt20 f-18 f-md-22 w400 text-center lh170 white-clr text-capitalize">
               <div class="postheadline">
               In This Free Training, we will also reveal, how you can Dominate on Social-Media, Drive Website Traffic, Create Ads, Optimize SEO, Generate Quality Leads, Create Compelling Content, and <span class="w700">Much More with MAVAS</span>
               </div>
            </div>
            </div>
         
         <div class="row d-flex align-items-center flex-wrap mt20 mt-md80">
            <div class="col-md-7 col-12 min-md-video-width-left">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/prelaunch/webinar-image.webp" class="img-fluid d-block mx-auto" alt="Prelaunch">
            </div>
            <div class="col-md-5 col-12 mt20 mt-md0 min-md-video-width-right" id="form">
               <div class="auto_responder_form inp-phold calendar-wrap" id="book-seat">
                  <div class="f">
                     <div class="f-20 f-md-24 w800 text-center lh180 orange-clr" editabletype="text" style="z-index:10;">
                        Register for Free Training <br>
                        <span class="f-20 f-md-24 w700 text-center black-clr" contenteditable="false">25<sup contenteditable="false">th</sup> October, 10 AM EST</span>
                     </div>
                     <div class="col-12 f-18 f-md-20 w500 lh140 text-center">
                        Book Your Seat now<br> (Limited to 100 Users)
                     </div>
                     <!-- Aweber Form Code -->
                     <div class="col-12 p0 mt15 mt-md20">
                        <form method="post" class="af-form-wrapper register-form-style1 " accept-charset="UTF-8" action="https://www.aweber.com/scripts/addlead.pl" style="z-index: 10;">
                           <div style="display: none;">
                              <input type="hidden" name="meta_web_form_id" value="448889756" />
                              <input type="hidden" name="meta_split_id" value="" />
                              <input type="hidden" name="listname" value="awlist6641005" />
                              <input type="hidden" name="redirect" value="https://www.getmavas.com/prelaunch-thankyou" id="redirect_d5af24ac5b16e3f69ea0226b05d60951" />
                              <input type="hidden" name="meta_adtracking" value="My_Web_Form" />
                              <input type="hidden" name="meta_message" value="1" />
                              <input type="hidden" name="meta_required" value="name,email" />
                              <input type="hidden" name="meta_tooltip" value="" />
                           </div>
                           <div id="af-form-448889756" class="af-form">
                              <div id="af-body-448889756" class="af-body af-standards">
                                 <div class="af-element width-form1">
                                    <label class="previewLabel" for="awf_field-116368966-first"></label>
                                    <div class="af-textWrap">
                                       <div class="input-type" style="z-index: 11;">
                                          <!--<span class="input-group-addon border1px"><i class="fa fa-user" aria-hidden="true"></i></span>-->
                                          <input id="awf_field-116368966-first" type="text" name="name" class="text form-control custom-input input-field" value="" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="500" placeholder="Your Name" />
                                       </div>
                                    </div>
                                    <div class="af-clear bottom-margin"></div>
                                 </div>

                                 
                                 <div class="af-element width-form1">
                                    <label class="previewLabel" for="awf_field-116368967"> </label>
                                    <div class="af-textWrap">
                                       <div class="input-type" style="z-index: 11;">
                                          <!--<span class="input-group-addon border1px"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>-->
                                          <input class="text form-control custom-input input-field" id="awf_field-116368967" type="text" name="email" value="" tabindex="501" onFocus=" if (this.value == '') { this.value = ''; }" onBlur="if (this.value == '') { this.value='';} " placeholder="Your Email" />
                                       </div>
                                    </div>
                                    <div class="af-clear bottom-margin"></div>
                                 </div>
                                 <input type="hidden" id="awf_field_aid" class="text" name="custom aid" value="" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="502" />
                                 <div class="af-element buttonContainer button-type register-btn1 f-22 f-md-21 mt-md0">
                                    <input name="submit" id="af-submit-image-348552691" type="submit" class="image" style="max-width: 100%;" alt="Submit Form" tabindex="503" value="Book Your Free Seat" />
                                    <div class="af-clear bottom-margin"></div>
                                 </div>
                              </div>
                           </div>
                           <div style="display: none;"><img src="https://forms.aweber.com/form/displays.htm?id=LCwcHByc7Kxs" alt="Form" /></div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Header Section End -->
   <!-- Header Section Start -->
   <div class="second-section">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="col-12">
                  <div class="row">
                     <div class="col-12">
                        <div class="f-md-36 f-24 w800 lh140 text-capitalize text-center gradient-clr">
                        Why Attend This Free Training Webinar?
                        </div>
                        <div class="f-20 f-md-22 w600 lh150 text-capitalize text-center black-clr mt20">
                        Our 19+ Years Of Experience Is Packaged In This State-Of-Art 1-Hour Webinar Where You’ll Learn:
                        </div>
                     </div>
                     <div class="col-12 mt20 mt-md50">
                        <ul class="features-list pl0 f-18 f-md-20 lh150 w400 black-clr text-capitalize">
                           <li>How We Have Made Over $9 Million by Selling & Securely Using the Power of MAVAS. and  <span class="w700">You Can Follow The Same To Build A Profitable Business Online. </span></li>
                           <li>How you can Run Your Business WITHOUT Any Expenses or Hassles. ZERO Salaries. ZERO Freelancer/Agency Fee</li>
                           <li>How We Have Earned 3000s Dollars Monthly Using MAVAS & You can Follow the same For Your Clients & Charge Them PREMIUM Fee </li>
                           <li>Get Paid a PREMIUM Fee for services rendered by MAVAS without incurring any expenses in salaries. Keep 100% of the profits. </li>
                           <li>A Sneak-Peak Of "MAVAS", First-To-Market #1 Super VA That Finish Your 100s Type of Marketing Tasks (Better & Faster Than Any CMO Out There...) </li>
                           <li>During This Launch Special Deal, Get All Benefits at A Limited Low One-Time-Fee. </li>
                           <li>Lastly, Everyone Who Attends This Session Will Get an Assured Gift from Us. </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>

         <div class="row mt20 mt-md70">
            <div class="col-12 col-md-12 mx-auto" id="form">
               <div class="auto_responder_form inp-phold formbg" id="book-seat1">
                  <div class="f-24 f-md-40 w800 text-center lh160 white-clr" editabletype="text" style="z-index:10;">
                     So Make Sure You Do NOT Miss This Webinar
                  </div>
                  <div class="col-12 f-md-22 f-18 w600 lh160 mx-auto my20 text-center white-clr">
                     Register Now! And Join Us Live On <br class="d-none d-md-block"> <span class="orange-clr">October 25th, 10:00 AM EST </span><br>
                     <span class="w400">Book Your Seat now (Limited to 100 Users Only)</span>
                  </div>
                  <!-- Aweber Form Code -->
                  <div class="col-12 p0 mt15 mt-md25" editabletype="form" style="z-index:10">
                     <!-- Aweber Form Code -->
                     <form method="post" class="af-form-wrapper register-form-style1 " accept-charset="UTF-8" action="https://www.aweber.com/scripts/addlead.pl" style="z-index: 10;">
                        <div style="display: none;">
                           <input type="hidden" name="meta_web_form_id" value="448889756" />
                           <input type="hidden" name="meta_split_id" value="" />
                           <input type="hidden" name="listname" value="awlist6641005" />
                           <input type="hidden" name="redirect" value="https://www.getmavas.com/prelaunch-thankyou" id="redirect_d5af24ac5b16e3f69ea0226b05d60951" />
                           <input type="hidden" name="meta_adtracking" value="My_Web_Form" />
                           <input type="hidden" name="meta_message" value="1" />
                           <input type="hidden" name="meta_required" value="name,email" />
                           <input type="hidden" name="meta_tooltip" value="" />
                        </div>
                        <div id="af-form-448889756" class="af-form">
                           <div id="af-body-448889756" class="af-body af-standards">
                              <div class="af-element width-form">
                                 <label class="previewLabel" for="awf_field-116368966-first"></label>
                                 <div class="af-textWrap">
                                    <div class="input-type" style="z-index: 11;">
                                       <!--<span class="input-group-addon border1px"><i class="fa fa-user" aria-hidden="true"></i></span>-->
                                       <input id="awf_field-116368966-first" type="text" name="name" class="text form-control custom-input input-field" value="" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="500" placeholder="Your Name">
                                    </div>
                                 </div>
                                 <div class="af-clear bottom-margin"></div>
                              </div>

                             


                              <div class="af-element width-form">
                                 <label class="previewLabel" for="awf_field-116368967"> </label>
                                 <div class="af-textWrap">
                                    <div class="input-type" style="z-index: 11;">
                                       <!--<span class="input-group-addon border1px"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>-->
                                       <input class="text form-control custom-input input-field" id="awf_field-116368967" type="text" name="email" value="" tabindex="501" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " placeholder="Your Email">
                                    </div>
                                 </div>
                                 <div class="af-clear bottom-margin"></div>
                              </div>
                              <input type="hidden" id="awf_field_aid" class="text" name="custom aid" value="undefined" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="502">
                              <div class="af-element buttonContainer button-type register-btn1 f-22 f-md-21 width-form mt-md0">
                                 <input name="submit" id="af-submit-image-348552691" type="submit" class="image" style="max-width: 100%;" alt="Submit Form" tabindex="503" value="Book Your Free Seat">
                                 <div class="af-clear bottom-margin"></div>
                              </div>
                           </div>
                        </div>
                        <div style="display: none;"><img src="https://forms.aweber.com/form/displays.htm?id=jAxMbOwcLOyczA==" alt="Form"></div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Header Section End -->
   <!--Footer Section Start -->
   <div class="footer-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
               <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 685 120" style="max-height:55px">
                     <defs>
                       <style>
                         .cls-1t {
                           fill: url(#linear-gradient-2);
                         }
                   
                         .cls-2t {
                           fill: #fff;
                         }
                   
                         .cls-3t {
                           fill: #21e5f4;
                         }
                   
                         .cls-4t {
                           fill: url(#linear-gradient);
                         }
                       </style>
                       <linearGradient id="linear-gradient" x1="0" y1="60" x2="25.31" y2="60" gradientUnits="userSpaceOnUse">
                         <stop offset="0" stop-color="#b956ff"></stop>
                         <stop offset="1" stop-color="#8d4ef9"></stop>
                       </linearGradient>
                       <linearGradient id="linear-gradient-2" x1="99.25" y1="59.94" x2="124.57" y2="59.94" gradientUnits="userSpaceOnUse">
                         <stop offset="0" stop-color="#8d57f4"></stop>
                         <stop offset="1" stop-color="#6e33ff"></stop>
                       </linearGradient>
                     </defs>
                     <g id="Layer_1-2" data-name="Layer 1">
                       <g>
                         <g>
                           <g>
                             <polygon class="cls-2t" points="343.95 66.06 343.95 66.06 300 .53 273.46 .53 343.96 99.29 355.45 83.2 343.95 66.06"></polygon>
                             <polygon class="cls-2t" points="367.54 66.26 414.45 .53 388.11 .53 355.71 48.61 367.54 66.26"></polygon>
                           </g>
                           <polygon class="cls-2t" points="218.06 0 147.57 99.82 172.51 99.82 182.71 84.58 198.02 61.87 218.07 32.36 237.91 61.88 253.21 84.59 263.4 99.82 288.55 99.82 218.06 0"></polygon>
                           <polygon class="cls-2t" points="469.86 0 399.36 99.82 424.31 99.82 434.5 84.58 449.81 61.87 469.87 32.36 489.7 61.88 505.01 84.59 515.2 99.82 540.35 99.82 469.86 0"></polygon>
                           <g>
                             <path class="cls-2t" d="m662.66,46.82l-96.97-14.26,16.04,21.03,5.2.9,69.12,11.61c3.34.56,5.78,3.45,5.78,6.83,0,3.83-3.1,6.93-6.93,6.93h-75.79l-15.37,20.13,94.46.02c14.79,0,26.78-11.99,26.78-26.78,0-13.07-9.43-24.22-22.31-26.4Z"></path>
                             <polygon class="cls-2t" points="685 1.08 589.71 1.08 589.71 1.06 581.89 1.06 566.5 21.21 669.63 21.21 685 1.08"></polygon>
                           </g>
                         </g>
                         <g>
                           <polygon class="cls-4t" points="25.31 20.82 25.31 120 0 99.11 0 0 25.31 20.82"></polygon>
                           <polygon class="cls-1t" points="124.57 0 124.57 99.13 99.25 119.89 99.25 20.76 124.57 0"></polygon>
                           <polygon class="cls-3t" points="92.74 25.17 92.74 62.88 62.24 100.89 31.82 62.89 31.82 25.17 62.24 63.17 92.74 25.17"></polygon>
                               </g>
                                </g>
                                 </g>
                                  </svg>
                  
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-16 f-md-18 w400 lh140 white-clr text-xs-center">Copyright © Mavas 2023</div>
                  <ul class="footer-ul w400 f-16 f-md-18 white-clr text-center text-md-right">
                     <li><a href="https://support.oppyo.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://getmavas.com/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://getmavas.com/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://getmavas.com/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://getmavas.com/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://getmavas.com/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://getmavas.com/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!--Footer Section End -->
<script type="text/javascript">
// Special handling for in-app browsers that don't always support new windows
(function() {
    function browserSupportsNewWindows(userAgent) {
        var rules = [
            'FBIOS',
            'Twitter for iPhone',
            'WebView',
            '(iPhone|iPod|iPad)(?!.*Safari\/)',
            'Android.*(wv|\.0\.0\.0)'
        ];
        var pattern = new RegExp('(' + rules.join('|') + ')', 'ig');
        return !pattern.test(userAgent);
    }

    if (!browserSupportsNewWindows(navigator.userAgent || navigator.vendor || window.opera)) {
        document.getElementById('af-form-448889756').parentElement.removeAttribute('target');
    }
})();
</script><script type="text/javascript">
    <!--
    (function() {
        var IE = /*@cc_on!@*/false;
        if (!IE) { return; }
        if (document.compatMode && document.compatMode == 'BackCompat') {  
            if (document.getElementById("af-form-448889756")) {
                document.getElementById("af-form-448889756").className = 'af-form af-quirksMode';
            }
            if (document.getElementById("af-body-448889756")) {
                document.getElementById("af-body-448889756").className = "af-body inline af-quirksMode";
            }
            if (document.getElementById("af-header-448889756")) {
                document.getElementById("af-header-448889756").className = "af-header af-quirksMode";
            }
            if (document.getElementById("af-footer-448889756")) {
                document.getElementById("af-footer-448889756").className = "af-footer af-quirksMode";
            }
        }
    })();
    -->
</script>
</body>

</html>

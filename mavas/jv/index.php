<html>
<head>
   <title>MAVAS | JV</title>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=9">
   <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
   <meta name="title" content="MAVAS | JV">
   <meta name="description" content="First-To-Market #1 Super VA That Finish Your 100s Type of Marketing Tasks (Better & Faster Than Any CMO Out There...) AND Trim Your Workload, Hiring/Firing Headaches, & Monthly Bills">
   <meta name="keywords" content="MAVAS">
   <meta property="og:image" content="https://getmavas.com/jv/thumbnail.png">
   <meta name="language" content="English">
   <meta name="revisit-after" content="1 days">
   <meta name="author" content="Dr. Amit Pareek">
   <!-- Open Graph / Facebook -->
   <meta property="og:type" content="website">
   <meta property="og:title" content="MAVAS | JV">
   <meta property="og:description" content="First-To-Market #1 Super VA That Finish Your 100s Type of Marketing Tasks (Better & Faster Than Any CMO Out There...) AND Trim Your Workload, Hiring/Firing Headaches, & Monthly Bills">
   <meta property="og:image" content="https://getmavas.com/jv/thumbnail.png">
   <!-- Twitter -->
   <meta property="twitter:card" content="summary_large_image">
   <meta property="twitter:title" content="MAVAS | JV">
   <meta property="twitter:description" content="First-To-Market #1 Super VA That Finish Your 100s Type of Marketing Tasks (Better & Faster Than Any CMO Out There...) AND Trim Your Workload, Hiring/Firing Headaches, & Monthly Bills">
   <meta property="twitter:image" content="https://getmavas.com/jv/thumbnail.png">
   <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
   <link rel="preconnect" href="https://fonts.googleapis.com">
   <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
   <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
   <!-- required -->
   <link rel="stylesheet" href="../common_assets/css/bootstrap.min.css" type="text/css">
   <link rel="stylesheet" href="assets/css/style.css" type="text/css">
   <link rel="stylesheet" href="assets/css/timer.css" type="text/css">
   <script src="https://cdn.oppyo.com/launches/yodrive/common_assets/js/jquery.min.js"></script>
</head>
<body>
<!-- New Timer  Start-->
<!-- <?php
   $date = 'October 25 2023 10:00 AM EST';
   $exp_date = strtotime($date);
   $now = time();  
   if ($now < $exp_date) {
   ?>
<?php
   } else {
      echo "Times Up";
   }
   ?> -->
<!-- New Timer End -->
<!-- Header Section Start -->
<div class="header-section">
   <div class="container">
      <div class="row align-items-center">
         <div class="col-md-3 col-12 text-center">
        
         <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 685 120" style="max-height:55px">
  <defs>
    <style>
      .cls-1 {
        fill: url(#linear-gradient-2);
      }

      .cls-2 {
        fill: #fff;
      }

      .cls-3 {
        fill: #21e5f4;
      }

      .cls-4 {
        fill: url(#linear-gradient);
      }
    </style>
    <linearGradient id="linear-gradient" x1="0" y1="60" x2="25.31" y2="60" gradientUnits="userSpaceOnUse">
      <stop offset="0" stop-color="#b956ff"></stop>
      <stop offset="1" stop-color="#8d4ef9"></stop>
    </linearGradient>
    <linearGradient id="linear-gradient-2" x1="99.25" y1="59.94" x2="124.57" y2="59.94" gradientUnits="userSpaceOnUse">
      <stop offset="0" stop-color="#8d57f4"></stop>
      <stop offset="1" stop-color="#6e33ff"></stop>
    </linearGradient>
  </defs>
  <g id="Layer_1-2" data-name="Layer 1">
    <g>
      <g>
        <g>
          <polygon class="cls-2" points="343.95 66.06 343.95 66.06 300 .53 273.46 .53 343.96 99.29 355.45 83.2 343.95 66.06"></polygon>
          <polygon class="cls-2" points="367.54 66.26 414.45 .53 388.11 .53 355.71 48.61 367.54 66.26"></polygon>
        </g>
        <polygon class="cls-2" points="218.06 0 147.57 99.82 172.51 99.82 182.71 84.58 198.02 61.87 218.07 32.36 237.91 61.88 253.21 84.59 263.4 99.82 288.55 99.82 218.06 0"></polygon>
        <polygon class="cls-2" points="469.86 0 399.36 99.82 424.31 99.82 434.5 84.58 449.81 61.87 469.87 32.36 489.7 61.88 505.01 84.59 515.2 99.82 540.35 99.82 469.86 0"></polygon>
        <g>
          <path class="cls-2" d="m662.66,46.82l-96.97-14.26,16.04,21.03,5.2.9,69.12,11.61c3.34.56,5.78,3.45,5.78,6.83,0,3.83-3.1,6.93-6.93,6.93h-75.79l-15.37,20.13,94.46.02c14.79,0,26.78-11.99,26.78-26.78,0-13.07-9.43-24.22-22.31-26.4Z"></path>
          <polygon class="cls-2" points="685 1.08 589.71 1.08 589.71 1.06 581.89 1.06 566.5 21.21 669.63 21.21 685 1.08"></polygon>
        </g>
      </g>
      <g>
        <polygon class="cls-4" points="25.31 20.82 25.31 120 0 99.11 0 0 25.31 20.82"></polygon>
        <polygon class="cls-1" points="124.57 0 124.57 99.13 99.25 119.89 99.25 20.76 124.57 0"></polygon>
        <polygon class="cls-3" points="92.74 25.17 92.74 62.88 62.24 100.89 31.82 62.89 31.82 25.17 62.24 63.17 92.74 25.17"></polygon>
      </g>
    </g>
  </g>
</svg>
         </div>
      
         <div class="col-md-9 mt15 mt-md7">
             <ul class="leader-ul f-20 w400 white-clr text-md-end text-center ">
               <li>
                  <a href="https://docs.google.com/document/d/1gMeSUVGTf6kOLpgNS7b8YE_qRw4xFeSu/edit" target="_blank">JV Doc</a>
               </li>
               <li>
                  <a href="https://docs.google.com/document/d/1-ldOBgBQ3bFsrxSkM_af514SFRIuTyct/edit" target="_blank">Swipes</a>
               </li>
               <li>
                  <a href="#live-sec" class="affiliate-link-btn ml-md15 mt10 mt-md0"> Grab Your Affiliate Link</a>
               </li>
            </ul>
         </div>
      </div>
      <div class="row">
            <div class="col-12 mt20 mt-md50 text-center">
               <!-- <div class="pre-heading f-18 f-md-22 w400 white-clr lh140">
                     <div class="pre-heading-content" style="padding:10px 25px;">
                     Run Your Business WITHOUT Any Expenses or Hassles. <br class="d-none d-md-block">ZERO Salaries. ZERO Freelancer/Agency Fee.</div>
                  </div> -->
               <div class="pre-heading f-18 f-md-22 w500 white-clr lh140">
              
               <div class="pre-heading-content" style="padding:10px 25px;">Introducing... M.A.V.A.S <span class="underline">(Multifunctional Automated Virtual Assistant System)</span></div>
               </div>
            </div>
         <div class="col-12 mt30 mt-md50 relative p-md0 ">
            <div class="mainheadline f-md-42 f-25 w400 text-center white-clr lh140">
               <div class="headline-text">
               First-To-Market <span class="w700 sky-blue">#1 Super VA That Finish Your 100s Type of Marketing Tasks <span class="caveat">(Better & Faster Than Any CMO Out There...)</span> </span> AND Trim Your Workload, Hiring/Firing Headaches, & Monthly Bills </div>
            </div>
         </div>
         <div class="col-12 mt20 mt-md30 p-md0">
            <div class="f-16 f-md-22 w500 text-center lh140 white-clr text-capitalize">
               <!-- Skyrocket Your Business Profits. No Hiring/Firing Headaches. No Hefty Payments -->
               <span class="d-block mt40 f-20 f-md-28 lh140">Get All Your/Client's Marketing Work Done I Loaded with 150,000+ Prompts I
               Trained in 100s type of Marketing Tasks</span>
            </div>
         </div>
         <div class="col-12 col-md-12 mt15 mt-md80">
            <div class="row align-items-center">
               <div class="col-md-8 col-12">
                  <div class="video-box">
                     <!-- <img src="assets/images/video-img.webp" class="img-fluid mx-auto d-block"> -->
                     <div style="padding-bottom: 56.25%;position: relative;"><iframe src="https://mavas.oppyo.com/video/embed/iz1d4yrhe9"
               style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
               box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div>
                  </div>
               </div>
               <div class="col-md-4 col-12 mt20 mt-md0">
                  <div class="calendar-wrap">
                     <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/date.webp" alt="Date" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="clearfix"></div>
                  <div class="countdown counter-black mt15 mt-md30">
                     <div class="timer-label text-center"><span class="f-30 f-md-40 lh100 timerbg col-12">00</span><br><span class="f-14 w500">Days</span> </div>
                     <div class="timer-label text-center"><span class="f-30 f-md-40 lh100 timerbg">00</span><br><span class="f-14 w500">Hours</span> </div>
                     <div class="timer-label text-center"><span class="f-30 f-md-40 lh100 timerbg">00</span><br><span class="f-14 w500">Mins</span> </div>
                     <div class="timer-label text-center"><span class="f-30 f-md-40 lh100 timerbg">00</span><br><span class="f-14 w500">Sec</span> </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- Header Section End -->
<!-- Live Section Start -->
<div class="live-section" id="live-sec">
   <div class="container">
      <div class="row">
         <div class="col-12 col-md-6">
            <div class="left-live-box">
               <div class="f-28 f-md-45 w700 text-center text-capitalize black-clr lh140">
                  Subscribe To Our <br class="d-none d-md-block"> JV List
               </div>
               <div class="f-20 f-md-22 w500 text-center text-capitalize black-clr lh140 mt10">
                  And Be The First to Know Our Special Contest, Events and Discounts
               </div>
               <!-- Aweber Form Code -->
               <div class="mt20 mt-md40">
                        <form method="post" class="af-form-wrapper mb-md5" accept-charset="UTF-8" action="https://www.aweber.com/scripts/addlead.pl">
                           <div style="display: none;">
                              <input type="hidden" name="meta_web_form_id" value="1703814699">
                              <input type="hidden" name="meta_split_id" value="">
                              <input type="hidden" name="listname" value="awlist6200898">
                              <input type="hidden" name="redirect" value="https://www.aweber.com/thankyou-coi.htm?m=text" id="redirect_3161ec97c09ca429c4183bf74a86fd90">
                              <input type="hidden" name="meta_adtracking" value="My_Web_Form">
                              <input type="hidden" name="meta_message" value="1">
                              <input type="hidden" name="meta_required" value="name,email">
                              <input type="hidden" name="meta_tooltip" value="">
                           </div>
                           <div id="af-form-1703814699" class="af-form">
                              <div id="af-body-1703814699" class="af-body af-standards row justify-content-center">
                                 <div class="af-element col-md-12">
                                    <label class="previewLabel" for="awf_field-115787791" style="display:none;">Name: </label>
                                    <div class="af-textWrap mb20 input-type">
                                       <input id="awf_field-115787791" class="frm-ctr-popup form-control input-field" type="text" name="name" placeholder="Your Name" value="" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="500">
                                    </div>
                                    <div class="af-clear"></div>
                                 </div>
                                 <div class="af-element mb20 col-md-12">
                                    <label class="previewLabel" for="awf_field-115787792" style="display:none;">Email: </label>
                                    <div class="af-textWrap input-type">
                                       <input class="text frm-ctr-popup form-control input-field" id="awf_field-115787792" type="text" name="email" placeholder="Your Email" value="" tabindex="501" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} ">
                                    </div>
                                    <div class="af-clear"></div>
                                 </div>
                                 <div class="af-element buttonContainer button-type form-btn white-clr col-md-12">
                                    <input name="submit" class="submit f-20 f-md-24 white-clr center-block" type="submit" value="Subscribe For JV Updates" tabindex="502">
                                    <div class="af-clear"></div>
                                 </div>
                              </div>
                           </div>
                           <div style="display: none;"><img src="https://forms.aweber.com/form/displays.htm?id=zIwcTIxMbMwc" alt="image"></div>
                        </form>
                     </div>
                     <!-- Aweber Form Code -->
            </div>
         </div>
         <div class="col-12 col-md-6 mt20 mt-md0 text-center">
            <div class="right-live-box">
               <div class="f-24 f-md-34 w700 text-center text-capitalize black-clr lh140">
                  Grab Your <span class="orange-clr">JVZoo</span> Affiliate Link to Jump on This Amazing Product Launch
               </div>
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/jvzoo.webp" class="img-fluid d-block mx-auto mt20 " alt="Jvzoo">
               <div class="request-affiliate mt20">
                  <a href="https://www.jvzoo.com/affiliate/affiliateinfonew/index/400755" class="f-20 f-md-22 w600 mx-auto white-clr" target="_blank">Request FE Link</a>
               </div>
               <div class="request-affiliate mt20">
                  <a href="https://www.jvzoo.com/affiliate/affiliateinfonew/index/400759" class="f-20 f-md-22 w600 mx-auto white-clr" target="_blank">Request Bundle Link</a>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- Live Section End -->
<!-- Technology-Section -->
<div class="technology-sec">
   <div class="container">
      <div class="row">
         <!-- <div class="col-12 text-center">
            <div class="f-md-32 f-20 w400 text-center orange-clr">
               Why Trust Us? <br> We Have 5+ Years of Experience in Hosting Files & Videos  
            </div>
         </div> -->
         <div class="col-12 col-md-11 mx-auto text-center gx-0 px-md-5">
            <div class="f-md-40 f-28 w700 text-center white-clr lh140 text-capitalize underline">
            Appointing Employee's Or Freelancer's <span class="w700 red">Sucks !!!</span>
            </div>
         </div>
      </div>
      <div class="row mt20 mt-md50 align-items-center">
      <div class="col-md-6 col-12">
         <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/cloud.png" class="img-fluid mx-auto d-block">
         </div>
         <div class="col-md-6 col-12 pl-md40">
            <div class="f-md-22 f-18 w400 white-clr">
               <ul class="f-18 f-md-22 w400 white-clr sad-icon">
               <li class="mb20">Running an online business demands either doing everything yourself or appointing employees/experts/freelancers.
                However, managing employees or coordinating with freelancers is itself a Hectic JOB</li>
               <li class="mb20">It involves investing 1000's of dollars & countless hours into each marketing project.</li>
               <li class="mb20">Furthermore, lacking requisite expertise further extends your workload, requiring additional time for consultations and task reviews.</li>
               <li>Moreover, catering to those who lack the requisite expertise further extends the workload, requiring additional time for consultations and task reviews. </li>
               </ul>
            </div>
         </div>
     
      </div>
   </div>
</div>
<!-- Technology-Section-End -->
<!-- Online Business Section Starts -->
<div class="online-business-sec">
<div class="container">
   <div class="row">
      <div class="col-md-12 col-12">
         <div class="f-28 f-md-45 lh140 red-clr w700 text-center">
         I’m Damm Sure, Your Customers Have Faced it…. Right ???
         </div>
      </div>
      <div class="col-lg-10 col-md-10 col-12 mx-auto">
         <div class="f-18 f-md-22 mt10 mt-md10 text-center">
         "What if A Super VA Near to Human Intelligence or Better with 100s of Expertise were available 24/7, ready to complete all your tasks without paying any fees?"
         </div> 
      </div>
      <div class="col-12 col-md-12">
      <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/online-bussines-man.webp" class="img-fluid d-block mx-auto mt20 mt-md50" alt="Phase1">
               
         
      </div>
      <div class="col-md-10 col-12 mx-auto">
         <div class="f-18 f-md-22  mt10 mt-md10 text-center mt20 mt-md30 px-md30">
         That’s Right, we have come up with MAVAS to revolutionize your marketing and skyrocket your online INCOME LIKE NEVER BEFORE. 
         </div> 
      </div>
   </div>
</div>
</div>
<!-- Online Business Section Ends -->
<!-- Proudly Section Starts -->
<div class="proudly-sec" id="product">
   <div class="container">
      <div class="row">
         <div class="col-12 text-center">
            <div class="f-28 f-md-38 w700 lh140 white-clr caveat proudly-head">
               Proudly Presenting…
            </div>
         </div>
         <!-- <div class="col-12 mx-auto mt-md50 mt20 text-center">
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 754.92 130.75" style="max-height:120px"><defs><style>.cls-1,.cls-10,.cls-11,.cls-2,.cls-3,.cls-4,.cls-5,.cls-6{fill-rule:evenodd;}.cls-1{fill:url(#linear-gradient);}.cls-2{fill:url(#linear-gradient-2);}.cls-3{fill:url(#linear-gradient-3);}.cls-4{fill:url(#linear-gradient-4);}.cls-5{fill:url(#linear-gradient-5);}.cls-6{fill:url(#linear-gradient-6);}.cls-7{fill:url(#linear-gradient-7);}.cls-8{fill:url(#linear-gradient-8);}.cls-9{fill:#fff;}.cls-10{fill:url(#linear-gradient-9);}.cls-11{fill:url(#linear-gradient-10);}</style><linearGradient id="linear-gradient" x1="15.05" y1="45.21" x2="93.72" y2="45.21" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#b956ff"/><stop offset="1" stop-color="#6e33ff"/></linearGradient><linearGradient id="linear-gradient-2" x1="50.44" y1="50.99" x2="65.24" y2="50.99" xlink:href="#linear-gradient"/><linearGradient id="linear-gradient-3" x1="0" y1="25.89" x2="85.69" y2="25.89" xlink:href="#linear-gradient"/><linearGradient id="linear-gradient-4" x1="0" y1="77.93" x2="29.76" y2="77.93" xlink:href="#linear-gradient"/><linearGradient id="linear-gradient-5" x1="68.92" y1="75.19" x2="114.65" y2="75.19" xlink:href="#linear-gradient"/><linearGradient id="linear-gradient-6" x1="26.29" y1="115.23" x2="64.02" y2="115.23" xlink:href="#linear-gradient"/><linearGradient id="linear-gradient-7" x1="130.19" y1="69" x2="200.69" y2="69" xlink:href="#linear-gradient"/><linearGradient id="linear-gradient-8" x1="208.38" y1="69" x2="225.46" y2="69" xlink:href="#linear-gradient"/><linearGradient id="linear-gradient-9" x1="628.92" y1="75.9" x2="686.57" y2="75.9" xlink:href="#linear-gradient"/><linearGradient id="linear-gradient-10" x1="697.27" y1="75.9" x2="754.92" y2="75.9" xlink:href="#linear-gradient"/></defs><title>Asset 1</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1" d="M19.88,36.05a2.44,2.44,0,0,1,1.64.63,2.38,2.38,0,0,1,.77,1.56.84.84,0,0,1,0,.12h0v.26a.88.88,0,0,0,0,.15,2.41,2.41,0,0,1-4.1,1.4h0a2.43,2.43,0,0,1,0-3.41h0a2.39,2.39,0,0,1,1.7-.7ZM38.45,25.13h0a2.27,2.27,0,0,1,.66,1.61,2.13,2.13,0,0,1-.18.88,2.36,2.36,0,0,1-.5.74l-.06.05h0a2.28,2.28,0,0,1-.71.44,2.14,2.14,0,0,1-.82.15,2.26,2.26,0,0,1-1.6-.66h0a2.27,2.27,0,1,1,3.21-3.2Zm-4,43.43h0A2.51,2.51,0,0,1,33.78,72h0a2.5,2.5,0,1,1,.55-3.69h0v0h0l0,0h0l0,.08h0l.06.08ZM55.8,64.78v.14a.82.82,0,0,1,0,.14,2.76,2.76,0,0,1-.92,1.72,2.73,2.73,0,0,1-3.74-.17,2.74,2.74,0,0,1-.5-3.06,2.91,2.91,0,0,1,.72-.92,2.68,2.68,0,0,1,1.8-.63,2.72,2.72,0,0,1,2.66,2.78ZM35.33,66a4.91,4.91,0,1,0-.23,8h0a4.94,4.94,0,0,0,2.11-3.11,4.89,4.89,0,0,0-.36-3.07l9.81-7.77.9-.71-.67-.93A13.32,13.32,0,1,1,58.16,64a5.12,5.12,0,0,0-5-4.42,5.16,5.16,0,0,0-3.39,1.19,5.17,5.17,0,0,0-1.34,1.73,5.32,5.32,0,0,0-.5,2.12,5.13,5.13,0,0,0,10,1.8A15.71,15.71,0,0,0,72.3,44.87c-.1-.25-.21-.49-.32-.73L93.72,22.41,92,20.7,70.76,42A15.83,15.83,0,0,0,68,38.8L89,17.27l-1.72-1.69L66,37.36,65.43,37a15.72,15.72,0,0,0-16.79.87l-7.86-8.64a4.24,4.24,0,0,0,.37-.7,4.62,4.62,0,0,0-1-5.13h0a4.68,4.68,0,1,0-6.61,6.62h0a4.68,4.68,0,0,0,3.3,1.37,4.79,4.79,0,0,0,1.69-.32,4,4,0,0,0,.47-.21l7.78,8.55-.75.78a15.82,15.82,0,0,0-1.85,2.52l-5.73-5.59-.85-.34-13.14.11a4.94,4.94,0,0,0-1.32-2,4.81,4.81,0,0,0-6.67.15h0a4.82,4.82,0,0,0,0,6.83h0a4.84,4.84,0,0,0,8.17-2.55l12.48-.11,6,5.81A15.71,15.71,0,0,0,44.3,58.89l-9,7.11Z"/><path class="cls-2" d="M62.82,51a5,5,0,1,1-1.46-3.52A5,5,0,0,1,62.82,51Zm.25-5.23A7.39,7.39,0,1,0,65.24,51a7.37,7.37,0,0,0-2.17-5.23Z"/><path class="cls-3" d="M7,45.56l23.13.29A4,4,0,0,1,32,44.2,3.87,3.87,0,0,1,34.52,44a4,4,0,0,1,0,7.67A4,4,0,0,1,32,51.45a3.94,3.94,0,0,1-1.84-1.66H0v-.16a51.31,51.31,0,0,1,85.56-36.5l.12.11-.11.12L74.86,24.08l-.11.11-.12-.11A39.9,39.9,0,0,0,7,45.56Z"/><path class="cls-4" d="M24.36,60.56A3.86,3.86,0,0,1,22.57,59H7.81c-1.56,2.85,5.61,13.78,5.91,14.24,6.67,6.68,9.69,24.17,9.7,24.23,2.63,9.15-9.07,3.59-9.27,3.49C7.52,98.84,9,91.74,9.07,91.43,10.38,79.35.85,69.55,0,54.93v-.17H22.73a3.87,3.87,0,0,1,4.36-1.43,3.79,3.79,0,0,1,2,1.46,3.87,3.87,0,0,1-.18,4.66,3.75,3.75,0,0,1-2.07,1.31A3.86,3.86,0,0,1,24.36,60.56Z"/><path class="cls-5" d="M83.09,38.05l12.83-13,.14-.15.11.18s9.69,16.21,6,31.82h0s-1.78,5.19,1.62,8.29l0,0,9.66,15.4s3.43,5.16-1.75,6.73h0s-11.22.3-8,10.45c.36.21,1.22,1.51-5.54,9.22.3,1.26,4,17.79-7.41,18.38-.05,0-14.6.91-19-9.45a37.75,37.75,0,0,1-1.68-23.65,44.22,44.22,0,0,1,5.46-15v0a48.31,48.31,0,0,0,8.88-17.44c0-.19,3.13-17.74-1.31-21.59L83,38.17Z"/><path class="cls-6" d="M55.39,111.4a3.85,3.85,0,0,1,3.83,3.84h0a3.84,3.84,0,0,1-3.83,3.83H34.92a3.85,3.85,0,0,1-3.84-3.83h0a3.85,3.85,0,0,1,3.83-3.84Zm4.79-11.68h0A3.85,3.85,0,0,1,64,103.55h0a3.85,3.85,0,0,1-3.84,3.84H30.12a3.85,3.85,0,0,1-3.83-3.84h0a3.84,3.84,0,0,1,3.83-3.83ZM50.1,123.08a3.85,3.85,0,0,1,3.84,3.84h0a3.85,3.85,0,0,1-3.84,3.83H40.2a3.84,3.84,0,0,1-3.83-3.83h0a3.85,3.85,0,0,1,3.83-3.84Z"/><path class="cls-7" d="M178.42,91.67H152.26l-4.2,12.38H130.19l25.36-70.11h19.78l25.36,70.11H182.62ZM174,78.48l-8.69-25.66-8.59,25.66Z"/><path class="cls-8" d="M225.46,33.94v70.11H208.38V33.94Z"/><path class="cls-9" d="M263.28,47.63V61.81h22.87V75H263.28V90.37h25.86v13.68H246.2V33.94h42.94V47.63Z"/><path class="cls-9" d="M387.86,54Q394,60.31,394,71.49v32.56H377V73.79q0-5.39-2.84-8.34a11.92,11.92,0,0,0-15.68,0q-2.85,3-2.85,8.34v30.26h-17V73.79c0-3.59-.94-6.37-2.84-8.34a11.92,11.92,0,0,0-15.68,0q-2.85,3-2.85,8.34v30.26H300.23V48.32H317.3v7a18.39,18.39,0,0,1,6.79-5.55,21.43,21.43,0,0,1,9.49-2,23.06,23.06,0,0,1,11.24,2.69,19.41,19.41,0,0,1,7.74,7.69,22.45,22.45,0,0,1,7.89-7.49,21.27,21.27,0,0,1,10.88-2.89Q381.71,47.73,387.86,54Z"/><path class="cls-9" d="M430,49.92a21.14,21.14,0,0,1,10.29-2.39A23.07,23.07,0,0,1,452.72,51a24.17,24.17,0,0,1,8.84,10,33.35,33.35,0,0,1,3.25,15.08,33.69,33.69,0,0,1-3.25,15.13,24.43,24.43,0,0,1-8.84,10.08,22.78,22.78,0,0,1-12.48,3.55,21.22,21.22,0,0,1-10.24-2.4,18.68,18.68,0,0,1-6.94-6.19v34.36H406V48.32h17.08v7.89A18.57,18.57,0,0,1,430,49.92Zm13.93,16.13a11.68,11.68,0,0,0-8.73-3.64,11.52,11.52,0,0,0-8.64,3.69Q423,69.79,423,76.19t3.55,10.08a11.93,11.93,0,0,0,17.32,0q3.6-3.73,3.6-10.13T443.88,66.05Z"/><path class="cls-9" d="M490.87,30.15v73.9H473.79V30.15Z"/><path class="cls-9" d="M513.89,101.35a25.6,25.6,0,0,1-10.29-10,29.92,29.92,0,0,1-3.74-15.18,29.59,29.59,0,0,1,3.79-15.13A25.8,25.8,0,0,1,514,51a33,33,0,0,1,29.56,0,25.8,25.8,0,0,1,10.39,10,29.59,29.59,0,0,1,3.79,15.13,29.34,29.34,0,0,1-3.84,15.13,26,26,0,0,1-10.49,10,31.24,31.24,0,0,1-14.83,3.5A30.64,30.64,0,0,1,513.89,101.35ZM537,86.47q3.45-3.59,3.44-10.28T537.06,65.9a11.36,11.36,0,0,0-16.53-.05q-3.3,3.56-3.3,10.34t3.25,10.28a10.5,10.5,0,0,0,8.14,3.6A11.13,11.13,0,0,0,537,86.47Z"/><path class="cls-9" d="M623.79,48.32l-34.95,82.2H570.46l12.79-28.37L560.58,48.32h19.07l12.88,34.86,12.79-34.86Z"/><path class="cls-10" d="M657.74,47.07A28.84,28.84,0,0,1,686,81.66H665.81a9.92,9.92,0,1,1,0-11.53h8.25a17.3,17.3,0,1,0-16.32,23.06h23.07a28.83,28.83,0,1,1-23.07-46.12Zm0,25.07A3.76,3.76,0,1,1,654,75.89a3.75,3.75,0,0,1,3.75-3.75Z"/><path class="cls-11" d="M726.09,47.07a28.84,28.84,0,0,1,28.25,34.59H734.16a9.92,9.92,0,1,1,0-11.53h8.25a17.3,17.3,0,1,0-16.32,23.06h23.07a28.83,28.83,0,1,1-23.07-46.12Zm0,25.07a3.76,3.76,0,1,1-3.75,3.75,3.75,3.75,0,0,1,3.75-3.75Z"/></g></g></svg>
         </div> -->
         <!--<div class="col-12 f-md-45 f-28 mt-md30 mt20 w700 text-center white-clr lh140">-->
         <!--   <span class="w400">World's First </span> AI-Powered Near to Human Intelligence Employees Automates Everything in Your Marketing, Cut Your Workload BIG Time. <!--<span class="w400">No Hiring/Firing Headaches. No Hefty Payments.</span>--->-->
         <!--</div>-->
      </div>
      <div class="row mt30 mt-md50 align-items-center">
         <div class="col-12 col-md-10 mx-auto">
            <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/product-image.webp" class="img-fluid d-block mx-auto img-animation">
         </div>
      </div>
      <div class="row feature-list-block mt30 mt-md50">
               <div class="col-12">
                  <ul class="proudly-list f-18 f-md-22 w300 white-clr p-md0">
                     <li>Near HUMAN or Better Intelligence Virtual Assistant Trained in 100s of expertise Preloaded with 150,000+ prompts</li>
                     <li>Automate 100s of Marketing Tasks Done in a Snap for You & Your Clients Better than Any CMO or Niche Expert</li>
                     <li>Eliminate the hassle of hiring/firing employees or freelancers and spare yourself from the burden of task explanations.</li>
                     <li>Embed MAVAS on any Website/Page, tirelessly working around the clock, 24/7</li>
                     <li>Generate traffic, Leads & sales and enhanced Customer Engagement.</li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
<!-- Proudly Section Ends -->
<!-- Features Section Starts -->
<!-- <div class="presenting-feature">
   <div class="container">
      <div class="row">
         <div class="col-12">
            
            </div>
         </div>
         Features Section Ends
         feature-sec Section Starts
         <div class="feature-sec">
            <div class="container p-md0">
               <div class="row">
                  <div class="col-12 text-center mt20 mt-md100 p-md0">
                     <div class="f-md-45 f-28 w700 text-center white-clr lh120 text-capitalize purple-brush">
                     "Unlock Business Growth With Mavas" 
                     </div>
                  </div>
               </div>
               <div class="row align-items-center mt20 mt-md100">
                  <div class="col-md-6 col-12 mt-md0 mt20">
                     <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/f1.webp" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="col-md-6 col-12 f-md-22 f-18 w400 ">
                     AiEmployee, a cutting-edge app, crafts Ai Employees with 100+ expertise, near-human intelligence, in just 3 clicks. Each expert boasts tens of thousands of prompts, excelling in their respective domains. 
                  </div>
                  
               </div>
               <div class="row align-items-center mt-md50 mt30" data-aos="fade-right">
               <div class="col-md-6 col-12 mt-md0 mt20 order-md-2">
                     <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/f2.webp" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="col-md-6 col-12 f-md-22 f-18 w400 order-md-1">
                     Consider a Legal Advisor; they can draft emails, articles, papers, and scripts with expert precision. We've equipped them with an extensive library of tens of thousands of prompts. 
                  </div>
               </div>
            </div>
         </div>
      feature-sec Starts
      </div>
   </div>
</div>  -->
<!-- POTENTIAL SECTION START -->
<div class="potential-sec">
   <div class="container">
      
      <div class="row">
         <div class="col-12 relative">
            <div class="row">
               <div class="col-12 col-md-12 f-md-45 f-25 w700 text-center black-clr lh140">
               
               Using MAVAS The Super VA SAVES You Thousands Of Dollars Every Months
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/down-icon.png" class="img-fluid mx-auto d-block mt20 mt-md30">
               </div>
               <div class="col-12 col-md-12 mt20 mt-md50">
                  <div class="expertise-left">
                     <div class="row align-items-center">
                        <div class="col-md-5 col-12">
                           <div class="row align-items-center">
                              <div class="col-md-3 col-4">
                                 <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/expertise-icon1.png" class="img-fluid mx-auto d-block">
                              </div>
                              <div class="col-md-8  col-8 ps-0">
                                 <div class="f-md-28 f-22 lh140 w600 white-clr">
                                 100s of Expertise
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-md-5 offset-md-1 pl-md0 col-12">
                           <div class="f-md-22 f-18 lh140 w400 black-clr  mt20 mt-md0">
                           Swift task execution, minutes instead of hours or days.
                           </div>
                        </div>
                     </div>
                  </div>
               </div>

               <div class="col-12 col-md-12 mt20 mt-md30">
                  <div class="expertise-right">
                     <div class="row align-items-center">
                        <div class="col-md-5 col-12 offset-md-1 order-md-2">
                           <div class="row align-items-center">
                              <div class="col-md-3 col-4 offset-md-1 order-md-2">
                                 <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/expertise-icon2.png" class="img-fluid mx-auto d-block">
                              </div>
                              <div class="col-md-7  col-8 offset-md-1 order-md-1">
                                 <div class="f-md-28 f-22 lh140 w600 white-clr">
                                 No Hiring or Firing Hassles
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-md-6 col-12">
                           <div class="f-md-22 f-18 lh140 w400 black-clr  mt20 mt-md0">
                           Eliminate the need for team/freelancer management. 
                           </div>
                        </div>
                     </div>
                  </div>
               </div>

               <div class="col-12 col-md-12 mt20 mt-md30">
                  <div class="expertise-left">
                     <div class="row align-items-center">
                        <div class="col-md-5 col-12">
                           <div class="row align-items-center">
                              <div class="col-md-3 col-4">
                                 <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/expertise-icon3.png" class="img-fluid mx-auto d-block">
                              </div>
                              <div class="col-md-8  col-8 ps-0">
                                 <div class="f-md-28 f-22 lh140 w600 white-clr">
                                 Streamlined Communication 
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-md-6 offset-md-1  col-12">
                           <div class="f-md-22 f-18 lh140 w400 black-clr  mt20 mt-md0">
                           No time wasted on extensive explanations and discussions. 
                           </div>
                        </div>
                     </div>
                  </div>
               </div>

               <div class="col-12 col-md-12 mt20 mt-md30">
                  <div class="expertise-right">
                     <div class="row align-items-center">
                        <div class="col-md-5 col-12 offset-md-2 order-md-2">
                           <div class="row align-items-center">
                              <div class="col-md-3 col-4 offset-md-1 order-md-2">
                                 <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/expertise-icon2.png" class="img-fluid mx-auto d-block">
                              </div>
                              <div class="col-md-7  col-8 offset-md-1 order-md-1">
                                 <div class="f-md-28 f-22 lh140 w600 white-clr">
                                 Cost-Efficient Solution 
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-md-5 col-12">
                           <div class="f-md-22 f-18 lh140 w400 black-clr  mt20 mt-md0">
                           Significant savings, no hefty bills for services.
                           </div>
                        </div>
                     </div>
                  </div>
               </div>

               <div class="col-12 col-md-12 mt20 mt-md30">
                  <div class="expertise-left">
                     <div class="row align-items-center">
                        <div class="col-md-5 col-12">
                           <div class="row align-items-center">
                              <div class="col-md-3 col-4">
                                 <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/expertise-icon3.png" class="img-fluid mx-auto d-block">
                              </div>
                              <div class="col-md-8  col-8 ps-0">
                                 <div class="f-md-28 f-22 lh140 w600 white-clr">
                                 Boosted Productivity 
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-md-6 offset-md-1  col-12">
                           <div class="f-md-22 f-18 lh140 w400 black-clr  mt20 mt-md0">
                           Enhanced efficiency, tasks completed promptly and proficiently. 
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
             </div>
         </div>
        
      </div>
   </div>
</div>
<!-- POTENTIAL SECTION END -->
<!-- blue-section start -->
<div class="blue-section">
   <div class="container">
      <div class="row">
         <div class="col-12 col-md-12">
               <div class="col-12 col-md-12">
                  <div class="f-md-45 f-28 lh140 w700 text-center white-clr purple-brush">
                     “Welcome to Near-Human Intelligence”
                  </div>
                  <div class="f-md-38 f-24 lh140 w600 text-center white-clr mt10 mt-md20">
                  Trim Your Bills and Workload! 
                  </div>
                  <div class="f-md-28 f-20 lh140 w600 text-center white-clr mt10 mt-md20">
                  </u><span class="underline"> With 150,000+ Done for You Niche Specific Prompts  </span></span>
               </div>
               <div class="f-md-24 f-20 lh140 w600 text-center white-clr mt10 mt-md10">
               We are not talking about some articles, text or Chat… Mavas <br class="d-none d-md-block"> can do anything… 
               </div>
               </div>
              
         </div>
         <div class="col-md-3 col-12 mt20 mt-md50">
            <div class="blue-box">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/b1.png" class="img-fluid mx-auto d-block">
               <div class="f-md-24 f-28 lh140 w400 text-center white-clr mt10 mt-md10">
               Sales pages
               </div>
            </div>
         </div>
         <div class="col-md-3 col-12 mt20 mt-md50">
            <div class="blue-box">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/b2.png" class="img-fluid mx-auto d-block">
               <div class="f-md-24 f-28 lh140 w400 text-center white-clr mt10 mt-md10">
               Google Ads
               </div>
            </div>
         <!--</div>-->
         <!--<div class="col-md-3 col-12 mt20 mt-md50">-->
         <!--   <div class="blue-box">-->
         <!--      <img src="assets/images/b3.png" class="img-fluid mx-auto d-block">-->
         <!--      <div class="f-md-24 f-28 lh140 w400 text-center white-clr mt10 mt-md10">-->
         <!--      Emails-->
         <!--      </div>-->
         <!--   </div>-->
         </div>
         <div class="col-md-3 col-12 mt20 mt-md50">
            <div class="blue-box">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/b3.png" class="img-fluid mx-auto d-block">
               <div class="f-md-24 f-28 lh140 w400 text-center white-clr mt10 mt-md10">
               Articles
               </div>
            </div>
         </div>
         <div class="col-md-3 col-12 mt20 mt-md50">
            <div class="blue-box">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/b5.png" class="img-fluid mx-auto d-block">
               <div class="f-md-24 f-28 lh140 w400 text-center white-clr mt10 mt-md10">
               Video descriptions
               </div>
            </div>
         </div>
         <div class="col-md-3 col-12 mt20 mt-md50">
            <div class="blue-box">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/b6.png" class="img-fluid mx-auto d-block">
               <div class="f-md-24 f-28 lh140 w400 text-center white-clr mt10 mt-md10">
               Business Plans
               </div>
            </div>
         </div>
         <div class="col-md-3 col-12 mt20 mt-md50">
            <div class="blue-box">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/b7.png" class="img-fluid mx-auto d-block">
               <div class="f-md-24 f-28 lh140 w400 text-center white-clr mt10 mt-md10">
               Research Papers
               </div>
            </div>
         </div>
         <div class="col-md-3 col-12 mt20 mt-md50">
            <div class="blue-box">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/b8.png" class="img-fluid mx-auto d-block">
               <div class="f-md-24 f-28 lh140 w400 text-center white-clr mt10 mt-md10">
               Email Marketing
               </div>
            </div>
         </div>
         <div class="col-md-3 col-12 mt20 mt-md50">
            <div class="blue-box">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/b9.png" class="img-fluid mx-auto d-block">
               <div class="f-md-24 f-28 lh140 w400 text-center white-clr mt10 mt-md10">
               Pro-Level Email Marketing
               </div>
            </div>
         </div>
         <div class="col-md-3 col-12 mt20 mt-md50">
            <div class="blue-box">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/b10.png" class="img-fluid mx-auto d-block">
               <div class="f-md-24 f-28 lh140 w400 text-center white-clr mt10 mt-md10">
               Social Media
               </div>
            </div>
         </div>
         <div class="col-md-3 col-12 mt20 mt-md50">
            <div class="blue-box">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/b11.png" class="img-fluid mx-auto d-block">
               <div class="f-md-24 f-28 lh140 w400 text-center white-clr mt10 mt-md10">
               YouTube
               </div>
            </div>
         </div>
         <div class="col-md-3 col-12 mt20 mt-md50">
            <div class="blue-box">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/b12.png" class="img-fluid mx-auto d-block">
               <div class="f-md-24 f-28 lh140 w400 text-center white-clr mt10 mt-md10">
               Twitter
               </div>
            </div>
         </div>
         <div class="col-md-3 col-12 mt20 mt-md50">
            <div class="blue-box">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/b13.png" class="img-fluid mx-auto d-block">
               <div class="f-md-24 f-28 lh140 w400 text-center white-clr mt10 mt-md10">
               LinkedIn Sales
               </div>
            </div>
         </div>
         <div class="col-md-3 col-12 mt20 mt-md50">
            <div class="blue-box">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/b14.png" class="img-fluid mx-auto d-block">
               <div class="f-md-24 f-28 lh140 w400 text-center white-clr mt10 mt-md10">
               SalesForce
               </div>
            </div>
         </div>
         <div class="col-md-3 col-12 mt20 mt-md50">
            <div class="blue-box">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/b15.png" class="img-fluid mx-auto d-block">
               <div class="f-md-24 f-28 lh140 w400 text-center white-clr mt10 mt-md10">
               SEO
               </div>
            </div>
         </div>
         <div class="col-md-3 col-12 mt20 mt-md50">
            <div class="blue-box">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/b16.png" class="img-fluid mx-auto d-block">
               <div class="f-md-24 f-28 lh140 w400 text-center white-clr mt10 mt-md10">
               Organic Traffic
               </div>
            </div>
         </div>
         <div class="col-md-3 col-12 mt20 mt-md50">
            <div class="blue-box">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/b17.png" class="img-fluid mx-auto d-block">
               <div class="f-md-24 f-28 lh140 w400 text-center white-clr mt10 mt-md10">
               Advertising
               </div>
            </div>
         </div>
         <div class="col-md-3 col-12 mt20 mt-md50">
            <div class="blue-box">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/b18.png" class="img-fluid mx-auto d-block">
               <div class="f-md-24 f-28 lh140 w400 text-center white-clr mt10 mt-md10">
               Instagram
               </div>
            </div>
         </div>
         <div class="col-md-3 col-12 mt20 mt-md50">
            <div class="blue-box">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/b19.png" class="img-fluid mx-auto d-block">
               <div class="f-md-24 f-28 lh140 w400 text-center white-clr mt10 mt-md10">
               FaceBook
               </div>
            </div>
         </div>
         <div class="col-md-3 col-12 mt20 mt-md50">
            <div class="blue-box">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/b20.png" class="img-fluid mx-auto d-block">
               <div class="f-md-24 f-28 lh140 w400 text-center white-clr mt10 mt-md10">
               Google Ads Work
               </div>
            </div>
         </div>
         <div class="col-md-3 col-12 mt20 mt-md50">
            <div class="blue-box">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/b21.png" class="img-fluid mx-auto d-block">
               <div class="f-md-24 f-28 lh140 w400 text-center white-clr mt10 mt-md10">
               Social Media Marketing
               </div>
            </div>
         </div>
         <div class="col-md-3 col-12 mt20 mt-md50">
            <div class="blue-box">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/b22.png" class="img-fluid mx-auto d-block">
               <div class="f-md-24 f-28 lh140 w400 text-center white-clr mt10 mt-md10">
               FaceBook Ads
               </div>
            </div>
         </div>
         <div class="col-md-3 col-12 mt20 mt-md50">
            <div class="blue-box">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/b23.png" class="img-fluid mx-auto d-block">
               <div class="f-md-24 f-28 lh140 w400 text-center white-clr mt10 mt-md10">
               Language
               </div>
            </div>
         </div>
         <div class="col-md-3 col-12 mt20 mt-md50">
            <div class="blue-box">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/b24.png" class="img-fluid mx-auto d-block">
               <div class="f-md-24 f-28 lh140 w400 text-center white-clr mt10 mt-md10">
               Excel
               </div>
            </div>
         </div>
         <div class="col-md-3 col-12 mt20 mt-md50">
            <div class="blue-box">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/b25.png" class="img-fluid mx-auto d-block">
               <div class="f-md-24 f-28 lh140 w400 text-center white-clr mt10 mt-md10">
               Google Sheet
               </div>
            </div>
         </div>
         <div class="col-md-3 col-12 mt20 mt-md50">
            <div class="blue-box">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/b26.png" class="img-fluid mx-auto d-block">
               <div class="f-md-24 f-28 lh140 w400 text-center white-clr mt10 mt-md10">
               Etsy
               </div>
            </div>
         </div>
         <div class="col-md-3 col-12 mt20 mt-md50">
            <div class="blue-box">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/b27.png" class="img-fluid mx-auto d-block">
               <div class="f-md-24 f-28 lh140 w400 text-center white-clr mt10 mt-md10">
               SMMA
               </div>
            </div>
         </div>
         <div class="col-md-3 col-12 mt20 mt-md50">
            <div class="blue-box">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/b28.png" class="img-fluid mx-auto d-block">
               <div class="f-md-24 f-28 lh140 w400 text-center white-clr mt10 mt-md10">
               SaaS
               </div>
            </div>
         </div>
         <div class="col-md-3 col-12 mt20 mt-md50">
            <div class="blue-box">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/b29.png" class="img-fluid mx-auto d-block">
               <div class="f-md-24 f-28 lh140 w400 text-center white-clr mt10 mt-md10">
               Affiliate Marketing
               </div>
            </div>
         </div>
         <div class="col-md-3 col-12 mt20 mt-md50">
            <div class="blue-box">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/b30.png" class="img-fluid mx-auto d-block">
               <div class="f-md-24 f-28 lh140 w400 text-center white-clr mt10 mt-md10">
               Crypto
               </div>
            </div>
         </div>
         <div class="col-md-3 col-12 mt20 mt-md50">
            <div class="blue-box">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/b31.png" class="img-fluid mx-auto d-block">
               <div class="f-md-24 f-28 lh140 w400 text-center white-clr mt10 mt-md10">
               NFT Trading
               </div>
            </div>
         </div>
         <div class="col-md-3 col-12 mt20 mt-md50">
            <div class="blue-box">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/b32.png" class="img-fluid mx-auto d-block">
               <div class="f-md-24 f-28 lh140 w400 text-center white-clr mt10 mt-md10">
               De-Fi Trading
               </div>
            </div>
         </div>
         <div class="col-md-3 col-12 mt20 mt-md50">
            <div class="blue-box">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/b33.png" class="img-fluid mx-auto d-block">
               <div class="f-md-24 f-28 lh140 w400 text-center white-clr mt10 mt-md10">
               Prompt Engineering
               </div>
            </div>
         </div>
         <div class="col-md-3 col-12 mt20 mt-md50">
            <div class="blue-box">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/b34.png" class="img-fluid mx-auto d-block">
               <div class="f-md-24 f-28 lh140 w400 text-center white-clr mt10 mt-md10">
               High Ticket Clients
               </div>
            </div>
         </div>
         <div class="col-md-3 col-12 mt20 mt-md50">
            <div class="blue-box">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/b35.png" class="img-fluid mx-auto d-block">
               <div class="f-md-24 f-28 lh140 w400 text-center white-clr mt10 mt-md10">
               High Ticket Conversion
               </div>
            </div>
         </div>
         <div class="col-md-3 col-12 mt20 mt-md50">
            <div class="blue-box">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/b36.png" class="img-fluid mx-auto d-block">
               <div class="f-md-24 f-28 lh140 w400 text-center white-clr mt10 mt-md10">
               Sales Funnel
               </div>
            </div>
         </div>
         <div class="col-md-3 col-12 mt20 mt-md50">
            <div class="blue-box">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/b37.png" class="img-fluid mx-auto d-block">
               <div class="f-md-24 f-28 lh140 w400 text-center white-clr mt10 mt-md10">
               Landing Pages
               </div>
            </div>
         </div>
         <div class="col-md-3 col-12 mt20 mt-md50">
            <div class="blue-box">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/b38.png" class="img-fluid mx-auto d-block">
               <div class="f-md-24 f-28 lh140 w400 text-center white-clr mt10 mt-md10">
               Cold Calling
               </div>
            </div>
         </div>
         <div class="col-md-3 col-12 mt20 mt-md50">
            <div class="blue-box">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/b39.png" class="img-fluid mx-auto d-block">
               <div class="f-md-24 f-28 lh140 w400 text-center white-clr mt10 mt-md10">
               OutSide Sales
               </div>
            </div>
         </div>
         <div class="col-md-3 col-12 mt20 mt-md50">
            <div class="blue-box">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/b40.png" class="img-fluid mx-auto d-block">
               <div class="f-md-24 f-28 lh140 w400 text-center white-clr mt10 mt-md10">
               Sales Engineering
               </div>
            </div>
         </div>
         <div class="col-md-3 col-12 mt20 mt-md50">
            <div class="blue-box">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/b41.png" class="img-fluid mx-auto d-block">
               <div class="f-md-24 f-28 lh140 w400 text-center white-clr mt10 mt-md10">
               Cause Related Marketing
               </div>
            </div>
         </div>
         <div class="col-md-3 col-12 mt20 mt-md50">
            <div class="blue-box">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/b42.png" class="img-fluid mx-auto d-block">
               <div class="f-md-24 f-28 lh140 w400 text-center white-clr mt10 mt-md10">
               ChatBot Marketing
               </div>
            </div>
         </div>
         <div class="col-md-3 col-12 mt20 mt-md50">
            <div class="blue-box">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/b43.png" class="img-fluid mx-auto d-block">
               <div class="f-md-24 f-28 lh140 w400 text-center white-clr mt10 mt-md10">
               Higy Ticket Webinar
               </div>
            </div>
         </div>
         <div class="col-md-3 col-12 mt20 mt-md50">
            <div class="blue-box">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/b44.png" class="img-fluid mx-auto d-block">
               <div class="f-md-24 f-28 lh140 w400 text-center white-clr mt10 mt-md10">
               Live Social Commerce
               </div>
            </div>
         </div>
         <div class="col-md-3 col-12 mt20 mt-md50">
            <div class="blue-box">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/b45.png" class="img-fluid mx-auto d-block mt0 mt-md10">
               <div class="f-md-24 f-28 lh140 w400 text-center white-clr mt10 mt-md10">
               Polls and Quizzes
               </div>
            </div>
         </div>
         <div class="col-md-3 col-12 mt20 mt-md50">
            <div class="blue-box">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/b46.png" class="img-fluid mx-auto d-block">
               <div class="f-md-24 f-28 lh140 w400 text-center white-clr mt10 mt-md10">
               User-Generated Content
               </div>
            </div>
         </div>
         <div class="col-md-3 col-12 mt20 mt-md50">
            <div class="blue-box">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/b47.png" class="img-fluid mx-auto d-block mt0 mt-md10">
               <div class="f-md-24 f-28 lh140 w400 text-center white-clr mt10 mt-md10">
               Pinterest
               </div>
            </div>
         </div>
         <div class="col-md-3 col-12 mt20 mt-md50">
            <div class="blue-box">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/b48.png" class="img-fluid mx-auto d-block mt0 mt-md10">
               <div class="f-md-24 f-28 lh140 w400 text-center white-clr mt10 mt-md10">
               B2B Sales
               </div>
            </div>
         </div>
         <div class="col-md-3 col-12 mt20 mt-md50">
            <div class="blue-box">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/b49.png" class="img-fluid mx-auto d-block mt0 mt-md10">
               <div class="f-md-24 f-28 lh140 w400 text-center white-clr mt10 mt-md10">
               B2C Sales
               </div>
            </div>
         </div>
         <div class="col-md-3 col-12 mt20 mt-md50">
            <div class="blue-box">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/b50.png" class="img-fluid mx-auto d-block mt0 mt-md10 ">
               <div class="f-md-24 f-28 lh140 w400 text-center white-clr mt10 mt-md10">
               Trade Shows
               </div>
            </div>
         </div>
         <div class="col-md-3 col-12 mt20 mt-md50">
            <div class="blue-box">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/b51.png" class="img-fluid mx-auto d-block mt0 mt-md10">
               <div class="f-md-24 f-28 lh140 w400 text-center white-clr mt10 mt-md10">
               Digital Product
               </div>
            </div>
         </div>
         <div class="col-md-3 col-12 mt20 mt-md50">
            <div class="blue-box">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/b52.png" class="img-fluid mx-auto d-block">
               <div class="f-md-24 f-28 lh140 w400 text-center white-clr mt10 mt-md10">
               Influencer Partnership
               </div>
            </div>
         </div>
         <div class="col-md-3 col-12 mt20 mt-md50">
            <div class="blue-box">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/b53.png" class="img-fluid mx-auto d-block">
               <div class="f-md-24 f-28 lh140 w400 text-center white-clr mt10 mt-md10">
               No-Code App Builder
               </div>
            </div>
         </div>
         <div class="col-md-3 col-12 mt20 mt-md50">
            <div class="blue-box">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/b54.png" class="img-fluid mx-auto d-block">
               <div class="f-md-24 f-28 lh140 w400 text-center white-clr mt10 mt-md10">
               Health Care
               </div>
            </div>
         </div>
         <div class="col-md-3 col-12 mt20 mt-md50">
            <div class="blue-box">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/b55.png" class="img-fluid mx-auto d-block">
               <div class="f-md-24 f-28 lh140 w400 text-center white-clr mt10 mt-md10">
               Augmented Reality
               </div>
            </div>
         </div>
         <div class="col-md-3 col-12 mt20 mt-md50">
            <div class="blue-box">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/b56.png" class="img-fluid mx-auto d-block">
               <div class="f-md-24 f-28 lh140 w400 text-center white-clr mt10 mt-md10">
               Social Media Followers
               </div>
            </div>
         </div>
         <div class="col-md-3 col-12 mt20 mt-md50">
            <div class="blue-box">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/b57.png" class="img-fluid mx-auto d-block">
               <div class="f-md-24 f-28 lh140 w400 text-center white-clr mt10 mt-md10">
               Product Scaling
               </div>
            </div>
         </div>
         <div class="col-md-3 col-12 mt20 mt-md50">
            <div class="blue-box">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/b58.png" class="img-fluid mx-auto d-block mt0 mt-md10">
               <div class="f-md-24 f-28 lh140 w400 text-center white-clr mt10 mt-md10">
               E-Commerce
               </div>
            </div>
         </div>
         <div class="col-md-3 col-12 mt20 mt-md50">
            <div class="blue-box">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/b59.png" class="img-fluid mx-auto d-block mt0 mt-md10">
               <div class="f-md-24 f-28 lh140 w400 text-center white-clr mt10 mt-md10">
               TikTok
               </div>
            </div>
         </div>
         <div class="col-md-3 col-12 mt20 mt-md50">
            <div class="blue-box">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/b60.png" class="img-fluid mx-auto d-block mt0 mt-md10">
               <div class="f-md-24 f-28 lh140 w400 text-center white-clr mt10 mt-md10">
                  Airbnb
               </div>
            </div>
         </div>
         <div class="col-md-3 col-12 mt20 mt-md50">
            <div class="blue-box">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/b61.png" class="img-fluid mx-auto d-block mt0 mt-md10">
               <div class="f-md-24 f-28 lh140 w400 text-center white-clr mt10 mt-md10">
                  Jailbreak
               </div>
            </div>
         </div>
         <div class="col-md-3 col-12 mt20 mt-md50">
            <div class="blue-box">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/b62.png" class="img-fluid mx-auto d-block mt0 mt-md10">
               <div class="f-md-24 f-28 lh140 w400 text-center white-clr mt10 mt-md10">
                  Personal Growth
               </div>
            </div>
         </div>
         <div class="col-md-3 col-12 mt20 mt-md50">
            <div class="blue-box">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/b63.png" class="img-fluid mx-auto d-block mt0 mt-md10">
               <div class="f-md-24 f-28 lh140 w400 text-center white-clr mt10 mt-md10">
                  Legal Advisory
               </div>
            </div>
         </div>
         <div class="col-md-3 col-12 mt20 mt-md50">
            <div class="blue-box">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/b64.png" class="img-fluid mx-auto d-block mt0 mt-md10">
               <div class="f-md-24 f-28 lh140 w400 text-center white-clr mt10 mt-md10">
                  HR Expert
               </div>
            </div>
         </div>
         <div class="col-md-3 col-12 mt20 mt-md50">
            <div class="blue-box">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/b65.png" class="img-fluid mx-auto d-block">
               <div class="f-md-24 f-28 lh140 w400 text-center white-clr mt10 mt-md10">
                  Finance Advisory
               </div>
            </div>
         </div>
         <div class="col-md-3 col-12 mt20 mt-md50">
            <div class="blue-box">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/b66.png" class="img-fluid mx-auto d-block">
               <div class="f-md-24 f-28 lh140 w400 text-center white-clr mt10 mt-md10">
               Entertainment
               </div>
            </div>
         </div>
         <div class="col-md-3 col-12 mt20 mt-md50">
            <div class="blue-box">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/b67.webp" class="img-fluid mx-auto d-block">
               <div class="f-md-24 f-28 lh140 w400 text-center white-clr mt10 mt-md10">
               Relationship
                </div>
            </div>
         </div>
         <div class="col-md-3 col-12 mt20 mt-md50">
            <div class="blue-box">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/b68.webp" class="img-fluid mx-auto d-block">
               <div class="f-md-24 f-28 lh140 w400 text-center white-clr mt10 mt-md10">
               Healthcare
                </div>
            </div>
         </div>
         <div class="col-md-3 col-12 mt20 mt-md50">
            <div class="blue-box">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/b69.webp" class="img-fluid mx-auto d-block">
               <div class="f-md-24 f-28 lh140 w400 text-center white-clr mt10 mt-md10">
               And Many More...
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- grey section end -->
<!-- white section start -->
<!-- <div class="white-section1">
   <div class="container">
      <div class="row">
         <div class="col-12 col-md-12">
            <div class="f-md-45 f-28 lh140 w600 text-center mt10 mt-md10">
                  Here are Some More <u>Amazing Features</u>
            </div>
         </div>
      </div>
      <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 mt20 mt-md50 gap30">
         <div class="col">
            <div class="amzing-box text-center">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/unlimited-businesses.webp" alt="Unlimited Businesses" class="mx-auto d-block img-fluid  ">
               <div class="f-20 f-md-22 w600 lh140 black-clr mt20">
               Create Unlimited Businesses/Sub-Domains
               </div>
            </div>
         </div>
         <div class="col mt20">
            <div class="amzing-box text-center">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/team-management.webp" alt="Single Dashboard to Manage All Businesses &amp; Clients" class="mx-auto d-block img-fluid  ">
               <div class="f-20 f-md-22 w600 lh140 black-clr mt20">
                  Single Dashboard to Manage All Businesses &amp; Clients
               </div>
            </div>
         </div>
         <div class="col mt20">
            <div class="amzing-box text-center ">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/ssl.webp" alt="Automatic &amp; FREE SSL Encryption" class="mx-auto d-block img-fluid ">
               <div class="f-20 f-md-22 w600 lh140 black-clr mt20">
                  Automatic &amp; FREE SSL Encryption
               </div>
            </div>
         </div>
         <div class="col  mt20 mt-md50">
            <div class="amzing-box text-center">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/cloud1.webp" alt="100% Cloud Based - No Hosting/Installation Required" class="mx-auto d-block img-fluid ">
               <div class="f-20 f-md-22 w600 lh140 black-clr mt20">
                  100% Cloud Based - No Hosting/Installation Required
               </div>
            </div>
         </div>
        
         <div class="col mt20 mt-md50">
            <div class="amzing-box text-center ">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/seo.webp" alt="Inbuilt Page SEO for Better SERP Ranking &amp; Traffic" class="mx-auto d-block img-fluid ">
               <div class="f-20 f-md-22 w600 lh140 black-clr mt20">
               Advanced Integrations with AR, webinar’s, CRM, pixabay & other file storage services.
               </div>
            </div>
         </div>
         <div class="col mt20 mt-md50">
            <div class="amzing-box text-center ">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/easy.webp" alt="Easy and Intuitive to Use Software" class="mx-auto d-block img-fluid ">
               <div class="f-20 f-md-22 w600 lh140 black-clr mt20">
                  Easy and Intuitive to Use Software
               </div>
            </div>
         </div>
         <div class="col">
            <div class="amzing-box text-center mt20 mt-md50">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/training.webp" alt="Complete Step-by-Step Video Training and Tutorials" class="mx-auto d-block img-fluid ">
               <div class="f-20 f-md-22 w600 lh140 black-clr mt20">
               Complete Step-by-Step Video Training and Tutorials Included
               </div>
            </div>
         </div>
         <div class="col">
            <div class="amzing-box text-center mt20 mt-md50">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/no-code.webp" alt="No Coding, Design or Technical Skills Required" class="mx-auto d-block img-fluid ">
               <div class="f-20 f-md-22 w600 lh140 black-clr mt20">
                  No Coding, Design or Technical Skills Required
               </div>
            </div>
         </div>
         <div class="col">
            <div class="amzing-box text-center mt20 mt-md50">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/software.webp" alt="Regular Updates" class="mx-auto d-block img-fluid ">
               <div class="f-20 f-md-22 w600 lh140 black-clr mt20">
                  Regular Updates
               </div>
            </div>
         </div>
      </div>
      <div class="row align-items-center justify-content-center mt30 mt-md50">
         <div class="col-12 col-md-4 mt-md50">
            <div class="much-more-box">
               <div class="f-20 f-md-22 w600 lh140 black-clr text-center">
                  And Much More...
               </div>
            </div>
         </div>
      </div>
   </div>
</div> -->
<!-- white section End -->
<!-- DEMO SECTION START -->
<div class="white-section">
   <div class="container">
      <div class="row">
         <div class="col-12">
            <div class="f-md-50 f-28 w700 lh140 text-center black-clr">
               Watch The Demo
               <br class="d-none d-md-block">
               Discover How Easy & Powerful It Is
            </div>
            <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/down-arrow.webp" class="img-fluid mx-auto d-block mt20 mt-md30">
         </div>
         <div class="col-12 col-md-8 mx-auto mt-md40 mt20">
            <div class="video-box">
               <!-- <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/video-img.webp" class="img-fluid mx-auto d-block"> -->
               <div style="padding-bottom: 56.25%;position: relative;"><iframe src="https://mavas.oppyo.com/video/embed/iz1d4yrhe9"
               style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
               box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- DEMO SECTION END -->
<!-- Grey section Start-->
<div class="blue-section">
   <div class="container">
      <div class="row">
         <div class="col-12 text-center">
            <div class="f-md-45 f-24 w600 text-center white-clr">
            Here's a Never-Ending List of Hungry Client Ready to Pay You for MAVAS Services…  

            </div>
         </div>
      </div>
      <div class="row row-cols-2 row-cols-md-3 gap30 justify-content-center mt20 mt-md50">
         <div class="col">
            <div class="feature-list1">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/copy-writers.png" class="img-fluid mx-auto d-block" alt="Business Coaches">
               <div class="f-18 f-md-20 w500 lh140 mt15">
               Copy Writers
               </div>
            </div>
         </div>
         <div class="col mt-md-0">
            <div class="feature-list1">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/affiliate-marketers.png" class="img-fluid mx-auto d-block" alt="Business Coaches">
               <div class="f-18 f-md-20 w500 lh140 mt15">
               Affiliate Marketers
               </div>
            </div>
         </div>
         <div class="col mt20 mt-md-0">
            <div class="feature-list1">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/email-marketers.png" class="img-fluid mx-auto d-block" alt="Business Coaches">
               <div class="f-18 f-md-20 w500 lh140 mt15">
               Email Marketers
               </div>
            </div>
         </div>  
         <div class="col mt20 mt-md50">
            <div class="feature-list1">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/video-marketers.png" class="img-fluid mx-auto d-block" alt="Business Coaches">
               <div class="f-18 f-md-20 w500 lh140 mt15">
               Video Marketers
               </div>
            </div>
         </div>   
         <div class="col mt20 mt-md50">
            <div class="feature-list1">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/social-media-managers.png" class="img-fluid mx-auto d-block" alt="Business Coaches">
               <div class="f-18 f-md-20 w500 lh140 mt15">
               Social Media Managers
               </div>
            </div>
         </div>   
         <div class="col mt20 mt-md50">
            <div class="feature-list1">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/business-coaches.png" class="img-fluid mx-auto d-block" alt="Business Coaches">
               <div class="f-18 f-md-20 w500 lh140 mt15">
                  Business Coaches 
               </div>
            </div>
         </div>
         <!-- <div class="col mt-md0">
            <div class="feature-list1">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/n2.webp" class="img-fluid mx-auto d-block" alt="Affiliate Marketers">
               <div class="f-18 f-md-20 w500 lh140 mt15">                 
                  Affiliate Marketers
               </div>
            </div>
         </div> -->
         <div class="col mt20 mt-md50">
            <div class="feature-list1">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/n3.webp" class="img-fluid mx-auto d-block" alt="E-Com Sellers">
               <div class="f-18 f-md-20 w500 lh140 mt15">
                  E-Com Sellers
               </div>
            </div>
         </div>
         <div class="col mt20 mt-md50">
            <div class="feature-list1">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/n4.webp" class="img-fluid mx-auto d-block" alt="All Info">
               <div class="f-18 f-md-20 w500 lh140 mt15">
                  All Info  
               </div>
            </div>
         </div>
         <div class="col mt20 mt-md50">
            <div class="feature-list1">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/n5.webp" class="img-fluid mx-auto d-block" alt="Gym">
               <div class="f-18 f-md-20 w500 lh140 mt15">
                  Gym 
               </div>
            </div>
         </div>
         <div class="col mt20 mt-md50">
            <div class="feature-list1">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/n6.webp" class="img-fluid mx-auto d-block" alt="Music Classes">
               <div class="f-18 f-md-20 w500 lh140 mt15">
                  Music Classes
               </div>
            </div>
         </div>
         <div class="col mt20 mt-md50">
            <div class="feature-list1">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/n7.webp" class="img-fluid mx-auto d-block" alt="Sports Clubs">
               <div class="f-18 f-md-20 w500 lh140 mt15">
                  Sports Clubs
               </div>
            </div>
         </div>
         <div class="col mt20 mt-md50">
            <div class="feature-list1">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/n8.webp" class="img-fluid mx-auto d-block" alt="Bars">
               <div class="f-18 f-md-20 w500 lh140 mt15">
                  Bars  
               </div>
            </div>
         </div>
         <div class="col mt20 mt-md50">
            <div class="feature-list1">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/n9.webp" class="img-fluid mx-auto d-block" alt="Restaurants">
               <div class="f-18 f-md-20 w500 lh140 mt15">
                  Restaurants 
               </div>
            </div>
         </div>
         <div class="col mt20 mt-md50">
            <div class="feature-list1">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/n10.webp" class="img-fluid mx-auto d-block" alt="Hotels">
               <div class="f-18 f-md-20 w500 lh140 mt15">
                  Hotels
               </div>
            </div>
         </div>
         <div class="col mt20 mt-md50">
            <div class="feature-list1">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/n11.webp" class="img-fluid mx-auto d-block" alt="Schools">
               <div class="f-18 f-md-20 w500 lh140 mt15">
                  Schools
               </div>
            </div>
         </div>
         <div class="col mt20 mt-md50">
            <div class="feature-list1">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/n12.webp" class="img-fluid mx-auto d-block" alt="Churches">
               <div class="f-18 f-md-20 w500 lh140 mt15">
                  Churches  
               </div>
            </div>
         </div>
         <div class="col mt20 mt-md50">
            <div class="feature-list1">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/n13.webp" class="img-fluid mx-auto d-block" alt="Taxi Services">
               <div class="f-18 f-md-20 w500 lh140 mt15">
                  Taxi Services 
               </div>
            </div>
         </div>
         <div class="col mt20 mt-md50">
            <div class="feature-list1">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/n14.webp" class="img-fluid mx-auto d-block" alt="Garage Owners">
               <div class="f-18 f-md-20 w500 lh140 mt15">
                  Garage Owners
               </div>
            </div>
         </div>
         <div class="col mt20 mt-md50">
            <div class="feature-list1">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/n15.webp" class="img-fluid mx-auto d-block" alt="Dentists">
               <div class="f-18 f-md-20 w500 lh140 mt15">
                  Dentists
               </div>
            </div>
         </div>
         <div class="col mt20 mt-md50">
            <div class="feature-list1">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/n16.webp" class="img-fluid mx-auto d-block" alt="Carpenters">
               <div class="f-18 f-md-20 w500 lh140 mt15">
                  Carpenters  
               </div>
            </div>
         </div>
         <div class="col mt20 mt-md50">
            <div class="feature-list1">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/n17.webp" class="img-fluid mx-auto d-block" alt="Chiropractors">
               <div class="f-18 f-md-20 w500 lh140 mt15">
                  Chiropractors 
               </div>
            </div>
         </div>
         <!-- <div class="col mt20 mt-md50">
            <div class="feature-list1">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/n18.webp" class="img-fluid mx-auto d-block" alt="LockSmiths">
               <div class="f-18 f-md-20 w500 lh140 mt15">
                  Locksmiths
               </div>
            </div>
         </div> -->
         <div class="col mt20 mt-md50">
            <div class="feature-list1">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/n19.webp" class="img-fluid mx-auto d-block" alt="Home Tutors">
               <div class="f-18 f-md-20 w500 lh140 mt15">
                  Home Tutors
               </div>
            </div>
         </div>
         <div class="col mt20 mt-md50">
            <div class="feature-list1">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/n20.webp" class="img-fluid mx-auto d-block" alt="Real-Estate">
               <div class="f-18 f-md-20 w500 lh140 mt15">
                  Real-Estate  
               </div>
            </div>
         </div>
         <div class="col mt20 mt-md50">
            <div class="feature-list1">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/n21.webp" class="img-fluid mx-auto d-block" alt="Lawyers">
               <div class="f-18 f-md-20 w500 lh140 mt15">
                  Lawyers 
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- Grey Section End-->

<!-- Deep Funnel Section Start -->
<div class="deep-funnel-sec">
   <div class="container">
      <div class="row">
         <div class="col-12 text-center">
            <div class="f-28 f-md-50 lh140 w700 black-clr">
               Our Deep & High Converting Sales Funnel
            </div>
            <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/funnel.webp" alt="Funnel" class="mx-auto d-block img-fluid  mt20 mt-md80">
         </div>
      </div>
   </div>
</div>
<!-- Deep Funnel Section End -->
<!-- Exciting Launch Section Start -->
<div class="white-section">
   <div class="container">
   <div class="row">
         <div class="col-12 text-center">
            <div class="f-md-50 f-28 black-clr w700 lh140">
            This Exciting Launch Event Is Divided <br class="d-none d-md-block"> Into 2 Phases
            </div>
         </div>
      </div>
      <div class="row mt20 mt-md50 align-items-center">
         <div class="col-12 col-md-6">
            <div class="f-24 f-md-34 lh140 black-clr w700">
               To Make You Max Commissions
            </div>
            <ul class="launch-list f-md-20 f-18 w400 mb0 mt-md30 mt20 pl20">
               <li>All Leads Are Hardcoded</li>
               <li>We'll Re-Market Your Leads Heavily</li>
               <li>Pitch Bundle Offer On Webinars</li>
            </ul>
         </div>
         <div class="col-12 col-md-6 mt20 mt-md0">
            <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/phase1.webp" class="img-fluid d-block mx-auto " alt="Phase1">
         </div>
      </div>
      <div class="row mt20 mt-md50 align-items-center">
         <div class="col-12 col-md-6 order-md-2">
            <div class="f-24 f-md-34 lh140 black-clr w700">
               Big Opening Contest & Bundle Offer
            </div>
            <ul class="launch-list f-md-20 f-18 w400 mb0 mt-md30 mt20 pl20">
               <li>High in Demand Product with Top Conversion</li>
               <li>Deep Funnel To Make You Double Digit EPCs</li>
               <li>Earn Up To $415/Sale</li>
               <li>Huge $10,000 JV Prizes</li>
            </ul>
         </div>
         <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
            <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/phase2.webp" class="img-fluid d-block mx-auto " alt="Phase2">
         </div>
      </div>
   </div>
</div>
<!-- Exciting Launch Section End -->
<!-- Prize Value Section Start -->
<div class="prize-value">
   <div class="container">
      <div class="row">
         <div class="col-12 text-center">
            <div class="f-md-50 f-28 w700 lh140 white-clr">Get Ready to Grab Your Share of</div>
            <div class="f-md-72 f-40 w700 orange-clr lh140">$10,000 JV Prizes</div>
         </div>
      </div>
   </div>
</div>
<!-- Prize Value Section End -->
<!-- Contest Section Start -->
<div class="contest-sec">
   <div class="container">
      <div class="row">
         <div class="col-md-6">
            <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/prelaunch.webp" alt="Prelaunch" class="mx-auto d-block img-fluid ">
         </div>
         <div class="col-md-6 mt20 mt-md0">
            <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/exiciting-launch.webp" alt="Exiciting" class="mx-auto d-block img-fluid ">
         </div>
      </div>
   </div>
</div>
<!-- Contest Section End -->
<!-- Reciprocate Section Start -->
<div class="reciprocate-sec">
   <div class="container">
      <div class="row">
         <div class="col-12 text-center">
            <div class="f-md-45 f-28 lh140 w600 orange-clr">
               Do We Reciprocate?
            </div>
            <div class="f-18 f-md-20 lh140 mt20 w400 text-center white-clr">
               We've been in top positions on hundreds of launch leaderboards &amp; sent huge sales for our valued JVs.
            </div>
            <div class="f-18 f-md-20 lh140 mt20 w400 text-center white-clr">
               So, if you have a top-notch product with top conversions AND that fits our list, we would love to drive loads of sales for you. Here's just some results from our recent promotions.
            </div>
         </div>
         <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
            <div class="logos-effect">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/logos.webp" class="img-fluid d-block mx-auto " alt="Logos">
            </div>
         </div>
         <div class="col-12 f-md-28 f-24 lh140 mt20 mt-md50 w600 text-center white-clr">
            And The List Goes On And On...
         </div>
      </div>
   </div>
</div>
<!-- Reciprocate Section End-->
<!-- Contact Section Start -->
<div class="contact-section">
   <div class="container">
      <div class="container-box">
         <div class="row">
            <div class="col-12 col-md-12 text-center">
               <div class="f-28 f-md-45 w600 white-clr lh140">
                  Have any Query? Contact us Anytime
               </div>
            </div>
         </div>
         <div class="row mt20 mt-md80 justify-content-between">
            <div class="col-12 col-md-12 mx-auto">
               <div class="row">
                  <div class="col-12 col-md-4 mx-auto">
                     <div class="contact-shape">
                        <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/amit-pareek-sir.webp" class="img-fluid d-block mx-auto minus " alt="Amit Pareek">
                        <div class="f-24 f-md-30 w600  lh140 text-center white-clr mt20">
                           Dr. Amit Pareek
                        </div>
                        <div class="col-12 mt30 d-flex justify-content-center">
                           <a href="http://facebook.com/Dr.AmitPareek" class="link-text mr20">
                           <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/am-fb.webp" class="center-block " alt="Facebook">
                           </a>
                           <a href="skype:amit.pareek77" class="link-text">
                              <div class="col-12 ">
                                 <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/am-skype.webp" class="center-block " alt="Skype">
                              </div>
                           </a>
                        </div>
                     </div>
                  </div>
                  <!-- <div class="col-12 col-md-4 mt20 mt-md0">
                     <div class="contact-shape">
                        <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/tim-verdouw.webp" class="img-fluid d-block mx-auto minus " alt="Tim Verdouw">
                        <div class="f-24 f-md-30 w600  lh140 text-center white-clr mt20">
                           Tim Verdouw
                        </div>
                        <div class="col-12 mt30 d-flex justify-content-center">
                           <a href="https://www.facebook.com/tim.verdouw" class="link-text mr20">
                              <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/am-fb.webp" class="center-block " alt="Facebook">
                           </a>
                           <a href="skype:tverdouw" class="link-text">
                              <div class="col-12 ">
                                 <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/am-skype.webp" class="center-block " alt="Skype">
                              </div>
                           </a>
                        </div>
                     </div>
                  </div> -->
                  <div class="col-12 col-md-4 mt20 mt-md0 mx-auto">
                     <div class="contact-shape">
                        <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/atul-parrek-sir.webp" class="img-fluid d-block mx-auto minus " alt=" Atul Pareek">
                        <div class="f-24 f-md-30 w600  lh140 text-center white-clr mt20">
                              Atul Pareek
                        </div>
                        <div class="col-12 mt30 d-flex justify-content-center">
                        <a href="https://www.facebook.com/profile.php?id=100076904623319" class="link-text mr20">
                              <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/am-fb.webp" class="center-block " alt="Facebook">
                           </a>
                           <a href="skype:live:.cid.c3d2302c4a2815e0" class="link-text">
                              <div class="col-12 ">
                                 <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/am-skype.webp" class="center-block " alt="Skype">
                              </div>
                           </a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- Contact Section End -->
<!-- Term Section Start -->
<div class="term-sec">
   <div class="container">
      <div class="row">
         <div class="col-12">
            <div class="f-28 f-md-45 lh140 w600 black-clr text-center">
               Affiliate Promotions Terms & Conditions
            </div>
            <div class="f-16 f-md-16 lh140 p-md0 mt-md20 mt20 w400 text-center">
               YOU MUST READ AND AGREE TO THESE AFFILIATE TERMS before requesting your affiliate link and being a part of this launch. Violation of ANY of these terms is cause for immediate termination and instant removal from this launch and any other of our launches - current or past -- and you agree that your current commissions will be forfeited without recourse and you may be banned from our future launches. Some violations may also be cause for LEGAL ACTIONS.:
            </div>
            <ul class="terms-list pl0 m0 f-16 f-md-16 lh140 w400 mt20 mt-md50">
               <li>1). All email contacts MUST be your OWN opt in email list. You cannot send to lists that have been purchased or “gifted” from other vendors, buy solo ads, use safe lists, or obtained by illegal means. Email lists that are not your own are considered spam.</li>
               <li>2) You may NOT create social media pages NOR purchase domain names with the PRODUCT NAME or BRAND NAME AND you may NOT use the PRODUCT NAME or the NAME OF THE VENDOR as your “from” in your emails instead of your own name AS IF YOU ARE THE PRODUCT OWNER. This is IMPERSONATION and will not be tolerated.</li>
               <li>3) You may NOT purchase domain name(s) with the same or similar name as the PRODUCT NAME or BRAND NAME nor CLONE or otherwise copy our site and use that site to sell our product as your own. Furthermore, you may not add our product - whether purchased through us or obtained in any other manner - and sell or offer it on any type of “membership” site where multiple people have access to this product for any kind of fee or arrangement. This constitutes theft of our intellectual property rights and considered FRAUDULENT and is cause for LEGAL ACTIONS. </li>
               <li>4) You may not encourage nor ask for or show a person HOW TO REFUND their purchase from another affiliate in order for them to purchase the same product through you.</li>
               <li>5) You may not post OTO links on Review Sites because this will lead to confusion and refunds for those people who do not purchase the FE first and end up with NO LOGIN &amp; NO software or main product. </li>
               <li>6) You may not use "negative" campaigns such as "is Product Name / Owner Name a scam?" or any other method to attract controversial click thru rates that an ordinary person would deem to portray a negative view of the product. You may not use offensive nor negative domain names.</li>
               <li>7) You may not use misleading claims, inaccurate information or false testimonials (or anything that does not comply with FTC guidelines).</li>
               <li>8) You may not use gray-hat/black-hat marketing practices to drive sales or for any other reason.</li>
               <li>9) You may not give cash rebates of any kind as it may increase refund rates.</li>
               <li>10) You may not purchase from your own affiliate link. Any 'self' purchase commission may be nullified or held back.</li>
            </ul>
            <div class="f-16 f-md-16 lh140 mt20 w400">
               <span class="w600">NOTE:</span> These terms may change at any time without notice. (Please check back here regularly).
            </div>
            <div class="f-16 f-md-16 lh140 mt20 w400">
               <span class="w600">NOTE:</span> Affiliate payments will be set according to the platform rules.
            </div>     
            <div class="f-16 f-md-16 lh140 mt20 w400">
               <span class="w600">CAUTION:</span> Do not send "raw" affiliate links. Utilize redirect links in emails &amp; website campaigns instead of your direct affiliate link. This increases conversions for both of us.
            </div> 
            <div class="f-16 f-md-16 lh140 mt20 w400">
               We run a legitimate business, which means that we always correctly illustrate and represent our products and their features and benefits to the customer.
            </div>
            <div class="f-16 f-md-16 lh140 mt20 w400">
               Please make sure you do the same.
            </div>
         </div>
      </div>
   </div>
</div>
<!-- Term Section End -->
<!--Footer Section Start -->
<div class="footer-section">
   <div class="container">
      <div class="row">
         <div class="col-12 text-center">
         <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 685 120" style="max-height:55px">
  <defs>
    <style>
      .cls-1 {
        fill: url(#linear-gradient-2);
      }

      .cls-2 {
        fill: #fff;
      }

      .cls-3 {
        fill: #21e5f4;
      }

      .cls-4 {
        fill: url(#linear-gradient);
      }
    </style>
    <linearGradient id="linear-gradient" x1="0" y1="60" x2="25.31" y2="60" gradientUnits="userSpaceOnUse">
      <stop offset="0" stop-color="#b956ff"></stop>
      <stop offset="1" stop-color="#8d4ef9"></stop>
    </linearGradient>
    <linearGradient id="linear-gradient-2" x1="99.25" y1="59.94" x2="124.57" y2="59.94" gradientUnits="userSpaceOnUse">
      <stop offset="0" stop-color="#8d57f4"></stop>
      <stop offset="1" stop-color="#6e33ff"></stop>
    </linearGradient>
  </defs>
  <g id="Layer_1-2" data-name="Layer 1">
    <g>
      <g>
        <g>
          <polygon class="cls-2" points="343.95 66.06 343.95 66.06 300 .53 273.46 .53 343.96 99.29 355.45 83.2 343.95 66.06"></polygon>
          <polygon class="cls-2" points="367.54 66.26 414.45 .53 388.11 .53 355.71 48.61 367.54 66.26"></polygon>
        </g>
        <polygon class="cls-2" points="218.06 0 147.57 99.82 172.51 99.82 182.71 84.58 198.02 61.87 218.07 32.36 237.91 61.88 253.21 84.59 263.4 99.82 288.55 99.82 218.06 0"></polygon>
        <polygon class="cls-2" points="469.86 0 399.36 99.82 424.31 99.82 434.5 84.58 449.81 61.87 469.87 32.36 489.7 61.88 505.01 84.59 515.2 99.82 540.35 99.82 469.86 0"></polygon>
        <g>
          <path class="cls-2" d="m662.66,46.82l-96.97-14.26,16.04,21.03,5.2.9,69.12,11.61c3.34.56,5.78,3.45,5.78,6.83,0,3.83-3.1,6.93-6.93,6.93h-75.79l-15.37,20.13,94.46.02c14.79,0,26.78-11.99,26.78-26.78,0-13.07-9.43-24.22-22.31-26.4Z"></path>
          <polygon class="cls-2" points="685 1.08 589.71 1.08 589.71 1.06 581.89 1.06 566.5 21.21 669.63 21.21 685 1.08"></polygon>
        </g>
      </g>
      <g>
        <polygon class="cls-4" points="25.31 20.82 25.31 120 0 99.11 0 0 25.31 20.82"></polygon>
        <polygon class="cls-1" points="124.57 0 124.57 99.13 99.25 119.89 99.25 20.76 124.57 0"></polygon>
        <polygon class="cls-3" points="92.74 25.17 92.74 62.88 62.24 100.89 31.82 62.89 31.82 25.17 62.24 63.17 92.74 25.17"></polygon>
      </g>
    </g>
  </g>
</svg>
         </div>
         <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md60">
            <div class="f-md-18 f-16 w400 lh140 white-clr text-xs-center">Copyright © MAVAS 2023</div>
            <ul class="footer-ul w400 f-md-18 f-16 white-clr text-center text-md-right">
               <li><a href="https://support.oppyo.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
               <li><a href="http://getmavas.com/legal/privacy-policy.html " class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
               <li><a href="http://getmavas.com/legal/terms-of-service.html " class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
               <li><a href="http://getmavas.com/legal/disclaimer.html " class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
               <li><a href="http://getmavas.com/legal/gdpr.html " class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
               <li><a href="http://getmavas.com/legal/dmca.html " class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
               <li><a href="http://getmavas.com/legal/anti-spam.html " class="white-clr t-decoration-none">Anti-Spam</a></li>
            </ul>
         </div>
      </div>
   </div>
</div>
<!--Footer Section End -->
      <!-- timer --->
      <?php
         if ($now < $exp_date) {
         
         ?>
      <script type="text/javascript">
         // Count down milliseconds = server_end - server_now = client_end - client_now
         var server_end = <?php echo $exp_date; ?> * 1000;
         var server_now = <?php echo time(); ?> * 1000;
         var client_now = new Date().getTime();
         var end = server_end - server_now + client_now; // this is the real end time
         
         var noob = $('.countdown').length;
         
         var _second = 1000;
         var _minute = _second * 60;
         var _hour = _minute * 60;
         var _day = _hour * 24
         var timer;
         
         function showRemaining() {
             var now = new Date();
             var distance = end - now;
             if (distance < 0) {
                 clearInterval(timer);
                 document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
                 return;
             }
         
             var days = Math.floor(distance / _day);
             var hours = Math.floor((distance % _day) / _hour);
             var minutes = Math.floor((distance % _hour) / _minute);
             var seconds = Math.floor((distance % _minute) / _second);
             if (days < 10) {
                 days = "0" + days;
             }
             if (hours < 10) {
                 hours = "0" + hours;
             }
             if (minutes < 10) {
                 minutes = "0" + minutes;
             }
             if (seconds < 10) {
                 seconds = "0" + seconds;
             }
             var i;
             var countdown = document.getElementsByClassName('countdown');
             for (i = 0; i < noob; i++) {
                 countdown[i].innerHTML = '';
         
                 if (days) {
                     countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-30 f-md-40 timerbg">' + days + '</span><br><span class="f-16 w500">Days</span> </div>';
                 }
         
                 countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-30 f-md-40 timerbg">' + hours + '</span><br><span class="f-16 w500">Hours</span> </div>';
         
                 countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-30 f-md-40 timerbg">' + minutes + '</span><br><span class="f-16 w500">Mins</span> </div>';
         
                 countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-30 f-md-40 timerbg">' + seconds + '</span><br><span class="f-16 w500">Sec</span> </div>';
             }
         
         }
         timer = setInterval(showRemaining, 1000);
      </script>    

      <?php
         } else {
         echo "Times Up";
         }
         ?>

<script type="text/javascript">
// Special handling for in-app browsers that don't always support new windows
(function() {
    function browserSupportsNewWindows(userAgent) {
        var rules = [
            'FBIOS',
            'Twitter for iPhone',
            'WebView',
            '(iPhone|iPod|iPad)(?!.*Safari\/)',
            'Android.*(wv|\.0\.0\.0)'
        ];
        var pattern = new RegExp('(' + rules.join('|') + ')', 'ig');
        return !pattern.test(userAgent);
    }

    if (!browserSupportsNewWindows(navigator.userAgent || navigator.vendor || window.opera)) {
        document.getElementById('af-form-155455963').parentElement.removeAttribute('target');
    }
})();
</script>
<script type="text/javascript">
 
    (function() {
        var IE = /*@cc_on!@*/false;
        if (!IE) { return; }
        if (document.compatMode && document.compatMode == 'BackCompat') {
            if (document.getElementById("af-form-155455963")) {
                document.getElementById("af-form-155455963").className = 'af-form af-quirksMode';
            }
            if (document.getElementById("af-body-155455963")) {
                document.getElementById("af-body-155455963").className = "af-body inline af-quirksMode";
            }
            if (document.getElementById("af-header-155455963")) {
                document.getElementById("af-header-155455963").className = "af-header af-quirksMode";
            }
            if (document.getElementById("af-footer-155455963")) {
                document.getElementById("af-footer-155455963").className = "af-footer af-quirksMode";
            }
        }
    })();
 
</script>
   </body>
</html>
<!Doctype html>
<html>
   <head>
      <title>MAVAS | Bundle Deal</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=9">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
      <!------Meta Tags-------->
      <meta name="title" content="MAVAS Exclusive Bundle Deal">
      <meta name="description" content="Get MAVAS With All The Upgrades For 64% Off & Save Over $650 When You Grab This Highly-Discounted Bundle Deal">
      <meta name="keywords" content="MAVAS">
      <meta property="og:image" content="https://www.getcloudfusion.co/bundle/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Dr. Amit Pareek">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="MAVAS Exclusive Bundle Deal">
      <meta property="og:description" content="Get MAVAS With All The Upgrades For 64% Off & Save Over $650 When You Grab This Highly-Discounted Bundle Deal">
      <meta property="og:image" content="https://www.getcloudfusion.co/bundle/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="MAVAS Exclusive Bundle Deal">
      <meta property="twitter:description" content="Get MAVAS With All The Upgrades For 64% Off & Save Over $650 When You Grab This Highly-Discounted Bundle Deal">
      <meta property="twitter:image" content="https://www.getcloudfusion.co/bundle/thumbnail.png">
      <!------Meta Tags-------->
      <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
      <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
      <!-- required -->
      <link rel="stylesheet" href="../common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style.css" type="text/css">
      <script type='text/javascript' src="../common_assets/js/jquery.min.js"></script>
      <link rel="stylesheet" href="assets/css/timer.css">
      <!-- <script src="assets/js/timer.js"></script> -->

      <!-- New Timer  Start-->
         <style>
            .timer-header-top{background: #282759; padding-top:10px; padding-bottom:10px;}
         </style>  
      <script>
         (function(w, i, d, g, e, t, s) {
             if(window.businessDomain != undefined){
                 console.log("Your page have duplicate embed code. Please check it.");
                 return false;
             }
             businessDomain = 'MAVAS';
             allowedDomain = 'getcloudfusion.co';
             if(!window.location.hostname.includes(allowedDomain)){
                 console.log("Your page have not authorized. Please check it.");
                 return false;
             }
             console.log("Your script is ready...");
             w[d] = w[d] || [];
             t = i.createElement(g);
             t.async = 1;
             t.src = e;
             s = i.getElementsByTagName(g)[0];
             s.parentNode.insertBefore(t, s);
         })(window, document, '_gscq', 'script', 'https://cdn.staticdcp.com/apps/engage/smart_engage/js/config-loader.js');
         </script>
          <style>
           
         </style>  
   </head>
      <!--<div class="timer-header-top fixed-top">-->
      <!--   <div class="container">-->
      <!--      <div class="row align-items-center">-->
      <!--         <div class="col-12 col-md-3">-->
      <!--            <div class="tht-left f-14 f-md-16 white-clr w700 text-md-start text-center">-->
      <!--                Coupon Code <span class="purple-clr">"MAVAS"</span> <br class="d-block d-md-none"> for <span class="purple-clr"> $50 Discount </span> Ending In-->
      <!--            </div>-->
      <!--         </div>-->
      <!--         <div class="col-7 col-md-6 text-center ">-->
      <!--            <div class="countdown counter-white text-center hs_hours"><div class="timer-label text-center"><span class="f-16 f-md-24 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-12 f-md-15 w500 smmltd">Days</span> </div><div class="timer-label text-center"><span class="f-16 f-md-24 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-12 f-md-15 w500 smmltd">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-16 f-md-24 timerbg oswald">58&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-12 f-md-15 w500 smmltd">Mins</span> </div><div class="timer-label text-center "><span class="f-16 f-md-24 timerbg oswald">54</span><br><span class="f-12 f-md-15 w500 smmltd">Sec</span> </div></div>-->
      <!--         </div>-->
      <!--         <div class="col-5 col-md-3 text-md-end">-->
      <!--            <div class="instant-btn1"><a href="#buybundle" class="t-decoration-none">Buy Now</a></div>-->
      <!--         </div>-->
      <!--      </div>-->
      <!--   </div>-->
      <!--</div>-->
   <body>
      <!--1. Header Section Start -->
      <div class="header-section">
         <div class="container">
            <div class="row">
               <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 685 120" style="max-height:55px">
                  <defs>
                     <style>
                        .cls-1 {
                        fill: url(#linear-gradient-2);
                        }

                        .cls-2 {
                        fill: #fff;
                        }

                        .cls-3 {
                        fill: #21e5f4;
                        }

                        .cls-4 {
                        fill: url(#linear-gradient);
                        }
                     </style>
                     <linearGradient id="linear-gradient" x1="0" y1="60" x2="25.31" y2="60" gradientUnits="userSpaceOnUse">
                        <stop offset="0" stop-color="#b956ff"/>
                        <stop offset="1" stop-color="#8d4ef9"/>
                     </linearGradient>
                     <linearGradient id="linear-gradient-2" x1="99.25" y1="59.94" x2="124.57" y2="59.94" gradientUnits="userSpaceOnUse">
                        <stop offset="0" stop-color="#8d57f4"/>
                        <stop offset="1" stop-color="#6e33ff"/>
                     </linearGradient>
                  </defs>
                  <g id="Layer_1-2" data-name="Layer 1">
                     <g>
                        <g>
                           <g>
                              <polygon class="cls-2" points="343.95 66.06 343.95 66.06 300 .53 273.46 .53 343.96 99.29 355.45 83.2 343.95 66.06"/>
                              <polygon class="cls-2" points="367.54 66.26 414.45 .53 388.11 .53 355.71 48.61 367.54 66.26"/>
                              </g>
                              <polygon class="cls-2" points="218.06 0 147.57 99.82 172.51 99.82 182.71 84.58 198.02 61.87 218.07 32.36 237.91 61.88 253.21 84.59 263.4 99.82 288.55 99.82 218.06 0"/>
                              <polygon class="cls-2" points="469.86 0 399.36 99.82 424.31 99.82 434.5 84.58 449.81 61.87 469.87 32.36 489.7 61.88 505.01 84.59 515.2 99.82 540.35 99.82 469.86 0"/>
                           <g>
                              <path class="cls-2" d="m662.66,46.82l-96.97-14.26,16.04,21.03,5.2.9,69.12,11.61c3.34.56,5.78,3.45,5.78,6.83,0,3.83-3.1,6.93-6.93,6.93h-75.79l-15.37,20.13,94.46.02c14.79,0,26.78-11.99,26.78-26.78,0-13.07-9.43-24.22-22.31-26.4Z"/>
                              <polygon class="cls-2" points="685 1.08 589.71 1.08 589.71 1.06 581.89 1.06 566.5 21.21 669.63 21.21 685 1.08"/>
                           </g>
                        </g>
                        <g>
                           <polygon class="cls-4" points="25.31 20.82 25.31 120 0 99.11 0 0 25.31 20.82"/>
                           <polygon class="cls-1" points="124.57 0 124.57 99.13 99.25 119.89 99.25 20.76 124.57 0"/>
                           <polygon class="cls-3" points="92.74 25.17 92.74 62.88 62.24 100.89 31.82 62.89 31.82 25.17 62.24 63.17 92.74 25.17"/>
                        </g>
                     </g>
                  </g>
               </svg>
            </div>
            <div class="row">
            <div class="col-12 mt20 mt-md50 text-center">
               <div class="preheadline f-18 f-md-22 w600 white-clr lh140">
               <span class="preheadline-content white-clr"><u class="red-clr">WARNING :</u> <span class="uppercase">This Limited Time DISCOUNTED Bundle Offer Expires Soon</span></span>
               </div>
            </div>
         <div class="col-12 mt30 mt-md50 relative p-md0">
            <div class="mainheadline f-md-42 f-25 w400 text-center white-clr lh140">
               <div class="headline-text">
                  <span class="w700 sky-blue">#1 Super VA That Finish 100s of Marketing Tasks <span class="caveat">(Better &amp; Faster Than Any CMO Out There...)</span> </span> &amp; Trim Your Workload, Hiring/Firing Headaches, &amp; Monthly Bills
               </div>
            </div>
         </div>
               
               
               <div class="col-12 col-md-9 mx-auto mt-md30 mt20 f-18 f-md-26 w500 text-center lh140 white-clr text-capitalize">
                  Get MAVAS With All The Upgrades For 78% Off & Save Over $1028 When You Grab This Highly-Discounted Bundle Deal
               </div>
               <div class="col-12 col-md-10 mt20 mt-md30 mx-auto">
                   <div class="responsive-video">
                     <iframe src="https://mavas.dotcompal.com/video/embed/s9vi6jzkmb" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                     box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                     </div> -->
                  <!--<img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/bundle/pbb.webp" class="img-fluid d-block mx-auto " alt="ProductBox">-->
               </div>
            </div>
         </div>
      </div>
      <!--1. Header Section End -->
      <!---New Section Start---->
      <div class="new-section content-mbl-space">
      <div class="container">
             <div class="row">
                 <div class="col-12 f-md-45 f-28 w500 text-center black-clr lh170 line-center ">
                     Save $985 RIGHT AWAY-
                     <div class="gradient-clr w700 f-md-70 f-40">
                         Deal Ends Soon
                     </div>
                 </div>
             </div>
             <div class="row">
                 <div class="col-12 col-md-6">
                    <div class="f-16 f-md-18 lh140 w500">
                       <ul class="menu-list pl0 m0">
                          <li>GET Complete MAVAS Package (FE + ALL Upgrades + Agency License) </li>
                          <li>No Monthly Payment Hassles- It's Just A One-Time Payment</li>
                          <li>GET Priority Support from Our Dedicated Support Engineers</li>
                       </ul>
                    </div>
                 </div>
                 <div class="col-12 col-md-6">
                    <div class="f-16 f-md-18 lh140 w500">
                       <ul class="menu-list pl0 m0">
                          <li>Provide Top-Notch Services to Your Clients</li>
                          <li>Grab All Benefits For A Low, ONE-TIME Discounted Price</li>
                          <li>GET 30-Days Money Back Guarantee</li>
                       </ul>
                    </div>
                 </div>
              </div>
             <div class="row">
                 <div class="col-12 mt20 mt-md40 f-md-26 f-22 lh140 w400 text-center black-clr">
                     <div class="f-24 f-md-36 w500 lh150 text-center black-clr">
                         <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/bundle/hurry.png " alt="Hurry " class="mr10 hurry-img">
                         <span class="red-clr ">Hurry!</span> Special One Time Price for Limited Time Only!
                     </div>
                     <div>
                         Use Coupon Code <span class="w600 purple-clr">"MAVAS"</span> for an Additional <span class="w600 purple-clr">$50 Discount</span> 
                     </div>
                     <div class="mt20 red-clr w600">
                     <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/bundle/red-close.webp " alt="Hurry" class="mr10 close-img">
                        No Recurring | No Monthly | No Yearly
                     </div>
                     <div class="col-md-8 col-12 instant-btn mt20 f-24 f-md-30 mx-auto px0">
                         <a href="#buybundle">
                      GET Bundle Offer Now &nbsp;<i class="fa fa-angle-double-right f-32 f-md-42 angle-right" aria-hidden="true"></i>
                      </a>
                     </div>
                     <div class="col-12 mt-md20 mt20 f-18 f-md-18 w600 lh140 text-center black-clr">
                         No Download or Installation Required
                     </div>
                     <div class="col-12 mt20 mt-md20">
                         <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/bundle/compaitable-with.png" class="img-fluid mx-auto d-block">
                      </div>
                 </div>
             </div>
         </div>
      </div>
      <!--1. New Section End -->
      <!--2. Second Section Start -->
      <div class="section1">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-md-45 f-28 black-clr  text-center w700 lh140">
                     Here’s What You Get Today With MAVAS <br class="d-none d-md-block">Limited Time Discounted Bundle
                  </div>
               </div>
               <!-- AGENCY BUNDLE START -->
               <div class="col-12 mt20 mt-md70 section-margin px-md-0">
                  <div class="col-12 plan1-shape">
                     <div class="col-md-10 col-12 px-md15 mt-md0 mt20 mx-auto">
                        <div class="pimg">
                           <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/bundle/commercial.webp" class="img-fluid d-block mx-auto" alt="Commercial">
                        </div>
                     </div>
                     <div class="row p0 mt20 mx-0 mt-md0">
                        <div class="col-12 p0 text-center mt-md50 mt20">
                           <div class="title-shape">
                              <div class="f-24 f-md-38 w600 lh150 white-clr text-center">
                                 MAVAS Commercial ($37)
                              </div>
                           </div>
                        </div>
                        <div class="col-12 col-md-6 px0 px-md15">
                           <div class="f-20 f-md-20 lh150 w400 mt20 mt-md50">
                              <ul class="kaptick pl0">
                                 <li>
                                    MAVAS - Super VA Perform All Your Marketing Task Better & Faster Than Human & Any CMO Out There
                                 </li>
                                 <li>Activate MAVAS in 30 Expertise</li>
                                 <li>Inbuilt 150,000+ Niche Specific Prompts</li>
                                 <li>Access MAVAS Super VA in Different Languages</li>
                                 <li>Assign Any Task to MAVAS</li>
                                 <li>ReGenerate Any Task According to Tone & Language</li>
                                 <li>Dynamic MAVAS Generates Unique Responses each time for any particular task.</li>
                                 <li>Natural Language Processing capabilities Generate Human-like or Better interactions</li>
                                 <li>Higher Customer satisfaction due to sensitive and meaningful responses that will actually help the customers</li>
                                 <li>MAVAS Super VA Will Handle Your All Marketing Tasks, Search Images, Videos, GIF's</li>
                                 <li>Speak with MAVAS - MAVAS Receive Your Voice Commands & Generates Top-Notch Quality Work</li>
                                 <li>Siri/Alexa Like Task Assignment</li>
                                 <li>Save & Manage Completed Work in Separate Folders</li>
                                 <li>Share Unlimited Assets, Images, Videos, GIF Files</li>
                                 <li>Different Background Theme Options for Tasks</li>
                                 <li>Get FREE 1 TB Storage</li>
                              </ul>
                           </div>
                        </div>
                        <div class="col-12 col-md-6 px0 px-md15">
                           <div class="f-20 f-md-20 lh150 w400 mt0 mt-md50">
                              <ul class="kaptick pl0 p0 m0">
                                 <li>Seamless Integration with 20+ Top Autoresponders to Send Emails to Your Clients/leads on Automation</li>
                                 <li>128-Bit SSL Encryption for Maximum Security of your Data & Files</li>
                                 <li>Completely Cloud-Based Platform - No Domain, Hosting or Installation Required</li>
                                 <li>100% ADA, GDPR - SPAM Compliant</li>
                                 <li>Customized Drag & Drop Business Central Dashboard</li>
                                 <li>No Coding, Design or Tech Skills Needed</li>
                                 <li>Easy And Intuitive To Use Software With Step By Step Video Training</li>
                                 <li>100% Newbie friendly</li>
                                 <li>24*5 Customer Support</li>
                                 <li>Commercial license Included</li>
                                 <li>Unparallel Price</li>
                                 <li>BONUSES WORTH $1,988 FREE!!!</li>
                                 <li>Bonus #1 - Live Training How to Make Money Using MAVAS</li>
                                 <li>Bonus #2 -Video Training On How To Start Online Business</li>
                                 <li>Bonus #3 -Mavas Guide Workshop</li>
                                 <li>Bonus #4 -Video Training On Get Your Head Into The AI</li>
                              </ul>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- AGENCY BUNDLE END -->
               <!-- ELITE BUNDLE START -->
               <div class="col-12 px-md0 mt40 mt-md140 section-margin">
                  <div class="col-12 plan2-shape">
                     <div class="col-md-12 mx-auto mt-md0 mt20">
                        <div class="pimg">
                           <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/bundle/elite.webp" class="img-fluid d-block mx-auto" alt="Elite">
                        </div>
                     </div>
                     <div class="row mt-md0 mt20 mx-0">
                        <div class="col-12 p0 text-center mt-md50 mt20">
                           <div class="title-shape">
                              <div class="f-22 f-md-28 w600 white-clr lh150 text-center">
                                 Pro Upgrade 1- MAVAS Elite ($197)
                              </div>
                           </div>
                        </div>
                        <div class="col-12 col-md-6 px0 px-md15">
                           <div class="f-20 f-md-20 lh150 w400 mt20 mt-md50">
                              <ul class="kaptick pl0">
                                 <li><span class="w600">Create UNLIMITED WorkSpace/Subdomains</span> To Keep Each Of Your & Client's Project Separate</li>
                                 <li>Activate MAVAS in <span class="w600">100+ Expertise</span> </li>
                                 <li>Activate MAVAS in <span class="w600">50 More Languages</span> </li>
                                 <li><span class="w600">Unlimited Task Assignment</span></li>
                                 <li><span class="w600">Unlimited Work Done Management</span></li>
                                 <li><span class="w600">Unlimited Folders</span></li>
                                 <li><span class="w600">Get Your Subscribers Auto-Registered</span> For Your Webinars With Webinar Platform Integrations</li> 
                                 <li><span class="w600">Unlimited History -</span>All History Is Saved Forever</li>       
                                 <li><span class="w600">Use 150,000+ Prompt</span> to Complete your Projects</li> 
                                 <li>Setup Work Length</li>
                                 <li>Setup Work Tone</li>
                              </ul>
                           </div>
                        </div>
                        <div class="col-12 col-md-6 px0 px-md15">
                           <div class="f-20 f-md-20 lh150 w400 mt0 mt-md50">
                              <ul class="kaptick pl0 p0 m0">
                                 <li>Change Work Response language</li>
                                 <li><span class="w600">Save Completed Work, Download or Copy Direct</span></li>
                                 <li><span class="w600">Regenerate Response of</span>any Assign task</li> 
                                 <li><span class="w600">Create Separate Conversation for</span>Separate Projects</li> 
                                 <li><span class="w600">Get Unlimited Images, Videos, Gif Files</span></li>
                                 <li>Self Updating MAVAS ( Ai Stores the response & generate relevant response based on History for example - Ton/Language/Word Limit/Response Style )</li>
                                 <li><span class="w600">Built in Speech</span> to Text Converter</li>    
                                 <li>Save any Particular task to Work Done ( Fg: Blog Post, Email, VSL, Sales Copy, Ad Copy, Scripts or anything in Work Done )</li>   
                                 <li><span class="w600">Get More FREE Storage Upto 1 TB</span></li>
                                 <li>Unparallel price</li>
                              </ul>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- ELITE BUNDLE END -->
               <!-- ENTERPRISE BUNDLE START -->
               <div class="col-12 px-md-0 mt20 mt-md70 section-margin">
                  <div class="col-12 plan1-shape">
                     <div class="col-md-12 mx-auto px-md15 mt-md0 mt20">
                        <div class="pimg">
                           <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/bundle/enterprise.webp" class="img-fluid d-block mx-auto" alt="Enterprise">
                        </div>
                     </div>
                     <div class="row p0 mt20 mx-0 mt-md0">
                        <div class="col-12 p0 text-center mt-md50 mt20">
                           <div class="title-shape">
                              <div class="f-22 f-md-28 w600 white-clr lh150 text-center">
                                 Pro Upgrade 2- MAVAS Enterprise – ( $297 )
                              </div>
                           </div>
                        </div>
                        <div class="col-12 col-md-6 px0 px-md15">
                           <div class="f-20 f-md-20 lh150 w400 mt20 mt-md50">
                              <ul class="kaptick pl0">
                                 <li><span class="w600">Embed MAVAS on any website or Page</span> by Just one line of code </li>
                                 <li>Train Your MAVAS Chat-Bot using any Website Url, Text file or upload your own data</li> 
                                 <li><span class="w600">Get the Desired Results</span> using Trained Chat-Bots</li>   
                                 <li><span class="w600">Embed Unlimited MAVAS Chat-Bots</span> On Unlimited Websites</li>
                                 <li><span class="w600">Capture UNLIMITED Leads</span> To Maximize Lead Generation</li>
                                 <li>MAVAS will Generate <span class="w600">Unlimited Email List</span> </li>
                                 <li>MAVAS Chat-Bot Capture Every Single Lead Visited Chat-Bot</li>
                                 <li><span class="w600">Manage Leads Effortlessly</span> & make the most from them with our powerful lead management feature.</li>
                                 <li>Change MAVAS’s Response Tone/ Response Length/ background theme</li>
                              </ul>
                           </div>
                        </div>
                        <div class="col-12 col-md-6 px0 px-md15">
                           <div class="f-20 f-md-20 lh150 w400 mt0 mt-md50">
                              <ul class="kaptick pl0 p0 m0">
                                 <li>Rebrand MAVAS on the website by Changing Name</li>
                                 <li>Rebrand MAVAS on the website by Changing Image</li>
                                 <li>Rebrand MAVAS on the website by Changing Color theme</li>
                                 <li>Rebrand MAVAS on the website by Changing Background Image</li>
                                 <li>Strengthen Your Relationship With Your Customers Using CRM Integrations</li>
                                 <li>Inbuilt Contact Management System</li>
                                 <li>Fully Encrypted MAVAS Task Management</li>
                                 <li>100% Task Privacy</li>
                                 <li>Unparallel price</li>
                              </ul>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- ENTERPRISE BUNDLE END -->
               <!-- BUSINESS DRIVE BUNDLE START -->
               <div class="col-12 px-md0 mt20 mt-md70 section-margin">
                  <div class="col-12 plan2-shape">
                     <div class="col-md-10 mx-auto mt-md0 mt20">
                        <div class="pimg">
                           <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/bundle/agency.webp" class="img-fluid d-block mx-auto" alt="Agency">
                        </div>
                     </div>
                     <div class="row mt-md0 mt20 mx-0">
                        <div class=" col-12 p0 text-center mt-md50 mt20">
                           <div class="title-shape">
                              <div class="f-22 f-md-28 w600 white-clr lh150 text-center">
                                 Pro Upgrade 3- MAVAS Agency ($297)
                              </div>
                           </div>
                        </div>
                        <div class="col-12 col-md-6 px0 px-md15">
                           <div class="f-20 f-md-20 lh150 w400 mt20 mt-md50">
                              <ul class="kaptick pl0">
                                 <li><span class="w600">Directly Provide Top Notch MAVAS Super VA Creation Services</span> to Your Client's and Charge Monthly or Recurring Fee For 100% Profits</li>
                                 <li>Comes With Dedicated Dashboard To Create <span class="w600">Accounts For Client's In 3 Simple Clicks</span></li>
                                 <li><span class="w600">Completely Cloud-Based Platform - No Domain, Hosting or Installation Required</span></li>
                                 <li>Agency Whitelable</li>
                                 <li><span class="w600">Accurate Analysis of Client's Activities For Effective Monitoring</span></li>
                              </ul>
                           </div>
                        </div>
                        <div class="col-12 col-md-6 px0 px-md15">
                           <div class="f-20 f-md-20 lh150 w400 mt0 mt-md50">
                              <ul class="kaptick pl0 p0 m0">
                                 <li><span class="w600">Dedicated chat support directly</span> from software, to you & your client's</li>
                                 <li><span class="w600">Business Management System</span> to Manage Your Clients Plans & Permissions</li>
                                 <li>Unparallel Price With No Recurring Fee</li>
                                 <li>License to serve to Unlimited clients</li>
                                 <li>Provide High In Demand Services</li>
                                 <li>Unparallel price</li>
                              </ul>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- BUSINESS DRIVE BUNDLE END -->
               <!-- FAST ACTION BONUSES BUNDLE START -->
               <div class="col-12 px-md-0 mt20 mt-md70 section-margin">
                  <div class="col-12 plan1-shape">
                     <div class="mx-auto mt-md0 mt20">
                        <div class="pimg ">
                           <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/bundle/fast-action.webp" class="img-fluid d-block mx-auto" alt="Fast-Action">
                        </div>
                     </div>
                     <div class="row mt20">
                        <div class="col-12 p0 mt-md50 mt20 text-center">
                           <div class="title-shape1">
                              <div class="f-22 f-md-28 w600 white-clr lh150 text-center text-capitalize">
                                 And You're Also Getting These Premium Fast Action Bonuses Worth ($497)
                              </div>
                           </div>
                        </div>
                        <div class="col-12 col-md-6 px0 px-md15">
                           <div class="f-20 f-md-20 lh150 w600 mt20 mt-md50">
                              <ul class="kaptick pl0">
                                 <li>Bonus #1 - Live Training How to Make Money Using MAVAS</li>
                                 <li>Bonus #2 -Video Training On How To Start Online Business</li>
                              </ul>
                           </div>
                        </div>
                        <div class="col-12 col-md-6 px0 px-md15">
                           <div class="f-20 f-md-20 lh150 w600 mt0 mt-md50">
                              <ul class="kaptick pl0 p0 m0">
                                 <li>Bonus #3 -Mavas Guide Workshop</li>
                                 <li>Bonus #4 -Video Training On Get Your Head Into The AI</li>
                              </ul>
                           </div>
                        </div>
                        <!-- <div class="col-12 col-md-6 px0 px-md15">
                           <div class="f-20 f-md-20 lh150 w600 mt20 mt-md50">
                               <ul class="kaptick pl0">
                                   <li>Super Value Bonus #5 – Video Training To Monetizing Your Website </li>
                                   <li>Super Value Bonus #6 – Training To Start Affiliate Marketing </li>
                                   <li>Super Value Bonus #7 – Video Training On Viral Marketing </li>
                                   <li>Super Value Bonus #8 – Video Marketing Profit Kit </li>
                               </ul>
                           </div>
                           </div> -->
                     </div>
                  </div>
               </div>
               <!-- FAST ACTION BONUSES BUNDLE END -->
            </div>
         </div>
      </div>
      <!--2. Second Section End -->
      <!--3. Third Section Start -->
      <div class="section2">
         <div class="container">
            <div class="row ">
               <div class="col-12 px0 px-md15" id="buybundle">
                  <div class="f-md-45 f-28 black-clr  text-center w700 lh140">
                     Grab This Exclusive BUNDLE DISCOUNT Today…
                  </div>
                  <div class="f-18 f-md-22 w400 lh150 text-center mt20 black-clr">
                     Use Coupon Code <span class="purple-clr w700">"MAVAS"</span> for an Additional <span class="purple-clr w700"> $50</span> Discount
                  </div>
               </div>
            </div>
            <div class="row justify-content-center">
               <div class="col-md-6 col-12 mt25 mt-md50 px-md15">
                  <div class="tablebox1">
                     <div class="col-12 tablebox-inner-bg text-center">
                        <div class="relative col-12 pt20 pt-md0">
                        <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 685 120" style="max-height:55px">
                              <defs>
                                 <style>
                                    .cls-1 {
                                    fill: url(#linear-gradient-2);
                                    }

                                    .cls-2 {
                                    fill: #fff;
                                    }

                                    .cls-3 {
                                    fill: #21e5f4;
                                    }

                                    .cls-4 {
                                    fill: url(#linear-gradient);
                                    }
                                 </style>
                                 <linearGradient id="linear-gradient" x1="0" y1="60" x2="25.31" y2="60" gradientUnits="userSpaceOnUse">
                                    <stop offset="0" stop-color="#b956ff"/>
                                    <stop offset="1" stop-color="#8d4ef9"/>
                                 </linearGradient>
                                 <linearGradient id="linear-gradient-2" x1="99.25" y1="59.94" x2="124.57" y2="59.94" gradientUnits="userSpaceOnUse">
                                    <stop offset="0" stop-color="#8d57f4"/>
                                    <stop offset="1" stop-color="#6e33ff"/>
                                 </linearGradient>
                              </defs>
                              <g id="Layer_1-2" data-name="Layer 1">
                                 <g>
                                    <g>
                                       <g>
                                          <polygon class="cls-2" points="343.95 66.06 343.95 66.06 300 .53 273.46 .53 343.96 99.29 355.45 83.2 343.95 66.06"/>
                                          <polygon class="cls-2" points="367.54 66.26 414.45 .53 388.11 .53 355.71 48.61 367.54 66.26"/>
                                       </g>
                                       <polygon class="cls-2" points="218.06 0 147.57 99.82 172.51 99.82 182.71 84.58 198.02 61.87 218.07 32.36 237.91 61.88 253.21 84.59 263.4 99.82 288.55 99.82 218.06 0"/>
                                       <polygon class="cls-2" points="469.86 0 399.36 99.82 424.31 99.82 434.5 84.58 449.81 61.87 469.87 32.36 489.7 61.88 505.01 84.59 515.2 99.82 540.35 99.82 469.86 0"/>
                                       <g>
                                          <path class="cls-2" d="m662.66,46.82l-96.97-14.26,16.04,21.03,5.2.9,69.12,11.61c3.34.56,5.78,3.45,5.78,6.83,0,3.83-3.1,6.93-6.93,6.93h-75.79l-15.37,20.13,94.46.02c14.79,0,26.78-11.99,26.78-26.78,0-13.07-9.43-24.22-22.31-26.4Z"/>
                                          <polygon class="cls-2" points="685 1.08 589.71 1.08 589.71 1.06 581.89 1.06 566.5 21.21 669.63 21.21 685 1.08"/>
                                       </g>
                                    </g>
                                    <g>
                                       <polygon class="cls-4" points="25.31 20.82 25.31 120 0 99.11 0 0 25.31 20.82"/>
                                       <polygon class="cls-1" points="124.57 0 124.57 99.13 99.25 119.89 99.25 20.76 124.57 0"/>
                                       <polygon class="cls-3" points="92.74 25.17 92.74 62.88 62.24 100.89 31.82 62.89 31.82 25.17 62.24 63.17 92.74 25.17"/>
                                    </g>
                                 </g>
                              </g>
                           </svg>
                        </div>
                        <div class="col-12 mt-md40 mt20 table-headline-shape">
                           <div class="w700 f-md-32 f-22 text-center text-uppercase black-clr lh150 ">
                              Bundle Offer
                           </div>
                        </div>
                        <!-- PLAN 1 START -->
                        <div class="row mx-0 odd-shape mt-md40 mt20">
                           <div class="col-md-9 col-12 px-md15 px0">
                              <div class="w600 f-md-28 f-22 text-left text-xs-center text-uppercase white-clr lh150 mt8">
                                 COMMERCIAL
                              </div>
                           </div>
                           <div class="col-md-3 col-12 px-md15 px0">
                              <div class="col-12 price-shape">
                                 <div class="w600 f-md-35 f-22 text-center black-clr lh150">
                                    $37
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- PLAN 1 END -->
                        <!-- PLAN 2 START -->
                        <div class="row mx-0 even-shape">
                           <div class="col-md-9 col-12 px-md15 px0">
                              <div class="w600 f-md-28 f-22 text-left text-xs-center text-uppercase white-clr lh150 mt8">
                                 ELITE  
                              </div>
                           </div>
                           <div class="col-md-3 col-12 px-md15 px0">
                              <div class="col-12 price-shape">
                                 <div class="w600 f-md-35 f-22 text-center black-clr lh150">
                                    $197
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- PLAN 2 END -->
                        <!-- PLAN 3 START -->
                        <div class="row mx-0 odd-shape">
                           <div class="col-md-9 col-12 px-md15 px0">
                              <div class="w600 f-md-28 f-22 text-left text-xs-center text-uppercase white-clr lh150 mt8">
                                 ENTERPRISE  
                              </div>
                           </div>
                           <div class="col-md-3 col-12 px-md15 px0">
                              <div class="col-12 price-shape">
                                 <div class="w600 f-md-35 f-22 text-center black-clr lh150">
                                    $297
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- PLAN 3 END -->
                        <!-- PLAN 4 START -->
                        <div class="row mx-0 even-shape">
                           <div class="col-md-9 col-12 px-md15 px0">
                              <div class="w600 f-md-28 f-22 text-left text-xs-center text-uppercase white-clr lh150 mt8">
                                 AGENCY
                              </div>
                           </div>
                           <div class="col-md-3 col-12 px-md15 px0">
                              <div class="col-12 price-shape">
                                 <div class="w600 f-md-35 f-22 text-center black-clr lh150">
                                    $297
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- PLAN 4 END -->
                        <!-- PLAN 5 START -->
                        <div class="row mx-0 odd-shape">
                           <div class="col-md-9 col-12 px-md15 px0">
                              <div class="w600 f-md-28 f-22 text-left text-xs-center text-uppercase white-clr lh150 mt8">
                                 FAST ACTION BONUSES 
                              </div>
                           </div>
                           <div class="col-md-3 col-12 px-md15 px0">
                              <div class="col-12 price-shape">
                                 <div class="w600 f-md-35 f-22 text-center black-clr lh150">
                                    $497
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- PLAN 5 END -->
                        <div class="col-12 px-md0 px15 mt-md30 mt20 w500 f-md-35 f-22 white-clr text-center">
                           Regular <strike>$1325</strike>
                        </div>
                        <div class="">
                           <div class="col-md-8 mx-auto col-12 now-price mt20 w700 f-md-60 f-32 white-clr text-center">
                              Now $297
                           </div>
                        </div>
                        <div class="col-12 px-md0 px15 mt20 w500 f-md-25 f-20 white-clr text-center">
                           One Time Fee
                        </div>
                        <div class="mt20 red-clr w600 f-22 d-md-27 lh140">
                           <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/bundle/red-close.webp " alt="Hurry" class="mr10 close-img">
                              No Recurring | No Monthly | No Yearly
                           </div>
                        <div class="row justify-content-center mx-0 ">
                           <div class="myfeatures f-md-25 f-16 w400 text-center lh160">
                              <div class="hideme-button">
                                 <!-- <a href="https://www.jvzoo.com/b/109507/398672/2"><img src="https://i.jvzoo.com/109507/398672/2" alt="MAVAS Bundle" border="0" class="img-fluid d-block mx-auto"/></a>  -->
                                 <a href="https://www.jvzoo.com/b/110063/400759/2"><img src="https://i.jvzoo.com/110063/400759/2" alt="MAVAS Bundle" border="0" class="img-fluid d-block mx-auto"/></a>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--3. Third Section End -->
      <!----Second Section---->
      <div class="second-header-section">
         <div class="container ">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-md-45 f-24 w600 lh140 text-center white-clr">
                     TOP Reasons Why You Can't Ignore <br class="d-none d-md-block"> <span class="f-md-50 f-24 w700 "> MAVAS Bundle Deal </span>
                  </div>
               </div>
            </div>
            <div class="row mt30 mt-md65">
               <div class="col-12 col-md-10 mx-auto">
                  <div class="step-shape">
                     <div class="f-20 f-md-22 w600 white-clr lh150">
                        Reason #1
                     </div>
                     <div class="f-18 f-md-18 w400 white-clr lh150 mt10 ">
                        Get All The Benefits Of MAVAS & Premium Upgrades To Multiply Your Results... Save More Time, Work Like A Pro & Ultimately Boost Your Agency Profits
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt30">
                  <div class="step-shape">
                     <div class="f-20 f-md-22 w600 white-clr lh150">
                        Reason #2
                     </div>
                     <div class="f-18 f-md-18 w400 white-clr lh150 mt10 ">
                        Regular Price For MAVAS, All Upgrades & Bonuses Is $1325. You Are Saving $1028 Today When You Grab The Exclusive Bundle Deal Now at ONLY $297.
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt30">
                  <div class="step-shape">
                     <div class="f-20 f-md-22 w600 white-clr lh150">
                        Reason #3
                     </div>
                     <div class="f-18 f-md-18 w400 white-clr lh150 mt10 ">
                        This Limited Time Additional $50 Coupon Will Expires As Soon As Timer Hits Zero So Take Action Now.
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt30">
                  <div class="step-shape">
                     <div class="f-20 f-md-22 w600 white-clr lh150">
                        Reason #4
                     </div>
                     <div class="f-18 f-md-18 w400 white-clr lh150 mt10 ">
                        Get Priority Support From Our Dedicated Support Team to Get Assured Success.
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!----Second Section---->
      <div class="riskfree-section">
         <div class="container ">
            <div class="row align-items-center ">
               <div class="f-28 f-md-45 w700 white-clr text-center col-12 mb-md40">Test Drive MAVAS Risk FREE For 30 Days
               </div>
               <div class="col-md-7 col-12 order-md-2">
                  <div class="f-md-18 f-18 w400 lh150 white-clr mt15 "><span class="w600">Your purchase is secured with our 30-day 100% money back guarantee.</span>
                     <br><br>
                     <span class="w600 ">So, you can buy with full confidence and try it for yourself and for your client’s risk free.</span> If you face any Issue or don’t get results you desired after using it, just raise a ticket on support desk
                     within 30 days and we'll refund you everything, down to the last penny.<br><br> We would like to clearly state that we do NOT offer a “no questions asked” money back guarantee. You must provide a genuine reason when you contact
                     our support and we will refund your hard-earned money.
                  </div>
               </div>
               <div class="col-md-5 order-md-1 col-12 mt20 mt-md0 ">
                  <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/bundle/riskfree-img.webp " class="img-fluid d-block mx-auto " alt="Risk Free">
               </div>
            </div>
         </div>
      </div>
      <!--3. Third Section Start -->
      <div class="section2">
         <div class="container">
            <div class="row ">
               <div class="col-12 px0 px-md15" id="buybundle">
                  <div class="f-md-45 f-28 f  text-center w700 lh140">
                     Limited Time Offer!
                  </div>
                  <div class="f-22 f-md-32 w700 lh150 text-center mt20 black-clr">
                     Get Complete Package of All <span class="w700">MAVAS</span> Products<br class="d-md-block d-none"> for A Low One-Time Fee
                  </div>
               </div>
               <div class="col-12 f-22 f-md-22 w400 lh150 text-center mt20 black-clr">
                  Use Coupon Code <span class="purple-clr w700 ">"MAVAS"</span> for an Additional <span class="purple-clr w700 "> $50</span> Discount
               </div>
            </div>
            <div class="row justify-content-center">
               <div class="col-md-6 col-12 mt25 mt-md50 px-md15">
                  <div class="tablebox1">
                     <div class="col-12 tablebox-inner-bg text-center">
                        <div class="relative col-12 p0">
                           <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 685 120" style="max-height:55px">
                              <defs>
                                 <style>
                                    .cls-1 {
                                    fill: url(#linear-gradient-2);
                                    }

                                    .cls-2 {
                                    fill: #fff;
                                    }

                                    .cls-3 {
                                    fill: #21e5f4;
                                    }

                                    .cls-4 {
                                    fill: url(#linear-gradient);
                                    }
                                 </style>
                                 <linearGradient id="linear-gradient" x1="0" y1="60" x2="25.31" y2="60" gradientUnits="userSpaceOnUse">
                                    <stop offset="0" stop-color="#b956ff"/>
                                    <stop offset="1" stop-color="#8d4ef9"/>
                                 </linearGradient>
                                 <linearGradient id="linear-gradient-2" x1="99.25" y1="59.94" x2="124.57" y2="59.94" gradientUnits="userSpaceOnUse">
                                    <stop offset="0" stop-color="#8d57f4"/>
                                    <stop offset="1" stop-color="#6e33ff"/>
                                 </linearGradient>
                              </defs>
                              <g id="Layer_1-2" data-name="Layer 1">
                                 <g>
                                    <g>
                                       <g>
                                          <polygon class="cls-2" points="343.95 66.06 343.95 66.06 300 .53 273.46 .53 343.96 99.29 355.45 83.2 343.95 66.06"/>
                                          <polygon class="cls-2" points="367.54 66.26 414.45 .53 388.11 .53 355.71 48.61 367.54 66.26"/>
                                       </g>
                                       <polygon class="cls-2" points="218.06 0 147.57 99.82 172.51 99.82 182.71 84.58 198.02 61.87 218.07 32.36 237.91 61.88 253.21 84.59 263.4 99.82 288.55 99.82 218.06 0"/>
                                       <polygon class="cls-2" points="469.86 0 399.36 99.82 424.31 99.82 434.5 84.58 449.81 61.87 469.87 32.36 489.7 61.88 505.01 84.59 515.2 99.82 540.35 99.82 469.86 0"/>
                                       <g>
                                          <path class="cls-2" d="m662.66,46.82l-96.97-14.26,16.04,21.03,5.2.9,69.12,11.61c3.34.56,5.78,3.45,5.78,6.83,0,3.83-3.1,6.93-6.93,6.93h-75.79l-15.37,20.13,94.46.02c14.79,0,26.78-11.99,26.78-26.78,0-13.07-9.43-24.22-22.31-26.4Z"/>
                                          <polygon class="cls-2" points="685 1.08 589.71 1.08 589.71 1.06 581.89 1.06 566.5 21.21 669.63 21.21 685 1.08"/>
                                       </g>
                                    </g>
                                    <g>
                                       <polygon class="cls-4" points="25.31 20.82 25.31 120 0 99.11 0 0 25.31 20.82"/>
                                       <polygon class="cls-1" points="124.57 0 124.57 99.13 99.25 119.89 99.25 20.76 124.57 0"/>
                                       <polygon class="cls-3" points="92.74 25.17 92.74 62.88 62.24 100.89 31.82 62.89 31.82 25.17 62.24 63.17 92.74 25.17"/>
                                    </g>
                                 </g>
                              </g>
                           </svg>
                        </div>
                        <div class="col-12 mt-md40 mt20 table-headline-shape">
                           <div class="w700 f-md-32 f-22 text-center text-uppercase black-clr lh150 ">
                              Bundle Offer
                           </div>
                        </div>
                        <!-- PLAN 1 START -->
                        <div class="row mx-0 odd-shape mt-md40 mt20">
                           <div class="col-md-9 col-12 px-md15 px0">
                              <div class="w600 f-md-28 f-22 text-left text-xs-center text-uppercase white-clr lh150 mt8">
                                 COMMERCIAL 
                              </div>
                           </div>
                           <div class="col-md-3 col-12 px-md15 px0">
                              <div class="col-12 price-shape">
                                 <div class="w600 f-md-35 f-22 text-center black-clr lh150">
                                    $37
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- PLAN 1 END -->
                        <!-- PLAN 2 START -->
                        <div class="row mx-0 even-shape">
                           <div class="col-md-9 col-12 px-md15 px0">
                              <div class="w600 f-md-28 f-22 text-left text-xs-center text-uppercase white-clr lh150 mt8">
                                 ELITE  
                              </div>
                           </div>
                           <div class="col-md-3 col-12 px-md15 px0">
                              <div class="col-12 price-shape">
                                 <div class="w600 f-md-35 f-22 text-center black-clr lh150">
                                    $197
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- PLAN 2 END -->
                        <!-- PLAN 3 START -->
                        <div class="row mx-0 odd-shape">
                           <div class="col-md-9 col-12 px-md15 px0">
                              <div class="w600 f-md-28 f-22 text-left text-xs-center text-uppercase white-clr lh150 mt8">
                                 ENTERPRISE  
                              </div>
                           </div>
                           <div class="col-md-3 col-12 px-md15 px0">
                              <div class="col-12 price-shape">
                                 <div class="w600 f-md-35 f-22 text-center black-clr lh150">
                                    $297
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- PLAN 3 END -->
                        <!-- PLAN 4 START -->
                        <div class="row mx-0 even-shape">
                           <div class="col-md-9 col-12 px-md15 px0">
                              <div class="w600 f-md-28 f-22 text-left text-xs-center text-uppercase white-clr lh150 mt8">
                                 AGENCY 
                              </div>
                           </div>
                           <div class="col-md-3 col-12 px-md15 px0">
                              <div class="col-12 price-shape">
                                 <div class="w600 f-md-35 f-22 text-center black-clr lh150">
                                    $297
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- PLAN 4 END -->
                        <!-- PLAN 5 START -->
                        <div class="row mx-0 odd-shape">
                           <div class="col-md-9 col-12 px-md15 px0">
                              <div class="w600 f-md-28 f-22 text-left text-xs-center text-uppercase white-clr lh150 mt8">
                                 FAST ACTION BONUSES 
                              </div>
                           </div>
                           <div class="col-md-3 col-12 px-md15 px0">
                              <div class="col-12 price-shape">
                                 <div class="w600 f-md-35 f-22 text-center black-clr lh150">
                                    $497
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- PLAN 5 END -->
                        <div class="col-12 px-md0 px15 mt-md30 mt20 w500 f-md-35 f-22 white-clr text-center">
                           Regular <strike>$1325</strike>
                        </div>
                        <div class="row justify-content-center mx-0 ">
                           <div class="col-md-8 col-12 now-price mt20 w700 f-md-60 f-32 white-clr text-center">
                              Now $297
                           </div>
                        </div>
                        <div class="col-12 px-md0 px15 mt20 w500 f-md-25 f-20 white-clr text-center">
                           One Time Fee
                        </div>
                        <div class="mt20 red-clr w600 f-22 d-md-27 lh140">
                           <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/bundle/red-close.webp" alt="Hurry" class="mr10 close-img">
                              No Recurring | No Monthly | No Yearly
                           </div>
                        <div class="row justify-content-center mx-0 ">
                           <div class="myfeatures f-md-25 f-16 w400 text-center lh160">
                              <div class="hideme-button">
                                 <!-- <a href="https://www.jvzoo.com/b/109507/398672/2"><img src="https://i.jvzoo.com/109507/398672/2" alt="MAVAS Bundle" border="0" class="img-fluid mx-auto d-block"/></a>  -->
                                 <a href="https://www.jvzoo.com/b/110063/400759/2"><img src="https://i.jvzoo.com/110063/400759/2" alt="MAVAS Bundle" border="0" class="img-fluid d-block mx-auto"/></a>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt-md50 mt20">
                  <div class="f-md-36 f-24 black-clr  text-center w700 lh140">
                     We are always here by your side <br class="d-none d-md-block">
                     <span class="red-clr">Get in touch with us …</span>
                  </div>
               </div>
            </div>

            <div class="container-box mt20 mt-md80">
               <div class="row">
                  <div class="col-12 col-md-12 text-center">
                     <div class="f-28 f-md-45 w600 white-clr lh140">
                        Have any Query? Contact us Anytime
                     </div>
                  </div>
               </div>
               <div class="row mt20 mt-md80 justify-content-between">
                  <div class="col-12 col-md-8 mx-auto">
                     <div class="row">
                        <div class="col-12 col-md-6">
                           <div class="contact-shape">
                              <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/amit-pareek-sir.webp" class="img-fluid d-block mx-auto minus " alt="Amit Pareek">
                              <div class="f-24 f-md-30 w600  lh140 text-center white-clr mt20">
                                 Dr. Amit Pareek
                              </div>
                              <!-- <div class="col-12 mt30 d-flex justify-content-center">
                                 <a href="http://facebook.com/Dr.AmitPareek" class="link-text mr20">
                                 <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/am-fb.webp" class="center-block " alt="Facebook">
                                 </a>
                                 <a href="skype:amit.pareek77" class="link-text">
                                    <div class="col-12 ">
                                       <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/am-skype.webp" class="center-block " alt="Skype">
                                    </div>
                                 </a>
                              </div> -->
                           </div>
                        </div>
                        <div class="col-12 col-md-6 mt20 mt-md0">
                           <div class="contact-shape">
                              <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/atul-parrek-sir.webp" class="img-fluid d-block mx-auto minus " alt=" Atul Pareek">
                              <div class="f-24 f-md-30 w600  lh140 text-center white-clr mt20">
                                  Atul Pareek
                              </div>
                              <!-- <div class="col-12 mt30 d-flex justify-content-center">
                                 <a href="https://www.facebook.com/profile.php?id=100076904623319" class="link-text mr20">
                                    <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/am-fb.webp" class="center-block " alt="Facebook">
                                 </a>
                                 <a href="skype:live:.cid.c3d2302c4a2815e0" class="link-text">
                                    <div class="col-12 ">
                                       <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/am-skype.webp" class="center-block " alt="Skype">
                                    </div>
                                 </a>
                              </div> -->
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>

         </div>
      </div>
      <!--3. Third Section End -->
      <!-- Footer Section Start -->
      <footer>
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 685 120" style="max-height:55px">
                     <defs>
                        <style>
                           .cls-1 {
                           fill: url(#linear-gradient-2);
                           }

                           .cls-2 {
                           fill: #fff;
                           }

                           .cls-3 {
                           fill: #21e5f4;
                           }

                           .cls-4 {
                           fill: url(#linear-gradient);
                           }
                        </style>
                        <linearGradient id="linear-gradient" x1="0" y1="60" x2="25.31" y2="60" gradientUnits="userSpaceOnUse">
                           <stop offset="0" stop-color="#b956ff"/>
                           <stop offset="1" stop-color="#8d4ef9"/>
                        </linearGradient>
                        <linearGradient id="linear-gradient-2" x1="99.25" y1="59.94" x2="124.57" y2="59.94" gradientUnits="userSpaceOnUse">
                           <stop offset="0" stop-color="#8d57f4"/>
                           <stop offset="1" stop-color="#6e33ff"/>
                        </linearGradient>
                     </defs>
                     <g id="Layer_1-2" data-name="Layer 1">
                        <g>
                           <g>
                              <g>
                                 <polygon class="cls-2" points="343.95 66.06 343.95 66.06 300 .53 273.46 .53 343.96 99.29 355.45 83.2 343.95 66.06"/>
                                 <polygon class="cls-2" points="367.54 66.26 414.45 .53 388.11 .53 355.71 48.61 367.54 66.26"/>
                              </g>
                              <polygon class="cls-2" points="218.06 0 147.57 99.82 172.51 99.82 182.71 84.58 198.02 61.87 218.07 32.36 237.91 61.88 253.21 84.59 263.4 99.82 288.55 99.82 218.06 0"/>
                              <polygon class="cls-2" points="469.86 0 399.36 99.82 424.31 99.82 434.5 84.58 449.81 61.87 469.87 32.36 489.7 61.88 505.01 84.59 515.2 99.82 540.35 99.82 469.86 0"/>
                              <g>
                                 <path class="cls-2" d="m662.66,46.82l-96.97-14.26,16.04,21.03,5.2.9,69.12,11.61c3.34.56,5.78,3.45,5.78,6.83,0,3.83-3.1,6.93-6.93,6.93h-75.79l-15.37,20.13,94.46.02c14.79,0,26.78-11.99,26.78-26.78,0-13.07-9.43-24.22-22.31-26.4Z"/>
                                 <polygon class="cls-2" points="685 1.08 589.71 1.08 589.71 1.06 581.89 1.06 566.5 21.21 669.63 21.21 685 1.08"/>
                              </g>
                           </g>
                           <g>
                              <polygon class="cls-4" points="25.31 20.82 25.31 120 0 99.11 0 0 25.31 20.82"/>
                              <polygon class="cls-1" points="124.57 0 124.57 99.13 99.25 119.89 99.25 20.76 124.57 0"/>
                              <polygon class="cls-3" points="92.74 25.17 92.74 62.88 62.24 100.89 31.82 62.89 31.82 25.17 62.24 63.17 92.74 25.17"/>
                           </g>
                        </g>
                     </g>
                  </svg>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-md-20 f-16 w400 lh140 white-clr text-xs-center">Copyright © MAVAS 2023</div>
                  <ul class="footer-ul w400 f-md-20 f-16 white-clr text-center text-md-right">
                     <li><a href="https://support.oppyo.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://getmavas.com/legal/privacy-policy.html " class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://getmavas.com/legal/terms-of-service.html " class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://getmavas.com/legal/disclaimer.html " class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://getmavas.com/legal/gdpr.html " class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://getmavas.com/legal/dmca.html " class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://getmavas.com/legal/anti-spam.html " class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </footer>
      <!-- Footer Section End -->
   
<!-- timer --->
<script type="text/javascript">
         var noob = $('.countdown').length;
         var stimeInSecs;
         var tickers;

         function timerBegin(seconds) {     
            stimeInSecs = parseInt(seconds);           
            showRemaining();
            //tickers = setInterval("showRemaining()", 1000);
         }

         function showRemaining() {

            var seconds = stimeInSecs;
               
            if (seconds > 0) {         
               stimeInSecs--;       
            } else {        
               clearInterval(tickers);       
               //timerBegin(59 * 60); 
            }

            var days = Math.floor(seconds / 86400);
      
            seconds %= 86400;
            
            var hours = Math.floor(seconds / 3600);
            
            seconds %= 3600;
            
            var minutes = Math.floor(seconds / 60);
            
            seconds %= 60;


            if (days < 10) {
               days = "0" + days;
            }
            if (hours < 10) {
               hours = "0" + hours;
            }
            if (minutes < 10) {
               minutes = "0" + minutes;
            }
            if (seconds < 10) {
               seconds = "0" + seconds;
            }
            var i;
            var countdown = document.getElementsByClassName('countdown');

            for (i = 0; i < noob; i++) {
               countdown[i].innerHTML = '';
            
               if (days) {
                  countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-16 f-md-24 timerbg oswald">' + days + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-12 f-md-15 w500 smmltd">Days</span> </div>';
               }
            
               countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-16 f-md-24 timerbg oswald">' + hours + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-12 f-md-15 w500 smmltd">Hours</span> </div>';
            
               countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"><span class="f-16 f-md-24 timerbg oswald">' + minutes + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-12 f-md-15 w500 smmltd">Mins</span> </div>';
            
               countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-16 f-md-24 timerbg oswald">' + seconds + '</span><br><span class="f-12 f-md-15 w500 smmltd">Sec</span> </div>';
            }
         
         }


      //timerBegin(59 * 60); 
      function getCookie(cname) {
            var name = cname + "=";
            var decodedCookie = decodeURIComponent(document.cookie);
            var ca = decodedCookie.split(';');
            for(var i = 0; i <ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        }
      var cnt = 60*60;
      function counter(){
         if(getCookie("cnt") > 0){
            cnt = getCookie("cnt");
         }
         cnt -= 1;
         document.cookie = "cnt="+ cnt;
         timerBegin(getCookie("cnt"));
         //jQuery("#counter").val(getCookie("cnt"));
         if(cnt>0){
            setTimeout(counter,1000);
        }
      }
      counter();
</script>
<!--- timer end-->

  <!-- Swiper JS -->
  <script src="https://cdn.jsdelivr.net/npm/swiper@10/swiper-bundle.min.js"></script>
              <!-- Initialize Swiper -->
  <script>
    var swiper = new Swiper(".mySwiper", {
      slidesPerView: 5,
      spaceBetween: 10,
      autoplay: {
        delay: 2500,
        disableOnInteraction: false,
      },
      pagination: {
        el: ".swiper-pagination",
        clickable: true,
      },
      breakpoints: {
        640: {
          slidesPerView: 2,
          spaceBetween: 20,
        },
        768: {
          slidesPerView: 4,
          spaceBetween: 30,
        },
        1024: {
          slidesPerView: 5,
          spaceBetween: 30,
        },
        1320:{
            slidesPerView: 6,
            spaceBetween: 30,
        },
        1500:{
            slidesPerView: 7,
            spaceBetween: 30,
        },
        1900:{
            slidesPerView: 8,
            spaceBetween: 30,
        },
      },
    });
  </script>    
  <script>
    var swiper = new Swiper(".mySwiper.bio-card", {
      slidesPerView: 5,
      spaceBetween: 10,
      autoplay: {
        delay: 2500,
        disableOnInteraction: false,
      },
      pagination: {
        el: ".swiper-pagination",
        clickable: true,
      },
      breakpoints: {
        640: {
          slidesPerView: 2,
          spaceBetween: 20,
        },
        768: {
          slidesPerView: 2,
          spaceBetween: 30,
        },
        1024: {
          slidesPerView: 3,
          spaceBetween: 30,
        },
      },
    });
  </script> 
   </body>
</html>
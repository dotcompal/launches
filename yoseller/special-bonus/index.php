<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <!-- Tell the browser to be responsive to screen width -->
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
      <meta name="title" content="YoSeller Bonuses">
      <meta name="description" content="YoSeller Bonuses">
      <meta name="keywords" content="Revealing: Cutting-Edge Technology to Launch & Sell Digital Products, Courses, Services & Goods Online with Zero Commission and No Monthly Fee Ever">
      <meta property="og:image" content="https://www.yoseller.co/special-bonus/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Dr. Amit Pareek">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="YoSeller Bonuses">
      <meta property="og:description" content="Revealing: Cutting-Edge Technology to Launch & Sell Digital Products, Courses, Services & Goods Online with Zero Commission and No Monthly Fee Ever">
      <meta property="og:image" content="https://www.yoseller.co/special-bonus/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="YoSeller Bonuses">
      <meta property="twitter:description" content="Revealing: Cutting-Edge Technology to Launch & Sell Digital Products, Courses, Services & Goods Online with Zero Commission and No Monthly Fee Ever">
      <meta property="twitter:image" content="https://www.yoseller.co/special-bonus/thumbnail.png">
      <title>YoSeller Bonuses</title>
      <!-- Shortcut Icon  -->
      <link rel="icon" href="https://cdn.oppyo.com/launches/yoseller/common_assets/images/favicon.png" type="image/png">
      <!-- Css CDN Load Link -->
      <link rel="stylesheet" type="text/css" href="assets/css/bonus-style.css">
      <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css2?family=Pacifico&display=swap" rel="stylesheet">
      <!-- Start Editor required -->
      <link rel="stylesheet" href="https://cdn.oppyo.com/launches/yoseller/common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style.css" type="text/css">
      <script src="https://cdn.oppyo.com/launches/writerarc/common_assets/js/jquery.min.js"></script>
   </head>
   <body>
      <!-- New Timer  Start-->
      <?php
         $date = 'October 13 2022 11:59 AM EST';
         $exp_date = strtotime($date);
         $now = time();  
         /*
         
         $date = date('F d Y g:i:s A eO');
         $rand_time_add = rand(700, 1200);
         $exp_date = strtotime($date) + $rand_time_add;
         $now = time();*/
         
         if ($now < $exp_date) {
         ?>
      <?php
         } else {
         	echo "Times Up";
         }
         ?>
      <!-- New Timer End -->
      <?php
         if(!isset($_GET['afflink'])){
         $_GET['afflink'] = 'https://jvz1.com/c/10103/387544/';
         $_GET['name'] = 'Dr. Amit Pareek';      
         }
         ?>
      <!-- Header Section Start -->   
      <div class="main-header">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="text-center">
                     <div class="f-md-32 f-20 lh140 w400 white-clr d-block d-md-flex align-items-center justify-content-center">
                        <span class="w600 yellow-clr"><?php echo $_GET['name'];?>'s</span> &nbsp;special bonus for &nbsp;
                        <svg version="1.1" id="Layer_11" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 253.7 80" style="enable-background:new 0 0 253.7 80; max-height:60px;" xml:space="preserve">
                           ;
                           <style type="text/css">
                              .st00{fill:#FFD60D;}
                              .st11{fill:#FFFFFF;}
                              .st22{fill:#2337F6;}
                           </style>
                           <g>
                              <path class="st00" d="M130.2,64.2l-4.8,1.1c-0.4-0.9-1.1-1.6-1.9-2.2c-1-0.7-2.2-1.1-3.5-1.1c-1.7,0-3.3,0.7-4.4,1.9
                                 c-1,1-1.6,2.4-1.7,3.9l-14.1,3.1c-1.1-2.5-3.6-4.3-6.5-4.3c-3.9,0-7.1,3.2-7.1,7.1v0.1l-1.8,0.4c-3.3,0.8-6.6-1.3-7.3-4.6l-10-43.9
                                 c-0.8-3.3,1.3-6.6,4.6-7.3l9.5-2.1v0.6c0.1,1.4,1.3,2.5,2.7,2.5c0.1,0,0.1,0,0.2,0c1.5-0.1,2.6-1.3,2.5-2.8c0-0.5-0.2-0.9-0.4-1.3
                                 l14.2-3.1v0.5c0,0.7,0.3,1.3,0.8,1.8c0.5,0.4,1.1,0.7,1.8,0.7c0.1,0,0.1,0,0.2,0c1.5-0.1,2.6-1.3,2.5-2.8c0-0.5-0.2-0.9-0.4-1.3
                                 l12.4-2.7c3.3-0.8,6.6,1.3,7.3,4.6l2,8.8l1.1,5l6.8,30.2C135.7,60.3,133.6,63.6,130.2,64.2z"></path>
                              <g>
                                 <path class="st11" d="M34.4,21.8L20.7,48.3v15.1h-6.8V48.3L0,21.8h7.6l9.6,20.4l9.6-20.4C26.8,21.8,34.4,21.8,34.4,21.8z"></path>
                                 <path class="st11" d="M41.5,61.8c-2.5-1.4-4.5-3.4-5.9-6c-1.4-2.6-2.2-5.6-2.2-8.9c0-3.4,0.7-6.3,2.2-8.9c1.5-2.6,3.5-4.6,6-6
                                    c2.6-1.4,5.4-2.1,8.6-2.1s6,0.7,8.6,2.1c2.6,1.4,4.6,3.4,6,6c1.5,2.6,2.2,5.6,2.2,8.9c0,3.4-0.8,6.3-2.3,8.9
                                    c-1.5,2.6-3.6,4.6-6.2,6c-2.6,1.4-5.5,2.1-8.6,2.1C46.8,63.9,44,63.2,41.5,61.8z M54.9,56.7c1.5-0.8,2.8-2.1,3.7-3.8
                                    c1-1.7,1.4-3.7,1.4-6.1c0-2.4-0.5-4.4-1.4-6.1c-0.9-1.7-2.1-2.9-3.7-3.7c-1.5-0.8-3.2-1.3-4.9-1.3c-1.8,0-3.4,0.4-4.9,1.3
                                    c-1.5,0.8-2.7,2.1-3.6,3.7c-0.9,1.7-1.3,3.7-1.3,6.1c0,3.6,0.9,6.3,2.7,8.2c1.8,1.9,4.1,2.9,6.9,2.9C51.7,58,53.3,57.5,54.9,56.7z
                                    "></path>
                              </g>
                              <g>
                                 <path class="st11" d="M167.1,50.3h-23.7c0.2,2.5,1.1,4.5,2.8,6c1.7,1.5,3.7,2.3,6.1,2.3c3.5,0,5.9-1.4,7.3-4.3h6.9
                                    c-0.9,2.9-2.6,5.2-5.1,7c-2.5,1.8-5.5,2.7-9.1,2.7c-3,0-5.6-0.7-8-2c-2.3-1.3-4.2-3.2-5.5-5.6s-2-5.2-2-8.4c0-3.2,0.6-6,1.9-8.4
                                    c1.3-2.4,3.1-4.3,5.5-5.6c2.3-1.3,5-2,8.1-2c2.9,0,5.5,0.6,7.8,1.9c2.3,1.3,4.1,3.1,5.3,5.4c1.3,2.3,1.9,5,1.9,8
                                    C167.3,48.3,167.2,49.3,167.1,50.3z M160.6,45.1c0-2.4-0.9-4.3-2.5-5.7c-1.7-1.4-3.7-2.1-6.1-2.1c-2.2,0-4.1,0.7-5.7,2.1
                                    c-1.6,1.4-2.5,3.3-2.8,5.7H160.6z"></path>
                                 <path class="st11" d="M180,21.8v41.6h-6.4V21.8H180z"></path>
                                 <path class="st11" d="M194.9,21.8v41.6h-6.4V21.8H194.9z"></path>
                                 <path class="st11" d="M231.5,50.3h-23.7c0.2,2.5,1.1,4.5,2.8,6c1.6,1.5,3.7,2.3,6.1,2.3c3.5,0,5.9-1.4,7.3-4.3h6.9
                                    c-0.9,2.9-2.6,5.2-5.1,7c-2.5,1.8-5.5,2.7-9.1,2.7c-3,0-5.6-0.7-8-2c-2.3-1.3-4.2-3.2-5.5-5.6c-1.3-2.4-2-5.2-2-8.4
                                    c0-3.2,0.6-6,1.9-8.4c1.3-2.4,3.1-4.3,5.5-5.6c2.3-1.3,5-2,8.1-2c2.9,0,5.5,0.6,7.8,1.9c2.3,1.3,4.1,3.1,5.3,5.4
                                    c1.3,2.3,1.9,5,1.9,8C231.7,48.3,231.7,49.3,231.5,50.3z M225,45.1c0-2.4-0.9-4.3-2.5-5.7c-1.7-1.4-3.7-2.1-6.1-2.1
                                    c-2.2,0-4.1,0.7-5.7,2.1c-1.6,1.4-2.5,3.3-2.8,5.7H225z"></path>
                                 <path class="st11" d="M248.2,33.2c1.6-0.9,3.4-1.3,5.5-1.3v6.6h-1.6c-2.5,0-4.4,0.6-5.7,1.9s-1.9,3.5-1.9,6.6v16.3H238v-31h6.4v4.5
                                    C245.4,35.3,246.6,34.1,248.2,33.2z"></path>
                              </g>
                              <circle class="st11" cx="93.4" cy="73.7" r="6.3"></circle>
                              <path class="st22" d="M131.6,51.7c0,5.2-3.4,9.7-8,11.4c-1-0.7-2.2-1.1-3.5-1.1c-1.7,0-3.3,0.7-4.4,1.9H78.9c0-3.9,3.1-7,7-7h33.3
                                 c2.9,0,5.3-2.3,5.3-5.2s-2.3-5.3-5.3-5.3h-11.4c-3.4,0-6.5-1.4-8.8-3.7c-2.2-2.2-3.5-5.3-3.5-8.7c0.1-6.8,5.6-12.2,12.4-12.2h22.2
                                 c0,1.9-0.8,3.7-2.1,5s-3,2-4.9,2H108c-2.9,0-5.3,2.3-5.3,5.2s2.3,5.3,5.3,5.3h11.4c3.4,0,6.5,1.4,8.8,3.7
                                 C130.3,45.3,131.6,48.3,131.6,51.7z"></path>
                              <path class="st22" d="M97.7,22.8c0.4-0.4,0.8-0.7,1.2-1H71.5l0,0c0,3.9,3.1,7,7,7h15.3C94.7,26.6,96,24.5,97.7,22.8z"></path>
                              <path class="st22" d="M85.6,38.8h8.1c-0.5-1.6-0.7-3.2-0.7-4.9c0-0.7,0.1-1.4,0.2-2.1H78.6l0,0C78.6,35.7,81.7,38.8,85.6,38.8z"></path>
                              <path class="st11" d="M104.3,11.2c-0.1-1-0.2-2.1-0.6-3.1C102,2.9,97-0.3,92,0c-0.5,0-1.1,0.1-1.6,0.2c-0.4,0.1-0.7,0.2-1.1,0.3
                                 c-6,2-9.1,8.6-7.1,14.7c0.1,0.2,0.1,0.4,0.2,0.6c-0.2,0.3-0.2,0.7-0.2,1c0.1,1,0.9,1.8,1.9,1.7s1.8-0.9,1.7-1.9
                                 c0-0.6-0.4-1.2-1-1.5c-0.1-0.2-0.2-0.4-0.2-0.6C83,9.6,85.5,4.5,90.1,3s9.6,1.1,11.2,5.9c0.3,0.8,0.4,1.6,0.5,2.3
                                 c-0.4,0.4-0.6,0.9-0.5,1.4c0.1,1,0.9,1.8,1.9,1.7c1-0.1,1.8-0.9,1.7-1.9C104.8,12,104.6,11.5,104.3,11.2z"></path>
                              <circle class="st11" cx="120.1" cy="68.1" r="5.2"></circle>
                           </g>
                        </svg>
                     </div>
                  </div>

               <div class="col-12 mt20 mt-md50 text-center">
                  <div class="preheadline f-20 f-md-22 w400 white-clr lh140">
                  Grab My 15 Exclusive Bonuses Before the Deal Ends...
                  </div>
               </div>


               <div class="col-12 mt-md30 mt20 f-md-48 f-28 w700 text-center white-clr lh140">
               Have A Sneak Peak of A Cutting-Edge Technology That Lets Anyone <span class="yellow-clr">Launch & Sell Digital Products, Courses, & Services Online in NEXT 7 Minutes Flat...</span> 
               </div>

               <div class="col-12 mt-md25 mt20 f-18 f-md-22 w500 text-center lh140 white-clr text-capitalize">
                  <div class="postheadline f-20 f-md-24 w600 text-center lh140 white-clr text-capitalize">               
                  It comes with 40+ Done-For-You Products to Start Selling Today.<br class="d-none d-md-block"> Watch My Quick YoSeller Review
                 </div>
               </div>
               
                  
            </div>
            </div>
            <div class="row mt20 mt-md40">
               <div class="col-md-10 col-12 mx-auto">
                  <div class="col-12 responsive-video border-video">
                     <iframe src="https://yoseller.dotcompal.com/video/mydrive/182331" style="width:100%; height:100%" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                  </div>
                  <!-- <img src="assets/images/product-box.webp" class="img-fluid mx-auto d-block" alt="ProductBox"> -->
               </div>
            </div>
            <div class="row row-cols-md-2 row-cols-xl-2 row-cols-1 mt20 mt-md40 calendar-wrap mx-auto">
               <div class="col">
                  <ul class="list-head pl0 m0 f-18 f-md-18 lh150 w400 white-clr">
                  <li> <span class="w600 white-clr">Sell Your Own </span> or Use Our Done-For-You Products</li>
                  </ul>
               </div>
               <div class="col">
                  <ul class="list-head pl0 m0 f-18 f-md-18 lh150 w400 white-clr">
                  <li><span class="w600 white-clr">Launch White Label/Reseller/PLR</span> Licensed Products Fast</li>
                  </ul>
               </div>
               <div class="col">
                  <ul class="list-head pl0 m0 f-18 f-md-18 lh150 w400 white-clr">
                  <li>Create Stunning <span class="w600 white-clr">Websites, Stores, and Membership </span>Sites Quick and Easy</li>
                  </ul>
               </div>
               <div class="col">
                  <ul class="list-head pl0 m0 f-18 f-md-18 lh150 w400 white-clr">
                  <li>Built-in Cart with Seamless <span class="w600 white-clr">Integration to PayPal, Stripe, ClickBank, JVZoo</span></li>
                  </ul>
               </div>
               <div class="col">
                  <ul class="list-head pl0 m0 f-18 f-md-18 lh150 w400 white-clr">
                  <li><span class="w600 white-clr">Sell On Your Own Marketplace</span>  Keep 100% Profits</li>
                  </ul>
               </div>
               <div class="col">
                  <ul class="list-head pl0 m0 f-18 f-md-18 lh150 w400 white-clr">
                  <li>Get Orders from Social Media, Emails  On Any Page using <span class="w600 white-clr">Smart-Checkout Links</span></li>
                  </ul>
               </div>
               <div class="col">
                  <ul class="list-head pl0 m0 f-18 f-md-18 lh150 w400 white-clr">
                  <li><span class="w600 white-clr">200+ Beautiful Templates</span> and Next-Gen Drag-N-Drop Editor</li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!-- Header Section End -->
      <!-- Step Section Start -->
      <div class="step-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-md-45 f-28 w700 lh140 text-center black-clr">
                     Encash The Latest Trend in E-Selling In
                  </div>
                  <div class="f-md-45 f-28 w700 lh140 text-center white-clr step-headline mt10 mt-md20">
                     3 Easy Steps…
                  </div>
               </div>
               <div class="col-12 f-18  w500 lh140 text-center mt20 mt-md40">As festive selling season is around &amp; it will stay till valentine’s day in Feb, now you can start selling online quick &amp; easy.</div>
            </div>
            <div class="row mt60 mt-md100 align-items-center">
               <div class="col-md-6 col-12 relative">
                  <div class="step-box">
                     <div class="step-title f-20 f-md-22 w600">step #1</div>
                     <div class="f-24 f-md-36 w700 black-clr">Add A Product</div>
                     <div class="w400 f-18 f-md-22 lh140 mt10 grey-clr">
                        To begin, just add your product. If you do not have one, use from Preloaded 40+ Done-For-You Products to Quick Start. Your products will be delivered in the login secured members area on automation.
                     </div>
                  </div>
                  <img src="assets/images/step-arrow.webp" class="img-fluid d-none d-md-block step-arrow" alt="step Arrow">
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                  <img src="assets/images/step-one.webp" class="img-fluid d-none d-md-block">
               </div>
            </div>
            <div class="row mt40 mt-md160 align-items-center">
               <div class="col-md-6 col-12 order-md-2 relative">
                  <div class="step-box">
                     <div class="step-title1 f-20 f-md-22 w600">step #2</div>
                     <div class="f-24 f-md-36 w700 black-clr">Choose Payment Gateway</div>
                     <div class="w400 f-18 f-md-22 lh140 mt10 grey-clr">
                        Integrate your PayPal or Stripe account to receive payments directly into your account. YoSeller also comes with seamless integration with JVZoo, ClickBank and WarriorPlus
                     </div>
                  </div>
                  <img src="assets/images/step-arrow1.webp" class="img-fluid d-none d-md-block step-arrow1" alt="step Arrow">
               </div>
               <div class="col-12 col-md-6 order-md-1 mt20 mt-md0">
                  <img src="assets/images/step-two.webp" class="img-fluid d-none d-md-block">
               </div>
            </div>
            <div class="row mt40 mt-md200 align-items-center">
               <div class="col-md-6 col-12 relative">
                  <div class="step-box">
                     <div class="step-title f-20 f-md-22 w600">step #3</div>
                     <div class="f-24 f-md-36 w700 black-clr">Publish &amp; Profit</div>
                     <div class="w400 f-18 f-md-22 lh140 mt10 grey-clr">Now get your smart checkout link to start getting orders on product pages, social media, and right from your emails like a pro. Start selling online - Quick and Easy.
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-6 order-md-1 mt20 mt-md0">
                  <img src="assets/images/step-three.webp" class="img-fluid d-none d-md-block">
               </div>
            </div>           
         </div>
      </div>
      <!-- Step Section End -->
      <!-- CTA Btn Start-->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 yellow-clr">"YOSELLER"</span> for an Additional <span class="w700 yellow-clr">10% Discount</span> on Agency Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab YoSeller + My 15 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 smmltd">Days</span>
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Sec</span>
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Btn End -->
      <!-- Features Section Start -->
      <div class="list-section">
         <div class="container">
            <div class="row">
               <div class="col-12 relative">
                  <div class="featues-list-block">
                     <div class="row">
                        <div class="f-md-45 f-28 w700 lh140 text-capitalize text-center black-clr">
                           “We Guarantee that YoSeller is the Last App
                           You'll Ever Need to Start &amp; Run A Profitable <br class="d-none d-md-block">
                           E-Selling Business”                           
                        </div>
                        <div class="col-12 mt20 mt-md50 relative">
                           <div class="row header-list-block gx-md-5">
                              <div class="col-12 col-md-6">
                                 <ul class="features-list pl0 f-16 f-md-18 lh140 w400 black-clr text-capitalize">
                                       <li>Tap into the <span class="w600">Super-Hot $25 Trillion E-Selling Industry</span> </li>
                                       <li>Complete <span class="w600"> All-in-One Online Selling Platform  </span> for Entrepreneurs, Marketers, and Newbies </li>
                                       <li><span class="w600"> Quick Start  </span>  Your Online Selling Business with <span class="w600"> 40+ Done-For-You Products</span></li>
                                       <li><span class="w600"> Easily Create and Manage Unlimited Products -  </span> Digital Products, Courses, Services, and Goods </li>
                                       <li>Create  <span class="w600">Membership Sites to Deliver Products  </span> in Secured &amp; Password Protected Member's Area </li>
                                       <li><span class="w600">Create </span> Unlimited Beautiful, Mobile Ready and Fast-Loading <span class="w600"> Landing Pages Easily</span></li>
                                       <li><span class="w600">	Use Smart-Checkout Links   </span> to Directly <span class="w600">Receive Payments </span> from Social Media, Emails &amp; On Any Page.</li>
                                       <li><span class="w600">	Precise Analytics  </span>  to Measure the Performance and Know Exactly What's Working and What's Not</li>
                                       <li><span class="w600">SEO Friendly &amp; Social Media Optimized  </span> Pages for More Traffic</li>
                                       <li><span class="w600">100% GDPR and Can-Spam   </span> Compliant</li>
                                      <li><span class="w600">Completely Cloud-Based -</span> No Domain, Hosting, or Installation Required</li>
                                      <li>Launch Fast - Create Stunning  <span class="w600">Websites &amp; Stores  </span> for Your and Your Client's Business in Any Niche </li>
                                    </ul>
                              </div>
                              <div class="col-12 col-md-6 mt10 mt-md0">
                                 <ul class="features-list pl0 f-16 f-md-18 lh140 w400 black-clr text-capitalize">
                                     
                                       <li>	Sell Unlimited Products and Accept  <span class="w600">Payments Through PayPal and Stripe</span> with Zero Fee, you can Keep 100% of your Profit </li>
                                       <li>	Selling on ClickBank, JVZoo &amp; Warrior Plus? Get <span class="w600">Seamless Integration </span> to Deliver Products on Automation. </li>
                                       <li><span class="w600">100+ Battle-Tested, </span> Beautiful, and Mobile-Ready  <span class="w600"> Page Templates</span>  </li>
                                       <li>Fully Customizable  <span class="w600">Drag &amp; Drop WYSIWYG Page Editor  </span> that Requires Zero Designing or Tech Skills </li>
                                       <li><span class="w600"> Inbuilt Lead Management System </span> for Effective Contacts Management in Automation
                                       </li><li>Connect YoSeller with your Favorite Tools, <span class="w600">20+ Integrations </span> with Autoresponders and other Services. </li>
                                       <li> <span class="w600"> 128 Bit Secured, SSL Encryption </span>for Maximum Security of Your Files, Data, and Websites </li>
                                       <li> Manage all the Pages, Products &amp; Customers Hassle-Free, <span class="w600">All in Single Dashboard. </span>  </li>
                                       <li><span class="w600">Step-By-Step Video Training </span> and Tutorials Included </li>
                                       <li><span class="w600">Premium Customer Support</span>  </li>
                                       <li><span class="w600">Plus, you'll also Receive Agency to Start Selling Pro Level Services to your Clients Right Away</span>  </li>                              
                                    </ul>
                                 </div>
                              </div>       
                           </div>
                        </div>                    
                     </div>
                  <img src="assets/images/announcement.webp" class="img-fluid d-none d-md-block mx-auto horn">
                  <img src="assets/images/cart.webp" class="img-fluid d-none d-md-block mx-auto cart">
               </div>
            </div>
          
         </div>
      </div>
      <!-- Features Section End -->


      <!-- Proven Section -->
      <div class="proof-section">
         <div class="container">
            <div class="row">
               <div class="f-24 f-md-40 w400 lh140 text-capitalize text-center black-clr col-12">
                  YoSeller Is So Powerful That We Are Personally Using It <br class="d-none d-md-block">
                  <span class="f-28 f-md-42 w600">To Run Our 6-Figure Online Business Without a Hitch!</span>
               </div>
               
               <div class="col-12 mt20 mt-md50">
                  <img src="assets/images/proof.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md50">
               <div class="col-12 col-md-6 f-md-36 f-22 w600 lh140 black-clr">
                  By Selling Online, We Have Generated $287k in Sales in The Last 7 Months Alone.
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                  <img src="assets/images/proof1.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>
         </div>
      </div>
      <!-- Proven Section Ends-->


      <!-- Technology Section Start -->
      <div class="technology-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-12">
                  <div class="f-22 f-md-32 lh140 w600 text-center yellow-clr">
                     But it's not just us...               
                  </div>
               </div>
               <div class="col-12 col-md-12 mt20">
                  <div class="f-28 f-md-45 lh140 w600 text-center white-clr">
                     YoSeller is a Proven Solution Backed by the same 
                     Technology that has been Happily Served...                     
                  </div>
               </div>
            </div>
            <div class="row row-cols-1 row-cols-md-3 mt20 mt-md50">
               <div class="col">
                  <div class="d-flex align-items-center">
                     <img src="assets/images/cloud-y.webp" alt="cloud" class="img-fluid mr20">
                     <div class=" text-capitalize ">
                        <div class="f-md-42 f-24 w700 yellow-clr">
                           16,500
                        </div>
                        <div class="f-18 f-md-20 w500 white-clr">
                           Happy Customers
                        </div>
                     </div>
                  </div>
                  </div>
                  <div class="col mt20 mt-md0">
                  <div class="d-flex align-items-center">
                     <img src="assets/images/play.webp" alt="play" class="img-fluid mr20">
                     <div class="text-capitalize ">
                        <div class="f-md-42 f-24 w700 yellow-clr">
                           100 
                        </div>
                        <div class="f-18 f-md-20 w500 white-clr">
                           Million Visitors
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col mt20 mt-md0">
                  <div class="d-flex align-items-center">
                     <img src="assets/images/web-hosting.webp" alt="Web-Hosting" class="img-fluid mr20">
                     <div class="text-capitalize ">
                        <div class="f-md-42 f-24 w700 yellow-clr">
                           150,000+
                        </div>
                        <div class="f-18 f-md-20 w500 white-clr">
                           Conversions
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-12 col-md-12 mt20 mt-md70">
               <div class="f-24 f-md-36 lh140 w600 text-center white-clr">
                  It's Totally MASSIVE.
               </div>
               <img src="assets/images/massive-line.webp" alt="Protfolio" class="img-fluid d-block mx-auto">
               <div class="mt10 f-16 f-md-18 lh140 w400 text-center white-clr">
                  And now it's your turn to power your own business without worrying about a hefty price tag during this launch special deal.
               </div>
            </div>
            <div class="row mt30 mt-md60">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 yellow-clr">"YOSELLER"</span> for an Additional <span class="w700 yellow-clr">10% Discount</span> on Agency Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab YoSeller + My 15 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 smmltd">Days</span>
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Sec</span>
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- Technology Section End -->
      <!-- Testimonial Section Starts -->
      <div class="testimonial-section">
         <div class="container ">
            <div class="row ">
               <div class="col-12">
                  <div class="f-md-45 f-28 lh140 w400 text-center black-clr">Check Out What Our Beta Testers Are <br class="d-none d-md-block"><span class="w700">Saying About YoSeller!</span>  </div>
               </div>
            </div>
            <div class="row gx-md-5">
               <div class="col-12 col-md-6 mt20 mt-md50">
                  <div class="testimonials-block-border">
                     <div class="testimonials-block f-18 w400 lh140">
                        Wow!!! It’s an <span class="w600">amazing creation that allows me to launch and sell my products &amp; services EASY &amp; FAST.</span> Apart from its features, the best part I personally love is that it <span class="w600">comes at a one-time price for the next few days!</span> Complete value for your money. Guys go for it... 
                            
                     <div class="author-wrap mt20 mt-md20">
                        <img src="assets/images/testi-img1.webp" class="img-fluid">
                        <div class="f-22 w600 lh140 pl15">Drake Lewis</div>
                     </div>	             							
                     </div>	                     								
                  </div>							
               </div>
               <div class="col-12 col-md-6 mt20 mt-md50">
                  <div class="testimonials-block-border">
                     <div class="testimonials-block f-18 w400 lh140">
                        Still looking for a solution that <span class="w600">enables you to set up &amp; manage your online selling business completely from one central dashboard</span> like a pro that entices visitors and converts them into lifetime customers; <span class="w600">YoSeller is what you’re looking for</span>...                          
                     <div class="author-wrap mt20 mt-md20">
                        <img src="assets/images/testi-img2.webp" class="img-fluid">
                        <div class="f-22 w600 lh140 pl15">Mike East</div>
                     </div>	             							
                     </div>	                     								
                  </div>								
               </div>
               <div class="col-12 col-md-6 mt20 mt-md30">
                  <div class="testimonials-block-border">
                     <div class="testimonials-block f-18 w400 lh140">
                        YoSeller is a revolutionary <span class="w600">all-in-one technology</span> that lets you take a plunge into the HUGE <span class="w600">Freelancing, E-Selling, and Info-Selling industry with no tech hassles ever.</span> The biggest benefit, you can drive tons of traffic and boost ROI for your efforts. Grab it before it flies away forever...
                     <div class="author-wrap mt20 mt-md20">
                        <img src="assets/images/testi-img3.webp" class="img-fluid">
                        <div class="f-22 w600 lh140 pl15">Harry Joe</div>
                     </div>	             							
                     </div>	                     								
                  </div>								
               </div>
               <div class="col-12 col-md-6 mt20 mt-md30">
                  <div class="testimonials-block-border">
                     <div class="testimonials-block f-18 w400 lh140">
                        Hey guys, <span class="w600">wanted to give you big applause.</span> I used YoSeller and yes, I have never seen results like this before. Instead of taking days or juggling with technicalities, <span class="w600">this technology gives a push-button solution.</span> It is a very versatile tool as I too can take <span class="w600">complete control of my e-selling business without any third-party dependence.</span> This is something that you can’t overlook...
                     <div class="author-wrap mt20 mt-md20">
                        <img src="assets/images/testi-img4.webp" class="img-fluid">
                        <div class="f-22 w600 lh140 pl15">Penelope White</div>
                     </div>	             							
                     </div>	                     								
                  </div>								
               </div>
            </div>
         </div>
      </div>
      <!-- Testimonial Section Ends -->

      <!-- Proudly Section Start -->
      <div class="proudly-sec" id="product">
      <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-24 f-md-38 w600 lh140 white-clr intro-title">
                     Presenting…
                  </div>
               </div>
               <div class="col-12 mt-md30 mt20 text-center">
                  <svg version="1.1" id="Layer_11" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 253.7 80" style="enable-background:new 0 0 253.7 80; max-height:180px;" xml:space="preserve">
                     ;
                     <style type="text/css">
                        .st00{fill:#FFD60D;}
                        .st11{fill:#FFFFFF;}
                        .st22{fill:#2337F6;}
                     </style>
                     <g>
                        <path class="st00" d="M130.2,64.2l-4.8,1.1c-0.4-0.9-1.1-1.6-1.9-2.2c-1-0.7-2.2-1.1-3.5-1.1c-1.7,0-3.3,0.7-4.4,1.9
                           c-1,1-1.6,2.4-1.7,3.9l-14.1,3.1c-1.1-2.5-3.6-4.3-6.5-4.3c-3.9,0-7.1,3.2-7.1,7.1v0.1l-1.8,0.4c-3.3,0.8-6.6-1.3-7.3-4.6l-10-43.9
                           c-0.8-3.3,1.3-6.6,4.6-7.3l9.5-2.1v0.6c0.1,1.4,1.3,2.5,2.7,2.5c0.1,0,0.1,0,0.2,0c1.5-0.1,2.6-1.3,2.5-2.8c0-0.5-0.2-0.9-0.4-1.3
                           l14.2-3.1v0.5c0,0.7,0.3,1.3,0.8,1.8c0.5,0.4,1.1,0.7,1.8,0.7c0.1,0,0.1,0,0.2,0c1.5-0.1,2.6-1.3,2.5-2.8c0-0.5-0.2-0.9-0.4-1.3
                           l12.4-2.7c3.3-0.8,6.6,1.3,7.3,4.6l2,8.8l1.1,5l6.8,30.2C135.7,60.3,133.6,63.6,130.2,64.2z"></path>
                        <g>
                           <path class="st11" d="M34.4,21.8L20.7,48.3v15.1h-6.8V48.3L0,21.8h7.6l9.6,20.4l9.6-20.4C26.8,21.8,34.4,21.8,34.4,21.8z"></path>
                           <path class="st11" d="M41.5,61.8c-2.5-1.4-4.5-3.4-5.9-6c-1.4-2.6-2.2-5.6-2.2-8.9c0-3.4,0.7-6.3,2.2-8.9c1.5-2.6,3.5-4.6,6-6
                              c2.6-1.4,5.4-2.1,8.6-2.1s6,0.7,8.6,2.1c2.6,1.4,4.6,3.4,6,6c1.5,2.6,2.2,5.6,2.2,8.9c0,3.4-0.8,6.3-2.3,8.9
                              c-1.5,2.6-3.6,4.6-6.2,6c-2.6,1.4-5.5,2.1-8.6,2.1C46.8,63.9,44,63.2,41.5,61.8z M54.9,56.7c1.5-0.8,2.8-2.1,3.7-3.8
                              c1-1.7,1.4-3.7,1.4-6.1c0-2.4-0.5-4.4-1.4-6.1c-0.9-1.7-2.1-2.9-3.7-3.7c-1.5-0.8-3.2-1.3-4.9-1.3c-1.8,0-3.4,0.4-4.9,1.3
                              c-1.5,0.8-2.7,2.1-3.6,3.7c-0.9,1.7-1.3,3.7-1.3,6.1c0,3.6,0.9,6.3,2.7,8.2c1.8,1.9,4.1,2.9,6.9,2.9C51.7,58,53.3,57.5,54.9,56.7z
                              "></path>
                        </g>
                        <g>
                           <path class="st11" d="M167.1,50.3h-23.7c0.2,2.5,1.1,4.5,2.8,6c1.7,1.5,3.7,2.3,6.1,2.3c3.5,0,5.9-1.4,7.3-4.3h6.9
                              c-0.9,2.9-2.6,5.2-5.1,7c-2.5,1.8-5.5,2.7-9.1,2.7c-3,0-5.6-0.7-8-2c-2.3-1.3-4.2-3.2-5.5-5.6s-2-5.2-2-8.4c0-3.2,0.6-6,1.9-8.4
                              c1.3-2.4,3.1-4.3,5.5-5.6c2.3-1.3,5-2,8.1-2c2.9,0,5.5,0.6,7.8,1.9c2.3,1.3,4.1,3.1,5.3,5.4c1.3,2.3,1.9,5,1.9,8
                              C167.3,48.3,167.2,49.3,167.1,50.3z M160.6,45.1c0-2.4-0.9-4.3-2.5-5.7c-1.7-1.4-3.7-2.1-6.1-2.1c-2.2,0-4.1,0.7-5.7,2.1
                              c-1.6,1.4-2.5,3.3-2.8,5.7H160.6z"></path>
                           <path class="st11" d="M180,21.8v41.6h-6.4V21.8H180z"></path>
                           <path class="st11" d="M194.9,21.8v41.6h-6.4V21.8H194.9z"></path>
                           <path class="st11" d="M231.5,50.3h-23.7c0.2,2.5,1.1,4.5,2.8,6c1.6,1.5,3.7,2.3,6.1,2.3c3.5,0,5.9-1.4,7.3-4.3h6.9
                              c-0.9,2.9-2.6,5.2-5.1,7c-2.5,1.8-5.5,2.7-9.1,2.7c-3,0-5.6-0.7-8-2c-2.3-1.3-4.2-3.2-5.5-5.6c-1.3-2.4-2-5.2-2-8.4
                              c0-3.2,0.6-6,1.9-8.4c1.3-2.4,3.1-4.3,5.5-5.6c2.3-1.3,5-2,8.1-2c2.9,0,5.5,0.6,7.8,1.9c2.3,1.3,4.1,3.1,5.3,5.4
                              c1.3,2.3,1.9,5,1.9,8C231.7,48.3,231.7,49.3,231.5,50.3z M225,45.1c0-2.4-0.9-4.3-2.5-5.7c-1.7-1.4-3.7-2.1-6.1-2.1
                              c-2.2,0-4.1,0.7-5.7,2.1c-1.6,1.4-2.5,3.3-2.8,5.7H225z"></path>
                           <path class="st11" d="M248.2,33.2c1.6-0.9,3.4-1.3,5.5-1.3v6.6h-1.6c-2.5,0-4.4,0.6-5.7,1.9s-1.9,3.5-1.9,6.6v16.3H238v-31h6.4v4.5
                              C245.4,35.3,246.6,34.1,248.2,33.2z"></path>
                        </g>
                        <circle class="st11" cx="93.4" cy="73.7" r="6.3"></circle>
                        <path class="st22" d="M131.6,51.7c0,5.2-3.4,9.7-8,11.4c-1-0.7-2.2-1.1-3.5-1.1c-1.7,0-3.3,0.7-4.4,1.9H78.9c0-3.9,3.1-7,7-7h33.3
                           c2.9,0,5.3-2.3,5.3-5.2s-2.3-5.3-5.3-5.3h-11.4c-3.4,0-6.5-1.4-8.8-3.7c-2.2-2.2-3.5-5.3-3.5-8.7c0.1-6.8,5.6-12.2,12.4-12.2h22.2
                           c0,1.9-0.8,3.7-2.1,5s-3,2-4.9,2H108c-2.9,0-5.3,2.3-5.3,5.2s2.3,5.3,5.3,5.3h11.4c3.4,0,6.5,1.4,8.8,3.7
                           C130.3,45.3,131.6,48.3,131.6,51.7z"></path>
                        <path class="st22" d="M97.7,22.8c0.4-0.4,0.8-0.7,1.2-1H71.5l0,0c0,3.9,3.1,7,7,7h15.3C94.7,26.6,96,24.5,97.7,22.8z"></path>
                        <path class="st22" d="M85.6,38.8h8.1c-0.5-1.6-0.7-3.2-0.7-4.9c0-0.7,0.1-1.4,0.2-2.1H78.6l0,0C78.6,35.7,81.7,38.8,85.6,38.8z"></path>
                        <path class="st11" d="M104.3,11.2c-0.1-1-0.2-2.1-0.6-3.1C102,2.9,97-0.3,92,0c-0.5,0-1.1,0.1-1.6,0.2c-0.4,0.1-0.7,0.2-1.1,0.3
                           c-6,2-9.1,8.6-7.1,14.7c0.1,0.2,0.1,0.4,0.2,0.6c-0.2,0.3-0.2,0.7-0.2,1c0.1,1,0.9,1.8,1.9,1.7s1.8-0.9,1.7-1.9
                           c0-0.6-0.4-1.2-1-1.5c-0.1-0.2-0.2-0.4-0.2-0.6C83,9.6,85.5,4.5,90.1,3s9.6,1.1,11.2,5.9c0.3,0.8,0.4,1.6,0.5,2.3
                           c-0.4,0.4-0.6,0.9-0.5,1.4c0.1,1,0.9,1.8,1.9,1.7c1-0.1,1.8-0.9,1.7-1.9C104.8,12,104.6,11.5,104.3,11.2z"></path>
                        <circle class="st11" cx="120.1" cy="68.1" r="5.2"></circle>
                     </g>
                  </svg>
               </div>
               <div class="col-12 f-md-38 f-22 mt-md30 mt20 w600 text-center white-clr lh140">
                  One of a Kind, All-In-One Platform to Market and Sell Any Type of Digital Products, Courses, Services, and Physical Goods with Zero Technical Hassle. 
               </div>
            </div>
            <div class="row mt30 mt-md50 align-items-center">
               <div class="col-12 col-md-6">
                  <img src="assets/images/product-image.webp" class="img-fluid d-block mx-auto img-animation">
               </div>
               <div class="col-md-6 mt20 mt-md0">
                  <div class="proudly-list-bg">
                     <div class="col-12">
                        <ul class="proudly-list f-18 w500 lh140 black-clr">
                           <li>
                              <span class="f-22 f-md-24 w600">All-In-One Business Platform</span>
                              <div class="f-18 f-md-18 w400 mt5">Everything you need to sell online, from Memberships Site, Website, Cart & Payments, Landing Pages, & Much More </div>
                           </li>
                           <li>
                              <span class="f-22 f-md-24 w600">Fast & Easy to Launch & Sell Online</span>
                              <div class="f-18 f-md-18 w400 mt5">Get Max Conversions, Leads & Sales with Zero Tech Hassles for your Digital Products, Courses, Services, & Goods.</div>
                           </li>
                           <li>
                              <span class="f-22 f-md-24 w600">Agency License to Serve Clients</span>
                              <div class="f-18 f-md-18 w400 mt5">Launch & Sell Your Own Products or Charge Your Clients & Keep 100% Profits</div>
                           </li>
                           <li>
                              <span class="f-22 f-md-24 w600">No Monthly Recurring Charges</span>
                              <div class="f-18 f-md-18 w400 mt5">During This Launch Special Deal, Get All Benefits at Limited Low One-Time-Fee.</div>
                           </li>
                           <li>
                              <span class="f-22 f-md-24 w600">50+ More Exciting Features</span>
                              <div class="f-18 f-md-18 w400 mt5">We've Left No Stone Unturned to Give You an Unmatched Experience</div>
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         </div>
      <!-- Proudly Section End  -->
      <!-- Bonus Section Header Start -->
      <div class="bonus-header">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-10 mx-auto heading-bg text-center">
                  <div class="f-24 f-md-36 lh140 w700"> When You Purchase YoSeller, You Also Get <br class="hidden-xs"> Instant Access To These 15 Exclusive Bonuses</div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus Section Header End -->
      <!-- Bonus #1 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 1</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus1.webp" class="img-fluid mx-auto d-block" alt="Bonus1">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        How To Start Online Coaching Business
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>Everybody is an expert (or 'expert enough') in at least one body of knowledge.  It doesn't matter what it is. </b></li>
                           <li> Maybe you know how to sing a little bit better than everybody else, maybe you know your way around the basketball court, maybe you have discovered a way of running a little bit faster, or maybe you know how to make money with Twitter or Facebook.</li>
                           <li> Regardless of who you're dealing with, everybody has at least one area of expertise.</li>
                           <li>There is a tremendous demand for online coaching services and will continue to rise in the foreseeable future.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #1 Section End -->
      <!-- Bonus #2 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 2</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30">
                  <div class="row">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="assets/images/bonus2.webp" class="img-fluid mx-auto d-block" alt="Bonus2">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md0 text-capitalize">
                        Infopreneur Academy
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>You Get to Learn Directly From a Successful Product Creator who is behind 7 figure product launches. This Step by Step 7 Part Video Course Will Show You How You Can Create Products and Decrease Refund Rates and Support Issues. There's No Theory Here! </li>
                           <li>This step by step, 7 part video series, takes you by the hand and shows you how to create a quality informational products, the right way! There's no theory involved here like other video series. You learn directly from a successful product developer who is behind 7 figure launches that went thru a lot of costly and time consuming mistakes; so that you don't have to.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #2 Section End -->
      <!-- Bonus #3 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 3</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus3.webp" class="img-fluid mx-auto d-block " alt="Bonus3">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Launch Your Online Course
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>Many people have switched to remote learning and online courses to get a leg up on their education and improve upon their intellectual skills.</b></li>
                           <li>This is a shocking increase in online learning that proves the importance of online course options in the modern world.</li>
                           <li>On top of the numerous benefits for students that come with online learning, creating online courses can have great benefits for the creators too. </li>
                           <li>The reason for this is that online course creators can charge for their courses, resulting in an increased income.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #3 Section End -->
      <!-- Bonus #4 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 4</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="assets/images/bonus4.webp" class="img-fluid mx-auto d-block " alt="Bonus4">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Course Engagement Hacks
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>Even though you have an excellent course, people's attention spans are short, and they can get sidetracked easily. </li>
                           <li>The truth is that if they don't complete your online course, then they will get buyer's remorse in the end and won't feel motivated to buy your other classes. That said, if, however they complete the online course, then they will at least if the course is good mini will feel that they got the right amount of value from you.</li>
                           <li><b>More consumption equals more trust.<br>More trust equals more sales to your other courses.</b></li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #4 End -->
      <!-- Bonus #5 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 5</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus5.webp" class="img-fluid mx-auto d-block " alt="Bonus5">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        eWriterPro - Professional eBook Creator
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>Everything You Need to Create Beautiful Professional Quality eBooks at the Touch of a Button.</li>
                           <li>If you are looking for a way to ethically make money on the internet, then the creation of information products is by far the easiest and most rewarding way! </li>
                           <li>An $8Billion Market, the opportunity of the millennium is - eBooks! </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #5 End -->
      <!-- CTA Button Section Start -->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 yellow-clr">"YOSELLER"</span> for an Additional <span class="w700 yellow-clr">10% Discount</span> on Agency Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab YoSeller + My 15 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                        <span class="f-14 f-md-18 w500 ">Days</span>
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                        <span class="f-14 f-md-18 w500 ">Hours</span>
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                        <span class="f-14 f-md-18 w500 ">Mins</span>
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                        <span class="f-14 f-md-18 w500 ">Sec</span>
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->
      <!-- Bonus #6 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 6</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="assets/images/bonus6.webp" class="img-fluid mx-auto d-block " alt="Bonus6">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Instant Video Creator
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>Give Your Web Site a Live touch - instantly add streaming video to their web site without expensive equipment, hiring expensive services or paying costly monthly fees!
                           </li>
                           <li>Instant Video Creator is a revolutionary new software that allows multiple users to create their videos under their own accounts. Not only can you use it to create just your own streaming videos, you can also start your own service where you charge a monthly fee to host such videos for your customers. </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #6 End -->
      <!-- Bonus #7 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 7</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus7.webp" class="img-fluid mx-auto d-block " alt="Bonus7">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Create Video with Camtasia 9 Advanced
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <b>This Is a Practical Video Course With 15 Additional Video Tutorials Showing You How to Do What You Learned in The Course.</b><br><br>
                           <li>Interactive Hotspot </li>
                           <li>Interactive Hosting </li>
                           <li>Survey Creation </li>
                           <li>Screen Drawing</li>
                           <li>Picture In Picture Video</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #7 End -->
      <!-- Bonus #8 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 8</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus8.webp" class="img-fluid mx-auto d-block " alt="Bonus8">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Project Genius
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>Do you have projects to plan? Do you want to take a professional approach? Do you want to increase your success rate? Then Project Genius is the software that will help you plan your projects! </li>
                           <li>From now on, you will be able to take a professional approach to your project planning and increase your success rate. You will no longer have surprises during the course of your project.</li>
                           <li>You will better know what you want, your will organize your brain storming sessions better, and you will set your milestones and deadlines with ease. </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #8 End -->
      <!-- Bonus #9 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 9</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus9.webp" class="img-fluid mx-auto d-block " alt="Bonus9">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Success Graphics Web Design Package
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>A Great Collection of Header Graphics, Matching Footers, Backgrounds & Buy Buttons - In .PSD & Blank .JPGs Ready-To-Use Formats!</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #9 End -->
      <!-- Bonus #10 start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 10</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus10.webp" class="img-fluid mx-auto d-block " alt="Bonus10">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        PLR Profit Machine
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>PLR material is content, which you can utilize as if you were the one who created it.  </b></li>
                           <li>In truth, another individual has done every task, making your job easier specifically in establishing and marketing information necessary in selling a product or service online.</li>
                           <li>It is a comprehensive course showing how to become proficient and profitable with PLR products.</li>
                           <li>The methods used in this course come from experience and actual teachings to ensure that everything taught here are feasible.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #10 End -->
      <!-- CTA Button Section Start -->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 yellow-clr">"YOSELLER"</span> for an Additional <span class="w700 yellow-clr">10% Discount</span> on Agency Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab YoSeller + My 15 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 smmltd">Days</span> </div>
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">0300</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                     <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                     <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->
      <!-- Bonus #11 start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 11</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus11.webp" class="img-fluid mx-auto d-block " alt="Bonus13">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Template Showcase Builder
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li><b>Build an Amazing Responsive Template Quickly! </b></li>
                           <li>It is a 'responsive template' viewer. What this means is you can show off yours or affiliate templates and allow visitors to view template in different sizes!</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #11 End -->
      <!-- Bonus #12 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 12</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus12.webp" class="img-fluid mx-auto d-block " alt="Bonus12">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        The Entrepreneur Code
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>Have you ever wondered if there is some kind of “code” that some entrepreneurs are able to break and others spend their whole lives working on and never becoming successful?</b></li>
                           <li>Well, there is, and unlocking that code will give you the same exact success as some of the world's richest people. </li>
                           <li>In this course you will find a list of the 50 most common habits among success stories like Bill Gates, Larry Page and even Barack Obama! Make their habits your habits and you can crack the code and become just as big of successes as they are!</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #12 End -->
      <!-- Bonus #13 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 13</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus13.webp" class="img-fluid mx-auto d-block " alt="Bonus13">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Web Conversion Videos
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>Learn How to Make More Money On the Existing Traffic of Your Website!</b></li>
                           <li>Does Your Site Convert The Maximum Number of Visitors to Customers? If not, you are just wasting your time marketing your brand or your website.</li>
                           <li>These videos cover the factors that will help your website increase its conversion rate beyond anything you have ever hoped for.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #13 End -->
      <!-- Bonus #14 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 14</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus14.webp" class="img-fluid mx-auto d-block " alt="Bonus14">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        10 Keys Of Product Creation Success
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           This course is a checklist of things that you want to consider throughout your WSO or product launch. <br><br>
                           <b>Topics covered:</b><br><br>

                           <li>What are the elements of top selling products?</li>
                           <li>What should your content be to drive traffic?</li>
                           <li>What are the keys to to good copy?</li>
                           <li>What are the 8 numbers you must track?</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #14 End -->
      <!-- Bonus #15 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 15</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus15.webp" class="img-fluid mx-auto d-block " alt="Bonus15">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Make First $100 On The Web
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>One of the easiest ways to get started in internet marketing is to promote affiliate products.</b> </li>
                           <li>This means you get a commission for each sale of a particular product that comes from your link or referral. It's definitely the easiest route into making money online.</li>
                           <li>This course will help you get started with all the information you need.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #15 End -->
      
      <!-- Huge Woth Section Start -->
      <div class="huge-area mt30 mt-md10">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="f-md-65 f-40 lh120 w700 white-clr">That's Huge Worth of</div>
                  <br>
                  <div class="f-md-60 f-40 lh120 w800 yellow-clr">$3300!</div>
               </div>
            </div>
         </div>
      </div>
      <!-- Huge Worth Section End -->
      <!-- text Area Start -->
      <div class="white-section pb0">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="f-md-36 f-25 lh140 w500">So what are you waiting for? You have a great opportunity <br class="hidden-xs"> ahead + <span class="w700 ">My 15 Bonus Products</span> are making it a <br class="hidden-xs"> <span class="w700 ">completely NO Brainer!!</span></div>
               </div>
            </div>
         </div>
      </div>
      <!-- text Area End -->
      <!-- CTA Button Section Start -->
      <div class="cta-btn-section">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center">
                  <a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab YoSeller + My 15 Exclusive Bonuses</span> 
                  </a>
               </div>
               <div class="col-12 mt15 mt-md20" align="center">
                  <h3 class="f-md-28 f-20 w500 text-center black">My Exclusive Bonuses Are Expiring in...</h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 col-md-10 mx-auto col-12 text-center">
                  <div class="countdown counter-black">
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                     <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                     <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->
      <div class="footer-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <svg version="1.1" id="Layer_11" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 253.7 80" style="enable-background:new 0 0 253.7 80; max-height:60px;" xml:space="preserve">
                     ;
                     <style type="text/css">
                        .st00{fill:#FFD60D;}
                        .st11{fill:#FFFFFF;}
                        .st22{fill:#2337F6;}
                     </style>
                     <g>
                        <path class="st00" d="M130.2,64.2l-4.8,1.1c-0.4-0.9-1.1-1.6-1.9-2.2c-1-0.7-2.2-1.1-3.5-1.1c-1.7,0-3.3,0.7-4.4,1.9
                           c-1,1-1.6,2.4-1.7,3.9l-14.1,3.1c-1.1-2.5-3.6-4.3-6.5-4.3c-3.9,0-7.1,3.2-7.1,7.1v0.1l-1.8,0.4c-3.3,0.8-6.6-1.3-7.3-4.6l-10-43.9
                           c-0.8-3.3,1.3-6.6,4.6-7.3l9.5-2.1v0.6c0.1,1.4,1.3,2.5,2.7,2.5c0.1,0,0.1,0,0.2,0c1.5-0.1,2.6-1.3,2.5-2.8c0-0.5-0.2-0.9-0.4-1.3
                           l14.2-3.1v0.5c0,0.7,0.3,1.3,0.8,1.8c0.5,0.4,1.1,0.7,1.8,0.7c0.1,0,0.1,0,0.2,0c1.5-0.1,2.6-1.3,2.5-2.8c0-0.5-0.2-0.9-0.4-1.3
                           l12.4-2.7c3.3-0.8,6.6,1.3,7.3,4.6l2,8.8l1.1,5l6.8,30.2C135.7,60.3,133.6,63.6,130.2,64.2z"></path>
                        <g>
                           <path class="st11" d="M34.4,21.8L20.7,48.3v15.1h-6.8V48.3L0,21.8h7.6l9.6,20.4l9.6-20.4C26.8,21.8,34.4,21.8,34.4,21.8z"></path>
                           <path class="st11" d="M41.5,61.8c-2.5-1.4-4.5-3.4-5.9-6c-1.4-2.6-2.2-5.6-2.2-8.9c0-3.4,0.7-6.3,2.2-8.9c1.5-2.6,3.5-4.6,6-6
                              c2.6-1.4,5.4-2.1,8.6-2.1s6,0.7,8.6,2.1c2.6,1.4,4.6,3.4,6,6c1.5,2.6,2.2,5.6,2.2,8.9c0,3.4-0.8,6.3-2.3,8.9
                              c-1.5,2.6-3.6,4.6-6.2,6c-2.6,1.4-5.5,2.1-8.6,2.1C46.8,63.9,44,63.2,41.5,61.8z M54.9,56.7c1.5-0.8,2.8-2.1,3.7-3.8
                              c1-1.7,1.4-3.7,1.4-6.1c0-2.4-0.5-4.4-1.4-6.1c-0.9-1.7-2.1-2.9-3.7-3.7c-1.5-0.8-3.2-1.3-4.9-1.3c-1.8,0-3.4,0.4-4.9,1.3
                              c-1.5,0.8-2.7,2.1-3.6,3.7c-0.9,1.7-1.3,3.7-1.3,6.1c0,3.6,0.9,6.3,2.7,8.2c1.8,1.9,4.1,2.9,6.9,2.9C51.7,58,53.3,57.5,54.9,56.7z
                              "></path>
                        </g>
                        <g>
                           <path class="st11" d="M167.1,50.3h-23.7c0.2,2.5,1.1,4.5,2.8,6c1.7,1.5,3.7,2.3,6.1,2.3c3.5,0,5.9-1.4,7.3-4.3h6.9
                              c-0.9,2.9-2.6,5.2-5.1,7c-2.5,1.8-5.5,2.7-9.1,2.7c-3,0-5.6-0.7-8-2c-2.3-1.3-4.2-3.2-5.5-5.6s-2-5.2-2-8.4c0-3.2,0.6-6,1.9-8.4
                              c1.3-2.4,3.1-4.3,5.5-5.6c2.3-1.3,5-2,8.1-2c2.9,0,5.5,0.6,7.8,1.9c2.3,1.3,4.1,3.1,5.3,5.4c1.3,2.3,1.9,5,1.9,8
                              C167.3,48.3,167.2,49.3,167.1,50.3z M160.6,45.1c0-2.4-0.9-4.3-2.5-5.7c-1.7-1.4-3.7-2.1-6.1-2.1c-2.2,0-4.1,0.7-5.7,2.1
                              c-1.6,1.4-2.5,3.3-2.8,5.7H160.6z"></path>
                           <path class="st11" d="M180,21.8v41.6h-6.4V21.8H180z"></path>
                           <path class="st11" d="M194.9,21.8v41.6h-6.4V21.8H194.9z"></path>
                           <path class="st11" d="M231.5,50.3h-23.7c0.2,2.5,1.1,4.5,2.8,6c1.6,1.5,3.7,2.3,6.1,2.3c3.5,0,5.9-1.4,7.3-4.3h6.9
                              c-0.9,2.9-2.6,5.2-5.1,7c-2.5,1.8-5.5,2.7-9.1,2.7c-3,0-5.6-0.7-8-2c-2.3-1.3-4.2-3.2-5.5-5.6c-1.3-2.4-2-5.2-2-8.4
                              c0-3.2,0.6-6,1.9-8.4c1.3-2.4,3.1-4.3,5.5-5.6c2.3-1.3,5-2,8.1-2c2.9,0,5.5,0.6,7.8,1.9c2.3,1.3,4.1,3.1,5.3,5.4
                              c1.3,2.3,1.9,5,1.9,8C231.7,48.3,231.7,49.3,231.5,50.3z M225,45.1c0-2.4-0.9-4.3-2.5-5.7c-1.7-1.4-3.7-2.1-6.1-2.1
                              c-2.2,0-4.1,0.7-5.7,2.1c-1.6,1.4-2.5,3.3-2.8,5.7H225z"></path>
                           <path class="st11" d="M248.2,33.2c1.6-0.9,3.4-1.3,5.5-1.3v6.6h-1.6c-2.5,0-4.4,0.6-5.7,1.9s-1.9,3.5-1.9,6.6v16.3H238v-31h6.4v4.5
                              C245.4,35.3,246.6,34.1,248.2,33.2z"></path>
                        </g>
                        <circle class="st11" cx="93.4" cy="73.7" r="6.3"></circle>
                        <path class="st22" d="M131.6,51.7c0,5.2-3.4,9.7-8,11.4c-1-0.7-2.2-1.1-3.5-1.1c-1.7,0-3.3,0.7-4.4,1.9H78.9c0-3.9,3.1-7,7-7h33.3
                           c2.9,0,5.3-2.3,5.3-5.2s-2.3-5.3-5.3-5.3h-11.4c-3.4,0-6.5-1.4-8.8-3.7c-2.2-2.2-3.5-5.3-3.5-8.7c0.1-6.8,5.6-12.2,12.4-12.2h22.2
                           c0,1.9-0.8,3.7-2.1,5s-3,2-4.9,2H108c-2.9,0-5.3,2.3-5.3,5.2s2.3,5.3,5.3,5.3h11.4c3.4,0,6.5,1.4,8.8,3.7
                           C130.3,45.3,131.6,48.3,131.6,51.7z"></path>
                        <path class="st22" d="M97.7,22.8c0.4-0.4,0.8-0.7,1.2-1H71.5l0,0c0,3.9,3.1,7,7,7h15.3C94.7,26.6,96,24.5,97.7,22.8z"></path>
                        <path class="st22" d="M85.6,38.8h8.1c-0.5-1.6-0.7-3.2-0.7-4.9c0-0.7,0.1-1.4,0.2-2.1H78.6l0,0C78.6,35.7,81.7,38.8,85.6,38.8z"></path>
                        <path class="st11" d="M104.3,11.2c-0.1-1-0.2-2.1-0.6-3.1C102,2.9,97-0.3,92,0c-0.5,0-1.1,0.1-1.6,0.2c-0.4,0.1-0.7,0.2-1.1,0.3
                           c-6,2-9.1,8.6-7.1,14.7c0.1,0.2,0.1,0.4,0.2,0.6c-0.2,0.3-0.2,0.7-0.2,1c0.1,1,0.9,1.8,1.9,1.7s1.8-0.9,1.7-1.9
                           c0-0.6-0.4-1.2-1-1.5c-0.1-0.2-0.2-0.4-0.2-0.6C83,9.6,85.5,4.5,90.1,3s9.6,1.1,11.2,5.9c0.3,0.8,0.4,1.6,0.5,2.3
                           c-0.4,0.4-0.6,0.9-0.5,1.4c0.1,1,0.9,1.8,1.9,1.7c1-0.1,1.8-0.9,1.7-1.9C104.8,12,104.6,11.5,104.3,11.2z"></path>
                        <circle class="st11" cx="120.1" cy="68.1" r="5.2"></circle>
                     </g>
                  </svg>
                  <div editabletype="text" class="f-16 f-md-16 w300 mt20 lh140 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-14 f-md-16 w400 lh150 white-clr text-xs-center">Copyright © YoSeller 2022</div>
                  <ul class="footer-ul w400 f-14 f-md-16 white-clr text-center text-md-right">
                     <li><a href="https://support.oppyo.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://yoseller.co/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://yoseller.co/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://yoseller.co/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://yoseller.co/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://yoseller.co/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://yoseller.co/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!-- timer --->
      <?php
         if ($now < $exp_date) {
         ?>
      <script type="text/javascript">
         // Count down milliseconds = server_end - server_now = client_end - client_now
         var server_end = <?php echo $exp_date; ?> * 1000;
         var server_now = <?php echo time(); ?> * 1000;
         var client_now = new Date().getTime();
         var end = server_end - server_now + client_now; // this is the real end time
         
         var noob = $('.countdown').length;
         
         var _second = 1000;
         var _minute = _second * 60;
         var _hour = _minute * 60;
         var _day = _hour * 24
         var timer;
         
         function showRemaining() {
         var now = new Date();
         var distance = end - now;
         if (distance < 0) {
         	clearInterval(timer);
         	document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
         	return;
         }
         
         var days = Math.floor(distance / _day);
         var hours = Math.floor((distance % _day) / _hour);
         var minutes = Math.floor((distance % _hour) / _minute);
         var seconds = Math.floor((distance % _minute) / _second);
         if (days < 10) {
         	days = "0" + days;
         }
         if (hours < 10) {
         	hours = "0" + hours;
         }
         if (minutes < 10) {
         	minutes = "0" + minutes;
         }
         if (seconds < 10) {
         	seconds = "0" + seconds;
         }
         var i;
         var countdown = document.getElementsByClassName('countdown');
         for (i = 0; i < noob; i++) {
         	countdown[i].innerHTML = '';
         
         	if (days) {
         		countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + days + '</span><span class="f-14 f-md-18 smmltd">Days</span> </div>';
         	}
         
         	countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + hours + '</span><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>';
         
         	countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">' + minutes + '</span><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>';
         
         	countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">' + seconds + '</span><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>';
         }
         
         }
         timer = setInterval(showRemaining, 1000);
         	
      </script>
      <?php
         } else {
         echo "Times Up";
         }
         ?>
      <!--- timer end-->
   </body>
</html>
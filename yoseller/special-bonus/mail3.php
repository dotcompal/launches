<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <!-- Tell the browser to be responsive to screen width -->
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
      <meta name="title" content="YoSeller Bonuses">
      <meta name="description" content="YoSeller Bonuses">
      <meta name="keywords" content="Revealing: Cutting-Edge Technology to Launch & Sell Digital Products, Courses, Services & Goods Online with Zero Commission and No Monthly Fee Ever">
      <meta property="og:image" content="https://www.yoseller.co/special-bonus/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Atul">
      <!-- Open Graph / Facebook -->
       <!-- Open Graph / Facebook -->
       <meta property="og:type" content="website">
      <meta property="og:title" content="YoSeller Bonuses">
      <meta property="og:description" content="Revealing: Cutting-Edge Technology to Launch & Sell Digital Products, Courses, Services & Goods Online with Zero Commission and No Monthly Fee Ever">
      <meta property="og:image" content="https://www.yoseller.co/special-bonus/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="YoSeller Bonuses">
      <meta property="twitter:description" content="Revealing: Cutting-Edge Technology to Launch & Sell Digital Products, Courses, Services & Goods Online with Zero Commission and No Monthly Fee Ever">
      <meta property="twitter:image" content="https://www.yoseller.co/special-bonus/thumbnail.png">
      <title>YoSeller Bonuses</title>
      <!-- Shortcut Icon  -->
      <link rel="icon" href="https://cdn.oppyo.com/launches/yoseller/common_assets/images/favicon.png" type="image/png">
      <!-- Css CDN Load Link -->
      <link rel="stylesheet" href="https://cdn.oppyo.com/launches/yoseller/common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" type="text/css" href="assets/css/bonus-style.css">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css2?family=Pacifico&display=swap" rel="stylesheet">
      <script src="https://cdn.oppyo.com/launches/yoseller/common_assets/js/jquery.min.js"></script>
   </head>
   <body>
   
      <!-- New Timer  Start-->
      <?php
         $date = 'October 13 2022 11:59 AM EST';
         $exp_date = strtotime($date);
         $now = time();  
         /*
         
         $date = date('F d Y g:i:s A eO');
         $rand_time_add = rand(700, 1200);
         $exp_date = strtotime($date) + $rand_time_add;
         $now = time();*/
         
         if ($now < $exp_date) {
         ?>
      <?php
         } else {
         	echo "Times Up";
         }
         ?>
      <!-- New Timer End -->
      <?php
         if(!isset($_GET['afflink'])){
         $_GET['afflink'] = 'https://jvz1.com/c/10103/387544/';
         $_GET['name'] = 'Achal';      
         }
         ?>
            <!-- Header Section Start -->   
            <div class="main-header">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="text-center">
                     <div class="f-md-32 f-20 lh140 w400 white-clr d-block d-md-flex align-items-center justify-content-center">
                        <span class="w600 yellow-clr"><?php echo $_GET['name'];?>'s</span> &nbsp;special bonus for &nbsp;
                        <svg version="1.1" id="Layer_11" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 253.7 80" style="enable-background:new 0 0 253.7 80; max-height:60px;" xml:space="preserve">
                     ;
                     <style type="text/css">
                        .st14{fill:#FFD60D;}
                        .st13{fill:#FFFFFF;}
                        .st22{fill:#2337F6;}
                     </style>
                     <g>
                        <path class="st14" d="M130.2,64.2l-4.8,1.1c-0.4-0.9-1.1-1.6-1.9-2.2c-1-0.7-2.2-1.1-3.5-1.1c-1.7,0-3.3,0.7-4.4,1.9
                           c-1,1-1.6,2.4-1.7,3.9l-14.1,3.1c-1.1-2.5-3.6-4.3-6.5-4.3c-3.9,0-7.1,3.2-7.1,7.1v0.1l-1.8,0.4c-3.3,0.8-6.6-1.3-7.3-4.6l-10-43.9
                           c-0.8-3.3,1.3-6.6,4.6-7.3l9.5-2.1v0.6c0.1,1.4,1.3,2.5,2.7,2.5c0.1,0,0.1,0,0.2,0c1.5-0.1,2.6-1.3,2.5-2.8c0-0.5-0.2-0.9-0.4-1.3
                           l14.2-3.1v0.5c0,0.7,0.3,1.3,0.8,1.8c0.5,0.4,1.1,0.7,1.8,0.7c0.1,0,0.1,0,0.2,0c1.5-0.1,2.6-1.3,2.5-2.8c0-0.5-0.2-0.9-0.4-1.3
                           l12.4-2.7c3.3-0.8,6.6,1.3,7.3,4.6l2,8.8l1.1,5l6.8,30.2C135.7,60.3,133.6,63.6,130.2,64.2z"></path>
                        <g>
                           <path class="st13" d="M34.4,21.8L20.7,48.3v15.1h-6.8V48.3L0,21.8h7.6l9.6,20.4l9.6-20.4C26.8,21.8,34.4,21.8,34.4,21.8z"></path>
                           <path class="st13" d="M41.5,61.8c-2.5-1.4-4.5-3.4-5.9-6c-1.4-2.6-2.2-5.6-2.2-8.9c0-3.4,0.7-6.3,2.2-8.9c1.5-2.6,3.5-4.6,6-6
                              c2.6-1.4,5.4-2.1,8.6-2.1s6,0.7,8.6,2.1c2.6,1.4,4.6,3.4,6,6c1.5,2.6,2.2,5.6,2.2,8.9c0,3.4-0.8,6.3-2.3,8.9
                              c-1.5,2.6-3.6,4.6-6.2,6c-2.6,1.4-5.5,2.1-8.6,2.1C46.8,63.9,44,63.2,41.5,61.8z M54.9,56.7c1.5-0.8,2.8-2.1,3.7-3.8
                              c1-1.7,1.4-3.7,1.4-6.1c0-2.4-0.5-4.4-1.4-6.1c-0.9-1.7-2.1-2.9-3.7-3.7c-1.5-0.8-3.2-1.3-4.9-1.3c-1.8,0-3.4,0.4-4.9,1.3
                              c-1.5,0.8-2.7,2.1-3.6,3.7c-0.9,1.7-1.3,3.7-1.3,6.1c0,3.6,0.9,6.3,2.7,8.2c1.8,1.9,4.1,2.9,6.9,2.9C51.7,58,53.3,57.5,54.9,56.7z
                              "></path>
                        </g>
                        <g>
                           <path class="st13" d="M167.1,50.3h-23.7c0.2,2.5,1.1,4.5,2.8,6c1.7,1.5,3.7,2.3,6.1,2.3c3.5,0,5.9-1.4,7.3-4.3h6.9
                              c-0.9,2.9-2.6,5.2-5.1,7c-2.5,1.8-5.5,2.7-9.1,2.7c-3,0-5.6-0.7-8-2c-2.3-1.3-4.2-3.2-5.5-5.6s-2-5.2-2-8.4c0-3.2,0.6-6,1.9-8.4
                              c1.3-2.4,3.1-4.3,5.5-5.6c2.3-1.3,5-2,8.1-2c2.9,0,5.5,0.6,7.8,1.9c2.3,1.3,4.1,3.1,5.3,5.4c1.3,2.3,1.9,5,1.9,8
                              C167.3,48.3,167.2,49.3,167.1,50.3z M160.6,45.1c0-2.4-0.9-4.3-2.5-5.7c-1.7-1.4-3.7-2.1-6.1-2.1c-2.2,0-4.1,0.7-5.7,2.1
                              c-1.6,1.4-2.5,3.3-2.8,5.7H160.6z"></path>
                           <path class="st13" d="M180,21.8v41.6h-6.4V21.8H180z"></path>
                           <path class="st13" d="M194.9,21.8v41.6h-6.4V21.8H194.9z"></path>
                           <path class="st13" d="M231.5,50.3h-23.7c0.2,2.5,1.1,4.5,2.8,6c1.6,1.5,3.7,2.3,6.1,2.3c3.5,0,5.9-1.4,7.3-4.3h6.9
                              c-0.9,2.9-2.6,5.2-5.1,7c-2.5,1.8-5.5,2.7-9.1,2.7c-3,0-5.6-0.7-8-2c-2.3-1.3-4.2-3.2-5.5-5.6c-1.3-2.4-2-5.2-2-8.4
                              c0-3.2,0.6-6,1.9-8.4c1.3-2.4,3.1-4.3,5.5-5.6c2.3-1.3,5-2,8.1-2c2.9,0,5.5,0.6,7.8,1.9c2.3,1.3,4.1,3.1,5.3,5.4
                              c1.3,2.3,1.9,5,1.9,8C231.7,48.3,231.7,49.3,231.5,50.3z M225,45.1c0-2.4-0.9-4.3-2.5-5.7c-1.7-1.4-3.7-2.1-6.1-2.1
                              c-2.2,0-4.1,0.7-5.7,2.1c-1.6,1.4-2.5,3.3-2.8,5.7H225z"></path>
                           <path class="st13" d="M248.2,33.2c1.6-0.9,3.4-1.3,5.5-1.3v6.6h-1.6c-2.5,0-4.4,0.6-5.7,1.9s-1.9,3.5-1.9,6.6v16.3H238v-31h6.4v4.5
                              C245.4,35.3,246.6,34.1,248.2,33.2z"></path>
                        </g>
                        <circle class="st13" cx="93.4" cy="73.7" r="6.3"></circle>
                        <path class="st22" d="M131.6,51.7c0,5.2-3.4,9.7-8,11.4c-1-0.7-2.2-1.1-3.5-1.1c-1.7,0-3.3,0.7-4.4,1.9H78.9c0-3.9,3.1-7,7-7h33.3
                           c2.9,0,5.3-2.3,5.3-5.2s-2.3-5.3-5.3-5.3h-11.4c-3.4,0-6.5-1.4-8.8-3.7c-2.2-2.2-3.5-5.3-3.5-8.7c0.1-6.8,5.6-12.2,12.4-12.2h22.2
                           c0,1.9-0.8,3.7-2.1,5s-3,2-4.9,2H108c-2.9,0-5.3,2.3-5.3,5.2s2.3,5.3,5.3,5.3h11.4c3.4,0,6.5,1.4,8.8,3.7
                           C130.3,45.3,131.6,48.3,131.6,51.7z"></path>
                        <path class="st22" d="M97.7,22.8c0.4-0.4,0.8-0.7,1.2-1H71.5l0,0c0,3.9,3.1,7,7,7h15.3C94.7,26.6,96,24.5,97.7,22.8z"></path>
                        <path class="st22" d="M85.6,38.8h8.1c-0.5-1.6-0.7-3.2-0.7-4.9c0-0.7,0.1-1.4,0.2-2.1H78.6l0,0C78.6,35.7,81.7,38.8,85.6,38.8z"></path>
                        <path class="st13" d="M104.3,11.2c-0.1-1-0.2-2.1-0.6-3.1C102,2.9,97-0.3,92,0c-0.5,0-1.1,0.1-1.6,0.2c-0.4,0.1-0.7,0.2-1.1,0.3
                           c-6,2-9.1,8.6-7.1,14.7c0.1,0.2,0.1,0.4,0.2,0.6c-0.2,0.3-0.2,0.7-0.2,1c0.1,1,0.9,1.8,1.9,1.7s1.8-0.9,1.7-1.9
                           c0-0.6-0.4-1.2-1-1.5c-0.1-0.2-0.2-0.4-0.2-0.6C83,9.6,85.5,4.5,90.1,3s9.6,1.1,11.2,5.9c0.3,0.8,0.4,1.6,0.5,2.3
                           c-0.4,0.4-0.6,0.9-0.5,1.4c0.1,1,0.9,1.8,1.9,1.7c1-0.1,1.8-0.9,1.7-1.9C104.8,12,104.6,11.5,104.3,11.2z"></path>
                        <circle class="st13" cx="120.1" cy="68.1" r="5.2"></circle>
                     </g>
                  </svg>
                     </div>
                  </div>
                 
                  <div class="col-12 mt20 mt-md50 text-center">
                  <div class="preheadline f-20 f-md-22 w400 white-clr lh140">
                  Grab My 15 Exclusive Bonuses Before the Deal Ends...
                  </div>
               </div>


               <div class="col-12 mt-md30 mt20 f-md-48 f-28 w700 text-center white-clr lh140">
               Have A Sneak Peak of A Cutting-Edge Technology That Lets Anyone <span class="yellow-clr">Launch & Sell Digital Products, Courses, & Services Online in NEXT 7 Minutes Flat...</span> 
               </div>

               <div class="col-12 mt-md25 mt20 f-18 f-md-22 w500 text-center lh140 white-clr text-capitalize">
                  <div class="postheadline f-20 f-md-24 w600 text-center lh140 white-clr text-capitalize">               
                  It comes with 40+ Done-For-You Products to Start Selling Today.<br class="d-none d-md-block"> Watch My Quick YoSeller Review
                 </div>
               </div>
               
               </div>
            </div>
            <div class="row mt20 mt-md40">
               <div class="col-md-10 col-12 mx-auto">
               <div class="col-12 responsive-video border-video">
                     <iframe src="https://yoseller.dotcompal.com/video/mydrive/182331" style="width:100%; height:100%" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                  </div>
                  <!-- <img src="assets/images/product-box.webp" class="img-fluid mx-auto d-block" alt="ProductBox"> -->
                  </div>
            </div>
            <div class="row row-cols-md-2 row-cols-xl-2 row-cols-1 mt20 mt-md40 calendar-wrap mx-auto">
               <div class="col">
                  <ul class="list-head pl0 m0 f-18 f-md-18 lh150 w400 white-clr">
                  <li> <span class="w600 white-clr">Sell Your Own </span> or Use Our Done-For-You Products</li>
                  </ul>
               </div>
               <div class="col">
                  <ul class="list-head pl0 m0 f-18 f-md-18 lh150 w400 white-clr">
                  <li><span class="w600 white-clr">Launch White Label/Reseller/PLR</span> Licensed Products Fast</li>
                  </ul>
               </div>
               <div class="col">
                  <ul class="list-head pl0 m0 f-18 f-md-18 lh150 w400 white-clr">
                  <li>Create Stunning <span class="w600 white-clr">Websites, Stores, and Membership </span>Sites Quick and Easy</li>
                  </ul>
               </div>
               <div class="col">
                  <ul class="list-head pl0 m0 f-18 f-md-18 lh150 w400 white-clr">
                  <li>Built-in Cart with Seamless <span class="w600 white-clr">Integration to PayPal, Stripe, ClickBank, JVZoo</span></li>
                  </ul>
               </div>
               <div class="col">
                  <ul class="list-head pl0 m0 f-18 f-md-18 lh150 w400 white-clr">
                  <li><span class="w600 white-clr">Sell On Your Own Marketplace</span> Keep 100% Profits</li>
                  </ul>
               </div>
               <div class="col">
                  <ul class="list-head pl0 m0 f-18 f-md-18 lh150 w400 white-clr">
                  <li>Get Orders from Social Media, Emails  On Any Page using <span class="w600 white-clr">Smart-Checkout Links</span></li>
                  </ul>
               </div>
               <div class="col">
                  <ul class="list-head pl0 m0 f-18 f-md-18 lh150 w400 white-clr">
                  <li><span class="w600 white-clr">200+ Beautiful Templates</span> and Next-Gen Drag-N-Drop Editor</li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!-- Header Section End -->

     <!-- Step Section Start -->
     <div class="step-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-md-45 f-28 w700 lh140 text-center black-clr">
                     Encash The Latest Trend in E-Selling In
                  </div>
                  <div class="f-md-45 f-28 w700 lh140 text-center white-clr step-headline mt10 mt-md20">
                     3 Easy Steps…
                  </div>
               </div>
               <div class="col-12 f-18  w500 lh140 text-center mt20 mt-md40">As festive selling season is around &amp; it will stay till valentine’s day in Feb, now you can start selling online quick &amp; easy.</div>
            </div>
            <div class="row mt60 mt-md100 align-items-center">
               <div class="col-md-6 col-12 relative">
                  <div class="step-box">
                     <div class="step-title f-20 f-md-22 w600">step #1</div>
                     <div class="f-24 f-md-36 w700 black-clr">Add A Product</div>
                     <div class="w400 f-18 f-md-22 lh140 mt10 grey-clr">
                        To begin, just add your product. If you do not have one, use from Preloaded 40+ Done-For-You Products to Quick Start. Your products will be delivered in the login secured members area on automation.
                     </div>
                  </div>
                  <img src="assets/images/step-arrow.webp" class="img-fluid d-none d-md-block step-arrow" alt="step Arrow">
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                  <img src="assets/images/step-one.webp" class="img-fluid d-none d-md-block">
               </div>
            </div>
            <div class="row mt40 mt-md160 align-items-center">
               <div class="col-md-6 col-12 order-md-2 relative">
                  <div class="step-box">
                     <div class="step-title1 f-20 f-md-22 w600">step #2</div>
                     <div class="f-24 f-md-36 w700 black-clr">Choose Payment Gateway</div>
                     <div class="w400 f-18 f-md-22 lh140 mt10 grey-clr">
                        Integrate your PayPal or Stripe account to receive payments directly into your account. YoSeller also comes with seamless integration with JVZoo, ClickBank and WarriorPlus
                     </div>
                  </div>
                  <img src="assets/images/step-arrow1.webp" class="img-fluid d-none d-md-block step-arrow1" alt="step Arrow">
               </div>
               <div class="col-12 col-md-6 order-md-1 mt20 mt-md0">
                  <img src="assets/images/step-two.webp" class="img-fluid d-none d-md-block">
               </div>
            </div>
            <div class="row mt40 mt-md200 align-items-center">
               <div class="col-md-6 col-12 relative">
                  <div class="step-box">
                     <div class="step-title f-20 f-md-22 w600">step #3</div>
                     <div class="f-24 f-md-36 w700 black-clr">Publish &amp; Profit</div>
                     <div class="w400 f-18 f-md-22 lh140 mt10 grey-clr">Now get your smart checkout link to start getting orders on product pages, social media, and right from your emails like a pro. Start selling online - Quick and Easy.
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-6 order-md-1 mt20 mt-md0">
                  <img src="assets/images/step-three.webp" class="img-fluid d-none d-md-block">
               </div>
            </div>           
         </div>
      </div>
      <!-- Step Section End -->
      <!-- CTA Btn Start-->
      <div class="dark-cta-sec ">
         <div class="container">
            <div class="row ">
               <div class="col-md-12 col-md-12 col-12 text-center mt20 mt-md20">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 yellow-clr">"ACHALVIP"</span> for an Additional <span class="w700 yellow-clr">15% Discount</span> on Agency Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab YoSeller + My 15 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                  Use Coupon Code <span class="w700 yellow-clr">"YOBUNDLE"</span> for an Additional <span class="w700 yellow-clr">$50 Discount</span> on Agency Licence
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="https://jvz8.com/c/10103/387542/" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab YoSeller Bundle + My 15 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 smmltd">Days</span>
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Sec</span>
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Btn End -->
      <!-- Features Section Start -->
      <div class="list-section">
         <div class="container">
            <div class="row">
               <div class="col-12 relative">
                  <div class="featues-list-block">
                     <div class="row">
                        <div class="f-md-45 f-28 w700 lh140 text-capitalize text-center black-clr">
                           “We Guarantee that YoSeller is the Last App
                           You'll Ever Need to Start Run A Profitable <br class="d-none d-md-block">
                           E-Selling Business”                           
                        </div>
                        <div class="col-12 mt20 mt-md50 relative">
                           <div class="row header-list-block gx-md-5">
                              <div class="col-12 col-md-6">
                                 <ul class="features-list pl0 f-16 f-md-18 lh140 w400 black-clr text-capitalize">
                                       <li>Tap into the <span class="w600">Super-Hot $25 Trillion E-Selling Industry</span> </li>
                                       <li>Complete <span class="w600"> All-in-One Online Selling Platform  </span> for Entrepreneurs, Marketers, and Newbies </li>
                                       <li><span class="w600"> Quick Start  </span>  Your Online Selling Business with <span class="w600"> 40+ Done-For-You Products</span></li>
                                       <li><span class="w600"> Easily Create and Manage Unlimited Products -  </span> Digital Products, Courses, Services, and Goods </li>
                                       <li>Create  <span class="w600">Membership Sites to Deliver Products  </span> in Secured &amp; Password Protected Member's Area </li>
                                       <li><span class="w600">Create </span> Unlimited Beautiful, Mobile Ready and Fast-Loading <span class="w600"> Landing Pages Easily</span></li>
                                       <li><span class="w600">	Use Smart-Checkout Links   </span> to Directly <span class="w600">Receive Payments </span> from Social Media, Emails &amp; On Any Page.</li>
                                       <li><span class="w600">	Precise Analytics  </span>  to Measure the Performance and Know Exactly What's Working and What's Not</li>
                                       <li><span class="w600">SEO Friendly &amp; Social Media Optimized  </span> Pages for More Traffic</li>
                                       <li><span class="w600">100% GDPR and Can-Spam   </span> Compliant</li>
                                      <li><span class="w600">Completely Cloud-Based -</span> No Domain, Hosting, or Installation Required</li>
                                      <li>Launch Fast - Create Stunning  <span class="w600">Websites &amp; Stores  </span> for Your and Your Client’s Business in Any Niche </li>
                                    </ul>
                              </div>
                              <div class="col-12 col-md-6 mt10 mt-md0">
                                 <ul class="features-list pl0 f-16 f-md-18 lh140 w400 black-clr text-capitalize">
                                     
                                       <li>	Sell Unlimited Products and Accept  <span class="w600">Payments Through PayPal and Stripe</span> with Zero Fee, you can Keep 100% of your Profit </li>
                                       <li>	Selling on ClickBank, JVZoo &amp; Warrior Plus? Get <span class="w600">Seamless Integration </span> to Deliver Products on Automation. </li>
                                       <li><span class="w600">100+ Battle-Tested, </span> Beautiful, and Mobile-Ready  <span class="w600"> Page Templates</span>  </li>
                                       <li>Fully Customizable  <span class="w600">Drag &amp; Drop WYSIWYG Page Editor  </span> that Requires Zero Designing or Tech Skills </li>
                                       <li><span class="w600"> Inbuilt Lead Management System </span> for Effective Contacts Management in Automation
                                       </li><li>Connect YoSeller with your Favorite Tools, <span class="w600">20+ Integrations </span> with Autoresponders and other Services. </li>
                                       <li> <span class="w600"> 128 Bit Secured, SSL Encryption </span>for Maximum Security of Your Files, Data, and Websites </li>
                                       <li> Manage all the Pages, Products &amp; Customers Hassle-Free, <span class="w600">All in Single Dashboard. </span>  </li>
                                       <li><span class="w600">Step-By-Step Video Training </span> and Tutorials Included </li>
                                       <li><span class="w600">Premium Customer Support</span>  </li>
                                       <li><span class="w600">Plus, you'll also Receive Agency to Start Selling Pro Level Services to your Clients Right Away</span>  </li>                              
                                    </ul>
                              </div>
                           </div>
                     
                        </div>
                     </div>                    
                  </div>
                  <img src="assets/images/announcement.webp" class="img-fluid d-none d-md-block mx-auto horn">
                  <img src="assets/images/cart.webp" class="img-fluid d-none d-md-block mx-auto cart">
               </div>
            </div>
          
         </div>
      </div>
      <!-- Features Section End -->


      <!-- Proven Section -->
      <div class="proof-section">
         <div class="container">
            <div class="row">
               <div class="f-24 f-md-40 w400 lh140 text-capitalize text-center black-clr col-12">
                  YoSeller Is So Powerful That We Are Personally Using It <br class="d-none d-md-block">
                  <span class="f-28 f-md-42 w600">To Run Our 6-Figure Online Business Without a Hitch!</span>
               </div>
               
               <div class="col-12 mt20 mt-md50">
                  <img src="assets/images/proof.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md50">
               <div class="col-12 col-md-6 f-md-36 f-22 w600 lh140 black-clr">
                  By Selling Online, We Have Generated $287k in Sales in The Last 7 Months Alone.
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                  <img src="assets/images/proof1.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>
         </div>
      </div>
      <!-- Proven Section Ends-->
     <!-- Technology Section Start -->
     <div class="technology-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-12">
                  <div class="f-22 f-md-32 lh140 w600 text-center yellow-clr">
                     But it’s not just us...               
                  </div>
               </div>
               <div class="col-12 col-md-12 mt20">
                  <div class="f-28 f-md-45 lh140 w600 text-center white-clr">
                     YoSeller is a Proven Solution Backed by the same 
                     Technology that has been Happily Served...                     
                  </div>
               </div>
            </div>
            <div class="row row-cols-1 row-cols-md-3 mt20 mt-md50">
               <div class="col">
                  <div class="d-flex align-items-center">
                     <img src="assets/images/cloud-y.webp" alt="cloud" class="img-fluid mr20">
                     <div class=" text-capitalize ">
                        <div class="f-md-42 f-24 w700 yellow-clr">
                           16,500
                        </div>
                        <div class="f-18 f-md-20 w500 white-clr">
                           Happy Customers
                        </div>
                     </div>
                  </div>
                  </div>
                  <div class="col mt20 mt-md0">
                  <div class="d-flex align-items-center">
                     <img src="assets/images/play.webp" alt="play" class="img-fluid mr20">
                     <div class="text-capitalize ">
                        <div class="f-md-42 f-24 w700 yellow-clr">
                           100 
                        </div>
                        <div class="f-18 f-md-20 w500 white-clr">
                           Million Visitors
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col mt20 mt-md0">
                  <div class="d-flex align-items-center">
                     <img src="assets/images/web-hosting.webp" alt="Web-Hosting" class="img-fluid mr20">
                     <div class="text-capitalize ">
                        <div class="f-md-42 f-24 w700 yellow-clr">
                           150,000+
                        </div>
                        <div class="f-18 f-md-20 w500 white-clr">
                           Conversions
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-12 col-md-12 mt20 mt-md70">
               <div class="f-24 f-md-36 lh140 w600 text-center white-clr">
                  It's Totally MASSIVE.
               </div>
               <img src="assets/images/massive-line.webp" alt="Protfolio" class="img-fluid d-block mx-auto">
               <div class="mt10 f-16 f-md-18 lh140 w400 text-center white-clr">
                  And now it's your turn to power your own business without worrying about a hefty price tag during this launch special deal.
               </div>
            </div>
            <div class="row mt30 mt-md60">
               <div class="col-md-12 col-md-12 col-12 text-center mt20 mt-md20">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 yellow-clr">"ACHALVIP"</span> for an Additional <span class="w700 yellow-clr">15% Discount</span> on Agency Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab YoSeller + My 15 Exclusive Bonuses</span>
                  </a>
               </div>

               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                  Use Coupon Code <span class="w700 yellow-clr">"YOBUNDLE"</span> for an Additional <span class="w700 yellow-clr">$50 Discount</span> on Agency Licence
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="https://jvz8.com/c/10103/387542/" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab YoSeller Bundle + My 15 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 smmltd">Days</span>
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Sec</span>
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- Technology Section End -->
     <!-- Testimonial Section Starts -->
     <div class="testimonial-section">
         <div class="container ">
            <div class="row ">
               <div class="col-12">
                  <div class="f-md-45 f-28 lh140 w400 text-center black-clr">Check Out What Our Beta Testers Are <br class="d-none d-md-block"><span class="w700">Saying About YoSeller!</span>  </div>
               </div>
            </div>
            <div class="row gx-md-5">
               <div class="col-12 col-md-6 mt20 mt-md50">
                  <div class="testimonials-block-border">
                     <div class="testimonials-block f-18 w400 lh140">
                        Wow!!! It’s an <span class="w600">amazing creation that allows me to launch and sell my products &amp; services EASY &amp; FAST.</span> Apart from its features, the best part I personally love is that it <span class="w600">comes at a one-time price for the next few days!</span> Complete value for your money. Guys go for it... 
                            
                     <div class="author-wrap mt20 mt-md20">
                        <img src="assets/images/testi-img1.webp" class="img-fluid">
                        <div class="f-22 w600 lh140 pl15">Drake Lewis</div>
                     </div>	             							
                     </div>	                     								
                  </div>							
               </div>
               <div class="col-12 col-md-6 mt20 mt-md50">
                  <div class="testimonials-block-border">
                     <div class="testimonials-block f-18 w400 lh140">
                        Still looking for a solution that <span class="w600">enables you to set up &amp; manage your online selling business completely from one central dashboard</span> like a pro that entices visitors and converts them into lifetime customers; <span class="w600">YoSeller is what you’re looking for</span>...                          
                     <div class="author-wrap mt20 mt-md20">
                        <img src="assets/images/testi-img2.webp" class="img-fluid">
                        <div class="f-22 w600 lh140 pl15">Mike East</div>
                     </div>	             							
                     </div>	                     								
                  </div>								
               </div>
               <div class="col-12 col-md-6 mt20 mt-md30">
                  <div class="testimonials-block-border">
                     <div class="testimonials-block f-18 w400 lh140">
                        YoSeller is a revolutionary <span class="w600">all-in-one technology</span> that lets you take a plunge into the HUGE <span class="w600">Freelancing, E-Selling, and Info-Selling industry with no tech hassles ever.</span> The biggest benefit, you can drive tons of traffic and boost ROI for your efforts. Grab it before it flies away forever...
                     <div class="author-wrap mt20 mt-md20">
                        <img src="assets/images/testi-img3.webp" class="img-fluid">
                        <div class="f-22 w600 lh140 pl15">Harry Joe</div>
                     </div>	             							
                     </div>	                     								
                  </div>								
               </div>
               <div class="col-12 col-md-6 mt20 mt-md30">
                  <div class="testimonials-block-border">
                     <div class="testimonials-block f-18 w400 lh140">
                        Hey guys, <span class="w600">wanted to give you big applause.</span> I used YoSeller and yes, I have never seen results like this before. Instead of taking days or juggling with technicalities, <span class="w600">this technology gives a push-button solution.</span> It is a very versatile tool as I too can take <span class="w600">complete control of my e-selling business without any third-party dependence.</span> This is something that you can’t overlook...
                     <div class="author-wrap mt20 mt-md20">
                        <img src="assets/images/testi-img4.webp" class="img-fluid">
                        <div class="f-22 w600 lh140 pl15">Penelope White</div>
                     </div>	             							
                     </div>	                     								
                  </div>								
               </div>
            </div>
         </div>
      </div>
      <!-- Testimonial Section Ends -->

      <!-- Proudly Section Start -->
      <div class="proudly-sec" id="product">
      <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-24 f-md-38 w600 lh140 white-clr intro-title">
                     Presenting…
                  </div>
               </div>
               <div class="col-12 mt-md30 mt20 text-center">
               <svg version="1.1" id="Layer_11" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 253.7 80" style="enable-background:new 0 0 253.7 80; max-height:180px;" xml:space="preserve">
                     ;
                     <style type="text/css">
                        .st00a{fill:#FFD60D;}
                        .st11b{fill:#FFFFFF;}
                        .st22c{fill:#2337F6;}
                     </style>
                     <g>
                        <path class="st00a" d="M130.2,64.2l-4.8,1.1c-0.4-0.9-1.1-1.6-1.9-2.2c-1-0.7-2.2-1.1-3.5-1.1c-1.7,0-3.3,0.7-4.4,1.9
                           c-1,1-1.6,2.4-1.7,3.9l-14.1,3.1c-1.1-2.5-3.6-4.3-6.5-4.3c-3.9,0-7.1,3.2-7.1,7.1v0.1l-1.8,0.4c-3.3,0.8-6.6-1.3-7.3-4.6l-10-43.9
                           c-0.8-3.3,1.3-6.6,4.6-7.3l9.5-2.1v0.6c0.1,1.4,1.3,2.5,2.7,2.5c0.1,0,0.1,0,0.2,0c1.5-0.1,2.6-1.3,2.5-2.8c0-0.5-0.2-0.9-0.4-1.3
                           l14.2-3.1v0.5c0,0.7,0.3,1.3,0.8,1.8c0.5,0.4,1.1,0.7,1.8,0.7c0.1,0,0.1,0,0.2,0c1.5-0.1,2.6-1.3,2.5-2.8c0-0.5-0.2-0.9-0.4-1.3
                           l12.4-2.7c3.3-0.8,6.6,1.3,7.3,4.6l2,8.8l1.1,5l6.8,30.2C135.7,60.3,133.6,63.6,130.2,64.2z"></path>
                        <g>
                           <path class="st11b" d="M34.4,21.8L20.7,48.3v15.1h-6.8V48.3L0,21.8h7.6l9.6,20.4l9.6-20.4C26.8,21.8,34.4,21.8,34.4,21.8z"></path>
                           <path class="st11b" d="M41.5,61.8c-2.5-1.4-4.5-3.4-5.9-6c-1.4-2.6-2.2-5.6-2.2-8.9c0-3.4,0.7-6.3,2.2-8.9c1.5-2.6,3.5-4.6,6-6
                              c2.6-1.4,5.4-2.1,8.6-2.1s6,0.7,8.6,2.1c2.6,1.4,4.6,3.4,6,6c1.5,2.6,2.2,5.6,2.2,8.9c0,3.4-0.8,6.3-2.3,8.9
                              c-1.5,2.6-3.6,4.6-6.2,6c-2.6,1.4-5.5,2.1-8.6,2.1C46.8,63.9,44,63.2,41.5,61.8z M54.9,56.7c1.5-0.8,2.8-2.1,3.7-3.8
                              c1-1.7,1.4-3.7,1.4-6.1c0-2.4-0.5-4.4-1.4-6.1c-0.9-1.7-2.1-2.9-3.7-3.7c-1.5-0.8-3.2-1.3-4.9-1.3c-1.8,0-3.4,0.4-4.9,1.3
                              c-1.5,0.8-2.7,2.1-3.6,3.7c-0.9,1.7-1.3,3.7-1.3,6.1c0,3.6,0.9,6.3,2.7,8.2c1.8,1.9,4.1,2.9,6.9,2.9C51.7,58,53.3,57.5,54.9,56.7z
                              "></path>
                        </g>
                        <g>
                           <path class="st11b" d="M167.1,50.3h-23.7c0.2,2.5,1.1,4.5,2.8,6c1.7,1.5,3.7,2.3,6.1,2.3c3.5,0,5.9-1.4,7.3-4.3h6.9
                              c-0.9,2.9-2.6,5.2-5.1,7c-2.5,1.8-5.5,2.7-9.1,2.7c-3,0-5.6-0.7-8-2c-2.3-1.3-4.2-3.2-5.5-5.6s-2-5.2-2-8.4c0-3.2,0.6-6,1.9-8.4
                              c1.3-2.4,3.1-4.3,5.5-5.6c2.3-1.3,5-2,8.1-2c2.9,0,5.5,0.6,7.8,1.9c2.3,1.3,4.1,3.1,5.3,5.4c1.3,2.3,1.9,5,1.9,8
                              C167.3,48.3,167.2,49.3,167.1,50.3z M160.6,45.1c0-2.4-0.9-4.3-2.5-5.7c-1.7-1.4-3.7-2.1-6.1-2.1c-2.2,0-4.1,0.7-5.7,2.1
                              c-1.6,1.4-2.5,3.3-2.8,5.7H160.6z"></path>
                           <path class="st11b" d="M180,21.8v41.6h-6.4V21.8H180z"></path>
                           <path class="st11b" d="M194.9,21.8v41.6h-6.4V21.8H194.9z"></path>
                           <path class="st11b" d="M231.5,50.3h-23.7c0.2,2.5,1.1,4.5,2.8,6c1.6,1.5,3.7,2.3,6.1,2.3c3.5,0,5.9-1.4,7.3-4.3h6.9
                              c-0.9,2.9-2.6,5.2-5.1,7c-2.5,1.8-5.5,2.7-9.1,2.7c-3,0-5.6-0.7-8-2c-2.3-1.3-4.2-3.2-5.5-5.6c-1.3-2.4-2-5.2-2-8.4
                              c0-3.2,0.6-6,1.9-8.4c1.3-2.4,3.1-4.3,5.5-5.6c2.3-1.3,5-2,8.1-2c2.9,0,5.5,0.6,7.8,1.9c2.3,1.3,4.1,3.1,5.3,5.4
                              c1.3,2.3,1.9,5,1.9,8C231.7,48.3,231.7,49.3,231.5,50.3z M225,45.1c0-2.4-0.9-4.3-2.5-5.7c-1.7-1.4-3.7-2.1-6.1-2.1
                              c-2.2,0-4.1,0.7-5.7,2.1c-1.6,1.4-2.5,3.3-2.8,5.7H225z"></path>
                           <path class="st11b" d="M248.2,33.2c1.6-0.9,3.4-1.3,5.5-1.3v6.6h-1.6c-2.5,0-4.4,0.6-5.7,1.9s-1.9,3.5-1.9,6.6v16.3H238v-31h6.4v4.5
                              C245.4,35.3,246.6,34.1,248.2,33.2z"></path>
                        </g>
                        <circle class="st11b" cx="93.4" cy="73.7" r="6.3"></circle>
                        <path class="st22c" d="M131.6,51.7c0,5.2-3.4,9.7-8,11.4c-1-0.7-2.2-1.1-3.5-1.1c-1.7,0-3.3,0.7-4.4,1.9H78.9c0-3.9,3.1-7,7-7h33.3
                           c2.9,0,5.3-2.3,5.3-5.2s-2.3-5.3-5.3-5.3h-11.4c-3.4,0-6.5-1.4-8.8-3.7c-2.2-2.2-3.5-5.3-3.5-8.7c0.1-6.8,5.6-12.2,12.4-12.2h22.2
                           c0,1.9-0.8,3.7-2.1,5s-3,2-4.9,2H108c-2.9,0-5.3,2.3-5.3,5.2s2.3,5.3,5.3,5.3h11.4c3.4,0,6.5,1.4,8.8,3.7
                           C130.3,45.3,131.6,48.3,131.6,51.7z"></path>
                        <path class="st22c" d="M97.7,22.8c0.4-0.4,0.8-0.7,1.2-1H71.5l0,0c0,3.9,3.1,7,7,7h15.3C94.7,26.6,96,24.5,97.7,22.8z"></path>
                        <path class="st22c" d="M85.6,38.8h8.1c-0.5-1.6-0.7-3.2-0.7-4.9c0-0.7,0.1-1.4,0.2-2.1H78.6l0,0C78.6,35.7,81.7,38.8,85.6,38.8z"></path>
                        <path class="st11b" d="M104.3,11.2c-0.1-1-0.2-2.1-0.6-3.1C102,2.9,97-0.3,92,0c-0.5,0-1.1,0.1-1.6,0.2c-0.4,0.1-0.7,0.2-1.1,0.3
                           c-6,2-9.1,8.6-7.1,14.7c0.1,0.2,0.1,0.4,0.2,0.6c-0.2,0.3-0.2,0.7-0.2,1c0.1,1,0.9,1.8,1.9,1.7s1.8-0.9,1.7-1.9
                           c0-0.6-0.4-1.2-1-1.5c-0.1-0.2-0.2-0.4-0.2-0.6C83,9.6,85.5,4.5,90.1,3s9.6,1.1,11.2,5.9c0.3,0.8,0.4,1.6,0.5,2.3
                           c-0.4,0.4-0.6,0.9-0.5,1.4c0.1,1,0.9,1.8,1.9,1.7c1-0.1,1.8-0.9,1.7-1.9C104.8,12,104.6,11.5,104.3,11.2z"></path>
                        <circle class="st11b" cx="120.1" cy="68.1" r="5.2"></circle>
                     </g>
                  </svg>
               </div>
               <div class="col-12 f-md-38 f-22 mt-md30 mt20 w600 text-center white-clr lh140">
                  One of a Kind, All-In-One Platform to Market and Sell Any Type of Digital Products, Courses, Services, and Physical Goods with Zero Technical Hassle. 
               </div>
            </div>
            <div class="row mt30 mt-md50 align-items-center">
               <div class="col-12 col-md-6">
                  <img src="assets/images/product-image.webp" class="img-fluid d-block mx-auto img-animation">
               </div>
               <div class="col-md-6 mt20 mt-md0">
                  <div class="proudly-list-bg">
                     <div class="col-12">
                     <ul class="proudly-list f-18 w500 lh140 black-clr">
                           <li>
                              <span class="f-22 f-md-24 w600">All-In-One Business Platform</span>
                              <div class="f-18 f-md-18 w400 mt5">Everything you need to sell online, from Memberships Site, Website, Cart & Payments, Landing Pages, & Much More </div>
                           </li>
                           <li>
                              <span class="f-22 f-md-24 w600">Fast & Easy to Launch & Sell Online</span>
                              <div class="f-18 f-md-18 w400 mt5">Get Max Conversions, Leads & Sales with Zero Tech Hassles for your Digital Products, Courses, Services, & Goods.</div>
                           </li>
                           <li>
                              <span class="f-22 f-md-24 w600">Agency License to Serve Clients</span>
                              <div class="f-18 f-md-18 w400 mt5">Launch & Sell Your Own Products or Charge Your Clients & Keep 100% Profits</div>
                           </li>
                           <li>
                              <span class="f-22 f-md-24 w600">No Monthly Recurring Charges</span>
                              <div class="f-18 f-md-18 w400 mt5">During This Launch Special Deal, Get All Benefits at Limited Low One-Time-Fee.</div>
                           </li>
                           <li>
                              <span class="f-22 f-md-24 w600">50+ More Exciting Features</span>
                              <div class="f-18 f-md-18 w400 mt5">We've Left No Stone Unturned to Give You an Unmatched Experience</div>
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         </div>
      <!-- Proudly Section End  -->

 <!-------Exclusive Bonus----------->
 <div class="exclusive-bonus">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <div class="white-clr f-24 f-md-36 lh140 w600">
                  Exclusive Bonus #1 : WebPrimo
               </div>
               <div class="f-18 f-md-20 w500 mt20 white-clr">
                  20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
               </div>
            </div>
         </div>
      </div>
   </div>
<!-------Exclusive Bonus Ends----------->



<!--WebPrimo Header Section Start -->
<div class="WebPrimo-header-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12"><img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/webprimo-logo.webp" class="img-fluid d-block mx-auto mr-md-auto" /></div>
                <div class="col-12 f-20 f-md-24 w500 text-center white-clr lh150 text-capitalize mt20 mt-md65">
                    <div>
                        The Simple 7 Minute Agency Service That Gets Clients Daily (Anyone Can Do This)
                    </div>
                    <div class="mt5">
                        <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/viddeyo-post-head-sep.webp" class="img-fluid mx-auto d-block">
                    </div>
                </div>
                <div class="col-12 mt-md25 mt20">
                    <div class="f-md-45 f-28 w600 text-center white-clr lh150">
						First Ever on JVZoo…  <br class="d-none d-md-block">
						<span class="w700 f-md-42 WebPrimo-highlihgt-heading">The Game Changing Website Builder Lets You </span>  <br>Create Beautiful Local WordPress Websites & Ecom Stores for Any Business in <span class="WebPrimo-higlight-text"> Just 7 Minutes</span>
                    </div>
                </div>
                <div class="col-12 mt20 mt-md20 f-md-22 f-20 w400 white-clr text-center">
					Easily create & sell websites and stores for big profits to dentists, attorney, gyms, spas, restaurants, cafes, retail stores, book shops, coaches & 100+ other niches...
                </div>
				  <div class="col-12 mt20 mt-md40">
				  <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-md-7 col-12 min-md-video-width-left">

							<div class="col-12 responsive-video">
                                <iframe src="https://webprimo.dotcompal.com/video/embed/8ghfrcnhp5" style="width:100%; height:100%" frameborder="0" allow="fullscreen" allowfullscreen></iframe>
                            </div> 

                        </div>
                        <div class="col-md-5 col-12 mt20 mt-md0 min-md-video-width-right">
                            <div class="WebPrimo-key-features-bg">
							<ul class="list-head pl0 m0 f-16 lh150 w400 white-clr">
							<li>Tap Into Fast-Growing $284 Billion Website Creation Industry</li>
							<li>Help Businesses Go Digital in Their Hard Time (IN TODAYS DIGITAL AGE)</li>
							<li>Create Elegant, Fast loading & SEO Friendly Website within 7 Minutes</li>
							<li>200+ Eye-Catching Templates for Local Niches Ready with Content</li>
							<li>Bonus Live Training - Find Tons of Local Clients Hassle-Free</li>
							<li>Agency License Included to Build an Incredible Income Helping Clients!</li>
							</ul>
                            </div>
                        </div>
                    </div>
                    </div>
            </div>
        </div>
    </div>
    <div class="WebPrimo-list-section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="row webprimo-header-list-block">
                        <div class="col-12 col-md-6 header-list-back">
                            <div class="f-16 f-md-18 lh150 w400">
                                <ul class="webprimo-header-bordered-list pl0 webprimo-orange-list">
                                    <li><span class="w600">Create Ultra-Fast Loading & Beautiful Websites</li>
                                    <li>Instantly Create Local Sites, E-com Sites, and Blogs-<span class="w600">For Any Business Need.</span>
									</li>
                                    <li>Built On World's No. 1, <span class="w600">Super-Customizable WP FRAMEWORK For BUSINESSES</span>
									</li>
                                    <li>SEO Optimized Websites for <span class="w600">More Search Traffic</span></li>
									<li><span class="w600">Get More Likes, Shares and Viral Traffic</span> with In-Built Social Media Tools</li>
									<li><span class="w600">Create 100% Mobile Friendly</span> And Ultra-Fast Loading Websites.</li>
									<li><span class="w600">Analytics & Remarketing Ready</span> Websites</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 header-list-back">
                            <div class="f-16 f-md-18 lh150 w400">
                                <ul class="webprimo-header-bordered-list pl0 webprimo-orange-list">
                                    <li>Newbie Friendly. <span class="w600">No Tech Skills Needed. No Monthly Fees.</span></li>
                                    <li>Customize and Update Themes on Live Website Easily</li>
                                    <li>Loaded with 30+ Themes with Super <span class="w600">Customizable 200+ Templates</span> and 2000+ possible design combinations.</li>
                                    <li>Customise Websites <span class="w600">Without Touching Any Code</span></li>
                                    <li><span class="w600">Accept Payments</span> For Your Services & Products With Seamless WooCommerce Integration</li>
									<li>Help Local Clients That Desperately Need Your Service And Make BIG Profits.</li>									
                                </ul>
                            </div>
                        </div>
						<div class="col-12  header-list-back">
                            <div class="f-16 f-md-18 lh150 w400">
                                <ul class="webprimo-header-bordered-list pl0 webprimo-orange-list">
									<li class="w600">Agency License Included to Build an Incredible Income Helping Clients!</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--WebPrimo Header Section End -->
    <div class="gr-1">
      <div class="container-fluid p0">
         <picture>
            <source media="(min-width:768px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/webprimo-steps.webp">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/webprimo-steps-mview.webp" style="width:100%" class="vidvee-mview">
            <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/webprimo-steps.webp" alt="Vidvee Steps" class="img-fluid" style="width: 100%;">
         </picture>
          <picture>
            <source media="(min-width:768px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/webprimo-doyouknow.webp">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/webprimo-doyouknow-mview.webp">
            <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/webprimo-doyouknow.webp" alt="Flowers" style="width:100%;">
         </picture>
         <picture>
            <source media="(min-width:768px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/webprimo-introducing.webp">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/webprimo-introducing-mview.webp">
            <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/webprimo-introducing.webp" alt="Flowers" style="width:100%;">
         </picture> 
      </div>
   </div>
   <!--WebPrimo ends-->
   <!-------Coursova Starts----------->
<!-------Exclusive Bonus----------->
<div class="exclusive-bonus">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <div class="white-clr f-24 f-md-36 lh140 w600">
                  Exclusive Bonus #2 : Coursova
               </div>
               <div class="f-18 f-md-20 w500 mt20 white-clr">
                  20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
               </div>
            </div>
         </div>
      </div>
   </div>

   <div class="coursova-bg">
                <div class="container">
                    <div class="row inner-content">
                        <div class="col-12">
                            <div class="col-12 text-center">
                                <img src="assets/images/logo-co-white.png" class="img-responsive mx-xs-center logo-height">
                            </div>
                         
                        </div>
                        <div class="col-12 text-center px-sm15">
                            <div class="mt20 mt-md50 mb20 mb-md50">
                                <div class="f-md-21 f-20 w500 lh140 text-center white-clr">
                                   Turn your passion, hobby, or skill into <u>a profitable e-learning business</u> right from your home…
                                </div>
                            </div>
                        </div>
           
                        <div class="col-12">
                        <div class="col-md-8 col-sm-3 ml-md70 mt20">
                                <div class="game-changer col-md-12">
                                    <div class="f-md-24 f-20 w600 text-center  black-clr lh120">Game-Changer</div>
                                </div>
                            </div>
                            <div class="col-12 heading-co-bg">
                                <div class="f-24 f-md-42 w500 text-center black-clr lh140  line-center rotate-5">								
								  <span class="w700"> Create and Sell Courses on Your Own Branded <br class="d-md-block d-none">E-Learning Marketplace</span>  without Any <br class="visible-lg"> Tech Hassles or Prior Skills
                                </div>
                            </div>
                            <div class="col-12 p0 f-18 f-md-24 w400 text-center  white-clr lh140 mt-md40 mt20">
								
								Sell your own courses or <u>choose from 10 done-for-you RED-HOT HD <br class="d-md-block d-none">video courses to start earning right away</u>

								</div>
						</div>
							  </div>
                        <div class="row align-items-center">
                            <div class="col-md-7 col-12 mt-sm100 mt20 px-sm15 min-sm-video-width-left">
                          
                               <div class="col-12 p0 mt-md15">
                                    <div class="col-12 responsive-video" editabletype="video" style="z-index: 10;">
                                        <iframe src="https://coursova.dotcompal.com/video/embed/n56ppjcyr4" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
box-shadow: none !important;" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5 col-12 f-20 f-md-20 worksans lh140 w400 white-clr mt-md37 mt20 pl15 min-sm-video-width-right">
                                <ul class="list-head list-head-co pl0 m0">
                                    <li>Tap Into Fast-Growing $398 Billion Industry Today</li>
                                    <li>Build Beautiful E-Learning Sites with Marketplace, Blog &amp; Members Area in Minutes</li>
                                    <li>Easily Create Courses - Add Unlimited Lessons (Video, E-Book &amp; Reports)</li>
                                    <li>Accept Payments with PayPal, JVZoo, ClickBank, W+ Etc.</li>
                                    <li>No Leads or Profit Sharing With 3rd Party Marketplaces</li>
                                    <li>Get Agency License and Charge Clients Monthly (Yoga Classes, Gym Etc.)</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

      <div class="gr-1">
         <div class="container-fluid p0">			
            <picture>
            <img src="assets/images/coursova-gr1.png" alt="Flowers" style="width:100%;">
            </picture>	
            <picture>
            <img src="assets/images/coursova-gr2.png" alt="Flowers" style="width:100%;">
            </picture>		
            <picture>
            <img src="assets/images/coursova-gr3.png" alt="Flowers" style="width:100%;">
            </picture>			
         </div>
		</div>
<!------Coursova Section ends------>

      <!-------Exclusive Bonus----------->
   <div class="exclusive-bonus">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <div class="white-clr f-24 f-md-36 lh140 w600">
                  Exclusive Bonus #3 : Trendio
               </div>
               <div class="f-18 f-md-20 w500 mt20 white-clr">
                  20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
               </div>
            </div>
         </div>
      </div>
   </div>
   <!------Trendio Section------>

   <div class="tr-header-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row">
                     <div class="col-md-3 text-center mx-auto">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 912.39 246.83" style="enable-background:new 0 0 912.39 246.83; max-height:50px;color: #023047;" xml:space="preserve">
                           <style type="text/css">
                              .st0{fill:#FFFFFF;}
                              .st1{opacity:0.3;}
                              .st2{fill:url(#SVGID_1_);}
                              .st3{fill:url(#SVGID_00000092439463549223389810000006963263784374171300_);}
                              .st4{fill:url(#SVGID_00000015332009897132114970000017423936041397956233_);}
                              .st5{fill:#023047;}
                           </style>
                           <g>
                              <g>
                                 <path class="st0" d="M18.97,211.06L89,141.96l2.57,17.68l10.86,74.64l2.12-1.97l160.18-147.9l5.24-4.84l6.8-6.27l5.24-4.85 l-25.4-27.51l-12.03,11.11l-5.26,4.85l-107.95,99.68l-2.12,1.97l-11.28-77.58l-2.57-17.68L0,177.17c0.31,0.72,0.62,1.44,0.94,2.15 c0.59,1.34,1.2,2.67,1.83,3.99c0.48,1.03,0.98,2.06,1.5,3.09c0.37,0.76,0.76,1.54,1.17,2.31c2.57,5.02,5.43,10,8.57,14.93 c0.58,0.89,1.14,1.76,1.73,2.65L18.97,211.06z"></path>
                              </g>
                              <g>
                                 <g>
                                    <polygon class="st0" points="328.54,0 322.97,17.92 295.28,106.98 279.91,90.33 269.97,79.58 254.51,62.82 244.58,52.05 232.01,38.45 219.28,24.66 "></polygon>
                                 </g>
                              </g>
                           </g>
                           <g class="st1">
                              <g>
                                 <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="17.428" y1="42.82" x2="18.9726" y2="42.82" gradientTransform="matrix(1 0 0 -1 0 252.75)">
                                    <stop offset="0" style="stop-color:#000000"></stop>
                                    <stop offset="1" style="stop-color:#333333"></stop>
                                 </linearGradient>
                                 <path class="st2" d="M17.43,208.8l1.54,2.26c-0.37-0.53-0.73-1.05-1.09-1.58c-0.02-0.02-0.02-0.03-0.03-0.03 c-0.06-0.09-0.11-0.17-0.17-0.27C17.6,209.06,17.51,208.92,17.43,208.8z"></path>
                                 <linearGradient id="SVGID_00000177457867953882822120000005144526232562103955_" gradientUnits="userSpaceOnUse" x1="18.98" y1="70.45" x2="91.57" y2="70.45" gradientTransform="matrix(1 0 0 -1 0 252.75)">
                                    <stop offset="0" style="stop-color:#000000"></stop>
                                    <stop offset="1" style="stop-color:#333333"></stop>
                                 </linearGradient>
                                 <path style="fill:url(#SVGID_00000177457867953882822120000005144526232562103955_);" d="M89,141.96l2.57,17.68l-63.83,63 c-1.19-1.45-2.34-2.9-3.46-4.35c-1.84-2.4-3.6-4.81-5.3-7.22L89,141.96z"></path>
                                 <linearGradient id="SVGID_00000010989203515549635820000018393619450353154703_" gradientUnits="userSpaceOnUse" x1="104.55" y1="120.005" x2="333.22" y2="120.005" gradientTransform="matrix(1 0 0 -1 0 252.75)">
                                    <stop offset="0" style="stop-color:#000000"></stop>
                                    <stop offset="1" style="stop-color:#333333"></stop>
                                 </linearGradient>
                                 <polygon style="fill:url(#SVGID_00000010989203515549635820000018393619450353154703_);" points="333.22,15.61 299.96,122.58 274.65,95.18 107.11,249.88 104.55,232.31 264.73,84.41 269.97,79.58 279.91,90.33 295.28,106.98 322.97,17.92 "></polygon>
                              </g>
                           </g>
                           <g>
                              <g>
                                 <g>
                                    <path class="st5" d="M229.46,241.94c-12.28,5.37-23.49,8.06-33.57,8.06c-9.63,0-18.21-2.44-25.71-7.35 c-23.05-15.07-23.72-46.17-23.72-49.67V61.67h32.2v30h39.27v32.2h-39.27v69.04c0.07,4.46,1.88,18.1,9.2,22.83 c5.51,3.55,15.7,2.38,28.68-3.3L229.46,241.94z"></path>
                                 </g>
                              </g>
                           </g>
                           <g>
                              <g>
                                 <path class="st5" d="M250.4,164.29c0-15.97-0.24-27.35-0.97-38h25.9l0.97,22.51h0.97c5.81-16.7,19.61-25.17,32.19-25.17 c2.9,0,4.6,0.24,7.02,0.73v28.08c-2.42-0.48-5.08-0.97-8.71-0.97c-14.28,0-23.96,9.2-26.63,22.51c-0.48,2.66-0.97,5.81-0.97,9.2 v61H250.4V164.29z"></path>
                              </g>
                           </g>
                           <g>
                              <g>
                                 <path class="st5" d="M459.28,161.39c0-13.55-0.24-24.93-0.97-35.1h26.14l1.45,17.67h0.73c5.08-9.2,17.91-20.33,37.52-20.33 c20.57,0,41.87,13.31,41.87,50.59v69.95h-29.77v-66.56c0-16.94-6.29-29.77-22.51-29.77c-11.86,0-20.09,8.47-23.24,17.43 c-0.97,2.66-1.21,6.29-1.21,9.68v69.23h-30.01V161.39z"></path>
                              </g>
                           </g>
                           <g>
                              <g>
                                 <path class="st5" d="M706.41,72.31V211c0,12.1,0.48,25.17,0.97,33.16h-26.63l-1.21-18.64h-0.48c-7.02,13.07-21.3,21.3-38.49,21.3 c-28.08,0-50.35-23.96-50.35-60.27c-0.24-39.45,24.45-62.93,52.77-62.93c16.22,0,27.84,6.78,33.16,15.49h0.48v-66.8 C676.63,72.31,706.41,72.31,706.41,72.31z M676.64,175.43c0-2.42-0.24-5.33-0.73-7.75c-2.66-11.62-12.1-21.06-25.66-21.06 c-19.12,0-29.77,16.94-29.77,38.97c0,21.54,10.65,37.28,29.53,37.28c12.1,0,22.75-8.23,25.66-21.06c0.73-2.66,0.97-5.57,0.97-8.71 V175.43z"></path>
                              </g>
                           </g>
                           <path class="st0" d="M769.68,89.95c-0.11-0.53-0.24-1.04-0.39-1.55c-0.12-0.39-0.25-0.76-0.39-1.14c-0.12-0.32-0.25-0.64-0.39-0.95 c-0.12-0.27-0.25-0.54-0.39-0.81c-0.12-0.24-0.25-0.47-0.39-0.7c-0.12-0.21-0.25-0.42-0.39-0.63c-0.13-0.19-0.26-0.38-0.39-0.57 c-0.13-0.17-0.26-0.35-0.39-0.51s-0.26-0.32-0.39-0.47s-0.26-0.3-0.39-0.44s-0.26-0.28-0.39-0.41c-0.13-0.13-0.26-0.25-0.39-0.37 s-0.26-0.23-0.39-0.35c-0.13-0.11-0.26-0.22-0.39-0.33c-0.13-0.1-0.26-0.2-0.39-0.3c-0.13-0.1-0.26-0.19-0.39-0.28 s-0.26-0.18-0.39-0.27c-0.13-0.08-0.26-0.16-0.39-0.24c-0.13-0.08-0.26-0.16-0.39-0.24c-0.13-0.07-0.26-0.14-0.39-0.21 s-0.26-0.14-0.39-0.2s-0.26-0.13-0.39-0.19c-0.13-0.06-0.26-0.12-0.39-0.18s-0.26-0.11-0.39-0.17c-0.13-0.05-0.26-0.1-0.39-0.15 s-0.26-0.1-0.39-0.14s-0.26-0.09-0.39-0.13s-0.26-0.08-0.39-0.12s-0.26-0.07-0.39-0.11c-0.13-0.03-0.26-0.07-0.39-0.1 s-0.26-0.06-0.39-0.09s-0.26-0.05-0.39-0.08s-0.26-0.05-0.39-0.07s-0.26-0.04-0.39-0.06s-0.26-0.04-0.39-0.06s-0.26-0.03-0.39-0.04 s-0.26-0.03-0.39-0.04s-0.26-0.02-0.39-0.03s-0.26-0.02-0.39-0.03s-0.26-0.01-0.39-0.02c-0.13,0-0.26-0.01-0.39-0.01 s-0.26-0.01-0.39-0.01s-0.26,0.01-0.39,0.01s-0.26,0-0.39,0.01c-0.13,0-0.26,0.01-0.39,0.02c-0.13,0.01-0.26,0.02-0.39,0.03 s-0.26,0.02-0.39,0.03s-0.26,0.03-0.39,0.04c-0.13,0.02-0.26,0.03-0.39,0.04c-0.13,0.02-0.26,0.04-0.39,0.06s-0.26,0.04-0.39,0.06 s-0.26,0.05-0.39,0.08s-0.26,0.05-0.39,0.08s-0.26,0.06-0.39,0.09s-0.26,0.07-0.39,0.1c-0.13,0.04-0.26,0.07-0.39,0.11 s-0.26,0.08-0.39,0.12s-0.26,0.09-0.39,0.13c-0.13,0.05-0.26,0.09-0.39,0.14s-0.26,0.1-0.39,0.15s-0.26,0.11-0.39,0.16 c-0.13,0.06-0.26,0.12-0.39,0.18s-0.26,0.12-0.39,0.19s-0.26,0.14-0.39,0.21s-0.26,0.14-0.39,0.21c-0.13,0.08-0.26,0.16-0.39,0.24 c-0.13,0.08-0.26,0.16-0.39,0.24c-0.13,0.09-0.26,0.18-0.39,0.27s-0.26,0.19-0.39,0.28c-0.13,0.1-0.26,0.2-0.39,0.3 c-0.13,0.11-0.26,0.22-0.39,0.33s-0.26,0.23-0.39,0.34c-0.13,0.12-0.26,0.24-0.39,0.37c-0.13,0.13-0.26,0.27-0.39,0.4 c-0.13,0.14-0.26,0.28-0.39,0.43s-0.26,0.3-0.39,0.46c-0.13,0.16-0.26,0.33-0.39,0.5c-0.13,0.18-0.26,0.37-0.39,0.55 c-0.13,0.2-0.26,0.4-0.39,0.61c-0.13,0.22-0.26,0.45-0.39,0.68c-0.14,0.25-0.27,0.51-0.39,0.77c-0.14,0.3-0.27,0.6-0.39,0.9 c-0.14,0.36-0.27,0.73-0.39,1.1c-0.15,0.48-0.28,0.98-0.39,1.48c-0.25,1.18-0.39,2.42-0.39,3.7c0,1.27,0.14,2.49,0.39,3.67 c0.11,0.5,0.24,0.99,0.39,1.47c0.12,0.37,0.24,0.74,0.39,1.1c0.12,0.3,0.25,0.6,0.39,0.89c0.12,0.26,0.25,0.51,0.39,0.76 c0.12,0.23,0.25,0.45,0.39,0.67c0.12,0.2,0.25,0.41,0.39,0.6c0.13,0.19,0.25,0.37,0.39,0.55c0.13,0.17,0.25,0.34,0.39,0.5 c0.13,0.15,0.26,0.3,0.39,0.45c0.13,0.14,0.26,0.28,0.39,0.42c0.13,0.13,0.26,0.26,0.39,0.39c0.13,0.12,0.26,0.25,0.39,0.37 c0.13,0.11,0.26,0.22,0.39,0.33s0.26,0.21,0.39,0.32c0.13,0.1,0.26,0.2,0.39,0.3c0.13,0.09,0.26,0.18,0.39,0.27s0.26,0.18,0.39,0.26 s0.26,0.16,0.39,0.24c0.13,0.08,0.26,0.15,0.39,0.23c0.13,0.07,0.26,0.14,0.39,0.21s0.26,0.13,0.39,0.19 c0.13,0.06,0.26,0.13,0.39,0.19c0.13,0.06,0.26,0.11,0.39,0.17s0.26,0.11,0.39,0.17c0.13,0.05,0.26,0.1,0.39,0.14 c0.13,0.05,0.26,0.1,0.39,0.14s0.26,0.08,0.39,0.12s0.26,0.08,0.39,0.12s0.26,0.07,0.39,0.1s0.26,0.07,0.39,0.1s0.26,0.06,0.39,0.08 c0.13,0.03,0.26,0.06,0.39,0.08c0.13,0.02,0.26,0.05,0.39,0.07s0.26,0.04,0.39,0.06s0.26,0.04,0.39,0.05 c0.13,0.02,0.26,0.03,0.39,0.04s0.26,0.03,0.39,0.04s0.26,0.02,0.39,0.03s0.26,0.02,0.39,0.03s0.26,0.01,0.39,0.01 s0.26,0.01,0.39,0.01c0.05,0,0.1,0,0.15,0c0.08,0,0.16,0,0.24-0.01c0.13,0,0.26,0,0.39-0.01c0.13,0,0.26,0,0.39-0.01 s0.26-0.02,0.39-0.03s0.26-0.01,0.39-0.03c0.13-0.01,0.26-0.02,0.39-0.04c0.13-0.01,0.26-0.03,0.39-0.04 c0.13-0.02,0.26-0.03,0.39-0.05c0.13-0.02,0.26-0.04,0.39-0.06s0.26-0.04,0.39-0.06s0.26-0.05,0.39-0.08s0.26-0.05,0.39-0.08 s0.26-0.06,0.39-0.09s0.26-0.06,0.39-0.1s0.26-0.07,0.39-0.11s0.26-0.08,0.39-0.12s0.26-0.08,0.39-0.13 c0.13-0.04,0.26-0.09,0.39-0.14s0.26-0.1,0.39-0.15s0.26-0.11,0.39-0.16c0.13-0.06,0.26-0.11,0.39-0.17s0.26-0.12,0.39-0.18 s0.26-0.13,0.39-0.2s0.26-0.14,0.39-0.21s0.26-0.15,0.39-0.23s0.26-0.15,0.39-0.24c0.13-0.08,0.26-0.17,0.39-0.26 c0.13-0.09,0.26-0.18,0.39-0.27c0.13-0.1,0.26-0.19,0.39-0.29s0.26-0.21,0.39-0.32s0.26-0.22,0.39-0.33 c0.13-0.12,0.26-0.24,0.39-0.36c0.13-0.13,0.26-0.26,0.39-0.39c0.13-0.14,0.26-0.28,0.39-0.42c0.13-0.15,0.26-0.3,0.39-0.45 c0.13-0.16,0.26-0.33,0.39-0.49c0.13-0.18,0.26-0.36,0.39-0.54c0.13-0.2,0.26-0.4,0.39-0.6c0.14-0.22,0.26-0.44,0.39-0.67 c0.14-0.25,0.27-0.5,0.39-0.76c0.14-0.29,0.27-0.58,0.39-0.88c0.14-0.36,0.27-0.72,0.39-1.1c0.15-0.48,0.28-0.97,0.39-1.46 c0.25-1.17,0.39-2.4,0.39-3.66C770.03,92.19,769.9,91.05,769.68,89.95z"></path>
                           <g>
                              <g>
                                 <rect x="738.36" y="126.29" class="st5" width="30.01" height="117.88"></rect>
                              </g>
                           </g>
                           <g>
                              <g>
                                 <path class="st5" d="M912.39,184.14c0,43.33-30.5,62.69-60.51,62.69c-33.4,0-59.06-22.99-59.06-60.75 c0-38.73,25.41-62.45,61-62.45C888.91,123.63,912.39,148.32,912.39,184.14z M823.56,185.35c0,22.75,11.13,39.94,29.29,39.94 c16.94,0,28.8-16.7,28.8-40.42c0-18.4-8.23-39.45-28.56-39.45C832.03,145.41,823.56,165.74,823.56,185.35z"></path>
                              </g>
                           </g>
                           <g>
                              <path class="st5" d="M386,243.52c-2.95,0-5.91-0.22-8.88-0.66c-15.75-2.34-29.65-10.67-39.13-23.46 c-9.48-12.79-13.42-28.51-11.08-44.26c2.34-15.75,10.67-29.65,23.46-39.13c12.79-9.48,28.51-13.42,44.26-11.08 s29.65,10.67,39.13,23.46l7.61,10.26l-55.9,41.45l-15.21-20.51l33.41-24.77c-3.85-2.36-8.18-3.94-12.78-4.62 c-9-1.34-17.99,0.91-25.3,6.33s-12.07,13.36-13.41,22.37c-1.34,9,0.91,17.99,6.33,25.3c5.42,7.31,13.36,12.07,22.37,13.41 c9,1.34,17.99-0.91,25.3-6.33c4.08-3.02,7.35-6.8,9.72-11.22l22.5,12.08c-4.17,7.77-9.89,14.38-17.02,19.66 C411,239.48,398.69,243.52,386,243.52z"></path>
                           </g>
                        </svg>
                     </div>
                  </div>
               </div>
               <div class="col-12 text-center lh150 mt20 mt-md50 px15 px-md0">
                  <div class="trpre-heading f-20 f-md-22 w500 lh150 yellow-clr">
                     <span>
                     It’s Time to Get Over Boring Content &amp; Cash into The Latest Trending Topics in 2022 
                     </span>
                  </div>
               </div>
               <div class="col-12 mt-md25 mt20 f-md-45 f-28 w500 text-center black-clr lh150">
                  Breakthrough A.I. Technology <span class="w700 blue-clr">Creates Traffic Pulling Websites Packed with Trendy Content &amp; Videos from Just One Keyword</span> in 60 Seconds... 
               </div>
               <div class="col-12 mt-md25 mt20 f-20 f-md-22 w500 text-center lh150 black-clr">
                  Anyone can easily create &amp; sell traffic generating websites for dentists, attorney, affiliates,<br class="d-none d-md-block"> coaches, SAAS, retail stores, book shops, gyms, spas, restaurants, &amp; 100+ other niches...
               </div>
            </div>
            <div class="row d-flex align-items-center flex-wrap mt20 mt-md40">
               <div class="col-md-7 col-12 min-md-video-width-left">
                  <div class="col-12 responsive-video">
                     <iframe src="https://trendio.dotcompal.com/video/embed/2p8pv1cndk" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                        box-shadow: none !important;" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                  </div>
               </div>
               <div class="col-md-5 col-12 mt20 mt-md0 min-md-video-width-right">
                  <div class="key-features-bg1">
                     <ul class="list-head1 pl0 m0 f-16 lh150 w500 black-clr">
                        <li>Works Seamlessly in Any Niche or Topic - <span class="w600">Just One Keyword</span> </li>
                        <li>All of That Without Content Creation, Editing, Camera, or Tech Hassles Ever </li>
                        <li>Drive Tons of FREE Viral Traffic Using the POWER Of Trendy Content </li>
                        <li>Grab More Authority, Engagement, &amp; Leads on your Business Website </li>
                        <li>Monetize Other’s Content Legally with Banner Ads, Affiliate Offers &amp; AdSense </li>
                        <li class="w600">FREE AGENCY LICENSE INCLUDED TO BUILD AN INCREDIBLE INCOME </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="tr-all">
         <picture>
            <source media="(min-width:767px)" srcset="https://cdn.oppyo.com/launches/jobiin/special-bonus/tr-lst-dt.webp" class="img-fluid d-block mx-auto" style="width:100%">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/jobiin/special-bonus/tr-lst-mb.webp" class="img-fluid d-block mx-auto">
            <img src="https://cdn.oppyo.com/launches/jobiin/special-bonus/tr-lst-dt.webp" alt="Steps" class="img-fluid d-block mx-auto" style="width:100%">
         </picture>
         <picture>
            <source media="(min-width:767px)" srcset="https://cdn.oppyo.com/launches/jobiin/special-bonus/tr-dt.webp" class="img-fluid d-block mx-auto" style="width:100%">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/jobiin/special-bonus/tr-mb.webp" class="img-fluid d-block mx-auto">
            <img src="https://cdn.oppyo.com/launches/jobiin/special-bonus/tr-dt.webp" alt="Steps" class="img-fluid d-block mx-auto" style="width:100%">
         </picture>
         <picture>
            <source media="(min-width:767px)" srcset="https://cdn.oppyo.com/launches/jobiin/special-bonus/pr-dt.webp" class="img-fluid d-block mx-auto" style="width:100%">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/jobiin/special-bonus/pr-mb.webp" class="img-fluid d-block mx-auto">
            <img src="https://cdn.oppyo.com/launches/jobiin/special-bonus/pr-dt.webp" alt="Steps" class="img-fluid d-block mx-auto" style="width:100%">
         </picture>
      </div>
 <!------Trendio Section Ends------>
   
<!-------Exclusive Bonus----------->
<div class="exclusive-bonus">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <div class="white-clr f-24 f-md-36 lh140 w600">
                  Exclusive Bonus #4 : Kyza
               </div>
               <div class="f-18 f-md-20 w500 mt20 white-clr">
                  20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
               </div>
            </div>
         </div>
      </div>
   </div>

   <div class="header-section-ky">
               <div class="container">
                  <div class="row">
                        <div class="col-md-3 col-md-12 mx-auto">
                           <img src="assets/images/kyza-logo.png" class="img-fluid d-block mx-auto">
                        </div>
                     <div class="col-12 text-center">
                     <div class="mt20 mt-md60 text-center px-sm15 px0">
                        <!--<div class="f-20 f-sm-24 w600 text-center post-heading lh140">
                          STOP Paying To Multiple Marketing Apps For Running Your Business!
                        </div>-->
						 <div class="f-20 f-sm-22 w600 text-center lh140 d-inline-flex align-items-center justify-content-center relative">
                           <img src="assets/images/stop.png" class="img-responsive mx-xs-center stop-img">
						   <div class="post-heading1">
						  <i>Paying 100s of Dollar Monthly To Multiple Marketing Apps! </i>
						   </div>
                        </div>
                     </div>
                     </div>
                     <div class="col-12 mt20 mt-md60 text-center">
                        <div class="f-md-41 f-28 w700 black-clr lh140 line-center">	
						Game Changer All-In-One Growth Suite... <br class="visible-lg">
						✔️Builds Beautiful Pages & Websites ✔️Unlocks Branding Designs Kit ✔️Creates AI-Powered Optin Forms & Pop-Ups, & ✔️Lets You Send UNLIMITED Emails 					
                        </div>
                        <div class="mt-md25 mt20 f-20 f-md-26 w600 black-clr text-center lh140">
                        All From Single Dashboard Without Any Tech Skills and <u>Zero Monthly Fees</u>
                        </div>
                        <div class="col-12 col-md-12  mt20 mt-md40">
							<div class="row d-flex flex-wrap align-items-center">
							   <div class="col-md-7 col-12 mt20 mt-md0">
									<div class="responsive-video">  
										<iframe class="embed-responsive-item"  src="https://kyza.dotcompal.com/video/embed/nr4t9jffvd" style="width: 100%;height: 100%; background: transparent !important;
										box-shadow: none !important;" frameborder="0" allow="fullscreen" allowfullscreen></iframe>
								  </div>
							   </div>
							   <div class="col-md-5 col-12 mt20 mt-md0">
									<div class="key-features-bg-ky">
									  <ul class="list-head-ky pl0 m0 f-18 f-md-19 lh140 w500 black-clr">
										<li>Create Unlimited Stunning Pages, Popups, And Forms </li>
										<li>Mobile, SEO, And Social Media Optimized Pages </li>
										<li>Send Unlimited Beautiful Emails For Tons Of Traffic & Sales </li>
										<li>Over 100 Done-For-You High Converting Templates </li>
										<li>2 Million+ Stock Images & Branding Graphics Kit </li>
										<li>Comes With Agency License To Serve & Charge Your Clients </li>
										<li>100% Newbie Friendly & Step-By-Step Training Included  </li>
									  </ul>
								   </div>
							   </div>
						   </div>
                        </div>
						
						
						
						
                     </div>
                  </div>
               </div>
            </div>
            <!-- Header Section End -->
<img src="assets/images/kyza-gb1.png" class="img-fluid mx-auto d-block" style="width: 100%;">
<img src="assets/images/kyza-gb2.png" class="img-fluid mx-auto d-block" style="width: 100%;">
<img src="assets/images/kyza-gb3.png" class="img-fluid mx-auto d-block" style="width: 100%;">


<!-- Vocalic Section Start -->
<div class="exclusive-bonus">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <div class="white-clr f-24 f-md-36 lh140 w600">
                            Exclusive Bonus #5 : Vocalic
                        </div>
                        <div class="f-18 f-md-20 w500 mt20 white-clr">
                            20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
                        </div>
                    </div>
                </div>
            </div>
        </div>




        <div class="header-section-vc">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row align-items-center">
                     <div class="col-md-12 text-center">
                        <svg xmlns="http://www.w3.org/2000/svg" id="Layer_2" viewBox="0 0 290.61 100" style="max-height:56px;">
                           <defs>
                              <style>
                                 .cls-1 {
                                 fill: #fff;
                                 }
                                 .cls-1,
                                 .cls-2 {
                                 fill-rule: evenodd;
                                 }
                                 .cls-2,
                                 .cls-3 {
                                 fill: #00aced;
                                 }
                              </style>
                           </defs>
                           <g id="Layer_1-2">
                              <g>
                                 <g>
                                    <path class="cls-3" d="M104.76,79.86l-5.11-6.34c-.16-.2-.45-.23-.65-.07-.37,.3-.75,.59-1.14,.87-3.69,2.66-7.98,4.34-12.49,4.9-1.11,.14-2.23,.21-3.35,.21s-2.24-.07-3.35-.21c-4.52-.56-8.8-2.24-12.49-4.9-.39-.28-.77-.57-1.14-.87-.2-.16-.49-.13-.65,.07l-5.11,6.34c-.16,.2-.13,.49,.07,.65,5.24,4.22,11.53,6.88,18.21,7.71,.37,.05,.74,.09,1.11,.12v11.67h6.7v-11.67c.37-.03,.74-.07,1.11-.12,6.68-.83,12.96-3.48,18.21-7.71,.2-.16,.23-.45,.07-.65Z">
                                    </path>
                                    <path class="cls-3" d="M82.02,74.47c10.57,0,19.13-7.45,19.13-16.65V31.08c0-4.6-2.14-8.76-5.6-11.77-3.46-3.01-8.24-4.88-13.53-4.88-10.57,0-19.13,7.45-19.13,16.65v26.74c0,9.2,8.57,16.65,19.13,16.65Zm-11.14-41.53c0-5.35,4.99-9.69,11.14-9.69,3.08,0,5.86,1.08,7.87,2.84,2.01,1.75,3.26,4.18,3.26,6.85v23.02c0,5.35-4.99,9.69-11.14,9.69s-11.14-4.34-11.14-9.69v-23.02Z">
                                    </path>
                                    <path class="cls-3" d="M82.02,38.89c1.71,0,3.1-1.39,3.1-3.1s-1.39-3.1-3.1-3.1-3.1,1.39-3.1,3.1,1.39,3.1,3.1,3.1Z">
                                    </path>
                                    <path class="cls-3" d="M82.02,47.54c1.71,0,3.1-1.39,3.1-3.1s-1.39-3.1-3.1-3.1-3.1,1.39-3.1,3.1,1.39,3.1,3.1,3.1Z">
                                    </path>
                                    <path class="cls-3" d="M82.02,56.2c1.71,0,3.1-1.39,3.1-3.1s-1.39-3.1-3.1-3.1-3.1,1.39-3.1,3.1,1.39,3.1,3.1,3.1Z">
                                    </path>
                                 </g>
                                 <path class="cls-1" d="M25.02,58.54L10.61,26.14c-.08-.19-.25-.29-.45-.29H.5c-.18,0-.32,.08-.42,.23-.1,.15-.11,.31-.04,.47l20.92,46.71c.08,.19,.25,.29,.45,.29h8.22c.2,0,.37-.1,.45-.29L60.4,15.13c.07-.16,.06-.32-.03-.47-.1-.15-.24-.23-.42-.23h-9.67c-.2,0-.37,.11-.45,.29L26.07,58.54c-.1,.21-.29,.34-.53,.34-.23,0-.43-.13-.53-.34h0Z">
                                 </path>
                                 <path class="cls-1" d="M117.96,52c-.13-.79-.2-1.58-.2-2.38s.07-1.6,.2-2.38c1.15-6.95,7.2-12.05,14.24-12.05,3.84,0,7.49,1.51,10.21,4.23l1.1,1.1c.19,.19,.49,.19,.68,0l6.03-6.03c.19-.19,.19-.49,0-.68l-1.1-1.1c-4.5-4.5-10.56-7.01-16.92-7.01-12.06,0-22.27,8.99-23.75,20.97-.12,.98-.18,1.97-.18,2.96s.06,1.98,.18,2.96c1.48,11.97,11.69,20.97,23.75,20.97,6.37,0,12.42-2.51,16.92-7.01l1.1-1.1c.19-.19,.19-.49,0-.68l-6.03-6.03c-.19-.19-.49-.19-.68,0l-1.1,1.1c-2.72,2.72-6.37,4.23-10.21,4.23-7.04,0-13.09-5.1-14.24-12.05h0Z">
                                 </path>
                                 <path class="cls-1" d="M258.21,52c-.13-.79-.2-1.58-.2-2.38s.07-1.6,.2-2.38c1.15-6.95,7.2-12.05,14.24-12.05,3.84,0,7.49,1.51,10.21,4.23l1.1,1.1c.19,.19,.49,.19,.68,0l6.03-6.03c.19-.19,.19-.49,0-.68l-1.1-1.1c-4.5-4.5-10.56-7.01-16.92-7.01-12.06,0-22.27,8.99-23.75,20.97-.12,.98-.18,1.97-.18,2.96s.06,1.98,.18,2.96c1.48,11.97,11.69,20.97,23.75,20.97,6.37,0,12.42-2.51,16.92-7.01l1.1-1.1c.19-.19,.19-.49,0-.68l-6.03-6.03c-.19-.19-.49-.19-.68,0l-1.1,1.1c-2.72,2.72-6.37,4.23-10.21,4.23-7.04,0-13.09-5.1-14.24-12.05h0Z">
                                 </path>
                                 <path class="cls-1" d="M174.91,25.87c-11.97,1.48-20.97,11.69-20.97,23.75s8.99,22.27,20.97,23.75c.98,.12,1.97,.18,2.96,.18s1.98-.06,2.96-.18c2.14-.26,4.24-.82,6.23-1.65,.19-.08,.3-.24,.3-.44v-9.77c0-.19-.09-.35-.27-.43-.17-.09-.35-.07-.5,.05-1.86,1.41-4.03,2.35-6.34,2.73-.79,.13-1.58,.2-2.38,.2s-1.6-.07-2.38-.2l-.2-.03c-6.86-1.23-11.85-7.24-11.85-14.21s5.1-13.09,12.05-14.24c.79-.13,1.58-.2,2.38-.2s1.59,.07,2.38,.2l.19,.03c6.87,1.23,11.87,7.24,11.87,14.21v22.7c0,.26,.22,.48,.48,.48h8.53c.26,0,.48-.22,.48-.48v-22.7c0-12.06-8.99-22.27-20.97-23.75-.98-.12-1.97-.18-2.96-.18s-1.98,.06-2.96,.18h0Z">
                                 </path>
                                 <path class="cls-1" d="M210.57,0c-.33,0-.59,.27-.59,.59V72.96c0,.33,.27,.59,.59,.59h10.5c.33,0,.59-.27,.59-.59V.59c0-.33-.27-.59-.59-.59h-10.5Z">
                                 </path>
                                 <path class="cls-1" d="M229.84,25.39c-.33,0-.59,.27-.59,.59v46.97c0,.33,.27,.59,.59,.59h10.5c.33,0,.59-.27,.59-.59V25.98c0-.33-.27-.59-.59-.59h-10.5Z">
                                 </path>
                                 <path class="cls-2" d="M229.84,8.96c-.33,0-.59,.07-.59,.15v11.94c0,.08,.27,.15,.59,.15h10.5c.33,0,.59-.07,.59-.15V9.11c0-.08-.27-.15-.59-.15h-10.5Z">
                                 </path>
                              </g>
                           </g>
                        </svg>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50 text-center">
                  <div class="preheadline f-18 f-md-22 w800 blue-clr lh140">
                     <i><span class="red-clr">Attention:</span> Futuristic A.I. Technology Do All The Work For You!</i>
                  </div>
               </div>
               <div class="col-12 mt-md30 mt20 f-md-50 f-28 w600 text-center white-clr lh140">
                <span class="w800"> Create Profit-Pulling Videos with Voiceovers   </span><br>
                <span class="w800 f-30 f-md-55 highlight-heading">In Just 60 Seconds <u>without</u>...</span> <br>
               Being in Front of Camera, Speaking A Single Word or Hiring Anyone Ever
                
               </div>
               <div class="col-12 mt-md35 mt20  text-center">
                  <div class="post-heading f-18 f-md-24 w600 text-center lh150 white-clr">
                    Create &amp; Sell Videos &amp; Voiceovers In ANY LANGUAGE For Big Profits To Dentists, Gyms, Spas, Cafes, Retail Stores, Book Shops, Affiliate Marketers, Coaches &amp; 100+ Other Niches… 
                  </div>
               </div>
            </div>
            <div class="row d-flex align-items-center flex-wrap mt20 mt-md40">
               <div class="col-md-7 col-12 min-md-video-width-left">
                  <div class="col-12 responsive-video border-video">
                     <iframe src="https://vocalic.dotcompal.com/video/embed/g1mdnyzmub" style="width:100%; height:100%" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                  </div>
                  <div class="col-12 mt40 d-none d-md-block">
                     <div class="f-18 f-md-26 w800 lh150 text-center white-clr">
                        FREE Agency License Included <br> for A Limited Time!
                     </div>
                     <div class="f-16 f-md-18 lh150 w500 text-center mt20 mt-md50 white-clr">
                        Use Coupon Code <span class="w800 yellow-clr">"VOCALIC"</span> for an Extra <span class="w800 yellow-clr">10% Discount</span> 
                     </div>
                     <div class="row">
                        <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center">
                           <a href="#buynow" class="cta-link-btn d-block px0">Get Instant Access To Vocalic</a>
                        </div>
                     </div>
                     <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                        <img src="https://cdn.oppyo.com/launches/viddeyo/special/compaitable-with1.webp" class="img-responsive mx-xs-center md-img-right" alt="visa">
                        <div class="d-md-block d-none visible-md px-md30"><img src="https://cdn.oppyo.com/launches/viddeyo/special/v-line.webp" class="img-fluid" alt="line">
                        </div>
                        <img src="https://cdn.oppyo.com/launches/viddeyo/special/days-gurantee1.webp" class="img-responsive mx-xs-auto mt15 mt-md0" alt="30 days">
                     </div>
                  </div>
               </div>
               <div class="col-md-5 col-12 mt20 mt-md0 min-md-video-width-right">
                  <div class="calendar-wrap mb20 mb-md0">
                     <ul class="list-head pl0 m0 f-18 lh150 w400 white-clr">
                        <li>Tap Into Fast-Growing <span class="w700">$200 Billion Video &amp; Voiceover Industry</span></li>
                        <li><span class="w700">Create Stunning Videos In ANY Niche</span> With In-Built Video Creator</li>
                        <li><span class="w700">Create &amp; Publish YouTube Shorts</span> For Tons Of FREE Traffic</li>
                        <li><span class="w700">Make Passive Affiliate Commissions</span> Creating Product Review Videos</li>
                        <li>Real Human-Like Voiceover In <span class="w700">150+ Unique Voices And 30+ Languages</span></li>
                        <li>Convert Any Text Script Into A <span class="w700">Whiteboard &amp; Explainer Videos</span></li>
                        <li><span class="w700">Create Videos Using Just A Keyword</span> With Ready-To-Use Stock Images</li>
                        <li><span class="w700">Boost Engagement &amp; Sales </span> Using Videos</li>
                        <li><span class="w700">Add Background Music</span> To Your Videos.</li>
                        <li><span class="w700">Built-In Content Spinner</span> - Make Unique Scripts</li>
                        <li><span class="w700">Store Media Files </span> With Integrated MyDrive</li>
                        <li><span class="w700">100% Newbie Friendly </span> - No Tech Hassles Ever</li>
                        <li><span class="w700">Agency License Included</span> To Build On Incredible Income Helping Clients</li>
                        <li>FREE Training To<span class="w700"> Find Tons Of Clients</span></li>                        
                     </ul>
                  </div>
                  <div class="d-md-none">
                     <div class="f-18 f-md-26 w800 lh150 text-center white-clr">
                        FREE Agency License Included for A Limited Time!
                     </div>
                     <div class="f-16 f-md-18 lh150 w500 text-center mt10 white-clr">
                        Use Coupon Code <span class="w800 yellow-clr">"VOCALIC"</span> for an Extra <span class="w800 yellow-clr">10% Discount</span> 
                     </div>
                     <div class="row">
                        <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center">
                           <a href="#buynow" class="cta-link-btn d-block px0">Get Instant Access To Vocalic</a>
                        </div>
                     </div>
                     <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                        <img src="https://cdn.oppyo.com/launches/viddeyo/special/compaitable-with1.webp" class="img-responsive mx-xs-center md-img-right" alt="visa">
                        <div class="d-md-block d-none visible-md px-md30"><img src="https://cdn.oppyo.com/launches/viddeyo/special/v-line.webp" class="img-fluid" alt="line">
                        </div>
                        <img src="https://cdn.oppyo.com/launches/viddeyo/special/days-gurantee1.webp" class="img-responsive mx-xs-auto mt15 mt-md0" alt="30 days">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <div class="gr-1">
      <div class="container-fluid p0">
         <picture>
            <source media="(min-width:768px)" srcset="assets/images/vocalic1.webp">
            <source media="(min-width:320px)" srcset="assets/images/vocalic1-mview.webp" style="width:100%" class="vidvee-mview">
            <img src="assets/images/vocalic1.webp" alt="Vidvee Steps" class="img-fluid" style="width: 100%;">
         </picture>
          <picture>
            <source media="(min-width:768px)" srcset="assets/images/vocalic2.webp">
            <source media="(min-width:320px)" srcset="assets/images/vocalic2-mview.webp">
            <img src="assets/images/vocalic2.webp" alt="Flowers" style="width:100%;">
         </picture>
      </div>
   </div>
<!-- Vocalic Section ENds -->




      

<!-------Exclusive Bonus----------->
<div class="exclusive-bonus">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <div class="white-clr f-24 f-md-36 lh140 w600">
                  Exclusive Bonus #6 : Viddeyo
               </div>
               <div class="f-18 f-md-20 w500 mt20 white-clr">
                  20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
               </div>
            </div>
         </div>
      </div>
   </div>



   <div class="viddeyo-header-section">
      <div class="container">
         <div class="row">
               <div class="col-12">
                  <div class="row align-items-center">
                     <div class="col-md-12 text-center">
                     <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 909.75 162.72" style="enable-background:new 0 0 909.75 162.72; max-height:56px;" xml:space="preserve">
                           <style type="text/css">
                              .st00{fill:#FFFFFF;}
                              .st11{fill:url(#SVGID_2_);}
                           </style>
                           <g>
                              <g>
                                 <path class="st00" d="M216.23,71.07L201.6,97.26l-35.84-64.75l-3.16-5.7h29.13l0.82,1.49L216.23,71.07z M288.35,26.81l-3.16,5.7
                                    l-56.4,103.68h-6.26l-11.71-22.25l14.63-26.18l0.01,0.02l2.73-4.94l4.81-8.68l25.38-45.87l0.82-1.49H288.35z"></path>
                                 <path class="st00" d="M329.85,26.61v109.57h-23.23V26.61H329.85z"></path>
                                 <path class="st00" d="M454.65,81.7c0,2.7-0.2,5.41-0.59,8.04c-3.58,24.36-23.15,43.45-47.59,46.42l-0.26,0.03h-50.93v-69.9h23.23
                                    v46.67h26.13c12.93-2,23.4-11.91,26.09-24.74c0.45-2.14,0.67-4.33,0.67-6.51c0-2.11-0.21-4.22-0.62-6.26
                                    c-1.52-7.54-5.76-14.28-11.95-18.97c-3.8-2.88-8.14-4.84-12.75-5.78c-1.58-0.32-3.19-0.52-4.83-0.6h-45.98V26.82L403.96,27
                                    c11.89,0.9,23.21,5.67,32.17,13.61c9.78,8.65,16.15,20.5,17.96,33.36C454.46,76.49,454.65,79.1,454.65,81.7z"></path>
                                 <path class="st00" d="M572.18,81.7c0,2.7-0.2,5.41-0.59,8.04c-3.58,24.36-23.15,43.45-47.59,46.42l-0.26,0.03h-50.93v-69.9h23.23
                                    v46.67h26.13c12.93-2,23.4-11.91,26.09-24.74c0.45-2.14,0.67-4.33,0.67-6.51c0-2.11-0.21-4.22-0.62-6.26
                                    c-1.52-7.54-5.76-14.28-11.95-18.97c-3.8-2.88-8.14-4.84-12.75-5.78c-1.58-0.32-3.19-0.52-4.83-0.6h-45.98V26.82L521.5,27
                                    c11.89,0.9,23.2,5.67,32.17,13.61c9.78,8.65,16.15,20.5,17.96,33.36C572,76.49,572.18,79.1,572.18,81.7z"></path>
                                 <path class="st00" d="M688.08,26.82v23.23h-97.72V31.18l0.01-4.36H688.08z M613.59,112.96h74.49v23.23h-97.72v-18.87l0.01-4.36
                                    V66.1l23.23,0.14h64.74v23.23h-64.74V112.96z"></path>
                                 <path class="st00" d="M808.03,26.53l-5.07,6.93l-35.84,48.99l-1.08,1.48L766,83.98l-0.04,0.05l0,0.01l-0.83,1.14v51h-23.23V85.22
                                    l-0.86-1.18l-0.01-0.01l-0.04-0.05l-0.04-0.06l-1.08-1.48l-35.84-48.98l-5.07-6.93h28.77l1.31,1.78l24.46,33.43l24.46-33.43
                                    l1.31-1.78H808.03z"></path>
                                 <path class="st00" d="M909.75,81.69c0,30.05-24.45,54.49-54.49,54.49c-30.05,0-54.49-24.45-54.49-54.49S825.2,27.2,855.25,27.2
                                    C885.3,27.2,909.75,51.65,909.75,81.69z M855.25,49.95c-17.51,0-31.75,14.24-31.75,31.75s14.24,31.75,31.75,31.75
                                    c17.51,0,31.75-14.24,31.75-31.75C887,64.19,872.76,49.95,855.25,49.95z"></path>
                              </g>
                           </g>
                           <linearGradient id="SVGID_2_" gradientUnits="userSpaceOnUse" x1="0" y1="81.3586" x2="145.8602" y2="81.3586">
                              <stop offset="0.4078" style="stop-color:#FFFFFF"></stop>
                              <stop offset="1" style="stop-color:#FC6DAB"></stop>
                           </linearGradient>
                           <path class="st11" d="M11.88,0c41.54,18.27,81.87,39.12,121.9,60.41c7.38,3.78,12.24,11.99,12.07,20.32
                              c0.03,7.94-4.47,15.72-11.32,19.73c-27.14,17.07-54.59,33.74-82.21,50c-0.57,0.39-15.29,8.93-15.47,9.04
                              c-1.4,0.77-2.91,1.46-4.44,1.96c-14.33,4.88-30.25-4.95-32.19-19.99c-0.18-1.2-0.23-2.5-0.24-3.72c0.35-25.84,1.01-53.01,2.04-78.83
                              C2.57,42.41,20.3,32.08,34.79,40.31c8.16,4.93,16.31,9.99,24.41,15.03c2.31,1.49,7.96,4.89,10.09,6.48
                              c5.06,3.94,7.76,10.79,6.81,17.03c-0.68,4.71-3.18,9.07-7.04,11.77c-0.38,0.24-1.25,0.88-1.63,1.07c-3.6,2.01-7.5,4.21-11.11,6.17
                              c-9.5,5.22-19.06,10.37-28.78,15.27c11.29-9.81,22.86-19.2,34.53-28.51c3.54-2.55,4.5-7.53,1.83-10.96c-0.72-0.8-1.5-1.49-2.49-1.9
                              c-0.18-0.08-0.42-0.22-0.6-0.29c-11.27-5.17-22.82-10.58-33.98-15.95c0,0-0.22-0.1-0.22-0.1l-0.09-0.02l-0.18-0.04
                              c-0.21-0.08-0.43-0.14-0.66-0.15c-0.11-0.01-0.2-0.07-0.31-0.04c-0.21,0.03-0.4-0.04-0.6,0.03c-0.1,0.02-0.2,0.02-0.29,0.03
                              c-0.14,0.06-0.28,0.1-0.43,0.12c-0.13,0.06-0.27,0.14-0.42,0.17c-0.09,0.03-0.17,0.11-0.26,0.16c-0.08,0.06-0.19,0.06-0.26,0.15
                              c-0.37,0.25-0.66,0.57-0.96,0.9c-0.1,0.2-0.25,0.37-0.32,0.59c-0.04,0.08-0.1,0.14-0.1,0.23c-0.1,0.31-0.17,0.63-0.15,0.95
                              c-0.06,0.15,0.04,0.35,0.02,0.52c0,0.02,0,0.08-0.01,0.1c0,0,0,0.13,0,0.13l0.02,0.51c0.94,24.14,1.59,49.77,1.94,73.93
                              c0,0,0.06,4.05,0.06,4.05l0,0.12l0.01,0.02l0.01,0.03c0,0.05,0.02,0.11,0.02,0.16c0.08,0.23,0.16,0.29,0.43,0.47
                              c0.31,0.16,0.47,0.18,0.71,0.09c0.06-0.04,0.11-0.06,0.18-0.1c0.02-0.01-0.01,0.01,0.05-0.03l0.22-0.13l0.87-0.52
                              c32.14-19.09,64.83-37.83,97.59-55.81c0,0,0.23-0.13,0.23-0.13c0.26-0.11,0.51-0.26,0.69-0.42c1.15-0.99,0.97-3.11-0.28-4.01
                              c0,0-0.43-0.27-0.43-0.27l-1.7-1.1C84.73,51.81,47.5,27.03,11.88,0L11.88,0z"></path>
                        </svg>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50 text-center">
                  <div class="viddeyo-preheadline f-18 f-md-20 w400 white-clr lh140">
                     <span class="w600 text-uppercase">Warning:</span> 1000s of Business Websites are Leaking Profit and Sales by Uploading Videos to YouTube
                  </div>
               </div>
               <div class="col-12 mt-md15 mt10 f-md-48 f-28 w400 text-center white-clr lh140">
                  Start Your Own <span class="w700 highlight-viddeyo">Video Hosting Agency with Unlimited Videos with Unlimited Bandwidth</span> at Lightning Fast Speed with No Monthly FEE Ever...
               </div>
               <div class="col-12 mt20 mt-md20 f-md-21 f-18 w400 white-clr text-center lh170">
                  Yes! Total control over your videos with zero delay, no ads and no traffic leak. Agency License Included
               </div>
         </div>
         <div class="row d-flex align-items-center flex-wrap mt20 mt-md40">
               <div class="col-md-7 col-12 min-md-video-width-left">
                  <!-- <img src="https://cdn.oppyo.com/launches/viddeyo/special/proudly.webp" class="img-fluid d-block mx-auto" alt="proudly"> -->
                  <div class="col-12 responsive-video border-video">
                     <iframe src="https://launch.oppyo.com/video/embed/xo6b4lwdid" style="width:100%; height:100%" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                  </div>
               </div>
               <div class="col-md-5 col-12 mt20 mt-md0 min-md-video-width-right">
                    <div class="calendar-wrap">
                        <div class="calendar-wrap-inline">
                            <ul class="list-head pl0 m0 f-18 f-md-18 lh150 w500 black-clr">
                                <li>100% Control on Your Video Traffic &amp; Channels</li>
                                <li>Close More Prospects for Your Agency Services</li>
                                <li>Boost Commissions, Customer Satisfaction &amp; Sales Online</li>
                                <li>Tap Into $398 Billion E-Learning Industry</li>
                                <li>Tap Into Huge 82% Of Overall Traffic on Internet </li>
                                <li>Boost Your Clients Sales-Agency License Included</li>
                            </ul>
                        </div>
                    </div>
                </div>
         </div>
      </div>
    </div></div>
    <div class="viddeyo-second-section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="row viddeyo-header-list-block">
                        <div class="col-12 col-md-6">
                            <ul class="viddeyo-features-list pl0 f-18 f-md-20 lh150 w400 black-clr text-capitalize">
                                <li><span class="w600">Upload Unlimited Videos</span> - Sales, E-Learning, Training, Product Demo, or ANY Video</li>
                                <li><span class="w600">Super-Fast Delivery (After All, Time Is Money!)</span> No Buffering. No Delay </li>
                                <li><span class="w600">Get Maximum Visitors Engagement</span>-No Traffic Leakage with Unwanted Ads or Video Suggestions</li>
                                <li>Create 100% Mobile &amp; SEO Optimized <span class="w600">Video Channels &amp; Playlists</span></li>
                                <li><span class="w600">Stunning Promo &amp; Social Ads Templates</span> for Monetization &amp; Social Traffic</li>
                                <li>Fully Customizable Drag and Drop <span class="w600">WYSIWYG Video Ads Editor</span></li>
                                <li><span class="w600">Intelligent Analytics</span> to Measure the Performance </li>
                            </ul>
                        </div>
                        <div class="col-12 col-md-6 mt10 mt-md0">
                            <ul class="viddeyo-features-list pl0 f-18 f-md-20 lh150 w400 black-clr text-capitalize">
                                <li class="w600">Tap Into Fast-Growing $398 Billion E-Learning, Video Marketing &amp; Video Agency Industry. </li>
                                <li>Manage All the Videos, Courses, &amp; Clients Hassle-Free, <span class="w600">All-In-One Central Dashboard.</span> </li>
                                <li>Robust Solution That Has <span class="w600">Served Over 69 million Video Views for Customers</span></li>
                                <li>HLS Player- Optimized to <span class="w600">Work Beautifully with All Browsers, Devices &amp; Page Builders</span> </li>
                                <li>Connect With Your Favourite Tools. <span class="w600">20+ Integrations</span> </li>
                                <li><span class="w600">Completely Cloud-Based</span> &amp; Step-By-Step Video Training Included </li>
                                <li>PLUS, YOU'LL RECEIVE - <span class="w600">A LIMITED AGENCY LICENSE IF YOU BUY TODAY</span> </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            
            </div>
        </div>
    </div>
    <!-- Header Viddeyo End -->
    <div class="gr-1">
      <div class="container-fluid p0">
         <picture>
            <source media="(min-width:768px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/viddeyo-steps.webp">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/viddeyo-steps-mview.webp" style="width:100%" class="vidvee-mview">
            <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/vidvee-steps.webp" alt="Vidvee Steps" class="img-fluid" style="width: 100%;">
         </picture>
          <picture>
            <source media="(min-width:768px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/viddeyo-letsfaceit.webp">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/viddeyo-letsfaceit-mview.webp">
            <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/viddeyo-letsfaceit.webp" alt="Flowers" style="width:100%;">
         </picture>
         <picture>
            <source media="(min-width:768px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/viddeyo-proudly.webp">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/viddeyo-proudly-mview.webp">
            <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/viddeyo-proudly.webp" alt="Flowers" style="width:100%;">
         </picture> 
      </div>
   </div>
   <!--Viddeyo ends-->
<!-------Exclusive Bonus----------->
<div class="exclusive-bonus">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                  <div class="white-clr f-24 f-md-36 lh140 w600">
                     Exclusive Bonus #7 : YoDrive
                  </div>
                  <div class="f-18 f-md-20 w500 mt20 white-clr">
                     20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
                  </div>
               </div>
            </div>
         </div>
      </div>

       <!--Yodrive Header Section Start -->
       <div class="header-section-yodrive">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row align-items-center">
                     <div class="col-md-12 text-center">
                        <svg id="Layer_11" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 300 76.82" style="max-height:55px ">
                           <defs>
                              <style>.cls-11{fill:#fff;}</style>
                           </defs>
                           <path class="cls-11" d="M189.27,24.02h11.1c3.81,0,6.79,.5,8.94,1.49,2.15,.99,3.68,2.51,4.6,4.57,.92,2.06,1.37,4.79,1.37,8.19v16.54c0,3.48-.45,6.27-1.35,8.38-.9,2.11-2.41,3.68-4.54,4.71-2.13,1.03-5.06,1.54-8.8,1.54h-11.32V24.02Zm11.21,38.96c1.61,0,2.79-.25,3.56-.76,.77-.5,1.25-1.24,1.46-2.21,.21-.97,.31-2.37,.31-4.21v-18.78c0-1.72-.12-3.02-.36-3.9-.24-.88-.74-1.53-1.49-1.96-.75-.43-1.93-.64-3.53-.64h-1.96V62.98h2.02Z"></path>
                           <path class="cls-11" d="M221.01,37.02h8.47v5.21c1.12-1.94,2.21-3.36,3.28-4.26,1.07-.9,2.31-1.35,3.73-1.35,.34,0,.77,.04,1.29,.11v8.63c-.97-.45-1.98-.67-3.03-.67-2.06,0-3.81,1.05-5.27,3.14v21.58h-8.47V37.02Z"></path>
                           <path class="cls-11" d="M241.69,25.36h8.52v6.34h-8.52v-6.34Zm.06,11.66h8.47v32.41h-8.47V37.02Z"></path>
                           <path class="cls-11" d="M253.8,37.02h8.19l2.97,21.81,2.8-21.81h8.13l-6.62,32.41h-8.97l-6.5-32.41Z"></path>
                           <path class="cls-11" d="M281.41,67.07c-1.85-1.94-2.78-4.75-2.78-8.41v-10.88c0-3.66,.93-6.47,2.8-8.41,1.87-1.94,4.56-2.92,8.07-2.92s6.28,.97,7.96,2.92c1.68,1.94,2.52,4.82,2.52,8.63v5.66h-12.95v6.28c0,1.38,.21,2.39,.64,3.03,.43,.64,1.06,.95,1.88,.95,1.68,0,2.52-1.21,2.52-3.64v-3.14h7.85v2.8c0,3.25-.9,5.74-2.69,7.46-1.79,1.72-4.37,2.58-7.74,2.58-3.55,0-6.25-.97-8.1-2.92Zm10.68-17.44v-3.14c0-1.42-.21-2.44-.62-3.06-.41-.62-1.07-.93-1.96-.93s-1.49,.34-1.88,1.01c-.39,.67-.59,1.87-.59,3.59v2.52h5.05Z"></path>
                           <path class="cls-11" d="M31.6,53.4c1.35,0,2.63,.33,3.76,.91l13.77-18.56c-.68-1.2-1.06-2.57-1.06-4.04,0-4.54,3.68-8.21,8.21-8.21s8.21,3.68,8.21,8.21c0,.72-.09,1.41-.27,2.08l11.6,7.39c1.5-1.6,3.63-2.6,5.99-2.6,2.68,0,5.06,1.28,6.56,3.27l8.18-5.34-3.67-4.8s10.52-1.66,13.34-2.1c-4.7-13.07-17.75-16.36-27.28-6.15-1.84-10.57-16.76-28.07-39.57-22.34C16.57,6.87,12.14,33.9,19.15,46.02c-3.5-3.85-5.65-8.34-6.45-13.49C5.41,35.99,.09,43.94,0,53.51l23.87,5.32v.07h.01c1.11-3.2,4.13-5.51,7.72-5.51Z"></path>
                           <path class="cls-11" d="M59.87,31.65c0,2.01-1.63,3.64-3.64,3.64s-3.64-1.63-3.64-3.64,1.63-3.64,3.64-3.64,3.64,1.63,3.64,3.64Z"></path>
                           <path class="cls-11" d="M110.28,36.8c.1-2.29,.82-7.94,.82-7.94l-8.09,16.26-3.52-4.87-9.45,6.17c0,.12,0,.25,0,.38,0,4.53-3.68,8.21-8.21,8.21s-8.21-3.68-8.21-8.21c0-.54,.05-1.06,.15-1.56l-11.79-7.6c-1.48,1.42-3.48,2.29-5.69,2.29-1.34,0-2.6-.32-3.72-.89l-13.8,18.56c.67,1.19,1.05,2.56,1.05,4.02,0,4.54-3.68,8.21-8.21,8.21-3.69,0-6.81-2.43-7.85-5.78L.49,58.54c2.15,11.88,14.08,18.21,21.28,18.21,.65,.05,78.92,.07,78.92,.07,10.66,.13,19.73-8.29,20.91-18.86,1.17-10.56-5.58-17.37-11.32-21.16Z"></path>
                           <path class="cls-11" d="M35.32,61.74c0,2.01-1.63,3.64-3.64,3.64s-3.64-1.63-3.64-3.64,1.63-3.64,3.64-3.64,3.64,1.63,3.64,3.64Z"></path>
                           <path class="cls-11" d="M85.37,46.85c0,2.01-1.63,3.64-3.64,3.64s-3.64-1.63-3.64-3.64,1.63-3.64,3.64-3.64,3.64,1.63,3.64,3.64Z"></path>
                           <path class="cls-11" d="M142.87,51.53l-8.86-27.43h8.7l4.88,15.99,4.49-15.99h8.47l-8.75,27.43v18.01h-8.92v-18.01Z"></path>
                           <rect class="cls-11" x="241.72" y="25.45" width="8.53" height="6.34"></rect>
                           <path class="cls-11" d="M142.86,51.43l-8.86-27.42h8.69l4.88,15.98,4.49-15.98h8.47l-8.75,27.42v18h-8.91v-18Z"></path>
                           <path class="cls-11" d="M164.52,67.09c-1.89-1.87-2.83-4.58-2.83-8.13v-11.66c0-3.55,.94-6.26,2.83-8.13,1.89-1.87,4.59-2.8,8.1-2.8s6.21,.93,8.1,2.8c1.89,1.87,2.83,4.58,2.83,8.13v11.66c0,3.55-.94,6.26-2.83,8.13-1.89,1.87-4.59,2.8-8.1,2.8s-6.21-.93-8.1-2.8Zm10.15-4.06c.35-.77,.53-1.92,.53-3.45v-12.84c0-1.53-.18-2.69-.53-3.48-.36-.78-1.02-1.18-1.99-1.18s-1.7,.4-2.07,1.21c-.37,.8-.56,1.95-.56,3.45v12.84c0,1.5,.19,2.64,.56,3.42,.37,.78,1.07,1.18,2.07,1.18s1.63-.38,1.99-1.15Z"></path>
                        </svg>
                     </div>
                     
                  </div>
               </div>
               <div class="col-12 mt30 mt-md50 text-center">
                  <div class="f-18 f-md-21 w600 white-clr lh140">
                     <div class="">
                        <span class="yellow-clr">Warning!</span> Businesses are Losing Over 53% Sales Due to Slow Loading Websites & Videos!
                     </div>
                  </div>
               </div>
               <!-- <div class="col-12 mt10 text-center">
                  <div class="game-changer">
                     Game Changer 
                  </div>
               </div> -->
               <div class="col-12 f-md-45 f-28 w600 text-center white-clr lh140 mt20 mt-md80 relative">
                    <img src="https://cdn.oppyo.com/launches/yodrive/special/game-changer.webp" class="img-fluid d-none d-md-block gameimg">
                    <div class="gametext d-md-none">
                     Game Changer
                    </div>
               <div class="mainheadline">
                  <span class="yellow-clr">Host UNLIMITED Videos, Website Images, Training, Audios or Any Media Files</span> at Blazing-Fast Speed at A Low One Time Fee
               </div>
               </div>
               <div class="col-12 mt-md40 mt20 f-18 f-md-24 w400 text-center lh150 white-clr text-capitalize">
                  And…Supercharge Your or Client’s Websites, Apps & Pages with Fast-Loading Media Content.<br class="d-none d-md-block"> NO Tech Hassles & Monthly Fee Ever…
               </div>
            </div>
            <div class="row d-flex align-items-center flex-wrap mt20 mt-md40">
               <div class="col-md-7 col-12 min-md-video-width-left">
                  <!-- <img src="https://cdn.oppyo.com/launches/yodrive/special/product-image.webp" class="img-fluid d-block mx-auto"> -->
                  <div class="col-12 responsive-video border-video">
                     <iframe src="https://yodrive.dotcompal.com/video/embed/2sjxc83pok" style="width:100%; height:100%" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                     </div>
               </div>
               <div class="col-md-5 col-12 mt20 mt-md0 min-md-video-width-right">
                  <div class="calendar-wrap">
                     <ul class="list-head pl0 m0 f-18 f-md-20 lh150 w400 white-clr">
                        <li>Must Have & Backbone to Run Any Business Online</li>
                        <li>Play Fast-Loading Videos on Any Page, Site, or Membership</li>
                        <li>Tap Into $398 Billion E-learning Industry</li>
                        <li>Deliver Client Projects, Docs & PDFs Securely</li>
                        <li>Boost Engagement, Leads, & Sales with Your Media Content</li>
                        <li>FREE Agency License Included to Bank In BIG</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <img src="https://cdn.oppyo.com/launches/yodrive/special/down-arrow.webp" class="img-fluid mx-auto d-block vert-move" alt="Arrow" style="margin-top:-70px;">
      
      <div class="gr-1">
         <div class="container-fluid p0">
            <picture>
               <source media="(min-width:768px)" srcset="assets/images/yodrive-1.webp">
               <source media="(min-width:320px)" srcset="assets/images/yodrive-1-mview.webp" style="width:100%" class="vidvee-mview">
               <img src="assets/images/yodrive-1.webp" alt="Vidvee Steps" class="img-fluid" style="width: 100%;">
            </picture>
            
         </div>
      </div>
      <!-- Yodrive Header Section End -->
      <!-- Bonus Section Header Start -->
      <div class="bonus-header">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-10 mx-auto heading-bg text-center">
                  <div class="f-24 f-md-36 lh140 w700"> When You Purchase YoSeller, You Also Get <br class="hidden-xs"> Instant Access To These 15 Exclusive Bonuses</div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus Section Header End -->

 <!-- Bonus #1 Section Start -->
 <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 1</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus1.webp" class="img-fluid mx-auto d-block" alt="Bonus1">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        How To Start Online Coaching Business
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>Everybody is an expert (or 'expert enough') in at least one body of knowledge.  It doesn't matter what it is. </b></li>
                           <li> Maybe you know how to sing a little bit better than everybody else, maybe you know your way around the basketball court, maybe you have discovered a way of running a little bit faster, or maybe you know how to make money with Twitter or Facebook.</li>
                           <li> Regardless of who you're dealing with, everybody has at least one area of expertise.</li>
                           <li>There is a tremendous demand for online coaching services and will continue to rise in the foreseeable future.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #1 Section End -->
      <!-- Bonus #2 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 2</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30">
                  <div class="row">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="assets/images/bonus2.webp" class="img-fluid mx-auto d-block" alt="Bonus2">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md0 text-capitalize">
                        Infopreneur Academy
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>You Get to Learn Directly From a Successful Product Creator who is behind 7 figure product launches. This Step by Step 7 Part Video Course Will Show You How You Can Create Products and Decrease Refund Rates and Support Issues. There's No Theory Here! </li>
                           <li>This step by step, 7 part video series, takes you by the hand and shows you how to create a quality informational products, the right way! There's no theory involved here like other video series. You learn directly from a successful product developer who is behind 7 figure launches that went thru a lot of costly and time consuming mistakes; so that you don't have to.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #2 Section End -->
      <!-- Bonus #3 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 3</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus3.webp" class="img-fluid mx-auto d-block " alt="Bonus3">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Launch Your Online Course
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>Many people have switched to remote learning and online courses to get a leg up on their education and improve upon their intellectual skills.</b></li>
                           <li>This is a shocking increase in online learning that proves the importance of online course options in the modern world.</li>
                           <li>On top of the numerous benefits for students that come with online learning, creating online courses can have great benefits for the creators too. </li>
                           <li>The reason for this is that online course creators can charge for their courses, resulting in an increased income.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #3 Section End -->
      <!-- Bonus #4 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 4</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="assets/images/bonus4.webp" class="img-fluid mx-auto d-block " alt="Bonus4">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Course Engagement Hacks
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>Even though you have an excellent course, people's attention spans are short, and they can get sidetracked easily. </li>
                           <li>The truth is that if they don't complete your online course, then they will get buyer's remorse in the end and won't feel motivated to buy your other classes. That said, if, however they complete the online course, then they will at least if the course is good mini will feel that they got the right amount of value from you.</li>
                           <li><b>More consumption equals more trust.<br>More trust equals more sales to your other courses.</b></li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #4 End -->
      <!-- Bonus #5 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 5</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus5.webp" class="img-fluid mx-auto d-block " alt="Bonus5">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        eWriterPro - Professional eBook Creator
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>Everything You Need to Create Beautiful Professional Quality eBooks at the Touch of a Button.</li>
                           <li>If you are looking for a way to ethically make money on the internet, then the creation of information products is by far the easiest and most rewarding way! </li>
                           <li>An $8Billion Market, the opportunity of the millennium is - eBooks! </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #5 End -->

      <!-- CTA Button Section Start -->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 yellow-clr">"ACHALVIP"</span> for an Additional <span class="w700 yellow-clr">15% Discount</span> on Agency Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                     <span class="text-center">Grab YoSeller + My 15 Exclusive Bonuses</span> <br>
                  </a>
               </div>

               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                     Use Coupon Code <span class="w700 yellow-clr">"YOBUNDLE"</span> for an Additional <span class="w700 yellow-clr">$50 Discount</span> on Agency Licence
                  </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="https://jvz8.com/c/10103/387542/" class="text-center bonusbuy-btn">
                     <span class="text-center">Grab YoSeller Bundle + My 15 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                        <span class="f-14 f-md-18 w500 ">Days</span>
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                        <span class="f-14 f-md-18 w500 ">Hours</span>
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                        <span class="f-14 f-md-18 w500 ">Mins</span>
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                        <span class="f-14 f-md-18 w500 ">Sec</span>
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->
<!-- Bonus #6 Start -->
<div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 6</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="assets/images/bonus6.webp" class="img-fluid mx-auto d-block " alt="Bonus6">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Instant Video Creator
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>Give Your Web Site a Live touch - instantly add streaming video to their web site without expensive equipment, hiring expensive services or paying costly monthly fees!
                           </li>
                           <li>Instant Video Creator is a revolutionary new software that allows multiple users to create their videos under their own accounts. Not only can you use it to create just your own streaming videos, you can also start your own service where you charge a monthly fee to host such videos for your customers. </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #6 End -->
      <!-- Bonus #7 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 7</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus7.webp" class="img-fluid mx-auto d-block " alt="Bonus7">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Create Video with Camtasia 9 Advanced
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <b>This Is a Practical Video Course With 15 Additional Video Tutorials Showing You How to Do What You Learned in The Course.</b><br><br>
                           <li>Interactive Hotspot </li>
                           <li>Interactive Hosting </li>
                           <li>Survey Creation </li>
                           <li>Screen Drawing</li>
                           <li>Picture In Picture Video</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #7 End -->
      <!-- Bonus #8 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 8</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus8.webp" class="img-fluid mx-auto d-block " alt="Bonus8">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Project Genius
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>Do you have projects to plan? Do you want to take a professional approach? Do you want to increase your success rate? Then Project Genius is the software that will help you plan your projects! </li>
                           <li>From now on, you will be able to take a professional approach to your project planning and increase your success rate. You will no longer have surprises during the course of your project.</li>
                           <li>You will better know what you want, your will organize your brain storming sessions better, and you will set your milestones and deadlines with ease. </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #8 End -->
      <!-- Bonus #9 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 9</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus9.webp" class="img-fluid mx-auto d-block " alt="Bonus9">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Success Graphics Web Design Package
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>A Great Collection of Header Graphics, Matching Footers, Backgrounds & Buy Buttons - In .PSD & Blank .JPGs Ready-To-Use Formats!</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #9 End -->
      <!-- Bonus #10 start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 10</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus10.webp" class="img-fluid mx-auto d-block " alt="Bonus10">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        PLR Profit Machine
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>PLR material is content, which you can utilize as if you were the one who created it.  </b></li>
                           <li>In truth, another individual has done every task, making your job easier specifically in establishing and marketing information necessary in selling a product or service online.</li>
                           <li>It is a comprehensive course showing how to become proficient and profitable with PLR products.</li>
                           <li>The methods used in this course come from experience and actual teachings to ensure that everything taught here are feasible.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #10 End -->

      <!-- CTA Button Section Start -->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 yellow-clr">"ACHALVIP"</span> for an Additional <span class="w700 yellow-clr">15% Discount</span> on Agency Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                     <span class="text-center">Grab YoSeller + My 15 Exclusive Bonuses</span> <br>
                  </a>
               </div>

               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                     Use Coupon Code <span class="w700 yellow-clr">"YOBUNDLE"</span> for an Additional <span class="w700 yellow-clr">$50 Discount</span> on Agency Licence
                  </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="https://jvz8.com/c/10103/387542/" class="text-center bonusbuy-btn">
                     <span class="text-center">Grab YoSeller Bundle + My 15 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr"><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 smmltd">Days</span> </div><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">0300</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div><div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div></div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 11</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus11.webp" class="img-fluid mx-auto d-block " alt="Bonus13">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Template Showcase Builder
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li><b>Build an Amazing Responsive Template Quickly! </b></li>
                           <li>It is a 'responsive template' viewer. What this means is you can show off yours or affiliate templates and allow visitors to view template in different sizes!</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #11 End -->
      <!-- Bonus #12 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 12</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus12.webp" class="img-fluid mx-auto d-block " alt="Bonus12">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        The Entrepreneur Code
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>Have you ever wondered if there is some kind of “code” that some entrepreneurs are able to break and others spend their whole lives working on and never becoming successful?</b></li>
                           <li>Well, there is, and unlocking that code will give you the same exact success as some of the world's richest people. </li>
                           <li>In this course you will find a list of the 50 most common habits among success stories like Bill Gates, Larry Page and even Barack Obama! Make their habits your habits and you can crack the code and become just as big of successes as they are!</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #12 End -->
      <!-- Bonus #13 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 13</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus13.webp" class="img-fluid mx-auto d-block " alt="Bonus13">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Web Conversion Videos
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>Learn How to Make More Money On the Existing Traffic of Your Website!</b></li>
                           <li>Does Your Site Convert The Maximum Number of Visitors to Customers? If not, you are just wasting your time marketing your brand or your website.</li>
                           <li>These videos cover the factors that will help your website increase its conversion rate beyond anything you have ever hoped for.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #13 End -->
      <!-- Bonus #14 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 14</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus14.webp" class="img-fluid mx-auto d-block " alt="Bonus14">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        10 Keys Of Product Creation Success
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           This course is a checklist of things that you want to consider throughout your WSO or product launch. <br><br>
                           <b>Topics covered:</b><br><br>

                           <li>What are the elements of top selling products?</li>
                           <li>What should your content be to drive traffic?</li>
                           <li>What are the keys to to good copy?</li>
                           <li>What are the 8 numbers you must track?</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #14 End -->
      <!-- Bonus #15 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 15</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus15.webp" class="img-fluid mx-auto d-block " alt="Bonus15">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Make First $100 On The Web
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>One of the easiest ways to get started in internet marketing is to promote affiliate products.</b> </li>
                           <li>This means you get a commission for each sale of a particular product that comes from your link or referral. It's definitely the easiest route into making money online.</li>
                           <li>This course will help you get started with all the information you need.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #15 End -->

      <!-- Huge Woth Section Start -->
      <div class="huge-area mt30 mt-md10">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="f-md-65 f-40 lh120 w700 white-clr">That's Huge Worth of</div>
                  <br>
                  <div class="f-md-60 f-40 lh120 w800 yellow-clr">$3300!</div>
               </div>
            </div>
         </div>
      </div>
      <!-- Huge Worth Section End -->

      <!-- text Area Start -->
      <div class="white-section pb0">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="f-md-36 f-25 lh140 w500">So what are you waiting for? You have a great opportunity <br class="hidden-xs"> ahead + <span class="w700">My 15 Bonus Products</span> are making it a <br class="hidden-xs"> <span class="w700">completely NO Brainer!!</span></div>
               </div>
            </div>
         </div>
      </div>
      <!-- text Area End -->

      <!-- CTA Button Section Start -->
      <div class="cta-btn-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-16 f-md-20 lh150 w500 text-center black-clr">
                  Use Coupon Code <span class="w700 blue-clr">"ACHALVIP"</span> for an Additional <span class="w700 blue-clr">15% Discount</span> on Agency Licence
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn px-md80">
                     Grab YoSeller + My 15 Exclusive Bonuses
                  </a>
               </div>

               <div class="f-16 f-md-20 lh150 w400 text-center mt20 black-clr mt-md30">
                     Use Coupon Code <span class="w700 blue-clr">"YOBUNDLE"</span> for an Additional <span class="w700 blue-clr">$50 Discount</span> on Agency Licence
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="https://jvz8.com/c/10103/387542/" class="text-center bonusbuy-btn">
                     <span class="text-center">Grab YoSeller Bundle + My 15 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-12 mt15 mt-md20" align="center">
                  <h3 class="f-md-28 f-20 w500 text-center black">My Exclusive Bonuses Are Expiring in...</h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 col-md-10 mx-auto col-12 text-center">
                  <div class="countdown counter-black">
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                     <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                     <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->

      <!-- Footer Section Start -->
      <div class="footer-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <svg version="1.1" id="Layer_11" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 253.7 80" style="enable-background:new 0 0 253.7 80; max-height:60px;" xml:space="preserve">
                     ;
                     <style type="text/css">
                        .st14{fill:#FFD60D;}
                        .st13{fill:#FFFFFF;}
                        .st22{fill:#2337F6;}
                     </style>
                     <g>
                        <path class="st14" d="M130.2,64.2l-4.8,1.1c-0.4-0.9-1.1-1.6-1.9-2.2c-1-0.7-2.2-1.1-3.5-1.1c-1.7,0-3.3,0.7-4.4,1.9
                           c-1,1-1.6,2.4-1.7,3.9l-14.1,3.1c-1.1-2.5-3.6-4.3-6.5-4.3c-3.9,0-7.1,3.2-7.1,7.1v0.1l-1.8,0.4c-3.3,0.8-6.6-1.3-7.3-4.6l-10-43.9
                           c-0.8-3.3,1.3-6.6,4.6-7.3l9.5-2.1v0.6c0.1,1.4,1.3,2.5,2.7,2.5c0.1,0,0.1,0,0.2,0c1.5-0.1,2.6-1.3,2.5-2.8c0-0.5-0.2-0.9-0.4-1.3
                           l14.2-3.1v0.5c0,0.7,0.3,1.3,0.8,1.8c0.5,0.4,1.1,0.7,1.8,0.7c0.1,0,0.1,0,0.2,0c1.5-0.1,2.6-1.3,2.5-2.8c0-0.5-0.2-0.9-0.4-1.3
                           l12.4-2.7c3.3-0.8,6.6,1.3,7.3,4.6l2,8.8l1.1,5l6.8,30.2C135.7,60.3,133.6,63.6,130.2,64.2z"></path>
                        <g>
                           <path class="st13" d="M34.4,21.8L20.7,48.3v15.1h-6.8V48.3L0,21.8h7.6l9.6,20.4l9.6-20.4C26.8,21.8,34.4,21.8,34.4,21.8z"></path>
                           <path class="st13" d="M41.5,61.8c-2.5-1.4-4.5-3.4-5.9-6c-1.4-2.6-2.2-5.6-2.2-8.9c0-3.4,0.7-6.3,2.2-8.9c1.5-2.6,3.5-4.6,6-6
                              c2.6-1.4,5.4-2.1,8.6-2.1s6,0.7,8.6,2.1c2.6,1.4,4.6,3.4,6,6c1.5,2.6,2.2,5.6,2.2,8.9c0,3.4-0.8,6.3-2.3,8.9
                              c-1.5,2.6-3.6,4.6-6.2,6c-2.6,1.4-5.5,2.1-8.6,2.1C46.8,63.9,44,63.2,41.5,61.8z M54.9,56.7c1.5-0.8,2.8-2.1,3.7-3.8
                              c1-1.7,1.4-3.7,1.4-6.1c0-2.4-0.5-4.4-1.4-6.1c-0.9-1.7-2.1-2.9-3.7-3.7c-1.5-0.8-3.2-1.3-4.9-1.3c-1.8,0-3.4,0.4-4.9,1.3
                              c-1.5,0.8-2.7,2.1-3.6,3.7c-0.9,1.7-1.3,3.7-1.3,6.1c0,3.6,0.9,6.3,2.7,8.2c1.8,1.9,4.1,2.9,6.9,2.9C51.7,58,53.3,57.5,54.9,56.7z
                              "></path>
                        </g>
                        <g>
                           <path class="st13" d="M167.1,50.3h-23.7c0.2,2.5,1.1,4.5,2.8,6c1.7,1.5,3.7,2.3,6.1,2.3c3.5,0,5.9-1.4,7.3-4.3h6.9
                              c-0.9,2.9-2.6,5.2-5.1,7c-2.5,1.8-5.5,2.7-9.1,2.7c-3,0-5.6-0.7-8-2c-2.3-1.3-4.2-3.2-5.5-5.6s-2-5.2-2-8.4c0-3.2,0.6-6,1.9-8.4
                              c1.3-2.4,3.1-4.3,5.5-5.6c2.3-1.3,5-2,8.1-2c2.9,0,5.5,0.6,7.8,1.9c2.3,1.3,4.1,3.1,5.3,5.4c1.3,2.3,1.9,5,1.9,8
                              C167.3,48.3,167.2,49.3,167.1,50.3z M160.6,45.1c0-2.4-0.9-4.3-2.5-5.7c-1.7-1.4-3.7-2.1-6.1-2.1c-2.2,0-4.1,0.7-5.7,2.1
                              c-1.6,1.4-2.5,3.3-2.8,5.7H160.6z"></path>
                           <path class="st13" d="M180,21.8v41.6h-6.4V21.8H180z"></path>
                           <path class="st13" d="M194.9,21.8v41.6h-6.4V21.8H194.9z"></path>
                           <path class="st13" d="M231.5,50.3h-23.7c0.2,2.5,1.1,4.5,2.8,6c1.6,1.5,3.7,2.3,6.1,2.3c3.5,0,5.9-1.4,7.3-4.3h6.9
                              c-0.9,2.9-2.6,5.2-5.1,7c-2.5,1.8-5.5,2.7-9.1,2.7c-3,0-5.6-0.7-8-2c-2.3-1.3-4.2-3.2-5.5-5.6c-1.3-2.4-2-5.2-2-8.4
                              c0-3.2,0.6-6,1.9-8.4c1.3-2.4,3.1-4.3,5.5-5.6c2.3-1.3,5-2,8.1-2c2.9,0,5.5,0.6,7.8,1.9c2.3,1.3,4.1,3.1,5.3,5.4
                              c1.3,2.3,1.9,5,1.9,8C231.7,48.3,231.7,49.3,231.5,50.3z M225,45.1c0-2.4-0.9-4.3-2.5-5.7c-1.7-1.4-3.7-2.1-6.1-2.1
                              c-2.2,0-4.1,0.7-5.7,2.1c-1.6,1.4-2.5,3.3-2.8,5.7H225z"></path>
                           <path class="st13" d="M248.2,33.2c1.6-0.9,3.4-1.3,5.5-1.3v6.6h-1.6c-2.5,0-4.4,0.6-5.7,1.9s-1.9,3.5-1.9,6.6v16.3H238v-31h6.4v4.5
                              C245.4,35.3,246.6,34.1,248.2,33.2z"></path>
                        </g>
                        <circle class="st13" cx="93.4" cy="73.7" r="6.3"></circle>
                        <path class="st22" d="M131.6,51.7c0,5.2-3.4,9.7-8,11.4c-1-0.7-2.2-1.1-3.5-1.1c-1.7,0-3.3,0.7-4.4,1.9H78.9c0-3.9,3.1-7,7-7h33.3
                           c2.9,0,5.3-2.3,5.3-5.2s-2.3-5.3-5.3-5.3h-11.4c-3.4,0-6.5-1.4-8.8-3.7c-2.2-2.2-3.5-5.3-3.5-8.7c0.1-6.8,5.6-12.2,12.4-12.2h22.2
                           c0,1.9-0.8,3.7-2.1,5s-3,2-4.9,2H108c-2.9,0-5.3,2.3-5.3,5.2s2.3,5.3,5.3,5.3h11.4c3.4,0,6.5,1.4,8.8,3.7
                           C130.3,45.3,131.6,48.3,131.6,51.7z"></path>
                        <path class="st22" d="M97.7,22.8c0.4-0.4,0.8-0.7,1.2-1H71.5l0,0c0,3.9,3.1,7,7,7h15.3C94.7,26.6,96,24.5,97.7,22.8z"></path>
                        <path class="st22" d="M85.6,38.8h8.1c-0.5-1.6-0.7-3.2-0.7-4.9c0-0.7,0.1-1.4,0.2-2.1H78.6l0,0C78.6,35.7,81.7,38.8,85.6,38.8z"></path>
                        <path class="st13" d="M104.3,11.2c-0.1-1-0.2-2.1-0.6-3.1C102,2.9,97-0.3,92,0c-0.5,0-1.1,0.1-1.6,0.2c-0.4,0.1-0.7,0.2-1.1,0.3
                           c-6,2-9.1,8.6-7.1,14.7c0.1,0.2,0.1,0.4,0.2,0.6c-0.2,0.3-0.2,0.7-0.2,1c0.1,1,0.9,1.8,1.9,1.7s1.8-0.9,1.7-1.9
                           c0-0.6-0.4-1.2-1-1.5c-0.1-0.2-0.2-0.4-0.2-0.6C83,9.6,85.5,4.5,90.1,3s9.6,1.1,11.2,5.9c0.3,0.8,0.4,1.6,0.5,2.3
                           c-0.4,0.4-0.6,0.9-0.5,1.4c0.1,1,0.9,1.8,1.9,1.7c1-0.1,1.8-0.9,1.7-1.9C104.8,12,104.6,11.5,104.3,11.2z"></path>
                        <circle class="st13" cx="120.1" cy="68.1" r="5.2"></circle>
                     </g>
                  </svg>
                  <div editabletype="text" class="f-16 f-md-16 w300 mt20 lh140 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-14 f-md-16 w400 lh150 white-clr text-xs-center">Copyright © YoSeller 2022</div>
                  <ul class="footer-ul w400 f-14 f-md-16 white-clr text-center text-md-right">
                     <li><a href="https://support.oppyo.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://yoseller.co/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://yoseller.co/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://yoseller.co/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://yoseller.co/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://yoseller.co/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://yoseller.co/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      </div>
      <!--Footer Section End -->
      
      <!-- timer --->
      <?php
         if ($now < $exp_date) {
         ?>
      <script type="text/javascript">
         // Count down milliseconds = server_end - server_now = client_end - client_now
         var server_end = <?php echo $exp_date; ?> * 1000;
         var server_now = <?php echo time(); ?> * 1000;
         var client_now = new Date().getTime();
         var end = server_end - server_now + client_now; // this is the real end time
         
         var noob = $('.countdown').length;
         
         var _second = 1000;
         var _minute = _second * 60;
         var _hour = _minute * 60;
         var _day = _hour * 24
         var timer;
         
         function showRemaining() {
         var now = new Date();
         var distance = end - now;
         if (distance < 0) {
         	clearInterval(timer);
         	document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
         	return;
         }
         
         var days = Math.floor(distance / _day);
         var hours = Math.floor((distance % _day) / _hour);
         var minutes = Math.floor((distance % _hour) / _minute);
         var seconds = Math.floor((distance % _minute) / _second);
         if (days < 10) {
         	days = "0" + days;
         }
         if (hours < 10) {
         	hours = "0" + hours;
         }
         if (minutes < 10) {
         	minutes = "0" + minutes;
         }
         if (seconds < 10) {
         	seconds = "0" + seconds;
         }
         var i;
         var countdown = document.getElementsByClassName('countdown');
         for (i = 0; i < noob; i++) {
         	countdown[i].innerHTML = '';
         
         	if (days) {
         		countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + days + '</span><span class="f-14 f-md-18 smmltd">Days</span> </div>';
         	}
         
         	countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + hours + '</span><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>';
         
         	countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">' + minutes + '</span><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>';
         
         	countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">' + seconds + '</span><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>';
         }
         
         }
         timer = setInterval(showRemaining, 1000);
         	
      </script>
      <?php
         } else {
         echo "Times Up";
         }
         ?>
      <!--- timer end-->
   </body>
</html>
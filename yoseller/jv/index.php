<html>
   <head>
      <title>JV Page - YoSeller JV Invite</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=9">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
      <meta name="title" content="YoSeller | JV">
      <meta name="description" content="Brand New Technology to Launch, Sell & Market Any Digital Products, Physical Goods, Courses & Services in Next 7 Minutes Flat.">
      <meta name="keywords" content="YoSeller">
      <meta property="og:image" content="https://www.yoseller.co/jv/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Dr. Amit Pareek">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="YoSeller | JV">
      <meta property="og:description" content="Brand New Technology to Launch, Sell & Market Any Digital Products, Physical Goods, Courses & Services in Next 7 Minutes Flat.">
      <meta property="og:image" content="https://www.yoseller.co/jv/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="YoSeller | JV">
      <meta property="twitter:description" content="Brand New Technology to Launch, Sell & Market Any Digital Products, Physical Goods, Courses & Services in Next 7 Minutes Flat.">
      <meta property="twitter:image" content="https://www.yoseller.co/jv/thumbnail.png">
      <link rel="icon" href="https://cdn.oppyo.com/launches/yoseller/common_assets/images/favicon.png" type="image/png">
      <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
      <!-- required -->
      <link rel="stylesheet" href="https://cdn.oppyo.com/launches/yoseller/common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style.css" type="text/css">
      <link rel="stylesheet" href="assets/css/timer.css" type="text/css">
      <script src="https://cdn.oppyo.com/launches/yoseller/common_assets/js/jquery.min.js"></script>
   </head>
   <script type="text/javascript">(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//forms.aweber.com/form/38/318212638.js";
      fjs.parentNode.insertBefore(js, fjs);
      }(document, "script", "aweber-wjs-lq8ztq6u7"));
   </script>
   <body>
      <!-- New Timer  Start-->
      <?php
         $date = 'october 11 2022 11:00 AM EST';
         $exp_date = strtotime($date);
         $now = time();  
         /*
         
         $date = date('F d Y g:i:s A eO');
         $rand_time_add = rand(700, 1200);
         $exp_date = strtotime($date) + $rand_time_add;
         $now = time();*/
         
         if ($now < $exp_date) {
         ?>
      <?php
         } else {
          echo "Times Up";
         }
         ?>
      <!-- New Timer End -->
      <!-- Header Section Start -->
      <div class="header-section">
         <div class="container">
            <div class="row">
               <div class="col-12 d-flex align-items-center justify-content-center justify-content-md-between flex-wrap flex-md-nowrap">
                  <svg version="1.1" id="Layer_11" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 253.7 80" style="enable-background:new 0 0 253.7 80; max-height:85px;" xml:space="preserve">
                     ;
                     <style type="text/css">
                        .st00{fill:#FFD60D;}
                        .st11{fill:#FFFFFF;}
                        .st22{fill:#2337F6;}
                     </style>
                     <g>
                        <path class="st00" d="M130.2,64.2l-4.8,1.1c-0.4-0.9-1.1-1.6-1.9-2.2c-1-0.7-2.2-1.1-3.5-1.1c-1.7,0-3.3,0.7-4.4,1.9
                           c-1,1-1.6,2.4-1.7,3.9l-14.1,3.1c-1.1-2.5-3.6-4.3-6.5-4.3c-3.9,0-7.1,3.2-7.1,7.1v0.1l-1.8,0.4c-3.3,0.8-6.6-1.3-7.3-4.6l-10-43.9
                           c-0.8-3.3,1.3-6.6,4.6-7.3l9.5-2.1v0.6c0.1,1.4,1.3,2.5,2.7,2.5c0.1,0,0.1,0,0.2,0c1.5-0.1,2.6-1.3,2.5-2.8c0-0.5-0.2-0.9-0.4-1.3
                           l14.2-3.1v0.5c0,0.7,0.3,1.3,0.8,1.8c0.5,0.4,1.1,0.7,1.8,0.7c0.1,0,0.1,0,0.2,0c1.5-0.1,2.6-1.3,2.5-2.8c0-0.5-0.2-0.9-0.4-1.3
                           l12.4-2.7c3.3-0.8,6.6,1.3,7.3,4.6l2,8.8l1.1,5l6.8,30.2C135.7,60.3,133.6,63.6,130.2,64.2z"/>
                        <g>
                           <path class="st11" d="M34.4,21.8L20.7,48.3v15.1h-6.8V48.3L0,21.8h7.6l9.6,20.4l9.6-20.4C26.8,21.8,34.4,21.8,34.4,21.8z"/>
                           <path class="st11" d="M41.5,61.8c-2.5-1.4-4.5-3.4-5.9-6c-1.4-2.6-2.2-5.6-2.2-8.9c0-3.4,0.7-6.3,2.2-8.9c1.5-2.6,3.5-4.6,6-6
                              c2.6-1.4,5.4-2.1,8.6-2.1s6,0.7,8.6,2.1c2.6,1.4,4.6,3.4,6,6c1.5,2.6,2.2,5.6,2.2,8.9c0,3.4-0.8,6.3-2.3,8.9
                              c-1.5,2.6-3.6,4.6-6.2,6c-2.6,1.4-5.5,2.1-8.6,2.1C46.8,63.9,44,63.2,41.5,61.8z M54.9,56.7c1.5-0.8,2.8-2.1,3.7-3.8
                              c1-1.7,1.4-3.7,1.4-6.1c0-2.4-0.5-4.4-1.4-6.1c-0.9-1.7-2.1-2.9-3.7-3.7c-1.5-0.8-3.2-1.3-4.9-1.3c-1.8,0-3.4,0.4-4.9,1.3
                              c-1.5,0.8-2.7,2.1-3.6,3.7c-0.9,1.7-1.3,3.7-1.3,6.1c0,3.6,0.9,6.3,2.7,8.2c1.8,1.9,4.1,2.9,6.9,2.9C51.7,58,53.3,57.5,54.9,56.7z
                              "/>
                        </g>
                        <g>
                           <path class="st11" d="M167.1,50.3h-23.7c0.2,2.5,1.1,4.5,2.8,6c1.7,1.5,3.7,2.3,6.1,2.3c3.5,0,5.9-1.4,7.3-4.3h6.9
                              c-0.9,2.9-2.6,5.2-5.1,7c-2.5,1.8-5.5,2.7-9.1,2.7c-3,0-5.6-0.7-8-2c-2.3-1.3-4.2-3.2-5.5-5.6s-2-5.2-2-8.4c0-3.2,0.6-6,1.9-8.4
                              c1.3-2.4,3.1-4.3,5.5-5.6c2.3-1.3,5-2,8.1-2c2.9,0,5.5,0.6,7.8,1.9c2.3,1.3,4.1,3.1,5.3,5.4c1.3,2.3,1.9,5,1.9,8
                              C167.3,48.3,167.2,49.3,167.1,50.3z M160.6,45.1c0-2.4-0.9-4.3-2.5-5.7c-1.7-1.4-3.7-2.1-6.1-2.1c-2.2,0-4.1,0.7-5.7,2.1
                              c-1.6,1.4-2.5,3.3-2.8,5.7H160.6z"/>
                           <path class="st11" d="M180,21.8v41.6h-6.4V21.8H180z"/>
                           <path class="st11" d="M194.9,21.8v41.6h-6.4V21.8H194.9z"/>
                           <path class="st11" d="M231.5,50.3h-23.7c0.2,2.5,1.1,4.5,2.8,6c1.6,1.5,3.7,2.3,6.1,2.3c3.5,0,5.9-1.4,7.3-4.3h6.9
                              c-0.9,2.9-2.6,5.2-5.1,7c-2.5,1.8-5.5,2.7-9.1,2.7c-3,0-5.6-0.7-8-2c-2.3-1.3-4.2-3.2-5.5-5.6c-1.3-2.4-2-5.2-2-8.4
                              c0-3.2,0.6-6,1.9-8.4c1.3-2.4,3.1-4.3,5.5-5.6c2.3-1.3,5-2,8.1-2c2.9,0,5.5,0.6,7.8,1.9c2.3,1.3,4.1,3.1,5.3,5.4
                              c1.3,2.3,1.9,5,1.9,8C231.7,48.3,231.7,49.3,231.5,50.3z M225,45.1c0-2.4-0.9-4.3-2.5-5.7c-1.7-1.4-3.7-2.1-6.1-2.1
                              c-2.2,0-4.1,0.7-5.7,2.1c-1.6,1.4-2.5,3.3-2.8,5.7H225z"/>
                           <path class="st11" d="M248.2,33.2c1.6-0.9,3.4-1.3,5.5-1.3v6.6h-1.6c-2.5,0-4.4,0.6-5.7,1.9s-1.9,3.5-1.9,6.6v16.3H238v-31h6.4v4.5
                              C245.4,35.3,246.6,34.1,248.2,33.2z"/>
                        </g>
                        <circle class="st11" cx="93.4" cy="73.7" r="6.3"/>
                        <path class="st22" d="M131.6,51.7c0,5.2-3.4,9.7-8,11.4c-1-0.7-2.2-1.1-3.5-1.1c-1.7,0-3.3,0.7-4.4,1.9H78.9c0-3.9,3.1-7,7-7h33.3
                           c2.9,0,5.3-2.3,5.3-5.2s-2.3-5.3-5.3-5.3h-11.4c-3.4,0-6.5-1.4-8.8-3.7c-2.2-2.2-3.5-5.3-3.5-8.7c0.1-6.8,5.6-12.2,12.4-12.2h22.2
                           c0,1.9-0.8,3.7-2.1,5s-3,2-4.9,2H108c-2.9,0-5.3,2.3-5.3,5.2s2.3,5.3,5.3,5.3h11.4c3.4,0,6.5,1.4,8.8,3.7
                           C130.3,45.3,131.6,48.3,131.6,51.7z"/>
                        <path class="st22" d="M97.7,22.8c0.4-0.4,0.8-0.7,1.2-1H71.5l0,0c0,3.9,3.1,7,7,7h15.3C94.7,26.6,96,24.5,97.7,22.8z"/>
                        <path class="st22" d="M85.6,38.8h8.1c-0.5-1.6-0.7-3.2-0.7-4.9c0-0.7,0.1-1.4,0.2-2.1H78.6l0,0C78.6,35.7,81.7,38.8,85.6,38.8z"/>
                        <path class="st11" d="M104.3,11.2c-0.1-1-0.2-2.1-0.6-3.1C102,2.9,97-0.3,92,0c-0.5,0-1.1,0.1-1.6,0.2c-0.4,0.1-0.7,0.2-1.1,0.3
                           c-6,2-9.1,8.6-7.1,14.7c0.1,0.2,0.1,0.4,0.2,0.6c-0.2,0.3-0.2,0.7-0.2,1c0.1,1,0.9,1.8,1.9,1.7s1.8-0.9,1.7-1.9
                           c0-0.6-0.4-1.2-1-1.5c-0.1-0.2-0.2-0.4-0.2-0.6C83,9.6,85.5,4.5,90.1,3s9.6,1.1,11.2,5.9c0.3,0.8,0.4,1.6,0.5,2.3
                           c-0.4,0.4-0.6,0.9-0.5,1.4c0.1,1,0.9,1.8,1.9,1.7c1-0.1,1.8-0.9,1.7-1.9C104.8,12,104.6,11.5,104.3,11.2z"/>
                        <circle class="st11" cx="120.1" cy="68.1" r="5.2"/>
                     </g>
                  </svg>
                  <div class="d-flex align-items-center flex-wrap justify-content-center  justify-content-md-end text-center text-md-end mt15 mt-md0">
                     <ul class="leader-ul f-16">
                        <li>
                           <a href="https://docs.google.com/document/d/1UqsMBPuKBNtFwOXtQYO0f6AdTs8pyEqj/edit?usp=sharing&ouid=117555941573669202344&rtpof=true&sd=true" target="_blank">JV Docs</a>
                        </li>
                        <li>|</li>
                        <li>
                           <a href="https://docs.google.com/document/d/1f2wdggLMcaC3FdmcY8wtX0MMwq8DKRLt/edit?usp=sharing&ouid=117555941573669202344&rtpof=true&sd=true" target="_blank">Swipes & Bonuses </a>
                        </li>
                        <li>|</li>
                        <li>
                           <a href="#funnel">Sales Pages</a>
                        </li>
                     </ul>
                     <a href="https://www.jvzoo.com/affiliate/affiliateinfonew/index/387544" class="affiliate-link-btn ml-md15 mt10 mt-md0" target="_blank"> Grab Your Affiliate Link</a>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50 text-center">
                  <div class="preheadline f-20 f-md-22 w400 white-clr lh140">
                     <div>Get Ready to Promote an Upcoming Trend in Online Selling</div>
                  </div>
               </div>
               <div class="col-12 mt-md30 mt20 f-md-48 f-30 w700 text-center white-clr lh140">
                  Cutting Edge Technology to <span class="yellow-clr">Launch & Sell <br class="d-none d-md-block">
                  Digital Products, Courses, Services & Goods</span> <br class="d-none d-md-block">
                  <u>in Next 7 Minutes Flat.</u>
               </div>
               <div class="col-12 mt20 mt-md30 text-center">
                  <div class="postheadline f-20 f-md-24 w600 text-center lh140 white-clr text-capitalize">               
                     Sell your Own or White Labelled/Reseller/PLR Licensed Products | <br class="d-none d-md-block">
                     Preloaded with 40+ DFY Products to Start Selling Right Away 
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md30">
                  <!-- <img src="assets/images/video-img.webp" alt="Video Image" class="img-fluid mx-auto d-block"> -->
                  <div style="padding-bottom: 56.25%;position: relative;"><iframe src="https://yoseller.dotcompal.com/video/embed/7s9ou23ndl" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                     box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div>
               </div>
            </div>
         </div>
      </div>
      <img src="assets/images/arrow.webp" alt="Arrow" class="img-fluid d-block mx-auto vert-move" style="margin-top:-40px;">
      <div class="list-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-md-45 f-28 lh140 w700 text-center black-clr">
                     <u> Encash The Latest Trend in E-Selling</u>
                  </div>
                  <div class="f-20 f-md-26 w400 text-center mt20">
                     As Festive Selling Season is Around & it will stay till Valentines day in Feb,<br class="d-none d-md-block"> Allow your  Subscribers to Start A RED Hot Digital Selling Business...
                  </div>
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md40">
               <div class="col-12 col-md-6">
                  <img src="assets/images/date.webp" alt="Date" class="img-fluid mx-auto d-block">
                  <div class="countdown counter-black mt20 col-md-8 mx-auto p0">
                     <div class="timer-label text-center"><span class="f-30 f-md-40 lh100 timerbg col-12">01</span><br><span class="f-14 w500">Days</span> </div>
                     <div class="timer-label text-center"><span class="f-30 f-md-40 lh100 timerbg">16</span><br><span class="f-14 w500">Hours</span> </div>
                     <div class="timer-label text-center"><span class="f-30 f-md-40 lh100 timerbg">59</span><br><span class="f-14 w500">Mins</span> </div>
                     <div class="timer-label text-center"><span class="f-30 f-md-40 lh100 timerbg">37</span><br><span class="f-14 w500">Sec</span> </div>
                  </div>
               </div>
               <div class="col-12 col-md-6">
                  <div class="f-18 f-md-20 lh140 w400">
                     <ul class="bonus-list pl0 m0">
                        <li>Tap into Hot <span class="w600">Digital Product and Info Selling</span> Industry</li>
                        <li>Create Stunning Websites, Stores, and Membership Sites <span class="w600">Quick and Easy</span></li>
                        <li><span class="w600">Built-in Cart with Seamless Integration</span> to PayPal, Stripe, ClickBank, JVZoo, and other Marketplace</li>
                        <li>Sell Product on your<span class="w600"> Own Marketplace & Keep 100% of the Profit.</span></li>
                        <li><span class="w600">Create Fast Loading</span> Landing Pages and Funnel for Lead Generation and Sale</li>
                        </li>
                        <li><span class="w600">200+ High Converting Templates</span> and WYSIWYG <span class="w600">Drag & Drop Editor</span></li>
                        <li><span class="w600">Included with Commercial License </span> to Provide High Demand Services to your Clients for Huge Profits</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
         <!-- Header Section End -->
         <div class="mt30 mt-md50">
            <div class="container">
               <div class="row">
                  <div class="col-12 col-md-12">
                     <div class="auto_responder_form inp-phold formbg col-12">
                        <div class="f-md-60 f-30 lh140 w700 text-center black-clr">
                           Subscribe To Our JV List
                        </div>
                        <div class="f-20 f-md-32 w500 text-center">
                           and Be The First to Know Our Special Contest, Events and Discounts                           
                        </div>
                        <!-- Aweber Form Code -->
                        <div class="mt15 mt-md50">
                           <form method="post" class="af-form-wrapper mb-md5" accept-charset="UTF-8" action="https://www.aweber.com/scripts/addlead.pl"  >
                              <div style="display: none;">
                                 <input type="hidden" name="meta_web_form_id" value="318212638" />
                                 <input type="hidden" name="meta_split_id" value="" />
                                 <input type="hidden" name="listname" value="awlist6353808" />
                                 <input type="hidden" name="redirect" value="https://www.aweber.com/thankyou-coi.htm?m=text" id="redirect_82c6d7c9f7af61342ef7293ef90a98f6" />
                                 <input type="hidden" name="meta_adtracking" value="My_Web_Form" />
                                 <input type="hidden" name="meta_message" value="1" />
                                 <input type="hidden" name="meta_required" value="name,email" />
                                 <input type="hidden" name="meta_tooltip" value="" />
                              </div>
                              <div id="af-form-318212638" class="af-form">
                                 <div id="af-body-318212638" class="af-body af-standards row justify-content-center">
                                    <div class="af-element col-md-4">
                                       <label class="previewLabel" for="awf_field-114790966" style="display:none;">Name: </label>
                                       <div class="af-textWrap mb15 mb-md15 input-type">
                                          <input id="awf_field-114790966" class="frm-ctr-popup form-control input-field" type="text" name="name" placeholder="Your Name" class="text" value=""  onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="500" />
                                       </div>
                                       <div class="af-clear"></div>
                                    </div>
                                    <div class="af-element mb15 mb-md25  col-md-4">
                                       <label class="previewLabel" for="awf_field-114790967" style="display:none;">Email: </label>
                                       <div class="af-textWrap input-type">
                                          <input class="text frm-ctr-popup form-control input-field" id="awf_field-114790967" type="text" name="email" placeholder="Your Email" value=""  tabindex="501" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " />
                                       </div>
                                       <div class="af-clear"></div>
                                    </div>
                                    <div class="af-element buttonContainer button-type form-btn white-clr col-md-4">
                                       <input name="submit" class="submit f-20 f-md-24 white-clr center-block" type="submit" value="Subscribe For JV Updates" tabindex="502" />
                                       <div class="af-clear"></div>
                                    </div>
                                 </div>
                              </div>
                              <div style="display: none;"><img src="https://forms.aweber.com/form/displays.htm?id=zIwcTIxMbMwc" alt="" /></div>
                           </form>
                           <!-- Aweber Form Code -->
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-12 mt20 mt-md70 p-md0">
                  <div class="f-24 f-md-38 w600 text-center lh140">
                     Grab Your JVZoo Affiliate Link to Jump on This <br class="d-none d-md-block">
                     Amazing Product Launch
                  </div>
               </div>
               <div class="col-md-12 mx-auto mt20">
                  <div class="row justify-content-center align-items-center">
                     <div class="col-12 col-md-3">
                        <img src="assets/images/jvzoo.webp" class="img-fluid d-block mx-auto" alt="Jvzoo"/>
                     </div>
                     <div class="col-12 col-md-4 affiliate-btn  mt-md19 mt15">
                        <a href="https://www.jvzoo.com/affiliate/affiliateinfonew/index/387544" class="f-22 f-md-30 w700 mx-auto" target="_blank">Request Affiliate Link</a>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Header Section End -->
      <!-- exciting-launch -->
      <div class="exciting-launch">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-md-44 f-28 white-clr text-center w700 lh140">
                     This Exciting Launch Event Is Divided Into 2 Phases 
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt-md60 mt30">
                  <div class="row align-items-center gx-5">
                     <div class="col-md-6 col-12">
                        <div class="exciting-launch-left">
                           <img src="assets/images/prelaunch.webp" class="img-fluid d-block mx-auto"> 
                           <div class="f-md-24 f-20 w500 lh140 text-center white-clr mt10">
                              (with Webinar)
                           </div>
                           <div class="f-18 f-md-20 w400 lh140 white-clr mt15 text-center">
                              <span class="w700">9th Oct'22</span> 10 AM EST to <br><span class="w700">11th Oct'22</span> 10:00 AM EST
                           </div>
                           <div class="f-md-24 f-20 lh140 w700 text-center yellow-clr mt20 mt-md30">
                              To Make You Max Commissions
                           </div>
                           <ul class="exciting-list f-18 w400 lh140 pl0 white-clr mt20 mt-md30">
                              <li>All Leads Are Hardcoded</li>
                              <li>Exciting $2000 Pre-Launch Contest</li>
                              <li>We'll Re-Market Your Leads Heavily</li>
                              <li>Pitch Bundle Offer on webinars.</li>
                           </ul>
                        </div>
                     </div>
                     <div class="col-md-6 col-12 mt-md0 mt20">
                        <div class="exciting-launch-left">
                           <div class="f-md-45 f-28 w700 lh140 text-center white-clr">
                              7 Days Launch Event
                           </div>
                           <div class="f-20 f-md-24 w400 lh140 white-clr mt15 text-center">
                              <span class="w700 yellow-clr">Cart Opens 11th Oct</span> at 11 AM EST<br>
                              <span class="w700 yellow-clr">Cart Closes 18th Oct,</span> 11:59 PM EST
                           </div>
                           <ul class="exciting-list f-18 w400 lh140 pl0 white-clr mt20 mt-md30">
                              <li>Big Opening Contest & Bundle Offer</li>
                              <li>High in Demand Product with Top Conversion</li>
                              <li>Deep Funnel to Make You Double-Digit EPCs</li>
                              <li>Earn up to $348/Sale</li>
                              <li>Huge $10K JV Prizes</li>
                              <li>We'll Re-Market Your Visitors Heavily</li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- exciting-launch end -->
      <div class="hello-awesome">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="awesome-feature-shape">
                     <div class="row">
                        <div class="col-12 text-center">
                           <div class="f-md-45 f-28 lh140 w700 black-clr hello-headling">
                              Hello Awesome JV’s
                           </div>
                        </div>
                        <div class="col-12 mt20 mt-md50">
                           <div class="row align-items-center">
                              <div class="col-12 col-md-6">
                                 <div class="f-18 lh140 w400 black-clr">
                                    It's Dr. Amit Pareek (Techpreneur) along with my Partner Atul Pareek (Entrepreneur & Product Creator).                                    
                                    <br><br>
                                    We have delivered 25+ 6-Figure Blockbuster Launches, Sold Software Products 
                                    of Over $9 Million, and paid over $4.5 Million in commission to our Affiliates.
                                    With the combined experience of 25+ years, we are coming back with another 
                                    Top Notch and High in Demand product that will provide a complete solution 
                                    for all your cloud storage, media content delivery, and video hosting needs under 
                                    one dashboard.<br><br>
                                    Check out the incredible features of this amazing technology that will blow away 
                                    your mind. And we guarantee that this offer will convert to Hot Cakes starting 
                                    from 11th October'22 at 11:00 AM EST! Get Ready!!
                                 </div>
                              </div>
                              <div class="col-md-6 col-12 text-center mt20 mt-md0">
                                 <div class="row">
                                    <div class="col-12"> <img src="assets/images/amit-pareek.webp" class="img-fluid d-block mx-auto"></div>
                                    <div class="col-12 col-md-6 mt20">                                      
                                       <img src="assets/images/atul-parrek.webp" class="img-fluid d-block mx-auto">
                                    </div>
                                    <!-- <div class="col-12 col-md-6 mt20">
                                       <img src="assets/images/achal-goswami.webp" class="img-fluid d-block mx-auto">
                                    </div> -->
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 text-center">
                  <div class="awesome-shape text-start">
                     <div class="row gx-0">
                        <div class="col-12 f-md-24 f-20 lh140 w600 text-center">
                           Also, here are some stats from our previous launches:
                        </div>
                        <div class="col-12 col-md-6 f-18 lh140 w400 mt20">
                           <ul class="awesome-list">
                              <li>Over 100 Pick of The Day Awards</li>
                              <li>Over $4Mn In Affiliate Sales for Partners</li>
                           </ul>
                        </div>
                        <div class="col-12 col-md-6 f-18 lh140 w400 mt20">
                           <ul class="awesome-list">
                              <li>Top 10 Affiliate & Seller (High Performance Leader)</li>
                              <li>Always in Top-10 of JVZoo Top Sellers</li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- next-generation -->
      <div class="proudly-sec" id="product">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-24 f-md-38 w700 lh140 white-clr intro-bg">
                     Introducing…
                  </div>
               </div>
               <div class="col-12 mt-md30 mt20 text-center">
                  <svg version="1.1" id="Layer_11" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 253.7 80" style="enable-background:new 0 0 253.7 80; max-height:180px;" xml:space="preserve">
                     ;
                     <style type="text/css">
                        .st00{fill:#FFD60D;}
                        .st11{fill:#FFFFFF;}
                        .st22{fill:#2337F6;}
                     </style>
                     <g>
                        <path class="st00" d="M130.2,64.2l-4.8,1.1c-0.4-0.9-1.1-1.6-1.9-2.2c-1-0.7-2.2-1.1-3.5-1.1c-1.7,0-3.3,0.7-4.4,1.9
                           c-1,1-1.6,2.4-1.7,3.9l-14.1,3.1c-1.1-2.5-3.6-4.3-6.5-4.3c-3.9,0-7.1,3.2-7.1,7.1v0.1l-1.8,0.4c-3.3,0.8-6.6-1.3-7.3-4.6l-10-43.9
                           c-0.8-3.3,1.3-6.6,4.6-7.3l9.5-2.1v0.6c0.1,1.4,1.3,2.5,2.7,2.5c0.1,0,0.1,0,0.2,0c1.5-0.1,2.6-1.3,2.5-2.8c0-0.5-0.2-0.9-0.4-1.3
                           l14.2-3.1v0.5c0,0.7,0.3,1.3,0.8,1.8c0.5,0.4,1.1,0.7,1.8,0.7c0.1,0,0.1,0,0.2,0c1.5-0.1,2.6-1.3,2.5-2.8c0-0.5-0.2-0.9-0.4-1.3
                           l12.4-2.7c3.3-0.8,6.6,1.3,7.3,4.6l2,8.8l1.1,5l6.8,30.2C135.7,60.3,133.6,63.6,130.2,64.2z"/>
                        <g>
                           <path class="st11" d="M34.4,21.8L20.7,48.3v15.1h-6.8V48.3L0,21.8h7.6l9.6,20.4l9.6-20.4C26.8,21.8,34.4,21.8,34.4,21.8z"/>
                           <path class="st11" d="M41.5,61.8c-2.5-1.4-4.5-3.4-5.9-6c-1.4-2.6-2.2-5.6-2.2-8.9c0-3.4,0.7-6.3,2.2-8.9c1.5-2.6,3.5-4.6,6-6
                              c2.6-1.4,5.4-2.1,8.6-2.1s6,0.7,8.6,2.1c2.6,1.4,4.6,3.4,6,6c1.5,2.6,2.2,5.6,2.2,8.9c0,3.4-0.8,6.3-2.3,8.9
                              c-1.5,2.6-3.6,4.6-6.2,6c-2.6,1.4-5.5,2.1-8.6,2.1C46.8,63.9,44,63.2,41.5,61.8z M54.9,56.7c1.5-0.8,2.8-2.1,3.7-3.8
                              c1-1.7,1.4-3.7,1.4-6.1c0-2.4-0.5-4.4-1.4-6.1c-0.9-1.7-2.1-2.9-3.7-3.7c-1.5-0.8-3.2-1.3-4.9-1.3c-1.8,0-3.4,0.4-4.9,1.3
                              c-1.5,0.8-2.7,2.1-3.6,3.7c-0.9,1.7-1.3,3.7-1.3,6.1c0,3.6,0.9,6.3,2.7,8.2c1.8,1.9,4.1,2.9,6.9,2.9C51.7,58,53.3,57.5,54.9,56.7z
                              "/>
                        </g>
                        <g>
                           <path class="st11" d="M167.1,50.3h-23.7c0.2,2.5,1.1,4.5,2.8,6c1.7,1.5,3.7,2.3,6.1,2.3c3.5,0,5.9-1.4,7.3-4.3h6.9
                              c-0.9,2.9-2.6,5.2-5.1,7c-2.5,1.8-5.5,2.7-9.1,2.7c-3,0-5.6-0.7-8-2c-2.3-1.3-4.2-3.2-5.5-5.6s-2-5.2-2-8.4c0-3.2,0.6-6,1.9-8.4
                              c1.3-2.4,3.1-4.3,5.5-5.6c2.3-1.3,5-2,8.1-2c2.9,0,5.5,0.6,7.8,1.9c2.3,1.3,4.1,3.1,5.3,5.4c1.3,2.3,1.9,5,1.9,8
                              C167.3,48.3,167.2,49.3,167.1,50.3z M160.6,45.1c0-2.4-0.9-4.3-2.5-5.7c-1.7-1.4-3.7-2.1-6.1-2.1c-2.2,0-4.1,0.7-5.7,2.1
                              c-1.6,1.4-2.5,3.3-2.8,5.7H160.6z"/>
                           <path class="st11" d="M180,21.8v41.6h-6.4V21.8H180z"/>
                           <path class="st11" d="M194.9,21.8v41.6h-6.4V21.8H194.9z"/>
                           <path class="st11" d="M231.5,50.3h-23.7c0.2,2.5,1.1,4.5,2.8,6c1.6,1.5,3.7,2.3,6.1,2.3c3.5,0,5.9-1.4,7.3-4.3h6.9
                              c-0.9,2.9-2.6,5.2-5.1,7c-2.5,1.8-5.5,2.7-9.1,2.7c-3,0-5.6-0.7-8-2c-2.3-1.3-4.2-3.2-5.5-5.6c-1.3-2.4-2-5.2-2-8.4
                              c0-3.2,0.6-6,1.9-8.4c1.3-2.4,3.1-4.3,5.5-5.6c2.3-1.3,5-2,8.1-2c2.9,0,5.5,0.6,7.8,1.9c2.3,1.3,4.1,3.1,5.3,5.4
                              c1.3,2.3,1.9,5,1.9,8C231.7,48.3,231.7,49.3,231.5,50.3z M225,45.1c0-2.4-0.9-4.3-2.5-5.7c-1.7-1.4-3.7-2.1-6.1-2.1
                              c-2.2,0-4.1,0.7-5.7,2.1c-1.6,1.4-2.5,3.3-2.8,5.7H225z"/>
                           <path class="st11" d="M248.2,33.2c1.6-0.9,3.4-1.3,5.5-1.3v6.6h-1.6c-2.5,0-4.4,0.6-5.7,1.9s-1.9,3.5-1.9,6.6v16.3H238v-31h6.4v4.5
                              C245.4,35.3,246.6,34.1,248.2,33.2z"/>
                        </g>
                        <circle class="st11" cx="93.4" cy="73.7" r="6.3"/>
                        <path class="st22" d="M131.6,51.7c0,5.2-3.4,9.7-8,11.4c-1-0.7-2.2-1.1-3.5-1.1c-1.7,0-3.3,0.7-4.4,1.9H78.9c0-3.9,3.1-7,7-7h33.3
                           c2.9,0,5.3-2.3,5.3-5.2s-2.3-5.3-5.3-5.3h-11.4c-3.4,0-6.5-1.4-8.8-3.7c-2.2-2.2-3.5-5.3-3.5-8.7c0.1-6.8,5.6-12.2,12.4-12.2h22.2
                           c0,1.9-0.8,3.7-2.1,5s-3,2-4.9,2H108c-2.9,0-5.3,2.3-5.3,5.2s2.3,5.3,5.3,5.3h11.4c3.4,0,6.5,1.4,8.8,3.7
                           C130.3,45.3,131.6,48.3,131.6,51.7z"/>
                        <path class="st22" d="M97.7,22.8c0.4-0.4,0.8-0.7,1.2-1H71.5l0,0c0,3.9,3.1,7,7,7h15.3C94.7,26.6,96,24.5,97.7,22.8z"/>
                        <path class="st22" d="M85.6,38.8h8.1c-0.5-1.6-0.7-3.2-0.7-4.9c0-0.7,0.1-1.4,0.2-2.1H78.6l0,0C78.6,35.7,81.7,38.8,85.6,38.8z"/>
                        <path class="st11" d="M104.3,11.2c-0.1-1-0.2-2.1-0.6-3.1C102,2.9,97-0.3,92,0c-0.5,0-1.1,0.1-1.6,0.2c-0.4,0.1-0.7,0.2-1.1,0.3
                           c-6,2-9.1,8.6-7.1,14.7c0.1,0.2,0.1,0.4,0.2,0.6c-0.2,0.3-0.2,0.7-0.2,1c0.1,1,0.9,1.8,1.9,1.7s1.8-0.9,1.7-1.9
                           c0-0.6-0.4-1.2-1-1.5c-0.1-0.2-0.2-0.4-0.2-0.6C83,9.6,85.5,4.5,90.1,3s9.6,1.1,11.2,5.9c0.3,0.8,0.4,1.6,0.5,2.3
                           c-0.4,0.4-0.6,0.9-0.5,1.4c0.1,1,0.9,1.8,1.9,1.7c1-0.1,1.8-0.9,1.7-1.9C104.8,12,104.6,11.5,104.3,11.2z"/>
                        <circle class="st11" cx="120.1" cy="68.1" r="5.2"/>
                     </g>
                  </svg>
               </div>
               <div class="col-12 f-md-38 f-22 mt-md30 mt20 w600 text-center white-clr lh140">
                  One of a Kind, All-In-One Platform to Market and Sell Any Type of Digital Products, Courses, S ervices, and Physical Goods with Zero Technical Hassle. 
               </div>
            </div>
            <div class="row mt30 mt-md50 align-items-center">
               <div class="col-12 col-md-6">
                  <img src="assets/images/product-image.webp" class="img-fluid d-block mx-auto img-animation">
               </div>
               <div class="col-md-6 col-12 mt-md0 mt20">
                  <div class="features-list">
                     <ul class="exciting-list f-18 w400 lh140 pl0 white-clr mt20">
                        <li><span class="f-24 purple-clr w600">All-In-One Business Platform</span><br>
                           Everything You Need in One Platform - From Memberships Site, Website, Cart & Payments, Landing Pages, Funnels, Email Follow Ups and Much More
                        </li>
                        <li><span class="f-24 purple-clr w600">Fast & Easy to Launch & Sell Online</span><br>
                           Effortlessly Get Max Conversions, Max Leads & Ultimately Max Sales with Zero Tech Hassles for your Products, Courses, eBooks or Services in Any Niche
                        </li>
                        <li><span class="f-24 purple-clr w600">Agency License to Serve Clients</span><br>
                           Launch & Sell Your Own Products or Charge Your Clients as an Agency & Keep 100% Profits
                        </li>
                        <li><span class="f-24 purple-clr w600">No Recurring Monthly Charges</span><br>
                           During This Launch Special Deal, Get All Benefits At Limited Low One-Time-Fee.
                        </li>
                        <li><span class="f-24 purple-clr w600">50+ More Exciting Features</span><br>
                           We’ve Left No Stone Unturned to Give You an Unmatched Experience
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- next-generation end -->
      <!-- FEATURE LIST SECTION START -->
      <div class="feature-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-md-45 f-28 w700 text-center">
                     <span class="yellow-bg">YoSeller is Packed with GROUND-BREAKING</span> <br class="d-none d-md-block">
                     Features That Make it a Cut Above the Rest
                  </div>
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md100">
               <div class="col-md-6 col-12">
                  <div class="f-md-32 f-22 w700 lh140">All-In-One Online Selling & Marketing Platform </div>
                  <div class="f-18 w400 lh150 mt15 grey-clr">
                     Gone are the days when you had to depend on multiple money-sucking service providers. 
                     YoSeller has everything you need to launch, sell & market your products, courses & services 
                     in one place. So, whatever you need- Create websites & stores for your products, integrating 
                     payment solutions, showcasing your services to a hungry audience, managing product 
                     delivery and much more, YoSeller has got your back covered. 
                  </div>
               </div>
               <div class="col-md-6 col-12 mt-md0 mt20">
                  <img src="assets/images/f1.png" class="img-fluid mx-auto d-block">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md100">
               <div class="col-md-6 col-12 order-md-2">
                  <div class="f-md-32 f-22 w700 lh140">Create and Manage Unlimited Products</div>
                  <div class="f-18 w400 lh150 mt15 grey-clr">
                     YoSeller gives you the power to remove all restrictions & grow unlimited. Yes, you’ve got 
                     the complete power to boost your sales & profits by selling Unlimited digital products, 
                     physical goods & services to hungry clients, and getting them connected to your offers 
                     for extended durations. 
                  </div>
               </div>
               <div class="col-md-6 col-12 mt-md0 mt20 order-md-1">
                  <img src="assets/images/f2.png" class="img-fluid mx-auto d-block">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md100">
               <div class="col-md-6 col-12">
                  <div class="f-md-32 f-22 w700 lh140">Create Stunning Websites/Stores in Any Niche</div>
                  <div class="f-18 w400 lh150 mt15 grey-clr">
                     Creating super engaging websites & stores has never been easier. Once you know the direction 
                     for your own business or that of your client, simply choose a template and start creating a site 
                     in only minutes. YoSeller helps you to create high-converting websites that are crafted to match 
                     the needs of specific audiences & get them hooked to your offers.
                  </div>
               </div>
               <div class="col-md-6 col-12 mt-md0 mt20">
                  <img src="assets/images/f3.png" class="img-fluid mx-auto d-block">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md100">
               <div class="col-md-6 col-12 order-md-2">
                  <div class="f-md-32 f-22 w700 lh140">Create Beautiful Membership Sites to Deliver Products</div>
                  <div class="f-18 w400 lh150 mt15 grey-clr">
                     Want to create a private area to deliver products for your clients, demonstrate your knowledge 
                     on a certain topic, create a new course, or share your passion and interests with your audience 
                     base, there’s nothing better than creating a membership site. YoSeller gives you everything you 
                     need to create membership sites that help you to attain your business objectives with ZERO 
                     third-party dependency. 
                  </div>
               </div>
               <div class="col-md-6 col-12 mt-md0 mt20 order-md-1">
                  <img src="assets/images/f4.png" class="img-fluid mx-auto d-block">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md100">
               <div class="col-md-6 col-12">
                  <div class="f-md-32 f-22 w700 lh140">Sell Unlimited Products & Accept Payments Directly in Your Account </div>
                  <div class="f-18 w400 lh150 mt15 grey-clr">
                     Along with giving you the power to sell unlimited products, YoSeller enables you to take complete 
                     control your online business by easily accepting payments from PayPal & Stripe from your audience 
                     scattered worldwide. Or you can also integrate 3rd party marketplace like JVZoo, WarriorPlus, Clickbank, etc.
                  </div>
               </div>
               <div class="col-md-6 col-12 mt-md0 mt20">
                  <img src="assets/images/f5.png" class="img-fluid mx-auto d-block">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md100">
               <div class="col-md-6 col-12 order-md-2">
                  <div class="f-md-32 f-22 w700 lh140">Get 40+ RED HOT, Done-For-You Products to Quick Start</div>
                  <div class="f-18 w400 lh150 mt15 grey-clr">
                     We’ve gone a step ahead in order to make online marketing easier for you. So, in order to help you 
                     not get confused while choosing from hundreds of daily updating products, we’re providing you 40+ 
                     carefully chosen high-converting products that will help you to maximize sales.
                  </div>
               </div>
               <div class="col-md-6 col-12 mt-md0 mt20 order-md-1">
                  <img src="assets/images/f6.png" class="img-fluid mx-auto d-block">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md100">
               <div class="col-md-6 col-12">
                  <div class="f-md-32 f-22 w700 lh140">250+ Battle-Tested DFY Templates </div>
                  <div class="f-18 w400 lh150 mt15 grey-clr">
                     You get Mobile optimized and elegant templates for almost all types of marketing campaigns 
                     to get everything MAX – max attention, max leads, max sales, and more importantly max profits. 
                  </div>
               </div>
               <div class="col-md-6 col-12 mt-md0 mt20">
                  <img src="assets/images/f7.png" class="img-fluid mx-auto d-block">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md100">
               <div class="col-md-6 col-12 order-md-2">
                  <div class="f-md-32 f-22 w700 lh140">Fully Drag and Drop WYSIWYG Page Editor that Requires Zero Tech Skills</div>
                  <div class="f-18 w400 lh150 mt15 grey-clr">
                     A Next generation, pixel-perfect, drag & drop editor to create whatever you want on the page 
                     and wherever you want without even 1 Pixel error. We have reinvented page editor which is 
                     not like old school bootstrap editor that set your elements without your control.
                  </div>
               </div>
               <div class="col-md-6 col-12 mt-md0 mt20 order-md-1">
                  <img src="assets/images/f8.png" class="img-fluid mx-auto d-block">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md100">
               <div class="col-md-6 col-12">
                  <div class="f-md-32 f-22 w700 lh140">
                     Inbuilt Lead Generation & Management System for Effective Management & Automation
                  </div>
                  <div class="f-18 w400 lh150 mt15 grey-clr">
                     Tap into ready use & beautiful Lead forms in 8 different colors. Use these forms to collect maximum leads 
                     and keep them organized in your very own lead management panel.
                  </div>
               </div>
               <div class="col-md-6 col-12 mt-md0 mt20">
                  <img src="assets/images/f9.png" class="img-fluid mx-auto d-block">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md100">
               <div class="col-md-6 col-12 order-md-2">
                  <div class="f-md-32 f-22 w700 lh140">Precise Analytics to Measure the Performance </div>
                  <div class="f-18 w400 lh150 mt15 grey-clr">
                     Know your numbers – what’s performing well and what simply is not working. 
                     Check out unique visitors, and conversions stats in nice graphs on the analytics 
                     page to build strategy & improve your marketing campaigns to get more Returns 
                     and Profits.
                  </div>
               </div>
               <div class="col-md-6 col-12 mt-md0 mt20 order-md-1">
                  <img src="assets/images/f10.png" class="img-fluid mx-auto d-block">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md100">
               <div class="col-md-6 col-12">
                  <div class="f-md-32 f-22 w700 lh140">
                     Connect YoSeller with Other Useful 20+ Tools with Advanced Integrations
                  </div>
                  <div class="f-18 w400 lh150 mt15 grey-clr">
                     Setup integration in a few clicks and send all your leads to your favorite autoresponders 
                     for prompt communication. Directly register potential customers for your next webinar with 
                     integration to Webinar platforms.  
                  </div>
               </div>
               <div class="col-md-6 col-12 mt-md0 mt20">
                  <img src="assets/images/f11.png" class="img-fluid mx-auto d-block">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md100">
               <div class="col-md-6 col-12 order-md-2">
                  <div class="f-md-32 f-22 w700 lh140">Send Unlimited Emails & Set Follow-up Emails Journey Build Customer Relations </div>
                  <div class="f-18 w400 lh150 mt15 grey-clr">
                     YoSeller gives you complete freedom to send and schedule unlimited emails to unlimited subscribers 
                     in a hassle-free manner. Also, you will be able to design your subscriber’s journey visually, and you’ll 
                     always know exactly how your email campaigns are set up to perform for you.
                  </div>
               </div>
               <div class="col-md-6 col-12 mt-md0 mt20 order-md-1">
                  <img src="assets/images/f12.png" class="img-fluid mx-auto d-block">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md100">
               <div class="col-md-6 col-12">
                  <div class="f-md-32 f-22 w700 lh140 ">
                     Business Drive to Store Your Media Securely & Share Faster with Your Clients 
                  </div>
                  <div class="f-18 w400 lh150 mt15 grey-clr">
                     Store Unlimited Files like images, videos, audio, and documents with YoSeller business drive. Quick 
                     delivery is the most important prerequisite for every successful marketer today. With our fast CDNs, 
                     sharing your files, documents, videos, etc. becomes quicker, faster, and easier with low latency 
                     and high transfer speeds.
                  </div>
               </div>
               <div class="col-md-6 col-12 mt-md0 mt20">
                  <img src="assets/images/f13.png" class="img-fluid mx-auto d-block">
               </div>
            </div>
         </div>
      </div>
      <div class="feature-list">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-45 f-28 w700 lh140 text-center white-clr">
                  YoSeller is Packed with GROUND-BREAKING
                  Features That Make It A Cut Above the Rest
               </div>
            </div>
            <div class="row rows-cols-2 row-cols-md-4 gap30 mt0 mt-md30">
               <div class="col">
                  <div class="feature-list-box">
                     <img src="assets/images/fr1.webp" class="img-fluid mx-auto d-block">
                     <p class="description">Create Unlimited Businesses/Domains
                     </p>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box">
                     <img src="assets/images/fr2.webp" class="img-fluid mx-auto d-block">
                     <p class="description">Custom Domains</span> 
                     </p>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box">
                     <img src="assets/images/fr3.webp" class="img-fluid mx-auto d-block">
                     <p class="description">A/B Testing</p>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box">
                     <img src="assets/images/fr4.webp" class="img-fluid mx-auto d-block">
                     <p class="description">Single Dashboard to Manage All Businesses & Clients</p>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box">
                     <img src="assets/images/fr5.webp" class="img-fluid mx-auto d-block">
                     <p class="description">
                        Store, Manage & Share Unlimited Files
                     </p>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box">
                     <img src="assets/images/fr6.webp" class="img-fluid mx-auto d-block">
                     <p class="description">1 Million+ Royalty Free Stock Photos and Videos</p>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box">
                     <img src="assets/images/fr7.webp" class="img-fluid mx-auto d-block">
                     <p class="description">128-bit SSL Encryption Security
                     </p>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box">
                     <img src="assets/images/fr8.webp" class="img-fluid mx-auto d-block">
                     <p class="description">100% Cloud Based - No Hosting/Installation Required</p>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box">
                     <img src="assets/images/fr9.webp" class="img-fluid mx-auto d-block">
                     <p class="description">100% GDPR and CAN-SPAM Compliant</p>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box">
                     <img src="assets/images/fr10.webp" class="img-fluid mx-auto d-block">
                     <p class="description">Inbuilt SEO Management for Website and Landing Pages</p>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box">
                     <img src="assets/images/fr11.webp" class="img-fluid mx-auto d-block">
                     <p class="description">Easy and Intuitive to Use Software</p>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box">
                     <img src="assets/images/fr12.webp" class="img-fluid mx-auto d-block">
                     <p class="description">Complete Step-by-Step Training Included</p>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box">
                     <img src="assets/images/fr13.webp" class="img-fluid mx-auto d-block">
                     <p class="description">No Coding, Design or Technical Skills Required</p>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box">
                     <img src="assets/images/fr14.webp" class="img-fluid mx-auto d-block">
                     <p class="description">Regular Updates</p>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box">
                     <img src="assets/images/fr15.webp" class="img-fluid mx-auto d-block">
                     <p class="description">And Much More</p>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- DEMO SECTION START -->
      <div class="demo-sec">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-md-50 f-28 w700 lh140 text-center">
                     <span class="blue-clr">Watch The Demo To </span>
                     <br class="d-none d-md-block">
                     Discover How Easy & Powerful It Is
                  </div>
               </div>
               <div class="col-12 col-md-8 mx-auto mt-md40 mt20">
                  <!-- <img src="assets/images/demo-video-poster.png" class="img-fluid d-block mx-auto"> -->
                  <div style="padding-bottom: 56.25%;position: relative;"><iframe src="https://yoseller.dotcompal.com/video/mydrive/182094" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                     box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div> 
               </div>
            </div>
         </div>
      </div>
      <!-- DEMO SECTION END -->
      <div class="deep-funnel" id="funnel">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-45 f-28 w700 text-center lh140">
                  Our Deep & High Converting Sales Funnel
               </div>
               <div class="col-12 col-md-12 mt20 mt-md70">
                  <div>
                     <img src="assets/images/funnel.webp" class="img-fluid d-block mx-auto">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="prize-value">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-50 f-28 w600 text-center white-clr lh140">
                  Get Ready to Grab Your Share of
               </div>
               <div class="col-12 f-md-72 f-40 w800 text-center yellow-clr lh140">
                  $12000 JV Prizes
               </div>
            </div>
         </div>
      </div>
      <div class="contest-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-45 f-28 w700 text-center black-clr">
                  Pre-Launch Lead Contest
               </div>
               <div class="col-12 f-md-18 f-18 w400 lh140 mt20 text-center">
                  Contest Starts on 9th October’22 at 10:00 AM EST and Ends on 11th October’22 at 10:00 AM EST
               </div>
               <div class="col-12 f-md-18 f-18 w400 lh140 mt10 text-center">
                  (Get Flat <span class="w700"> $0.50c </span> For Every Lead You Send for <span class="w700"> Pre-Launch Webinar)</span>
               </div>
               <div class="col-12 mt20 mt-md50">
                  <div class="row align-items-center">
                     <div class="col-12 col-md-5">
                        <img src="assets/images/happy-man.webp" class="img-fluid d-none d-md-block mx-auto" alt="Contest">
                     </div>
                     <div class="col-12 col-md-7">
                        <img src="assets/images/contest-img2.webp" class="img-fluid d-block mx-auto" alt="Contest">
                     </div>
                  </div>
               </div>
               <div class="col-12 f-18 f-md-18 lh140 mt-md40 mt20 w400 text-center black-clr">
                  *(Eligibility – All leads should have at least 1% conversion on launch and should have min 100 leads)
               </div>
            </div>
         </div>
      </div>
      <div class="prize-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-md-65 f-28 lh140 w800 white-clr prize-shape">
                     $10,000 launch contest
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md70">
               <div class="col-12 col-md-8 mx-auto">
                  <div class="row align-items-center">
                     <div class="col-md-6">
                        <img src="assets/images/prize-img1.webp" alt="Prize Image" class="mx-auto d-block img-fluid">
                     </div>
                     <div class="col-md-6 position-relative">
                        <img src="assets/images/prize-img2.webp" alt="Prize Image" class="mx-auto d-block img-fluid">
                        <img src="assets/images/prize-img3.webp" alt="Prize Image" class="mx-auto d-none d-md-block img-fluid" style="position:absolute; bottom: -90px; right: -330px;">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="reciprocate-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-md-45 f-28 lh140 w700 black-clr">
                     Our Solid Track Record of <br class="d-md-block d-none"> <span class="blue-clr"> Launching Top Converting Products</span>
                  </div>
               </div>
               <div class="col-12 col-md-12 mt20">
                  <img src="assets/images/product-logo.png" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 text-center f-md-45 f-28 lh140 w700 white-clr mt20 mt-md70 blue-clr">
                  Do We Reciprocate?
               </div>
               <div class="col-12 f-18 f-md-18 lh140 mt20 w400 text-center black-clr">
                  We've been in top positions on various launch leaderboards & sent huge sales for our valued JVs.<br><br>  
                  So, if you have a top-notch product with top conversions that fits our list, we would love to drive loads of sales for you. Here are just some results from our recent promotions. 
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
                  <div class="logos-effect">
                     <img src="assets/images/logos.png" class="img-fluid d-block mx-auto">
                  </div>
               </div>
               <div class="col-12 f-md-28 f-24 lh140 mt20 mt-md50 w600 text-center">
                  And The List Goes On And On...
               </div>
            </div>
         </div>
      </div>
      <div class="contact-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="contact-block">
                     <div class="row gx-md-5">
                        <div class="col-12 mb20 mb-md50">
                           <div class="f-md-45 f-28 w700 lh140 text-center white-clr">
                              Have any Query? Contact us Anytime
                           </div>
                        </div>
                        <div class="col-md-4 col-12 text-center">
                           <div class="contact-shape">
                              <img src="assets/images/amit-pareek-sir.webp" class="img-fluid d-block mx-auto minus">
                              <div class="f-24 f-md-30 w600  lh140 text-center white-clr mt20">
                                 Dr Amit Pareek
                              </div>
                              <div class="f-14 w400 lh140 text-center white-clr">
                                 (Techpreneur & Marketer)
                              </div>
                              <div class="col-12 mt30 d-flex justify-content-center">
                                 <a href="http://facebook.com/Dr.AmitPareek" class="link-text mr20">
                                 <img src="https://cdn.oppyo.com/launches/webpull/jv/am-fb.webp" class="center-block">
                                 </a>
                                 <a href="skype:amit.pareek77" class="link-text">
                                    <div class="col-12 ">
                                       <img src="https://cdn.oppyo.com/launches/webpull/jv/am-skype.webp" class="center-block">
                                    </div>
                                 </a>
                              </div>
                           </div>
                        </div>
                        <div class="col-md-4 col-12 text-center mt20 mt-md0">
                           <div class="contact-shape">
                              <img src="assets/images/atul-parrek-sir.webp" class="img-fluid d-block mx-auto minus">
                              <div class="f-24 f-md-30 w600  lh140 text-center white-clr mt20">
                                 Atul Pareek
                              </div>
                              <div class="f-14 w400 lh140 text-center white-clr">
                                 (Entrepreneur & Product Creator)
                              </div>
                              <div class="col-12 mt30 d-flex justify-content-center">
                                 <a href="https://www.facebook.com/profile.php?id=100076904623319" class="link-text mr20">
                                 <img src="https://cdn.oppyo.com/launches/webpull/jv/am-fb.webp" class="center-block">
                                 </a>
                                 <a href="skype:live:.cid.c3d2302c4a2815e0" class="link-text">
                                    <div class="col-12 ">
                                       <img src="https://cdn.oppyo.com/launches/webpull/jv/am-skype.webp" class="center-block">
                                    </div>
                                 </a>
                              </div>
                           </div>
                        </div>
                        <!-- <div class="col-md-4 col-12 text-center mt20 mt-md0">
                           <div class="contact-shape">
                              <img src="assets/images/achal-goswami-sir.webp" class="img-fluid d-block mx-auto minus">
                              <div class="f-24 f-md-30 w600  lh140 text-center white-clr mt20">
                                 Achal Goswami
                              </div>
                              <div class="f-14 w400 lh140 text-center white-clr">
                                 (Entrepreneur & Internet Marketer)
                              </div>
                              <div class="col-12 mt30 d-flex justify-content-center">
                                 <a href="https://www.facebook.com/dcp.ambassador.achal/" class="link-text mr20">
                                 <img src="https://cdn.oppyo.com/launches/webpull/jv/am-fb.webp" class="center-block">
                                 </a>
                                 <a href="skype:live:.cid.78f368e20e6d5afa" class="link-text">
                                    <div class="col-12 ">
                                       <img src="https://cdn.oppyo.com/launches/webpull/jv/am-skype.webp" class="center-block">
                                    </div>
                                 </a>
                              </div>
                           </div>
                        </div> -->
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="terms-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-50 f-28 w700 lh140 text-center">
                  Affiliate Promotions Terms & Conditions
               </div>
               <div class="col-md-12 col-12 f-18 lh140 p-md0 mt-md20 mt20 w400 text-center">
                  We request you to read this section carefully before requesting for your affiliate link and being a part of this launch. Once you are approved to be a part of this launch and promote this product, you must abide by the instructions listed below: 
               </div>
               <div class="col-md-12 col-12 px-md-0 mt15 mt-md50">
                  <ul class="terms-list m0 f-16 lh140 w400">
                     <li>
                        Please be sure to NOT send spam of any kind. This includes cheap traffic methods in any way whatsoever. In case anyone does so, they will be banned from all future promotion with us without any reason whatsoever. No exceptions will be entertained.
                     </li>
                     <li>
                        Please ensure you or your members DON’T use negative words such as 'scam' in any of your promotional campaigns in any way. If this comes to our knowledge that you have violated it, your affiliate account will be removed from our system with immediate effect.
                     </li>
                     <li>
                        Please make it a point to not OFFER any cash incentives (such as rebates), cash backs etc or any such malpractices to people buying through your affiliate link.
                     </li>
                     <li>
                        Please note that you are not AUTHORIZED to create social media pages or run any such campaigns that may be detrimental using our product or brand name in any way possible. Doing this may result in serious consequences.
                     </li>
                     <li>
                        We reserve the right to TERMINATE any affiliate with immediate effect if they’re found to breach any/all of the conditions mentioned above in any manner.
                     </li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!--Footer Section Start -->
      <div class="footer-section">
         <div class="container ">
            <div class="row">
               <div class="col-12 text-center">
                  <svg version="1.1" id="Layer_11" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 253.7 80" style="enable-background:new 0 0 253.7 80; max-height:60px;" xml:space="preserve">
                     ;
                     <style type="text/css">
                        .st00{fill:#FFD60D;}
                        .st11{fill:#FFFFFF;}
                        .st22{fill:#2337F6;}
                     </style>
                     <g>
                        <path class="st00" d="M130.2,64.2l-4.8,1.1c-0.4-0.9-1.1-1.6-1.9-2.2c-1-0.7-2.2-1.1-3.5-1.1c-1.7,0-3.3,0.7-4.4,1.9
                           c-1,1-1.6,2.4-1.7,3.9l-14.1,3.1c-1.1-2.5-3.6-4.3-6.5-4.3c-3.9,0-7.1,3.2-7.1,7.1v0.1l-1.8,0.4c-3.3,0.8-6.6-1.3-7.3-4.6l-10-43.9
                           c-0.8-3.3,1.3-6.6,4.6-7.3l9.5-2.1v0.6c0.1,1.4,1.3,2.5,2.7,2.5c0.1,0,0.1,0,0.2,0c1.5-0.1,2.6-1.3,2.5-2.8c0-0.5-0.2-0.9-0.4-1.3
                           l14.2-3.1v0.5c0,0.7,0.3,1.3,0.8,1.8c0.5,0.4,1.1,0.7,1.8,0.7c0.1,0,0.1,0,0.2,0c1.5-0.1,2.6-1.3,2.5-2.8c0-0.5-0.2-0.9-0.4-1.3
                           l12.4-2.7c3.3-0.8,6.6,1.3,7.3,4.6l2,8.8l1.1,5l6.8,30.2C135.7,60.3,133.6,63.6,130.2,64.2z"/>
                        <g>
                           <path class="st11" d="M34.4,21.8L20.7,48.3v15.1h-6.8V48.3L0,21.8h7.6l9.6,20.4l9.6-20.4C26.8,21.8,34.4,21.8,34.4,21.8z"/>
                           <path class="st11" d="M41.5,61.8c-2.5-1.4-4.5-3.4-5.9-6c-1.4-2.6-2.2-5.6-2.2-8.9c0-3.4,0.7-6.3,2.2-8.9c1.5-2.6,3.5-4.6,6-6
                              c2.6-1.4,5.4-2.1,8.6-2.1s6,0.7,8.6,2.1c2.6,1.4,4.6,3.4,6,6c1.5,2.6,2.2,5.6,2.2,8.9c0,3.4-0.8,6.3-2.3,8.9
                              c-1.5,2.6-3.6,4.6-6.2,6c-2.6,1.4-5.5,2.1-8.6,2.1C46.8,63.9,44,63.2,41.5,61.8z M54.9,56.7c1.5-0.8,2.8-2.1,3.7-3.8
                              c1-1.7,1.4-3.7,1.4-6.1c0-2.4-0.5-4.4-1.4-6.1c-0.9-1.7-2.1-2.9-3.7-3.7c-1.5-0.8-3.2-1.3-4.9-1.3c-1.8,0-3.4,0.4-4.9,1.3
                              c-1.5,0.8-2.7,2.1-3.6,3.7c-0.9,1.7-1.3,3.7-1.3,6.1c0,3.6,0.9,6.3,2.7,8.2c1.8,1.9,4.1,2.9,6.9,2.9C51.7,58,53.3,57.5,54.9,56.7z
                              "/>
                        </g>
                        <g>
                           <path class="st11" d="M167.1,50.3h-23.7c0.2,2.5,1.1,4.5,2.8,6c1.7,1.5,3.7,2.3,6.1,2.3c3.5,0,5.9-1.4,7.3-4.3h6.9
                              c-0.9,2.9-2.6,5.2-5.1,7c-2.5,1.8-5.5,2.7-9.1,2.7c-3,0-5.6-0.7-8-2c-2.3-1.3-4.2-3.2-5.5-5.6s-2-5.2-2-8.4c0-3.2,0.6-6,1.9-8.4
                              c1.3-2.4,3.1-4.3,5.5-5.6c2.3-1.3,5-2,8.1-2c2.9,0,5.5,0.6,7.8,1.9c2.3,1.3,4.1,3.1,5.3,5.4c1.3,2.3,1.9,5,1.9,8
                              C167.3,48.3,167.2,49.3,167.1,50.3z M160.6,45.1c0-2.4-0.9-4.3-2.5-5.7c-1.7-1.4-3.7-2.1-6.1-2.1c-2.2,0-4.1,0.7-5.7,2.1
                              c-1.6,1.4-2.5,3.3-2.8,5.7H160.6z"/>
                           <path class="st11" d="M180,21.8v41.6h-6.4V21.8H180z"/>
                           <path class="st11" d="M194.9,21.8v41.6h-6.4V21.8H194.9z"/>
                           <path class="st11" d="M231.5,50.3h-23.7c0.2,2.5,1.1,4.5,2.8,6c1.6,1.5,3.7,2.3,6.1,2.3c3.5,0,5.9-1.4,7.3-4.3h6.9
                              c-0.9,2.9-2.6,5.2-5.1,7c-2.5,1.8-5.5,2.7-9.1,2.7c-3,0-5.6-0.7-8-2c-2.3-1.3-4.2-3.2-5.5-5.6c-1.3-2.4-2-5.2-2-8.4
                              c0-3.2,0.6-6,1.9-8.4c1.3-2.4,3.1-4.3,5.5-5.6c2.3-1.3,5-2,8.1-2c2.9,0,5.5,0.6,7.8,1.9c2.3,1.3,4.1,3.1,5.3,5.4
                              c1.3,2.3,1.9,5,1.9,8C231.7,48.3,231.7,49.3,231.5,50.3z M225,45.1c0-2.4-0.9-4.3-2.5-5.7c-1.7-1.4-3.7-2.1-6.1-2.1
                              c-2.2,0-4.1,0.7-5.7,2.1c-1.6,1.4-2.5,3.3-2.8,5.7H225z"/>
                           <path class="st11" d="M248.2,33.2c1.6-0.9,3.4-1.3,5.5-1.3v6.6h-1.6c-2.5,0-4.4,0.6-5.7,1.9s-1.9,3.5-1.9,6.6v16.3H238v-31h6.4v4.5
                              C245.4,35.3,246.6,34.1,248.2,33.2z"/>
                        </g>
                        <circle class="st11" cx="93.4" cy="73.7" r="6.3"/>
                        <path class="st22" d="M131.6,51.7c0,5.2-3.4,9.7-8,11.4c-1-0.7-2.2-1.1-3.5-1.1c-1.7,0-3.3,0.7-4.4,1.9H78.9c0-3.9,3.1-7,7-7h33.3
                           c2.9,0,5.3-2.3,5.3-5.2s-2.3-5.3-5.3-5.3h-11.4c-3.4,0-6.5-1.4-8.8-3.7c-2.2-2.2-3.5-5.3-3.5-8.7c0.1-6.8,5.6-12.2,12.4-12.2h22.2
                           c0,1.9-0.8,3.7-2.1,5s-3,2-4.9,2H108c-2.9,0-5.3,2.3-5.3,5.2s2.3,5.3,5.3,5.3h11.4c3.4,0,6.5,1.4,8.8,3.7
                           C130.3,45.3,131.6,48.3,131.6,51.7z"/>
                        <path class="st22" d="M97.7,22.8c0.4-0.4,0.8-0.7,1.2-1H71.5l0,0c0,3.9,3.1,7,7,7h15.3C94.7,26.6,96,24.5,97.7,22.8z"/>
                        <path class="st22" d="M85.6,38.8h8.1c-0.5-1.6-0.7-3.2-0.7-4.9c0-0.7,0.1-1.4,0.2-2.1H78.6l0,0C78.6,35.7,81.7,38.8,85.6,38.8z"/>
                        <path class="st11" d="M104.3,11.2c-0.1-1-0.2-2.1-0.6-3.1C102,2.9,97-0.3,92,0c-0.5,0-1.1,0.1-1.6,0.2c-0.4,0.1-0.7,0.2-1.1,0.3
                           c-6,2-9.1,8.6-7.1,14.7c0.1,0.2,0.1,0.4,0.2,0.6c-0.2,0.3-0.2,0.7-0.2,1c0.1,1,0.9,1.8,1.9,1.7s1.8-0.9,1.7-1.9
                           c0-0.6-0.4-1.2-1-1.5c-0.1-0.2-0.2-0.4-0.2-0.6C83,9.6,85.5,4.5,90.1,3s9.6,1.1,11.2,5.9c0.3,0.8,0.4,1.6,0.5,2.3
                           c-0.4,0.4-0.6,0.9-0.5,1.4c0.1,1,0.9,1.8,1.9,1.7c1-0.1,1.8-0.9,1.7-1.9C104.8,12,104.6,11.5,104.3,11.2z"/>
                        <circle class="st11" cx="120.1" cy="68.1" r="5.2"/>
                     </g>
                  </svg>
                  <div class="f-14 f-md-16 w400 mt20 lh150 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-14 f-md-16 w400 lh150 white-clr text-xs-center">Copyright © YoSeller 2022</div>
                  <ul class="footer-ul w400 f-14 f-md-16 white-clr text-center text-md-right">
                     <li><a href="https://support.oppyo.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://yoseller.co/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://yoseller.co/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://yoseller.co/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://yoseller.co/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://yoseller.co/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://yoseller.co/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!--Footer Section End -->
      <!-- timer --->
      <?php
         if ($now < $exp_date) {
         
         ?>
      <script type="text/javascript">
         // Count down milliseconds = server_end - server_now = client_end - client_now
         var server_end = <?php echo $exp_date; ?> * 1000;
         var server_now = <?php echo time(); ?> * 1000;
         var client_now = new Date().getTime();
         var end = server_end - server_now + client_now; // this is the real end time
         
         var noob = $('.countdown').length;
         
         var _second = 1000;
         var _minute = _second * 60;
         var _hour = _minute * 60;
         var _day = _hour * 24
         var timer;
         
         function showRemaining() {
             var now = new Date();
             var distance = end - now;
             if (distance < 0) {
                 clearInterval(timer);
                 document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
                 return;
             }
         
             var days = Math.floor(distance / _day);
             var hours = Math.floor((distance % _day) / _hour);
             var minutes = Math.floor((distance % _hour) / _minute);
             var seconds = Math.floor((distance % _minute) / _second);
             if (days < 10) {
                 days = "0" + days;
             }
             if (hours < 10) {
                 hours = "0" + hours;
             }
             if (minutes < 10) {
                 minutes = "0" + minutes;
             }
             if (seconds < 10) {
                 seconds = "0" + seconds;
             }
             var i;
             var countdown = document.getElementsByClassName('countdown');
             for (i = 0; i < noob; i++) {
                 countdown[i].innerHTML = '';
         
                 if (days) {
                     countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-30 f-md-40 timerbg">' + days + '</span><br><span class="f-16 w500">Days</span> </div>';
                 }
         
                 countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-30 f-md-40 timerbg">' + hours + '</span><br><span class="f-16 w500">Hours</span> </div>';
         
                 countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-30 f-md-40 timerbg">' + minutes + '</span><br><span class="f-16 w500">Mins</span> </div>';
         
                 countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-30 f-md-40 timerbg">' + seconds + '</span><br><span class="f-16 w500">Sec</span> </div>';
             }
         
         }
         timer = setInterval(showRemaining, 1000);
      </script>    
      <?php
         } else {
         echo "Times Up";
         }
         ?>
      <script type="text/javascript">
         // Special handling for in-app browsers that don't always support new windows
         (function() {
             function browserSupportsNewWindows(userAgent) {
                 var rules = [
                     'FBIOS',
                     'Twitter for iPhone',
                     'WebView',
                     '(iPhone|iPod|iPad)(?!.*Safari\/)',
                     'Android.*(wv|\.0\.0\.0)'
                 ];
                 var pattern = new RegExp('(' + rules.join('|') + ')', 'ig');
                 return !pattern.test(userAgent);
             }
         
             if (!browserSupportsNewWindows(navigator.userAgent || navigator.vendor || window.opera)) {
                 document.getElementById('af-form-318212638').parentElement.removeAttribute('target');
             }
         })();
      </script><script type="text/javascript">
         (function() {
             var IE = /*@cc_on!@*/false;
             if (!IE) { return; }
             if (document.compatMode && document.compatMode == 'BackCompat') {
                 if (document.getElementById("af-form-318212638")) {
                     document.getElementById("af-form-318212638").className = 'af-form af-quirksMode';
                 }
                 if (document.getElementById("af-body-318212638")) {
                     document.getElementById("af-body-318212638").className = "af-body inline af-quirksMode";
                 }
                 if (document.getElementById("af-header-318212638")) {
                     document.getElementById("af-header-318212638").className = "af-header af-quirksMode";
                 }
                 if (document.getElementById("af-footer-318212638")) {
                     document.getElementById("af-footer-318212638").className = "af-footer af-quirksMode";
                 }
             }
         })();
      </script>
      <!-- /AWeber Web Form Generator 3.0.1 -->
   </body>
</html>
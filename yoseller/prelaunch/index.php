<!Doctype html>
<html>

<head>
   <title>YoSeller Prelaunch</title>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=9">
   <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
   <meta name="title" content="YoSeller | Prelaunch">
   <meta name="description" content="Discover How to Launch & Sales Digital Products, Courses, 
Services & Goods Online to Make All-Time Big Profit! ">
   <meta name="keywords" content="YoSeller">
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   <meta property="og:image" content="https://www.yoseller.co/prelaunch/thumbnail.png">
   <meta name="language" content="English">
   <meta name="revisit-after" content="1 days">
   <meta name="author" content="Dr. Amit Pareek">
   <!-- Open Graph / Facebook -->
   <meta property="og:type" content="website">
   <meta property="og:title" content="YoSeller | Prelaunch">
   <meta property="og:description" content="Discover How to Launch & Sales Digital Products, Courses, 
Services & Goods Online to Make All-Time Big Profit! ">
   <meta property="og:image" content="https://www.yoseller.co/prelaunch/thumbnail.png">
   <!-- Twitter -->
   <meta property="twitter:card" content="summary_large_image">
   <meta property="twitter:title" content="YoSeller | Prelaunch">
   <meta property="twitter:description" content="Discover How to Launch & Sales Digital Products, Courses, 
Services & Goods Online to Make All-Time Big Profit! ">
   <meta property="twitter:image" content="https://www.yoseller.co/prelaunch/thumbnail.png">
   <link rel="icon" href="https://cdn.oppyo.com/launches/yoseller/common_assets/images/favicon.png" type="image/png">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    <!-- Start Editor required -->
    <link rel="stylesheet" href="https://cdn.oppyo.com/launches/yoseller/common_assets/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="assets/css/style.css" type="text/css">
      <link rel="stylesheet" href="assets/css/aweberform.css" type="text/css">
   
</head>

<body>
   
   <!-- Header Section Start -->
   <div class="header-section">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="row align-items-center">
                  <div class="col-md-3 text-center text-md-start">
                  <svg version="1.1" id="Layer_11" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 253.7 80" style="enable-background:new 0 0 253.7 80; max-height:60px;" xml:space="preserve">;
<style type="text/css">
	.st00{fill:#FFD60D;}
	.st11{fill:#FFFFFF;}
	.st22{fill:#2337F6;}
</style>
<g>
	<path class="st00" d="M130.2,64.2l-4.8,1.1c-0.4-0.9-1.1-1.6-1.9-2.2c-1-0.7-2.2-1.1-3.5-1.1c-1.7,0-3.3,0.7-4.4,1.9
		c-1,1-1.6,2.4-1.7,3.9l-14.1,3.1c-1.1-2.5-3.6-4.3-6.5-4.3c-3.9,0-7.1,3.2-7.1,7.1v0.1l-1.8,0.4c-3.3,0.8-6.6-1.3-7.3-4.6l-10-43.9
		c-0.8-3.3,1.3-6.6,4.6-7.3l9.5-2.1v0.6c0.1,1.4,1.3,2.5,2.7,2.5c0.1,0,0.1,0,0.2,0c1.5-0.1,2.6-1.3,2.5-2.8c0-0.5-0.2-0.9-0.4-1.3
		l14.2-3.1v0.5c0,0.7,0.3,1.3,0.8,1.8c0.5,0.4,1.1,0.7,1.8,0.7c0.1,0,0.1,0,0.2,0c1.5-0.1,2.6-1.3,2.5-2.8c0-0.5-0.2-0.9-0.4-1.3
		l12.4-2.7c3.3-0.8,6.6,1.3,7.3,4.6l2,8.8l1.1,5l6.8,30.2C135.7,60.3,133.6,63.6,130.2,64.2z"/>
	<g>
		<path class="st11" d="M34.4,21.8L20.7,48.3v15.1h-6.8V48.3L0,21.8h7.6l9.6,20.4l9.6-20.4C26.8,21.8,34.4,21.8,34.4,21.8z"/>
		<path class="st11" d="M41.5,61.8c-2.5-1.4-4.5-3.4-5.9-6c-1.4-2.6-2.2-5.6-2.2-8.9c0-3.4,0.7-6.3,2.2-8.9c1.5-2.6,3.5-4.6,6-6
			c2.6-1.4,5.4-2.1,8.6-2.1s6,0.7,8.6,2.1c2.6,1.4,4.6,3.4,6,6c1.5,2.6,2.2,5.6,2.2,8.9c0,3.4-0.8,6.3-2.3,8.9
			c-1.5,2.6-3.6,4.6-6.2,6c-2.6,1.4-5.5,2.1-8.6,2.1C46.8,63.9,44,63.2,41.5,61.8z M54.9,56.7c1.5-0.8,2.8-2.1,3.7-3.8
			c1-1.7,1.4-3.7,1.4-6.1c0-2.4-0.5-4.4-1.4-6.1c-0.9-1.7-2.1-2.9-3.7-3.7c-1.5-0.8-3.2-1.3-4.9-1.3c-1.8,0-3.4,0.4-4.9,1.3
			c-1.5,0.8-2.7,2.1-3.6,3.7c-0.9,1.7-1.3,3.7-1.3,6.1c0,3.6,0.9,6.3,2.7,8.2c1.8,1.9,4.1,2.9,6.9,2.9C51.7,58,53.3,57.5,54.9,56.7z
			"/>
	</g>
	<g>
		<path class="st11" d="M167.1,50.3h-23.7c0.2,2.5,1.1,4.5,2.8,6c1.7,1.5,3.7,2.3,6.1,2.3c3.5,0,5.9-1.4,7.3-4.3h6.9
			c-0.9,2.9-2.6,5.2-5.1,7c-2.5,1.8-5.5,2.7-9.1,2.7c-3,0-5.6-0.7-8-2c-2.3-1.3-4.2-3.2-5.5-5.6s-2-5.2-2-8.4c0-3.2,0.6-6,1.9-8.4
			c1.3-2.4,3.1-4.3,5.5-5.6c2.3-1.3,5-2,8.1-2c2.9,0,5.5,0.6,7.8,1.9c2.3,1.3,4.1,3.1,5.3,5.4c1.3,2.3,1.9,5,1.9,8
			C167.3,48.3,167.2,49.3,167.1,50.3z M160.6,45.1c0-2.4-0.9-4.3-2.5-5.7c-1.7-1.4-3.7-2.1-6.1-2.1c-2.2,0-4.1,0.7-5.7,2.1
			c-1.6,1.4-2.5,3.3-2.8,5.7H160.6z"/>
		<path class="st11" d="M180,21.8v41.6h-6.4V21.8H180z"/>
		<path class="st11" d="M194.9,21.8v41.6h-6.4V21.8H194.9z"/>
		<path class="st11" d="M231.5,50.3h-23.7c0.2,2.5,1.1,4.5,2.8,6c1.6,1.5,3.7,2.3,6.1,2.3c3.5,0,5.9-1.4,7.3-4.3h6.9
			c-0.9,2.9-2.6,5.2-5.1,7c-2.5,1.8-5.5,2.7-9.1,2.7c-3,0-5.6-0.7-8-2c-2.3-1.3-4.2-3.2-5.5-5.6c-1.3-2.4-2-5.2-2-8.4
			c0-3.2,0.6-6,1.9-8.4c1.3-2.4,3.1-4.3,5.5-5.6c2.3-1.3,5-2,8.1-2c2.9,0,5.5,0.6,7.8,1.9c2.3,1.3,4.1,3.1,5.3,5.4
			c1.3,2.3,1.9,5,1.9,8C231.7,48.3,231.7,49.3,231.5,50.3z M225,45.1c0-2.4-0.9-4.3-2.5-5.7c-1.7-1.4-3.7-2.1-6.1-2.1
			c-2.2,0-4.1,0.7-5.7,2.1c-1.6,1.4-2.5,3.3-2.8,5.7H225z"/>
		<path class="st11" d="M248.2,33.2c1.6-0.9,3.4-1.3,5.5-1.3v6.6h-1.6c-2.5,0-4.4,0.6-5.7,1.9s-1.9,3.5-1.9,6.6v16.3H238v-31h6.4v4.5
			C245.4,35.3,246.6,34.1,248.2,33.2z"/>
	</g>
	<circle class="st11" cx="93.4" cy="73.7" r="6.3"/>
	<path class="st22" d="M131.6,51.7c0,5.2-3.4,9.7-8,11.4c-1-0.7-2.2-1.1-3.5-1.1c-1.7,0-3.3,0.7-4.4,1.9H78.9c0-3.9,3.1-7,7-7h33.3
		c2.9,0,5.3-2.3,5.3-5.2s-2.3-5.3-5.3-5.3h-11.4c-3.4,0-6.5-1.4-8.8-3.7c-2.2-2.2-3.5-5.3-3.5-8.7c0.1-6.8,5.6-12.2,12.4-12.2h22.2
		c0,1.9-0.8,3.7-2.1,5s-3,2-4.9,2H108c-2.9,0-5.3,2.3-5.3,5.2s2.3,5.3,5.3,5.3h11.4c3.4,0,6.5,1.4,8.8,3.7
		C130.3,45.3,131.6,48.3,131.6,51.7z"/>
	<path class="st22" d="M97.7,22.8c0.4-0.4,0.8-0.7,1.2-1H71.5l0,0c0,3.9,3.1,7,7,7h15.3C94.7,26.6,96,24.5,97.7,22.8z"/>
	<path class="st22" d="M85.6,38.8h8.1c-0.5-1.6-0.7-3.2-0.7-4.9c0-0.7,0.1-1.4,0.2-2.1H78.6l0,0C78.6,35.7,81.7,38.8,85.6,38.8z"/>
	<path class="st11" d="M104.3,11.2c-0.1-1-0.2-2.1-0.6-3.1C102,2.9,97-0.3,92,0c-0.5,0-1.1,0.1-1.6,0.2c-0.4,0.1-0.7,0.2-1.1,0.3
		c-6,2-9.1,8.6-7.1,14.7c0.1,0.2,0.1,0.4,0.2,0.6c-0.2,0.3-0.2,0.7-0.2,1c0.1,1,0.9,1.8,1.9,1.7s1.8-0.9,1.7-1.9
		c0-0.6-0.4-1.2-1-1.5c-0.1-0.2-0.2-0.4-0.2-0.6C83,9.6,85.5,4.5,90.1,3s9.6,1.1,11.2,5.9c0.3,0.8,0.4,1.6,0.5,2.3
		c-0.4,0.4-0.6,0.9-0.5,1.4c0.1,1,0.9,1.8,1.9,1.7c1-0.1,1.8-0.9,1.7-1.9C104.8,12,104.6,11.5,104.3,11.2z"/>
	<circle class="st11" cx="120.1" cy="68.1" r="5.2"/>
</g>
</svg>

             </div>


             
                  <div class="col-md-9 mt20 mt-md5">
                     <ul class="leader-ul f-18 f-md-20 white-clr text-md-end text-center">
                        <li class="affiliate-link-btn"><a href="#book-seat" class="t-decoration-none ">Book Your Free Seat</a></li>
                     </ul>
                  </div>
               </div>
            </div>
            

            <div class="col-12 mt20 mt-md50 text-center">
                  <div class="preheadline f-20 f-md-22 w400 white-clr lh140">
                  Register for One Time Only Value Packed Free Training  & Get an Assured Gift + 10 Free Licenses 
                  </div>
               </div>
               <div class="col-12 mt-md30 mt20 f-md-48 f-30 w700 text-center white-clr lh140">
               Discover How to <span class="yellow-clr">Launch & Sales Digital Products, Courses, Services & Goods Online</span> to Make All-Time Big Profit! 
               </div>
               <div class="col-12 mt-md25 mt20 f-18 f-md-22 w500 text-center lh140 white-clr text-capitalize">
                  <div class="postheadline f-20 f-md-24 w600 text-center lh140 white-clr text-capitalize">               
                  In This Free Training, We Will Also Reveal, How We Have Sold Over $9+ Mn of Digital Products and How You Can Get Started Easily Without Any Tech Hassles.
                 </div>
               </div>
            
         
            </div>
         
         <div class="row d-flex align-items-center flex-wrap mt20 mt-md40">
            <div class="col-md-7 col-12 min-md-video-width-left">
               <img src="assets/images/video-thumb.webp" class="img-fluid d-block mx-auto" alt="Prelaunch">
            </div>
            <div class="col-md-5 col-12 mt20 mt-md0 min-md-video-width-right" id="form">
               <div class="auto_responder_form inp-phold calendar-wrap" id="book-seat">
                     <div class="f-20 f-md-24 w800 text-center lh180 blue-clr" editabletype="text" style="z-index:10;">
                        Register for Free Training <br>
                        <span class="f-20 f-md-24 w700 text-center black-clr" contenteditable="false">11<sup contenteditable="false">th</sup> October, 10 AM EST</span>
                     </div>
                     <div class="col-12 f-18 f-md-20 w500 lh140 text-center">
                        Book Your Seat now<br> (Limited to 100 Users)
                     </div>
                     <!-- Aweber Form Code -->
                     <div class="col-12 p0 mt15 mt-md20">
                        <form method="post" class="af-form-wrapper register-form-style1 " accept-charset="UTF-8" action="https://www.aweber.com/scripts/addlead.pl" style="z-index: 10;">
                           <div style="display: none;">
                              <input type="hidden" name="meta_web_form_id" value="1316343921" />
                              <input type="hidden" name="meta_split_id" value="" />
                              <input type="hidden" name="listname" value="awlist6353811" />
                              <input type="hidden" name="redirect" value="https://www.yoseller.co/prelaunch-thankyou" id="redirect_43d241fe07ec0f3421af0af36db65e2f" />
                              <input type="hidden" name="meta_adtracking" value="My_Web_Form" />
                              <input type="hidden" name="meta_message" value="1" />
                              <input type="hidden" name="meta_required" value="name,email" />
                              <input type="hidden" name="meta_tooltip" value="" />
                           </div>
                           <div id="af-form-1316343921" class="af-form">
                              <div id="af-body-1316343921" class="af-body af-standards">
                                 <div class="af-element width-form1">
                                    <label class="previewLabel" for="awf_field-114790991"></label>
                                    <div class="af-textWrap">
                                       <div class="input-type" style="z-index: 11;">
                                          <!--<span class="input-group-addon border1px"><i class="fa fa-user" aria-hidden="true"></i></span>-->
                                          <input id="awf_field-114790991" type="text" name="name" class="text form-control custom-input input-field" value="" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="500" placeholder="Your Name" />
                                       </div>
                                    </div>
                                    <div class="af-clear bottom-margin"></div>
                                 </div>
                                 <div class="af-element width-form1">
                                    <label class="previewLabel" for="awf_field-114790992"> </label>
                                    <div class="af-textWrap">
                                       <div class="input-type" style="z-index: 11;">
                                          <!--<span class="input-group-addon border1px"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>-->
                                          <input class="text form-control custom-input input-field" id="awf_field-114790992" type="text" name="email" value="" tabindex="501" onFocus=" if (this.value == '') { this.value = ''; }" onBlur="if (this.value == '') { this.value='';} " placeholder="Your Email" />
                                       </div>
                                    </div>
                                    <div class="af-clear bottom-margin"></div>
                                 </div>
                                 <input type="hidden" id="awf_field_aid" class="text" name="custom aid" value="" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="502" />
                                 <div class="af-element buttonContainer button-type register-btn1 f-22 f-md-21 mt-md0">
                                    <input name="submit" id="af-submit-image-348552691" type="submit" class="image" style="max-width: 100%;" alt="Submit Form" tabindex="503" value="Book Your Free Seat" />
                                    <div class="af-clear bottom-margin"></div>
                                 </div>
                              </div>
                           </div>
                           <div style="display: none;"><img src="https://forms.aweber.com/form/displays.htm?id=jMyMbMwszJxMjA==" alt="" /></div>
                        </form>
                     </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Header Section End -->
   <!-- Header Section Start -->
   <div class="second-section">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="col-12">
                  <div class="row">
                     <div class="col-12">
                        <div class="f-md-35 f-24 w800 lh140 text-capitalize text-center gradient-clr">
                        Why Attend This Free Training Webinar & What Will You Learn?
                        </div>
                        <div class="f-20 f-md-22 w600 lh150 text-capitalize text-center black-clr mt20">
                        Our 20+ Years of Experience Is Packaged in This State-Of-Art 1-Hour Webinar Where You’ll Learn:
                        </div>
                     </div>
                     <div class="col-12 mt20 mt-md50">
                        <ul class="features-list pl0 f-18 f-md-18 lh150 w400 black-clr text-capitalize">
                           <li>How We Have Sold Over $9Mn+ In Digital Products and You Can Follow the Same to Build a Profitable Business Online. </li>
                           <li>How You Can Launch & Sell any type of Digital Products, Courses, Services & Goods by using an all-in-one tool.  </li>
                           <li>How to Sell your Own or White Labelled/Reseller/PLR Licensed Products. </li>
                           <li>How to Sell Products on your Own Marketplace & Keep 100% of the Profit.</li>
                           <li>How to Tap into the $25 Trillion Hot E-Selling Industry. </li>
                           <li>How to set up Stunning Websites, Stores, and Membership Sites Quick and Easy. </li>
                           <li>Lastly, Everyone Who Attends This Session Will Get an Assured Gift from Us Plus We Are Giving 10 Licenses of Our Premium Solution – YoSeller </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>

         <div class="row mt20 mt-md70">
            <div class="col-12 col-md-12 mx-auto" id="form">
               <div class="auto_responder_form inp-phold formbg" id="book-seat1">
                  <div class="f-24 f-md-40 w800 text-center lh140 black-clr" editabletype="text" style="z-index:10;">
                  So Make Sure You Do NOT Miss This Webinar
                  </div>
                  <div class="col-12 f-md-22 f-18 w600 lh140 mx-auto my20 text-center black-clr">
                  Register Now! And Join Us Live on October 11th, 10:00 AM EST
                  </div>
                  <!-- Aweber Form Code -->
                  <div class="col-12 p0 mt15 mt-md25" editabletype="form" style="z-index:10">
                     <!-- Aweber Form Code -->
                     <form method="post" class="af-form-wrapper register-form-style1 " accept-charset="UTF-8" action="https://www.aweber.com/scripts/addlead.pl" style="z-index: 10;">
                        <div style="display: none;">
                           <input type="hidden" name="meta_web_form_id" value="1316343921">
                           <input type="hidden" name="meta_split_id" value="">
                           <input type="hidden" name="listname" value="awlist6353811">
                           <input type="hidden" name="redirect" value="https://www.yoseller.co/prelaunch-thankyou" id="redirect_43d241fe07ec0f3421af0af36db65e2f">
                           <input type="hidden" name="meta_adtracking" value="My_Web_Form">
                           <input type="hidden" name="meta_message" value="1">
                           <input type="hidden" name="meta_required" value="name,email">
                           <input type="hidden" name="meta_tooltip" value="">
                        </div>
                        <div id="af-form-1316343921" class="af-form">
                           <div id="af-body-1316343921" class="af-body af-standards">
                              <div class="af-element width-form">
                                 <label class="previewLabel" for="awf_field-114790991"></label>
                                 <div class="af-textWrap">
                                    <div class="input-type" style="z-index: 11;">
                                       <!--<span class="input-group-addon border1px"><i class="fa fa-user" aria-hidden="true"></i></span>-->
                                       <input id="awf_field-114790991" type="text" name="name" class="text form-control custom-input input-field" value="" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="500" placeholder="Your Name">
                                    </div>
                                 </div>
                                 <div class="af-clear bottom-margin"></div>
                              </div>
                              <div class="af-element width-form">
                                 <label class="previewLabel" for="awf_field-114790992"> </label>
                                 <div class="af-textWrap">
                                    <div class="input-type" style="z-index: 11;">
                                       <!--<span class="input-group-addon border1px"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>-->
                                       <input class="text form-control custom-input input-field" id="awf_field-114790992" type="text" name="email" value="" tabindex="501" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " placeholder="Your Email">
                                    </div>
                                 </div>
                                 <div class="af-clear bottom-margin"></div>
                              </div>
                              <input type="hidden" id="awf_field_aid" class="text" name="custom aid" value="undefined" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="502">
                              <div class="af-element buttonContainer button-type register-btn1 f-22 f-md-21 width-form mt-md0">
                                 <input name="submit" id="af-submit-image-348552691" type="submit" class="image" style="max-width: 100%;" alt="Submit Form" tabindex="503" value="Book Your Free Seat">
                                 <div class="af-clear bottom-margin"></div>
                              </div>
                           </div>
                        </div>
                        <div style="display: none;"><img src="https://forms.aweber.com/form/displays.htm?id=jMyMbMwszJxMjA==" alt="" /></div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Header Section End -->

   <!--Footer Section Start -->
   <div class="footer-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
               <svg version="1.1" id="Layer_11" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 253.7 80" style="enable-background:new 0 0 253.7 80; max-height:60px;" xml:space="preserve">;
<style type="text/css">
	.st00{fill:#FFD60D;}
	.st11{fill:#FFFFFF;}
	.st22{fill:#2337F6;}
</style>
<g>
	<path class="st00" d="M130.2,64.2l-4.8,1.1c-0.4-0.9-1.1-1.6-1.9-2.2c-1-0.7-2.2-1.1-3.5-1.1c-1.7,0-3.3,0.7-4.4,1.9
		c-1,1-1.6,2.4-1.7,3.9l-14.1,3.1c-1.1-2.5-3.6-4.3-6.5-4.3c-3.9,0-7.1,3.2-7.1,7.1v0.1l-1.8,0.4c-3.3,0.8-6.6-1.3-7.3-4.6l-10-43.9
		c-0.8-3.3,1.3-6.6,4.6-7.3l9.5-2.1v0.6c0.1,1.4,1.3,2.5,2.7,2.5c0.1,0,0.1,0,0.2,0c1.5-0.1,2.6-1.3,2.5-2.8c0-0.5-0.2-0.9-0.4-1.3
		l14.2-3.1v0.5c0,0.7,0.3,1.3,0.8,1.8c0.5,0.4,1.1,0.7,1.8,0.7c0.1,0,0.1,0,0.2,0c1.5-0.1,2.6-1.3,2.5-2.8c0-0.5-0.2-0.9-0.4-1.3
		l12.4-2.7c3.3-0.8,6.6,1.3,7.3,4.6l2,8.8l1.1,5l6.8,30.2C135.7,60.3,133.6,63.6,130.2,64.2z"/>
	<g>
		<path class="st11" d="M34.4,21.8L20.7,48.3v15.1h-6.8V48.3L0,21.8h7.6l9.6,20.4l9.6-20.4C26.8,21.8,34.4,21.8,34.4,21.8z"/>
		<path class="st11" d="M41.5,61.8c-2.5-1.4-4.5-3.4-5.9-6c-1.4-2.6-2.2-5.6-2.2-8.9c0-3.4,0.7-6.3,2.2-8.9c1.5-2.6,3.5-4.6,6-6
			c2.6-1.4,5.4-2.1,8.6-2.1s6,0.7,8.6,2.1c2.6,1.4,4.6,3.4,6,6c1.5,2.6,2.2,5.6,2.2,8.9c0,3.4-0.8,6.3-2.3,8.9
			c-1.5,2.6-3.6,4.6-6.2,6c-2.6,1.4-5.5,2.1-8.6,2.1C46.8,63.9,44,63.2,41.5,61.8z M54.9,56.7c1.5-0.8,2.8-2.1,3.7-3.8
			c1-1.7,1.4-3.7,1.4-6.1c0-2.4-0.5-4.4-1.4-6.1c-0.9-1.7-2.1-2.9-3.7-3.7c-1.5-0.8-3.2-1.3-4.9-1.3c-1.8,0-3.4,0.4-4.9,1.3
			c-1.5,0.8-2.7,2.1-3.6,3.7c-0.9,1.7-1.3,3.7-1.3,6.1c0,3.6,0.9,6.3,2.7,8.2c1.8,1.9,4.1,2.9,6.9,2.9C51.7,58,53.3,57.5,54.9,56.7z
			"/>
	</g>
	<g>
		<path class="st11" d="M167.1,50.3h-23.7c0.2,2.5,1.1,4.5,2.8,6c1.7,1.5,3.7,2.3,6.1,2.3c3.5,0,5.9-1.4,7.3-4.3h6.9
			c-0.9,2.9-2.6,5.2-5.1,7c-2.5,1.8-5.5,2.7-9.1,2.7c-3,0-5.6-0.7-8-2c-2.3-1.3-4.2-3.2-5.5-5.6s-2-5.2-2-8.4c0-3.2,0.6-6,1.9-8.4
			c1.3-2.4,3.1-4.3,5.5-5.6c2.3-1.3,5-2,8.1-2c2.9,0,5.5,0.6,7.8,1.9c2.3,1.3,4.1,3.1,5.3,5.4c1.3,2.3,1.9,5,1.9,8
			C167.3,48.3,167.2,49.3,167.1,50.3z M160.6,45.1c0-2.4-0.9-4.3-2.5-5.7c-1.7-1.4-3.7-2.1-6.1-2.1c-2.2,0-4.1,0.7-5.7,2.1
			c-1.6,1.4-2.5,3.3-2.8,5.7H160.6z"/>
		<path class="st11" d="M180,21.8v41.6h-6.4V21.8H180z"/>
		<path class="st11" d="M194.9,21.8v41.6h-6.4V21.8H194.9z"/>
		<path class="st11" d="M231.5,50.3h-23.7c0.2,2.5,1.1,4.5,2.8,6c1.6,1.5,3.7,2.3,6.1,2.3c3.5,0,5.9-1.4,7.3-4.3h6.9
			c-0.9,2.9-2.6,5.2-5.1,7c-2.5,1.8-5.5,2.7-9.1,2.7c-3,0-5.6-0.7-8-2c-2.3-1.3-4.2-3.2-5.5-5.6c-1.3-2.4-2-5.2-2-8.4
			c0-3.2,0.6-6,1.9-8.4c1.3-2.4,3.1-4.3,5.5-5.6c2.3-1.3,5-2,8.1-2c2.9,0,5.5,0.6,7.8,1.9c2.3,1.3,4.1,3.1,5.3,5.4
			c1.3,2.3,1.9,5,1.9,8C231.7,48.3,231.7,49.3,231.5,50.3z M225,45.1c0-2.4-0.9-4.3-2.5-5.7c-1.7-1.4-3.7-2.1-6.1-2.1
			c-2.2,0-4.1,0.7-5.7,2.1c-1.6,1.4-2.5,3.3-2.8,5.7H225z"/>
		<path class="st11" d="M248.2,33.2c1.6-0.9,3.4-1.3,5.5-1.3v6.6h-1.6c-2.5,0-4.4,0.6-5.7,1.9s-1.9,3.5-1.9,6.6v16.3H238v-31h6.4v4.5
			C245.4,35.3,246.6,34.1,248.2,33.2z"/>
	</g>
	<circle class="st11" cx="93.4" cy="73.7" r="6.3"/>
	<path class="st22" d="M131.6,51.7c0,5.2-3.4,9.7-8,11.4c-1-0.7-2.2-1.1-3.5-1.1c-1.7,0-3.3,0.7-4.4,1.9H78.9c0-3.9,3.1-7,7-7h33.3
		c2.9,0,5.3-2.3,5.3-5.2s-2.3-5.3-5.3-5.3h-11.4c-3.4,0-6.5-1.4-8.8-3.7c-2.2-2.2-3.5-5.3-3.5-8.7c0.1-6.8,5.6-12.2,12.4-12.2h22.2
		c0,1.9-0.8,3.7-2.1,5s-3,2-4.9,2H108c-2.9,0-5.3,2.3-5.3,5.2s2.3,5.3,5.3,5.3h11.4c3.4,0,6.5,1.4,8.8,3.7
		C130.3,45.3,131.6,48.3,131.6,51.7z"/>
	<path class="st22" d="M97.7,22.8c0.4-0.4,0.8-0.7,1.2-1H71.5l0,0c0,3.9,3.1,7,7,7h15.3C94.7,26.6,96,24.5,97.7,22.8z"/>
	<path class="st22" d="M85.6,38.8h8.1c-0.5-1.6-0.7-3.2-0.7-4.9c0-0.7,0.1-1.4,0.2-2.1H78.6l0,0C78.6,35.7,81.7,38.8,85.6,38.8z"/>
	<path class="st11" d="M104.3,11.2c-0.1-1-0.2-2.1-0.6-3.1C102,2.9,97-0.3,92,0c-0.5,0-1.1,0.1-1.6,0.2c-0.4,0.1-0.7,0.2-1.1,0.3
		c-6,2-9.1,8.6-7.1,14.7c0.1,0.2,0.1,0.4,0.2,0.6c-0.2,0.3-0.2,0.7-0.2,1c0.1,1,0.9,1.8,1.9,1.7s1.8-0.9,1.7-1.9
		c0-0.6-0.4-1.2-1-1.5c-0.1-0.2-0.2-0.4-0.2-0.6C83,9.6,85.5,4.5,90.1,3s9.6,1.1,11.2,5.9c0.3,0.8,0.4,1.6,0.5,2.3
		c-0.4,0.4-0.6,0.9-0.5,1.4c0.1,1,0.9,1.8,1.9,1.7c1-0.1,1.8-0.9,1.7-1.9C104.8,12,104.6,11.5,104.3,11.2z"/>
	<circle class="st11" cx="120.1" cy="68.1" r="5.2"/>
</g>
</svg>

                  <div class="f-16 f-md-16 w300 mt20 lh140 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-16 f-md-16 w300 lh140 white-clr text-xs-center">Copyright © YoSeller</div>
                  <ul class="footer-ul w300 f-16 f-md-16 white-clr text-center text-md-right">
                     <li><a href="https://support.oppyo.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://yoseller.co/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://yoseller.co/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://yoseller.co/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://yoseller.co/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://yoseller.co/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://yodrive.co/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!--Footer Section End -->


      <script type="text/javascript">
// Special handling for in-app browsers that don't always support new windows
(function() {
    function browserSupportsNewWindows(userAgent) {
        var rules = [
            'FBIOS',
            'Twitter for iPhone',
            'WebView',
            '(iPhone|iPod|iPad)(?!.*Safari\/)',
            'Android.*(wv|\.0\.0\.0)'
        ];
        var pattern = new RegExp('(' + rules.join('|') + ')', 'ig');
        return !pattern.test(userAgent);
    }

    if (!browserSupportsNewWindows(navigator.userAgent || navigator.vendor || window.opera)) {
        document.getElementById('af-form-1316343921').parentElement.removeAttribute('target');
    }
})();
</script><script type="text/javascript">
    (function() {
        var IE = /*@cc_on!@*/false;
        if (!IE) { return; }
        if (document.compatMode && document.compatMode == 'BackCompat') {
            if (document.getElementById("af-form-1316343921")) {
                document.getElementById("af-form-1316343921").className = 'af-form af-quirksMode';
            }
            if (document.getElementById("af-body-1316343921")) {
                document.getElementById("af-body-1316343921").className = "af-body inline af-quirksMode";
            }
            if (document.getElementById("af-header-1316343921")) {
                document.getElementById("af-header-1316343921").className = "af-header af-quirksMode";
            }
            if (document.getElementById("af-footer-1316343921")) {
                document.getElementById("af-footer-1316343921").className = "af-footer af-quirksMode";
            }
        }
    })();
</script>
</body>

</html>
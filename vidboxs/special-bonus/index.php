<html xmlns="http://www.w3.org/1999/xhtml">

<head>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
   <!-- Tell the browser to be responsive to screen width -->
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
   <meta name="title" content="VidBoxs Special Bonuses">
   <meta name="description" content="Get Premium, Limited Time Bonuses With Your VidBoxs Purchase & Make It Even Better">
   <meta name="keywords" content="">
   <meta property="og:image" content="https://www.vidboxs.com/special-bonus/thumbnail.png">
   <meta name="language" content="English">
   <meta name="revisit-after" content="1 days">
   <meta name="author" content="Ayush jain">
   <!-- Open Graph / Facebook -->
   <meta property="og:type" content="website">
   <meta property="og:title" content="VidBoxs Special Bonuses">
   <meta property="og:description" content="Get Premium, Limited Time Bonuses With Your VidBoxs Purchase & Make It Even Better">
   <meta property="og:image" content="https://www.vidboxs.com/special-bonus/thumbnail.png">
   <!-- Twitter -->
   <meta property="twitter:card" content="summary_large_image">
   <meta property="twitter:title" content="VidBoxs Special Bonuses">
   <meta property="twitter:description" content="Get Premium, Limited Time Bonuses With Your VidBoxs Purchase & Make It Even Better">
   <meta property="twitter:image" content="https://www.vidboxs.com/special-bonus/thumbnail.png">
   <title>VidBoxs Special Bonuses</title>

   <link rel="preconnect" href="https://fonts.googleapis.com">
   <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
   <link href="https://fonts.googleapis.com/css2?family=Mulish:wght@300;400;500;600;700;800;900&display=swap" rel="stylesheet">
   <!-- Shortcut Icon  -->
   <link rel="icon" href="https://cdn.oppyo.com/launches/vidboxs/common_assets/images/favicon.png" type="image/png">
   <!-- Css CDN Load Link -->
   <link rel="stylesheet" href="https://cdn.oppyo.com/launches/vidboxs/common_assets/css/bootstrap.min.css" type="text/css">
   <link rel="stylesheet" type="text/css" href="assets/css/bonus-style.css">
   <link rel="stylesheet" href="assets/css/style.css" type="text/css">
   <script src="https://cdn.oppyo.com/launches/vidboxs/common_assets/js/jquery.min.js"></script>
</head>

<body>
   <!-- New Timer  Start-->
   <?php
   $date = 'August 24 2022 11:00 AM EST';
   $exp_date = strtotime($date);
   $now = time();
   /*
         
         $date = date('F d Y g:i:s A eO');
         $rand_time_add = rand(700, 1200);
         $exp_date = strtotime($date) + $rand_time_add;
         $now = time();*/

   if ($now < $exp_date) {
   ?>
   <?php
   } else {
      echo "Times Up";
   }
   ?>
   <!-- New Timer End -->
   <?php
   if (!isset($_GET['afflink'])) {
      $_GET['afflink'] = 'https://warriorplus.com/o2/a/vqh4s8/0';
      $_GET['name'] = 'Ayush Jain';
   }
   ?>
   <div class="main-header">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-12 text-center">
               <div class="text-center">
                  <div class="f-md-32 f-20 lh140 w400 white-clr d-block d-md-flex align-items-center justify-content-center">
                     <span class="w700"><?php echo $_GET['name']; ?>'s</span> &nbsp;special bonus for &nbsp;
                     <svg version="1.1" id="Layer_11" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 865.5 207.4" style="enable-background:new 0 0 865.5 207.4; max-height:50px" xml:space="preserve">
                        <style type="text/css">
                           .st00{fill:#FFFFFF;}
                           .st11{fill:url(#SVGID_1_);}
                           .st22{fill:url(#SVGID_00000003804682614402079440000013301449542194118298_);}
                           .st33{fill:url(#SVGID_00000106859270421856661840000011347085370863025833_);}
                           .st44{opacity:0.3;}
                           .st55{fill:#0F0F0F;}
                        </style>
                        <g>
                           <path class="st00" d="M482.4,27.5v105.7c0,17.9-10.1,33.4-24.9,41.3v-32.9c1-2.6,1.6-5.4,1.6-8.4s-0.6-5.8-1.6-8.4V27.5H482.4z"></path>
                           <path class="st00" d="M457.6,92c-6.5-3.4-13.9-5.4-21.8-5.4c-25.8,0-46.6,20.9-46.6,46.6c0,25.8,20.9,46.6,46.6,46.6
                           c7.9,0,15.3-1.9,21.8-5.4c14.8-7.8,24.9-23.4,24.9-41.3C482.4,115.3,472.4,99.8,457.6,92z M457.6,141.6c-2,5.3-5.9,9.6-10.9,12.2
                           c-3.2,1.7-7,2.7-10.9,2.7c-12.9,0-23.3-10.4-23.3-23.3s10.4-23.3,23.3-23.3c3.9,0,7.6,1,10.9,2.7c5,2.6,8.8,7,10.9,12.2
                           c1,2.6,1.6,5.4,1.6,8.4C459.1,136.2,458.6,139,457.6,141.6z"></path>
                           <path class="st00" d="M498,27.5v105.7c0,17.9,10.1,33.4,24.9,41.3v-32.9c-1-2.6-1.6-5.4-1.6-8.4s0.6-5.8,1.6-8.4V27.5H498z"></path>
                           <path class="st00" d="M498,133.2c0,17.9,10.1,33.4,24.9,41.3c6.5,3.4,13.9,5.4,21.8,5.4c25.8,0,46.6-20.9,46.6-46.6
                           c0-25.8-20.9-46.6-46.6-46.6c-7.9,0-15.3,1.9-21.8,5.4C508.1,99.8,498,115.3,498,133.2z M521.3,133.2c0-3,0.6-5.8,1.6-8.4
                           c2-5.3,5.9-9.6,10.9-12.2c3.2-1.7,7-2.7,10.9-2.7c12.9,0,23.3,10.4,23.3,23.3s-10.4,23.3-23.3,23.3c-3.9,0-7.6-1-10.9-2.7
                           c-5-2.6-8.8-7-10.9-12.2C521.8,139,521.3,136.2,521.3,133.2z"></path>
                           <path class="st00" d="M597.5,133.2c0,17.9,10.1,33.4,24.9,41.3c6.5,3.4,13.9,5.4,21.8,5.4c25.8,0,46.6-20.9,46.6-46.6
                           c0-25.8-20.9-46.6-46.6-46.6c-7.9,0-15.3,1.9-21.8,5.4C607.5,99.8,597.5,115.3,597.5,133.2z M620.8,133.2c0-3,0.6-5.8,1.6-8.4
                           c2-5.3,5.9-9.6,10.9-12.2c3.2-1.7,7-2.7,10.9-2.7c12.9,0,23.3,10.4,23.3,23.3s-10.4,23.3-23.3,23.3c-3.9,0-7.6-1-10.9-2.7
                           c-5-2.6-8.8-7-10.9-12.2C621.3,139,620.8,136.2,620.8,133.2z"></path>
                           <polygon class="st00" points="721.5,179.9 690.4,179.9 756.1,86.5 787.2,86.5 	"></polygon>
                           <polygon class="st00" points="756.1,179.9 787.2,179.9 721.5,86.5 690.4,86.5 	"></polygon>
                       
                           <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="291.6235" y1="980.703" x2="309.5574" y2="1019.9633" gradientTransform="matrix(1 0 0 1 0 -870)">
                              <stop offset="0" style="stop-color:#F40C28"></stop>
                              <stop offset="0.8911" style="stop-color:#FF2C50"></stop>
                           </linearGradient>
                           <polygon class="st11" points="264.4,179.9 278.5,150 308.3,86.5 339.4,86.5 295.5,179.9 292.5,179.9 	"></polygon>
                           <polygon class="st00" points="292.5,179.9 278.5,150 248.6,86.5 217.5,86.5 261.5,179.9 264.4,179.9 	"></polygon>
                           <rect x="351.9" y="86.6" class="st00" width="24.9" height="93.3"></rect>
                           <rect x="351.9" y="51.8" class="st00" width="24.9" height="24.9"></rect>
                           <g>
                              <path class="st00" d="M810.1,175.9c-5.9-2.7-10.5-6.3-14-10.9c-3.4-4.6-5.3-9.6-5.6-15.2h23c0.4,3.5,2.1,6.4,5.1,8.6
                              c3,2.3,6.7,3.4,11.2,3.4c4.3,0,7.8-0.9,10.2-2.6c2.4-1.7,3.7-4,3.7-6.7c0-2.9-1.5-5.1-4.5-6.6s-7.8-3.1-14.3-4.8
                              c-6.7-1.6-12.3-3.3-16.6-5.1c-4.3-1.7-8-4.4-11.1-8c-3.1-3.6-4.7-8.4-4.7-14.5c0-5,1.4-9.6,4.3-13.7c2.9-4.1,7-7.4,12.4-9.8
                              s11.7-3.6,19-3.6c10.8,0,19.4,2.7,25.8,8.1c6.4,5.4,10,12.6,10.6,21.8h-21.9c-0.3-3.6-1.8-6.4-4.5-8.6c-2.7-2.1-6.2-3.2-10.7-3.2
                              c-4.1,0-7.3,0.8-9.5,2.3s-3.3,3.6-3.3,6.4c0,3,1.5,5.4,4.6,6.9c3,1.6,7.8,3.2,14.2,4.8c6.5,1.6,11.9,3.3,16.2,5.1
                              c4.2,1.7,7.9,4.4,11,8.1c3.1,3.6,4.7,8.5,4.8,14.4c0,5.2-1.4,9.9-4.3,14c-2.9,4.1-7,7.4-12.4,9.7s-11.7,3.5-18.8,3.5
                              C822.6,179.9,815.9,178.5,810.1,175.9z"></path>
                           </g>
                        </g>
                        <g>
                           <g>
                              <linearGradient id="SVGID_00000090999748961388969530000010752564172096406444_" gradientUnits="userSpaceOnUse" x1="-1360.6178" y1="-1339.845" x2="-1175.7527" y2="-1283.7964" gradientTransform="matrix(0.6181 -0.7861 -0.7861 -0.6181 -176.8927 -1732.0651)">
                                 <stop offset="0.2973" style="stop-color:#F40C28"></stop>
                                 <stop offset="0.7365" style="stop-color:#FF2C50"></stop>
                              </linearGradient>
                              <path style="fill:url(#SVGID_00000090999748961388969530000010752564172096406444_);" d="M125.8,6.8c0.4,0.3,0.8,0.7,1.2,1
                              c6.2,5.4,9.8,12.7,10.8,20.3c1,8.1-1.2,16.6-6.6,23.6l-75.8,96.4c0,0-18.4,20.5-8.9,35l-32-25.2c-1.1-0.9-2.2-1.8-3.2-2.8
                              c-13.5-13.3-15.2-35-3.2-50.3l72.8-92.6C91.8-1.7,111.9-4.1,125.8,6.8z"></path>
                              <linearGradient id="SVGID_00000108307696041867937860000005451407988123938486_" gradientUnits="userSpaceOnUse" x1="-1216.8146" y1="1899.8297" x2="-973.6699" y2="1975.1873" gradientTransform="matrix(-0.6181 0.7861 0.7861 0.6181 -2080.1633 -197.5988)">
                                 <stop offset="0.3687" style="stop-color:#F40C28"></stop>
                                 <stop offset="1" style="stop-color:#FF2C50"></stop>
                              </linearGradient>
                              <path style="fill:url(#SVGID_00000108307696041867937860000005451407988123938486_);" d="M67.7,199.8c-0.4-0.3-0.8-0.7-1.2-1
                              c-6.2-5.4-9.8-12.7-10.8-20.3c-1-8.1,1.2-16.6,6.6-23.6l75.8-96.4c0,0,18.4-20.5,8.9-35l32,25.2c1.1,0.9,2.2,1.8,3.2,2.8
                              c13.5,13.3,15.2,35,3.2,50.3l-72.8,92.6C101.7,208.3,81.6,210.7,67.7,199.8z"></path>
                           </g>
                           <g class="st44">
                              <path class="st55" d="M68.8,69v78.4c0,8.7,9.8,13.7,16.8,8.6l54.6-39.2c5.9-4.2,5.9-13,0-17.3L85.6,60.4
                              C78.6,55.3,68.8,60.4,68.8,69z"></path>
                           </g>
                           <g>
                              <path class="st00" d="M66.8,66v78.4c0,8.7,9.8,13.7,16.8,8.6l54.6-39.2c5.9-4.2,5.9-13,0-17.3L83.6,57.4
                              C76.6,52.3,66.8,57.4,66.8,66z"></path>
                           </g>
                        </g>
                     </svg>
                  </div>
               </div>
               <div class="col-12 text-center mt20 mt-md50">
                  <div class="pre-heading f-18 f-md-22 w800 lh140 white-clr">
                     Grab My 20 Exclusive Bonuses Before the Deal Ends…
                  </div>
               </div>
               <div class="col-12 mt-md25 mt20 f-md-50 f-28 w800 text-center white-clr lh140 text-capitalize">
                  Revealing A <span class="orange-clr"> Push-Button Technology Creates Short Videos to Publish on YouTube, and Drive Viral Traffic to Any of Your Offers</span> With No Monthly Fee Ever
               </div>
               <div class="col-12 mt-md25 mt20  text-center">
                  <div class="f-18 f-md-24 w600 text-center lh140 white-clr text-capitalize">
                     Watch My Quick Review Video
                  </div>      
                </div>
            </div>
         </div>
         <div class="row mt20 mt-md30">
            <div class="col-12 col-md-10 mx-auto">
               <!-- <img src="assets/images/productbox.webp" class="img-fluid mx-auto d-block" alt="ProductBox"> -->
                <div class="responsive-video">
                  <iframe src=" https://vidboxs.dotcompal.com/video/embed/w4rbh264oa" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                        box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
               </div> 
            </div>
         </div>
      </div>
   </div>
   <!-- Header Section Start -->
   
   <div class="list-section">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="row header-list-block">
                  <div class="col-12 col-md-6">
                     <div class="f-18 f-md-20 lh140 w600">
                        <ul class="bonus-list pl0">
                        <li>Tap Into Fast-Growing $200 Billion+ Video Industry & Bank BIG </li>
                        <li>Create Unlimited YouTube Short Videos on Any Topic </li>
                        <li>Drive Unlimited Traffic & Sales to Any Offer or Page. </li>
                        <li>Make Tons of Affiliate Commissions or Ad Profits  </li>
                        <li>Create Short Videos from Any Text in 3 Simple Clicks </li>
                        <li>Create Videos Using A keyword or Stock Videos </li>
                        </ul>
                     </div>
                  </div>
                  <div class="col-12 col-md-6">
                     <div class="f-18 f-md-20 lh140 w600">
                        <ul class="bonus-list pl0">
                        <li>Add Background Music and/or Voiceover to Any Video </li>
                        <li>Inbuilt Voiceover Creator with 150+ Human Voice in 30+ Languages </li>
                        <li>100% Newbie Friendly, Required No Prior Experience or Tech Skills </li>
                        <li>No Camera Recording, No Voice, or Complex Editing Required </li>
                        
                        
                        </ul>
                     </div>
                  </div>
                  <div class="col-12 col-md-12">
                     <div class="f-18 f-md-20 lh140 w600">
                        <ul class="bonus-list pl0">
                        <li>Free Commercial License to Sell Video Services for High Profits </li>
                  </ul>
               </div>
            </div>
            </div>
            </div>
         </div>
      </div>
   </div>

   <!-- Header Section End -->

   <!-- CTA Section Start -->
   <div class="dark-cta-sec">
      <div class="container-fluid">
         <div class="row">
            <div class="col-md-12 col-md-12 col-12 text-center ">
               <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
               <div class="f-md-36 f-22 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
               <div class="f-18 f-md-22 lh140 w400 text-center mt20 white-clr">
                  Use Coupon Code <span class="w700 orange-clr">"VIDBOXS"</span> for an Additional <span class="w700 orange-clr">$3 Discount</span> on Commercial Licence
               </div>
            </div>
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab VidBoxs + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
               <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-20 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
               <div class="countdown counter-white white-clr"><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">00</span><br><span class="f-14 f-md-18 ">Days</span> </div><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">10</span><br><span class="f-14 f-md-18 w500 ">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald w700">28</span><br><span class="f-14 f-md-18 w500 ">Mins</span> </div><div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald w700">20</span><br><span class="f-14 f-md-18 w500">Sec</span> </div></div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>
   <!-- CTA Section End -->

<<<<<<< HEAD
<!-- Step Section -->
<div class="step-section1">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-45 f-28 w600 text-center black-clr lh140">
                  Create & Share Stunning YouTube Shorts for <br class="d-none d-md-block"> Massive Traffic To Any Offer, Page, or Link <br> <span class="w900 step-shape1">in Just 3 Easy Steps </span>
               </div>
            </div>
            <div class="row mt20 mt-md50">
               <div class="col-12 col-md-4">
                  <div class="step-block">
                     <div class="d-flex align-items-center flex-nowrap gap-4">
                        <img src="https://cdn.oppyo.com/launches/vidboxs/fe/step-icon1.webp" class="img-fluid">
                        <div class="w-100">
                           <div class="f-20 f-md-22 w500 lh140 black-clr">STEP 1</div>
                           <div class="border-line"></div>
                        </div>
                     </div>
                     <div class="f-22 f-md-32 w800 lh140 black-clr mt15">Choose</div>
                     <div class="f-18 w400 lh140 black-clr mt10">
                        Login to access VidBoxs dashboard and choose whether you want to create Shorts with Text, by image, or by using your own video clips. 
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-4 mt20 mt-md0">
                  <div class="step-block">
                     <div class="d-flex align-items-center flex-nowrap gap-4">
                        <img src="https://cdn.oppyo.com/launches/vidboxs/fe/step-icon2.webp" class="img-fluid">
                        <div class="w-100">
                           <div class="f-20 f-md-22 w500 lh140 black-clr">STEP 2</div>
                           <div class="border-line"></div>
                        </div>
                     </div>
                     <div class="f-22 f-md-32 w800 lh140 black-clr mt15">Create</div>
                     <div class="f-18 w400 lh140 black-clr mt10">
                        Just copy-paste text script, use a keywords, or stock video clips to create engaging videos within minutes.
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-4 mt20 mt-md0">
                  <div class="step-block">
                     <div class="d-flex align-items-center flex-nowrap gap-4">
                        <img src="https://cdn.oppyo.com/launches/vidboxs/fe/step-icon3.webp" class="img-fluid">
                        <div class="w-100">
                           <div class="f-20 f-md-22 w500 lh140 black-clr">STEP 3</div>
                           <div class="border-line"></div>
                        </div>
                     </div>
                     <div class="f-22 f-md-32 w800 lh140 black-clr mt15">Publish and Profit</div>
                     <div class="f-18 w400 lh140 black-clr mt10">
                        Now publish your Shorts on YouTube with a click to drive unlimited viral traffic & sales on your offer, page, or website. 
                     </div>
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md70">
               <div class="col-12 col-md-4 col-md-4">
                  <!-- <img src="https://cdn.oppyo.com/launches/vidboxs/fe/no-download.webp" class="img-fluid d-block mx-auto"> -->
                  <div class="w700 f-24 f-md-26 black-clr text-center lh140">
                     No Download/Installation 
                  </div>
               </div>
               <div class="col-12 col-md-4 mt20 mt-md0">
                  <!-- <img src="https://cdn.oppyo.com/launches/vidboxs/fe/no-prior-knowledge.webp" class="img-fluid d-block mx-auto"> -->
                  <div class="w700 f-24 f-md-26 black-clr text-center lh140">
                     No Prior Knowledge                                         
                  </div>
               </div>
               <div class="col-12 col-md-4 mt20 mt-md0">
                  <!-- <img src="https://cdn.oppyo.com/launches/vidboxs/fe/beginners-friendly.webp" class="img-fluid d-block mx-auto"> -->
                  <div class="w700 f-24 f-md-26 black-clr text-center lh140">
                     100% Beginners Friendly 
                  </div>
               </div>
               <div class="col-12 f-18 f-md-20 w400 lh140 black-clr mt20 mt-md30">
                  In just 60 seconds, you can create your first profitable YouTube Short to enjoy MASSIVE FREE Traffic, Sales & Profits coming into your bank account on autopilot. 
               </div>
            </div>
         </div>
=======
   <!-- Step Section -->
   <div class="step-section1">
      <div class="container">
         <div class="row">
            <div class="col-12 f-md-45 f-28 w600 text-center black-clr lh140">
               Create Stunning YouTube Shorts and Drive Massive Traffic and Sales To Any Offer, Page, or Link <br><span class="w900 step-shape1">in Just 3 Easy Steps. </span>
            </div>
         </div>
         <div class="row mt20 mt-md50">
            <div class="col-12 col-md-4">
            <div class="step-block">
               <div class="d-flex align-items-center flex-nowrap gap-4">
                  <img src="assets/images/step-icon1.webp" class="img-fluid">
                  <div class="w-100">
                  <div class="f-20 f-md-22 w500 lh140 black-clr">STEP 1</div>
                  <div class="border-line"></div>
                  </div>
               </div>
               
               <div class="f-22 f-md-32 w800 lh140 black-clr mt15">Choose</div>
               <div class="f-18 w400 lh140 black-clr mt10">
                  Login to access VidBoxs dashboard and choose whether you want to create shots with whiteboard video, by image, or by using your own video clips.
               </div>
            </div>
            </div>
            <div class="col-12 col-md-4 mt20 mt-md0">
               <div class="step-block">
                  <div class="d-flex align-items-center flex-nowrap gap-4">
                     <img src="assets/images/step-icon2.webp" class="img-fluid">
                     <div class="w-100">
                     <div class="f-20 f-md-22 w500 lh140 black-clr">STEP 2</div>
                     <div class="border-line"></div>
                     </div>
                  </div>
                  
                  <div class="f-22 f-md-32 w800 lh140 black-clr mt15">Create</div>
                  <div class="f-18 w400 lh140 black-clr mt10">
                     Just copy-paste text script for whiteboard video, or simply use keywords to find images or upload your own, or else use your own video clips to create engaging videos.
                  </div>
               </div>
               </div>
               <div class="col-12 col-md-4 mt20 mt-md0">
                  <div class="step-block">
                     <div class="d-flex align-items-center flex-nowrap gap-4">
                        <img src="assets/images/step-icon3.webp" class="img-fluid">
                        <div class="w-100">
                        <div class="f-20 f-md-22 w500 lh140 black-clr">STEP 3</div>
                        <div class="border-line"></div>
                        </div>
                     </div>
                     
                     <div class="f-22 f-md-32 w800 lh140 black-clr mt15">Publish and Profit</div>
                     <div class="f-18 w400 lh140 black-clr mt10">
                        Now publish your Shorts on YouTube with a click to drive unlimited viral traffic &amp; sales on your offer, page, or website.
                     </div>
                  </div>
                  </div>
         </div>  

         <div class="row mt20 mt-md70">
            <div class="col-12 col-md-4 col-md-4">
                  <img src="assets/images/no-download.webp" class="img-fluid d-block mx-auto">
                  <div class="w700 f-24 f-md-26 mt20 mt-md30 black-clr text-center lh140">
                     No Download/Installation 
                  </div>
            </div>
            <div class="col-12 col-md-4 mt20 mt-md0">
                  <img src="assets/images/no-prior-knowledge.webp" class="img-fluid d-block mx-auto">
                  <div class="w700 f-24 f-md-26 mt20 mt-md30 black-clr text-center lh140">
                  No Prior Knowledge                                           
                  </div>
            </div>
            <div class="col-12 col-md-4 mt20 mt-md0">
                  <img src="assets/images/beginners-friendly.webp" class="img-fluid d-block mx-auto">
                  <div class="w700 f-24 f-md-26 mt20 mt-md30 black-clr text-center lh140">
                  100% Beginners Friendly
                  </div>
            </div>
            <div class="col-12 f-18 f-md-20 w400 lh140 black-clr mt20 mt-md30">
               In just 7 minutes, you can create your first profitable YouTube Short to enjoy MASSIVE FREE Traffic, Sales &amp; Profits coming into your bank account on autopilot. 
            </div>
         </div>
   
>>>>>>> c1537705 (no message)
      </div>
      <!-- Step Section End -->
      <div class="proof-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-40 f-28 w800 lh140 text-center black-clr">
                  Got 34,309 Targeted Visitors in Last 30 Days… .                  
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
                  <img src="https://cdn.oppyo.com/launches/vidboxs/fe/proof.webp" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 mt20 mt-md50 f-md-40 f-28 w800 lh140 text-center black-clr">
                  Consistently Making Average $535 In Profits <br class="d-none d-md-block"> Each & Every Day              
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
                  <img src="https://cdn.oppyo.com/launches/vidboxs/fe/proof1.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>
         </div>
      </div>
     <!-- Testimonials Section -->
     <div class="testimonial-section">
         <div class="container ">
            <div class="row ">
               <div class="f-md-45 f-28 w800 lh140 text-center black-clr">
                  Checkout What VidBoxs Early Users Have to SAY  
               </div>
            </div>
            <div class="row mt25 mt-md50 align-items-center">
               <div class="col-12 col-md-10">
                  <div class="testi-block">
                     <div class="row d-flex align-items-center flex-wrap">
                         <div class="col-12 col-md-3 order-md-2">
                             <img src="https://cdn.oppyo.com/launches/vidboxs/fe/c1.webp" class="img-fluid d-block mx-auto">
                             <div class="f-22 f-md-24 w800 lh120 red-clr mt20 text-center">Liam Smith</div>
                         </div>
                         <div class="col-12 col-md-9 order-md-1 mt20 mt-md0 text-center text-md-start">
                             <div class="f-22 f-md-28 w800 lh140">I got 2500+ Views & 509 Subscribers in 4 days</div>
                             <div class="f-20 w400 lh140 mt20 quote1">
                              Hi, I am from Australia and promote weight loss products on ClickBank & Amazon as an affiliate. I was struggling to get traffic on my offers and my YouTube channels have only 257 subscribers the reason is that I could not create and post Videos and Shorts regularly as it takes lots of effort and time. But recently, I got access to VidBoxs, I created around 15 Short Videos with almost no effort, that was amazingly EASY. And <span class="w700">I got 2500+ Views & 509 Subscribers in 4 days</span> on my weight loss channel. Kudos to the team for creating a solution that can make the complete process fast & easy. 
                             </div>
                            
                         </div>
                     </div>
                 </div>
               </div>
               <div class="col-12 col-md-10 offset-md-2 mt20 mt-md40">
                  <div class="testi-block">
                     <div class="row d-flex align-items-center flex-wrap">
                         <div class="col-12 col-md-3">
                             <img src="https://cdn.oppyo.com/launches/vidboxs/fe/c2.webp" class="img-fluid d-block mx-auto">
                             <div class="f-22 f-md-24 w800 red-clr mt20 text-center">Noah Brown</div>
                         </div>
                         <div class="col-12 col-md-9 mt20 mt-md0 text-center text-md-start">
                             <div class="f-22 f-md-28 w800 lh140">Best part about VidBoxs is that it takes away all hassles</div>
                             <div class="f-20 w400 lh140 mt20 quote1">
                              Thanks for the early access, VidBoxs Team. <span class="w700">The best part about VidBoxs is that it takes away</span> all hassles like video recording or editing, voiceover recording, background music and driving traffic to your offers, etc. I am sure this will give you complete value for your money and help you get the desired success in the long run.
                             </div>
                            
                         </div>
                     </div>
                 </div>
               </div>
               <div class="col-12 col-md-10 mt20 mt-md40">
                  <div class="testi-block">
                     <div class="row d-flex align-items-center flex-wrap">
                         <div class="col-12 col-md-3 order-md-2">
                             <img src="https://cdn.oppyo.com/launches/vidboxs/fe/c3.webp" class="img-fluid d-block mx-auto">
                             <div class="f-22 f-md-24 w800 lh120 red-clr mt20 text-center">Benjamin Davis</div>
                         </div>
                         <div class="col-12 col-md-9 order-md-1 mt20 mt-md0 text-center text-md-start">
                             <div class="f-22 f-md-28 w800 lh140">Create super engaging video shorts for YouTube</div>
                             <div class="f-20 w400 lh140 mt20 quote1">
                              VidBoxs is the perfect platform to <span class="w700">create super engaging video shorts for YouTube and social media and drive tons of viral traffic in a stress-free manner.</span> I am enjoying all the functionalities of this amazing software. If someone asks me to give marks, I would love to give ten-on-ten to this fabulous product.
                             </div>
                           
                         </div>
                     </div>
                 </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Testimonials Section End -->

   <!-- CTA Section Start -->
   <div class="dark-cta-sec">
      <div class="container-fluid">
         <div class="row">
            <div class="col-md-12 col-md-12 col-12 text-center ">
               <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
               <div class="f-md-36 f-22 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
               <div class="f-18 f-md-22 lh140 w400 text-center mt20 white-clr">
                  Use Coupon Code <span class="w700 orange-clr">"VIDBOXS"</span> for an Additional <span class="w700 orange-clr">$3 Discount</span> on Commercial Licence
               </div>
            </div>
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab VidBoxs + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
               <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-20 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
               <div class="countdown counter-white white-clr"><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">00</span><br><span class="f-14 f-md-18 ">Days</span> </div><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">10</span><br><span class="f-14 f-md-18 w500 ">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald w700">28</span><br><span class="f-14 f-md-18 w500 ">Mins</span> </div><div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald w700">20</span><br><span class="f-14 f-md-18 w500">Sec</span> </div></div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>
   <!-- CTA Section End -->

   <div class="facts-bg">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-28 f-md-45 w800 lh140 text-center">And here are some eye-opening facts that will let <br class="d-none d-md-block"> you know why one should get started with<br class="d-none d-md-block"> YouTube Shorts! 
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50">
                  <div class="row align-items-center">
                     <div class="col-12 col-md-6">
                        <div class="f-22 f-md-32 w800 lh140">YouTube has 2+ Bn Users & 70% of Videos are Viewed on Mobile</div>
                        <div class="f-18 w400 lh140 mt10">YouTube has 2 billion monthly active users, <span class="w700">1 billion hours of content is watched every day, and 70% of videos are viewed on mobile.</span> A vast majority of YouTube content is viewed on mobile devices and it has reaffirmed the industry's shift towards short-form, easily digestible, and mobile-first content.And because of this fact, YouTube is also investing so heavily in this area</div>
                     </div>
                     <div class="col-12 col-md-6 mt20 mt-md0">
                        <img src="https://cdn.oppyo.com/launches/vidboxs/fe/facts3.webp" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50">
                  <div class="row align-items-center">
                     <div class="col-12 col-md-6 order-md-2">
                        <div class="f-22 f-md-32 w800 lh140">YouTube Confirms Ads Are Coming to Shorts: </div>
                        <div class="f-18 w400 lh140 mt10">Ads in Shorts would give creators in the YouTube Partner Program the ability to earn more revenue on the platform.</div>
                     </div>
                     <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
                        <img src="https://cdn.oppyo.com/launches/vidboxs/fe/facts1.webp" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50">
                  <div class="row align-items-center">
                     <div class="col-12 col-md-6">
                        <div class="f-22 f-md-32 w800 lh140">Another Great Earning Potential is YouTube Shorts Fund: </div>
                        <div class="f-18 w400 lh140 mt10">YouTube has signaled its attention to helping creators monetize Shorts and transform them into a solid new revenue stream. To that concern, the company has decided to pay creators at least $100 million for their YouTube Shorts monetization through 2022.</div>
                     </div>
                     <div class="col-12 col-md-6 mt20 mt-md0">
                        <img src="https://cdn.oppyo.com/launches/vidboxs/fe/facts2.webp" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>              
               <div class="col-12 mt20 mt-md50">
                  <div class="row align-items-center">
                     <div class="col-12 col-md-6 order-md-2">
                        <div class="f-22 f-md-32 w800 lh140">YouTube Shorts Shelf in Trending Pages:</div>
                        <div class="f-18 w400 lh140 mt10">YouTube trending page is getting a carousel dedicated to short form of videos or what the company refers to as a short shelf. </div>
                     </div>
                     <div class="col-12 col-md-6 order-md-1 mt20 mt-md0">
                        <img src="https://cdn.oppyo.com/launches/vidboxs/fe/facts4.webp" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50">
                  <div class="row align-items-center">
                     <div class="col-12 col-md-6">
                        <div class="f-22 f-md-32 w800 lh140">YouTube Shorts on Desktop and Mobile Web: </div>
                        <div class="f-18 w400 lh140 mt10">Shorts are gaining more visibility on desktop and mobile browsers with the addition of a new tab and home page carousel.  </div>
                     </div>
                     <div class="col-12 col-md-6 mt20 mt-md0">
                        <img src="https://cdn.oppyo.com/launches/vidboxs/fe/facts5.webp" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md70 text-center">
                  <div class="f-24 f-md-40 w800 lh140 text-center">So, Capitalising on YouTube Shorts in the Video Marketing Strategy of Any Business is the Need of The Hour.                     
                  </div>
                  <img src="https://cdn.oppyo.com/launches/vidboxs/fe/sepration.webp" class="img-fluid mx-auto d-block mt15">
                  <div class="skew-shape mt20 mt-md50">
                     <div class="f-20 f-md-32 lh140 w800">Also, It Opens the Door to Fastest Growing</div>
                     <div class="f-24 f-md-65 lh140 w900">BIG $200 Billion Opportunity</div>
                  </div>                 
               </div>
            </div>
         </div>
      </div>

  <!-- Proudly Section -->
  <div class="proudly-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center lh140">
                  <div class="f-28 f-md-32 w800 text-center orange-clr lh140 text-capitalize">Presenting…</div>
               </div>
               <div class="col-12 mt-md40 mt20 text-center">
                  <svg version="1.1" id="Layer_11" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 865.5 207.4" style="enable-background:new 0 0 865.5 207.4; max-height:100px" xml:space="preserve">
                     <style type="text/css">
                        .st00{fill:#FFFFFF;}
                        .st11{fill:url(#SVGID_1_);}
                        .st22{fill:url(#SVGID_00000003804682614402079440000013301449542194118298_);}
                        .st33{fill:url(#SVGID_00000106859270421856661840000011347085370863025833_);}
                        .st44{opacity:0.3;}
                        .st55{fill:#0F0F0F;}
                     </style>
                     <g>
                        <path class="st00" d="M482.4,27.5v105.7c0,17.9-10.1,33.4-24.9,41.3v-32.9c1-2.6,1.6-5.4,1.6-8.4s-0.6-5.8-1.6-8.4V27.5H482.4z"></path>
                        <path class="st00" d="M457.6,92c-6.5-3.4-13.9-5.4-21.8-5.4c-25.8,0-46.6,20.9-46.6,46.6c0,25.8,20.9,46.6,46.6,46.6
                           c7.9,0,15.3-1.9,21.8-5.4c14.8-7.8,24.9-23.4,24.9-41.3C482.4,115.3,472.4,99.8,457.6,92z M457.6,141.6c-2,5.3-5.9,9.6-10.9,12.2
                           c-3.2,1.7-7,2.7-10.9,2.7c-12.9,0-23.3-10.4-23.3-23.3s10.4-23.3,23.3-23.3c3.9,0,7.6,1,10.9,2.7c5,2.6,8.8,7,10.9,12.2
                           c1,2.6,1.6,5.4,1.6,8.4C459.1,136.2,458.6,139,457.6,141.6z"></path>
                        <path class="st00" d="M498,27.5v105.7c0,17.9,10.1,33.4,24.9,41.3v-32.9c-1-2.6-1.6-5.4-1.6-8.4s0.6-5.8,1.6-8.4V27.5H498z"></path>
                        <path class="st00" d="M498,133.2c0,17.9,10.1,33.4,24.9,41.3c6.5,3.4,13.9,5.4,21.8,5.4c25.8,0,46.6-20.9,46.6-46.6
                           c0-25.8-20.9-46.6-46.6-46.6c-7.9,0-15.3,1.9-21.8,5.4C508.1,99.8,498,115.3,498,133.2z M521.3,133.2c0-3,0.6-5.8,1.6-8.4
                           c2-5.3,5.9-9.6,10.9-12.2c3.2-1.7,7-2.7,10.9-2.7c12.9,0,23.3,10.4,23.3,23.3s-10.4,23.3-23.3,23.3c-3.9,0-7.6-1-10.9-2.7
                           c-5-2.6-8.8-7-10.9-12.2C521.8,139,521.3,136.2,521.3,133.2z"></path>
                        <path class="st00" d="M597.5,133.2c0,17.9,10.1,33.4,24.9,41.3c6.5,3.4,13.9,5.4,21.8,5.4c25.8,0,46.6-20.9,46.6-46.6
                           c0-25.8-20.9-46.6-46.6-46.6c-7.9,0-15.3,1.9-21.8,5.4C607.5,99.8,597.5,115.3,597.5,133.2z M620.8,133.2c0-3,0.6-5.8,1.6-8.4
                           c2-5.3,5.9-9.6,10.9-12.2c3.2-1.7,7-2.7,10.9-2.7c12.9,0,23.3,10.4,23.3,23.3s-10.4,23.3-23.3,23.3c-3.9,0-7.6-1-10.9-2.7
                           c-5-2.6-8.8-7-10.9-12.2C621.3,139,620.8,136.2,620.8,133.2z"></path>
                        <polygon class="st00" points="721.5,179.9 690.4,179.9 756.1,86.5 787.2,86.5 	"></polygon>
                        <polygon class="st00" points="756.1,179.9 787.2,179.9 721.5,86.5 690.4,86.5 	"></polygon>
                        
                           <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="291.6235" y1="980.703" x2="309.5574" y2="1019.9633" gradientTransform="matrix(1 0 0 1 0 -870)">
                           <stop offset="0" style="stop-color:#F40C28"></stop>
                           <stop offset="0.8911" style="stop-color:#FF2C50"></stop>
                        </linearGradient>
                        <polygon class="st11" points="264.4,179.9 278.5,150 308.3,86.5 339.4,86.5 295.5,179.9 292.5,179.9 	"></polygon>
                        <polygon class="st00" points="292.5,179.9 278.5,150 248.6,86.5 217.5,86.5 261.5,179.9 264.4,179.9 	"></polygon>
                        <rect x="351.9" y="86.6" class="st00" width="24.9" height="93.3"></rect>
                        <rect x="351.9" y="51.8" class="st00" width="24.9" height="24.9"></rect>
                        <g>
                           <path class="st00" d="M810.1,175.9c-5.9-2.7-10.5-6.3-14-10.9c-3.4-4.6-5.3-9.6-5.6-15.2h23c0.4,3.5,2.1,6.4,5.1,8.6
                              c3,2.3,6.7,3.4,11.2,3.4c4.3,0,7.8-0.9,10.2-2.6c2.4-1.7,3.7-4,3.7-6.7c0-2.9-1.5-5.1-4.5-6.6s-7.8-3.1-14.3-4.8
                              c-6.7-1.6-12.3-3.3-16.6-5.1c-4.3-1.7-8-4.4-11.1-8c-3.1-3.6-4.7-8.4-4.7-14.5c0-5,1.4-9.6,4.3-13.7c2.9-4.1,7-7.4,12.4-9.8
                              s11.7-3.6,19-3.6c10.8,0,19.4,2.7,25.8,8.1c6.4,5.4,10,12.6,10.6,21.8h-21.9c-0.3-3.6-1.8-6.4-4.5-8.6c-2.7-2.1-6.2-3.2-10.7-3.2
                              c-4.1,0-7.3,0.8-9.5,2.3s-3.3,3.6-3.3,6.4c0,3,1.5,5.4,4.6,6.9c3,1.6,7.8,3.2,14.2,4.8c6.5,1.6,11.9,3.3,16.2,5.1
                              c4.2,1.7,7.9,4.4,11,8.1c3.1,3.6,4.7,8.5,4.8,14.4c0,5.2-1.4,9.9-4.3,14c-2.9,4.1-7,7.4-12.4,9.7s-11.7,3.5-18.8,3.5
                              C822.6,179.9,815.9,178.5,810.1,175.9z"></path>
                        </g>
                     </g>
                     <g>
                        <g>
                           
                              <linearGradient id="SVGID_00000090999748961388969530000010752564172096406444_" gradientUnits="userSpaceOnUse" x1="-1360.6178" y1="-1339.845" x2="-1175.7527" y2="-1283.7964" gradientTransform="matrix(0.6181 -0.7861 -0.7861 -0.6181 -176.8927 -1732.0651)">
                              <stop offset="0.2973" style="stop-color:#F40C28"></stop>
                              <stop offset="0.7365" style="stop-color:#FF2C50"></stop>
                           </linearGradient>
                           <path style="fill:url(#SVGID_00000090999748961388969530000010752564172096406444_);" d="M125.8,6.8c0.4,0.3,0.8,0.7,1.2,1
                              c6.2,5.4,9.8,12.7,10.8,20.3c1,8.1-1.2,16.6-6.6,23.6l-75.8,96.4c0,0-18.4,20.5-8.9,35l-32-25.2c-1.1-0.9-2.2-1.8-3.2-2.8
                              c-13.5-13.3-15.2-35-3.2-50.3l72.8-92.6C91.8-1.7,111.9-4.1,125.8,6.8z"></path>
                           
                              <linearGradient id="SVGID_00000108307696041867937860000005451407988123938486_" gradientUnits="userSpaceOnUse" x1="-1216.8146" y1="1899.8297" x2="-973.6699" y2="1975.1873" gradientTransform="matrix(-0.6181 0.7861 0.7861 0.6181 -2080.1633 -197.5988)">
                              <stop offset="0.3687" style="stop-color:#F40C28"></stop>
                              <stop offset="1" style="stop-color:#FF2C50"></stop>
                           </linearGradient>
                           <path style="fill:url(#SVGID_00000108307696041867937860000005451407988123938486_);" d="M67.7,199.8c-0.4-0.3-0.8-0.7-1.2-1
                              c-6.2-5.4-9.8-12.7-10.8-20.3c-1-8.1,1.2-16.6,6.6-23.6l75.8-96.4c0,0,18.4-20.5,8.9-35l32,25.2c1.1,0.9,2.2,1.8,3.2,2.8
                              c13.5,13.3,15.2,35,3.2,50.3l-72.8,92.6C101.7,208.3,81.6,210.7,67.7,199.8z"></path>
                        </g>
                        <g class="st44">
                           <path class="st55" d="M68.8,69v78.4c0,8.7,9.8,13.7,16.8,8.6l54.6-39.2c5.9-4.2,5.9-13,0-17.3L85.6,60.4
                              C78.6,55.3,68.8,60.4,68.8,69z"></path>
                        </g>
                        <g>
                           <path class="st00" d="M66.8,66v78.4c0,8.7,9.8,13.7,16.8,8.6l54.6-39.2c5.9-4.2,5.9-13,0-17.3L83.6,57.4
                              C76.6,52.3,66.8,57.4,66.8,66z"></path>
                        </g>
                     </g>
                     </svg>
               </div>
               <div class="col-12 f-md-48 f-28 mt-md30 mt20 w800 text-center white-clr lh140">
                  A Push-Button Technology Creates Short Videos to Publish on YouTube, 
                  or other social media and Drive Viral Traffic to Any of Your Offers
                  
               </div>
               <div class="col-12 f-20 f-md-22 lh140 w500 text-center white-clr mt20">
                 
It enabled you to create Engaging, Highly Professional Short Videos, <br class="d-none d-md-block">
Add Voiceover or Background Music with Zero Tech Hassles. 
 
               </div>
            </div>
            <div class="row mt20 mt-md50 align-items-center">
               <div class="col-12">
                  <img src="https://cdn.oppyo.com/launches/vidboxs/fe/product-image.webp" class="img-fluid d-block mx-auto img-animation"
                     alt="Product">
               </div>
            </div>
         </div>
      </div>
      <!-- Proudly Section End -->

   <!-- Bonus Section Header Start -->
   <div class="bonus-header">
      <div class="container">
         <div class="row">
            <div class="col-12 col-md-10 mx-auto heading-bg text-center">
               <div class="f-24 f-md-36 lh140 w700"> When You Purchase VidBoxs, You Also Get <br class="hidden-xs"> Instant Access To These 20 Exclusive Bonuses</div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus Section Header End -->

   <!-- Bonus #1 Section Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 1</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus1.webp" class="img-fluid mx-auto d-block" alt="Bonus1">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w800 lh140 bonus-title-color">Evergreen Infographics Pack</div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li class="w700">
                           One of the most important types of visuals for a business owners in the online arena are infographics.
                        </li>
                        <li>
                           Infographics are used to showcase statistics, to sell products, to advertise, promote, attract and connect better with your audience.
                        </li>
                        <li>
                           It is the visual representation that you showcase to your potential customers and that sends a clear message to them!
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #1 Section End -->

   <!-- Bonus #2 Section Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 2</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30">
               <div class="row">
                  <div class="col-md-5 col-12 order-md-2">
                     <img src="assets/images/bonus2.webp" class="img-fluid mx-auto d-block" alt="Bonus2">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                     <div class="f-22 f-md-32 w800 lh140 bonus-title-color mt20 mt-md0">
                        Find Your Niche
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li class="w700">
                           Owning a business has many advantages from being able to set your own hours to have the control to sell what you want. 
                        </li>
                        <li>
                           Unfortunately, too many new business owners fail within their first year. 
                        </li>
                        <li>
                           While it isn't for lack of effort, those looking to start a new online business fail to complete the crucial first step; they fail to research to find a viable and profitable niche.
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #2 Section End -->

   <!-- Bonus #3 Section Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 3</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus3.webp" class="img-fluid mx-auto d-block " alt="Bonus3">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w800 lh140 bonus-title-color">
                        Email Monetizer
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li class="w700">Turning your email list into a passive income money maker isn’t as difficult, or time consuming as you may think.</li>
                        <li>Every day, thousands of online marketers are transforming their mailing lists into powerful cash funnels, and quite often, they don’t even have their own product line!</li>
                        <li>This special report will make it easy for you to start making money with your subscriber base even if you’re just starting out.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #3 Section End -->

   <!-- Bonus #4 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 4</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12 order-md-2">
                     <img src="assets/images/bonus4.webp" class="img-fluid mx-auto d-block " alt="Bonus4">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                     <div class="f-22 f-md-32 w800 lh140 bonus-title-color">
                        Background 4K Stock Videos
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li class="w700">
                           By 2019, Internet Video Traffic will account for 80% of all consumer Internet traffic. Here is an excellent opportunity to leverage the power of Videos and use this medium to catapult your web business to the next level. 
                        </li>
                        <li>
                           There are Endless Possibilities where you can use these videos and here are just some of them.
                           <br> <br>
                           <b>You can:</b> 
                        </li>
                        <li>Use them as Background video on your sales page/sales video to enchance its appearance</li>
                        <li>Use them as Sales video on your Squeeze Page/Landing Page</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #4 End -->

   <!-- Bonus #5 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 5</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus5.webp" class="img-fluid mx-auto d-block " alt="Bonus5">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w800 lh140 bonus-title-color">
                        Article Rewriter Pro
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>
                        <b>More and more people are creating their own websites containing videos or other forms of active content, such as games. </b> The problem with this sort of website is that there is nothing on the page to attract search engines. Search engines like text that gives them an indication of the content - preferably unique.
                        </li>
                        <li>
                        Article Rewriter Pro software offers a quick and easy way to create suitable text, with minimal effort. The articles consist of full sentences, so can be analysed by search engines.
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #5 End -->

   <!-- CTA Button Section Start -->
   <div class="dark-cta-sec">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-md-12 col-12 text-center ">
               <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
               <div class="f-md-36 f-22 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
               <div class="f-18 f-md-22 lh140 w400 text-center mt20 white-clr">
                  Use Coupon Code <span class="w700 orange-clr">"VIDBOXS"</span> for an Additional <span class="w700 orange-clr">$3 Discount</span> on Commercial Licence
               </div>
            </div>
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab VidBoxs + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
               <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-20 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
               <div class="countdown counter-white white-clr">
                  <div class="timer-label text-center">
                     <span class="f-31 f-md-60 timerbg oswald w700">01</span>
                     <span class="f-14 f-md-18 w500 ">Days</span>
                  </div>
                  <div class="timer-label text-center">
                     <span class="f-31 f-md-60 timerbg oswald w700">16</span>
                     <span class="f-14 f-md-18 w500 ">Hours</span>
                  </div>
                  <div class="timer-label text-center timer-mrgn">
                     <span class="f-31 f-md-60 timerbg oswald w700">59</span>
                     <span class="f-14 f-md-18 w500 ">Mins</span>
                  </div>
                  <div class="timer-label text-center ">
                     <span class="f-31 f-md-60 timerbg oswald w700">37</span>
                     <span class="f-14 f-md-18 w500 ">Sec</span>
                  </div>
               </div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>
   <!-- CTA Button Section End -->

   <!-- Bonus #6 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 6</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12 order-md-2">
                     <img src="assets/images/bonus6.webp" class="img-fluid mx-auto d-block " alt="Bonus6">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                     <div class="f-22 f-md-32 w800 lh140 bonus-title-color">
                        Two Step Opt-in Generator
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>
                           <b>At Last! You Can Easily Increase Your Opt-In Conversions, In Just Minutes...Builds 2 Step Opt-In Pages With Lightbox!</b>
                        </li>
                        <li>
                           The money is the list. This is what successful internet marketers always shared to us if you want to become success in the internet business.
                        </li>
                        <li>
                           The fact is that, building email list is time consuming and sometimes takes time to gather a good amount of subscribers.
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #6 End -->

   <!-- Bonus #7 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 7</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus7.webp" class="img-fluid mx-auto d-block " alt="Bonus7">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w800 lh140 bonus-title-color">
                        Master Shorty
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>
                           <b>A must have program for all online marketers!</b>
                        </li>
                        <li>
                           If you are a blogger or affiliate marketer, having an IM Tools that will make your business run at ease online is a huge help to lessen the effort and have more productive tasks that you can accomplish.
                        </li>
                        <li>
                           One of the tasks that is included to be time-consuming is the cloaking of affiliate links or URL specially if you are selling affiliate physical products from Amazon or even a digital products from Clickbank or the likes.
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #7 End -->

   <!-- Bonus #8 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 8</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="assets/images/bonus8.webp" class="img-fluid mx-auto d-block " alt="Bonus8">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w800 lh140 bonus-title-color">
                        Video Marketing Unleashed
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>
                           <b>Effective video marketing is all about conversions. The video has to play a role in getting the viewer to whip out their credit card and buy something or click on an ad that pays you or enter their email address or zip code into a form.</b>
                        </li>
                        <li>
                           The challenge to video marketers nowadays is that video may have been the victim of its own success.
                        </li>
                        <li>The problem now is that there’s so much video out there that most of them simply don’t have an impact. They don’t get people to convert to buyers.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #8 End -->

   <!-- Bonus #9 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 9</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus9.webp" class="img-fluid mx-auto d-block " alt="Bonus9">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w800 lh140 bonus-title-color">
                        Video Marketing Domination
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>
                           <b>By 2021 video will make upmore than 80% of internet traffic! After watching a video 64% of users are more likely to buy a product online. YouTube reports that mobile video consumptions rise 100% EVERY YEAR.</b>
                        </li>
                        <li>
                           All of this really proves the point that absolutely every business should leverage video in their marketing.
                        </li>
                        <li>
                           With this video course you will learn to create videos that generate hundreds and thousands of visitors to my websites, funnels, and offers… On a month to month basis.
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #9 End -->

   <!-- Bonus #10 start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 10</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="assets/images/bonus10.webp" class="img-fluid mx-auto d-block " alt="Bonus10">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w800 lh140 bonus-title-color">
                        Unique Exit Popup
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>
                           <b>Grab the Attention of Your Visitors When They Are About to Leave Your Website!</b>
                        </li>
                        <li>
                           Traffic is very important to every website. But what if those people who visit your website will just go away doing nothing?
                        </li>
                        <li>
                           Well, inside this product is a software that will change the game of this issue. This plugin will engage and get the attention of your readers that is about to leave your page and offer them something valuable from your website.
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #10 End -->

   <!-- CTA Button Section Start -->
   <div class="dark-cta-sec">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-md-12 col-12 text-center ">
               <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
               <div class="f-md-36 f-22 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
               <div class="f-18 f-md-22 lh140 w400 text-center mt20 white-clr">
                  Use Coupon Code <span class="w700 Yellow-clr">"VIDBOXS"</span> for an Additional <span class="w700 orange-clr">$3 Discount</span> on Commercial Licence
               </div>
            </div>
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab VidBoxs + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
               <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-20 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
               <div class="countdown counter-white white-clr">
                  <div class="timer-label text-center">
                     <span class="f-31 f-md-60 timerbg oswald w700">01</span>
                     <span class="f-14 f-md-18 w500 ">Days</span>
                  </div>
                  <div class="timer-label text-center">
                     <span class="f-31 f-md-60 timerbg oswald w700">16</span>
                     <span class="f-14 f-md-18 w500 ">Hours</span>
                  </div>
                  <div class="timer-label text-center timer-mrgn">
                     <span class="f-31 f-md-60 timerbg oswald w700">59</span>
                     <span class="f-14 f-md-18 w500 ">Mins</span>
                  </div>
                  <div class="timer-label text-center ">
                     <span class="f-31 f-md-60 timerbg oswald w700">37</span>
                     <span class="f-14 f-md-18 w500 ">Sec</span>
                  </div>
               </div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>
   <!-- CTA Button Section End -->

   <!-- Bonus #11 start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 11</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus11.webp" class="img-fluid mx-auto d-block " alt="Bonus11">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w800 lh140 bonus-title-color">
                        Covert Video Squeeze Page Creator
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li><b>Introducing The Sneaky Video Squeeze Page Maker!</b></li>
                        <li>The money is the in the list. That's what many successful online entrepreneur's are saying.</li>
                        <li>And if you are not doing it also, you are leaving a lot of money in front of you.</li>
                        <li>The question is that, how are you going to build a list? Well, the most effective way to do it is by using video squeeze pages.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #11 End -->

   <!-- Bonus #12 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 12</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="assets/images/bonus12.webp" class="img-fluid mx-auto d-block " alt="Bonus12">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w800 lh140 bonus-title-color">
                        Featured Video Plus
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li><b>If a picture is worth a thousand words then how many words is a video worth?</b></li>
                        <li>Add Featured Videos to your posts and pages. Works like magic with most themes which use Featured Images!</li>
                        <li>Featured Video Plus enables you to define Featured Videos, which, if set, take the place of Featured Images.</li>
                        <li>This plugin adds customization options to your Media Settings.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #12 End -->

   <!-- Bonus #13 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 13</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus13.webp" class="img-fluid mx-auto d-block " alt="Bonus13">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w800 lh140 bonus-title-color">
                        Affiliate Marketing A-Z
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>The Affiliate Marketing A-Z report is a very enticing lead magnet, especially for newbies. It explains the process of affiliate marketing and how to get started in the best possible way. Readers will learn what they must do to get started and what they must avoid. There is an A-Z of the most commonly used affiliate marketing terms that are essential to know.</li>
                        <li>This powerful report is ideal for those new to affiliate marketing. The introduction explains the benefits of getting started with affiliate marketing and provides some different ways for affiliates to make money. Your readers will understand that they do not require any special skills or experience to be an affiliate marketer.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #13 End -->

   <!-- Bonus #14 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 14</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="assets/images/bonus14.webp" class="img-fluid mx-auto d-block " alt="Bonus14">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w800 lh140 bonus-title-color">
                        Project Genius
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Do you have projects to plan? Do you want to take a professional approach? Do you want to increase your success rate? Then Project Genius is the software that will help you plan your projects!</li>
                        <li>From now on, you will be able to take a professional approach to your project planning and increase your success rate. You will no longer have surprises during the course of your project. You will better know what you want, your will organize your brain storming sessions better, and you will set your milestones and deadlines with ease.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #14 End -->

   <!-- Bonus #15 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 15</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus15.webp" class="img-fluid mx-auto d-block " alt="Bonus15">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w800 lh140 bonus-title-color">
                        eProfit Generator
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Completely Automate The Entire Sales & Product Delivery Process For All Of Your PayPal Based Sites in 7 Minutes or Less - No Programming Skills Required! This Could Be The Easiest System Ever Put Together To Automate All Of Your sales site Processes From PayPal IPN, To Emailing Your Customers, Handling Downloads, And More!</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #15 End -->

   <!-- CTA Button Section Start -->
   <div class="dark-cta-sec">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-md-12 col-12 text-center ">
               <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
               <div class="f-md-36 f-22 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
               <div class="f-18 f-md-22 lh140 w400 text-center mt20 white-clr">
                  Use Coupon Code <span class="w700 orange-clr">"VIDBOXS"</span> for an Additional <span class="w700 orange-clr">$3 Discount</span> on Commercial Licence
               </div>
            </div>
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab VidBoxs + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
               <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-20 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
               <div class="countdown counter-white white-clr">
                  <div class="timer-label text-center">
                     <span class="f-31 f-md-60 timerbg oswald w700">01</span>
                     <span class="f-14 f-md-18 w500 ">Days</span>
                  </div>
                  <div class="timer-label text-center">
                     <span class="f-31 f-md-60 timerbg oswald w700">16</span>
                     <span class="f-14 f-md-18 w500 ">Hours</span>
                  </div>
                  <div class="timer-label text-center timer-mrgn">
                     <span class="f-31 f-md-60 timerbg oswald w700">59</span>
                     <span class="f-14 f-md-18 w500 ">Mins</span>
                  </div>
                  <div class="timer-label text-center ">
                     <span class="f-31 f-md-60 timerbg oswald w700">37</span>
                     <span class="f-14 f-md-18 w500 ">Sec</span>
                  </div>
               </div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>
   <!-- CTA Button Section End -->

   <!-- Bonus #16 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 16</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="assets/images/bonus16.webp" class="img-fluid mx-auto d-block " alt="Bonus16">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w800 lh140 bonus-title-color">
                        Phoenix Podcast Studio
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>EVERYTHING you need to create professional Podcast - with this incredible Podcast software from PodcastDirector.net, you can not only create podcasts of your choice but you can also convert any textual information to a sound file for your podcast.</li>
                        <li>A podcast is basically an audio file that is stored on the Internet (An rss file that contains audio content) that people can download to their computers , iPODS and MP3 players and listen to whenever they want.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #16 End -->

   <!-- Bonus #17 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 17</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus17.webp" class="img-fluid mx-auto d-block " alt="Bonus17">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w800 lh140 bonus-title-color">
                        Easy Poll Creator
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Find Out What Your Website Visitors Think, Like, Need & Want! Create Your Own Branded, Fully-Customized, Attractive (Text/Image) Polls, Easily - in Minutes!</li>
                        <li>Knowing all this about your site visitors is the guaranteed way to increased site stickiness, repeat visitors, growing traffic, happy and satisfied visitors/customers, a mega opt-in list, and a lot more, right? What is the easiest and fastest way for you to know what your website visitors think, like, need and want?</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #17 End -->

   <!-- Bonus #18 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 18</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="assets/images/bonus18.webp" class="img-fluid mx-auto d-block " alt="Bonus18">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w800 lh140 bonus-title-color">
                        The Amazing Banner
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Discover The Innovative Banner Software That Will Force You to Stop Making Excuses and Begin Creating Banners Better Than the Pros - Sales Are Far Better Than Excuses Anyway! Now Get Your Banner Done In Three Steps!</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #18 End -->

   <!-- Bonus #19 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 19</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus19.webp" class="img-fluid mx-auto d-block " alt="Bonus19">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w800 lh140 bonus-title-color">
                        3D Box Templates V1
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>3D Box Templates allow you to create great looking eBox covers in just a few simple steps; <b>Upload template, Enter text, Set color and you're done!</b> </li>
                        <li>This package comes with 5 eBox templates.</li>
                        <li>Each template comes in 3 versions; <b>with text, without text and with .psd source you you can make changes!</b> </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #19 End -->

   <!-- Bonus #20 start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 20</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="assets/images/bonus20.webp" class="img-fluid mx-auto d-block " alt="Bonus20">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w800 lh140 bonus-title-color">
                        Miscellaneous Stock Photos
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li><b>Stock Images For You To Use In Your Projects And Your Clients Projects. Plus You Can Resell Them!</b></li>
                        <li>If you are an internet user, you may already noticed that images are everywhere. And if you an internet marketer, images is one of the best media to capture your audience's attention to click your ads or see what's your offer.</li>
                        <li>This is why successful online business owners invest money into this media to have a professionally-looking graphics that converts traffic into subscribers.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #20 End -->

   <!-- Huge Woth Section Start -->
   <div class="huge-area mt30 mt-md10">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-12 text-center">
               <div class="f-md-65 f-40 lh120 w700 white-clr">That’s Huge Worth of</div>
               <br>
               <div class="f-md-60 f-40 lh120 w800 orange-clr">$2285!</div>
            </div>
         </div>
      </div>
   </div>
   <!-- Huge Worth Section End -->

   <!-- text Area Start -->
   <div class="white-section pb0">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-12 text-center">
               <div class="f-md-40 f-28 lh140 w700">So what are you waiting for? You have a great opportunity <br class="hidden-xs"> ahead + My 20 Bonus Products are making it a <br class="hidden-xs"> completely NO Brainer!!</div>
            </div>
         </div>
      </div>
   </div>
   <!-- text Area End -->

   <!-- CTA Button Section Start -->
   <div class="cta-btn-section">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab VidBoxs + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-20 f-20 w500 text-center black">My Exclusive Bonuses Are Expiring in...</h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 col-md-10 mx-auto col-12 text-center">
               <div class="countdown counter-black">
                  <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">01</span><br><span class="f-14 f-md-18 w500  ">Days</span> </div>
                  <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">16</span><br><span class="f-14 f-md-18 w500 ">Hours</span> </div>
                  <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald w700">59</span><br><span class="f-14 f-md-18 w500 ">Mins</span> </div>
                  <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald w700">37</span><br><span class="f-14 f-md-18 w500 ">Sec</span> </div>
               </div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>
   <!-- CTA Button Section End -->


   <!--Footer Section Start -->
   <div class="footer-section">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <svg version="1.1" id="Layer_11" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 865.5 207.4" style="enable-background:new 0 0 865.5 207.4; max-height:50px" xml:space="preserve">
                  <style type="text/css">
                     .st00{fill:#FFFFFF;}
                     .st11{fill:url(#SVGID_1_);}
                     .st22{fill:url(#SVGID_00000003804682614402079440000013301449542194118298_);}
                     .st33{fill:url(#SVGID_00000106859270421856661840000011347085370863025833_);}
                     .st44{opacity:0.3;}
                     .st55{fill:#0F0F0F;}
                  </style>
                  <g>
                     <path class="st00" d="M482.4,27.5v105.7c0,17.9-10.1,33.4-24.9,41.3v-32.9c1-2.6,1.6-5.4,1.6-8.4s-0.6-5.8-1.6-8.4V27.5H482.4z"></path>
                     <path class="st00" d="M457.6,92c-6.5-3.4-13.9-5.4-21.8-5.4c-25.8,0-46.6,20.9-46.6,46.6c0,25.8,20.9,46.6,46.6,46.6
                     c7.9,0,15.3-1.9,21.8-5.4c14.8-7.8,24.9-23.4,24.9-41.3C482.4,115.3,472.4,99.8,457.6,92z M457.6,141.6c-2,5.3-5.9,9.6-10.9,12.2
                     c-3.2,1.7-7,2.7-10.9,2.7c-12.9,0-23.3-10.4-23.3-23.3s10.4-23.3,23.3-23.3c3.9,0,7.6,1,10.9,2.7c5,2.6,8.8,7,10.9,12.2
                     c1,2.6,1.6,5.4,1.6,8.4C459.1,136.2,458.6,139,457.6,141.6z"></path>
                     <path class="st00" d="M498,27.5v105.7c0,17.9,10.1,33.4,24.9,41.3v-32.9c-1-2.6-1.6-5.4-1.6-8.4s0.6-5.8,1.6-8.4V27.5H498z"></path>
                     <path class="st00" d="M498,133.2c0,17.9,10.1,33.4,24.9,41.3c6.5,3.4,13.9,5.4,21.8,5.4c25.8,0,46.6-20.9,46.6-46.6
                     c0-25.8-20.9-46.6-46.6-46.6c-7.9,0-15.3,1.9-21.8,5.4C508.1,99.8,498,115.3,498,133.2z M521.3,133.2c0-3,0.6-5.8,1.6-8.4
                     c2-5.3,5.9-9.6,10.9-12.2c3.2-1.7,7-2.7,10.9-2.7c12.9,0,23.3,10.4,23.3,23.3s-10.4,23.3-23.3,23.3c-3.9,0-7.6-1-10.9-2.7
                     c-5-2.6-8.8-7-10.9-12.2C521.8,139,521.3,136.2,521.3,133.2z"></path>
                     <path class="st00" d="M597.5,133.2c0,17.9,10.1,33.4,24.9,41.3c6.5,3.4,13.9,5.4,21.8,5.4c25.8,0,46.6-20.9,46.6-46.6
                     c0-25.8-20.9-46.6-46.6-46.6c-7.9,0-15.3,1.9-21.8,5.4C607.5,99.8,597.5,115.3,597.5,133.2z M620.8,133.2c0-3,0.6-5.8,1.6-8.4
                     c2-5.3,5.9-9.6,10.9-12.2c3.2-1.7,7-2.7,10.9-2.7c12.9,0,23.3,10.4,23.3,23.3s-10.4,23.3-23.3,23.3c-3.9,0-7.6-1-10.9-2.7
                     c-5-2.6-8.8-7-10.9-12.2C621.3,139,620.8,136.2,620.8,133.2z"></path>
                     <polygon class="st00" points="721.5,179.9 690.4,179.9 756.1,86.5 787.2,86.5 	"></polygon>
                     <polygon class="st00" points="756.1,179.9 787.2,179.9 721.5,86.5 690.4,86.5 	"></polygon>
                  
                     <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="291.6235" y1="980.703" x2="309.5574" y2="1019.9633" gradientTransform="matrix(1 0 0 1 0 -870)">
                        <stop offset="0" style="stop-color:#F40C28"></stop>
                        <stop offset="0.8911" style="stop-color:#FF2C50"></stop>
                     </linearGradient>
                     <polygon class="st11" points="264.4,179.9 278.5,150 308.3,86.5 339.4,86.5 295.5,179.9 292.5,179.9 	"></polygon>
                     <polygon class="st00" points="292.5,179.9 278.5,150 248.6,86.5 217.5,86.5 261.5,179.9 264.4,179.9 	"></polygon>
                     <rect x="351.9" y="86.6" class="st00" width="24.9" height="93.3"></rect>
                     <rect x="351.9" y="51.8" class="st00" width="24.9" height="24.9"></rect>
                     <g>
                        <path class="st00" d="M810.1,175.9c-5.9-2.7-10.5-6.3-14-10.9c-3.4-4.6-5.3-9.6-5.6-15.2h23c0.4,3.5,2.1,6.4,5.1,8.6
                        c3,2.3,6.7,3.4,11.2,3.4c4.3,0,7.8-0.9,10.2-2.6c2.4-1.7,3.7-4,3.7-6.7c0-2.9-1.5-5.1-4.5-6.6s-7.8-3.1-14.3-4.8
                        c-6.7-1.6-12.3-3.3-16.6-5.1c-4.3-1.7-8-4.4-11.1-8c-3.1-3.6-4.7-8.4-4.7-14.5c0-5,1.4-9.6,4.3-13.7c2.9-4.1,7-7.4,12.4-9.8
                        s11.7-3.6,19-3.6c10.8,0,19.4,2.7,25.8,8.1c6.4,5.4,10,12.6,10.6,21.8h-21.9c-0.3-3.6-1.8-6.4-4.5-8.6c-2.7-2.1-6.2-3.2-10.7-3.2
                        c-4.1,0-7.3,0.8-9.5,2.3s-3.3,3.6-3.3,6.4c0,3,1.5,5.4,4.6,6.9c3,1.6,7.8,3.2,14.2,4.8c6.5,1.6,11.9,3.3,16.2,5.1
                        c4.2,1.7,7.9,4.4,11,8.1c3.1,3.6,4.7,8.5,4.8,14.4c0,5.2-1.4,9.9-4.3,14c-2.9,4.1-7,7.4-12.4,9.7s-11.7,3.5-18.8,3.5
                        C822.6,179.9,815.9,178.5,810.1,175.9z"></path>
                     </g>
                  </g>
                  <g>
                     <g>
                        <linearGradient id="SVGID_00000090999748961388969530000010752564172096406444_" gradientUnits="userSpaceOnUse" x1="-1360.6178" y1="-1339.845" x2="-1175.7527" y2="-1283.7964" gradientTransform="matrix(0.6181 -0.7861 -0.7861 -0.6181 -176.8927 -1732.0651)">
                           <stop offset="0.2973" style="stop-color:#F40C28"></stop>
                           <stop offset="0.7365" style="stop-color:#FF2C50"></stop>
                        </linearGradient>
                        <path style="fill:url(#SVGID_00000090999748961388969530000010752564172096406444_);" d="M125.8,6.8c0.4,0.3,0.8,0.7,1.2,1
                        c6.2,5.4,9.8,12.7,10.8,20.3c1,8.1-1.2,16.6-6.6,23.6l-75.8,96.4c0,0-18.4,20.5-8.9,35l-32-25.2c-1.1-0.9-2.2-1.8-3.2-2.8
                        c-13.5-13.3-15.2-35-3.2-50.3l72.8-92.6C91.8-1.7,111.9-4.1,125.8,6.8z"></path>

                        <linearGradient id="SVGID_00000108307696041867937860000005451407988123938486_" gradientUnits="userSpaceOnUse" x1="-1216.8146" y1="1899.8297" x2="-973.6699" y2="1975.1873" gradientTransform="matrix(-0.6181 0.7861 0.7861 0.6181 -2080.1633 -197.5988)">
                           <stop offset="0.3687" style="stop-color:#F40C28"></stop>
                           <stop offset="1" style="stop-color:#FF2C50"></stop>
                        </linearGradient>
                        <path style="fill:url(#SVGID_00000108307696041867937860000005451407988123938486_);" d="M67.7,199.8c-0.4-0.3-0.8-0.7-1.2-1
                        c-6.2-5.4-9.8-12.7-10.8-20.3c-1-8.1,1.2-16.6,6.6-23.6l75.8-96.4c0,0,18.4-20.5,8.9-35l32,25.2c1.1,0.9,2.2,1.8,3.2,2.8
                        c13.5,13.3,15.2,35,3.2,50.3l-72.8,92.6C101.7,208.3,81.6,210.7,67.7,199.8z"></path>
                     </g>
                     <g class="st44">
                        <path class="st55" d="M68.8,69v78.4c0,8.7,9.8,13.7,16.8,8.6l54.6-39.2c5.9-4.2,5.9-13,0-17.3L85.6,60.4
                        C78.6,55.3,68.8,60.4,68.8,69z"></path>
                     </g>
                     <g>
                        <path class="st00" d="M66.8,66v78.4c0,8.7,9.8,13.7,16.8,8.6l54.6-39.2c5.9-4.2,5.9-13,0-17.3L83.6,57.4
                        C76.6,52.3,66.8,57.4,66.8,66z"></path>
                     </g>
                  </g>
               </svg>
               <div editabletype="text" class="f-16 f-md-18 w400 lh140 mt20 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
            </div>
            <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
               <div class="f-16 f-md-18 w400 lh140 white-clr text-xs-center">Copyright © VidBoxs</div>
               <ul class="footer-ul f-16 f-md-18 w400 white-clr text-center text-md-right">
                  <li><a href="https://support.bizomart.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                  <li><a href="http://www.vidboxs.com/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                  <li><a href="http://www.vidboxs.com/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                  <li><a href="http://www.vidboxs.com/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                  <li><a href="http://www.vidboxs.com/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                  <li><a href="http://www.vidboxs.com/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                  <li><a href="http://www.vidboxs.com/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
               </ul>
            </div>
         </div>
      </div>
   </div>
   <!--Footer Section End -->
   <!-- Back to top button -->
   <a id="button">
      <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 300.003 300.003" style="enable-background:new 0 0 300.003 300.003;" xml:space="preserve">
         <g>
            <g>
               <path d="M150,0C67.159,0,0.001,67.159,0.001,150c0,82.838,67.157,150.003,149.997,150.003S300.002,232.838,300.002,150    C300.002,67.159,232.842,0,150,0z M217.685,189.794c-5.47,5.467-14.338,5.47-19.81,0l-48.26-48.27l-48.522,48.516    c-5.467,5.467-14.338,5.47-19.81,0c-2.731-2.739-4.098-6.321-4.098-9.905s1.367-7.166,4.103-9.897l56.292-56.297    c0.539-0.838,1.157-1.637,1.888-2.368c2.796-2.796,6.476-4.142,10.146-4.077c3.662-0.062,7.348,1.281,10.141,4.08    c0.734,0.729,1.349,1.528,1.886,2.365l56.043,56.043C223.152,175.454,223.156,184.322,217.685,189.794z" />
            </g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
      </svg>
   </a>
   <!--Footer Section End -->
   <!-- timer --->
   <?php
   if ($now < $exp_date) {
   ?>
      <script type="text/javascript">
         // Count down milliseconds = server_end - server_now = client_end - client_now
         var server_end = <?php echo $exp_date; ?> * 1000;
         var server_now = <?php echo time(); ?> * 1000;
         var client_now = new Date().getTime();
         var end = server_end - server_now + client_now; // this is the real end time

         var noob = $('.countdown').length;

         var _second = 1000;
         var _minute = _second * 60;
         var _hour = _minute * 60;
         var _day = _hour * 24
         var timer;

         function showRemaining() {
            var now = new Date();
            var distance = end - now;
            if (distance < 0) {
               clearInterval(timer);
               document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
               return;
            }

            var days = Math.floor(distance / _day);
            var hours = Math.floor((distance % _day) / _hour);
            var minutes = Math.floor((distance % _hour) / _minute);
            var seconds = Math.floor((distance % _minute) / _second);
            if (days < 10) {
               days = "0" + days;
            }
            if (hours < 10) {
               hours = "0" + hours;
            }
            if (minutes < 10) {
               minutes = "0" + minutes;
            }
            if (seconds < 10) {
               seconds = "0" + seconds;
            }
            var i;
            var countdown = document.getElementsByClassName('countdown');
            for (i = 0; i < noob; i++) {
               countdown[i].innerHTML = '';

               if (days) {
                  countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">' + days + '</span><br><span class="f-14 f-md-18 ">Days</span> </div>';
               }

               countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">' + hours + '</span><br><span class="f-14 f-md-18 w500 ">Hours</span> </div>';

               countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald w700">' + minutes + '</span><br><span class="f-14 f-md-18 w500 ">Mins</span> </div>';

               countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald w700">' + seconds + '</span><br><span class="f-14 f-md-18 w500">Sec</span> </div>';
            }

         }
         timer = setInterval(showRemaining, 1000);
      </script>
   <?php
   } else {
      echo "Times Up";
   }
   ?>
   <!--- timer end-->

   
   </script>
</body>

</html>
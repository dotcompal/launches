<!Doctype html>
<html>
   <head>
      <title>VidBoxs Prelaunch</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=9">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
      <meta name="title" content="VidBoxs Prelaunch">
      <meta name="description" content="Creates YouTube Short Videos, Drive Tons of Free Viral Traffic & Profit">
      <meta name="keywords" content="VidBoxs">
      <meta property="og:image" content="https://www.vidboxs.com/prelaunch/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Ayush Jain">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="VidBoxs Prelaunch">
      <meta property="og:description" content="Creates YouTube Short Videos, Drive Tons of Free Viral Traffic & Profit">
      <meta property="og:image" content="https://www.vidboxs.com/prelaunch/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="VidBoxs Prelaunch">
      <meta property="twitter:description" content="Creates YouTube Short Videos, Drive Tons of Free Viral Traffic & Profit">
      <meta property="twitter:image" content="https://www.vidboxs.com/prelaunch/thumbnail.png">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link rel="icon" href="https://cdn.oppyo.com/launches/vidboxs/common_assets/images/favicon.png" type="image/png">
      <link href="https://fonts.googleapis.com/css2?family=Mulish:wght@300;400;500;600;700;800;900&display=swap" rel="stylesheet">
      <link rel="stylesheet" href="https://cdn.oppyo.com/launches/vidboxs/common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style.css" type="text/css">
      <link rel="stylesheet" href="assets/css/aweberform.css" type="text/css">
      <script src="../common_assets/js/jquery.min.js"></script>
      <script>
         function getUrlVars() {
             var vars = [],
                 hash;
             var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
             for (var i = 0; i < hashes.length; i++) {
                 hash = hashes[i].split('=');
                 vars.push(hash[0]);
                 vars[hash[0]] = hash[1];
             }
             return vars;
         }
         var first = getUrlVars()["aid"];
         //alert(first);
         document.addEventListener('DOMContentLoaded', (event) => {
                 document.getElementById('awf_field_aid').setAttribute('value', first);
             })
             //document.getElementById('myField').value = first;
      </script>
   </head>
   <body>
      <!--Header Section Start -->
      <div class="header-section">
         <div class="container">
            <div class="row">
               <div class="col-md-3 text-md-start text-center">
                  <svg version="1.1" id="Layer_11" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 865.5 207.4" style="enable-background:new 0 0 865.5 207.4; max-height:50px" xml:space="preserve">
                     <style type="text/css">
                       .st00{fill:#FFFFFF;}
                       .st11{fill:url(#SVGID_1_);}
                       .st22{fill:url(#SVGID_00000003804682614402079440000013301449542194118298_);}
                       .st33{fill:url(#SVGID_00000106859270421856661840000011347085370863025833_);}
                       .st44{opacity:0.3;}
                       .st55{fill:#0F0F0F;}
                     </style>
                     <g>
                        <path class="st00" d="M482.4,27.5v105.7c0,17.9-10.1,33.4-24.9,41.3v-32.9c1-2.6,1.6-5.4,1.6-8.4s-0.6-5.8-1.6-8.4V27.5H482.4z"/>
                        <path class="st00" d="M457.6,92c-6.5-3.4-13.9-5.4-21.8-5.4c-25.8,0-46.6,20.9-46.6,46.6c0,25.8,20.9,46.6,46.6,46.6
                        c7.9,0,15.3-1.9,21.8-5.4c14.8-7.8,24.9-23.4,24.9-41.3C482.4,115.3,472.4,99.8,457.6,92z M457.6,141.6c-2,5.3-5.9,9.6-10.9,12.2
                        c-3.2,1.7-7,2.7-10.9,2.7c-12.9,0-23.3-10.4-23.3-23.3s10.4-23.3,23.3-23.3c3.9,0,7.6,1,10.9,2.7c5,2.6,8.8,7,10.9,12.2
                        c1,2.6,1.6,5.4,1.6,8.4C459.1,136.2,458.6,139,457.6,141.6z"/>
                        <path class="st00" d="M498,27.5v105.7c0,17.9,10.1,33.4,24.9,41.3v-32.9c-1-2.6-1.6-5.4-1.6-8.4s0.6-5.8,1.6-8.4V27.5H498z"/>
                        <path class="st00" d="M498,133.2c0,17.9,10.1,33.4,24.9,41.3c6.5,3.4,13.9,5.4,21.8,5.4c25.8,0,46.6-20.9,46.6-46.6
                        c0-25.8-20.9-46.6-46.6-46.6c-7.9,0-15.3,1.9-21.8,5.4C508.1,99.8,498,115.3,498,133.2z M521.3,133.2c0-3,0.6-5.8,1.6-8.4
                        c2-5.3,5.9-9.6,10.9-12.2c3.2-1.7,7-2.7,10.9-2.7c12.9,0,23.3,10.4,23.3,23.3s-10.4,23.3-23.3,23.3c-3.9,0-7.6-1-10.9-2.7
                        c-5-2.6-8.8-7-10.9-12.2C521.8,139,521.3,136.2,521.3,133.2z"/>
                        <path class="st00" d="M597.5,133.2c0,17.9,10.1,33.4,24.9,41.3c6.5,3.4,13.9,5.4,21.8,5.4c25.8,0,46.6-20.9,46.6-46.6
                        c0-25.8-20.9-46.6-46.6-46.6c-7.9,0-15.3,1.9-21.8,5.4C607.5,99.8,597.5,115.3,597.5,133.2z M620.8,133.2c0-3,0.6-5.8,1.6-8.4
                        c2-5.3,5.9-9.6,10.9-12.2c3.2-1.7,7-2.7,10.9-2.7c12.9,0,23.3,10.4,23.3,23.3s-10.4,23.3-23.3,23.3c-3.9,0-7.6-1-10.9-2.7
                        c-5-2.6-8.8-7-10.9-12.2C621.3,139,620.8,136.2,620.8,133.2z"/>
                        <polygon class="st00" points="721.5,179.9 690.4,179.9 756.1,86.5 787.2,86.5 	"/>
                        <polygon class="st00" points="756.1,179.9 787.2,179.9 721.5,86.5 690.4,86.5 	"/>
                        <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="291.6235" y1="980.703" x2="309.5574" y2="1019.9633" gradientTransform="matrix(1 0 0 1 0 -870)">
                           <stop  offset="0" style="stop-color:#F40C28"/>
                           <stop  offset="0.8911" style="stop-color:#FF2C50"/>
                        </linearGradient>
                        <polygon class="st11" points="264.4,179.9 278.5,150 308.3,86.5 339.4,86.5 295.5,179.9 292.5,179.9 	"/>
                        <polygon class="st00" points="292.5,179.9 278.5,150 248.6,86.5 217.5,86.5 261.5,179.9 264.4,179.9 	"/>
                        <rect x="351.9" y="86.6" class="st00" width="24.9" height="93.3"/>
                        <rect x="351.9" y="51.8" class="st00" width="24.9" height="24.9"/>
                        <g>
                           <path class="st00" d="M810.1,175.9c-5.9-2.7-10.5-6.3-14-10.9c-3.4-4.6-5.3-9.6-5.6-15.2h23c0.4,3.5,2.1,6.4,5.1,8.6
                           c3,2.3,6.7,3.4,11.2,3.4c4.3,0,7.8-0.9,10.2-2.6c2.4-1.7,3.7-4,3.7-6.7c0-2.9-1.5-5.1-4.5-6.6s-7.8-3.1-14.3-4.8
                           c-6.7-1.6-12.3-3.3-16.6-5.1c-4.3-1.7-8-4.4-11.1-8c-3.1-3.6-4.7-8.4-4.7-14.5c0-5,1.4-9.6,4.3-13.7c2.9-4.1,7-7.4,12.4-9.8
                           s11.7-3.6,19-3.6c10.8,0,19.4,2.7,25.8,8.1c6.4,5.4,10,12.6,10.6,21.8h-21.9c-0.3-3.6-1.8-6.4-4.5-8.6c-2.7-2.1-6.2-3.2-10.7-3.2
                           c-4.1,0-7.3,0.8-9.5,2.3s-3.3,3.6-3.3,6.4c0,3,1.5,5.4,4.6,6.9c3,1.6,7.8,3.2,14.2,4.8c6.5,1.6,11.9,3.3,16.2,5.1
                           c4.2,1.7,7.9,4.4,11,8.1c3.1,3.6,4.7,8.5,4.8,14.4c0,5.2-1.4,9.9-4.3,14c-2.9,4.1-7,7.4-12.4,9.7s-11.7,3.5-18.8,3.5
                           C822.6,179.9,815.9,178.5,810.1,175.9z"/>
                        </g>
                     </g>
                     <g>
                        <g>
                           <linearGradient id="SVGID_00000090999748961388969530000010752564172096406444_" gradientUnits="userSpaceOnUse" x1="-1360.6178" y1="-1339.845" x2="-1175.7527" y2="-1283.7964" gradientTransform="matrix(0.6181 -0.7861 -0.7861 -0.6181 -176.8927 -1732.0651)">
                              <stop  offset="0.2973" style="stop-color:#F40C28"/>
                              <stop  offset="0.7365" style="stop-color:#FF2C50"/>
                           </linearGradient>
                           <path style="fill:url(#SVGID_00000090999748961388969530000010752564172096406444_);" d="M125.8,6.8c0.4,0.3,0.8,0.7,1.2,1
                           c6.2,5.4,9.8,12.7,10.8,20.3c1,8.1-1.2,16.6-6.6,23.6l-75.8,96.4c0,0-18.4,20.5-8.9,35l-32-25.2c-1.1-0.9-2.2-1.8-3.2-2.8
                           c-13.5-13.3-15.2-35-3.2-50.3l72.8-92.6C91.8-1.7,111.9-4.1,125.8,6.8z"/>
                           <linearGradient id="SVGID_00000108307696041867937860000005451407988123938486_" gradientUnits="userSpaceOnUse" x1="-1216.8146" y1="1899.8297" x2="-973.6699" y2="1975.1873" gradientTransform="matrix(-0.6181 0.7861 0.7861 0.6181 -2080.1633 -197.5988)">
                              <stop  offset="0.3687" style="stop-color:#F40C28"/>
                              <stop  offset="1" style="stop-color:#FF2C50"/>
                           </linearGradient>
                           <path style="fill:url(#SVGID_00000108307696041867937860000005451407988123938486_);" d="M67.7,199.8c-0.4-0.3-0.8-0.7-1.2-1
                           c-6.2-5.4-9.8-12.7-10.8-20.3c-1-8.1,1.2-16.6,6.6-23.6l75.8-96.4c0,0,18.4-20.5,8.9-35l32,25.2c1.1,0.9,2.2,1.8,3.2,2.8
                           c13.5,13.3,15.2,35,3.2,50.3l-72.8,92.6C101.7,208.3,81.6,210.7,67.7,199.8z"/>
                        </g>
                        <g class="st44">
                           <path class="st55" d="M68.8,69v78.4c0,8.7,9.8,13.7,16.8,8.6l54.6-39.2c5.9-4.2,5.9-13,0-17.3L85.6,60.4
                           C78.6,55.3,68.8,60.4,68.8,69z"/>
                        </g>
                        <g>
                           <path class="st00" d="M66.8,66v78.4c0,8.7,9.8,13.7,16.8,8.6l54.6-39.2c5.9-4.2,5.9-13,0-17.3L83.6,57.4
                           C76.6,52.3,66.8,57.4,66.8,66z"/>
                        </g>
                     </g>
                  </svg>   
               </div>
               <div class="col-md-9  mt-md0 mt15">
                  <ul class="leader-ul f-16 f-md-18 w400 white-clr text-md-end text-center ">
                     <li>
                        <a href="#book-seat1" class="affiliate-link-btn">Subscribe To EarlyBird VIP List</a>
                     </li>
                  </ul>
               </div>
               <div class="col-12 text-center mt20 mt-md50">
                  <div class="pre-heading f-18 f-md-22 w800 lh140 white-clr">
                     Copy Our Secret Formula To PULL Free Viral Traffic From YouTube Every Day!
                  </div>
               </div>
               <div class="col-12 mt-md25 mt20 f-md-50 f-28 w800 text-center white-clr lh140 text-shadow">
                  A Push-Button Technology <span class="orange-clr"> Creates <br class="d-none d-md-block">  YouTube Short Videos, Drive Tons of Free <br class="d-none d-md-block"> Viral Traffic & Profit</span>
               </div>
               <div class="col-12 mt-md25 mt20 f-18 f-md-24 w600 text-center lh140 white-clr text-capitalize">
                  Create Unlimited YouTube Shorts | No Camera & Recording | No Complex Editing Needed |<br class="d-none d-md-block"> No Paid Traffic | No Tech Hassles | Commercial License Included | 100% Newbie Friendly
               </div>
            </div>
            <div class="row d-flex align-items-center flex-wrap mt20 mt-md40">
               <div class="col-md-10 col-12 mx-auto">
			         <img src="assets/images/product-box.webp" alt="ProductBOX " class="img-fluid d-block mx-auto" />
                  <!--<div class="responsive-video">
                     <iframe src="https://webpull.dotcompal.com/video/embed/9vxq6c88sw" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                        box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                  </div>-->
               </div>
            </div>
         </div>
      </div>
      <!--Header Section End -->

      <!-- Second-Section Start -->
      <div class="second-section">
         <div class="container">
            <div class="row">
               <div class="col-12 z-index-9">
                  <div class="row header-list-block">
                     <div class="col-12 col-md-6">
                        <ul class="features-list pl0 f-18 f-md-20 lh140 w400 black-clr text-capitalize">
                           <li>
                              Tap into the <span class="w700"> Fast-Growing $200 Billion Video Marketing Industry </span> to Make Easy Income
                           </li>
                           <li>
                              <span class="w700">Create Unlimited YouTube Short Videos</span> for Any Niche and Offer
                           </li>
                           <li>
                              <span class="w700">Create Videos from Any Text or Script</span> in just 3 Simple Clicks
                           </li>
                           <li>
                              Create Videos <span class="w700">Using your Own Images or Search Images</span> with keywords
                           </li>
                           <li>
                              <span class="w700">Drive Unlimited Traffic & Sales</span> to Any Offer or Page
                           </li>
                        </ul>
                     </div>
                     <div class="col-12 col-md-6 mt10 mt-md0">
                        <ul class="features-list pl0 f-18 f-md-20 lh140 w400 black-clr text-capitalize">
                           <li>Create Videos by <span class="w700">Using Your Own Video Clips</span> or Use Stock Videos</li>
                           <li><span class="w700">Add Background Music and/or Voiceover</span> to Any Video</li>
                           <li>Inbuilt <span class="w700">Voiceover Creator</span> with 150+ Human Voice in 30+ Languages</li>
                           <li><span class="w700">No Camera</span>  Recording, <span class="w700">No Voice</span> , and <span class="w700">No Complex Editing</span> Required</li>
                           <li><span class="w700">100% Newbie Friendly,</span>  Required No Prior Experience or Tech Skills</li>
                        </ul>
                     </div>
                     <div class="col-12 mt10 mt-md20">
                        <ul class="features-list pl0 f-18 f-md-20 lh140 w400 black-clr text-capitalize">
                           <li><span class="w700">Free Commercial License</span> to Sell Video Services for High Profits</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div> 
            <div class="row mt20 mt-md70">
               <div class="col-12">
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 text-center">
                        <a href="#book-seat1" class="cta-link-btn">Subscribe To EarlyBird VIP List</a>
                     </div>
                  </div>
                  <div class="col-12 mx-auto mt20 mt-md40 f-18 f-md-20 w400 white-clr text-left lh160 text-md-center">
                     <ul class="ul-tick-icon1 p0 m0">
                        <li>Don't Pay $47/M, Get All Benefits For $17 One Time Only</li>
                        <li>Also Get A Chance To Win 10 FREE Copies</li>
                     </ul>
                </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Second-Second-End -->

      <!-------Form Section Start------------->
      <div class="form-sec">
         <div class="container">
            <div class="row mt30 mt-md70 mb30 mb-md70">
               <div class="col-12 col-md-12 mx-auto" id="form">
                  <div class="auto_responder_form inp-phold formbg" id="book-seat1">
                     <div class="f-28 f-md-40 w800 text-center lh140 black-clr" editabletype="text" style="z-index:10;">
                        Awesome! You're Almost Done.							
                     </div>
                     <div class="col-12 f-md-20 f-16 w600 lh140 mx-auto my20 text-center">
                        Fill up the details below
                     </div>
                     <!-- Aweber Form Code -->
                     <div class="col-12 p0 mt15 mt-md25" editabletype="form" style="z-index:10">
                        <!-- Aweber Form Code -->
                        <form method="post" class="af-form-wrapper register-form-style1 " accept-charset="UTF-8" action="https://www.aweber.com/scripts/addlead.pl" style="z-index: 10;">
                           <div style="display: none;">
                              <input type="hidden" name="meta_web_form_id" value="1914364641">
                              <input type="hidden" name="meta_split_id" value="">
                              <input type="hidden" name="listname" value="awlist6318712">
                              <input type="hidden" name="redirect" value="https://www.vidboxs.com/prelaunch-thankyou" id="redirect_6cd30a0aed0c2d7c98670b17f7027610">
                              <input type="hidden" name="meta_adtracking" value="My_Web_Form_2">
                              <input type="hidden" name="meta_message" value="1">
                              <input type="hidden" name="meta_required" value="name,email">
                              <input type="hidden" name="meta_tooltip" value="">
                           </div>
                           <div id="af-form-1914364641" class="af-form">
                              <div id="af-body-1914364641" class="af-body af-standards">
                                 <div class="af-element width-form">
                                    <label class="previewLabel" for="awf_field-114582649"></label>
                                    <div class="af-textWrap">
                                       <div class="input-type" style="z-index: 11;">
                                          <!--<span class="input-group-addon border1px"><i class="fa fa-user" aria-hidden="true"></i></span>-->
                                          <input id="awf_field-114582649" type="text" name="name" class="text form-control custom-input input-field" value="" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="500" placeholder="Your Name">
                                       </div>
                                    </div>
                                    <div class="af-clear bottom-margin"></div>
                                 </div>
                                 <div class="af-element width-form">
                                    <label class="previewLabel" for="awf_field-114582650"> </label>
                                    <div class="af-textWrap">
                                       <div class="input-type" style="z-index: 11;">
                                          <!--<span class="input-group-addon border1px"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>-->
                                          <input class="text form-control custom-input input-field" id="awf_field-114582650" type="text" name="email" value="" tabindex="501" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " placeholder="Your Email">
                                       </div>
                                    </div>
                                    <div class="af-clear bottom-margin"></div>
                                 </div>
                                 <input type="hidden" id="awf_field_aid" class="text" name="custom aid" value="undefined" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="502">
                                 <div class="af-element buttonContainer button-type register-btn1 f-18 f-md-21 width-form mt-md0">
                                    <input name="submit" id="af-submit-image-348552691" type="submit" class="image" style="max-width: 100%;" alt="Submit Form" tabindex="503" value="Subscribe To EarlyBird VIP List">
                                    <div class="af-clear bottom-margin"></div>
                                 </div>
                              </div>
                           </div>
                           <div style="display: none;"><img src="https://forms.aweber.com/form/displays.htm?id=7OycjOwsrEw=" alt=""></div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
     <!-------Form Section End------------->

     <!-- Footer-Section-Start -->
      <div class="footer-section">
         <div class="container ">
            <div class="row">
               <div class="col-12 text-center">
                  <svg version="1.1" id="Layer_11" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                  viewBox="0 0 865.5 207.4" style="enable-background:new 0 0 865.5 207.4; max-height:50px" xml:space="preserve">
                     <style type="text/css">
                        .st00{fill:#FFFFFF;}
                        .st11{fill:url(#SVGID_1_);}
                        .st22{fill:url(#SVGID_00000003804682614402079440000013301449542194118298_);}
                        .st33{fill:url(#SVGID_00000106859270421856661840000011347085370863025833_);}
                        .st44{opacity:0.3;}
                        .st55{fill:#0F0F0F;}
                     </style>
                     <g>
                        <path class="st00" d="M482.4,27.5v105.7c0,17.9-10.1,33.4-24.9,41.3v-32.9c1-2.6,1.6-5.4,1.6-8.4s-0.6-5.8-1.6-8.4V27.5H482.4z"/>
                        <path class="st00" d="M457.6,92c-6.5-3.4-13.9-5.4-21.8-5.4c-25.8,0-46.6,20.9-46.6,46.6c0,25.8,20.9,46.6,46.6,46.6
                        c7.9,0,15.3-1.9,21.8-5.4c14.8-7.8,24.9-23.4,24.9-41.3C482.4,115.3,472.4,99.8,457.6,92z M457.6,141.6c-2,5.3-5.9,9.6-10.9,12.2
                        c-3.2,1.7-7,2.7-10.9,2.7c-12.9,0-23.3-10.4-23.3-23.3s10.4-23.3,23.3-23.3c3.9,0,7.6,1,10.9,2.7c5,2.6,8.8,7,10.9,12.2
                        c1,2.6,1.6,5.4,1.6,8.4C459.1,136.2,458.6,139,457.6,141.6z"/>
                        <path class="st00" d="M498,27.5v105.7c0,17.9,10.1,33.4,24.9,41.3v-32.9c-1-2.6-1.6-5.4-1.6-8.4s0.6-5.8,1.6-8.4V27.5H498z"/>
                        <path class="st00" d="M498,133.2c0,17.9,10.1,33.4,24.9,41.3c6.5,3.4,13.9,5.4,21.8,5.4c25.8,0,46.6-20.9,46.6-46.6
                        c0-25.8-20.9-46.6-46.6-46.6c-7.9,0-15.3,1.9-21.8,5.4C508.1,99.8,498,115.3,498,133.2z M521.3,133.2c0-3,0.6-5.8,1.6-8.4
                        c2-5.3,5.9-9.6,10.9-12.2c3.2-1.7,7-2.7,10.9-2.7c12.9,0,23.3,10.4,23.3,23.3s-10.4,23.3-23.3,23.3c-3.9,0-7.6-1-10.9-2.7
                        c-5-2.6-8.8-7-10.9-12.2C521.8,139,521.3,136.2,521.3,133.2z"/>
                        <path class="st00" d="M597.5,133.2c0,17.9,10.1,33.4,24.9,41.3c6.5,3.4,13.9,5.4,21.8,5.4c25.8,0,46.6-20.9,46.6-46.6
                        c0-25.8-20.9-46.6-46.6-46.6c-7.9,0-15.3,1.9-21.8,5.4C607.5,99.8,597.5,115.3,597.5,133.2z M620.8,133.2c0-3,0.6-5.8,1.6-8.4
                        c2-5.3,5.9-9.6,10.9-12.2c3.2-1.7,7-2.7,10.9-2.7c12.9,0,23.3,10.4,23.3,23.3s-10.4,23.3-23.3,23.3c-3.9,0-7.6-1-10.9-2.7
                        c-5-2.6-8.8-7-10.9-12.2C621.3,139,620.8,136.2,620.8,133.2z"/>
                        <polygon class="st00" points="721.5,179.9 690.4,179.9 756.1,86.5 787.2,86.5 	"/>
                        <polygon class="st00" points="756.1,179.9 787.2,179.9 721.5,86.5 690.4,86.5 	"/>
                        <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="291.6235" y1="980.703" x2="309.5574" y2="1019.9633" gradientTransform="matrix(1 0 0 1 0 -870)">
                           <stop  offset="0" style="stop-color:#F40C28"/>
                           <stop  offset="0.8911" style="stop-color:#FF2C50"/>
                        </linearGradient>
                        <polygon class="st11" points="264.4,179.9 278.5,150 308.3,86.5 339.4,86.5 295.5,179.9 292.5,179.9 	"/>
                        <polygon class="st00" points="292.5,179.9 278.5,150 248.6,86.5 217.5,86.5 261.5,179.9 264.4,179.9 	"/>
                        <rect x="351.9" y="86.6" class="st00" width="24.9" height="93.3"/>
                        <rect x="351.9" y="51.8" class="st00" width="24.9" height="24.9"/>
                        <g>
                           <path class="st00" d="M810.1,175.9c-5.9-2.7-10.5-6.3-14-10.9c-3.4-4.6-5.3-9.6-5.6-15.2h23c0.4,3.5,2.1,6.4,5.1,8.6
                           c3,2.3,6.7,3.4,11.2,3.4c4.3,0,7.8-0.9,10.2-2.6c2.4-1.7,3.7-4,3.7-6.7c0-2.9-1.5-5.1-4.5-6.6s-7.8-3.1-14.3-4.8
                           c-6.7-1.6-12.3-3.3-16.6-5.1c-4.3-1.7-8-4.4-11.1-8c-3.1-3.6-4.7-8.4-4.7-14.5c0-5,1.4-9.6,4.3-13.7c2.9-4.1,7-7.4,12.4-9.8
                           s11.7-3.6,19-3.6c10.8,0,19.4,2.7,25.8,8.1c6.4,5.4,10,12.6,10.6,21.8h-21.9c-0.3-3.6-1.8-6.4-4.5-8.6c-2.7-2.1-6.2-3.2-10.7-3.2
                           c-4.1,0-7.3,0.8-9.5,2.3s-3.3,3.6-3.3,6.4c0,3,1.5,5.4,4.6,6.9c3,1.6,7.8,3.2,14.2,4.8c6.5,1.6,11.9,3.3,16.2,5.1
                           c4.2,1.7,7.9,4.4,11,8.1c3.1,3.6,4.7,8.5,4.8,14.4c0,5.2-1.4,9.9-4.3,14c-2.9,4.1-7,7.4-12.4,9.7s-11.7,3.5-18.8,3.5
                           C822.6,179.9,815.9,178.5,810.1,175.9z"/>
                        </g>
                     </g>
                     <g>
                        <g>
                           <linearGradient id="SVGID_00000090999748961388969530000010752564172096406444_" gradientUnits="userSpaceOnUse" x1="-1360.6178" y1="-1339.845" x2="-1175.7527" y2="-1283.7964" gradientTransform="matrix(0.6181 -0.7861 -0.7861 -0.6181 -176.8927 -1732.0651)">
                              <stop  offset="0.2973" style="stop-color:#F40C28"/>
                              <stop  offset="0.7365" style="stop-color:#FF2C50"/>
                           </linearGradient>
                           <path style="fill:url(#SVGID_00000090999748961388969530000010752564172096406444_);" d="M125.8,6.8c0.4,0.3,0.8,0.7,1.2,1
                           c6.2,5.4,9.8,12.7,10.8,20.3c1,8.1-1.2,16.6-6.6,23.6l-75.8,96.4c0,0-18.4,20.5-8.9,35l-32-25.2c-1.1-0.9-2.2-1.8-3.2-2.8
                           c-13.5-13.3-15.2-35-3.2-50.3l72.8-92.6C91.8-1.7,111.9-4.1,125.8,6.8z"/>
                           <linearGradient id="SVGID_00000108307696041867937860000005451407988123938486_" gradientUnits="userSpaceOnUse" x1="-1216.8146" y1="1899.8297" x2="-973.6699" y2="1975.1873" gradientTransform="matrix(-0.6181 0.7861 0.7861 0.6181 -2080.1633 -197.5988)">
                              <stop  offset="0.3687" style="stop-color:#F40C28"/>
                              <stop  offset="1" style="stop-color:#FF2C50"/>
                           </linearGradient>
                           <path style="fill:url(#SVGID_00000108307696041867937860000005451407988123938486_);" d="M67.7,199.8c-0.4-0.3-0.8-0.7-1.2-1
                           c-6.2-5.4-9.8-12.7-10.8-20.3c-1-8.1,1.2-16.6,6.6-23.6l75.8-96.4c0,0,18.4-20.5,8.9-35l32,25.2c1.1,0.9,2.2,1.8,3.2,2.8
                           c13.5,13.3,15.2,35,3.2,50.3l-72.8,92.6C101.7,208.3,81.6,210.7,67.7,199.8z"/>
                        </g>
                        <g class="st44">
                           <path class="st55" d="M68.8,69v78.4c0,8.7,9.8,13.7,16.8,8.6l54.6-39.2c5.9-4.2,5.9-13,0-17.3L85.6,60.4
                           C78.6,55.3,68.8,60.4,68.8,69z"/>
                        </g>
                        <g>
                           <path class="st00" d="M66.8,66v78.4c0,8.7,9.8,13.7,16.8,8.6l54.6-39.2c5.9-4.2,5.9-13,0-17.3L83.6,57.4
                           C76.6,52.3,66.8,57.4,66.8,66z"/>
                        </g>
                     </g>
                  </svg>
                  <div class="f-14 f-md-16 w400 mt30 mt-md40 lh140 white-clr text-center" >
                     <span class="w600">Note:</span> This site is not a part of the Facebook website or Facebook Inc. Additionally, this site is NOT endorsed by Facebook in any way. FACEBOOK & INSTAGRAM are the trademarks of FACEBOOK, Inc.
                  </div>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-md-18 f-16 w400 lh140 white-clr text-xs-center">Copyright © VidBoxs 2022</div>
                  <ul class="footer-ul w400 f-md-18 f-16 white-clr text-center text-md-right">
                     <li><a href="https://support.bizomart.com/hc/en-us " class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://vidboxs.com/legal/privacy-policy.html " class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://vidboxs.com/legal/terms-of-service.html " class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://vidboxs.com/legal/disclaimer.html " class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://vidboxs.com/legal/gdpr.html " class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://vidboxs.com/legal/dmca.html " class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://vidboxs.com/legal/anti-spam.html " class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!-- Footer-Section-End -->
      <script type="text/javascript">
      // Special handling for in-app browsers that don't always support new windows
      (function() {
         function browserSupportsNewWindows(userAgent) {
            var rules = [
                  'FBIOS',
                  'Twitter for iPhone',
                  'WebView',
                  '(iPhone|iPod|iPad)(?!.*Safari\/)',
                  'Android.*(wv|\.0\.0\.0)'
            ];
            var pattern = new RegExp('(' + rules.join('|') + ')', 'ig');
            return !pattern.test(userAgent);
         }

         if (!browserSupportsNewWindows(navigator.userAgent || navigator.vendor || window.opera)) {
            document.getElementById('af-form-1914364641').parentElement.removeAttribute('target');
         }
      })();
      </script>
      <script type="text/javascript">

         (function() {
            var IE = /*@cc_on!@*/false;
            if (!IE) { return; }
            if (document.compatMode && document.compatMode == 'BackCompat') {
                  if (document.getElementById("af-form-1914364641")) {
                     document.getElementById("af-form-1914364641").className = 'af-form af-quirksMode';
                  }
                  if (document.getElementById("af-body-1914364641")) {
                     document.getElementById("af-body-1914364641").className = "af-body inline af-quirksMode";
                  }
                  if (document.getElementById("af-header-1914364641")) {
                     document.getElementById("af-header-1914364641").className = "af-header af-quirksMode";
                  }
                  if (document.getElementById("af-footer-1914364641")) {
                     document.getElementById("af-footer-1914364641").className = "af-footer af-quirksMode";
                  }
            }
         })();

      </script>


   </body>
</html>
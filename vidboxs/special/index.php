<!Doctype html>
<html>
   <head>
      <title>VidBoxs - Creates & Publish YouTube Shorts in Just 60 Seconds…</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=9">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
      <meta name="title" content="VidBoxs Special">
      <meta name="description" content="Creates & Publish YouTube Shorts in Just 60 Seconds… And Drive Tons of FREE Traffic, Sales & Profits Hands-free">
      <meta name="keywords" content="VidBoxs">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta property="og:image" content="https://www.vidboxs.co/special/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Ayush Jain">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="VidBoxs Special">
      <meta property="og:description" content="Creates & Publish YouTube Shorts in Just 60 Seconds… And Drive Tons of FREE Traffic, Sales & Profits Hands-free">
      <meta property="og:image" content="https://www.vidboxs.co/special/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="VidBoxs Special">
      <meta property="twitter:description" content="Creates & Publish YouTube Shorts in Just 60 Seconds… And Drive Tons of FREE Traffic, Sales & Profits Hands-free">
      <meta property="twitter:image" content="https://www.vidboxs.co/special/thumbnail.png">
      <link rel="icon" href="https://cdn.oppyo.com/launches/vidboxs/common_assets/images/favicon.png" type="image/png">
      <link href="https://fonts.googleapis.com/css2?family=Mulish:wght@300;400;500;600;700;800;900&display=swap" rel="stylesheet">
      <link rel="stylesheet" href="https://cdn.oppyo.com/launches/vidboxs/common_assets/css/bootstrap.min.css"
         type="text/css">
      <link rel="stylesheet" href="assets/css/style.css" type="text/css">
      <link rel="stylesheet" href="assets/css/timer.css" type="text/css">
      <script src="https://cdn.oppyo.com/launches/vidboxs/common_assets/js/jquery.min.js"></script>
   <script src="https://cdn.oppyo.com/launches/vidboxs/common_assets/js/bootstrap.min.js"></script>

   <script>
(function(w, i, d, g, e, t, s) {
	if(window.businessDomain != undefined){
		console.log("Your page have duplicate embed code. Please check it.");
		return false;
	}
	businessDomain = 'vidboxs';
	allowedDomain = 'vidboxs.com';
	if(!window.location.hostname.includes(allowedDomain)){
		console.log("Your page have not authorized. Please check it.");
		return false;
	}
	console.log("Your script is ready...");
	w[d] = w[d] || [];
	t = i.createElement(g);
	t.async = 1;
	t.src = e;
	s = i.getElementsByTagName(g)[0];
	s.parentNode.insertBefore(t, s);
})(window, document, '_gscq', 'script', 'https://cdn.staticdcp.com/apps/engage/smart_engage/js/config-loader.js');
</script>
   </head>
   <body>
      <!-- Header Section Start -->
      <div class="header-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row align-items-center">
                     <div class="col-md-3 text-center text-md-start">
                        <svg version="1.1" id="Layer_11" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 865.5 207.4" style="enable-background:new 0 0 865.5 207.4; max-height:50px" xml:space="preserve">
                           <style type="text/css">
                              .st00{fill:#FFFFFF;}
                              .st11{fill:url(#SVGID_1_);}
                              .st22{fill:url(#SVGID_00000003804682614402079440000013301449542194118298_);}
                              .st33{fill:url(#SVGID_00000106859270421856661840000011347085370863025833_);}
                              .st44{opacity:0.3;}
                              .st55{fill:#0F0F0F;}
                           </style>
                           <g>
                              <path class="st00" d="M482.4,27.5v105.7c0,17.9-10.1,33.4-24.9,41.3v-32.9c1-2.6,1.6-5.4,1.6-8.4s-0.6-5.8-1.6-8.4V27.5H482.4z"></path>
                              <path class="st00" d="M457.6,92c-6.5-3.4-13.9-5.4-21.8-5.4c-25.8,0-46.6,20.9-46.6,46.6c0,25.8,20.9,46.6,46.6,46.6
                                 c7.9,0,15.3-1.9,21.8-5.4c14.8-7.8,24.9-23.4,24.9-41.3C482.4,115.3,472.4,99.8,457.6,92z M457.6,141.6c-2,5.3-5.9,9.6-10.9,12.2
                                 c-3.2,1.7-7,2.7-10.9,2.7c-12.9,0-23.3-10.4-23.3-23.3s10.4-23.3,23.3-23.3c3.9,0,7.6,1,10.9,2.7c5,2.6,8.8,7,10.9,12.2
                                 c1,2.6,1.6,5.4,1.6,8.4C459.1,136.2,458.6,139,457.6,141.6z"></path>
                              <path class="st00" d="M498,27.5v105.7c0,17.9,10.1,33.4,24.9,41.3v-32.9c-1-2.6-1.6-5.4-1.6-8.4s0.6-5.8,1.6-8.4V27.5H498z"></path>
                              <path class="st00" d="M498,133.2c0,17.9,10.1,33.4,24.9,41.3c6.5,3.4,13.9,5.4,21.8,5.4c25.8,0,46.6-20.9,46.6-46.6
                                 c0-25.8-20.9-46.6-46.6-46.6c-7.9,0-15.3,1.9-21.8,5.4C508.1,99.8,498,115.3,498,133.2z M521.3,133.2c0-3,0.6-5.8,1.6-8.4
                                 c2-5.3,5.9-9.6,10.9-12.2c3.2-1.7,7-2.7,10.9-2.7c12.9,0,23.3,10.4,23.3,23.3s-10.4,23.3-23.3,23.3c-3.9,0-7.6-1-10.9-2.7
                                 c-5-2.6-8.8-7-10.9-12.2C521.8,139,521.3,136.2,521.3,133.2z"></path>
                              <path class="st00" d="M597.5,133.2c0,17.9,10.1,33.4,24.9,41.3c6.5,3.4,13.9,5.4,21.8,5.4c25.8,0,46.6-20.9,46.6-46.6
                                 c0-25.8-20.9-46.6-46.6-46.6c-7.9,0-15.3,1.9-21.8,5.4C607.5,99.8,597.5,115.3,597.5,133.2z M620.8,133.2c0-3,0.6-5.8,1.6-8.4
                                 c2-5.3,5.9-9.6,10.9-12.2c3.2-1.7,7-2.7,10.9-2.7c12.9,0,23.3,10.4,23.3,23.3s-10.4,23.3-23.3,23.3c-3.9,0-7.6-1-10.9-2.7
                                 c-5-2.6-8.8-7-10.9-12.2C621.3,139,620.8,136.2,620.8,133.2z"></path>
                              <polygon class="st00" points="721.5,179.9 690.4,179.9 756.1,86.5 787.2,86.5 	"></polygon>
                              <polygon class="st00" points="756.1,179.9 787.2,179.9 721.5,86.5 690.4,86.5 	"></polygon>
                              <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="291.6235" y1="980.703" x2="309.5574" y2="1019.9633" gradientTransform="matrix(1 0 0 1 0 -870)">
                                 <stop offset="0" style="stop-color:#F40C28"></stop>
                                 <stop offset="0.8911" style="stop-color:#FF2C50"></stop>
                              </linearGradient>
                              <polygon class="st11" points="264.4,179.9 278.5,150 308.3,86.5 339.4,86.5 295.5,179.9 292.5,179.9 	"></polygon>
                              <polygon class="st00" points="292.5,179.9 278.5,150 248.6,86.5 217.5,86.5 261.5,179.9 264.4,179.9 	"></polygon>
                              <rect x="351.9" y="86.6" class="st00" width="24.9" height="93.3"></rect>
                              <rect x="351.9" y="51.8" class="st00" width="24.9" height="24.9"></rect>
                              <g>
                                 <path class="st00" d="M810.1,175.9c-5.9-2.7-10.5-6.3-14-10.9c-3.4-4.6-5.3-9.6-5.6-15.2h23c0.4,3.5,2.1,6.4,5.1,8.6
                                    c3,2.3,6.7,3.4,11.2,3.4c4.3,0,7.8-0.9,10.2-2.6c2.4-1.7,3.7-4,3.7-6.7c0-2.9-1.5-5.1-4.5-6.6s-7.8-3.1-14.3-4.8
                                    c-6.7-1.6-12.3-3.3-16.6-5.1c-4.3-1.7-8-4.4-11.1-8c-3.1-3.6-4.7-8.4-4.7-14.5c0-5,1.4-9.6,4.3-13.7c2.9-4.1,7-7.4,12.4-9.8
                                    s11.7-3.6,19-3.6c10.8,0,19.4,2.7,25.8,8.1c6.4,5.4,10,12.6,10.6,21.8h-21.9c-0.3-3.6-1.8-6.4-4.5-8.6c-2.7-2.1-6.2-3.2-10.7-3.2
                                    c-4.1,0-7.3,0.8-9.5,2.3s-3.3,3.6-3.3,6.4c0,3,1.5,5.4,4.6,6.9c3,1.6,7.8,3.2,14.2,4.8c6.5,1.6,11.9,3.3,16.2,5.1
                                    c4.2,1.7,7.9,4.4,11,8.1c3.1,3.6,4.7,8.5,4.8,14.4c0,5.2-1.4,9.9-4.3,14c-2.9,4.1-7,7.4-12.4,9.7s-11.7,3.5-18.8,3.5
                                    C822.6,179.9,815.9,178.5,810.1,175.9z"></path>
                              </g>
                           </g>
                           <g>
                              <g>
                                 <linearGradient id="SVGID_00000090999748961388969530000010752564172096406444_" gradientUnits="userSpaceOnUse" x1="-1360.6178" y1="-1339.845" x2="-1175.7527" y2="-1283.7964" gradientTransform="matrix(0.6181 -0.7861 -0.7861 -0.6181 -176.8927 -1732.0651)">
                                    <stop offset="0.2973" style="stop-color:#F40C28"></stop>
                                    <stop offset="0.7365" style="stop-color:#FF2C50"></stop>
                                 </linearGradient>
                                 <path style="fill:url(#SVGID_00000090999748961388969530000010752564172096406444_);" d="M125.8,6.8c0.4,0.3,0.8,0.7,1.2,1
                                    c6.2,5.4,9.8,12.7,10.8,20.3c1,8.1-1.2,16.6-6.6,23.6l-75.8,96.4c0,0-18.4,20.5-8.9,35l-32-25.2c-1.1-0.9-2.2-1.8-3.2-2.8
                                    c-13.5-13.3-15.2-35-3.2-50.3l72.8-92.6C91.8-1.7,111.9-4.1,125.8,6.8z"></path>
                                 <linearGradient id="SVGID_00000108307696041867937860000005451407988123938486_" gradientUnits="userSpaceOnUse" x1="-1216.8146" y1="1899.8297" x2="-973.6699" y2="1975.1873" gradientTransform="matrix(-0.6181 0.7861 0.7861 0.6181 -2080.1633 -197.5988)">
                                    <stop offset="0.3687" style="stop-color:#F40C28"></stop>
                                    <stop offset="1" style="stop-color:#FF2C50"></stop>
                                 </linearGradient>
                                 <path style="fill:url(#SVGID_00000108307696041867937860000005451407988123938486_);" d="M67.7,199.8c-0.4-0.3-0.8-0.7-1.2-1
                                    c-6.2-5.4-9.8-12.7-10.8-20.3c-1-8.1,1.2-16.6,6.6-23.6l75.8-96.4c0,0,18.4-20.5,8.9-35l32,25.2c1.1,0.9,2.2,1.8,3.2,2.8
                                    c13.5,13.3,15.2,35,3.2,50.3l-72.8,92.6C101.7,208.3,81.6,210.7,67.7,199.8z"></path>
                              </g>
                              <g class="st44">
                                 <path class="st55" d="M68.8,69v78.4c0,8.7,9.8,13.7,16.8,8.6l54.6-39.2c5.9-4.2,5.9-13,0-17.3L85.6,60.4
                                    C78.6,55.3,68.8,60.4,68.8,69z"></path>
                              </g>
                              <g>
                                 <path class="st00" d="M66.8,66v78.4c0,8.7,9.8,13.7,16.8,8.6l54.6-39.2c5.9-4.2,5.9-13,0-17.3L83.6,57.4
                                    C76.6,52.3,66.8,57.4,66.8,66z"></path>
                              </g>
                           </g>
                        </svg>
                     </div>
                     <div class="col-md-9 mt20 mt-md5">
                        <ul class="leader-ul f-18 f-md-20 white-clr text-md-end text-center">
                           <li><a href="#features" class="t-decoration-none">Features</a><span
                              class="pl9 white-clr">|</span></li>
                           <li><a href="#demo" class="t-decoration-none">Demo</a></li>
                           <li class="affiliate-link-btn"><a href="#buynow" class="t-decoration-none">Buy Now</a>
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="col-12 text-center mt20 mt-md50">
                  <div class="pre-heading f-18 f-md-22 w800 lh140 white-clr">
                     Copy A Proven System That Make Us $535/Day Over and Over Again from YouTube 
                  </div>
               </div>
               <div class="col-12 mt-md30 mt20 f-md-48 f-28 w800 text-center white-clr lh140">
                  Brand New Software <span class="orange-clr">Creates & Publish YouTube Shorts in Just 60 Seconds…</span> And Drive Tons of FREE Traffic, Sales & Profits Hands-free 
               </div>
               <div class="col-12 mt-md25 mt20 f-18 f-md-24 w600 text-center lh140 white-clr text-capitalize">
               No Camera & Recording | No Editing | No Paid Traffic | 100% Newbie Friendly 
               </div>
            </div>
            <div class="row d-flex align-items-center flex-wrap mt20 mt-md40">
               <div class="col-md-7 col-12 min-md-video-width-left">
                  <!-- <img src="https://cdn.oppyo.com/launches/vidboxs/fe/product-image.webp" class="img-fluid mx-auto d-block"> -->
                  <div class="col-12 responsive-video border-video">
                     <iframe src="https://vidboxs.dotcompal.com/video/embed/4ioexjgrco"
                        style="width:100%; height:100%" frameborder="0" allow="fullscreen"
                        allowfullscreen=""></iframe>
                     </div>
                  <div class="col-12 mt40 d-none d-md-block">
                     <div class="f-18 w800 lh140 text-center white-clr">
                        Free Commercial License + <br class="d-none d-md-block"> Low 1-Time Price For Launch Period Only 
                     </div>
                     <div class="row">
                        <div class="col-md-11 mx-auto col-12 mt20 mt-md20 text-center">
                           <a href="#buynow" class="cta-link-btn d-block px0">Get Instant Access To VidBoxs</a>
                        </div>
                     </div>
                     <div class="col-12 col-md-10 mx-auto d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                        <img src="https://cdn.oppyo.com/launches/vidboxs/fe/compaitable-with1.webp"
                           class="img-fluid mx-xs-center md-img-right" alt="visa">
                        <div class="d-md-block d-none visible-md px-md30"><img
                           src="https://cdn.oppyo.com/launches/vidboxs/fe/v-line.webp" class="img-fluid"
                           alt="line">
                        </div>
                        <img src="https://cdn.oppyo.com/launches/vidboxs/fe/days-gurantee1.webp"
                           class="img-fluid mx-xs-auto mt15 mt-md0" alt="30 days">
                     </div>
                     <div class="col-12 col-md-8 mx-auto mt30 d-none d-md-block">
                        <img src="https://cdn.oppyo.com/launches/vidboxs/fe/limited-time.webp" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
               <div class="col-md-5 col-12 mt20 mt-md0 min-md-video-width-right">
                  <div class="calendar-wrap mb20 mb-md0">
                     <ul class="list-head pl0 m0 f-18 lh140 w400 white-clr">
                        <li>Tap Into Fast-Growing $200 Billion+ Video Industry & Bank BIG </li>
                        <li>Create Unlimited YouTube Short Videos on Any Topic </li>
                        <li>Drive Unlimited Traffic & Sales to Any Offer or Page. </li>
                        <li>Make Tons of Affiliate Commissions or Ad Profits  </li>
                        <li>Create Short Videos from Any Text in 3 Simple Clicks </li>
                        <li>Create Videos Using A keyword or Stock Videos </li>
                        <li>Add Background Music and/or Voiceover to Any Video </li>
                        <li>Inbuilt Voiceover Creator with 150+ Human Voice in 30+ Languages </li>
                        <li>100% Newbie Friendly, Required No Prior Experience or Tech Skills </li>
                        <li>No Camera Recording, No Voice, or Complex Editing Required </li>
                        <li>Free Commercial License to Sell Video Services for High Profits </li>
                     </ul>
                  </div>
                  <div class="d-md-none">
                     <div class="f-18 f-md-26 w800 lh140 text-center white-clr">
                        Free Commercial License + Low 1-Time Price For Launch Period Only 
                     </div>
                     <div class="row">
                        <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center">
                           <a href="#buynow" class="cta-link-btn d-block px0">Get Instant Access To VidBoxs</a>
                        </div>
                     </div>
                     <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                        <img src="https://cdn.oppyo.com/launches/vidboxs/fe/compaitable-with1.webp"
                           class="img-responsive mx-xs-center md-img-right" alt="visa">
                        <div class="d-md-block d-none visible-md px-md30"><img
                           src="https://cdn.oppyo.com/launches/vidboxs/fe/v-line.webp" class="img-fluid"
                           alt="line">
                        </div>
                        <img src="https://cdn.oppyo.com/launches/vidboxs/fe/days-gurantee1.webp"
                           class="img-responsive mx-xs-auto mt15 mt-md0" alt="30 days">
                     </div>
                     <div class="col-12 col-md-8 mx-auto mt30 d-md-none">
                        <img src="https://cdn.oppyo.com/launches/vidboxs/fe/limited-time.webp" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Header Section End -->
      <!-- Step Section -->
      <div class="step-section1">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-45 f-28 w600 text-center black-clr lh140">
                  Create & Share Stunning YouTube Shorts for <br class="d-none d-md-block"> Massive Traffic To Any Offer, Page, or Link <br> <span class="w900 step-shape1">in Just 3 Easy Steps </span>
               </div>
            </div>
            <div class="row mt20 mt-md50">
               <div class="col-12 col-md-4">
                  <div class="step-block">
                     <div class="d-flex align-items-center flex-nowrap gap-4">
                        <img src="https://cdn.oppyo.com/launches/vidboxs/fe/step-icon1.webp" class="img-fluid">
                        <div class="w-100">
                           <div class="f-20 f-md-22 w500 lh140 black-clr">STEP 1</div>
                           <div class="border-line"></div>
                        </div>
                     </div>
                     <div class="f-22 f-md-32 w800 lh140 black-clr mt15">Choose</div>
                     <div class="f-18 w400 lh140 black-clr mt10">
                        Login to access VidBoxs dashboard and choose whether you want to create Shorts with Text, by image, or by using your own video clips. 
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-4 mt20 mt-md0">
                  <div class="step-block">
                     <div class="d-flex align-items-center flex-nowrap gap-4">
                        <img src="https://cdn.oppyo.com/launches/vidboxs/fe/step-icon2.webp" class="img-fluid">
                        <div class="w-100">
                           <div class="f-20 f-md-22 w500 lh140 black-clr">STEP 2</div>
                           <div class="border-line"></div>
                        </div>
                     </div>
                     <div class="f-22 f-md-32 w800 lh140 black-clr mt15">Create</div>
                     <div class="f-18 w400 lh140 black-clr mt10">
                        Just copy-paste text script, use a keywords, or stock video clips to create engaging videos within minutes.
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-4 mt20 mt-md0">
                  <div class="step-block">
                     <div class="d-flex align-items-center flex-nowrap gap-4">
                        <img src="https://cdn.oppyo.com/launches/vidboxs/fe/step-icon3.webp" class="img-fluid">
                        <div class="w-100">
                           <div class="f-20 f-md-22 w500 lh140 black-clr">STEP 3</div>
                           <div class="border-line"></div>
                        </div>
                     </div>
                     <div class="f-22 f-md-32 w800 lh140 black-clr mt15">Publish and Profit</div>
                     <div class="f-18 w400 lh140 black-clr mt10">
                        Now publish your Shorts on YouTube with a click to drive unlimited viral traffic & sales on your offer, page, or website. 
                     </div>
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md70">
               <div class="col-12 col-md-4 col-md-4">
                  <!-- <img src="https://cdn.oppyo.com/launches/vidboxs/fe/no-download.webp" class="img-fluid d-block mx-auto"> -->
                  <div class="w700 f-24 f-md-26 black-clr text-center lh140">
                     No Download/Installation 
                  </div>
               </div>
               <div class="col-12 col-md-4 mt20 mt-md0">
                  <!-- <img src="https://cdn.oppyo.com/launches/vidboxs/fe/no-prior-knowledge.webp" class="img-fluid d-block mx-auto"> -->
                  <div class="w700 f-24 f-md-26 black-clr text-center lh140">
                     No Prior Knowledge                                         
                  </div>
               </div>
               <div class="col-12 col-md-4 mt20 mt-md0">
                  <!-- <img src="https://cdn.oppyo.com/launches/vidboxs/fe/beginners-friendly.webp" class="img-fluid d-block mx-auto"> -->
                  <div class="w700 f-24 f-md-26 black-clr text-center lh140">
                     100% Beginners Friendly 
                  </div>
               </div>
               <div class="col-12 f-18 f-md-20 w400 lh140 black-clr mt20 mt-md30">
                  In just 60 seconds, you can create your first profitable YouTube Short to enjoy MASSIVE FREE Traffic, Sales & Profits coming into your bank account on autopilot. 
               </div>
            </div>
         </div>
      </div>
      <!-- Step Section End -->
      <div class="proof-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-40 f-28 w800 lh140 text-center black-clr">
                  Got 34,309 Targeted Visitors in Last 30 Days… .                  
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
                  <img src="https://cdn.oppyo.com/launches/vidboxs/fe/proof.webp" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 mt20 mt-md50 f-md-40 f-28 w800 lh140 text-center black-clr">
                  Consistently Making Average $535 In Profits <br class="d-none d-md-block"> Each & Every Day              
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
                  <img src="https://cdn.oppyo.com/launches/vidboxs/fe/proof1.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>
         </div>
      </div>
      <!-- Testimonials Section -->
      <div class="testimonial-section">
         <div class="container ">
            <div class="row ">
               <div class="f-md-45 f-28 w800 lh140 text-center black-clr">
                  Checkout What VidBoxs Early Users Have to SAY  
               </div>
            </div>
            <div class="row mt25 mt-md50 align-items-center">
               <div class="col-12 col-md-10">
                  <div class="testi-block">
                     <div class="row d-flex align-items-center flex-wrap">
                         <div class="col-12 col-md-3 order-md-2">
                             <img src="https://cdn.oppyo.com/launches/vidboxs/fe/c1.webp" class="img-fluid d-block mx-auto">
                             <div class="f-22 f-md-24 w800 lh120 red-clr mt20 text-center">Liam Smith</div>
                         </div>
                         <div class="col-12 col-md-9 order-md-1 mt20 mt-md0 text-center text-md-start">
                             <div class="f-22 f-md-28 w800 lh140">I got 2500+ Views & 509 Subscribers in 4 days</div>
                             <div class="f-20 w400 lh140 mt20 quote1">
                              Hi, I am from Australia and promote weight loss products on ClickBank & Amazon as an affiliate. I was struggling to get traffic on my offers and my YouTube channels have only 257 subscribers the reason is that I could not create and post Videos and Shorts regularly as it takes lots of effort and time. But recently, I got access to VidBoxs, I created around 15 Short Videos with almost no effort, that was amazingly EASY. And <span class="w700">I got 2500+ Views & 509 Subscribers in 4 days</span> on my weight loss channel. Kudos to the team for creating a solution that can make the complete process fast & easy. 
                             </div>
                            
                         </div>
                     </div>
                 </div>
               </div>
               <div class="col-12 col-md-10 offset-md-2 mt20 mt-md40">
                  <div class="testi-block">
                     <div class="row d-flex align-items-center flex-wrap">
                         <div class="col-12 col-md-3">
                             <img src="https://cdn.oppyo.com/launches/vidboxs/fe/c2.webp" class="img-fluid d-block mx-auto">
                             <div class="f-22 f-md-24 w800 red-clr mt20 text-center">Noah Brown</div>
                         </div>
                         <div class="col-12 col-md-9 mt20 mt-md0 text-center text-md-start">
                             <div class="f-22 f-md-28 w800 lh140">Best part about VidBoxs is that it takes away all hassles</div>
                             <div class="f-20 w400 lh140 mt20 quote1">
                              Thanks for the early access, VidBoxs Team. <span class="w700">The best part about VidBoxs is that it takes away</span> all hassles like video recording or editing, voiceover recording, background music and driving traffic to your offers, etc. I am sure this will give you complete value for your money and help you get the desired success in the long run.
                             </div>
                            
                         </div>
                     </div>
                 </div>
               </div>
               <div class="col-12 col-md-10 mt20 mt-md40">
                  <div class="testi-block">
                     <div class="row d-flex align-items-center flex-wrap">
                         <div class="col-12 col-md-3 order-md-2">
                             <img src="https://cdn.oppyo.com/launches/vidboxs/fe/c3.webp" class="img-fluid d-block mx-auto">
                             <div class="f-22 f-md-24 w800 lh120 red-clr mt20 text-center">Benjamin Davis</div>
                         </div>
                         <div class="col-12 col-md-9 order-md-1 mt20 mt-md0 text-center text-md-start">
                             <div class="f-22 f-md-28 w800 lh140">Create super engaging video shorts for YouTube</div>
                             <div class="f-20 w400 lh140 mt20 quote1">
                              VidBoxs is the perfect platform to <span class="w700">create super engaging video shorts for YouTube and social media and drive tons of viral traffic in a stress-free manner.</span> I am enjoying all the functionalities of this amazing software. If someone asks me to give marks, I would love to give ten-on-ten to this fabulous product.
                             </div>
                           
                         </div>
                     </div>
                 </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Testimonials Section End -->
      <!-- CTA Section-->
      <div class="cta-section-new">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-18 f-md-26 w800 lh140 text-center white-clr">
                     Free Commercial License + Low 1-Time Price For Launch Period Only
                  </div>
                  <div class="f-20 f-md-24 w400 lh140 text-center mt20 white-clr">
                     Use Discount Coupon <span class="orange-clr w700">"vidboxs"</span> for Instant <span class="orange-clr w700">$3 OFF</span>
                 </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center">
                        <a href="#buynow" class="cta-link-btn">Get Instant Access To VidBoxs</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                     <img src="https://cdn.oppyo.com/launches/vidboxs/fe/compaitable-with1.webp" class="img-responsive mx-xs-center md-img-right" alt="visa">
                     <div class="d-md-block d-none visible-md px-md30">
                        <img src="https://cdn.oppyo.com/launches/vidboxs/fe/v-line.webp" class="img-fluid" alt="line">
                     </div>
                     <img src="https://cdn.oppyo.com/launches/vidboxs/fe/days-gurantee1.webp" class="img-responsive mx-xs-auto mt15 mt-md0" alt="30 days">
                  </div>
                  <div class="mt20 mt-md40 d-inline-flex align-items-center">
                     <img src="https://cdn.oppyo.com/launches/vidboxs/fe/limited-time-offer.webp" class="img-fluid mx-auto d-block">
                     <div class="cta-shape f-18 f-md-22 w800">Grab Your Copy Before The Price Increases!</div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- CTA Section End-->
      <div class="billion-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-20 f-md-36 w800 white-clr">Did You Know? YouTube Shorts Hit</div>
                  <div class="f-45 f-md-120 w900 orange-clr">30 Billion View </div>
                  <div class="f-20 f-md-45 w800 white-clr">Every Single Day!  </div>
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-12 col-md-7">
                  <div class="f-18 f-md-24 w800 white-clr lh140">Yes, YouTube Shorts are Generating 30 Billion+ Views Per Day, A 4X Increase From This Time Last Year
                  </div>
                  <div class="f-18 f-md-22 w400  lh140 white-clr mt15">This clearly shows the trend is changing. Internet users prefer short videos to consume content instead of watching long videos like earlier… </div>
               </div>
               <div class="col-12 col-md-5 mt20 mt-md0">
                  <img src="https://cdn.oppyo.com/launches/vidboxs/fe/billion.webp" class="img-fluid mx-auto d-block">
               </div>
               <div class="col-12 mt20 mt-md50 text-center">
                  <div class="f-32 f-md-65 w900 orange-clr lh140">No Doubt! 
                  </div>
                  <div class="f-20 f-md-36 w800 lh140 white-clr mt10">YouTube Shorts Are Present and Future of Video Marketing… </div>
               </div>
            </div>
         </div>
      </div>
      <div class="facts-bg">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-28 f-md-45 w800 lh140 text-center">And here are some eye-opening facts that will let <br class="d-none d-md-block"> you know why one should get started with<br class="d-none d-md-block"> YouTube Shorts! 
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50">
                  <div class="row align-items-center">
                     <div class="col-12 col-md-6">
                        <div class="f-22 f-md-32 w800 lh140">YouTube has 2+ Bn Users & 70% of Videos are Viewed on Mobile</div>
                        <div class="f-18 w400 lh140 mt10">YouTube has 2 billion monthly active users, <span class="w700">1 billion hours of content is watched every day, and 70% of videos are viewed on mobile.</span> A vast majority of YouTube content is viewed on mobile devices and it has reaffirmed the industry's shift towards short-form, easily digestible, and mobile-first content.And because of this fact, YouTube is also investing so heavily in this area</div>
                     </div>
                     <div class="col-12 col-md-6 mt20 mt-md0">
                        <img src="https://cdn.oppyo.com/launches/vidboxs/fe/facts3.webp" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50">
                  <div class="row align-items-center">
                     <div class="col-12 col-md-6 order-md-2">
                        <div class="f-22 f-md-32 w800 lh140">YouTube Confirms Ads Are Coming to Shorts: </div>
                        <div class="f-18 w400 lh140 mt10">Ads in Shorts would give creators in the YouTube Partner Program the ability to earn more revenue on the platform.</div>
                     </div>
                     <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
                        <img src="https://cdn.oppyo.com/launches/vidboxs/fe/facts1.webp" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50">
                  <div class="row align-items-center">
                     <div class="col-12 col-md-6">
                        <div class="f-22 f-md-32 w800 lh140">Another Great Earning Potential is YouTube Shorts Fund: </div>
                        <div class="f-18 w400 lh140 mt10">YouTube has signaled its attention to helping creators monetize Shorts and transform them into a solid new revenue stream. To that concern, the company has decided to pay creators at least $100 million for their YouTube Shorts monetization through 2022.</div>
                     </div>
                     <div class="col-12 col-md-6 mt20 mt-md0">
                        <img src="https://cdn.oppyo.com/launches/vidboxs/fe/facts2.webp" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>              
               <div class="col-12 mt20 mt-md50">
                  <div class="row align-items-center">
                     <div class="col-12 col-md-6 order-md-2">
                        <div class="f-22 f-md-32 w800 lh140">YouTube Shorts Shelf in Trending Pages:</div>
                        <div class="f-18 w400 lh140 mt10">YouTube trending page is getting a carousel dedicated to short form of videos or what the company refers to as a short shelf. </div>
                     </div>
                     <div class="col-12 col-md-6 order-md-1 mt20 mt-md0">
                        <img src="https://cdn.oppyo.com/launches/vidboxs/fe/facts4.webp" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50">
                  <div class="row align-items-center">
                     <div class="col-12 col-md-6">
                        <div class="f-22 f-md-32 w800 lh140">YouTube Shorts on Desktop and Mobile Web: </div>
                        <div class="f-18 w400 lh140 mt10">Shorts are gaining more visibility on desktop and mobile browsers with the addition of a new tab and home page carousel.  </div>
                     </div>
                     <div class="col-12 col-md-6 mt20 mt-md0">
                        <img src="https://cdn.oppyo.com/launches/vidboxs/fe/facts5.webp" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md70 text-center">
                  <div class="f-24 f-md-40 w800 lh140 text-center">So, Capitalising on YouTube Shorts in the Video Marketing Strategy of Any Business is the Need of The Hour.                     
                  </div>
                  <img src="https://cdn.oppyo.com/launches/vidboxs/fe/sepration.webp" class="img-fluid mx-auto d-block mt15">
                  <div class="skew-shape mt20 mt-md50">
                     <div class="f-20 f-md-32 lh140 w800">Also, It Opens the Door to Fastest Growing</div>
                     <div class="f-24 f-md-65 lh140 w900">BIG $200 Billion Opportunity</div>
                  </div>                 
               </div>
            </div>
         </div>
      </div>
      <div class="power-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-20 f-md-32 w400 lh140 text-center">Now You’re Probably Wondering “Can Anyone Make a Full-Time Income?”<br>
                     <span class="w800">Answer Is - Of Course, You Can!</span>                                       
                  </div>
                  <div class="f-28 f-md-45 w800 lh140 text-center black-clr mt20 mt-md40">
                     See How People Are Making Crazy <br class="d-none d-md-block">
                     Money Using the POWER Of YouTube                        
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md150">
                  <div class="power-shape">
                     <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-12 col-md-2">
                           <img src="https://cdn.oppyo.com/launches/vidboxs/fe/ryan.webp" class="img-fluid d-block mx-auto ryn-img">
                        </div>
                        <div class="col-12 col-md-10 mt30 mt-md0">
                           <div class="f-md-22 f-20 lh140 w400 text-center text-md-start pl-md60">
                              <span class="w800">Here’s “Ryan’s World” </span>– A 10-Year-Old Kid that has over <br class="d-none d-md-block"> 31.9M Subscribers  with a whooping  <span class="w800">Net Worth of $32M</span><br class="d-none d-md-block"> and growing… As he is now capitalizing on YouTube Shorts. <br><br>
                              Yes! You heard me right... He is just a 10-year-kid but making BIG.
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md150">
                  <div class="power-shape">
                     <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-12 col-md-2 order-md-2">
                           <img src="https://cdn.oppyo.com/launches/vidboxs/fe/gary-vaynerchuk.webp" class="img-fluid d-block mx-auto ryn-img1">
                        </div>
                        <div class="col-12 col-md-10 mt30 mt-md0 order-md-1">
                           <div class="f-md-22 f-20 lh140 w400 text-center text-md-start pl-md30 pr-md40">
                              Even Pro Marketers are bidding heavily on YouTube Shorts:<br><br>
                              <span class="w800">Mr. Gary Vaynerchuk</span>, an entrepreneur and business Guru 
                              who’s also known as the first wine guru has a <span class="w800">Net Worth of $200M+ </span>                       
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row mt30 mt-md100">
               <div class="col-12 text-center">
                  <div class="but-design-border">
                     <div class="f-md-45 f-28 lh140 w900 white-clr button-shape">
                        Impressive right?
                     </div>
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md20">
               <div class="col-12 f-md-28 f-20 lh160 w400 text-center">
                  So, it can be safely stated that…<br>
                  <span class="f-22 f-md-32 w800">YouTube Shorts Are The BIGGEST, Traffic and Income<br class="d-none d-md-block"> Opportunity Right Now! </span>
               </div>
            </div>
         </div>
      </div>
      <div class="hi-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-md-24 f-22 w500 lh140 black-clr">
                     Dear Struggling Marketer, 
                  </div>
                  <div class="f-md-32 f-24 w800 lh140 black-clr mt10">
                     From the desk of Ayush Jain and Pranshu Gupta
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50">
                  <div class="row align-items-center">
                     <div class="col-12 col-md-7">
                        <div class="f-18 f-md-20 w400 lh140 black-clr">
                           I’m Ayush Jain along with my friend Pranshu Gupta. 
                           <br><br>
                           We are happily living the internet lifestyle for the last 10 years with  
                           full flexibility of working from anywhere at any time. 
                           <br><br>
                           Over time, we’ve learned that to be successful online, you must get  
                           your hands on a fool proof system that works again and again and again.  
                        </div>
                        <div class="mt20 f-md-22 w800 f-20 lh140 black-clr">
                           And… 95% of the marketers quit in their first year of business because  they fail to find a proven system to make money! 
                        </div>
                        <div class="f-18 f-md-20 w400 lh140 black-clr mt20">
                           Till now, you might already have discovered that shorts <span class="w700">videos are an  untapped goldmine to tap into oceans of viral traffic</span> that’s ready to be pounced upon to boost leads, sales &amp; profits for your offers.
                           <br><br>
                           When it’s combined with the <span class="w700">most powerful video giant YouTube,</span>   
                           the power it provides to your business IS TRULY MAGICAL. 
                        </div>
                     </div>
                     <div class="col-md-5 col-12 text-center mt20 mt-md0">
                        <img src="https://cdn.oppyo.com/launches/vidboxs/fe/ayush-pranshu.webp" class="img-fluid mx-auto d-block" alt="Ayush-Pranshu">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Affiliate Section Start -->
      <div class="affiliate-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-18 f-md-20 w400 lh140 black-clr">
                  <span class="w800">The SECRET Is: </span><br><br> When you can combine selling top products, YouTube shorts, and free viral traffic to make sales &amp; profits, you’re the fastest way possible.
               </div>
               <div class="col-12 f-18 f-md-20 w500 lh140 black-clr mt20 mt-md50">
                  <img src="https://cdn.oppyo.com/launches/vidboxs/fe/fb-viral.webp" class="img-fluid d-block mx-auto" alt="Viral">
               </div>
               <div class="col-12 mt20 mt-md50">
                  <div class="f-md-40 f-24 w800 lh140 text-capitalize text-center black-clr">
                     Checkout Free Traffic &amp; Commissions <br class="d-none d-md-block"> 
                     We’re Getting by Following This Proven System… 
                  </div>
               </div>
               <div class="col-12 col-md-10 mt20 mt-md30 mx-auto">
                  <img src="https://cdn.oppyo.com/launches/vidboxs/fe/proof-02.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>
         </div>
      </div>
      <!-- Affiliate Section End -->
   

      <!-----Option Section Start------>
      <div class="option-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-24 f-md-45 w700 lh140 black-clr text-center">That’s A Real Deal But…</div>
                  <div class="f-28 f-md-65 w900 lh140 red-clr1 text-center">Here’s The Big Problem</div>
               </div>
               <div class="col-12 mt-md70 mt20 ">
                  <div class="row align-items-center">
                     <div class="col-md-6">
                        <div class="f-24 f-md-32 w800 red-clr lh140">
                           Creating Content Daily is Painful & Time Consuming! 
                        </div>
                        <div class="f-18 w500 lh140 black-clr mt20">
                          <ul class="problem-list">
                           <li> You need to research, plan, and write content daily. And staying 
                           up-to-date with the latest niche trends needs lots of effort </li>

                           <li>You Need To Be On Camera. This can be a major blocker in the case 
                           you are shy or introverted and have a fear that people would judge 
                           and laugh at your videos.</li>                           
                          </ul>                        
                        </div>
                     </div>
                     <div class="col-md-6">
                        <img src="https://cdn.oppyo.com/launches/vidboxs/fe/opt1.webp" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
               <div class="col-12 mt-md70 mt20 ">
                  <div class="row align-items-center">
                     <div class="col-md-6 order-md-2">
                        <div class="f-24 f-md-32 w800 red-clr lh140">
                           Buying Expensive Equipment Can Leave Your Back Accounts Dry                        
                        </div>
                        <div class="f-18 w500 lh140 black-clr mt20">
                           To even get started with the first video, you need expensive equipment, like a<br><br>
                           <ul class="problem-list pl0">
                              <li>Nice camera</li>
                              <li>Microphone, and</li>
                              <li>Video-audio editing software That would cost you THOUSANDS of dollars.</li>
                           </ul>
                        </div>
                     </div>
                     <div class="col-md-6 order-md-1">
                        <img src="https://cdn.oppyo.com/launches/vidboxs/fe/opt2.webp" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>               
            </div>
            <div class="row mt-md70 mt20">
               <div class="col-12">
                  <div class="row align-items-center">
                     <div class="col-md-6">
                        <div class="f-24 f-md-32 w800 red-clr lh140">
                           You Need To Learn Complex Editing Tools 
                        </div>
                        <div class="f-18 w500 lh140 black-clr mt20">
                           <ul class="problem-list pl0">
                              <li>You need to learn COMPLEX video & audio editing skills. 
                              Most software are complex and difficult to learn, especially 
                              if you are a non-technical guy like us.
                              </li>
                           </ul>
                        </div>
                     </div>
                     <div class="col-md-6">
                        <img src="https://cdn.oppyo.com/launches/vidboxs/fe/opt3.webp" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
               <div class="col-12 mt-md70 mt20">
                  <div class="row align-items-center">
                     <div class="col-md-6 order-md-2">
                        <div class="f-24 f-md-32 w800 red-clr lh140">
                           Even If You Outsource Any of the Above Tasks! 
                        </div>
                        <div class="f-18 w500 lh140 black-clr mt20">
                           <ul class="problem-list pl0">
                              <li> 	Firstly, it can cost you 1000’s Dollars even for a 
                                 small project.                                  
                              </li>
                              <li> 	Finding a well skilled Freelancer who could handle 
                                 all your task is a big headache. 
                                 </li>
                                 <li> 	Even then you have to work for countless hours 
                                    finding ideas and explaining your requirement to them 
                                    </li>
                           </ul>
                        </div>
                     </div>
                     <div class="col-md-6 order-md-1">
                        <img src="https://cdn.oppyo.com/launches/vidboxs/fe/opt4.webp" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>              

            </div>
            <div class="row mt-md50 mt20">
               <div class="col-12 f-md-22 f-20 w400 lh140 black-clr">
                  <span class="w700"> Problem does not end here, after all these efforts, still there is no security that you will get the desired results.</span> <br><br>
                  There’re tons of areas where marketers can face challenges that we had faced in our journey before creating this masterpiece.
               </div>
            </div>
            <div class="row mt-md50 mt20">
               <div class="col-12 col-md-10 mx-auto">
                  <div class="any-shape text-center">
                     <div class="f-md-45 f-28 lh140 orange-clr w800 text-uppercase">
                        But Not Anymore!
                     </div>
                     <div class="f-18 lh140 white-clr w400 mt15">
                        Imagine if you had an ultimate ground-breaking beginner friendly technology that’s custom <br class="d-none d-md-block">created keeping every marketer in mind, and that creates Short Videos for YouTube without<br class="d-none d-md-block"> having to spend money on costly equipment, without creating much content, and without <br class="d-none d-md-block">having to be on camera at all!              
                     </div>

                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-----Option Section End------>
      <!-- Proudly Section -->
      <div class="proudly-sec" id="product">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center lh140">
                  <div class="f-28 f-md-32 w800 text-center orange-clr lh140 text-capitalize">Presenting…</div>
               </div>
               <div class="col-12 mt-md40 mt20 text-center">
                  <svg version="1.1" id="Layer_11" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 865.5 207.4" style="enable-background:new 0 0 865.5 207.4; max-height:100px" xml:space="preserve">
                     <style type="text/css">
                        .st00{fill:#FFFFFF;}
                        .st11{fill:url(#SVGID_1_);}
                        .st22{fill:url(#SVGID_00000003804682614402079440000013301449542194118298_);}
                        .st33{fill:url(#SVGID_00000106859270421856661840000011347085370863025833_);}
                        .st44{opacity:0.3;}
                        .st55{fill:#0F0F0F;}
                     </style>
                     <g>
                        <path class="st00" d="M482.4,27.5v105.7c0,17.9-10.1,33.4-24.9,41.3v-32.9c1-2.6,1.6-5.4,1.6-8.4s-0.6-5.8-1.6-8.4V27.5H482.4z"></path>
                        <path class="st00" d="M457.6,92c-6.5-3.4-13.9-5.4-21.8-5.4c-25.8,0-46.6,20.9-46.6,46.6c0,25.8,20.9,46.6,46.6,46.6
                           c7.9,0,15.3-1.9,21.8-5.4c14.8-7.8,24.9-23.4,24.9-41.3C482.4,115.3,472.4,99.8,457.6,92z M457.6,141.6c-2,5.3-5.9,9.6-10.9,12.2
                           c-3.2,1.7-7,2.7-10.9,2.7c-12.9,0-23.3-10.4-23.3-23.3s10.4-23.3,23.3-23.3c3.9,0,7.6,1,10.9,2.7c5,2.6,8.8,7,10.9,12.2
                           c1,2.6,1.6,5.4,1.6,8.4C459.1,136.2,458.6,139,457.6,141.6z"></path>
                        <path class="st00" d="M498,27.5v105.7c0,17.9,10.1,33.4,24.9,41.3v-32.9c-1-2.6-1.6-5.4-1.6-8.4s0.6-5.8,1.6-8.4V27.5H498z"></path>
                        <path class="st00" d="M498,133.2c0,17.9,10.1,33.4,24.9,41.3c6.5,3.4,13.9,5.4,21.8,5.4c25.8,0,46.6-20.9,46.6-46.6
                           c0-25.8-20.9-46.6-46.6-46.6c-7.9,0-15.3,1.9-21.8,5.4C508.1,99.8,498,115.3,498,133.2z M521.3,133.2c0-3,0.6-5.8,1.6-8.4
                           c2-5.3,5.9-9.6,10.9-12.2c3.2-1.7,7-2.7,10.9-2.7c12.9,0,23.3,10.4,23.3,23.3s-10.4,23.3-23.3,23.3c-3.9,0-7.6-1-10.9-2.7
                           c-5-2.6-8.8-7-10.9-12.2C521.8,139,521.3,136.2,521.3,133.2z"></path>
                        <path class="st00" d="M597.5,133.2c0,17.9,10.1,33.4,24.9,41.3c6.5,3.4,13.9,5.4,21.8,5.4c25.8,0,46.6-20.9,46.6-46.6
                           c0-25.8-20.9-46.6-46.6-46.6c-7.9,0-15.3,1.9-21.8,5.4C607.5,99.8,597.5,115.3,597.5,133.2z M620.8,133.2c0-3,0.6-5.8,1.6-8.4
                           c2-5.3,5.9-9.6,10.9-12.2c3.2-1.7,7-2.7,10.9-2.7c12.9,0,23.3,10.4,23.3,23.3s-10.4,23.3-23.3,23.3c-3.9,0-7.6-1-10.9-2.7
                           c-5-2.6-8.8-7-10.9-12.2C621.3,139,620.8,136.2,620.8,133.2z"></path>
                        <polygon class="st00" points="721.5,179.9 690.4,179.9 756.1,86.5 787.2,86.5 	"></polygon>
                        <polygon class="st00" points="756.1,179.9 787.2,179.9 721.5,86.5 690.4,86.5 	"></polygon>
                        
                           <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="291.6235" y1="980.703" x2="309.5574" y2="1019.9633" gradientTransform="matrix(1 0 0 1 0 -870)">
                           <stop offset="0" style="stop-color:#F40C28"></stop>
                           <stop offset="0.8911" style="stop-color:#FF2C50"></stop>
                        </linearGradient>
                        <polygon class="st11" points="264.4,179.9 278.5,150 308.3,86.5 339.4,86.5 295.5,179.9 292.5,179.9 	"></polygon>
                        <polygon class="st00" points="292.5,179.9 278.5,150 248.6,86.5 217.5,86.5 261.5,179.9 264.4,179.9 	"></polygon>
                        <rect x="351.9" y="86.6" class="st00" width="24.9" height="93.3"></rect>
                        <rect x="351.9" y="51.8" class="st00" width="24.9" height="24.9"></rect>
                        <g>
                           <path class="st00" d="M810.1,175.9c-5.9-2.7-10.5-6.3-14-10.9c-3.4-4.6-5.3-9.6-5.6-15.2h23c0.4,3.5,2.1,6.4,5.1,8.6
                              c3,2.3,6.7,3.4,11.2,3.4c4.3,0,7.8-0.9,10.2-2.6c2.4-1.7,3.7-4,3.7-6.7c0-2.9-1.5-5.1-4.5-6.6s-7.8-3.1-14.3-4.8
                              c-6.7-1.6-12.3-3.3-16.6-5.1c-4.3-1.7-8-4.4-11.1-8c-3.1-3.6-4.7-8.4-4.7-14.5c0-5,1.4-9.6,4.3-13.7c2.9-4.1,7-7.4,12.4-9.8
                              s11.7-3.6,19-3.6c10.8,0,19.4,2.7,25.8,8.1c6.4,5.4,10,12.6,10.6,21.8h-21.9c-0.3-3.6-1.8-6.4-4.5-8.6c-2.7-2.1-6.2-3.2-10.7-3.2
                              c-4.1,0-7.3,0.8-9.5,2.3s-3.3,3.6-3.3,6.4c0,3,1.5,5.4,4.6,6.9c3,1.6,7.8,3.2,14.2,4.8c6.5,1.6,11.9,3.3,16.2,5.1
                              c4.2,1.7,7.9,4.4,11,8.1c3.1,3.6,4.7,8.5,4.8,14.4c0,5.2-1.4,9.9-4.3,14c-2.9,4.1-7,7.4-12.4,9.7s-11.7,3.5-18.8,3.5
                              C822.6,179.9,815.9,178.5,810.1,175.9z"></path>
                        </g>
                     </g>
                     <g>
                        <g>
                           
                              <linearGradient id="SVGID_00000090999748961388969530000010752564172096406444_" gradientUnits="userSpaceOnUse" x1="-1360.6178" y1="-1339.845" x2="-1175.7527" y2="-1283.7964" gradientTransform="matrix(0.6181 -0.7861 -0.7861 -0.6181 -176.8927 -1732.0651)">
                              <stop offset="0.2973" style="stop-color:#F40C28"></stop>
                              <stop offset="0.7365" style="stop-color:#FF2C50"></stop>
                           </linearGradient>
                           <path style="fill:url(#SVGID_00000090999748961388969530000010752564172096406444_);" d="M125.8,6.8c0.4,0.3,0.8,0.7,1.2,1
                              c6.2,5.4,9.8,12.7,10.8,20.3c1,8.1-1.2,16.6-6.6,23.6l-75.8,96.4c0,0-18.4,20.5-8.9,35l-32-25.2c-1.1-0.9-2.2-1.8-3.2-2.8
                              c-13.5-13.3-15.2-35-3.2-50.3l72.8-92.6C91.8-1.7,111.9-4.1,125.8,6.8z"></path>
                           
                              <linearGradient id="SVGID_00000108307696041867937860000005451407988123938486_" gradientUnits="userSpaceOnUse" x1="-1216.8146" y1="1899.8297" x2="-973.6699" y2="1975.1873" gradientTransform="matrix(-0.6181 0.7861 0.7861 0.6181 -2080.1633 -197.5988)">
                              <stop offset="0.3687" style="stop-color:#F40C28"></stop>
                              <stop offset="1" style="stop-color:#FF2C50"></stop>
                           </linearGradient>
                           <path style="fill:url(#SVGID_00000108307696041867937860000005451407988123938486_);" d="M67.7,199.8c-0.4-0.3-0.8-0.7-1.2-1
                              c-6.2-5.4-9.8-12.7-10.8-20.3c-1-8.1,1.2-16.6,6.6-23.6l75.8-96.4c0,0,18.4-20.5,8.9-35l32,25.2c1.1,0.9,2.2,1.8,3.2,2.8
                              c13.5,13.3,15.2,35,3.2,50.3l-72.8,92.6C101.7,208.3,81.6,210.7,67.7,199.8z"></path>
                        </g>
                        <g class="st44">
                           <path class="st55" d="M68.8,69v78.4c0,8.7,9.8,13.7,16.8,8.6l54.6-39.2c5.9-4.2,5.9-13,0-17.3L85.6,60.4
                              C78.6,55.3,68.8,60.4,68.8,69z"></path>
                        </g>
                        <g>
                           <path class="st00" d="M66.8,66v78.4c0,8.7,9.8,13.7,16.8,8.6l54.6-39.2c5.9-4.2,5.9-13,0-17.3L83.6,57.4
                              C76.6,52.3,66.8,57.4,66.8,66z"></path>
                        </g>
                     </g>
                     </svg>
               </div>
               <div class="col-12 f-md-48 f-28 mt-md30 mt20 w800 text-center white-clr lh140">
                  A Push-Button Technology Creates Short Videos to Publish on YouTube, 
                  or other social media and Drive Viral Traffic to Any of Your Offers
                  
               </div>
               <div class="col-12 f-20 f-md-22 lh140 w500 text-center white-clr mt20">
                 
It enabled you to create Engaging, Highly Professional Short Videos, <br class="d-none d-md-block">
Add Voiceover or Background Music with Zero Tech Hassles. 
 
               </div>
            </div>
            <div class="row mt20 mt-md50 align-items-center">
               <div class="col-12">
                  <img src="https://cdn.oppyo.com/launches/vidboxs/fe/product-image.webp" class="img-fluid d-block mx-auto img-animation"
                     alt="Product">
               </div>
            </div>
         </div>
      </div>
      <!-- Proudly Section End -->
      <!-- Step Section -->
      <div class="step-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-45 f-28 w600 text-center black-clr lh140">
                  Create & Share Stunning YouTube Shorts for <br class="d-none d-md-block"> Massive Traffic To Any Offer, Page, or Link <br> <span class="w900 step-shape1">in Just 3 Easy Steps </span>
               </div>
            </div>
            <div class="row mt20 mt-md50 align-items-center">
               <div class="col-12 col-md-5">
                  <div class="step-shape">Step 1</div>
                  <div class="f-22 f-md-32 w800 lh140 black-clr mt15">Choose</div>
                  <div class="f-18 f-md-20 w400 lh140 black-clr mt10">
                     Login to access VidBoxs dashboard and choose whether you want to create Shorts with Text, by image, or by using your own video clips.                   
                  </div>
               </div>
               <div class="col-12 col-md-7 mt20 mt-md0">
                  <img src="https://cdn.oppyo.com/launches/vidboxs/fe/step1.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>
            <div class="row mt20 mt-md70 align-items-center">
               <div class="col-12 col-md-5 order-md-2">
                  <div class="step-shape">Step 2</div>
                  <div class="f-22 f-md-32 w800 lh140 black-clr mt15">Create </div>
                  <div class="f-18 f-md-20 w400 lh140 black-clr mt10">
                     Just copy-paste text script, use a keywords, or stock video clips to create engaging videos within minutes. 
                  </div>
               </div>
               <div class="col-12 col-md-7 mt20 mt-md0 order-md-1">
                  <img src="https://cdn.oppyo.com/launches/vidboxs/fe/step2.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>
            <div class="row mt20 mt-md70 align-items-center">
               <div class="col-12 col-md-5">
                  <div class="step-shape">Step 3</div>
                  <div class="f-22 f-md-32 w800 lh140 black-clr mt15">Publish and Profit </div>
                  <div class="f-18 f-md-20 w400 lh140 black-clr mt10">
                     Now publish your Shorts on YouTube with a click to drive unlimited viral traffic & sales on your offer, page, or website.          
                  </div>
               </div>
               <div class="col-12 col-md-7 mt20 mt-md0">
                  <img src="https://cdn.oppyo.com/launches/vidboxs/fe/step3.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>
         </div>
      </div>
      <!-- Step Section End -->

         <!-- Demo Section  -->
         <div class="demo-sec" id="demo">
            <div class="container">
               <div class="row">
                  <div class="col-12">
                     <div class="red-clr f-md-45 f-28 w800 text-center">
                        Watch VidBoxs in Action
                     </div>
                  </div>
                  <div class="col-12 col-md-9 mx-auto mt-md40 mt20">
                       <div class="responsive-video">
                        <iframe src="https://vidboxs.dotcompal.com/video/embed/l9gjujg7vk" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                           box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen"
                           allowfullscreen></iframe>
                        </div>
                  </div>
                  <div class="col-12 text-center mt20 mt-md40">
                     <div class="f-18 f-md-26 w800 lh140 text-center black-clr">
                        Free Commercial License + Low 1-Time Price For Launch Period Only
                     </div>
                     <div class="f-20 f-md-24 w400 lh140 text-center mt20 black-clr">
                        Use Discount Coupon <span class="red-clr w700">"vidboxs"</span> for Instant <span class="red-clr w700">$3 OFF</span>
                    </div>
                     <div class="row">
                        <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center">
                           <a href="#buynow" class="cta-link-btn">Get Instant Access To VidBoxs</a>
                        </div>
                     </div>
                     <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                        <img src="https://cdn.oppyo.com/launches/vidboxs/fe/compaitable-with.webp" class="img-responsive mx-xs-center md-img-right" alt="visa">
                        <div class="d-md-block d-none visible-md px-md30">
                           <img src="https://cdn.oppyo.com/launches/vidboxs/fe/v-line.webp" class="img-fluid" alt="line">
                        </div>
                        <img src="https://cdn.oppyo.com/launches/vidboxs/fe/days-gurantee.webp" class="img-responsive mx-xs-auto mt15 mt-md0" alt="30 days">
                     </div>
                     <div class="mt20 mt-md40 d-inline-flex align-items-center">
                        <img src="https://cdn.oppyo.com/launches/vidboxs/fe/limited-time-offer.webp" class="img-fluid mx-auto d-block">
                        <div class="cta-shape f-18 f-md-22 w800">Grab Your Copy Before The Price Increases!</div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- Demo Section End -->


               <!-- Testimonials Section -->
      <div class="testimonial-section">
         <div class="container ">
            <div class="row ">
               <div class="f-md-45 f-28 w800 lh140 text-center black-clr">
                  Here's What REAL Users Have to <br class="d-none d-md-block"> Say About VidBoxs
               </div>
            </div>
            <div class="row mt25 mt-md50 align-items-center">
               <div class="col-12 col-md-10 offset-md-2">
                  <div class="testi-block">
                     <div class="row d-flex align-items-center flex-wrap">
                         <div class="col-12 col-md-3">
                             <img src="https://cdn.oppyo.com/launches/vidboxs/fe/c4.webp" class="img-fluid d-block mx-auto">
                             <div class="f-22 f-md-24 w800 lh120 red-clr mt20 text-center">Lucas Rodriguez</div>
                         </div>
                         <div class="col-12 col-md-9 mt20 mt-md0 text-center text-md-start">
                             <div class="f-22 f-md-28 w800 lh140">Easy To Use Software with Training Included</div>
                             <div class="f-20 w400 lh140 mt20 quote1">
                              VidBoxs is next-level quality software to full fill all my YouTube marketing needs. Also, it is  <span class="w700">so easy to use, and the training included makes it even easier</span> to create highly engaging and professional videos & shorts in a few minutes. I'd say that this is a MUST HAVE technology for marketers.
                             </div>
                            
                         </div>
                     </div>
                 </div>
               </div>
               <div class="col-12 col-md-10 mt20 mt-md40">
                  <div class="testi-block">
                     <div class="row d-flex align-items-center flex-wrap">
                         <div class="col-12 col-md-3 order-md-2">
                             <img src="https://cdn.oppyo.com/launches/vidboxs/fe/c5.webp" class="img-fluid d-block mx-auto">
                             <div class="f-22 f-md-24 w800 lh120 red-clr mt20 text-center">Theodore Miller</div>
                         </div>
                         <div class="col-12 col-md-9 order-md-1 mt20 mt-md0 text-center text-md-start">
                             <div class="f-22 f-md-28 w800 lh140">Revolutionary software comes at a one-time price</div>
                             <div class="f-20 w400 lh140 mt20 quote1">
                              VidBoxs combines ease of use with  <span class="w700">super-powerful video creation technology.</span> I'm using it to create short videos for my YouTube channel that are well-suited to my audience.<br><br>
                              Trust me, it works like a breeze and the best part is, <span class="w700">that this revolutionary software comes at a one-time price.</span> It’s amazing! I am enjoying working with it. Great Job Ayush & Pranshu. 
                              
                             </div>
                           
                         </div>
                     </div>
                 </div>
               </div>
               <div class="col-12 col-md-10 offset-md-2 mt20 mt-md40">
                  <div class="testi-block">
                     <div class="row d-flex align-items-center flex-wrap">
                         <div class="col-12 col-md-3">
                             <img src="https://cdn.oppyo.com/launches/vidboxs/fe/c6.webp" class="img-fluid d-block mx-auto">
                             <div class="f-22 f-md-24 w800 lh120 red-clr mt20 text-center">Harper Garcia</div>
                         </div>
                         <div class="col-12 col-md-9 mt20 mt-md0 text-center text-md-start">
                             <div class="f-22 f-md-28 w800 lh140">I Can Make Money with Complete freedom</div>
                             <div class="f-20 w400 lh140 mt20 quote1">
                              I have been in the Video Marketing arena for quite some time now, and I must say that VidBoxs will take the industry by storm.<br.<br>
                              The coolest part is that <span class="w700">it enables you to create video shorts in just a few minutes which usually takes me a long day to create even a single video</span> to boost engagement and traffic on my YouTube channel. Now, I can make money with Amazon/Affiliate offers, AdSense & my own offers with fewer efforts on video creation.<span class="w700"> Complete freedom is what it provides me.</span> Something to check on a serious note. 
                              
                             </div>
                            
                         </div>
                     </div>
                 </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Testimonials Section End -->


      <div class="feature-highlight" id="features">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-40 f-24 w800 text-center black-clr lh140">
                     Here Are The GROUND - BREAKING Features<br class="d-none d-md-block"> 
                     That Makes VidBoxs A CUT ABOVE The Rest      
               </div>
            </div>
         </div>
      </div>
      <!-- Features Section Star -->
      <div class="features-section-one">
         <div class="container">           
            <div class="row align-items-center">
               <div class="col-12 mb20 mb-md50">
                  <div class="f-md-34 f-28 w700 lh140 white-clr">
                     Create Highly Engaging and Professional Short Videos for YouTube
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="f-md-32 f-24 w700 lh140 white-clr">
                     Create High-Quality Explanatory Whiteboard Video Shorts 
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 white-clr mt15">
                     Create whiteboard video shorts by just adding your text and 
                     customizing font, color, and alignment. These videos are highly 
                     effective and informative.                     
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0">
                  <img src="https://cdn.oppyo.com/launches/vidboxs/fe/f1.webp" class="img-fluid d-block mx-auto" alt="Features">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-md-6 order-md-2">
                  <div class="f-md-32 f-24 w700 lh140 white-clr">
                     Create Videos by Images
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 white-clr mt15">
                     Find images for videos by searching images with keywords
                     inside VidBoxs or upload your own images. 
                   </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0 order-md-1">
                  <img src="https://cdn.oppyo.com/launches/vidboxs/fe/f2.webp" class="img-fluid d-block mx-auto" alt="Features">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-md-6">
                  <div class="f-md-32 f-24 w700 lh140 white-clr">
                     Create Videos Using Video Clips
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 white-clr mt15">
                     You can create video by adding one or more video clips. 
                     You can use stock videos from VidBoxs library or can 
                     upload your own clips.                      
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0">
                  <img src="https://cdn.oppyo.com/launches/vidboxs/fe/f3.webp" class="img-fluid d-block mx-auto" alt="Features">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-md-6 order-md-2">
                  <div class="f-md-32 f-24 w700 lh140 white-clr">
                     Add Voiceover or Background Music to Any of Your Videos
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 white-clr mt15">
                     You can add voiceover, background music or both to your videos 
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0 order-md-1">
                  <img src="https://cdn.oppyo.com/launches/vidboxs/fe/f4.webp" class="img-fluid d-block mx-auto" alt="Features">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-md-6">
                  <div class="f-md-32 f-24 w700 lh140 white-clr">
                     Easy To Use Video Creator
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 white-clr mt15">
                     Built-In, 100% newbie-friendly video creator needs no prior 
                     designing or editing skills. Even a first-timer can easily create 
                     & customize videos with Video Animation Effects.                     
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0">
                  <img src="https://cdn.oppyo.com/launches/vidboxs/fe/f5.webp" class="img-fluid d-block mx-auto" alt="Features">
               </div>
            </div>
         </div>
      </div>
      <div class="grey-section1">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-6">
                  <div class="f-md-32 f-24 w700 lh140 black-clr">
                     A.I Based Text to Speech Creator
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 black-clr mt15">
                     Get an Inbuilt AI-Based Text to Speech Converter with a 
                     Total 0f 75+ Human and AI Voices & 30+ languages both 
                     for males as well as females.                     
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0">
                  <img src="https://cdn.oppyo.com/launches/vidboxs/fe/f6.webp" class="img-fluid d-block mx-auto" alt="Features">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-md-6 order-md-2">
                  <div class="f-md-32 f-24 w700 lh140 black-clr">
                     Voiceover Speed Control
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 black-clr mt15">
                     You can manage the speaking speed and volume of your 
                     voiceover and can set other controls.                     
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0 order-md-1">
                  <img src="https://cdn.oppyo.com/launches/vidboxs/fe/f7.webp" class="img-fluid d-block mx-auto" alt="Features">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-md-6">
                  <div class="f-md-32 f-24 w700 lh140 black-clr">
                     Use Preloaded Background Music or Add Your Own 
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 black-clr mt15">
                     You can add background music to any video from the VidBoxs 
                     library or can upload your own background music.
                     
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0">
                  <img src="https://cdn.oppyo.com/launches/vidboxs/fe/f8.webp" class="img-fluid d-block mx-auto" alt="Features">
               </div>
            </div>
         </div>
      </div>
      <div class="white-feature">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-6 order-md-2">
                  <div class="f-md-32 f-24 w700 lh140 black-clr">
                     In-Built Content Spinner
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 black-clr mt15">
                     Use the Spin Content Feature & modify the text to get unique 
                     and professional content for Video and Voiceover. No external 
                     tools are required. 
                     
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0 order-md-1">
                  <img src="https://cdn.oppyo.com/launches/vidboxs/fe/f9.webp" class="img-fluid d-block mx-auto" alt="Features">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-md-6">
                  <div class="f-md-32 f-24 w700 lh140 black-clr">
                     Multiple Video Resolution
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 black-clr mt15">
                     You can set the Video Resolution as per your requirement and audience.              
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0">
                  <img src="https://cdn.oppyo.com/launches/vidboxs/fe/f10.webp" class="img-fluid d-block mx-auto" alt="Features">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-md-6 order-md-2">
                  <div class="f-md-32 f-24 w700 lh140 black-clr">
                     Build Authority by Adding Your Branding 
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 black-clr mt15">
                     Build your authority in the audience by adding your brand 
                     watermarks and logos in the video.                     
                     
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0 order-md-1">
                  <img src="https://cdn.oppyo.com/launches/vidboxs/fe/f11.webp" class="img-fluid d-block mx-auto" alt="Features">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-md-6">
                  <div class="f-md-32 f-24 w700 lh140 black-clr">
                     Share Shorts Directly to YouTube
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 black-clr mt15">
                     You can easily share the video on YouTube or can download 
                     it is to use wherever you want.
                      
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0">
                  <img src="https://cdn.oppyo.com/launches/vidboxs/fe/f12.webp" class="img-fluid d-block mx-auto" alt="Features">
               </div>
            </div>
         </div>
      </div>

      <div class="grey-section1">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-6 order-md-2">
                  <div class="f-md-32 f-24 w700 lh140 black-clr">
                     Completely Cloud-Based Software
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 black-clr mt15">
                     Fully Cloud-Based Software with Zero Tech 
                     Hassles & 24*7 Customer Support                               
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0 order-md-1">
                  <img src="https://cdn.oppyo.com/launches/vidboxs/fe/f13.webp" class="img-fluid d-block mx-auto" alt="Features">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md50">
               <div class="col-md-6">
                  <div class="f-md-32 f-24 w700 lh140 black-clr">
                     100% Newbie Friendly
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 black-clr mt15">
                     Easy and Intuitive to Use Software. Also comes with 
                     Step-by-Step Video Training & PDF
                               
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0">
                  <img src="https://cdn.oppyo.com/launches/vidboxs/fe/f14.webp" class="img-fluid d-block mx-auto" alt="Features">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md50">
               <div class="col-md-6 order-md-2">
                  <div class="f-md-32 f-24 w700 lh140 black-clr">
                     No Monthly Fees or Additional Charges
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0 order-md-1">
                  <img src="https://cdn.oppyo.com/launches/vidboxs/fe/f15.webp" class="img-fluid d-block mx-auto" alt="Features">
               </div>
            </div>
         </div>
      </div>
      <!-- Features Section End -->
       <!-- CTA Section -->
       <div class="cta-section-new">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-18 f-md-26 w800 lh140 text-center white-clr">
                     Free Commercial License + Low 1-Time Price For Launch Period Only
                  </div>
                  <div class="f-20 f-md-24 w400 lh140 text-center mt20 white-clr">
                     Use Discount Coupon <span class="orange-clr w700">"vidboxs"</span> for Instant <span class="orange-clr w700">$3 OFF</span>
                 </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center">
                        <a href="#buynow" class="cta-link-btn">Get Instant Access To VidBoxs</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                     <img src="https://cdn.oppyo.com/launches/vidboxs/fe/compaitable-with1.webp" class="img-responsive mx-xs-center md-img-right" alt="visa">
                     <div class="d-md-block d-none visible-md px-md30">
                        <img src="https://cdn.oppyo.com/launches/vidboxs/fe/v-line.webp" class="img-fluid" alt="line">
                     </div>
                     <img src="https://cdn.oppyo.com/launches/vidboxs/fe/days-gurantee1.webp" class="img-responsive mx-xs-auto mt15 mt-md0" alt="30 days">
                  </div>
                  <div class="mt20 mt-md40 d-inline-flex align-items-center">
                     <img src="https://cdn.oppyo.com/launches/vidboxs/fe/limited-time-offer.webp" class="img-fluid mx-auto d-block">
                     <div class="cta-shape f-18 f-md-22 w800">Grab Your Copy Before The Price Increases!</div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- CTA Section End -->
      
      <!-- Compare Table -->
      <!-- <section class="comp-table">
         <div class="comparetable-section">
            <div class="container ">
               <div class="row ">
                  <div class="col-12 text-center">
                     <div class="but-design-border">
                        <div class="f-md-45 f-28 lh140 w800 white-clr button-shape">
                           There Is No Competition!
                        </div>
                     </div>
                  </div>
                  <div class="col-12 f-22 f-md-36 w800 lh140 black-clr text-center mt30">
                     No Other Technology Comes Even Close To VidBoxs
                  </div>
                  <div class="col-12">
                     <div class="f-md-36 f-22 w600 black-clr text-center ">
                     </div>
                     <div class="row g-0 mt70 mt-md100">
                        <div class="col-md-4 col-4">
                           <div class="fist-row">
                              <ul class="f-md-18 f-14 w500 lh120">
                              <li class="f-md-32 f-16 w800 justify-content-start justify-content-md-center">
                                 Features
                              </li>
                              <li class="f-md-28 w600">Price </li>
                              <li class="w600">Create Unlimited Videos</li>
                              <li class="text-start w600">Generate Unlimited Voiceover</li>
                              <li class="w600">Create Podcast</li>
                              <li class="w600">Whiteboard Videos </li>
                              <li><span class="w600">Explainer Videos</li>
                              <li>Videos by Keyword Search</li>
                              <li class="w600">Create Video from Your Images </li>
                              <li><span class="w600">Easy of Use</span></li>
                              <li class="w600">A.I Text To Speech Converter</li>
                              <li class="w600">150+ Tone & 30+ Languages</li>
                              <li>Multiple Video <span class="w600">Quality</span> </li>
                              <li class="w600">Withelable - Add you Own Branding</li>
                              <li><span class="w600">Stock Images & Videos</span> </li>
                              <li> <span class="w600">Background Music</span></li>
                              <li><span class="w600">Content Spinner </span></li>
                              <li class="w600">Storage Drive</li>
                              <li class="w600">Unlimited Media Storage</li>
                              <li class="w600">Host & Play Videos </li>
                              <li class="w600">Store & Share Media</li>
                              <li class="w600">Branded Video Channels & Share Page</li>
                              <li class="w600">Folder Management </li>
                              <li><span class="w600">Online Backup & 30 Days Recovery</span></li>
                              <li><span class="w600">Team Management</span></li>
                           </div>
                        </div>
                        <div class="col-md-2 col-2">
                           <div class="second-row">
                              <ul class="f-md-16 f-14 w600 white-clr ttext-center lh120">
                                 <li class="f-md-20 f-16 white-clr">
                                    <svg version="1.1" id="Layer_11" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 865.5 207.4" style="enable-background:new 0 0 865.5 207.4; max-height:50px" xml:space="preserve">
                                       <style type="text/css">
                                          .st00{fill:#FFFFFF;}
                                          .st11{fill:url(#SVGID_1_);}
                                          .st22{fill:url(#SVGID_00000003804682614402079440000013301449542194118298_);}
                                          .st33{fill:url(#SVGID_00000106859270421856661840000011347085370863025833_);}
                                          .st44{opacity:0.3;}
                                          .st55{fill:#0F0F0F;}
                                       </style>
                                       <g>
                                          <path class="st00" d="M482.4,27.5v105.7c0,17.9-10.1,33.4-24.9,41.3v-32.9c1-2.6,1.6-5.4,1.6-8.4s-0.6-5.8-1.6-8.4V27.5H482.4z"></path>
                                          <path class="st00" d="M457.6,92c-6.5-3.4-13.9-5.4-21.8-5.4c-25.8,0-46.6,20.9-46.6,46.6c0,25.8,20.9,46.6,46.6,46.6
                                             c7.9,0,15.3-1.9,21.8-5.4c14.8-7.8,24.9-23.4,24.9-41.3C482.4,115.3,472.4,99.8,457.6,92z M457.6,141.6c-2,5.3-5.9,9.6-10.9,12.2
                                             c-3.2,1.7-7,2.7-10.9,2.7c-12.9,0-23.3-10.4-23.3-23.3s10.4-23.3,23.3-23.3c3.9,0,7.6,1,10.9,2.7c5,2.6,8.8,7,10.9,12.2
                                             c1,2.6,1.6,5.4,1.6,8.4C459.1,136.2,458.6,139,457.6,141.6z"></path>
                                          <path class="st00" d="M498,27.5v105.7c0,17.9,10.1,33.4,24.9,41.3v-32.9c-1-2.6-1.6-5.4-1.6-8.4s0.6-5.8,1.6-8.4V27.5H498z"></path>
                                          <path class="st00" d="M498,133.2c0,17.9,10.1,33.4,24.9,41.3c6.5,3.4,13.9,5.4,21.8,5.4c25.8,0,46.6-20.9,46.6-46.6
                                             c0-25.8-20.9-46.6-46.6-46.6c-7.9,0-15.3,1.9-21.8,5.4C508.1,99.8,498,115.3,498,133.2z M521.3,133.2c0-3,0.6-5.8,1.6-8.4
                                             c2-5.3,5.9-9.6,10.9-12.2c3.2-1.7,7-2.7,10.9-2.7c12.9,0,23.3,10.4,23.3,23.3s-10.4,23.3-23.3,23.3c-3.9,0-7.6-1-10.9-2.7
                                             c-5-2.6-8.8-7-10.9-12.2C521.8,139,521.3,136.2,521.3,133.2z"></path>
                                          <path class="st00" d="M597.5,133.2c0,17.9,10.1,33.4,24.9,41.3c6.5,3.4,13.9,5.4,21.8,5.4c25.8,0,46.6-20.9,46.6-46.6
                                             c0-25.8-20.9-46.6-46.6-46.6c-7.9,0-15.3,1.9-21.8,5.4C607.5,99.8,597.5,115.3,597.5,133.2z M620.8,133.2c0-3,0.6-5.8,1.6-8.4
                                             c2-5.3,5.9-9.6,10.9-12.2c3.2-1.7,7-2.7,10.9-2.7c12.9,0,23.3,10.4,23.3,23.3s-10.4,23.3-23.3,23.3c-3.9,0-7.6-1-10.9-2.7
                                             c-5-2.6-8.8-7-10.9-12.2C621.3,139,620.8,136.2,620.8,133.2z"></path>
                                          <polygon class="st00" points="721.5,179.9 690.4,179.9 756.1,86.5 787.2,86.5 	"></polygon>
                                          <polygon class="st00" points="756.1,179.9 787.2,179.9 721.5,86.5 690.4,86.5 	"></polygon>
                                          <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="291.6235" y1="980.703" x2="309.5574" y2="1019.9633" gradientTransform="matrix(1 0 0 1 0 -870)">
                                             <stop offset="0" style="stop-color:#F40C28"></stop>
                                             <stop offset="0.8911" style="stop-color:#FF2C50"></stop>
                                          </linearGradient>
                                          <polygon class="st11" points="264.4,179.9 278.5,150 308.3,86.5 339.4,86.5 295.5,179.9 292.5,179.9 	"></polygon>
                                          <polygon class="st00" points="292.5,179.9 278.5,150 248.6,86.5 217.5,86.5 261.5,179.9 264.4,179.9 	"></polygon>
                                          <rect x="351.9" y="86.6" class="st00" width="24.9" height="93.3"></rect>
                                          <rect x="351.9" y="51.8" class="st00" width="24.9" height="24.9"></rect>
                                          <g>
                                             <path class="st00" d="M810.1,175.9c-5.9-2.7-10.5-6.3-14-10.9c-3.4-4.6-5.3-9.6-5.6-15.2h23c0.4,3.5,2.1,6.4,5.1,8.6
                                                c3,2.3,6.7,3.4,11.2,3.4c4.3,0,7.8-0.9,10.2-2.6c2.4-1.7,3.7-4,3.7-6.7c0-2.9-1.5-5.1-4.5-6.6s-7.8-3.1-14.3-4.8
                                                c-6.7-1.6-12.3-3.3-16.6-5.1c-4.3-1.7-8-4.4-11.1-8c-3.1-3.6-4.7-8.4-4.7-14.5c0-5,1.4-9.6,4.3-13.7c2.9-4.1,7-7.4,12.4-9.8
                                                s11.7-3.6,19-3.6c10.8,0,19.4,2.7,25.8,8.1c6.4,5.4,10,12.6,10.6,21.8h-21.9c-0.3-3.6-1.8-6.4-4.5-8.6c-2.7-2.1-6.2-3.2-10.7-3.2
                                                c-4.1,0-7.3,0.8-9.5,2.3s-3.3,3.6-3.3,6.4c0,3,1.5,5.4,4.6,6.9c3,1.6,7.8,3.2,14.2,4.8c6.5,1.6,11.9,3.3,16.2,5.1
                                                c4.2,1.7,7.9,4.4,11,8.1c3.1,3.6,4.7,8.5,4.8,14.4c0,5.2-1.4,9.9-4.3,14c-2.9,4.1-7,7.4-12.4,9.7s-11.7,3.5-18.8,3.5
                                                C822.6,179.9,815.9,178.5,810.1,175.9z"></path>
                                          </g>
                                       </g>
                                       <g>
                                          <g>
                                             <linearGradient id="SVGID_00000090999748961388969530000010752564172096406444_" gradientUnits="userSpaceOnUse" x1="-1360.6178" y1="-1339.845" x2="-1175.7527" y2="-1283.7964" gradientTransform="matrix(0.6181 -0.7861 -0.7861 -0.6181 -176.8927 -1732.0651)">
                                                <stop offset="0.2973" style="stop-color:#F40C28"></stop>
                                                <stop offset="0.7365" style="stop-color:#FF2C50"></stop>
                                             </linearGradient>
                                             <path style="fill:url(#SVGID_00000090999748961388969530000010752564172096406444_);" d="M125.8,6.8c0.4,0.3,0.8,0.7,1.2,1
                                                c6.2,5.4,9.8,12.7,10.8,20.3c1,8.1-1.2,16.6-6.6,23.6l-75.8,96.4c0,0-18.4,20.5-8.9,35l-32-25.2c-1.1-0.9-2.2-1.8-3.2-2.8
                                                c-13.5-13.3-15.2-35-3.2-50.3l72.8-92.6C91.8-1.7,111.9-4.1,125.8,6.8z"></path>
                                             <linearGradient id="SVGID_00000108307696041867937860000005451407988123938486_" gradientUnits="userSpaceOnUse" x1="-1216.8146" y1="1899.8297" x2="-973.6699" y2="1975.1873" gradientTransform="matrix(-0.6181 0.7861 0.7861 0.6181 -2080.1633 -197.5988)">
                                                <stop offset="0.3687" style="stop-color:#F40C28"></stop>
                                                <stop offset="1" style="stop-color:#FF2C50"></stop>
                                             </linearGradient>
                                             <path style="fill:url(#SVGID_00000108307696041867937860000005451407988123938486_);" d="M67.7,199.8c-0.4-0.3-0.8-0.7-1.2-1
                                                c-6.2-5.4-9.8-12.7-10.8-20.3c-1-8.1,1.2-16.6,6.6-23.6l75.8-96.4c0,0,18.4-20.5,8.9-35l32,25.2c1.1,0.9,2.2,1.8,3.2,2.8
                                                c13.5,13.3,15.2,35,3.2,50.3l-72.8,92.6C101.7,208.3,81.6,210.7,67.7,199.8z"></path>
                                          </g>
                                          <g class="st44">
                                             <path class="st55" d="M68.8,69v78.4c0,8.7,9.8,13.7,16.8,8.6l54.6-39.2c5.9-4.2,5.9-13,0-17.3L85.6,60.4
                                                C78.6,55.3,68.8,60.4,68.8,69z"></path>
                                          </g>
                                          <g>
                                             <path class="st00" d="M66.8,66v78.4c0,8.7,9.8,13.7,16.8,8.6l54.6-39.2c5.9-4.2,5.9-13,0-17.3L83.6,57.4
                                                C76.6,52.3,66.8,57.4,66.8,66z"></path>
                                          </g>
                                       </g>
                                    </svg>
                                 </li>
                                 <li class="f-md-28 w600" style="line-height:36px;"> $37 <br>One Time</li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/right-tick.webp" class="img-fluid mx-auto d-block" alt="tick">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/right-tick.webp" class="img-fluid mx-auto d-block" alt="tick">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/right-tick.webp" class="img-fluid mx-auto d-block" alt="tick">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/right-tick.webp" class="img-fluid mx-auto d-block" alt="tick">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/right-tick.webp" class="img-fluid mx-auto d-block" alt="tick">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/right-tick.webp" class="img-fluid mx-auto d-block" alt="tick">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/right-tick.webp" class="img-fluid mx-auto d-block" alt="tick">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/right-tick.webp" class="img-fluid mx-auto d-block" alt="tick">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/right-tick.webp" class="img-fluid mx-auto d-block" alt="tick">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/right-tick.webp" class="img-fluid mx-auto d-block" alt="tick">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/right-tick.webp" class="img-fluid mx-auto d-block" alt="tick">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/right-tick.webp" class="img-fluid mx-auto d-block" alt="tick">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/right-tick.webp" class="img-fluid mx-auto d-block" alt="tick">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/right-tick.webp" class="img-fluid mx-auto d-block" alt="tick">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/right-tick.webp" class="img-fluid mx-auto d-block" alt="tick">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/right-tick.webp" class="img-fluid mx-auto d-block"
                                       alt="tick">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/right-tick.webp" class="img-fluid mx-auto d-block"
                                       alt="tick">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/right-tick.webp" class="img-fluid mx-auto d-block"
                                       alt="tick">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/right-tick.webp" class="img-fluid mx-auto d-block"
                                       alt="tick">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/right-tick.webp" class="img-fluid mx-auto d-block"
                                       alt="tick">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/right-tick.webp" class="img-fluid mx-auto d-block"
                                       alt="tick">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/right-tick.webp" class="img-fluid mx-auto d-block"
                                       alt="tick">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/right-tick.webp" class="img-fluid mx-auto d-block"
                                       alt="tick">
                                 </li>
                                 <li></li>
                              </ul>
                           </div>
                        </div>
                        <div class="col-md-2 col-2">
                           <div class="third-row">
                              <ul class="f-md-16 f-14 w500 lh120">
                                 <li class="f-md-32 f-16 w800"><span>Lovo</span></li>
                                 <li class="f-md-28 w600">$149/M</li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/red-cross.webp" class="img-fluid mx-auto d-block" alt="cross ">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/tick-icon.webp" class="img-fluid mx-auto d-block" alt="right ">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/red-cross.webp" class="img-fluid mx-auto d-block"
                                       alt="cross">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/red-cross.webp" class="img-fluid mx-auto d-block" alt="cross">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/tick-icon.webp" class="img-fluid mx-auto d-block" alt="right">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/red-cross.webp" class="img-fluid mx-auto d-block" alt="cross">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/red-cross.webp" class="img-fluid mx-auto d-block" alt="cross">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/tick-icon.webp" class="img-fluid mx-auto d-block" alt="right">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/tick-icon.webp" class="img-fluid mx-auto d-block"
                                       alt="right">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/tick-icon.webp" class="img-fluid mx-auto d-block"
                                       alt="right arrow">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/red-cross.webp" class="img-fluid mx-auto d-block"
                                       alt="cross">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/red-cross.webp" class="img-fluid mx-auto d-block"
                                       alt="cross">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/red-cross.webp" class="img-fluid mx-auto d-block"
                                       alt="cross">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/red-cross.webp" class="img-fluid mx-auto d-block"
                                       alt="cross">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/red-cross.webp" class="img-fluid mx-auto d-block"
                                       alt="cross">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/red-cross.webp" class="img-fluid mx-auto d-block"
                                       alt="cross">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/red-cross.webp" class="img-fluid mx-auto d-block"
                                       alt="cross">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/red-cross.webp" class="img-fluid mx-auto d-block"
                                       alt="cross">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/red-cross.webp" class="img-fluid mx-auto d-block"
                                       alt="cross">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/red-cross.webp" class="img-fluid mx-auto d-block"
                                       alt="cross">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/red-cross.webp" class="img-fluid mx-auto d-block"
                                       alt="cross">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/red-cross.webp" class="img-fluid mx-auto d-block"
                                       alt="cross">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/red-cross.webp" class="img-fluid mx-auto d-block"
                                       alt="cross">
                                 </li>
                              </ul>
                           </div>
                        </div>
                        <div class="col-md-2 col-2">
                           <div class="forth-row">
                              <ul class="f-md-16 f-14 w500 lh120">
                                 <li class="f-md-32 f-16 w800"><span>MURF</span></li>
                                 <li class="f-md-28 w600">$249/M</li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/red-cross.webp" class="img-fluid mx-auto d-block" alt="cross">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/tick-icon.webp" class="img-fluid mx-auto d-block" alt="right arrow">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/tick-icon.webp" class="img-fluid mx-auto d-block"
                                       alt="right arrow">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/red-cross.webp" class="img-fluid mx-auto d-block" alt="cross">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/tick-icon.webp" class="img-fluid mx-auto d-block"
                                       alt="right">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/red-cross.webp" class="img-fluid mx-auto d-block"
                                       alt="cross">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/tick-icon.webp" class="img-fluid mx-auto d-block" alt="right">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/tick-icon.webp" class="img-fluid mx-auto d-block"
                                       alt="right">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/tick-icon.webp" class="img-fluid mx-auto d-block"
                                       alt="right arrow">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/tick-icon.webp" class="img-fluid mx-auto d-block"
                                       alt="right arrow">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/tick-icon.webp" class="img-fluid mx-auto d-block"
                                       alt="right arrow">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/tick-icon.webp" class="img-fluid mx-auto d-block"
                                       alt="right arrow">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/red-cross.webp" class="img-fluid mx-auto d-block"
                                       alt="cross">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/tick-icon.webp" class="img-fluid mx-auto d-block"
                                       alt="right arrow">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/red-cross.webp" class="img-fluid mx-auto d-block"
                                       alt="cross">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/red-cross.webp" class="img-fluid mx-auto d-block"
                                       alt="cross">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/red-cross.webp" class="img-fluid mx-auto d-block"
                                       alt="cross">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/red-cross.webp" class="img-fluid mx-auto d-block"
                                       alt="cross">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/red-cross.webp" class="img-fluid mx-auto d-block"
                                       alt="cross">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/red-cross.webp" class="img-fluid mx-auto d-block"
                                       alt="cross">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/red-cross.webp" class="img-fluid mx-auto d-block"
                                       alt="cross">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/red-cross.webp" class="img-fluid mx-auto d-block"
                                       alt="cross">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/red-cross.webp" class="img-fluid mx-auto d-block"
                                       alt="cross">
                                 </li>
                              </ul>
                           </div>
                        </div>
                        <div class="col-md-2 col-2">
                           <div class="fifth-row">
                              <ul class="f-md-16 f-14">
                                 <li class="f-md-32 f-16 w800"><span>Woord</span></li>
                                 <li class="w600 f-md-28"> $100/M </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/red-cross.webp" class="img-fluid mx-auto d-block"
                                       alt="cross">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/tick-icon.webp" class="img-fluid mx-auto d-block"
                                       alt="right arrow">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/red-cross.webp" class="img-fluid mx-auto d-block"
                                       alt="cross">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/red-cross.webp" class="img-fluid mx-auto d-block"
                                       alt="cross">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/red-cross.webp" class="img-fluid mx-auto d-block"
                                       alt="cross">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/red-cross.webp" class="img-fluid mx-auto d-block"
                                       alt="cross">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/red-cross.webp" class="img-fluid mx-auto d-block"
                                       alt="cross">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/tick-icon.webp" class="img-fluid mx-auto d-block"
                                       alt="right">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/tick-icon.webp" class="img-fluid mx-auto d-block"
                                       alt="right">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/tick-icon.webp" class="img-fluid mx-auto d-block"
                                       alt="right">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/red-cross.webp" class="img-fluid mx-auto d-block"
                                       alt="cross">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/red-cross.webp" class="img-fluid mx-auto d-block"
                                       alt="cross">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/red-cross.webp" class="img-fluid mx-auto d-block"
                                       alt="cross">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/tick-icon.webp" class="img-fluid mx-auto d-block"
                                       alt="right">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/red-cross.webp" class="img-fluid mx-auto d-block"
                                       alt="cross">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/red-cross.webp" class="img-fluid mx-auto d-block"
                                       alt="cross">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/red-cross.webp" class="img-fluid mx-auto d-block"
                                       alt="cross">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/red-cross.webp" class="img-fluid mx-auto d-block"
                                       alt="cross">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/red-cross.webp" class="img-fluid mx-auto d-block"
                                       alt="cross">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/red-cross.webp" class="img-fluid mx-auto d-block"
                                       alt="cross">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/red-cross.webp" class="img-fluid mx-auto d-block"
                                       alt="cross">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/red-cross.webp" class="img-fluid mx-auto d-block"
                                       alt="cross">
                                 </li>
                                 <li>
                                    <img src="https://cdn.oppyo.com/launches/vidboxs/fe/red-cross.webp" class="img-fluid mx-auto d-block"
                                       alt="cross">
                                 </li>
                              </ul>
                           </div>
                        </div>
                     </div>
                     <div class="mt-md30 mt20 f-16 f-md-18 w400 lh140">
                        <span class="w800 ">Note:</span> All the features mentioned in the above table are bifurcated into different upgrade options according to the need of individual users and the features that you will get with the purchase of the VidBoxs Start or Pro plan is mentioned in the pricing table below on this page.
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section> -->
      <!-- Compare Table End-->
      <!-- Imagine Section Start -->
      <section class="imagine-sec">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-12 col-md-6 f-18 f-md-20 w400 lh140 black-clr order-md-2">
                  A new lightweight piece of technology that’s so incredibly sophisticated yet intuitive & easy to use that allows you to do it all, with literally ONE click!<br><br>
                  <span class="w800">That’s EXACTLY what we’ve designed VidBoxs to do for you.</span>
                  <br><br>
                  So, if you want to build super engaging Short Videos for YouTube and other social media at the push of a button, and get viral traffic automatically and convert it into SALES & PROFITS, all from start to finish, then VidBoxs is made for you!
               </div>
               <div class="col-md-6 col-12 order-md-1 mt20 mt-md0">
                  <img src="https://cdn.oppyo.com/launches/vidboxs/fe/imagine-box.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>
         </div>
      </section>
      <!-- Imagine Section End -->

      <div class="targeted-section">
         <div class="container">
             <div class="row">
                 <div class="col-12">
                     <div class="row d-flex align-items-center flex-wrap">
                         <div class="col-12 col-md-7">
                           <div class="f-18 f-md-20 w400 lh140 black-clr">
                              <ul class="noneed-listing pl0">
                                 <li>Create instant videos shorts for YouTube and other social media in just 3 clicks</li>
                                 <li>Drive tons of viral traffic from RED-HOT YouTube & other social media giants. </li>
                                 <li>Get the best results without getting into complex video creation & editing hassles</li>
                                 <li>Get more exposure for your offers in a hands-down manner</li>
                                 <li>Boost sales & profits without spending much</li>
                                 <li>Jump into the most trending video marketing strategy with YouTube Shorts.</li>
                              </ul>
                           </div>
                         </div>
                         <div class="col-12 col-md-5 mt20 mt-md0">
                             <img src="https://cdn.oppyo.com/launches/vidboxs/fe/target-arrow.webp" class="img-fluid d-block mx-auto">
 
                         </div>
                     </div>
                 </div>
                 <div class="col-12 mt-md30 mt20">
                     <div class="f-18 f-md-20 w400 lh140 black-clr">
                        <span class="w700">Seriously, having to do all video creation manually is expensive,</span> time-consuming and put frankly downright irritating, especially when you have to do this on a daily basis. <br><br>
                        That's exactly the reason so many people are afraid to even get started with YouTube Shorts and the same  <span class="w700">reason why out of all that DO get started, very few truly "make it".</span><br><br>
                        
                        We have done all the grunt work for you. So, you don’t need to worry at all
                        
 
                     </div>
                 </div>
             </div>
         </div>
     </div>
      <!-- Need Section Start -->

      <div class="need-marketing">
         <div class="container">
             <div class="row">
                 <div class="col-12">
                     <div class="f-md-45 f-28 lh140 w800 text-center black-clr">Who Can Benefit from VidBoxs?
                     </div>
                 </div>
                 <div class="col-12 mt-md30 mt20">
                     <div class="f-20 f-md-22 w400 lh140 black-clr">
                       <span class="w700"> This is Simple.</span> It helps<br><br>
                     </div>
                 </div>
             
              <div class="col-12 mt0 mt-md20">
                     <div class="row flex-wrap">
                         <div class="col-12 col-md-5">
                             <div class="f-20 f-md-20 w400 lh140 black-clr">
                                 <ul class="noneed-listing pl0">
<li> <span class="w700">Marketers</span> to drive more traffic</li>
<li> <span class="w700">Affiliates,</span> make more commissions</li>
<li> <span class="w700">SEO marketers</span> get #1 ranking and traffic</li>
<li> <span class="w700">Social & Video Marketers</span> get free viral traffic</li>
<li> <span class="w700">Creators</span> can get more income handsfree without extra efforts in video creation.</li> 
                                    
 
                                 </ul>
                             </div>
                         </div>
                         <div class="col-12 col-md-7 mt0 mt-md0">
                            <div class="f-20 f-md-20 w400 lh140 black-clr">
                                 <ul class="noneed-listing pl0">
<li><span class="w700">Product sellers, </span> sell more with RED HOT traffic</li>
<li><span class="w700">List builders</span>  skyrocket their subscribers...</li>
<li><span class="w700">Freelancers and Agencies –</span>  Provide RED Hot service & make profits</li>
<li><span class="w700">Local Business Owners – </span> get more engagement & TRAFFIC</li>
<li>Weight Loss, Gaming, Real Estate, Health, Wealth –</span> <span class="w700">Any Kind of Business</span> </li>

 
                                 </ul>
                             </div>
                         </div>
                     </div>
                 </div>
             
              <div class="col-12 mt20 mt-md70 text-center">
                     
                         <div class="f-22 f-md-32 w800 lh140 red-clr">
                           It Works Seamlessly for ANY NICHE… All at the push of a button. 
                         </div>
                 </div>
             
                 <div class="col-12 mt20 mt-md50">
                     <div class="row align-items-center flex-wrap">
                         <div class="col-12 col-md-7">
                   <div class="f-20 f-md-26 w700 lh140 black-clr text-center text-md-start">
                     VidBoxs Is Designed to Meet Every Marketer’s Need:
                         </div>
                             <div class="f-20 f-md-20 w400 lh140 black-clr mt20">
                      
                                 <ul class="noneed-listing pl0">
<li>Anyone who wants a nice PASSIVE income while focusing on bigger projects</li>
<li>Lazy people who want easy traffic & profits</li>
<li>Anyone who wants to value their business and money and is not ready to sacrifice them</li>
<li>People with products or services who want to kickstart online</li>                                    
 
                                 </ul>
                             </div>
                         </div>
                         <div class="col-12 col-md-5 mt20 mt-md0">
                             <img src="https://cdn.oppyo.com/launches/vidboxs/fe/platfrom.webp" class="img-fluid d-block mx-auto">
                         </div>
                     </div>
                 </div>
                 <div class="col-12 mt20 mt-md70 text-center">
                     <div class="look-shape">
                         <div class="f-22 f-md-30 w800 lh140 black-clr">
                           Look, it doesn't matter who you are or what you're doing.
                         </div>
                         <div class="f-20 f-md-22 w500 lh140 black-clr mt10">
                           If you want to finally be successful online, make the money that you want and live in a way<br class="d-none d-md-block"> you dream of, VidBoxs is for you.
                         </div>
                     </div>
                 </div>
                 <div class="col-12 mt20 mt-md70">
                     <div class="f-md-45 f-28 lh140 w800 text-center black-clr">Even BETTER: It’s Entirely Newbie Friendly
                     </div>
                 </div>
                 <div class="col-12 mt20 mt-md50">
                     <div class="row align-items-center flex-wrap">
                         <div class="col-12 col-md-7">
                             <div class="f-18 f-md-20 w400 lh140 black-clr">
                              Whatever your level of technical expertise, this amazing software enables 
                              you to create engaging & highly professional video shorts for any niche in 
                              the 7-minute flat. It really doesn’t matter.
                              <br><br>
                              <span class="w700">The ease of use for VidBoxs is STUNNING,</span> allowing users to drive 
                              free viral traffic from YouTube, without ever having to turn their hair 
                              grey for complex video editing skills.
                              <br><br>
                              With VidBoxs, not only do you get the BEST possible system for the LOWEST 
                              price around, but you can also sleep safe knowing you CONTROL every aspect of your business, from building a stunning video marketing strategy to promoting affiliate products, to traffic generation, monetization, list building, and TON more - no need to ever outsource those tasks again!
                              
                             </div>
                         </div>
                         <div class="col-12 col-md-5 mt20 mt-md0">
                             <img src="https://cdn.oppyo.com/launches/vidboxs/fe/better.webp" class="img-fluid d-block mx-auto">
                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </div>
      <!-- Need Section End -->
      <!-- CTA Section -->
      <div class="cta-section-new">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-18 f-md-26 w800 lh140 text-center white-clr">
                     Free Commercial License + Low 1-Time Price For Launch Period Only
                  </div>
                  <div class="f-20 f-md-24 w400 lh140 text-center mt20 white-clr">
                     Use Discount Coupon <span class="orange-clr w700">"vidboxs"</span> for Instant <span class="orange-clr w700">$3 OFF</span>
                 </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center">
                        <a href="#buynow" class="cta-link-btn">Get Instant Access To VidBoxs</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                     <img src="https://cdn.oppyo.com/launches/vidboxs/fe/compaitable-with1.webp" class="img-responsive mx-xs-center md-img-right" alt="visa">
                     <div class="d-md-block d-none visible-md px-md30">
                        <img src="https://cdn.oppyo.com/launches/vidboxs/fe/v-line.webp" class="img-fluid" alt="line">
                     </div>
                     <img src="https://cdn.oppyo.com/launches/vidboxs/fe/days-gurantee1.webp" class="img-responsive mx-xs-auto mt15 mt-md0" alt="30 days">
                  </div>
                  <div class="mt20 mt-md40 d-inline-flex align-items-center">
                     <img src="https://cdn.oppyo.com/launches/vidboxs/fe/limited-time-offer.webp" class="img-fluid mx-auto d-block">
                     <div class="cta-shape f-18 f-md-22 w800">Grab Your Copy Before The Price Increases!</div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- CTA Section End -->
      <!-- Silver Platter Section Start -->
      <!-- <div class="silver-platter-section ">
         <div class="container ">
            <div class="row mx-0 ">
               <div class="col-12 huge-opportunity-shape ">
                  <div class="f-28 f-md-48 w800 lh140 black-clr text-center ">
                     VidBoxs Brings You<br class="d-none d-md-block ">A HUGE Opportunity On A Silver Platter
                  </div>
                  <div class="f-24 f-md-36 w700 lh140 black-clr text-center mt15 ">
                     You Just Need To Capitalize On It – The Easier Way. No Competition With Others.
                  </div>
               </div>
            </div>
         </div>
      </div> -->
      <!-- Silver Platter Section End -->
      <!-- <div class="wearenot-alone-section">
         <div class="container ">
            <div class="row mx-0 ">
               <div class="col-12 col-md-10 mx-auto alone-bg ">
                  <div class="row align-items-center">
                     <div class="col-md-1 ">
                        <img src="https://cdn.oppyo.com/launches/vidboxs/fe/dollars-image.webp " class="img-fluid d-block dollars-sm-image " />
                     </div>
                     <div class="col-md-11 f-24 f-md-36 w700 lh140 black-clr text-center mt15 mt-md0 ">
                        You can build a profitable 6-figure Agency <br class="visible-md"> by just serving 1 client a day.
                     </div>
                  </div>
               </div>
               <div class="col-12 px10 f-28 f-md-40 w800 lh140 black-clr text-center mt30 mt-md80 ">Here’s a simple math</div>
               <div class="col-12 mt30 mt-md50">
                  <img src="https://cdn.oppyo.com/launches/vidboxs/fe/make-icon.webp " class="img-fluid d-block mx-auto " />
               </div>
               <div class="col-12 text-center mt20 mt-md70">
                  <div class="but-design-border">
                     <div class="f-md-45 f-28 lh140 w800 white-clr button-shape">
                        And It’s Not A 1-Time Income
                     </div>
                  </div>
               </div>
               <div class="col-12 f-20 f-md-22 w400 lh140 mt40  text-center ">
                  You can do this for life and charge your new as well as existing clients for your <br class="d-none d-md-block ">services again and again forever.
               </div>
            </div>
         </div>
      </div> -->
      <!-- Silver Platter Section End -->

      <!---Bonus Section Starts-->
      <div class="bonus-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="but-design-border">
                     <div class="f-md-45 f-28 lh140 w800 white-clr button-shape">
                        We’re not done Yet!
                     </div>
                  </div>
               </div>
               <div class="col-12 mt30 mt-md30 f-20 f-md-22 w500 black-clr lh140 text-center">
                  When You Grab Your VidBoxs Account Today, You’ll Also Get These <span class="w700">Fast Action Bonuses!</span>
               </div>
               <div class="col-12 ">
                  <div class="row ">
                     <!-------bonus 1------->
                     <div class="col-12 col-md-6 mt-md50 mt20 ">
                        <div class="bonus-section-shape ">
                           <div class="row align-items-center ">
                              <div class="col-md-6 mx-auto">
                                 <img src="https://cdn.oppyo.com/launches/vidboxs/fe/bonus-box.webp " class="img-fluid d-block mx-auto ">
                              </div>
                              <div class="col-12 mt20 mt-md40 ">
                                 <div class="w800 f-22 f-md-24 black-clr lh140 ">
                                    DotcomPal- All in One Growth Platform for Entrepreneurs (That's PRICELESS)
                                 </div>
                                 <div class="f-18 f-md-20 w400 black-clr lh140 mt15 ">
                                    All-in-one growth platform for Entrepreneurs & SMBs to generate more leads, bring more growth & sell more, more quickly. It’s got everything you need to convert more visitors and bring more growth at every customer touchpoint... without any designer, tech team, or the usual hassles. 
<br><br>
 This package is something that’s of huge worth for your business, but we’re offering it for free only with your investment in Cordova today. 
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-------bonus 2------->
                     <div class="col-12 col-md-6 mt-md50 mt20 ">
                        <div class="bonus-section-shape col-12 ">
                           <div class="row align-items-center ">
                              <div class="col-md-6 mx-auto col-12">
                                 <img src="https://cdn.oppyo.com/launches/vidboxs/fe/bonus-box.webp " class="img-fluid d-block mx-auto ">
                              </div>
                              <div class="col-12 mt20 mt-md40 ">
                                 <div class="w800 f-22 f-md-24 black-clr lh140 ">
                                    Video Training on Viral Marketing Secrets
                                 </div>
                                 <div class="f-18 f-md-20 w400 black-clr lh140 mt15 ">
                                    Believe it or not, people interested in whatever it is you are promoting are already congregating online. Social media platforms want you to succeed. The more popular your content becomes, the more traffic they get. 
<br><br>
                                    So, with this video training you will discover a shortcut to online viral marketing secrets. This will cover - The Two-Step Trick to Effective Viral Marketing, How Do You Find Hot Content? Maximize Niche Targeting for Your Curated Content, remember to protect yourself when sharing others’ content, how to share viral content on Facebook, how to share viral content on Twitter, Filter your content format to go viral on many platforms and much more. 
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt-md30 mt20 ">
                  <div class="row ">
                     <!-------bonus 3------->
                     <div class="col-12 col-md-6 ">
                        <div class="bonus-section-shape col-12 ">
                           <div class="row align-items-center ">
                              <div class="col-md-6 mx-auto">
                                 <img src="https://cdn.oppyo.com/launches/vidboxs/fe/bonus-box.webp " class="img-fluid d-block mx-auto ">
                              </div>
                              <div class="col-12 mt20 mt-md50">
                                 <div class="w800 f-22 f-md-24 black-clr lh140 "> Modern Video Marketing
                                 </div>
                                 <div class="f-18 f-md-20 w400 black-clr lh140 mt15 ">
                                    It's about time for you to learn the ins and outs of successful online video marketing! <br><br>

                                    Regardless of what you've heard, video marketing as a whole is not exactly a new phenomenon. Video marketing content is increasingly plugged into a larger marketing infrastructure.  
                                    <br><br>
                                    You have to wrap your mind around the fact that modern video marketing is both new and old. Depending on how you navigate these factors, you will either succeed or fail. Either your video is going to convert people into buyers or they're just going to sit there on YouTube getting zero views. 
                                    <br><br>
                                    So, with this video training you will discover the secrets of successful video marketing- The Modern and Effective Ways of Video Marketing, Modern Video Marketing Essentials, Types of Video Marketing and much more 
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-------bonus 4------->
                     <div class="col-12 col-md-6 mt20 mt-md0 ">
                        <div class="bonus-section-shape col-12 ">
                           <div class="row align-items-center ">
                              <div class="col-md-6 mx-auto">
                                 <img src="https://cdn.oppyo.com/launches/vidboxs/fe/bonus-box.webp " class="img-fluid d-block mx-auto ">
                              </div>
                              <div class="col-12 mt20 mt-md40">
                                 <div class="w800 f-22 f-md-24 black-clr lh140 ">
                                    Youtube Authority  
                                 </div>
                                 <div class="f-18 f-md-20 w400 black-clr lh140 mt15 ">
                                    Since its launch in 2005, YouTube has come a long way. It has become an extremely powerful tool for businesses to increase awareness of their brand, drive more traffic to their company sites, and reach a broad audience around the world.So, if you are isn’t already leveraging the power of YouTube there are some massive benefits that you’re missing out on. 
<br><br>
                                    With this video course you will: <br>
                                    <div class="f-20 f-md-20 w400 lh140 black-clr">
                      
                                       <ul class="noneed-listing pl0">
<li>A Clear understanding on starting a YouTube channel.  </li>
<li>Determine your target audience.  </li>
<li>Learn about the different types of videos  </li>
<li>Discover how you can increase engagement  </li>
<li>Learn the different avenues for monetizing your YouTube channel </li>
<li>Learn about the different mistakes that you can make on your YouTube channel     </li>                           
       
                                       </ul>
                                   </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt30 mt-md70">
                  <div class="f-24 f-md-50 w800 black-clr lh140 text-center ">
                     That’s A Total Value of <br class="d-none d-md-block "> <span class="f-28 f-md-50 red-clr"> $2285</span>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--Bonus Section ends -->
<!-- License Section Start -->
<div class="license-section">
   <div class="container">
      <div class="row">

         <div class="col-12 text-center">
            <div class="w800 f-28 f-md-45 black-clr text-center lh140">
               Also Get Our Free Commercial License When <br class="d-none d-md-block">
               You Get Access To VidBoxs Today!
            </div>
         </div>
      </div>
      <div class="row align-items-center mt20 mt-md70">
         <div class="col-md-6 order-md-2">
            <div class="f-md-20 f-18 w400 lh140 black-clr">
               As we have shown you, there are tons of businesses & marketers – yoga gurus, fitness centers, social media marketers or anybody else who want to start online & Drive FREE Traffic, NEED YOUR SERVICE & would love pay you monthly for your services.<br><br>

               Build their branded & traffic generating video channels. We’ve taken care of everything so that you can deliver those services easily & 100% profits in your pocket.<br><br>
               
              <span class="w700"> Note:</span> This special commercial license is being included in the Advanced plan and ONLY during this launch. Take advantage of it now because it will never be offered again.
            </div>
         </div>
         <div class="col-md-6 col-10 mx-auto mt20 mt-md0 order-md-1">
            <img src="https://cdn.oppyo.com/launches/vidboxs/fe/commercial-license.webp" class="img-fluid d-block mx-auto" alt="Features">
         </div>
      </div>
   </div>
</div>
<!-- License Section End -->
  <!-- Guarantee Section Start -->
  <div class="riskfree-section ">
   <div class="container ">
      <div class="row align-items-center ">
         <div class="col-12 text-center mb20 mb-md40">
            <div class="f-24 f-md-36 w600 lh140 white-clr">
               We’re Backing Everything Up with An Iron Clad...
            </div>
            <div class="f-md-45 f-28 w800 lh140 white-clr">
               "30-Day Risk-Free Money Back Guarantee"
            </div>
         </div>
         <div class="col-md-7 col-12 order-md-2">
            <div class="f-md-20 f-18 w400 lh140 white-clr mt15">
               I'm 100% confident that VidBoxs will help you take your online business to the next level. I'm so confident on it that I'm making this offer a risk-free investment for you.
<br><br>
               If you concluded that, HONESTLY nothing of this has helped you in any way, you can take advantage of our "30-day Money Back Guarantee" and simply ask for a refund within 30 days!
               <br><br>
               Note: For refund, we will require a valid reason along with the proof that you tried our system, but it didn't work for you!
               <br><br>
               I am considering your money to be kept safe on the table between us and waiting for you to APPLY and successfully use this product, and get best results, so then you can feel it is a great investment.
            </div>
         </div>
         <div class="col-md-5 order-md-1 col-12 mt20 mt-md0 ">
            <img src="https://cdn.oppyo.com/launches/vidboxs/fe/riskfree-img.webp " class="img-fluid d-block mx-auto ">
         </div>
      </div>
   </div>
</div>
<!-- Guarantee Section End -->

      <!-- Discounted Price Section Start -->
      <div class="table-section " id="buynow">
         <div class="container ">
            <div class="row ">
               <div class="col-12">
                  <div class="f-20 f-md-24 w500 text-center lh140">
                     Take Advantage of Our Limited Time Deal
                  </div>
                  <div class="f-md-48 f-28 w800 lh140 text-center mt10 ">
                     Get VidBoxs For A Low One-Time-<br class="d-none d-md-block">
                     Price, No Monthly Fee.
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 text-center mt20 balck-clr">
                     Use Discount Coupon <span class="red-clr w700">"vidboxs"</span> for Instant <span class="red-clr w700">$3 OFF</span> on Commercial Plan
                 </div>
               </div>
               <div class="col-12 col-md-8 mx-auto mt20 mt-md50 ">
                  <div class="row ">
                     <div class="col-12 col-md-10 mx-auto mt20 mt-md0">
                        <div class="table-wrap1 ">
                           <div class="table-head1 ">
                              <svg version="1.1" id="Layer_11" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 865.5 207.4" style="enable-background:new 0 0 865.5 207.4; max-height:50px" xml:space="preserve">
                                 <style type="text/css">
                                    .st00{fill:#FFFFFF;}
                                    .st11{fill:url(#SVGID_1_);}
                                    .st22{fill:url(#SVGID_00000003804682614402079440000013301449542194118298_);}
                                    .st33{fill:url(#SVGID_00000106859270421856661840000011347085370863025833_);}
                                    .st44{opacity:0.3;}
                                    .st55{fill:#0F0F0F;}
                                 </style>
                                 <g>
                                    <path class="st00" d="M482.4,27.5v105.7c0,17.9-10.1,33.4-24.9,41.3v-32.9c1-2.6,1.6-5.4,1.6-8.4s-0.6-5.8-1.6-8.4V27.5H482.4z"></path>
                                    <path class="st00" d="M457.6,92c-6.5-3.4-13.9-5.4-21.8-5.4c-25.8,0-46.6,20.9-46.6,46.6c0,25.8,20.9,46.6,46.6,46.6
                                       c7.9,0,15.3-1.9,21.8-5.4c14.8-7.8,24.9-23.4,24.9-41.3C482.4,115.3,472.4,99.8,457.6,92z M457.6,141.6c-2,5.3-5.9,9.6-10.9,12.2
                                       c-3.2,1.7-7,2.7-10.9,2.7c-12.9,0-23.3-10.4-23.3-23.3s10.4-23.3,23.3-23.3c3.9,0,7.6,1,10.9,2.7c5,2.6,8.8,7,10.9,12.2
                                       c1,2.6,1.6,5.4,1.6,8.4C459.1,136.2,458.6,139,457.6,141.6z"></path>
                                    <path class="st00" d="M498,27.5v105.7c0,17.9,10.1,33.4,24.9,41.3v-32.9c-1-2.6-1.6-5.4-1.6-8.4s0.6-5.8,1.6-8.4V27.5H498z"></path>
                                    <path class="st00" d="M498,133.2c0,17.9,10.1,33.4,24.9,41.3c6.5,3.4,13.9,5.4,21.8,5.4c25.8,0,46.6-20.9,46.6-46.6
                                       c0-25.8-20.9-46.6-46.6-46.6c-7.9,0-15.3,1.9-21.8,5.4C508.1,99.8,498,115.3,498,133.2z M521.3,133.2c0-3,0.6-5.8,1.6-8.4
                                       c2-5.3,5.9-9.6,10.9-12.2c3.2-1.7,7-2.7,10.9-2.7c12.9,0,23.3,10.4,23.3,23.3s-10.4,23.3-23.3,23.3c-3.9,0-7.6-1-10.9-2.7
                                       c-5-2.6-8.8-7-10.9-12.2C521.8,139,521.3,136.2,521.3,133.2z"></path>
                                    <path class="st00" d="M597.5,133.2c0,17.9,10.1,33.4,24.9,41.3c6.5,3.4,13.9,5.4,21.8,5.4c25.8,0,46.6-20.9,46.6-46.6
                                       c0-25.8-20.9-46.6-46.6-46.6c-7.9,0-15.3,1.9-21.8,5.4C607.5,99.8,597.5,115.3,597.5,133.2z M620.8,133.2c0-3,0.6-5.8,1.6-8.4
                                       c2-5.3,5.9-9.6,10.9-12.2c3.2-1.7,7-2.7,10.9-2.7c12.9,0,23.3,10.4,23.3,23.3s-10.4,23.3-23.3,23.3c-3.9,0-7.6-1-10.9-2.7
                                       c-5-2.6-8.8-7-10.9-12.2C621.3,139,620.8,136.2,620.8,133.2z"></path>
                                    <polygon class="st00" points="721.5,179.9 690.4,179.9 756.1,86.5 787.2,86.5 	"></polygon>
                                    <polygon class="st00" points="756.1,179.9 787.2,179.9 721.5,86.5 690.4,86.5 	"></polygon>
                                    <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="291.6235" y1="980.703" x2="309.5574" y2="1019.9633" gradientTransform="matrix(1 0 0 1 0 -870)">
                                       <stop offset="0" style="stop-color:#F40C28"></stop>
                                       <stop offset="0.8911" style="stop-color:#FF2C50"></stop>
                                    </linearGradient>
                                    <polygon class="st11" points="264.4,179.9 278.5,150 308.3,86.5 339.4,86.5 295.5,179.9 292.5,179.9 	"></polygon>
                                    <polygon class="st00" points="292.5,179.9 278.5,150 248.6,86.5 217.5,86.5 261.5,179.9 264.4,179.9 	"></polygon>
                                    <rect x="351.9" y="86.6" class="st00" width="24.9" height="93.3"></rect>
                                    <rect x="351.9" y="51.8" class="st00" width="24.9" height="24.9"></rect>
                                    <g>
                                       <path class="st00" d="M810.1,175.9c-5.9-2.7-10.5-6.3-14-10.9c-3.4-4.6-5.3-9.6-5.6-15.2h23c0.4,3.5,2.1,6.4,5.1,8.6
                                          c3,2.3,6.7,3.4,11.2,3.4c4.3,0,7.8-0.9,10.2-2.6c2.4-1.7,3.7-4,3.7-6.7c0-2.9-1.5-5.1-4.5-6.6s-7.8-3.1-14.3-4.8
                                          c-6.7-1.6-12.3-3.3-16.6-5.1c-4.3-1.7-8-4.4-11.1-8c-3.1-3.6-4.7-8.4-4.7-14.5c0-5,1.4-9.6,4.3-13.7c2.9-4.1,7-7.4,12.4-9.8
                                          s11.7-3.6,19-3.6c10.8,0,19.4,2.7,25.8,8.1c6.4,5.4,10,12.6,10.6,21.8h-21.9c-0.3-3.6-1.8-6.4-4.5-8.6c-2.7-2.1-6.2-3.2-10.7-3.2
                                          c-4.1,0-7.3,0.8-9.5,2.3s-3.3,3.6-3.3,6.4c0,3,1.5,5.4,4.6,6.9c3,1.6,7.8,3.2,14.2,4.8c6.5,1.6,11.9,3.3,16.2,5.1
                                          c4.2,1.7,7.9,4.4,11,8.1c3.1,3.6,4.7,8.5,4.8,14.4c0,5.2-1.4,9.9-4.3,14c-2.9,4.1-7,7.4-12.4,9.7s-11.7,3.5-18.8,3.5
                                          C822.6,179.9,815.9,178.5,810.1,175.9z"></path>
                                    </g>
                                 </g>
                                 <g>
                                    <g>
                                       <linearGradient id="SVGID_00000090999748961388969530000010752564172096406444_" gradientUnits="userSpaceOnUse" x1="-1360.6178" y1="-1339.845" x2="-1175.7527" y2="-1283.7964" gradientTransform="matrix(0.6181 -0.7861 -0.7861 -0.6181 -176.8927 -1732.0651)">
                                          <stop offset="0.2973" style="stop-color:#F40C28"></stop>
                                          <stop offset="0.7365" style="stop-color:#FF2C50"></stop>
                                       </linearGradient>
                                       <path style="fill:url(#SVGID_00000090999748961388969530000010752564172096406444_);" d="M125.8,6.8c0.4,0.3,0.8,0.7,1.2,1
                                          c6.2,5.4,9.8,12.7,10.8,20.3c1,8.1-1.2,16.6-6.6,23.6l-75.8,96.4c0,0-18.4,20.5-8.9,35l-32-25.2c-1.1-0.9-2.2-1.8-3.2-2.8
                                          c-13.5-13.3-15.2-35-3.2-50.3l72.8-92.6C91.8-1.7,111.9-4.1,125.8,6.8z"></path>
                                       <linearGradient id="SVGID_00000108307696041867937860000005451407988123938486_" gradientUnits="userSpaceOnUse" x1="-1216.8146" y1="1899.8297" x2="-973.6699" y2="1975.1873" gradientTransform="matrix(-0.6181 0.7861 0.7861 0.6181 -2080.1633 -197.5988)">
                                          <stop offset="0.3687" style="stop-color:#F40C28"></stop>
                                          <stop offset="1" style="stop-color:#FF2C50"></stop>
                                       </linearGradient>
                                       <path style="fill:url(#SVGID_00000108307696041867937860000005451407988123938486_);" d="M67.7,199.8c-0.4-0.3-0.8-0.7-1.2-1
                                          c-6.2-5.4-9.8-12.7-10.8-20.3c-1-8.1,1.2-16.6,6.6-23.6l75.8-96.4c0,0,18.4-20.5,8.9-35l32,25.2c1.1,0.9,2.2,1.8,3.2,2.8
                                          c13.5,13.3,15.2,35,3.2,50.3l-72.8,92.6C101.7,208.3,81.6,210.7,67.7,199.8z"></path>
                                    </g>
                                    <g class="st44">
                                       <path class="st55" d="M68.8,69v78.4c0,8.7,9.8,13.7,16.8,8.6l54.6-39.2c5.9-4.2,5.9-13,0-17.3L85.6,60.4
                                          C78.6,55.3,68.8,60.4,68.8,69z"></path>
                                    </g>
                                    <g>
                                       <path class="st00" d="M66.8,66v78.4c0,8.7,9.8,13.7,16.8,8.6l54.6-39.2c5.9-4.2,5.9-13,0-17.3L83.6,57.4
                                          C76.6,52.3,66.8,57.4,66.8,66z"></path>
                                    </g>
                                 </g>
                              </svg>
                              <div class="f-22 f-md-32 w700 lh140 text-center text-uppercase mt15 white-clr ">
                                 Commercial
                              </div>
                           </div>
                           <div class=" ">
                              <ul class="table-list1 pl0 f-18 f-md-20 lh140 w400 black-clr mb0">
                                 
                              <li>Create Unlimited YouTube Shorts </li>
                              <li>Create Unlimited Whiteboard Videos from Text  </li>
                              <li>Create Video by Using Keyword Search to Find Images </li>
                              <li>Create Videos by Using Video Clips  </li>
                              <li>Easy to Use Video Editor to Customize your Videos</li>
                              <li>Add VoiceOver to any Video </li>
                              <li>Add Background Music to any Video</li>
                              <li>A.I. Based Text To Speech Creation with Ton of Human and AI Voices  </li>
                              <li>75+ Voices and 30+ Languages </li>
                              <li>Customize the Speed of VoiceOver as per your Videos Need </li>
                              <li>Render Video to different video qualities like HD, 720p, etc as per your need  </li>
                              <li>Add Your Brand Logo and/or Watermark to your Videos  </li>
                              <li>Share Videos Directly to YouTube Shorts  </li>
                              <li>24*7 Customer Support </li>
                              <li>Commercial License Included </li>
                              <li>Provide High In-Demand Video Creation Services to your Clients  </li>


                                 <li class="headline ">BONUSES WORTH $2285 FREE!!!</li>
                                 <li>Fast Action Bonus 1 - DotcomPal</li>
                                 <li>Fast Action Bonus 2 - Viral Marketing Secrets</li>
                                 <li>Fast Action Bonus 3 - Modern Video Marketing</li>
                                 <li>Fast Action Bonus 4 - Youtube Authority</li>
                              </ul>
                           </div>
                           <div class="table-btn">
                              <div class="hideme-button ">
                                 <a href="https://warriorplus.com/o2/buy/yhq9b4/g9jkvr/jq3pcg"><img src="https://warriorplus.com/o2/btn/fn200011000/yhq9b4/g9jkvr/318870" class="img-fluid mx-auto d-block"></a>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt30 ">
                  <div class="f-md-22 f-20 w400 lh140 text-center ">
                     *Commercial License Is ONLY Available With The Purchase Of Commercial Plan
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Discounted Price Section End -->
    
       <!--------To Your Awesome Section ----------->
       <div class="awesome-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-18 f-md-18 w400 lh140 black-clr text-xs-center">
                     <span class="w800">You can agree that the price we're asking is extremely low.</span> The price is rising with every few hours, so it won't be long until it's more than double what it is today!<br><br> We could easily charge hundreds of dollars for a revolutionary tool like this, but we want to offer you an attractive and affordable price that will finally help you build super engaging video channels in the best possible way - without wasting tons of money!
                     <br><br>  So, take action now... and I promise you won't be disappointed!
                  </div>
               </div>
              
               <div class="col-12 w700 f-md-28 f-22 text-start mt20 mt-md70">To your success</div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
                  <div class="row">
                     <div class="col-md-6 col-12 text-center">
                        <img src="https://cdn.oppyo.com/launches/vidboxs/fe/ayush-sir.webp" class="img-fluid d-block mx-auto " alt="Ayush Jain">
                        <div class="f-24 f-md-28 w800 lh140 black-clr mt20">
                           Ayush Jain
                        </div>
                        <div class="f-16 lh140 w700 black-clr mt10">
                           (Entrepreneur &amp; Internet Marketer)
                        </div>
                     </div>
                     <div class="col-md-6 col-12 text-center mt20 mt-md0">
                        <img src="https://cdn.oppyo.com/launches/vidboxs/fe/pranshu-sir.webp" class="img-fluid d-block mx-auto " alt="Pranshu Gupta">
                        <div class="f-24 f-md-28 w800 lh140 text-center black-clr mt20">
                           Pranshu Gupta
                        </div>
                        <div class="f-16 lh140 w700 text-center black-clr mt10">
                           (Internet Marketer &amp; Product Creator)
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50">
                  <div class="f-18 f-md-18 w400 lh140 black-clr">
                     <span class="w800">P.S- You can try "VIDBOXS" for 30 days without any worries.</span><br><br> Listen, we know there are a lot of crappy software tools out there that will get you nowhere. Most of the software is overpriced and an absolute waste of money. So, if you're a bit sceptical, that's perfectly fine. I'm so sure you'll see the potential of this ground-breaking software that I'll let you try it out 100% risk-free.

                     <br><br> Just test it for 30 days and if you're not able to build jaw-dropping video channels packed with video content and we cannot help you in any way, you will be eligible for a refund - no tricks, no hassles.
                     <br><br>
                     <span class="w800">P.S.S Don't Procrastinate - Take Action NOW! Get your copy of VIDBOXS!  
                     </span><br><br> By now you should be really excited about all the wonderful benefits of such an amazing piece of software. You don't want to miss out on the wonderful opportunity presented today... And then regret later when it costs more than double... or it's even completely off the market!
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--------To Your Awesome Section End----------->
      <!--------Faq Section------------->
      <div class="faq-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-28 f-md-48 lh140 w800 text-center black-clr">
                  Frequently Asked <span class="red-clr">Questions</span>
               </div>
               <div class="col-12 mt20 mt-md30 ">
                  <div class="row">
                     <div class="col-md-6 col-12 ">
                        <div class="faq-list ">
                           <div class="f-md-22 f-20 w700 black-clr lh140 ">
                              What exactly VidBoxs is all about?
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-left ">
                              VidBoxs is the ultimate push-button technology that creates engagin video shorts, drives FREE viral traffic & makes handsfree commissions, ad profits & sales.
                           </div>
                        </div>
                        <div class="faq-list ">
                           <div class="f-md-22 f-20 w700 black-clr lh140">
                              Is my investment risk free?
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-left ">
                              We know the worth of your money. You can be rest assured that your investment is as safe as houses. However, we would like to clearly state that we don’t offer a no questions asked money back guarantee. You must provide a genuine reason and show us proof that you did everything before asking for a refund.
                           </div>
                        </div>
                        <div class="faq-list ">
                           <div class="f-md-22 f-20 w700 black-clr lh140 ">
                              Do I have to install VidBoxs?
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-left ">
                              NO! VidBoxs is fully cloud based. Just create an account and you can get started immediately online. It is 100% web-based platform hosted on the cloud. This means you never have to download anything ever. You can access it at any time from any device that has an internet connection.
                           </div>
                        </div>
                        <div class="faq-list ">
                           <div class="f-md-22 f-20 w700 black-clr lh140">
                              Is it ‘Newbie Friendly’?
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-left ">
                              Yep, my friend, VidBoxs is 100% newbie friendly. We know that there are a lot of technical hassles that most software has, but our software is a cut above the rest, and everyone can use it with complete ease.
                           </div>
                        </div>
                        <div class="faq-list ">
                           <div class="f-md-22 f-20 w700 black-clr lh140">
                              Do you charge any monthly fees?
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-left ">
                              NOT AT ALL. There are NO monthly fees to use VidBoxs during the launch period. During this period, you pay once and never again. We always believe in providing complete value for your money.
                           </div>
                        </div>
                     </div>
                     <div class="col-md-6 col-12 ">
                        <div class="faq-list ">
                           <div class="f-md-22 f-20 w700 black-clr lh140">
                              Is VidBoxs easy to use?
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-left ">
                              I bet you it’s the easiest tool you might have seen so far. Our #1 priority during the development of the software was to make it simple and easy for everyone. There is nothing to install, just create your account and login to take a plunge into hugely profitable video marketing arena.
                           </div>
                        </div>
                        <div class="faq-list ">
                           <div class="f-md-22 f-20 w700 black-clr lh140">
                              How do I know how well my campaigns are working?
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-left ">
                              You can see real-time reports for your campaigns in your account dashboard and make changes accordingly. But there’s a small catch, you need to upgrade your purchase to use these benefits.
                           </div>
                        </div>
                        <div class="faq-list ">
                           <div class="f-md-22 f-20 w700 black-clr lh140">
                              Will I able to drive hordes of viral traffic from day one?
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-left ">
                              Well, that depends on how well you make the use of this ultimate software. We’ve created this from grounds up to make everything simple and easy and ensure that you move ahead without any hassles.
                           </div>
                        </div>
                        <div class="faq-list ">
                           <div class="f-md-22 f-20 w700 black-clr lh140">
                              Will I get any training or support?
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-left ">
                              YES. We made detailed and step-by-step training videos that show you every step of how to get setup and you can access them in the member’s area.
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50 ">
                  <div class="f-22 f-md-32 w700 black-clr px0 text-md-center lh140 ">If you have any query, simply reach to us at <a href="mailto:support@bizomart.com" class="red-clr">support@bizomart.com</a>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--------Faq Section End------------->
      <!------Final Section------->
      <div class="final-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="but-design-border">
                     <div class="f-md-45 f-28 lh140 w800 white-clr button-shape">
                        FINAL CALL
                     </div>
                  </div>
               </div>
               <div class="col-12 passport-content">
                  <div class="col-12 f-24 f-md-32 lh140 w600 text-center white-clr mt20">
                     You Still Have the Opportunity to Raise Your Game… <br> Remember, It’s Your Make-or-Break Time! 
                  </div>
               </div>
            </div>
            <!-- CTA Btn Section Start -->
            <div class="row mt20 mt-md40">
               <div class="col-12 text-center">
                  <div class="f-18 f-md-26 w800 lh140 text-center white-clr">
                     Free Commercial License + Low 1-Time Price For Launch Period Only
                  </div>
                  <div class="f-20 f-md-24 w400 lh140 text-center mt20 white-clr">
                     Use Discount Coupon <span class="orange-clr w700">"vidboxs"</span> for Instant <span class="orange-clr w700">$3 OFF</span>
                 </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center">
                        <a href="#buynow" class="cta-link-btn">Get Instant Access To VidBoxs</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                     <img src="https://cdn.oppyo.com/launches/vidboxs/fe/compaitable-with1.webp" class="img-responsive mx-xs-center md-img-right" alt="visa">
                     <div class="d-md-block d-none visible-md px-md30">
                        <img src="https://cdn.oppyo.com/launches/vidboxs/fe/v-line.webp" class="img-fluid" alt="line">
                     </div>
                     <img src="https://cdn.oppyo.com/launches/vidboxs/fe/days-gurantee1.webp" class="img-responsive mx-xs-auto mt15 mt-md0" alt="30 days">
                  </div>
                  <div class="mt20 mt-md40 d-inline-flex align-items-center">
                     <img src="https://cdn.oppyo.com/launches/vidboxs/fe/limited-time-offer.webp" class="img-fluid mx-auto d-block">
                     <div class="cta-shape f-18 f-md-22 w800">Grab Your Copy Before The Price Increases!</div>
                  </div>
               </div>
            </div>
            <!-- CTA Btn Section End -->
         </div>
      </div>
      <!------Final Section End------->
      <!------Footer Section-------->
      <div class="footer-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <svg version="1.1" id="Layer_11" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 865.5 207.4" style="enable-background:new 0 0 865.5 207.4; max-height:50px" xml:space="preserve">
                     <style type="text/css">
                        .st00{fill:#FFFFFF;}
                        .st11{fill:url(#SVGID_1_);}
                        .st22{fill:url(#SVGID_00000003804682614402079440000013301449542194118298_);}
                        .st33{fill:url(#SVGID_00000106859270421856661840000011347085370863025833_);}
                        .st44{opacity:0.3;}
                        .st55{fill:#0F0F0F;}
                     </style>
                     <g>
                        <path class="st00" d="M482.4,27.5v105.7c0,17.9-10.1,33.4-24.9,41.3v-32.9c1-2.6,1.6-5.4,1.6-8.4s-0.6-5.8-1.6-8.4V27.5H482.4z"></path>
                        <path class="st00" d="M457.6,92c-6.5-3.4-13.9-5.4-21.8-5.4c-25.8,0-46.6,20.9-46.6,46.6c0,25.8,20.9,46.6,46.6,46.6
                           c7.9,0,15.3-1.9,21.8-5.4c14.8-7.8,24.9-23.4,24.9-41.3C482.4,115.3,472.4,99.8,457.6,92z M457.6,141.6c-2,5.3-5.9,9.6-10.9,12.2
                           c-3.2,1.7-7,2.7-10.9,2.7c-12.9,0-23.3-10.4-23.3-23.3s10.4-23.3,23.3-23.3c3.9,0,7.6,1,10.9,2.7c5,2.6,8.8,7,10.9,12.2
                           c1,2.6,1.6,5.4,1.6,8.4C459.1,136.2,458.6,139,457.6,141.6z"></path>
                        <path class="st00" d="M498,27.5v105.7c0,17.9,10.1,33.4,24.9,41.3v-32.9c-1-2.6-1.6-5.4-1.6-8.4s0.6-5.8,1.6-8.4V27.5H498z"></path>
                        <path class="st00" d="M498,133.2c0,17.9,10.1,33.4,24.9,41.3c6.5,3.4,13.9,5.4,21.8,5.4c25.8,0,46.6-20.9,46.6-46.6
                           c0-25.8-20.9-46.6-46.6-46.6c-7.9,0-15.3,1.9-21.8,5.4C508.1,99.8,498,115.3,498,133.2z M521.3,133.2c0-3,0.6-5.8,1.6-8.4
                           c2-5.3,5.9-9.6,10.9-12.2c3.2-1.7,7-2.7,10.9-2.7c12.9,0,23.3,10.4,23.3,23.3s-10.4,23.3-23.3,23.3c-3.9,0-7.6-1-10.9-2.7
                           c-5-2.6-8.8-7-10.9-12.2C521.8,139,521.3,136.2,521.3,133.2z"></path>
                        <path class="st00" d="M597.5,133.2c0,17.9,10.1,33.4,24.9,41.3c6.5,3.4,13.9,5.4,21.8,5.4c25.8,0,46.6-20.9,46.6-46.6
                           c0-25.8-20.9-46.6-46.6-46.6c-7.9,0-15.3,1.9-21.8,5.4C607.5,99.8,597.5,115.3,597.5,133.2z M620.8,133.2c0-3,0.6-5.8,1.6-8.4
                           c2-5.3,5.9-9.6,10.9-12.2c3.2-1.7,7-2.7,10.9-2.7c12.9,0,23.3,10.4,23.3,23.3s-10.4,23.3-23.3,23.3c-3.9,0-7.6-1-10.9-2.7
                           c-5-2.6-8.8-7-10.9-12.2C621.3,139,620.8,136.2,620.8,133.2z"></path>
                        <polygon class="st00" points="721.5,179.9 690.4,179.9 756.1,86.5 787.2,86.5 	"></polygon>
                        <polygon class="st00" points="756.1,179.9 787.2,179.9 721.5,86.5 690.4,86.5 	"></polygon>
                        <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="291.6235" y1="980.703" x2="309.5574" y2="1019.9633" gradientTransform="matrix(1 0 0 1 0 -870)">
                           <stop offset="0" style="stop-color:#F40C28"></stop>
                           <stop offset="0.8911" style="stop-color:#FF2C50"></stop>
                        </linearGradient>
                        <polygon class="st11" points="264.4,179.9 278.5,150 308.3,86.5 339.4,86.5 295.5,179.9 292.5,179.9 	"></polygon>
                        <polygon class="st00" points="292.5,179.9 278.5,150 248.6,86.5 217.5,86.5 261.5,179.9 264.4,179.9 	"></polygon>
                        <rect x="351.9" y="86.6" class="st00" width="24.9" height="93.3"></rect>
                        <rect x="351.9" y="51.8" class="st00" width="24.9" height="24.9"></rect>
                        <g>
                           <path class="st00" d="M810.1,175.9c-5.9-2.7-10.5-6.3-14-10.9c-3.4-4.6-5.3-9.6-5.6-15.2h23c0.4,3.5,2.1,6.4,5.1,8.6
                              c3,2.3,6.7,3.4,11.2,3.4c4.3,0,7.8-0.9,10.2-2.6c2.4-1.7,3.7-4,3.7-6.7c0-2.9-1.5-5.1-4.5-6.6s-7.8-3.1-14.3-4.8
                              c-6.7-1.6-12.3-3.3-16.6-5.1c-4.3-1.7-8-4.4-11.1-8c-3.1-3.6-4.7-8.4-4.7-14.5c0-5,1.4-9.6,4.3-13.7c2.9-4.1,7-7.4,12.4-9.8
                              s11.7-3.6,19-3.6c10.8,0,19.4,2.7,25.8,8.1c6.4,5.4,10,12.6,10.6,21.8h-21.9c-0.3-3.6-1.8-6.4-4.5-8.6c-2.7-2.1-6.2-3.2-10.7-3.2
                              c-4.1,0-7.3,0.8-9.5,2.3s-3.3,3.6-3.3,6.4c0,3,1.5,5.4,4.6,6.9c3,1.6,7.8,3.2,14.2,4.8c6.5,1.6,11.9,3.3,16.2,5.1
                              c4.2,1.7,7.9,4.4,11,8.1c3.1,3.6,4.7,8.5,4.8,14.4c0,5.2-1.4,9.9-4.3,14c-2.9,4.1-7,7.4-12.4,9.7s-11.7,3.5-18.8,3.5
                              C822.6,179.9,815.9,178.5,810.1,175.9z"></path>
                        </g>
                     </g>
                     <g>
                        <g>
                           <linearGradient id="SVGID_00000090999748961388969530000010752564172096406444_" gradientUnits="userSpaceOnUse" x1="-1360.6178" y1="-1339.845" x2="-1175.7527" y2="-1283.7964" gradientTransform="matrix(0.6181 -0.7861 -0.7861 -0.6181 -176.8927 -1732.0651)">
                              <stop offset="0.2973" style="stop-color:#F40C28"></stop>
                              <stop offset="0.7365" style="stop-color:#FF2C50"></stop>
                           </linearGradient>
                           <path style="fill:url(#SVGID_00000090999748961388969530000010752564172096406444_);" d="M125.8,6.8c0.4,0.3,0.8,0.7,1.2,1
                              c6.2,5.4,9.8,12.7,10.8,20.3c1,8.1-1.2,16.6-6.6,23.6l-75.8,96.4c0,0-18.4,20.5-8.9,35l-32-25.2c-1.1-0.9-2.2-1.8-3.2-2.8
                              c-13.5-13.3-15.2-35-3.2-50.3l72.8-92.6C91.8-1.7,111.9-4.1,125.8,6.8z"></path>
                           <linearGradient id="SVGID_00000108307696041867937860000005451407988123938486_" gradientUnits="userSpaceOnUse" x1="-1216.8146" y1="1899.8297" x2="-973.6699" y2="1975.1873" gradientTransform="matrix(-0.6181 0.7861 0.7861 0.6181 -2080.1633 -197.5988)">
                              <stop offset="0.3687" style="stop-color:#F40C28"></stop>
                              <stop offset="1" style="stop-color:#FF2C50"></stop>
                           </linearGradient>
                           <path style="fill:url(#SVGID_00000108307696041867937860000005451407988123938486_);" d="M67.7,199.8c-0.4-0.3-0.8-0.7-1.2-1
                              c-6.2-5.4-9.8-12.7-10.8-20.3c-1-8.1,1.2-16.6,6.6-23.6l75.8-96.4c0,0,18.4-20.5,8.9-35l32,25.2c1.1,0.9,2.2,1.8,3.2,2.8
                              c13.5,13.3,15.2,35,3.2,50.3l-72.8,92.6C101.7,208.3,81.6,210.7,67.7,199.8z"></path>
                        </g>
                        <g class="st44">
                           <path class="st55" d="M68.8,69v78.4c0,8.7,9.8,13.7,16.8,8.6l54.6-39.2c5.9-4.2,5.9-13,0-17.3L85.6,60.4
                              C78.6,55.3,68.8,60.4,68.8,69z"></path>
                        </g>
                        <g>
                           <path class="st00" d="M66.8,66v78.4c0,8.7,9.8,13.7,16.8,8.6l54.6-39.2c5.9-4.2,5.9-13,0-17.3L83.6,57.4
                              C76.6,52.3,66.8,57.4,66.8,66z"></path>
                        </g>
                     </g>
                  </svg>
                  <div editabletype="text" class="f-16 f-md-18 w400 lh140 mt20 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-16 f-md-18 w400 lh140 white-clr text-xs-center">Copyright © VidBoxs</div>
                  <ul class="footer-ul f-16 f-md-18 w400 white-clr text-center text-md-right">
                     <li><a href="https://support.bizomart.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://www.vidboxs.com/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://www.vidboxs.com/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://www.vidboxs.com/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://www.vidboxs.com/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://www.vidboxs.com/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://www.vidboxs.com/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!------Footer Section End-------->
         <!-- Exit Popup and Timer -->
   <?php include('timer.php'); ?>
   <!-- Exit Popup and Timer End -->
   </body>
</html>
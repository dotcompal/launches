<!DOCTYPE html>
<html>
   <head>
      <title>MailZilo Advanced</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=9">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
      <meta name="title" content="MailZilo Advanced">
      <meta name="description" content="Most Advanced and Powerful Tag & Segment Based Email Software That Gets Us 4X More Opening & Click without Any Monthly Fee Ever!">
      <meta name="keywords" content="MailZilo Advanced">
      <meta property="og:image" content="https://www.mailzilo.com/advanced/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Ayush Jain">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="MailZilo Advanced">
      <meta property="og:description" content="Most Advanced and Powerful Tag & Segment Based Email Software That Gets Us 4X More Opening & Click without Any Monthly Fee Ever!">
      <meta property="og:image" content="https://www.mailzilo.com/advanced/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="MailZilo Advanced">
      <meta property="twitter:description" content="Most Advanced and Powerful Tag & Segment Based Email Software That Gets Us 4X More Opening & Click without Any Monthly Fee Ever!">
      <meta property="twitter:image" content="https://www.mailzilo.com/advanced/thumbnail.png">
      <link rel="icon" href="https://cdn.oppyo.com/launches/mailzilo/common_assets/images/favicon.png" type="image/png">
      <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
      <link rel="stylesheet" href="https://cdn.oppyo.com/launches/webpull/common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style.css" type="text/css" />
      <link rel="stylesheet" href="assets/css/timer.css" type="text/css" />
      <script src="https://cdn.oppyo.com/launches/webpull/common_assets/js/jquery.min.js"></script>   
      <script>
      (function(w, i, d, g, e, t, s) {
            if(window.businessDomain != undefined){
               console.log("Your page have duplicate embed code. Please check it.");
               return false;
            }
            businessDomain = 'mailzilo';
            allowedDomain = 'mailzilo.com';
            if(!window.location.hostname.includes(allowedDomain)){
               console.log("Your page have not authorized. Please check it.");
               return false;
            }
            console.log("Your script is ready...");
            w[d] = w[d] || [];
            t = i.createElement(g);
            t.async = 1;
            t.src = e;
            s = i.getElementsByTagName(g)[0];
            s.parentNode.insertBefore(t, s);
      })(window, document, '_gscq', 'script', 'https://cdn.staticdcp.com/apps/engage/smart_engage/js/config-loader.js');
      </script>
   </head>
   <body>
      <!-- Header Section Start -->
      <div class="header-section">
         <div class="container">
            <div class="row">
               <div class="col-md-3 text-md-left text-center">
                  <svg version="1.1" x="0px" y="0px" viewBox="0 0 1501.16 276.9" style="enable-background:new 0 0 1501.16 276.9; max-height:40px;" xml:space="preserve">
                     <style type="text/css">
                        .st0{fill:#FFFFFF;}
                     </style>
                     <g>
                        <path class="st0" d="M122.38,159.77l82.88-67.64h39.5v182.43h-45.44V160.68l-73.53,57.2h-6.82l-73.52-57.2v113.87H0V92.13h39.46
                           L122.38,159.77z"></path>
                        <path class="st0" d="M391.87,92.13l81.03,182.43h-48.87L409,238.51h-81.7l-13.62,36.04h-48.16l73.69-182.43H391.87z M395.71,206.65
                           l-29.57-70.91l-26.8,70.91H395.71z"></path>
                        <path class="st0" d="M540.93,92.13v182.43h-45.44V92.13H540.93z"></path>
                        <path class="st0" d="M722.06,241.12v33.43H582.98V92.13h45.44v149H722.06z"></path>
                        <path class="st0" d="M913.75,92.13l-99.48,149h99.48v33.43H736.68l100.26-149H743.6V92.13H913.75z"></path>
                        <path class="st0" d="M989.75,92.13v182.43h-45.44V92.13H989.75z"></path>
                        <path class="st0" d="M1170.88,241.12v33.43H1031.8V92.13h45.44v149H1170.88z"></path>
                        <path class="st0" d="M1332.43,209.97c-2.33,5.08-5.42,9.64-9.28,13.71c-9.7,10.23-22.48,15.35-38.35,15.35
                           c-15.94,0-28.8-5.12-38.53-15.35c-9.74-10.22-14.61-23.66-14.61-40.28c0-16.72,4.87-30.17,14.61-40.35
                           c5.49-5.75,11.97-9.87,19.44-12.37l-26.14-31.06c-9.05,4.52-17.19,10.6-24.43,18.22c-17.75,18.72-26.63,40.57-26.63,65.56
                           c0,25.41,8.94,47.36,26.81,65.81c17.87,18.45,41.03,27.68,69.49,27.68c28.13,0,51.18-9.26,69.13-27.81
                           c2.07-2.15,4.04-4.34,5.87-6.58L1332.43,209.97z"></path>
                        <path class="st0" d="M1501.16,0l-114.63,241.95l-71.11-84.51l-31.99,29.36l2.17-64.79c67.33-7.59,111.49-65.77,112.17-66.67
                           c-41.92,31-125.97,49.4-125.97,49.4l-28.6-33.1L1501.16,0z"></path>
                     </g>
                  </svg>
               </div>
               <div class="col-md-7 mt20 mt-md0">
                  <ul class="leader-ul f-16 f-md-18 w500 white-clr text-md-end text-center">
                     <li><a href="#features" class="white-clr">Features</a></li>
                     <li><a href="#demo" class="white-clr">Demo</a></li>
                     <li class="d-md-none affiliate-link-btn"><a href="https://warriorplus.com/o2/buy/xgwy6m/y4pjnq/nh90r1" class="white-clr">Buy Now</a></li>
                  </ul>
               </div>
               <div class="col-md-2 affiliate-link-btn text-md-end text-center mt15 mt-md0 d-none d-md-block f-18">
                  <a href="https://warriorplus.com/o2/buy/xgwy6m/y4pjnq/nh90r1">Buy Now</a>
               </div>
               <div class="col-12 text-center mt20 mt-md50">
                  <div class="pre-heading f-md-22 f-16 w500 lh140 white-clr">
                  Are you Sick & Tired of Paying 100s of Dollars To Old School Autoresponders That Never Deliver?
                  </div>
               </div>
               <div class="col-12 mt-md30 mt20 f-md-45 f-28 w600 text-center white-clr lh140">
               Get 4X Email Opens & Traffic Using <span class="yellow-clr">the Powerful Tagging Based Email Marketing Platform & 90,000+ Done-For-You Proven Swipes</span> with <br class="d-none d-md-block">No Monthly Fee Ever!
               </div>
               <div class="col-12 mt-md30 mt20 text-center">
                  <div class="post-heading f-16 f-md-24 w500 text-center lh140 white-clr text-capitalize">
                  100% Newbie Friendly Platform to Collect & Manage Leads | Send Unlimited Emails <br class="d-none d-md-block">to Get Tons of Traffic, Affiliate Commissions, & Sales on Autopilot
                  </div>
               </div>
               <div class="col-12 mt20 mt-md40">
                  <div class="row">
                     <div class="col-md-7 col-12 min-md-video-width-left">
                        <!-- <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/product-box.webp" class="img-fluid d-block mx-auto"> -->
                        <div class="col-12 responsive-video">
                           <iframe src="https://mailzilo.dotcompal.com/video/embed/caepa779qt" style="width:100%; height:100%" frameborder="0" allow="fullscreen" allowfullscreen></iframe>
                        </div>
                        <div class="col-12 mt20 mt-md40 d-none d-md-block">
                           <div class="f-22 f-md-24 w500 lh140 text-center white-clr">
                              Get Free 30 Reseller License + Commercial License <br class="d-none d-md-block"> If You Buy Today!! 
                           </div>
                           <div class="row">
                              <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center ">
                                 <a href="https://warriorplus.com/o2/buy/xgwy6m/y4pjnq/nh90r1" class="cta-link-btn px0 d-block">Get Instant Access To MailZilo</a>
                              </div>
                           </div>
                           <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30 ">
                              <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/compaitable-with1.webp " class="img-fluid d-block mx-xs-center md-img-right ">
                              <div class="d-md-block d-none px-md30 "><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/v-line.webp " class="img-fluid "></div>
                              <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/days-gurantee1.webp " class="img-fluid d-block mx-xs-auto mt15 mt-md0 ">
                           </div>
                        </div>
                     </div>
                     <div class="col-md-5 col-12 mt20 mt-md0 min-md-video-width-right">
                        <div class="key-features-bg">
                           <ul class="list-head pl0 m0 f-18 lh150 w400 white-clr">
                              <li><span class="list-highlight">Send Unlimited Emails</span> to Unlimited Subscribers</li>
                              <li><span class="list-highlight">Collect Unlimited Leads</span> with Built-In Lead Form</li>
                              <li><span class="list-highlight">Free SMTP</span> for unrestricted Mailing</li>
                              <li><span class="list-highlight">Upload Unlimited List</span> with Zero Restrictions</li>
                              <li><span class="list-highlight">Get 4X More ROI</span> and Profits than Ever</li>
                              <li><span class="list-highlight">Smart Tagging</span> for Lead Personalisation</li>
                              <li><span class="list-highlight">100% Control</span> on your online business </li>
                              <li><span class="list-highlight">Built-in Inline</span> Editor to Craft Beautiful Emails</li>
                              <li><span class="list-highlight">Hassle-Free</span> Subscribers Management</li>
                              <li><span class="list-highlight">100+ High Converting</span> Templates for Webforms & Email </li>
                              <li><span class="list-highlight">GDPR & Can-Spam Compliant</span></li>
                              <li><span class="list-highlight">100% Newbie Friendly</span> & Easy to Use</li>
                              <li><span class="list-highlight">Commercial License Included</span></li>
                           </ul>
                        </div>
                        <div class="col-12 mt20 mt-md60 d-md-none">
                           <div class="f-20 f-md-24 w500 lh140 text-center white-clr">
                              Get Free 30 Reseller License + Commercial License <br class="d-none d-md-block"> If You Buy Today!! 
                           </div>
                           <div class="row">
                              <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center ">
                                 <a href="https://warriorplus.com/o2/buy/xgwy6m/y4pjnq/nh90r1" class="cta-link-btn px0 d-block">Get Instant Access To MailZilo</a>
                              </div>
                           </div>
                           <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30 ">
                              <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/compaitable-with1.webp " class="img-fluid d-block mx-xs-center md-img-right ">
                              <div class="d-md-block d-none px-md30 "><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/v-line.webp " class="img-fluid "></div>
                              <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/days-gurantee1.webp " class="img-fluid d-block mx-xs-auto mt15 mt-md0 ">
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="step-section">
         <div class="container ">
            <div class="row ">
               <div class="col-12 f-28 f-md-45 w600 lh140 black-clr text-center">
                  Replace your old-school email marketing <br class="d-none d-md-block"> techniques with this cutting-edge solution that<br class="d-none d-md-block"> powers your business <span class="orange-clr d-inline-grid">in just 3 easy steps… <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/underline.webp " class="img-fluid"></span>
               </div>
               <div class="col-12 mt30 mt-md80">
                  <div class="row align-items-center">
                     <div class="col-12 col-md-6 relative">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/step1.webp" class="img-fluid">
                        <div class="f-28 f-md-40 w600 lh140 mt15">
                           Upload
                        </div>
                        <div class="f-18 f-md-20 w400 lh140 mt15">
                           To begin with, all you need to do is upload your subscribers 
                           or generate leads using MailZilo opt-in forms.   
                        </div>
                        <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/left-arrow.webp" class="img-fluid d-none d-md-block ms-auto step-arrow1">
                     </div>
                     <div class="col-12 col-md-6 mt20 mt-md0 ">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/step-one.webp" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
               <div class="col-12 mt30 mt-md80">
                  <div class="row align-items-center ">
                     <div class="col-12 col-md-6 order-md-2 relative">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/step2.webp" class="img-fluid">
                        <div class="f-28 f-md-40 w600 lh140 mt15">
                           Select or Create
                        </div>
                        <div class="f-18 f-md-20 w400 lh140 mt15">
                           Select from the list of beautifully designed email templates 
                           or create your own email template.   
                        </div>
                        <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/right-arrow.webp" class="img-fluid d-none d-md-block step-arrow2">
                     </div>
                     <div class="col-md-6 col-md-pull-6 mt20 mt-md0 order-md-1 ">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/step-two.webp " class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
               <div class="col-12 mt30 mt-md80">
                  <div class="row align-items-center">
                     <div class="col-12 col-md-6">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/step3.webp" class="img-fluid">
                        <div class="f-28 f-md-40 w600 lh140 mt15">
                           Send & Profit
                        </div>
                        <div class="f-18 f-md-20 w400 lh140 mt15">
                           Send Unlimited Email to your Subscriber or Schedule it for later.
                        </div>
                     </div>
                     <div class="col-12 col-md-6 mt20 mt-md0 ">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/step-three.webp " class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md60 text-center">
                  <div class="d-flex gap-2 gap-md-4 justify-content-center flex-wrap">
                     <div class="d-flex align-items-center gap-3 justify-content-center">
                        <img src="https://cdn.oppyo.com/launches/writerarc/bizdrive/cross.webp" alt="Cross" class="img-fluid d-block">
                        <div class="f-md-28 w700 lh140 f-20 red-clr"> No SMTP</div>
                     </div>
                     <div class="d-flex align-items-center gap-3 justify-content-center">
                        <img src="https://cdn.oppyo.com/launches/writerarc/bizdrive/cross.webp" alt="Cross" class="img-fluid d-block">
                        <div class="f-md-28 w700 lh140 f-20 red-clr"> No Installation</div>
                     </div>
                     <div class="d-flex align-items-center gap-3 justify-content-center">
                        <img src="https://cdn.oppyo.com/launches/writerarc/bizdrive/cross.webp" alt="Cross" class="img-fluid d-block">
                        <div class="f-md-28 w700 lh140 f-20 red-clr">No Other Techie Stuff</div>
                     </div>
                  </div>
                  <div class="f-20 f-md-28 w500 lh140 mt20">It’s EASY, FAST & Built with LOVE. </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Proof Section -->
      <div class="proof-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 ">
                  <div class="f-28 f-md-45 w600 lh140 black-clr text-center">
                  Here is What We’ve Made in the Past 3 Months <br class="d-none d-md-block"> 
                  by Using the Exact Same Technology
                  </div>
               </div>
               <div class="col-12 mx-auto mt20 mt-md50">
                  <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/proof.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>
            <div class="row mt-md70 mt30 align-items-center">
               <div class="col-md-6 f-md-32 f-22 w600 text-center text-md-start">
                  Got 20K+ Visitors in Just 15 Days on My Affiliate Offers Using the Power of Emails  
               </div>
               <div class="col-md-6 mt-md0 mt20">
                  <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/proof1.webp" alt="" class="img-fluid d-block mx-auto">
               </div>
            </div>
            <div class="row mt-md70 mt30 align-items-center">
               <div class="col-md-6 f-md-32 f-22 w600 order-md-2 text-center text-md-start">
                  And Let’s Check Out the Crazy Open And Click Rates 
                  We’ve Got for A Simple Email I Sent Using Mailzilo.
               </div>
               <div class="col-md-6 mt-md0 mt20 order-md-1">
                  <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/proof2.webp" alt="" class="img-fluid d-block mx-auto">
               </div>
            </div>
         </div>
      </div>
      <!-- Proof Section End -->  
      <!-- Testimonial Section Start -->
      <div class="testimonial-section">
         <div class="container ">
            <div class="row ">
               <div class="col-12 f-28 f-md-45 w600 lh140 black-clr text-center">
                  Here's What Industry Experts Have to <br class="d-none d-md-block"> Say About MailZilo
               </div>
            </div>
            <div class="row row-cols-md-2 row-cols-1 gx4">
               <div class="col mt50">
                  <div class="single-testimonial">
                     <p>
                     Finally, I am getting my emails delivered, clicked, and opened. Yes,<span class="w600"> MailZilo is one of the BEST tools</span> I’ve used for my business yet! No more dependency on other email software. Stupidly simple to use. MailZilo made me go crazy. <span class="w600">Five Stars from my side for this product… </span>
                     </p>
                     <div class="client-info">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/amit-gaikwad.webp" class="img-fluid d-block mx-auto">
                        <div class="client-details">
                        Amit Gaikwad
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col mt50">
                  <div class="single-testimonial">
                     <p>
                     BINGO, I've tested MailZilo for myself, and man, <span class="w600">it delivered exactly as promised.</span> I just recently sent mail to my entire list and <span class="w600">got 20% more open rates</span> with this super-amazing Email Marketing Technology. <span class="w600">Two-Thumbs up for this one…</span> 
                     </p>
                     <div class="client-info">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/anirudh-bharwa.webp" class="img-fluid d-block mx-auto">
                        <div class="client-details">
                        Anirudh Bharwa
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col mt50">
                  <div class="single-testimonial">
                     <p>
                     MailZilo will surely take the industry by storm and will <span class="w600">help marketers to generate more leads</span> from any Blog, eCommerce, or WordPress site. I like its <span class="w600">latest smart tag feature</span> which enables me to build targeted marketing campaigns. Yet another killer product from Team Ayush & Pranshu… 
                     </p>
                     <div class="client-info">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/akshat-gupta.webp" class="img-fluid d-block mx-auto">
                        <div class="client-details">
                        Akshat Gupta
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col mt50">
                  <div class="single-testimonial">
                     <p>
                     <span class="w600">Importing Lists without losing any Leads? DONE!<br>
                        Sending UNLIMITED Emails? DONE!<br>
                        Want to Generate More Leads? DONE!<br>
                        Automating Email Marketing Campaigns? That’s DONE as well!<br><br></span>
                        This software really impressed me with its amazing features. A great option for anyone looking to make the most from email marketing…. 

                     </p>
                     <div class="client-info">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/uddhab-pramanik.webp" class="img-fluid d-block mx-auto">
                        <div class="client-details">
                        Uddhab Pramanik
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col mt50">
                  <div class="single-testimonial">
                     <p>
                     <span class="w600">No more dependency on 3rd party money-sucking service providers.</span> No more worrying about paying a fortune to get minuscule results. Now, I can have complete control of my email marketing campaigns. The good days are back folks. Ayush & Pranshu, take a bow.
                     </p>
                     <div class="client-info">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/art-flair.webp" class="img-fluid d-block mx-auto">
                        <div class="client-details">
                        Art Flair
                        </div>
                     </div>
                  </div>
               </div>
               <!-- <div class="col mt50">
                  <div class="single-testimonial">
                     <p>
                     Bingo guys!!! <span class="w600">This is another wonderful product from your team.</span> Now you too can build strong relationships with your subscribers with well-crafted emails. I am just loving using this gorgeously designed piece of technology. 

                     </p>
                     <div class="client-info">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/uddhab-pramanik.webp" class="img-fluid d-block mx-auto">
                        <div class="client-details">
                        Uddhab Pramanik
                        </div>
                     </div>
                  </div>
               </div> -->
            </div>
         </div>
      </div>
      <!-- Testimonial Section End -->
      <!-- Cta Section -->
      <div class="cta-section-new">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/limited-time-offer-banner.webp" class="img-fluid d-block mx-auto">
                  <div class="f-22 f-md-28 w700 lh140 text-center white-clr mt20">
                     Grab MailZilo Now at 80% Launch Special Discount!
                  </div>
                  <div class="f-18 f-md-22 w500 lh140 text-center yellow-clr mt10">
                     (Get Free 30 Reseller License + Commercial License + Special Bonuses)
                  </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 text-center">
                        <a href="https://warriorplus.com/o2/buy/xgwy6m/y4pjnq/nh90r1" class="cta-link-btn">Get Instant Access To MailZilo</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20">
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/compaitable-with1.webp" class="img-fluid d-block mx-xs-center md-img-right">
                     <div class="d-md-block d-none px-md30"><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/v-line.webp" class="img-fluid"></div>
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/days-gurantee1.webp" class="img-fluid d-block mx-xs-auto mt15 mt-md0">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Cta Section End -->
      <!--  Email Marketing Section Start -->
      <div class="research-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="reason-box">
                     <div class="f-24 f-md-38 w600 lh140 blue-clr">
                        Email Marketing Yields an
                     </div>
                     <div class="f-28 f-md-45 w700 lh140 black-clr">
                        Average 4300% Return on Investment
                     </div>
                     <div class="f-24 f-md-38 w600 lh140 blue-clr">
                        for businesses worldwide.
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50">
                  <div class="row align-items-center">
                     <div class="col-12 col-md-6">
                        <div class="f-20 f-md-24 w500 lh140 text-center text-md-start">
                           Yes, Emails are the most powerful, profitable, and low-cost marketing tool and get 3 times higher conversions than social media or any other mode of marketing.
                        </div>
                     </div>
                     <div class="col-12 col-md-6 mt20 mt-md0">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/investment.webp" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md70">
                  <div class="row">
                     <div class="col-12 col-md-7 order-md-2">
                        <div class="f-28 f-md-45 w700 lh140 text-center text-md-start green-clr">
                           That’s Right…
                        </div>
                        <div class="f-20 f-md-24 w500 lh140 text-center text-md-start mt10">
                           Email marketing is the key to getting new customers and creating deeper relationships with your existing customers at a fraction of the cost.
                        </div>
                     </div>
                     <div class="col-12 col-md-5 order-md-1 mt20 mt-md0 relative d-none d-md-block">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/girl.webp" class="img-fluid d-block mx-auto girl-img">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Email Marketing Section End -->
      <div class="stats-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-45 w700 lh140 text-center white-clr stats-headline">Here are Some Eye Openers </div>
               </div>
            </div>
            <div class="row row-cols-1 row-cols-md-2 gap30 mt0 t-md20">
               <div class="col">
                  <div class="stats-block">
                     <div><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/mail.webp" class="img-fluid d-block mx-auto"></div>
                     <div class="f-20 f-md-24 w400 lh140 white-clr">There are <span class="orange-clr w700">4.5+ Billion</span> Daily Email Users</div>
                  </div>
               </div>
               <div class="col">
                  <div class="stats-block">
                     <div><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/mail.webp" class="img-fluid d-block mx-auto"></div>
                     <div class="f-20 f-md-24 w400 lh140 white-clr"><span class="orange-clr w700">350+ Billion Emails</span> are Circulated
                        Every Single Day
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="stats-block">
                     <div><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/mail.webp" class="img-fluid d-block mx-auto"></div>
                     <div class="f-20 f-md-24 w400 lh140 white-clr"><span class="orange-clr w700">$400 Million</span> are Spent Every Year
                        on Email Marketing in US Alone
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="stats-block">
                     <div><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/mail.webp" class="img-fluid d-block mx-auto"></div>
                     <div class="f-20 f-md-24 w400 lh140 white-clr"><span class="orange-clr w700">95% of Email</span> Users Check
                        Their Emails Daily
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="stats-block">
                     <div><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/mail.webp" class="img-fluid d-block mx-auto"></div>
                     <div class="f-20 f-md-24 w400 lh140 white-clr"><span class="orange-clr w700">85% of Businesses</span> Believe that
                        Email Marketing Improves
                        Customer Retention
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="stats-block">
                     <div><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/mail.webp" class="img-fluid d-block mx-auto"></div>
                     <div class="f-20 f-md-24 w400 lh140 white-clr"><span class="orange-clr w700">80% of Youth</span> Prefer Business
                        Communications via Email
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="stats-block">
                     <div><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/mail.webp" class="img-fluid d-block mx-auto"></div>
                     <div class="f-20 f-md-24 w400 lh140 white-clr"><span class="orange-clr w700">90% of Marketers</span> Use Email
                        as the Primary Channel
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="stats-block">
                     <div><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/mail.webp" class="img-fluid d-block mx-auto"></div>
                     <div class="f-20 f-md-24 w400 lh140 white-clr"><span class="orange-clr w700">Since 10+ Years,</span> Email Marketing
                        Generates the Highest ROI for
                        Marketers.
                     </div>
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md80">
               <div class="col-12 f-24 f-md-36 w600 white-clr lh140 text-center">This Show Why Big Businesses <br class="d-none d-md-block">
                  & Marketers are Capitalizing on Email Marketing
               </div>
            </div>
         </div>
      </div>
      <div class="power-section">
         <div class="container ">
            <div class="row align-items-center">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-45 w700 lh140 black-clr">
                     And Do You Know How Much People Are Making  <br>
                     <span class="orange-clr">Using the POWER of Emails Marketing?</span>
                  </div>
                  <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/power-line.webp" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 col-md-11 offset-md-1 mt20 mt-md50">
                  <div class="power-block">
                     <div class="f-28 f-md-36 w700 orange-clr lh140 text-center text-md-start">Tom Sather</div>
                     <div class="f-md-20 f-18 w400 lh140 white-clr mt10 text-center text-md-start">
                        He is known for his work as the Director of Return Path which has an Annual Revenue of $10+ Million. Return Path, with emphasis on Emails Marketing, offers unparalleled insights on why emails end up undeliverable and where they end up bouncing.
                     </div>
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/tom.webp" class="img-fluid d-block mx-auto left-img mt20 mt-md0">
                  </div>
               </div>
               <div class="col-12">
                  <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/connect.webp" class="img-fluid d-block mx-auto connect-line">
               </div>
               <div class="col-12 col-md-11 mt20">
                  <div class="power-block right">
                     <div class="f-28 f-md-36 w700 orange-clr lh140 text-center text-md-start">Matthew Lloyd Smith</div>
                     <div class="f-md-20 f-18 w400 lh140 white-clr mt10 text-center text-md-start">Matthew Lloyd Smith created the insanely helpful website Really Good Emails, which showcases the best in email marketing on the internet. Really Good Emails has a Revenue of over $5 Million.
                     </div>
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/matthew.webp" class="img-fluid d-block mx-auto right-img mt20 mt-md0">
                  </div>
               </div>
               <div class="col-12">
                  <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/connect1.webp" class="img-fluid d-block mx-auto connect-line">
               </div>
               <div class="col-12 col-md-11 offset-md-1 mt20">
                  <div class="power-block">
                     <div class="f-28 f-md-36 w700 orange-clr lh140 text-center text-md-start">Kath Pay</div>
                     <div class="f-md-20 f-18 w400 lh140 white-clr mt10 text-center text-md-start">
                        Kath Pay has been in the email game for over 19 years. She is an email marketing consultant and founder of Holistic Email Marketing. She’s been named one of the top email influencers in the world and makes annual revenue of over $8 Million.
                     </div>
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/kath.webp" class="img-fluid d-block mx-auto left-img mt20 mt-md0">
                  </div>
               </div>
               <div class="col-12 mt20 mt-md100">
                  <div class="f-20 f-md-32 w400 lh140 text-center">And all of that just by using the Power of Email Marketing</div>
                  <div class="f-32 f-md-55 w700 lh140 text-center green-clr my20">Impressive, Right?</div>
                  <div class="f-20 f-md-32 w400 lh140 text-center">So, it can be safely stated that…<br>
                     <span class="w600">Email Marketing is the BIGGEST Income Opportunity Right Now!</span>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Hi Section End -->
      <div class="hithere-section">
         <div class="container ">
            <div class="row align-items-center">
               <div class="col-12">
                  <div class="f-28 f-md-45 w600 lh140 white-clr">
                     Dear Struggling Marketer,
                  </div>
                  <div class="f-26 f-md-40 w400 lh140 white-clr mt10">
                     I’m Ayush Jain along with my friend Pranshu Gupta.
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Hi Section End -->
      <div class="hithere-section1">
         <div class="container ">
            <div class="row align-items-center">
               <div class="col-12 col-md-7">
                  <div class="f-md-22 f-20 w400 lh160 black-clr">
                     We are happily living the internet lifestyle for the last 10 years with full flexibility of working from anywhere at any time.
                     Over time, we’ve learned that to be successful online, you must get your hands on a foolproof system that works again and again and again.<br><br>
                     <span class="w600">And without a doubt, that’s EMAIL MARKETING.</span><br><br>
                     Till now, you might already have discovered that Email Marketing is an untapped goldmine to tap upon and boost leads, sales & profits for your offers.
                  </div>
               </div>
               <div class="col-12 col-md-5 mt20 mt-md0">
                  <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/ayush-pranshu.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>
         </div>
      </div>
      <div class="big-question-bg">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-7">
                  <div class="f-28 f-md-45 w700 white-clr big-headline">
                     But Here’s a Big Question…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 white-clr mt20 text-center text-md-start">
                     Even after the fact that Email Marketing is the need of the hour and is the 
                     a most efficient form of marketing. It is surprising to see that only 46% of 
                     small businesses use email marketing to reach customers. 
                     <br><br>
                     <span class="w600 f-24 f-md-32">Why?</span>
                  </div>
               </div>
               <div class="col-md-5 mt-md0 mt20">
                  <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/bigquestion-shape.webp" alt="" class="img-fluid d-block mx-auto">
               </div>
            </div>
         </div>
      </div>
      <div class="problem-bg-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-50 w600 lh140 text-center red-clr1">
                     Because The Problem Is
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 text-center white-clr mt20">
                     Even after the fact that Email Marketing is the need of the hour and is the a most efficient form of marketing. It is surprising to see that only 46% of small businesses use email marketing to reach customers.
                  </div>
                  <div class="f-20 f-md-22 w600 lh140 text-center white-clr mt20 problem-area-shape mt-md30">
                     So, If You are Planning for a 3rd Party Email Service, THINK AGAIN!! 
                  </div>
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md50">
               <div class="col-12 col-md-7">
                  <ul class="problem f-18 w400 lh160 white-clr">
                     <li>They will have access to your valuable list, so data leak is also a BIG risk. </li>
                     <li>It’s almost impossible to import your subscriber list and if you succeed, 
                        you still may lose 20-30% of your leads. 
                     </li>
                     <li>You’re stuck with many other customers of theirs who share the same 
                        resources, Constant downtimes, spam complaints, bounces, and 
                        unexplained delays. It happens even when you don’t do anything 
                        wrong.  It's due to someone else’s mistake. 
                     </li>
                     <li>That’s not all. They can shut down your account anytime without 
                        any prior notice & you can lose your entire business overnight.  
                     </li>
                     <li>They charge you anywhere from $100-150 PER MONTH for just 
                        10,000 subscribers and you can only send limited emails to those 
                        limited subscribers for that price. 
                     </li>
                     <li>They will have access to your valuable list, so data leak is also a BIG risk.</li>
                  </ul>
               </div>
               <div class="col-12 col-md-5 mt20 mt-md0">
                  <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/problem.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>
            <div class="row mt20 mt-md50">
               <div class="col-12">
                  <div class="f-20 f-md-30 w600 text-center white-clr">Even after that, there is still no guarantee that you’ll get the desired ROI from your campaigns as you expected.</div>
               </div>
            </div>
         </div>
      </div>
      <div class="problem-bg-section1">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-12 col-md-7 order-md-2">
                  <div class="f-24 f-md-38 w600 white-clr lh140">Let me tell you my story: </div>
                  <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/let-line.webp" class="img-fluid">
                  <div class="f-18 w400 lh160 mt20 white-clr">
                     3 years back, I was searching for a better solution for my email marketing.  
                     So, after doing my research and numerous brainstorming sessions,  
                     I decided to choose MailChimp for sending emails for boosting sales and profits. 
                     <br><br>
                     Things were going quite smooth, my emails were getting opened and click-through  
                     rates were also decent enough. So, I felt like a king, and my dreams clouds started  
                     showering rains of overnight fame and success.
                  </div>
               </div>
               <div class="col-12 col-md-5 mt20 mt-md0 order-md-1">
                  <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/problem1.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>
            <div class="row mt20 mt-md50">
               <div class="col-12">
                  <div class="f-20 f-md-28 w500 text-center white-clr">But destiny had something else in the store, and one morning while sipping my daily cappuccino, my eyes were split wide open to read this horrendous email- </div>
               </div>
            </div>
         </div>
      </div>
      <div class="problem-bg-section2">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-12">
                  <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/suspended.webp" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 f-18 f-md-20 w400 lh160 mt20 mt-md50 white-clr mb20 mb-md50">
                  These were literally the first words that echoed in my mind after I checked that mail. After all, I didn’t do anything wrong or unethical. I wasn’t spamming, nor was I sending fishy emails to my subscribers. <br><br>
                  I felt completely done and dusted. I couldn’t do anything as I had ZERO control and had to rely on a 3rd party for sending mails and earning my daily bread. 
               </div>
               <div class="col-12 col-md-5 order-md-2">
                  <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/problem2.webp" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 col-md-7 order-md-1 mt20 mt-md0">
                  <div class="f-18 w400 lh160 white-clr">
                     I gathered some courage and got in touch with their support team. And you’ll be
                     amazed to know that I couldn’t get a justifiable and believable reply from their 
                     end, and ignored any contact attempts that I made after that. They kept playing 
                     with me like a football, kicking me mercilessly from one corner to another.
                     <br><br>
                     I am still amazed why it happened. It sounds as difficult as a Greek puzzle.
                     <br><br>
                     My business was dying a slow, painful death, and I wasn't giving my 
                     subscribers what they needed... they were not getting due worth for 
                     their faith in me.
                     <br><br>
                     If you’ve already gone through all this, there’s no doubt you’ve experienced 
                     frustration and the loss of profits. And so, all that came to my mind is that 
                     I had to prepare for a hard and bumpy ride.
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="problem-bg-section3">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-12 text-center">
                  <div class="f-24 f-md-38 w600 black-clr shape-headline lh140">
                     I NEED FULLControl Over My Online Business
                  </div>
               </div>
               <div class="col-12 f-18 f-md-20  w400 lh160 mt20 mt-md50 white-clr">
                  That’s it, I told myself. I decided from now on, I won’t put my faith in these money-sucking service providers that are sitting with a big crocodile’s mouth open to eat you once and for all.<br><br>
                  So, I put everything else aside, got my technical team into a huddle, and decided that we will create a cloud-based email marketing platform that gives better and trackable results time and time again.<br><br>
                  After lots of hard work and burning my midnight lamps for countless nights, I came up with this magnificent feature-packed Email marketing system that gives me complete freedom of work & control over my business.
               </div>
            </div>
         </div>
      </div>
      <div class="problem-bg-section4">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-45 w600 black-clr lh140">
                     Here’s Where the Unique TWIST Comes In...
                  </div>
                  <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/comes-line.webp" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 f-18 f-md-20 w400 lh160 mt20 black-clr">           
                  With this ultimate technology I am managing my own 40,000+ subscribers for more than 1 year and I have witnessed a 34% increase in my open and click-through rates and so, my email marketing profits also increased by 34%.  
               </div>
               <div class="col-12 f-18 f-md-20 w400 lh160 mt20 mt-md50 black-clr">           
                  <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/problem5.webp" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 f-18 f-md-20 w400 lh160 mt20 mt-md50 black-clr">           
                  Now that we have the most powerful system on earth at your fingertips, allowing you to instantly automate your email marketing campaigns as you’ve always wanted, boost inboxing and ultimately get the best results Hands FREE. <br><br>
                  But as they say, knowledge not shared is knowledge wasted. <br><br>
                  So here I present something that enables YOU to control your own destiny - and not big corporations.
               </div>
            </div>
         </div>
      </div>
      <!--Proudly Introducing Start -->
      <div class="next-gen-sec" id="product">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="pre-heading f-md-32 f-24 w600 text-center white-clr lh140">
                     Proudly Presenting... 
                  </div>
               </div>
               <div class="col-12 mt-md20 mt20 text-center">
                  <svg version="1.1" x="0px" y="0px" viewBox="0 0 1501.16 276.9" style="enable-background:new 0 0 1501.16 276.9; max-height:100px;" xml:space="preserve">
                     <style type="text/css">
                        .st0{fill:#FFFFFF;}
                     </style>
                     <g>
                        <path class="st0" d="M122.38,159.77l82.88-67.64h39.5v182.43h-45.44V160.68l-73.53,57.2h-6.82l-73.52-57.2v113.87H0V92.13h39.46
                           L122.38,159.77z"></path>
                        <path class="st0" d="M391.87,92.13l81.03,182.43h-48.87L409,238.51h-81.7l-13.62,36.04h-48.16l73.69-182.43H391.87z M395.71,206.65
                           l-29.57-70.91l-26.8,70.91H395.71z"></path>
                        <path class="st0" d="M540.93,92.13v182.43h-45.44V92.13H540.93z"></path>
                        <path class="st0" d="M722.06,241.12v33.43H582.98V92.13h45.44v149H722.06z"></path>
                        <path class="st0" d="M913.75,92.13l-99.48,149h99.48v33.43H736.68l100.26-149H743.6V92.13H913.75z"></path>
                        <path class="st0" d="M989.75,92.13v182.43h-45.44V92.13H989.75z"></path>
                        <path class="st0" d="M1170.88,241.12v33.43H1031.8V92.13h45.44v149H1170.88z"></path>
                        <path class="st0" d="M1332.43,209.97c-2.33,5.08-5.42,9.64-9.28,13.71c-9.7,10.23-22.48,15.35-38.35,15.35
                           c-15.94,0-28.8-5.12-38.53-15.35c-9.74-10.22-14.61-23.66-14.61-40.28c0-16.72,4.87-30.17,14.61-40.35
                           c5.49-5.75,11.97-9.87,19.44-12.37l-26.14-31.06c-9.05,4.52-17.19,10.6-24.43,18.22c-17.75,18.72-26.63,40.57-26.63,65.56
                           c0,25.41,8.94,47.36,26.81,65.81c17.87,18.45,41.03,27.68,69.49,27.68c28.13,0,51.18-9.26,69.13-27.81
                           c2.07-2.15,4.04-4.34,5.87-6.58L1332.43,209.97z"></path>
                        <path class="st0" d="M1501.16,0l-114.63,241.95l-71.11-84.51l-31.99,29.36l2.17-64.79c67.33-7.59,111.49-65.77,112.17-66.67
                           c-41.92,31-125.97,49.4-125.97,49.4l-28.6-33.1L1501.16,0z"></path>
                     </g>
                  </svg>
               </div>
               <div class="f-md-36 f-24 w600 yellow-clr lh140 col-12 mt20 mt-md40 text-center">
                  The Most Advanced & Powerful Email Marketing Automation Software that 
                  allows you to Create, Send, Track, and Profit from Email Marketing.
               </div>
               <div class="f-20 f-md-24 lh140 w400 text-center white-clr mt20">              
                  It is a must-have tool in your marketing arsenal that comes with a ONE-TIME FEE, <br class="d-none d-md-block"> and that will send all the existing money-sucking autoresponders back to their nest.
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md50">
               <div class="col-12 col-md-10 mx-auto">
                  <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/product-box.webp" class="img-fluid d-block mx-auto" alt="Product">
               </div>
            </div>
            <div class="row mt20 mt-md50">
               <div class="col-12">
                  <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/limited-time-offer-banner.webp" class="img-fluid d-block mx-auto">
                  <div class="f-22 f-md-28 w700 lh140 text-center white-clr mt20">
                     Grab MailZilo Now at 80% Launch Special Discount!
                  </div>
                  <div class="f-18 f-md-22 w500 lh140 text-center yellow-clr mt10">
                     (Get Free 30 Reseller License + Commercial License + Special Bonuses)
                  </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 text-center">
                        <a href="https://warriorplus.com/o2/buy/xgwy6m/y4pjnq/nh90r1" class="cta-link-btn">Get Instant Access To MailZilo</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20">
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/compaitable-with1.webp" class="img-fluid d-block mx-xs-center md-img-right">
                     <div class="d-md-block d-none px-md30"><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/v-line.webp" class="img-fluid"></div>
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/days-gurantee1.webp" class="img-fluid d-block mx-xs-auto mt15 mt-md0">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--Proudly Introducing End -->
      <div class="zero">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-24 f-md-32 w600 lh140 text-center">Use this Intelligent BUT Stupid SIMPLE Software to get more leads & higher Inboxing again and again… With ZERO Technical Skills,
                     ZERO Headache, and ZERO Grunt Work...
                  </div>
               </div>
            </div>
            <div class="row row-cols-1 row-cols-md-3 mt0 gap30">
               <div class="col">
                  <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/z1.webp" class="img-fluid d-block mx-auto">
                  <div class="f-20 w400 lh140 text-center mt20">Generate More Leads from any
                     blog, ecommerce or WordPress site
                  </div>
               </div>
               <div class="col">
                  <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/z2.webp" class="img-fluid d-block mx-auto">
                  <div class="f-20 w400 lh140 text-center mt20">Get mails delivered straight
                     to INBOX
                  </div>
               </div>
               <div class="col">
                  <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/z3.webp" class="img-fluid d-block mx-auto">
                  <div class="f-20 w400 lh140 text-center mt20">Send Unlimited Mails to
                     Unlimited subscribers
                  </div>
               </div>
               <div class="col">
                  <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/z4.webp" class="img-fluid d-block mx-auto">
                  <div class="f-20 w400 lh140 text-center mt20">Use smart tags to segment your
                     subscribers & Send exclusive emails
                  </div>
               </div>
               <div class="col">
                  <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/z5.webp" class="img-fluid d-block mx-auto">
                  <div class="f-20 w400 lh140 text-center mt20">Have Complete Control On Your
                     Business – don’t rely on 3rd party services
                  </div>
               </div>
               <div class="col">
                  <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/z6.webp" class="img-fluid d-block mx-auto">
                  <div class="f-20 w400 lh140 text-center mt20">No need to Pay Huge
                     Monthly Fees
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="step-section">
         <div class="container ">
            <div class="row ">
               <div class="col-12 f-28 f-md-45 w600 lh140 black-clr text-center">
                  Replace your old-school email marketing <br class="d-none d-md-block"> techniques with this cutting-edge solution that<br class="d-none d-md-block"> powers your business <span class="orange-clr d-inline-grid">in just 3 easy steps… <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/underline.webp " class="img-fluid"></span>
               </div>
               <div class="col-12 mt30 mt-md80">
                  <div class="row align-items-center">
                     <div class="col-12 col-md-6 relative">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/step1.webp" class="img-fluid">
                        <div class="f-28 f-md-40 w600 lh140 mt15">
                           Upload
                        </div>
                        <div class="f-18 f-md-20 w400 lh140 mt15">
                           To begin with, all you need to do is upload your subscribers 
                           or generate leads using MailZilo opt-in forms.   
                        </div>
                        <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/left-arrow.webp" class="img-fluid d-none d-md-block ms-auto step-arrow1">
                     </div>
                     <div class="col-12 col-md-6 mt20 mt-md0 ">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/step-one.webp" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
               <div class="col-12 mt30 mt-md80">
                  <div class="row align-items-center ">
                     <div class="col-12 col-md-6 order-md-2 relative">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/step2.webp" class="img-fluid">
                        <div class="f-28 f-md-40 w600 lh140 mt15">
                           Select or Create
                        </div>
                        <div class="f-18 f-md-20 w400 lh140 mt15">
                           Select from the list of beautifully designed email templates 
                           or create your own email template.   
                        </div>
                        <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/right-arrow.webp" class="img-fluid d-none d-md-block step-arrow2">
                     </div>
                     <div class="col-md-6 col-md-pull-6 mt20 mt-md0 order-md-1 ">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/step-two.webp " class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
               <div class="col-12 mt30 mt-md80">
                  <div class="row align-items-center">
                     <div class="col-12 col-md-6">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/step3.webp" class="img-fluid">
                        <div class="f-28 f-md-40 w600 lh140 mt15">
                           Send & Profit
                        </div>
                        <div class="f-18 f-md-20 w400 lh140 mt15">
                           Send Unlimited Email to your Subscriber or Schedule it for later.
                        </div>
                     </div>
                     <div class="col-12 col-md-6 mt20 mt-md0 ">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/step-three.webp " class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md60 text-center">
                  <div class="d-flex gap-2 gap-md-4 justify-content-center flex-wrap">
                     <div class="d-flex align-items-center gap-3 justify-content-center">
                        <img src="https://cdn.oppyo.com/launches/writerarc/bizdrive/cross.webp" alt="Cross" class="img-fluid d-block">
                        <div class="f-md-28 w700 lh140 f-20 red-clr"> No SMTP</div>
                     </div>
                     <div class="d-flex align-items-center gap-3 justify-content-center">
                        <img src="https://cdn.oppyo.com/launches/writerarc/bizdrive/cross.webp" alt="Cross" class="img-fluid d-block">
                        <div class="f-md-28 w700 lh140 f-20 red-clr"> No Installation</div>
                     </div>
                     <div class="d-flex align-items-center gap-3 justify-content-center">
                        <img src="https://cdn.oppyo.com/launches/writerarc/bizdrive/cross.webp" alt="Cross" class="img-fluid d-block">
                        <div class="f-md-28 w700 lh140 f-20 red-clr">No Other Techie Stuff</div>
                     </div>
                  </div>
                  <div class="f-20 f-md-28 w500 lh140 mt20">It’s EASY, FAST & Built with LOVE. </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Demo Video Section Start -->
      <div class="demo-section-bg" id="demo">
         <div class="container ">
            <div class="row ">
               <div class="col-12 f-28 f-md-45 w500 lh140 black-clr text-capitalize text-center">
                  <span class="orange-clr w600">Watch This Short Demo Video</span> <br class="d-none d-md-block">
                  & See How Easy and POWERFUL MailZilo Is...
               </div>
               <div class="col-12 col-md-9 mx-auto mt20 mt-md40">
                  <div class="responsive-video">
                     <iframe src="https://mailzilo.dotcompal.com/video/embed/wnbil2v1yd" style="width: 100%; height: 100%; " frameborder="0 " allow="fullscreen " allowfullscreen></iframe>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Demo Video Section End -->
      <!-- Testimonial Section Start -->
      <div class="testimonial-section">
         <div class="container ">
            <div class="row ">
               <div class="col-12 f-28 f-md-45 w600 lh140 black-clr text-center">
                  Here's What REAL Users Have To <br class="d-none d-md-block">
                  Say About MailZilo
               </div>
            </div>
            <div class="row row-cols-md-2 row-cols-1 gx4">
               <div class="col mt50">
                  <div class="single-testimonial">
                     <p>
                     I got a chance to beta-test MailZilo, and I must confess, <span class="w600">I really liked its performance. It gets your emails delivered straight to the inbox and boost opt-in rates</span> with no additional effort or expense. MailZilo is fully personalized, unique, and reliable. Yet another masterpiece. 
                     </p>
                     <div class="client-info">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/lewis-grey.webp" class="img-fluid d-block mx-auto">
                        <div class="client-details">
                           Lewis Grey
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col mt50">
                  <div class="single-testimonial">
                     <p>
                     MailZilo is an easy-to-use email marketing platform that cuts the technicalities and makes <span class="w600">it easier for beginners to start off easily.</span> The ability to send mails without any limit <span class="w600">with built-in SMTP</span> definitely helps save month costs as well. 
                     </p>
                     <div class="client-info">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/john-warner.webp" class="img-fluid d-block mx-auto">
                        <div class="client-details">
                           John Warner
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col mt50">
                  <div class="single-testimonial">
                     <p>
                     Trust me when I say this: MailZilo is the only product you need to invest in this New Year. It’s incredibly simple to use, yet has everything you could need.<br><br>
                     <span class="w600">Super Easy Interface? Check.<br>
Eye-Catchy Templates? Check.<br>
Massive sales just with laser-targeted promo campaigns? Hell yeah.</span><br>
Don't Think - Just Get This!

                     </p>
                     <div class="client-info">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/stuart-johnson.webp" class="img-fluid d-block mx-auto">
                        <div class="client-details">
                        Stuart Johnson
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col mt50">
                  <div class="single-testimonial">
                     <p>
                     Ayush Jain, thanks a ton for this incredibly amazing email marketing software. <span class="w600">It resolved a huge problem that I had with other software. Losing my precious list while uploading!</span> Finally, with MailZilo I am back to winning ways
                     </p>
                     <div class="client-info">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/chuk-akspan.webp" class="img-fluid d-block mx-auto">
                        <div class="client-details">
                           Chuk Akspan
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Testimonial Section End -->
      <!-- Cta Section -->
      <div class="cta-section-new">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/limited-time-offer-banner.webp" class="img-fluid d-block mx-auto">
                  <div class="f-22 f-md-28 w700 lh140 text-center white-clr mt20">
                     Grab MailZilo Now at 80% Launch Special Discount!
                  </div>
                  <div class="f-18 f-md-22 w500 lh140 text-center yellow-clr mt10">
                     (Get Free 30 Reseller License + Commercial License + Special Bonuses)
                  </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 text-center">
                        <a href="https://warriorplus.com/o2/buy/xgwy6m/y4pjnq/nh90r1" class="cta-link-btn">Get Instant Access To MailZilo</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20">
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/compaitable-with1.webp" class="img-fluid d-block mx-xs-center md-img-right">
                     <div class="d-md-block d-none px-md30"><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/v-line.webp" class="img-fluid"></div>
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/days-gurantee1.webp" class="img-fluid d-block mx-xs-auto mt15 mt-md0">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Cta Section End -->
      <!-- Features Part -->
      <!-- Features Section Start -->
      <div class="feature-highlight" id="features">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-45 f-28 w600 text-center white-clr lh140">
                  Checkout The Ground-Breaking Features<br class="d-none d-md-block"> That Make MailZilo A Cut Above The Rest   
               </div>
            </div>
         </div>
      </div>
      <!-- Features Part -->
      <!-- Features Section Start -->
      <div class="white-section">
         <div class="container">
            <div class="row align-items-center">
               <!-- Landing Grid 1 -->
               <div class="col-12 ">
                  <div class="row align-items-center ">
                     <div class="col-md-6 col-12 order-md-2">
                        <div class="f-24 f-md-30 w600 lh140 features-title">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/common_assets/images/1.webp" class="img-responsive size">
                           Import Unlimited Subscribers without even losing a single email ID
                        </div>
                        <div class="f-18 w400 mt20 lh140 ">
                           MailZilo enables you to import unlimited subscribers list and the best part is that you won't lose even a single ID in the process. So, you can mail freely to your subscribers without any restrictions whatsoever.<br><br>
                           Most email marketing service providers charge a hefty fees for importing your lists so stop paying a huge monthly rental just to keep your list even if you don't mail them.<br><br>
                           They also need double optin before allowing to import so you lose anywhere from 20-30% of your subscribers. But with MailZilo, all that will become a case of the bygone era.
                        </div>
                     </div>
                     <div class="col-md-6 col-12 mt20 mt-md0 order-md-1">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/f1.webp" class="img-fluid d-block mx-auto " />
                     </div>
                  </div>
               </div>
               <!-- Landing Grid 2 -->
               <div class="col-12 mt-md90 mt20 ">
                  <div class="row align-items-center ">
                     <div class="col-md-6 col-12 ">
                     <div class="f-24 f-md-30 w600 lh140 features-title">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/common_assets/images/2.webp" class="img-responsive size">
                           Send Unlimited Mails Instantly or Schedule Them for Later
                        </div>
                        <div class="f-18 w400 mt20 lh140 ">
                           Yes, the sky is the limit for mailing with MailZilo. It allows you to send unlimited emails or newsletters and get rid of endless complications of email marketing. You can send emails right away or schedule them for a later date and time.
                        </div>
                     </div>
                     <div class="col-md-6 col-12 mt20 mt-md0 ">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/f2.webp" class="img-fluid d-block mx-auto " />
                     </div>
                  </div>
               </div>
               <!-- Landing Grid 3 -->
               <div class="col-12 mt-md90 mt20 ">
                  <div class="row align-items-center ">
                     <div class="col-md-6 col-12 order-md-2">
                        < <div class="f-24 f-md-30 w600 lh140 features-title">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/common_assets/images/3.webp" class="img-responsive size">
                           Built-In SMTP to Send Unlimited Emails Every Month  
                        </div>
                        <div class="f-18 w400 mt20 lh140 ">
                           MailZilo comes with Built-In SMTP to send unlimited Mails Every Month or you also can set up your own SMTP within seconds
                        </div>
                     </div>
                     <div class="col-md-6 col-12 mt20 mt-md0 order-md-1">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/f3.webp" class="img-fluid d-block mx-auto " />
                     </div>
                  </div>
               </div>
         </div></div></div>

         <div class="blue-section">
         <div class="container">
            <div class="row align-items-center">
                        <!-- Landing Grid 15 -->
                        <div class="col-12">
                  <div class="row align-items-center ">
                     <div class="col-md-6 col-12">
                     <div class="f-24 f-md-30 w600 lh140 features-title white-clr">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/common_assets/images/4.webp" class="img-responsive size">
                           90,000+ DFY Email Swipes 
                        </div>
                        <div class="f-18 w600 mt20 lh140 white-clr">
                        You will Never have to Hassle Again with Writing Email Content That Converts!
                        </div>
                        <div class="f-18 w400 mt20 lh140 white-clr">
                           Yes, you will also get 90,000+ Done for You High Converting Readily Prepared Email Swipes to ensure that your Brand will be Marketed in Approximately the Same Style as Big Marketers do.
                        </div>
                     </div>
                     <div class="col-md-6 col-12 mt20 mt-md0">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/f15.webp" class="img-fluid d-block mx-auto " />
                     </div>
                  </div>
               </div>

               </div></div></div>
               <div class="white-section">
         <div class="container">
            <div class="row align-items-center">
               <!-- Landing Grid 4 -->
               <div class="col-12">
                  <div class="row align-items-center ">
                     <div class="col-md-6 col-12 order-md-2">
                     <div class="f-24 f-md-30 w600 lh140 features-title">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/common_assets/images/5.webp" class="img-responsive size">
                           Generate more potential subscribers with our attractive in-built lead form
                        </div>
                        <div class="f-18 w400 mt20 lh140 ">
                           Lead forms are the simplest way to generate qualified leads and build a huge base of cash paying customers for your business.
                           <br><br>
                           Keeping this in mind, MailZilo helps you to grab attention of more and more subscribers on your blog, e-commerce sites or WordPress sites with an eye-catchy lead generation form.
                           <br><br>
                           All you need to do is: copy one line of code and paste it on your site.
                        </div>
                     </div>
                     <div class="col-md-6 col-12 mt20 mt-md0 order-md-1">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/f4.webp" class="img-fluid d-block mx-auto " />
                     </div>
                  </div>
               </div>
               <!-- Landing Grid 5 -->
               <div class="col-12 mt-md90 mt20 ">
                  <div class="row align-items-center ">
                     <div class="col-md-6 col-12">
                     <div class="f-24 f-md-30 w600 lh140 features-title">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/common_assets/images/6.webp" class="img-responsive size">
                           Boost email delivery, click and open rate
                        </div>
                        <div class="f-18 w400 mt20 lh140 ">
                           Opens and clicks give you an accurate idea about how your campaign is performing. Today all the marketers want to get their mails delivered in inbox, clicked and opened on time and that is what MailZilo is made for.
                           <br><br>
                           It increases your delivery, click and open rate and gives you full control on your campaigns.
                        </div>
                     </div>
                     <div class="col-md-6 col-12 mt20 mt-md0 ">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/f5.webp" class="img-fluid d-block mx-auto " />
                     </div>
                  </div>
               </div>
                <!-- Landing Grid 6 -->
                <div class="col-12 mt-md90 mt20">
                  <div class="row align-items-center ">
                     <div class="col-md-6 col-12 order-md-2">
                     <div class="f-24 f-md-30 w600 lh140 features-title black-clr">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/common_assets/images/7.webp" class="img-responsive size">
                           Use smart tags to segment your subscribers & Send exclusive emails
                        </div>
                        <div class="f-18 w400 mt20 lh140 black-clr">
                           This is our masterpiece. Using this Latest & VERY Powerful feature, you can assign tags to your subscribers and segment them in a very simple manner. Now you can send emails exclusively to the subscribers related to any smart Tag or group.
                        </div>
                     </div>
                     <div class="col-md-6 col-12 mt20 mt-md0 order-md-1">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/f6.webp" class="img-fluid d-block mx-auto " />
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <div class="grey-section">
         <div class="container">
            <div class="row align-items-center">
               <!-- Landing Grid 7 -->
               <div class="col-12">
                  <div class="row align-items-center ">
                     <div class="col-md-6 col-12 ">
                     <div class="f-24 f-md-30 w600 lh140 features-title">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/common_assets/images/8.webp" class="img-responsive size">
                           Reduce bounce rate
                        </div>
                        <div class="f-18 w400 mt20 lh140 ">
                           Higher bounce spoils your image and get murky with every bounce. With MailZilo, you can get rid of all the bounced and spammed mails. MailZilo automatically remove mails that were counted as bounce and make your list clear without any grunt work.
                        </div>
                     </div>
                     <div class="col-md-6 col-12 mt20 mt-md0 ">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/f7.webp" class="img-fluid d-block mx-auto " />
                     </div>
                  </div>
               </div>
               <!-- Landing Grid 8 -->
               <div class="col-12 mt-md90 mt20">
                  <div class="row align-items-center ">
                     <div class="col-md-6 col-12 order-md-2">
                     <div class="f-24 f-md-30 w600 lh140 features-title">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/common_assets/images/9.webp" class="img-responsive size">
                           Inbuilt Text & Inline Editor to craft best emails
                        </div>
                        <div class="f-18 w400 mt20 lh140 ">
                           You can create simple text emails or html emails with our LIVE Inline editor feature to send best emails for maximum engagement. This is all built to attract, capture, nurture and convert your potential prospect.
                        </div>
                     </div>
                     <div class="col-md-6 col-12 mt20 mt-md0 order-md-1">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/f8.webp" class="img-fluid d-block mx-auto " />
                     </div>
                  </div>
               </div>
               <!-- Landing Grid 9 -->
               <div class="col-12 mt-md90 mt20 ">
                  <div class="row align-items-center ">
                     <div class="col-md-6 col-12 ">
                     <div class="f-24 f-md-30 w600 lh140 features-title">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/common_assets/images/10.webp" class="img-responsive size">
                           MailZilo is CAN SPAM compliant
                        </div>
                        <div class="f-18 w400 mt20 lh140 ">
                           It provide one-click unsubscribe feature that is user-friendly and helps to greatly reduce spam complaints and built your better send reputation.
                        </div>
                     </div>
                     <div class="col-md-6 col-12 mt20 mt-md0 ">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/f9.webp" class="img-fluid d-block mx-auto " />
                     </div>
                  </div>
               </div>
               <!-- Landing Grid 10 -->
               <div class="col-12 mt-md90 mt20">
                  <div class="row align-items-center ">
                     <div class="col-md-6 col-12 order-md-2">
                     <div class="f-24 f-md-30 w600 lh140 features-title">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/common_assets/images/11.webp" class="img-responsive size">
                           Manage your subscribers hassle-free
                        </div>
                        <div class="f-18 w400 mt20 lh140 ">
                           MailZilo offers you the easiest way to find, filter or clean your subscribers in never-ending lists. You can find out a subscriber out of a list of thousands with just 1 click.
                           <br><br>
                           Track duplicate entries and create a backup of your list in no time with MailZilo.
                        </div>
                     </div>
                     <div class="col-md-6 col-12 mt20 mt-md0 order-md-1">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/f10.webp" class="img-fluid d-block mx-auto" />
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="white-section">
         <div class="container">
            <div class="row align-items-center">
               <!-- Landing Grid 11 -->
               <div class="col-12">
                  <div class="row align-items-center ">
                     <div class="col-md-6 col-12 ">
                     <div class="f-24 f-md-30 w600 lh140 features-title">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/common_assets/images/12.webp" class="img-responsive size">
                           All-in-one cloud based email marketing software
                        </div>
                        <div class="f-18 w400 mt20 lh140 ">
                           MailZilo is built on the idea to deliver maximum quality, ease and efficiency. And to make it simpler we made it a cloud based platform.
                           <br><br>
                           Guys you will be thrilled by numbers of features that we are offering to you to make your email marketing simple and fun.
                        </div>
                     </div>
                     <div class="col-md-6 col-12 mt20 mt-md0 ">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/f11.webp" class="img-fluid d-block mx-auto " />
                     </div>
                  </div>
               </div>
               <!-- Landing Grid 12 -->
               <div class="col-12 mt-md90 mt20">
                  <div class="row align-items-center ">
                     <div class="col-md-6 col-12 order-md-2">
                     <div class="f-24 f-md-30 w600 lh140 features-title">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/common_assets/images/13.webp" class="img-responsive size">
                           Boost sales and increase revenue
                        </div>
                        <div class="f-18 w400 mt20 lh140 ">
                           Email marketing is taking the world by storm and if you are searching the right way to send best emails to attract more customers, MailZilo is the best among rest.
                           <br><br>
                           You could send a free whitepaper, more information on your products, "subscriber-only" discount, or even personize mails to contend your customers for taking action. Your imagination and creativity really are the only limits and you have endless opportunities to get success.
                        </div>
                     </div>
                     <div class="col-md-6 col-12 mt20 mt-md0 order-md-1">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/f12.webp" class="img-fluid d-block mx-auto " />
                     </div>
                  </div>
               </div>
               <!-- Landing Grid 13 -->
               <div class="col-12 mt-md90 mt20 ">
                  <div class="row align-items-center ">
                     <div class="col-md-6 col-12 ">
                     <div class="f-24 f-md-30 w600 lh140 features-title">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/common_assets/images/14.webp" class="img-responsive size">
                           Personalize your mails to get high opening rates
                        </div>
                        <div class="f-18 w400 mt20 lh140 ">
                           Now this is something that will really prove to be of great worth for your marketing efforts. Personalization of emails is the best and easiest way to get attention of your subscribers, with MailZilo, you can personalize your every email to every subscriber to get high open rates.
                        </div>
                     </div>
                     <div class="col-md-6 col-12 mt20 mt-md0 ">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/f13.webp" class="img-fluid d-block mx-auto " />
                     </div>
                  </div>
               </div>
               <!-- Landing Grid 14 -->
               <div class="col-12 mt-md90 mt20">
                  <div class="row align-items-center ">
                     <div class="col-md-6 col-12 order-md-2">
                     <div class="f-24 f-md-30 w600 lh140 features-title">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/common_assets/images/15.webp" class="img-responsive size">
                           Create long-term relationships with your subscribers with beautiful newsletters
                        </div>
                        <div class="f-18 w400 mt20 lh140 ">
                           Always try to keep in touch with your subscribers to create a strong relationship with them. You can send newsletters to update them about your products or upcoming launches.
                           <br><br>
                           You can redirect them on your blog or website to keep your brand in their mind.
                        </div>
                     </div>
                     <div class="col-md-6 col-12 mt20 mt-md0 order-md-1">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/f14.webp" class="img-fluid d-block mx-auto " />
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="grey-section">
         <div class="container">
            <div class="row align-items-center">
               <!-- Landing Grid 16-->
               <div class="col-12">
                  <div class="row align-items-center ">
                     <div class="col-md-6 col-12">
                     <div class="f-24 f-md-30 w600 lh140 features-title">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/common_assets/images/16.webp" class="img-responsive size">
                           100% newbie friendly and fully automated
                        </div>
                        <div class="f-18 w400 mt20 lh140 ">
                           Effective email marketing is all about sending right email to right people at right time and our software team has got everything covered and made it no hands software.
                           <br><br>
                           With the robust features of MailZilo, you have the complete freedom to automate your email marketing. The software delivers automated, one-to-one messages across all of your marketing channels and even a newbie can manage his email marketing campaign without any hassle.
                        </div>
                     </div>
                     <div class="col-md-6 col-12 mt20 mt-md0 ">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/f16.webp" class="img-fluid d-block mx-auto " />
                     </div>
                  </div>
               </div>
               <!-- Landing Grid 17 -->
               <div class="col-12 mt-md90 mt20">
                  <div class="row align-items-center ">
                     <div class="col-md-6 col-12 order-md-2">
                     <div class="f-24 f-md-30 w600 lh140 features-title">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/common_assets/images/17.webp" class="img-responsive size">
                           No monthly fees or additional charges
                        </div>
                        <div class="f-18 w400 mt20 lh140 ">
                           If you are a newbie and starting out with email marketing, then sending mails can be a costly affair. They charge you like wildfire and you have to provide a heavy monthly fee just to reach out to your email list.
                           <br><br>
                           But MailZilo is hands down the best email marketing software available today that charges no recurring fee and allows you to send unlimited emails with just one click.
                        </div>
                     </div>
                     <div class="col-md-6 col-12 mt20 mt-md0 order-md-1">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/f17.webp" class="img-fluid d-block mx-auto " />
                     </div>
                  </div>
               </div>
               <!-- Landing Grid 18 -->
               <div class="col-12 mt-md90 mt20 ">
                  <div class="row align-items-center ">
                     <div class="col-md-6 col-12">
                     <div class="f-24 f-md-30 w600 lh140 features-title">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/common_assets/images/18.webp" class="img-responsive size">
                           Brand new system- Absolutely NO rehashes
                        </div>
                        <div class="f-18 w400 mt20 lh140 ">
                           We always believe in giving you something that's packed with latest features and which is simply not an add-on to a pre-existing product.
                           <br><br>
                           So, MailZilo is packed with great features and it's the ultimate email marketing technology that's never been seen before.
                        </div>
                     </div>
                     <div class="col-md-6 col-12 mt20 mt-md0 ">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/f18.webp" class="img-fluid d-block mx-auto " />
                     </div>
                  </div>
               </div>
               <!-- Landing Grid 19 -->
               <div class="col-12 mt-md90 mt20">
                  <div class="row align-items-center ">
                     <div class="col-md-6 col-12 order-md-2">
                     <div class="f-24 f-md-30 w600 lh140 features-title">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/common_assets/images/19.webp" class="img-responsive size">
                           Designed by Marketers for Marketers
                        </div>
                        <div class="f-18 w400 mt20 lh140 ">
                           MailZilo has been built from the ground up to be A-Z marketer-friendly, meaning you can upload your list of subscribers straight into the software with no technical hassles, and get best results without any complications.
                        </div>
                     </div>
                     <div class="col-md-6 col-12 mt20 mt-md0 order-md-1">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/f19.webp" class="img-fluid d-block mx-auto " />
                     </div>
                  </div>
               </div>
               <!-- Landing Grid 20 -->
               <div class="col-12 mt-md90 mt20">
                  <div class="row align-items-center ">
                     <div class="col-md-6 col-12">
                     <div class="f-24 f-md-30 w600 lh140 features-title">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/common_assets/images/20.webp" class="img-responsive size">
                           Step-By-Step Training to Make Everything Easy For You...
                        </div>
                        <div class="f-18 w400 mt20 lh140 ">
                           Yep, we know software can get complex. And while MailZilo is DEAD easy to use, we wanted to make 100% sure it's accessible to everyone and everyone can make money with it. That's why we did 2 things:
                           <br><br>
                           #1 We've added in-depth video training for every feature so you can always look at the RIGHT way to do things
                           <br><br>
                           #2 We're also offering 24*7 on-going support so you're always just a message away from having your problem solved.
                        </div>
                     </div>
                     <div class="col-md-6 col-12 mt20 mt-md0 ">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/f20.webp" class="img-fluid d-block mx-auto " />
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Features Part End-->
      <!-- Cta Section -->
      <div class="cta-section-new">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/limited-time-offer-banner.webp" class="img-fluid d-block mx-auto">
                  <div class="f-22 f-md-28 w700 lh140 text-center white-clr mt20">
                     Grab MailZilo Now at 80% Launch Special Discount!
                  </div>
                  <div class="f-18 f-md-22 w500 lh140 text-center yellow-clr mt10">
                     (Get Free 30 Reseller License + Commercial License + Special Bonuses)
                  </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 text-center">
                        <a href="https://warriorplus.com/o2/buy/xgwy6m/y4pjnq/nh90r1" class="cta-link-btn">Get Instant Access To MailZilo</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20">
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/compaitable-with1.webp" class="img-fluid d-block mx-xs-center md-img-right">
                     <div class="d-md-block d-none px-md30"><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/v-line.webp" class="img-fluid"></div>
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/days-gurantee1.webp" class="img-fluid d-block mx-xs-auto mt15 mt-md0">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Cta Section End -->     
      <section class="imagine-sec">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-12 col-md-6 f-18 f-md-20 w400 lh160 black-clr order-md-2">
                  ... ! A new lightweight piece of technology that’s so incredibly sophisticated yet intuitive & easy to use that allows you to do it all with literally ONE click!
                  <br><br>
                  <span class="w600">That’s EXACTLY what we’ve designed MailZilo to do for you.</span> 
                  <br><br>
                  So, if you want to create your own fully controlled Email marketing system to generate more leads & email traffic to any offer without paying a BIG monthly fee forever, all ON AUTOPILOT from start to finish, then MailZilo is made for you!
                  <br><br>
                  Finally, you now have the ability to generate the kind of results you’ve seen in the screenshots above.
               </div>
               <div class="col-md-6 col-12 order-md-1 mt20 mt-md0">
                  <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/imagine-box.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>
         </div>
      </section>
      <div class="targeted-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row d-flex align-items-center flex-wrap">
                     <div class="col-12 col-md-7">
                        <div class=" f-18 f-md-20 lh160 w400  black-clr">
                           <ul class="noneed-listing pl0">
                              <li>Import unlimited lists easily without any restrictions</li>
                              <li>Get boatloads of qualified leads with minimum time and money invested</li>
                              <li>Build strong relationships with your subscribers with well-crafted emails</li>
                              <li>Send UNLIMITED emails to UNLIMITED Subscribers without paying any monthly fees</li>
                              <li>Get faster inbox delivery, a massive increase in opens, and make more money from your existing subscriber list</li>
                              <li>Have complete control of your email marketing campaigns</li>
                              <li>Greatly reduce spam complaints and protects your send reputation</li>
                              <li>Enjoy Traffic by sending Unlimited & best emails</li>
                           </ul>
                        </div>
                     </div>
                     <div class="col-12 col-md-5 mt20 mt-md0">
                        <img src="https://cdn.oppyo.com/launches/vidboxs/fe/target-arrow.webp" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
               <div class="col-12 mt-md30 mt20">
                  <div class="f-18 f-md-20 w400 lh160 black-clr">
                     <span class="w600">Seriously, having to do all that manually is expensive,</span>  time-consuming, and put frankly downright irritating, especially once you want to build more than 1 channel that’s packed with gold content.
                     <br><br>
                     That's exactly the reason so many people are afraid to even get started in video marketing and the same <span class="w600">reason why out of all that DO get started, very few truly "make it".</span> 
                     <br><br>
                     We have done all the grunt work for you. So, you don’t need to worry at all. 
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="benefit-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-28 f-md-45 w700 lh140 text-center white-clr">Who Can Benefit from MailZilo?</div>
               </div>
            </div>
            <div class="row row-cols-1 gap30 mt0 mt-md20">
               <div class="col">
                  <div class="benefit-block">
                     <div><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/check.webp" class="img-fluid d-block mx-auto"></div>
                     <div class="f-18 w400 lh140 white-clr">Any Internet Marketer, regardless of the niche: MailZilo helps email marketers boost inboxing and make the most from
                        their email marketing campaigns, and list builders skyrocket their subscribers numbers... all at the push of a button
                     </div>
                  </div>
               </div>
            </div>
            <div class="row row-cols-1 row-cols-md-3 gap30 mt0">
               <div class="col">
                  <div class="benefit-block">
                     <div><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/check.webp" class="img-fluid d-block mx-auto"></div>
                     <div class="f-18 w400 lh140 white-clr">
                        Anyone who wants to streamline their business while focusing on bigger projects
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="benefit-block">
                     <div><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/check.webp" class="img-fluid d-block mx-auto"></div>
                     <div class="f-18 w400 lh140 white-clr">
                        Lazy people who are reluctant to work
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="benefit-block">
                     <div><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/check.webp" class="img-fluid d-block mx-auto"></div>
                     <div class="f-18 w400 lh140 white-clr">
                        Anyone who wants to value their business and money and is not ready to sacrifice them
                     </div>
                  </div>
               </div>
            </div>
            <div class="row row-cols-2 row-cols-md-5 gaps mt0">
               <div class="col text-center">
                  <div class="img-block">
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/b1.webp" class="img-fluid d-block mx-auto">
                     <div class="f-18 f-md-20 w400 lh140 white-clr">
                        Business
                     </div>
                  </div>
               </div>
               <div class="col text-center">
                  <div class="img-block">
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/b2.webp" class="img-fluid d-block mx-auto">
                     <div class="f-18 f-md-20 w400 lh140 white-clr">
                        Internet Marketing
                     </div>
                  </div>
               </div>
               <div class="col text-center">
                  <div class="img-block">
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/b3.webp" class="img-fluid d-block mx-auto">
                     <div class="f-18 f-md-20 w400 lh140 white-clr">
                        Software and Networking
                     </div>
                  </div>
               </div>
               <div class="col text-center">
                  <div class="img-block">
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/b4.webp" class="img-fluid d-block mx-auto">
                     <div class="f-18 f-md-20 w400 lh140 white-clr">
                        Electronics and Computers
                     </div>
                  </div>
               </div>
               <div class="col text-center">
                  <div class="img-block">
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/b5.webp" class="img-fluid d-block mx-auto">
                     <div class="f-18 f-md-20 w400 lh140 white-clr">
                        Fun and Entertainment
                     </div>
                  </div>
               </div>
               <div class="col text-center">
                  <div class="img-block">
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/b6.webp" class="img-fluid d-block mx-auto">
                     <div class="f-18 f-md-20 w400 lh140 white-clr">
                        Knowledge and Education
                     </div>
                  </div>
               </div>
               <div class="col text-center">
                  <div class="img-block">
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/b7.webp" class="img-fluid d-block mx-auto">
                     <div class="f-18 f-md-20 w400 lh140 white-clr">
                        Jobs and Employment
                     </div>
                  </div>
               </div>
               <div class="col text-center">
                  <div class="img-block">
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/b8.webp" class="img-fluid d-block mx-auto">
                     <div class="f-18 f-md-20 w400 lh140 white-clr">
                        Individual/Self Enhancement
                     </div>
                  </div>
               </div>
               <div class="col text-center">
                  <div class="img-block">
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/b9.webp" class="img-fluid d-block mx-auto">
                     <div class="f-18 f-md-20 w400 lh140 white-clr">
                        Health
                     </div>
                  </div>
               </div>
               <div class="col text-center">
                  <div class="img-block">
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/b10.webp" class="img-fluid d-block mx-auto">
                     <div class="f-18 f-md-20 w400 lh140 white-clr">
                        Sports and Recreation
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="letsee-section">
         <div class="container">
            <div class="letsee-block">
               <div class="row">
                  <div class="col-12 text-center">
                     <div class="f-24 f-md-38 w600 yellow-clr lh140">
                        Let’s see the total value you are getting with MailZilo
                     </div>
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/yellow-line.webp" alt="Line" class="img-fluid mx-auto d-block mt10">
                  </div>
               </div>
               <div class="letsee-price-table mt20 mt-md40">
                  <div class="row">
                     <div class="col-8 col-md-10 text-center">
                        <svg version="1.1" x="0px" y="0px" viewBox="0 0 1501.16 276.9" style="enable-background:new 0 0 1501.16 276.9; max-height:40px;" xml:space="preserve">
                           <style type="text/css">
                              .st0{fill:#FFFFFF;}
                           </style>
                           <g>
                              <path class="st0" d="M122.38,159.77l82.88-67.64h39.5v182.43h-45.44V160.68l-73.53,57.2h-6.82l-73.52-57.2v113.87H0V92.13h39.46
                                 L122.38,159.77z"></path>
                              <path class="st0" d="M391.87,92.13l81.03,182.43h-48.87L409,238.51h-81.7l-13.62,36.04h-48.16l73.69-182.43H391.87z M395.71,206.65
                                 l-29.57-70.91l-26.8,70.91H395.71z"></path>
                              <path class="st0" d="M540.93,92.13v182.43h-45.44V92.13H540.93z"></path>
                              <path class="st0" d="M722.06,241.12v33.43H582.98V92.13h45.44v149H722.06z"></path>
                              <path class="st0" d="M913.75,92.13l-99.48,149h99.48v33.43H736.68l100.26-149H743.6V92.13H913.75z"></path>
                              <path class="st0" d="M989.75,92.13v182.43h-45.44V92.13H989.75z"></path>
                              <path class="st0" d="M1170.88,241.12v33.43H1031.8V92.13h45.44v149H1170.88z"></path>
                              <path class="st0" d="M1332.43,209.97c-2.33,5.08-5.42,9.64-9.28,13.71c-9.7,10.23-22.48,15.35-38.35,15.35
                                 c-15.94,0-28.8-5.12-38.53-15.35c-9.74-10.22-14.61-23.66-14.61-40.28c0-16.72,4.87-30.17,14.61-40.35
                                 c5.49-5.75,11.97-9.87,19.44-12.37l-26.14-31.06c-9.05,4.52-17.19,10.6-24.43,18.22c-17.75,18.72-26.63,40.57-26.63,65.56
                                 c0,25.41,8.94,47.36,26.81,65.81c17.87,18.45,41.03,27.68,69.49,27.68c28.13,0,51.18-9.26,69.13-27.81
                                 c2.07-2.15,4.04-4.34,5.87-6.58L1332.43,209.97z"></path>
                              <path class="st0" d="M1501.16,0l-114.63,241.95l-71.11-84.51l-31.99,29.36l2.17-64.79c67.33-7.59,111.49-65.77,112.17-66.67
                                 c-41.92,31-125.97,49.4-125.97,49.4l-28.6-33.1L1501.16,0z"></path>
                           </g>
                        </svg>
                     </div>
                     <div class="col-4 col-md-2">
                        <div class="f-28 f-md-45 w600 lh140 white-clr">
                           Price
                        </div>
                     </div>
                     <div class="col-12 lets-see-price-table mt20 mt-md30">
                        <div class="f-18 f-md-24 w500 white-clr lh140">
                           Groundbreaking Email Marketing Technology
                        </div>
                        <div class="f-22 f-md-32 w600 lh140 black-clr price-shape">
                           $197
                        </div>
                     </div>
                     <div class="col-12 lets-see-price-table mt20 mt-md30">
                        <div class="f-18 f-md-24 w500 white-clr lh140">
                           100% Cloud-Based Technology
                        </div>
                        <div class="f-22 f-md-32 w600 lh140 black-clr price-shape">
                           $97
                        </div>
                     </div>
                     <div class="col-12 lets-see-price-table mt20 mt-md30">
                        <div class="f-18 f-md-24 w500 white-clr lh140">
                           Unlimited Emails with Free SMTP
                        </div>
                        <div class="f-22 f-md-32 w600 lh140 black-clr price-shape">
                           $97
                        </div>
                     </div>
                     <div class="col-12 lets-see-price-table mt20 mt-md30">
                        <div class="f-18 f-md-24 w500 white-clr lh140">
                           Schedule Emails Anytime with a push of a button
                        </div>
                        <div class="f-22 f-md-32 w600 lh140 black-clr price-shape">
                           $67
                        </div>
                     </div>
                     <div class="col-12 lets-see-price-table mt20 mt-md30">
                        <div class="f-18 f-md-24 w500 white-clr lh140">
                           Smart Tagging & Segmenting
                        </div>
                        <div class="f-22 f-md-32 w600 lh140 black-clr price-shape">
                           $67
                        </div>
                     </div>
                     <div class="col-12 lets-see-price-table mt20 mt-md30">
                        <div class="f-18 f-md-24 w500 white-clr lh140">
                           List Management
                        </div>
                        <div class="f-22 f-md-32 w600 lh140 black-clr price-shape">
                           $47
                        </div>
                     </div>
                     <div class="col-12 lets-see-price-table mt20 mt-md30">
                        <div class="f-20 f-md-24 w500 white-clr lh140">
                           Automation and Autoresponder
                        </div>
                        <div class="f-22 f-md-32 w600 lh140 black-clr price-shape">
                           $97
                        </div>
                     </div>
                     <div class="col-12 lets-see-price-table mt20 mt-md30">
                        <div class="f-18 f-md-24 w500 white-clr lh140">
                           Inbuilt Text & Inline Editor
                        </div>
                        <div class="f-22 f-md-32 w600 lh140 black-clr price-shape">
                           $67
                        </div>
                     </div>
                     <div class="col-12 lets-see-price-table mt20 mt-md30">
                        <div class="f-18 f-md-24 w500 white-clr lh140">
                           Stunning Optin Forms
                        </div>
                        <div class="f-22 f-md-32 w600 lh140 black-clr price-shape">
                           $67
                        </div>
                     </div>
                     <div class="col-12 lets-see-price-table mt20 mt-md30">
                        <div class="f-18 f-md-24 w500 white-clr lh140">
                           Ready-To-Use Templates
                        </div>
                        <div class="f-22 f-md-32 w600 lh140 black-clr price-shape">
                           $97
                        </div>
                     </div>
                     <div class="col-12 lets-see-price-table mt20 mt-md30">
                        <div class="f-18 f-md-24 w500 white-clr lh140">
                           CAN SPAM compliant
                        </div>
                        <div class="f-22 f-md-32 w600 lh140 black-clr price-shape">
                           $47
                        </div>
                     </div>
                     <div class="col-12 lets-see-price-table mt20 mt-md30">
                        <div class="f-18 f-md-24 w500 white-clr lh140">
                           Commercial Use License
                        </div>
                        <div class="f-22 f-md-32 w600 lh140 black-clr price-shape">
                           $67
                        </div>
                     </div>
                     <div class="col-12 lets-see-price-table mt20 mt-md30">
                        <div class="f-18 f-md-24 w500 white-clr lh140">
                           Beginner Friendly
                        </div>
                        <div class="f-22 f-md-32 w600 lh140 black-clr price-shape">
                           Priceless
                        </div>
                     </div>
                     <div class="col-12 lets-see-price-table mt20 mt-md30 border-b">
                        <div class="f-18 f-md-24 w500 white-clr lh140">
                           Fully Automated. 100% Hands-Free
                        </div>
                        <div class="f-22 f-md-32 w600 lh140 black-clr price-shape">
                           Priceless
                        </div>
                     </div>
                     <div class="col-12  mt20 mt-md30 text-center">
                        <div class="f-28 f-md-45 lh140 w500 white-clr">
                           Total value You Get Today: <span class="w700 green-clr1">$947</span> 
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="newbie-headline">
         <div class="container ">
            <div class="row">
               <div class="col-12">
                  <div class="f-28 f-md-45 w700 lh140 text-center white-clr">THE BEST PART - It's Entirely Newbie Friendly</div>
               </div>
            </div>
         </div>
      </div>
      <div class="newbie-section">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-12 col-md-6 f-18 f-md-20 lh160 w400 text-center text-md-start">               
                  If you are newbie & have no or little knowledge of marketing! Then, the trickiest thing is to get 
                  maximum open rates for your mails and make more profits. But not anymore as MailZilo follows
                  a proven formula & works round the clock in the background and automates email marketing 
                  and takes care of your business even while you’re busy with other tasks.
                  <br><br>
                  <span class="w600">And it will do a whole lot of things you haven't even thought were possible yet... 
                  All with ZERO technical skills and with ZERO headaches...</span>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                  <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/newbie.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>
         </div>
      </div>
      <!-- CTA Btn Start -->
      <div class="cta-section-new">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/limited-time-offer-banner.webp" class="img-fluid d-block mx-auto">
                  <div class="f-22 f-md-28 w700 lh140 text-center white-clr mt20">
                     Grab MailZilo Now at 80% Launch Special Discount!
                  </div>
                  <div class="f-18 f-md-22 w500 lh140 text-center yellow-clr mt10">
                     (Get Free 30 Reseller License + Commercial License + Special Bonuses)
                  </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 text-center">
                        <a href="https://warriorplus.com/o2/buy/xgwy6m/y4pjnq/nh90r1" class="cta-link-btn">Get Instant Access To MailZilo</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20">
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/compaitable-with1.webp" class="img-fluid d-block mx-xs-center md-img-right">
                     <div class="d-md-block d-none px-md30"><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/v-line.webp" class="img-fluid"></div>
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/days-gurantee1.webp" class="img-fluid d-block mx-xs-auto mt15 mt-md0">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- CTA Btn End -->

      <div class="huge-potential">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-12">
                  <div class=" f-24 f-md-32 w700 lh140 black-clr text-center">
                     Email Marketing has very huge potential but there are very few people who use it to its full level. And that's because they lack ONE crucial tool. But not anymore, as with Mailzilo you don't have to
                  </div>
                  <div class="mt20 mt-md40">
                     <div class="f-18 f-md-20 w400  lh160">
                        <ul class="noneed-list2 pl0">
                           <li><span class="w600">No more losing</span> 20-30% of your list while importing so don't lose money again & make more profits</li>
                           <li><span class="w600">No more feeling bankrupt</span> paying money month after month to email marketing service providers that cost a fortune</li>
                           <li><span class="w600">No more sitting</span> for long hours & playing the waiting game to get authentic results</li>
                           <li><span class="w600">No more worrying about a 3rd party company crippling</span> your business that can cost you a fortune ever again</li>
                           <li><span class="w600">No more worrying about the privacy</span>  of your list</li>
                           <li> <span class="w600">No more dependency</span> on various trial-and-error methods</li>
                           <li><span class="w600">No more Limits on sending emails and subscribers</span>  so send emails as often as you’d like with NO downtime</li>
                           <li><span class="w600">No more techy stuff, it’s very Easy to setup and use for anyone,</span>  regardless of prior technical experience</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <!---Bonus Section Starts-->
      <div class="bonus-section ">
         <div class="container ">
            <div class="row ">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-45 w700 black-clr shape-headline lh140">
                     We're Not Done Yet!
                  </div>
               </div>
               <!-- <div class="col-12 text-center f-28 f-md-45 w700 lh140 black-clr text-center ">
                  Plus You’ll Also Receive These Amazing Bonuses With Your Purchase Today…
                  </div> -->
               <div class="col-12 mt20 mt-md20 f-20 f-md-22 w400 black-clr lh140 text-center ">
                  When You Grab Your MailZilo Account Today, You’ll Also Get These <span class="w600">Fast Action Bonuses!</span> 
               </div>
               <div class="col-12 ">
                  <div class="row ">
                     <!-------bonus 1------->
                     <div class="col-12 col-md-6 mt-md30 mt20 ">
                        <div class="bonus-section-shape ">
                           <div class="row align-items-center ">
                              <div class="col-md-6 mx-auto">
                                 <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/bonus-box.webp " class="img-fluid d-block mx-auto ">
                              </div>
                              <div class="col-12 mt20 mt-md40 ">
                                 <div class="w600 f-22 f-md-24 black-clr lh140 ">
                                 Email Marketing DFY Business (with PLR Rights) 
                                 </div>
                                 <div class="f-18 f-md-18 w400 black-clr lh140 mt15 ">
                                 Both online and offline marketers can make a killing using this up-to-date Email Marketing DFY Business. Our step-by-step Email Marketing exclusive training is going to take you and your customers by the hand and show you how to get some amazing marketing results in the shortest time ever.
                                 </div>
                                 <div class="f-18 f-md-18 w400 black-clr lh140 mt20">
                                       <ul class="revo-list1 pl0">
                                          <li>Top quality training to sell under your name!</li>
                                          <li>Hottest & proven-seller topic on the web!</li>
                                          <li>Ready-to-go sales material to start selling today!</li>
                                          <li>Sell unlimited copies!</li>
                                       </ul>
                                    </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-------bonus 2------->
                     <div class="col-12 col-md-6 mt-md30 mt20 ">
                        <div class="bonus-section-shape col-12 ">
                           <div class="row align-items-center ">
                              <div class="col-md-6 mx-auto col-12">
                                 <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/bonus-box.webp " class="img-fluid d-block mx-auto ">
                              </div>
                              <div class="col-12 mt20 mt-md40 ">
                                 <div class="w600 f-22 f-md-24 black-clr lh140 ">
                                 Progressive List Building DFY Business (with PLR Rights) 
                                 </div>
                                 <div class="f-18 f-md-18 w400 black-clr lh140 mt15 ">
                                 Both online and offline marketers can make a killing using this up-to-date Progressive List Building DFY Business training. Our step-by-step Progressive List Building DFY Business exclusive training is going to take you and your customers by the hand and show you how to get some amazing marketing results in the shortest time ever. 
                                 </div>
                                 <div class="f-18 f-md-18 w400 black-clr lh140 mt20">
                                       <ul class="revo-list1 pl0">
                                          <li>Top quality training to sell under your name!</li>
                                          <li>Hottest & proven-seller topic on the web!</li>
                                          <li>Ready-to-go sales material to start selling today!</li>
                                          <li>Sell unlimited copies!</li>
                                       </ul>
                                    </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt-md30 mt20 ">
                  <div class="row ">
                     <!-------bonus 3------->
                     <div class="col-12 col-md-6 ">
                        <div class="bonus-section-shape col-12 ">
                           <div class="row align-items-center ">
                              <div class="col-md-6 mx-auto">
                                 <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/bonus-box.webp " class="img-fluid d-block mx-auto ">
                              </div>
                              <div class="col-12 mt20 mt-md50">
                                 <div class="w600 f-22 f-md-24 black-clr lh140">Affiliate Marketing Made Easy 
                                 </div>
                                 <div class="f-18 f-md-18 w400 black-clr lh140 mt15 ">
                                    
Marketers do not want to waste their money, time and effort just for not doing, and even worse, for not knowing how to do Affiliate Marketing!
<br><br>

Our step-by-step Affiliate Marketing Training System is going to take you and your customers by the hand and show you how to make as much money as possible, in the shortest time ever with Affiliate Marketing online. 
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-------bonus 4------->
                     <div class="col-12 col-md-6 mt20 mt-md0 ">
                        <div class="bonus-section-shape col-12 ">
                           <div class="row align-items-center ">
                              <div class="col-md-6 mx-auto">
                                 <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/bonus-box.webp " class="img-fluid d-block mx-auto ">
                              </div>
                              <div class="col-12 mt20 mt-md40">
                                 <div class="w600 f-22 f-md-24 black-clr lh140 ">
                                 Auto Video Creator - Create your own professional videos! 
                                 </div>
                                 <div class="f-18 f-md-18 w400 black-clr lh140 mt15 ">
                                 Uncover the secrets to create your own professional videos in minutes with this easy to use software. You don't even have to speak ... the software will do it for you!<br>
Auto Video Creator - Create your own professional videos in a snap!
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt30 mt-md70 ">
                  <div class="f-24 f-md-38 w700 black-clr lh140 text-center ">
                     That’s A Total Value of <br class="visible-lg "><span class="f-30 f-md-50">$3300</span>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--Bonus Section ends -->
      <!-- License Section Start -->
      <div class="license-section">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-12 col-md-6">
                  <div class="f-28 f-md-45 w700 lh140 black-clr text-center text-md-start">
                     Also, Get Free Commercial License 
                  </div>
                  <div class="f-md-20 f-18 w400 lh140 black-clr mt20 text-center text-md-start">
                     MailZilo comes with Commercial use license so that you can use 
                     MailZilo for commercial purposes and sell copies to your clients.
                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                  <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/commercial-license.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>
         </div>
      </div>
      <!-- Guarantee Section Start -->
      <div class="riskfree-section ">
         <div class="container ">
            <div class="row align-items-center ">
               <div class="f-28 f-md-45 w400 lh140 white-clr text-center col-12 mb-md40">
                  We’re Backing Everything Up with An Iron Clad... <br class="d-none d-md-block"><span class="w600 yellow-clr">"30-Day Risk-Free Money Back Guarantee"</span>
               </div>
               <div class="col-md-7 col-12 order-md-2">
                  <div class="f-md-20 f-18 w400 lh140 white-clr mt15 ">
                     I'm 100% confident that MailZilo will help you take your online business to the next level. I'm so confident on it that I'm making this offer a risk-free investment for you.
                     <br><br>
                     If you concluded that, HONESTLY nothing of this has helped you in any way, you can take advantage of our "30-day Money Back Guarantee" and simply ask for a refund within 30 days!
                     <br><br>    
                     Note: For refund, we will require a valid reason along with the proof that you tried our system, but it didn't work for you!
                     <br><br>
                     I am considering your money to be kept safe on the table between us and waiting for you to APPLY and successfully use this product, and get best results, so then you can feel it is a great investment.
                  </div>
               </div>
               <div class="col-md-5 order-md-1 col-12 mt20 mt-md0 ">
                  <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/riskfree-img.webp " class="img-fluid d-block mx-auto ">
               </div>
            </div>
         </div>
      </div>
      <!-- Guarantee Section End -->
      <!-- Discounted Price Section Start -->
      <div class="table-section" id="buynow">
         <div class="container">
            <div class="row ">
               <div class="col-12 ">
                  <div class="f-20 f-md-24 w400 text-center lh140">
                     Take Advantage of Our Limited Time Deal
                  </div>
                  <div class="f-md-45 f-28 w600 lh140 text-center mt10 ">
                     Get MailZilo for A Low One-Time-
                     <br class="d-none d-md-block "> Price, No Monthly Fee.
                  </div>
               </div>
               <div class="col-12 col-md-8 mx-auto mt20 mt-md50 ">
                  <div class="row ">
                     <div class="col-12 col-md-10 mx-auto mt20 mt-md0">
                        <div class="table-wrap1 ">
                           <div class="table-head1 ">
                           </div>
                           <div class=" ">
                              <ul class="table-list1 pl0 f-18 f-md-20 w500 text-center lh140 black-clr mt15">
                                 <li>Groundbreaking Email Marketing Technology</li>
                                 <li>100% Cloud-Based Technology</li>
                                 <li>Unlimited Emails with Free SMTP</li>
                                 <li>Schedule Emails Anytime with a push of a button</li>
                                 <li>Smart Tagging & Segmenting</li>
                                 <li>List Management</li>
                                 <li>Automation and Autoresponder</li>
                                 <li>Inbuilt Text & Inline Editor </li>
                                 <li>Stunning Optin Forms</li>
                                 <li>Ready-To-Use Templates</li>
                                 <li>CAN SPAM compliant </li>
                                 <li>Commercial Use License </li>
                                 <li>Beginner Friendly</li>
                                 <li>Fully Automated. 100% Hands-Free</li>
                                 <li class="headline my15">Fast Action Bonuses!</li>
                                 <li>Fast Action Bonus #1 – Email Marketing DFY Business (with PLR Rights)</li>
                                 <li>Fast Action Bonus #2 – Progressive List Building DFY Business (with PLR Rights)</li>
                                 <li>Fast Action Bonus #3 – Affiliate Marketing Made Easy</li>
                                 <li>Fast Action Bonus #4 – Auto Video Creator - Create your own professional videos!</li>
                              </ul>
                           </div>
                           <div class="table-btn ">
                              <div class="hideme-button">
                                 <a href="https://warriorplus.com/o2/buy/xgwy6m/y4pjnq/nh90r1"><img src="https://warriorplus.com/o2/btn/fn200011000/xgwy6m/y4pjnq/320688" class="img-fluid d-block mx-auto"></a>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt30 ">
                  <div class="f-md-22 f-20 w400 lh140 text-center ">
                     *Commercial License Is ONLY Available With The Purchase Of Commercial Plan
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Discounted Price Section End -->
      <!-- To your awesome section -->
      <div class="awesome-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-18 f-md-18 w400 lh160 black-clr text-xs-center">
                     <span class="w600">You can agree that the price we're asking is extremely low.</span> The price is rising with every few hours, so it won't be long until it's more than double what it is today!<br><br> We could easily charge hundreds of dollars for a revolutionary tool like this, but we want to offer you an attractive and affordable price that will finally <span class="w600">help you build beautiful sites ready-to-sell - without wasting tons of money!
                     </span> <br><br> <span class="w600">So, take action now... and I promise you won't be disappointed! </span>
                  </div>
               </div>
               <div class="col-12 mt40 mt-md60">
                  <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/limited-time-offer-banner.webp" class="img-fluid d-block mx-auto">
                  <div class="f-22 f-md-28 w700 lh140 text-center black-clr mt20">
                     Grab MailZilo Now at 80% Launch Special Discount!
                  </div>
                  <div class="f-18 f-md-22 w500 lh140 text-center black-clr mt10">
                     (Get Free 30 Reseller License + Commercial License + Special Bonuses)
                  </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 text-center">
                        <a href="https://warriorplus.com/o2/buy/xgwy6m/y4pjnq/nh90r1" class="cta-link-btn">Get Instant Access To MailZilo</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20">
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/compaitable-with.webp" class="img-fluid d-block mx-xs-center md-img-right">
                     <div class="d-md-block d-none px-md30"><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/v-line.webp" class="img-fluid"></div>
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/days-gurantee.webp" class="img-fluid d-block mx-xs-auto mt15 mt-md0">
                  </div>
               </div>
               <div class="col-12 w600 f-md-28 f-22 text-start mt20 mt-md100">To your success</div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
                  <div class="row">
                     <div class="col-md-6 col-12 text-center">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/ayush-sir.webp" class="img-fluid d-block mx-auto " alt="Ayush Jain">
                        <div class="f-24 f-md-28 w600 lh140 black-clr mt20">
                           Ayush Jain
                        </div>
                        <div class="f-16 lh140 w500 black-clr mt10">
                           (Entrepreneur &amp; Internet Marketer)
                        </div>
                     </div>
                     <div class="col-md-6 col-12 text-center mt20 mt-md0">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/pranshu-sir.webp" class="img-fluid d-block mx-auto " alt="Pranshu Gupta">
                        <div class="f-24 f-md-28 w600 lh140 text-center black-clr mt20">
                           Pranshu Gupta
                        </div>
                        <div class="f-16 lh140 w500 text-center black-clr mt10">
                           (Internet Marketer &amp; Product Creator)
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50">
                  <div class="f-18 f-md-18 w400 lh160 black-clr">
                     <span class="w600">P.S- You can try "MailZilo" for 30 days without any worries.</span><br><br> Listen, we know there are a lot of crappy software tools out there that will get you nowhere. Most of the software is overpriced and an
                     absolute waste of money. So, if you're a bit sceptical, that's perfectly fine. I'm so sure you'll see the potential of this ground-breaking software that I'll let you try it out 100% risk-free.
                     <br><br> Just test it for 30 days and if you're not able to build jaw-dropping websites and we cannot help you in any way, you will be eligible for a refund - no tricks, no hassles.
                     <br><br>
                     <span class="w600">P.S.S Don't Procrastinate - Get your copy of MailZilo! </span><br><br> By now you should be really excited about all the wonderful benefits of such an amazing piece of software. You don't want to
                     miss out on the wonderful opportunity presented today... And then regret later when it costs more than double... or it's even completely off the market!
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- To your awesome section End -->
      <!-- FAQ Section Start -->
      <div class="faq-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-45 f-28 w600 lh150 text-center black-clr">
                  Frequently Asked <span class="orange-clr">Questions</span>
               </div>
               <div class="col-12 mt20 mt-md30">
                  <div class="row">
                     <div class="col-md-6 col-12">
                        <div class="faq-list">
                           <div class="f-md-22 f-20 w600 black-clr lh150">
                              What exactly MailZilo is all about? 
                           </div>
                           <div class="f-18 w400 lh150 mt10 text-start">
                              MailZilo is the ultimate push-button software that creates self-updating websites packed with awesome &amp; TRENDY content, drives FREE viral traffic &amp; makes handsfree commissions, ad profits &amp; sales. 
                           </div>
                        </div>
                        <div class="faq-list">
                           <div class="f-md-22 f-20 w600 black-clr lh150">
                              Is my investment risk free?
                           </div>
                           <div class="f-18 w400 lh150 mt10 text-start">
                              MailZilo is the ultimate push-button software that creates self-updating websites packed with awesome &amp; TRENDY content, drives FREE viral traffic &amp; makes handsfree commissions, ad profits &amp; sales. 
                           </div>
                        </div>
                        <div class="faq-list">
                           <div class="f-md-22 f-20 w600 black-clr lh150">
                              Do I have to install MailZilo?
                           </div>
                           <div class="f-18 w400 lh150 mt10 text-start">
                              NO! MailZilo is fully cloud based. Just create an account and you can get started immediately online. It is 100% web-based platform hosted on the cloud. This means you never have to download anything ever. You can access it at any time from any device that
                              has an internet connection.
                           </div>
                        </div>
                        <div class="faq-list">
                           <div class="f-md-22 f-20 w600 black-clr lh150">
                              Is it ‘Newbie Friendly’?
                           </div>
                           <div class="f-18 w400 lh150 mt10 text-start">
                              Yep, my friend, MailZilo is 100% newbie friendly. We know that there are a lot of technical hassles that most software has, but our software is a cut above the rest, and everyone can use it with complete ease.
                           </div>
                        </div>
                        <div class="faq-list">
                           <div class="f-md-22 f-20 w600 black-clr lh150">
                              Do you charge any monthly fees?
                           </div>
                           <div class="f-18 w400 lh150 mt10 text-start">
                              NOT AT ALL. There are NO monthly fees to use MailZilo during the launch period. During this period, you pay once and never again. We always believe in providing complete value for your money.
                           </div>
                        </div>
                     </div>
                     <div class="col-md-6 col-12">
                        <div class="faq-list">
                           <div class="f-md-22 f-20 w600 black-clr lh150">
                              Is MailZilo easy to use?
                           </div>
                           <div class="f-18 w400 lh150 mt10 text-start">
                              I bet you it’s the easiest tool you might have seen so far. Our #1 priority during the development of the software was to make it simple and easy for everyone. There is nothing to install, just create your account and login to take a plunge into hugely
                              profitable video marketing arena.
                           </div>
                        </div>
                        <div class="faq-list">
                           <div class="f-md-22 f-20 w600 black-clr lh150">
                              How do I know how well my campaigns are working?
                           </div>
                           <div class="f-18 w400 lh150 mt10 text-start">
                              You can see real-time reports for your campaigns in your account dashboard and make changes accordingly. But there’s a small catch, you need to upgrade your purchase to use these benefits.
                           </div>
                        </div>
                        <div class="faq-list">
                           <div class="f-md-22 f-20 w600 black-clr lh150">
                              Will I able to drive hordes of viral traffic from day one?
                           </div>
                           <div class="f-18 w400 lh150 mt10 text-start">
                              Well, that depends on how well you make the use of this ultimate software. We’ve created this from grounds up to make everything simple and easy and ensure that you move ahead without any hassles.
                           </div>
                        </div>
                        <div class="faq-list">
                           <div class="f-md-22 f-20 w600 black-clr lh150">
                              Will I get any training or support?
                           </div>
                           <div class="f-18 w400 lh150 mt10 text-start">
                              YES. We made detailed and step-by-step training videos that show you every step of how to get setup and you can access them in the member’s area.
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50">
                  <div class="f-22 f-md-26 w600 black-clr px0 text-xs-center lh150">If you have any query, simply reach to us at <a href="mailto:support@bizomart.com" class="blue-clr">support@bizomart.com</a></div>
               </div>
            </div>
         </div>
      </div>
      <!-- FAQ Section End -->
      <!--final section-->
      <div class="final-section ">
         <div class="container ">
            <div class="row">
               <div class="col-12">
                  <div class="final-block">
                     <div class="text-center">
                        <div class="f-45 f-md-45 lh140 w700 text-center black-clr red-shape2">
                           FINAL CALL
                        </div>
                     </div>
                     <div class="col-12 f-22 f-md-28 lh140 w500 text-center white-clr mt20 p0">
                        You Still Have the Opportunity To Raise Your Game… <br class="d-none d-md-block">
                        Remember, It’s Your Make-or-Break Time!
                     </div>
                     <!-- CTA Btn Section Start -->
                     <div class="col-12 mt40 mt-md40">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/limited-time-offer-banner.webp" class="img-fluid d-block mx-auto">
                        <div class="f-22 f-md-28 w700 lh140 text-center white-clr mt20">
                           Grab MailZilo Now at 80% Launch Special Discount!
                        </div>
                        <div class="f-18 f-md-22 w500 lh140 text-center yellow-clr mt10">
                           (Get Free 30 Reseller License + Commercial License + Special Bonuses)
                        </div>
                        <div class="row">
                           <div class="col-md-10 mx-auto col-12 mt20 text-center">
                              <a href="https://warriorplus.com/o2/buy/xgwy6m/y4pjnq/nh90r1" class="cta-link-btn">Get Instant Access To MailZilo</a>
                           </div>
                        </div>
                        <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20">
                           <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/compaitable-with1.webp" class="img-fluid d-block mx-xs-center md-img-right">
                           <div class="d-md-block d-none px-md30"><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/v-line.webp" class="img-fluid"></div>
                           <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/days-gurantee1.webp" class="img-fluid d-block mx-xs-auto mt15 mt-md0">
                        </div>
                     </div>
                     <!-- CTA Btn Section End -->
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--final section-->
      <!--Footer Section Start -->
      <div class="footer-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <svg version="1.1" x="0px" y="0px" viewBox="0 0 1501.16 276.9" style="enable-background:new 0 0 1501.16 276.9; max-height:40px;" xml:space="preserve">
                     <style type="text/css">
                        .st0{fill:#FFFFFF;}
                     </style>
                     <g>
                        <path class="st0" d="M122.38,159.77l82.88-67.64h39.5v182.43h-45.44V160.68l-73.53,57.2h-6.82l-73.52-57.2v113.87H0V92.13h39.46
                           L122.38,159.77z"></path>
                        <path class="st0" d="M391.87,92.13l81.03,182.43h-48.87L409,238.51h-81.7l-13.62,36.04h-48.16l73.69-182.43H391.87z M395.71,206.65
                           l-29.57-70.91l-26.8,70.91H395.71z"></path>
                        <path class="st0" d="M540.93,92.13v182.43h-45.44V92.13H540.93z"></path>
                        <path class="st0" d="M722.06,241.12v33.43H582.98V92.13h45.44v149H722.06z"></path>
                        <path class="st0" d="M913.75,92.13l-99.48,149h99.48v33.43H736.68l100.26-149H743.6V92.13H913.75z"></path>
                        <path class="st0" d="M989.75,92.13v182.43h-45.44V92.13H989.75z"></path>
                        <path class="st0" d="M1170.88,241.12v33.43H1031.8V92.13h45.44v149H1170.88z"></path>
                        <path class="st0" d="M1332.43,209.97c-2.33,5.08-5.42,9.64-9.28,13.71c-9.7,10.23-22.48,15.35-38.35,15.35
                           c-15.94,0-28.8-5.12-38.53-15.35c-9.74-10.22-14.61-23.66-14.61-40.28c0-16.72,4.87-30.17,14.61-40.35
                           c5.49-5.75,11.97-9.87,19.44-12.37l-26.14-31.06c-9.05,4.52-17.19,10.6-24.43,18.22c-17.75,18.72-26.63,40.57-26.63,65.56
                           c0,25.41,8.94,47.36,26.81,65.81c17.87,18.45,41.03,27.68,69.49,27.68c28.13,0,51.18-9.26,69.13-27.81
                           c2.07-2.15,4.04-4.34,5.87-6.58L1332.43,209.97z"></path>
                        <path class="st0" d="M1501.16,0l-114.63,241.95l-71.11-84.51l-31.99,29.36l2.17-64.79c67.33-7.59,111.49-65.77,112.17-66.67
                           c-41.92,31-125.97,49.4-125.97,49.4l-28.6-33.1L1501.16,0z"></path>
                     </g>
                  </svg>
                  <div class="f-14 f-md-16 w400 mt20 lh150 white-clr text-start" style="color:#7d8398;"><span class="w600">Note:</span> This site is not a part of the Facebook website or Facebook Inc. Additionally, this site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc.<br><br>
                     <span class="w600">Earning Disclaimer:</span> Please make your purchase decision wisely. There is no promise or representation that you will make a certain amount of money, or any money, or not lose money, as a result of using
                     our products and services. Any stats, conversion, earnings, or income statements are strictly estimates. There is no guarantee that you will make these levels for yourself. As with any business, your results will vary and will
                     be based on your personal abilities, experience, knowledge, capabilities, level of desire, and an infinite number of variables beyond our control, including variables we or you have not anticipated. There are no guarantees concerning
                     the level of success you may experience. Each person’s results will vary. There are unknown risks in any business, particularly with the Internet where advances and changes can happen quickly. The use of the plug-in and the given
                     information should be based on your own due diligence, and you agree that we are not liable for your success or failure.
                  </div>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-md-16 f-16 w400 lh140 white-clr text-xs-center">Copyright © MailZilo 2022</div>
                  <ul class="footer-ul w400 f-md-16 f-16 white-clr text-center text-md-right">
                     <li><a href="https://support.bizomart.com/hc/en-us " class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://mailzilo.com/legal/privacy-policy.html " class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://mailzilo.com/legal/terms-of-service.html " class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://mailzilo.com/legal/disclaimer.html " class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://mailzilo.com/legal/gdpr.html " class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://mailzilo.com/legal/dmca.html " class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://mailzilo.com/legal/anti-spam.html " class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!--Footer Section End -->
      <!-- Exit Popup and Timer -->
      <?php include('timer.php'); ?>
      <!-- Exit Popup and Timer End -->
   </body>
</html>
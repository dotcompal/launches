<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <!-- Tell the browser to be responsive to screen width -->
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
      <meta name="title" content="MailZilo Bonuses">
      <meta name="description" content="MailZilo Bonuses">
      <meta name="keywords" content="Revealing: Breath-Taking A.I. Technology That Creates Stunning Content for Any Local or Online Niche At Low One-Time Fee">
      <meta property="og:image" content="https://www.mailzilo.com/special-bonus/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Atul">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="MailZilo Bonuses">
      <meta property="og:description" content="Revealing: Breath-Taking A.I. Technology That Creates Stunning Content for Any Local or Online Niche At Low One-Time Fee">
      <meta property="og:image" content="https://www.mailzilo.com/special-bonus/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="MailZilo Bonuses">
      <meta property="twitter:description" content="Revealing: Breath-Taking A.I. Technology That Creates Stunning Content for Any Local or Online Niche At Low One-Time Fee">
      <meta property="twitter:image" content="https://www.mailzilo.com/special-bonus/thumbnail.png">
      <title>MailZilo Bonuses</title>
      <!-- Shortcut Icon  -->
      <link rel="icon" href="https://cdn.oppyo.com/launches/mailzilo/common_assets/images/favicon.png" type="image/png">
      <!-- Css CDN Load Link -->
      <link rel="stylesheet" href="https://cdn.oppyo.com/launches/writerarc/common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" type="text/css" href="assets/css/bonus-style.css">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
      <script src="https://cdn.oppyo.com/launches/writerarc/common_assets/js/jquery.min.js"></script>
   </head>
   <body>
   
      <!-- New Timer  Start-->
      <?php
         $date = 'September 23 2022 11:59 AM EST';
         $exp_date = strtotime($date);
         $now = time();  
         /*
         
         $date = date('F d Y g:i:s A eO');
         $rand_time_add = rand(700, 1200);
         $exp_date = strtotime($date) + $rand_time_add;
         $now = time();*/
         
         if ($now < $exp_date) {
         ?>
      <?php
         } else {
         	echo "Times Up";
         }
         ?>
      <!-- New Timer End -->
      <?php
         if(!isset($_GET['afflink'])){
         $_GET['afflink'] = 'https://warriorplus.com/o2/a/gfxj2c/0';
         $_GET['name'] = 'Achal';      
         }
         ?>

            <!-- Header Section Start -->   
            <div class="main-header">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="text-center">
                     <div class="f-md-32 f-20 lh140 w400 white-clr d-block d-md-flex align-items-center justify-content-center">
                        <span class="w600"><?php echo $_GET['name'];?>'s</span> &nbsp;special bonus for &nbsp;
                        <svg version="1.1" x="0px" y="0px" viewBox="0 0 1501.16 276.9" style="enable-background:new 0 0 1501.16 276.9; max-height:40px;" xml:space="preserve">
                           <style type="text/css">
                              .st0{fill:#FFFFFF;}
                           </style>
                           <g>
                              <path class="st0" d="M122.38,159.77l82.88-67.64h39.5v182.43h-45.44V160.68l-73.53,57.2h-6.82l-73.52-57.2v113.87H0V92.13h39.46
                              L122.38,159.77z"></path>
                              <path class="st0" d="M391.87,92.13l81.03,182.43h-48.87L409,238.51h-81.7l-13.62,36.04h-48.16l73.69-182.43H391.87z M395.71,206.65
                              l-29.57-70.91l-26.8,70.91H395.71z"></path>
                              <path class="st0" d="M540.93,92.13v182.43h-45.44V92.13H540.93z"></path>
                              <path class="st0" d="M722.06,241.12v33.43H582.98V92.13h45.44v149H722.06z"></path>
                              <path class="st0" d="M913.75,92.13l-99.48,149h99.48v33.43H736.68l100.26-149H743.6V92.13H913.75z"></path>
                              <path class="st0" d="M989.75,92.13v182.43h-45.44V92.13H989.75z"></path>
                              <path class="st0" d="M1170.88,241.12v33.43H1031.8V92.13h45.44v149H1170.88z"></path>
                              <path class="st0" d="M1332.43,209.97c-2.33,5.08-5.42,9.64-9.28,13.71c-9.7,10.23-22.48,15.35-38.35,15.35
                              c-15.94,0-28.8-5.12-38.53-15.35c-9.74-10.22-14.61-23.66-14.61-40.28c0-16.72,4.87-30.17,14.61-40.35
                              c5.49-5.75,11.97-9.87,19.44-12.37l-26.14-31.06c-9.05,4.52-17.19,10.6-24.43,18.22c-17.75,18.72-26.63,40.57-26.63,65.56
                              c0,25.41,8.94,47.36,26.81,65.81c17.87,18.45,41.03,27.68,69.49,27.68c28.13,0,51.18-9.26,69.13-27.81
                              c2.07-2.15,4.04-4.34,5.87-6.58L1332.43,209.97z"></path>
                              <path class="st0" d="M1501.16,0l-114.63,241.95l-71.11-84.51l-31.99,29.36l2.17-64.79c67.33-7.59,111.49-65.77,112.17-66.67
                              c-41.92,31-125.97,49.4-125.97,49.4l-28.6-33.1L1501.16,0z"></path>
                           </g>
                        </svg>
                     </div>
                  </div>
                  <div class="preheadline f-md-20 f-18 w600 lh150 mt20 mt-md40">
						   <div class="skew-con d-flex gap20 align-items-center ">
                        <span>Grab My 20 Exclusive Bonuses Before the Deal Ends…</span>
						      <div class="d-none d-md-block">
							      <img src="assets/images/fav.webp" class="img-fluid d-block">
                        </div>
						   </div>
                  </div>
                  <div class="col-12 mt-md30 mt20 f-md-42 f-28 w600 text-center white-clr lh140">
                     <span class="cyan-clr"> Revealing: </span> Advanced Tag & Segment Based Email Software <span class="yellow-clr"> That Gets 4X More Opening & Click </span> At Low One-Time Fee
                  </div>
                  <div class="col-12 mt-md30 mt20  text-center">
                     <div class="f-18 f-md-22 w600 text-center lh150 white-clr">
                        Watch My Quick Review Video
                     </div>
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md30">
               <div class="col-md-10 col-12 mx-auto">
                  <div class="col-12 responsive-video border-video">
                     <iframe src="https://mailzilo.dotcompal.com/video/embed/fy9w5ktacr" style="width:100%; height:100%" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                  </div> 
                  <!-- <img src="assets/images/productbox.webp" class="img-fluid d-block mx-auto">-->
               </div>
            </div>
         </div>
      </div>
      <!-- Header Section End -->

      <!-- header List Section Start -->
      <div class="header-list-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row header-list-block">
                     <div class="col-12 col-md-6">
                        <div class="f-16 f-md-18 lh140 w400">
                           <ul class="bonus-list pl0 m0">
                              <li><span class="w600">Send Unlimited Emails</span> to Unlimited Subscribers</li>
                              <li><span class="w600">Collect Unlimited Leads</span> with Built-In Lead Form</li>
                              <li> <span class="w600">Free SMTP</span> for unrestricted Mailing</li>
                              <li><span class="w600">Upload Unlimited List</span> with Zero Restrictions</li>
                              <li><span class="w600">Get 4X More ROI</span> and Profits than Ever</li>
                              <li><span class="w600">Smart Tagging</span> for Lead Personalisation</li>
                              <li><span class="w600">100% Control</span> on your online business </li>
                           </ul>
                        </div>
                     </div>
                     <div class="col-12 col-md-6">
                        <div class="f-16 f-md-18 lh140 w400">
                           <ul class="bonus-list pl0 m0">
                              <li><span class="w600">Built-in Inline</span> Editor to Craft Beautiful Emails</li>
                              <li><span class="w600">Hassle-Free</span> Subscribers Management</li>
                              <li><span class="w600">100+ High Converting</span> Templates for Webforms & Email</li>
                              <li><span class="w600">GDPR & Can-Spam Compliant</span></li>
                              <li><span class="w600">100% Newbie Friendly</span> & Easy to Use</li>
                              <li><span class="w600">Commercial License Included</span></li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- header List Section End -->

      <!-- Step Section Start -->
      <div class="step-section">
         <div class="container ">
            <div class="row ">
               <div class="col-12 f-28 f-md-45 w600 lh140 black-clr text-center">
                  Replace your old-school email marketing <br class="d-none d-md-block"> techniques with this cutting-edge solution that<br class="d-none d-md-block"> powers your business <span class="orange-clr d-inline-grid">in just 3 easy steps… <img src="assets/images/underline.webp " class="img-fluid"></span>
               </div>
               <div class="col-12 mt30 mt-md80">
                  <div class="row align-items-center">
                     <div class="col-12 col-md-6 relative">
                        <img src="assets/images/step1.webp" class="img-fluid">
                        <div class="f-28 f-md-40 w600 lh140 mt15">
                           Upload
                        </div>
                        <div class="f-18 f-md-20 w400 lh140 mt15">
                           To begin with, all you need to do is upload your subscribers 
                           or generate leads using MailZilo opt-in forms.   
                        </div>
                        <img src="assets/images/left-arrow.webp" class="img-fluid d-none d-md-block ms-auto step-arrow1">
                     </div>
                     <div class="col-12 col-md-6 mt20 mt-md0 ">
                        <img src="assets/images/step-one.webp" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
               <div class="col-12 mt30 mt-md80">
                  <div class="row align-items-center ">
                     <div class="col-12 col-md-6 order-md-2 relative">
                        <img src="assets/images/step2.webp" class="img-fluid">
                        <div class="f-28 f-md-40 w600 lh140 mt15">
                           Select or Create
                        </div>
                        <div class="f-18 f-md-20 w400 lh140 mt15">
                           Select from the list of beautifully designed email templates 
                           or create your own email template.   
                        </div>
                        <img src="assets/images/right-arrow.webp" class="img-fluid d-none d-md-block step-arrow2">
                     </div>
                     <div class="col-md-6 col-md-pull-6 mt20 mt-md0 order-md-1 ">
                        <img src="assets/images/step-two.webp " class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
               <div class="col-12 mt30 mt-md80">
                  <div class="row align-items-center">
                     <div class="col-12 col-md-6">
                        <img src="assets/images/step3.webp" class="img-fluid">
                        <div class="f-28 f-md-40 w600 lh140 mt15">
                           Send & Profit
                        </div>
                        <div class="f-18 f-md-20 w400 lh140 mt15">
                           Send Unlimited Email to your Subscriber or Schedule it for later.
                        </div>
                     </div>
                     <div class="col-12 col-md-6 mt20 mt-md0 ">
                        <img src="assets/images/step-three.webp " class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md60 text-center">
                  <div class="d-flex gap-2 gap-md-4 justify-content-center flex-wrap">
                     <div class="d-flex align-items-center gap-3 justify-content-center">
                        <img src="https://cdn.oppyo.com/launches/writerarc/bizdrive/cross.webp" alt="Cross" class="img-fluid d-block">
                        <div class="f-md-28 w700 lh140 f-20 red-clr"> No SMTP</div>
                     </div>
                     <div class="d-flex align-items-center gap-3 justify-content-center">
                        <img src="https://cdn.oppyo.com/launches/writerarc/bizdrive/cross.webp" alt="Cross" class="img-fluid d-block">
                        <div class="f-md-28 w700 lh140 f-20 red-clr"> No Installation</div>
                     </div>
                     <div class="d-flex align-items-center gap-3 justify-content-center">
                        <img src="https://cdn.oppyo.com/launches/writerarc/bizdrive/cross.webp" alt="Cross" class="img-fluid d-block">
                        <div class="f-md-28 w700 lh140 f-20 red-clr">No Other Techie Stuff</div>
                     </div>
                  </div>
                  <div class="f-20 f-md-28 w500 lh140 mt20">It's EASY, FAST & Built with LOVE.</div>
               </div>
            </div>
         </div>
      </div>
      <!-- Step Section End -->

      <!-- CTA Btn Start-->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 yellow-clr">"MAILVIP"</span> for an Additional <span class="w700 yellow-clr">$5 Discount</span> on Commercial Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                     <span class="text-center">Grab MailZilo + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                           <br>
                        <span class="f-14 f-md-18 smmltd">Days</span>
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Sec</span>
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Btn End -->

      <div class="proof-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 ">
                  <div class="f-28 f-md-45 w600 lh140 black-clr text-center">
                  Here is What We’ve Made in the Past 3 Months <br class="d-none d-md-block"> 
                  by Using the Exact Same Technology
                  </div>
               </div>
               <div class="col-12 mx-auto mt20 mt-md50">
                  <img src="assets/images/proof.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>
            <div class="row mt-md70 mt30 align-items-center">
               <div class="col-md-6 f-md-32 f-22 w600 text-center text-md-start">
                  Got 20K+ Visitors in Just 15 Days on My Affiliate Offers Using the Power of Emails  
               </div>
               <div class="col-md-6 mt-md0 mt20">
                  <img src="assets/images/proof1.webp" alt="" class="img-fluid d-block mx-auto">
               </div>
            </div>
            <div class="row mt-md70 mt30 align-items-center">
               <div class="col-md-6 f-md-32 f-22 w600 order-md-2 text-center text-md-start">
                  And Let’s Check Out the Crazy Open And Click Rates 
                  We’ve Got for A Simple Email I Sent Using Mailzilo.
               </div>
               <div class="col-md-6 mt-md0 mt20 order-md-1">
                  <img src="assets/images/proof2.webp" alt="" class="img-fluid d-block mx-auto">
               </div>
            </div>
         </div>
      </div>
      <!-- Proof Section End -->  
      <!-- Testimonial Section Start -->
      <div class="testimonial-section">
         <div class="container ">
            <div class="row ">
               <div class="col-12 f-28 f-md-45 w600 lh140 black-clr text-center">
                  Here's What Industry Experts Have to <br class="d-none d-md-block"> Say About MailZilo
               </div>
            </div>
            <div class="row row-cols-md-2 row-cols-1 gx4">
               <div class="col mt50">
                  <div class="single-testimonial">
                     <p>
                     Finally, I am getting my emails delivered, clicked, and opened. Yes,<span class="w600"> MailZilo is one of the BEST tools</span> I’ve used for my business yet! No more dependency on other email software. Stupidly simple to use. MailZilo made me go crazy. <span class="w600">Five Stars from my side for this product… </span>
                     </p>
                     <div class="client-info">
                        <img src="assets/images/amit-gaikwad.webp" class="img-fluid d-block mx-auto">
                        <div class="client-details">
                        Amit Gaikwad
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col mt50">
                  <div class="single-testimonial">
                     <p>
                     BINGO, I've tested MailZilo for myself, and man, <span class="w600">it delivered exactly as promised.</span> I just recently sent mail to my entire list and <span class="w600">got 20% more open rates</span> with this super-amazing Email Marketing Technology. <span class="w600">Two-Thumbs up for this one…</span> 
                     </p>
                     <div class="client-info">
                        <img src="assets/images/anirudh-bharwa.webp" class="img-fluid d-block mx-auto">
                        <div class="client-details">
                        Anirudh Bharwa
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col mt50">
                  <div class="single-testimonial">
                     <p>
                     MailZilo will surely take the industry by storm and will <span class="w600">help marketers to generate more leads</span> from any Blog, eCommerce, or WordPress site. I like its <span class="w600">latest smart tag feature</span> which enables me to build targeted marketing campaigns. Yet another killer product from Team Ayush & Pranshu… 
                     </p>
                     <div class="client-info">
                        <img src="assets/images/akshat-gupta.webp" class="img-fluid d-block mx-auto">
                        <div class="client-details">
                        Akshat Gupta
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col mt50">
                  <div class="single-testimonial">
                     <p>
                     <span class="w600">Importing Lists without losing any Leads? DONE!<br>
Sending UNLIMITED Emails? DONE!<br>
Want to Generate More Leads? DONE!<br>
Automating Email Marketing Campaigns? That’s DONE as well!<br><br></span>
This software really impressed me with its amazing features. A great option for anyone looking to make the most from email marketing…. 

                     </p>
                     <div class="client-info">
                        <img src="assets/images/uddhab-pramanik.webp" class="img-fluid d-block mx-auto">
                        <div class="client-details">
                        Uddhab Pramanik
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Testimonial Section End -->

      <!-- CTA Btn Start -->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 yellow-clr">"MAILVIP"</span> for an Additional <span class="w700 yellow-clr">$5 Discount</span> on Commercial Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                     <span class="text-center">Grab MailZilo + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr"><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">02</span><span class="f-14 f-md-18 smmltd">Days</span> </div><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">05</span><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">14</span><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div><div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">58</span><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div></div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Section End -->

      <!-- Research Section Start -->
      <div class="research-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="reason-box">
                     <div class="f-24 f-md-38 w600 lh140 blue-clr">
                        Email Marketing Yields an
                     </div>
                     <div class="f-28 f-md-45 w700 lh140 black-clr">
                        Average 4300% Return on Investment
                     </div>
                     <div class="f-24 f-md-38 w600 lh140 blue-clr">
                        for businesses worldwide.
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50">
                  <div class="row align-items-center">
                     <div class="col-12 col-md-6">
                        <div class="f-20 f-md-24 w500 lh140 text-center text-md-start">
                           Yes, Emails are the most powerful, profitable, and low-cost marketing tool and get 3 times higher conversions than social media or any other mode of marketing.
                        </div>
                     </div>
                     <div class="col-12 col-md-6 mt20 mt-md0">
                        <img src="assets/images/investment.webp" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md70">
                  <div class="row">
                     <div class="col-12 col-md-7 order-md-2">
                        <div class="f-28 f-md-45 w700 lh140 text-center text-md-start green-clr">
                           That’s Right…
                        </div>
                        <div class="f-20 f-md-24 w500 lh140 text-center text-md-start mt10">
                           Email marketing is the key to getting new customers and creating deeper relationships with your existing customers at a fraction of the cost.
                        </div>
                     </div>
                     <div class="col-12 col-md-5 order-md-1 mt20 mt-md0 relative d-none d-md-block">
                        <img src="assets/images/girl.webp" class="img-fluid d-block mx-auto girl-img">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Research Section End -->

      <!-- Stats Section Start -->
      <div class="stats-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-45 w700 lh140 text-center white-clr stats-headline">Here are Some Eye Openers </div>
               </div>
            </div>
            <div class="row row-cols-1 row-cols-md-2 gap30 mt0 t-md20">
               <div class="col">
                  <div class="stats-block">
                     <div><img src="assets/images/mail.webp" class="img-fluid d-block mx-auto"></div>
                     <div class="f-20 f-md-24 w400 lh140 white-clr">There are <span class="orange-clr w700">4.5+ Billion</span> Daily Email Users</div>
                  </div>
               </div>
               <div class="col">
                  <div class="stats-block">
                     <div><img src="assets/images/mail.webp" class="img-fluid d-block mx-auto"></div>
                     <div class="f-20 f-md-24 w400 lh140 white-clr"><span class="orange-clr w700">350+ Billion Emails</span> are Circulated Every Single Day</div>
                  </div>
               </div>
               <div class="col">
                  <div class="stats-block">
                     <div><img src="assets/images/mail.webp" class="img-fluid d-block mx-auto"></div>
                     <div class="f-20 f-md-24 w400 lh140 white-clr"><span class="orange-clr w700">$400 Million</span> are Spent Every Year on Email Marketing in US Alone</div>
                  </div>
               </div>
               <div class="col">
                  <div class="stats-block">
                     <div><img src="assets/images/mail.webp" class="img-fluid d-block mx-auto"></div>
                     <div class="f-20 f-md-24 w400 lh140 white-clr"><span class="orange-clr w700">95% of Email</span> Users Check Their Emails Daily</div>
                  </div>
               </div>
               <div class="col">
                  <div class="stats-block">
                     <div><img src="assets/images/mail.webp" class="img-fluid d-block mx-auto"></div>
                     <div class="f-20 f-md-24 w400 lh140 white-clr"><span class="orange-clr w700">85% of Businesses</span> Believe that Email Marketing Improves Customer Retention</div>
                  </div>
               </div>
               <div class="col">
                  <div class="stats-block">
                     <div><img src="assets/images/mail.webp" class="img-fluid d-block mx-auto"></div>
                     <div class="f-20 f-md-24 w400 lh140 white-clr"><span class="orange-clr w700">80% of Youth</span> Prefer Business Communications via Email</div>
                  </div>
               </div>
               <div class="col">
                  <div class="stats-block">
                     <div><img src="assets/images/mail.webp" class="img-fluid d-block mx-auto"></div>
                     <div class="f-20 f-md-24 w400 lh140 white-clr"><span class="orange-clr w700">90% of Marketers</span> Use Email as the Primary Channel</div>
                  </div>
               </div>
               <div class="col">
                  <div class="stats-block">
                     <div><img src="assets/images/mail.webp" class="img-fluid d-block mx-auto"></div>
                     <div class="f-20 f-md-24 w400 lh140 white-clr"><span class="orange-clr w700">Since 10+ Years,</span> Email Marketing Generates the Highest ROI for Marketers.</div>
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md80">
               <div class="col-12 f-24 f-md-36 w600 white-clr lh140 text-center">This Show Why Big Businesses <br class="d-none d-md-block">&amp; Marketers are Capitalizing on Email Marketing</div>
            </div>
         </div>
      </div>
      <!-- Stats Section End -->

      <!-- Power Section Start -->
      <div class="power-section">
         <div class="container ">
            <div class="row align-items-center">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-45 w700 lh140 black-clr">
                     And Do You Know How Much People Are Making  <br>
                     <span class="orange-clr">Using the POWER of Emails Marketing?</span>
                  </div>
                  <img src="assets/images/power-line.webp" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 col-md-11 offset-md-1 mt20 mt-md50">
                  <div class="power-block">
                     <div class="f-28 f-md-36 w700 orange-clr lh140 text-center text-md-start">Tom Sather</div>
                     <div class="f-md-20 f-18 w400 lh140 white-clr mt10 text-center text-md-start">
                        He is known for his work as the Director of Return Path which has an Annual Revenue of $10+ Million. Return Path, with emphasis on Emails Marketing, offers unparalleled insights on why emails end up undeliverable and where they end up bouncing.
                     </div>
                     <img src="assets/images/tom.webp" class="img-fluid d-block mx-auto left-img mt20 mt-md0">
                  </div>
               </div>
               <div class="col-12">
                  <img src="assets/images/connect.webp" class="img-fluid d-block mx-auto connect-line">
               </div>
               <div class="col-12 col-md-11 mt20">
                  <div class="power-block right">
                     <div class="f-28 f-md-36 w700 orange-clr lh140 text-center text-md-start">Matthew Lloyd Smith</div>
                     <div class="f-md-20 f-18 w400 lh140 white-clr mt10 text-center text-md-start">Matthew  Lloyd Smith created the insanely helpful website Really Good Emails, which showcases the best in email marketing on the internet. Really Good Emails has a Revenue of over $5 Million.
                     </div>
                     <img src="assets/images/matthew.webp" class="img-fluid d-block mx-auto right-img mt20 mt-md0">
                  </div>
               </div>
               <div class="col-12">
                  <img src="assets/images/connect1.webp" class="img-fluid d-block mx-auto connect-line">
               </div>
               <div class="col-12 col-md-11 offset-md-1 mt20">
                  <div class="power-block">
                     <div class="f-28 f-md-36 w700 orange-clr lh140 text-center text-md-start">Kath Pay</div>
                     <div class="f-md-20 f-18 w400 lh140 white-clr mt10 text-center text-md-start">
                        Kath Pay has been in the email game for over 19 years. She is an email marketing consultant and founder of Holistic Email Marketing. She’s been named one of the top email influencers in the world and makes annual revenue of over $8 Million.
                     </div>
                     <img src="assets/images/kath.webp" class="img-fluid d-block mx-auto left-img mt20 mt-md0">
                  </div>
               </div>
               <div class="col-12 mt20 mt-md100">
                  <div class="f-20 f-md-32 w400 lh140 text-center">And all of that just by using the Power of Email Marketing</div>
                  <div class="f-32 f-md-55 w700 lh140 text-center green-clr my20">Impressive, Right?</div>
                  <div class="f-20 f-md-32 w400 lh140 text-center">So, it can be safely stated that…<br>
                     <span class="w600">Email Marketing is the BIGGEST Income Opportunity Right Now!</span>
                  </div>
               </div>
            </div>  
         </div>
      </div>
      <!-- Power Section End -->

      <!-- Proudly Section Start -->
      <div class="next-gen-sec" id="product">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="pre-heading f-md-32 f-24 w600 text-center white-clr lh140">
                  Proudly Presenting... </div>
               </div>
               <div class="col-12 mt-md20 mt20 text-center">
                  <svg version="1.1" x="0px" y="0px" viewBox="0 0 1501.16 276.9" style="enable-background:new 0 0 1501.16 276.9; max-height:100px;" xml:space="preserve">
                     <style type="text/css">
                     .st0{fill:#FFFFFF;}
                     </style>
                     <g>
                        <path class="st0" d="M122.38,159.77l82.88-67.64h39.5v182.43h-45.44V160.68l-73.53,57.2h-6.82l-73.52-57.2v113.87H0V92.13h39.46
                        L122.38,159.77z"></path>
                        <path class="st0" d="M391.87,92.13l81.03,182.43h-48.87L409,238.51h-81.7l-13.62,36.04h-48.16l73.69-182.43H391.87z M395.71,206.65
                        l-29.57-70.91l-26.8,70.91H395.71z"></path>
                        <path class="st0" d="M540.93,92.13v182.43h-45.44V92.13H540.93z"></path>
                        <path class="st0" d="M722.06,241.12v33.43H582.98V92.13h45.44v149H722.06z"></path>
                        <path class="st0" d="M913.75,92.13l-99.48,149h99.48v33.43H736.68l100.26-149H743.6V92.13H913.75z"></path>
                        <path class="st0" d="M989.75,92.13v182.43h-45.44V92.13H989.75z"></path>
                        <path class="st0" d="M1170.88,241.12v33.43H1031.8V92.13h45.44v149H1170.88z"></path>
                        <path class="st0" d="M1332.43,209.97c-2.33,5.08-5.42,9.64-9.28,13.71c-9.7,10.23-22.48,15.35-38.35,15.35
                        c-15.94,0-28.8-5.12-38.53-15.35c-9.74-10.22-14.61-23.66-14.61-40.28c0-16.72,4.87-30.17,14.61-40.35
                        c5.49-5.75,11.97-9.87,19.44-12.37l-26.14-31.06c-9.05,4.52-17.19,10.6-24.43,18.22c-17.75,18.72-26.63,40.57-26.63,65.56
                        c0,25.41,8.94,47.36,26.81,65.81c17.87,18.45,41.03,27.68,69.49,27.68c28.13,0,51.18-9.26,69.13-27.81
                        c2.07-2.15,4.04-4.34,5.87-6.58L1332.43,209.97z"></path>
                        <path class="st0" d="M1501.16,0l-114.63,241.95l-71.11-84.51l-31.99,29.36l2.17-64.79c67.33-7.59,111.49-65.77,112.17-66.67
                        c-41.92,31-125.97,49.4-125.97,49.4l-28.6-33.1L1501.16,0z"></path>
                     </g>
                  </svg>
               </div>
               <div class="f-md-36 f-24 w600 white-clr lh140 col-12 mt20 mt-md40 text-center">
                  The Most Advanced &amp; Powerful Email Marketing Automation Software that 
                  allows you to Create, Send, Track, and Profit from Email Marketing.
               </div>
               <div class="f-20 f-md-24 lh140 w400 text-center white-clr mt20">              
                  It is a must-have tool in your marketing arsenal that comes with a ONE-TIME FEE, <br class="d-none d-md-block"> and that will send all the existing money-sucking autoresponders back to their nest.
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md50">
               <div class="col-12 col-md-10 mx-auto">
                  <img src="assets/images/product-box.webp" class="img-fluid d-block mx-auto" alt="Product">
               </div>            
            </div>
            <div class="row mt20 mt-md50">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 yellow-clr">"MAILVIP"</span> for an Additional <span class="w700 yellow-clr">$5 Discount</span> on Commercial Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                     <span class="text-center">Grab MailZilo + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr"><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">02</span><span class="f-14 f-md-18 smmltd">Days</span> </div><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">03</span><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">28</span><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div><div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">44</span><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div></div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- Proudly Section End  -->

      <!-------Coursova Starts----------->
      <!-------Exclusive Bonus----------->
      <div class="exclusive-bonus">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                  <div class="white-clr f-24 f-md-36 lh140 w600">
                     Exclusive Bonus #1 : Coursova
                  </div>
                  <div class="f-18 f-md-20 w500 mt20 white-clr">
                     20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
                  </div>
               </div>
            </div>
         </div>
      </div>

      <div class="coursova-bg">
         <div class="container">
            <div class="row inner-content">
               <div class="col-12">
                  <div class="col-12 text-center">
                     <img src="assets/images/logo-co-white.webp" class="img-responsive mx-xs-center logo-height">
                  </div>
               </div>
               <div class="col-12 text-center px-sm15">
                  <div class="mt20 mt-md50 mb20 mb-md50">
                     <div class="f-md-21 f-20 w500 lh140 text-center white-clr">
                        Turn your passion, hobby, or skill into <u>a profitable e-learning business</u> right from your home…
                     </div>
                  </div>
               </div>
               <div class="col-12">
                  <div class="col-md-8 col-sm-3 ml-md70 mt20">
                     <div class="game-changer col-md-12">
                        <div class="f-md-24 f-20 w600 text-center  black-clr lh120">Game-Changer</div>
                     </div>
                  </div>
                  <div class="col-12 heading-co-bg">
                     <div class="f-24 f-md-42 w500 text-center black-clr lh140  line-center rotate-5">								
                        <span class="w700"> Create and Sell Courses on Your Own Branded <br class="d-md-block d-none">E-Learning Marketplace</span>  without Any <br class="visible-lg"> Tech Hassles or Prior Skills
                     </div>
                  </div>
                  <div class="col-12 p0 f-18 f-md-24 w400 text-center  white-clr lh140 mt-md40 mt20">
							Sell your own courses or <u>choose from 10 done-for-you RED-HOT HD <br class="d-md-block d-none">video courses to start earning right away</u>
                  </div>
					</div>
				</div>
            <div class="row align-items-center">
               <div class="col-md-7 col-12 mt-sm100 mt20 px-sm15 min-sm-video-width-left">
                  <div class="col-12 p0 mt-md15">
                     <div class="col-12 responsive-video" editabletype="video" style="z-index: 10;">
                        <iframe src="https://coursova.dotcompal.com/video/embed/n56ppjcyr4" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;box-shadow: none !important;" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                     </div>
                  </div>
               </div>
               <div class="col-md-5 col-12 f-20 f-md-20 worksans lh140 w400 white-clr mt-md37 mt20 pl15 min-sm-video-width-right">
                  <ul class="list-head list-head-co pl0 m0">
                     <li>Tap Into Fast-Growing $398 Billion Industry Today</li>
                     <li>Build Beautiful E-Learning Sites with Marketplace, Blog &amp; Members Area in Minutes</li>
                     <li>Easily Create Courses - Add Unlimited Lessons (Video, E-Book &amp; Reports)</li>
                     <li>Accept Payments with PayPal, JVZoo, ClickBank, W+ Etc.</li>
                     <li>No Leads or Profit Sharing With 3rd Party Marketplaces</li>
                     <li>Get Commercial License and Charge Clients Monthly (Yoga Classes, Gym Etc.)</li>
                  </ul>
               </div>
            </div>
         </div>
      </div>

      <div class="gr-1">
         <div class="container-fluid p0">			
            <picture>
               <img src="assets/images/coursova-gr1.webp" alt="Flowers" style="width:100%;">
            </picture>	
            <picture>
               <img src="assets/images/coursova-gr2.webp" alt="Flowers" style="width:100%;">
            </picture>		
            <picture>
               <img src="assets/images/coursova-gr3.webp" alt="Flowers" style="width:100%;">
            </picture>			
         </div>
		</div>
      <!------Coursova Section ends------>

      <!-------Exclusive Bonus----------->
      <div class="exclusive-bonus">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                  <div class="white-clr f-24 f-md-36 lh140 w600">
                     Exclusive Bonus #2 : Trendio
                  </div>
                  <div class="f-18 f-md-20 w500 mt20 white-clr">
                     20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!------Trendio Section------>

      <div class="tr-header-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row">
                     <div class="col-md-3 text-center mx-auto">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 912.39 246.83" style="enable-background:new 0 0 912.39 246.83; max-height:50px;color: #023047;" xml:space="preserve">
                           <style type="text/css">
                              .st0{fill:#FFFFFF;}
                              .st1{opacity:0.3;}
                              .st2{fill:url(#SVGID_1_);}
                              .st3{fill:url(#SVGID_00000092439463549223389810000006963263784374171300_);}
                              .st4{fill:url(#SVGID_00000015332009897132114970000017423936041397956233_);}
                              .st5{fill:#023047;}
                           </style>
                           <g>
                              <g>
                                 <path class="st0" d="M18.97,211.06L89,141.96l2.57,17.68l10.86,74.64l2.12-1.97l160.18-147.9l5.24-4.84l6.8-6.27l5.24-4.85 l-25.4-27.51l-12.03,11.11l-5.26,4.85l-107.95,99.68l-2.12,1.97l-11.28-77.58l-2.57-17.68L0,177.17c0.31,0.72,0.62,1.44,0.94,2.15 c0.59,1.34,1.2,2.67,1.83,3.99c0.48,1.03,0.98,2.06,1.5,3.09c0.37,0.76,0.76,1.54,1.17,2.31c2.57,5.02,5.43,10,8.57,14.93 c0.58,0.89,1.14,1.76,1.73,2.65L18.97,211.06z"></path>
                              </g>
                              <g>
                                 <g>
                                    <polygon class="st0" points="328.54,0 322.97,17.92 295.28,106.98 279.91,90.33 269.97,79.58 254.51,62.82 244.58,52.05 232.01,38.45 219.28,24.66 "></polygon>
                                 </g>
                              </g>
                           </g>
                           <g class="st1">
                              <g>
                                 <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="17.428" y1="42.82" x2="18.9726" y2="42.82" gradientTransform="matrix(1 0 0 -1 0 252.75)">
                                    <stop offset="0" style="stop-color:#000000"></stop>
                                    <stop offset="1" style="stop-color:#333333"></stop>
                                 </linearGradient>
                                 <path class="st2" d="M17.43,208.8l1.54,2.26c-0.37-0.53-0.73-1.05-1.09-1.58c-0.02-0.02-0.02-0.03-0.03-0.03 c-0.06-0.09-0.11-0.17-0.17-0.27C17.6,209.06,17.51,208.92,17.43,208.8z"></path>
                                 <linearGradient id="SVGID_00000177457867953882822120000005144526232562103955_" gradientUnits="userSpaceOnUse" x1="18.98" y1="70.45" x2="91.57" y2="70.45" gradientTransform="matrix(1 0 0 -1 0 252.75)">
                                    <stop offset="0" style="stop-color:#000000"></stop>
                                    <stop offset="1" style="stop-color:#333333"></stop>
                                 </linearGradient>
                                 <path style="fill:url(#SVGID_00000177457867953882822120000005144526232562103955_);" d="M89,141.96l2.57,17.68l-63.83,63 c-1.19-1.45-2.34-2.9-3.46-4.35c-1.84-2.4-3.6-4.81-5.3-7.22L89,141.96z"></path>
                                 <linearGradient id="SVGID_00000010989203515549635820000018393619450353154703_" gradientUnits="userSpaceOnUse" x1="104.55" y1="120.005" x2="333.22" y2="120.005" gradientTransform="matrix(1 0 0 -1 0 252.75)">
                                    <stop offset="0" style="stop-color:#000000"></stop>
                                    <stop offset="1" style="stop-color:#333333"></stop>
                                 </linearGradient>
                                 <polygon style="fill:url(#SVGID_00000010989203515549635820000018393619450353154703_);" points="333.22,15.61 299.96,122.58 274.65,95.18 107.11,249.88 104.55,232.31 264.73,84.41 269.97,79.58 279.91,90.33 295.28,106.98 322.97,17.92 "></polygon>
                              </g>
                           </g>
                           <g>
                              <g>
                                 <g>
                                    <path class="st5" d="M229.46,241.94c-12.28,5.37-23.49,8.06-33.57,8.06c-9.63,0-18.21-2.44-25.71-7.35 c-23.05-15.07-23.72-46.17-23.72-49.67V61.67h32.2v30h39.27v32.2h-39.27v69.04c0.07,4.46,1.88,18.1,9.2,22.83 c5.51,3.55,15.7,2.38,28.68-3.3L229.46,241.94z"></path>
                                 </g>
                              </g>
                           </g>
                           <g>
                              <g>
                                 <path class="st5" d="M250.4,164.29c0-15.97-0.24-27.35-0.97-38h25.9l0.97,22.51h0.97c5.81-16.7,19.61-25.17,32.19-25.17 c2.9,0,4.6,0.24,7.02,0.73v28.08c-2.42-0.48-5.08-0.97-8.71-0.97c-14.28,0-23.96,9.2-26.63,22.51c-0.48,2.66-0.97,5.81-0.97,9.2 v61H250.4V164.29z"></path>
                              </g>
                           </g>
                           <g>
                              <g>
                                 <path class="st5" d="M459.28,161.39c0-13.55-0.24-24.93-0.97-35.1h26.14l1.45,17.67h0.73c5.08-9.2,17.91-20.33,37.52-20.33 c20.57,0,41.87,13.31,41.87,50.59v69.95h-29.77v-66.56c0-16.94-6.29-29.77-22.51-29.77c-11.86,0-20.09,8.47-23.24,17.43 c-0.97,2.66-1.21,6.29-1.21,9.68v69.23h-30.01V161.39z"></path>
                              </g>
                           </g>
                           <g>
                              <g>
                                 <path class="st5" d="M706.41,72.31V211c0,12.1,0.48,25.17,0.97,33.16h-26.63l-1.21-18.64h-0.48c-7.02,13.07-21.3,21.3-38.49,21.3 c-28.08,0-50.35-23.96-50.35-60.27c-0.24-39.45,24.45-62.93,52.77-62.93c16.22,0,27.84,6.78,33.16,15.49h0.48v-66.8 C676.63,72.31,706.41,72.31,706.41,72.31z M676.64,175.43c0-2.42-0.24-5.33-0.73-7.75c-2.66-11.62-12.1-21.06-25.66-21.06 c-19.12,0-29.77,16.94-29.77,38.97c0,21.54,10.65,37.28,29.53,37.28c12.1,0,22.75-8.23,25.66-21.06c0.73-2.66,0.97-5.57,0.97-8.71 V175.43z"></path>
                              </g>
                           </g>
                           <path class="st0" d="M769.68,89.95c-0.11-0.53-0.24-1.04-0.39-1.55c-0.12-0.39-0.25-0.76-0.39-1.14c-0.12-0.32-0.25-0.64-0.39-0.95 c-0.12-0.27-0.25-0.54-0.39-0.81c-0.12-0.24-0.25-0.47-0.39-0.7c-0.12-0.21-0.25-0.42-0.39-0.63c-0.13-0.19-0.26-0.38-0.39-0.57 c-0.13-0.17-0.26-0.35-0.39-0.51s-0.26-0.32-0.39-0.47s-0.26-0.3-0.39-0.44s-0.26-0.28-0.39-0.41c-0.13-0.13-0.26-0.25-0.39-0.37 s-0.26-0.23-0.39-0.35c-0.13-0.11-0.26-0.22-0.39-0.33c-0.13-0.1-0.26-0.2-0.39-0.3c-0.13-0.1-0.26-0.19-0.39-0.28 s-0.26-0.18-0.39-0.27c-0.13-0.08-0.26-0.16-0.39-0.24c-0.13-0.08-0.26-0.16-0.39-0.24c-0.13-0.07-0.26-0.14-0.39-0.21 s-0.26-0.14-0.39-0.2s-0.26-0.13-0.39-0.19c-0.13-0.06-0.26-0.12-0.39-0.18s-0.26-0.11-0.39-0.17c-0.13-0.05-0.26-0.1-0.39-0.15 s-0.26-0.1-0.39-0.14s-0.26-0.09-0.39-0.13s-0.26-0.08-0.39-0.12s-0.26-0.07-0.39-0.11c-0.13-0.03-0.26-0.07-0.39-0.1 s-0.26-0.06-0.39-0.09s-0.26-0.05-0.39-0.08s-0.26-0.05-0.39-0.07s-0.26-0.04-0.39-0.06s-0.26-0.04-0.39-0.06s-0.26-0.03-0.39-0.04 s-0.26-0.03-0.39-0.04s-0.26-0.02-0.39-0.03s-0.26-0.02-0.39-0.03s-0.26-0.01-0.39-0.02c-0.13,0-0.26-0.01-0.39-0.01 s-0.26-0.01-0.39-0.01s-0.26,0.01-0.39,0.01s-0.26,0-0.39,0.01c-0.13,0-0.26,0.01-0.39,0.02c-0.13,0.01-0.26,0.02-0.39,0.03 s-0.26,0.02-0.39,0.03s-0.26,0.03-0.39,0.04c-0.13,0.02-0.26,0.03-0.39,0.04c-0.13,0.02-0.26,0.04-0.39,0.06s-0.26,0.04-0.39,0.06 s-0.26,0.05-0.39,0.08s-0.26,0.05-0.39,0.08s-0.26,0.06-0.39,0.09s-0.26,0.07-0.39,0.1c-0.13,0.04-0.26,0.07-0.39,0.11 s-0.26,0.08-0.39,0.12s-0.26,0.09-0.39,0.13c-0.13,0.05-0.26,0.09-0.39,0.14s-0.26,0.1-0.39,0.15s-0.26,0.11-0.39,0.16 c-0.13,0.06-0.26,0.12-0.39,0.18s-0.26,0.12-0.39,0.19s-0.26,0.14-0.39,0.21s-0.26,0.14-0.39,0.21c-0.13,0.08-0.26,0.16-0.39,0.24 c-0.13,0.08-0.26,0.16-0.39,0.24c-0.13,0.09-0.26,0.18-0.39,0.27s-0.26,0.19-0.39,0.28c-0.13,0.1-0.26,0.2-0.39,0.3 c-0.13,0.11-0.26,0.22-0.39,0.33s-0.26,0.23-0.39,0.34c-0.13,0.12-0.26,0.24-0.39,0.37c-0.13,0.13-0.26,0.27-0.39,0.4 c-0.13,0.14-0.26,0.28-0.39,0.43s-0.26,0.3-0.39,0.46c-0.13,0.16-0.26,0.33-0.39,0.5c-0.13,0.18-0.26,0.37-0.39,0.55 c-0.13,0.2-0.26,0.4-0.39,0.61c-0.13,0.22-0.26,0.45-0.39,0.68c-0.14,0.25-0.27,0.51-0.39,0.77c-0.14,0.3-0.27,0.6-0.39,0.9 c-0.14,0.36-0.27,0.73-0.39,1.1c-0.15,0.48-0.28,0.98-0.39,1.48c-0.25,1.18-0.39,2.42-0.39,3.7c0,1.27,0.14,2.49,0.39,3.67 c0.11,0.5,0.24,0.99,0.39,1.47c0.12,0.37,0.24,0.74,0.39,1.1c0.12,0.3,0.25,0.6,0.39,0.89c0.12,0.26,0.25,0.51,0.39,0.76 c0.12,0.23,0.25,0.45,0.39,0.67c0.12,0.2,0.25,0.41,0.39,0.6c0.13,0.19,0.25,0.37,0.39,0.55c0.13,0.17,0.25,0.34,0.39,0.5 c0.13,0.15,0.26,0.3,0.39,0.45c0.13,0.14,0.26,0.28,0.39,0.42c0.13,0.13,0.26,0.26,0.39,0.39c0.13,0.12,0.26,0.25,0.39,0.37 c0.13,0.11,0.26,0.22,0.39,0.33s0.26,0.21,0.39,0.32c0.13,0.1,0.26,0.2,0.39,0.3c0.13,0.09,0.26,0.18,0.39,0.27s0.26,0.18,0.39,0.26 s0.26,0.16,0.39,0.24c0.13,0.08,0.26,0.15,0.39,0.23c0.13,0.07,0.26,0.14,0.39,0.21s0.26,0.13,0.39,0.19 c0.13,0.06,0.26,0.13,0.39,0.19c0.13,0.06,0.26,0.11,0.39,0.17s0.26,0.11,0.39,0.17c0.13,0.05,0.26,0.1,0.39,0.14 c0.13,0.05,0.26,0.1,0.39,0.14s0.26,0.08,0.39,0.12s0.26,0.08,0.39,0.12s0.26,0.07,0.39,0.1s0.26,0.07,0.39,0.1s0.26,0.06,0.39,0.08 c0.13,0.03,0.26,0.06,0.39,0.08c0.13,0.02,0.26,0.05,0.39,0.07s0.26,0.04,0.39,0.06s0.26,0.04,0.39,0.05 c0.13,0.02,0.26,0.03,0.39,0.04s0.26,0.03,0.39,0.04s0.26,0.02,0.39,0.03s0.26,0.02,0.39,0.03s0.26,0.01,0.39,0.01 s0.26,0.01,0.39,0.01c0.05,0,0.1,0,0.15,0c0.08,0,0.16,0,0.24-0.01c0.13,0,0.26,0,0.39-0.01c0.13,0,0.26,0,0.39-0.01 s0.26-0.02,0.39-0.03s0.26-0.01,0.39-0.03c0.13-0.01,0.26-0.02,0.39-0.04c0.13-0.01,0.26-0.03,0.39-0.04 c0.13-0.02,0.26-0.03,0.39-0.05c0.13-0.02,0.26-0.04,0.39-0.06s0.26-0.04,0.39-0.06s0.26-0.05,0.39-0.08s0.26-0.05,0.39-0.08 s0.26-0.06,0.39-0.09s0.26-0.06,0.39-0.1s0.26-0.07,0.39-0.11s0.26-0.08,0.39-0.12s0.26-0.08,0.39-0.13 c0.13-0.04,0.26-0.09,0.39-0.14s0.26-0.1,0.39-0.15s0.26-0.11,0.39-0.16c0.13-0.06,0.26-0.11,0.39-0.17s0.26-0.12,0.39-0.18 s0.26-0.13,0.39-0.2s0.26-0.14,0.39-0.21s0.26-0.15,0.39-0.23s0.26-0.15,0.39-0.24c0.13-0.08,0.26-0.17,0.39-0.26 c0.13-0.09,0.26-0.18,0.39-0.27c0.13-0.1,0.26-0.19,0.39-0.29s0.26-0.21,0.39-0.32s0.26-0.22,0.39-0.33 c0.13-0.12,0.26-0.24,0.39-0.36c0.13-0.13,0.26-0.26,0.39-0.39c0.13-0.14,0.26-0.28,0.39-0.42c0.13-0.15,0.26-0.3,0.39-0.45 c0.13-0.16,0.26-0.33,0.39-0.49c0.13-0.18,0.26-0.36,0.39-0.54c0.13-0.2,0.26-0.4,0.39-0.6c0.14-0.22,0.26-0.44,0.39-0.67 c0.14-0.25,0.27-0.5,0.39-0.76c0.14-0.29,0.27-0.58,0.39-0.88c0.14-0.36,0.27-0.72,0.39-1.1c0.15-0.48,0.28-0.97,0.39-1.46 c0.25-1.17,0.39-2.4,0.39-3.66C770.03,92.19,769.9,91.05,769.68,89.95z"></path>
                           <g>
                              <g>
                                 <rect x="738.36" y="126.29" class="st5" width="30.01" height="117.88"></rect>
                              </g>
                           </g>
                           <g>
                              <g>
                                 <path class="st5" d="M912.39,184.14c0,43.33-30.5,62.69-60.51,62.69c-33.4,0-59.06-22.99-59.06-60.75 c0-38.73,25.41-62.45,61-62.45C888.91,123.63,912.39,148.32,912.39,184.14z M823.56,185.35c0,22.75,11.13,39.94,29.29,39.94 c16.94,0,28.8-16.7,28.8-40.42c0-18.4-8.23-39.45-28.56-39.45C832.03,145.41,823.56,165.74,823.56,185.35z"></path>
                              </g>
                           </g>
                           <g>
                              <path class="st5" d="M386,243.52c-2.95,0-5.91-0.22-8.88-0.66c-15.75-2.34-29.65-10.67-39.13-23.46 c-9.48-12.79-13.42-28.51-11.08-44.26c2.34-15.75,10.67-29.65,23.46-39.13c12.79-9.48,28.51-13.42,44.26-11.08 s29.65,10.67,39.13,23.46l7.61,10.26l-55.9,41.45l-15.21-20.51l33.41-24.77c-3.85-2.36-8.18-3.94-12.78-4.62 c-9-1.34-17.99,0.91-25.3,6.33s-12.07,13.36-13.41,22.37c-1.34,9,0.91,17.99,6.33,25.3c5.42,7.31,13.36,12.07,22.37,13.41 c9,1.34,17.99-0.91,25.3-6.33c4.08-3.02,7.35-6.8,9.72-11.22l22.5,12.08c-4.17,7.77-9.89,14.38-17.02,19.66 C411,239.48,398.69,243.52,386,243.52z"></path>
                           </g>
                        </svg>
                     </div>
                  </div>
               </div>
               <div class="col-12 text-center lh150 mt20 mt-md50 px15 px-md0">
                  <div class="trpre-heading f-20 f-md-22 w500 lh150 yellow-clr">
                     <span>
                     It's Time to Get Over Boring Content &amp; Cash into The Latest Trending Topics in 2022 
                     </span>
                  </div>
               </div>
               <div class="col-12 mt-md25 mt20 f-md-45 f-28 w500 text-center black-clr lh150">
                  Breakthrough A.I. Technology <span class="w700 blue-clr">Creates Traffic Pulling Websites Packed with Trendy Content &amp; Videos from Just One Keyword</span> in 60 Seconds... 
               </div>
               <div class="col-12 mt-md25 mt20 f-20 f-md-22 w500 text-center lh150 black-clr">
                  Anyone can easily create &amp; sell traffic generating websites for dentists, attorney, affiliates,<br class="d-none d-md-block"> coaches, SAAS, retail stores, book shops, gyms, spas, restaurants, &amp; 100+ other niches...
               </div>
            </div>
            <div class="row d-flex align-items-center flex-wrap mt20 mt-md40">
               <div class="col-md-7 col-12 min-md-video-width-left">
                  <div class="col-12 responsive-video">
                     <iframe src="https://trendio.dotcompal.com/video/embed/2p8pv1cndk" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                        box-shadow: none !important;" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                  </div>
               </div>
               <div class="col-md-5 col-12 mt20 mt-md0 min-md-video-width-right">
                  <div class="key-features-bg1">
                     <ul class="list-head1 pl0 m0 f-16 lh150 w500 black-clr">
                        <li>Works Seamlessly in Any Niche or Topic - <span class="w600">Just One Keyword</span> </li>
                        <li>All of That Without Content Creation, Editing, Camera, or Tech Hassles Ever </li>
                        <li>Drive Tons of FREE Viral Traffic Using the POWER Of Trendy Content </li>
                        <li>Grab More Authority, Engagement, &amp; Leads on your Business Website </li>
                        <li>Monetize Other's Content Legally with Banner Ads, Affiliate Offers &amp; AdSense </li>
                        <li class="w600">FREE COMMERCIAL LICENSE INCLUDED TO BUILD AN INCREDIBLE INCOME </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="tr-all">
         <picture>
            <source media="(min-width:767px)" srcset="https://cdn.oppyo.com/launches/jobiin/special-bonus/tr-lst-dt.webp" class="img-fluid d-block mx-auto" style="width:100%">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/jobiin/special-bonus/tr-lst-mb.webp" class="img-fluid d-block mx-auto">
            <img src="https://cdn.oppyo.com/launches/jobiin/special-bonus/tr-lst-dt.webp" alt="Steps" class="img-fluid d-block mx-auto" style="width:100%">
         </picture>
         <picture>
            <source media="(min-width:767px)" srcset="https://cdn.oppyo.com/launches/jobiin/special-bonus/tr-dt.webp" class="img-fluid d-block mx-auto" style="width:100%">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/jobiin/special-bonus/tr-mb.webp" class="img-fluid d-block mx-auto">
            <img src="https://cdn.oppyo.com/launches/jobiin/special-bonus/tr-dt.webp" alt="Steps" class="img-fluid d-block mx-auto" style="width:100%">
         </picture>
         <picture>
            <source media="(min-width:767px)" srcset="https://cdn.oppyo.com/launches/jobiin/special-bonus/pr-dt.webp" class="img-fluid d-block mx-auto" style="width:100%">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/jobiin/special-bonus/pr-mb.webp" class="img-fluid d-block mx-auto">
            <img src="https://cdn.oppyo.com/launches/jobiin/special-bonus/pr-dt.webp" alt="Steps" class="img-fluid d-block mx-auto" style="width:100%">
         </picture>
      </div>
      <!------Trendio Section Ends------>
   
      <!-------Exclusive Bonus----------->
      <div class="exclusive-bonus">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                  <div class="white-clr f-24 f-md-36 lh140 w600">
                     Exclusive Bonus #3 : Kyza
                  </div>
                  <div class="f-18 f-md-20 w500 mt20 white-clr">
                     20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
                  </div>
               </div>
            </div>
         </div>
      </div>

      <div class="header-section-ky">
         <div class="container">
            <div class="row">
               <div class="col-md-3 col-md-12 mx-auto">
                  <img src="assets/images/kyza-logo.webp" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 text-center">
                  <div class="mt20 mt-md60 text-center px-sm15 px0">
						   <div class="f-20 f-sm-22 w600 text-center lh140 d-inline-flex align-items-center justify-content-center relative">
                        <img src="assets/images/stop.webp" class="img-responsive mx-xs-center stop-img">
						      <div class="post-heading1">
						         <i>Paying 100s of Dollar Monthly To Multiple Marketing Apps! </i>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md60 text-center">
                  <div class="f-md-41 f-28 w700 black-clr lh140 line-center">	
						   Game Changer All-In-One Growth Suite... <br class="visible-lg">
						   ✔️Builds Beautiful Pages & Websites ✔️Unlocks Branding Designs Kit ✔️Creates AI-Powered Optin Forms & Pop-Ups, & ✔️Lets You Send UNLIMITED Emails 					
                  </div>
                  <div class="mt-md25 mt20 f-20 f-md-26 w600 black-clr text-center lh140">
                     All From Single Dashboard Without Any Tech Skills and <u>Zero Monthly Fees</u>
                  </div>
                  <div class="col-12 col-md-12  mt20 mt-md40">
							<div class="row d-flex flex-wrap align-items-center">
							   <div class="col-md-7 col-12 mt20 mt-md0">
									<div class="responsive-video">  
										<iframe class="embed-responsive-item"  src="https://kyza.dotcompal.com/video/embed/nr4t9jffvd" style="width: 100%;height: 100%; background: transparent !important;
										box-shadow: none !important;" frameborder="0" allow="fullscreen" allowfullscreen></iframe>
								  </div>
							   </div>
							   <div class="col-md-5 col-12 mt20 mt-md0">
									<div class="key-features-bg-ky">
									   <ul class="list-head-ky pl0 m0 f-18 f-md-19 lh140 w500 black-clr">
                                 <li>Create Unlimited Stunning Pages, Popups, And Forms </li>
                                 <li>Mobile, SEO, And Social Media Optimized Pages </li>
                                 <li>Send Unlimited Beautiful Emails For Tons Of Traffic & Sales </li>
                                 <li>Over 100 Done-For-You High Converting Templates </li>
                                 <li>2 Million+ Stock Images & Branding Graphics Kit </li>
                                 <li>Comes With Commercial License To Serve & Charge Your Clients </li>
                                 <li>100% Newbie Friendly & Step-By-Step Training Included  </li>
									   </ul>
								   </div>
							   </div>
						   </div>
                  </div>
					</div>
            </div>
         </div>
      </div>
      <!-- Header Section End -->
      <img src="assets/images/kyza-gb1.webp" class="img-fluid mx-auto d-block" style="width: 100%;">
      <img src="assets/images/kyza-gb2.webp" class="img-fluid mx-auto d-block" style="width: 100%;">
      <img src="assets/images/kyza-gb3.webp" class="img-fluid mx-auto d-block" style="width: 100%;">


      <!-- Vocalic Section Start -->
      <div class="exclusive-bonus">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                  <div class="white-clr f-24 f-md-36 lh140 w600">
                     Exclusive Bonus #4 : Vocalic
                  </div>
                  <div class="f-18 f-md-20 w500 mt20 white-clr">
                     20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
                  </div>
               </div>
            </div>
         </div>
      </div>

      <div class="header-section-vc">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row align-items-center">
                     <div class="col-md-12 text-center">
                        <svg xmlns="http://www.w3.org/2000/svg" id="Layer_2" viewBox="0 0 290.61 100" style="max-height:56px;">
                           <defs>
                              <style>
                                 .cls-1 {
                                 fill: #fff;
                                 }
                                 .cls-1,
                                 .cls-2 {
                                 fill-rule: evenodd;
                                 }
                                 .cls-2,
                                 .cls-3 {
                                 fill: #00aced;
                                 }
                              </style>
                           </defs>
                           <g id="Layer_1-2">
                              <g>
                                 <g>
                                    <path class="cls-3" d="M104.76,79.86l-5.11-6.34c-.16-.2-.45-.23-.65-.07-.37,.3-.75,.59-1.14,.87-3.69,2.66-7.98,4.34-12.49,4.9-1.11,.14-2.23,.21-3.35,.21s-2.24-.07-3.35-.21c-4.52-.56-8.8-2.24-12.49-4.9-.39-.28-.77-.57-1.14-.87-.2-.16-.49-.13-.65,.07l-5.11,6.34c-.16,.2-.13,.49,.07,.65,5.24,4.22,11.53,6.88,18.21,7.71,.37,.05,.74,.09,1.11,.12v11.67h6.7v-11.67c.37-.03,.74-.07,1.11-.12,6.68-.83,12.96-3.48,18.21-7.71,.2-.16,.23-.45,.07-.65Z">
                                    </path>
                                    <path class="cls-3" d="M82.02,74.47c10.57,0,19.13-7.45,19.13-16.65V31.08c0-4.6-2.14-8.76-5.6-11.77-3.46-3.01-8.24-4.88-13.53-4.88-10.57,0-19.13,7.45-19.13,16.65v26.74c0,9.2,8.57,16.65,19.13,16.65Zm-11.14-41.53c0-5.35,4.99-9.69,11.14-9.69,3.08,0,5.86,1.08,7.87,2.84,2.01,1.75,3.26,4.18,3.26,6.85v23.02c0,5.35-4.99,9.69-11.14,9.69s-11.14-4.34-11.14-9.69v-23.02Z">
                                    </path>
                                    <path class="cls-3" d="M82.02,38.89c1.71,0,3.1-1.39,3.1-3.1s-1.39-3.1-3.1-3.1-3.1,1.39-3.1,3.1,1.39,3.1,3.1,3.1Z">
                                    </path>
                                    <path class="cls-3" d="M82.02,47.54c1.71,0,3.1-1.39,3.1-3.1s-1.39-3.1-3.1-3.1-3.1,1.39-3.1,3.1,1.39,3.1,3.1,3.1Z">
                                    </path>
                                    <path class="cls-3" d="M82.02,56.2c1.71,0,3.1-1.39,3.1-3.1s-1.39-3.1-3.1-3.1-3.1,1.39-3.1,3.1,1.39,3.1,3.1,3.1Z">
                                    </path>
                                 </g>
                                 <path class="cls-1" d="M25.02,58.54L10.61,26.14c-.08-.19-.25-.29-.45-.29H.5c-.18,0-.32,.08-.42,.23-.1,.15-.11,.31-.04,.47l20.92,46.71c.08,.19,.25,.29,.45,.29h8.22c.2,0,.37-.1,.45-.29L60.4,15.13c.07-.16,.06-.32-.03-.47-.1-.15-.24-.23-.42-.23h-9.67c-.2,0-.37,.11-.45,.29L26.07,58.54c-.1,.21-.29,.34-.53,.34-.23,0-.43-.13-.53-.34h0Z">
                                 </path>
                                 <path class="cls-1" d="M117.96,52c-.13-.79-.2-1.58-.2-2.38s.07-1.6,.2-2.38c1.15-6.95,7.2-12.05,14.24-12.05,3.84,0,7.49,1.51,10.21,4.23l1.1,1.1c.19,.19,.49,.19,.68,0l6.03-6.03c.19-.19,.19-.49,0-.68l-1.1-1.1c-4.5-4.5-10.56-7.01-16.92-7.01-12.06,0-22.27,8.99-23.75,20.97-.12,.98-.18,1.97-.18,2.96s.06,1.98,.18,2.96c1.48,11.97,11.69,20.97,23.75,20.97,6.37,0,12.42-2.51,16.92-7.01l1.1-1.1c.19-.19,.19-.49,0-.68l-6.03-6.03c-.19-.19-.49-.19-.68,0l-1.1,1.1c-2.72,2.72-6.37,4.23-10.21,4.23-7.04,0-13.09-5.1-14.24-12.05h0Z">
                                 </path>
                                 <path class="cls-1" d="M258.21,52c-.13-.79-.2-1.58-.2-2.38s.07-1.6,.2-2.38c1.15-6.95,7.2-12.05,14.24-12.05,3.84,0,7.49,1.51,10.21,4.23l1.1,1.1c.19,.19,.49,.19,.68,0l6.03-6.03c.19-.19,.19-.49,0-.68l-1.1-1.1c-4.5-4.5-10.56-7.01-16.92-7.01-12.06,0-22.27,8.99-23.75,20.97-.12,.98-.18,1.97-.18,2.96s.06,1.98,.18,2.96c1.48,11.97,11.69,20.97,23.75,20.97,6.37,0,12.42-2.51,16.92-7.01l1.1-1.1c.19-.19,.19-.49,0-.68l-6.03-6.03c-.19-.19-.49-.19-.68,0l-1.1,1.1c-2.72,2.72-6.37,4.23-10.21,4.23-7.04,0-13.09-5.1-14.24-12.05h0Z">
                                 </path>
                                 <path class="cls-1" d="M174.91,25.87c-11.97,1.48-20.97,11.69-20.97,23.75s8.99,22.27,20.97,23.75c.98,.12,1.97,.18,2.96,.18s1.98-.06,2.96-.18c2.14-.26,4.24-.82,6.23-1.65,.19-.08,.3-.24,.3-.44v-9.77c0-.19-.09-.35-.27-.43-.17-.09-.35-.07-.5,.05-1.86,1.41-4.03,2.35-6.34,2.73-.79,.13-1.58,.2-2.38,.2s-1.6-.07-2.38-.2l-.2-.03c-6.86-1.23-11.85-7.24-11.85-14.21s5.1-13.09,12.05-14.24c.79-.13,1.58-.2,2.38-.2s1.59,.07,2.38,.2l.19,.03c6.87,1.23,11.87,7.24,11.87,14.21v22.7c0,.26,.22,.48,.48,.48h8.53c.26,0,.48-.22,.48-.48v-22.7c0-12.06-8.99-22.27-20.97-23.75-.98-.12-1.97-.18-2.96-.18s-1.98,.06-2.96,.18h0Z">
                                 </path>
                                 <path class="cls-1" d="M210.57,0c-.33,0-.59,.27-.59,.59V72.96c0,.33,.27,.59,.59,.59h10.5c.33,0,.59-.27,.59-.59V.59c0-.33-.27-.59-.59-.59h-10.5Z">
                                 </path>
                                 <path class="cls-1" d="M229.84,25.39c-.33,0-.59,.27-.59,.59v46.97c0,.33,.27,.59,.59,.59h10.5c.33,0,.59-.27,.59-.59V25.98c0-.33-.27-.59-.59-.59h-10.5Z">
                                 </path>
                                 <path class="cls-2" d="M229.84,8.96c-.33,0-.59,.07-.59,.15v11.94c0,.08,.27,.15,.59,.15h10.5c.33,0,.59-.07,.59-.15V9.11c0-.08-.27-.15-.59-.15h-10.5Z">
                                 </path>
                              </g>
                           </g>
                        </svg>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50 text-center">
                  <div class="preheadline f-18 f-md-22 w800 blue-clr lh140">
                     <i><span class="red-clr">Attention:</span> Futuristic A.I. Technology Do All The Work For You!</i>
                  </div>
               </div>
               <div class="col-12 mt-md30 mt20 f-md-50 f-28 w600 text-center white-clr lh140">
                  <span class="w800"> Create Profit-Pulling Videos with Voiceovers   </span><br>
                  <span class="w800 f-30 f-md-55 highlight-heading">In Just 60 Seconds <u>without</u>...</span> <br>
                  Being in Front of Camera, Speaking A Single Word or Hiring Anyone Ever
               </div>
               <div class="col-12 mt-md35 mt20  text-center">
                  <div class="post-heading f-18 f-md-24 w600 text-center lh150 white-clr">
                     Create &amp; Sell Videos &amp; Voiceovers In ANY LANGUAGE For Big Profits To Dentists, Gyms, Spas, Cafes, Retail Stores, Book Shops, Affiliate Marketers, Coaches &amp; 100+ Other Niches… 
                  </div>
               </div>
            </div>
            <div class="row d-flex flex-wrap mt20 mt-md40">
               <div class="col-md-7 col-12 min-md-video-width-left">
                  <div class="col-12 responsive-video border-video">
                     <iframe src="https://vocalic.dotcompal.com/video/embed/g1mdnyzmub" style="width:100%; height:100%" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                  </div>
               </div>
               <div class="col-md-5 col-12 mt20 mt-md0 min-md-video-width-right">
                  <div class="calendar-wrap mb20 mb-md0">
                     <ul class="list-head pl0 m0 f-18 lh150 w400 white-clr">
                        <li>Tap Into Fast-Growing <span class="w700">$200 Billion Video &amp; Voiceover Industry</span></li>
                        <li><span class="w700">Create Stunning Videos In ANY Niche</span> With In-Built Video Creator</li>
                        <li><span class="w700">Create &amp; Publish YouTube Shorts</span> For Tons Of FREE Traffic</li>
                        <li><span class="w700">Make Passive Affiliate Commissions</span> Creating Product Review Videos</li>
                        <li>Real Human-Like Voiceover In <span class="w700">150+ Unique Voices And 30+ Languages</span></li>
                        <li>Convert Any Text Script Into A <span class="w700">Whiteboard &amp; Explainer Videos</span></li>
                        <li><span class="w700">Create Videos Using Just A Keyword</span> With Ready-To-Use Stock Images</li>
                        <li><span class="w700">Boost Engagement &amp; Sales </span> Using Videos</li>
                        <li><span class="w700">Add Background Music</span> To Your Videos.</li>
                        <li><span class="w700">Built-In Content Spinner</span> - Make Unique Scripts</li>
                        <li><span class="w700">Store Media Files </span> With Integrated MyDrive</li>
                        <li><span class="w700">100% Newbie Friendly </span> - No Tech Hassles Ever</li>
                        <li><span class="w700">Commercial License Included</span> To Build On Incredible Income Helping Clients</li>
                        <li>FREE Training To<span class="w700"> Find Tons Of Clients</span></li>                        
                     </ul>
                  </div>
                  <div class="d-md-none">
                     <div class="f-18 f-md-26 w800 lh150 text-center white-clr">
                        FREE Commercial License Included for A Limited Time!
                     </div>
                     <div class="f-16 f-md-18 lh150 w500 text-center mt10 white-clr">
                        Use Coupon Code <span class="w800 yellow-clr">"VOCALIC"</span> for an Extra <span class="w800 yellow-clr">10% Discount</span> 
                     </div>
                     <div class="row">
                        <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center">
                           <a href="#buynow" class="cta-link-btn d-block px0">Get Instant Access To Vocalic</a>
                        </div>
                     </div>
                     <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                        <img src="https://cdn.oppyo.com/launches/viddeyo/special/compaitable-with1.webp" class="img-responsive mx-xs-center md-img-right" alt="visa">
                        <div class="d-md-block d-none visible-md px-md30"><img src="https://cdn.oppyo.com/launches/viddeyo/special/v-line.webp" class="img-fluid" alt="line">
                        </div>
                        <img src="https://cdn.oppyo.com/launches/viddeyo/special/days-gurantee1.webp" class="img-responsive mx-xs-auto mt15 mt-md0" alt="30 days">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <div class="gr-1">
         <div class="container-fluid p0">
            <picture>
               <source media="(min-width:768px)" srcset="assets/images/vocalic1.webp">
               <source media="(min-width:320px)" srcset="assets/images/vocalic1-mview.webp" style="width:100%" class="vidvee-mview">
               <img src="assets/images/vocalic1.webp" alt="Vidvee Steps" class="img-fluid" style="width: 100%;">
            </picture>
            <picture>
               <source media="(min-width:768px)" srcset="assets/images/vocalic2.webp">
               <source media="(min-width:320px)" srcset="assets/images/vocalic2-mview.webp">
               <img src="assets/images/vocalic2.webp" alt="Flowers" style="width:100%;">
            </picture>
         </div>
      </div>
      <!-- Vocalic Section ENds -->
    

      <!-------Exclusive Bonus----------->
      <div class="exclusive-bonus">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                  <div class="white-clr f-24 f-md-36 lh140 w600">
                     Exclusive Bonus #5 : Viddeyo
                  </div>
                  <div class="f-18 f-md-20 w500 mt20 white-clr">
                     20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
                  </div>
               </div>
            </div>
         </div>
      </div>

      <div class="viddeyo-header-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row align-items-center">
                     <div class="col-md-12 text-center">
                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 909.75 162.72" style="enable-background:new 0 0 909.75 162.72; max-height:56px;" xml:space="preserve">
                           <style type="text/css">
                              .st00{fill:#FFFFFF;}
                              .st11{fill:url(#SVGID_2_);}
                           </style>
                           <g>
                              <g>
                                 <path class="st00" d="M216.23,71.07L201.6,97.26l-35.84-64.75l-3.16-5.7h29.13l0.82,1.49L216.23,71.07z M288.35,26.81l-3.16,5.7
                                 l-56.4,103.68h-6.26l-11.71-22.25l14.63-26.18l0.01,0.02l2.73-4.94l4.81-8.68l25.38-45.87l0.82-1.49H288.35z"></path>
                                 <path class="st00" d="M329.85,26.61v109.57h-23.23V26.61H329.85z"></path>
                                 <path class="st00" d="M454.65,81.7c0,2.7-0.2,5.41-0.59,8.04c-3.58,24.36-23.15,43.45-47.59,46.42l-0.26,0.03h-50.93v-69.9h23.23
                                 v46.67h26.13c12.93-2,23.4-11.91,26.09-24.74c0.45-2.14,0.67-4.33,0.67-6.51c0-2.11-0.21-4.22-0.62-6.26
                                 c-1.52-7.54-5.76-14.28-11.95-18.97c-3.8-2.88-8.14-4.84-12.75-5.78c-1.58-0.32-3.19-0.52-4.83-0.6h-45.98V26.82L403.96,27
                                 c11.89,0.9,23.21,5.67,32.17,13.61c9.78,8.65,16.15,20.5,17.96,33.36C454.46,76.49,454.65,79.1,454.65,81.7z"></path>
                                 <path class="st00" d="M572.18,81.7c0,2.7-0.2,5.41-0.59,8.04c-3.58,24.36-23.15,43.45-47.59,46.42l-0.26,0.03h-50.93v-69.9h23.23
                                 v46.67h26.13c12.93-2,23.4-11.91,26.09-24.74c0.45-2.14,0.67-4.33,0.67-6.51c0-2.11-0.21-4.22-0.62-6.26
                                 c-1.52-7.54-5.76-14.28-11.95-18.97c-3.8-2.88-8.14-4.84-12.75-5.78c-1.58-0.32-3.19-0.52-4.83-0.6h-45.98V26.82L521.5,27
                                 c11.89,0.9,23.2,5.67,32.17,13.61c9.78,8.65,16.15,20.5,17.96,33.36C572,76.49,572.18,79.1,572.18,81.7z"></path>
                                 <path class="st00" d="M688.08,26.82v23.23h-97.72V31.18l0.01-4.36H688.08z M613.59,112.96h74.49v23.23h-97.72v-18.87l0.01-4.36
                                 V66.1l23.23,0.14h64.74v23.23h-64.74V112.96z"></path>
                                 <path class="st00" d="M808.03,26.53l-5.07,6.93l-35.84,48.99l-1.08,1.48L766,83.98l-0.04,0.05l0,0.01l-0.83,1.14v51h-23.23V85.22
                                 l-0.86-1.18l-0.01-0.01l-0.04-0.05l-0.04-0.06l-1.08-1.48l-35.84-48.98l-5.07-6.93h28.77l1.31,1.78l24.46,33.43l24.46-33.43
                                 l1.31-1.78H808.03z"></path>
                                 <path class="st00" d="M909.75,81.69c0,30.05-24.45,54.49-54.49,54.49c-30.05,0-54.49-24.45-54.49-54.49S825.2,27.2,855.25,27.2
                                 C885.3,27.2,909.75,51.65,909.75,81.69z M855.25,49.95c-17.51,0-31.75,14.24-31.75,31.75s14.24,31.75,31.75,31.75
                                 c17.51,0,31.75-14.24,31.75-31.75C887,64.19,872.76,49.95,855.25,49.95z"></path>
                              </g>
                           </g>
                           <linearGradient id="SVGID_2_" gradientUnits="userSpaceOnUse" x1="0" y1="81.3586" x2="145.8602" y2="81.3586">
                              <stop offset="0.4078" style="stop-color:#FFFFFF"></stop>
                              <stop offset="1" style="stop-color:#FC6DAB"></stop>
                           </linearGradient>
                           <path class="st11" d="M11.88,0c41.54,18.27,81.87,39.12,121.9,60.41c7.38,3.78,12.24,11.99,12.07,20.32
                           c0.03,7.94-4.47,15.72-11.32,19.73c-27.14,17.07-54.59,33.74-82.21,50c-0.57,0.39-15.29,8.93-15.47,9.04
                           c-1.4,0.77-2.91,1.46-4.44,1.96c-14.33,4.88-30.25-4.95-32.19-19.99c-0.18-1.2-0.23-2.5-0.24-3.72c0.35-25.84,1.01-53.01,2.04-78.83
                           C2.57,42.41,20.3,32.08,34.79,40.31c8.16,4.93,16.31,9.99,24.41,15.03c2.31,1.49,7.96,4.89,10.09,6.48
                           c5.06,3.94,7.76,10.79,6.81,17.03c-0.68,4.71-3.18,9.07-7.04,11.77c-0.38,0.24-1.25,0.88-1.63,1.07c-3.6,2.01-7.5,4.21-11.11,6.17
                           c-9.5,5.22-19.06,10.37-28.78,15.27c11.29-9.81,22.86-19.2,34.53-28.51c3.54-2.55,4.5-7.53,1.83-10.96c-0.72-0.8-1.5-1.49-2.49-1.9
                           c-0.18-0.08-0.42-0.22-0.6-0.29c-11.27-5.17-22.82-10.58-33.98-15.95c0,0-0.22-0.1-0.22-0.1l-0.09-0.02l-0.18-0.04
                           c-0.21-0.08-0.43-0.14-0.66-0.15c-0.11-0.01-0.2-0.07-0.31-0.04c-0.21,0.03-0.4-0.04-0.6,0.03c-0.1,0.02-0.2,0.02-0.29,0.03
                           c-0.14,0.06-0.28,0.1-0.43,0.12c-0.13,0.06-0.27,0.14-0.42,0.17c-0.09,0.03-0.17,0.11-0.26,0.16c-0.08,0.06-0.19,0.06-0.26,0.15
                           c-0.37,0.25-0.66,0.57-0.96,0.9c-0.1,0.2-0.25,0.37-0.32,0.59c-0.04,0.08-0.1,0.14-0.1,0.23c-0.1,0.31-0.17,0.63-0.15,0.95
                           c-0.06,0.15,0.04,0.35,0.02,0.52c0,0.02,0,0.08-0.01,0.1c0,0,0,0.13,0,0.13l0.02,0.51c0.94,24.14,1.59,49.77,1.94,73.93
                           c0,0,0.06,4.05,0.06,4.05l0,0.12l0.01,0.02l0.01,0.03c0,0.05,0.02,0.11,0.02,0.16c0.08,0.23,0.16,0.29,0.43,0.47
                           c0.31,0.16,0.47,0.18,0.71,0.09c0.06-0.04,0.11-0.06,0.18-0.1c0.02-0.01-0.01,0.01,0.05-0.03l0.22-0.13l0.87-0.52
                           c32.14-19.09,64.83-37.83,97.59-55.81c0,0,0.23-0.13,0.23-0.13c0.26-0.11,0.51-0.26,0.69-0.42c1.15-0.99,0.97-3.11-0.28-4.01
                           c0,0-0.43-0.27-0.43-0.27l-1.7-1.1C84.73,51.81,47.5,27.03,11.88,0L11.88,0z"></path>
                        </svg>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50 text-center">
                  <div class="viddeyo-preheadline f-18 f-md-20 w400 white-clr lh140">
                     <span class="w600 text-uppercase">Warning:</span> 1000s of Business Websites are Leaking Profit and Sales by Uploading Videos to YouTube
                  </div>
               </div>
               <div class="col-12 mt-md15 mt10 f-md-48 f-28 w400 text-center white-clr lh140">
                  Start Your Own <span class="w700 highlight-viddeyo">Video Hosting Agency with Unlimited Videos with Unlimited Bandwidth</span> at Lightning Fast Speed with No Monthly FEE Ever...
               </div>
               <div class="col-12 mt20 mt-md20 f-md-21 f-18 w400 white-clr text-center lh170">
                  Yes! Total control over your videos with zero delay, no ads and no traffic leak. Commercial License Included
               </div>
            </div>

            <div class="row d-flex align-items-center flex-wrap mt20 mt-md40">
               <div class="col-md-7 col-12 min-md-video-width-left">
                  <!-- <img src="https://cdn.oppyo.com/launches/viddeyo/special/proudly.webp" class="img-fluid d-block mx-auto" alt="proudly"> -->
                  <div class="col-12 responsive-video border-video">
                     <iframe src="https://launch.oppyo.com/video/embed/xo6b4lwdid" style="width:100%; height:100%" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                  </div>
               </div>
               <div class="col-md-5 col-12 mt20 mt-md0 min-md-video-width-right">
                  <div class="calendar-wrap">
                     <div class="calendar-wrap-inline">
                        <ul class="list-head pl0 m0 f-18 f-md-18 lh150 w500 black-clr">
                           <li>100% Control on Your Video Traffic &amp; Channels</li>
                           <li>Close More Prospects for Your Agency Services</li>
                           <li>Boost Commissions, Customer Satisfaction &amp; Sales Online</li>
                           <li>Tap Into $398 Billion E-Learning Industry</li>
                           <li>Tap Into Huge 82% Of Overall Traffic on Internet </li>
                           <li>Boost Your Clients Sales-Commercial License Included</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>

         </div>
      </div>


      <div class="viddeyo-second-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row viddeyo-header-list-block">
                     <div class="col-12 col-md-6">
                        <ul class="viddeyo-features-list pl0 f-18 f-md-20 lh150 w400 black-clr text-capitalize">
                           <li><span class="w600">Upload Unlimited Videos</span> - Sales, E-Learning, Training, Product Demo, or ANY Video</li>
                           <li><span class="w600">Super-Fast Delivery (After All, Time Is Money!)</span> No Buffering. No Delay </li>
                           <li><span class="w600">Get Maximum Visitors Engagement</span>-No Traffic Leakage with Unwanted Ads or Video Suggestions</li>
                           <li>Create 100% Mobile &amp; SEO Optimized <span class="w600">Video Channels &amp; Playlists</span></li>
                           <li><span class="w600">Stunning Promo &amp; Social Ads Templates</span> for Monetization &amp; Social Traffic</li>
                           <li>Fully Customizable Drag and Drop <span class="w600">WYSIWYG Video Ads Editor</span></li>
                           <li><span class="w600">Intelligent Analytics</span> to Measure the Performance </li>
                        </ul>
                     </div>
                     <div class="col-12 col-md-6 mt10 mt-md0">
                        <ul class="viddeyo-features-list pl0 f-18 f-md-20 lh150 w400 black-clr text-capitalize">
                           <li class="w600">Tap Into Fast-Growing $398 Billion E-Learning, Video Marketing &amp; Video Agency Industry. </li>
                           <li>Manage All the Videos, Courses, &amp; Clients Hassle-Free, <span class="w600">All-In-One Central Dashboard.</span> </li>
                           <li>Robust Solution That Has <span class="w600">Served Over 69 million Video Views for Customers</span></li>
                           <li>HLS Player- Optimized to <span class="w600">Work Beautifully with All Browsers, Devices &amp; Page Builders</span> </li>
                           <li>Connect With Your Favourite Tools. <span class="w600">20+ Integrations</span> </li>
                           <li><span class="w600">Completely Cloud-Based</span> &amp; Step-By-Step Video Training Included </li>
                           <li>PLUS, YOU'LL RECEIVE - <span class="w600">A LIMITED COMMERCIAL LICENSE IF YOU BUY TODAY</span> </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <!-- Header Viddeyo End -->
      <div class="gr-1">
         <div class="container-fluid p0">
            <picture>
               <source media="(min-width:768px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/viddeyo-steps.webp">
               <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/viddeyo-steps-mview.webp" style="width:100%" class="vidvee-mview">
               <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/vidvee-steps.webp" alt="Vidvee Steps" class="img-fluid" style="width: 100%;">
            </picture>
            <picture>
               <source media="(min-width:768px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/viddeyo-letsfaceit.webp">
               <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/viddeyo-letsfaceit-mview.webp">
               <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/viddeyo-letsfaceit.webp" alt="Flowers" style="width:100%;">
            </picture>
            <picture>
               <source media="(min-width:768px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/viddeyo-proudly.webp">
               <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/viddeyo-proudly-mview.webp">
               <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/viddeyo-proudly.webp" alt="Flowers" style="width:100%;">
            </picture> 
         </div>
      </div>
      <!--Viddeyo ends-->

      <!-- Bonus Section Header Start -->
      <div class="bonus-header">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-10 mx-auto heading-bg text-center">
                  <div class="f-24 f-md-36 lh140 w700"> When You Purchase MailZilo, You Also Get <br class="hidden-xs"> Instant Access To These 20 Exclusive Bonuses</div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus Section Header End -->

      <!-- Bonus #1 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 1</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus1.webp" class="img-fluid mx-auto d-block" alt="Bonus1">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Your own@SEO
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">With this simple plugin you can get the true insight on your web traffic efforts in only seconds!</span> Watch as your social network shares increase, your google PageRank and more. </li>
                           <li>Get a clear vision on what you need to start focusing on with your SEO efforts. Start more effective backlinks from Facebook, Twitter, Google and more.</li>
                           <li>You will get all of the most important stats that you need to know for your SEO web traffic.</li>
                           <li>Focus on the amount of shares you have on popular social sites like Facebook, Twitter, Stumble Upon and more.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #1 Section End -->

      <!-- Bonus #2 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 2</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30">
                  <div class="row">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="assets/images/bonus2.webp" class="img-fluid mx-auto d-block" alt="Bonus2">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md0 text-capitalize">
                        Email Niche Plus
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">Owning a business has many advantages from being able to set your own hours to have the control to sell what you want. </span> </li>
                           <li>Unfortunately, too many new business owners fail within their first year. </li>
                           <li>While it isn't for lack of effort, those looking to start a new online business fail to complete the crucial first step; they fail to research to find a viable and profitable niche.</li>
                           <li>This comprehensive guide covers everything you need to know for finding your niche so you can stand out and create success faster.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #2 Section End -->

      <!-- Bonus #3 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 3</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus3.webp" class="img-fluid mx-auto d-block " alt="Bonus3">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Grab your Traffic Booster
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">Businesses both large and small are always hoping that their target audience will be able to find their site among the thousands of websites they are competing against.</span></li>
                           <li>One of the best ways to do this is to utilize the free and paid methods for boosting website traffic. </li>
                           <li>However, like so many online marketing methods, it isn't always clear on how to do this. Finding an effective way to boost the traffic to your website can not only be confusing, but it can also be a bit frustrating.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #3 Section End -->

      <!-- Bonus #4 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 4</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="assets/images/bonus4.webp" class="img-fluid mx-auto d-block " alt="Bonus4">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        All in one video marketing kit 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">Most people fail with Internet Marketing because they do not take consistent action.</span> There is a solid and dependable method that has been working for many years and will continue to work in the future.</li>
                           <li>It's time for you to learn how to make $10,000+ per month in 90 days or even less!</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #4 End -->

      <!-- Bonus #5 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 5</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus5.webp" class="img-fluid mx-auto d-block " alt="Bonus5">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Master E Secret kit
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        This is a collection of Internet Marketing Magazines with 380+ pages of quality content!<br><br>
                        <span class="w600">Topics covered:</span>
                           <li class="mt20">Your Online Business Startup</li>
                           <li>Lead Generation Strategies</li>
                           <li>Your Conversion Strategies</li>
                           <li>Network Marketing Guide</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #5 End -->

      <!-- CTA Button Section Start -->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 yellow-clr">"MAILVIP "</span> for an Additional <span class="w700 yellow-clr">$5 Discount</span> on Commercial Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                     <span class="text-center">Grab MailZilo + My 20 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                        <span class="f-14 f-md-18 w500 ">Days</span>
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                        <span class="f-14 f-md-18 w500 ">Hours</span>
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                        <span class="f-14 f-md-18 w500 ">Mins</span>
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                        <span class="f-14 f-md-18 w500 ">Sec</span>
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->

      <!-- Bonus #6 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 6</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="assets/images/bonus6.webp" class="img-fluid mx-auto d-block " alt="Bonus6">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Trendy Headline Bee
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">Learn the Techniques to Make Your Email Subject Line Stand Out Multi Media!</span></li>
                           <li>Your subject line will certainly stand out, and your email will be opened if you make your email unique, useful to the reader, and focused on what the reader either needs to know or wants to know. </li>
                           <li>Maximize your email marketing efforts by simply having the highest results that you haven't experience before. </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #6 End -->

      <!-- Bonus #7 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 7</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus7.webp" class="img-fluid mx-auto d-block " alt="Bonus7">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        LeadPlus 1 click 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">It doesn't matter what kind of business you're in, if you aren't able to generate new leads and turn them into paying customers, your company will never succeed. </span></li>
                           <li>You need to be constantly bringing in new customers if you want your business to thrive.</li>
                           <li>Generating more leads is anything but easy and if you don't have a solid marketing strategy that will drive more traffic to your website, you'll never be able to generate the leads you need for your business to succeed. </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #7 End -->

      <!-- Bonus #8 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 8</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus8.webp" class="img-fluid mx-auto d-block " alt="Bonus8">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Soci Marketing Agency
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">Social media is designed to keep us entertained. Many of us spend hours trawling through content looking to find things that are unique and special to add to our feeds.</span> </li>
                           <li>We are overwhelmed with having so many accounts that we can follow—but not all content is created equal.</li>
                           <li>Some content is much more popular than others; and it stands out on our feeds. Creating fantastic content and posting it at the optimal time with call-to-actions is a key strategy to succeeding on social media.  </li>
                          
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #8 End -->

      <!-- Bonus #9 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 9</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus9.webp" class="img-fluid mx-auto d-block " alt="Bonus9">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Image collection V3 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">Stock Images For You To Use In Your Projects And Your Clients Projects. Plus You Can Resell Them!</span> </li>
                           <li>Many successful online business owners have said that making money online is easy as a piece of cake as long as you have all the ingredients in doing the process.</li>
                           <li>And one of those ingredients is graphics or images which is a huge help in marketing your product or service online using social media networking sites.</li>
                           <li>The challenge is that you can't simply use images for granted or you might end up sued by copyright violation.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #9 End -->

      <!-- Bonus #10 start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 10</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus10.webp" class="img-fluid mx-auto d-block " alt="Bonus10">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        20k Top header collections
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">Ready to go abstract headers for your minisites!</span></li>
                           <li>If you want to make money online, one of the best media to sell or promote affiliate products is through you website or blog. </li>
                           <li>And if you want to stand from the crowded competition of the industry you are into, having a good-looking header for your blog or site is one of the factors to get noticed and remembered.</li>
                           <li>The good news is that inside this product is a bundle of abstract header background that you can use to your own project or website and you also have the authority to resell it to your customers.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #10 End -->

      <!-- CTA Button Section Start -->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 yellow-clr">"MAILVIP"</span> for an Additional <span class="w700 yellow-clr">$5 Discount</span> on Commercial Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                     <span class="text-center">Grab MailZilo + My 20 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr"><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 smmltd">Days</span> </div><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">0300</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div><div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div></div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->

      <!-- Bonus #11 start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 11</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus11.webp" class="img-fluid mx-auto d-block " alt="Bonus11">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Eye Catching sales templates
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>20 Eye Catching WP Sales Page Themes, You Receive The Resell Rights To The Full Package.</li>
                           
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #11 End -->

      <!-- Bonus #12 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 12</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus12.webp" class="img-fluid mx-auto d-block " alt="Bonus12">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Internet EDITION PLUS
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">With more than 40,000 Google searches happening every second, you need to find a way to gain the attention of these potential customers. </span></li>
                           <li>Knowing how your target audience engages with their favorite brands and the things that influence them to buy are the keys to growing your online business.</li>
                           <li>This video course details the 79 actions that you need to take today that will lead you to realizing huge results in your Internet business. </li>
                           <li>It covers everything from validating your business idea, to transitioning your business for growth, to successfully scaling your business to grow beyond your wildest dreams.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #12 End -->

      <!-- Bonus #13 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 13</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus13.webp" class="img-fluid mx-auto d-block " alt="Bonus13">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Build Up Your List 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        Start creating engagement with your new leads by making your landing page memorable and enjoyable!<br><br>
                        <span class="w600">Here is what you can do with this plugin:</span>
                        <li class="mt20">Edit every detail on the fly with the simple options panel for each page</li>
                        <li>Customize all of the content areas that are designed to be readable</li>
                        <li>Choose from a variety of colors for the 'call to action' buttons on your page</li>
                        <li>Load up the form code from any service like Aweber, MailChimp, and more</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #13 End -->

      <!-- Bonus #14 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 14</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus14.webp" class="img-fluid mx-auto d-block " alt="Bonus14">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Content Writer Pro
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">If you run a blog or website you understand the need for writing regular content. While this may sound easy, it is not always easy to come up with ideas of what to write about.</span> </li>
                           <li>Even then you need to know how to write a compelling blog post that will attract attention. </li>
                           <li>When it comes to writing online there are a few differences which you must be aware of. Writing this type of content is different than writing a novel or non-fiction book.</li>
                           <li>Inside this ebook you will find a compilation of 25 writing tips which have been designed to help you become a better writer. </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #14 End -->

      <!-- Bonus #15 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 15</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus15.webp" class="img-fluid mx-auto d-block " alt="Bonus15">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        4k Videos Pack
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        By 2019, Internet Video Traffic will account for 80% of all consumer Internet traffic. Here is an excellent opportunity to leverage the power of Videos and use this medium to catapult your web business to the next level. <br><br>
                        <span class="w600">You can:</span>

                           <li class="mt20">Use them as Background video on your sales page/sales video to enchance its appearance. </li>
                           <li>Use them as Sales video on your Squeeze Page/Landing Page.</li>
                           <li>Generate an additional passive income stream.</li>
                           <li>Use them as Promotional videos  on Youtube, FB, Instagram or any other networking or website</li>
                           
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #15 End -->

      <!-- CTA Button Section Start -->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 yellow-clr">"MAILVIP"</span> for an Additional <span class="w700 yellow-clr">$5 Discount</span> on Commercial Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                     <span class="text-center">Grab MailZilo + My 20 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <span class="f-14 f-md-18 smmltd">Days</span>
                      </div>
                      <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        
                        <span class="f-14 f-md-18 w500 smmltd">Hours</span>
                      </div>
                      <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        
                        <span class="f-14 f-md-18 w500 smmltd">Mins</span>
                      </div>
                      <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        
                        <span class="f-14 f-md-18 w500 smmltd">Sec</span> 
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->

      <!-- Bonus #16 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 16</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus16.webp" class="img-fluid mx-auto d-block " alt="Bonus16">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Boom social media
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">Turn tour Wordpress blog into a social powerhouse! The simple and fast way to increase social conversions.</span></li>
                           <li>Take the social features of some of the highest shared websites like Buzzfeed or UpWorthy and add them to your blog posts.</li>
                           <li>No matter what theme you are using you can add these shortcodes to get all the social share features you need to have viral blog posts.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #16 End -->

      <!-- Bonus #17 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 17</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus17.webp" class="img-fluid mx-auto d-block " alt="Bonus17">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Massive Website Visitors kit
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>An intrinsic understanding of your competition and how to better them is the most important component of any digital marketing strategy. Today, there is such small distances between you and your competition - it's unlikely that you're offering anything that cannot be bought on some other website. </li>
                           <li>Now, when a consumer wants and needs a product or information, all they must do is input their desires into the Google search and peruse the search engine results page.</li>
                           <li>Also, you will learn all the terms you need to know for a proper understanding of the web marketing industry.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #17 End -->

      <!-- Bonus #18 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 18</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus18.webp" class="img-fluid mx-auto d-block " alt="Bonus18">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        High sales Kit
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li><span class="w600">If you want to sell anything online, it's actually not all that hard.</span>
                        Closing a sale is not the problem. Generating consistent, high-value sales, that's the issue. </li>
                        <li>Even if you are able to master the art of online sales, you may still continue to struggle. </li>
                        <li>Using the high ticket sales secrets from this video course, you will learn how to maximize the return that you get for all your efforts. It all boils down to being at the right place, talking about the right things at the right time with the right people.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #18 End -->

      <!-- Bonus #19 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 19</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus19.webp" class="img-fluid mx-auto d-block " alt="Bonus19">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Dynamic page plugin
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">You probably have noticed that most businesses online are listed in directories such as YellowPages only... Now with one wordpress plugin you can create an all-in-one website that will pull in multiple sources and display in one place.</span> </li>
                           <li>This is a stand alone plugin that will create a business website in one landing page. Add tabbed content to keep your visitors staying on one page!</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #19 End -->

      <!-- Bonus #20 start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 20</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus20.webp" class="img-fluid mx-auto d-block " alt="Bonus20">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Affiliate Marketing Secrets
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        Discover How To Make Money With Affiliate Marketing - Even If You Are a Complete Beginner!<br><br>
                        <span class="w600">Topics covered:</span>
                           <li class="mt20">4 Ways you are killing your passive income empire dreams</li>
                           <li>The money is in the list but not in the way you think</li>
                           <li>7 secrets of affiliate success most marketers will not tell you</li>
                           <li>7 Reasons why you should focus on niche selection</li>
                           <li>6 Ways your niche may be holding your affiliate income</li>
                           <li>How to turbocharge your affiliate income in one step</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #20 End -->


      <!-- Huge Woth Section Start -->
      <div class="huge-area mt30 mt-md10">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="f-md-65 f-40 lh120 w700 white-clr">That's Huge Worth of</div>
                  <br>
                  <div class="f-md-60 f-40 lh120 w800 yellow-clr">$3300!</div>
               </div>
            </div>
         </div>
      </div>
      <!-- Huge Worth Section End -->

      <!-- text Area Start -->
      <div class="white-section pb0">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="f-md-36 f-25 lh140 w500">So what are you waiting for? You have a great opportunity <br class="hidden-xs"> ahead + <span class="w700">My 20 Bonus Products</span> are making it a <br class="hidden-xs"> <span class="w700">completely NO Brainer!!</span></div>
               </div>
            </div>
         </div>
      </div>
      <!-- text Area End -->

      <!-- CTA Button Section Start -->
      <div class="cta-btn-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-16 f-md-20 lh150 w500 text-center black-clr">
                  Use Coupon Code <span class="w700 blue-clr">"MAILVIP"</span> for an Additional <span class="w700 blue-clr">$5 Discount</span> on Commercial Licence
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn px-md80">
                     Grab MailZilo + My 20 Exclusive Bonuses
                  </a>
               </div>

               
               <div class="col-12 mt15 mt-md20" align="center">
                  <h3 class="f-md-28 f-20 w500 text-center black">My Exclusive Bonuses Are Expiring in...</h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 col-md-10 mx-auto col-12 text-center">
                  <div class="countdown counter-black">
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                     <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                     <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->

      <!-- Footer Section Start -->
      <div class="footer-section">
        <div class="container ">
            <div class="row">
                <div class="col-12 text-center">
                    <svg version="1.1" x="0px" y="0px" viewBox="0 0 1501.16 276.9" style="enable-background:new 0 0 1501.16 276.9; max-height:40px;" xml:space="preserve">
							<style type="text/css">
							.st0{fill:#FFFFFF;}
							</style>
							<g>
							<path class="st0" d="M122.38,159.77l82.88-67.64h39.5v182.43h-45.44V160.68l-73.53,57.2h-6.82l-73.52-57.2v113.87H0V92.13h39.46
							L122.38,159.77z"></path>
							<path class="st0" d="M391.87,92.13l81.03,182.43h-48.87L409,238.51h-81.7l-13.62,36.04h-48.16l73.69-182.43H391.87z M395.71,206.65
							l-29.57-70.91l-26.8,70.91H395.71z"></path>
							<path class="st0" d="M540.93,92.13v182.43h-45.44V92.13H540.93z"></path>
							<path class="st0" d="M722.06,241.12v33.43H582.98V92.13h45.44v149H722.06z"></path>
							<path class="st0" d="M913.75,92.13l-99.48,149h99.48v33.43H736.68l100.26-149H743.6V92.13H913.75z"></path>
							<path class="st0" d="M989.75,92.13v182.43h-45.44V92.13H989.75z"></path>
							<path class="st0" d="M1170.88,241.12v33.43H1031.8V92.13h45.44v149H1170.88z"></path>
							<path class="st0" d="M1332.43,209.97c-2.33,5.08-5.42,9.64-9.28,13.71c-9.7,10.23-22.48,15.35-38.35,15.35
							c-15.94,0-28.8-5.12-38.53-15.35c-9.74-10.22-14.61-23.66-14.61-40.28c0-16.72,4.87-30.17,14.61-40.35
							c5.49-5.75,11.97-9.87,19.44-12.37l-26.14-31.06c-9.05,4.52-17.19,10.6-24.43,18.22c-17.75,18.72-26.63,40.57-26.63,65.56
							c0,25.41,8.94,47.36,26.81,65.81c17.87,18.45,41.03,27.68,69.49,27.68c28.13,0,51.18-9.26,69.13-27.81
							c2.07-2.15,4.04-4.34,5.87-6.58L1332.43,209.97z"></path>
							<path class="st0" d="M1501.16,0l-114.63,241.95l-71.11-84.51l-31.99,29.36l2.17-64.79c67.33-7.59,111.49-65.77,112.17-66.67
							c-41.92,31-125.97,49.4-125.97,49.4l-28.6-33.1L1501.16,0z"></path>
							</g>
							</svg>
                    <div class="f-14 f-md-16 w400 mt20 lh160 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
                </div>
                <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                    <div class="f-14 f-md-16 w400 lh160 white-clr text-xs-center">Copyright © MailZilo 2022</div>
                    <ul class="footer-ul w400 f-14 f-md-16 white-clr text-center text-md-right">
                        <li><a href="https://support.bizomart.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                        <li><a href="http://mailzilo.com/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                        <li><a href="http://mailzilo.com/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                        <li><a href="http://mailzilo.com/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                        <li><a href="http://mailzilo.com/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                        <li><a href="http://mailzilo.com/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                        <li><a href="http://mailzilo.com/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
      <!--Footer Section End -->
      
      <!-- timer --->
      <?php
         if ($now < $exp_date) {
         ?>
      <script type="text/javascript">
         // Count down milliseconds = server_end - server_now = client_end - client_now
         var server_end = <?php echo $exp_date; ?> * 1000;
         var server_now = <?php echo time(); ?> * 1000;
         var client_now = new Date().getTime();
         var end = server_end - server_now + client_now; // this is the real end time
         
         var noob = $('.countdown').length;
         
         var _second = 1000;
         var _minute = _second * 60;
         var _hour = _minute * 60;
         var _day = _hour * 24
         var timer;
         
         function showRemaining() {
         var now = new Date();
         var distance = end - now;
         if (distance < 0) {
         	clearInterval(timer);
         	document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
         	return;
         }
         
         var days = Math.floor(distance / _day);
         var hours = Math.floor((distance % _day) / _hour);
         var minutes = Math.floor((distance % _hour) / _minute);
         var seconds = Math.floor((distance % _minute) / _second);
         if (days < 10) {
         	days = "0" + days;
         }
         if (hours < 10) {
         	hours = "0" + hours;
         }
         if (minutes < 10) {
         	minutes = "0" + minutes;
         }
         if (seconds < 10) {
         	seconds = "0" + seconds;
         }
         var i;
         var countdown = document.getElementsByClassName('countdown');
         for (i = 0; i < noob; i++) {
         	countdown[i].innerHTML = '';
         
         	if (days) {
         		countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + days + '</span><span class="f-14 f-md-18 smmltd">Days</span> </div>';
         	}
         
         	countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + hours + '</span><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>';
         
         	countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">' + minutes + '</span><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>';
         
         	countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">' + seconds + '</span><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>';
         }
         
         }
         timer = setInterval(showRemaining, 1000);
         	
      </script>
      <?php
         } else {
         echo "Times Up";
         }
         ?>
      <!--- timer end-->
   </body>
</html>
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <!-- Tell the browser to be responsive to screen width -->
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
      <meta name="title" content="MailZilo Bonuses">
      <meta name="description" content="MailZilo Bonuses">
      <meta name="keywords" content="Revealing: Breath-Taking A.I. Technology That Creates Stunning Content for Any Local or Online Niche At Low One-Time Fee">
      <meta property="og:image" content="https://www.mailzilo.com/special-bonus/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Dr. Amit Pareek">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="MailZilo Bonuses">
      <meta property="og:description" content="Revealing: Breath-Taking A.I. Technology That Creates Stunning Content for Any Local or Online Niche At Low One-Time Fee">
      <meta property="og:image" content="https://www.mailzilo.com/special-bonus/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="MailZilo Bonuses">
      <meta property="twitter:description" content="Revealing: Breath-Taking A.I. Technology That Creates Stunning Content for Any Local or Online Niche At Low One-Time Fee">
      <meta property="twitter:image" content="https://www.mailzilo.com/special-bonus/thumbnail.png">
      <title>MailZilo Bonuses</title>
      <!-- Shortcut Icon  -->
      <link rel="icon" href="https://cdn.oppyo.com/launches/mailzilo/common_assets/images/favicon.png" type="image/png">
      <!-- Css CDN Load Link -->
      <link rel="stylesheet" href="https://cdn.oppyo.com/launches/vidboxs/common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" type="text/css" href="assets/css/bonus-style.css">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
      <script src="https://cdn.oppyo.com/launches/writerarc/common_assets/js/jquery.min.js"></script>
   </head>
   <body>
   
      <!-- New Timer  Start-->
      <?php
         $date = 'Oct 31 2022 11:59 AM EST';
         $exp_date = strtotime($date);
         $now = time();  
         /*
         
         $date = date('F d Y g:i:s A eO');
         $rand_time_add = rand(700, 1200);
         $exp_date = strtotime($date) + $rand_time_add;
         $now = time();*/
         
         if ($now < $exp_date) {
         ?>
      <?php
         } else {
         	echo "Times Up";
         }
         ?>
      <!-- New Timer End -->
      <?php
         if(!isset($_GET['afflink'])){
         $_GET['afflink'] = 'https://warriorplus.com/o2/a/w9kmvh/0';
         $_GET['name'] = 'Ayush Jain';      
         }
         ?>

      <!-- Header Section Start -->   
      <div class="main-header">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="text-center">
                     <div class="f-md-32 f-20 lh140 w400 white-clr d-block d-md-flex align-items-center justify-content-center">
                        <span class="w600"><?php echo $_GET['name'];?>'s</span> &nbsp;special bonus for &nbsp;
                        <svg version="1.1" x="0px" y="0px" viewBox="0 0 1501.16 276.9" style="enable-background:new 0 0 1501.16 276.9; max-height:40px;" xml:space="preserve">
                           <style type="text/css">
                              .st0{fill:#FFFFFF;}
                           </style>
                           <g>
                              <path class="st0" d="M122.38,159.77l82.88-67.64h39.5v182.43h-45.44V160.68l-73.53,57.2h-6.82l-73.52-57.2v113.87H0V92.13h39.46
                              L122.38,159.77z"></path>
                              <path class="st0" d="M391.87,92.13l81.03,182.43h-48.87L409,238.51h-81.7l-13.62,36.04h-48.16l73.69-182.43H391.87z M395.71,206.65
                              l-29.57-70.91l-26.8,70.91H395.71z"></path>
                              <path class="st0" d="M540.93,92.13v182.43h-45.44V92.13H540.93z"></path>
                              <path class="st0" d="M722.06,241.12v33.43H582.98V92.13h45.44v149H722.06z"></path>
                              <path class="st0" d="M913.75,92.13l-99.48,149h99.48v33.43H736.68l100.26-149H743.6V92.13H913.75z"></path>
                              <path class="st0" d="M989.75,92.13v182.43h-45.44V92.13H989.75z"></path>
                              <path class="st0" d="M1170.88,241.12v33.43H1031.8V92.13h45.44v149H1170.88z"></path>
                              <path class="st0" d="M1332.43,209.97c-2.33,5.08-5.42,9.64-9.28,13.71c-9.7,10.23-22.48,15.35-38.35,15.35
                              c-15.94,0-28.8-5.12-38.53-15.35c-9.74-10.22-14.61-23.66-14.61-40.28c0-16.72,4.87-30.17,14.61-40.35
                              c5.49-5.75,11.97-9.87,19.44-12.37l-26.14-31.06c-9.05,4.52-17.19,10.6-24.43,18.22c-17.75,18.72-26.63,40.57-26.63,65.56
                              c0,25.41,8.94,47.36,26.81,65.81c17.87,18.45,41.03,27.68,69.49,27.68c28.13,0,51.18-9.26,69.13-27.81
                              c2.07-2.15,4.04-4.34,5.87-6.58L1332.43,209.97z"></path>
                              <path class="st0" d="M1501.16,0l-114.63,241.95l-71.11-84.51l-31.99,29.36l2.17-64.79c67.33-7.59,111.49-65.77,112.17-66.67
                              c-41.92,31-125.97,49.4-125.97,49.4l-28.6-33.1L1501.16,0z"></path>
                           </g>
                        </svg>
                     </div>
                  </div>
                  <div class="preheadline f-md-20 f-18 w600 lh150 mt20 mt-md40">
						   <div class="skew-con d-flex gap20 align-items-center ">
                        <span>Grab My 20 Exclusive Bonuses Before the Deal Ends…</span>
						      <div class="d-none d-md-block">
							      <img src="assets/images/fav.webp" class="img-fluid d-block">
                        </div>
						   </div>
                  </div>
                  <div class="col-12 mt-md30 mt20 f-md-42 f-28 w600 text-center white-clr lh140">
                     <span class="cyan-clr"> Revealing: </span> Advanced Tag & Segment Based Email Software <span class="yellow-clr"> That Gets 4X More Opening & Click </span> At Low One-Time Fee
                  </div>
                  <div class="col-12 mt-md30 mt20  text-center">
                     <div class="f-18 f-md-22 w600 text-center lh150 white-clr">
                        Watch My Quick Review Video
                     </div>
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md30">
               <div class="col-md-10 col-12 mx-auto">
                  <div class="col-12 responsive-video border-video">
                     <iframe src="https://mailzilo.dotcompal.com/video/embed/fy9w5ktacr" style="width:100%; height:100%" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                  </div> 
                   <!--<img src="assets/images/productbox.webp" class="img-fluid d-block mx-auto">-->
               </div>
            </div>
         </div>
      </div>
      <!-- Header Section End -->

            <!-- header List Section Start -->
            <div class="header-list-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row header-list-block">
                     <div class="col-12 col-md-6">
                        <div class="f-16 f-md-18 lh140 w400">
                           <ul class="bonus-list pl0 m0">
                              <li><span class="w600">Send Unlimited Emails</span> to Unlimited Subscribers</li>
                              <li><span class="w600">Collect Unlimited Leads</span> with Built-In Lead Form</li>
                              <li> <span class="w600">Free SMTP</span> for unrestricted Mailing</li>
                              <li><span class="w600">Upload Unlimited List</span> with Zero Restrictions</li>
                              <li><span class="w600">Get 4X More ROI</span> and Profits than Ever</li>
                              <li><span class="w600">Smart Tagging</span> for Lead Personalisation</li>
                              <li><span class="w600">100% Control</span> on your online business </li>
                           </ul>
                        </div>
                     </div>
                     <div class="col-12 col-md-6">
                        <div class="f-16 f-md-18 lh140 w400">
                           <ul class="bonus-list pl0 m0">
                              <li><span class="w600">Built-in Inline</span> Editor to Craft Beautiful Emails</li>
                              <li><span class="w600">Hassle-Free</span> Subscribers Management</li>
                              <li><span class="w600">100+ High Converting</span> Templates for Webforms & Email</li>
                              <li><span class="w600">GDPR & Can-Spam Compliant</span></li>
                              <li><span class="w600">100% Newbie Friendly</span> & Easy to Use</li>
                              <li><span class="w600">Commercial License Included</span></li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- header List Section End -->

      <!-- Step Section Start -->
      <div class="step-section">
         <div class="container ">
            <div class="row ">
               <div class="col-12 f-28 f-md-45 w600 lh140 black-clr text-center">
                  Replace your old-school email marketing <br class="d-none d-md-block"> techniques with this cutting-edge solution that<br class="d-none d-md-block"> powers your business <span class="orange-clr d-inline-grid">in just 3 easy steps… <img src="assets/images/underline.webp " class="img-fluid"></span>
               </div>
               <div class="col-12 mt30 mt-md80">
                  <div class="row align-items-center">
                     <div class="col-12 col-md-6 relative">
                        <img src="assets/images/step1.webp" class="img-fluid">
                        <div class="f-28 f-md-40 w600 lh140 mt15">
                           Upload
                        </div>
                        <div class="f-18 f-md-20 w400 lh140 mt15">
                           To begin with, all you need to do is upload your subscribers 
                           or generate leads using MailZilo opt-in forms.   
                        </div>
                        <img src="assets/images/left-arrow.webp" class="img-fluid d-none d-md-block ms-auto step-arrow1">
                     </div>
                     <div class="col-12 col-md-6 mt20 mt-md0 ">
                        <img src="assets/images/step-one.webp" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
               <div class="col-12 mt30 mt-md80">
                  <div class="row align-items-center ">
                     <div class="col-12 col-md-6 order-md-2 relative">
                        <img src="assets/images/step2.webp" class="img-fluid">
                        <div class="f-28 f-md-40 w600 lh140 mt15">
                           Select or Create
                        </div>
                        <div class="f-18 f-md-20 w400 lh140 mt15">
                           Select from the list of beautifully designed email templates 
                           or create your own email template.   
                        </div>
                        <img src="assets/images/right-arrow.webp" class="img-fluid d-none d-md-block step-arrow2">
                     </div>
                     <div class="col-md-6 col-md-pull-6 mt20 mt-md0 order-md-1 ">
                        <img src="assets/images/step-two.webp " class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
               <div class="col-12 mt30 mt-md80">
                  <div class="row align-items-center">
                     <div class="col-12 col-md-6">
                        <img src="assets/images/step3.webp" class="img-fluid">
                        <div class="f-28 f-md-40 w600 lh140 mt15">
                           Send & Profit
                        </div>
                        <div class="f-18 f-md-20 w400 lh140 mt15">
                           Send Unlimited Email to your Subscriber or Schedule it for later.
                        </div>
                     </div>
                     <div class="col-12 col-md-6 mt20 mt-md0 ">
                        <img src="assets/images/step-three.webp " class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md60 text-center">
                  <div class="d-flex gap-2 gap-md-4 justify-content-center flex-wrap">
                     <div class="d-flex align-items-center gap-3 justify-content-center">
                        <img src="https://cdn.oppyo.com/launches/writerarc/bizdrive/cross.webp" alt="Cross" class="img-fluid d-block">
                        <div class="f-md-28 w700 lh140 f-20 red-clr"> No SMTP</div>
                     </div>
                     <div class="d-flex align-items-center gap-3 justify-content-center">
                        <img src="https://cdn.oppyo.com/launches/writerarc/bizdrive/cross.webp" alt="Cross" class="img-fluid d-block">
                        <div class="f-md-28 w700 lh140 f-20 red-clr"> No Installation</div>
                     </div>
                     <div class="d-flex align-items-center gap-3 justify-content-center">
                        <img src="https://cdn.oppyo.com/launches/writerarc/bizdrive/cross.webp" alt="Cross" class="img-fluid d-block">
                        <div class="f-md-28 w700 lh140 f-20 red-clr">No Other Techie Stuff</div>
                     </div>
                  </div>
                  <div class="f-20 f-md-28 w500 lh140 mt20">It's EASY, FAST & Built with LOVE.</div>
               </div>
            </div>
         </div>
      </div>
      <!-- Step Section End -->

      <!-- CTA Btn Start-->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 yellow-clr">"MAILZILO"</span> for an Additional <span class="w700 yellow-clr">$3 Discount</span> on Commercial Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                     <span class="text-center">Grab MailZilo + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                           <br>
                        <span class="f-14 f-md-18 smmltd">Days</span>
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Sec</span>
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Btn End -->

      <div class="proof-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 ">
                  <div class="f-28 f-md-45 w600 lh140 black-clr text-center">
                  Here is What We’ve Made in the Past 3 Months <br class="d-none d-md-block"> 
                  by Using the Exact Same Technology
                  </div>
               </div>
               <div class="col-12 mx-auto mt20 mt-md50">
                  <img src="assets/images/proof.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>
            <div class="row mt-md70 mt30 align-items-center">
               <div class="col-md-6 f-md-32 f-22 w600 text-center text-md-start">
                  Got 20K+ Visitors in Just 15 Days on My Affiliate Offers Using the Power of Emails  
               </div>
               <div class="col-md-6 mt-md0 mt20">
                  <img src="assets/images/proof1.webp" alt="" class="img-fluid d-block mx-auto">
               </div>
            </div>
            <div class="row mt-md70 mt30 align-items-center">
               <div class="col-md-6 f-md-32 f-22 w600 order-md-2 text-center text-md-start">
                  And Let’s Check Out the Crazy Open And Click Rates 
                  We’ve Got for A Simple Email I Sent Using Mailzilo.
               </div>
               <div class="col-md-6 mt-md0 mt20 order-md-1">
                  <img src="assets/images/proof2.webp" alt="" class="img-fluid d-block mx-auto">
               </div>
            </div>
         </div>
      </div>
      <!-- Proof Section End -->  
      <!-- Testimonial Section Start -->
      <div class="testimonial-section">
         <div class="container ">
            <div class="row ">
               <div class="col-12 f-28 f-md-45 w600 lh140 black-clr text-center">
                  Here's What Industry Experts Have to <br class="d-none d-md-block"> Say About MailZilo
               </div>
            </div>
            <div class="row row-cols-md-2 row-cols-1 gx4">
               <div class="col mt50">
                  <div class="single-testimonial">
                     <p>
                     Finally, I am getting my emails delivered, clicked, and opened. Yes,<span class="w600"> MailZilo is one of the BEST tools</span> I’ve used for my business yet! No more dependency on other email software. Stupidly simple to use. MailZilo made me go crazy. <span class="w600">Five Stars from my side for this product… </span>
                     </p>
                     <div class="client-info">
                        <img src="assets/images/amit-gaikwad.webp" class="img-fluid d-block mx-auto">
                        <div class="client-details">
                        Amit Gaikwad
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col mt50">
                  <div class="single-testimonial">
                     <p>
                     BINGO, I've tested MailZilo for myself, and man, <span class="w600">it delivered exactly as promised.</span> I just recently sent mail to my entire list and <span class="w600">got 20% more open rates</span> with this super-amazing Email Marketing Technology. <span class="w600">Two-Thumbs up for this one…</span> 
                     </p>
                     <div class="client-info">
                        <img src="assets/images/anirudh-bharwa.webp" class="img-fluid d-block mx-auto">
                        <div class="client-details">
                        Anirudh Bharwa
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col mt50">
                  <div class="single-testimonial">
                     <p>
                     MailZilo will surely take the industry by storm and will <span class="w600">help marketers to generate more leads</span> from any Blog, eCommerce, or WordPress site. I like its <span class="w600">latest smart tag feature</span> which enables me to build targeted marketing campaigns. Yet another killer product from Team Ayush & Pranshu… 
                     </p>
                     <div class="client-info">
                        <img src="assets/images/akshat-gupta.webp" class="img-fluid d-block mx-auto">
                        <div class="client-details">
                        Akshat Gupta
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col mt50">
                  <div class="single-testimonial">
                     <p>
                     <span class="w600">Importing Lists without losing any Leads? DONE!<br>
Sending UNLIMITED Emails? DONE!<br>
Want to Generate More Leads? DONE!<br>
Automating Email Marketing Campaigns? That’s DONE as well!<br><br></span>
This software really impressed me with its amazing features. A great option for anyone looking to make the most from email marketing…. 

                     </p>
                     <div class="client-info">
                        <img src="assets/images/uddhab-pramanik.webp" class="img-fluid d-block mx-auto">
                        <div class="client-details">
                        Uddhab Pramanik
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Testimonial Section End -->

      <!-- CTA Btn Start -->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 yellow-clr">"MAILZILO"</span> for an Additional <span class="w700 yellow-clr">$3 Discount</span> on Commercial Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                     <span class="text-center">Grab MailZilo + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr"><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">02</span><span class="f-14 f-md-18 smmltd">Days</span> </div><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">05</span><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">14</span><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div><div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">58</span><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div></div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Section End -->

      <!-- Research Section Start -->
      <div class="research-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="reason-box">
                     <div class="f-24 f-md-38 w600 lh140 blue-clr">
                        Email Marketing Yields an
                     </div>
                     <div class="f-28 f-md-45 w700 lh140 black-clr">
                        Average 4300% Return on Investment
                     </div>
                     <div class="f-24 f-md-38 w600 lh140 blue-clr">
                        for businesses worldwide.
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50">
                  <div class="row align-items-center">
                     <div class="col-12 col-md-6">
                        <div class="f-20 f-md-24 w500 lh140 text-center text-md-start">
                           Yes, Emails are the most powerful, profitable, and low-cost marketing tool and get 3 times higher conversions than social media or any other mode of marketing.
                        </div>
                     </div>
                     <div class="col-12 col-md-6 mt20 mt-md0">
                        <img src="assets/images/investment.webp" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md70">
                  <div class="row">
                     <div class="col-12 col-md-7 order-md-2">
                        <div class="f-28 f-md-45 w700 lh140 text-center text-md-start green-clr">
                           That’s Right…
                        </div>
                        <div class="f-20 f-md-24 w500 lh140 text-center text-md-start mt10">
                           Email marketing is the key to getting new customers and creating deeper relationships with your existing customers at a fraction of the cost.
                        </div>
                     </div>
                     <div class="col-12 col-md-5 order-md-1 mt20 mt-md0 relative d-none d-md-block">
                        <img src="assets/images/girl.webp" class="img-fluid d-block mx-auto girl-img">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Research Section End -->

      <!-- Stats Section Start -->
      <div class="stats-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-45 w700 lh140 text-center white-clr stats-headline">Here are Some Eye Openers </div>
               </div>
            </div>
            <div class="row row-cols-1 row-cols-md-2 gap30 mt0 t-md20">
               <div class="col">
                  <div class="stats-block">
                     <div><img src="assets/images/mail.webp" class="img-fluid d-block mx-auto"></div>
                     <div class="f-20 f-md-24 w400 lh140 white-clr">There are <span class="orange-clr w700">4.5+ Billion</span> Daily Email Users</div>
                  </div>
               </div>
               <div class="col">
                  <div class="stats-block">
                     <div><img src="assets/images/mail.webp" class="img-fluid d-block mx-auto"></div>
                     <div class="f-20 f-md-24 w400 lh140 white-clr"><span class="orange-clr w700">350+ Billion Emails</span> are Circulated Every Single Day</div>
                  </div>
               </div>
               <div class="col">
                  <div class="stats-block">
                     <div><img src="assets/images/mail.webp" class="img-fluid d-block mx-auto"></div>
                     <div class="f-20 f-md-24 w400 lh140 white-clr"><span class="orange-clr w700">$400 Million</span> are Spent Every Year on Email Marketing in US Alone</div>
                  </div>
               </div>
               <div class="col">
                  <div class="stats-block">
                     <div><img src="assets/images/mail.webp" class="img-fluid d-block mx-auto"></div>
                     <div class="f-20 f-md-24 w400 lh140 white-clr"><span class="orange-clr w700">95% of Email</span> Users Check Their Emails Daily</div>
                  </div>
               </div>
               <div class="col">
                  <div class="stats-block">
                     <div><img src="assets/images/mail.webp" class="img-fluid d-block mx-auto"></div>
                     <div class="f-20 f-md-24 w400 lh140 white-clr"><span class="orange-clr w700">85% of Businesses</span> Believe that Email Marketing Improves Customer Retention</div>
                  </div>
               </div>
               <div class="col">
                  <div class="stats-block">
                     <div><img src="assets/images/mail.webp" class="img-fluid d-block mx-auto"></div>
                     <div class="f-20 f-md-24 w400 lh140 white-clr"><span class="orange-clr w700">80% of Youth</span> Prefer Business Communications via Email</div>
                  </div>
               </div>
               <div class="col">
                  <div class="stats-block">
                     <div><img src="assets/images/mail.webp" class="img-fluid d-block mx-auto"></div>
                     <div class="f-20 f-md-24 w400 lh140 white-clr"><span class="orange-clr w700">90% of Marketers</span> Use Email as the Primary Channel</div>
                  </div>
               </div>
               <div class="col">
                  <div class="stats-block">
                     <div><img src="assets/images/mail.webp" class="img-fluid d-block mx-auto"></div>
                     <div class="f-20 f-md-24 w400 lh140 white-clr"><span class="orange-clr w700">Since 10+ Years,</span> Email Marketing Generates the Highest ROI for Marketers.</div>
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md80">
               <div class="col-12 f-24 f-md-36 w600 white-clr lh140 text-center">This Show Why Big Businesses <br class="d-none d-md-block">&amp; Marketers are Capitalizing on Email Marketing</div>
            </div>
         </div>
      </div>
      <!-- Stats Section End -->

      <!-- Power Section Start -->
      <div class="power-section">
         <div class="container ">
            <div class="row align-items-center">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-45 w700 lh140 black-clr">
                     And Do You Know How Much People Are Making  <br>
                     <span class="orange-clr">Using the POWER of Emails Marketing?</span>
                  </div>
                  <img src="assets/images/power-line.webp" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 col-md-11 offset-md-1 mt20 mt-md50">
                  <div class="power-block">
                     <div class="f-28 f-md-36 w700 orange-clr lh140 text-center text-md-start">Tom Sather</div>
                     <div class="f-md-20 f-18 w400 lh140 white-clr mt10 text-center text-md-start">
                        He is known for his work as the Director of Return Path which has an Annual Revenue of $10+ Million. Return Path, with emphasis on Emails Marketing, offers unparalleled insights on why emails end up undeliverable and where they end up bouncing.
                     </div>
                     <img src="assets/images/tom.webp" class="img-fluid d-block mx-auto left-img mt20 mt-md0">
                  </div>
               </div>
               <div class="col-12">
                  <img src="assets/images/connect.webp" class="img-fluid d-block mx-auto connect-line">
               </div>
               <div class="col-12 col-md-11 mt20">
                  <div class="power-block right">
                     <div class="f-28 f-md-36 w700 orange-clr lh140 text-center text-md-start">Matthew Lloyd Smith</div>
                     <div class="f-md-20 f-18 w400 lh140 white-clr mt10 text-center text-md-start">Matthew  Lloyd Smith created the insanely helpful website Really Good Emails, which showcases the best in email marketing on the internet. Really Good Emails has a Revenue of over $3 Million.
                     </div>
                     <img src="assets/images/matthew.webp" class="img-fluid d-block mx-auto right-img mt20 mt-md0">
                  </div>
               </div>
               <div class="col-12">
                  <img src="assets/images/connect1.webp" class="img-fluid d-block mx-auto connect-line">
               </div>
               <div class="col-12 col-md-11 offset-md-1 mt20">
                  <div class="power-block">
                     <div class="f-28 f-md-36 w700 orange-clr lh140 text-center text-md-start">Kath Pay</div>
                     <div class="f-md-20 f-18 w400 lh140 white-clr mt10 text-center text-md-start">
                        Kath Pay has been in the email game for over 19 years. She is an email marketing consultant and founder of Holistic Email Marketing. She’s been named one of the top email influencers in the world and makes annual revenue of over $8 Million.
                     </div>
                     <img src="assets/images/kath.webp" class="img-fluid d-block mx-auto left-img mt20 mt-md0">
                  </div>
               </div>
               <div class="col-12 mt20 mt-md100">
                  <div class="f-20 f-md-32 w400 lh140 text-center">And all of that just by using the Power of Email Marketing</div>
                  <div class="f-32 f-md-55 w700 lh140 text-center green-clr my20">Impressive, Right?</div>
                  <div class="f-20 f-md-32 w400 lh140 text-center">So, it can be safely stated that…<br>
                     <span class="w600">Email Marketing is the BIGGEST Income Opportunity Right Now!</span>
                  </div>
               </div>
            </div>  
         </div>
      </div>
      <!-- Power Section End -->

      <!-- Proudly Section Start -->
      <div class="next-gen-sec" id="product">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="pre-heading f-md-32 f-24 w600 text-center white-clr lh140">
                  Proudly Presenting... </div>
               </div>
               <div class="col-12 mt-md20 mt20 text-center">
                  <svg version="1.1" x="0px" y="0px" viewBox="0 0 1501.16 276.9" style="enable-background:new 0 0 1501.16 276.9; max-height:100px;" xml:space="preserve">
                     <style type="text/css">
                     .st0{fill:#FFFFFF;}
                     </style>
                     <g>
                        <path class="st0" d="M122.38,159.77l82.88-67.64h39.5v182.43h-45.44V160.68l-73.53,57.2h-6.82l-73.52-57.2v113.87H0V92.13h39.46
                        L122.38,159.77z"></path>
                        <path class="st0" d="M391.87,92.13l81.03,182.43h-48.87L409,238.51h-81.7l-13.62,36.04h-48.16l73.69-182.43H391.87z M395.71,206.65
                        l-29.57-70.91l-26.8,70.91H395.71z"></path>
                        <path class="st0" d="M540.93,92.13v182.43h-45.44V92.13H540.93z"></path>
                        <path class="st0" d="M722.06,241.12v33.43H582.98V92.13h45.44v149H722.06z"></path>
                        <path class="st0" d="M913.75,92.13l-99.48,149h99.48v33.43H736.68l100.26-149H743.6V92.13H913.75z"></path>
                        <path class="st0" d="M989.75,92.13v182.43h-45.44V92.13H989.75z"></path>
                        <path class="st0" d="M1170.88,241.12v33.43H1031.8V92.13h45.44v149H1170.88z"></path>
                        <path class="st0" d="M1332.43,209.97c-2.33,5.08-5.42,9.64-9.28,13.71c-9.7,10.23-22.48,15.35-38.35,15.35
                        c-15.94,0-28.8-5.12-38.53-15.35c-9.74-10.22-14.61-23.66-14.61-40.28c0-16.72,4.87-30.17,14.61-40.35
                        c5.49-5.75,11.97-9.87,19.44-12.37l-26.14-31.06c-9.05,4.52-17.19,10.6-24.43,18.22c-17.75,18.72-26.63,40.57-26.63,65.56
                        c0,25.41,8.94,47.36,26.81,65.81c17.87,18.45,41.03,27.68,69.49,27.68c28.13,0,51.18-9.26,69.13-27.81
                        c2.07-2.15,4.04-4.34,5.87-6.58L1332.43,209.97z"></path>
                        <path class="st0" d="M1501.16,0l-114.63,241.95l-71.11-84.51l-31.99,29.36l2.17-64.79c67.33-7.59,111.49-65.77,112.17-66.67
                        c-41.92,31-125.97,49.4-125.97,49.4l-28.6-33.1L1501.16,0z"></path>
                     </g>
                  </svg>
               </div>
               <div class="f-md-36 f-24 w600 white-clr lh140 col-12 mt20 mt-md40 text-center">
                  The Most Advanced &amp; Powerful Email Marketing Automation Software that 
                  allows you to Create, Send, Track, and Profit from Email Marketing.
               </div>
               <div class="f-20 f-md-24 lh140 w400 text-center white-clr mt20">              
                  It is a must-have tool in your marketing arsenal that comes with a ONE-TIME FEE, <br class="d-none d-md-block"> and that will send all the existing money-sucking autoresponders back to their nest.
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md50">
               <div class="col-12 col-md-10 mx-auto">
                  <img src="assets/images/product-box.webp" class="img-fluid d-block mx-auto" alt="Product">
               </div>            
            </div>
            <div class="row mt20 mt-md50">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 yellow-clr">"MAILZILO"</span> for an Additional <span class="w700 yellow-clr">$3 Discount</span> on Commercial Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                     <span class="text-center">Grab MailZilo + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr"><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">02</span><span class="f-14 f-md-18 smmltd">Days</span> </div><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">03</span><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">28</span><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div><div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">44</span><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div></div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- Proudly Section End  -->

      <!-- Bonus Section Header Start -->
      <div class="bonus-header">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-10 mx-auto heading-bg text-center">
                  <div class="f-24 f-md-36 lh140 w700"> When You Purchase MailZilo, You Also Get <br class="hidden-xs"> Instant Access To These 20 Exclusive Bonuses</div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus Section Header End -->

      <!-- Bonus #1 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 1</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus1.webp" class="img-fluid mx-auto d-block" alt="Bonus1">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Your own@SEO
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">With this simple plugin you can get the true insight on your web traffic efforts in only seconds!</span> Watch as your social network shares increase, your google PageRank and more. </li>
                           <li>Get a clear vision on what you need to start focusing on with your SEO efforts. Start more effective backlinks from Facebook, Twitter, Google and more.</li>
                           <li>You will get all of the most important stats that you need to know for your SEO web traffic.</li>
                           <li>Focus on the amount of shares you have on popular social sites like Facebook, Twitter, Stumble Upon and more.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #1 Section End -->

      <!-- Bonus #2 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 2</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30">
                  <div class="row">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="assets/images/bonus2.webp" class="img-fluid mx-auto d-block" alt="Bonus2">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md0 text-capitalize">
                        Email Niche Plus
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">Owning a business has many advantages from being able to set your own hours to have the control to sell what you want. </span> </li>
                           <li>Unfortunately, too many new business owners fail within their first year. </li>
                           <li>While it isn't for lack of effort, those looking to start a new online business fail to complete the crucial first step; they fail to research to find a viable and profitable niche.</li>
                           <li>This comprehensive guide covers everything you need to know for finding your niche so you can stand out and create success faster.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #2 Section End -->

      <!-- Bonus #3 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 3</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus3.webp" class="img-fluid mx-auto d-block " alt="Bonus3">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Grab your Traffic Booster
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">Businesses both large and small are always hoping that their target audience will be able to find their site among the thousands of websites they are competing against.</span></li>
                           <li>One of the best ways to do this is to utilize the free and paid methods for boosting website traffic. </li>
                           <li>However, like so many online marketing methods, it isn't always clear on how to do this. Finding an effective way to boost the traffic to your website can not only be confusing, but it can also be a bit frustrating.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #3 Section End -->

      <!-- Bonus #4 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 4</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="assets/images/bonus4.webp" class="img-fluid mx-auto d-block " alt="Bonus4">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        All in one video marketing kit 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">Most people fail with Internet Marketing because they do not take consistent action.</span> There is a solid and dependable method that has been working for many years and will continue to work in the future.</li>
                           <li>It's time for you to learn how to make $10,000+ per month in 90 days or even less!</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #4 End -->

      <!-- Bonus #5 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 5</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus5.webp" class="img-fluid mx-auto d-block " alt="Bonus5">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Master E Secret kit
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        This is a collection of Internet Marketing Magazines with 380+ pages of quality content!<br><br>
                        <span class="w600">Topics covered:</span>
                           <li class="mt20">Your Online Business Startup</li>
                           <li>Lead Generation Strategies</li>
                           <li>Your Conversion Strategies</li>
                           <li>Network Marketing Guide</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #5 End -->

      <!-- CTA Button Section Start -->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 yellow-clr">"MAILZILO"</span> for an Additional <span class="w700 yellow-clr">$3 Discount</span> on Commercial Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                     <span class="text-center">Grab MailZilo + My 20 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                        <span class="f-14 f-md-18 w500 ">Days</span>
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                        <span class="f-14 f-md-18 w500 ">Hours</span>
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                        <span class="f-14 f-md-18 w500 ">Mins</span>
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                        <span class="f-14 f-md-18 w500 ">Sec</span>
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->

      <!-- Bonus #6 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 6</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="assets/images/bonus6.webp" class="img-fluid mx-auto d-block " alt="Bonus6">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Trendy Headline Bee
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">Learn the Techniques to Make Your Email Subject Line Stand Out Multi Media!</span></li>
                           <li>Your subject line will certainly stand out, and your email will be opened if you make your email unique, useful to the reader, and focused on what the reader either needs to know or wants to know. </li>
                           <li>Maximize your email marketing efforts by simply having the highest results that you haven't experience before. </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #6 End -->

      <!-- Bonus #7 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 7</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus7.webp" class="img-fluid mx-auto d-block " alt="Bonus7">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        LeadPlus 1 click 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">It doesn't matter what kind of business you're in, if you aren't able to generate new leads and turn them into paying customers, your company will never succeed. </span></li>
                           <li>You need to be constantly bringing in new customers if you want your business to thrive.</li>
                           <li>Generating more leads is anything but easy and if you don't have a solid marketing strategy that will drive more traffic to your website, you'll never be able to generate the leads you need for your business to succeed. </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #7 End -->

      <!-- Bonus #8 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 8</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus8.webp" class="img-fluid mx-auto d-block " alt="Bonus8">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Soci Marketing Agency
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">Social media is designed to keep us entertained. Many of us spend hours trawling through content looking to find things that are unique and special to add to our feeds.</span> </li>
                           <li>We are overwhelmed with having so many accounts that we can follow—but not all content is created equal.</li>
                           <li>Some content is much more popular than others; and it stands out on our feeds. Creating fantastic content and posting it at the optimal time with call-to-actions is a key strategy to succeeding on social media.  </li>
                          
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #8 End -->

      <!-- Bonus #9 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 9</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus9.webp" class="img-fluid mx-auto d-block " alt="Bonus9">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Image collection V3 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">Stock Images For You To Use In Your Projects And Your Clients Projects. Plus You Can Resell Them!</span> </li>
                           <li>Many successful online business owners have said that making money online is easy as a piece of cake as long as you have all the ingredients in doing the process.</li>
                           <li>And one of those ingredients is graphics or images which is a huge help in marketing your product or service online using social media networking sites.</li>
                           <li>The challenge is that you can't simply use images for granted or you might end up sued by copyright violation.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #9 End -->

      <!-- Bonus #10 start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 10</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus10.webp" class="img-fluid mx-auto d-block " alt="Bonus10">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        20k Top header collections
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">Ready to go abstract headers for your minisites!</span></li>
                           <li>If you want to make money online, one of the best media to sell or promote affiliate products is through you website or blog. </li>
                           <li>And if you want to stand from the crowded competition of the industry you are into, having a good-looking header for your blog or site is one of the factors to get noticed and remembered.</li>
                           <li>The good news is that inside this product is a bundle of abstract header background that you can use to your own project or website and you also have the authority to resell it to your customers.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #10 End -->

      <!-- CTA Button Section Start -->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 yellow-clr">"MAILZILO"</span> for an Additional <span class="w700 yellow-clr">$3 Discount</span> on Commercial Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                     <span class="text-center">Grab MailZilo + My 20 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr"><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 smmltd">Days</span> </div><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">0300</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div><div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div></div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->

      <!-- Bonus #11 start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 11</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus11.webp" class="img-fluid mx-auto d-block " alt="Bonus11">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Eye Catching sales templates
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>20 Eye Catching WP Sales Page Themes, You Receive The Resell Rights To The Full Package.</li>
                           
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #11 End -->

      <!-- Bonus #12 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 12</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus12.webp" class="img-fluid mx-auto d-block " alt="Bonus12">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Internet EDITION PLUS
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">With more than 40,000 Google searches happening every second, you need to find a way to gain the attention of these potential customers. </span></li>
                           <li>Knowing how your target audience engages with their favorite brands and the things that influence them to buy are the keys to growing your online business.</li>
                           <li>This video course details the 79 actions that you need to take today that will lead you to realizing huge results in your Internet business. </li>
                           <li>It covers everything from validating your business idea, to transitioning your business for growth, to successfully scaling your business to grow beyond your wildest dreams.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #12 End -->

      <!-- Bonus #13 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 13</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus13.webp" class="img-fluid mx-auto d-block " alt="Bonus13">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Build Up Your List 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        Start creating engagement with your new leads by making your landing page memorable and enjoyable!<br><br>
                        <span class="w600">Here is what you can do with this plugin:</span>
                        <li class="mt20">Edit every detail on the fly with the simple options panel for each page</li>
                        <li>Customize all of the content areas that are designed to be readable</li>
                        <li>Choose from a variety of colors for the 'call to action' buttons on your page</li>
                        <li>Load up the form code from any service like Aweber, MailChimp, and more</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #13 End -->

      <!-- Bonus #14 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 14</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus14.webp" class="img-fluid mx-auto d-block " alt="Bonus14">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Content Writer Pro
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">If you run a blog or website you understand the need for writing regular content. While this may sound easy, it is not always easy to come up with ideas of what to write about.</span> </li>
                           <li>Even then you need to know how to write a compelling blog post that will attract attention. </li>
                           <li>When it comes to writing online there are a few differences which you must be aware of. Writing this type of content is different than writing a novel or non-fiction book.</li>
                           <li>Inside this ebook you will find a compilation of 25 writing tips which have been designed to help you become a better writer. </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #14 End -->

      <!-- Bonus #15 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 15</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus15.webp" class="img-fluid mx-auto d-block " alt="Bonus15">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        4k Videos Pack
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        By 2019, Internet Video Traffic will account for 80% of all consumer Internet traffic. Here is an excellent opportunity to leverage the power of Videos and use this medium to catapult your web business to the next level. <br><br>
                        <span class="w600">You can:</span>

                           <li class="mt20">Use them as Background video on your sales page/sales video to enchance its appearance. </li>
                           <li>Use them as Sales video on your Squeeze Page/Landing Page.</li>
                           <li>Generate an additional passive income stream.</li>
                           <li>Use them as Promotional videos  on Youtube, FB, Instagram or any other networking or website</li>
                           
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #15 End -->

      <!-- CTA Button Section Start -->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 yellow-clr">"MAILZILO"</span> for an Additional <span class="w700 yellow-clr">$3 Discount</span> on Commercial Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                     <span class="text-center">Grab MailZilo + My 20 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <span class="f-14 f-md-18 smmltd">Days</span>
                      </div>
                      <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        
                        <span class="f-14 f-md-18 w500 smmltd">Hours</span>
                      </div>
                      <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        
                        <span class="f-14 f-md-18 w500 smmltd">Mins</span>
                      </div>
                      <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        
                        <span class="f-14 f-md-18 w500 smmltd">Sec</span> 
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->

      <!-- Bonus #16 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 16</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus16.webp" class="img-fluid mx-auto d-block " alt="Bonus16">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Boom social media
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">Turn tour Wordpress blog into a social powerhouse! The simple and fast way to increase social conversions.</span></li>
                           <li>Take the social features of some of the highest shared websites like Buzzfeed or UpWorthy and add them to your blog posts.</li>
                           <li>No matter what theme you are using you can add these shortcodes to get all the social share features you need to have viral blog posts.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #16 End -->

      <!-- Bonus #17 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 17</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus17.webp" class="img-fluid mx-auto d-block " alt="Bonus17">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Massive Website Visitors kit
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>An intrinsic understanding of your competition and how to better them is the most important component of any digital marketing strategy. Today, there is such small distances between you and your competition - it's unlikely that you're offering anything that cannot be bought on some other website. </li>
                           <li>Now, when a consumer wants and needs a product or information, all they must do is input their desires into the Google search and peruse the search engine results page.</li>
                           <li>Also, you will learn all the terms you need to know for a proper understanding of the web marketing industry.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #17 End -->

      <!-- Bonus #18 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 18</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus18.webp" class="img-fluid mx-auto d-block " alt="Bonus18">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        High sales Kit
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li><span class="w600">If you want to sell anything online, it's actually not all that hard.</span>
                        Closing a sale is not the problem. Generating consistent, high-value sales, that's the issue. </li>
                        <li>Even if you are able to master the art of online sales, you may still continue to struggle. </li>
                        <li>Using the high ticket sales secrets from this video course, you will learn how to maximize the return that you get for all your efforts. It all boils down to being at the right place, talking about the right things at the right time with the right people.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #18 End -->

      <!-- Bonus #19 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 19</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus19.webp" class="img-fluid mx-auto d-block " alt="Bonus19">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Dynamic page plugin
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">You probably have noticed that most businesses online are listed in directories such as YellowPages only... Now with one wordpress plugin you can create an all-in-one website that will pull in multiple sources and display in one place.</span> </li>
                           <li>This is a stand alone plugin that will create a business website in one landing page. Add tabbed content to keep your visitors staying on one page!</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #19 End -->

      <!-- Bonus #20 start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 20</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus20.webp" class="img-fluid mx-auto d-block " alt="Bonus20">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Affiliate Marketing Secrets
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        Discover How To Make Money With Affiliate Marketing - Even If You Are a Complete Beginner!<br><br>
                        <span class="w600">Topics covered:</span>
                           <li class="mt20">4 Ways you are killing your passive income empire dreams</li>
                           <li>The money is in the list but not in the way you think</li>
                           <li>7 secrets of affiliate success most marketers will not tell you</li>
                           <li>7 Reasons why you should focus on niche selection</li>
                           <li>6 Ways your niche may be holding your affiliate income</li>
                           <li>How to turbocharge your affiliate income in one step</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #20 End -->

      <!-- Huge Woth Section Start -->
      <div class="huge-area mt30 mt-md10">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="f-md-65 f-40 lh120 w700 white-clr">That's Huge Worth of</div>
                  <br>
                  <div class="f-md-60 f-40 lh120 w800 yellow-clr">$3300!</div>
               </div>
            </div>
         </div>
      </div>
      <!-- Huge Worth Section End -->

      <!-- text Area Start -->
      <div class="white-section pb0">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="f-md-36 f-25 lh140 w500">So what are you waiting for? You have a great opportunity <br class="hidden-xs"> ahead + <span class="w700 ">My 20 Bonus Products</span> are making it a <br class="hidden-xs"> <span class="w700 ">completely NO Brainer!!</span></div>
               </div>
            </div>
         </div>
      </div>
      <!-- text Area End -->

      <!-- CTA Button Section Start -->
      <div class="cta-btn-section">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center">
                  <a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab MailZilo + My 20 Exclusive Bonuses</span> 
                  </a>
               </div>
               <div class="col-12 mt15 mt-md20" align="center">
                  <h3 class="f-md-28 f-20 w500 text-center black">My Exclusive Bonuses Are Expiring in...</h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 col-md-10 mx-auto col-12 text-center">
                  <div class="countdown counter-black">
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                     <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                     <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->

      <div class="footer-section">
        <div class="container ">
            <div class="row">
                <div class="col-12 text-center">
                    <svg version="1.1" x="0px" y="0px" viewBox="0 0 1501.16 276.9" style="enable-background:new 0 0 1501.16 276.9; max-height:40px;" xml:space="preserve">
							<style type="text/css">
							.st0{fill:#FFFFFF;}
							</style>
							<g>
							<path class="st0" d="M122.38,159.77l82.88-67.64h39.5v182.43h-45.44V160.68l-73.53,57.2h-6.82l-73.52-57.2v113.87H0V92.13h39.46
							L122.38,159.77z"></path>
							<path class="st0" d="M391.87,92.13l81.03,182.43h-48.87L409,238.51h-81.7l-13.62,36.04h-48.16l73.69-182.43H391.87z M395.71,206.65
							l-29.57-70.91l-26.8,70.91H395.71z"></path>
							<path class="st0" d="M540.93,92.13v182.43h-45.44V92.13H540.93z"></path>
							<path class="st0" d="M722.06,241.12v33.43H582.98V92.13h45.44v149H722.06z"></path>
							<path class="st0" d="M913.75,92.13l-99.48,149h99.48v33.43H736.68l100.26-149H743.6V92.13H913.75z"></path>
							<path class="st0" d="M989.75,92.13v182.43h-45.44V92.13H989.75z"></path>
							<path class="st0" d="M1170.88,241.12v33.43H1031.8V92.13h45.44v149H1170.88z"></path>
							<path class="st0" d="M1332.43,209.97c-2.33,5.08-5.42,9.64-9.28,13.71c-9.7,10.23-22.48,15.35-38.35,15.35
							c-15.94,0-28.8-5.12-38.53-15.35c-9.74-10.22-14.61-23.66-14.61-40.28c0-16.72,4.87-30.17,14.61-40.35
							c5.49-5.75,11.97-9.87,19.44-12.37l-26.14-31.06c-9.05,4.52-17.19,10.6-24.43,18.22c-17.75,18.72-26.63,40.57-26.63,65.56
							c0,25.41,8.94,47.36,26.81,65.81c17.87,18.45,41.03,27.68,69.49,27.68c28.13,0,51.18-9.26,69.13-27.81
							c2.07-2.15,4.04-4.34,5.87-6.58L1332.43,209.97z"></path>
							<path class="st0" d="M1501.16,0l-114.63,241.95l-71.11-84.51l-31.99,29.36l2.17-64.79c67.33-7.59,111.49-65.77,112.17-66.67
							c-41.92,31-125.97,49.4-125.97,49.4l-28.6-33.1L1501.16,0z"></path>
							</g>
							</svg>
                    <div class="f-14 f-md-16 w400 mt20 lh160 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
                </div>
                <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                    <div class="f-14 f-md-16 w400 lh160 white-clr text-xs-center">Copyright © MailZilo 2022</div>
                    <ul class="footer-ul w400 f-14 f-md-16 white-clr text-center text-md-right">
                        <li><a href="https://support.bizomart.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                        <li><a href="http://mailzilo.com/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                        <li><a href="http://mailzilo.com/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                        <li><a href="http://mailzilo.com/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                        <li><a href="http://mailzilo.com/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                        <li><a href="http://mailzilo.com/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                        <li><a href="http://mailzilo.com/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

      <!-- timer --->
      <?php
         if ($now < $exp_date) {
         ?>
      <script type="text/javascript">
         // Count down milliseconds = server_end - server_now = client_end - client_now
         var server_end = <?php echo $exp_date; ?> * 1000;
         var server_now = <?php echo time(); ?> * 1000;
         var client_now = new Date().getTime();
         var end = server_end - server_now + client_now; // this is the real end time
         
         var noob = $('.countdown').length;
         
         var _second = 1000;
         var _minute = _second * 60;
         var _hour = _minute * 60;
         var _day = _hour * 24
         var timer;
         
         function showRemaining() {
         var now = new Date();
         var distance = end - now;
         if (distance < 0) {
         	clearInterval(timer);
         	document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
         	return;
         }
         
         var days = Math.floor(distance / _day);
         var hours = Math.floor((distance % _day) / _hour);
         var minutes = Math.floor((distance % _hour) / _minute);
         var seconds = Math.floor((distance % _minute) / _second);
         if (days < 10) {
         	days = "0" + days;
         }
         if (hours < 10) {
         	hours = "0" + hours;
         }
         if (minutes < 10) {
         	minutes = "0" + minutes;
         }
         if (seconds < 10) {
         	seconds = "0" + seconds;
         }
         var i;
         var countdown = document.getElementsByClassName('countdown');
         for (i = 0; i < noob; i++) {
         	countdown[i].innerHTML = '';
         
         	if (days) {
         		countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + days + '</span><span class="f-14 f-md-18 smmltd">Days</span> </div>';
         	}
         
         	countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + hours + '</span><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>';
         
         	countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">' + minutes + '</span><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>';
         
         	countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">' + seconds + '</span><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>';
         }
         
         }
         timer = setInterval(showRemaining, 1000);
         	
      </script>
      <?php
         } else {
         echo "Times Up";
         }
         ?>
      <!--- timer end-->
   </body>
</html>
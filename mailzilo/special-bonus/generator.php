<!DOCTYPE html>
<html>

   <head>
      <title>Bonus Landing Page Generator</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=9">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
      <meta name="title" content="MailZilo Special Bonuses">
      <meta name="description" content="MailZilo Special Bonuses">
      <meta name="keywords" content="MailZilo Special Bonuses">
      <meta property="og:image" content="https://www.mailzilo.com/special-bonus/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Dr. Amit Pareek">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="MailZilo Special Bonuses">
      <meta property="og:description" content="MailZilo Special Bonuses">
      <meta property="og:image" content="https://www.mailzilo.com/special-bonus/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="MailZilo Special Bonuses">
      <meta property="twitter:description" content="MailZilo Special Bonuses">
      <meta property="twitter:image" content="https://www.mailzilo.com/special-bonus/thumbnail.png">	
      <!-- Shortcut Icon  -->
      <link rel="icon" href="https://cdn.oppyo.com/launches/mailzilo/common_assets/images/favicon.png" type="image/png">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
      <!--Load External CSS -->
      <link rel="stylesheet" href="https://cdn.oppyo.com/launches/writerarc/common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" href="assets/css/bonus-style.css" type="text/css" />
      <script src="https://cdn.oppyo.com/launches/writerarc/common_assets/js/jquery.min.js"></script>

   </head>

   <body>
      <div class="whitesection">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
               <svg version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1501.16 276.9" style="enable-background:new 0 0 1501.16 276.9; max-height:40px" xml:space="preserve">
                                    <style type="text/css">
                                    .stblue{fill:#0041B5;}
                                    .storg{fill:#FF7E1D;}
                                    </style>
                                    <g>
                                    <path class="stblue" d="M122.38,159.77l82.88-67.64h39.5v182.43h-45.44V160.68l-73.53,57.2h-6.82l-73.52-57.2v113.87H0V92.13h39.46
                                    L122.38,159.77z"></path>
                                    <path class="stblue" d="M391.87,92.13l81.03,182.43h-48.87L409,238.51h-81.7l-13.62,36.04h-48.16l73.69-182.43H391.87z M395.71,206.65
                                    l-29.57-70.91l-26.8,70.91H395.71z"></path>
                                    <path class="stblue" d="M540.93,92.13v182.43h-45.44V92.13H540.93z"></path>
                                    <path class="stblue" d="M722.06,241.12v33.43H582.98V92.13h45.44v149H722.06z"></path>
                                    <path class="storg" d="M913.75,92.13l-99.48,149h99.48v33.43H736.68l100.26-149H743.6V92.13H913.75z"></path>
                                    <path class="storg" d="M989.75,92.13v182.43h-45.44V92.13H989.75z"></path>
                                    <path class="storg" d="M1170.88,241.12v33.43H1031.8V92.13h45.44v149H1170.88z"></path>
                                    <path class="storg" d="M1332.43,209.97c-2.33,5.08-5.42,9.64-9.28,13.71c-9.7,10.23-22.48,15.35-38.35,15.35
                                    c-15.94,0-28.8-5.12-38.53-15.35c-9.74-10.22-14.61-23.66-14.61-40.28c0-16.72,4.87-30.17,14.61-40.35
                                    c5.49-5.75,11.97-9.87,19.44-12.37l-26.14-31.06c-9.05,4.52-17.19,10.6-24.43,18.22c-17.75,18.72-26.63,40.57-26.63,65.56
                                    c0,25.41,8.94,47.36,26.81,65.81c17.87,18.45,41.03,27.68,69.49,27.68c28.13,0,51.18-9.26,69.13-27.81
                                    c2.07-2.15,4.04-4.34,5.87-6.58L1332.43,209.97z"></path>
                                    <path class="stblue" d="M1501.16,0l-114.63,241.95l-71.11-84.51l-31.99,29.36l2.17-64.79c67.33-7.59,111.49-65.77,112.17-66.67
                                    c-41.92,31-125.97,49.4-125.97,49.4l-28.6-33.1L1501.16,0z"></path>
                                    </g>
                                    </svg>
               </div>
               <div class="col-12 f-md-45 f-28 w700 text-center black-clr lh150 mt20">
                  Here's How You Can Generate <br class="d-none d-md-block"> Your Own Bonus Page Easily
               </div>
            </div>
         </div>
      </div>

      <div class="formsection">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-6 col-12">
                  <img src="assets/images/productbox.webp" class="img-fluid mx-auto d-block" alt="ProductBox">
               </div>
               <div class="col-md-6 col-12 mt20 mt-md0">
               <?php //echo "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI']; ?>
               <?php //echo "http://" . $_SERVER['SERVER_NAME'] ."/". basename(__DIR__)."/index.php?name=".$name."&affid=".$affid."&pic=".$filename;  ?>
               <?php
                  if(isset($_POST['submit'])) 
                  {
                     $name=$_REQUEST['name'];
                     $afflink=$_REQUEST['afflink'];
                     //$picname=$_REQUEST['pic'];
                     /*$tmpname=$_FILES['pic']['tmp_name'];
                     $type=$_FILES['pic']['type'];
                     $size=$_FILES['pic']['size']/1024;
                     $rand=rand(1111111,9999999999);*/
                     if($afflink=="")
                     {
                        echo 'Please fill the details.';
                     }
                     else
                     {
                        /*if(($type!="image/jpg" && $type!="image/jpeg" && $type!="image/png" && $type!="image/gif" && $type!="image/bmp") or $size>1024)
                        {
                           echo "Please upload image file (size must be less than 1 MB)";	
                        }
                        else
                        {*/
                           //$filename=$rand."-".$picname;
                           //move_uploaded_file($tmpname,"images/".$filename);
                           $url="https://mailzilo.com/special-bonus/?afflink=".trim($afflink)."&name=".urlencode(trim($name));
                           echo "<a target='_blank' href=".$url.">".$url."</a><br>";
                           //header("Location:$url");
                        //}	
                     }
                  }
               ?>
               <form class="row" action="" method="post" enctype="multipart/form-data">
                  <div class="col-12 form_area ">
                     <div class="col-12 clear">
                        <input type="text" name="name" placeholder="Your Name..." required>
                     </div>

                     <div class="col-12 mt20 clear">
                        <input type="text" name="afflink" placeholder="Your Affiliate Link..." required>
                     </div>

                     <div class="col-12 f-24 f-md-30 white-clr center-block mt10 mt-md20 w500 clear">
                        <input type="submit" value="Generate Page" name="submit" class="f-md-30 f-24" />
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>

   <!--Footer Section Start -->
   <div class="footer-section">
        <div class="container ">
            <div class="row">
                <div class="col-12 text-center">
                    <svg version="1.1" x="0px" y="0px" viewBox="0 0 1501.16 276.9" style="enable-background:new 0 0 1501.16 276.9; max-height:40px;" xml:space="preserve">
                        <style type="text/css">
                        .st0{fill:#FFFFFF;}
                        </style>
                        <g>
                            <path class="st0" d="M122.38,159.77l82.88-67.64h39.5v182.43h-45.44V160.68l-73.53,57.2h-6.82l-73.52-57.2v113.87H0V92.13h39.46
                            L122.38,159.77z"></path>
                            <path class="st0" d="M391.87,92.13l81.03,182.43h-48.87L409,238.51h-81.7l-13.62,36.04h-48.16l73.69-182.43H391.87z M395.71,206.65
                            l-29.57-70.91l-26.8,70.91H395.71z"></path>
                            <path class="st0" d="M540.93,92.13v182.43h-45.44V92.13H540.93z"></path>
                            <path class="st0" d="M722.06,241.12v33.43H582.98V92.13h45.44v149H722.06z"></path>
                            <path class="st0" d="M913.75,92.13l-99.48,149h99.48v33.43H736.68l100.26-149H743.6V92.13H913.75z"></path>
                            <path class="st0" d="M989.75,92.13v182.43h-45.44V92.13H989.75z"></path>
                            <path class="st0" d="M1170.88,241.12v33.43H1031.8V92.13h45.44v149H1170.88z"></path>
                            <path class="st0" d="M1332.43,209.97c-2.33,5.08-5.42,9.64-9.28,13.71c-9.7,10.23-22.48,15.35-38.35,15.35
                            c-15.94,0-28.8-5.12-38.53-15.35c-9.74-10.22-14.61-23.66-14.61-40.28c0-16.72,4.87-30.17,14.61-40.35
                            c5.49-5.75,11.97-9.87,19.44-12.37l-26.14-31.06c-9.05,4.52-17.19,10.6-24.43,18.22c-17.75,18.72-26.63,40.57-26.63,65.56
                            c0,25.41,8.94,47.36,26.81,65.81c17.87,18.45,41.03,27.68,69.49,27.68c28.13,0,51.18-9.26,69.13-27.81
                            c2.07-2.15,4.04-4.34,5.87-6.58L1332.43,209.97z"></path>
                            <path class="st0" d="M1501.16,0l-114.63,241.95l-71.11-84.51l-31.99,29.36l2.17-64.79c67.33-7.59,111.49-65.77,112.17-66.67
                            c-41.92,31-125.97,49.4-125.97,49.4l-28.6-33.1L1501.16,0z"></path>
                        </g>
                    </svg>
                    <div class="f-14 f-md-16 w400 mt20 lh150 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
                </div>
                <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                    <div class="f-14 f-md-16 w400 lh150 white-clr text-xs-center">Copyright © MailZilo 2022</div>
                    <ul class="footer-ul w400 f-14 f-md-16 white-clr text-center text-md-right">
                        <li><a href="https://support.bizomart.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                        <li><a href="http://mailzilo.com/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                        <li><a href="http://mailzilo.com/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                        <li><a href="http://mailzilo.com/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                        <li><a href="http://mailzilo.com/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                        <li><a href="http://mailzilo.com/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                        <li><a href="http://mailzilo.com/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
   <!--Footer Section End -->
</body>
</html>
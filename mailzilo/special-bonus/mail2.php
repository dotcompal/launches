<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <!-- Tell the browser to be responsive to screen width -->
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
      <meta name="title" content="MailZilo Bonuses">
      <meta name="description" content="MailZilo Bonuses">
      <meta name="keywords" content="Revealing: Breath-Taking A.I. Technology That Creates Stunning Content for Any Local or Online Niche At Low One-Time Fee">
      <meta property="og:image" content="https://www.mailzilo.com/special-bonus/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Ayush">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="MailZilo Bonuses">
      <meta property="og:description" content="Revealing: Breath-Taking A.I. Technology That Creates Stunning Content for Any Local or Online Niche At Low One-Time Fee">
      <meta property="og:image" content="https://www.mailzilo.com/special-bonus/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="MailZilo Bonuses">
      <meta property="twitter:description" content="Revealing: Breath-Taking A.I. Technology That Creates Stunning Content for Any Local or Online Niche At Low One-Time Fee">
      <meta property="twitter:image" content="https://www.mailzilo.com/special-bonus/thumbnail.png">
      <title>MailZilo Bonuses</title>
      <!-- Shortcut Icon  -->
      <link rel="icon" href="https://cdn.oppyo.com/launches/mailzilo/common_assets/images/favicon.png">
      <!-- Css CDN Load Link -->
      <link rel="stylesheet" href="https://cdn.oppyo.com/launches/writerarc/common_assets/css/bootstrap.min.css" type="text/css">
      
      <link rel="stylesheet" type="text/css" href="assets/css/bonus-style.css">
      <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
      <script src="https://cdn.oppyo.com/launches/writerarc/common_assets/js/jquery.min.js"></script>
      <script src="assets/js/jquery.min.js"></script>
   </head>
   <body>
   
      <!-- New Timer  Start-->
      <?php
         $date = 'September 23 2022 11:59 AM EST';
         $exp_date = strtotime($date);
         $now = time();  
         /*
         
         $date = date('F d Y g:i:s A eO');
         $rand_time_add = rand(700, 1200);
         $exp_date = strtotime($date) + $rand_time_add;
         $now = time();*/
         
         if ($now < $exp_date) {
         ?>
      <?php
         } else {
         	echo "Times Up";
         }
         ?>
      <!-- New Timer End -->
      <?php
         if(!isset($_GET['afflink'])){
         $_GET['afflink'] = 'https://warriorplus.com/o2/a/w9kmvh/0';
         $_GET['name'] = 'Ayush';      
         }
         ?>

<!-- Header Section Start -->   
<div class="main-header">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="text-center">
                     <div class="f-md-32 f-20 lh140 w400 white-clr d-block d-md-flex align-items-center justify-content-center">
                        <span class="w600"><?php echo $_GET['name'];?>'s</span> &nbsp;special bonus for &nbsp;
                        <svg version="1.1" x="0px" y="0px" viewBox="0 0 1501.16 276.9" style="enable-background:new 0 0 1501.16 276.9; max-height:40px;" xml:space="preserve">
                           <style type="text/css">
                              .st0{fill:#FFFFFF;}
                           </style>
                           <g>
                              <path class="st0" d="M122.38,159.77l82.88-67.64h39.5v182.43h-45.44V160.68l-73.53,57.2h-6.82l-73.52-57.2v113.87H0V92.13h39.46
                              L122.38,159.77z"></path>
                              <path class="st0" d="M391.87,92.13l81.03,182.43h-48.87L409,238.51h-81.7l-13.62,36.04h-48.16l73.69-182.43H391.87z M395.71,206.65
                              l-29.57-70.91l-26.8,70.91H395.71z"></path>
                              <path class="st0" d="M540.93,92.13v182.43h-45.44V92.13H540.93z"></path>
                              <path class="st0" d="M722.06,241.12v33.43H582.98V92.13h45.44v149H722.06z"></path>
                              <path class="st0" d="M913.75,92.13l-99.48,149h99.48v33.43H736.68l100.26-149H743.6V92.13H913.75z"></path>
                              <path class="st0" d="M989.75,92.13v182.43h-45.44V92.13H989.75z"></path>
                              <path class="st0" d="M1170.88,241.12v33.43H1031.8V92.13h45.44v149H1170.88z"></path>
                              <path class="st0" d="M1332.43,209.97c-2.33,5.08-5.42,9.64-9.28,13.71c-9.7,10.23-22.48,15.35-38.35,15.35
                              c-15.94,0-28.8-5.12-38.53-15.35c-9.74-10.22-14.61-23.66-14.61-40.28c0-16.72,4.87-30.17,14.61-40.35
                              c5.49-5.75,11.97-9.87,19.44-12.37l-26.14-31.06c-9.05,4.52-17.19,10.6-24.43,18.22c-17.75,18.72-26.63,40.57-26.63,65.56
                              c0,25.41,8.94,47.36,26.81,65.81c17.87,18.45,41.03,27.68,69.49,27.68c28.13,0,51.18-9.26,69.13-27.81
                              c2.07-2.15,4.04-4.34,5.87-6.58L1332.43,209.97z"></path>
                              <path class="st0" d="M1501.16,0l-114.63,241.95l-71.11-84.51l-31.99,29.36l2.17-64.79c67.33-7.59,111.49-65.77,112.17-66.67
                              c-41.92,31-125.97,49.4-125.97,49.4l-28.6-33.1L1501.16,0z"></path>
                           </g>
                        </svg>
                     </div>
                  </div>
                  <div class="preheadline f-md-20 f-18 w600 lh150 mt20 mt-md40">
						   <div class="skew-con d-flex gap20 align-items-center ">
                        <span>Grab My 20 Exclusive Bonuses Before the Deal Ends…</span>
						      <div class="d-none d-md-block">
							      <img src="assets/images/fav.webp" class="img-fluid d-block">
                        </div>
						   </div>
                  </div>
                  <div class="col-12 mt-md30 mt20 f-md-42 f-28 w600 text-center white-clr lh140">
                     <span class="cyan-clr"> Revealing: </span> Advanced Tag & Segment Based Email Software <span class="yellow-clr"> That Gets 4X More Opening & Click </span> At Low One-Time Fee
                  </div>
                  <div class="col-12 mt-md30 mt20  text-center">
                     <div class="f-18 f-md-22 w600 text-center lh150 white-clr">
                        Watch My Quick Review Video
                     </div>
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md30">
               <div class="col-md-10 col-12 mx-auto">
                 <div class="col-12 responsive-video border-video">
                     <iframe src="https://mailzilo.dotcompal.com/video/embed/fy9w5ktacr" style="width:100%; height:100%" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                  </div> 
                   <!-- <img src="assets/images/productbox.webp" class="img-fluid d-block mx-auto">-->
               </div>
            </div>
         </div>
      </div>
      <!-- Header Section End -->

      <!-- header List Section Start -->
      <div class="header-list-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row header-list-block">
                     <div class="col-12 col-md-6">
                        <div class="f-16 f-md-18 lh140 w400">
                           <ul class="bonus-list pl0 m0">
                              <li><span class="w600">Send Unlimited Emails</span> to Unlimited Subscribers</li>
                              <li><span class="w600">Collect Unlimited Leads</span> with Built-In Lead Form</li>
                              <li> <span class="w600">Free SMTP</span> for unrestricted Mailing</li>
                              <li><span class="w600">Upload Unlimited List</span> with Zero Restrictions</li>
                              <li><span class="w600">Get 4X More ROI</span> and Profits than Ever</li>
                              <li><span class="w600">Smart Tagging</span> for Lead Personalisation</li>
                              <li><span class="w600">100% Control</span> on your online business </li>
                           </ul>
                        </div>
                     </div>
                     <div class="col-12 col-md-6">
                        <div class="f-16 f-md-18 lh140 w400">
                           <ul class="bonus-list pl0 m0">
                              <li><span class="w600">Built-in Inline</span> Editor to Craft Beautiful Emails</li>
                              <li><span class="w600">Hassle-Free</span> Subscribers Management</li>
                              <li><span class="w600">100+ High Converting</span> Templates for Webforms & Email</li>
                              <li><span class="w600">GDPR & Can-Spam Compliant</span></li>
                              <li><span class="w600">100% Newbie Friendly</span> & Easy to Use</li>
                              <li><span class="w600">Commercial License Included</span></li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- header List Section End -->

      <!-- Step Section Start -->
      <div class="step-section">
         <div class="container ">
            <div class="row ">
               <div class="col-12 f-28 f-md-45 w600 lh140 black-clr text-center">
                  Replace your old-school email marketing <br class="d-none d-md-block"> techniques with this cutting-edge solution that<br class="d-none d-md-block"> powers your business <span class="orange-clr d-inline-grid">in just 3 easy steps… <img src="assets/images/underline.webp " class="img-fluid"></span>
               </div>
               <div class="col-12 mt30 mt-md80">
                  <div class="row align-items-center">
                     <div class="col-12 col-md-6 relative">
                        <img src="assets/images/step1.webp" class="img-fluid">
                        <div class="f-28 f-md-40 w600 lh140 mt15">
                           Upload
                        </div>
                        <div class="f-18 f-md-20 w400 lh140 mt15">
                           To begin with, all you need to do is upload your subscribers 
                           or generate leads using MailZilo opt-in forms.   
                        </div>
                        <img src="assets/images/left-arrow.webp" class="img-fluid d-none d-md-block ms-auto step-arrow1">
                     </div>
                     <div class="col-12 col-md-6 mt20 mt-md0 ">
                        <img src="assets/images/step-one.webp" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
               <div class="col-12 mt30 mt-md80">
                  <div class="row align-items-center ">
                     <div class="col-12 col-md-6 order-md-2 relative">
                        <img src="assets/images/step2.webp" class="img-fluid">
                        <div class="f-28 f-md-40 w600 lh140 mt15">
                           Select or Create
                        </div>
                        <div class="f-18 f-md-20 w400 lh140 mt15">
                           Select from the list of beautifully designed email templates 
                           or create your own email template.   
                        </div>
                        <img src="assets/images/right-arrow.webp" class="img-fluid d-none d-md-block step-arrow2">
                     </div>
                     <div class="col-md-6 col-md-pull-6 mt20 mt-md0 order-md-1 ">
                        <img src="assets/images/step-two.webp " class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
               <div class="col-12 mt30 mt-md80">
                  <div class="row align-items-center">
                     <div class="col-12 col-md-6">
                        <img src="assets/images/step3.webp" class="img-fluid">
                        <div class="f-28 f-md-40 w600 lh140 mt15">
                           Send & Profit
                        </div>
                        <div class="f-18 f-md-20 w400 lh140 mt15">
                           Send Unlimited Email to your Subscriber or Schedule it for later.
                        </div>
                     </div>
                     <div class="col-12 col-md-6 mt20 mt-md0 ">
                        <img src="assets/images/step-three.webp " class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md60 text-center">
                  <div class="d-flex gap-2 gap-md-4 justify-content-center flex-wrap">
                     <div class="d-flex align-items-center gap-3 justify-content-center">
                        <img src="https://cdn.oppyo.com/launches/writerarc/bizdrive/cross.webp" alt="Cross" class="img-fluid d-block">
                        <div class="f-md-28 w700 lh140 f-20 red-clr"> No SMTP</div>
                     </div>
                     <div class="d-flex align-items-center gap-3 justify-content-center">
                        <img src="https://cdn.oppyo.com/launches/writerarc/bizdrive/cross.webp" alt="Cross" class="img-fluid d-block">
                        <div class="f-md-28 w700 lh140 f-20 red-clr"> No Installation</div>
                     </div>
                     <div class="d-flex align-items-center gap-3 justify-content-center">
                        <img src="https://cdn.oppyo.com/launches/writerarc/bizdrive/cross.webp" alt="Cross" class="img-fluid d-block">
                        <div class="f-md-28 w700 lh140 f-20 red-clr">No Other Techie Stuff</div>
                     </div>
                  </div>
                  <div class="f-20 f-md-28 w500 lh140 mt20">It's EASY, FAST & Built with LOVE.</div>
               </div>
            </div>
         </div>
      </div>
      <!-- Step Section End -->

      <!-- CTA Btn Start-->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 yellow-clr">"LOVEMAIL"</span> for an Additional <span class="w700 yellow-clr">$5 Discount</span> on Commercial Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                     <span class="text-center">Grab MailZilo + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                           <br>
                        <span class="f-14 f-md-18 smmltd">Days</span>
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Sec</span>
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Btn End -->

      <div class="proof-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 ">
                  <div class="f-28 f-md-45 w600 lh140 black-clr text-center">
                  Here is What We’ve Made in the Past 3 Months <br class="d-none d-md-block"> 
                  by Using the Exact Same Technology
                  </div>
               </div>
               <div class="col-12 mx-auto mt20 mt-md50">
                  <img src="assets/images/proof.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>
            <div class="row mt-md70 mt30 align-items-center">
               <div class="col-md-6 f-md-32 f-22 w600 text-center text-md-start">
                  Got 20K+ Visitors in Just 15 Days on My Affiliate Offers Using the Power of Emails  
               </div>
               <div class="col-md-6 mt-md0 mt20">
                  <img src="assets/images/proof1.webp" alt="" class="img-fluid d-block mx-auto">
               </div>
            </div>
            <div class="row mt-md70 mt30 align-items-center">
               <div class="col-md-6 f-md-32 f-22 w600 order-md-2 text-center text-md-start">
                  And Let’s Check Out the Crazy Open And Click Rates 
                  We’ve Got for A Simple Email I Sent Using Mailzilo.
               </div>
               <div class="col-md-6 mt-md0 mt20 order-md-1">
                  <img src="assets/images/proof2.webp" alt="" class="img-fluid d-block mx-auto">
               </div>
            </div>
         </div>
      </div>
      <!-- Proof Section End -->  
      <!-- Testimonial Section Start -->
      <div class="testimonial-section">
         <div class="container ">
            <div class="row ">
               <div class="col-12 f-28 f-md-45 w600 lh140 black-clr text-center">
                  Here's What Industry Experts Have to <br class="d-none d-md-block"> Say About MailZilo
               </div>
            </div>
            <div class="row row-cols-md-2 row-cols-1 gx4">
               <div class="col mt50">
                  <div class="single-testimonial">
                     <p>
                     Finally, I am getting my emails delivered, clicked, and opened. Yes,<span class="w600"> MailZilo is one of the BEST tools</span> I’ve used for my business yet! No more dependency on other email software. Stupidly simple to use. MailZilo made me go crazy. <span class="w600">Five Stars from my side for this product… </span>
                     </p>
                     <div class="client-info">
                        <img src="assets/images/amit-gaikwad.webp" class="img-fluid d-block mx-auto">
                        <div class="client-details">
                        Amit Gaikwad
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col mt50">
                  <div class="single-testimonial">
                     <p>
                     BINGO, I've tested MailZilo for myself, and man, <span class="w600">it delivered exactly as promised.</span> I just recently sent mail to my entire list and <span class="w600">got 20% more open rates</span> with this super-amazing Email Marketing Technology. <span class="w600">Two-Thumbs up for this one…</span> 
                     </p>
                     <div class="client-info">
                        <img src="assets/images/anirudh-bharwa.webp" class="img-fluid d-block mx-auto">
                        <div class="client-details">
                        Anirudh Bharwa
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col mt50">
                  <div class="single-testimonial">
                     <p>
                     MailZilo will surely take the industry by storm and will <span class="w600">help marketers to generate more leads</span> from any Blog, eCommerce, or WordPress site. I like its <span class="w600">latest smart tag feature</span> which enables me to build targeted marketing campaigns. Yet another killer product from Team Ayush & Pranshu… 
                     </p>
                     <div class="client-info">
                        <img src="assets/images/akshat-gupta.webp" class="img-fluid d-block mx-auto">
                        <div class="client-details">
                        Akshat Gupta
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col mt50">
                  <div class="single-testimonial">
                     <p>
                     <span class="w600">Importing Lists without losing any Leads? DONE!<br>
Sending UNLIMITED Emails? DONE!<br>
Want to Generate More Leads? DONE!<br>
Automating Email Marketing Campaigns? That’s DONE as well!<br><br></span>
This software really impressed me with its amazing features. A great option for anyone looking to make the most from email marketing…. 

                     </p>
                     <div class="client-info">
                        <img src="assets/images/uddhab-pramanik.webp" class="img-fluid d-block mx-auto">
                        <div class="client-details">
                        Uddhab Pramanik
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Testimonial Section End -->

      <!-- CTA Btn Start -->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 yellow-clr">"LOVEMAIL"</span> for an Additional <span class="w700 yellow-clr">$5 Discount</span> on Commercial Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                     <span class="text-center">Grab MailZilo + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr"><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">02</span><span class="f-14 f-md-18 smmltd">Days</span> </div><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">05</span><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">14</span><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div><div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">58</span><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div></div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Section End -->

      <!-- Research Section Start -->
      <div class="research-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="reason-box">
                     <div class="f-24 f-md-38 w600 lh140 blue-clr">
                        Email Marketing Yields an
                     </div>
                     <div class="f-28 f-md-45 w700 lh140 black-clr">
                        Average 4300% Return on Investment
                     </div>
                     <div class="f-24 f-md-38 w600 lh140 blue-clr">
                        for businesses worldwide.
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50">
                  <div class="row align-items-center">
                     <div class="col-12 col-md-6">
                        <div class="f-20 f-md-24 w500 lh140 text-center text-md-start">
                           Yes, Emails are the most powerful, profitable, and low-cost marketing tool and get 3 times higher conversions than social media or any other mode of marketing.
                        </div>
                     </div>
                     <div class="col-12 col-md-6 mt20 mt-md0">
                        <img src="assets/images/investment.webp" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md70">
                  <div class="row">
                     <div class="col-12 col-md-7 order-md-2">
                        <div class="f-28 f-md-45 w700 lh140 text-center text-md-start green-clr">
                           That’s Right…
                        </div>
                        <div class="f-20 f-md-24 w500 lh140 text-center text-md-start mt10">
                           Email marketing is the key to getting new customers and creating deeper relationships with your existing customers at a fraction of the cost.
                        </div>
                     </div>
                     <div class="col-12 col-md-5 order-md-1 mt20 mt-md0 relative d-none d-md-block">
                        <img src="assets/images/girl.webp" class="img-fluid d-block mx-auto girl-img">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Research Section End -->

      <!-- Stats Section Start -->
      <div class="stats-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-45 w700 lh140 text-center white-clr stats-headline">Here are Some Eye Openers </div>
               </div>
            </div>
            <div class="row row-cols-1 row-cols-md-2 gap30 mt0 t-md20">
               <div class="col">
                  <div class="stats-block">
                     <div><img src="assets/images/mail.webp" class="img-fluid d-block mx-auto"></div>
                     <div class="f-20 f-md-24 w400 lh140 white-clr">There are <span class="orange-clr w700">4.5+ Billion</span> Daily Email Users</div>
                  </div>
               </div>
               <div class="col">
                  <div class="stats-block">
                     <div><img src="assets/images/mail.webp" class="img-fluid d-block mx-auto"></div>
                     <div class="f-20 f-md-24 w400 lh140 white-clr"><span class="orange-clr w700">350+ Billion Emails</span> are Circulated Every Single Day</div>
                  </div>
               </div>
               <div class="col">
                  <div class="stats-block">
                     <div><img src="assets/images/mail.webp" class="img-fluid d-block mx-auto"></div>
                     <div class="f-20 f-md-24 w400 lh140 white-clr"><span class="orange-clr w700">$400 Million</span> are Spent Every Year on Email Marketing in US Alone</div>
                  </div>
               </div>
               <div class="col">
                  <div class="stats-block">
                     <div><img src="assets/images/mail.webp" class="img-fluid d-block mx-auto"></div>
                     <div class="f-20 f-md-24 w400 lh140 white-clr"><span class="orange-clr w700">95% of Email</span> Users Check Their Emails Daily</div>
                  </div>
               </div>
               <div class="col">
                  <div class="stats-block">
                     <div><img src="assets/images/mail.webp" class="img-fluid d-block mx-auto"></div>
                     <div class="f-20 f-md-24 w400 lh140 white-clr"><span class="orange-clr w700">85% of Businesses</span> Believe that Email Marketing Improves Customer Retention</div>
                  </div>
               </div>
               <div class="col">
                  <div class="stats-block">
                     <div><img src="assets/images/mail.webp" class="img-fluid d-block mx-auto"></div>
                     <div class="f-20 f-md-24 w400 lh140 white-clr"><span class="orange-clr w700">80% of Youth</span> Prefer Business Communications via Email</div>
                  </div>
               </div>
               <div class="col">
                  <div class="stats-block">
                     <div><img src="assets/images/mail.webp" class="img-fluid d-block mx-auto"></div>
                     <div class="f-20 f-md-24 w400 lh140 white-clr"><span class="orange-clr w700">90% of Marketers</span> Use Email as the Primary Channel</div>
                  </div>
               </div>
               <div class="col">
                  <div class="stats-block">
                     <div><img src="assets/images/mail.webp" class="img-fluid d-block mx-auto"></div>
                     <div class="f-20 f-md-24 w400 lh140 white-clr"><span class="orange-clr w700">Since 10+ Years,</span> Email Marketing Generates the Highest ROI for Marketers.</div>
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md80">
               <div class="col-12 f-24 f-md-36 w600 white-clr lh140 text-center">This Show Why Big Businesses <br class="d-none d-md-block">&amp; Marketers are Capitalizing on Email Marketing</div>
            </div>
         </div>
      </div>
      <!-- Stats Section End -->

      <!-- Power Section Start -->
      <div class="power-section">
         <div class="container ">
            <div class="row align-items-center">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-45 w700 lh140 black-clr">
                     And Do You Know How Much People Are Making  <br>
                     <span class="orange-clr">Using the POWER of Emails Marketing?</span>
                  </div>
                  <img src="assets/images/power-line.webp" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 col-md-11 offset-md-1 mt20 mt-md50">
                  <div class="power-block">
                     <div class="f-28 f-md-36 w700 orange-clr lh140 text-center text-md-start">Tom Sather</div>
                     <div class="f-md-20 f-18 w400 lh140 white-clr mt10 text-center text-md-start">
                        He is known for his work as the Director of Return Path which has an Annual Revenue of $10+ Million. Return Path, with emphasis on Emails Marketing, offers unparalleled insights on why emails end up undeliverable and where they end up bouncing.
                     </div>
                     <img src="assets/images/tom.webp" class="img-fluid d-block mx-auto left-img mt20 mt-md0">
                  </div>
               </div>
               <div class="col-12">
                  <img src="assets/images/connect.webp" class="img-fluid d-block mx-auto connect-line">
               </div>
               <div class="col-12 col-md-11 mt20">
                  <div class="power-block right">
                     <div class="f-28 f-md-36 w700 orange-clr lh140 text-center text-md-start">Matthew Lloyd Smith</div>
                     <div class="f-md-20 f-18 w400 lh140 white-clr mt10 text-center text-md-start">Matthew  Lloyd Smith created the insanely helpful website Really Good Emails, which showcases the best in email marketing on the internet. Really Good Emails has a Revenue of over $5 Million.
                     </div>
                     <img src="assets/images/matthew.webp" class="img-fluid d-block mx-auto right-img mt20 mt-md0">
                  </div>
               </div>
               <div class="col-12">
                  <img src="assets/images/connect1.webp" class="img-fluid d-block mx-auto connect-line">
               </div>
               <div class="col-12 col-md-11 offset-md-1 mt20">
                  <div class="power-block">
                     <div class="f-28 f-md-36 w700 orange-clr lh140 text-center text-md-start">Kath Pay</div>
                     <div class="f-md-20 f-18 w400 lh140 white-clr mt10 text-center text-md-start">
                        Kath Pay has been in the email game for over 19 years. She is an email marketing consultant and founder of Holistic Email Marketing. She’s been named one of the top email influencers in the world and makes annual revenue of over $8 Million.
                     </div>
                     <img src="assets/images/kath.webp" class="img-fluid d-block mx-auto left-img mt20 mt-md0">
                  </div>
               </div>
               <div class="col-12 mt20 mt-md100">
                  <div class="f-20 f-md-32 w400 lh140 text-center">And all of that just by using the Power of Email Marketing</div>
                  <div class="f-32 f-md-55 w700 lh140 text-center green-clr my20">Impressive, Right?</div>
                  <div class="f-20 f-md-32 w400 lh140 text-center">So, it can be safely stated that…<br>
                     <span class="w600">Email Marketing is the BIGGEST Income Opportunity Right Now!</span>
                  </div>
               </div>
            </div>  
         </div>
      </div>
      <!-- Power Section End -->

      <!-- Proudly Section Start -->
      <div class="next-gen-sec" id="product">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="pre-heading f-md-32 f-24 w600 text-center white-clr lh140">
                  Proudly Presenting... </div>
               </div>
               <div class="col-12 mt-md20 mt20 text-center">
                  <svg version="1.1" x="0px" y="0px" viewBox="0 0 1501.16 276.9" style="enable-background:new 0 0 1501.16 276.9; max-height:100px;" xml:space="preserve">
                     <style type="text/css">
                     .st0{fill:#FFFFFF;}
                     </style>
                     <g>
                        <path class="st0" d="M122.38,159.77l82.88-67.64h39.5v182.43h-45.44V160.68l-73.53,57.2h-6.82l-73.52-57.2v113.87H0V92.13h39.46
                        L122.38,159.77z"></path>
                        <path class="st0" d="M391.87,92.13l81.03,182.43h-48.87L409,238.51h-81.7l-13.62,36.04h-48.16l73.69-182.43H391.87z M395.71,206.65
                        l-29.57-70.91l-26.8,70.91H395.71z"></path>
                        <path class="st0" d="M540.93,92.13v182.43h-45.44V92.13H540.93z"></path>
                        <path class="st0" d="M722.06,241.12v33.43H582.98V92.13h45.44v149H722.06z"></path>
                        <path class="st0" d="M913.75,92.13l-99.48,149h99.48v33.43H736.68l100.26-149H743.6V92.13H913.75z"></path>
                        <path class="st0" d="M989.75,92.13v182.43h-45.44V92.13H989.75z"></path>
                        <path class="st0" d="M1170.88,241.12v33.43H1031.8V92.13h45.44v149H1170.88z"></path>
                        <path class="st0" d="M1332.43,209.97c-2.33,5.08-5.42,9.64-9.28,13.71c-9.7,10.23-22.48,15.35-38.35,15.35
                        c-15.94,0-28.8-5.12-38.53-15.35c-9.74-10.22-14.61-23.66-14.61-40.28c0-16.72,4.87-30.17,14.61-40.35
                        c5.49-5.75,11.97-9.87,19.44-12.37l-26.14-31.06c-9.05,4.52-17.19,10.6-24.43,18.22c-17.75,18.72-26.63,40.57-26.63,65.56
                        c0,25.41,8.94,47.36,26.81,65.81c17.87,18.45,41.03,27.68,69.49,27.68c28.13,0,51.18-9.26,69.13-27.81
                        c2.07-2.15,4.04-4.34,5.87-6.58L1332.43,209.97z"></path>
                        <path class="st0" d="M1501.16,0l-114.63,241.95l-71.11-84.51l-31.99,29.36l2.17-64.79c67.33-7.59,111.49-65.77,112.17-66.67
                        c-41.92,31-125.97,49.4-125.97,49.4l-28.6-33.1L1501.16,0z"></path>
                     </g>
                  </svg>
               </div>
               <div class="f-md-36 f-24 w600 white-clr lh140 col-12 mt20 mt-md40 text-center">
                  The Most Advanced &amp; Powerful Email Marketing Automation Software that 
                  allows you to Create, Send, Track, and Profit from Email Marketing.
               </div>
               <div class="f-20 f-md-24 lh140 w400 text-center white-clr mt20">              
                  It is a must-have tool in your marketing arsenal that comes with a ONE-TIME FEE, <br class="d-none d-md-block"> and that will send all the existing money-sucking autoresponders back to their nest.
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md50">
               <div class="col-12 col-md-10 mx-auto">
                  <img src="assets/images/product-box.webp" class="img-fluid d-block mx-auto" alt="Product">
               </div>            
            </div>
            <div class="row mt20 mt-md50">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 yellow-clr">"LOVEMAIL"</span> for an Additional <span class="w700 yellow-clr">$5 Discount</span> on Commercial Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                     <span class="text-center">Grab MailZilo + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr"><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">02</span><span class="f-14 f-md-18 smmltd">Days</span> </div><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">03</span><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">28</span><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div><div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">44</span><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div></div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- Proudly Section End  -->

 <!-------Exclusive Bonus----------->
 <div class="exclusive-bonus">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <div class="white-clr f-24 f-md-36 lh140 w600">
                  Exclusive Bonus #1 : Acadmeyio
               </div>
               <div class="f-18 f-md-20 w500 mt20 white-clr">
                  20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
               </div>
            </div>
         </div>
      </div>
   </div>
<!-------Exclusive Bonus Ends----------->
<div class="header-section-acadmeyio">
         <div class="container">
            <div class="row">
               <div class="col-md-12  text-center">
                  <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1311.65 265.5" style="enable-background:new 0 0 1311.65 265.5; max-height:45px;" xml:space="preserve">
                     <style type="text/css">
                        .st0{fill:#FFFFFF;}
                        .st1{fill-rule:evenodd;clip-rule:evenodd;fill:#FFFFFF;}
                     </style>
                     <g>
                        <g>
                           <path class="st0" d="M97.8,186.5h-53l-8.8,25.8H0L51.8,71.9h39.4l51.8,140.4h-36.4L97.8,186.5z M89,160.5l-17.6-52l-17.8,52H89z"></path>
                           <path class="st0" d="M245.8,111.19c9.73,7.93,15.87,18.83,18.4,32.7H228c-1.07-4.8-3.27-8.53-6.6-11.2c-3.33-2.67-7.54-4-12.6-4
                              c-6,0-10.93,2.37-14.8,7.1c-3.87,4.73-5.8,11.63-5.8,20.7c0,9.07,1.93,15.97,5.8,20.7c3.87,4.73,8.8,7.1,14.8,7.1
                              c5.07,0,9.27-1.33,12.6-4c3.33-2.67,5.53-6.4,6.6-11.2h36.2c-2.53,13.87-8.67,24.77-18.4,32.7c-9.73,7.93-21.8,11.9-36.2,11.9
                              c-10.93,0-20.63-2.3-29.1-6.9c-8.47-4.6-15.1-11.23-19.9-19.9c-4.8-8.67-7.2-18.8-7.2-30.4c0-11.73,2.37-21.9,7.1-30.5
                              c4.73-8.6,11.37-15.2,19.9-19.8c8.53-4.6,18.27-6.9,29.2-6.9C224,99.3,236.06,103.26,245.8,111.19z"></path>
                           <path class="st0" d="M349.4,105.09c6.13,3.87,10.67,9.13,13.6,15.8v-20.2h34v111.6h-34v-20.2c-2.93,6.67-7.47,11.93-13.6,15.8
                              c-6.13,3.87-13.47,5.8-22,5.8c-9.2,0-17.44-2.3-24.7-6.9c-7.27-4.6-13-11.23-17.2-19.9c-4.2-8.67-6.3-18.8-6.3-30.4
                              c0-11.73,2.1-21.9,6.3-30.5c4.2-8.6,9.93-15.2,17.2-19.8c7.27-4.6,15.5-6.9,24.7-6.9C335.93,99.3,343.26,101.23,349.4,105.09z
                              M320.7,136.5c-4.47,4.8-6.7,11.47-6.7,20c0,8.53,2.23,15.2,6.7,20c4.47,4.8,10.37,7.2,17.7,7.2c7.2,0,13.1-2.47,17.7-7.4
                              c4.6-4.93,6.9-11.53,6.9-19.8c0-8.4-2.3-15.03-6.9-19.9c-4.6-4.87-10.5-7.3-17.7-7.3C331.06,129.29,325.17,131.69,320.7,136.5z"></path>
                           <path class="st0" d="M488,105.09c6.13,3.87,10.6,9.13,13.4,15.8V64.3h34.2v148h-34.2v-20.2c-2.8,6.67-7.27,11.93-13.4,15.8
                              c-6.13,3.87-13.47,5.8-22,5.8c-9.2,0-17.43-2.3-24.7-6.9c-7.27-4.6-13-11.23-17.2-19.9c-4.2-8.67-6.3-18.8-6.3-30.4
                              c0-11.73,2.1-21.9,6.3-30.5c4.2-8.6,9.93-15.2,17.2-19.8c7.27-4.6,15.5-6.9,24.7-6.9C474.53,99.3,481.86,101.23,488,105.09z
                              M459.3,136.5c-4.47,4.8-6.7,11.47-6.7,20c0,8.53,2.23,15.2,6.7,20c4.46,4.8,10.37,7.2,17.7,7.2c7.2,0,13.1-2.47,17.7-7.4
                              c4.6-4.93,6.9-11.53,6.9-19.8c0-8.4-2.3-15.03-6.9-19.9c-4.6-4.87-10.5-7.3-17.7-7.3C469.66,129.29,463.76,131.69,459.3,136.5z"></path>
                           <path class="st0" d="M667.2,162.69h-77.4c0.4,8.4,2.53,14.43,6.4,18.1c3.87,3.67,8.8,5.5,14.8,5.5c5.07,0,9.27-1.27,12.6-3.8
                              c3.33-2.53,5.53-5.8,6.6-9.8h36.2c-1.47,7.87-4.67,14.9-9.6,21.1c-4.93,6.2-11.2,11.07-18.8,14.6c-7.6,3.53-16.07,5.3-25.4,5.3
                              c-10.94,0-20.63-2.3-29.1-6.9c-8.47-4.6-15.1-11.23-19.9-19.9c-4.8-8.67-7.2-18.8-7.2-30.4c0-11.73,2.37-21.9,7.1-30.5
                              c4.73-8.6,11.37-15.2,19.9-19.8c8.53-4.6,18.27-6.9,29.2-6.9c11.06,0,20.8,2.27,29.2,6.8c8.4,4.53,14.9,10.9,19.5,19.1
                              c4.6,8.2,6.9,17.63,6.9,28.3C668.2,156.29,667.86,159.36,667.2,162.69z M627.7,131.79c-4.07-3.67-9.1-5.5-15.1-5.5
                              c-6.27,0-11.47,1.87-15.6,5.6c-4.13,3.73-6.47,9.2-7,16.4h43.6C633.73,140.96,631.76,135.46,627.7,131.79z"></path>
                           <path class="st0" d="M865.99,112.19c7.87,8.47,11.8,20.23,11.8,35.3v64.8h-34v-60.8c0-7.07-1.9-12.57-5.7-16.5
                              c-3.8-3.93-8.97-5.9-15.5-5.9c-6.8,0-12.17,2.1-16.1,6.3c-3.93,4.2-5.9,10.1-5.9,17.7v59.2h-34.2v-60.8
                              c0-7.07-1.87-12.57-5.6-16.5c-3.73-3.93-8.87-5.9-15.4-5.9c-6.8,0-12.2,2.07-16.2,6.2c-4,4.13-6,10.07-6,17.8v59.2h-34.2v-111.6
                              h34.2v19c2.93-6.27,7.43-11.2,13.5-14.8c6.06-3.6,13.17-5.4,21.3-5.4c8.53,0,16.07,1.97,22.6,5.9c6.53,3.93,11.46,9.57,14.8,16.9
                              c3.87-6.93,9.17-12.47,15.9-16.6c6.73-4.13,14.17-6.2,22.3-6.2C847.33,99.5,858.13,103.73,865.99,112.19z"></path>
                           <path class="st0" d="M905.99,56.4c3.73-3.4,8.67-5.1,14.8-5.1c6.13,0,11.06,1.7,14.8,5.1c3.73,3.4,5.6,7.7,5.6,12.9
                              c0,5.07-1.87,9.3-5.6,12.7c-3.73,3.4-8.67,5.1-14.8,5.1c-6.13,0-11.07-1.7-14.8-5.1c-3.73-3.4-5.6-7.63-5.6-12.7
                              C900.39,64.09,902.26,59.8,905.99,56.4z M937.79,100.69v111.6h-34.2v-111.6H937.79z"></path>
                           <path class="st0" d="M990.19,100.69l26.8,68.4l25-68.4h37.8l-69.6,164.8h-37.6l26.2-57.4l-46.8-107.4H990.19z"></path>
                           <path class="st0" d="M1176.09,106.19c8.73,4.6,15.6,11.23,20.6,19.9c5,8.67,7.5,18.8,7.5,30.4c0,11.6-2.5,21.73-7.5,30.4
                              c-5,8.67-11.87,15.3-20.6,19.9c-8.73,4.6-18.63,6.9-29.7,6.9c-11.07,0-21-2.3-29.8-6.9c-8.8-4.6-15.7-11.23-20.7-19.9
                              c-5-8.67-7.5-18.8-7.5-30.4c0-11.6,2.5-21.73,7.5-30.4c5-8.67,11.9-15.3,20.7-19.9c8.8-4.6,18.73-6.9,29.8-6.9
                              C1157.45,99.3,1167.35,101.59,1176.09,106.19z M1129.89,136c-4.47,4.73-6.7,11.57-6.7,20.5c0,8.93,2.23,15.73,6.7,20.4
                              c4.47,4.67,9.97,7,16.5,7c6.53,0,12-2.33,16.4-7c4.4-4.67,6.6-11.47,6.6-20.4c0-8.93-2.2-15.77-6.6-20.5
                              c-4.4-4.73-9.87-7.1-16.4-7.1C1139.85,128.9,1134.35,131.26,1129.89,136z"></path>
                        </g>
                        <path class="st1" d="M1194.92,0l116.73,110.41l-35.46,4.1c4.41,13.79,5,32.47,1.82,48.73l4.22-0.62l-3.73,31.86
                           c-26.34,0.96-21.56-4.57-18.74-28.59l4.69-0.68c3.5-16.75,7.12-41.11,1.73-56.35c-2.23-6.28-6.22-11.98-11.28-17
                           c-4.72-4.46-9.96-8.39-15.44-11.72h-0.02l-0.29-0.18l-0.02-0.02c-12.42-7.26-26.54-11.79-37.48-13.04
                           c-7.65-0.65-14.13,0.37-18.28,3.24c18.61-4.17,69.25,12.94,78.85,42.95c0.31,0.98,0.55,1.95,0.73,2.96l-26.47,3.06l-4.44,23.25
                           c-4.97-10.88-12.05-20.89-21.28-30.01c-19.99-19.8-44.19-29.68-72.6-29.68c-14.05,0-27.03,2.38-38.9,7.12l14.25-16.14l-58.62-56.4
                           L1194.92,0z"></path>
                     </g>
                  </svg>
               </div>
              
               <div class="col-12 text-center lh150 mt20 mt-md50">
                  <div class="pre-heading1 f-16 f-md-18 w700 lh160 black-clr">
                  The Simple 60 Seconds Academy Gets Automated Sales Daily (Anyone Can Do This)
                  </div>
               </div>
               <div class="col-12 mt-md35 mt20 f-md-36 f-28 w600 text-center white-clr lh150">
                  <u>EXPOSED</u>–A Breakthrough 1-Click Software That <br><span class="w900 f-md-45 yellow-clr">Makes Us $525/Day Over &amp; Over Again</span><br> <span class="lh130">
                  By Creating Beautiful Udemy<sup class="f-20">TM</sup> Like Sites Pre-Loaded with 400+ HOT Video Courses in JUST 60 Seconds…
                  </span> 
               </div>
               <div class="col-12 mt-md25 mt20">
                  <div class="f-20 f-md-22 w500 text-center lh150 white-clr2">
                     No Course Creation. No Camera or Editing. No Tech Hassles Ever... Even A 100%<br class="d-none d-md-block"> <span class="w700">Beginner or Camera SHY Can Start Profiting</span> Right Away 
                  </div>
               </div>
               <div class="col-md-10 mx-auto col-12 mt-md50 mt20">
                  <!-- <img src="assets/images/productbox.webp" class="img-fluid d-block mx-auto"> -->
                  <div class="responsive-video">
                     <iframe src="https://academiyo.dotcompal.com/video/embed/4inqz8dc2v" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                        box-shadow: none !important;" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                     </div>
               </div>
               <div class="col-12 f-20 f-md-26 w700 lh150 text-center white-clr mt-md50 mt20">
               LIMITED TIME - FREE COMMERCIAL LICENSE INCLUDED 
               </div>
            </div>
         </div>
      </div>


      <!--WebPrimo Header Section End -->
    <div class="gr-1">
      <div class="container-fluid p0">
         <picture>
            <source media="(min-width:768px)" srcset="assets/images/academiyo.webp">
            <source media="(min-width:320px)" srcset="assets/images/academiyo-mview.webp" style="width:100%" class="vidvee-mview">
            <img src="assets/images/academiyo.webp" alt="Vidvee Steps" class="img-fluid" style="width: 100%;">
         </picture>
      </div>
   </div>
   <!-------Exclusive Bonus----------->
   <div class="exclusive-bonus">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <div class="white-clr f-24 f-md-36 lh140 w600">
                  Exclusive Bonus #2 : WebPull
               </div>
               <div class="f-18 f-md-20 w500 mt20 white-clr">
                  20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
               </div>
            </div>
         </div>
      </div>
   </div>
<!-------Exclusive Bonus Ends----------->



<!--WebPull Header Section Start -->
<div class="webpull-header-section">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 394.2 100" style="enable-background:new 0 0 394.2 100; max-height:60px" xml:space="preserve">
                  <style type="text/css">
                     .st00 {
                        fill: #FFFFFF;
                     }

                     .st11 {
                        fill: url(#SVGID_1_);
                     }

                     .st22 {
                        fill: url(#SVGID_00000021093791325859232720000015953972570224053676_);
                     }

                     .st33 {
                        fill: #FFC802;
                     }

                     .st44 {
                        fill: url(#SVGID_00000003068462962921929030000010945183465240898967_);
                     }
                  </style>
                  <path class="st00" d="M117.7,37.9l-5,12.4H92.9v12.4h18.6l-5,12.4H92.9v12.4h24.8l-5,12.5H92.9H80.5V75.1V62.7V37.9H117.7z">
                  </path>
                  <path class="st00" d="M323.2,37.9v49.7H348V100h-37.3V37.9H323.2z"></path>
                  <path class="st00" d="M369.4,37.9v49.7h24.8V100h-37.3V37.9H369.4z"></path>
                  <g>
                     <g>
                        <polygon class="st00" points="59.1,71.4 52.4,54.4 59.1,37.9 72.6,37.9   "></polygon>
                        <polygon class="st00" points="29.4,54.7 36.3,37.9 54.5,82.9 47.7,100   "></polygon>
                        <polygon class="st00" points="24.8,100 31.6,82.8 13.4,37.9 0,37.9   "></polygon>
                     </g>
                  </g>
                  <path class="st00" d="M222.8,44.3c-4.3-4.3-9.4-6.4-15.4-6.4h-9.3h-12.5v21.7v21.7V100h12.5V81.3h0V68.9h0v-9.3v-9.3h9.3  c2.6,0,4.8,0.9,6.6,2.8c1.8,1.8,2.7,4,2.7,6.6c0,2.6-0.9,4.8-2.7,6.6c-1.8,1.8-4,2.7-6.6,2.7h-0.7v12.4h0.7c6,0,11.1-2.1,15.4-6.4  c4.3-4.3,6.4-9.4,6.4-15.4C229.2,53.6,227.1,48.5,222.8,44.3z">
                  </path>
                  <rect x="252.1" y="31.8" transform="matrix(0.4226 -0.9063 0.9063 0.4226 114.0209 255.0236)" class="st00" width="10.2" height="12.5"></rect>
                  <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="249.3741" y1="97.3262" x2="270.7371" y2="53.5256">
                     <stop offset="0" style="stop-color:#FA8460"></stop>
                     <stop offset="1" style="stop-color:#FCB121"></stop>
                  </linearGradient>
                  <path class="st11" d="M271.7,80.3c-1.4,3.1-3.8,5.2-7,6.4c-3.2,1.2-6.4,1-9.5-0.4c-3.1-1.4-5.2-3.8-6.4-7c-1.2-3.2-1-6.4,0.4-9.5  l9.9-21.2l-11.3-5.3l-9.9,21.2c-2.9,6.2-3.2,12.6-0.8,19c2.3,6.5,6.6,11.1,12.9,14c6.2,2.9,12.5,3.2,19,0.8  c6.5-2.3,11.2-6.6,14.1-12.8l9.9-21.2l-11.3-5.3L271.7,80.3z">
                  </path>
                  <rect x="285.9" y="47.6" transform="matrix(0.4226 -0.9063 0.9063 0.4226 119.2494 294.738)" class="st00" width="10.2" height="12.5"></rect>
                  <path class="st00" d="M173.3,76.3c-0.8-1.7-1.7-3.2-2.9-4.5c-1.2-1.3-2.5-2.3-4-3.1c-1.5-0.8-3-1.2-4.5-1.3c1.5-0.1,2.8-0.5,4.1-1.3  c1.2-0.8,2.3-1.8,3.2-3c0.9-1.2,1.6-2.5,2.1-3.9c0.5-1.4,0.8-2.8,0.8-4c0-3.5-0.5-6.4-1.4-8.6c-1-2.2-2.3-4-3.9-5.3  c-1.6-1.3-3.6-2.2-5.8-2.6c-2.2-0.5-4.6-0.7-7-0.7h-27.3v10.5h11.9v0h15.4c0.8,0,1.7,0.2,2.4,0.6c0.8,0.4,1.4,0.9,2,1.5  c0.6,0.6,1,1.4,1.4,2.2c0.3,0.9,0.5,1.7,0.5,2.6c0,0.9-0.2,1.8-0.5,2.8c-0.3,0.9-0.8,1.8-1.4,2.5c-0.6,0.7-1.3,1.3-2.2,1.8  c-0.9,0.5-1.9,0.7-3.1,0.7h-9.1v10.5h10c2.7,0,4.9,0.7,6.4,2.1c1.6,1.4,2.3,3.2,2.3,5.3c0,1-0.2,2-0.6,3c-0.4,1-0.9,1.9-1.6,2.7  c-0.7,0.8-1.5,1.5-2.4,2c-0.9,0.5-1.9,0.7-2.9,0.7h-16.8h0V73.7h0V63.2h0v-9.3h-11.9V100h9.6h2.3h16.8c2.7,0,5.2-0.3,7.5-0.9  c2.3-0.6,4.4-1.6,6.2-3.1c1.8-1.4,3.1-3.3,4.2-5.7c1-2.3,1.5-5.2,1.5-8.6C174.4,79.8,174,78,173.3,76.3z">
                  </path>
                  <g>
                     <linearGradient id="SVGID_00000142155450205615942230000010409392281687221429_" gradientUnits="userSpaceOnUse" x1="270.7616" y1="25.6663" x2="296.63" y2="25.6663">
                        <stop offset="0" style="stop-color:#FA8460"></stop>
                        <stop offset="4.692155e-02" style="stop-color:#FA9150"></stop>
                        <stop offset="0.1306" style="stop-color:#FBA43A"></stop>
                        <stop offset="0.2295" style="stop-color:#FBB229"></stop>
                        <stop offset="0.3519" style="stop-color:#FCBC1E"></stop>
                        <stop offset="0.5235" style="stop-color:#FCC117"></stop>
                        <stop offset="1" style="stop-color:#FCC315"></stop>
                     </linearGradient>
                     <path style="fill:url(#SVGID_00000142155450205615942230000010409392281687221429_);" d="M289.6,14.2c-2.1-1.1-4.3-1.5-6.5-1.4   c-0.1,0-0.1,0-0.2,0c-0.1,0-0.2,0-0.2,0c0,0-0.1,0-0.1,0c-0.1,0-0.2,0-0.2,0c-0.1,0-0.2,0-0.3,0c-0.1,0-0.2,0-0.2,0   c-0.3,0-0.5,0.1-0.8,0.1c-0.1,0-0.2,0-0.3,0.1c-0.1,0-0.2,0-0.3,0.1c-0.1,0-0.3,0.1-0.4,0.1c-0.1,0-0.2,0-0.2,0.1   c-0.1,0-0.2,0.1-0.3,0.1c-0.2,0.1-0.5,0.2-0.7,0.3c0,0,0,0,0,0c-0.3,0.1-0.6,0.3-0.9,0.4c-0.3,0.1-0.6,0.3-0.9,0.5   c-0.1,0.1-0.3,0.2-0.4,0.2c-0.1,0.1-0.2,0.2-0.4,0.2c-0.1,0.1-0.2,0.1-0.2,0.2c0,0-0.1,0-0.1,0.1c-0.1,0.1-0.2,0.1-0.2,0.2   c-0.1,0-0.1,0.1-0.2,0.2c-0.1,0.1-0.2,0.2-0.3,0.3c-0.1,0-0.1,0.1-0.2,0.2c-0.5,0.4-0.9,0.9-1.3,1.4c-0.1,0.1-0.1,0.1-0.2,0.2   c0,0-0.1,0.1-0.1,0.1c-0.1,0.1-0.1,0.2-0.2,0.3c0,0-0.1,0.1-0.1,0.1c-0.1,0.1-0.1,0.2-0.2,0.3c0,0,0,0.1-0.1,0.1   c-0.1,0.1-0.1,0.2-0.2,0.3c0,0,0,0.1-0.1,0.1c-0.1,0.1-0.1,0.2-0.2,0.3c-0.1,0.1-0.2,0.3-0.2,0.4c-3.3,6.3-0.8,14.1,5.6,17.4   c1.9,1,3.9,1.4,5.9,1.4c0.1,0,0.3,0,0.4,0c0.1,0,0.3,0,0.4,0c0.1,0,0.1,0,0.2,0c0.1,0,0.2,0,0.3,0c0,0,0,0,0.1,0c0.1,0,0.2,0,0.3,0   c0.1,0,0.2,0,0.2,0c0.1,0,0.2,0,0.3-0.1c0.1,0,0.3,0,0.4-0.1c0.1,0,0.3-0.1,0.4-0.1c0.1,0,0.2-0.1,0.3-0.1c0.2,0,0.4-0.1,0.5-0.2   c0.1,0,0.3-0.1,0.4-0.1c0.1,0,0.3-0.1,0.4-0.1c0,0,0,0,0,0c0.1,0,0.2-0.1,0.4-0.1c0,0,0,0,0,0c0,0,0.1,0,0.1,0c0,0,0.1,0,0.1-0.1   c0,0,0,0,0.1,0c0,0,0.1,0,0.1,0c0,0,0,0,0.1,0c0.1,0,0.2-0.1,0.3-0.2c0,0,0,0,0.1,0c0.1-0.1,0.2-0.1,0.3-0.2   c0.1-0.1,0.3-0.1,0.4-0.2c0.4-0.2,0.7-0.5,1.1-0.7c0.2-0.2,0.5-0.4,0.7-0.6c0.2-0.2,0.4-0.4,0.7-0.6c0.2-0.2,0.4-0.4,0.6-0.6   c0.2-0.2,0.4-0.5,0.6-0.7c0.1-0.1,0.2-0.2,0.3-0.4c0,0,0,0,0,0c0.1-0.1,0.2-0.2,0.3-0.4c0.1-0.1,0.2-0.3,0.2-0.4   c0.1-0.1,0.1-0.2,0.2-0.3c0-0.1,0.1-0.1,0.1-0.2c0.1-0.1,0.1-0.2,0.2-0.3C298.5,25.3,296,17.5,289.6,14.2z M279.6,26.7L279.6,26.7   l0.3,0.6c0.4,0.7,1,1.3,1.8,1.7c0.3,0.2,0.7,0.3,1,0.3c0.3,0,0.5-0.1,0.7-0.2c0.1,0,0.1-0.1,0.2-0.2c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0-0.1c0.2-0.3,0.2-0.7-0.1-1.3c0,0,0,0,0,0   c-0.1-0.2-0.2-0.4-0.4-0.6c-0.1-0.1-0.2-0.2-0.3-0.3c-0.4-0.4-0.7-0.8-0.9-1.2c-0.1-0.1-0.2-0.2-0.2-0.4c0,0,0,0,0-0.1   c0,0,0-0.1-0.1-0.1c0,0,0-0.1-0.1-0.1c-0.1-0.2-0.1-0.3-0.2-0.5c-0.1-0.2-0.1-0.4-0.2-0.6c0,0,0-0.1,0-0.1c0-0.1,0-0.2,0-0.3v-0.1   c0-0.1,0-0.1,0-0.2c0,0,0-0.1,0-0.1c0-0.1,0-0.2,0-0.3c0.1-0.3,0.2-0.6,0.3-0.8c0,0,0,0,0-0.1c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0-0.1c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0l0,0c0.4-0.5,0.9-0.9,1.5-1.1c0.7-0.2,1.4-0.3,2.1-0.1l0.9-1.7l1.9,1l-0.8,1.6c0.7,0.4,1.2,1,1.6,1.6   l0.2,0.4l-1.8,1.6l-0.3-0.5c-0.1-0.1-0.3-0.4-0.5-0.7c-0.1-0.1-0.3-0.3-0.5-0.4c0,0-0.1-0.1-0.1-0.1c-0.1-0.1-0.3-0.2-0.4-0.2   c0,0-0.1,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0   c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0h0c0,0,0,0-0.1,0h0   c0,0,0,0-0.1,0h0c0,0,0,0,0,0h0c0,0,0,0,0,0h0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c-0.2,0.1-0.4,0.3-0.5,0.5c-0.3,0.5-0.2,0.9,1,2.3c0.8,0.9,1.2,1.6,1.4,2.4   c0.2,0.8,0.1,1.6-0.3,2.5c-0.8,1.5-2.4,2.2-4.2,1.8l-0.8,1.6l-0.1,0.2l-2-1l0.9-1.7c-0.1-0.1-0.2-0.1-0.3-0.2   c-0.3-0.2-0.5-0.4-0.8-0.6c0,0-0.1-0.1-0.1-0.1c-0.3-0.3-0.5-0.6-0.7-1l-0.2-0.4L279.6,26.7z">
                     </path>
                     <path class="st33" d="M278,37c-6.3-3.3-8.8-11.1-5.6-17.4c1.3-2.4,3.2-4.3,5.4-5.5c-2.4,1.2-4.4,3.1-5.7,5.6   c-3.3,6.3-0.8,14.1,5.6,17.4c3.9,2,8.4,1.9,12-0.1C286.2,38.9,281.8,39,278,37z">
                     </path>
                  </g>
                  <g>
                     <linearGradient id="SVGID_00000016062571573767098340000005260099975199807150_" gradientUnits="userSpaceOnUse" x1="79.734" y1="-130.471" x2="91.3973" y2="-130.471" gradientTransform="matrix(0.8624 -0.5063 0.5063 0.8624 260.6894 161.6657)">
                        <stop offset="0" style="stop-color:#FA8460"></stop>
                        <stop offset="4.692155e-02" style="stop-color:#FA9150"></stop>
                        <stop offset="0.1306" style="stop-color:#FBA43A"></stop>
                        <stop offset="0.2295" style="stop-color:#FBB229"></stop>
                        <stop offset="0.3519" style="stop-color:#FCBC1E"></stop>
                        <stop offset="0.5235" style="stop-color:#FCC117"></stop>
                        <stop offset="1" style="stop-color:#FCC315"></stop>
                     </linearGradient>
                     <path style="fill:url(#SVGID_00000016062571573767098340000005260099975199807150_);" d="M268.1,0c-1,0.1-2,0.4-2.8,0.9   c0,0-0.1,0-0.1,0c0,0-0.1,0-0.1,0.1c0,0,0,0,0,0c0,0-0.1,0-0.1,0.1c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0-0.1,0.1   c-0.1,0.1-0.2,0.1-0.3,0.2c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0.1-0.1,0.1   c-0.1,0.1-0.1,0.2-0.2,0.3c0,0,0,0,0,0c-0.1,0.1-0.2,0.2-0.2,0.4c-0.1,0.1-0.2,0.3-0.2,0.4c0,0.1-0.1,0.1-0.1,0.2   c0,0.1-0.1,0.1-0.1,0.2c0,0,0,0.1-0.1,0.1c0,0,0,0,0,0.1c0,0,0,0.1-0.1,0.1c0,0,0,0.1,0,0.1c0,0.1,0,0.1-0.1,0.2c0,0,0,0.1,0,0.1   c-0.1,0.3-0.1,0.5-0.2,0.8c0,0,0,0.1,0,0.1c0,0,0,0.1,0,0.1c0,0.1,0,0.1,0,0.2c0,0,0,0,0,0.1c0,0.1,0,0.1,0,0.2c0,0,0,0,0,0.1   c0,0.1,0,0.1,0,0.2c0,0,0,0,0,0.1c0,0.1,0,0.1,0,0.2c0,0.1,0,0.1,0,0.2c0.2,3.2,2.9,5.7,6.1,5.5c1-0.1,1.9-0.3,2.6-0.8   c0.1,0,0.1-0.1,0.2-0.1c0.1,0,0.1-0.1,0.1-0.1c0,0,0,0,0.1,0c0,0,0.1,0,0.1-0.1c0,0,0,0,0,0c0,0,0.1-0.1,0.1-0.1c0,0,0.1,0,0.1-0.1   c0,0,0.1-0.1,0.1-0.1c0,0,0.1-0.1,0.1-0.1c0,0,0.1-0.1,0.1-0.1c0,0,0.1-0.1,0.1-0.1c0.1-0.1,0.1-0.1,0.2-0.2c0,0,0.1-0.1,0.1-0.1   c0,0,0.1-0.1,0.1-0.1c0,0,0,0,0,0c0,0,0.1-0.1,0.1-0.1c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0-0.1c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0.1-0.1,0.1-0.1c0,0,0,0,0,0c0,0,0.1-0.1,0.1-0.1c0-0.1,0.1-0.1,0.1-0.2c0.1-0.2,0.2-0.4,0.3-0.5c0.1-0.1,0.1-0.2,0.1-0.4   c0-0.1,0.1-0.3,0.1-0.4c0-0.1,0.1-0.3,0.1-0.4c0-0.1,0-0.3,0.1-0.4c0-0.1,0-0.1,0-0.2c0,0,0,0,0,0c0-0.1,0-0.1,0-0.2   c0-0.1,0-0.1,0-0.2c0-0.1,0-0.1,0-0.2c0,0,0-0.1,0-0.1c0-0.1,0-0.1,0-0.2C274.1,2.3,271.3-0.2,268.1,0z M267.1,7.2L267.1,7.2   l0.3,0.2c0.3,0.2,0.7,0.3,1.1,0.3c0.2,0,0.3-0.1,0.4-0.1c0.1-0.1,0.2-0.1,0.2-0.3c0,0,0-0.1,0-0.1c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0-0.2-0.1-0.3-0.3-0.5c0,0,0,0,0,0   c-0.1,0-0.2-0.1-0.3-0.1c-0.1,0-0.1,0-0.2-0.1c-0.2-0.1-0.4-0.2-0.6-0.2c-0.1,0-0.1-0.1-0.2-0.1c0,0,0,0,0,0c0,0,0,0-0.1,0   c0,0,0,0-0.1,0c-0.1,0-0.1-0.1-0.2-0.1c-0.1-0.1-0.1-0.1-0.2-0.2c0,0,0,0,0,0c0,0-0.1-0.1-0.1-0.1l0,0c0,0,0,0,0-0.1c0,0,0,0,0,0   c0,0,0-0.1-0.1-0.1c0-0.1-0.1-0.3-0.1-0.4c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0l0,0   c0-0.3,0.2-0.5,0.3-0.8c0.2-0.2,0.5-0.4,0.8-0.5l0-0.9l1-0.1l0,0.8c0.4,0,0.7,0.1,1,0.3l0.2,0.1l-0.3,1l-0.3-0.1   c-0.1,0-0.2-0.1-0.4-0.1c-0.1,0-0.2,0-0.3-0.1c0,0-0.1,0-0.1,0c-0.1,0-0.1,0-0.2,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0l0,0c0,0,0,0,0,0l0,0c0,0,0,0,0,0l0,0c0,0,0,0,0,0l0,0c0,0,0,0,0,0l0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c-0.1,0.1-0.1,0.2-0.1,0.3c0,0.2,0.1,0.4,0.9,0.7c0.5,0.2,0.8,0.4,1.1,0.6c0.3,0.3,0.4,0.6,0.4,1c0,0.8-0.4,1.4-1.2,1.7l0,0.8   l0,0.1l-1,0.1l0-0.8c-0.1,0-0.1,0-0.2,0c-0.2,0-0.3,0-0.4-0.1c0,0-0.1,0-0.1,0c-0.2-0.1-0.4-0.1-0.5-0.2l-0.2-0.1L267.1,7.2z">
                     </path>
                     <path class="st33" d="M268.8,11.5c-3.2,0.2-6-2.3-6.1-5.5c-0.1-1.2,0.3-2.4,0.9-3.4c-0.7,1-1,2.2-0.9,3.5c0.2,3.2,2.9,5.7,6.1,5.5   c2-0.1,3.7-1.2,4.6-2.8C272.4,10.4,270.7,11.4,268.8,11.5z">
                     </path>
                  </g>
                  <polygon class="st00" points="297.8,43.3 296.4,42.7 294.6,46.7 295.4,41.2 296.8,41.8 298.7,37.9 "></polygon>
                  <polygon class="st00" points="264.2,27.2 262.9,26.4 260.7,30.2 262,24.8 263.3,25.6 265.5,21.8 ">
                  </polygon>
               </svg>
            </div>
            <div class="col-12 text-center mt20 mt-md50">
               <div class="pre-heading f-md-21 f-20 w500 lh140 white-clr">
               Copy Our Secret 60-Seconds System That Makes Us $525/Day Over &amp; Over Again
               </div>
            </div>
            <div class="col-12 mt-md25 mt20 f-md-50 f-28 w500 text-center white-clr lh140 text-shadow">
               <span class="f-md-42">The Breakthrough Technology Creates Done-For-You</span>
               <span class="w700">Profit-Pulling Websites In 10,000+ Niches</span> <br>
               <span class="w800 f-30 f-md-55 highlight-heading">In Just 60 Seconds Flat…</span>
            </div>
            <div class="col-12 mt-md25 mt20 f-18 f-md-24 w500 text-center lh140 white-clr2 text-capitalize">
               <span class="w700">Easily Create &amp; Sell Websites for BIG Profits</span> to Affiliate Marketers, Coaches, <br class="d-none d-md-block">Local Businesses like Attorney, Dentists, Gyms etc., Self Help, Weight Loss, Dating,<br class="d-none d-md-block">  Crypto &amp;  10,000+ Other Niches I No Coding Needed Ever...
            </div>
            <div class="col-12 mt20 mt-md40">
               <div class="row">
                  <div class="col-md-7 col-12 min-md-video-width-left">
                     <div class="col-12 responsive-video">
                        <iframe src="https://webpull.dotcompal.com/video/embed/6p5m1zpj87" style="width:100%; height:100%" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                     </div>

                    

                  </div>
                  <div class="col-md-5 col-12 mt20 mt-md0 min-md-video-width-right">
                     <div class="key-features-bg">
                        <ul class="list-head pl0 m0 f-16 f-md-18 lh150 w400 white-clr">
                           <li class="w600">Tap Into Fast-Growing $284 Billion Industry &amp; Bank BIG </li>
                           <li>Create <span class="w600">Elegant, Ultra-Fast Loading &amp; Mobile Ready Websites</span> in 60 Seconds </li>
                           <li class="w600">400+ Eye-Catching Themes with Done-For-You Content </li>
                           <li><span class="w600">100% SEO Ready Websites</span> for Tons of FREE Search Traffic </li>
                           <li><span class="w600">Get More Likes, Shares and Viral Traffic with</span> In-Built Social Media Tools </li>
                           <li><span class="w600">Accept Payments</span> For Your Services &amp; Products </li>
                           <li><span class="w600">Without Touching Any Code,</span> Customize Websites Easily</li>
                           <li>Analytics &amp; Remarketing Ready </li>
                           <li><span class="w600">Newbie-Friendly</span> - No Tech Skills Needed, No Monthly Fees Ever. </li>
                           <li><span class="w600">Commercial License Included</span> to Build an Incredible Income Helping Clients! </li>
                           <li>Bonus Live Training - <span class="w600">Find Tons of Local Clients Hassle-Free</span> </li>
                        </ul>
                     </div>
                     <div class="col-12 mt20 mt-md60 d-md-none">
                        <div class="f-22 f-md-28 w700 lh140 text-center white-clr">
                           (Free Commercial License + <br class="d-none d-md-block"> Low 1-Time Price For Launch Period Only)
                        </div>
                        <div class="row">
                           <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center ">
                              <a href="https://warriorplus.com/o2/buy/vr5dvn/f8s4cl/ftvdmb" class="cta-link-btn px-md30">Get Instant Access To WEBPULL</a>
                           </div>
                        </div>
                        <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30 ">
                           <img src="https://cdn.oppyo.com/launches/webpull/special/compaitable-with1.webp " class="img-fluid d-block mx-xs-center md-img-right ">
                           <div class="d-md-block d-none px-md30 "><img src="https://cdn.oppyo.com/launches/webpull/special/v-line.webp " class="img-fluid "></div>
                           <img src="https://cdn.oppyo.com/launches/webpull/special/days-gurantee1.webp " class="img-fluid d-block mx-xs-auto mt15 mt-md0 ">
                        </div>
                     </div>
                     <img src="https://cdn.oppyo.com/launches/webpull/special/limited-time.webp" class="mt20 img-fluid d-md-none mx-auto">
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="gr-1">
      <div class="container-fluid p0">
         <picture>
            <source media="(min-width:768px)" srcset="assets/images/webpull-steps.webp">
            <source media="(min-width:320px)" srcset="assets/images/webpull-mview-steps.webp">
            <img src="assets/images/webpull-steps.webp" alt="Steps" style="width:100%;">
         </picture>
      </div>
   </div>
   <!--WebPrimo ends-->


    <!-------Exclusive Bonus----------->
    <div class="exclusive-bonus">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <div class="white-clr f-24 f-md-36 lh140 w600">
                  Exclusive Bonus #3 : Buzzify
               </div>
               <div class="f-18 f-md-20 w500 mt20 white-clr">
                  20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
               </div>
            </div>
         </div>
      </div>
   </div>
   <!------Viddeyo Section------>

   <!-- Header buzzify Start -->

   <div class="buz-header-section">
      <div class="container">
         <div class="row">

            <div class="col-12">
               <div class="row">
                  <div class="col-md-3 text-center mx-auto">
                     <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 973.3 276.9" style="enable-background:new 0 0 973.3 276.9; max-height:40px;" xml:space="preserve">
                        <style type="text/css">
                           .st0 {
                              fill: #FFFFFF;
                           }
                        </style>
                        <g>
                           <path class="st0" d="M0,212.5V12.1h98.6c19.3,0,34.4,4.2,45.3,12.7c10.9,8.5,16.4,21,16.4,37.7c0,20.8-8.6,35.5-25.7,44.1
                                 c14,2.2,24.8,7.6,32.2,16.1c7.5,8.5,11.2,19.7,11.2,33.6c0,11.7-2.6,21.8-7.9,30.2c-5.3,8.4-12.8,14.8-22.5,19.3
                                 c-9.7,4.5-21.2,6.7-34.5,6.7H0z M39.2,93.3h50.3c10.7,0,18.8-2.2,24.5-6.6c5.6-4.4,8.4-10.7,8.4-19c0-8.3-2.8-14.6-8.3-18.8
                                 c-5.5-4.2-13.7-6.3-24.6-6.3H39.2V93.3z M39.2,181.9h60.6c12.3,0,21.6-2.2,27.7-6.7c6.2-4.5,9.3-11.2,9.3-20.2
                                 c0-8.9-3.2-15.8-9.5-20.6c-6.4-4.8-15.5-7.2-27.5-7.2H39.2V181.9z"></path>
                           <path class="st0" d="M268.1,216.4c-22.8,0-39.1-4.5-49.1-13.5c-10-9-15-23.6-15-44V88.2h37.3v61.9c0,13.1,2,22.4,6,27.7
                                 c4,5.4,10.9,8.1,20.8,8.1c9.7,0,16.6-2.7,20.6-8.1c4-5.4,6.1-14.6,6.1-27.7V88.2h37.3V159c0,20.3-5,35-14.9,44
                                 C307.2,211.9,290.9,216.4,268.1,216.4z"></path>
                           <path class="st0" d="M356.2,212.5l68.6-95.9h-58.7V88.2h121.6L419,184.1h64.5v28.4H356.2z"></path>
                           <path class="st0" d="M500.3,212.5l68.6-95.9h-58.7V88.2h121.6l-68.7,95.9h64.5v28.4H500.3z"></path>
                           <path class="st0" d="M679.8,62.3c-4.1,0-7.8-1-11.1-3c-3.4-2-6-4.7-8-8.1s-3-7.1-3-11.2c0-4,1-7.7,3-11c2-3.3,4.7-6,8-8
                                 c3.3-2,7-3,11.1-3c4,0,7.7,1,11.1,3c3.3,2,6,4.6,8,8c2,3.3,3,7,3,11c0,4.1-1,7.8-3,11.2c-2,3.4-4.7,6.1-8,8.1
                                 C687.5,61.3,683.8,62.3,679.8,62.3z M661.2,212.5V88.2h37.3v124.4H661.2z"></path>
                           <path class="st0" d="M747.9,212.5v-96.1h-16.8V88.2h16.8V57.3c0-11.7,1.8-21.9,5.5-30.5c3.6-8.6,8.9-15.2,15.7-19.9
                                 c6.8-4.7,15-7,24.6-7c5.9,0,11.7,0.9,17.5,2.7c5.7,1.8,10.7,4.4,14.8,7.6l-12.9,26.1c-3.8-2.3-8.1-3.5-12.9-3.5
                                 c-4.2,0-7.4,1.1-9.6,3.2c-2.2,2.1-3.7,5.1-4.4,8.9c-0.8,3.8-1.2,8.2-1.2,13.1v30.1h29.4v28.3h-29.4v96.1H747.9z"></path>
                           <path class="st0" d="M835.6,275.7l40.1-78.7l-59-108.8h42.7l38,72.9l33.2-72.9h42.7l-95,187.5H835.6z"></path>
                        </g>
                     </svg>
                  </div>
               </div>
            </div>
            <!-- <div class="col-12 mt20 mt-md50"> <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/demo-vid1.webp" class="img-fluid d-block mx-auto"></div> -->
            <div class="col-12 text-center lh140 mt20 mt-md30">
               <div class="pre-heading-b f-md-20 f-18 w600 lh140">
                  <div class="skew-con d-flex gap20 align-items-center ">
                     It's Time to Get Over Boring Content and Cash into The Latest Trending Topics in 2022
                  </div>
               </div>
            </div>
            <div class="col-12 mt-md30 mt20 f-md-40 f-28 w600 text-center white-clr lh140">
               Breakthrough 3-Click Software Uses a <span class="under yellow-clr w700"> Secret Method to Make Us $528/Day Over and Over</span> Again Using the Power of Trending Content &amp; Videos
            </div>
            <div class="col-12 mt-md25 mt20 f-18 f-md-21 w600 text-center lh140 yellow-clr">
               All of That Without Content Creation, Editing, Camera, or Tech Hassles Ever! Even Newbies Can Get Unlimited FREE Traffic and Steady PASSIVE Income…Every Day
            </div>
         </div>
         <div class="row d-flex align-items-center flex-wrap mt20 mt-md40">
            <div class="col-md-10 mx-auto col-12 mt20 mt-md0">
               <div class="col-12 responsive-video">
                  <iframe src="https://buzzify.dotcompal.com/video/embed/satfj9nvaq" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                        box-shadow: none !important;" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
               </div>
            </div>
         </div>
      </div>
   </div>
    <!-- Header buzzify End -->
    <div class="bsecond-section">
      <div class="container">
         <div class="row">
            <div class="col-12 p-md0">
               <div class="col-12 key-features-bg-b d-flex align-items-center flex-wrap">
                  <div class="row">
                     <div class="col-12 col-md-6">
                        <ul class="list-head-b pl0 m0 f-16 f-md-18 lh160 w400 white-clr text-capitalize">
                           <li><span class="w600">Creates Beautiful &amp; Self-Updating Sites </span>with Hot Trending Content &amp; Videos </li>
                           <li class="w600">Built-In 1-Click Traffic Generating System </li>
                           <li class="w600">Puts Most Profitable Links on Your Websites using AI </li>
                           <li>Legally Use Other's Trending Content To Generate Automated Profits- <span class="w600">Works in Any Niche or TOPIC </span></li>
                           <li>1-Click Social Media Automation - <span class="w600">People ONLY WANT TRENDY Topics These Days To Click.</span> </li>
                           <li><span class="w600">100% SEO Friendly </span> Website and Built-In Remarketing System </li>
                           <li>Complete Newbie Friendly And <span class="w600">Works Even If You Have No Prior Experience And Technical Skills</span> </li>
                        </ul>
                     </div>
                     <div class="col-12 col-md-6">
                        <ul class="list-head-b pl0 m0 f-16 f-md-18 lh160 w400 white-clr text-capitalize">
                           <li><span class="w600">Set and Forget System</span> With Single Keyword - Set Rules To Find &amp; Publish Trending Posts</li>
                           <li><span class="w600">Automatically Translate Your Sites In 15+ Language</span> According To Location For More Traffic</li>
                           <li><span class="w600">Make 5K-10K With Commercial License </span>That Allows You To Serve Your Clients &amp; Charge Them</li>
                           <li><span class="w600">Integration with Major Platforms</span> Including Autoresponders And Social Media Apps </li>
                           <li><span class="w600">In-Built Content Spinner</span> to Make your Content Fresh and More Engaging </li>
                           <li><span class="w600">A-Z Complete Video Training </span>Is Included To Make It Easier For You </li>
                           <li><span class="w600">Limited-Time Special</span> Bonuses Worth $2285 If You Buy Today </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>

      </div>
   </div>

   <div class="gr-1">
      <div class="container-fluid p0">
         <picture>
            <source media="(min-width:768px)" srcset="https://cdn.oppyo.com/launches/webpull/special-bonus/b3d.webp">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/webpull/special-bonus/b3m.webp">
            <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/b3d.webp" alt="Flowers" style="width:100%;">
         </picture>
         <picture>
            <source media="(min-width:768px)" srcset="https://cdn.oppyo.com/launches/webpull/special-bonus/bnd.webp">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/webpull/special-bonus/bnm.webp">
            <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/bnd.webp" alt="Flowers" style="width:100%;">
         </picture>
         <picture>
            <source media="(min-width:768px)" srcset="https://cdn.oppyo.com/launches/webpull/special-bonus/bpd.webp">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/webpull/special-bonus/bpm.webp">
            <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/bpd.webp" alt="Flowers" style="width:100%;">
         </picture>
      </div>
   </div>
   <!--buzzify ends-->
<!-------Exclusive Bonus----------->
<div class="exclusive-bonus">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <div class="white-clr f-24 f-md-36 lh140 w600">
                  Exclusive Bonus #4 : NinjaKash
               </div>
               <div class="f-18 f-md-20 w500 mt20 white-clr">
                  20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-------Exclusive Bonus----------->
   <div class="headernj-section">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="mt-md7">
							<img src="assets/images/njlogo.webp" class="img-fluid mx-auto d-block">
						</div>
					</div>
					<div class="col-12 col-md-12 text-center">
						<div class="mt20 mt-md50">
							<div class="f-md-24 f-20 w500 lh140 text-center black-clr post-heading-nj">
								Weird System Pays Us $500-1000/Day for Just Copying and Pasting...
							</div>
						</div>
					</div>

					<div class="col-12">
						<div class="mt-md30 mt20">
							<div class="f-28 f-md-50 w500 text-center white-clr lh140">
							
							<span class="w700"> Break-Through App Creates a Ninja Affiliate Funnel In 5 Minutes Flat</span> That Drives FREE Traffic &amp; Sales on 100% Autopilot
							</div>
						</div>
						<div class="f-22 f-md-28 w600 text-center orange lh140 mt-md30 mt20">
							
							NO Worries For Finding Products | NO Paid Traffic | No Tech Skills
							
						</div>
					</div>
				   <div class="col-12 col-md-12">
						<div class="row">
                  <div class="col-md-7 col-12 min-md-video-width-left mt20 mt-md50">
                     <div class="col-12 responsive-video">
                        <iframe src="https://ninjakash.dotcompal.com/video/embed/t4spbhm1vi" style="width:100%; height:100%" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                     </div> 

                  </div>
							
							<div class="col-md-4 col-12 f-20 f-md-20 worksans lh140 w400 white-clr mt-md40 mt20 min-md-video-width-right">
								<ul class="list-head pl0 m0">
									<li>Kickstart with 50 Hand-Picked Products</li>
									<li>Promote Any Offer in Any niche HANDS FREE</li>
									<li>SEO Optimized, Mobile Responsive Affiliate Funnels</li>
									<li>Drive TONS OF Social &amp; Viral traffic</li>
									<li>Build Your List and Convert AFFILIATE Sales in NO Time</li>
									<li>No Monthly Fees…EVER</li>
								</ul>
							</div>
						</div>
					</div>						
				</div>
			</div>
		</div>
      <div class="">
			<div class="container-fluid p0">
				<img src="assets/images/ninjakash-grandb1.webp" class="img-fluid d-block mx-auto" style="width: 100%;">
				<img src="assets/images/ninjakash-grandb2.webp" class="img-fluid d-block mx-auto" style="width: 100%;">
				<img src="assets/images/ninjakash-grandb3.webp" class="img-fluid d-block mx-auto" style="width: 100%;">
			</div> 
		</div>
   <!-------Exclusive Bonus----------->
<div class="exclusive-bonus">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <div class="white-clr f-24 f-md-36 lh140 w600">
                  Exclusive Bonus #5 : JOBiin
               </div>
               <div class="f-18 f-md-20 w500 mt20 white-clr">
                  20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-------Exclusive Bonus----------->
 <!-- JOBiin section Starts -->
   <div class="jobiin-header-section">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <svg id="Layer_1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 128.19 38" style="max-height:55px;">
                  <defs>
                     <style>
                        .cls-1,
                        .cls-2 {
                           fill: #fff;
                        }

                        .cls-2 {
                           fill-rule: evenodd;
                        }

                        .cls-3 {
                           fill: #0a52e2;
                        }
                     </style>
                  </defs>
                  <rect class="cls-1" x="79.21" width="48.98" height="38" rx="4" ry="4"></rect>
                  <path class="cls-1" d="M18.64,4.67V24.72c0,1.29-.21,2.47-.63,3.54-.42,1.07-1.03,1.97-1.84,2.71-.81,.74-1.79,1.32-2.93,1.74-1.15,.42-2.45,.63-3.9,.63s-2.64-.19-3.7-.57c-1.07-.38-1.98-.92-2.75-1.62-.77-.7-1.39-1.54-1.88-2.51S.19,26.6,0,25.41l5.74-1.13c.46,2.45,1.63,3.68,3.52,3.68,.89,0,1.65-.28,2.28-.85,.63-.57,.95-1.46,.95-2.67V9.6H3.92V4.67h14.72Z"></path>
                  <path class="cls-1" d="M54.91,33.37V4.63h9.71c3.38,0,6.02,.66,7.92,1.97,1.9,1.32,2.84,3.28,2.84,5.9,0,1.33-.35,2.52-1.06,3.56-.7,1.05-1.73,1.83-3.07,2.36,1.72,.37,3.02,1.16,3.88,2.37,.86,1.21,1.29,2.61,1.29,4.21,0,2.75-.91,4.83-2.72,6.25-1.82,1.42-4.39,2.12-7.72,2.12h-11.08Zm5.76-16.7h4.15c1.54,0,2.72-.32,3.55-.95,.83-.63,1.24-1.55,1.24-2.76,0-1.33-.42-2.31-1.25-2.94-.84-.63-2.08-.95-3.74-.95h-3.95v7.6Zm0,3.99v8.27h5.31c1.53,0,2.69-.33,3.49-.99,.8-.66,1.2-1.64,1.2-2.94,0-1.4-.34-2.48-1.03-3.22-.68-.74-1.76-1.11-3.24-1.11h-5.75Z"></path>
                  <g>
                     <path class="cls-1" d="M46.84,4.19C42.31-.12,36.87-1.08,31.16,1.2c-5.82,2.33-9.1,6.88-9.52,13.21-.41,6.17,2.57,10.9,6.84,15.15,.95-.84,1.22-1.22,2.01-2.05-.79-.65-1.41-1.09-2.43-2.11-5.86-6.15-5.78-15.66,1.04-20.46,5-3.52,11.71-3.13,16.37,.95,4.29,3.75,5.54,10.44,2.59,15.61-1.2,2.11-3.09,3.84-4.86,5.98,.28,.45,.72,1.18,1.44,2.35,1.77-2.01,3.11-3.72,4.38-5.63,4.39-6.6,3.56-14.59-2.17-20.02Z"></path>
                     <g>
                        <path class="cls-2" d="M39.67,27.34c-2.49,.37-4.9,.52-7.23-.46-.54-.23-1.13-1.06-1.18-1.65-.3-3.69,1.25-5.27,4.98-5.26,.31,0,.62,0,.92,.01,5.31-.01,5.96,6.85,2.51,7.36Z"></path>
                        <path class="cls-2" d="M39.49,16.69c-.04,1.69-1.37,2.98-3.06,2.98-1.6,0-3.01-1.43-3.01-3.06s1.39-3.04,3.02-3.03c1.75,0,3.1,1.38,3.06,3.12Z"></path>
                     </g>
                     <g>
                        <g>
                           <path class="cls-2" d="M42.93,26.23c-.67-3.46-.93-5.25-3.46-7.18,.63-1.21,1.3-1.83,2.1-2,2.43-.53,3.98,.21,4.84,1.97,.66,1.34,.78,2.29-.37,3.77-1.03,1.17-1.85,2.21-3.11,3.43Z"></path>
                           <path class="cls-2" d="M44.94,13.4c.01,1.6-1.27,2.91-2.88,2.92-1.6,.01-2.91-1.28-2.92-2.88-.01-1.6,1.28-2.9,2.88-2.92,1.6-.01,2.91,1.28,2.92,2.88Z"></path>
                        </g>
                        <g>
                           <path class="cls-2" d="M30.1,26.23c.67-3.46,.93-5.25,3.46-7.18-.63-1.21-1.3-1.83-2.1-2-2.43-.53-3.98,.21-4.84,1.97-.66,1.34-.88,2.01,.27,3.49,.96,1.3,1.88,2.44,3.21,3.71Z"></path>
                           <path class="cls-2" d="M28.08,13.4c0,1.6,1.28,2.91,2.88,2.92,1.6,.01,2.91-1.28,2.92-2.88,.01-1.6-1.27-2.9-2.88-2.92-1.6-.01-2.91,1.28-2.92,2.88Z"></path>
                        </g>
                     </g>
                     <path class="cls-1" d="M42.02,27.74c-.7,.41-6.95,2.1-10.73,.08l-2.22,2.57c.55,.61,5.12,5.27,7.55,7.6,2.22-2.17,7.2-7.37,7.2-7.37l-1.8-2.89Z"></path>
                  </g>
                  <g>
                     <rect class="cls-3" x="85.05" y="4.63" width="5.38" height="5.4" rx="2.69" ry="2.69"></rect>
                     <rect class="cls-3" x="85.05" y="13.43" width="5.38" height="19.94"></rect>
                     <rect class="cls-3" x="95.13" y="4.63" width="5.38" height="5.4" rx="2.69" ry="2.69"></rect>
                     <rect class="cls-3" x="95.13" y="13.43" width="5.38" height="19.94"></rect>
                     <path class="cls-3" d="M109.85,13.43l.24,2.86c.66-1.02,1.48-1.81,2.45-2.38,.97-.56,2.06-.85,3.26-.85,2.01,0,3.59,.63,4.72,1.9,1.13,1.27,1.7,3.25,1.7,5.95v12.46h-5.4v-12.47c0-1.34-.27-2.29-.81-2.85-.54-.56-1.36-.84-2.45-.84-.71,0-1.35,.14-1.92,.43-.57,.29-1.04,.7-1.42,1.23v14.5h-5.38V13.43h5.01Z"></path>
                  </g>
               </svg>
            </div>
            <div class="col-12 text-center mt20 mt-md50 lh130">
               <div class=" f-md-20 f-16 w600 lh130 headline-highlight">
                  Copy &amp; Paste Our SECRET FORMULA That Makes Us $328/Day Over &amp; Over Again!
               </div>
            </div>
            <div class="col-12 mt-md25 mt20 f-md-46 f-26 w600 text-center white-clr lh140">
               EXPOSED-A Breakthrough Software That<span class="w700 yellow-clr"> Creates <span class="tm">
                     Indeed<sup class="f-20 w500 lh200">TM</sup>
                  </span> Like JOB Search Sites in Next 60 Seconds</span> with 100% Autopilot Job Listings, FREE Traffic &amp; Passive Commissions
            </div>
            <div class="col-12 mt-md25 mt20 f-18 f-md-22 w400 text-center lh140 white-clr">
               Pre-Loaded with 10 million+ Job Listings I Earn Commissions on Every Click on Listings &amp; Banners | <br class="d-none d-md-block"> No Content Writing I No Tech Hassel Ever… Even A Beginner Can Start Right Away
            </div>
         </div>
         <div class="row d-flex align-items-center flex-wrap mt20 mt-md40">
            <div class="col-md-8 col-12 mx-auto">
               <div class="responsive-video">
                  <iframe src="https://jobiin.dotcompal.com/video/embed/x83jxcd4vl" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                        box-shadow: none !important;" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
               </div>
            </div>
            <div class="col-12 f-20 f-md-26 w700 lh150 text-center white-clr mt-md50 mt20">
               LIMITED TIME - FREE COMMERCIAL LICENSE INCLUDED
            </div>
         </div>
      </div>
      <img src="https://cdn.oppyo.com/launches/jobiin/common_assets/images/header-left-element.webp" alt="Header Element " class="img-fluid d-none header-left-element ">
      <img src="https://cdn.oppyo.com/launches/jobiin/common_assets/images/header-right-element.webp" alt="Header Element " class="img-fluid d-none header-right-element ">
   </div>

   <div class="jobiin-second-section">
      <div class="container">
         <div class="row">
            <div class="col-12 z-index-9">
               <div class="row jobiin-header-list-block">
                  <div class="col-12 col-md-6">
                     <ul class="features-list-jobin pl0 f-18 f-md-20 lh150 w500 black-clr text-capitalize">
                        <li><span class="w700">Tap into the $212 Billion Recruitment Industry</span></li>
                        <li><span class="w700">Create Multiple UNIQUE Job Sites</span> - Niche Focused (Automotive, Banking Jobs etc.) or City Focused (New York, London Jobs etc.)</li>
                        <li><span class="w700">100% Automated Job Posting &amp; Updates</span> on Your Site from 10 million+ Open Jobs</li>
                        <li>Get Job Listed from TOP Brands &amp; Global Giants <span class="w700">Like Amazon, Walmart, Costco, etc.</span></li>
                        <li>Create Website in <span class="w700">13 Different Languages &amp; Target 30+ Top Countries</span></li>
                        <li class="w700">Built-In FREE Search and Social Traffic from Google &amp; 80+ Social Media Platforms</li>
                        <li class="w700">Built-In SEO Optimised Fresh Content &amp; Blog Articles</li>
                     </ul>
                  </div>
                  <div class="col-12 col-md-6 mt10 mt-md0">
                     <ul class="features-list-jobin pl0 f-18 f-md-20 lh150 w500 black-clr text-capitalize">
                        <li class="w700">Make Commissions from Top Recruitment Companies on Every Click of Job Listings</li>
                        <li>Promote TOP Affiliate Offers Through <span class="w700">Banner and Text Ads for Extra Commission</span></li>
                        <li>Even Apply Yourself for Any <span class="w700">Good Part-Time Job to Make Extra Income</span></li>
                        <li>Easy And Intuitive to Use Software with <span class="w700">Step-by-Step Video Training</span></li>
                        <li><span class="w700">Get Started Immediately</span> - Launch Done-for-You Job Site and Start Making Money Right Away</li>
                        <li><span class="w700">Made For Absolute Newbies</span> &amp; Experienced Marketers</li>
                        <li class="w700">PLUS, FREE COMMERCIAL LICENSE IF YOU Start TODAY!</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>

   <div class="gr-1">
      <div class="container-fluid p0">
         <picture>
            <source media="(min-width:768px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/jobiin-steps.webp">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/jobiin-steps-mview.webp">
            <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/jobiin-steps.webp" alt="Steps" style="width:100%;">
         </picture>
         <picture>
            <source media="(min-width:768px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/jobiin-big-job.webp">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/jobiin-big-job-mview.webp">
            <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/jobiin-big-job.webp" alt="Real Result" style="width:100%;">
         </picture>
         <picture>
            <source media="(min-width:768px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/jobiin-impressive.webp">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/jobiin-impressive-mview.webp">
            <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/jobiin-impressive.webp" alt="Real Result" style="width:100%;">
         </picture>
         <picture>
            <source media="(min-width:768px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/jobiin-proudly-presenting.webp">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/jobiin-proudly-presenting-mview.webp">
            <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/jobiin-proudly-presenting.webp" alt="Real Result" style="width:100%;">
         </picture>
      </div>
   </div>

 <!-- JOBiin section ends -->


    <!-------Exclusive Bonus----------->
<div class="exclusive-bonus">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <div class="white-clr f-24 f-md-36 lh140 w600">
                  Exclusive Bonus #6 : VidBoxs
               </div>
               <div class="f-18 f-md-20 w500 mt20 white-clr">
                  20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-------Exclusive Bonus----------->


 <div class="header-section-vidboxs">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row align-items-center">
                     <div class="col-md-12 text-center">
                        <svg version="1.1" id="Layer_11" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 865.5 207.4" style="enable-background:new 0 0 865.5 207.4; max-height:50px" xml:space="preserve">
                           <style type="text/css">
                              .st00{fill:#FFFFFF;}
                              .st11{fill:url(#SVGID_1v_);}
                              .st22{fill:url(#SVGID_00000003804682614402079440000013301449542194118298_);}
                              .st33{fill:url(#SVGID_00000106859270421856661840000011347085370863025833_);}
                              .st44{opacity:0.3;}
                              .st55{fill:#0F0F0F;}
                           </style>
                           <g>
                              <path class="st00" d="M482.4,27.5v105.7c0,17.9-10.1,33.4-24.9,41.3v-32.9c1-2.6,1.6-5.4,1.6-8.4s-0.6-5.8-1.6-8.4V27.5H482.4z"></path>
                              <path class="st00" d="M457.6,92c-6.5-3.4-13.9-5.4-21.8-5.4c-25.8,0-46.6,20.9-46.6,46.6c0,25.8,20.9,46.6,46.6,46.6
                                 c7.9,0,15.3-1.9,21.8-5.4c14.8-7.8,24.9-23.4,24.9-41.3C482.4,115.3,472.4,99.8,457.6,92z M457.6,141.6c-2,5.3-5.9,9.6-10.9,12.2
                                 c-3.2,1.7-7,2.7-10.9,2.7c-12.9,0-23.3-10.4-23.3-23.3s10.4-23.3,23.3-23.3c3.9,0,7.6,1,10.9,2.7c5,2.6,8.8,7,10.9,12.2
                                 c1,2.6,1.6,5.4,1.6,8.4C459.1,136.2,458.6,139,457.6,141.6z"></path>
                              <path class="st00" d="M498,27.5v105.7c0,17.9,10.1,33.4,24.9,41.3v-32.9c-1-2.6-1.6-5.4-1.6-8.4s0.6-5.8,1.6-8.4V27.5H498z"></path>
                              <path class="st00" d="M498,133.2c0,17.9,10.1,33.4,24.9,41.3c6.5,3.4,13.9,5.4,21.8,5.4c25.8,0,46.6-20.9,46.6-46.6
                                 c0-25.8-20.9-46.6-46.6-46.6c-7.9,0-15.3,1.9-21.8,5.4C508.1,99.8,498,115.3,498,133.2z M521.3,133.2c0-3,0.6-5.8,1.6-8.4
                                 c2-5.3,5.9-9.6,10.9-12.2c3.2-1.7,7-2.7,10.9-2.7c12.9,0,23.3,10.4,23.3,23.3s-10.4,23.3-23.3,23.3c-3.9,0-7.6-1-10.9-2.7
                                 c-5-2.6-8.8-7-10.9-12.2C521.8,139,521.3,136.2,521.3,133.2z"></path>
                              <path class="st00" d="M597.5,133.2c0,17.9,10.1,33.4,24.9,41.3c6.5,3.4,13.9,5.4,21.8,5.4c25.8,0,46.6-20.9,46.6-46.6
                                 c0-25.8-20.9-46.6-46.6-46.6c-7.9,0-15.3,1.9-21.8,5.4C607.5,99.8,597.5,115.3,597.5,133.2z M620.8,133.2c0-3,0.6-5.8,1.6-8.4
                                 c2-5.3,5.9-9.6,10.9-12.2c3.2-1.7,7-2.7,10.9-2.7c12.9,0,23.3,10.4,23.3,23.3s-10.4,23.3-23.3,23.3c-3.9,0-7.6-1-10.9-2.7
                                 c-5-2.6-8.8-7-10.9-12.2C621.3,139,620.8,136.2,620.8,133.2z"></path>
                              <polygon class="st00" points="721.5,179.9 690.4,179.9 756.1,86.5 787.2,86.5 	"></polygon>
                              <polygon class="st00" points="756.1,179.9 787.2,179.9 721.5,86.5 690.4,86.5 	"></polygon>
                              <linearGradient id="SVGID_1v_" gradientUnits="userSpaceOnUse" x1="291.6235" y1="980.703" x2="309.5574" y2="1019.9633" gradientTransform="matrix(1 0 0 1 0 -870)">
                                 <stop offset="0" style="stop-color:#F40C28"></stop>
                                 <stop offset="0.8911" style="stop-color:#FF2C50"></stop>
                              </linearGradient>
                              <polygon class="st11" points="264.4,179.9 278.5,150 308.3,86.5 339.4,86.5 295.5,179.9 292.5,179.9 	"></polygon>
                              <polygon class="st00" points="292.5,179.9 278.5,150 248.6,86.5 217.5,86.5 261.5,179.9 264.4,179.9 	"></polygon>
                              <rect x="351.9" y="86.6" class="st00" width="24.9" height="93.3"></rect>
                              <rect x="351.9" y="51.8" class="st00" width="24.9" height="24.9"></rect>
                              <g>
                                 <path class="st00" d="M810.1,175.9c-5.9-2.7-10.5-6.3-14-10.9c-3.4-4.6-5.3-9.6-5.6-15.2h23c0.4,3.5,2.1,6.4,5.1,8.6
                                    c3,2.3,6.7,3.4,11.2,3.4c4.3,0,7.8-0.9,10.2-2.6c2.4-1.7,3.7-4,3.7-6.7c0-2.9-1.5-5.1-4.5-6.6s-7.8-3.1-14.3-4.8
                                    c-6.7-1.6-12.3-3.3-16.6-5.1c-4.3-1.7-8-4.4-11.1-8c-3.1-3.6-4.7-8.4-4.7-14.5c0-5,1.4-9.6,4.3-13.7c2.9-4.1,7-7.4,12.4-9.8
                                    s11.7-3.6,19-3.6c10.8,0,19.4,2.7,25.8,8.1c6.4,5.4,10,12.6,10.6,21.8h-21.9c-0.3-3.6-1.8-6.4-4.5-8.6c-2.7-2.1-6.2-3.2-10.7-3.2
                                    c-4.1,0-7.3,0.8-9.5,2.3s-3.3,3.6-3.3,6.4c0,3,1.5,5.4,4.6,6.9c3,1.6,7.8,3.2,14.2,4.8c6.5,1.6,11.9,3.3,16.2,5.1
                                    c4.2,1.7,7.9,4.4,11,8.1c3.1,3.6,4.7,8.5,4.8,14.4c0,5.2-1.4,9.9-4.3,14c-2.9,4.1-7,7.4-12.4,9.7s-11.7,3.5-18.8,3.5
                                    C822.6,179.9,815.9,178.5,810.1,175.9z"></path>
                              </g>
                           </g>
                           <g>
                              <g>
                                 <linearGradient id="SVGID_00000090999748961388969530000010752564172096406444_" gradientUnits="userSpaceOnUse" x1="-1360.6178" y1="-1339.845" x2="-1175.7527" y2="-1283.7964" gradientTransform="matrix(0.6181 -0.7861 -0.7861 -0.6181 -176.8927 -1732.0651)">
                                    <stop offset="0.2973" style="stop-color:#F40C28"></stop>
                                    <stop offset="0.7365" style="stop-color:#FF2C50"></stop>
                                 </linearGradient>
                                 <path style="fill:url(#SVGID_00000090999748961388969530000010752564172096406444_);" d="M125.8,6.8c0.4,0.3,0.8,0.7,1.2,1
                                    c6.2,5.4,9.8,12.7,10.8,20.3c1,8.1-1.2,16.6-6.6,23.6l-75.8,96.4c0,0-18.4,20.5-8.9,35l-32-25.2c-1.1-0.9-2.2-1.8-3.2-2.8
                                    c-13.5-13.3-15.2-35-3.2-50.3l72.8-92.6C91.8-1.7,111.9-4.1,125.8,6.8z"></path>
                                 <linearGradient id="SVGID_00000108307696041867937860000005451407988123938486_" gradientUnits="userSpaceOnUse" x1="-1216.8146" y1="1899.8297" x2="-973.6699" y2="1975.1873" gradientTransform="matrix(-0.6181 0.7861 0.7861 0.6181 -2080.1633 -197.5988)">
                                    <stop offset="0.3687" style="stop-color:#F40C28"></stop>
                                    <stop offset="1" style="stop-color:#FF2C50"></stop>
                                 </linearGradient>
                                 <path style="fill:url(#SVGID_00000108307696041867937860000005451407988123938486_);" d="M67.7,199.8c-0.4-0.3-0.8-0.7-1.2-1
                                    c-6.2-5.4-9.8-12.7-10.8-20.3c-1-8.1,1.2-16.6,6.6-23.6l75.8-96.4c0,0,18.4-20.5,8.9-35l32,25.2c1.1,0.9,2.2,1.8,3.2,2.8
                                    c13.5,13.3,15.2,35,3.2,50.3l-72.8,92.6C101.7,208.3,81.6,210.7,67.7,199.8z"></path>
                              </g>
                              <g class="st44">
                                 <path class="st55" d="M68.8,69v78.4c0,8.7,9.8,13.7,16.8,8.6l54.6-39.2c5.9-4.2,5.9-13,0-17.3L85.6,60.4
                                    C78.6,55.3,68.8,60.4,68.8,69z"></path>
                              </g>
                              <g>
                                 <path class="st00" d="M66.8,66v78.4c0,8.7,9.8,13.7,16.8,8.6l54.6-39.2c5.9-4.2,5.9-13,0-17.3L83.6,57.4
                                    C76.6,52.3,66.8,57.4,66.8,66z"></path>
                              </g>
                           </g>
                        </svg>
                     </div>
                  </div>
               </div>
               <div class="col-12 text-center mt20 mt-md50">
                  <div class="pre-heading f-18 f-md-22 w700 lh140 white-clr">
                     Copy A Proven System That Make Us $535/Day Over and Over Again from YouTube 
                  </div>
               </div>
               <div class="col-12 mt-md30 mt20 f-md-47 f-28 w700 text-center white-clr lh140">
                  Brand New Software <span class="yellow-clr">Creates &amp; Publish YouTube Shorts in Just 60 Seconds…</span> And Drive Tons of FREE Traffic, Sales &amp; Profits Hands-free 
               </div>
               <div class="col-12 mt-md25 mt20 f-18 f-md-24 w600 text-center lh140 white-clr text-capitalize">
               No Camera &amp; Recording | No Editing | No Paid Traffic | 100% Newbie Friendly 
               </div>
            </div>
            <div class="row d-flex align-items-center flex-wrap mt20 mt-md40">
               <div class="col-md-7 col-12 min-md-video-width-left">
                  <!-- <img src="https://cdn.oppyo.com/launches/vidboxs/fe/product-image.webp" class="img-fluid mx-auto d-block"> -->
                  <div class="col-12 responsive-video border-video">
                     <iframe src="https://vidboxs.dotcompal.com/video/embed/4ioexjgrco" style="width:100%; height:100%" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                     </div>
             
               </div>
               <div class="col-md-5 col-12 mt20 mt-md0 min-md-video-width-right">
                  <div class="calendar-wrap mb20 mb-md0">
                     <ul class="list-head pl0 m0 f-18 lh140 w400 white-clr">
                        <li>Tap Into Fast-Growing $200 Billion+ Video Industry &amp; Bank BIG </li>
                        <li>Create Unlimited YouTube Short Videos on Any Topic </li>
                        <li>Drive Unlimited Traffic &amp; Sales to Any Offer or Page. </li>
                        <li>Make Tons of Affiliate Commissions or Ad Profits  </li>
                        <li>Create Short Videos from Any Text in 3 Simple Clicks </li>
                        <li>Create Videos Using A keyword or Stock Videos </li>
                        <li>Add Background Music and/or Voiceover to Any Video </li>
                        <li>Inbuilt Voiceover Creator with 150+ Human Voice in 30+ Languages </li>
                        <li>100% Newbie Friendly, Required No Prior Experience or Tech Skills </li>
                        <li>No Camera Recording, No Voice, or Complex Editing Required </li>
                        <li>Free Commercial License to Sell Video Services for High Profits </li>
                     </ul>
                  </div>
                  <div class="d-md-none">
                     <div class="f-18 f-md-26 w800 lh140 text-center white-clr">
                        Free Commercial License + Low 1-Time Price For Launch Period Only 
                     </div>
                     <div class="row">
                        <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center">
                           <a href="#buynow" class="cta-link-btn d-block px0">Get Instant Access To VidBoxs</a>
                        </div>
                     </div>
                     <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                        <img src="https://cdn.oppyo.com/launches/vidboxs/fe/compaitable-with1.webp" class="img-responsive mx-xs-center md-img-right" alt="visa">
                        <div class="d-md-block d-none visible-md px-md30"><img src="https://cdn.oppyo.com/launches/vidboxs/fe/v-line.webp" class="img-fluid" alt="line">
                        </div>
                        <img src="https://cdn.oppyo.com/launches/vidboxs/fe/days-gurantee1.webp" class="img-responsive mx-xs-auto mt15 mt-md0" alt="30 days">
                     </div>
                     <div class="col-12 col-md-8 mx-auto mt30 d-md-none">
                        <img src="https://cdn.oppyo.com/launches/vidboxs/fe/limited-time.webp" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <div class="gr-1">
         <div class="container-fluid p0">
            <picture>
               <source media="(min-width:768px)" srcset="assets/images/vidboxs.webp">
               <source media="(min-width:320px)" srcset="assets/images/vidboxs-mview.webp">
               <img src="assets/images/vidboxs.webp" alt="Steps" style="width:100%;">
            </picture>
         </div>
      </div>

      <!-- Bonus Section Header Start -->
      <div class="bonus-header">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-10 mx-auto heading-bg text-center">
                  <div class="f-24 f-md-36 lh140 w700"> When You Purchase MailZilo, You Also Get <br class="hidden-xs"> Instant Access To These 20 Exclusive Bonuses</div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus Section Header End -->


      <!-- Bonus #1 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 1</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus1.webp" class="img-fluid mx-auto d-block" alt="Bonus1">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Your own@SEO
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">With this simple plugin you can get the true insight on your web traffic efforts in only seconds!</span> Watch as your social network shares increase, your google PageRank and more. </li>
                           <li>Get a clear vision on what you need to start focusing on with your SEO efforts. Start more effective backlinks from Facebook, Twitter, Google and more.</li>
                           <li>You will get all of the most important stats that you need to know for your SEO web traffic.</li>
                           <li>Focus on the amount of shares you have on popular social sites like Facebook, Twitter, Stumble Upon and more.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #1 Section End -->

      <!-- Bonus #2 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 2</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30">
                  <div class="row">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="assets/images/bonus2.webp" class="img-fluid mx-auto d-block" alt="Bonus2">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md0 text-capitalize">
                        Email Niche Plus
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">Owning a business has many advantages from being able to set your own hours to have the control to sell what you want. </span> </li>
                           <li>Unfortunately, too many new business owners fail within their first year. </li>
                           <li>While it isn't for lack of effort, those looking to start a new online business fail to complete the crucial first step; they fail to research to find a viable and profitable niche.</li>
                           <li>This comprehensive guide covers everything you need to know for finding your niche so you can stand out and create success faster.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #2 Section End -->

      <!-- Bonus #3 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 3</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus3.webp" class="img-fluid mx-auto d-block " alt="Bonus3">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Grab your Traffic Booster
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">Businesses both large and small are always hoping that their target audience will be able to find their site among the thousands of websites they are competing against.</span></li>
                           <li>One of the best ways to do this is to utilize the free and paid methods for boosting website traffic. </li>
                           <li>However, like so many online marketing methods, it isn't always clear on how to do this. Finding an effective way to boost the traffic to your website can not only be confusing, but it can also be a bit frustrating.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #3 Section End -->

      <!-- Bonus #4 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 4</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="assets/images/bonus4.webp" class="img-fluid mx-auto d-block " alt="Bonus4">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        All in one video marketing kit 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">Most people fail with Internet Marketing because they do not take consistent action.</span> There is a solid and dependable method that has been working for many years and will continue to work in the future.</li>
                           <li>It's time for you to learn how to make $10,000+ per month in 90 days or even less!</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #4 End -->

      <!-- Bonus #5 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 5</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus5.webp" class="img-fluid mx-auto d-block " alt="Bonus5">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Master E Secret kit
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        This is a collection of Internet Marketing Magazines with 380+ pages of quality content!<br><br>
                        <span class="w600">Topics covered:</span>
                           <li class="mt20">Your Online Business Startup</li>
                           <li>Lead Generation Strategies</li>
                           <li>Your Conversion Strategies</li>
                           <li>Network Marketing Guide</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #5 End -->

      <!-- CTA Button Section Start -->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 yellow-clr">"LOVEMAIL"</span> for an Additional <span class="w700 yellow-clr">$5 Discount</span> on Commercial Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                     <span class="text-center">Grab MailZilo + My 20 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                        <span class="f-14 f-md-18 w500 ">Days</span>
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                        <span class="f-14 f-md-18 w500 ">Hours</span>
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                        <span class="f-14 f-md-18 w500 ">Mins</span>
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                        <span class="f-14 f-md-18 w500 ">Sec</span>
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->

      <!-- Bonus #6 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 6</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="assets/images/bonus6.webp" class="img-fluid mx-auto d-block " alt="Bonus6">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Trendy Headline Bee
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">Learn the Techniques to Make Your Email Subject Line Stand Out Multi Media!</span></li>
                           <li>Your subject line will certainly stand out, and your email will be opened if you make your email unique, useful to the reader, and focused on what the reader either needs to know or wants to know. </li>
                           <li>Maximize your email marketing efforts by simply having the highest results that you haven't experience before. </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #6 End -->

      <!-- Bonus #7 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 7</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus7.webp" class="img-fluid mx-auto d-block " alt="Bonus7">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        LeadPlus 1 click 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">It doesn't matter what kind of business you're in, if you aren't able to generate new leads and turn them into paying customers, your company will never succeed. </span></li>
                           <li>You need to be constantly bringing in new customers if you want your business to thrive.</li>
                           <li>Generating more leads is anything but easy and if you don't have a solid marketing strategy that will drive more traffic to your website, you'll never be able to generate the leads you need for your business to succeed. </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #7 End -->

      <!-- Bonus #8 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 8</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus8.webp" class="img-fluid mx-auto d-block " alt="Bonus8">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Soci Marketing Agency
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">Social media is designed to keep us entertained. Many of us spend hours trawling through content looking to find things that are unique and special to add to our feeds.</span> </li>
                           <li>We are overwhelmed with having so many accounts that we can follow—but not all content is created equal.</li>
                           <li>Some content is much more popular than others; and it stands out on our feeds. Creating fantastic content and posting it at the optimal time with call-to-actions is a key strategy to succeeding on social media.  </li>
                          
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #8 End -->

      <!-- Bonus #9 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 9</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus9.webp" class="img-fluid mx-auto d-block " alt="Bonus9">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Image collection V3 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">Stock Images For You To Use In Your Projects And Your Clients Projects. Plus You Can Resell Them!</span> </li>
                           <li>Many successful online business owners have said that making money online is easy as a piece of cake as long as you have all the ingredients in doing the process.</li>
                           <li>And one of those ingredients is graphics or images which is a huge help in marketing your product or service online using social media networking sites.</li>
                           <li>The challenge is that you can't simply use images for granted or you might end up sued by copyright violation.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #9 End -->

      <!-- Bonus #10 start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 10</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus10.webp" class="img-fluid mx-auto d-block " alt="Bonus10">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        20k Top header collections
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">Ready to go abstract headers for your minisites!</span></li>
                           <li>If you want to make money online, one of the best media to sell or promote affiliate products is through you website or blog. </li>
                           <li>And if you want to stand from the crowded competition of the industry you are into, having a good-looking header for your blog or site is one of the factors to get noticed and remembered.</li>
                           <li>The good news is that inside this product is a bundle of abstract header background that you can use to your own project or website and you also have the authority to resell it to your customers.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #10 End -->

      <!-- CTA Button Section Start -->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 yellow-clr">"LOVEMAIL"</span> for an Additional <span class="w700 yellow-clr">$5 Discount</span> on Commercial Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                     <span class="text-center">Grab MailZilo + My 20 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr"><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 smmltd">Days</span> </div><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">0300</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div><div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div></div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->

      <!-- Bonus #11 start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 11</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus11.webp" class="img-fluid mx-auto d-block " alt="Bonus11">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Eye Catching sales templates
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>20 Eye Catching WP Sales Page Themes, You Receive The Resell Rights To The Full Package.</li>
                           
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #11 End -->

      <!-- Bonus #12 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 12</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus12.webp" class="img-fluid mx-auto d-block " alt="Bonus12">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Internet EDITION PLUS
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">With more than 40,000 Google searches happening every second, you need to find a way to gain the attention of these potential customers. </span></li>
                           <li>Knowing how your target audience engages with their favorite brands and the things that influence them to buy are the keys to growing your online business.</li>
                           <li>This video course details the 79 actions that you need to take today that will lead you to realizing huge results in your Internet business. </li>
                           <li>It covers everything from validating your business idea, to transitioning your business for growth, to successfully scaling your business to grow beyond your wildest dreams.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #12 End -->

      <!-- Bonus #13 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 13</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus13.webp" class="img-fluid mx-auto d-block " alt="Bonus13">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Build Up Your List 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        Start creating engagement with your new leads by making your landing page memorable and enjoyable!<br><br>
                        <span class="w600">Here is what you can do with this plugin:</span>
                        <li class="mt20">Edit every detail on the fly with the simple options panel for each page</li>
                        <li>Customize all of the content areas that are designed to be readable</li>
                        <li>Choose from a variety of colors for the 'call to action' buttons on your page</li>
                        <li>Load up the form code from any service like Aweber, MailChimp, and more</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #13 End -->

      <!-- Bonus #14 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 14</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus14.webp" class="img-fluid mx-auto d-block " alt="Bonus14">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Content Writer Pro
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">If you run a blog or website you understand the need for writing regular content. While this may sound easy, it is not always easy to come up with ideas of what to write about.</span> </li>
                           <li>Even then you need to know how to write a compelling blog post that will attract attention. </li>
                           <li>When it comes to writing online there are a few differences which you must be aware of. Writing this type of content is different than writing a novel or non-fiction book.</li>
                           <li>Inside this ebook you will find a compilation of 25 writing tips which have been designed to help you become a better writer. </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #14 End -->

      <!-- Bonus #15 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 15</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus15.webp" class="img-fluid mx-auto d-block " alt="Bonus15">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        4k Videos Pack
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        By 2019, Internet Video Traffic will account for 80% of all consumer Internet traffic. Here is an excellent opportunity to leverage the power of Videos and use this medium to catapult your web business to the next level. <br><br>
                        <span class="w600">You can:</span>

                           <li class="mt20">Use them as Background video on your sales page/sales video to enchance its appearance. </li>
                           <li>Use them as Sales video on your Squeeze Page/Landing Page.</li>
                           <li>Generate an additional passive income stream.</li>
                           <li>Use them as Promotional videos  on Youtube, FB, Instagram or any other networking or website</li>
                           
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #15 End -->

      <!-- CTA Button Section Start -->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 yellow-clr">"LOVEMAIL"</span> for an Additional <span class="w700 yellow-clr">$5 Discount</span> on Commercial Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                     <span class="text-center">Grab MailZilo + My 20 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <span class="f-14 f-md-18 smmltd">Days</span>
                      </div>
                      <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        
                        <span class="f-14 f-md-18 w500 smmltd">Hours</span>
                      </div>
                      <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        
                        <span class="f-14 f-md-18 w500 smmltd">Mins</span>
                      </div>
                      <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        
                        <span class="f-14 f-md-18 w500 smmltd">Sec</span> 
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->

      <!-- Bonus #16 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 16</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus16.webp" class="img-fluid mx-auto d-block " alt="Bonus16">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Boom social media
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">Turn tour Wordpress blog into a social powerhouse! The simple and fast way to increase social conversions.</span></li>
                           <li>Take the social features of some of the highest shared websites like Buzzfeed or UpWorthy and add them to your blog posts.</li>
                           <li>No matter what theme you are using you can add these shortcodes to get all the social share features you need to have viral blog posts.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #16 End -->

      <!-- Bonus #17 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 17</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus17.webp" class="img-fluid mx-auto d-block " alt="Bonus17">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Massive Website Visitors kit
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>An intrinsic understanding of your competition and how to better them is the most important component of any digital marketing strategy. Today, there is such small distances between you and your competition - it's unlikely that you're offering anything that cannot be bought on some other website. </li>
                           <li>Now, when a consumer wants and needs a product or information, all they must do is input their desires into the Google search and peruse the search engine results page.</li>
                           <li>Also, you will learn all the terms you need to know for a proper understanding of the web marketing industry.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #17 End -->

      <!-- Bonus #18 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 18</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus18.webp" class="img-fluid mx-auto d-block " alt="Bonus18">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        High sales Kit
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li><span class="w600">If you want to sell anything online, it's actually not all that hard.</span>
                        Closing a sale is not the problem. Generating consistent, high-value sales, that's the issue. </li>
                        <li>Even if you are able to master the art of online sales, you may still continue to struggle. </li>
                        <li>Using the high ticket sales secrets from this video course, you will learn how to maximize the return that you get for all your efforts. It all boils down to being at the right place, talking about the right things at the right time with the right people.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #18 End -->

      <!-- Bonus #19 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 19</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus19.webp" class="img-fluid mx-auto d-block " alt="Bonus19">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Dynamic page plugin
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">You probably have noticed that most businesses online are listed in directories such as YellowPages only... Now with one wordpress plugin you can create an all-in-one website that will pull in multiple sources and display in one place.</span> </li>
                           <li>This is a stand alone plugin that will create a business website in one landing page. Add tabbed content to keep your visitors staying on one page!</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #19 End -->

      <!-- Bonus #20 start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 20</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus20.webp" class="img-fluid mx-auto d-block " alt="Bonus20">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Affiliate Marketing Secrets
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        Discover How To Make Money With Affiliate Marketing - Even If You Are a Complete Beginner!<br><br>
                        <span class="w600">Topics covered:</span>
                           <li class="mt20">4 Ways you are killing your passive income empire dreams</li>
                           <li>The money is in the list but not in the way you think</li>
                           <li>7 secrets of affiliate success most marketers will not tell you</li>
                           <li>7 Reasons why you should focus on niche selection</li>
                           <li>6 Ways your niche may be holding your affiliate income</li>
                           <li>How to turbocharge your affiliate income in one step</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #20 End -->

      <!-- Huge Woth Section Start -->
      <div class="huge-area mt30 mt-md10">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="f-md-65 f-40 lh120 w700 white-clr">That's Huge Worth of</div>
                  <br>
                  <div class="f-md-60 f-40 lh120 w800 yellow-clr">$3300!</div>
               </div>
            </div>
         </div>
      </div>
      <!-- Huge Worth Section End -->

      <!-- text Area Start -->
      <div class="white-section pb0">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="f-md-36 f-25 lh140 w500">So what are you waiting for? You have a great opportunity <br class="hidden-xs"> ahead + <span class="w700">My 20 Bonus Products</span> are making it a <br class="hidden-xs"> <span class="w700">completely NO Brainer!!</span></div>
               </div>
            </div>
         </div>
      </div>
      <!-- text Area End -->

      <!-- CTA Button Section Start -->
      <div class="cta-btn-section">
         <div class="container">
            <div class="row">
               <div class="f-16 f-md-20 lh150 w500 text-center black-clr">
                  Use Coupon Code <span class="w700 blue-clr">"LOVEMAIL"</span> for an Additional <span class="w700 blue-clr">$5 Discount</span> on Commercial Licence
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20 ">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn px-md80">
                     Grab MailZilo + My 20 Exclusive Bonuses
                  </a>
               </div>

               
               <div class="col-12 mt15 mt-md20" align="center">
                  <h3 class="f-md-28 f-20 w500 text-center black">My Exclusive Bonuses Are Expiring in...</h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 col-md-10 mx-auto col-12 text-center">
                  <div class="countdown counter-black">
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                     <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                     <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->

      <!-- Footer Section Start -->
    
      <div class="footer-section">
        <div class="container ">
            <div class="row">
                <div class="col-12 text-center">
                    <svg version="1.1" x="0px" y="0px" viewBox="0 0 1501.16 276.9" style="enable-background:new 0 0 1501.16 276.9; max-height:40px;" xml:space="preserve">
							<style type="text/css">
							.st0{fill:#FFFFFF;}
							</style>
							<g>
							<path class="st0" d="M122.38,159.77l82.88-67.64h39.5v182.43h-45.44V160.68l-73.53,57.2h-6.82l-73.52-57.2v113.87H0V92.13h39.46
							L122.38,159.77z"></path>
							<path class="st0" d="M391.87,92.13l81.03,182.43h-48.87L409,238.51h-81.7l-13.62,36.04h-48.16l73.69-182.43H391.87z M395.71,206.65
							l-29.57-70.91l-26.8,70.91H395.71z"></path>
							<path class="st0" d="M540.93,92.13v182.43h-45.44V92.13H540.93z"></path>
							<path class="st0" d="M722.06,241.12v33.43H582.98V92.13h45.44v149H722.06z"></path>
							<path class="st0" d="M913.75,92.13l-99.48,149h99.48v33.43H736.68l100.26-149H743.6V92.13H913.75z"></path>
							<path class="st0" d="M989.75,92.13v182.43h-45.44V92.13H989.75z"></path>
							<path class="st0" d="M1170.88,241.12v33.43H1031.8V92.13h45.44v149H1170.88z"></path>
							<path class="st0" d="M1332.43,209.97c-2.33,5.08-5.42,9.64-9.28,13.71c-9.7,10.23-22.48,15.35-38.35,15.35
							c-15.94,0-28.8-5.12-38.53-15.35c-9.74-10.22-14.61-23.66-14.61-40.28c0-16.72,4.87-30.17,14.61-40.35
							c5.49-5.75,11.97-9.87,19.44-12.37l-26.14-31.06c-9.05,4.52-17.19,10.6-24.43,18.22c-17.75,18.72-26.63,40.57-26.63,65.56
							c0,25.41,8.94,47.36,26.81,65.81c17.87,18.45,41.03,27.68,69.49,27.68c28.13,0,51.18-9.26,69.13-27.81
							c2.07-2.15,4.04-4.34,5.87-6.58L1332.43,209.97z"></path>
							<path class="st0" d="M1501.16,0l-114.63,241.95l-71.11-84.51l-31.99,29.36l2.17-64.79c67.33-7.59,111.49-65.77,112.17-66.67
							c-41.92,31-125.97,49.4-125.97,49.4l-28.6-33.1L1501.16,0z"></path>
							</g>
							</svg>
                    <div class="f-14 f-md-16 w400 mt20 lh160 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
                </div>
                <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                    <div class="f-14 f-md-16 w400 lh160 white-clr text-xs-center">Copyright © MailZilo 2022</div>
                    <ul class="footer-ul w400 f-14 f-md-16 white-clr text-center text-md-right">
                        <li><a href="https://support.bizomart.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                        <li><a href="http://mailzilo.com/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                        <li><a href="http://mailzilo.com/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                        <li><a href="http://mailzilo.com/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                        <li><a href="http://mailzilo.com/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                        <li><a href="http://mailzilo.com/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                        <li><a href="http://mailzilo.com/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
      <!--Footer Section End -->

      <!-- timer --->
      <?php
         if ($now < $exp_date) {
         ?>
      <script type="text/javascript">
         // Count down milliseconds = server_end - server_now = client_end - client_now
         var server_end = <?php echo $exp_date; ?> * 1000;
         var server_now = <?php echo time(); ?> * 1000;
         var client_now = new Date().getTime();
         var end = server_end - server_now + client_now; // this is the real end time
         
         var noob = $('.countdown').length;
         
         var _second = 1000;
         var _minute = _second * 60;
         var _hour = _minute * 60;
         var _day = _hour * 24
         var timer;
         
         function showRemaining() {
         var now = new Date();
         var distance = end - now;
         if (distance < 0) {
         	clearInterval(timer);
         	document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
         	return;
         }
         
         var days = Math.floor(distance / _day);
         var hours = Math.floor((distance % _day) / _hour);
         var minutes = Math.floor((distance % _hour) / _minute);
         var seconds = Math.floor((distance % _minute) / _second);
         if (days < 10) {
         	days = "0" + days;
         }
         if (hours < 10) {
         	hours = "0" + hours;
         }
         if (minutes < 10) {
         	minutes = "0" + minutes;
         }
         if (seconds < 10) {
         	seconds = "0" + seconds;
         }
         var i;
         var countdown = document.getElementsByClassName('countdown');
         for (i = 0; i < noob; i++) {
         	countdown[i].innerHTML = '';
         
         	if (days) {
         		countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + days + '</span><span class="f-14 f-md-18 smmltd">Days</span> </div>';
         	}
         
         	countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + hours + '</span><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>';
         
         	countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">' + minutes + '</span><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>';
         
         	countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">' + seconds + '</span><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>';
         }
         
         }
         timer = setInterval(showRemaining, 1000);
         	
      </script>
      <?php
         } else {
         echo "Times Up";
         }
         ?>
      <!--- timer end-->
   </body>
</html>
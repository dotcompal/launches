<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- Tell the browser to be responsive to screen width -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=9">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
	  
	<meta name="title" content="Kyza | Special Bonus">
	<meta name="description" content="Kyza">
	<meta name="keywords" content="Kyza">

	<meta property="og:image" content="https://www.kyza.biz/special-bonus/thumbnail.png">
	<meta name="language" content="English">
	<meta name="revisit-after" content="1 days">
	<meta name="author" content="Dr. Amit Pareek">

	<!-- Open Graph / Facebook -->
	<meta property="og:type" content="website">
	<meta property="og:title" content="Kyza | Special Bonus">
	<meta property="og:description" content="Kyza">
	<meta property="og:image" content="https://www.kyza.biz/special-bonus/thumbnail.png">

	<!-- Twitter -->
	<meta property="twitter:card" content="summary_large_image">
	<meta property="twitter:title" content="Kyza | Special Bonus">
	<meta property="twitter:description" content="Kyza">
	<meta property="twitter:image" content="https://www.kyza.biz/special-bonus/thumbnail.png">


<title>Kyza  Bonuses</title>
<!-- Shortcut Icon  -->
<link rel="shortcut icon" href="assets/images/favicon.png"/>
<!-- Css CDN Load Link -->
<link rel="stylesheet" href="assets/css/font-awesome.min.css" type="text/css" />
<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
<link rel="stylesheet" href="assets/css/general.css">
<link rel="stylesheet" href="assets/css/timer.css" type="text/css" />
<link rel="stylesheet" type="text/css" href="assets/css/bonus-style.css">
<link rel="stylesheet" type="text/css" href="assets/css/bonus.css">
<link rel="stylesheet" type="text/css" href="assets/css/style.css">
<link rel="stylesheet" type="text/css" href="assets/css/style-feature.css">
<link rel="stylesheet" type="text/css" href="assets/css/style-bottom.css">
<!-- Font Family CDN Load Links -->
<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Barlow:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
<!-- Javascript File Load -->
<script src="assets/js/jquery.min.js" type="text/javascript"></script>
<script type='text/javascript' src="assets/js/bootstrap.min.js"></script>
<!-- Buy Button Lazy load Script -->
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TKWBVSR');</script>
<!-- End Google Tag Manager -->


<script>
 $(document).ready(function() {
  /* Every time the window is scrolled ... */
  $(window).scroll(function() {
	 /* Check the location of each desired element */
	 $('.hideme').each(function(i) {
		 var bottom_of_object = $(this).offset().top + $(this).outerHeight();
		 var bottom_of_window = $(window).scrollTop() + $(window).height();
		 /* If the object is completely visible in the window, fade it it */
		 if ((bottom_of_window - bottom_of_object) > -200) {
			 $(this).animate({
				 'opacity': '1'
			 }, 300);
		 }
	 });
  });
 });
</script>
<!-- Smooth Scrolling Script -->
<script>
 $(document).ready(function() {
  // Add smooth scrolling to all links
  $("a").on('click', function(event) {
 
	 // Make sure this.hash has a value before overriding default behavior
	 if (this.hash !== "") {
		 // Prevent default anchor click behavior
		 event.preventDefault();
 
		 // Store hash
		 var hash = this.hash;
 
		 // Using jQuery's animate() method to add smooth page scroll
		 // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
		 $('html, body').animate({
			 scrollTop: $(hash).offset().top
		 }, 800, function() {
 
			 // Add hash (#) to URL when done scrolling (default click behavior)
			 window.location.hash = hash;
		 });
	 } // End if
  });
 });
</script>


</head>
<body>



  <!-- New Timer  Start-->
  <?php
	 $date = 'March 01 2022 11:59 PM EST';
	 $exp_date = strtotime($date);
	 $now = time();  
	 /*
	 
	 $date = date('F d Y g:i:s A eO');
	 $rand_time_add = rand(700, 1200);
	 $exp_date = strtotime($date) + $rand_time_add;
	 $now = time();*/
	 
	 if ($now < $exp_date) {
	 ?>
  <?php
	 } else {
		 echo "Times Up";
	 }
	 ?>
  <!-- New Timer End -->
  <?php
	 if(!isset($_GET['afflink'])){
	 $_GET['afflink'] = 'https://cutt.ly/PP2UuuL';
	 $_GET['name'] = 'Dr. Amit Pareek';      
	 }
	 ?>


	<div class="main-header">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-xs-12 text-center p0">
                    <div class="col-xs-12 text-center">
                    	<div class="f-sm-32 f-20 lh140 w400">
                    		<span class="w600"><?php echo $_GET['name'];?>'s</span> special bonus page for &nbsp; <img src="assets/images/logo-black.png" class="mt15 mt-sm0">
                    	</div>
                    </div>

                    <div class="col-xs-12 p0 mt20 mt-sm40">
                        <div class="f-20 f-sm-24 w600 text-center lh140 yellow-bg black-clr"><span class="w600">Grab My 20 Exclusive Bonuses </span>Before the Deal Ends…</div>
                    </div>

                    <div class="col-sm-12 col-xs-12 mt20 mt-sm40 p0">
						<div class="">
							<div class="f-sm-45 f-28 w500 text-center black-clr lh140 line-center worksans">
							Take A Sneak Peek– How This All-In-One Business Suite <span class="w700 blue-head">Creates Beautiful Pages, Branding Graphics, Forms, Pop-Ups & Sends Unlimited Emails.
							</span>
							</div>
						</div>
                    </div>
                    <div class="col-sm-12 col-xs-12 text-center mt20 mt-sm30 p0">
                        <div class="f-sm-24 f-20 lh140 w600">All From Single Dashboard Without Any Tech Skills and Zero Monthly Fees</div>
                    </div>
                </div>
            </div>

            <div class="row mt20 mt-sm30">
                <div class="col-xs-12 col-sm-10 col-sm-offset-1 p0">
                    <div class="video-frame col-xs-12">
                      <div class="col-xs-12 p0">
                            <div class="col-xs-12 responsive-video p0">
                              <iframe src="https://kyza.dotcompal.com/video/embed/aevzl6ljzp" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	</div>

	<!-- Step Section Start -->
	<div class="space-section step-section1">
			   <div class="container px-sm15">
				  <div class="row inner-content">
						<div class="col-xs-12 p0">
						   <div class="f-sm-45 f-28 w500 lh140 text-center">
							<span class="f-24 f-sm-34">Create Amazing Profit Pulling Campaigns…</span> <br class="visible-lg">  <span class="w700">In Just 3-Easy Steps!</span>
						   </div>
						</div>
						<div class="col-xs-12 mt20 mt-sm50 px-sm15 px0">
						   <div class="row">
							  <div class="col-xs-12 col-sm-4">
								 <div class="steps-block">
									<div class="step-bg">
									   <div class="f-22 f-sm-22 w700 white-clr">
										 1. Select
									   </div>
									</div>
									<div class="mt15 mt-sm25">
									   <img src="assets/images/step1.gif" alt="" class="img-responsive center-block">
									</div>
									<div class="step-headline f-20 f-sm-20 w400 black-clr lh140 mt15 mt-sm-30 text-center">
									  Select Any ‘Proven To Convert’ Landing Page, OptIn Form & Email Template As Per Your Needs And Niche. 
									</div>
								 </div>
							  </div>
							  <div class="col-xs-12 col-sm-4 mt20 mt-sm0">
								 <div class="steps-block">
									<div class="step-bg">
									   <div class="f-22 f-sm-22 w700 white-clr">
										 2. Customize 
									   </div>
									</div>
									<div class="mt15 mt-sm25">
									   <img src="assets/images/step2.gif" alt="" class="img-responsive center-block">
									</div>
									<div class="step-headline f-20 f-sm-20 w400 black-clr lh140 mt15 mt-sm-30 text-center">
									   Edit And Customize Matching Your Marketing Objectives. 
									</div>
								 </div>
							  </div>
							  <div class="col-xs-12 col-sm-4 mt20 mt-sm0">
								 <div class="steps-block">
									<div class="step-bg">
									   <div class="f-22 f-sm-22 w700 white-clr">
										  3. Publish & Profit 
									   </div>
									</div>
									<div class="mt15 mt-sm25">
									   <img src="assets/images/step3.gif" alt="" class="img-responsive center-block">
									</div>
									<div class="step-headline f-20 f-sm-20 w400 black-clr lh140 mt15 mt-sm-30 text-center">
									 That’s It! Now, Start Publishing Your Pages Or Send Emails To Your List In Their Primary Mailbox And Start Profiting Like Never Before! 
									</div>
								 </div>
							  </div>
						   </div>
						</div>				
				  </div>
			   </div>
			</div>
			<!-- Step Section End -->
			
			<!-- Business Growth Section Start -->
			<div class="business-growth-section">
			   <div class="container px0 px-sm15">
				  <div class="row inner-content">
					 <div class="col-xs-12">
						<div class="f-sm-45 f-28 w700 lh140 text-center">
						We Guarantee! This is Your Last Solution To Start, Grow & Scale Online - <u>All Under Single Dashboard!</u>
						</div>
					 </div>
					 <div class="col-xs-12 mt20 mt-sm70">
						<div class="row">
						   <div class="col-xs-12 col-sm-4">
							  <img src="assets/images/grow1.png" class="img-responsive center-block">
							  <div class="f-20 w700 blue-clr lh140 text-center mt15">
								 Landing Page & Website <br>Builder 
							  </div>
							  <div class="f-18 w500 black-clr lh140 text-center mt10">
								In-built drag-n-drop landing page & website builder lets you create high-converting mobile responsive landing pages, websites & e-stores within minutes! 
							  </div>
						   </div>
						   <div class="col-xs-12 col-sm-4 mt20 mt-sm0">
							  <img src="assets/images/grow2.png" class="img-responsive center-block">
							  <div class="f-20 w700 blue-clr lh140 text-center mt15">
								100+ Done-For-You & Beautiful <br> Templates 
							  </div>
							  <div class="f-18 w500 black-clr lh140 text-center mt10">
								Loaded with high-converting & fully customizable templates for pages, popups and forms. Suitable for every niche and turns your visitors into leads and then into sales. 
							  </div>
						   </div>
						   <div class="col-xs-12 col-sm-4 mt20 mt-sm0">
							  <img src="assets/images/grow3.png" class="img-responsive center-block">
							  <div class="f-20 w700 blue-clr lh140 text-center mt15 mt-sm28">
								Ultrafast Hosting & Secure CDN 
							  </div>
							  <div class="f-18 w500 black-clr lh140 text-center mt10 mt-sm25">
								 Comes with ultrafast & secure hosting and CDN (Content Delivery Network) that loads your pages & websites within seconds… 
							  </div>
						   </div>
						</div>
					 </div>
					 <div class="col-xs-12 mt20 mt-sm50">
						<div class="row">
						   <div class="col-xs-12 col-sm-4">
							  <img src="assets/images/grow4.png" class="img-responsive center-block">
							  <div class="f-20 w700 blue-clr lh140 text-center mt15">
								 Business Branding Design Kit
							  </div>
							  <div class="f-18 w500 black-clr lh140 text-center mt10">
								Loaded with 20+ local niches graphic template pack that includes Abstract Images, Stock Photos, Social Timeline Covers, Logo Kit, Ebook Covers, Business Cards, Brochures & much more… 
							  </div>
						   </div>
						   <div class="col-xs-12 col-sm-4 mt20 mt-sm0">
							  <img src="assets/images/grow5.png" class="img-responsive center-block">
							  <div class="f-20 w700 blue-clr lh140 text-center mt15">
								Access to Over 2.5 Million Digital Assets 
							  </div>
							  <div class="f-18 w500 black-clr lh140 text-center mt10">
								In-built Pixabay integration allows you to access over 2.5 million stock images, graphics, videos & much more… Use it to create attention-grabbing branding material & promotion. 
							  </div>
						   </div>
						   <div class="col-xs-12 col-sm-4 mt20 mt-sm0">
							  <img src="assets/images/grow6.png" class="img-responsive center-block">
							  <div class="f-20 w700 blue-clr lh140 text-center mt15">
								 Smart Lead Generating Optin Forms 
							  </div>
							  <div class="f-18 w500 black-clr lh140 text-center mt10">
								Loaded with multiple optin forms designs templates allowing you to generate leads, webinar registrations, run discount coupons lead generation campaigns & more… 
							  </div>
						   </div>
						</div>
					 </div>
					 <div class="col-xs-12 mt20 mt-sm50">
						<div class="row">
						   <div class="col-xs-12 col-sm-4">
							  <img src="assets/images/grow7.png" class="img-responsive center-block">
							  <div class="f-20 w700 blue-clr lh140 text-center mt15">
								AI-Powered Smart Pop-Ups <br> Technology 
							  </div>
							  <div class="f-18 w500 black-clr lh140 text-center mt10">
								Advanced AI powered Pop-up technology shows user a Pop-up according to their interest… It means you can boost your lead/ sales by showcasing right offers to right audience. 
							  </div>
						   </div>
						   <div class="col-xs-12 col-sm-4 mt20 mt-sm0">
							  <img src="assets/images/grow8.png" class="img-responsive center-block">
							  <div class="f-20 w700 blue-clr lh140 text-center mt15 mt-sm28">
								 Complete Email Marketing Solution 
							  </div>
							  <div class="f-18 w500 black-clr lh140 text-center mt10 mt-sm25">
								Send emails directly in primary inbox to get more open & click rates. It comes with the advanced geo targeting, personalized emails, smart user segmentation & more… 
							  </div>
						   </div>
						   <div class="col-xs-12 col-sm-4 mt20 mt-sm0">
							  <img src="assets/images/grow9.png" class="img-responsive center-block">
							  <div class="f-20 w700 blue-clr lh140 text-center mt15">
								 DFY High-Converting Email <br> Templates 
							  </div>
							  <div class="f-18 w500 black-clr lh140 text-center mt10">
								Loaded with high-converting email templates designed by professionals. With these templates you can experience a massive jump in open rates & click rates! 
							  </div>
						   </div>
						</div>
					 </div>
					 <div class="col-xs-12 mt20 mt-sm50 text-center">
						<div class="f-22 w700 lh140 business-shape">
						   And So Much More…
						</div>
					 </div>
				  </div>
			   </div>
			</div>
			<!-- Business Growth Section End -->

			<!-- CTA Button Section Start -->
			<div class="cta-btn-section">
			   <div class="container">
				  <div class="row">
					 <div class="col-md-12 col-sm-12 col-xs-12 text-center">
						<div class="f-md-26 f-sm-22 f-18 text-center mt3 black lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
						<div class="f-sm-24 f-22 text-center purple lh120 w700 mt15 mt-sm20 purple">TAKE ACTION NOW!</div>
						<div class="f-sm-24 f-17 lh120 w600 mt15 mt-sm20">Use Discount Coupon <span class="w800 purple">"kyza6"</span> for Instant <span class="w700 purple">6% Discount</span> on Commercial Plan</div>
					 </div>
					 <div class="col-md-12 col-md-offset-0 col-sm-10 col-sm-offset-1 col-xs-12 col-xs-offset-0 text-center mt15 mt-sm20">
						<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
						<span class="text-center">Grab Kyza + My 20 Exclusive Bonuses</span> <br>
						</a>
					 </div>
					 <div class="col-md-10 col-md-offset-1 col-sm-12 col-sm-offset-0 col-xs-12 col-xs-offset-0 mt15 mt-sm20">
						<img src="assets/images/payment.png" class="img-responsive center-block">
					 </div>
					 <div class="col-xs-12 mt15 mt-sm20" align="center">
						<h3 class="f-md-35 f-sm-28 f-20 w500 text-center black">Coupon Is Expiring In... </h3>
					 </div>
					 <!-- Timer -->
					 <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 col-xs-12 text-center">
						<div class="countdown counter-black">
						   <div class="timer-label text-center"><span class="f-31 f-sm-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-sm-18 w500 smmltd">Days</span> </div>
						   <div class="timer-label text-center"><span class="f-31 f-sm-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-sm-18 w500 smmltd">Hours</span> </div>
						   <div class="timer-label text-center timer-mrgn"><span class="f-31 f-sm-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-sm-18 w500 smmltd">Mins</span> </div>
						   <div class="timer-label text-center "><span class="f-31 f-sm-60 timerbg oswald">37</span><br><span class="f-14 f-sm-18 w500 smmltd">Sec</span> </div>
						</div>
					 </div>
					 <!-- Timer End -->
				  </div>
			   </div>
			</div>
		<!-- CTA Button Section End   -->
			<!-- Testimonials Section Start -->
            <div class="testimonials-section">
               <div class="container px-sm15 px0">
                  <div class="row inner-content">
                    <div class="col-xs-12">
                        <div class="f-sm-45 f-28 w700 lh140 text-center black-clr">
                       Everyone Is <span class="blue-clr">In Love With Kyza</span> - So Will You…
                        </div>
                     </div>
					<div class="col-xs-12 mt20 mt-sm50">
                        <div class="row">
							<div class="col-xs-12 col-sm-6">
								<div class="testimonials-block-border">
									<div class="testimonials-block f-18 w400 lh140">
										“I’m a new Affiliate Marketer, and my biz survives on leads. I always run lead-gen campaigns by creating lead magnet funnels. I paid whopping $297 every month for landing page builder and $67 for email autoresponder.  <br><br>

										<span class="w600">But thanks to Kyza, I’m getting everything under 1 roof at a one-time real low price. Now, my leads have increased by 33%, with biz profits increasing by 31% in only 2 months. I owe all this to Kyza!” </span>					
									<div class="col-xs-12 p0 mt20">
										<div class="author-wrap">
											<img src="assets/images/testi-img1.png" class="img-responsive center-block">
											<div class="f-22 w700 lh140 pl15">John Dylan</div>
										</div>
									</div>									
									</div>									
								</div>								
							</div>
							<div class="col-xs-12 col-sm-6">
								<div class="testimonials-block-border">
									<div class="testimonials-block f-18 w400 lh140">
									“I’m a real estate agent who had been taking different routes to improve my biz rankings on Google. Competitors started to edge past my biz, and this led to a downfall.<br><br>  

									<span class="w600">Thankfully, it changed for the better once I opted for Kyza. And boom, the landing pages created with Kyza are 100% SEO friendly & led to a supersonic boost in my rankings. Wow, I would recommend Kyza to all.” </span>
									<div class="col-xs-12 p0 mt20 mt-sm40">
										<div class="author-wrap">
											<img src="assets/images/testi-img2.png" class="img-responsive center-block">
											<div class="f-22 w700 lh140 pl15">Mike East</div>
										</div>
									</div>									
									</div>									
								</div>								
							</div>
						</div>
                     </div>  	
                  </div>
               </div>
            </div>
            <!-- Testimonials Section End -->

			<!-- Aware Section Start -->
            <div class="aware-section">
               <div class="container px-sm15 px0">
                  <div class="row inner-content">
                    <div class="col-xs-12">
                        <div class="f-sm-45 f-28 w700 lh140 text-center black-clr">
                      Are You Aware Of How Much You Need To Pay For  <br class="visible-lg"> Each Of These Tools In The Current Market? 
                        </div>
						<div class="f-20 w400 lh140 black-clr mt20 mt-sm30">
						Well, every business needs a landing page & website builder for their lead generation and marketing campaigns.<br><br>

						And a marketing campaign will fail if you don’t use attention-grabbing graphics elements. So there is no doubt that you need stock images, videos, graphics and much more…<br><br>

						Don’t forget about how you are going to capture the leads and run email marketing campaigns to nurture those leads and turn them into paying customers.
                        </div>
                     </div>				 
                  </div>
               </div>
            </div>
            <!-- Aware Section End -->
			
			<!-- Current Market Section Start -->
            <div class="current-market-section">
               <div class="container px-sm15 px0">
                  <div class="row inner-content">
                    <div class="col-xs-12">
                       <div class="f-19 f-sm-20 w500 lh140 white-clr text-center">
						In-short you’ll need to invest in 4 to 5 different apps for staring and running your business successfully.  <br><br>

						Each of these apps will cost you monthly recurring fees! 
                        </div>
                     </div>
					<div class="col-xs-12 mt20 mt-sm50">
                        <div class="row">
							<div class="col-xs-12 col-sm-3">
								<div class="current-market-wrap">
									<div class="f-22 w600 lh140">Landing Page <br>Builder</div>
									<img src="assets/images/clickfunnels.png" class="img-responsive center-block my20">
									<div class="f-20 w700 lh140 value-wrap">Up To $297/Month</div>
								</div>								
							</div>
							<div class="col-xs-12 col-sm-3 mt50 mt-sm0">
								<div class="current-market-wrap">
									<div class="f-22 w600 lh140">Graphics & Design <br> Toolkit</div>
									<img src="assets/images/canva.png" class="img-responsive center-block my20">
									<div class="f-20 w700 lh140 value-wrap">Up To $30/Month</div>
								</div>								
							</div>
							<div class="col-xs-12 col-sm-3 mt50 mt-sm0">
								<div class="current-market-wrap">
									<div class="f-22 w600 lh140">Lead Generation <br>Optin Forms</div>
									<img src="assets/images/optinmonster.png" class="img-responsive center-block my20">
									<div class="f-20 w700 lh140 value-wrap">Up To $49/Month</div>
								</div>								
							</div>
							<div class="col-xs-12 col-sm-3 mt50 mt-sm0">
								<div class="current-market-wrap">
									<div class="f-22 w600 lh140">Email Marketing - <br>Auto Responder</div>
									<img src="assets/images/mailchimp.png" class="img-responsive center-block my20">
									<div class="f-20 w700 lh140 value-wrap">Up To $299/Month</div>
								</div>								
							</div>
						</div>
                     </div> 
					 <div class="col-xs-12 mt50 mt-sm70">
                       <div class="f-20 w500 lh140 white-clr text-center">
						So just do the simple math here…For all of these apps… 
                        </div>
                     </div>
					 <div class="col-xs-12 mt20 mt-sm50">
                       <div class="f-24 f-sm-34 w700 lh140 white-clr text-center">
						You’ll End Up Paying Over $8,100+ Each Year! 
                        </div>
						 <div class="f-19 f-sm-20 w500 lh140 white-clr text-center mt10">
						For Any Small Or Medium Or Growing Start-up That’s a MASSIVE Investment… 
                        </div>
                     </div>
					 
					  <div class="col-xs-12 mt20 mt-sm50">
                       <div class="f-24 f-sm-34 w700 lh140 orange-clr1 text-center">
						And If You Decide To Outsource Each Of These Tasks To  <br class="visible-lg"> Freelancer Or Agencies… It’ll Put A Hole In Your Pockets! 
                        </div>
						 <div class="f-19 f-sm-20 w500 lh140 white-clr text-center mt10">
						You’ll need to pay anywhere between $50 to $500 for each of these tasks… and You must continue <br class="visible-lg"> paying them every month for getting some of these marketing materials to run your business. Even<br class="visible-lg"> after doing that, you may not know if it’ll solve your business purpose or not! 
                        </div>
                     </div>
					 
					 <div class="col-xs-12 mt20 mt-sm50">
                       <img src="assets/images/proof-img.png" class="img-responsive center-block">
                     </div>
					 
					 <div class="col-xs-12 mt20 mt-sm50">
                       <div class="f-24 f-sm-34 w700 lh140 white-clr text-center">
						But, Why Spend So Much, And Still Not Get Results In Your Favour? 
                        </div>
                     </div> 						 
                  </div>
               </div>
            </div>
            <!-- Current Market Section End -->	
			
			<!-- INtroduction Section -->
			<div class="introduction-section">
			   <div class="container px0 px-sm15">
				  <div class="row inner-content">
					<div class="col-xs-12 p0 text-center">						   
					   <div class="prdly-pres f-sm-45 f-28 w700 white-clr lh140">
                           Introducing…
                        </div>
					</div>
					<div class="col-xs-12 mt20 mt-sm70">
						 <img src="assets/images/product-intro.png" class="img-responsive center-block">
                    </div>				
				  </div>
			   </div>
			</div>
			<!-- INtroduction Section End -->
			
			<!-- App Suite Section -->
			<div class="appsuite-section">
			   <div class="container px0 px-sm15">
				  <div class="row inner-content">
					<div class="col-xs-12">						
                       <div class="f-sm-43 f-28 w700 lh140 black-clr text-center">
						The <span class="headline-style">All-In-One Business App Suite</span> You Ever Need To Capture Crazy Leads, Crash Sales & Pocket Real Biz Profits!
						</div>	
                     </div>
					<div class="col-xs-12 mt20 mt-sm50">
                        <div class="row">
                           <div class="col-xs-12 col-sm-4">
                              <div class="appsuite-block">
								 <img src="assets/images/icon1.png" class="img-responsive center-block">	
                                 <div class="f-24 f-sm-30 w600 lh140 blue-clr mt20 mt-sm45">
                                   All-In-One Business Growth Solution
                                 </div>
								  <div class="f-20 f-sm-22 w400 lh140 black-clr mt15 mt-sm35">
                                To Let You Create Landing Pages, Optin Forms, Pop-Ups,  Graphics And Send Unlimited Biz Emails All Under One Dashboard!
                                 </div>
                              </div>
                           </div> 
							<div class="col-xs-12 col-sm-4  mt20 mt-sm0">
                              <div class="appsuite-block"> 
								<img src="assets/images/icon2.png" class="img-responsive center-block">
                                 <div class="f-24 f-sm-30 w600 lh140 blue-clr mt20">
                                  Mobile Ready Solution For Pages, Email, Graphics & More…
                                 </div>
								  <div class="f-20 f-sm-22 w400 lh140 black-clr mt15">
                                Creates Pages, Email, Graphics That Works Smoothly On Any Mobile, Tablet, Laptop Devices Super-Fast…
                                 </div>
                              </div>
                           </div>
						   <div class="col-xs-12 col-sm-4 mt20 mt-sm0">
                              <div class="appsuite-block">  
                              <img src="assets/images/icon3.png" class="img-responsive center-block">
                                 <div class="f-24 f-sm-30 w600 lh140 blue-clr mt20">
                                  Robust & Proven Technology That Works For Every Niche
                                 </div>
								  <div class="f-20 f-sm-22 w400 lh140 black-clr mt15">
                                Robust & Proven AI-Enabled Pop-ups & Optin Forms Technology Boosts Leads By Showing Offers As Per Visitor’s Interest
                                 </div>
                              </div>
                           </div>							
                        </div>
                     </div>	
					<div class="col-xs-12 col-sm-8 col-sm-offset-2 mt20 mt-sm30">
                        <div class="row">
                           <div class="col-xs-12 col-sm-6">
                              <div class="appsuite-block">  
								<img src="assets/images/icon4.png" class="img-responsive center-block">
                                 <div class="f-24 f-sm-30 w600 lh140 blue-clr mt20">
                                  Commercial License Included Allowing You To Provide Services To Hungry Clients
                                 </div>
								  <div class="f-20 f-sm-22 w400 lh140 black-clr mt15">
                                Offer Landing Pages, Email Marketing & Other Biz Growth Services & Pocket 100% Profit Of Every Sale!
                                 </div>
                              </div>
                           </div> 
							<div class="col-xs-12 col-sm-6 mt20 mt-sm0">
                              <div class="appsuite-block"> 
								<img src="assets/images/icon5.png" class="img-responsive center-block">
                                 <div class="f-24 f-sm-30 w600 lh140 blue-clr mt20">
                                  Fast-Action Bonuses Worth $1,988 Available FREE!!!
                                 </div>
								  <div class="f-20 f-sm-22 w400 lh140 black-clr mt15">
                               To Let You Make The Most Of Kyza And Ensure Every Marketing Activity Stands Out At No Extra Cost!
							   <br class="visible-lg"> <br class="visible-lg">
                                 </div>
                              </div>
                           </div>						  							
                        </div>
                     </div>	
					
				  </div>
			   </div>
			</div>
			<!-- App Suite Section End -->
			
		<!-- Demo Section Start -->
			<div class="demo-section" id="demo">
			   <div class="container px-sm15 px0">
				  <div class="row inner-content">
					 <div class="col-xs-12">
						<div class="f-sm-45 f-28 w700 lh140 text-center ">
						  See For Yourself How EASY & POWERFUL Kyza Is:
						</div>
					 </div>
					 <div class="col-xs-12 col-sm-10 col-sm-offset-1 mt20 mt-sm50">
						<div class="responsive-video">
							<iframe src="https://kyza.dotcompal.com/video/embed/irq6bfzf2m" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe><
						</div>
					 </div>
					 <div class="col-xs-12 mt20 mt-sm70">
		  <div class="row">
			 <div class="col-md-12 col-sm-12 col-xs-12 text-center">
				<div class="f-md-26 f-sm-22 f-18 text-center mt3 black lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
				<div class="f-sm-24 f-22 text-center purple lh120 w700 mt15 mt-sm20 purple">TAKE ACTION NOW!</div>
				<div class="f-sm-24 f-17 lh120 w600 mt15 mt-sm20">Use Discount Coupon <span class="w800 purple">"kyza6"</span> for Instant <span class="w700 purple">6% Discount</span> on Commercial Plan</div>
			 </div>
			 <div class="col-md-12 col-md-offset-0 col-sm-10 col-sm-offset-1 col-xs-12 col-xs-offset-0 text-center mt15 mt-sm20">
				<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
				<span class="text-center">Grab Kyza + My 20 Exclusive Bonuses</span> <br>
				</a>
			 </div>
			 <div class="col-md-10 col-md-offset-1 col-sm-12 col-sm-offset-0 col-xs-12 col-xs-offset-0 mt15 mt-sm20">
				<img src="assets/images/payment.png" class="img-responsive center-block">
			 </div>
			 <div class="col-xs-12 mt15 mt-sm20" align="center">
				<h3 class="f-md-35 f-sm-28 f-20 w500 text-center black">Coupon Is Expiring In... </h3>
			 </div>
			 <!-- Timer -->
			 <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 col-xs-12 text-center">
				<div class="countdown counter-black">
				   <div class="timer-label text-center"><span class="f-31 f-sm-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-sm-18 w500 smmltd">Days</span> </div>
				   <div class="timer-label text-center"><span class="f-31 f-sm-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-sm-18 w500 smmltd">Hours</span> </div>
				   <div class="timer-label text-center timer-mrgn"><span class="f-31 f-sm-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-sm-18 w500 smmltd">Mins</span> </div>
				   <div class="timer-label text-center "><span class="f-31 f-sm-60 timerbg oswald">37</span><br><span class="f-14 f-sm-18 w500 smmltd">Sec</span> </div>
				</div>
			 </div>
			 <!-- Timer End -->
		  </div>
	   </div>
				  </div>
			   </div>
			</div>
			<!-- Demo Section End -->			
	

	
	
  	<!-- Bonus Section Header Start -->
	<div class="bonus-header">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-10 col-sm-offset-1 heading-bg text-center p0">
					<div class="f-24 f-sm-36 lh140 w700">When You Purchase Kyza, You Also Get <br class="hidden-xs"> Instant Access To These 20 Exclusive Bonuses</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Bonus Section Header End -->

	<!-- Bonus #1 Section Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
			 <div class="col-xs-12 text-center">
				<div class="bonus-title-bg">
				   <div class="f-22 f-sm-28 lh120 w700">Bonus 1</div>
				</div>
			 </div>
			 <div class="col-xs-12 mt20 mt-sm30 d-flex align-content-center flex-wrap">
				<div class="row">
				   <div class="col-sm-5 col-xs-12">
					  <img src="assets/images/bonus1.png" class="img-responsive center-block bonus-img">
				   </div>
				   <div class="col-sm-7 col-xs-12 mt20 mt-sm0">
					  <div class="f-22 f-sm-34 w600 lh140 bonus-title-color">
						 33 Mobile Responsive Sales Pages
					  </div>
					  <ul class="bonus-list f-20 f-sm-22 lh140 w400 mt20 mt-sm30 p0">
						<li>Get Ready to go Mobile Responsive Squeeze Pages, Landing pages and Sales Pages!</li>
						<li>This Bonus when combined with Kyza will 10x your Conversions by providing various Landing Page Templates for all Kinds of Businesses and Freelancing Services  </li>						
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #1 Section End -->

	<!-- Bonus #2 Section Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
			
			 <div class="col-xs-12">
				<div class="row d-flex align-items-center flex-wrap">
				  
				   <div class="col-sm-7 col-xs-12 mt20 mt-sm0 ">
					 <div class="col-xs-12 xstext1">
				<div class="bonus-title-bg">
				   <div class="f-22 f-sm-28 lh120 w700">Bonus 2</div>
				</div>
			 </div>
					  <div class="col-xs-12 p0 f-22 f-sm-34 w600 lh140 bonus-title-color mt20 mt-sm30">
						Make First $100 On The Web
					  </div>
					  <ul class="col-xs-12 p0 bonus-list f-20 f-sm-22 lh140 w400 mt20 mt-sm30 p0">
						 <li>Get Affiliate Marketing Course with the Blueprint-Steps of a Successful Online Business </li>
						 <li>This Bonus when combined with Kyza’s Online Business Tools will ease your way to make your First $100 on The Web. </li>
					  </ul>
				   </div>
				    <div class="col-sm-5 col-xs-12 ">
					  <img src="assets/images/bonus2.png" class="img-responsive center-block">
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #2 Section End -->

	<!-- Bonus #3 Section Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
			 <div class="col-xs-12 text-center">
				<div class="bonus-title-bg">
				   <div class="f-22 f-sm-28 lh120 w700">Bonus 3</div>
				</div>
			 </div>
			 <div class="col-xs-12 mt20 mt-sm30 d-flex align-content-center flex-wrap">
				<div class="row">
				   <div class="col-sm-5 col-xs-12">
					  <img src="assets/images/bonus3.png" class="img-responsive center-block bonus-img">
				   </div>
				   <div class="col-sm-7 col-xs-12 mt20 mt-sm0">
					  <div class="f-22 f-sm-34 w600 lh140 bonus-title-color">
						 More Subscribers
					  </div>
					  <ul class="bonus-list f-20 f-sm-22 lh140 w400 mt20 mt-sm30 p0">
						 <li>Discover 100 Profitable and Latest Tips To Generate More Subscribers To Your Email List. </li>
						 <li>This Bonus with Kyza’s Highly Converting Email templates and Autoresponder Integration will Boost your Sales. </li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #3 Section End -->

	<!-- Bonus #4 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
			 <div class="col-xs-12 xstext1">
				<div class="bonus-title-bg">
				   <div class="f-22 f-sm-28 lh120 w700">Bonus 4</div>
				</div>
			 </div>
			 <div class="col-xs-12 mt20 mt-sm30 d-flex align-content-center flex-wrap">
				<div class="row">
				   <div class="col-sm-5 col-xs-12 col-sm-push-7">
					  <img src="assets/images/bonus4.png" class="img-responsive center-block bonus-img">
				   </div>
				   <div class="col-sm-7 col-xs-12 mt20 mt-sm0 col-sm-pull-5">
					  <div class="f-22 f-sm-34 w600 lh140 bonus-title-color">
						Niche Graphics Set 12 Pack
					  </div>
					  <ul class="bonus-list f-20 f-sm-22 lh140 w400 mt20 mt-sm30 p0">
							<li>This pack includes 12 Simple but highly profitable and recommended Niched Graphics with additional 7 Important Latest Tools  </li>
							<li>This Bonus with Kyza will help you to multiply your profits by building Businesses in different Niches </li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #4 End -->

	<!-- Bonus #5 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
			 <div class="col-xs-12 text-center">
				<div class="bonus-title-bg">
				   <div class="f-22 f-sm-28 lh120 w700">Bonus 5</div>
				</div>
			 </div>
			 <div class="col-xs-12 mt20 mt-sm30 d-flex align-content-center flex-wrap">
				<div class="row">
				   <div class="col-sm-5 col-xs-12">
					  <img src="assets/images/bonus5.png" class="img-responsive center-block bonus-img">
				   </div>
				   <div class="col-sm-7 col-xs-12 mt20 mt-sm0">
					  <div class="f-22 f-sm-34 w600 lh140 bonus-title-color">
						Search Marketing 2.0
					  </div>
					  <ul class="bonus-list f-20 f-sm-22 lh140 w400 mt20 mt-sm30 p0">
							<li>Get professionally designed lead capture system that comes with complete website and email course!  </li>
							<li>This Bonus is Super Add-On to Kyza’s Social Mozo Feature to capture Hot Leads from the Internet and run Social Media Campaigns with Analytics </li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #5 End -->

	<!-- CTA Button Section Start -->
	<div class="cta-btn-section">
	   <div class="container">
		  <div class="row">
			 <div class="col-md-12 col-sm-12 col-xs-12 text-center">
				<div class="f-md-26 f-sm-22 f-18 text-center mt3 black lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
				<div class="f-sm-24 f-22 text-center purple lh120 w700 mt15 mt-sm20 purple">TAKE ACTION NOW!</div>
				<div class="f-sm-24 f-17 lh120 w600 mt15 mt-sm20">Use Discount Coupon <span class="w800 purple">"kyza6"</span> for Instant <span class="w700 purple">6% Discount</span> on Commercial Plan</div>
			 </div>
			 <div class="col-md-12 col-md-offset-0 col-sm-10 col-sm-offset-1 col-xs-12 col-xs-offset-0 text-center mt15 mt-sm20">
				<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
				<span class="text-center">Grab Kyza + My 20 Exclusive Bonuses</span> <br>
				</a>
			 </div>
			 <div class="col-md-10 col-md-offset-1 col-sm-12 col-sm-offset-0 col-xs-12 col-xs-offset-0 mt15 mt-sm20">
				<img src="assets/images/payment.png" class="img-responsive center-block">
			 </div>
			 <div class="col-xs-12 mt15 mt-sm20" align="center">
				<h3 class="f-md-35 f-sm-28 f-20 w500 text-center black">Coupon Is Expiring In... </h3>
			 </div>
			 <!-- Timer -->
			 <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 col-xs-12 text-center">
				<div class="countdown counter-black">
				   <div class="timer-label text-center"><span class="f-31 f-sm-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-sm-18 w500 smmltd">Days</span> </div>
				   <div class="timer-label text-center"><span class="f-31 f-sm-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-sm-18 w500 smmltd">Hours</span> </div>
				   <div class="timer-label text-center timer-mrgn"><span class="f-31 f-sm-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-sm-18 w500 smmltd">Mins</span> </div>
				   <div class="timer-label text-center "><span class="f-31 f-sm-60 timerbg oswald">37</span><br><span class="f-14 f-sm-18 w500 smmltd">Sec</span> </div>
				</div>
			 </div>
			 <!-- Timer End -->
		  </div>
	   </div>
	</div>
	<!-- CTA Button Section End   -->


	<!-- Bonus #6 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
			 <div class="col-xs-12 xstext1">
				<div class="bonus-title-bg">
				   <div class="f-22 f-sm-28 lh120 w700">Bonus 6</div>
				</div>
			 </div>
			 <div class="col-xs-12 mt20 mt-sm30 d-flex align-content-center flex-wrap">
				<div class="row">
				   <div class="col-sm-5 col-sm-push-7 col-xs-12">
					  <img src="assets/images/bonus6.png" class="img-responsive center-block bonus-img">
				   </div>
				   <div class="col-sm-7 col-sm-pull-5 col-xs-12 mt20 mt-sm0">
					  <div class="f-22 f-sm-34 w600 lh140 bonus-title-color">
						Build Your Audience
					  </div>
					  <ul class="bonus-list f-20 f-sm-22 lh140 w400 mt20 mt-sm30 p0">
						 <li>Get Super-Valuable Guide teaching you the secrets and strategies to build a loyal audience along with ready sales materials  </li>
						 <li>This Bonus along with Kyza’s Webinar Integration will give you unique brand identity by building new hot and loyal audience  </li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #6 End -->

	<!-- Bonus #7 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
			 <div class="col-xs-12 text-center">
				<div class="bonus-title-bg">
				   <div class="f-22 f-sm-28 lh120 w700">Bonus 7</div>
				</div>
			 </div>
			 <div class="col-xs-12 mt20 mt-sm30 d-flex align-content-center flex-wrap">
				<div class="row">
				   <div class="col-sm-5 col-xs-12">
					  <img src="assets/images/bonus7.png" class="img-responsive center-block bonus-img">
				   </div>
				   <div class="col-sm-7 col-xs-12 mt20 mt-sm0">
					  <div class="f-22 f-sm-34 w600 lh140 bonus-title-color">
						Fancy Box Poppers
					  </div>
					  <ul class="bonus-list f-20 f-sm-22 lh140 w400 mt20 mt-sm30 p0">
						 <li>Quickly Create Professional Lightweight PopUp Boxes with Customizable Popup size and Timeout </li>
						<li>This Bonus when combined with Kyza’s Customizable Popup Campaigns will boost your Landing Pages Conversions  </li>				
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #7 End -->

	<!-- Bonus #8 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
			 <div class="col-xs-12 xstext1">
				<div class="bonus-title-bg">
				   <div class="f-22 f-sm-28 lh120 w700">Bonus 8</div>
				</div>
			 </div>
			 <div class="col-xs-12 mt20 mt-sm30 d-flex align-content-center flex-wrap">
				<div class="row">
				   <div class="col-sm-5 col-sm-push-7 col-xs-12">
					  <img src="assets/images/bonus8.png" class="img-responsive center-block bonus-img">
				   </div>
				   <div class="col-sm-7 col-sm-pull-5 col-xs-12 mt20 mt-sm0">
					  <div class="f-22 f-sm-34 w600 lh140 bonus-title-color">
						 10 Free SEO Tools
					  </div>
					  <ul class="bonus-list f-20 f-sm-22 lh140 w400 mt20 mt-sm30 p0">
						 <li>Get 10 Free SEO tools for keyword research, link building, backlink checking, analytics, on-page optimization and many more. </li>
						 <li>This Bonus when combined with Kyza’s SEO Keyword integration will help your Landing Pages Rank #1 on Google and Other Search Engines </li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #8 End -->

	<!-- Bonus #9 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
			 <div class="col-xs-12 text-center">
				<div class="bonus-title-bg">
				   <div class="f-22 f-sm-28 lh120 w700">Bonus 9</div>
				</div>
			 </div>
			 <div class="col-xs-12 mt20 mt-sm30 d-flex align-content-center flex-wrap">
				<div class="row">
				   <div class="col-sm-5 col-xs-12">
					  <img src="assets/images/bonus9.png" class="img-responsive center-block bonus-img">
				   </div>
				   <div class="col-sm-7 col-xs-12 mt20 mt-sm0">
					  <div class="f-22 f-sm-34 w600 lh140 bonus-title-color">
						 Evergreen Internet Profits
					  </div>
					  <ul class="bonus-list f-20 f-sm-22 lh140 w400 mt20 mt-sm30 p0">
						 <li>Get Step-by-Step Blueprint used by the most successful entrepreneurs to grow their online businesses from zero to everlasting profits </li>
						 <li>This Bonus as guide will help any Newbie to start an Online Business from scratch and build an everlasting brand on Kyza </li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #9 End -->

	<!-- Bonus #10 start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
			 <div class="col-xs-12 xstext1">
				<div class="bonus-title-bg">
				   <div class="f-22 f-sm-28 lh120 w700">Bonus 10</div>
				</div>
			 </div>
			 <div class="col-xs-12 mt20 mt-sm30 d-flex align-content-center flex-wrap">
				<div class="row">
				   <div class="col-sm-5 col-sm-push-7 col-xs-12">
					  <img src="assets/images/bonus10.png" class="img-responsive center-block bonus-img">
				   </div>
				   <div class="col-sm-7 col-sm-pull-5 col-xs-12 mt20 mt-sm0">
					  <div class="f-22 f-sm-34 w600 lh140 bonus-title-color">
							Gangsta Optins Plugin
					  </div>
					  <ul class="bonus-list f-20 f-sm-22 lh140 w400 mt20 mt-sm30 p0">
						 <li>Discover how you can easily create beautiful forms in just minutes - all without leaving your dashboard! </li>
						 <li>This Bonus is must-to-have Bonus with Kyza’s Webform to encourage your prospects signup and become your hot lead </li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #10 End -->


	<!-- CTA Button Section Start -->
	<div class="cta-btn-section">
	   <div class="container">
		  <div class="row">
			 <div class="col-md-12 col-sm-12 col-xs-12 text-center">
				<div class="f-md-26 f-sm-22 f-18 text-center mt3 black lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
				<div class="f-sm-24 f-22 text-center purple lh120 w700 mt15 mt-sm20 purple">TAKE ACTION NOW!</div>
				<div class="f-sm-24 f-17 lh120 w600 mt15 mt-sm20">Use Discount Coupon <span class="w800 purple">"kyza6"</span> for Instant <span class="w700 purple">6% Discount</span> on Commercial Plan</div>
			 </div>
			 <div class="col-md-12 col-md-offset-0 col-sm-10 col-sm-offset-1 col-xs-12 col-xs-offset-0 text-center mt15 mt-sm20">
				<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
				<span class="text-center">Grab Kyza + My 20 Exclusive Bonuses</span> <br>
				</a>
			 </div>
			 <div class="col-md-10 col-md-offset-1 col-sm-12 col-sm-offset-0 col-xs-12 col-xs-offset-0 mt15 mt-sm20">
				<img src="assets/images/payment.png" class="img-responsive center-block">
			 </div>
			 <div class="col-xs-12 mt15 mt-sm20" align="center">
				<h3 class="f-md-35 f-sm-28 f-20 w500 text-center black">Coupon Is Expiring In... </h3>
			 </div>
			 <!-- Timer -->
			 <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 col-xs-12 text-center">
				<div class="countdown counter-black">
				   <div class="timer-label text-center"><span class="f-31 f-sm-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-sm-18 w500 smmltd">Days</span> </div>
				   <div class="timer-label text-center"><span class="f-31 f-sm-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-sm-18 w500 smmltd">Hours</span> </div>
				   <div class="timer-label text-center timer-mrgn"><span class="f-31 f-sm-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-sm-18 w500 smmltd">Mins</span> </div>
				   <div class="timer-label text-center "><span class="f-31 f-sm-60 timerbg oswald">37</span><br><span class="f-14 f-sm-18 w500 smmltd">Sec</span> </div>
				</div>
			 </div>
			 <!-- Timer End -->
		  </div>
	   </div>
	</div>
	<!-- CTA Button Section End -->


	<!-- Bonus #11 start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
			 <div class="col-xs-12 text-center">
				<div class="bonus-title-bg">
				   <div class="f-22 f-sm-28 lh120 w700">Bonus 11</div>
				</div>
			 </div>
			 <div class="col-xs-12 mt20 mt-sm30 d-flex align-content-center flex-wrap">
				<div class="row">
				   <div class="col-sm-5 col-xs-12">
					  <img src="assets/images/bonus11.png" class="img-responsive center-block bonus-img">
				   </div>
				   <div class="col-sm-7 col-xs-12 mt20 mt-sm0">
					  <div class="f-22 f-sm-34 w600 lh140 bonus-title-color">
						Mobile Optimizer WP Plugin
					  </div>
					  <ul class="bonus-list f-20 f-sm-22 lh140 w400 mt20 mt-sm30 p0">
						 <li>GetPremiumDesigned Plugin to optimize your WordPress blogs for mobile devices  </li>
						 <li>Using this Bonus with Kyza Pup-up feature, you can increase conversions on your Wordpress Blog by displaying MobileOptimized Content and Popups. </li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #11 End -->


	<!-- Bonus #12 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
			 <div class="col-xs-12 xstext1">
				<div class="bonus-title-bg">
				   <div class="f-22 f-sm-28 lh120 w700">Bonus 12</div>
				</div>
			 </div>
			 <div class="col-xs-12 mt20 mt-sm30 d-flex align-content-center flex-wrap">
				<div class="row">
				   <div class="col-sm-5 col-sm-push-7 col-xs-12">
					  <img src="assets/images/bonus12.png" class="img-responsive center-block bonus-img">
				   </div>
				   <div class="col-sm-7 col-sm-pull-5 col-xs-12 mt20 mt-sm0">
					  <div class="f-22 f-sm-34 w600 lh140 bonus-title-color">
						 Bootstrap Web Development
					  </div>
					  <ul class="bonus-list f-20 f-sm-22 lh140 w400 mt20 mt-sm30 p0">
						 <li>Get 26-part video tutorial teaching you the main components of Twitter Bootstrap 3 with <b>CSS Framework Overview.</b></li>
						 <li>This Bonus also includes16 Reusable BootStrap templates that you can quickly copy and paste into Kyza Landing Pages  </li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #12 End -->

	<!-- Bonus #13 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
			 <div class="col-xs-12 text-center">
				<div class="bonus-title-bg">
				   <div class="f-22 f-sm-28 lh120 w700">Bonus 13</div>
				</div>
			 </div>
			 <div class="col-xs-12 mt20 mt-sm30 d-flex align-content-center flex-wrap">
				<div class="row">
				   <div class="col-sm-5 col-xs-12">
					  <img src="assets/images/bonus13.png" class="img-responsive center-block bonus-img">
				   </div>
				   <div class="col-sm-7 col-xs-12 mt20 mt-sm0">
					  <div class="f-22 f-sm-34 w600 lh140 bonus-title-color">
						RSS 2 Email
					  </div>
					  <ul class="bonus-list f-20 f-sm-22 lh140 w400 mt20 mt-sm30 p0">
						 <li>Step-by-Step Video Tutorials using Little-Known "RSS 2 Email" Tactics revealing The Dirty Little Secrets of Millionaire Bloggers </li>
						 <li>Combine this Bonus with countless features of Kyza to build your Million Dollar Dynamic Blog. </li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #13 End -->

	<!-- Bonus #14 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
			 <div class="col-xs-12 xstext1">
				<div class="bonus-title-bg">
				   <div class="f-22 f-sm-28 lh120 w700">Bonus 14</div>
				</div>
			 </div>
			 <div class="col-xs-12 mt20 mt-sm30 d-flex align-content-center flex-wrap">
				<div class="row">
				   <div class="col-sm-5 col-sm-push-7 col-xs-12">
					  <img src="assets/images/bonus14.png" class="img-responsive center-block bonus-img">
				   </div>
				   <div class="col-sm-7 col-sm-pull-5 col-xs-12 mt20 mt-sm0">
					  <div class="f-22 f-sm-34 w600 lh140 bonus-title-color">
						10 Product Review Affiliate Websites
					  </div>
					  <ul class="bonus-list f-20 f-sm-22 lh140 w400 mt20 mt-sm30 p0">
						 <li>Discover The Power Super Affiliates Are Using To Effortlessly Produce THOUSANDS Of Dollars By Promoting Other People's Products. </li>
						 <li>Use this Bonus and build A 6-Figure Business With The Amazing Power Of Ready-Made Product Review Websites on Kyza </li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #14 End -->


	<!-- Bonus #15 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
			 <div class="col-xs-12 text-center">
				<div class="bonus-title-bg">
				   <div class="f-22 f-sm-28 lh120 w700">Bonus 15</div>
				</div>
			 </div>
			 <div class="col-xs-12 mt20 mt-sm30 d-flex align-content-center flex-wrap">
				<div class="row">
				   <div class="col-sm-5 col-xs-12">
					  <img src="assets/images/bonus15.png" class="img-responsive center-block bonus-img">
				   </div>
				   <div class="col-sm-7 col-xs-12 mt20 mt-sm0">
					  <div class="f-22 f-sm-34 w600 lh140 bonus-title-color">
						 Email Auto Format
					  </div>
					  <ul class="bonus-list f-20 f-sm-22 lh140 w400 mt20 mt-sm30 p0">
							<li>Discover How You Can Get White-Listed, and Get Your Email Past The SPAM Filters  </li>
							<li>This Bonus when Combined with Kyza Mail and Autoresponder Integration can boost your E-mail reach and Lead Conversions  </li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #15 End -->


	<!-- CTA Button Section Start -->
	<div class="cta-btn-section">
	   <div class="container">
		  <div class="row">
			 <div class="col-md-12 col-sm-12 col-xs-12 text-center">
				<div class="f-md-26 f-sm-22 f-18 text-center mt3 black lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
				<div class="f-sm-24 f-22 text-center purple lh120 w700 mt15 mt-sm20 purple">TAKE ACTION NOW!</div>
				<div class="f-sm-24 f-17 lh120 w600 mt15 mt-sm20">Use Discount Coupon <span class="w800 purple">"kyza6"</span> for Instant <span class="w700 purple">6% Discount</span> on Commercial Plan</div>
			 </div>
			 <div class="col-md-12 col-md-offset-0 col-sm-10 col-sm-offset-1 col-xs-12 col-xs-offset-0 text-center mt15 mt-sm20">
				<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
				<span class="text-center">Grab Kyza + My 20 Exclusive Bonuses</span> <br>
				</a>
			 </div>
			 <div class="col-md-10 col-md-offset-1 col-sm-12 col-sm-offset-0 col-xs-12 col-xs-offset-0 mt15 mt-sm20">
				<img src="assets/images/payment.png" class="img-responsive center-block">
			 </div>
			 <div class="col-xs-12 mt15 mt-sm20" align="center">
				<h3 class="f-md-35 f-sm-28 f-20 w500 text-center black">Coupon Is Expiring In... </h3>
			 </div>
			 <!-- Timer -->
			 <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 col-xs-12 text-center">
				<div class="countdown counter-black">
				   <div class="timer-label text-center"><span class="f-31 f-sm-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-sm-18 w500 smmltd">Days</span> </div>
				   <div class="timer-label text-center"><span class="f-31 f-sm-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-sm-18 w500 smmltd">Hours</span> </div>
				   <div class="timer-label text-center timer-mrgn"><span class="f-31 f-sm-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-sm-18 w500 smmltd">Mins</span> </div>
				   <div class="timer-label text-center "><span class="f-31 f-sm-60 timerbg oswald">37</span><br><span class="f-14 f-sm-18 w500 smmltd">Sec</span> </div>
				</div>
			 </div>
			 <!-- Timer End -->
		  </div>
	   </div>
	</div>
	<!-- CTA Button Section End -->
	
	<!-- Bonus #16 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
			 <div class="col-xs-12 xstext1">
				<div class="bonus-title-bg">
				   <div class="f-22 f-sm-28 lh120 w700">Bonus 16</div>
				</div>
			 </div>
			 <div class="col-xs-12 mt20 mt-sm30 d-flex align-content-center flex-wrap">
				<div class="row">
				   <div class="col-sm-5 col-sm-push-7 col-xs-12">
					  <img src="assets/images/bonus16.png" class="img-responsive center-block bonus-img">
				   </div>
				   <div class="col-sm-7 col-sm-pull-5 col-xs-12 mt20 mt-sm0">
					  <div class="f-22 f-sm-34 w600 lh140 bonus-title-color">
						How To Create An Out-of-Control Viral Marketing Campaign!
					  </div>
					  <ul class="bonus-list f-20 f-sm-22 lh140 w400 mt20 mt-sm30 p0">
						 <li>Powerful Techniques on Viral List Building, Affiliate Marketing and Brand Name Building While Leveraging On The Internet </li>
						 <li>This Bonus will Viral your marketing campaigns when build using Kyza Landing Pages, various Niche Templates and Social Mozo. </li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #16 End -->

	<!-- Bonus #17 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
			 <div class="col-xs-12 text-center">
				<div class="bonus-title-bg">
				   <div class="f-22 f-sm-28 lh120 w700">Bonus 17</div>
				</div>
			 </div>
			 <div class="col-xs-12 mt20 mt-sm30 d-flex align-content-center flex-wrap">
				<div class="row">
				   <div class="col-sm-5 col-xs-12">
					  <img src="assets/images/bonus17.png" class="img-responsive center-block bonus-img">
				   </div>
				   <div class="col-sm-7 col-xs-12 mt20 mt-sm0">
					  <div class="f-22 f-sm-34 w600 lh140 bonus-title-color">
						One Click Mobile Web App
					  </div>
					  <ul class="bonus-list f-20 f-sm-22 lh140 w400 mt20 mt-sm30 p0">
						 <li>Brand New WordPress Plugin That Turns Any Website Into An Web App & Pushes To Install on your Customers’ iPhone  </li>
						<li>With this Bonus along with Kyza’s unlimited Mobile Responsive Features you can build a unique brand image with your own one click mobile App.  </li>				
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #17 End -->

	<!-- Bonus #18 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
			 <div class="col-xs-12 xstext1">
				<div class="bonus-title-bg">
				   <div class="f-22 f-sm-28 lh120 w700">Bonus 18</div>
				</div>
			 </div>
			 <div class="col-xs-12 mt20 mt-sm30 d-flex align-content-center flex-wrap">
				<div class="row">
				   <div class="col-sm-5 col-sm-push-7 col-xs-12">
					  <img src="assets/images/bonus18.png" class="img-responsive center-block bonus-img">
				   </div>
				   <div class="col-sm-7 col-sm-pull-5 col-xs-12 mt20 mt-sm0">
					  <div class="f-22 f-sm-34 w600 lh140 bonus-title-color">
						 Writing Tips Made Easy
					  </div>
					  <ul class="bonus-list f-20 f-sm-22 lh140 w400 mt20 mt-sm30 p0">
						 <li>This ebook with a compilation of 25 writing tips which have been designed to help you become professional content writer. </li>
						 <li>This bonus when combined with Kyza Templates, will help you build high converting attractive landing pages with professional content. </li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #18 End -->

	<!-- Bonus #19 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
			 <div class="col-xs-12 text-center">
				<div class="bonus-title-bg">
				   <div class="f-22 f-sm-28 lh120 w700">Bonus 19</div>
				</div>
			 </div>
			 <div class="col-xs-12 mt20 mt-sm30 d-flex align-content-center flex-wrap">
				<div class="row">
				   <div class="col-sm-5 col-xs-12">
					  <img src="assets/images/bonus19.png" class="img-responsive center-block bonus-img">
				   </div>
				   <div class="col-sm-7 col-xs-12 mt20 mt-sm0">
					  <div class="f-22 f-sm-34 w600 lh140 bonus-title-color">
						 Secrets To Free Web Hosting
					  </div>
					  <ul class="bonus-list f-20 f-sm-22 lh140 w400 mt20 mt-sm30 p0">
						 <li>Discover the secrets to free web hosting and get a massive break in cash spent! </li>
						 <li>Using this Bonus as information and secrets you can build your own Ebook on Kyza and use it as lead magnet to generate leads </li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #19 End -->

	<!-- Bonus #20 start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
			 <div class="col-xs-12 xstext1">
				<div class="bonus-title-bg">
				   <div class="f-22 f-sm-28 lh120 w700">Bonus 20</div>
				</div>
			 </div>
			 <div class="col-xs-12 mt20 mt-sm30 d-flex align-content-center flex-wrap">
				<div class="row">
				   <div class="col-sm-5 col-sm-push-7 col-xs-12">
					  <img src="assets/images/bonus20.png" class="img-responsive center-block bonus-img">
				   </div>
				   <div class="col-sm-7 col-sm-pull-5 col-xs-12 mt20 mt-sm0">
					  <div class="f-22 f-sm-34 w600 lh140 bonus-title-color">
						Insta Profit Magnet
					  </div>
					  <ul class="bonus-list f-20 f-sm-22 lh140 w400 mt20 mt-sm30 p0">
						 <li>With this guide you will discover a list of strategies, tools and practices that can assist you building your Instagram business. </li>
						 <li>Use this Bonus as Instagram Business Guide to create Brand awareness and build a high profitable Business using Kyza’s Unlimited Features </li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #20 End -->

	<!-- Huge Woth Section Start -->
	<div class="huge-area mt30 mt-sm10">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-xs-12 text-center">
                    <div class="f-sm-65 f-40 lh120 w700 black-clr">That’s Huge Worth of</div>
                    <br>
                    <div class="f-sm-60 f-40 lh120 w800 kapblue">$2565!</div>
                </div>
            </div>
        </div>
    </div>
	<!-- Huge Worth Section End -->

	<!-- text Area Start -->
	<div class="white-section">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-xs-12 text-center p0">
                	<div class="f-sm-36 f-25 lh140 w400">So what are you waiting for? You have a great opportunity <br class="hidden-xs"> ahead + <span class="w600">My 20 Bonus Products</span> are making it a <br class="hidden-xs"> <b>completely NO Brainer!!</b></div>
            	</div>
			</div>
		</div>
	</div>
	<!-- text Area End -->

	<!-- CTA Button Section Start -->
	<div class="cta-btn-section">
	   <div class="container">
		  <div class="row">
			 <div class="col-md-12 col-md-offset-0 col-sm-10 col-sm-offset-1 col-xs-12 col-xs-offset-0 text-center">
				<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
				<span class="text-center">Grab Kyza + My 20 Exclusive Bonuses</span> <br>
				</a>
			 </div>
			 
			 <div class="col-xs-12 mt15 mt-sm20" align="center">
				<h3 class="f-sm-30 f-20 w500 text-center black">My Exclusive Bonuses Are Expiring in...</h3>
			 </div>
			 <!-- Timer -->
			 <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 col-xs-12 text-center">
				<div class="countdown counter-black">
				   <div class="timer-label text-center"><span class="f-31 f-sm-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-sm-18 w500 smmltd ">Days</span> </div>
				   <div class="timer-label text-center"><span class="f-31 f-sm-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-sm-18 w500 smmltd">Hours</span> </div>
				   <div class="timer-label text-center timer-mrgn"><span class="f-31 f-sm-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-sm-18 w500 smmltd">Mins</span> </div>
				   <div class="timer-label text-center "><span class="f-31 f-sm-60 timerbg oswald">37</span><br><span class="f-14 f-sm-18 w500 smmltd">Sec</span> </div>
				</div>
			 </div>
			 <!-- Timer End -->
		  </div>
	   </div>
	</div>
	<!-- CTA Button Section End -->

	
	
	<!-- Footer Section Start -->
	<div class="strip_footer clear mt20 mt-sm40">
        <div class="container">
            <div class="row">
                <img src="assets/images/white-logo.png" alt="logo" class="img-responsive center-block">
                <div class="col-md-12 col-sm-12 col-xs-12 f-18 f-sm-18 w400 mt15 mt-sm35 lh140 white-clr text-center">
                   Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. 
                    
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-sm-3 col-xs-12 f-18 f-sm-20 w400 w400 mt10 mt-sm58 lh140 white-clr text-xs-center">Copyright © Kyza</div>

                    <div class="col-sm-9 col-xs-12 f-sm-20 w400 f-18 white-clr mt10 mt-sm60 xstext-center text-right">
                        <a href="mailto:support@bizomart.com">Contact</a>&nbsp;&nbsp;|&nbsp;
                        <a href="http://kyza.biz/legal/privacy-policy.html">Privacy</a>&nbsp;|&nbsp;
                        <a href="http://kyza.biz/legal/terms-of-service.html">T&amp;C</a>&nbsp;|&nbsp;
                        <a href="http://kyza.biz/legal/disclaimer.html">Disclaimer</a>&nbsp;|&nbsp;
                        <a href="http://kyza.biz/legal/gdpr.html">GDPR</a>&nbsp;|&nbsp;
                        <a href="http://kyza.biz/legal/dmca.html">DMCA</a>&nbsp;|&nbsp;
                        <a href="http://kyza.biz/legal/anti-spam.html">Anti-Spam</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<!-- Footer Section End -->


	<!-- Exit Pop-Box Start -->
	<!-- Load UC Bounce Modal CDN Start -->
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/ouibounce/0.0.11/ouibounce.min.js"></script>

	<!-- <div id="ouibounce-modal" style="z-index:77777; display:none;">
	    <div class="underlay"></div>
	    	<div class="modal popupbg" style="display:block; ">
	        <div class="modal-header" style="border:0; padding:0;">
	            <button type="button" class="close" onclick="document.getElementById('ouibounce-modal').style.display = 'none';">&times;</button>
	       	</div>

	        <div class="modal-body">
	            <div class="innerbody">
	                <div class="col-lg-12 col-md-12 col-sm-12 col-sm-offset-0 col-xs-12 col-xs-offset-0 mt2 xsmt2">
	                    <img src="assets/images/waitimg.png" class="img-responsive center-block mt2 xsmt2">
	                    <h2 class="text-center mt2 w500 sm25 xs20 lh140">I HAVE SOMETHING SPECIAL<br /> FOR YOU</h2>
	                    <a href="<?php echo $_GET['afflink']; ?>" class="createfree md20 sm18 xs16" style="color:#ffffff;">Stay</a>
	                </div>
	            </div>
	        </div>
	    </div>
	</div> -->

	<div class="modal fade in" id="ouibounce-modal" role="dialog" style="display:none; overflow-y:auto;">
		<div class="modal-dialog modal-dg">
			<button type="button" class="close" onclick="document.getElementById('ouibounce-modal').style.display = 'none';">&times;</button>	 
			<div class="col-xs-12 modal-content pop-bg pop-padding">			
				<div class="col-xs-12 modal-body text-center border-pop px0">	
					<div class="col-xs-12 px0">
						<img src="assets/images/waitimg.png" class="img-responsive center-block">
					</div>
					
					<div class="col-xs-12 text-center mt20 mt-sm25 px0">
						<div class="f-20 f-sm-28 lh140 w600">I HAVE SOMETHING SPECIAL <br class="hidden-xs"> FOR YOU</div>
					</div>

					<div class="col-xs-12 mt20 mt-sm20">
						<div class="link-btn">
							<a href="<?php echo $_GET['afflink'];?>">STAY</a>
						</div>
					</div>
				</div>		
			</div>
		</div>
	</div>

	<!-- <script type="text/javascript">
        var _ouibounce = ouibounce(document.getElementById('ouibounce-modal'),{
        aggressive: true, //Making this true makes ouibounce not to obey "once per visitor" rule
        });
    </script> -->
	<!-- Load UC Bounce Modal CDN End -->
	<!-- Exit Pop-Box End -->


	<!-- Google Tag Manager (noscript) -->

<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K95FCV8"

height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

<!-- End Google Tag Manager (noscript) -->
  <!-- Facebook Pixel Code -->
  <script>
	 ! function(f, b, e, v, n, t, s) {
	  if (f.fbq) return;
	  n = f.fbq = function() {
		 n.callMethod ?
			 n.callMethod.apply(n, arguments) : n.queue.push(arguments)
	  };
	  if (!f._fbq) f._fbq = n;
	  n.push = n;
	  n.loaded = !0;
	  n.version = '2.0';
	  n.queue = [];
	  t = b.createElement(e);
	  t.async = !0;
	  t.src = v;
	  s = b.getElementsByTagName(e)[0];
	  s.parentNode.insertBefore(t, s)
	 }(window,
	  document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');
	 fbq('init', '1777584378755780');
	 fbq('track', 'PageView');
  </script>
  <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=1777584378755780&ev=PageView&noscript=1" /></noscript>
  <!-- DO NOT MODIFY -->
  <!-- End Facebook Pixel Code -->
  <!-- Google Code for Remarketing Tag -->
  <!-------------------------------------------------- Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup --------------------------------------------------->
  <div style="display:none;">
	 <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 748114601;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		var google_user_id = '<unique user id>';
		/* ]]> */
	 </script>
	 <script type="text/javascript" src="https://www.googleadservices.com/pagead/conversion.js"></script>
	 <noscript>
		<div style="display:inline;">
		   <img height="1" width="1" style="border-style:none;" alt="" src="https://googleads.g.doubleclick.net/pagead/viewthroughconversion/748114601/?guid=ON&amp;script=0&amp;userId=<unique user id>" />
		</div>
	 </noscript>
  </div>
  <!-- Google Code for Remarketing Tag -->
  <!-- timer --->
  <?php
	 if ($now < $exp_date) {
	 
	 ?>
  <script type="text/javascript">
	 // Count down milliseconds = server_end - server_now = client_end - client_now
	 var server_end = <?php echo $exp_date; ?> * 1000;
	 var server_now = <?php echo time(); ?> * 1000;
	 var client_now = new Date().getTime();
	 var end = server_end - server_now + client_now; // this is the real end time
	 
	 var noob = $('.countdown').length;
	 
	 var _second = 1000;
	 var _minute = _second * 60;
	 var _hour = _minute * 60;
	 var _day = _hour * 24
	 var timer;
	 
	 function showRemaining() {
	  var now = new Date();
	  var distance = end - now;
	  if (distance < 0) {
		 clearInterval(timer);
		 document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
		 return;
	  }
	 
	  var days = Math.floor(distance / _day);
	  var hours = Math.floor((distance % _day) / _hour);
	  var minutes = Math.floor((distance % _hour) / _minute);
	  var seconds = Math.floor((distance % _minute) / _second);
	  if (days < 10) {
		 days = "0" + days;
	  }
	  if (hours < 10) {
		 hours = "0" + hours;
	  }
	  if (minutes < 10) {
		 minutes = "0" + minutes;
	  }
	  if (seconds < 10) {
		 seconds = "0" + seconds;
	  }
	  var i;
	  var countdown = document.getElementsByClassName('countdown');
	  for (i = 0; i < noob; i++) {
		 countdown[i].innerHTML = '';
	 
		 if (days) {
			 countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-sm-60 timerbg oswald">' + days + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-sm-18 smmltd">Days</span> </div>';
		 }
	 
		 countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-sm-60 timerbg oswald">' + hours + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-sm-18 w500 smmltd">Hours</span> </div>';
	 
		 countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"><span class="f-31 f-sm-60 timerbg oswald">' + minutes + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-sm-18 w500 smmltd">Mins</span> </div>';
	 
		 countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-31 f-sm-60 timerbg oswald">' + seconds + '</span><br><span class="f-14 f-sm-18 w500 smmltd">Sec</span> </div>';
	  }
	 
	 }
	 timer = setInterval(showRemaining, 1000);
  </script>
  <?php
	 } else {
	 echo "Times Up";
	 }
	 ?>
  <!--- timer end-->
  
  <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TKWBVSR"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
</body>
</html>

<!Doctype html>
<html>

<head>
    <title>Prelaunch Special Webinar | Kyza</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=9">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    
    <meta name="title" content="Kyza | Prelaunch">
	<meta name="description" content="Kyza">
	<meta name="keywords" content="Kyza">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta property="og:image" content="https://www.kyza.io/prelaunch/thumbnail.png">
	<meta name="language" content="English">
	<meta name="revisit-after" content="1 days">
	<meta name="author" content="Dr. Amit Pareek">

    <!-- Open Graph / Facebook -->
	<meta property="og:type" content="website">
	<meta property="og:title" content="Kyza | Prelaunch">
	<meta property="og:description" content="Kyza">
	<meta property="og:image" content="https://www.kyza.io/prelaunch/thumbnail.png">

	<!-- Twitter -->
	<meta property="twitter:card" content="summary_large_image">
	<meta property="twitter:title" content="Kyza | Prelaunch">
	<meta property="twitter:description" content="Kyza">
	<meta property="twitter:image" content="https://www.kyza.io/prelaunch/thumbnail.png">

    <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Barlow:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&display=swap" rel="stylesheet">

    <!-- Start Editor required -->
    <link rel="stylesheet" href="../common_assets/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="../common_assets/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="../common_assets/css/common-add-element-v1.css" type="text/css">
    <link rel="stylesheet" href="../common_assets/css/general-v1.css">
    <script src="../common_assets/js/jquery.min.js"></script>
    <script src="../common_assets/js/bootstrap.min.js"></script>
    <script src="../common_assets/js/common.min.js"></script>
    <script src="../common_assets/js/moment/jquery.countdown.js"></script>
    <script src="../common_assets/js/moment/jquery.moment.js"></script>
    <script src="../common_assets/js/moment/jquery.moment.data.js"></script>
    <link rel="stylesheet" href="assets/css/style.css" type="text/css">
    <link rel="stylesheet" href="assets/css/aweberform.css" type="text/css" >
	<link rel="stylesheet" href="assets/css/timer.css" type="text/css">
    <!-- End -->
<script>
function getUrlVars()
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}
var first = getUrlVars()["aid"];
//alert(first);
document.addEventListener('DOMContentLoaded', (event) => {
document.getElementById('awf_field_aid').setAttribute('value',first);
})
//document.getElementById('myField').value = first;
</script>

<script>
	(function(w, i, d, g, e, t, s) {
	if(window.businessDomain != undefined){
	console.log("Your page have duplicate embed code. Please check it.");
	return false;
	}
	businessDomain = 'kyza';
	allowedDomain = 'www.kyza.io';
	if(!window.location.hostname.includes(allowedDomain)){
	console.log("Your page have not authorized. Please check it.");
	return false;
	}
	console.log("Your script is ready...");
	w[d] = w[d] || [];
	t = i.createElement(g);
	t.async = 1;
	t.src = e;
	s = i.getElementsByTagName(g)[0];
	s.parentNode.insertBefore(t, s);
	})(window, document, '_gscq', 'script', 'https://cdn.staticdcp.com/apps/engage/smart_engage/js/config-loader.js');
	</script>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-P9F8PNM');</script>
<!-- End Google Tag Manager -->

</head>

<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P9F8PNM"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- New Timer  Start-->
      <?php
         $date = 'February 26 2022 09:00 AM EST';
         $exp_date = strtotime($date);
         $now = time();  
         /*
         
         $date = date('F d Y g:i:s A eO');
         $rand_time_add = rand(700, 1200);
         $exp_date = strtotime($date) + $rand_time_add;
         $now = time();*/
         
         if ($now < $exp_date) {
         ?>
      <?php
         } else {
          echo "Times Up";
         }
         ?>
      <!-- New Timer End -->

<div id="templateBody" class="dcp-editor-v1">
	<div id="space-parent">
		<!-- Header Section Start -->
		<div class="space-section banner-section" editabletype="space">
			<div class="container">
				<div class="row inner-content">
					<div class="col-xs-12 p0">

                            <div editabletype="image" style="z-index: 10;">
                                <img src="assets/images/logo.png" class="img-responsive center-block">
                            </div>
							<!-- <div class="col-xs-12 p0 f-sm-24 f-20 w600 text-center  post-heading mt20 mt-sm50 lh140 lh140">
                                Register for your Coursova launch special FREE webinar & get assured gift + 10 free licenses
                            </div> -->
                            <div class="col-xs-12 mt20 mt-sm50 text-center px-sm15 px0">
                                <div class="p0 f-20 f-sm-24 w600 text-center post-heading lh140">
                                    Register for Kyza Pre-Launch Webinar & Get an Assured Gift + 10 Free Licenses
                                </div>
                             </div>
						<!-- <div class="mt-sm20 mt15 col-xs-12 p0">
							<img src="assets/images/head-sep.png" class="mx-auto img-responsive" alt="seperator"/>
						</div> -->
                        <div class="col-xs-12 px-sm15 px0 mt-sm15 mt15">
                            <div class="text-center">
								<div class="f-sm-45 f-28 w700 text-center black-clr lh140 line-center">
									Save Time, Frustration, and 100s of Dollars Monthly Firing 5+ Expensive Marketing Apps and Ensure Your Business Growth in 2022
								</div>
                            </div>
                        </div>
                        <div class="col-xs-12 mt20 mb15 mb-sm10 px0 text-center">
                            <div class="p0 f-18 f-sm-20 w700 text-center black-clr lh140">
                                Discover how we have sold over $9 Mn in products & earned $6Mn in affiliate commission by doing it all from<br class="visible-lg"> one single dashboard and without any tech hassles.
                            </div>
                        </div>

                        </div>
					<div class="col-xs-12 p0">
						<div class="col-xs-12 col-sm-7 px0 px-sm15 mt20 mt-sm70">
							<!-- <div class="responsive-video" editabletype="video" style="z-index: 10;">
								<iframe src="https://Coursova.dotcompal.com/video/embed/9bo529lat3" style="width: 100%;height: 100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                            </div> -->
							<div class="" editabletype="image" style="z-index: 10;">
                                <img src="assets/images/video-bg.jpg" class="img-responsive center-block">
                            </div>
						</div>
						<div class="col-xs-12 col-sm-5 px0 px-sm15 mt20 mt-sm40">
							<div class="col-xs-12 register-form-back" editabletype="shape" style="z-index:9">
								<div class="col-xs-12 p0">
									<div class="f-sm-32 f-24 w700 lh140 text-center black-clr spartan-font" editabletype="text" style="z-index: 10;">
										Register for Free Training
									</div>
									<div class="f-sm-19 f-20 text-center w400 lh150 mb15 mt8" editabletype="text" style="z-index: 10;">
                                      <span class="w700">On 26<sup>th</sup> February, 9 AM EST.</span> <br>
									  Book Your Seat now (Limited to 100 Users)
                                    </div>
								</div>
								<!-- Form Code -->
								<div class="col-xs-12 p0" editabletype="form" style="z-index:10">

							<form method="post" class="af-form-wrapper register-form-style1 " accept-charset="UTF-8" action="https://www.aweber.com/scripts/addlead.pl" style="z-index: 10;">
                              <div style="display: none;">
                                 <input type="hidden" name="meta_web_form_id" value="2147261403" />
                                 <input type="hidden" name="meta_split_id" value="" />
                                 <input type="hidden" name="listname" value="awlist6219766" />
                                 <input type="hidden" name="redirect" value="https://www.kyza.io/prelaunch-thankyou" id="redirect_8f9857581821edf5088a60423b98cb04" />
                                 <input type="hidden" name="meta_adtracking" value="kyza_Prelaunch" />
                                 <input type="hidden" name="meta_message" value="1" />
                                 <input type="hidden" name="meta_required" value="name,email" />
                                 <input type="hidden" name="meta_tooltip" value="" />
                              </div>
                              <div id="af-form-2147261403" class="af-form">
                                 <div id="af-body-2147261403" class="af-body af-standards">
                                    <div class="af-element">
                                       <label class="previewLabel" for="awf_field-113603361"></label>
                                       <div class="af-textWrap ">
                                          <div class="input-type" style="z-index: 11;">
                                             <!--<span class="input-group-addon border1px"><i class="fa fa-user" aria-hidden="true"></i></span>-->
                                             <input id="awf_field-113603361" type="text" name="name" class="text form-control custom-input input-field" value=""  onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="500"  placeholder="Your Name"/>
                                          </div>
                                       </div>
                                       <div class="af-clear bottom-margin"></div>
                                    </div>
                                    <div class="af-element">
                                       <label class="previewLabel" for="awf_field-113603362"> </label>
                                       <div class="af-textWrap">
                                          <div class="input-type" style="z-index: 11;">
                                             <!--<span class="input-group-addon border1px"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>-->
                                             <input class="text form-control custom-input input-field" id="awf_field-113603362" type="text" name="email" value="" tabindex="501" onFocus=" if (this.value == '') { this.value = ''; }" onBlur="if (this.value == '') { this.value='';} " placeholder="Your Email" />
                                          </div>
                                       </div>
                                       <div class="af-clear bottom-margin"></div>
                                    </div>
									<input type="hidden" id="awf_field_aid" class="text" name="custom aid" value=""  onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="502" />
                                   
									 <div class="af-element" style="padding:0px;">
                                       <label class="previewLabel" for="awf_field-102446990" style="display: none;">wplus</label>
                                       <div class="af-textWrap pt5"><input type="hidden" id="awf_field-102446990" class="text" name="custom wplus" value="&lt;?php echo $_GET['platform']; ?&gt;" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="503" autocomplete="off"></div>
                                       <div class="af-clear"></div>
                                    </div>
									
								   <div class="af-element buttonContainer mt0 xsmt3 button-type register-btn f-22 f-sm-26" style="padding:0;">
                                       <input name="submit" id="af-submit-image-348552691" type="submit" class="image" style="max-width: 100%;" alt="Submit Form" tabindex="503" value="Book Your Free Seat" />
                                       <div class="af-clear bottom-margin"></div>
                                    </div>
                                 </div>
                              </div>
                              <div style="display: none;"><img src="https://forms.aweber.com/form/displays.htm?id=TIws7ExsjCwMzA==" alt="" /></div>
                           </form>									
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Header Section End -->

		<!-- Prize Section Start -->
		<div class="space-section prize-section" editabletype="space">
			<div class="container">
				<div class="row inner-content">
					<div class="col-xs-12 col-sm-4 col-sm-offset-4 p0 mb-sm50 mb20 timer-block text-center">
						<div class="col-sm-12 col-xs-12 f-sm-22 f-20 w600 lh140 px0 px-sm15">
							Hurry Up! Free Offer Going Away in… 
						</div>
						<div class="col-xs-12 col-sm-12 col-sm-offset-0 p0 mt-sm20 mt20">
							<div class="countdown counter-black">
								<div class="timer-label text-center"><span class="f-40 f-sm-40 timerbg timerbg">01</span><br><span class="f-16 w500">Days</span> </div>
								<div class="timer-label text-center"><span class="f-40 f-sm-40 timerbg timerbg">16</span><br><span class="f-16 w500">Hours</span> </div>
								<div class="timer-label text-center"><span class="f-40 f-sm-40 timerbg timerbg">59</span><br><span class="f-16 w500">Mins</span> </div>
								<div class="timer-label text-center "><span class="f-40 f-sm-40 timerbg timerbg">37</span><br><span class="f-14 f-sm-18 w500 f-16 w500">Sec</span> </div>
							</div>
						</div>
					</div>
					<div class="col-xs-12 px0 px-sm15">
						<div class="f-sm-45 f-28 lh150 w700 black-clr text-center" editabletype="text" style="z-index: 10;">
							Register for this FREE Training Webinar and Get a<br class="visible-lg"> Chance to WIN Free Kyza Access
						</div>
					</div>
					<div class="col-xs-12 col-sm-8 col-sm-offset-2 px0 pl-sm15 ">
						<div class="f-sm-22 f-20 w600 lh150 mt10 mt-sm15" editabletype="text" style="z-index: 10;">
							Our 25+ Years of Combined Experience is packed in this State-of-Art 1 Hour Webinar where You’ll learn:
						</div>
						<div class="f-sm-22 f-20 w400 lh150 p0 mt20 mt-sm40" editabletype="text" style="z-index: 10;">
							<ul class="list-style lh150">
                                <li>How we have sold over $9Mn in digital products online and you can follow the same to build a profitable business online.</li>
                                <li>How we earned $6Mn in Affiliate Commission without any dependency on expensive third-party apps and Saved 100s of Dollar Monthly.</li>
                                <li>How you can get rid of the Headache of Managing Multiple Apps & Dashboards to run your marketing campaigns. </li>
                                <li>A Sneak-Peak of Kyza to Create Lightning-Fast Marketing Pages, Pop-Ups, Optin-Forms, and Send Unlimited Emails Without Any Technical Hassles – Under a Single Dashboard.</li>
                                <li>Lastly, everyone who attends this session will get a chance to Win a Free Gift as we are giving 10 licenses of our Premium solution – Kyza
                                </li>
							</ul>
						</div>
					</div>
					<div class="col-xs-12 col-sm-10 col-sm-offset-1 mt25 mt-sm85 p0" id="form">
						<div class="auto_responder_form inp-phold formbg col-xs-12 p0" editabletype="shape" style="z-index:9">
							<div class="f-20 f-sm-24 w500 text-center lh180 black-clr" editabletype="text" style="z-index:10;">
								Register For This Free Training Webinar & Subscribe to Our EarlyBird VIP List Now! <br>
								<span class="f-22 f-sm-32 w700 text-center black-clr" contenteditable="false">26<sup contenteditable="false">th</sup> February, 9 am Est.(100 Seats Only)</span>
							</div>
							<div class="col-xs-12 col-sm-6 col-sm-offset-3 p0 my20 timer-block text-center">
								<div class="col-sm-12 col-xs-12 f-sm-22 f-20 w600 lh140 px0 px-sm15">
									Hurry Up! Free Offer Going Away in… 
								</div>
								<div class="col-xs-12 col-sm-10 col-sm-offset-0 p0 mt-sm20 mt20">
									<div class="countdown counter-black">
										<div class="timer-label text-center"><span class="f-40 f-sm-40 timerbg timerbg">01</span><br><span class="f-16 w500">Days</span> </div>
										<div class="timer-label text-center"><span class="f-40 f-sm-40 timerbg timerbg">16</span><br><span class="f-16 w500">Hours</span> </div>
										<div class="timer-label text-center"><span class="f-40 f-sm-40 timerbg timerbg">59</span><br><span class="f-16 w500">Mins</span> </div>
										<div class="timer-label text-center "><span class="f-40 f-sm-40 timerbg timerbg">37</span><br><span class="f-14 f-sm-18 w500 f-16 w500">Sec</span> </div>
									</div>
								</div>
							</div>
							<!-- Aweber Form Code -->
							<div class="col-xs-12 p0 mt15 mt-sm25" editabletype="form" style="z-index:10">
								
								<!-- Aweber Form Code -->
								<form method="post" class="af-form-wrapper register-form-style1 " accept-charset="UTF-8" action="https://www.aweber.com/scripts/addlead.pl" style="z-index: 10;">
                              <div style="display: none;">
                                 <input type="hidden" name="meta_web_form_id" value="2147261403" />
                                 <input type="hidden" name="meta_split_id" value="" />
                                 <input type="hidden" name="listname" value="awlist6219766" />
                                 <input type="hidden" name="redirect" value="https://www.kyza.io/prelaunch-thankyou" id="redirect_8f9857581821edf5088a60423b98cb04" />
                                 <input type="hidden" name="meta_adtracking" value="kyza_Prelaunch" />
                                 <input type="hidden" name="meta_message" value="1" />
                                 <input type="hidden" name="meta_required" value="name,email" />
                                 <input type="hidden" name="meta_tooltip" value="" />
                              </div>
                              <div id="af-form-2147261403" class="af-form">
                                 <div id="af-body-2147261403" class="af-body af-standards">
                                    <div class="af-element width-form">
                                       <label class="previewLabel" for="awf_field-113603361"></label>
                                       <div class="af-textWrap">
                                          <div class="input-type" style="z-index: 11;">
                                             <!--<span class="input-group-addon border1px"><i class="fa fa-user" aria-hidden="true"></i></span>-->
                                             <input id="awf_field-113603361" type="text" name="name" class="text form-control custom-input input-field" value=""  onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="500"  placeholder="Your Name"/>
                                          </div>
                                       </div>
                                       <div class="af-clear bottom-margin"></div>
                                    </div>
                                    <div class="af-element width-form">
                                       <label class="previewLabel" for="awf_field-113603362"> </label>
                                       <div class="af-textWrap">
                                          <div class="input-type" style="z-index: 11;">
                                             <!--<span class="input-group-addon border1px"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>-->
                                             <input class="text form-control custom-input input-field" id="awf_field-113603362" type="text" name="email" value="" tabindex="501" onFocus=" if (this.value == '') { this.value = ''; }" onBlur="if (this.value == '') { this.value='';} " placeholder="Your Email" />
                                          </div>
                                       </div>
                                       <div class="af-clear bottom-margin"></div>
                                    </div>
									<input type="hidden" id="awf_field_aid" class="text" name="custom aid" value=""  onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="502" />
                                   
									 <!--<div class="af-element">
                                       <label class="previewLabel" for="awf_field-102446990" style="display: none;">wplus</label>
                                       <div class="af-textWrap pt5"><input type="hidden" id="awf_field-102446990" class="text" name="custom wplus" value="&lt;?php echo $_GET['platform']; ?&gt;" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="503" autocomplete="off"></div>
                                       <div class="af-clear"></div>
                                    </div>-->
									
								   <div class="af-element buttonContainer mt0 xsmt3 button-type register-btn1 f-22 f-sm-21 width-form mt10" style="padding-top:5px;">
                                       <input name="submit" id="af-submit-image-348552691" type="submit" class="image" style="max-width: 100%;" alt="Submit Form" tabindex="503" value="Book Your Free Seat" />
                                       <div class="af-clear bottom-margin"></div>
                                    </div>
                                 </div>
                              </div>
                             <div style="display: none;"><img src="https://forms.aweber.com/form/displays.htm?id=TIws7ExsjCwMzA==" alt="" /></div>
                           </form>		
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Prize Section End -->	
		 <!--Footer Section Start -->
            <div class="space-section footer-section">
                <div class="container px-sm15 px0">
                    <div class="row inner-content">
                        <div class="col-xs-12 text-center">
                            <div> <img src="assets/images/footer-logo.png" class="img-responsive center-block"> </div>
                            <div editabletype="text" class="f-18 f-sm-18 w400 mt15 mt-sm35 lh140 white-clr text-center" style="z-index: 10;">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
                        </div>
                        <div class="col-sm-3 col-xs-12">
                            <div editabletype="text" class="f-18 f-sm-20 w400 mt10 mt-sm58 lh140 white-clr text-xs-center" style="z-index: 10;">Copyright © Kyza</div>
                        </div>
                        <div class="col-sm-9 col-xs-12 mt10 mt-sm60 xstext-center text-right">
                            <ul class="footer-ul w400 f-18 f-sm-20 white-clr">
                                <li><a href="mailto:support@bizomart.com" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                                <li><a href="http://kyza.biz/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                                <li><a href="http://kyza.biz/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                                <li><a href="http://kyza.biz/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                                <li><a href="http://kyza.biz/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                                <li><a href="http://kyza.biz/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                                <li><a href="http://kyza.biz/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!--Footer Section End -->
	</div>
</div>

<script type="text/javascript">
// Special handling for in-app browsers that don't always support new windows
(function() {
    function browserSupportsNewWindows(userAgent) {
        var rules = [
            'FBIOS',
            'Twitter for iPhone',
            'WebView',
            '(iPhone|iPod|iPad)(?!.*Safari\/)',
            'Android.*(wv|\.0\.0\.0)'
        ];
        var pattern = new RegExp('(' + rules.join('|') + ')', 'ig');
        return !pattern.test(userAgent);
    }

    if (!browserSupportsNewWindows(navigator.userAgent || navigator.vendor || window.opera)) {
        document.getElementById('af-form-2147261403').parentElement.removeAttribute('target');
    }
})();
</script><script type="text/javascript">
    <!--
    (function() {
        var IE = /*@cc_on!@*/false;
        if (!IE) { return; }
        if (document.compatMode && document.compatMode == 'BackCompat') {
            if (document.getElementById("af-form-2147261403")) {
                document.getElementById("af-form-2147261403").className = 'af-form af-quirksMode';
            }
            if (document.getElementById("af-body-2147261403")) {
                document.getElementById("af-body-2147261403").className = "af-body inline af-quirksMode";
            }
            if (document.getElementById("af-header-2147261403")) {
                document.getElementById("af-header-2147261403").className = "af-header af-quirksMode";
            }
            if (document.getElementById("af-footer-2147261403")) {
                document.getElementById("af-footer-2147261403").className = "af-footer af-quirksMode";
            }
        }
    })();
    -->
</script>

<!-- /AWeber Web Form Generator 3.0.1 -->

<!-- timer --->
      <?php
         if ($now < $exp_date) {
         
         ?>
      <script type="text/javascript">
         // Count down milliseconds = server_end - server_now = client_end - client_now
         var server_end = <?php echo $exp_date; ?> * 1000;
         var server_now = <?php echo time(); ?> * 1000;
         var client_now = new Date().getTime();
         var end = server_end - server_now + client_now; // this is the real end time
         
         var noob = $('.countdown').length;
         
         var _second = 1000;
         var _minute = _second * 60;
         var _hour = _minute * 60;
         var _day = _hour * 24
         var timer;
         
         function showRemaining() {
          var now = new Date();
          var distance = end - now;
          if (distance < 0) {
          clearInterval(timer);
          document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
          return;
          }
         
          var days = Math.floor(distance / _day);
          var hours = Math.floor((distance % _day) / _hour);
          var minutes = Math.floor((distance % _hour) / _minute);
          var seconds = Math.floor((distance % _minute) / _second);
          if (days < 10) {
          days = "0" + days;
          }
          if (hours < 10) {
          hours = "0" + hours;
          }
          if (minutes < 10) {
          minutes = "0" + minutes;
          }
          if (seconds < 10) {
          seconds = "0" + seconds;
          }
          var i;
          var countdown = document.getElementsByClassName('countdown');
          for (i = 0; i < noob; i++) {
          countdown[i].innerHTML = '';
         
          if (days) {
         	 countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-40 f-sm-40 timerbg">' + days + '</span><br><span class="f-16 w500">Days</span> </div>';
          }
         
          countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-40 f-sm-40 timerbg">' + hours + '</span><br><span class="f-16 w500">Hours</span> </div>';
         
          countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-40 f-sm-40 timerbg">' + minutes + '</span><br><span class="f-16 w500">Mins</span> </div>';
         
          countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-40 f-sm-40 timerbg">' + seconds + '</span><br><span class="f-16 w500">Sec</span> </div>';
          }
         
         }
         timer = setInterval(showRemaining, 1000);
      </script>
      <?php
         } else {
         echo "Times Up";
         }
         ?>
      <!--- timer end-->
</body>

</html>
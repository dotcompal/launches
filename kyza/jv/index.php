<html>
   <head>
      <title>JV Page - Kyza JV Invite</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=9">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
	  
	<meta name="title" content="Kyza | JV">
	<meta name="description" content="Kyza">
	<meta name="keywords" content="Kyza">

	<meta property="og:image" content="https://www.kyza.io/jv/thumbnail.png">
	<meta name="language" content="English">
	<meta name="revisit-after" content="1 days">
	<meta name="author" content="Dr. Amit Pareek">

	<!-- Open Graph / Facebook -->
	<meta property="og:type" content="website">
	<meta property="og:title" content="Kyza | JV">
	<meta property="og:description" content="Kyza">
	<meta property="og:image" content="https://www.kyza.io/jv/thumbnail.png">

	<!-- Twitter -->
	<meta property="twitter:card" content="summary_large_image">
	<meta property="twitter:title" content="Kyza | JV">
	<meta property="twitter:description" content="Kyza">
	<meta property="twitter:image" content="https://www.kyza.io/jv/thumbnail.png">
	
	  
      <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Barlow:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
      <!-- Start Editor required -->
      <link rel="stylesheet" href="../common_assets/css/font-awesome.min.css" type="text/css">
      <link rel="stylesheet" href="../common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" href="../common_assets/css/common-add-element-v1.css" type="text/css">
      <link rel="stylesheet" href="../common_assets/css/general-v1.css">
      <script src="../common_assets/js/jquery.min.js"></script>
      <script src="../common_assets/js/bootstrap.min.js"></script>
      <script src="../common_assets/js/common.min.js"></script>
      <script src="../common_assets/js/moment/jquery.countdown.js"></script>
      <script src="../common_assets/js/moment/jquery.moment.js"></script>
      <script src="../common_assets/js/moment/jquery.moment.data.js"></script>
      <link rel="stylesheet" href="assets/css/style.css" type="text/css">
      <link rel="stylesheet" href="assets/css/m-style.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style-bottom.css" type="text/css">
      <link rel="stylesheet" href="assets/css/timer.css" type="text/css">
      <!-- End -->
      <script type="text/javascript">
         // Special handling for in-app browsers that don't always support new windows
         (function() {
             function browserSupportsNewWindows(userAgent) {
                 var rules = [
                     'FBIOS',
                     'Twitter for iPhone',
                     'WebView',
                     '(iPhone|iPod|iPad)(?!.*Safari\/)',
                     'Android.*(wv|\.0\.0\.0)'
                 ];
                 var pattern = new RegExp('(' + rules.join('|') + ')', 'ig');
                 return !pattern.test(userAgent);
             }
         
             if (!browserSupportsNewWindows(navigator.userAgent || navigator.vendor || window.opera)) {
                 document.getElementById('af-form-1059432944').parentElement.removeAttribute('target');
             }
         })();
      </script><script type="text/javascript">
         <!--
         (function() {
             var IE = /*@cc_on!@*/false;
             if (!IE) { return; }
             if (document.compatMode && document.compatMode == 'BackCompat') {
                 if (document.getElementById("af-form-1059432944")) {
                     document.getElementById("af-form-1059432944").className = 'af-form af-quirksMode';
                 }
                 if (document.getElementById("af-body-1059432944")) {
                     document.getElementById("af-body-1059432944").className = "af-body inline af-quirksMode";
                 }
                 if (document.getElementById("af-header-1059432944")) {
                     document.getElementById("af-header-1059432944").className = "af-header af-quirksMode";
                 }
                 if (document.getElementById("af-footer-1059432944")) {
                     document.getElementById("af-footer-1059432944").className = "af-footer af-quirksMode";
                 }
             }
         })();
         -->
      </script>
      <!-- /AWeber Web Form Generator 3.0.1 -->
	  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-P9F8PNM');</script>
<!-- End Google Tag Manager -->

   </head>
   <body>
   <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P9F8PNM"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

      <!-- New Timer  Start-->
      <?php
         $date = 'February 26 2022 10:00 AM EST';
         $exp_date = strtotime($date);
         $now = time();  
         /*
         
         $date = date('F d Y g:i:s A eO');
         $rand_time_add = rand(700, 1200);
         $exp_date = strtotime($date) + $rand_time_add;
         $now = time();*/
         
         if ($now < $exp_date) {
         ?>
      <?php
         } else {
          echo "Times Up";
         }
         ?>
      <!-- New Timer End -->
      <div id="templateBody">
         <div id="space-parent">
            <!-- Header Section Start -->
            <div class="space-section header-section">
               <div class="container">
                  <div class="row inner-content">
                     <div class="col-sm-3"> 
                        <img src="assets/images/logo.png" class="img-responsive mx-xs-auto logo-height"> 
                     </div>
                     <div class="col-sm-6 mt15 mt-sm12">
                        <ul class="leader-ul f-sm-18 f-16 w500 black-clr xstext-center">
                           <li>
                              <a href="https://docs.google.com/document/d/17jnREso0jXQ9TyNtC87XaOLoMGMeQmXPuq6dirf8lX4/edit?usp=sharing" target="_blank" class="black-clr t-decoration-none w500">JV Doc</a><span class="pl15">|</span>
                           </li>
                           <li>
                              <a href="#funnel" class="black-clr t-decoration-none w500">Launch Funnel</a>
                           </li>
                        </ul>
                     </div>
                     <div class="col-sm-3 mt15 mt-sm0">
                        <div class="affiliate-link-btn xstext-center">
                           <a href="https://www.jvzoo.com/affiliates/info/377791">Grab Your Affiliate Link</a>
                        </div>
                     </div>
                     <div class="col-xs-12 mt20 mt-sm50 text-center px-sm15 px0">
                        <div class="p0 f-20 f-sm-24 w600 text-center post-heading lh140">
                           Get Ready! To Make Up To $447/Sale and $12K in JV Prizes
                        </div>
                     </div>
                     <div class="col-xs-12 px-sm15 px0 mt-sm25 mt20">
                        <div class="text-center">
                           <div class="f-sm-45 f-28 w700 text-center black-clr lh140 line-center">
							   Promote This Revolutionary All-In-One Business Growth  Suite That Match <span class="blue-head">All Your Marketing Needs Under One Dashboard </span> with Zero Tech Hassle
                           </div>
                        </div>
                     </div>
                     <div class="col-xs-12 px-sm15 px0 mt-sm25 mt17 f-sm-25 f-17 w600 black-clr text-center">
					 Lightning-Fast Pages | Send Unlimited Emails | Boost Lead Generation | AI-Powered Optin Forms | <br class="visible-lg"> Event Based Smart Pop-Ups | Graphics Branding Kit | No Monthly Fee                  
                     </div>
                     <div class="col-xs-12 col-sm-12 px0 px-sm15 mt20 mt-sm50">
                        <div class="row">
                           <div class="col-sm-8 col-xs-12 min-sm-video-width-left">
							<div class="col-xs-12 responsive-video p0">
								<iframe src="https://kyza.dotcompal.com/video/embed/kxcu6m1l9g" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
								box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
							</div>
                           </div>
                           <div class="col-sm-4 col-xs-12 min-sm-video-width-right mt20 mt-sm20">
                              <img src="assets/images/date.png" class="img-responsive center-block">
                              <div class="countdown counter-black mt20 mt-sm30">
                                 <div class="timer-label text-center"><span class="f-40 f-sm-40 timerbg timerbg">01</span><br><span class="f-16 w500">Days</span> </div>
                                 <div class="timer-label text-center"><span class="f-40 f-sm-40 timerbg timerbg">16</span><br><span class="f-16 w500">Hours</span> </div>
                                 <div class="timer-label text-center"><span class="f-40 f-sm-40 timerbg timerbg">59</span><br><span class="f-16 w500">Mins</span> </div>
                                 <div class="timer-label text-center "><span class="f-40 f-sm-40 timerbg timerbg">37</span><br><span class="f-14 f-sm-18 w500 f-16 w500">Sec</span> </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-xs-12 px0 px-sm15 mt-sm50">
                        <div class="row">
                           <div class="col-xs-12 col-sm-6 header-list-back">
                              <div class="f-18 f-sm-18 lh140 w500">
                                 <ul class="header-bordered-list pl0 orange-list">
                                    <li>Make your Subscribers to Say Goodbye to Complicated <br class="visible-lg"> & Recurring Marketing Apps.</li>
                                    <li>Send Unlimited Emails Directly into Inbox for more <br class="visible-lg"> Opens & Clicks</li>
                                    <li>Included with Commercial License</span> to Server your <br class="visible-lg"> clients</li>
                                 </ul>
                              </div>
                           </div>
                           <div class="col-xs-12 col-sm-6 header-list-back">
                              <div class="f-18 f-sm-18 lh140 w500">
                                 <ul class="header-bordered-list pl0 orange-list">
                                    <li>Create Unlimited Sales Pages, Lead Pages, Forms and <br class="visible-lg"> Emails etc.</li>
                                    <li>Choose from Over 300+ Landing Page, Emails, Opt-in <br class="visible-lg"> Forms and Notification Templates.</li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Header Section End -->
            <div class="space-section live-section">
               <div class="container p0 px-sm15">
                  <div class="row inner-content">
                     <div class="col-xs-12 col-sm-12">
                        <div class="auto_responder_form inp-phold formbg col-xs-12">
                           <div class="f-24 f-sm-30 w600 text-center black-clr lh140" editabletype="text" style="z-index:10;">
                              <span class="blue-clr">Subscribe To Our JV List </span>and Be The First to Know Our Special Contest,<br class="visible-lg"> Events and Discounts
                           </div>
                           <!-- <div class="mt-sm20 mt15 col-xs-12 p0">
                              <img src="assets/images/sep-line.png" class="center-block img-responsive"/>
                              </div> -->
                           <!-- Aweber Form Code -->
                           <div class="col-xs-12 mt15 mt-sm25" editabletype="form" style="z-index:10">
                              <form method="post" class="af-form-wrapper" accept-charset="UTF-8" action="https://www.aweber.com/scripts/addlead.pl">
                                 <div style="display: none;">
                                    <input type="hidden" name="meta_web_form_id" value="1059432944" />
                                    <input type="hidden" name="meta_split_id" value="" />
                                    <input type="hidden" name="listname" value="awlist6214886" />
                                    <input type="hidden" name="redirect" value="https://www.aweber.com/thankyou.htm?m=default" id="redirect_f165afc649dddb1fee5e9485621f8b2f" />
                                    <input type="hidden" name="meta_adtracking" value="JV_Form" />
                                    <input type="hidden" name="meta_message" value="1" />
                                    <input type="hidden" name="meta_required" value="name,email" />
                                    <input type="hidden" name="meta_tooltip" value="" />
                                 </div>
                                 <div id="af-form-1059432944" class="af-form row">
                                    <div id="af-body-1059432944" class="af-body af-standards">
                                       <div class="af-element col-sm-6 px0 px-sm15">
                                          <label class="previewLabel" for="awf_field-113559364" style="display:none;"></label>
                                          <div class="af-textWrap mb15 mb-sm15 input-type">
                                             <input id="awf_field-113559364" type="text" name="name" placeholder="Your Name" class="text frm-ctr-popup form-control input-field" value="" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="500" />
                                          </div>
                                          <div class="af-clear"></div>
                                       </div>
                                       <div class="af-element mb15 mb-sm25 col-sm-6 px0 px-sm15">
                                          <label class="previewLabel" for="awf_field-113559365" style="display:none;"></label>
                                          <div class="af-textWrap input-type">
                                             <input class="text frm-ctr-popup form-control input-field" id="awf_field-113559365" type="text" name="email" placeholder="Your Email" value="" tabindex="501" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " />
                                          </div>
                                          <div class="af-clear"></div>
                                       </div>
                                       <div class="af-element buttonContainer  button-type popup-btn white-clr">
                                          <input name="submit" class="submit f-24 f-sm-30 white-clr center-block" type="submit" value="Subscribe for JV Updates" tabindex="502" />
                                          <div class="af-clear"></div>
                                       </div>
                                    </div>
                                    <div id="af-footer-1059432944" class="af-footer">
                                       <div class="bodyText">
                                          <p style="height:0px;">&nbsp;</p>
                                       </div>
                                    </div>
                                 </div>
                                 <div style="display: none;">
                                    <img src="https://forms.aweber.com/form/displays.htm?id=jAysnCzMTJwsLA==" alt="" />
                                 </div>
                              </form>
                              <!-- Aweber Form Code -->                      
                           </div>
                        </div>
                        <div class="col-xs-12 p0">
                           <img src="assets/images/form-shadow.png" class="center-block img-responsive"/>
                        </div>
                     </div>
                     <div class="col-xs-12 col-sm-12 px-sm50 mt-sm30">
                        <div class="f-24 f-sm-30 w600 text-center lh140">
                           <span class="orange-clr">Grab Your JVZoo Affiliate Link</span> to Jump on This Amazing Product Launch
                        </div>
                        <div class="col-xs-12 p0 mt-sm19 mt15">
                           <img src="assets/images/jvzoo.png" class="center-block img-responsive"/>
                        </div>
                        <div class="col-xs-12 affiliate-btn p0 mt-sm19 mt15">
                           <a href="https://www.jvzoo.com/affiliates/info/377791" class="f-24 f-sm-30 center-block">Request Affiliate Link</a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- launch-special -->
             <!-- exciting-launch -->
            <div class="space-section exciting-launch">
               <div class="container">
                  <div class="row inner-content">
                     <div class="col-xs-12 p0">
                        <div class="f-sm-45 f-28 black-clr  text-center w700 lh140">
                           This Exciting Launch Event Is Divided Into 2 Phases 
                        </div>
                     </div>
                     <div class="col-xs-12 p0">
                        <div class="row">
                           <div class="col-sm-6 col-xs-12 mt-sm80 mt20 pr-sm40">
                              <div class="launch-box1 col-xs-12 p0">
                                 <div class="f-sm-45 f-28 w700 black-clr lh140 text-center">
                                    Pre Launch 
                                 </div>
                                 <div class="f-sm-28 f-24 text-center w500">
                                    (With Webinar)
                                 </div>
                                 <div class="f-sm-32 f-20 mt-sm20 mt15 text-center w500 lh160 col-xs-12 p0">
                                    <span class="w700">23<sup>rd</sup> Feb'22</span>, 10:00 AM EST to <br>
                                    <span class="w700">26<sup>th</sup> Feb'22</span>, 9:00 AM EST
                                 </div>
                                 <div class="f-sm-24 f-20 mt-sm20 mt15 text-center  w500 col-xs-12 p0 lh140">
                                    To Make You Max Commissions
                                 </div>
                                 <div class="f-sm-18 f-18 p0 mt-sm25 mt20 w500 col-xs-12 p0 pl-sm25">
                                    <ul class="launch-tick1 lh140 pl0 pl-sm60">
                                       <li>All Leads Are Hardcoded</li>
                                       <li>Exciting $2000 Pre-Launch Contest</li>
                                       <li>We'll Re-Market Your Leads Heavily</li>
                                       <li>Pitch Bundle Offer on webinars.</li>
                                    </ul>
                                 </div>
                              </div>
                           </div>
                           <div class="col-sm-6 col-xs-12 mt20 mt-sm80 pl-sm40">
                              <div class="launch-box2 col-xs-12 p0" editableType="shape" style="z-index: 9;">
                                 <div class="f-sm-45 f-28 w700 orange-clr text-center">
                                    5-Days Launch Event 
                                 </div>
                                 <div class="f-sm-28 f-20 mt-sm20 mt15 text-center w500 lh140 col-xs-12 p0 white-clr">
                                    <span class="w700">Cart Opens 26<sup>th</sup> Feb</span> 10:00 AM EST
                                 </div>
                                 <div class="f-sm-28 f-20 text-center w500 lh140 col-xs-12 p0 white-clr mt8">
                                    <span class="w700">Cart Closes 2<sup>nd</sup> 
                                    March</span> 11:59 PM EST
                                 </div>
                                 <div class="f-sm-18 f-18 p0 mt-sm25 mt20 w500 col-xs-12 p0">
                                    <ul class="launch-tick1 lh140 pl0 pl-sm30 white-clr">
                                       <li>High in Demand Product with Top Conversion</li>
                                       <li>Deep Funnel to Make You Double Digit EPCs</li>
                                       <li>Earn upto $447/Sale</li>
                                       <li>Huge $10K JV Prizes</li>
                                       <li>We'll Re-Market Your Visitors Heavily</li>
                                    </ul>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- exciting-launch end -->
          <div class="space-section hello-awesome">
               <div class="container">
                  <div class="row inner-content">
                     <div class="col-sm-6 col-xs-12 px0">
                        <div class="f-sm-45 f-28 lh140 w700 black-clr heading-design text-center">
                           Hello Awesome JV's
                        </div>
                        <div class="f-sm-18 f-18 lh150 w400 mt-sm15 mt15 black-clr">
                           It's Dr Amit Pareek, CEO & Founder of 2 Start-ups (Funded by investors) along with my Partner  Simon Warner (Internet Marketer & JV Manager).
                           <br>
                           <br>
                           We have delivered 25+ 6-Figure Blockbuster Launches, Sold Software Products of Over $9 Million and paid over $4 Million in commission to our Affiliates.  
                           <br>
                           <br>
                           With the combined experience of 25+ years, we are coming back with another Top Notch and High in Demand product that will provide a real solution for all the Marketing needs of your subscribers under one dashboard. 
                           <br>
                           <br>
                           "Kyza" a Revolutionary 5-in-1 App that delivers solution for all your marketing needs from lead generation to selling your products under one dashboard. 
                           <br><br>
                           Check out the incredible features of the amazing creation that will blow away your mind. And we Guarantee that this offer will convert like Hot Cakes starting from 26th Feb'22 at 10:00 AM EST! Get Ready!! 
                        </div>
                       
                     </div>
                     <div class="col-sm-6 col-xs-12 px-sm15 px0">
                        <!-- <div class="mt20 mt-sm0" editabletype="image" style="z-index: 10;">
                           <img src="assets/images/awesome-side-image.png" class="img-responsive center-block">
                        </div> -->
                        <div class="col-sm-12">
                           <div class="mt20 mt-sm0" editabletype="image" style="z-index: 10;">
                              <img src="assets/images/amit-pareek.png" class="img-responsive center-block">
                           </div>
                        </div>
                        <!-- <div class="col-sm-6 col-xs-12">
                           <div class="mt20 mt-sm50" editabletype="image" style="z-index: 10;">
                              <img src="assets/images/achal-goswami.png" class="img-responsive center-block">
                           </div>
                        </div> -->
                        <div class="col-sm-12 col-xs-12 mx-auto">
                           <div class="mt20 mt-sm50" editabletype="image" style="z-index: 10;">
                              <img src="assets/images/simon-warner.png" class="img-responsive center-block">
                           </div>
                        </div>
                     </div>

                     <div class="col-xs-12">
                        <div class="f-sm-18 f-18 lh150 w700 mt-sm50 mt20 text-center" editabletype="text" style="z-index: 10;">
                           Also, here are some stats from our previous launches:
                        </div>
                     </div>
                     <div class="col-xs-12 col-sm-10 col-sm-offset-1 px0 px-sm15">
                        <div class="col-xs-12 col-sm-6 f-18 f-sm-18 lh140 w500 p0 mt-sm25 mt20">
                           <ul class="pl0 no-orange-list">
                              <li>Over 100 Pick of The Day Awards</li>
                              <li>Over $1.5Mn In Affiliate Sales for Partners</li>
                           </ul>
                        </div>
                        <div class="col-xs-12 col-sm-6 f-18 f-sm-18 lh140 w500 p0 mt-sm25 mt0">
                           <ul class="pl0 no-orange-list">
                              <li>Top 10 Affiliate & Seller (High Performance Leader)</li>
                              <li>Always in Top-10 of JVZoo Top Sellers</li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
           
		  

	   <!-- next-generation -->
            <div class="space-section next-gen-sec">
               <div class="container px0 px-sm15">
                  <div class="row inner-content">
                     <div class="col-xs-12 text-center">
                        <div class="prdly-pres f-sm-45 f-28 w700 white-clr lh140">
                           Presenting…
                        </div>
                     </div>
                     <div class="col-xs-12 f-sm-42 f-28 mt-sm30 mt20 w700 text-center  black-clr lh140">
						All-In-One Business Growth Suite That Builds Lightning Fast High-Converting Landing Pages & Websites, Unlocks Graphics Branding Kit, Creates AI-Powered Optin Forms, Event Based Smart Pop-Ups & A Powerful Email Marketing Solution To Send Unlimited Emails
                     </div>
					  <div class="col-xs-12 mt20 mt-sm50">
					  <div class="row d-flex flex-wrap" style="align-items:end;">
					  <div class="col-xs-12 col-sm-7">
						<div class="col-xs-12 p0">
							<img src="assets/images/product-Image.png" class="img-responsive center-block margin-bottom-15">
						</div>
                     </div>
					 <div class="col-sm-5 col-xs-12 mt20 mt-sm0">
					 <div class="proudly-list-bg">
                           <div class="d-flex">
                              <div class="mt5 mr10">
                                 <img src="assets/images/tick.png">
                              </div>
                                 <div class="f-20 f-sm-24 w500 black-clr lh140">
                                  Create Unlimited Engaging Pages, Emails and Sales Boosters
                                 </div>
                           </div>
                           <div class="d-flex mt25">
                              <div class="mt5 mr10">
                                 <img src="assets/images/tick.png">
                              </div>
                                 <div class="f-20 f-sm-24 w500 black-clr lh140">
                                Send Unlimited Targeted Emails, Drive Unlimited Traffic, Sell Unlimited Products 
                                 </div>
                           </div>
						     <div class="d-flex mt25">
                              <div class="mt5 mr10">
                                 <img src="assets/images/tick.png">
                              </div>
                                 <div class="f-20 f-sm-24 w500 black-clr lh140">
                                 Use A.I Based Sales Boosters To Triple Conversions & Profits
                                 </div>
                           </div>
						     <div class="d-flex mt25">
                              <div class="mt5 mr10">
                                 <img src="assets/images/tick.png">
                              </div>
                                 <div class="f-20 f-sm-24 w500 black-clr lh140">
                                Over 300 Landing Page, Emails, Optin Forms and Notification Templates
                                 </div>
                           </div>
						     <div class="d-flex mt25">
                              <div class="mt5 mr10">
                                 <img src="assets/images/tick.png">
                              </div>
                                 <div class="f-20 f-sm-24 w500 black-clr lh140">
                              Commercial License to Charge Your Clients and Customers one time or recurring
                                 </div>
                           </div>
                        </div>
                        </div>
                        </div>
                        </div>
                     
                  </div>
               </div>
			   <!-- Demo video link -->
			   <div class="demo-link-wrapper mt20 mt-sm0">
            <div class="link-text align-items-center">
              <div class="link-body text-right">
                <div class="mr15 text-uppercase f-20 f-sm-28 w700">Watch <br>Product Demo</div>
              </div>
              <div class="link-right"><a class="demo-link" href="#" data-toggle="modal" data-target="#myModal">
                  <div class="demo-link-bg"><span class="video-link-overlay"></span></div><span class="icon fa fa-play"></span></a></div>
            </div>
          </div>

            </div>
            <!-- next-generation end -->
            
            <div class="space-section tons-sec">
               <div class="container">
                  <div class="row inner-content">
                     <div class="col-xs-12 px0 px-sm0 f-sm-45 f-28 w700 text-center  black-clr lh140">
                      Have A Look at Some More Cutting-Edge Features  <br class="visible-lg">
of Kyza...
                     </div>
                     <div class="col-xs-12 px0 mt-sm30 mt20">
                     <div class="row">
                        <div class="col-sm-4 col-xs-12">
							<div class="features-list-bg min-height-first">
							   <div class="icon-height">
								  <img src="assets/images/f1.png" class="img-responsive center-block">
							   </div>
							   <div class="col-xs-12 px0 f-sm-26 f-22 w600 mt20 text-center black-clr lh140">
								 Over 500 Ready to <br class="visible-lg">Use Templates
							   </div>
							</div>
                        </div>
                         <div class="col-sm-4 col-xs-12 mt20 mt-sm0">
							<div class="features-list-bg min-height-first">
							   <div class="icon-height">
								  <img src="assets/images/f2.png" class="img-responsive center-block">
							   </div>
							   <div class="col-xs-12 px0 f-sm-26 f-22 w600 mt20 text-center black-clr lh140">
								Cloud Based System with Single Dashboard 
							   </div>
							</div>
                        </div>
                         <div class="col-sm-4 col-xs-12 mt20 mt-sm0">
							<div class="features-list-bg min-height-first">
							   <div class="icon-height">
								  <img src="assets/images/f1.png" class="img-responsive center-block">
							   </div>
							   <div class="col-xs-12 px0 f-sm-26 f-22 w600 mt20 text-center black-clr lh140">
								Total Control Over Your System with No Compliance or Blocking
							   </div>
							</div>
                        </div>
                     </div>
                     </div>
                     <div class="col-xs-12 px0 mt-sm30 mt20">
                     <div class="row">
                        <div class="col-sm-4 col-xs-12">
							<div class="features-list-bg min-height-first">
							   <div class="icon-height">
								  <img src="assets/images/f4.png" class="img-responsive center-block">
							   </div>
							   <div class="col-xs-12 px0 f-sm-26 f-22 w600 mt20 text-center black-clr lh140">
								100+ Integrations for Marketing Platform & Migration 
							   </div>
							</div>
                        </div>
                         <div class="col-sm-4 col-xs-12 mt20 mt-sm0">
							<div class="features-list-bg min-height-first">
							   <div class="icon-height">
								  <img src="assets/images/f5.png" class="img-responsive center-block">
							   </div>
							   <div class="col-xs-12 px0 f-sm-26 f-22 w600 mt20 text-center black-clr lh140">
								Behavior Automation to Triple Your Profit on <br class="visible-lg"> any Sites
							   </div>
							</div>
                        </div>
                         <div class="col-sm-4 col-xs-12 mt20 mt-sm0">
							<div class="features-list-bg min-height-first">
							   <div class="icon-height">
								  <img src="assets/images/f6.png" class="img-responsive center-block">
							   </div>
							   <div class="col-xs-12 px0 f-sm-26 f-22 w600 mt20 text-center black-clr lh140">
								Commercial License to Charge from Your Clients 
							   </div>
							</div>
                        </div>
                     </div>
                     </div>
					 <div class="col-xs-12 px0 mt-sm30 mt20">
                     <div class="row">
                        <div class="col-sm-4 col-xs-12">
							<div class="features-list-bg">
							   <div class="icon-height">
								  <img src="assets/images/f7.png" class="img-responsive center-block">
							   </div>
							   <div class="col-xs-12 px0 f-sm-26 f-22 w600 mt20 text-center black-clr lh140">
								 Advance Sales Booster System to 3X Your Sales
							   </div>
							</div>
                        </div>
                         <div class="col-sm-4 col-xs-12 mt20 mt-sm0">
							<div class="features-list-bg">
							   <div class="icon-height">
								  <img src="assets/images/f8.png" class="img-responsive center-block">
							   </div>
							   <div class="col-xs-12 px0 f-sm-26 f-22 w600 mt20 text-center black-clr lh140">
								Super-Fast & Easy to Use with 100% Mobile Friendly 
							   </div>
							</div>
                        </div>
                         <div class="col-sm-4 col-xs-12 mt20 mt-sm0">
							<div class="features-list-bg">
							   <div class="icon-height">
								  <img src="assets/images/f9.png" class="img-responsive center-block">
							   </div>
							   <div class="col-xs-12 px0 f-sm-26 f-22 w600 mt20 text-center black-clr lh140">
								No Hosting or Domain Needed
							   </div>
							</div>
                        </div>
                     </div>
                     </div>
					 <div class="col-xs-12 px0 mt-sm30 mt20">
                     <div class="row">
                        <div class="col-sm-4 col-xs-12">
							<div class="features-list-bg">
							   <div class="icon-height">
								  <img src="assets/images/f10.png" class="img-responsive center-block">
							   </div>
							   <div class="col-xs-12 px0 f-sm-26 f-22 w600 mt20 text-center black-clr lh140">
								Safe, Secure & 100% GDPR Complaint
							   </div>
							</div>
                        </div>
                         <div class="col-sm-4 col-xs-12 mt20 mt-sm0">
							<div class="features-list-bg">
							   <div class="icon-height">
								  <img src="assets/images/f11.png" class="img-responsive center-block">
							   </div>
							   <div class="col-xs-12 px0 f-sm-26 f-22 w600 mt20 text-center black-clr lh140">
								Advance Reports & <br class="visible-lg"> Analytics
							   </div>
							</div>
                        </div>
                         <div class="col-sm-4 col-xs-12 mt20 mt-sm0">
							<div class="features-list-bg">
							   <div class="icon-height">
								  <img src="assets/images/f12.png" class="img-responsive center-block">
							   </div>
							   <div class="col-xs-12 px0 f-sm-26 f-22 w600 mt20 text-center black-clr lh140">
								Step-by-Step Video <br class="visible-lg"> Training
							   </div>
							</div>
                        </div>
                     </div>
                     </div>
     </div>
               </div>
            </div>
          <div class="space-section deep-funnel" id="funnel">
               <div class="container px0">
                  <div class="row inner-content">
                     <div class="col-xs-12 f-sm-45 f-28 w700 text-center  black-clr lh140">
                       Our Deep & High Converting Sales Funnel 
                     </div>
                     <div class="col-xs-12 col-sm-12 mt20 mt-sm70">
                        <div>
                           <img src="assets/images/funnel.png" class="img-responsive center-block">
                        </div>
                     </div>

                  </div>
               </div>
            </div>
			<div class="space-section contest-section1">
               <div class="container px0">
                  <div class="row inner-content">
                     <div class="col-xs-12 f-sm-45 f-28 w700 text-center white-clr lh140">
                      Get Ready to Grab Your Share of
                     </div>
					  <div class="col-xs-12 f-sm-70 f-40 w700 text-center orange-clr1 lh140">
                     $12000 JV Prizes
                     </div>
                     

                  </div>
               </div>
            </div>
        <div class="space-section contest-section">
               <div class="container px0">
                  <div class="row inner-content">
                     <div class="col-xs-12 f-sm-45 f-28 w700 text-center blue-clr">
                       Pre-Launch Lead Contest
                     </div>
                     <div class="col-xs-12 f-sm-22 f-20 w400 lh140 mt20 text-center">
                       Contest Starts from 23rd Feb’22 at 10:00 AM EST and Ends at 26th Feb’22 at 9:00 AM EST
                     </div>
                     <div class="col-xs-12 f-sm-22 f-20 w400 lh140 mt10 text-center">					
						(Get Flat <span class="w700">$0.50c</span> For Every Lead You Send for <span class="w700">Pre-Launch Webinar</span>)
                     </div>
                     <div class="col-xs-12 col-sm-5 col-sm-offset-0 mt-sm180 mt20">
                        <div>
                           <img src="assets/images/contest-img1.png" class="img-responsive center-block">
                        </div>
                     </div>
                     <div class="col-xs-12 col-sm-7 mt-sm50 mt20">
                        <div>
                           <img src="assets/images/contest-img2.png" class="img-responsive center-block">
                        </div>
                     </div>
                     <div class="col-xs-12 f-20 f-sm-22 lh140 mt-sm50 mt20 w400 text-center black-clr">
                      *(Eligibility – All leads should have at least 1% conversion on launch and should have min 100 leads) 
                     </div>
                  </div>
               </div>
            </div>
            <div class="space-section prize-section">
               <div class="container px0">
                  <div class="row inner-content">
                     <div class="col-xs-12">
                        <div editabletype="image" style="z-index: 10;">
                           <img src="assets/images/prize-img1.png" class="img-responsive center-block">
                        </div>
                     </div>
					  <div class="col-xs-12 mt20 mt-sm50">
                        <div editabletype="image" style="z-index: 10;">
                           <img src="assets/images/prize-img2.png" class="img-responsive center-block">
                        </div>
                     </div>
                     <div class="col-xs-12 f-22 f-sm-34 w600 lh140 mt20 mt-sm50 black-clr">
                        *Contest Policies:
                     </div>
                     <div class="col-xs-12 f-20 f-sm-24 w400 lh140 mt10">
                        1. Team of maximum two is allowed. <br>
                        2. To be eligible to win one of the sales leaderboard prizes, you must have made commissions equal to or greater than the value of the prize. If this criterion is not met, then you will be eligible for the next prize.
                     </div>
                     <div class="col-xs-12 mt-sm0 mt30 px0"></div>
                     <div class="col-xs-12 col-sm-10 col-sm-offset-1 mt20 mt-sm55">
                        <div>
                           <img src="assets/images/prize-img3.png" class="img-responsive center-block">
                        </div>
                     </div>
                     <div class="col-xs-12 mt20 mt-sm30">
                        <div class="f-20 f-sm-24 w400 lh140">
                           <span class="f-22 f-sm-34 w600 lh140">Note:</span>
						   <br>We will announce the winners on 3rd March & Prizes will be distributed through Payoneer from 3rd March Onwards.
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            
            <div class="space-section reciprocate-sec">
               <div class="container px0">
                  <div class="row inner-content">
                     <div class="col-xs-12 text-center">
                      <div class="f-sm-45 f-28 lh140 w700 black-clr converting-shape">
                          Our Solid Track Record of Launching Top <br class="visible-lg"> Converting Products
                        </div>
                     </div>
                     <div class="col-xs-12 f-20 f-sm-22 lh140 mt20 w400 text-center black-clr ">
                       Dr Amit Pareek's Team and Simon Warner are Top Vendors with 6-Figure Launches Consecutively<br class="visible-lg">  
from 2017, 2018, 2019, 2020, 2021 & 2022 
                     </div>
                     <div class="col-xs-12 col-sm-12 mt20">
                           <img src="assets/images/product-logo.png" class="img-responsive center-block">
                     </div>
					 <div class="col-xs-12 text-center f-sm-45 f-28 lh140 w700 black-clr mt20 mt-sm70">                     
                        Do We Reciprocate?                      
                     </div>
					  <div class="col-xs-12 f-20 f-sm-22 lh140 mt20 w400 text-center black-clr ">
                     We've been in top positions on various launch leaderboards & sent huge sales for our valued JVs. <br><br>
So, if you have a top-notch product with top conversions and that fits our list, we would love to drive loads of sales for you. Here's just some results from our recent promotions.
                     </div>
					  <div class="col-xs-12 col-sm-12 mt20 mt-sm50">
                           <img src="assets/images/logos.png" class="img-responsive center-block">
                     </div>
                     <div class="col-xs-12 f-sm-28 f-24 lh140 mt20 mt-sm50 w600 text-center">
                       And The List Goes On And On...
                     </div>
                  </div>
               </div>
            </div>
            <div class="space-section contact-section">
               <div class="container">
                  <div class="row inner-content">
                     <div class="col-xs-12 px0 px-sm15">
                        <div class="f-sm-45 f-28 w700 lh140 text-center white-clr">
                           Have any Query? <span class="w500"> Contact us Anytime</span>
                        </div>
                     </div>
                     <div class="col-xs-12 mt20 mt-sm50 p0 mx-auto">
                        <div class="col-sm-6 col-xs-12 text-center mx-auto">
                           <img src="assets/images/amit-pareek-sir.png" class="img-responsive center-block">
                           <div class="f-22 f-sm-22 w700 mt15 mt-sm20  lh140 text-center white-clr" style="z-index: 10;">
                            Dr Amit Pareek
                           </div>
                           <div class="f-16 w500  lh140 text-center white-clr" style="z-index: 10;">
                              (TechPreneur & Marketer)
                           </div>
                           <div class="col-xs-12 mt30 d-flex justify-content-center">
                              <a href="skype:amit.pareek77" class="link-text">
                                 <div class="col-xs-12 p0">
                                    <img src="assets/images/skype.png" class="center-block">
                                 </div>
                              </a>
							   <a href="http://facebook.com/Dr.AmitPareek" class="link-text">
                                
                                    <img src="assets/images/am-fb.png" class="center-block">
                              </a>
                           </div>
                           
                        </div>
                        <!-- <div class="col-sm-4 col-xs-12 text-center mt20 mt-sm0">
                           <img src="assets/images/achal-goswami-sir.png" class="img-responsive center-block">
                           <div class="f-22 f-sm-22 w700 mt15 mt-sm20  lh140 text-center white-clr" style="z-index: 10;">
                           Achal Goswami
                           </div>
                           <div class="f-16 w500  lh140 text-center white-clr" style="z-index: 10;">
                             (Entrepreneur & Internet Marketer)
                           </div>
                           <div class="col-xs-12 mt30 d-flex justify-content-center">
                              <a href="skype:live:.cid.78f368e20e6d5afa" class="link-text">
                                
                                    <img src="assets/images/skype.png" class="center-block">
                              </a>
							         <a href="https://www.facebook.com/dcp.ambassador.achal/" class="link-text">                                 
                                    <img src="assets/images/am-fb.png" class="center-block">
                              </a>
                           </div>
                           <div class="col-xs-12 mt15">
                             
                           </div>
                        </div> -->
                        <div class="col-sm-6 col-xs-12 text-center mt20 mt-sm0 mx-auto">
                           <img src="assets/images/simon-warner-sir.png" class="img-responsive center-block">
                           <div class="f-22 f-sm-22 w700 mt15 mt-sm20  lh140 text-center white-clr" style="z-index: 10;">
                          Simon Warner 
                           </div>
                           <div class="f-16 w500  lh140 text-center white-clr" style="z-index: 10;">
                             (Internet Marketer & JV Manager)
                           </div>
                           <div class="col-xs-12 mt30 d-flex justify-content-center">
                              <a href="skype:warnerhypnotist" class="link-text">
                                    <img src="assets/images/skype.png" class="center-block">
                              </a>
							   <a href="https://www.facebook.com/simondoesmarketing9" class="link-text">
                                    <img src="assets/images/am-fb.png" class="center-block">
                              </a>
                           </div>
                              </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="space-section terms-section">
               <div class="container px-sm15">
                  <div class="row inner-content">
                     <div class="col-xs-12 px0 px-sm15 f-sm-45 f-28 w700 lh140  text-center black-clr">
                        Affiliate Promotions Terms & Conditions
                     </div>
                     <div class="col-sm-12 col-xs-12 f-20 f-sm-22 lh140 p-sm0 px0 mt-sm20 mt20 w400 text-center">
                       We request you to read this section carefully before requesting for your affiliate link and being a part of this launch. Once
you are approved to be a part of this launch and promote this product, you must abide by the instructions listed below:
                     </div>
                     <div class="col-sm-12 col-xs-12 p0 mt15 mt-sm30">
                        <div class="f-sm-18 f-18 lh140 w400">
                           <ul class="b-tick1 pl0 m0">
 <li>Please be sure to NOT send spam of any kind. This includes cheap traffic methods in any way whatsoever. In case anyone does so, they will be banned from all future promotion with us without any reason whatsoever. No exceptions will be entertained. </li>
 <li>Please ensure you or your members DON’T use negative words such as 'scam' in any of your promotional campaigns in any way. If this comes to our knowledge that you have violated it, your affiliate account will be removed from our system with immediate effect. </li>
 <li>Please make it a point to not OFFER any cash incentives (such as rebates), cash backs etc or any such malpractices to people buying through your affiliate link. </li>
 <li>Please note that you are not AUTHORIZED to create social media pages or run any such campaigns that may be detrimental using our product or brand name in any way possible. Doing this may result in serious consequences. </li>
 <li>We reserve the right to TERMINATE any affiliate with immediate effect if they’re found to breach any/all of the conditions mentioned above in any manner. </li>

                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!--Footer Section Start -->
            <div class="space-section footer-section">
               <div class="container px-sm15 px0">
                  <div class="row inner-content">
                     <div class="col-xs-12 text-center">
                        <div> 
                           <img src="assets/images/footer-logo.png" class="img-responsive center-block"> 
                        </div>
                        <div editabletype="text" class="f-18 f-sm-18 w400 mt15 mt-sm35 lh140 white-clr text-center" style="z-index: 10;">
                           Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. 
                        </div>
                     </div>
                     <div class="col-sm-3 col-xs-12">
                        <div editabletype="text" class="f-18 f-sm-20 w400 mt10 mt-sm58 lh140 white-clr text-xs-center" style="z-index: 10;">
                           Copyright © Kyza
                        </div>
                     </div>
                     <div class="col-sm-9 col-xs-12 mt10 mt-sm60 xstext-center text-right">
                        <ul class="footer-ul w400 f-18 f-sm-20 white-clr">
                           <li><a href="mailto:support@bizomart.com" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                           <li><a href="http://kyza.biz/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                           <li><a href="http://kyza.biz/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                           <li><a href="http://kyza.biz/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                           <li><a href="http://kyza.biz/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                           <li><a href="http://kyza.biz/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                           <li><a href="http://kyza.biz/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
            <!--Footer Section End -->
         </div>
      </div>
      <!-- timer --->
      <?php
         if ($now < $exp_date) {
         
         ?>
      <script type="text/javascript">
         // Count down milliseconds = server_end - server_now = client_end - client_now
         var server_end = <?php echo $exp_date; ?> * 1000;
         var server_now = <?php echo time(); ?> * 1000;
         var client_now = new Date().getTime();
         var end = server_end - server_now + client_now; // this is the real end time
         
         var noob = $('.countdown').length;
         
         var _second = 1000;
         var _minute = _second * 60;
         var _hour = _minute * 60;
         var _day = _hour * 24
         var timer;
         
         function showRemaining() {
          var now = new Date();
          var distance = end - now;
          if (distance < 0) {
          clearInterval(timer);
          document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
          return;
          }
         
          var days = Math.floor(distance / _day);
          var hours = Math.floor((distance % _day) / _hour);
          var minutes = Math.floor((distance % _hour) / _minute);
          var seconds = Math.floor((distance % _minute) / _second);
          if (days < 10) {
          days = "0" + days;
          }
          if (hours < 10) {
          hours = "0" + hours;
          }
          if (minutes < 10) {
          minutes = "0" + minutes;
          }
          if (seconds < 10) {
          seconds = "0" + seconds;
          }
          var i;
          var countdown = document.getElementsByClassName('countdown');
          for (i = 0; i < noob; i++) {
          countdown[i].innerHTML = '';
         
          if (days) {
         	 countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-40 f-sm-40 timerbg">' + days + '</span><br><span class="f-16 w500">Days</span> </div>';
          }
         
          countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-40 f-sm-40 timerbg">' + hours + '</span><br><span class="f-16 w500">Hours</span> </div>';
         
          countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-40 f-sm-40 timerbg">' + minutes + '</span><br><span class="f-16 w500">Mins</span> </div>';
         
          countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-40 f-sm-40 timerbg">' + seconds + '</span><br><span class="f-16 w500">Sec</span> </div>';
          }
         
         }
         timer = setInterval(showRemaining, 1000);
      </script>
      <?php
         } else {
         echo "Times Up";
         }
         ?>
      <!--- timer end-->
	  
	  <!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
 <a href="javascript:void(0);" data-dismiss="modal" class="close-link">&times;</a>
  <div class="modal-dialog help-video-modal">
  

    <!-- Modal content-->
    <div class="modal-content">
       

      <div class="modal-body p0">
<div class="col-xs-12 responsive-video p0">
								<iframe id="test-id" src="https://kyza.dotcompal.com/video/embed/irq6bfzf2m" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
								box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen ></iframe>
							</div>
      </div>

    </div>

  </div>
</div>
 
<script>
$('#myModal').on('hidden.bs.modal', function() {
  //$('iframe').contents().find('video')[0].pause();
  //console.log("sdkfskjfskjfhsjkf");
  //angular.element("#themeCtrl .poster").scope().playPause('pause');
  //console.log(angular.element("#themeCtrl video"));
  //angular.element('#themeCtrl').scope().playPause('pause');
  document.getElementById("test-id").contentWindow.angular.element("#themeCtrl").scope().playPause('pause');
});
</script>
   </body>
</html>
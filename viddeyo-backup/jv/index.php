<html>
   <head>
      <title>JV Page - Viddeyo JV Invite</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=9">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
      <meta name="title" content="Viddeyo | JV">
      <meta name="description" content="BLAZING-FAST Video Hosting, Player & Marketing Technology">
      <meta name="keywords" content="Viddeyo">
      <meta property="og:image" content="https://www.viddeyo.co/jv/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Dr. Amit Pareek">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="Viddeyo | JV">
      <meta property="og:description" content="BLAZING-FAST Video Hosting, Player & Marketing Technology">
      <meta property="og:image" content="https://www.viddeyo.co/jv/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="Viddeyo | JV">
      <meta property="twitter:description" content="BLAZING-FAST Video Hosting, Player & Marketing Technology">
      <meta property="twitter:image" content="https://www.viddeyo.co/jv/thumbnail.png">
      <link rel="icon" href="https://cdn.oppyo.com/launches/viddeyo/assets/images/favicon.png" type="image/png">
      <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
      <link rel="stylesheet" href="https://cdn.oppyo.com/launches/viddeyo/assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style.css" type="text/css">
      <link rel="stylesheet" href="assets/css/timer.css" type="text/css">
      <link rel="stylesheet" href="assets/css/jquery.bxslider.min.css" type="text/css">
      <script src="../common_assets/js/jquery.min.js"></script>
      <script src="assets/css/jquery.bxslider.min.js"></script>
      <!-- Google Tag Manager -->
      <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
         new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
         j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
         'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
         })(window,document,'script','dataLayer','GTM-NMK2MBB');
      </script>
      <!-- End Google Tag Manager -->
   </head>
   <body>
      <!-- Google Tag Manager (noscript) -->
      <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NMK2MBB"
         height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
      <!-- End Google Tag Manager (noscript) --> 
      <!-- New Timer  Start-->
      <?php
         $date = 'June 2 2022 10:00 AM EST';
         $exp_date = strtotime($date);
         $now = time();  
         /*
         
         $date = date('F d Y g:i:s A eO');
         $rand_time_add = rand(700, 1200);
         $exp_date = strtotime($date) + $rand_time_add;
         $now = time();*/
         
         if ($now < $exp_date) {
         ?>
      <?php
         } else {
          echo "Times Up";
         }
         ?>
      <!-- New Timer End -->
      <!-- Header Section Start -->
      <div class="header-section">
         <div class="container-xxl">
            <div class="row">
               <div class="col-12 d-flex align-items-center justify-content-center  justify-content-md-between flex-wrap flex-md-nowrap">
                  <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 909.75 162.72" style="enable-background:new 0 0 909.75 162.72; max-height:56px;" xml:space="preserve">
                     <style type="text/css">
                        .st00{fill:#FFFFFF;}
                        .st11{fill:url(#SVGID_1_);}
                     </style>
                     <g>
                        <g>
                           <path class="st00" d="M216.23,71.07L201.6,97.26l-35.84-64.75l-3.16-5.7h29.13l0.82,1.49L216.23,71.07z M288.35,26.81l-3.16,5.7
                              l-56.4,103.68h-6.26l-11.71-22.25l14.63-26.18l0.01,0.02l2.73-4.94l4.81-8.68l25.38-45.87l0.82-1.49H288.35z"/>
                           <path class="st00" d="M329.85,26.61v109.57h-23.23V26.61H329.85z"/>
                           <path class="st00" d="M454.65,81.7c0,2.7-0.2,5.41-0.59,8.04c-3.58,24.36-23.15,43.45-47.59,46.42l-0.26,0.03h-50.93v-69.9h23.23
                              v46.67h26.13c12.93-2,23.4-11.91,26.09-24.74c0.45-2.14,0.67-4.33,0.67-6.51c0-2.11-0.21-4.22-0.62-6.26
                              c-1.52-7.54-5.76-14.28-11.95-18.97c-3.8-2.88-8.14-4.84-12.75-5.78c-1.58-0.32-3.19-0.52-4.83-0.6h-45.98V26.82L403.96,27
                              c11.89,0.9,23.21,5.67,32.17,13.61c9.78,8.65,16.15,20.5,17.96,33.36C454.46,76.49,454.65,79.1,454.65,81.7z"/>
                           <path class="st00" d="M572.18,81.7c0,2.7-0.2,5.41-0.59,8.04c-3.58,24.36-23.15,43.45-47.59,46.42l-0.26,0.03h-50.93v-69.9h23.23
                              v46.67h26.13c12.93-2,23.4-11.91,26.09-24.74c0.45-2.14,0.67-4.33,0.67-6.51c0-2.11-0.21-4.22-0.62-6.26
                              c-1.52-7.54-5.76-14.28-11.95-18.97c-3.8-2.88-8.14-4.84-12.75-5.78c-1.58-0.32-3.19-0.52-4.83-0.6h-45.98V26.82L521.5,27
                              c11.89,0.9,23.2,5.67,32.17,13.61c9.78,8.65,16.15,20.5,17.96,33.36C572,76.49,572.18,79.1,572.18,81.7z"/>
                           <path class="st00" d="M688.08,26.82v23.23h-97.72V31.18l0.01-4.36H688.08z M613.59,112.96h74.49v23.23h-97.72v-18.87l0.01-4.36
                              V66.1l23.23,0.14h64.74v23.23h-64.74V112.96z"/>
                           <path class="st00" d="M808.03,26.53l-5.07,6.93l-35.84,48.99l-1.08,1.48L766,83.98l-0.04,0.05l0,0.01l-0.83,1.14v51h-23.23V85.22
                              l-0.86-1.18l-0.01-0.01l-0.04-0.05l-0.04-0.06l-1.08-1.48l-35.84-48.98l-5.07-6.93h28.77l1.31,1.78l24.46,33.43l24.46-33.43
                              l1.31-1.78H808.03z"/>
                           <path class="st00" d="M909.75,81.69c0,30.05-24.45,54.49-54.49,54.49c-30.05,0-54.49-24.45-54.49-54.49S825.2,27.2,855.25,27.2
                              C885.3,27.2,909.75,51.65,909.75,81.69z M855.25,49.95c-17.51,0-31.75,14.24-31.75,31.75s14.24,31.75,31.75,31.75
                              c17.51,0,31.75-14.24,31.75-31.75C887,64.19,872.76,49.95,855.25,49.95z"/>
                        </g>
                     </g>
                     <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="0" y1="81.3586" x2="145.8602" y2="81.3586">
                        <stop  offset="0.4078" style="stop-color:#FFFFFF"/>
                        <stop  offset="1" style="stop-color:#FC6DAB"/>
                     </linearGradient>
                     <path class="st11" d="M11.88,0c41.54,18.27,81.87,39.12,121.9,60.41c7.38,3.78,12.24,11.99,12.07,20.32
                        c0.03,7.94-4.47,15.72-11.32,19.73c-27.14,17.07-54.59,33.74-82.21,50c-0.57,0.39-15.29,8.93-15.47,9.04
                        c-1.4,0.77-2.91,1.46-4.44,1.96c-14.33,4.88-30.25-4.95-32.19-19.99c-0.18-1.2-0.23-2.5-0.24-3.72c0.35-25.84,1.01-53.01,2.04-78.83
                        C2.57,42.41,20.3,32.08,34.79,40.31c8.16,4.93,16.31,9.99,24.41,15.03c2.31,1.49,7.96,4.89,10.09,6.48
                        c5.06,3.94,7.76,10.79,6.81,17.03c-0.68,4.71-3.18,9.07-7.04,11.77c-0.38,0.24-1.25,0.88-1.63,1.07c-3.6,2.01-7.5,4.21-11.11,6.17
                        c-9.5,5.22-19.06,10.37-28.78,15.27c11.29-9.81,22.86-19.2,34.53-28.51c3.54-2.55,4.5-7.53,1.83-10.96c-0.72-0.8-1.5-1.49-2.49-1.9
                        c-0.18-0.08-0.42-0.22-0.6-0.29c-11.27-5.17-22.82-10.58-33.98-15.95c0,0-0.22-0.1-0.22-0.1l-0.09-0.02l-0.18-0.04
                        c-0.21-0.08-0.43-0.14-0.66-0.15c-0.11-0.01-0.2-0.07-0.31-0.04c-0.21,0.03-0.4-0.04-0.6,0.03c-0.1,0.02-0.2,0.02-0.29,0.03
                        c-0.14,0.06-0.28,0.1-0.43,0.12c-0.13,0.06-0.27,0.14-0.42,0.17c-0.09,0.03-0.17,0.11-0.26,0.16c-0.08,0.06-0.19,0.06-0.26,0.15
                        c-0.37,0.25-0.66,0.57-0.96,0.9c-0.1,0.2-0.25,0.37-0.32,0.59c-0.04,0.08-0.1,0.14-0.1,0.23c-0.1,0.31-0.17,0.63-0.15,0.95
                        c-0.06,0.15,0.04,0.35,0.02,0.52c0,0.02,0,0.08-0.01,0.1c0,0,0,0.13,0,0.13l0.02,0.51c0.94,24.14,1.59,49.77,1.94,73.93
                        c0,0,0.06,4.05,0.06,4.05l0,0.12l0.01,0.02l0.01,0.03c0,0.05,0.02,0.11,0.02,0.16c0.08,0.23,0.16,0.29,0.43,0.47
                        c0.31,0.16,0.47,0.18,0.71,0.09c0.06-0.04,0.11-0.06,0.18-0.1c0.02-0.01-0.01,0.01,0.05-0.03l0.22-0.13l0.87-0.52
                        c32.14-19.09,64.83-37.83,97.59-55.81c0,0,0.23-0.13,0.23-0.13c0.26-0.11,0.51-0.26,0.69-0.42c1.15-0.99,0.97-3.11-0.28-4.01
                        c0,0-0.43-0.27-0.43-0.27l-1.7-1.1C84.73,51.81,47.5,27.03,11.88,0L11.88,0z"/>
                  </svg>
                  <div class="d-flex align-items-center flex-wrap justify-content-center  justify-content-md-end mt15 mt-md0">
                     <ul class="leader-ul f-18 f-md-20">
                        <li>
                           <a href="https://docs.google.com/document/d/1LWe2ZrBc4qgSWg_CWUUlyPqezSYpeFo2/edit?usp=sharing&ouid=100915838436199608736&rtpof=true&sd=true" target="_blank">JV Doc</a>
                        </li>
                        <li>|</li>
                        <li>
                           <a href="https://docs.google.com/document/d/14fFcNnTHhncTWZaDE9XkPL3VcxljZCea/edit?usp=sharing&ouid=100915838436199608736&rtpof=true&sd=true" target="_blank">Swipes</a>
                        </li>
                        <li>|</li>
                        <li>
                           <a href="#funnel">Funnel</a>
                        </li>
                     </ul>
                     <a href="https://www.jvzoo.com/affiliate/affiliateinfonew/index/382061" class="affiliate-link-btn ml-md15 mt10 mt-md0">Grab Your Affiliate Link</a>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50 text-center">
                  <div class="preheadline f-18 f-md-30 w600 white-clr lh140">
                     Join Us On June 2<sup>nd</sup> For Something EXTRAORDINARY!
                  </div>
               </div>
               <div class="col-12 mt-md10 mt10 f-md-50 f-28 w400 text-center white-clr lh140">
                  Bank In Big By Promoting A <span class="w700 highlight">BLAZING-FAST Video
                  Hosting, Player & Marketing Technology</span><br class="d-none d-md-block">
                  That’s Smartly... 
               </div>
               <div class="col-12 mt20 mt-md20 f-md-22 f-20 w400 white-clr text-center">
                  <ul class="bxslider text f-28 f-md-50">
                     <li class="text-clr1">
                        <div> <img src="https://cdn.oppyo.com/launches/viddeyo/jv/video-icon.png" class="img-fluid"></div>
                        <div class="text">Processed 150,324+ Videos Successfully</div>
                     </li>
                     <li class="text-clr2">
                        <div> <img src="https://cdn.oppyo.com/launches/viddeyo/jv/eye-icon.png" class="img-fluid"></div>
                        <div class="text">Got 69,330,042+ Video Views Till Now</div>
                     </li>
                     <li class="text-clr3">
                        <div> <img src="https://cdn.oppyo.com/launches/viddeyo/jv/rocket-icon.png" class="img-fluid"></div>
                        <div class="text">Played 4,870,031+ Minutes of Videos</div>
                     </li>
                     <li class="text-clr4">
                        <div> <img src="https://cdn.oppyo.com/launches/viddeyo/jv/bag-icon.png" class="img-fluid"></div>
                        <div class="text">Served 15,810+ Successful Businesses</div>
                     </li>
                  </ul>
               </div>
               <div class="col-12 col-md-12 mt20 mt-md30">
                  <div class="row">
                     <div class="col-md-8 col-12">
                        <div class="responsive-video">
                           <iframe class="embed-responsive-item" src="https://launch.oppyo.com/video/embed/pmqohwdz8n" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                              box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                        </div>
                     </div>
                     <div class="col-md-4 col-12 mt20 mt-md0">
                        <div class="calendar-wrap">
                           <div class="calendar-wrap-inline">
                              <div class="text-uppercase f-26 f-md-32 w500">Thursday</div>
                              <div class="f-36 f-md-50 w600">June</div>
                              <div class="date mt10">02<span>nd</span></div>
                              <div class="text-uppercase f-24 f-md-32 w600">At 11 AM EST</div>
                           </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="countdown counter-black mt10">
                           <div class="timer-label text-center"><span class="f-40 f-md-50 lh100 timerbg col-12">01</span><br><span class="f-20 w600">Days</span> </div>
                           <div class="timer-label text-center"><span class="f-40 f-md-50 lh100 timerbg">16</span><br><span class="f-20 w600">Hours</span> </div>
                           <div class="timer-label text-center"><span class="f-40 f-md-50 lh100 timerbg">59</span><br><span class="f-20 w600">Mins</span> </div>
                           <div class="timer-label text-center"><span class="f-40 f-md-50 lh100 timerbg">37</span><br><span class="f-20 w600">Sec</span> </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="marketing-section">
         <div class="container-xxl">
            <div class="row">
               <div class="col-12 f-30 f-md-56 w700 text-center">
                  Welcome to the <span class="gradient-clr">Future of Video Marketing</span>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md60" data-aos="fade-up">
                  <div class="content-box">
                     <div class="title">Lightning FAST Video Hosting &
                        Player for HD videos
                     </div>
                     <img src="https://cdn.oppyo.com/launches/viddeyo/jv/m1.png" class="img-fluid d-block mx-auto"> 
                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md60" data-aos="fade-up">
                  <div class="content-box">
                     <div class="title">Premium Video Hosting
                        & Marketing For 1-Time Price
                     </div>
                     <img src="https://cdn.oppyo.com/launches/viddeyo/jv/m2.png" class="img-fluid d-block mx-auto"> 
                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md60" data-aos="fade-up">
                  <div class="content-box">
                     <div class="title">Sell, Promote or Collect Leads
                        Directly Inside The Videos
                     </div>
                     <img src="https://cdn.oppyo.com/launches/viddeyo/jv/m3.png" class="img-fluid d-block mx-auto"> 
                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md60" data-aos="fade-up">
                  <div class="content-box">
                     <div class="title">Seamlessly Integrated With <br class="d-none d-md-block">
                        Major Autoresponders
                     </div>
                     <img src="https://cdn.oppyo.com/launches/viddeyo/jv/m4.png" class="img-fluid d-block mx-auto"> 
                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md60" data-aos="fade-up">
                  <div class="content-box">
                     <div class="title">Create Beautiful & SEO Friendly
                        Video Channels & Playlists
                     </div>
                     <img src="https://cdn.oppyo.com/launches/viddeyo/jv/m5.png" class="img-fluid d-block mx-auto"> 
                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md60" data-aos="fade-up">
                  <div class="content-box">
                     <div class="title">Advanced Analytics & Stats
                        Comparison for Various Videos
                     </div>
                     <img src="https://cdn.oppyo.com/launches/viddeyo/jv/m6.png" class="img-fluid d-block mx-auto"> 
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Header Section End -->
      <div class="form-section">
         <div class="container-xxl">
            <div class="row">
               <div class="col-12 col-md-12">
                  <div class="auto_responder_form inp-phold formbg col-12">
                     <div class="f-md-32 f-24 d-block mb0 lh140 w600 text-center white-clr">
                        <span class="w700">Subscribe To Our JV List</span> and Be The First to Know Our<br class="d-none d-md-block">
                        Special Contest, Events and Discounts
                     </div>
                     <!-- Aweber Form Code -->
                     <div class="mt15 mt-md50">
                        <form method="post" class="af-form-wrapper mb-md5" accept-charset="UTF-8" action="https://www.aweber.com/scripts/addlead.pl"  >
                           <div style="display: none;">
                              <input type="hidden" name="meta_web_form_id" value="905695664" />
                              <input type="hidden" name="meta_split_id" value="" />
                              <input type="hidden" name="listname" value="awlist6273250" />
                              <input type="hidden" name="redirect" value="https://www.aweber.com/thankyou.htm?m=default" id="redirect_784bc4c23f3d421c6579a853a56ed723" />
                              <input type="hidden" name="meta_adtracking" value="Viddeyo_JV_Signup" />
                              <input type="hidden" name="meta_message" value="1" />
                              <input type="hidden" name="meta_required" value="name,email" />
                              <input type="hidden" name="meta_tooltip" value="" />
                           </div>
                           <div id="af-form-905695664" class="af-form">
                              <div id="af-body-905695664" class="af-body af-standards row justify-content-center">
                                 <div class="af-element col-md-4">
                                    <label class="previewLabel" for="awf_field-114140182" style="display:none;">Name: </label>
                                    <div class="af-textWrap mb15 mb-md15 input-type">
                                       <input id="awf_field-114140182" class="frm-ctr-popup form-control input-field" type="text" name="name" placeholder="Your Name" class="text" value=""  onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="500" />
                                    </div>
                                    <div class="af-clear"></div>
                                 </div>
                                 <div class="af-element mb15 mb-md25  col-md-4">
                                    <label class="previewLabel" for="awf_field-114140183" style="display:none;">Email: </label>
                                    <div class="af-textWrap input-type">
                                       <input class="text frm-ctr-popup form-control input-field" id="awf_field-114140183" type="text" name="email" placeholder="Your Email" value=""  tabindex="501" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " />
                                    </div>
                                    <div class="af-clear"></div>
                                 </div>
                                 <div class="af-element buttonContainer button-type form-btn white-clr col-md-4">
                                    <input name="submit" class="submit f-20 f-md-24 white-clr center-block" type="submit" value="Subscribe For JV Updates" tabindex="502" />
                                    <div class="af-clear"></div>
                                 </div>
                              </div>
                           </div>
                           <div style="display: none;"><img src="https://forms.aweber.com/form/displays.htm?id=nAysbJysbGws" alt="" /></div>
                        </form>
                        <!-- Aweber Form Code -->
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-12 mt20 mt-md70 p-md0">
                  <div class="f-24 f-md-30 w600 text-center lh140">
                     <span>Grab Your JVZoo Affiliate Link</span> to Jump on This Amazing Product Launch
                  </div>
               </div>
               <div class="col-md-12 mx-auto mt20">
                  <div class="row justify-content-center align-items-center">
                     <div class="col-12 col-md-3">
                        <img src="https://cdn.oppyo.com/launches/viddeyo/jv/jvzoo.png" class="img-fluid d-block mx-auto" />
                     </div>
                     <div class="col-12 col-md-4 affiliate-btn  mt-md19 mt15">
                        <a href="https://www.jvzoo.com/affiliate/affiliateinfonew/index/382061" class="f-22 f-md-24 w600 mx-auto">Request Affiliate Link</a>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Header Section End -->
      <!-- exciting-launch -->
      <div class="exciting-launch">
         <div class="container-xxl">
            <div class="row">
               <div class="col-12">
                  <div class="f-md-48 f-28 white-clr text-center w600 lh140">
                     This Exciting Launch Event Is Divided Into 2 Phases
                  </div>
               </div>
               <div class="col-12 mt-md60 mt30">
                  <div class="row align-items-center">
                     <div class="col-md-6 col-12 order-md-2">
                        <img src="https://cdn.oppyo.com/launches/viddeyo/jv/phase1.png" class="img-fluid d-block mx-auto" />
                     </div>
                     <div class="col-md-6 col-12 p-md0 mt-md0 mt20 order-md-1 white-clr">
                        <div class="f-md-32 f-24 lh140 w500">
                           To Make You Max Commissions
                        </div>
                        <ul class="launch-list f-md-20 f-18 w400 mb0 mt-md30 mt20 pl20">
                           <li>All Leads Are Hardcoded</li>
                           <li>Exciting $2000 Pre-Launch Contest</li>
                           <li>We'll Re-Market Your Leads Heavily</li>
                           <li>Pitch Bundle Offer on webinars.</li>
                        </ul>
                     </div>
                  </div>
                  <div class="row mt-md50 mt40 align-items-center">
                     <div class="col-md-6 col-12">
                        <img src="https://cdn.oppyo.com/launches/viddeyo/jv/phase2.png" class="img-fluid d-block mx-auto" />
                     </div>
                     <div class="col-md-6 col-12 mt-md0 mt20 white-clr">
                        <div class="f-md-32 f-24 lh140 w500">
                           Big Opening Contest & Bundle Offer
                        </div>
                        <ul class="launch-list f-md-20 f-18 w400 mb0 mt-md30 mt20 pl20">
                           <li>High in Demand Product with Top Conversion</li>
                           <li>Deep Funnel to Make You Double Digit EPCs</li>
                           <li>Earn upto $415/Sale</li>
                           <li>Huge $12K JV Prizes</li>
                           <li>We'll Re-Market Your Visitors Heavily</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- exciting-launch end -->
      <div class="hello-awesome">
         <div class="container-xxl">
            <div class="row">
               <div class="col-12 mt20 mt-md40">
                  <div class="awesome-shape">
                     <div class="row">
                        <div class="col-12 text-center">
                           <div class="f-md-50 f-28 lh140 w600 white-clr heading-design">
                              Hello Awesome JV’s
                           </div>
                        </div>
                        <div class="col-12 mt20 mt-md30">
                           <div class="row align-items-center">
                              <div class="col-12 col-md-6">
                                 <div class="f-18 f-md-20 lh140 w400 black-clr">
                                    It's Dr. Amit Pareek, CEO & Founder of 2 Start-ups (Funded by investors) along with my Partner  Atul Pareek (Internet Marketer & JV Manager). <br><br>
                                    We have delivered 25+ 6-Figure Blockbuster Launches, Sold Software Products of Over $9 Million, and paid over $4 Million in commission to our Affiliates. <br><br>
                                    With the combined experience of 25+ years, we are coming back with another Top Notch and High in Demand product that will provide a complete solution for all your video marketing needs under one dashboard. 
                                    <br><br>
                                    Check out the incredible features of this amazing technology that will blow away your mind. And we guarantee that this offer will convert like Hot Cakes starting from 2nd June'22 at 11:00 AM EST! Get Ready!!
                                 </div>
                              </div>
                              <div class="col-md-6 col-12 text-center" data-aos="fade-left">
                                 <img src="https://cdn.oppyo.com/launches/viddeyo/jv/amit-pareek.png" class="img-fluid d-block mx-auto mt20">
                                
                                 <img src="https://cdn.oppyo.com/launches/viddeyo/jv/atul-parrek.png" class="img-fluid d-block mx-auto mt20">
                              </div>
                           </div>
                        </div>
                        <div class="col-12 mt-md40 mt20">
                           <div class="awesome-feature-shape">
                              <div class="row ">
                                 <div class="col-12 f-md-24 f-20 lh140 w600 text-center mb20 mb-md30">
                                    Also, here are some stats from our previous launches:
                                 </div>
                                 <div class="col-12 col-md-5 f-18 f-md-20 lh140 w400">
                                    <ul class="awesome-list">
                                       <li>Over 150 Pick of The Day Awards</li>
                                       <li>Over $4Mn in Affiliate Sales for Partners</li>
                                    </ul>
                                 </div>
                                 <div class="col-12 col-md-7 f-18 f-md-20 lh140 w400 mt10 mt-md0">
                                    <ul class="awesome-list">
                                       <li>Under Top 10 JVZoo Affiliate (High Performance Leader)</li>
                                       <li>In Top-10 of JVZoo Top Selling Vendors</li>
                                    </ul>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- next-generation -->
      <div class="proudly-sec" id="product">
         <div class="container-xxl">
            <div class="row">
               <div class="col-12">
                  <img src="https://cdn.oppyo.com/launches/viddeyo/jv/arrow.png" class="img-fluid d-block mx-auto vert-move">
               </div>
               <div class="col-12 text-center f-28 f-md-50 w600 lh140 white-clr mt10 mt-md50">
                  Presenting…
               </div>
               <div class="col-12 mt-md30 mt20 text-center">
                  <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 909.75 162.72" style="enable-background:new 0 0 909.75 162.72; max-height:100px;" xml:space="preserve">
                     <style type="text/css">
                        .st00{fill:#FFFFFF;}
                        .st11{fill:url(#SVGID_1_);}
                     </style>
                     <g>
                        <g>
                           <path class="st00" d="M216.23,71.07L201.6,97.26l-35.84-64.75l-3.16-5.7h29.13l0.82,1.49L216.23,71.07z M288.35,26.81l-3.16,5.7
                              l-56.4,103.68h-6.26l-11.71-22.25l14.63-26.18l0.01,0.02l2.73-4.94l4.81-8.68l25.38-45.87l0.82-1.49H288.35z"/>
                           <path class="st00" d="M329.85,26.61v109.57h-23.23V26.61H329.85z"/>
                           <path class="st00" d="M454.65,81.7c0,2.7-0.2,5.41-0.59,8.04c-3.58,24.36-23.15,43.45-47.59,46.42l-0.26,0.03h-50.93v-69.9h23.23
                              v46.67h26.13c12.93-2,23.4-11.91,26.09-24.74c0.45-2.14,0.67-4.33,0.67-6.51c0-2.11-0.21-4.22-0.62-6.26
                              c-1.52-7.54-5.76-14.28-11.95-18.97c-3.8-2.88-8.14-4.84-12.75-5.78c-1.58-0.32-3.19-0.52-4.83-0.6h-45.98V26.82L403.96,27
                              c11.89,0.9,23.21,5.67,32.17,13.61c9.78,8.65,16.15,20.5,17.96,33.36C454.46,76.49,454.65,79.1,454.65,81.7z"/>
                           <path class="st00" d="M572.18,81.7c0,2.7-0.2,5.41-0.59,8.04c-3.58,24.36-23.15,43.45-47.59,46.42l-0.26,0.03h-50.93v-69.9h23.23
                              v46.67h26.13c12.93-2,23.4-11.91,26.09-24.74c0.45-2.14,0.67-4.33,0.67-6.51c0-2.11-0.21-4.22-0.62-6.26
                              c-1.52-7.54-5.76-14.28-11.95-18.97c-3.8-2.88-8.14-4.84-12.75-5.78c-1.58-0.32-3.19-0.52-4.83-0.6h-45.98V26.82L521.5,27
                              c11.89,0.9,23.2,5.67,32.17,13.61c9.78,8.65,16.15,20.5,17.96,33.36C572,76.49,572.18,79.1,572.18,81.7z"/>
                           <path class="st00" d="M688.08,26.82v23.23h-97.72V31.18l0.01-4.36H688.08z M613.59,112.96h74.49v23.23h-97.72v-18.87l0.01-4.36
                              V66.1l23.23,0.14h64.74v23.23h-64.74V112.96z"/>
                           <path class="st00" d="M808.03,26.53l-5.07,6.93l-35.84,48.99l-1.08,1.48L766,83.98l-0.04,0.05l0,0.01l-0.83,1.14v51h-23.23V85.22
                              l-0.86-1.18l-0.01-0.01l-0.04-0.05l-0.04-0.06l-1.08-1.48l-35.84-48.98l-5.07-6.93h28.77l1.31,1.78l24.46,33.43l24.46-33.43
                              l1.31-1.78H808.03z"/>
                           <path class="st00" d="M909.75,81.69c0,30.05-24.45,54.49-54.49,54.49c-30.05,0-54.49-24.45-54.49-54.49S825.2,27.2,855.25,27.2
                              C885.3,27.2,909.75,51.65,909.75,81.69z M855.25,49.95c-17.51,0-31.75,14.24-31.75,31.75s14.24,31.75,31.75,31.75
                              c17.51,0,31.75-14.24,31.75-31.75C887,64.19,872.76,49.95,855.25,49.95z"/>
                        </g>
                     </g>
                     <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="0" y1="81.3586" x2="145.8602" y2="81.3586">
                        <stop  offset="0.4078" style="stop-color:#FFFFFF"/>
                        <stop  offset="1" style="stop-color:#FC6DAB"/>
                     </linearGradient>
                     <path class="st11" d="M11.88,0c41.54,18.27,81.87,39.12,121.9,60.41c7.38,3.78,12.24,11.99,12.07,20.32
                        c0.03,7.94-4.47,15.72-11.32,19.73c-27.14,17.07-54.59,33.74-82.21,50c-0.57,0.39-15.29,8.93-15.47,9.04
                        c-1.4,0.77-2.91,1.46-4.44,1.96c-14.33,4.88-30.25-4.95-32.19-19.99c-0.18-1.2-0.23-2.5-0.24-3.72c0.35-25.84,1.01-53.01,2.04-78.83
                        C2.57,42.41,20.3,32.08,34.79,40.31c8.16,4.93,16.31,9.99,24.41,15.03c2.31,1.49,7.96,4.89,10.09,6.48
                        c5.06,3.94,7.76,10.79,6.81,17.03c-0.68,4.71-3.18,9.07-7.04,11.77c-0.38,0.24-1.25,0.88-1.63,1.07c-3.6,2.01-7.5,4.21-11.11,6.17
                        c-9.5,5.22-19.06,10.37-28.78,15.27c11.29-9.81,22.86-19.2,34.53-28.51c3.54-2.55,4.5-7.53,1.83-10.96c-0.72-0.8-1.5-1.49-2.49-1.9
                        c-0.18-0.08-0.42-0.22-0.6-0.29c-11.27-5.17-22.82-10.58-33.98-15.95c0,0-0.22-0.1-0.22-0.1l-0.09-0.02l-0.18-0.04
                        c-0.21-0.08-0.43-0.14-0.66-0.15c-0.11-0.01-0.2-0.07-0.31-0.04c-0.21,0.03-0.4-0.04-0.6,0.03c-0.1,0.02-0.2,0.02-0.29,0.03
                        c-0.14,0.06-0.28,0.1-0.43,0.12c-0.13,0.06-0.27,0.14-0.42,0.17c-0.09,0.03-0.17,0.11-0.26,0.16c-0.08,0.06-0.19,0.06-0.26,0.15
                        c-0.37,0.25-0.66,0.57-0.96,0.9c-0.1,0.2-0.25,0.37-0.32,0.59c-0.04,0.08-0.1,0.14-0.1,0.23c-0.1,0.31-0.17,0.63-0.15,0.95
                        c-0.06,0.15,0.04,0.35,0.02,0.52c0,0.02,0,0.08-0.01,0.1c0,0,0,0.13,0,0.13l0.02,0.51c0.94,24.14,1.59,49.77,1.94,73.93
                        c0,0,0.06,4.05,0.06,4.05l0,0.12l0.01,0.02l0.01,0.03c0,0.05,0.02,0.11,0.02,0.16c0.08,0.23,0.16,0.29,0.43,0.47
                        c0.31,0.16,0.47,0.18,0.71,0.09c0.06-0.04,0.11-0.06,0.18-0.1c0.02-0.01-0.01,0.01,0.05-0.03l0.22-0.13l0.87-0.52
                        c32.14-19.09,64.83-37.83,97.59-55.81c0,0,0.23-0.13,0.23-0.13c0.26-0.11,0.51-0.26,0.69-0.42c1.15-0.99,0.97-3.11-0.28-4.01
                        c0,0-0.43-0.27-0.43-0.27l-1.7-1.1C84.73,51.81,47.5,27.03,11.88,0L11.88,0z"/>
                  </svg>
               </div>
               <div class="col-12 f-md-38 f-22 mt-md30 mt20 w600 text-center white-clr lh140">
                  One Platform for All Your Video Marketing Needs
               </div>
               <div class="col-12 mt20 mt-md50">
                  <img src="https://cdn.oppyo.com/launches/viddeyo/jv/product-image.png" class="img-fluid d-block mx-auto img-animation">
               </div>
            </div>
         </div>
      </div>
      <!-- next-generation end -->
      <!-- FEATURE LIST SECTION START -->
      <div class="feature-sec">
         <div class="container-xxl">
            <div class="row align-items-center" data-aos="fade-left">
               <div class="col-md-6 col-12 f-md-32 f-22 w600">
                  <img src="https://cdn.oppyo.com/launches/viddeyo/jv/left-arrow.png" class="img-fluid ms-auto d-none d-md-block">
                  Lightning-FAST Video Hosting & Marketing Platform that Deliver HD Videos without any delay <br class="d-none d-md-block"> or buffering
               </div>
               <div class="col-md-6 col-12 mt-md0 mt20">
                  <img src="https://cdn.oppyo.com/launches/viddeyo/jv/f1.png" class="img-fluid mx-auto d-block">
               </div>
            </div>
            <div class="row align-items-center mt-md80 mt30" data-aos="fade-right">
               <div class="col-md-6 col-12 f-md-32 f-22 w600 order-md-2">
                  <img src="https://cdn.oppyo.com/launches/viddeyo/jv/right-arrow.png" class="img-fluid d-none d-md-block">
                  Play Elegant Videos on Any Site, Page, or 
                  A device without touching any code, just 
                  copy-paste-play videos beautifully in 3 easy steps
               </div>
               <div class="col-md-6 col-12 mt-md0 mt20 order-md-1">
                  <img src="https://cdn.oppyo.com/launches/viddeyo/jv/f2.png" class="img-fluid mx-auto d-block">
               </div>
            </div>
            <div class="row align-items-center mt-md80 mt30" data-aos="fade-left">
               <div class="col-md-6 col-12 f-md-32 f-22 w600">
                  <img src="https://cdn.oppyo.com/launches/viddeyo/jv/left-arrow.png" class="img-fluid ms-auto d-none d-md-block">
                  Create Unlimited Video <br class="d-none d-md-block"> Channels 
                  to Boost Your Brand
               </div>
               <div class="col-md-6 col-12 mt-md0 mt20">
                  <img src="https://cdn.oppyo.com/launches/viddeyo/jv/f3.png" class="img-fluid mx-auto d-block">
               </div>
            </div>
            <div class="row align-items-center mt-md80 mt30" data-aos="fade-right">
               <div class="col-md-6 col-12 f-md-32 f-22 w600 order-md-2">
                  <img src="https://cdn.oppyo.com/launches/viddeyo/jv/right-arrow.png" class="img-fluid d-none d-md-block">
                  Have 100% Control over Your Videos Traffic
                  as No Traffic Leakage, Disturbances or 
                  Distractions due to Any 3rd Party Ads.
               </div>
               <div class="col-md-6 col-12 mt-md0 mt20 order-md-1">
                  <img src="https://cdn.oppyo.com/launches/viddeyo/jv/f4.png" class="img-fluid mx-auto d-block">
               </div>
            </div>
            <div class="row align-items-center mt-md80 mt30" data-aos="fade-left">
               <div class="col-md-6 col-12 f-md-32 f-22 w600">
                  <img src="https://cdn.oppyo.com/launches/viddeyo/jv/left-arrow.png" class="img-fluid ms-auto d-none d-md-block">
                  Robust & Proven Solution with powerful 
                  battle-tested architecture and ultra-fast CDN 
               </div>
               <div class="col-md-6 col-12 mt-md0 mt20">
                  <img src="https://cdn.oppyo.com/launches/viddeyo/jv/f5.png" class="img-fluid mx-auto d-block">
               </div>
            </div>
            <div class="row align-items-center mt-md80 mt30" data-aos="fade-right">
               <div class="col-md-6 col-12 f-md-32 f-22 w600 order-md-2">
                  <img src="https://cdn.oppyo.com/launches/viddeyo/jv/right-arrow.png" class="img-fluid d-none d-md-block">
                  Sell, Promote or Collect Leads 
                  Directly Inside the Videos
               </div>
               <div class="col-md-6 col-12 mt-md0 mt20 order-md-1">
                  <img src="https://cdn.oppyo.com/launches/viddeyo/jv/f6.png" class="img-fluid mx-auto d-block">
               </div>
            </div>
            <div class="row align-items-center mt-md80 mt30" data-aos="fade-left">
               <div class="col-md-6 col-12 f-md-32 f-22 w600">
                  <img src="https://cdn.oppyo.com/launches/viddeyo/jv/left-arrow.png" class="img-fluid ms-auto d-none d-md-block">
                  Advanced Analytics & Compare 
                  Stats for Various Videos
               </div>
               <div class="col-md-6 col-12 mt-md0 mt20">
                  <img src="https://cdn.oppyo.com/launches/viddeyo/jv/f7.png" class="img-fluid mx-auto d-block">
               </div>
            </div>
         </div>
      </div>
      <div class="feature-list">
         <div class="container-xxl">
            <div class="row">
               <div class="col-12 f-md-50 f-28 w600 lh140 text-center white-clr">
                  Here are Some More Amazing Features
               </div>
            </div>
            <div class="row">
               <div class="col-12 col-md-4 mt30">
                  <div class="feature-list-box">
                     <img src="https://cdn.oppyo.com/launches/viddeyo/jv/af1.png" class="img-fluid mx-auto d-block icon-position">
                     <p class="description">Auto-play Your Videos in All Browsers</p>
                  </div>
               </div>
               <div class="col-12 col-md-4 mt30">
                  <div class="feature-list-box">
                     <img src="https://cdn.oppyo.com/launches/viddeyo/jv/af2.png" class="img-fluid mx-auto d-block icon-position">
                     <p class="description">Manage Videos in Playlists Effortlessly</p>
                  </div>
               </div>
               <div class="col-12 col-md-4 mt30">
                  <div class="feature-list-box">
                     <img src="https://cdn.oppyo.com/launches/viddeyo/jv/af3.png" class="img-fluid mx-auto d-block icon-position">
                     <p class="description">Video A-B Repeat Functionality To Replay Videos From Specified Duration</p>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-12 col-md-4 mt30">
                  <div class="feature-list-box">
                     <img src="https://cdn.oppyo.com/launches/viddeyo/jv/af4.png" class="img-fluid mx-auto d-block icon-position">
                     <p class="description">Full feature Drag and Drop Editor to Edit Templates</p>
                  </div>
               </div>
               <div class="col-12 col-md-4 mt30">
                  <div class="feature-list-box">
                     <img src="https://cdn.oppyo.com/launches/viddeyo/jv/af5.png" class="img-fluid mx-auto d-block icon-position">
                     <p class="description">100% Mobile Responsive Video Pages and Player</p>
                  </div>
               </div>
               <div class="col-12 col-md-4 mt30">
                  <div class="feature-list-box">
                     <img src="https://cdn.oppyo.com/launches/viddeyo/jv/af6.png" class="img-fluid mx-auto d-block icon-position">
                     <p class="description">Unmatched Video Player Customization</p>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-12 col-md-4 mt30">
                  <div class="feature-list-box">
                     <img src="https://cdn.oppyo.com/launches/viddeyo/jv/af7.png" class="img-fluid mx-auto d-block icon-position">
                     <p class="description">Smooth Playback on All Devices & Browsers</p>
                  </div>
               </div>
               <div class="col-12 col-md-4 mt30">
                  <div class="feature-list-box">
                     <img src="https://cdn.oppyo.com/launches/viddeyo/jv/af8.png" class="img-fluid mx-auto d-block icon-position">
                     <p class="description">Advanced Integration With 1000+ Marketing Apps</p>
                  </div>
               </div>
               <div class="col-12 col-md-4 mt30">
                  <div class="feature-list-box">
                     <img src="https://cdn.oppyo.com/launches/viddeyo/jv/af9.png" class="img-fluid mx-auto d-block icon-position">
                     <p class="description">Advanced Advertisement Technology</p>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-12 col-md-4 mt30">
                  <div class="feature-list-box">
                     <img src="https://cdn.oppyo.com/launches/viddeyo/jv/af10.png" class="img-fluid mx-auto d-block icon-position">
                     <p class="description">Maximize Visitor Engagement with Ad-Free Videos</p>
                  </div>
               </div>
               <div class="col-12 col-md-4 mt30">
                  <div class="feature-list-box">
                     <img src="https://cdn.oppyo.com/launches/viddeyo/jv/af10.png" class="img-fluid mx-auto d-block icon-position">
                     <p class="description">Custom Domain</p>
                  </div>
               </div>
               <div class="col-12 col-md-4 mt30">
                  <div class="feature-list-box">
                     <img src="https://cdn.oppyo.com/launches/viddeyo/jv/af12.png" class="img-fluid mx-auto d-block icon-position">
                     <p class="description">My Drive to Store Your Media Securely & Share Faster with Your Clients</p>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-12 col-md-4 mt30">
                  <div class="feature-list-box">
                     <img src="https://cdn.oppyo.com/launches/viddeyo/jv/af13.png" class="img-fluid mx-auto d-block icon-position">
                     <p class="description">Get More Leads with Premium Lead Generation Templates</p>
                  </div>
               </div>
               <div class="col-12 col-md-4 mt30">
                  <div class="feature-list-box">
                     <img src="https://cdn.oppyo.com/launches/viddeyo/jv/af14.png" class="img-fluid mx-auto d-block icon-position">
                     <p class="description">Stunning Promo Templates for Extra Monetization & Traffic</p>
                  </div>
               </div>
               <div class="col-12 col-md-4 mt30">
                  <div class="feature-list-box">
                     <img src="https://cdn.oppyo.com/launches/viddeyo/jv/af15.png" class="img-fluid mx-auto d-block icon-position">
                     <p class="description">Accounts System to Track Complete Leads Behaviour</p>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-12 col-md-4 mt30">
                  <div class="feature-list-box">
                     <img src="https://cdn.oppyo.com/launches/viddeyo/jv/af16.png" class="img-fluid mx-auto d-block icon-position">
                     <p class="description">Hassle-Free Video Management</p>
                  </div>
               </div>
               <div class="col-12 col-md-4 mt30">
                  <div class="feature-list-box">
                     <img src="https://cdn.oppyo.com/launches/viddeyo/jv/af17.png" class="img-fluid mx-auto d-block icon-position">
                     <p class="description">Comment Management System to Enhance Brand Reputation</p>
                  </div>
               </div>
               <div class="col-12 col-md-4 mt30">
                  <div class="feature-list-box">
                     <img src="https://cdn.oppyo.com/launches/viddeyo/jv/af18.png" class="img-fluid mx-auto d-block icon-position">
                     <p class="description">Fully Newbie friendly</p>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-12 col-md-4 mt30">
                  <div class="feature-list-box">
                     <img src="https://cdn.oppyo.com/launches/viddeyo/jv/af19.png" class="img-fluid mx-auto d-block icon-position">
                     <p class="description">Capture Unlimited Leads from Your Videos</p>
                  </div>
               </div>
               <div class="col-12 col-md-4 mt30">
                  <div class="feature-list-box">
                     <img src="https://cdn.oppyo.com/launches/viddeyo/jv/af20.png" class="img-fluid mx-auto d-block icon-position">
                     <p class="description">A-Z Complete Video Training Included</p>
                  </div>
               </div>
               <div class="col-12 col-md-4 mt30">
                  <div class="feature-list-box">
                     <img src="https://cdn.oppyo.com/launches/viddeyo/jv/af21.png" class="img-fluid mx-auto d-block icon-position">
                     <p class="description">50+ More Cool Features In-Store</p>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- DEMO SECTION START -->
      <div class="demo-sec">
         <div class="container-xxl">
            <div class="row">
               <div class="col-12">
                  <div class="f-md-50 f-28 w600 lh140 text-center">
                     <span class="gradient-clr">Watch The Demo</span>
                     <br class="d-none d-md-block">
                     Discover How Easy & Powerful It Is
                  </div>
               </div>
               <div class="col-12 col-md-8 mx-auto mt-md40 mt20">
                  <img src="https://cdn.oppyo.com/launches/viddeyo/jv/demo-video-poster.png" class="img-fluid d-block mx-auto">
                  <!--<div class="responsive-video">
                     <iframe src="https://Academiyo.dotcompal.com/video/embed/yaa3911t5h" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                     box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                     </div>-->
               </div>
            </div>
         </div>
      </div>
      <!-- DEMO SECTION END -->
      <!-- POTENTIAL SECTION START -->
      <div class="potential-sec">
         <div class="container-xxl">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-md-50 f-28 w600 text-center">
                     Here’s a Never-Ending List of All Marketers and Niches 
                     Who Needs This Amazing Software
                  </div>
               </div>
            </div>
            <div class="row mt-md40 mt20">
               <div class="col-md-3 col-6">
                  <div class="">
                     <img src="https://cdn.oppyo.com/launches/viddeyo/jv/pn2.png" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Info-Product Sellers
                  </div>
               </div>
               <div class="col-md-3 col-6 mt-md0 mt0">
                  <div class="">
                     <img src="https://cdn.oppyo.com/launches/viddeyo/jv/pn1.png" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     E-Com Sellers
                  </div>
               </div>
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="https://cdn.oppyo.com/launches/viddeyo/jv/pn3.png" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Business Coaches
                  </div>
               </div>
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="https://cdn.oppyo.com/launches/viddeyo/jv/pn4.png" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Affiliate Marketers
                  </div>
               </div>
            </div>
            <div class="row mt-md50 mt0">
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="https://cdn.oppyo.com/launches/viddeyo/jv/pn5.png" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Lead Generation Agencies
                  </div>
               </div>
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="https://cdn.oppyo.com/launches/viddeyo/jv/pn6.png" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Digital & SAAS Product Sellers
                  </div>
               </div>
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="https://cdn.oppyo.com/launches/viddeyo/jv/pn7.png" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Local Business Owner
                  </div>
               </div>
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="https://cdn.oppyo.com/launches/viddeyo/jv/pn8.png" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Freelancers
                  </div>
               </div>
            </div>
            <div class="row mt-md50 mt0">
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="https://cdn.oppyo.com/launches/viddeyo/jv/pn9.png" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Agency Owners
                  </div>
               </div>
               <div class="col-12 mt30 mt-md50">
                  <div class="f-18 f-md-20 lh140 w600 black-clr text-center">
                     There are just few examples, but Video Marketing is Present & Future and Every Business Needs this. 
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- POTENTIAL SECTION END -->
      <div class="deep-funnel" id="funnel">
         <div class="container-xxl">
            <div class="row">
               <div class="col-12 f-md-50 f-28 w600 text-center lh140">
                  Our Deep & High Converting Sales Funnel
               </div>
               <div class="col-12 col-md-12 mt20 mt-md70">
                  <div>
                     <img src="https://cdn.oppyo.com/launches/viddeyo/jv/funnel.png" class="img-fluid d-block mx-auto">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="prize-value">
         <div class="container-xxl">
            <div class="row">
               <div class="col-12 f-md-50 f-28 w600 text-center white-clr lh140">
                  Get Ready to Grab Your Share of
               </div>
               <div class="col-12 f-md-72 f-40 w800 text-center gradient-clr lh140">
                  $12000 JV Prizes
               </div>
            </div>
         </div>
      </div>
      <div class="contest-section">
         <div class="container-xxl">
            <div class="row">
               <div class="col-12 f-md-45 f-28 w600 text-center black-clr">
                  Pre-Launch Lead Contest
               </div>
               <div class="col-12 f-md-18 f-18 w400 lh140 mt20 text-center">
                  Contest Runs From 28th May’22, 10:00 AM EST to 2nd June’22 10:00 PM EST
               </div>
               <div class="col-12 f-md-18 f-18 w400 lh140 mt10 text-center">
                  (Get Flat <span class="w700">$0.50c</span> For Every Lead You Send for <span class="w700">Pre-Launch Webinar</span>) 
               </div>
               <div class="col-12 mt20 mt-md80">
                  <div class="row align-items-center">
                     <div class="col-12 col-md-5">
                        <img src="https://cdn.oppyo.com/launches/viddeyo/jv/contest-img1.png" class="img-fluid d-block mx-auto">
                     </div>
                     <div class="col-12 col-md-7">
                        <img src="https://cdn.oppyo.com/launches/viddeyo/jv/contest-img2.png" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
               <div class="col-12 f-18 f-md-18 lh140 mt-md70 mt20 w400 text-center black-clr">
                  *(Eligibility – All leads should have at least 1% conversion on launch and should have min 100 leads) 
               </div>
            </div>
         </div>
      </div>
      <div class="prize-section">
         <div class="container-xxl">
            <div class="row">
               <div class="col-12">
                  <img src="https://cdn.oppyo.com/launches/viddeyo/jv/prize-img1.png" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 mt20 mt-md70">
                  <img src="https://cdn.oppyo.com/launches/viddeyo/jv/prize-img2.png" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 f-18 f-md-20 w600 lh100 mt20 mt-md50 mb10 mb-md20 black-clr">
                  *Contest Policies:
               </div>
               <div class="col-12 f-16 f-md-20 w400 lh140">
                  1. Team of maximum two is allowed. <br> <span class="mt10 d-block">
                  2. To be eligible to win one of the sales leaderboard prizes, you must have made commissions equal to or greater than the value of the prize. If this criterion is not met, then you will be eligible for the next prize. 
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md55">
                  <img src="https://cdn.oppyo.com/launches/viddeyo/jv/prize-img3.png" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 mt20 mt-md30">
                  <div class="f-16 f-md-20 w400 lh140">
                     <span class="w600 d-md-block">Note:</span>
                     We will announce the winners on 9th June & Prizes will be distributed through Payoneer from 10th June Onwards. 
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="reciprocate-sec">
         <div class="container-xxl">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-md-50 f-28 lh140 w600 black-clr">
                     Our Solid Track Record of <br class="d-lg-block d-none"> <span class="gradient-clr"> Launching Top Converting Products</span>
                  </div>
               </div>
               <div class="col-12 col-md-12 mt20">
                  <img src="https://cdn.oppyo.com/launches/viddeyo/jv/product-logo.png" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 text-center f-md-45 f-28 lh140 w700 white-clr mt20 mt-md70 gradient-clr">
                  Do We Reciprocate?
               </div>
               <div class="col-12 f-18 f-md-18 lh140 mt20 w400 text-center black-clr">
                  We've been in top positions on various launch leaderboards & sent huge sales for our valued JVs. <br><br>  
                  So, if you have a top-notch product with top conversions and that fits our list, we would love to drive loads of sales for you. Here's just some results from our recent promotions. 
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
                  <div class="logos-effect">
                     <img src="https://cdn.oppyo.com/launches/viddeyo/jv/logos.png" class="img-fluid d-block mx-auto">
                  </div>
               </div>
               <div class="col-12 f-md-28 f-24 lh140 mt20 mt-md50 w600 text-center">
                  And The List Goes On And On...
               </div>
            </div>
         </div>
      </div>
      <div class="awesome-section">
        <div class="container-xxl">
            <div class="row">
                <div class="col-12">
                    <div class="f-md-50 f-28 w600 lh140 text-center black-clr">
                        Thanks for Checking out Viddeyo
                    </div>
                </div>
            </div>
            <div class="row mt20 mt-md50 ">
                <div class="col-md-4 col-12 text-center mx-auto">
                    <div class="contact-shape">
                        <img src="https://cdn.oppyo.com/launches/viddeyo/special/amit-pareek-sir.webp" class="img-fluid d-block mx-auto" alt="Dr. Amit Pareek">
                        <div class="f-22 f-md-32 w700 mt15 mt-md20 lh140 text-center white-clr">
                            Dr Amit Pareek
                        </div>
                        <div class="f-15 w500 lh140 text-center white-clr">
                            (Techpreneur &amp; Marketer)
                        </div>
                    </div>
                </div>
               
                <div class="col-md-4 col-12 text-center mt20 mt-md0 mx-auto">
                    <div class="contact-shape">
                        <img src="https://cdn.oppyo.com/launches/viddeyo/special/atul-parrek-sir.webp" class="img-fluid d-block mx-auto" alt="Atul Pareek">
                        <div class="f-22 f-md-32 w700 mt15 mt-md20  lh140 text-center white-clr">
                            Atul Pareek
                        </div>
                        <div class="f-15 w500  lh140 text-center white-clr">
                            (Entrepreneur &amp; JV Manager)
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
      <div class="terms-section">
         <div class="container-xxl">
            <div class="row">
               <div class="col-12 f-md-50 f-28 w600 lh140 text-center">
                  Affiliate Promotions Terms & Conditions 
               </div>
               <div class="col-md-12 col-12 f-18 lh140 p-md0 mt-md20 mt20 w400 text-center">
                  We request you to read this section carefully before requesting for your affiliate link and being a part of this launch. Once you are approved to be a part of this launch and promote this product, you must abide by the instructions listed below: 
               </div>
               <div class="col-md-12 col-12 px-md-0 mt15 mt-md50">
                  <ul class="terms-list pl0 m0 f-16 lh140 w400">
                     <li>Please be sure to NOT send spam of any kind. This includes cheap traffic methods in any way whatsoever. In case anyone does so, they will be banned from all future promotion with us without any reason whatsoever. No
                        exceptions will be entertained. 
                     </li>
                     <li>Please ensure you or your members DON’T use negative words such as 'scam' in any of your promotional campaigns in any way. If this comes to our knowledge that you have violated it, your affiliate account will be removed
                        from our system with immediate effect. 
                     </li>
                     <li>Please make it a point to not OFFER any cash incentives (such as rebates), cash backs etc or any such malpractices to people buying through your affiliate link. </li>
                     <li>Please note that you are not AUTHORIZED to create social media pages or run any such campaigns that may be detrimental using our product or brand name in any way possible. Doing this may result in serious consequences.
                     </li>
                     <li>We reserve the right to TERMINATE any affiliate with immediate effect if they’re found to breach any/all of the conditions mentioned above in any manner. </li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!--Footer Section Start -->
      <div class="footer-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 909.75 162.72" style="enable-background:new 0 0 909.75 162.72; max-height:45px;" xml:space="preserve">
                     <style type="text/css">
                        .st00{fill:#FFFFFF;}
                        .st11{fill:url(#SVGID_1_);}
                     </style>
                     <g>
                        <g>
                           <path class="st00" d="M216.23,71.07L201.6,97.26l-35.84-64.75l-3.16-5.7h29.13l0.82,1.49L216.23,71.07z M288.35,26.81l-3.16,5.7
                              l-56.4,103.68h-6.26l-11.71-22.25l14.63-26.18l0.01,0.02l2.73-4.94l4.81-8.68l25.38-45.87l0.82-1.49H288.35z"/>
                           <path class="st00" d="M329.85,26.61v109.57h-23.23V26.61H329.85z"/>
                           <path class="st00" d="M454.65,81.7c0,2.7-0.2,5.41-0.59,8.04c-3.58,24.36-23.15,43.45-47.59,46.42l-0.26,0.03h-50.93v-69.9h23.23
                              v46.67h26.13c12.93-2,23.4-11.91,26.09-24.74c0.45-2.14,0.67-4.33,0.67-6.51c0-2.11-0.21-4.22-0.62-6.26
                              c-1.52-7.54-5.76-14.28-11.95-18.97c-3.8-2.88-8.14-4.84-12.75-5.78c-1.58-0.32-3.19-0.52-4.83-0.6h-45.98V26.82L403.96,27
                              c11.89,0.9,23.21,5.67,32.17,13.61c9.78,8.65,16.15,20.5,17.96,33.36C454.46,76.49,454.65,79.1,454.65,81.7z"/>
                           <path class="st00" d="M572.18,81.7c0,2.7-0.2,5.41-0.59,8.04c-3.58,24.36-23.15,43.45-47.59,46.42l-0.26,0.03h-50.93v-69.9h23.23
                              v46.67h26.13c12.93-2,23.4-11.91,26.09-24.74c0.45-2.14,0.67-4.33,0.67-6.51c0-2.11-0.21-4.22-0.62-6.26
                              c-1.52-7.54-5.76-14.28-11.95-18.97c-3.8-2.88-8.14-4.84-12.75-5.78c-1.58-0.32-3.19-0.52-4.83-0.6h-45.98V26.82L521.5,27
                              c11.89,0.9,23.2,5.67,32.17,13.61c9.78,8.65,16.15,20.5,17.96,33.36C572,76.49,572.18,79.1,572.18,81.7z"/>
                           <path class="st00" d="M688.08,26.82v23.23h-97.72V31.18l0.01-4.36H688.08z M613.59,112.96h74.49v23.23h-97.72v-18.87l0.01-4.36
                              V66.1l23.23,0.14h64.74v23.23h-64.74V112.96z"/>
                           <path class="st00" d="M808.03,26.53l-5.07,6.93l-35.84,48.99l-1.08,1.48L766,83.98l-0.04,0.05l0,0.01l-0.83,1.14v51h-23.23V85.22
                              l-0.86-1.18l-0.01-0.01l-0.04-0.05l-0.04-0.06l-1.08-1.48l-35.84-48.98l-5.07-6.93h28.77l1.31,1.78l24.46,33.43l24.46-33.43
                              l1.31-1.78H808.03z"/>
                           <path class="st00" d="M909.75,81.69c0,30.05-24.45,54.49-54.49,54.49c-30.05,0-54.49-24.45-54.49-54.49S825.2,27.2,855.25,27.2
                              C885.3,27.2,909.75,51.65,909.75,81.69z M855.25,49.95c-17.51,0-31.75,14.24-31.75,31.75s14.24,31.75,31.75,31.75
                              c17.51,0,31.75-14.24,31.75-31.75C887,64.19,872.76,49.95,855.25,49.95z"/>
                        </g>
                     </g>
                     <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="0" y1="81.3586" x2="145.8602" y2="81.3586">
                        <stop  offset="0.4078" style="stop-color:#FFFFFF"/>
                        <stop  offset="1" style="stop-color:#FC6DAB"/>
                     </linearGradient>
                     <path class="st11" d="M11.88,0c41.54,18.27,81.87,39.12,121.9,60.41c7.38,3.78,12.24,11.99,12.07,20.32
                        c0.03,7.94-4.47,15.72-11.32,19.73c-27.14,17.07-54.59,33.74-82.21,50c-0.57,0.39-15.29,8.93-15.47,9.04
                        c-1.4,0.77-2.91,1.46-4.44,1.96c-14.33,4.88-30.25-4.95-32.19-19.99c-0.18-1.2-0.23-2.5-0.24-3.72c0.35-25.84,1.01-53.01,2.04-78.83
                        C2.57,42.41,20.3,32.08,34.79,40.31c8.16,4.93,16.31,9.99,24.41,15.03c2.31,1.49,7.96,4.89,10.09,6.48
                        c5.06,3.94,7.76,10.79,6.81,17.03c-0.68,4.71-3.18,9.07-7.04,11.77c-0.38,0.24-1.25,0.88-1.63,1.07c-3.6,2.01-7.5,4.21-11.11,6.17
                        c-9.5,5.22-19.06,10.37-28.78,15.27c11.29-9.81,22.86-19.2,34.53-28.51c3.54-2.55,4.5-7.53,1.83-10.96c-0.72-0.8-1.5-1.49-2.49-1.9
                        c-0.18-0.08-0.42-0.22-0.6-0.29c-11.27-5.17-22.82-10.58-33.98-15.95c0,0-0.22-0.1-0.22-0.1l-0.09-0.02l-0.18-0.04
                        c-0.21-0.08-0.43-0.14-0.66-0.15c-0.11-0.01-0.2-0.07-0.31-0.04c-0.21,0.03-0.4-0.04-0.6,0.03c-0.1,0.02-0.2,0.02-0.29,0.03
                        c-0.14,0.06-0.28,0.1-0.43,0.12c-0.13,0.06-0.27,0.14-0.42,0.17c-0.09,0.03-0.17,0.11-0.26,0.16c-0.08,0.06-0.19,0.06-0.26,0.15
                        c-0.37,0.25-0.66,0.57-0.96,0.9c-0.1,0.2-0.25,0.37-0.32,0.59c-0.04,0.08-0.1,0.14-0.1,0.23c-0.1,0.31-0.17,0.63-0.15,0.95
                        c-0.06,0.15,0.04,0.35,0.02,0.52c0,0.02,0,0.08-0.01,0.1c0,0,0,0.13,0,0.13l0.02,0.51c0.94,24.14,1.59,49.77,1.94,73.93
                        c0,0,0.06,4.05,0.06,4.05l0,0.12l0.01,0.02l0.01,0.03c0,0.05,0.02,0.11,0.02,0.16c0.08,0.23,0.16,0.29,0.43,0.47
                        c0.31,0.16,0.47,0.18,0.71,0.09c0.06-0.04,0.11-0.06,0.18-0.1c0.02-0.01-0.01,0.01,0.05-0.03l0.22-0.13l0.87-0.52
                        c32.14-19.09,64.83-37.83,97.59-55.81c0,0,0.23-0.13,0.23-0.13c0.26-0.11,0.51-0.26,0.69-0.42c1.15-0.99,0.97-3.11-0.28-4.01
                        c0,0-0.43-0.27-0.43-0.27l-1.7-1.1C84.73,51.81,47.5,27.03,11.88,0L11.88,0z"/>
                  </svg>
                  <div editabletype="text" class="f-16 f-md-16 w300 mt20 lh140 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-16 f-md-16 w300 lh140 white-clr text-xs-center">Copyright © Viddeyo</div>
                  <ul class="footer-ul w300 f-16 f-md-16 white-clr text-center text-md-right">
                     <li><a href="https://support.oppyo.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://viddeyo.co/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://viddeyo.co/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://viddeyo.co/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://viddeyo.co/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://viddeyo.co/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://viddeyo.co/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!--Footer Section End -->
      <!-- timer --->
      <?php
         if ($now < $exp_date) {
         
         ?>
      <script type="text/javascript">
         // Count down milliseconds = server_end - server_now = client_end - client_now
         var server_end = <?php echo $exp_date; ?> * 1000;
         var server_now = <?php echo time(); ?> * 1000;
         var client_now = new Date().getTime();
         var end = server_end - server_now + client_now; // this is the real end time
         
         var noob = $('.countdown').length;
         
         var _second = 1000;
         var _minute = _second * 60;
         var _hour = _minute * 60;
         var _day = _hour * 24
         var timer;
         
         function showRemaining() {
             var now = new Date();
             var distance = end - now;
             if (distance < 0) {
                 clearInterval(timer);
                 document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
                 return;
             }
         
             var days = Math.floor(distance / _day);
             var hours = Math.floor((distance % _day) / _hour);
             var minutes = Math.floor((distance % _hour) / _minute);
             var seconds = Math.floor((distance % _minute) / _second);
             if (days < 10) {
                 days = "0" + days;
             }
             if (hours < 10) {
                 hours = "0" + hours;
             }
             if (minutes < 10) {
                 minutes = "0" + minutes;
             }
             if (seconds < 10) {
                 seconds = "0" + seconds;
             }
             var i;
             var countdown = document.getElementsByClassName('countdown');
             for (i = 0; i < noob; i++) {
                 countdown[i].innerHTML = '';
         
                 if (days) {
                     countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-40 f-md-50 timerbg">' + days + '</span><br><span class="f-20 w600">Days</span> </div>';
                 }
         
                 countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-40 f-md-50 timerbg">' + hours + '</span><br><span class="f-20 w600">Hours</span> </div>';
         
                 countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-40 f-md-50 timerbg">' + minutes + '</span><br><span class="f-20 w600">Mins</span> </div>';
         
                 countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-40 f-md-50 timerbg">' + seconds + '</span><br><span class="f-20 w600">Sec</span> </div>';
             }
         
         }
         timer = setInterval(showRemaining, 1000);
      </script>
      <script type="text/javascript">
         // Special handling for in-app browsers that don't always support new windows
         (function() {
             function browserSupportsNewWindows(userAgent) {
                 var rules = [
                     'FBIOS',
                     'Twitter for iPhone',
                     'WebView',
                     '(iPhone|iPod|iPad)(?!.*Safari\/)',
                     'Android.*(wv|\.0\.0\.0)'
                 ];
                 var pattern = new RegExp('(' + rules.join('|') + ')', 'ig');
                 return !pattern.test(userAgent);
             }
         
             if (!browserSupportsNewWindows(navigator.userAgent || navigator.vendor || window.opera)) {
                 document.getElementById('af-form-905695664').parentElement.removeAttribute('target');
             }
         })();
      </script><script type="text/javascript">
         <!--
         (function() {
             var IE = /*@cc_on!@*/false;
             if (!IE) { return; }
             if (document.compatMode && document.compatMode == 'BackCompat') {
                 if (document.getElementById("af-form-905695664")) {
                     document.getElementById("af-form-905695664").className = 'af-form af-quirksMode';
                 }
                 if (document.getElementById("af-body-905695664")) {
                     document.getElementById("af-body-905695664").className = "af-body inline af-quirksMode";
                 }
                 if (document.getElementById("af-header-905695664")) {
                     document.getElementById("af-header-905695664").className = "af-header af-quirksMode";
                 }
                 if (document.getElementById("af-footer-905695664")) {
                     document.getElementById("af-footer-905695664").className = "af-footer af-quirksMode";
                 }
             }
         })();
         -->
      </script>
      <?php
         } else {
         echo "Times Up";
         }
         ?>
      <!--- timer end-->
      <script id="rendered-js" >
         $('.bxslider.text').bxSlider({
           mode: 'vertical',
           pager: false,
           controls: false,
           infiniteLoop: true,
           auto: true,
           speed: 300,
           pause: 2000 });
                
      </script>
   </body>
</html>
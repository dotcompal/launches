﻿<!-- OPPYO Header Include File -->
<?php include 'includes/deal-header.php'; ?>

<!-- OPPYO Header Include File end -->
<link rel="stylesheet" href="<?php echo $basedir; ?>assets/css/owl.carousel.min.css" />
<link rel="stylesheet" href="<?php echo $basedir; ?>assets/css/carousel-slider.css" />
<link rel="stylesheet" href="<?php echo $basedir; ?>assets/css/slick.css"/>

<!-- welcome Banner Section -->
<!-- Banner Section -->
<section class="deal-page-banner">
	<div class="container container-1170">
		<div class="row">
			<div class="col-12 text-center mt50 mt-lg70">
				<p class="f-20 f-md-30 lh140 w400 pre-title">
					UNLEASH The FULL POWER Of VIDDEYO With OPPYO!
				</p>
				<h1 class="f-30 f-md-36 f-xl-48 firasans-font lh140 w400 title mt15 mt-md25">			
					Get <span class="w600">One-Time ACCESS</span> to ALL 20+ Premium OPPYO Apps For a<span class="w600"> LOW ONE TIME PRICE…</span>
				</h1>
				<p class="f-16 f-md-18 lh180 w400 pre-title mt10 mt-md20">
					
				
				SAVE UPTO $16,000 EVERY YEAR  <span class="px5">|</span> GET UNLIMITED GROWTH  <span class="px5">|</span> NO COMPLEX INTEGRATIONS
				</p>
				
				<!--<img src="https://cdn.oppyo.com/launches/viddeyo/premium-membership/deal-page/banner-image.png" class="img-fluid d-bloc mx-auto mt20 mt-md30 mt-xl50" alt="Banner">-->
				
				
			</div>
			
			<div class="col-12 col-lg-10 mx-auto mt-md35 mt20">		
			<img src="https://cdn.oppyo.com/launches/viddeyo/premium-membership/product.png" class="img-fluid d-bloc mx-auto mt-md35 mt20" alt="Banner">			
					<!--<div class="embed-responsive embed-responsive-16by9 video-border-color">
						<iframe _ngcontent-serverapp-c1="" allowfullscreen="" class="embed-responsive-item" id="preview-player" scrolling="no" src="https://sellerspal.dotcompal.com/video/embed/wbkbrs68rn"></iframe>
					</div>-->
			</div>
			<div class="col-12 f-md-25 f-16 lh160 w400 firasans-font mt20 mt-md30 mt-xl50 text-center">This Customer’s Only Special Deal Is Only Available For A Limited Time <br class="d-block d-md-none d-xl-block">So Don’t Miss It… </div>
			
		</div>		
	</div>
	
</section>
<!-- Banner section end-->
<!-- OPPYO Old Way section -->	
<section class="old-way-section">
	<div class="container container-1170">
		<!-- Heading -->
		<div class="row">
			<div class="col-12 text-center">
				<h1 class="home-page-heading">
					The “Old Way” Of Building Your Online Business
				</h1>
			</div>
		</div>
		<div class="row no-gutters">
			<div class="col-12 col-lg-10 ml-auto pl-md35 pl15">
				<div class="mt20 mt-md30 mt-lg55 old-way-block">
					<div class="d-flex align-items-md-center">
						<div class="website-help-cion">
							<i class="icon-questionmark"></i>
						</div>
						<div class="ml15 ml-md25">
							<h4 class="title">Must purchase multiple apps and spend time managing them </h4>
							<p class="description">Most business owners use 10+ software & services to run their online business. The result is a complicated (and expensive) mess that waste a lot of time and decreases your effectiveness.</p>
						</div>
					</div>
					<div class="d-flex align-items-md-center mt15 mt-md30">
						<div class="website-help-cion">
							<i class="icon-questionmark"></i>
						</div>
						<div class="ml15 ml-md25">
							<h4 class="title">Information overload & complex technical stuff  </h4>
							<p class="description">You need to learn using multiple apps resulting in information overload and confusion. You are also expected to have technical knowledge to use these apps.</p>
						</div>
					</div>
					<div class="d-flex align-items-md-center mt15 mt-md30">
						<div class="website-help-cion">
							<i class="icon-questionmark"></i>
						</div>
						<div class="ml15 ml-md25">
							<h4 class="title">Apps working in silos and difficult to connect one with other</h4>
							<p class="description">Is your online business duct-taped together with different software? Connecting all these apps is difficult and you lose important data, intelligence and output that slows your growth.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row mt15 mt-md30 mt-lg60">
			<div class="col-12 text-center">
				<h3 class="home-page-subtitle">What if you could transform your business with a single app?</h3>
				<p class="home-page-title mt15 mt-md35">
				Never waste your time, money, and energy again dealing with multiple apps. Invest that time and energy to do more business and <br class="d-none d-xl-block">bring more growth from a single platform.
				</p>
			</div>
		</div>
	</div>	
</section>	
<!-- OPPYO Old Way section end -->
<!-- Say Hello to OPPYO section -->	
<section class="say-hello-section">
	<div class="container container-1170">
		<div class="row">
			<!-- Heading -->
			<div class="col-12 text-center">
				<h1 class="home-page-heading">
					Say Hello to OPPYO
				</h1>
				<p class="home-page-sub-heading mt10 mt-md25">OPPYO is beyond just building funnels & landing pages, websites and sending emails. With OPPYO, everything lives<br class="d-none d-xl-block"> in one place so you can get started quick and build more, sell more, and grow more online.</p>
			</div>
			<div class="col-12 col-md-10 mx-auto mt25 mt-md30 mt-lg60">
				<img src="https://cdn.oppyo.com/launches/viddeyo/premium-membership/features-img.webp" class="img-fluid d-block mx-auto" alt="Feature">
			</div>
		</div>
	</div>	
</section>	
<!-- Say Hello to OPPYO section end -->	
<!-- With OPPYO section -->
<section class="white-blue-bg">
	<div class="container container-1170">
		<div class="row no-gutters">
			<div class="col-12">
				<div class="with-dotcompal-block">
					<div class="row">
						<!-- Heading -->
						<div class="col-12 text-center">
							<h1 class="home-page-heading">
								With OPPYO, Online Business Is Redefined 
							</h1>
						</div>
					</div>
					<div class="row">
						<div class="col-12 col-md-4 mt20 mt-md30 mt-lg45 pr-xl40">
							<div class="d-flex">
								<span class="mr10 mr15">
									<img src="https://cdn.oppyo.com/launches/viddeyo/premium-membership/save-more.png" class="img-fluid w-100 min-w-24" alt="Save More">
								</span>
								<div>
									<p class="f-15 f-md-16 p-blue-clr w500 text-capitalize mt3">SAVE TIME & EFFORT</p>
									<h4 class="title mt10 mt-md12">Everything you need,<br class="d-none d-xl-block"> all in one place.</h4>
									<p class="description mt10 mt-md15">OPPYO’s all-in-one integrated platform helps you start, manage, and grow your entire online business from one place. Save your time, money and energy and do more business online.</p>
								</div>
							</div>
						</div>
						<div class="col-12 col-md-4 mt20 mt-md30 mt-lg45 pl-xl50">
							<div class="d-flex">
								<span class="mr10 mr15">
									<img src="https://cdn.oppyo.com/launches/viddeyo/premium-membership/bring-more.png" class="img-fluid w-100 min-w-20" alt="Bring More">
								</span>
								<div>
									<p class="f-15 f-md-16 p-blue-clr w500 text-capitalize mt3">BRING MORE GROWTH</p>
									<h4 class="title mt10 mt-md12">At Every Stage Of Customer Lifecycle</h4>
									<p class="description mt10 mt-md15">OPPYO brings you <span class="w700">fortune 500 companies’ growth strategy and technology in easy way</span> to get more conversions, more sales, and more growth.</p>
								</div>
							</div>
						</div>
						<div class="col-12 col-md-4 mt20 mt-md30 mt-lg45 pl-xl40">
							<div class="d-flex">
								<span class="mr10 mr15">
									<img src="https://cdn.oppyo.com/launches/viddeyo/premium-membership/start.png" class="img-fluid w-100 min-w-23" alt="Start">
								</span>
								<div>
									<p class="f-15 f-md-16 p-blue-clr w500 text-capitalize mt3">START - QUICK & EASY</p>
									<h4 class="title mt10 mt-md12">Easy Platform with <br class="d-none d-xl-block"> Top-Notch Support</h4>
									<p class="description mt10 mt-md15">OPPYO is made for business owners like yourself and very easy to use. Also, our 24*7 support & FREE training will help you get started quick & easy.</p>
								</div>
							</div>
						</div>
					</div>
					
					<div class="row">
						<div class="col-12 col-md-7 col-xl-5 mx-auto mt20 mt-md30 mt-xl50 text-center">
							<!-- Button -->
							<a href="#buynow" class="base-btn m-blue-btn btn-block dcp-btn-get-start px-xl30">Upgrade to OPPYO PRO One-Time</a>
						</div>
						<div class="col-12 text-center mt10 mt-md15">
							<p class="home-page-description">This customer only deal is available for very limited time. Grab this one-time founder special offer.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>		
<!-- With OPPYO section End -->
<!-- In just 6 months section -->
<section class="online-entrepreneurs-bg">
	<div class="container container-1170">
		<div class="row text-center">
			<div class="col-12">
				<h2 class="home-page-title-white">In just 6 months, OPPYO has enabled 16,500+ Entrepreneurs to…</h2>
			</div>
			
			<div class="col-12 col-md-6 col-xl-3 mt20 mt-md30 mt-lg55">
				<div class="business-blocks">
					<h6 class="pre-heading">Build</h6>
					<h1 class="heading yellow">28,500</h1>
					<p class="description mt10 mt-md18">Websites, pages & <br> membership sites</p>
				</div>
			</div>
			
			<div class="col-12 col-md-6 col-xl-3 mt20 mt-md30 mt-lg55">
				<div class="business-blocks">
					<h6 class="pre-heading">Serve</h6>
					<h1 class="heading cerise">74 Million</h1>
					<p class="description mt10 mt-md18">Website & marketing <br> campaigns visits</p>
				</div>
			</div>
			
			<div class="col-12 col-md-6 col-xl-3 mt20 mt-md30 mt-lg55">
				<div class="business-blocks">
					<h6 class="pre-heading">Play</h6>
					<h1 class="heading java">33 Million</h1>
					<p class="description mt10 mt-md18">Minutes of fast videos on <br> their sites and pages</p>
				</div>
			</div>
			
			<div class="col-12 col-md-6 col-xl-3 mt20 mt-md30 mt-lg55">
				<div class="business-blocks">
					<h6 class="pre-heading">Generate</h6>
					<h1 class="heading blue">150K+</h1>
					<p class="description mt10 mt-md18"><span class="w700">Conversions</span> from 3Mn <br> landing pages views</p>
				</div>
			</div>
			<div class="col-12 mt25 mt-md30 mt-lg60">
					<p class="white-clr f-16 f-md-20 lh160"><span class="w700">OPPYO is growing exponentially like charm.</span> Business owners are loving it because OPPYO enables them with all the  <br class="d-none d-xl-block">tools, proven growth strategies, and support needed in a single platform as a Pal to simplify their online business.
					<br>
					<br>
					<span class="w700">We are on mission to empower 100K businesses in 2021 and become NO. 1 online business <br class="d-none d-xl-block">
					platform for entrepreneurs like yourself.</span></p>
			</div>
		</div>
	</div>
</section>		
<!-- In just 6 months section End -->
<!-- More Business section -->
<section class="more-business-section">
	<div class="container container-1170">
		<!-- Heading -->
		<div class="row" id="buildonlinepresence">
			<div class="col-12 text-center">
			<div id="buildonlinepresence1" class="d-block d-md-none"></div>
				<h2 class="home-page-title-grey">Start online and Grow your online business</h2>
				<h1 class="home-page-heading mt-md10 mt8">
					Everything You Need, All In One Place
				</h1>				
			</div>
		</div>
		
		<div class="row align-items-center mt20 mt-md30 mt-lg100">
			<div class="col-12 col-md-6 pr-lg55">
				<h3 class="f-18 f-md-25 w500 p-blue-clr text-uppercase">BUILD</h3>
				<h2 class="featurs-heading mt10 mt-md15">Website & Membership Sites</h2>
				<p class="home-page-title mt10 mt-md20">
					<span class="w700">Build your online presence</span> by creating your marketing website with domain and free hosting
				</p>
				<div class="d-flex mt20 mt-md30">
					<span class="mr10 mr-md20">
						<i class="icon-businesswebsite feature-icon"></i>
					</span>
					<div>
						<p class="home-page-title">
							Create a professional business <span class="w700">Website</span>
						</p>
					</div>
				</div>
				<div class="d-flex mt15 mt-md20">
					<span class="mr10 mr-md20">
						<i class="icon-freehosting feature-icon"></i>
					</span>
					<div>
						<p class="home-page-title">
							Free OPPYO <span class="w700">Domain & Hosting.</span> Also connect your own custom domain
						</p>
					</div>
				</div>
				<div class="d-flex mt15 mt-md20">
					<span class="mr10 mr-md20">
						<i class="icon-customsubdomain feature-icon"></i>
					</span>
					<div>
						<p class="home-page-title">
							<span class="w700">Build membership sites</span> (multi-level deep) to deliver products and courses.
						</p>
					</div>
				</div>
				<div class="d-flex mt15 mt-md20">
					<span class="mr10 mr-md20">
						<i class="icon-friendlytemplates feature-icon"></i>
					</span>
					<div>
						<p class="home-page-title">
							<span class="w700">300+ mobile-ready templates</span> with drag and drop editor.
						</p>
					</div>
				</div>
				<div class="d-flex mt15 mt-md20">
					<span class="mr10 mr-md20">
						<i class="icon-buildmultilevel feature-icon"></i>
					</span>
					<div>
						<p class="home-page-title">
							<span class="w700">Quick-Start</span> with a free logo & migration assistance from Team OPPYO
						</p>
					</div>
				</div>
				
				<a id="sellproducts" class="base-btn blue-btn feature-signup-btn mt20 mt-md40" title="Upgrade to OPPYO Pro One-Time" href="#buynow">
					Upgrade to OPPYO Pro One-Time
				</a>
			</div>
			<div class="col-12 col-md-6 mt20 mt-md0 pl-md0">
				<img src="https://cdn.oppyo.com/launches/viddeyo/premium-membership/build-img.gif" class="img-fluid d-block mx-auto" alt="Build">
			</div>
		</div>
		<div class="row align-items-center mt20 mt-md30 mt-lg100">
			<div class="col-12 col-md-6 col-lg-5 offset-lg-1 order-md-2">
				<h3 class="f-18 f-md-25 w500 p-blue-clr text-uppercase">SELL</h3>
				<h2 class="featurs-heading mt10 mt-md15">Products & Accept Payments</h2>
				<p class="home-page-title mt10 mt-md20">
					<span class="w700">Sell products, services, and courses online.</span> Deliver in beautiful membership sites.
				</p>
				<div class="d-flex mt20 mt-md30">
					<span class="mr10 mr-md20">
						<i class="icon-yourproducts feature-icon"></i>
					</span>
					<div>
						<p class="home-page-title">
							<span class="w700">Sell products</span>, courses, memberships, services, or physical goods
						</p>
					</div>
				</div>
				<div class="d-flex mt15 mt-md20">
					<span class="mr10 mr-md20">
						<i class="icon-sellunlimited feature-icon"></i>
					</span>
					<div>
						<p class="home-page-title">
							<span class="w700">Accept payments</span> worldwide using PayPal & Stripe. 
						</p>
					</div>
				</div>
				<div class="d-flex mt15 mt-md20">
					<span class="mr10 mr-md20">
						<i class="icon-manageorders feature-icon"></i>
					</span>
					<div>
						<p class="home-page-title">
							<span class="w700">Sell unlimited. ZERO OPPYO fee</span> (limited offer)
						</p>
					</div>
				</div>
				<div class="d-flex mt15 mt-md20">
					<span class="mr10 mr-md20">
						<i class="icon-feeonsales feature-icon"></i>
					</span>
					<div>
						<p class="home-page-title">
							<span class="w700">Deliver securely</span> with Memberships & MyDrive
						</p>
					</div>
				</div>
				<div class="d-flex mt15 mt-md20">
					<span class="mr10 mr-md20">
						<i class="icon-sellmodule feature-icon"></i>
					</span>
					<div>
						<p class="home-page-title">
							<span class="w700">Boost Sales</span> - Use upsells, cross-sell, discount coupons or create offers
						</p>
					</div>
				</div>
				
				<a id="growyourbusiness" class="base-btn blue-btn feature-signup-btn mt20 mt-md40" title="Upgrade to OPPYO Pro One-Time" href="#buynow">
					Upgrade to OPPYO Pro One-Time
				</a>
			</div>
			<div class="col-12 col-md-6 col-lg-6 order-md-1 mt20 mt-md0 pl-md0">
				<img src="https://cdn.oppyo.com/launches/viddeyo/premium-membership/sell-img.png" class="img-fluid d-block mx-auto" alt="Sell">
				
			</div>
			<div id="growyourbusiness1" class="d-block d-md-none"></div>
		</div>
		<div class="row align-items-center mt20 mt-md30 mt-lg100">
			<div class="col-12 col-md-6 pr-lg55">
				<h3 class="f-18 f-md-25 w500 p-blue-clr text-uppercase">Grow</h3>
				<h2 class="featurs-heading mt10 mt-md15">All-In-One Integrated Marketing Solution</h2>
				<p class="home-page-title mt10 mt-md20">
					Attract, engage, and convert customers and <br class="d-none d-xl-block"><span class="w700">grow your business online.</span>
				</p>
				<div class="d-flex mt20 mt-md30">
					<span class="mr10 mr-md20">
						<i class="icon-landingpages1 feature-icon"></i>
					</span>
					<div>
						<p class="home-page-title">
							High converting <span class="w700">Landing pages</span>
						</p>
					</div>
				</div>
				<div class="d-flex mt15 mt-md20">
					<span class="mr10 mr-md20">
						<i class="icon-video-help feature-icon"></i>
					</span>
					<div>
						<p class="home-page-title">
							Fast <span class="w700">Video hosting and Player</span>
						</p>
					</div>
				</div>
				<div class="d-flex mt15 mt-md20">
					<span class="mr10 mr-md20">
						<i class=" icon-salesfunnels feature-icon"></i>
					</span>
					<div>
						<p class="home-page-title">
							<span class="w700">Funnels</span>/Customer Journey
						</p>
					</div>
				</div>
				<div class="d-flex mt15 mt-md20">
					<span class="mr10 mr-md20">
						<i class="icon-sendemails1 feature-icon"></i>
					</span>
					<div>
						<p class="home-page-title">
							<span class="w700">Send Emails</span>
						</p>
					</div>
				</div>
				<div class="d-flex mt15 mt-md20">
					<span class="mr10 mr-md20">
						<i class="icon-dynamicpopups feature-icon"></i>
					</span>
					<div>
						<p class="home-page-title">
							Dynamic <span class="w700">popups, bars, and notification boxes</span>
						</p>
					</div>
				</div>
				<div class="d-flex mt15 mt-md20">
					<span class="mr10 mr-md20">
						<i class="icon-salesfunnels feature-icon"></i>
					</span>
					<div>
						<p class="home-page-title">
							<span class="w700">A/B test</span> emails, pages, and popups
						</p>
					</div>
				</div>
				<div class="d-flex mt15 mt-md20">
					<span class="mr10 mr-md20">
						<i class="icon-deepanalytics feature-icon"></i>
					</span>
					<div>
						<p class="home-page-title">
							<span class="w700">Deep Analytics</span>
						</p>
					</div>
				</div>
				
				<a id="manageeverything" class="base-btn blue-btn feature-signup-btn mt20 mt-md40" title="Upgrade to OPPYO Pro One-Time" href="#buynow">
					Upgrade to OPPYO Pro One-Time
				</a>
			</div>
			<div class="col-12 col-md-6 mt20 mt-md0 pl-md0">
				<img src="https://cdn.oppyo.com/launches/viddeyo/premium-membership/grow-img.gif" class="img-fluid d-block mx-auto" alt="Grow">
			</div>
			<div id="manageeverything1" class="d-block d-md-none"></div>
		</div>
		
		<div class="row align-items-center mt20 mt-md30 mt-lg100" >
			<div class="col-12 col-md-6 col-lg-5 offset-lg-1 order-md-2">
				<h3 class="f-18 f-md-25 w500 p-blue-clr text-uppercase">MANAGE</h3>
				<h2 class="featurs-heading mt10 mt-md15">Audience, Team, Integrations & More</h2>
				<p class="home-page-title mt10 mt-md20">
					Manage everything – Leads, customers, businesses, team, integrations & more
				</p>
				<div class="d-flex mt15 mt-md30">
					<span class="mr10 mr-md20">
						<i class="icon-addmanagecontacts feature-icon"></i>
					</span>
					<div>
						<p class="home-page-title">
							<span class="w700">Manage Unlimited Contacts</span> (Leads & Customers)
						</p>
					</div>
				</div>
				<div class="d-flex mt20 mt-md30">
					<span class="mr10 mr-md20">
						<i class="icon-smartaudience feature-icon"></i>
					</span>
					<div>
						<p class="home-page-title">
							Smart Audience <span class="w700">Segments </span>
						</p>
					</div>
				</div>
				<div class="d-flex mt15 mt-md20">
					<span class="mr10 mr-md20">
						<i class="icon-advancedleadmanagement feature-icon"></i>
					</span>
					<div>
						<p class="home-page-title">
							<span class="w700">Easy lead management</span> – with lists, tags & lead scoring
						</p>
					</div>
				</div>
				<div class="d-flex mt15 mt-md20">
					<span class="mr10 mr-md20">
						<i class="icon-managemediacontent feature-icon"></i>
					</span>
					<div>
						<p class="home-page-title">
							<span class="w700">Manage Media Content</span> (store, share and deliver) with MyDrive
						</p>
					</div>
				</div>
				<div class="d-flex mt15 mt-md20">
					<span class="mr10 mr-md20">
						<i class="icon-integrationsandourapi feature-icon"></i>
					</span>
					<div>
						<p class="home-page-title">
							Connect with all major Apps <span class="w700">with 40+ Integrations & </span> OPPYO API
						</p>
					</div>
				</div>
				<div class="d-flex mt15 mt-md20">
					<span class="mr10 mr-md20">
						<i class="icon-singledashboard feature-icon"></i>
					</span>
					<div>
						<p class="home-page-title">
							Manage all your <span class="w700">businesses and team</span> easily in single dashboard
						</p>
					</div>
				</div>
			</div>
			<div class="col-12 col-md-6 col-lg-6 order-md-1 mt20 mt-md0 pl-md0">
				<img src="https://cdn.oppyo.com/launches/viddeyo/premium-membership/manage-img.png" class="img-fluid d-block mx-auto" alt="Manage-Img">
			</div>
		</div>
		<div class="row text-center">
			<div class="col-12 col-md-7 col-xl-5 px-xl30 mx-auto mt-md30 mt-xl90">
				<!-- Button -->
				<a href="#buynow" class="base-btn m-blue-btn btn-block dcp-btn-get-start">Upgrade to OPPYO PRO One-Time</a>
			</div>
			<div class="col-12 mt10 mt-md15">
				<p class="home-page-description">This customer only deal is available for very limited time. Grab this one-time founder special offer.</p>
			</div>
		</div>
	</div>
</section>		
<!-- More Business section End -->
<!-- FEATURE SECTION -->
<section class="feature-section benefits-section" id="bringmoregrowth">
<div class="container container-1170">
	<!-- Heading -->
	<div class="row">
		<div class="col-12 text-center">
			<h2 class="home-page-title-grey">Our growth strategy & all-in-one integrated marketing apps </h2>
			<h1 class="home-page-heading mt-md10 mt8">
				Bring More Growth At Every Customer Touchpoint
			</h1>
		</div>
	</div>
	<!-- Desktop View -->
	<div class="col-12 p0 d-none d-md-block">
		<!-- Steps Carousel -->
		<div id="step-carousel-slider" class="owl-carousel owl-theme mt15 mt-md30 mt-lg55 px-xl55">
			<!--1. Steps Heading -->
			<div class="item">
				<span class="step-shape f-50 f-md-24 f-lg-50"><i class="icon-moreengagement"></i></span>
				<h5>More Engagement</h5>
				<span class="current-step"><span></span><span></span><span></span><span></span><span></span></span>
			</div>
			<!--2. Steps Heading -->
			<div class="item">
				<span class="step-shape"><i class="icon-moreconversions f-40 f-md-22 f-lg-40"></i></span>
				<h5>More Leads</h5>
				<span class="current-step"><span></span><span></span><span></span><span></span><span></span></span>
			</div>
			<!--3. Steps Heading -->
			<div class="item">
				<span class="step-shape"><i class="icon-moreinvestmentback"></i></span>
				<h5>More Engaging Follow-ups</h5>
				<span class="current-step"><span></span><span></span><span></span><span></span><span></span></span>
			</div>
			<!--4. Steps Heading -->
			<div class="item">
				<span class="step-shape"><i class="icon-moresales"></i></span>
				<h5>More Sales & Growth</h5>
				<span class="current-step"><span></span><span></span><span></span><span></span><span></span></span>
			</div>
			<!--5. Steps Heading -->
			<div class="item">
				<span class="step-shape"><i class="icon-moregrowth"></i></span>
				<h5>More Happy Customers</h5>
				<span class="current-step"><span></span><span></span><span></span><span></span><span></span></span>
			</div>
		</div>
		<!-- Carousel Slider -->
		<div id="carousel-steps-container" class="owl-carousel owl-theme mt15 mt-md30 mt-lg70">
			<!--1. Steps -->
			<div class="item">
				<div class="row">
					<div class="col-12 col-md-7 col-xl-6 text-center text-md-left">
						<div class="d-flex justify-content-center justify-content-md-start">
							<span class="step-heading-shape"></span>
							<h3 class="feature-title">Customer engagement at scale </h3>
						</div>
						<h1 class="feature-heading">Attract & Engage More Visitors</h1>
						<p class="home-page-title mt10 mt-md15">
							Set your online business up for success by engaging your visitors with relevant messages and offers using OPPYO Popups, Bars and Notification Boxes. It enables you to present customized offers according to behaviour, timer-based deals, lead forms and many more options to engage more visitors on your website and pages. <br><br>
							Use OPPYO Fast loading Videos & player (sales, demos, and testimonials videos) to ensure that your visitors see and hear your message loud and clear, preventing bouncing.<br><br>
							Furthermore, send follow up emails to your leads based on their activity on your website. 
						</p>
					</div>
					<div class="col-12 col-md-5 col-xl-5 ml-lg-auto mt15 mt-md0 d-none d-md-block">
						<img src="https://cdn.oppyo.com/launches/viddeyo/premium-membership/step1.png" class="img-fluid d-block mx-auto step-image" alt="Step1">
					</div>
				</div>
				<div class="row mt20 mt-md30 mt-lg55">
					<!-- Button -->
					<div class="col-12 col-md-6 col-xl-5 px-xl25 mx-auto text-center">
						<a href="#buynow" class="base-btn m-blue-btn btn-block dcp-btn-get-start">Upgrade to OPPYO PRO One-Time</a>
					</div>
					<div class="col-12 text-center mt10 mt-md15">
						<p class="home-page-description">This customer only deal is available for very limited time. Grab this one-time founder special offer.</p>
					</div>
				</div>
			</div>
			<!--2. Steps -->
			<div class="item">
				<div class="row">
					<div class="col-12 col-md-7 col-xl-6 text-center text-md-left">
						<div class="d-flex justify-content-center justify-content-md-start">
							<span class="step-heading-shape"></span>
							<h3 class="feature-title">Convert more traffic</h3>
						</div>
						<h1 class="feature-heading">Capture More Targeted Leads</h1>
						<p class="home-page-title mt10 mt-md15">
							OPPYO helps you capture more targeted leads from your paid and organic traffic around the clock. Create simple lead funnels to generate new leads, follow up with them even after they leave your page or website!<br><br>
							Use OPPYO high converting lead pages and website pop-ups and sticky bars with inbuilt lead forms to capture more leads, register more people for your webinars, increase subscribers to your blogs/newsletters, etc. and it help you convert them while they are hot!
						</p>
					</div>
					<div class="col-12 col-md-5 col-xl-5 ml-lg-auto mt15 mt-md0 d-none d-md-block">
						<img src="https://cdn.oppyo.com/launches/viddeyo/premium-membership/step2.png" class="img-fluid d-block mx-auto step-image" alt="Step2">
					</div>
				</div>
				<div class="row mt20 mt-md30 mt-lg55">
					<!-- Button -->
					<div class="col-12 col-md-6 col-xl-5 px-xl25 mx-auto text-center">
						<a href="#buynow" class="base-btn m-blue-btn btn-block dcp-btn-get-start">Upgrade to OPPYO PRO One-Time</a>
					</div>
					<div class="col-12 text-center mt10 mt-md15">
						<p class="home-page-description">This customer only deal is available for very limited time. Grab this one-time founder special offer.</p>
					</div>
				</div>
			</div>
			<!--3. Steps -->
			<div class="item">
				<div class="row">
					<div class="col-12 col-md-7 col-xl-6 text-center text-md-left">
						<div class="d-flex justify-content-center justify-content-md-start">
							<span class="step-heading-shape"></span>
							<h3 class="feature-title">Nurture & close prospects</h3>
						</div>
						<h1 class="feature-heading">Send Better Follow-up Emails</h1>
						<p class="home-page-title mt10 mt-md15">
							Harness the power of trigger-based follow up email sequence to nurture and activate your leads and convert them into paying customers. <br><br>
							OPPYO Mail enables you to send special offers, loyalty bonuses, training material, updates, newsletters, etc. to your leads based on their interaction with your website, landing pages, and email communication. And keep them coming back for more!
							Also get access to customizable email automation workflow templates with OPPYO. 
						</p>
					</div>
					<div class="col-12 col-md-5 col-xl-5 ml-lg-auto mt15 mt-md0 d-none d-md-block">
						<img src="https://cdn.oppyo.com/launches/viddeyo/premium-membership/step4.png" class="img-fluid d-block mx-auto step-image" alt="Step4">
					</div>
				</div>
				<div class="row mt20 mt-md30 mt-lg55">
					<!-- Button -->
					<div class="col-12 col-md-6 col-xl-5 px-xl25 mx-auto text-center">
						<a href="#buynow" class="base-btn m-blue-btn btn-block dcp-btn-get-start">Upgrade to OPPYO PRO One-Time</a>
					</div>
					<div class="col-12 text-center mt10 mt-md15">
						<p class="home-page-description">This customer only deal is available for very limited time. Grab this one-time founder special offer.</p>
					</div>
				</div>
			</div>
			<!--4. Steps -->
			<div class="item">
				<div class="row">
					<div class="col-12 col-md-7 col-xl-6 text-center text-md-left">
						<div class="d-flex justify-content-center justify-content-md-start">
							<span class="step-heading-shape"></span>
							<h3 class="feature-title">Boost your sales </h3>
						</div>
						<h1 class="feature-heading">Sell More. More Quickly</h1>
						<p class="home-page-title mt10 mt-md15">
							Move more deals through your marketing funnel, using personalized customer journeys based on user data and behaviour.<br><br>
							With OPPYO, create high converting offer pages or complete high converting sales funnels with upsells and down-sells that are designed to help you convert traffic through each step of the sales process and convert them from “visitors” to “buyers” faster.
						</p>
					</div>
					<div class="col-12 col-md-5 col-xl-5 ml-lg-auto mt15 mt-md0 d-none d-md-block mt5 mt-md0">
						<img src="https://cdn.oppyo.com/launches/viddeyo/premium-membership/step3.png" class="img-fluid d-block mx-auto step-image" alt="Step3">
					</div>
				</div>
				<div class="row mt20 mt-md30 mt-lg55">
					<!-- Button -->
					<div class="col-12 col-md-6 col-xl-5 px-xl25 mx-auto text-center">
						<a href="#buynow" class="base-btn m-blue-btn btn-block dcp-btn-get-start">Upgrade to OPPYO PRO One-Time</a>
					</div>
					<div class="col-12 text-center mt10 mt-md15">
						<p class="home-page-description">This customer only deal is available for very limited time. Grab this one-time founder special offer.</p>
					</div>
				</div>
			</div>
			<!--5. Steps -->
			<div class="item">
				<div class="row">
					<div class="col-12 col-md-7 col-xl-6 text-center text-md-left">
						<div class="d-flex justify-content-center justify-content-md-start">
							<span class="step-heading-shape"></span>
							<h3 class="feature-title">Delight your customers</h3>
						</div>
						<h1 class="feature-heading">From Customers to Your Brand Advocates</h1>
						<p class="home-page-title mt10 mt-md15">
							Convert your valued customers to brand advocates by delighting them with after-sales services delivered using OPPYO like – fast loading training videos, deliver their digital products inside membership sites, secured documents & files, etc. <br><br>
							Testimonial videos, social proof videos, case studies, etc can further help you increase your credibility with prospects and customers alike, as well as help you get word of mouth publicity. 
						</p>
					</div>
					<div class="col-12 col-md-5 col-xl-5 ml-lg-auto mt15 mt-md0 d-none d-md-block">
						<img src="https://cdn.oppyo.com/launches/viddeyo/premium-membership/step5.png" class="img-fluid d-block mx-auto step-image" alt="Step5">
					</div>
				</div>
				<div class="row mt20 mt-md30 mt-lg55">
					<!-- Button -->
					<div class="col-12 col-md-6 col-xl-5 px-xl25 mx-auto text-center">
						<a href="#buynow" class="base-btn m-blue-btn btn-block dcp-btn-get-start">Upgrade to OPPYO PRO One-Time</a>
					</div>
					<div class="col-12 text-center mt10 mt-md15">
						<p class="home-page-description">This customer only deal is available for very limited time. Grab this one-time founder special offer.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Mobile View -->
	<div class="col-12 p0 d-bolck d-md-none mt20 mt-md0">
	<div class="slider-nav">
		<div>
			<span class="xs-step-shape"><i class="icon-moreengagement"></i></span>
			<h5>More Engagement</h5>
		</div>
		<div>
			<span class="xs-step-shape"><i class="icon-moreconversions"></i></span>
			<h5>More Leads</h5>
		</div>
		<div>
			<span class="xs-step-shape"><i class="icon-moreinvestmentback"></i></span>
			<h5>More Engaging Follow-ups</h5>
		</div>
		<div>
			<span class="xs-step-shape"><i class="icon-moresales"></i></span>
			<h5>More Sales & Growth</h5>
		</div>
		<div>
			<span class="xs-step-shape"><i class="icon-moregrowth"></i></span>
			<h5>More Happy Customers</h5>
		</div>
	</div>
	<div class="slider-for mt20">
		<div>
			<div class="row">
				<div class="col-12 text-center p0">
					<h3 class="feature-title">Customer engagement at scale </h3>
					<h1 class="feature-heading">Attract & Engage More Visitors</h1>
					<p class="home-page-title mt10">
					Set your online business up for success by engaging your visitors with relevant messages and offers using OPPYO Popups, Bars and Notification Boxes. It enables you to present customized offers according to behaviour, timer-based deals, lead forms and many more options to engage more visitors on your website and pages. <br><br>
					Use OPPYO Fast loading Videos & player (sales, demos, and testimonials videos) to ensure that your visitors see and hear your message loud and clear, preventing bouncing.<br><br>
					Furthermore, send follow up emails to your leads based on their activity on your website. 
					</p>
					<img src="https://cdn.oppyo.com/launches/viddeyo/premium-membership/slider/step1.png" class="img-fluid d-block mx-auto step-image mt15" alt="Step1">
				</div>
			</div>
			<div class="row mt20">
				<!-- Button -->
				<div class="col-12 mx-auto text-center">
					<a href="#buynow" class="base-btn m-blue-btn btn-block dcp-btn-get-start">Upgrade to OPPYO PRO One-Time</a>
				</div>
				<div class="col-12 p0 text-center mt10 mt-md15 p0">
					<p class="home-page-description">This customer only deal is available for very limited time. Grab this one-time founder special offer.</p>
				</div>
			</div>
		</div>
		<div>
			<div class="row">
				<div class="col-12 text-center p0">
					<h3 class="feature-title">Convert more traffic</h3>
					<h1 class="feature-heading">Capture More Targeted Leads</h1>
					<p class="home-page-title mt10">
						OPPYO helps you capture more targeted leads from your paid and organic traffic around the clock. Create simple lead funnels to generate new leads, follow up with them even after they leave your page or website!<br><br>
						Use OPPYO high converting lead pages and website pop-ups and sticky bars with inbuilt lead forms to capture more leads, register more people for your webinars, increase subscribers to your blogs/newsletters, etc. and it help you convert them while they are hot!
					</p>
					<img src="https://cdn.oppyo.com/launches/viddeyo/premium-membership/slider/step2.png" class="img-fluid d-block mx-auto step-image mt15" alt="Step2">
				</div>
			</div>
			<div class="row mt20">
				<!-- Button -->
				<div class="col-12 mx-auto text-center">
					<a href="#buynow" class="base-btn m-blue-btn btn-block dcp-btn-get-start">Upgrade to OPPYO PRO One-Time</a>
				</div>
				<div class="col-12 p0 text-center mt10 mt-md15">
					<p class="home-page-description">This customer only deal is available for very limited time. Grab this one-time founder special offer.</p>
				</div>
			</div>
		</div>
		<div>
			<div class="row">
				<div class="col-12 text-center p0">
					<h3 class="feature-title"> Nurture & close prospects</h3>
					<h1 class="feature-heading">Send Better Follow-up Emails</h1>
					<p class="home-page-title mt10">
						Harness the power of trigger-based follow up email sequence to nurture and activate your leads and convert them into paying customers.<br><br> 
						OPPYO Mail enables you to send special offers, loyalty bonuses, training material, updates, newsletters, etc. to your leads based on their interaction with your website, landing pages, and email communication. And keep them coming back for more!<br><br>
						Also get access to customizable email automation workflow templates with OPPYO. 
					</p>
					<img src="https://cdn.oppyo.com/launches/viddeyo/premium-membership/slider/step3.png" class="img-fluid d-block mx-auto step-image mt15" alt="Step3">
				</div>
			</div>
			<div class="row mt20">
				<!-- Button -->
				<div class="col-12 mx-auto text-center">
					<a href="#buynow" class="base-btn m-blue-btn btn-block dcp-btn-get-start">Upgrade to OPPYO PRO One-Time</a>
				</div>
				<div class="col-12 p0 text-center mt10 mt-md15">
					<p class="home-page-description">This customer only deal is available for very limited time. Grab this one-time founder special offer.</p>
				</div>
			</div>
		</div>
		<div>
			<div class="row">
				<div class="col-12 text-center p0">
					<h3 class="feature-title">Boost your sales</h3>
					<h1 class="feature-heading">Sell More. More Quickly</h1>
					<p class="home-page-title mt10">
						Move more deals through your marketing funnel, using personalized customer journeys based on user data and behaviour.<br><br>
						With OPPYO, create high converting offer pages or complete high converting sales funnels with upsells and down-sells that are designed to help you convert traffic through each step of the sales process and convert them from “visitors” to “buyers” faster.
					</p>
					<img src="https://cdn.oppyo.com/launches/viddeyo/premium-membership/slider/step4.png" class="img-fluid d-block mx-auto step-image mt15" alt="Step4">
				</div>
			</div>
			<div class="row mt20">
				<!-- Button -->
				<div class="col-12 mx-auto text-center">
					<a href="#buynow" class="base-btn m-blue-btn btn-block dcp-btn-get-start">Upgrade to OPPYO PRO One-Time</a>
				</div>
				<div class="col-12 p0 text-center mt10 mt-md15">
					<p class="home-page-description">This customer only deal is available for very limited time. Grab this one-time founder special offer.</p>
				</div>
			</div>
		</div>
		<div>
			<div class="row">
				<div class="col-12 text-center p0">
					<h3 class="feature-title">Delight your customers</h3>
					<h1 class="feature-heading">From Customers to Your Brand Advocates</h1>
					<p class="home-page-title mt10">
						Convert your valued customers to brand advocates by delighting them with after-sales services delivered using OPPYO like – fast loading training videos, deliver their digital products inside membership sites, secured documents & files, etc. <br><br>
						Testimonial videos, social proof videos, case studies, etc can further help you increase your credibility with prospects and customers alike, as well as help you get word of mouth publicity. 
					</p>
					<img src="https://cdn.oppyo.com/launches/viddeyo/premium-membership/slider/step5.png" class="img-fluid d-block mx-auto step-image mt15" alt="Step5">
				</div>
			</div>
			<div class="row mt20">
				<!-- Button -->
				<div class="col-12 mx-auto text-center">
					<a href="#buynow" class="base-btn m-blue-btn btn-block dcp-btn-get-start">Upgrade to OPPYO PRO One-Time</a>
				</div>
				<div class="col-12 p0 text-center mt10 mt-md15">
					<p class="home-page-description">This customer only deal is available for very limited time. Grab this one-time founder special offer.</p>
				</div>
			</div>
		</div>
	</div>
	</div>
</div>
</section>
<!-- FEATURE SECTION END  -->
<!-- CALCULATE YOUR SAVINGS section -->
<section class="more-business-section">
   <div class="container container-1170">
      <!-- Heading -->
      <div class="row" id="buildonlinepresence">
         <div class="col-12 text-center">
            <div id="buildonlinepresence1" class="d-block d-md-none"></div>
            <h2 class="home-page-title-grey ">Not only does OPPYO help you build and grow your business  <br class="d-none d-xl-block"> 
              from a single platform without any tech hassles, but also helps you
            </h2>
            <h1 class="home-page-heading mt-md10 mt8">
              Replace Multiple Apps and Save Upto $16,000 <br class="d-none d-xl-block"> Every Year
            </h1>
            <h2 class="home-page-title-grey">CALCULATE YOUR SAVINGS</h2>
         </div>
      </div>
      <div class="row align-items-center mt20 mt-md30">
         <div class="col-12 col-xl-10 offset-xl-1">
            <div class="table-responsive compare-new-table">
               <table class="table table-bordered text-center">
                  <thead>
                     <tr>
                        <th class="head-left">Replace Multiple Software - Save Time, Efforts and Money

                        </th>
                        <th class="head-right">OPPYO PRO – Founders Only One-Time Access
                        </th>
                     </tr>
                  </thead>
                  <tbody>
                     <tr>
                        <td>Clickfunnels/Kartra - $3564/Year</td>
                        <td>OPPYO Landing Pages</td>
                     </tr>
                     <tr>
                        <td>SamCart - $2388/Year</td>
                        <td>OPPYO Sell</td>
                     </tr>
                     <tr>
                        <td>Kajabi - $3828/Year</td>
                        <td>OPPYO Memberships</td>
                     </tr>
                     <tr>
                        <td>Wistia - $1188/Year</td>
                        <td>OPPYO Videos</td>
                     </tr>
                     <tr>
                        <td>ActiveCampaign - $2748/Year</td>
                        <td>OPPYO Mail</td>
                     </tr>
                     <tr>
                        <td>WIX - $360/Year</td>
                        <td>OPPYO Websites</td>
                     </tr>
                     <tr>
                        <td>Shopify - $948/Year</td>
                        <td>OPPYO Sell (Physical Goods)</td>
                     </tr>
                     <tr>
                        <td>OptinMonster - $600/Year</td>
                        <td>OPPYO Popups, Bars & Notifications</td>
                     </tr>
                     <tr>
                        <td>Amazon S3/Dropbox - $1200/Year</td>
                        <td>OPPYO MyDrive</td>
                     </tr>
                     <tr>
                        <td>-</td>
                        <td>OPPYO Funnels/Customer Journey</td>
                     </tr>
                     <tr>
                        <td>-</td>
                        <td>OPPYO Audience</td>
                     </tr>
                     <tr>
                        <td>-</td>
                        <td>OPPYO Analytics</td>
                     </tr>
                     <tr>
                        <td>-</td>
                        <td>OPPYO A/B Testing</td>
                     </tr>
                     <tr>
                        <td>-</td>
                        <td>OPPYO Apps Coming in 2021</td>
                     </tr>
                     <tr>
                        <td>EverWebinar</td>
                        <td>OPPYO AutoWebinars</td>
                     </tr>
                     <tr>
                        <td>Twilio</td>
                        <td>OPPYO SMS</td>
                     </tr>
                     <tr>
                        <td>WordPress</td>
                        <td>OPPYO Blog</td>
                     </tr>
                     <tr>
                        <td>Zendesk</td>
                        <td>OPPYO Helpdesk</td>
                     </tr>
                     <tr>
                        <td>Teachable</td>
                        <td>OPPYO Course Builder</td>
                     </tr>
                     <tr>
                        <td>Appointy</td>
                        <td>OPPYO Appointments</td>
                     </tr>
                     <tr>
                        <td>Calendly</td>
                        <td>OPPYO Calendar</td>
                     </tr>
                     <tr>
                        <td>Tapfiliate</td>
                        <td>OPPYO Affiliates</td>
                     </tr>
                     <tr>
                        <td>SurveyMonkey</td>
                        <td>OPPYO Survey</td>
                     </tr>
                     <tr>
                        <td>-</td>
                        <td>OPPYO Marketplace</td>
                     </tr>
                     <tr>
                        <td>Total Savings: <br>
							<strong class="f-md-50 f-35">$16,824<span class="f-15">/Year</span></strong> <br><br>
							<span class="w600">Plus more OPPYO apps will come in 2021 to save<br class="d-none d-xl-block"> you more money, time and efforts.</span>						
						</td>
                        <td class="last-table-col">Get Everything for a Low One-Time Price of Only @$699</td>
                     </tr>
                  </tbody>
               </table>
            </div>
         </div>
      </div>
		<div class="row align-items-center mt20 mt-md30">
			<div class="col-12 col-xl-10 offset-xl-1 f-14 lh160">
				<strong>Note*</strong> All Prices for above companies are verified on Jan, 6th 2021 on their website. They can change prices anytime and above prices are only for comparison
							 point of view only, savings depends on your business size and growth so calculate your savings yourself.
			</div>
		</div>
	  <div class="row align-items-center mt20 mt-md30 mt-lg80">
         <div class="col-12">
            <h2 class="home-page-title-grey text-center">With the One-Time founder’s deal, <u>you’re saving over $16,000 yearly</u> !!!!</h2>
            <h1 class="home-page-heading mt-md10 mt8 text-center">
               Now that is one heck of an investment, isn’t it…?
            </h1>
            <p class="home-page-title f-md-22 mt10 mt-md20 text-center">OPPYO is unrivalled in its offerings. With the special One-Time founders deal, you get access to 20+ premium OPPYO Apps at unmatched one-time pricing of $699 only.
            </p>
         </div>
      </div>
   </div>
</section>		
<!-- CALCULATE YOUR SAVINGS section End -->
<!-- Pricing Table -->
<section class="pricing-table-container">
<div id="table-mobile-fixed" class="d-none" style="height:192px; background:#ffffff;"></div>
<div class="price-table-xs-header shadow-fixed">
		<div class="container container-1170 pt10">
			<div class="row">
				<div class="col-12 text-center">
					<div id="buildonlinepresence1" class="d-block d-md-none"></div>
					<h1 class="home-page-heading" id="buynow">
						Upgrade to OPPYO PRO Plan
					</h1>		
					<h2 class="home-page-title-grey mt-md10 mt8">(Grab Limited One-Time Deal!)</h2>
				</div>
				
				<div class="col-12 mt20 table-price-show">
					<div id="price_table_nav2" class="owl-carousel owl-theme price_table_nav Annually-xs-tab xs-tab-content d-block">
						<div class="item">
							<div class="f-12 price-title">Monthly </div>
							<p class="f-18 w700 lh120 price-num"><span class="w500">$</span>147<span class="f-9">/Mo</span></p>
							<!--<p class="price-subtitle">/month</p>-->
						</div>
					  <div class="item">
							<div class="f-12 price-title">One-Time</div>
							<p class="f-18 w700 lh120 price-num"><span class="w500">$</span>699<span class="f-9">/One-time</span></p>
							<!--<p class="price-subtitle">/month</p>-->
					   </div>
					   <div class="item">
							<div class="f-12 price-title">3 Installments</div>
							<p class="f-18 w700 lh120 price-num"><span class="w500">$</span>267<span class="f-9">/Mo</span></p>
							<!--<p class="price-subtitle">/month</p>-->
					   </div>
					</div>
				</div>
			</div>
		</div>
	</div>
<div class="container container-1170 mb30 mb-lg80">
    <!-- Pricing Table start -->
    <div class="row mt10 mt-md30 mt-xl70">
		<div class="col-12 Annually-xs-tab xs-tab-content d-block px0 px-md15" id="pricing-tab2">
            <div id="price_table_xs2" class="owl-carousel owl-theme price_table_xs">
               <div class="item mt23 mt-md0">
                    <div class="pricing-table-box border-l-0 border-l-0">
						<div class="pricing-table-header">
                            <div class="f-15 text-black w500">OPPYO Pro Monthly</div>
							<div class="pricing mt20">
								<div class="value">$77</div>
								<span class="month">/Month</span>
							</div>
							<div class="f-14 d-gblue-clr mt10"><!--<span class="m-blue-clr w700">Save $120/yr</span>    | --> Billed monthly</div>
                        </div>
						<div class="pricing-buy-btn-wrap">							
							<a href="" class="price-btn firasans-font blue-btn">Buy Now</a>
						</div>
                        <div class="pricing-table-content">
                            <ul class="features pricing-first-row">
                                <li>
                                    <strong>Unlimited</strong> Visits & Leads
                                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Unlimited Visits & Leads" class="price-info-icon">
                                        <i class="icon-infotooltip"></i>
                                    </a>
                                </li>
                                <li>
                                   <strong>Unlimited</strong> Contacts
                                   <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Unlimited Contacts" class="price-info-icon">
                                        <i class="icon-infotooltip"></i>
                                    </a>
                                </li>
								<li>
                                   <strong>Unlimited</strong> Websites
                                </li>
								<li>
                                   <strong>Unlimited</strong> Landing Pages <br class="d-none d-md-block d-xl-none">
								   <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="1 Team Member" class="price-info-icon" aria-describedby="tooltip186904">
                                        <i class="icon-infotooltip"></i>
                                    </a>
                                </li>
								<li>
                                   <strong>300</strong> Mobile Ready<br class="d-none d-md-block d-xl-none"> Templates
								   <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="1 Team Member" class="price-info-icon" aria-describedby="tooltip186904">
                                        <i class="icon-infotooltip"></i>
                                    </a>
                                </li>
								<li>
                                   Send <strong>300,000</strong> <br class="d-none d-md-block d-xl-none">emails/month
                                </li>
								<li>
                                   <strong>Unlimited</strong> Team Member<br class="d-none d-md-block d-xl-none">
                                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="1 Team Member" class="price-info-icon">
                                        <i class="icon-infotooltip"></i>
                                    </a>
                                </li>
								<li>
									<strong>Priority Support Via Call,</strong> Chat,<br class="d-xl-block d-none">
									Email & Tickets
                                </li>
                            </ul>
                            <!-- <div class="includes-back">
                                <p class="f-14 f-md-15">
                                    Everything in GROW and more:
                                </p>
                            </div> -->
							<div class="price-table-heading firasans-font">
							<span class="d-md-none">Build Your Online Business</span>
							<br class="d-none d-md-block">
							</div>
                            <ul class="features w400 pt-sm20 pricing-second-row">
                                <li>
                                    <strong> Advanced </strong>Business<br class="d-none d-md-block d-xl-none"> Website 
                                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Advanced Business Website " class="price-info-icon">
                                        <i class="icon-infotooltip"></i>
                                    </a>
									<br class="d-none d-md-block d-xl-none"><br class="d-none d-md-block d-xl-none">
                                </li>
                                <li>
                                   <strong>Advanced</strong> Membership Sites
                                   <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Advanced Membership Sites" class="price-info-icon">
                                        <i class="icon-infotooltip"></i>
                                    </a>
									<br class="d-none d-md-block d-xl-none">
									<br class="d-none d-md-block d-xl-none">
                                </li>
								<li>
                                   Create & Sell <strong>Unlimited</strong><br class="d-xl-block d-none"> Products
                                   <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Create & Sell Unlimited, Products" class="price-info-icon">
                                        <i class="icon-infotooltip"></i>
                                    </a>
									<br class="d-none d-md-block d-xl-none">
									<br class="d-none d-md-block d-xl-none">
                                </li>
								<li>
                                  0% fee on sales <strong>(Limited Time)</strong>
                                   <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="0% fee on sales (Limited Time)" class="price-info-icon">
                                        <i class="icon-infotooltip"></i>
                                    </a>
                                </li>
								<li>
									<strong>Cross-Sell, Uspell, Offers & Coupon</strong>
                                </li>
                            </ul>
							<div class="price-table-heading firasans-font">
								<span class="d-md-none">Grow Your Business</span>
								<br class="d-none d-md-block">
							</div>
							<ul class="features w400 pt-sm20 pricing-third-row">
                                <li>
                                    <strong>Unlimited</strong> Landing <br class="d-none d-md-block d-xl-none">Pages
                                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="50 Landing Pages" class="price-info-icon">
                                        <i class="icon-infotooltip"></i>
                                    </a>
                                </li>
								<li>
                                     <strong>Unlimited</strong> Email Campaigns
                                </li>
								<li>
                                    Host upto <strong>100 Videos</strong>
                                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Host upto 100 Videos" class="price-info-icon">
                                        <i class="icon-infotooltip"></i>
                                    </a>
                                </li>
								<li>
                                   Unlimited Pop Ups, Bars & Notification Boxes
                                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Unlimited Pop Ups, Bars & Notification Boxes" class="price-info-icon">
                                        <i class="icon-infotooltip"></i>
                                    </a>
                                </li>
								<li>
                                    <strong>Unlimited</strong> Funnels/Customer journey <strong>(All Type)</strong>
                                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="20 Funnels/Customer journey" class="price-info-icon">
                                        <i class="icon-infotooltip"></i>
                                    </a>
                                </li>
								<li>
                                    Advanced Analytics 
                                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Advanced Analytics" class="price-info-icon">
                                        <i class="icon-infotooltip"></i>
                                    </a>
                                </li>
								<li>
                                    <strong>Advanced</strong> Audience Management
                                </li>
								<li>
                                    <strong>Unlimited</strong> Audience Segments
                                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="20 Audience Segments" class="price-info-icon">
                                        <i class="icon-infotooltip"></i>
                                    </a>
                                </li>
								<li>
                                    <strong>Upload Contacts</strong>
                                </li>
								<li>
                                    A/B Test for Pages, Emails and Popups
                                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="A/B Test for Pages, Emails and Popups" class="price-info-icon">
                                        <i class="icon-infotooltip"></i>
                                    </a>
                                </li>
								<li>
                                    Dynamics Popups
                                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Dynamics Popups" class="price-info-icon">
                                        <i class="icon-infotooltip"></i>
                                    </a>
                                </li>
								<li>
									<strong>Video Playlist</strong>
                                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Dynamics Popups" class="price-info-icon">
                                        <i class="icon-infotooltip"></i>
                                    </a>
                                </li>
								<li>
									<strong>Audience journey details with Timeline</strong>
                                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Dynamics Popups" class="price-info-icon">
                                        <i class="icon-infotooltip"></i>
                                    </a>
                                </li>
                            </ul>
							<div class="price-table-heading firasans-font">
								<span class="d-md-none">Other Features</span>
								<br class="d-none d-md-block">
							</div>
							<ul class="features w400 pt-sm20 pricing-four-row">
                                <li>
                                    <strong>Advanced</strong> MyDrive<br class="d-none d-md-block d-xl-none">
                                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="MyDrive- Store, Share & deliver files & folder" class="price-info-icon">
                                        <i class="icon-infotooltip"></i>
                                    </a>
									<br class="d-none d-md-block"><br class="d-none d-md-block">
                                </li>
                                <li>
                                   <strong>Remove our logo from emails & Add Your Logo on videos</strong>
                                   <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="OPPYO Ads after 1000 visits/month" class="price-info-icon">
                                        <i class="icon-infotooltip"></i>
                                    </a>
									<br class="d-none d-md-block d-xl-none">
									<br class="d-none d-md-block d-xl-none">
                                </li>
								<li>
                                   <strong>50</strong> GB Storage, <strong>200</strong> GB Bandwidth/ month
                                   <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Create & Sell Unlimited, Products" class="price-info-icon">
                                        <i class="icon-infotooltip"></i>
                                    </a>
                                </li>
								<li>
                                   <strong>Double commission for OPPYO Affiliate program</strong>
									<a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Create & Sell Unlimited, Products" class="price-info-icon">
                                        <i class="icon-infotooltip"></i>
                                    </a><br class="d-none d-md-block"><br class="d-none d-md-block">
                                </li>
								<li>
                                   <strong>OPPYO Agency </strong>
									<a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Create & Sell Unlimited, Products" class="price-info-icon">
                                        <i class="icon-infotooltip"></i>
                                    </a>
                                </li>
								<li>
                                   40+ Integrations with<strong> Advanced integrations API</strong>
                                   <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="40+ Integration" class="price-info-icon">
                                        <i class="icon-infotooltip"></i>
                                    </a>
                                </li>
								<li>
                                  <strong>Founder's Special - Quick start
									bonuses</strong> 1 free custom domain,
									1 logo creation and your first online
									business & marketing campaign
									setup assistance from Team
									OPPYO
                                </li>
								<li>
                                  <strong> Founders Only Access to Upcoming
									Apps </strong>Advanced Cart, Affiliate
									manager, Auto Webinars, SMS,
									Blog, HelpDesk, Course Builder,
									Appointments, Calendar,
									Ecommerce, Survey, OPPYO
									Marketplace & More…
                                </li>
                            </ul>
                        </div>
                        <div class="pricing-table-bottom d-block d-md-none">							
                          <a href="" class="price-btn firasans-font blue-btn">Buy Now</a>
                        </div>
                    </div>
                </div>
				<div class="item mt23 mt-md0">
                    <div class="pricing-table-box border-l-0 limited-pricing">
					<div class="table-subtitle">Founders Special One-Time Deal</div>
                        <div class="pricing-table-header">
                            <div class="f-15 text-black w500">OPPYO Pro One-Time (Bestseller)</div>
							<div class="pricing mt20">
								<div class="value">$697</div>
								<span class="month">/One-time</span>
							</div>
							<div class="f-14 d-gblue-clr mt-sm10"><span class="m-blue-clr w700"> Save $16,941 </span> | Get 70% Discount
								<a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Only $999 for One-Time service - Billed Onetime only" class="price-info-icon ml5">
									<i class="icon-infotooltip"></i>
								</a>
							</div>
                        </div>
						<div class="pricing-buy-btn-wrap">
										<a href="" class="price-btn firasans-font blue-btn">Buy Now</a>
						</div>
                        <div class="pricing-table-content">
                            <ul class="features pricing-first-row">
                                <li>
                                    <strong>Unlimited</strong> Visits & Leads
                                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Unlimited Visits & Leads" class="price-info-icon">
                                        <i class="icon-infotooltip"></i>
                                    </a>
                                </li>
                                <li>
                                   <strong>Unlimited</strong> Contacts
                                   <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Unlimited Contacts" class="price-info-icon">
                                        <i class="icon-infotooltip"></i>
                                    </a>
                                </li>
								<li>
                                   <strong>Unlimited</strong> Websites
                                </li>
								<li>
                                   <strong>Unlimited</strong> Landing Pages <br class="d-none d-md-block d-xl-none">
								   <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="1 Team Member" class="price-info-icon" aria-describedby="tooltip186904">
                                        <i class="icon-infotooltip"></i>
                                    </a>
                                </li>
								<li>
                                   <strong>300</strong> Mobile Ready<br class="d-none d-md-block d-xl-none"> Templates
								   <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="1 Team Member" class="price-info-icon" aria-describedby="tooltip186904">
                                        <i class="icon-infotooltip"></i>
                                    </a>
                                </li>
								<li>
                                   Send <strong>300,000</strong> <br class="d-none d-md-block d-xl-none">emails/month
                                </li>
								<li>
                                   <strong>Unlimited</strong> Team Member<br class="d-none d-md-block d-xl-none">
                                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="1 Team Member" class="price-info-icon">
                                        <i class="icon-infotooltip"></i>
                                    </a>
                                </li>
								<li>
									<strong>Priority Support Via Call,</strong> Chat,<br class="d-xl-block d-none">
									Email & Tickets
                                </li>
                            </ul>
                            <!-- <div class="includes-back">
                                <p class="f-14 f-md-15">
                                    Everything in GROW and more:
                                </p>
                            </div> -->
							<div class="price-table-heading firasans-font">
							<span class="d-md-none">Build Your Online Business</span>
							<br class="d-none d-md-block">
							</div>
                            <ul class="features w400 pt-sm20 pricing-second-row">
                                <li>
                                    <strong> Advanced </strong>Business<br class="d-none d-md-block d-xl-none"> Website 
                                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Advanced Business Website " class="price-info-icon">
                                        <i class="icon-infotooltip"></i>
                                    </a>
									<br class="d-none d-md-block d-xl-none"><br class="d-none d-md-block d-xl-none">
                                </li>
                                <li>
                                   <strong>Advanced</strong> Membership Sites
                                   <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Advanced Membership Sites" class="price-info-icon">
                                        <i class="icon-infotooltip"></i>
                                    </a>
									<br class="d-none d-md-block d-xl-none">
									<br class="d-none d-md-block d-xl-none">
                                </li>
								<li>
                                   Create & Sell <strong>Unlimited</strong><br class="d-xl-block d-none"> Products
                                   <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Create & Sell Unlimited, Products" class="price-info-icon">
                                        <i class="icon-infotooltip"></i>
                                    </a>
									<br class="d-none d-md-block d-xl-none">
									<br class="d-none d-md-block d-xl-none">
                                </li>
								<li>
                                  0% fee on sales <strong>(Limited Time)</strong>
                                   <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="0% fee on sales (Limited Time)" class="price-info-icon">
                                        <i class="icon-infotooltip"></i>
                                    </a>
                                </li>
								<li>
									<strong>Cross-Sell, Uspell, Offers & Coupon</strong>
                                </li>
                            </ul>
							<div class="price-table-heading firasans-font">
								<span class="d-md-none">Grow Your Business</span>
								<br class="d-none d-md-block">
							</div>
							<ul class="features w400 pt-sm20 pricing-third-row">
                                <li>
                                    <strong>Unlimited</strong> Landing <br class="d-none d-md-block d-xl-none">Pages
                                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="50 Landing Pages" class="price-info-icon">
                                        <i class="icon-infotooltip"></i>
                                    </a>
                                </li>
								<li>
                                     <strong>Unlimited</strong> Email Campaigns
                                </li>
								<li>
                                    Host upto <strong>100 Videos</strong>
                                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Host upto 100 Videos" class="price-info-icon">
                                        <i class="icon-infotooltip"></i>
                                    </a>
                                </li>
								<li>
                                   Unlimited Pop Ups, Bars & Notification Boxes
                                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Unlimited Pop Ups, Bars & Notification Boxes" class="price-info-icon">
                                        <i class="icon-infotooltip"></i>
                                    </a>
                                </li>
								<li>
                                    <strong>Unlimited</strong> Funnels/Customer journey <strong>(All Type)</strong>
                                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="20 Funnels/Customer journey" class="price-info-icon">
                                        <i class="icon-infotooltip"></i>
                                    </a>
                                </li>
								<li>
                                    Advanced Analytics 
                                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Advanced Analytics" class="price-info-icon">
                                        <i class="icon-infotooltip"></i>
                                    </a>
                                </li>
								<li>
                                    <strong>Advanced</strong> Audience Management
                                </li>
								<li>
                                    <strong>Unlimited</strong> Audience Segments
                                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="20 Audience Segments" class="price-info-icon">
                                        <i class="icon-infotooltip"></i>
                                    </a>
                                </li>
								<li>
                                    <strong>Upload Contacts</strong>
                                </li>
								<li>
                                    A/B Test for Pages, Emails and Popups
                                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="A/B Test for Pages, Emails and Popups" class="price-info-icon">
                                        <i class="icon-infotooltip"></i>
                                    </a>
                                </li>
								<li>
                                    Dynamics Popups
                                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Dynamics Popups" class="price-info-icon">
                                        <i class="icon-infotooltip"></i>
                                    </a>
                                </li>
								<li>
									<strong>Video Playlist</strong>
                                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Dynamics Popups" class="price-info-icon">
                                        <i class="icon-infotooltip"></i>
                                    </a>
                                </li>
								<li>
									<strong>Audience journey details with Timeline</strong>
                                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Dynamics Popups" class="price-info-icon">
                                        <i class="icon-infotooltip"></i>
                                    </a>
                                </li>
                            </ul>
							<div class="price-table-heading firasans-font">
								<span class="d-md-none">Other Features</span>
								<br class="d-none d-md-block">
							</div>
							<ul class="features w400 pt-sm20 pricing-four-row">
                                <li>
                                    <strong>Advanced</strong> MyDrive<br class="d-none d-md-block d-xl-none">
                                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="MyDrive- Store, Share & deliver files & folder" class="price-info-icon">
                                        <i class="icon-infotooltip"></i>
                                    </a>
									<br class="d-none d-md-block"><br class="d-none d-md-block">
                                </li>
                                <li>
                                   <strong>Remove our logo from emails & Add Your Logo on videos</strong>
                                   <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="OPPYO Ads after 1000 visits/month" class="price-info-icon">
                                        <i class="icon-infotooltip"></i>
                                    </a>
									<br class="d-none d-md-block d-xl-none">
									<br class="d-none d-md-block d-xl-none">
                                </li>
								<li>
                                   <strong>50</strong> GB Storage, <strong>200</strong> GB Bandwidth/ month
                                   <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Create & Sell Unlimited, Products" class="price-info-icon">
                                        <i class="icon-infotooltip"></i>
                                    </a>
                                </li>
								<li>
                                   <strong>Double commission for OPPYO Affiliate program</strong>
									<a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Create & Sell Unlimited, Products" class="price-info-icon">
                                        <i class="icon-infotooltip"></i>
                                    </a><br class="d-none d-md-block"><br class="d-none d-md-block">
                                </li>
								<li>
                                   <strong>OPPYO Agency </strong>
									<a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Create & Sell Unlimited, Products" class="price-info-icon">
                                        <i class="icon-infotooltip"></i>
                                    </a>
                                </li>
								<li>
                                   40+ Integrations with<strong> Advanced integrations API</strong>
                                   <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="40+ Integration" class="price-info-icon">
                                        <i class="icon-infotooltip"></i>
                                    </a>
                                </li>
								<li>
                                  <strong>Founder's Special - Quick start
									bonuses</strong> 1 free custom domain,
									1 logo creation and your first online
									business & marketing campaign
									setup assistance from Team
									OPPYO
                                </li>
								<li>
                                  <strong> Founders Only Access to Upcoming
									Apps </strong>Advanced Cart, Affiliate
									manager, Auto Webinars, SMS,
									Blog, HelpDesk, Course Builder,
									Appointments, Calendar,
									Ecommerce, Survey, OPPYO
									Marketplace & More…
                                </li>
                            </ul>
                        </div>
                        <div class="pricing-table-bottom d-block d-md-none">
							
                             <a href="" class="price-btn firasans-font blue-btn">Buy Now</a>
                        </div>
                    </div>
                </div>
				<div class="item">
                    <div class="pricing-table-box border-lr-0 limited-pricing">                        
                        <div class="pricing-table-header header-limited-pric">
                            <div class="f-15 text-black w500">OPPYO Pro One-Time (3 Installments)</div>
							<div class="pricing mt20">
								<!--<div class="value f-20 mt15"><strike>$699</strike></div>
								<span class="month f-10" style="top:-15px"><strike>/Mo</strike></span>-->
								<div class="value">$247</div>
								<span class="month">/Month</span>							
							</div>
							<div class="f-14 d-gblue-clr mt-sm10">Billed monthly for 3 months
								<a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Only $999 for One-Time service - Billed Onetime only" class="price-info-icon ml5">
									<i class="icon-infotooltip"></i>
								</a>
							</div>
                        </div>
						<div class="pricing-buy-btn-wrap">
							
							<a href="" class="price-btn active firasans-font">Buy Now </a>
						</div>
                        <div class="pricing-table-content active">
                            <ul class="features pricing-first-row">
                                <li>
                                    <strong>Unlimited</strong> Visits & Leads
                                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Unlimited Visits & Leads" class="price-info-icon">
                                        <i class="icon-infotooltip"></i>
                                    </a>
                                </li>
                                <li>
                                   <strong>Unlimited</strong> Contacts
                                   <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Unlimited Contacts" class="price-info-icon">
                                        <i class="icon-infotooltip"></i>
                                    </a>
                                </li>
								<li>
                                   <strong>Unlimited</strong> Websites
                                </li>
								<li>
                                   <strong>Unlimited</strong> Landing Pages <br class="d-none d-md-block d-xl-none">
								   <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="1 Team Member" class="price-info-icon" aria-describedby="tooltip186904">
                                        <i class="icon-infotooltip"></i>
                                    </a>
                                </li>
								<li>
                                   <strong>300</strong> Mobile Ready<br class="d-none d-md-block d-xl-none"> Templates
								   <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="1 Team Member" class="price-info-icon" aria-describedby="tooltip186904">
                                        <i class="icon-infotooltip"></i>
                                    </a>
                                </li>
								<li>
                                   Send <strong>300,000</strong> <br class="d-none d-md-block d-xl-none">emails/month
                                </li>
								<li>
                                   <strong>Unlimited</strong> Team Member<br class="d-none d-md-block d-xl-none">
                                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="1 Team Member" class="price-info-icon">
                                        <i class="icon-infotooltip"></i>
                                    </a>
                                </li>
								<li>
									<strong>Priority Support Via Call,</strong> Chat,<br class="d-xl-block d-none">
									Email & Tickets
                                </li>
                            </ul>
                            <!-- <div class="includes-back">
                                <p class="f-14 f-md-15">
                                    Everything in GROW and more:
                                </p>
                            </div> -->
							<div class="price-table-heading firasans-font">
							<span class="d-md-none">Build Your Online Business</span>
							<br class="d-none d-md-block">
							</div>
                            <ul class="features w400 pt-sm20 pricing-second-row">
                                <li>
                                    <strong> Advanced </strong>Business<br class="d-none d-md-block d-xl-none"> Website 
                                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Advanced Business Website " class="price-info-icon">
                                        <i class="icon-infotooltip"></i>
                                    </a>
									<br class="d-none d-md-block d-xl-none"><br class="d-none d-md-block d-xl-none">
                                </li>
                                <li>
                                   <strong>Advanced</strong> Membership Sites
                                   <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Advanced Membership Sites" class="price-info-icon">
                                        <i class="icon-infotooltip"></i>
                                    </a>
									<br class="d-none d-md-block d-xl-none">
									<br class="d-none d-md-block d-xl-none">
                                </li>
								<li>
                                   Create & Sell <strong>Unlimited</strong><br class="d-xl-block d-none"> Products
                                   <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Create & Sell Unlimited, Products" class="price-info-icon">
                                        <i class="icon-infotooltip"></i>
                                    </a>
									<br class="d-none d-md-block d-xl-none">
									<br class="d-none d-md-block d-xl-none">
                                </li>
								<li>
                                  0% fee on sales <strong>(Limited Time)</strong>
                                   <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="0% fee on sales (Limited Time)" class="price-info-icon">
                                        <i class="icon-infotooltip"></i>
                                    </a>
                                </li>
								<li>
									<strong>Cross-Sell, Uspell, Offers & Coupon</strong>
                                </li>
                            </ul>
							<div class="price-table-heading firasans-font">
								<span class="d-md-none">Grow Your Business</span>
								<br class="d-none d-md-block">
							</div>
							<ul class="features w400 pt-sm20 pricing-third-row">
                                <li>
                                    <strong>Unlimited</strong> Landing <br class="d-none d-md-block d-xl-none">Pages
                                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="50 Landing Pages" class="price-info-icon">
                                        <i class="icon-infotooltip"></i>
                                    </a>
                                </li>
								<li>
                                     <strong>Unlimited</strong> Email Campaigns
                                </li>
								<li>
                                    Host upto <strong>100 Videos</strong>
                                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Host upto 100 Videos" class="price-info-icon">
                                        <i class="icon-infotooltip"></i>
                                    </a>
                                </li>
								<li>
                                   Unlimited Pop Ups, Bars & Notification Boxes
                                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Unlimited Pop Ups, Bars & Notification Boxes" class="price-info-icon">
                                        <i class="icon-infotooltip"></i>
                                    </a>
                                </li>
								<li>
                                    <strong>Unlimited</strong> Funnels/Customer journey <strong>(All Type)</strong>
                                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="20 Funnels/Customer journey" class="price-info-icon">
                                        <i class="icon-infotooltip"></i>
                                    </a>
                                </li>
								<li>
                                    Advanced Analytics 
                                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Advanced Analytics" class="price-info-icon">
                                        <i class="icon-infotooltip"></i>
                                    </a>
                                </li>
								<li>
                                    <strong>Advanced</strong> Audience Management
                                </li>
								<li>
                                    <strong>Unlimited</strong> Audience Segments
                                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="20 Audience Segments" class="price-info-icon">
                                        <i class="icon-infotooltip"></i>
                                    </a>
                                </li>
								<li>
                                    <strong>Upload Contacts</strong>
                                </li>
								<li>
                                    A/B Test for Pages, Emails and Popups
                                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="A/B Test for Pages, Emails and Popups" class="price-info-icon">
                                        <i class="icon-infotooltip"></i>
                                    </a>
                                </li>
								<li>
                                    Dynamics Popups
                                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Dynamics Popups" class="price-info-icon">
                                        <i class="icon-infotooltip"></i>
                                    </a>
                                </li>
								<li>
									<strong>Video Playlist</strong>
                                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Dynamics Popups" class="price-info-icon">
                                        <i class="icon-infotooltip"></i>
                                    </a>
                                </li>
								<li>
									<strong>Audience journey details with Timeline</strong>
                                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Dynamics Popups" class="price-info-icon">
                                        <i class="icon-infotooltip"></i>
                                    </a>
                                </li>
                            </ul>
							<div class="price-table-heading firasans-font">
								<span class="d-md-none">Other Features</span>
								<br class="d-none d-md-block">
							</div>
							<ul class="features w400 pt-sm20 pricing-four-row">
                                <li>
                                    <strong>Advanced</strong> MyDrive<br class="d-none d-md-block d-xl-none">
                                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="MyDrive- Store, Share & deliver files & folder" class="price-info-icon">
                                        <i class="icon-infotooltip"></i>
                                    </a>
									<br class="d-none d-md-block"><br class="d-none d-md-block">
                                </li>
                                <li>
                                   <strong>Remove our logo from emails & Add Your Logo on videos</strong>
                                   <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="OPPYO Ads after 1000 visits/month" class="price-info-icon">
                                        <i class="icon-infotooltip"></i>
                                    </a>
									<br class="d-none d-md-block d-xl-none">
									<br class="d-none d-md-block d-xl-none">
                                </li>
								<li>
                                   <strong>50</strong> GB Storage, <strong>200</strong> GB Bandwidth/ month
                                   <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Create & Sell Unlimited, Products" class="price-info-icon">
                                        <i class="icon-infotooltip"></i>
                                    </a>
                                </li>
								<li>
                                   <strong>Double commission for OPPYO Affiliate program</strong>
									<a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Create & Sell Unlimited, Products" class="price-info-icon">
                                        <i class="icon-infotooltip"></i>
                                    </a><br class="d-none d-md-block"><br class="d-none d-md-block">
                                </li>
								<li>
                                   <strong>OPPYO Agency </strong>
									<a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Create & Sell Unlimited, Products" class="price-info-icon">
                                        <i class="icon-infotooltip"></i>
                                    </a>
                                </li>
								<li>
                                   40+ Integrations with<strong> Advanced integrations API</strong>
                                   <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="40+ Integration" class="price-info-icon">
                                        <i class="icon-infotooltip"></i>
                                    </a>
                                </li>
								<li>
                                  <strong>Founder's Special - Quick start
									bonuses</strong> 1 free custom domain,
									1 logo creation and your first online
									business & marketing campaign
									setup assistance from Team
									OPPYO
                                </li>
								<li>
                                  <strong> Founders Only Access to Upcoming
									Apps </strong>Advanced Cart, Affiliate
									manager, Auto Webinars, SMS,
									Blog, HelpDesk, Course Builder,
									Appointments, Calendar,
									Ecommerce, Survey, OPPYO
									Marketplace & More…
                                </li>
                            </ul>
                        </div>
                        <div class="pricing-table-bottom d-block d-md-none">
                           <a href="" class="price-btn active firasans-font">Buy Now </a>
                        </div>
                    </div>
                </div>
			</div>
            <div class="row no-gutters d-none d-md-flex md-bottom-button">                
                <div class="col-12 col-md-4">
                    <div class="pricing-table-bottom">
					  
					 <!--<a href="https://www.jvzoo.com/b/106077/382318/2"><img src="https://i.jvzoo.com/106077/382318/2" alt="Viddeyo Premium Monthly Membership" border="0" class="img-fluid mx-auto d-block"/></a>-->
                 </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="pricing-table-bottom">   				 
					 <!--<a href=""><img src="https://i.jvzoo.com/106077/382322/2" alt="Viddeyo Premium One-Time Plan" border="0" class="img-fluid mx-auto d-block"/></a>-->
                    </div>
                </div>
				<div class="col-12 col-md-4">
                    <div class="pricing-table-bottom">			
						<!--<a href="https://www.jvzoo.com/b/106077/382320/2"><img src="https://i.jvzoo.com/106077/382320/2" alt="Viddeyo Premium 3 Instalment Plan" border="0" class="img-fluid mx-auto d-block" /></a>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="price_table_scroll_end"></div>
</div>	
</section>
<!-- Pricing Table End-->

<!-- FREE TRAINING SECTION -->
<section class="free-training-section grey-section">
	<div class="container container-1170">
		<!-- Heading -->
		<div class="row">
			<div class="col-12 text-center">
				<h2 class="home-page-title-grey">That’s Not All</h2>
				<h1 class="home-page-heading mt-md10 mt8">
					All Serious Entrepreneurs Who Will Secure Founder’s Special Deal Today Will Also Get Fast Action Bonuses:
				</h1>
				<p class="home-page-title mt15 mt-md40">
					YES, If you are one of early action takers who upgrade to the founder’s special deal today, you not only secure your One-Time access to the OPPYO PRO plan but also get these very limited bonuses
				</p>
			</div>
		</div>
		<div class="row text-center">
			<div class="col-12 col-md-4 mt25 mt-md-30 mt-xl80">
				<img src="https://cdn.oppyo.com/launches/viddeyo/premium-membership/training-img1.png" class="img-fluid d-block mx-auto" alt="Training1">
				<h4 class="title mt15 mt-md25">1 Free Custom <br class="d-none d-xl-block"> Domain <span class="w400">for 1 year</span></h4>
			</div>
			<div class="col-12 col-md-4 mt25 mt-md-30 mt-xl80">
				<img src="https://cdn.oppyo.com/launches/viddeyo/premium-membership/training-img2.png" class="img-fluid d-block mx-auto" alt="Training2">
				<h4 class="title mt15 mt-md25"> 1 Custom & <br class="d-none d-xl-block"> Professional Logo <br class="d-none d-xl-block"> <span class="w400">For Your Business</span></h4>
			</div>
			<div class="col-12 col-md-4 mt25 mt-md-30 mt-xl80">
				<img src="https://cdn.oppyo.com/launches/viddeyo/premium-membership/training-img3.png" class="img-fluid d-block mx-auto" alt="Training3">
				<h4 class="title mt15 mt-md25">1-To-1 Assistance</h4>
				<p class="f-16 f-md-20 lh160 w400 mt10 mt-md15">
					Migration help, Launch your first business and growth campaign quick & easy with team OPPYO
				</p>
			</div>
		</div>
		<div class="row text-center">
			<!--<div class="col-12 mt20 mt-md30 mt-lg65">
				<p class="f-16 f-md-20 lh160 w400">
					If you ever need an extra hand, we also have an amazing, FREE quick setup service (migration help & online business setup) to help you at any step along the way. We can help you set up your logo, domain, offers, funnels, and marketing campaigns in OPPYO easily. We also provide marketing expertise as and when required so you never get stuck.
				</p>
			</div>-->
			<div class="col-12 col-md-7 col-xl-5 px-xl30 mx-auto mt20 mt-md30 mt-xl60">
				<!-- Button -->
				<a href="#buynow" class="base-btn m-blue-btn btn-block dcp-btn-get-start">Upgrade to OPPYO PRO One-Time</a>
			</div>
			<div class="col-12 mt10 mt-md15">
				<p class="home-page-description">This customer only deal is available for very limited time. Grab this one-time founder special offer.</p>
			</div>
		</div>
	</div>
</section>		
<!-- FREE TRAINING SECTION END -->

<!-- More Business section -->
<section class="more-business-section" id="allsolutions">
	<div class="container container-1170">
		<!-- Heading -->
		<div class="row" id="buildonlinepresence">
			<div class="col-12 text-center">
			<div class="d-block d-md-none"></div>
				<h1 class="home-page-heading text-capitalize">
					Why This Crazily LOW Pricing?
				</h1>
				<h2 class="home-page-title-grey mt-md10 mt8">
				Great deals come seldom. As we are still in BETA and want your support  <br class="d-none d-xl-block"> to make it reality and In return, we want to save you a huge money, time and efforts. We want to see you succeed with us.
				</h2>
								
			</div>
		</div>
		
		<div class="row align-items-center mt20 mt-md30 mt-lg100">
			<div class="col-12 col-md-6 col-lg-6 order-md-2">				
				<p class="home-page-title">
					OPPYO has been used and thoroughly tested by 16,500 + entrepreneurs like yourself over the last 6 months even though it is still in beta as we are continuously adding new apps and solutions. Our team of more than 40 full time and inhouse developers is working round the clock to get OPPYO fully loaded. More and more features and tools are being released every month so we need your support in using it, testing it, and improving it by suggesting features while we will help you save tons of time and money. <br><br>
					As a result, we are offering you a chance to join OPPYO at the ground level, by becoming a <span class="w700">FOUNDING MEMBER </span>and taking advantage of the  <span class="w700">One-Time FOUNDERS DEAL</span> and getting access to all the features OPPYO has to offer now and as and when they will be released in future.
				</p>
			</div>
			<div class="col-12 col-md-6 col-lg-6 order-md-1 mt20 mt-md0 pl-md0">
				<img src="https://cdn.oppyo.com/launches/viddeyo/premium-membership/great-deals.png" class="img-fluid d-block mx-auto" alt="Great-Deal">
				
			</div>
		</div>
	</div>
</section>		
<!-- More Business section End -->
<!-- More Business section -->
<section class="feature-section">
	<div class="container container-1170">
		<!-- Heading -->
		<div class="row" id="buildonlinepresence">
			<div class="col-12 text-center">
			<div id="buildonlinepresence1" class="d-block d-md-none"></div>				
				<h1 class="home-page-heading">				
					Also, If You Upgrade To The OPPYO PRO <br class="d-none d-xl-block">
One-Time Deal Today

				</h1>				
			</div>
		</div>
		
		<div class="row align-items-center mt20 mt-md30 mt-lg100">
			<div class="col-12 col-md-6 pr-lg55">			
				<p class="home-page-title mt10 mt-md20">
					You become a Founding Member and enjoy 50% commissions on all your referrals FOR ever (Usually 30%) You automatically become an affiliate as Soon as you join OPPYO.<br><br> Just let people know about OPPYO and we will do the heavy lifting while you enjoy the revenue.Upgrade now to earn more from each referral.

				</p>

			</div>
			<div class="col-12 col-md-6 mt20 mt-md0 pl-md0">
				<img src="https://cdn.oppyo.com/launches/viddeyo/premium-membership/lifetime-deal.png" class="img-fluid d-block mx-auto" alt="Life Time Deal">
			</div>
		</div>
		<div class="row text-center">
			<div class="col-12 col-md-7 col-xl-5 px-xl30 mx-auto mt20 mt-md30 mt-xl90">
				<!-- Button -->
				<a href="#buynow" class="base-btn m-blue-btn btn-block dcp-btn-get-start">Upgrade to OPPYO PRO One-Time</a>
			</div>
			<div class="col-12 mt10 mt-md15">
				<p class="home-page-description">This customer only deal is available for very limited time. Grab this one-time founder special offer.</p>
			</div>
		</div>
	</div>
</section>		
<!-- More Business section End -->

<!-- BUSINESS OWNERS START -->
<section class="business-owners">
	<div class="container container-1170">
		<!-- Heading -->
		<div class="row">
			<div class="col-12 text-center">
				<h1 class="home-page-heading">
					Here’s Why 16,500+ Online Entrepreneurs <br class="d-none d-lg-block"> Switched To OPPYO?
				</h1>
			</div>
		</div>
		
		<div class="row">
			<div class="col-12 col-md-6 px-lg35 mt70 mt-md90 mt-xl120">
				<div class="testimonial-block">
					<img src="https://cdn.oppyo.com/launches/viddeyo/premium-membership/joe-lervolino.png" class="img-fluid d-block mx-auto testi-image-size" alt="Jeo Lervolino">
					<!--<h3 class="title mt15 mt-md25">
						All-In-one: Save time & Bring growth at one place.
					</h3>-->
					<p class="testi-description mt15 mt-md25">
						The simplicity and ease-of-use, along with the ability to have full control on my marketing is the foremost reason for me to like and recommend OPPYO.
					</p>
					<p class="f-14 f-md-16 w400 lh140 mt20 mt-md25"><i>- Joe Iervolino</i></p>
				</div>
			</div>
			
			<div class="col-12 col-md-6 px-lg35 mt70 mt-md90 mt-xl120">
				<div class="testimonial-block">
					<img src="https://cdn.oppyo.com/launches/viddeyo/premium-membership/dennis-mcmanus.png" class="img-fluid d-block mx-auto testi-image-size" alt="Dennis Mamanus">
					<!--<h3 class="title mt15 mt-md25">
						Top-notch support
					</h3>-->
					<p class="testi-description mt15 mt-md25">
						After getting OPPYO, I quickly, smartly and easily achieve our goals with no hassles at all.  <br class="d-none d-lg-block"><br class="d-none d-lg-block">
					</p>
					<p class="f-14 f-md-16 w400 lh140 mt20 mt-md25"><i>- Dennis McManus</i></p>
				</div>
			</div>
			
			<div class="col-12 col-md-6 px-lg35 mt70 mt-md90 mt-xl120">
				<div class="testimonial-block">
					<img src="https://cdn.oppyo.com/launches/viddeyo/premium-membership/lyall-stichbury.png" class="img-fluid d-block mx-auto testi-image-size" alt="Lyall Stichbury">
					<!--<h3 class="title mt15 mt-md25">
						It is FREE to build an amazing online business
					</h3>-->
					<p class="testi-description mt15 mt-md25">
						OPPYO is fast, easy and 100% bankable.
					</p>
					<p class="f-14 f-md-16 w400 lh140 mt20 mt-md25"><i>- Lyall Stichbury</i></p>
				</div>
			</div>
			
			<div class="col-12 col-md-6 px-lg35 mt70 mt-md90 mt-xl120">
				<div class="testimonial-block">
					<img src="https://cdn.oppyo.com/launches/viddeyo/premium-membership/testi-img4.png" class="img-fluid d-block mx-auto testi-image-size" alt="Testimonial-Image">
					<h3 class="title mt15 mt-md25">
						Top-notch support
					</h3>
					<p class="testi-description mt15">
						OPPYO customer success team hats off to you all!! 
						I have never had such a wonderful support experience from any other service provider. The team has always been available to understand and answer any concern or issue I might be facing, no matter the time, no matter the complexity of the problem, without giving any excuses or making me wait for long holds. Marvellous experience.  
					</p>
					<p class="f-14 f-md-16 w400 lh140 mt20 mt-md25"><i>- John Doe, Multiple product websites</i></p>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- BUSINESS OWNERS END -->
<!-- Frequently Asked Questions Section -->
<section class="section-padding pt0">
<div class="container container-1170">
   <!-- Heading -->
   <div class="row">
      <div class="col-12 text-center section-title">
         <h1 class="heading-text">
            Frequently Asked Questions
         </h1>
      </div>
   </div>
   <div class="row mt30 mt-md80">
      <div class="col-12">
         <!-- FAQ's -->
         <div id="faq-accordion" class="faq-accordion">
            <div class="row">
               <div class="col-12 col-md-2 col-lg-2 p-md0 mt0 mt-lg200">
                  <img src="https://cdn.oppyo.com/launches/viddeyo/premium-membership/support-img.png" class="img-fluid d-block mx-auto" alt="Support">
                  <p class="text-center f-16 f-md-18 w700 d-mblue-clr"><i>World Class<br>24/7 Support</i></p>
               </div>
			   <!-- FAQ Accordion for DeskTop -->
               <div class="col-12 col-md-10 col-lg-10 d-none d-lg-block">				
                  <div class="row">
                     <div class="col-12 col-md-6">
                        <!-- FAQ 1 -->
                        <div class="card">
                           <div class="card-header" data-toggle="collapse" data-target="#collapseOne">
                              <a class="card-link">
								What do you mean by all-in-one growth platform?
                              </a>
                           </div>
                           <div id="collapseOne" class="collapse show" data-parent="#faq-accordion">
                              <div class="card-body">
                                 <p class="description">
                                    All in one growth platform is an integrated marketing solution which contains all the tools and training to help you build and grow your business. These tools include, but are not limited to, landing page and website builders, email automation, video hosting and player, customer journeys/funnels, etc. OPPYO is the first of its kind all-in-one growth platform.  
                                 </p>
                              </div>
                           </div>
                        </div>						
                        <!-- FAQ 3 -->
                        <div class="card">
                           <div class="card-header collapsed" data-toggle="collapse" data-target="#collapseFour" >
                              <a class="card-link">What happens if I exceed my free plan limits?</a>
                           </div>
                           <div id="collapseFour" class="collapse" data-parent="#faq-accordion">
                              <div class="card-body">
                                 <p class="description">
									In case you have exceeded your limit of resources in the free plan you have a choice to upgrade to any of the 3 customized pricing plans, START, GROW and PRO, based on your business needs. You can find the information HERE
								 </p>
                              </div>
                           </div>
                        </div>
                        <!-- FAQ 5 -->
                        <div class="card">
                           <div class="card-header collapsed" data-toggle="collapse" data-target="#collapseSeven" >
                              <a class="card-link">Can I switch my plan anytime?</a>
                           </div>
                           <div id="collapseSeven" class="collapse" data-parent="#faq-accordion">
                              <div class="card-body">
                                 <p class="description">
									OPPYO’s promise to you is that we grow when you grow! Hence, you have complete freedom to upgrade/downgrade your plans anytime you want as per your business needs. 
                                 </p>
                              </div>
                           </div>
                        </div>
                        <!-- FAQ 7 -->
                        <div class="card">
                           <div class="card-header collapsed" data-toggle="collapse" data-target="#collapseNine">
                              <a class="card-link">What Payment methods do you accept?</a>
                           </div>
                           <div id="collapseNine" class="collapse" data-parent="#faq-accordion">
                              <div class="card-body">
                                 <p class="description">
								 We accept all popular credit/debit cards, PayPal, online wallets, etc.
								 </p>
                              </div>
                           </div>
                        </div>
						<!-- FAQ 9 -->
                        <div class="card">
                           <div class="card-header collapsed" data-toggle="collapse" data-target="#collapseeleven">
                              <a class="card-link">Is there any setup cost? Hidden charges?</a>
                           </div>
                           <div id="collapseeleven" class="collapse" data-parent="#faq-accordion">
                              <div class="card-body">
                                 <p class="description">
									What you see is what you get! There are no hidden charges or setup costs associated with OPPYO. 
								 </p>
                              </div>
                           </div>
                        </div> 
						<!-- FAQ 11 -->
                        <div class="card">
                           <div class="card-header collapsed" data-toggle="collapse" data-target="#collapsethirteen">
                              <a class="card-link">Will my data be private and safe?</a>
                           </div>
                           <div id="collapsethirteen" class="collapse" data-parent="#faq-accordion">
                              <div class="card-body">
                                 <p class="description">
									OPPYO uses SSL encryption and two step authentications to ensure that your data remains private and safe.
								 </p>
                              </div>
                           </div>
                        </div>  
						<!-- FAQ 13 -->
                        <div class="card">
                           <div class="card-header collapsed" data-toggle="collapse" data-target="#collapsefifteen">
                              <a class="card-link">Have more questions?</a>
                           </div>
                           <div id="collapsefifteen" class="collapse" data-parent="#faq-accordion">
                              <div class="card-body">
                                 <p class="description">
									You can head to our community by <a href="https://support.dotcompal.com/hc/en-us/community/topics" target="_blank" class="p-blue-clr">clicking here</a> and ask any question that you might be having. 
								 </p>
                              </div>
                           </div>
                        </div>    
					 </div>
                     <div class="col-12 col-md-6">
                       <!-- FAQ 2 -->
                        <div class="card">
                           <div class="card-header collapsed" data-toggle="collapse" data-target="#collapseThree" >
                              <a class="card-link">Does OPPYO integrate with third party platforms?</a>
                           </div>
                           <div id="collapseThree" class="collapse" data-parent="#faq-accordion">
                              <div class="card-body">
                                 <p class="description">
									Yes OPPYO can be integrated with more than 50+ 3rd party tools and apps.
								 </p>
                              </div>
                           </div>
                        </div>
                        <!-- FAQ 4 -->
                        <div class="card">
                           <div class="card-header collapsed" data-toggle="collapse" data-target="#collapseSix" >
                              <a class="card-link">Is OPPYO FREE plan really free for One-Time?</a>
                           </div>
                           <div id="collapseSix" class="collapse" data-parent="#faq-accordion">
                              <div class="card-body">
                                 <p class="description">
									Absolutely. Once you have created your free account you will have access to your projects and creatives forever. We never break up with our Pals. 
								 </p>
                              </div>
                           </div>
                        </div>
						<!-- FAQ 6 -->
                        <div class="card">
                           <div class="card-header collapsed" data-toggle="collapse" data-target="#collapseEight" >
                              <a class="card-link">What’s included in my Free Plan?</a>
                           </div>
                           <div id="collapseEight" class="collapse" data-parent="#faq-accordion">
                              <div class="card-body">
                                 <p class="description">
									With the free account you get access to create 10 landing pages, Host 5 videos, community support and many more features. You can check this link to see all features that are available with OPPYO free plan.
								 </p>
                              </div>
                           </div>
                        </div>
					   <!-- FAQ 8 -->
                        <div class="card">
                           <div class="card-header collapsed" data-toggle="collapse" data-target="#collapseten">
                              <a class="card-link">Do I need to enter my credit card info to sign up for the Free Account?</a>
                           </div>
                           <div id="collapseten" class="collapse" data-parent="#faq-accordion">
                              <div class="card-body">
                                 <p class="description">
								 You do not need a credit card to sign up for the OPPYO Free Account.
								 </p>
                              </div>
                           </div>
                        </div>
						<!-- FAQ 10 -->
                        <div class="card">
                           <div class="card-header collapsed" data-toggle="collapse" data-target="#collapsetwelve">
                              <a class="card-link">Can I pay in instalments?</a>
                           </div>
                           <div id="collapsetwelve" class="collapse" data-parent="#faq-accordion">
                              <div class="card-body">
                                 <p class="description">
									Currently instalments are only available for the Founder’s Deal plan which gives you access to OPPYO Pro plan for One-Time for just $999. You can check this link for more info.  
								 </p>
                              </div>
                           </div>
                        </div>  
						<!-- FAQ 12 -->
                        <div class="card">
                           <div class="card-header collapsed" data-toggle="collapse" data-target="#collapsefourteen">
                              <a class="card-link">What is your cancellation policy?</a>
                           </div>
                           <div id="collapsefourteen" class="collapse" data-parent="#faq-accordion">
                              <div class="card-body">
                                 <p class="description">
									If for some reason you are still unsatisfied with the service, we offer a no questions asked, 14 days money-back guarantee*.
								 </p>
                              </div>
                           </div>
                        </div>  
						</div>
                  </div>
               </div>
			   <!-- FAQ Accordion for Mobile -->
			   <div class="col-12 col-md-10 col-lg-10 mt30 mt-md0 d-lg-none">			   
				  <div class="row">
					<div class="col-12">
                        <!-- FAQ 1 -->
                        <div class="card">
                           <div class="card-header" data-toggle="collapse" data-target="#collapseOne1">
                              <a class="card-link">
							  What do you mean by all-in-one growth platform?
							  </a>
                           </div>
                           <div id="collapseOne1" class="collapse show" data-parent="#faq-accordion">
                              <div class="card-body">
                                 <p class="description">
                                    All in one growth platform is an integrated marketing solution which contains all the tools and training to help you build and grow your business. These tools include, but are not limited to, landing page and website builders, email automation, video hosting and player, customer journeys/funnels, etc. OPPYO is the first of its kind all-in-one growth platform. 
                                 </p>
                              </div>
                           </div>
                        </div>
						<!-- FAQ 2-->
                        <div class="card">
                           <div class="card-header collapsed" data-toggle="collapse" data-target="#collapseTwo2" >
                              <a class="card-link">Does OPPYO integrate with third party platforms?</a>
                           </div>
                           <div id="collapseTwo2" class="collapse" data-parent="#faq-accordion">
                              <div class="card-body">
                                 <p class="description">
									Yes OPPYO can be integrated with more than 50+ 3rd party tools and apps. 
								 </p>
                              </div>
                           </div>
                        </div>						
						<!-- FAQ 3 -->
                        <div class="card">
                           <div class="card-header collapsed" data-toggle="collapse" data-target="#collapseFour4" >
                              <a class="card-link">What happens if I exceed my free plan limits?</a>
                           </div>
                           <div id="collapseFour4" class="collapse" data-parent="#faq-accordion">
                              <div class="card-body">
                                 <p class="description">
									In case you have exceeded your limit of resources in the free plan you have a choice to upgrade to any of the 3 customized pricing plans, START, GROW and PRO, based on your business needs. You can find the information HERE.
								 </p>
                              </div>
                           </div>
                        </div>
                        <!-- FAQ 4 -->
                        <div class="card">
                           <div class="card-header collapsed" data-toggle="collapse" data-target="#collapseFive5" >
                              <a class="card-link">Is OPPYO FREE plan really free for One-Time?</a>
                           </div>
                           <div id="collapseFive5" class="collapse" data-parent="#faq-accordion">
                              <div class="card-body">
                                 <p class="description">
									Absolutely. Once you have created your free account you will have access to your projects and creatives forever. We never break up with our Pals. 
								 </p>
                              </div>
                           </div>
                        </div>
						 <!-- FAQ 5 -->
                        <div class="card">
                           <div class="card-header collapsed" data-toggle="collapse" data-target="#collapseSix6">
                              <a class="card-link">Can I switch my plan anytime?</a>
                           </div>
                           <div id="collapseSix6" class="collapse" data-parent="#faq-accordion">
                              <div class="card-body">
                                 <p class="description">
								 OPPYO’s promise to you is that we grow when you grow! Hence, you have complete freedom to upgrade/downgrade your plans anytime you want as per your business needs. 
								 </p>
                              </div>
                           </div>
                        </div>
                        <!-- FAQ 6 -->
                        <div class="card">
                           <div class="card-header collapsed" data-toggle="collapse" data-target="#collapseSeven7" >
                              <a class="card-link">What’s included in my Free Plan?</a>
                           </div>
                           <div id="collapseSeven7" class="collapse" data-parent="#faq-accordion">
                              <div class="card-body">
                                 <p class="description">
								 With the free account you get access to create 10 landing pages, Host 5 videos, community support and many more features. You can check this link (Link to landing page) to see all features that are available with OPPYO free plan.
                                 </p>
                              </div>
                           </div>
                        </div>						
                        <!-- FAQ 7 -->
                        <div class="card">
                           <div class="card-header collapsed" data-toggle="collapse" data-target="#collapseNine9" >
                              <a class="card-link">What Payment methods do you accept?</a>
                           </div>
                           <div id="collapseNine9" class="collapse" data-parent="#faq-accordion">
                              <div class="card-body">
                                 <p class="description">
									We accept all popular credit/debit cards, PayPal, online wallets, etc.
								 </p>
                              </div>
                           </div>
                        </div> 
						<!-- FAQ 8 -->
                        <div class="card">
                           <div class="card-header collapsed" data-toggle="collapse" data-target="#collapse8" >
                              <a class="card-link">Do I need to enter my credit card info to sign up for the Free Account?</a>
                           </div>
                           <div id="collapse8" class="collapse" data-parent="#faq-accordion">
                              <div class="card-body">
                                 <p class="description">
									You do not need a credit card to sign up for the OPPYO Free Account.
								 </p>
                              </div>
                           </div>
                        </div>
						<!-- FAQ 9 -->
                        <div class="card">
                           <div class="card-header collapsed" data-toggle="collapse" data-target="#collapse9" >
                              <a class="card-link">Is there any setup cost? Hidden charges?</a>
                           </div>
                           <div id="collapse9" class="collapse" data-parent="#faq-accordion">
                              <div class="card-body">
                                 <p class="description">
									What you see is what you get! There are no hidden charges or setup costs associated with OPPYO. 
								 </p>
                              </div>
                           </div>
                        </div>
						<!-- FAQ 10 -->
                        <div class="card">
                           <div class="card-header collapsed" data-toggle="collapse" data-target="#collapse10" >
                              <a class="card-link">Can I pay in instalments?</a>
                           </div>
                           <div id="collapse10" class="collapse" data-parent="#faq-accordion">
                              <div class="card-body">
                                 <p class="description">
									Currently instalments are only available for the Founder’s Deal plan which gives you access to OPPYO Pro plan for One-Time for just $999. You can check this link for more info.  
								 </p>
                              </div>
                           </div>
                        </div>
						<!-- FAQ 11 -->
                        <div class="card">
                           <div class="card-header collapsed" data-toggle="collapse" data-target="#collapse11" >
                              <a class="card-link">Will my data be private and safe?</a>
                           </div>
                           <div id="collapse11" class="collapse" data-parent="#faq-accordion">
                              <div class="card-body">
                                 <p class="description">
									OPPYO uses SSL encryption and two step authentications to ensure that your data remains private and safe.
								 </p>
                              </div>
                           </div>
                        </div>
						<!-- FAQ 12 -->
                        <div class="card">
                           <div class="card-header collapsed" data-toggle="collapse" data-target="#collapse12">
                              <a class="card-link">What is your cancellation policy?</a>
                           </div>
                           <div id="collapse12" class="collapse" data-parent="#faq-accordion">
                              <div class="card-body">
                                 <p class="description">
									If for some reason you are still unsatisfied with the service, we offer a no questions asked, 14 days money-back guarantee*.
								 </p>
                              </div>
                           </div>
                        </div>
						<!-- FAQ 13 -->
                        <div class="card">
                           <div class="card-header collapsed" data-toggle="collapse" data-target="#collapse13">
                              <a class="card-link">Have more questions?</a>
                           </div>
                           <div id="collapse13" class="collapse" data-parent="#faq-accordion">
                              <div class="card-body">
                                 <p class="description">
									You can head to our community by <a href="https://support.dotcompal.com/hc/en-us/community/topics" target="_blank" class="p-blue-clr">clicking here</a> and ask any question that you might be having. 
								 </p>
                              </div>
                           </div>
                        </div>
                     </div>
				  </div>
			   </div>
            </div>
         </div>
      </div>
   </div>
</div>
</section>
<!-- Frequently Asked Questions Section end-->
<!-- Ready To Grow Section -->
<section class="support-section">
<div class="container container-1170">
	<div class="row text-center">
		<div class="col-12">
			<h1 class="support-section-heading">Are you ready to start online and grow?</h1>
		</div>
		<div class="col-12 col-md-7 col-xl-5 px-xl30 mx-auto mt20 mt-md25">
			<!-- Button -->
			<a href="#buynow" class="base-btn m-blue-btn btn-block dcp-btn-get-start">Upgrade to OPPYO PRO One-Time</a>
		</div>
		<div class="col-12 mt15 mt-md20">
			<p class="white-clr f-md-16 f-14 lh140 w400">This customer only deal is available for very limited time. Grab this one-time founder special offer.</p>
		</div>
	</div>
</div>
</section>
<!-- Ready To Grow Section end-->

<!-- OPPYO Footer Include File-->
<?php include 'includes/footer.php'; ?>
<!-- OPPYO Footer Include File end -->
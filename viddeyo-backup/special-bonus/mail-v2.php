<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<!-- Tell the browser to be responsive to screen width -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="title" content="Viddeyo Bonuses">
    <meta name="description" content="Viddeyo Bonuses">
    <meta name="keywords" content="Revealing A BLAZING-FAST Video Hosting, Player & Marketing Technology With No Monthly Fee Ever">
    <meta property="og:image" content="https://www.viddeyo.co/special-bonus/thumbnail.png">
    <meta name="language" content="English">
    <meta name="revisit-after" content="1 days">
    <meta name="author" content="Achal & Atul">
    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:title" content="Viddeyo Bonuses">
    <meta property="og:description" content="Revealing A BLAZING-FAST Video Hosting, Player & Marketing Technology With No Monthly Fee Ever">
    <meta property="og:image" content="https://www.viddeyo.co/special-bonus/thumbnail.png">
    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:title" content="Viddeyo Bonuses">
    <meta property="twitter:description" content="Revealing A BLAZING-FAST Video Hosting, Player & Marketing Technology With No Monthly Fee Ever">
    <meta property="twitter:image" content="https://www.viddeyo.co/special-bonus/thumbnail.png">

	<title>Viddeyo Bonuses</title>
	<!-- Shortcut Icon  -->
	<link rel="icon" href="https://cdn.oppyo.com/launches/viddeyo/assets/images/favicon.png" type="image/png">
	<!-- Css CDN Load Link -->
	<link rel="stylesheet" href="assets/css/font-awesome.min.css" type="text/css" />
	<link rel="stylesheet" href="https://cdn.oppyo.com/launches/viddeyo/assets/css/bootstrap.min.css" type="text/css">
	<link rel="stylesheet" type="text/css" href="assets/css/bonus-style.css">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
	<script src="../common_assets/js/jquery.min.js"></script>

	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-NMK2MBB');</script>
	<!-- End Google Tag Manager -->

</head>
<body>

	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NMK2MBB"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) --> 

	<!-- New Timer  Start-->
	<?php
		$date = 'June 06 2022 10:00 AM EST';
		$exp_date = strtotime($date);
		$now = time();  
		/*
		
		$date = date('F d Y g:i:s A eO');
		$rand_time_add = rand(700, 1200);
		$exp_date = strtotime($date) + $rand_time_add;
		$now = time();*/
		
		if ($now < $exp_date) {
	?>
	<?php
		} else {
			echo "Times Up";
		}
	?>
	<!-- New Timer End -->

	<?php
		if(!isset($_GET['afflink'])){
		$_GET['afflink'] = 'https://cutt.ly/cJdcRgI';
		$_GET['name'] = 'Achal & Atul';      
		}
	?>

	<div class="main-header">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-12 text-center">
                    <div class="text-center">
                    	<div class="f-md-32 f-20 lh140 w400 white-clr d-block d-md-flex align-items-center justify-content-center">
                    		<span class="w600"><?php echo $_GET['name'];?>'s</span> &nbsp;special bonus for &nbsp;
							<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
							viewBox="0 0 909.75 162.72" style="enable-background:new 0 0 909.75 162.72; max-height:50px;" xml:space="preserve">
								<style type="text/css">
									.st00{fill:#FFFFFF;}
									.st11{fill:url(#SVGID_1_);}
								</style>
								<g>
									<g>
										<path class="st00" d="M216.23,71.07L201.6,97.26l-35.84-64.75l-3.16-5.7h29.13l0.82,1.49L216.23,71.07z M288.35,26.81l-3.16,5.7
										l-56.4,103.68h-6.26l-11.71-22.25l14.63-26.18l0.01,0.02l2.73-4.94l4.81-8.68l25.38-45.87l0.82-1.49H288.35z"/>
										<path class="st00" d="M329.85,26.61v109.57h-23.23V26.61H329.85z"/>
										<path class="st00" d="M454.65,81.7c0,2.7-0.2,5.41-0.59,8.04c-3.58,24.36-23.15,43.45-47.59,46.42l-0.26,0.03h-50.93v-69.9h23.23
										v46.67h26.13c12.93-2,23.4-11.91,26.09-24.74c0.45-2.14,0.67-4.33,0.67-6.51c0-2.11-0.21-4.22-0.62-6.26
										c-1.52-7.54-5.76-14.28-11.95-18.97c-3.8-2.88-8.14-4.84-12.75-5.78c-1.58-0.32-3.19-0.52-4.83-0.6h-45.98V26.82L403.96,27
										c11.89,0.9,23.21,5.67,32.17,13.61c9.78,8.65,16.15,20.5,17.96,33.36C454.46,76.49,454.65,79.1,454.65,81.7z"/>
										<path class="st00" d="M572.18,81.7c0,2.7-0.2,5.41-0.59,8.04c-3.58,24.36-23.15,43.45-47.59,46.42l-0.26,0.03h-50.93v-69.9h23.23
										v46.67h26.13c12.93-2,23.4-11.91,26.09-24.74c0.45-2.14,0.67-4.33,0.67-6.51c0-2.11-0.21-4.22-0.62-6.26
										c-1.52-7.54-5.76-14.28-11.95-18.97c-3.8-2.88-8.14-4.84-12.75-5.78c-1.58-0.32-3.19-0.52-4.83-0.6h-45.98V26.82L521.5,27
										c11.89,0.9,23.2,5.67,32.17,13.61c9.78,8.65,16.15,20.5,17.96,33.36C572,76.49,572.18,79.1,572.18,81.7z"/>
										<path class="st00" d="M688.08,26.82v23.23h-97.72V31.18l0.01-4.36H688.08z M613.59,112.96h74.49v23.23h-97.72v-18.87l0.01-4.36
										V66.1l23.23,0.14h64.74v23.23h-64.74V112.96z"/>
										<path class="st00" d="M808.03,26.53l-5.07,6.93l-35.84,48.99l-1.08,1.48L766,83.98l-0.04,0.05l0,0.01l-0.83,1.14v51h-23.23V85.22
										l-0.86-1.18l-0.01-0.01l-0.04-0.05l-0.04-0.06l-1.08-1.48l-35.84-48.98l-5.07-6.93h28.77l1.31,1.78l24.46,33.43l24.46-33.43
										l1.31-1.78H808.03z"/>
										<path class="st00" d="M909.75,81.69c0,30.05-24.45,54.49-54.49,54.49c-30.05,0-54.49-24.45-54.49-54.49S825.2,27.2,855.25,27.2
										C885.3,27.2,909.75,51.65,909.75,81.69z M855.25,49.95c-17.51,0-31.75,14.24-31.75,31.75s14.24,31.75,31.75,31.75
										c17.51,0,31.75-14.24,31.75-31.75C887,64.19,872.76,49.95,855.25,49.95z"/>
									</g>
								</g>
								<linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="0" y1="81.3586" x2="145.8602" y2="81.3586">
									<stop  offset="0.4078" style="stop-color:#FFFFFF"/>
									<stop  offset="1" style="stop-color:#FC6DAB"/>
								</linearGradient>
								<path class="st11" d="M11.88,0c41.54,18.27,81.87,39.12,121.9,60.41c7.38,3.78,12.24,11.99,12.07,20.32
								c0.03,7.94-4.47,15.72-11.32,19.73c-27.14,17.07-54.59,33.74-82.21,50c-0.57,0.39-15.29,8.93-15.47,9.04
								c-1.4,0.77-2.91,1.46-4.44,1.96c-14.33,4.88-30.25-4.95-32.19-19.99c-0.18-1.2-0.23-2.5-0.24-3.72c0.35-25.84,1.01-53.01,2.04-78.83
								C2.57,42.41,20.3,32.08,34.79,40.31c8.16,4.93,16.31,9.99,24.41,15.03c2.31,1.49,7.96,4.89,10.09,6.48
								c5.06,3.94,7.76,10.79,6.81,17.03c-0.68,4.71-3.18,9.07-7.04,11.77c-0.38,0.24-1.25,0.88-1.63,1.07c-3.6,2.01-7.5,4.21-11.11,6.17
								c-9.5,5.22-19.06,10.37-28.78,15.27c11.29-9.81,22.86-19.2,34.53-28.51c3.54-2.55,4.5-7.53,1.83-10.96c-0.72-0.8-1.5-1.49-2.49-1.9
								c-0.18-0.08-0.42-0.22-0.6-0.29c-11.27-5.17-22.82-10.58-33.98-15.95c0,0-0.22-0.1-0.22-0.1l-0.09-0.02l-0.18-0.04
								c-0.21-0.08-0.43-0.14-0.66-0.15c-0.11-0.01-0.2-0.07-0.31-0.04c-0.21,0.03-0.4-0.04-0.6,0.03c-0.1,0.02-0.2,0.02-0.29,0.03
								c-0.14,0.06-0.28,0.1-0.43,0.12c-0.13,0.06-0.27,0.14-0.42,0.17c-0.09,0.03-0.17,0.11-0.26,0.16c-0.08,0.06-0.19,0.06-0.26,0.15
								c-0.37,0.25-0.66,0.57-0.96,0.9c-0.1,0.2-0.25,0.37-0.32,0.59c-0.04,0.08-0.1,0.14-0.1,0.23c-0.1,0.31-0.17,0.63-0.15,0.95
								c-0.06,0.15,0.04,0.35,0.02,0.52c0,0.02,0,0.08-0.01,0.1c0,0,0,0.13,0,0.13l0.02,0.51c0.94,24.14,1.59,49.77,1.94,73.93
								c0,0,0.06,4.05,0.06,4.05l0,0.12l0.01,0.02l0.01,0.03c0,0.05,0.02,0.11,0.02,0.16c0.08,0.23,0.16,0.29,0.43,0.47
								c0.31,0.16,0.47,0.18,0.71,0.09c0.06-0.04,0.11-0.06,0.18-0.1c0.02-0.01-0.01,0.01,0.05-0.03l0.22-0.13l0.87-0.52
								c32.14-19.09,64.83-37.83,97.59-55.81c0,0,0.23-0.13,0.23-0.13c0.26-0.11,0.51-0.26,0.69-0.42c1.15-0.99,0.97-3.11-0.28-4.01
								c0,0-0.43-0.27-0.43-0.27l-1.7-1.1C84.73,51.81,47.5,27.03,11.88,0L11.88,0z"/>
							</svg>
						</div>
					</div>

					<div class="col-12 mt20 mt-md50 text-center">
						<div class="preheadline f-18 f-md-24 w400 white-clr lh140">
							Grab My 20 Exclusive Bonuses Before the Deal Ends… 
						</div>
					</div>
				
					<div class="col-12 mt-md20 mt10 f-md-50 f-28 w400 text-center white-clr lh140">
						Revealing A <span class="w700 highlight">BLAZING-FAST Video Hosting, Player & Marketing Technology</span> With No Monthly Fee Ever
					</div>

					<div class="col-12 mt20 mt-md20 f-md-22 f-18 w400 white-clr text-center lh150">
						Watch My Quick Review Video
					</div>

				</div>
            </div>

			<div class="row mt20 mt-md30">
				<div class="col-12 col-md-10 mx-auto">
					<!-- <img src="https://cdn.oppyo.com/launches/viddeyo/special-bonus/product-box.webp" class="img-fluid mx-auto d-block" alt="ProductBox"> -->
					<div class="responsive-video">
                        <iframe src="https://launch.oppyo.com/video/embed/px6ff3cd9e" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                        box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                    </div>
				</div>
			</div>
        </div>
	</div>

	<!-- Header Section Start -->
	<div class="second-section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="row header-list-block">
                        <div class="col-12 col-md-6">
                            <ul class="features-list pl0 f-18 f-md-20 lh150 w400 black-clr text-capitalize">
								<li><span class="w600">Upload Unlimited Videos</span> - Sales, E-Learning, Training, Product Demo, or ANY Video</li>
								<li><span class="w600">Super-Fast Delivery (After All, Time Is Money!)</span> No Buffering. No Delay </li>
								<li><span class="w600">Get Maximum Visitors Engagement</span>–No Traffic Leakage with Unwanted Ads or Video Suggestions</li>
								<li>Create 100% Mobile & SEO Optimized <span class="w600">Video Channels & Playlists</span></li>
								<li><span class="w600">Stunning Promo & Social Ads Templates</span> for Monetization & Social Traffic</li>
								<li>Fully Customizable Drag and Drop <span class="w600">WYSIWYG Video Ads Editor</span></li>
								<li><span class="w600">Intelligent Analytics</span> to Measure the Performance </li>
                            </ul>
                        </div>
                        <div class="col-12 col-md-6 mt10 mt-md0">
                            <ul class="features-list pl0 f-18 f-md-20 lh150 w400 black-clr text-capitalize">
								<li class="w600">Tap Into Fast-Growing $398 Billion E-Learning, Video Marketing & Video Agency Industry. </li>
								<li>Manage All the Videos, Courses, & Clients Hassle-Free, <span class="w600">All-In-One Central Dashboard.</span> </li>
								<li>Robust Solution That Has <span class="w600">Served Over 69 million Video Views for Customers</span></li>
								<li>HLS Player- Optimized to <span class="w600">Work Beautifully with All Browsers, Devices & Page Builders</span> </li>
								<li>Connect With Your Favourite Tools. <span class="w600">20+ Integrations</span> </li>
								<li><span class="w600">Completely Cloud-Based</span> & Step-By-Step Video Training Included </li>
								<li>PLUS, YOU’LL RECEIVE – <span class="w600">A LIMITED COMMERCIAL LICENSE IF YOU BUY TODAY</span> </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row mt20 mt-md70">
               	<!-- CTA Btn Section Start -->
               	<div class="col-md-12 col-md-12 col-12 text-center ">
                  	<div class="f-md-24 f-20 text-center mt3 black black-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
                  	<div class="f-md-36 f-22 text-center  lh120 w700 mt20 mt-md20 black-clr">TAKE ACTION NOW!</div>
                  	<div class="f-18 f-md-22 lh150 w400 text-center mt20 black-clr">
                     	Use Coupon Code <span class="w700 purple-clr">“VIDDEYO”</span> for an Additional <span class="w700 purple-clr">10% Discount</span> on Commercial Licence
                  	</div>
               	</div>
               	<div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  	<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
                  		<span class="text-center">Grab Viddeyo + My 20 Exclusive Bonuses</span> <br>
                  	</a>
               	</div>
               	<div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
                  	<img src="https://cdn.oppyo.com/launches/viddeyo/special-bonus/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               	</div>
               	<div class="col-12 mt15 mt-md20" align="center">
                  	<h3 class="f-md-35 f-md-28 f-20 w500 text-center black">Coupon Is Expiring In... </h3>
               	</div>
               	<!-- Timer -->
               	<div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  	<div class="countdown counter-black">
						<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Days</span> </div>
						<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
						<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
						<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                  	</div>
               	</div>
               <!-- Timer End -->
               <!-- CTA Button Section End   -->
            </div>
        </div>
    </div>
	<!-- Header Section End -->
	
	<!-- Step Section Start -->
	<div class="step-section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="f-md-50 f-28 w600 lh150 text-capitalize text-center black-clr">
						With Viddeyo, Publish A Captivating Video<br class="d-none d-md-block"> In 3 Easy Steps 
                    </div>
                </div>
                <div class="col-12 mt40 mt-md60">
                    <div class="row">
                        <div class="col-12 col-md-4">
                            <div class="steps-block">
                                <div class="step-bg">1</div>
                                <img src="https://cdn.oppyo.com/launches/viddeyo/special/step-1.webp" class="img-fluid mx-auto d-block mt10" alt="Upload">
                                <div class="f-24 f-md-32 w700 black-clr lh150 mt20 mt-md30 text-center">
                                    Upload
                                </div>
                                <div class="f-18 w400 black-clr lh150 mt15 text-center">
                                    Upload one or more videos at a time and let Viddeyo optimize them for faster delivery on any browser, page & device.
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-4 mt40 mt-md0">
                            <div class="steps-block">
                                <div class="step-bg">2</div>
                                <img src="https://cdn.oppyo.com/launches/viddeyo/special/step-2.webp" class="img-fluid mx-auto d-block mt10" alt="Customize">
                                <div class="f-24 f-md-32 w700 black-clr lh150 mt20 mt-md30 text-center">
                                    Customize
                                </div>
                                <div class="f-18 w400 black-clr lh150 mt15 text-center">
                                    The look and feel of the player in just a few clicks to use it for your brand or to monetize it…
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-4 mt40 mt-md0">
                            <div class="steps-block">
                                <div class="step-bg">3</div>
                                <img src="https://cdn.oppyo.com/launches/viddeyo/special/step-3.webp" alt="Publish" class="img-fluid mx-auto d-block mt10">
                                <div class="f-24 f-md-32 w700 black-clr lh150 mt20 mt-md30 text-center">
                                    Publish
                                </div>
                                <div class="f-18 w400 black-clr lh150 mt15 text-center">
                                    Your videos on any website or landing page and watch your customer engagement,sales and profits rolling infast.
                                </div>
                            </div>
                        </div>
                        <div class="col-12 f-18 f-md-20 w400 lh150 mt20 mt-md70 text-center">
                            Play ANY Video (Client, Demo, Sales, Lead Gen, Training…) on Any Website (WP, Shopify, Html…) and Any device to 
                        </div>
                        <div class="col-12 w600 f-24 f-md-32 black-clr text-center lh150 mt5">
                           boost learning, leads, sales and customer satisfaction… 
                        </div>
                        <div class="col-12 mt20 mt-md70">
                            <div class="row">
                                <div class="col-md-4 col-12">
                                    <img src="https://cdn.oppyo.com/launches/viddeyo/special/download.webp" class="img-fluid d-block mx-auto" alt="Nothing To Download Or Install">
                                    <div class="f-20 f-md-24 w600 black-clr lh150 mt15 text-center">Nothing To Download <br class="d-none d-md-block">or Install </div>
                                </div>
                                <div class="col-md-4 col-12 mt20 mt-md0">
                                    <img src="https://cdn.oppyo.com/launches/viddeyo/special/experience.webp" class="img-fluid d-block mx-auto" alt="No Prior Experience Needed. Zero Learning Curve">
                                    <div class="f-20 f-md-24 w600 black-clr lh150 mt15 text-center">No Prior Experience Needed.<br> Zero Learning Curve </div>
                                </div>
                                <div class="col-md-4 col-12 mt20 mt-md0">
                                    <img src="https://cdn.oppyo.com/launches/viddeyo/special/newbie.webp" class="img-fluid d-block mx-auto" alt="Newbie-Friendly - Created Keeping Both First Timers And Part-Timers In Mind">
                                    <div class="f-20 f-md-24 w600 black-clr lh150 mt15 text-center">Built Keeping Both First Timers <br class="d-none d-md-block">and Part-Timers in Mind 
									</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<!-- Step Section End -->

	<!-- Proof Section Start -->
	<div class="proof-section">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center f-28 f-md-45 w600 black-clr lh150">
                    It's The Same Proven Solution That We've Been <br class="d-none d-md-block"><span class="gradient-clr"> Using to Run Achal & Atul's 7 Figure Business Online </span>
                </div>
                <div class="col-12 f-20 f-md-24 w500 lh150 black-clr mt20 text-center">
                    <span class="w600">Note: All Videos on This Page Are Proudly Hosted and Played By VIDDEYO
                </div>
				 <div class="col-12 f-18 w400 lh150 black-clr mt20">
                    We are using videos in all areas (sales, training, e-learning & affiliate promos) of our online business. Even we are using Viddeyo on this sales page to play demo videos, sales videos, and customer testimonials. 
                </div>
                <div class="col-12 text-center f-18 f-md-20 w400 black-clr lh150 mt20 mt-md50">
                    <span class="f-22 f-md-32 w600 lh150">And By Using Power of Videos with VIDDEYO, We Have Generated</span>
                </div>
                <div class="col-12 col-md-10 mt20 mt-md50">
                     <img src="https://cdn.oppyo.com/launches/viddeyo/special/a1.webp" class="img-fluid d-block mx-auto" alt="Proof">
                </div>
				 <div class="col-12 col-md-10 ms-auto mt20 mt-md30">
					  <img src="https://cdn.oppyo.com/launches/viddeyo/special/a2.webp" class="img-fluid d-block mx-auto" alt="Proof">
                </div>
                <div class="col-12 mt20 mt-md70">
                    <div class="row align-items-center">
                        <div class="col-12 f-20 f-md-24 w400 lh150 black-clr text-center">
                           But it’s not just us. Using Viddeyo... 
                        </div>
                        <div class="col-12 text-center f-22 f-md-28 w600 black-clr lh150 mb20 mb-md50">
                           Over 16800 Businesses and Marketers Got 69 million+ Video Views <br class="d-none d-md-block"> and They Are ABSOLUTELY LOVING IT 
                        </div>
                        <div class="col-12 col-md-6 order-md-2">
                            <div class="f-18 f-md-20 w700 lh150 black-clr">
                              Till now it has Smartly... 
                            </div>
                            <ul class="features-list pl0 f-18 f-md-20 lh150 w400 black-clr text-capitalize mt20">
                                <li>Served 16,000+ Successful Businesses & Marketers</li>
                                <li>Processed Over 150K Videos </li>
                                <li>Got More Than 69 million Video Views </li>
                                <li>And Played Around 4.9 million Minutes of Videos </li>
                            </ul>
                        </div>
                        <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
                            <img src="https://cdn.oppyo.com/launches/viddeyo/special/massive.webp" class="img-fluid d-block mx-auto" alt="Proof">
                        </div>
                    </div>
                </div>
                <div class="col-12 text-center f-24 f-md-36 w600 gradient-clr lh150 mt20 mt-md70">
                  It is Totally MASSIVE. 
                </div>
                <div class="col-12 f-18 f-md-20 w400 lh150 black-clr text-center">
                   And now it's your turn to power your own business without worrying about a hefty price tag during this launch special... 
                </div>
            </div>
            <div class="row mt20 mt-md70">
               	<!-- CTA Btn Section Start -->
               	<div class="col-md-12 col-md-12 col-12 text-center ">
                 	<div class="f-md-24 f-20 text-center mt3 black black-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
                  	<div class="f-md-36 f-22 text-center  lh120 w700 mt20 mt-md20 black-clr">TAKE ACTION NOW!</div>
                  	<div class="f-18 f-md-22 lh150 w400 text-center mt20 black-clr">
                     	Use Coupon Code <span class="w700 purple-clr">“VIDDEYO”</span> for an Additional <span class="w700 purple-clr">10% Discount</span> on Commercial Licence
                  	</div>
               	</div>
               	<div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  	<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
                  		<span class="text-center">Grab Viddeyo + My 20 Exclusive Bonuses</span> <br>
                  	</a>
               	</div>
               	<div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
                  	<img src="https://cdn.oppyo.com/launches/viddeyo/special-bonus/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               	</div>
               	<div class="col-12 mt15 mt-md20" align="center">
                  	<h3 class="f-md-35 f-md-28 f-20 w500 text-center black">Coupon Is Expiring In... </h3>
               	</div>
               	<!-- Timer -->
               	<div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  	<div class="countdown counter-black">
                     	<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Days</span> </div>
                     	<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                     	<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                     	<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                  	</div>
               </div>
               <!-- Timer End -->
               <!-- CTA Button Section End   -->
        	</div>
    	</div>
	</div>
	<!-- Proof Section End -->
	<div class="testimonial-section">
         <div class="container ">
             <div class="row ">
                 <div class="col-12">
                     <div class="f-md-45 f-28 lh150 w700 text-center black-clr">And Here’re What Some Early Users of VIDDEYO <br class="d-none d-md-block"> Say About This Powerful &amp; Smart Technology</div>
                 </div>
             </div>
             <div class="row align-items-center">
                 <div class="col-md-6 p-md0">
                     <img src="https://cdn.oppyo.com/launches/viddeyo/special/t1.webp" alt="" class="img-fluid d-block mx-auto mt20 mt-md50">
                     <img src="https://cdn.oppyo.com/launches/viddeyo/special/t2.webp" alt="" class="img-fluid d-block mx-auto mt20 mt-md50">
                 </div>
                 <div class="col-md-6 p-md0">
                     <img src="https://cdn.oppyo.com/launches/viddeyo/special/t3.webp" alt="" class="img-fluid d-block mx-auto mt20 mt-md50">
                     <img src="https://cdn.oppyo.com/launches/viddeyo/special/t4.webp" alt="" class="img-fluid d-block mx-auto mt20 mt-md50">
                 </div>
             </div>
         </div>
         </div>
	<!-- That's All Section Start-->
	<div class="thatsall-section relative">
        <div class="container">
            <div class="row">
                <div class="col-12 mt-20px">
                    <img src="https://cdn.oppyo.com/launches/viddeyo/special/arrow.webp" class="img-fluid d-block mx-auto vert-move" alt="arrow">
                </div>
                <div class="col-12 mx-auto text-center mt20 mt-md50">
                    <div class="f-24 f-md-50 w600 white-clr heading-design m-0">
                        Let’s Face It
                    </div>
                </div>
                <div class="col-12 f-md-50 f-30 w600 white-clr lh130 text-center mt30">
					Videos Are The PRESENT & FUTURE Of Internet
                </div>
            </div>
            <div class="row">
				 <div class="col-12 col-md-6 mt20 mt-md30">
                    <div class="content-box">
                        <div class="stats">80%</div>
                        <div class="title">Boost in Conversions and Sales Using Videos</div>
                    </div>
                </div>
                <div class="col-12 col-md-6 mt20 mt-md30">
                    <div class="content-box">
                        <div class="stats">300%</div>
                        <div class="title">Increase in Email Click-Through Rates with Videos</div>
                    </div>
                </div>               
                <div class="col-12 col-md-6 mt20 mt-md30">
                    <div class="content-box">
                        <div class="stats">41%</div>
                        <div class="title">More Web Traffic from Search If Use Videos </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 mt20 mt-md30">
                    <div class="content-box">
                        <div class="stats">92%</div>
                        <div class="title">Mobile Video Viewers Share Videos with Others</div>
                    </div>
                </div>
                <div class="col-12 col-md-6 mt20 mt-md30">
                    <div class="content-box">
                        <div class="stats flex-wrap">$398<br> <span>Billion</span></div>
                        <div class="title">E-Learning Industry is Also Served Using Videos </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 mt20 mt-md30">
                    <div class="content-box">
                        <div class="stats flex-wrap">100 <br> <span>Min.</span></div>
                        <div class="title">Average Time Spent In 2022 By A Person Daily Watching Videos</div>
                    </div>
                </div>
                <div class="mt20 mt-md80 text-center number-shape mx-auto">
                    <div class="f-20 f-md-28 w400 white-clr text-element">So, Capitalize on Videos Which Is 82% Of All Web Traffic</div>
                    <div class="f-28 f-md-35 w700 white-clr">HUGE Audience of</div>
                    <div class="f-45 f-md-60 w700 pink-clr lh120">3,669,153,342</div>
                    <div class="f-18 f-md-20 w400 white-clr">(Over 3.6 BILLION People)</div>
                    <img src="https://cdn.oppyo.com/launches/viddeyo/special/arrow1.webp" class="img-fluid d-none d-md-block mx-auto arrow1" alt="Arrow">
                </div>
				<div class="col-12 mt20 mt-md70">
					<div class="row align-items-center">
					<div class="col-12 col-md-6">
					<div class="text-center f-20 f-md-24 w600 lh140 white-clr">
					In This Digital Era, Using Videos for Any Business Is Not an Option Anymore…
					</div>
					<div class="mt15 text-center f-28 f-md-50 w700 lh140 white-clr">
					It is MANDATORY.
					</div>
					</div>
					<div class="col-md-6 mt20 mt-md0">
						<img src="https://cdn.oppyo.com/launches/viddeyo/special/every-business.webp" class="img-fluid d-block mx-auto" alt="Every Business">
					</div>
					</div>
				</div>
            </div>
            <img src="https://cdn.oppyo.com/launches/viddeyo/special/man.webp" class="img-fluid d-none that-man" alt="Man">
            <img src="https://cdn.oppyo.com/launches/viddeyo/special/camera.webp" class="img-fluid d-none that-camera" alt="Camera">
        </div>
    </div>
	<!-- That's All Section End-->


	<!-- Proudly Section Start -->
	<div class="proudly-sec" id="product">
        <div class="container-xxl">
            <div class="row">
               	<div class="col-12">
                	<img src="https://cdn.oppyo.com/launches/viddeyo/special/arrow.webp" class="img-fluid d-block mx-auto vert-move" alt="Arrow">
               	</div>
               	<div class="col-12 text-center f-24 f-md-38 w500 lh140 white-clr mt10 mt-md50">
                  	Proudly Presenting...
               	</div>
               	<div class="col-12 mt-md30 mt20 text-center">
                  	<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 909.75 162.72" style="enable-background:new 0 0 909.75 162.72; max-height:100px;" xml:space="preserve">
						<style type="text/css">
							.st00{fill:#FFFFFF;}
							.st11{fill:url(#SVGID_1_);}
						</style>
						<g>
							<g>
								<path class="st00" d="M216.23,71.07L201.6,97.26l-35.84-64.75l-3.16-5.7h29.13l0.82,1.49L216.23,71.07z M288.35,26.81l-3.16,5.7
								l-56.4,103.68h-6.26l-11.71-22.25l14.63-26.18l0.01,0.02l2.73-4.94l4.81-8.68l25.38-45.87l0.82-1.49H288.35z"></path>
								<path class="st00" d="M329.85,26.61v109.57h-23.23V26.61H329.85z"></path>
								<path class="st00" d="M454.65,81.7c0,2.7-0.2,5.41-0.59,8.04c-3.58,24.36-23.15,43.45-47.59,46.42l-0.26,0.03h-50.93v-69.9h23.23
								v46.67h26.13c12.93-2,23.4-11.91,26.09-24.74c0.45-2.14,0.67-4.33,0.67-6.51c0-2.11-0.21-4.22-0.62-6.26
								c-1.52-7.54-5.76-14.28-11.95-18.97c-3.8-2.88-8.14-4.84-12.75-5.78c-1.58-0.32-3.19-0.52-4.83-0.6h-45.98V26.82L403.96,27
								c11.89,0.9,23.21,5.67,32.17,13.61c9.78,8.65,16.15,20.5,17.96,33.36C454.46,76.49,454.65,79.1,454.65,81.7z"></path>
								<path class="st00" d="M572.18,81.7c0,2.7-0.2,5.41-0.59,8.04c-3.58,24.36-23.15,43.45-47.59,46.42l-0.26,0.03h-50.93v-69.9h23.23
								v46.67h26.13c12.93-2,23.4-11.91,26.09-24.74c0.45-2.14,0.67-4.33,0.67-6.51c0-2.11-0.21-4.22-0.62-6.26
								c-1.52-7.54-5.76-14.28-11.95-18.97c-3.8-2.88-8.14-4.84-12.75-5.78c-1.58-0.32-3.19-0.52-4.83-0.6h-45.98V26.82L521.5,27
								c11.89,0.9,23.2,5.67,32.17,13.61c9.78,8.65,16.15,20.5,17.96,33.36C572,76.49,572.18,79.1,572.18,81.7z"></path>
								<path class="st00" d="M688.08,26.82v23.23h-97.72V31.18l0.01-4.36H688.08z M613.59,112.96h74.49v23.23h-97.72v-18.87l0.01-4.36
								V66.1l23.23,0.14h64.74v23.23h-64.74V112.96z"></path>
								<path class="st00" d="M808.03,26.53l-5.07,6.93l-35.84,48.99l-1.08,1.48L766,83.98l-0.04,0.05l0,0.01l-0.83,1.14v51h-23.23V85.22
								l-0.86-1.18l-0.01-0.01l-0.04-0.05l-0.04-0.06l-1.08-1.48l-35.84-48.98l-5.07-6.93h28.77l1.31,1.78l24.46,33.43l24.46-33.43
								l1.31-1.78H808.03z"></path>
								<path class="st00" d="M909.75,81.69c0,30.05-24.45,54.49-54.49,54.49c-30.05,0-54.49-24.45-54.49-54.49S825.2,27.2,855.25,27.2
								C885.3,27.2,909.75,51.65,909.75,81.69z M855.25,49.95c-17.51,0-31.75,14.24-31.75,31.75s14.24,31.75,31.75,31.75
								c17.51,0,31.75-14.24,31.75-31.75C887,64.19,872.76,49.95,855.25,49.95z"></path>
                        	</g>
                    	</g>
						<linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="0" y1="81.3586" x2="145.8602" y2="81.3586">
							<stop offset="0.4078" style="stop-color:#FFFFFF"></stop>
							<stop offset="1" style="stop-color:#FC6DAB"></stop>
						</linearGradient>
						<path class="st11" d="M11.88,0c41.54,18.27,81.87,39.12,121.9,60.41c7.38,3.78,12.24,11.99,12.07,20.32
						c0.03,7.94-4.47,15.72-11.32,19.73c-27.14,17.07-54.59,33.74-82.21,50c-0.57,0.39-15.29,8.93-15.47,9.04
						c-1.4,0.77-2.91,1.46-4.44,1.96c-14.33,4.88-30.25-4.95-32.19-19.99c-0.18-1.2-0.23-2.5-0.24-3.72c0.35-25.84,1.01-53.01,2.04-78.83
						C2.57,42.41,20.3,32.08,34.79,40.31c8.16,4.93,16.31,9.99,24.41,15.03c2.31,1.49,7.96,4.89,10.09,6.48
						c5.06,3.94,7.76,10.79,6.81,17.03c-0.68,4.71-3.18,9.07-7.04,11.77c-0.38,0.24-1.25,0.88-1.63,1.07c-3.6,2.01-7.5,4.21-11.11,6.17
						c-9.5,5.22-19.06,10.37-28.78,15.27c11.29-9.81,22.86-19.2,34.53-28.51c3.54-2.55,4.5-7.53,1.83-10.96c-0.72-0.8-1.5-1.49-2.49-1.9
						c-0.18-0.08-0.42-0.22-0.6-0.29c-11.27-5.17-22.82-10.58-33.98-15.95c0,0-0.22-0.1-0.22-0.1l-0.09-0.02l-0.18-0.04
						c-0.21-0.08-0.43-0.14-0.66-0.15c-0.11-0.01-0.2-0.07-0.31-0.04c-0.21,0.03-0.4-0.04-0.6,0.03c-0.1,0.02-0.2,0.02-0.29,0.03
						c-0.14,0.06-0.28,0.1-0.43,0.12c-0.13,0.06-0.27,0.14-0.42,0.17c-0.09,0.03-0.17,0.11-0.26,0.16c-0.08,0.06-0.19,0.06-0.26,0.15
						c-0.37,0.25-0.66,0.57-0.96,0.9c-0.1,0.2-0.25,0.37-0.32,0.59c-0.04,0.08-0.1,0.14-0.1,0.23c-0.1,0.31-0.17,0.63-0.15,0.95
						c-0.06,0.15,0.04,0.35,0.02,0.52c0,0.02,0,0.08-0.01,0.1c0,0,0,0.13,0,0.13l0.02,0.51c0.94,24.14,1.59,49.77,1.94,73.93
						c0,0,0.06,4.05,0.06,4.05l0,0.12l0.01,0.02l0.01,0.03c0,0.05,0.02,0.11,0.02,0.16c0.08,0.23,0.16,0.29,0.43,0.47
						c0.31,0.16,0.47,0.18,0.71,0.09c0.06-0.04,0.11-0.06,0.18-0.1c0.02-0.01-0.01,0.01,0.05-0.03l0.22-0.13l0.87-0.52
						c32.14-19.09,64.83-37.83,97.59-55.81c0,0,0.23-0.13,0.23-0.13c0.26-0.11,0.51-0.26,0.69-0.42c1.15-0.99,0.97-3.11-0.28-4.01
						c0,0-0.43-0.27-0.43-0.27l-1.7-1.1C84.73,51.81,47.5,27.03,11.88,0L11.88,0z"></path>
                	</svg>
            	</div>
				<div class="col-12 f-md-38 f-22 mt-md30 mt20 w600 text-center white-clr lh140">
					BLAZING-FAST Video Hosting, Player & Marketing Technology That Plays Videos on Any Website, Page & Device Beautifully Without Any Delay or Buffering...
				</div>
				<div class="col-12 mt20 mt-md50">
					<img src="https://cdn.oppyo.com/launches/viddeyo/special/proudly.webp" class="img-fluid d-block mx-auto img-animation" alt="Proudly Presenting...">
				</div>
        	</div>
        </div>
    </div>
	<!-- Proudly Section End -->

	<!-- Loading Section Start -->
	<div class="loading-section">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <div class="f-22 f-md-32 w600 white-clr heading-design lh150 m-0">
                        Same Video, Faster Loading, Better Results…
                    </div>
                </div>
                <div class="col-12 mt20 mt-md30">
                    <div class="f-22 f-md-32 w600 text-center lh150 white-clr">
                        Get More Sales, Leads &amp; Customer Satisfaction with Your Videos.
                    </div>
                    <div class="f-18 w400 lh150 white-clr text-center mt20">
                        Whether it’s Sales, Prospecting or Training Videos– It got to be loading fast &amp; ensure no delays &amp; buffering. <span class="w600">Every second delay- can decrease your sales &amp; customer satisfaction by 20%.</span> So,
                        if your videos load faster, it’s GREAT, but if not,<br class="d-none d-md-block"> then you’re staring down the line.
                    </div>
                    <div class="f-22 f-md-32 w400 lh150 white-clr text-center mt20 mt-md50">
                        Viddeyo is Built with LOVE to Impress Your Visitors.
                    </div>
                    <div class="f-22 f-md-32 w600 lh150 text-center mb-md20 mb10 white-clr ">
                        It’s <span class="pink-clr">LIGHTER &amp; FASTER</span> on Every Device
                    </div>
                    <div class="f-18 f-md-20 w400 lh150 white-clr text-center mt20">
                        Compare yourself the player loading speed with Google’s performance tracking tool
                    </div>
                </div>
                <div class="col-12 mt20 mt-md50">
                    <div class="row">
                        <div class="col-12 col-md-4">
                            <div class="f-20 f-md-28 w600 lh130 white-clr text-center">
                                With Viddeyo
                            </div>
                            <div class="mt10">
                                <img src="https://cdn.oppyo.com/launches/viddeyo/special/viddeyo.webp" class="img-fluid d-block mx-auto" alt="Viddeyo">
                            </div>
                        </div>
                        <div class="col-12 col-md-4 mt20 mt-md0">
                            <div class="f-20 f-md-28 w600 lh130 white-clr text-center">
                                With Wistia
                            </div>
                            <div class="mt10">
                                <img src="https://cdn.oppyo.com/launches/viddeyo/special/wistia.webp" class="img-fluid d-block mx-auto" alt="Wistia">
                            </div>
                        </div>
                        <div class="col-12 col-md-4 mt20 mt-md0">
                            <div class="f-20 f-md-28 w600 lh130 white-clr text-center">
                                With Vimeo
                            </div>
                            <div class="mt10">
                                <img src="https://cdn.oppyo.com/launches/viddeyo/special/vimeo.webp" class="img-fluid d-block mx-auto" alt="Vimeo">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<!-- Loading Section End -->
    
    <!-- Web Primo Section Start -->
	<div class="webprimo-exclusive-bonus">
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center">
					<div class="white-clr f-24 f-md-36 lh140 w600">
						Exclusive Bonus #1 : WebPrimo (with 100 Reseller licenses)
					</div>
					<div class="f-18 f-md-20 w500 mt20 white-clr">
						20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="webprimo-logo-sec">
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center">
					<img src="https://cdn.oppyo.com/launches/viddeyo/special-bonus/webprimo-logo.webp" alt="WebPrimo Logo" class="img-fluid mx-auto d-block">
				</div>
				<div class="col-12 f-20 f-md-24 w500 text-center white-clr lh150 text-capitalize mt20 mt-md65">
                    <div>
                        The Simple 7 Minute Agency Service That Gets Clients Daily (Anyone Can Do This)
                    </div>
                    <div class="mt5">
                        <img src="https://cdn.oppyo.com/launches/viddeyo/special-bonus/line.webp" class="img-fluid mx-auto d-block">
                    </div>
				</div>
				<div class="col-12 mt-md25 mt20">
                    <div class="f-md-45 f-28 w600 text-center white-clr lh150">
						First Ever on JVZoo…  <br class="d-none d-md-block">
						<span class="w700 f-md-42 highlihgt-heading">The Game Changing Website Builder Lets You </span>  Create Beautiful Local WordPress Websites &amp; Ecom Stores for Any Business in <span class="higlight-text"> Just 7 Minutes</span>
                    </div>
				</div>
				<div class="col-12 mt20 mt-md20 f-md-22 f-20 w400 white-clr text-center">
					Easily create &amp; sell websites and stores for big profits to dentists, attorney, gyms, spas, restaurants, <br class="hidden-xs"> cafes, retail stores, book shops, coaches &amp; 100+ other niches...
				</div>
				<div class="col-12 mt20 mt-md40">
				  	<div class="row d-flex align-items-center flex-wrap">
                        <div class="col-md-7 col-12 webprimo-video-left">
							<div class="col-12 responsive-video">
                                <iframe src="https://webprimo.dotcompal.com/video/embed/8ghfrcnhp5" style="width:100%; height:100%" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                            </div> 
						</div>
                        <div class="col-md-5 col-12 mt20 mt-md0 webprimo-video-right">
                            <div class="webprimo-features">
								<ul class="webprimo-list pl0 m0 f-16 lh150 w400 white-clr">
									<li>Tap Into Fast-Growing $284 Billion Website Creation Industry</li>
									<li>Help Businesses Go Digital in Their Hard Time (IN TODAYS DIGITAL AGE)</li>
									<li>Create Elegant, Fast loading &amp; SEO Friendly Website within 7 Minutes</li>
									<li>200+ Eye-Catching Templates for Local Niches Ready with Content</li>
									<li>Bonus Live Training - Find Tons of Local Clients Hassle-Free</li>
									<li>Agency License Included to Build an Incredible Income Helping Clients!</li>
								</ul>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
		</div>
	</div>


	<div class="feature-list-sec">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="row feature-list-block">
                        <div class="col-12 col-md-6 feature-list-back">
                            <div class="f-16 f-md-18 lh150 w400">
                                <ul class="feature-bordered-list pl0 orange-list">
                                    <li><span class="w600">Create Ultra-Fast Loading &amp; Beautiful Websites</span></li>
                                    <li>Instantly Create Local Sites, E-com Sites, and Blogs–<span class="w600">For Any Business Need.</span>
									</li>
                                    <li>Built On World's No. 1, <span class="w600">Super-Customizable WP FRAMEWORK For BUSINESSES</span>
									</li>
                                    <li>SEO Optimized Websites for <span class="w600">More Search Traffic</span></li>
									<li><span class="w600">Get More Likes, Shares and Viral Traffic</span> with In-Built Social Media Tools</li>
									<li><span class="w600">Create 100% Mobile Friendly</span> And Ultra-Fast Loading Websites.</li>
									<li><span class="w600">Analytics &amp; Remarketing Ready</span> Websites</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 feature-list-back">
                            <div class="f-16 f-md-18 lh150 w400">
                                <ul class="feature-bordered-list pl0 orange-list">
                                    <li>Newbie Friendly. <span class="w600">No Tech Skills Needed. No Monthly Fees.</span></li>
                                    <li>Customize and Update Themes on Live Website Easily</li>
                                    <li>Loaded with 30+ Themes with Super <span class="w600">Customizable 200+ Templates</span> and 2000+ possible design combinations.</li>
                                    <li>Customise Websites <span class="w600">Without Touching Any Code</span></li>
                                    <li><span class="w600">Accept Payments</span> For Your Services &amp; Products With Seamless WooCommerce Integration</li>
									<li>Help Local Clients That Desperately Need Your Service And Make BIG Profits.</li>									
                                </ul>
                            </div>
                        </div>
						<div class="col-12  feature-list-back">
                            <div class="f-16 f-md-18 lh150 w400">
                                <ul class="feature-bordered-list pl0 orange-list">
									<li class="w600">Agency License Included to Build an Incredible Income Helping Clients!</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</div>
	
	<div class="webprimo-steps">
		<picture>
			<source media="(min-width:767px)" srcset="https://cdn.oppyo.com/launches/viddeyo/special-bonus/step-sec.webp" class="img-fluid d-block mx-auto">
			<source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/viddeyo/special-bonus/steps-mview.webp" class="img-fluid d-block mx-auto">
			<img src="https://cdn.oppyo.com/launches/viddeyo/special-bonus/step-sec.webp" alt="Steps" class="img-fluid d-block mx-auto" >
		</picture>
	</div>

	<div class="webprimo-know">
		<picture>
			<source media="(min-width:767px)" srcset="https://cdn.oppyo.com/launches/viddeyo/special-bonus/do-know.webp" class="img-fluid d-block mx-auto">
			<source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/viddeyo/special-bonus/know-mview.webp" class="img-fluid d-block mx-auto">
			<img src="https://cdn.oppyo.com/launches/viddeyo/special-bonus/do-know.webp" alt="Do You Know" class="img-fluid d-block mx-auto" >
		</picture>
	</div>

	<div class="webprimo-proudly">
		<picture>
			<source media="(min-width:767px)" srcset="https://cdn.oppyo.com/launches/viddeyo/special-bonus/webprimo-proudly.webp" class="img-fluid d-block mx-auto">
			<source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/viddeyo/special-bonus/webprimo-proudly-mview.webp" class="img-fluid d-block mx-auto">
			<img src="https://cdn.oppyo.com/launches/viddeyo/special-bonus/webprimo-proudly.webp" alt="Proudly" class="img-fluid d-block mx-auto" >
		</picture>
	</div>

	<!-- Web Primo Section End -->
	
	<!-- ProfitMozo Section Start -->

	<div class="exclusive-bonus">
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center">
					<div class="white-clr f-24 f-md-36 lh140 w600">
						Exclusive Bonus #2 : ProfitMozo (with 100 Reseller licenses)
					</div>
					<div class="f-18 f-md-20 w500 mt20 white-clr">
						20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="profitmozo">
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center">
					<img src="https://cdn.oppyo.com/launches/viddeyo/special-bonus/logo-profit.webp" alt="ProfitMozo" class="img-fluid mx-auto d-block">
				</div>
			</div>
		</div>
	</div>

	<div class="fast-easy">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="heading-head">
						<div class="text-center f-18 f-md-20 lh130 white-clr">
							Are you sick &amp; tired of paying hundreds of dollars monthly to landing page builders <br class="hidden-xs"> that constantly fail to deliver?
						</div>
						<div class="f-md-40 f-30 lh140 w700 mt30 text-center blue-sky">
							Revealed: <span class="orange underline">Amazingly Fast And Easy,</span> 100% Cloud-Based 1-Click Landing Page Builder That Creates High Converting Marketing Pages And Also Drives Tons Of Traffic From Facebook <span class="orange">Without Paying Any Monthly Fees Forever...</span>
						</div>
						<div class="mt30 f-md-23 f-18 lh150 w400 text-center white-clr">
							Create stunning promo pages, lead pages, webinar pages, bonus &amp; review pages &amp; much <br class="hidden-xs"> more in just a few minutes…
						</div>
					</div>	
				</div>
				<div class="col-md-8 mx-auto mt30 mt-md70">
					<div class="responsive-video">
						<iframe width="750" height="422" src="https://www.youtube.com/embed/nvu791Og7KI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
					</div>
				</div>	
			</div>
		</div>
	</div>

	<div class="marketing">
		<picture>
			<source media="(min-width:767px)" srcset="https://cdn.oppyo.com/launches/viddeyo/special-bonus/marketing.webp" class="img-fluid d-block mx-auto">
			<source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/viddeyo/special-bonus/marketing-mview.webp" class="img-fluid d-block mx-auto">
			<img src="https://cdn.oppyo.com/launches/viddeyo/special-bonus/marketing.webp" alt="Marketing" class="img-fluid d-block mx-auto" >
		</picture>
	</div>		
	

	<div class="marketing">
		<picture>
			<source media="(min-width:767px)" srcset="https://cdn.oppyo.com/launches/viddeyo/special-bonus/marketing1.webp" class="img-fluid d-block mx-auto">
			<source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/viddeyo/special-bonus/marketing1-mview.webp" class="img-fluid d-block mx-auto">
			<img src="https://cdn.oppyo.com/launches/viddeyo/special-bonus/marketing1.webp" alt="Marketing1" class="img-fluid d-block mx-auto" >
		</picture>
	</div>	

	<div class="marketing">
		<picture>
			<source media="(min-width:767px)" srcset="https://cdn.oppyo.com/launches/viddeyo/special-bonus/marketing2.webp" class="img-fluid d-block mx-auto">
			<source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/viddeyo/special-bonus/marketing2-mview.webp" class="img-fluid d-block mx-auto">
			<img src="https://cdn.oppyo.com/launches/viddeyo/special-bonus/marketing2.webp" alt="Marketing2" class="img-fluid d-block mx-auto" >
		</picture>
	</div>

	<div class="marketing">
		<picture>
			<source media="(min-width:767px)" srcset="https://cdn.oppyo.com/launches/viddeyo/special-bonus/marketing3.webp" class="img-fluid d-block mx-auto">
			<source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/viddeyo/special-bonus/marketing3-mview.webp" class="img-fluid d-block mx-auto">
			<img src="https://cdn.oppyo.com/launches/viddeyo/special-bonus/marketing3.webp" alt="Marketing3" class="img-fluid d-block mx-auto" >
		</picture>
	</div>

	<div class="profitmozo-action">
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center">
					<div class="f-md-30 f-25 lh130 w700 orange">Want to see ProfitMozo in action???</div>
				</div>
				<div class="col-md-10 mx-auto text-center mt30 ">
                	<div class="f-md-20 f-18 lh140 w500 white-clr">
						Watch how simple it is to build profitable landing pages FAST inside ProfitMozo in this <br class="hidden-xs"> quick 2 minute demo...
					</div>
            	</div>
				<div class="col-md-8 mx-auto mt30 mt-md-40">
					<div class="responsive-video">
						<iframe width="750" height="422" src="https://www.youtube.com/embed/oWUqRXCQtmc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
					</div>
            	</div>
			</div>
		</div>
	</div>
	<!-- Profitmozo Section End -->

  <!-- Bonus Section Header Start -->
	<div class="bonus-header">
		<div class="container">
			<div class="row">
				<div class="col-12 col-md-10 mx-auto heading-bg text-center">
					<div class="f-24 f-md-36 lh140 w700"> When You Purchase Viddeyo, You Also Get <br class="hidden-xs"> Instant Access To These 20 Exclusive Bonuses</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Bonus Section Header End -->

	<!-- Bonus #1 Section Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-12 text-center">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">BONUS 1</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 col-12">
					  		<img src="https://cdn.oppyo.com/launches/viddeyo/special-bonus/bonus1.webp" class="img-fluid mx-auto d-block" alt="Bonus1">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
					  		<div class="f-22 f-md-32 w600 lh150 bonus-title-color">Auto Video Creator</div>
					  		<ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
								<li>
									Uncover the secrets to create your own professional videos in minutes with this useful package. You don't even have to speak ... the software will do it for you!
								</li>
								<li>
									Just use this mind-blowing package with Viddeyo and scale your business to the next level.
								</li>						
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #1 Section End -->

	<!-- Bonus #2 Section Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
				<div class="col-12 xstext1">
					<div class="bonus-title-bg">
					   <div class="f-22 f-md-28 lh120 w700">BONUS 2</div>
					</div>
				</div>
				 
			 	<div class="col-12 mt20 mt-md30">
					<div class="row">
						<div class="col-md-5 col-12 order-md-2">
							<img src="https://cdn.oppyo.com/launches/viddeyo/special-bonus/bonus2.webp" class="img-fluid mx-auto d-block" alt="Bonus2">
						</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
					  		<div class="f-22 f-md-32 w600 lh150 bonus-title-color mt20 mt-md0">
							  Text To Speech Converter
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
								
								<li>
									Tired of reading endless texts and lengthy articles? Breathe easy as we’ve got something that will ease all your worries once and for all! Text to Speech Converter is an easy to use software that turns any text into audio, and you can listen it anytime, anywhere.
								</li>
								<li>
									When combined with Viddeyo, it enhances your video marketing results & helps you become the market pro.
								</li>
							</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #2 Section End -->

	<!-- Bonus #3 Section Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-12 text-center">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">BONUS 3</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 col-12">
					  		<img src="https://cdn.oppyo.com/launches/viddeyo/special-bonus/bonus3.webp" class="img-fluid mx-auto d-block " alt="Bonus3">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
					  		<div class="f-22 f-md-32 w600 lh150 bonus-title-color">
							  Pro Background Music Tracks
							</div>
					  		<ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
								<li>Professional quality music tracks are powerful tool for your marketing needs. Searching for something like this, you’re at the right destination.</li>
								<li>This easy to use software has countless quality tracks that vary in length from 30 seconds to 5 minutes and can be inserted anywhere in your videos.</li>
								<li>Using this package with Viddeyo will turn your videos into highly professional attention magnets without spending a fortune.</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #3 Section End -->

	<!-- Bonus #4 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-12 xstext1">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">BONUS 4</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 col-12 order-md-2">
					  		<img src="https://cdn.oppyo.com/launches/viddeyo/special-bonus/bonus4.webp" class="img-fluid mx-auto d-block " alt="Bonus4">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
					 	 	<div class="f-22 f-md-32 w600 lh150 bonus-title-color">
							  Covert Video Squeeze Page Creator
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
								<li>							  
									The most effective way to build a list is by using video squeeze pages. Keeping this in mind, here’s a very useful tool that builds amazing and high-converting squeeze pages that will generate unlimited subscribers for you.
								</li>
								<li>
									Use this tool with Viddeyo and turn your video squeeze pages into high-converting and effective pages that generates lots of new subscribers.
								</li>
							</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #4 End -->

	<!-- Bonus #5 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-12 text-center">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">BONUS 5</div>
					</div>
			 	</div>
				<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 col-12">
					  		<img src="https://cdn.oppyo.com/launches/viddeyo/special-bonus/bonus5.webp" class="img-fluid mx-auto d-block " alt="Bonus5">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
					  		<div class="f-22 f-md-32 w600 lh150 bonus-title-color">
							  Getting Traffic Video Series
					  		</div>
							<ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
								<li>
									Selling digital media products is the simplest business model to make massive profits. Follow this system to make thousands per month as an eBay PowerSeller and digital media products seller on the world wide web!
								</li>
								<li>
									Combine this system with Viddeyo to get even more sales and make even more profits from your online business.
								</li>
							</ul>
				   		</div>
					</div>
				</div>
			</div>
		</div>					
	</div>
	<!-- Bonus #5 End -->

	<!-- CTA Button Section Start -->
	<div class="dark-cta-sec">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-md-12 col-12 text-center ">
					<div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
					<div class="f-md-36 f-22 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
					<div class="f-18 f-md-22 lh150 w400 text-center mt20 white-clr">
					Use Coupon Code <span class="w700 white-clr">“VIDDEYO”</span> for an Additional <span class="w700 white-clr">10% Discount</span> on Commercial Licence
				</div>
				</div>

				<div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
					<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
						<span class="text-center">Grab Viddeyo + My 20 Exclusive Bonuses</span> <br>
					</a>
				</div>

				<div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
					<img src="https://cdn.oppyo.com/launches/viddeyo/special-bonus/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
				</div>

				<div class="col-12 mt15 mt-md20" align="center">
					<h3 class="f-md-35 f-md-28 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
				</div>

				<!-- Timer -->
				<div class="col-md-8 mx-auto col-md-10 col-12 text-center">
					<div class="countdown counter-white white-clr">
						<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Days</span> </div>
						<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
						<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
						<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
					</div>
				</div>
				<!-- Timer End -->
			</div>
		</div>
	</div>
	<!-- CTA Button Section End   -->


	<!-- Bonus #6 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
				<div class="col-12 xstext1">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">BONUS 6</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 col-12 order-md-2">
					  		<img src="https://cdn.oppyo.com/launches/viddeyo/special-bonus/bonus6.webp" class="img-fluid mx-auto d-block " alt="Bonus6">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
					  		<div class="f-22 f-md-32 w600 lh150 bonus-title-color">
							  Branding Secrets

					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
								<li>
									Want to know about secrets behind building a brand that people love? Here you will find out all the tips, techniques and exact steps to build your brand and develop a perfect marketing strategy.
								</li>
								<li>
									While using Viddeyo for video marketing, integrate this with your overall marketing strategy & boost your brand building efforts in the long run. 
								</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #6 End -->

	<!-- Bonus #7 Start -->
	<div class="section-bonus">
	   	<div class="container">
		 	<div class="row">
			 	<div class="col-12 text-center">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">BONUS 7</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 col-12">
					  		<img src="https://cdn.oppyo.com/launches/viddeyo/special-bonus/bonus7.webp" class="img-fluid mx-auto d-block " alt="Bonus7">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
					  		<div class="f-22 f-md-32 w600 lh150 bonus-title-color">
							  Turbo GIF Animator
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
								<li>
									Every marketer knows how powerful graphics are in promoting products or services online. But creating them is not everyone’s cup of tea.
								</li>			
								<li>
									So to bail you out from this, we’re providing this useful tool using which you can easily create animated images in GIF format.
								</li>	
								<li>
									Now use this amazing tool along with Viddeyo to intensify your business growth like never before.
								</li>		
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #7 End -->

	<!-- Bonus #8 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-12 xstext1">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">BONUS 8</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 order-md-2 col-12">
					  		<img src="https://cdn.oppyo.com/launches/viddeyo/special-bonus/bonus8.webp" class="img-fluid mx-auto d-block " alt="Bonus8">
				   		</div>
				   		<div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
					  		<div class="f-22 f-md-32 w600 lh150 bonus-title-color">
							  Public Speaking
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
								<li>
									Public Speaking is all about magnetize and amaze your audience. It easily lives up to its name when it comes to learning about this hot topic, on demand among Niche Marketers.
								</li>
								<li>
									So, here’s an exclusive package that makes public speaking a reality for all introverts and helps them break the stereotype. When used with Viddeyo, this package will surely become a top-notch business booster.
								</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #8 End -->

	<!-- Bonus #9 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-12 text-center">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">BONUS 9</div>
					</div>
				</div>
				<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
						<div class="col-md-5 col-12">
							<img src="https://cdn.oppyo.com/launches/viddeyo/special-bonus/bonus9.webp" class="img-fluid mx-auto d-block " alt="Bonus9">
						</div>
						<div class="col-md-7 col-12 mt20 mt-md0">
							<div class="f-22 f-md-32 w600 lh150 bonus-title-color">
								The Animation Playbook
							</div>
							<ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
								<li>
									With this product you will discover the exact process to create multiple kinds of video, multiple stories and multiple scenarios. Here are the simple steps to creating animated videos to attract new customers and prospects.
								</li>
								<li>
									This package is a must have and when combined with powerful video hosting & marketing technology Viddeyo, reaps great results for you in the long run.
								</li>
							</ul>
						</div>
					</div>
				</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #9 End -->

	<!-- Bonus #10 start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-12 xstext1">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">BONUS 10</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 order-md-2 col-12">
					  		<img src="https://cdn.oppyo.com/launches/viddeyo/special-bonus/bonus10.webp" class="img-fluid mx-auto d-block " alt="Bonus10">
				   		</div>
				   		<div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
					  		<div class="f-22 f-md-32 w600 lh150 bonus-title-color">
								Video Launch Method
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
							  	<li>
								  	Video product launches are the go-to for the world’s best brands. Here’s the guide that reveals the best techniques used in the successful product launch videos to improve brand positioning.
								</li>
								<li>
									By combining this package with powerful Viddeyo, you will reap out maximum benefits from your video launches.
								</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #10 End -->


	<!-- CTA Button Section Start -->
	<div class="dark-cta-sec">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-md-12 col-12 text-center ">
					<div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
					<div class="f-md-36 f-22 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
					<div class="f-18 f-md-22 lh150 w400 text-center mt20 white-clr">
					Use Coupon Code <span class="w700 white-clr">“VIDDEYO”</span> for an Additional <span class="w700 white-clr">10% Discount</span> on Commercial Licence
				</div>
				</div>

				<div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
					<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
						<span class="text-center">Grab Viddeyo + My 20 Exclusive Bonuses</span> <br>
					</a>
				</div>

				<div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
					<img src="https://cdn.oppyo.com/launches/viddeyo/special-bonus/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
				</div>

				<div class="col-12 mt15 mt-md20" align="center">
					<h3 class="f-md-35 f-md-28 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
				</div>

				<!-- Timer -->
				<div class="col-md-8 mx-auto col-md-10 col-12 text-center">
					<div class="countdown counter-white white-clr">
						<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Days</span> </div>
						<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
						<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
						<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
					</div>
				</div>
				<!-- Timer End -->
			</div>
		</div>
	</div>
	<!-- CTA Button Section End -->


	<!-- Bonus #11 start -->
	<div class="section-bonus">
	   	<div class="container">
		 	<div class="row">
			 	<div class="col-12 text-center">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">BONUS 11</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 col-12">
					  		<img src="https://cdn.oppyo.com/launches/viddeyo/special-bonus/bonus11.webp" class="img-fluid mx-auto d-block " alt="Bonus11">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
					  		<div class="f-22 f-md-32 w600 lh150 bonus-title-color">
							  	60 Photoshop Action Scripts
					  		</div>
					 		<ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
								<li>Are you a blogger, affiliate marketer or an email marketing expert, then you must offer a lead magnet to make your visitors join your email list? </li>
								<li>And its no secret that attractive visuals grab your visitors eye and get them hooked. So, here’s an easy to use package that creates a 3D eBook cover design in just a few clicks. </li>
								<li>Now stop thinking and use this with video hosting &amp; marketing prowess of Viddeyo to take your business to the next level.</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #11 End -->


	<!-- Bonus #12 Start -->
	<div class="section-bonus">
	   	<div class="container">
		 	<div class="row">
			 	<div class="col-12 xstext1">
					<div class="bonus-title-bg">
						<div class="f-22 f-md-28 lh120 w700">BONUS 12</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 order-md-2 col-12">
					  		<img src="https://cdn.oppyo.com/launches/viddeyo/special-bonus/bonus12.webp" class="img-fluid mx-auto d-block " alt="Bonus12">
				   		</div>
				   		<div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
					  		<div class="f-22 f-md-32 w600 lh150 bonus-title-color">
							  	300 Logo Templates-
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
								<li>Branding is critical for every marketer and if overlooked, it proves to be fatal in the long run. But if you’re not a graphic designer, chances are you may have to invest tons of time and money for the same. </li>
								<li>So to bail you out from this mess, we’re giving this tool that helps to build bundle of logo templates that you can use today for your own product to your clients or resell it. </li>
								<li>Now all you need to do is create your own brand and use video hosting champ Viddeyo to zoom past your competitors at will.</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #12 End -->

	<!-- Bonus #13 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-12 text-center">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">BONUS 13</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 col-12">
					  		<img src="https://cdn.oppyo.com/launches/viddeyo/special-bonus/bonus13.webp" class="img-fluid mx-auto d-block " alt="Bonus13">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
					  		<div class="f-22 f-md-32 w600 lh150 bonus-title-color">
							  Animated Pop Over Window Generator-
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
							  <li>To be successful, you must grab attention of your visitors and convert them into lifetime buyers. </li>
							  <li>So we’re giving you this useful software that creates a window that slides over your current window, when the open link is clicked. </li>
							  <li>This bonus when combined with Viddeyo, becomes an ultimate growth booster for business owners.</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #13 End -->

	<!-- Bonus #14 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-12 xstext1">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">BONUS 14</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 order-md-2 col-12">
					  		<img src="https://cdn.oppyo.com/launches/viddeyo/special-bonus/bonus14.webp" class="img-fluid mx-auto d-block " alt="Bonus14">
				   		</div>
				   		<div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
					  		<div class="f-22 f-md-32 w600 lh150 bonus-title-color">
							  Abstract Image Collection V4-
							</div>
					  		<ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
							  <li>There’s no secret that abstract images are the best way to capture audience eyeballs &amp; get them lured to your offers. But creating them is a big pain and can’t be done easily.</li>
							  <li>Fortunately, we’re providing this pack that includes bundle of abstract digital graphics that you can use right away. This bonus when combined with Viddeyo proves to be a great resource for every success hungry marketer.</li>	
							</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #14 End -->

	

	<!-- Bonus #15 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-12 text-center">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">BONUS 15</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 col-12">
					  		<img src="https://cdn.oppyo.com/launches/viddeyo/special-bonus/bonus15.webp" class="img-fluid mx-auto d-block " alt="Bonus15">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
					  		<div class="f-22 f-md-32 w600 lh150 bonus-title-color">
							  Instant Video Suite-
							</div>
					  		<ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
							  <li>Creating multi-media streaming video presentations is used by all top brands today in order to scale their profits to the next level. Wondering how you can do the same, time to breathe a sigh of relief.</li>
							  <li>Every visitor with stunning, top-notch multi-media streaming video presentations and convert each of them into buying customers. So, get in active mode and use this bonus with Viddeyo to intensify your growth prospects like never before.</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #15 End -->


	<!-- CTA Button Section Start -->
	<div class="dark-cta-sec">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-md-12 col-12 text-center ">
					<div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
					<div class="f-md-36 f-22 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
					<div class="f-18 f-md-22 lh150 w400 text-center mt20 white-clr">
					Use Coupon Code <span class="w700 white-clr">“VIDDEYO”</span> for an Additional <span class="w700 white-clr">10% Discount</span> on Commercial Licence
				</div>
				</div>

				<div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
					<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
						<span class="text-center">Grab Viddeyo + My 20 Exclusive Bonuses</span> <br>
					</a>
				</div>

				<div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
					<img src="https://cdn.oppyo.com/launches/viddeyo/special-bonus/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
				</div>

				<div class="col-12 mt15 mt-md20" align="center">
					<h3 class="f-md-35 f-md-28 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
				</div>

				<!-- Timer -->
				<div class="col-md-8 mx-auto col-md-10 col-12 text-center">
					<div class="countdown counter-white white-clr">
						<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Days</span> </div>
						<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
						<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
						<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
					</div>
				</div>
				<!-- Timer End -->
			</div>
		</div>
	</div>
	<!-- CTA Button Section End -->
	
	<!-- Bonus #16 Start -->
	<div class="section-bonus">
	   	<div class="container">
		 	<div class="row">
			 	<div class="col-12 xstext1">
					<div class="bonus-title-bg">
						<div class="f-22 f-md-28 lh120 w700">BONUS 16</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 order-md-2 col-12">
					  		<img src="https://cdn.oppyo.com/launches/viddeyo/special-bonus/bonus16.webp" class="img-fluid mx-auto d-block " alt="Bonus16">
				   		</div>
				   		<div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
					  		<div class="f-22 f-md-32 w600 lh150 bonus-title-color">
							  Podcasting Made Easy-
							</div>
					  		<ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
							  <li>The Power Of Podcasting is not unknown in the marketing scenario today. So if you also wanted to use its immense power, you’re at the right place.</li>
							  <li>With this 5-day crash course, you too can record your podcast &amp; broadcast to a growing audience in several ways. Now stop thinking and use the growing power of podcasts to fuel your growth prospects.</li>
							</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #16 End -->

	<!-- Bonus #17 Start -->
	<div class="section-bonus">
	  	<div class="container">
		  	<div class="row">
			 	<div class="col-12 text-center">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">BONUS 17</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 col-12">
					  		<img src="https://cdn.oppyo.com/launches/viddeyo/special-bonus/bonus17.webp" class="img-fluid mx-auto d-block " alt="Bonus17">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
					  		<div class="f-22 f-md-32 w600 lh150 bonus-title-color">
							  Videos For Profit-
							</div>
					  		<ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
							  <li>Every business whether online or offline uses videos to communicate with their clients.</li>
							  <li>Creating amazing YouTube videos will finally be a reality now as this 31-part video course helps to make powerful demos, presentation and tutorials with just a smartphone, your Mac &amp; affordable tools.</li>
							  <li>When used with the video marketing powers of Viddeyo, this package will surely become a top-notch business booster.</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #17 End -->

	<!-- Bonus #18 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-12 xstext1">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">BONUS 18</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 order-md-2 col-12">
					  		<img src="https://cdn.oppyo.com/launches/viddeyo/special-bonus/bonus18.webp" class="img-fluid mx-auto d-block " alt="Bonus18">
				   		</div>
				   		<div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
					  		<div class="f-22 f-md-32 w600 lh150 bonus-title-color">
							  List Building Videos-
							</div>
					  		<ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
							  <li>Building an email list is critical for every successful marketer. So, checkout this exciting package that streamlines the entire path how to build your first profitable list &amp; use it to boost your affiliate commissions like a pro. </li>
							  <li>This package is a sure shot success booster and will give great results when combined with Viddeyo, it becomes a lethal combination to fuel your online business growth.</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #18 End -->

	<!-- Bonus #19 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-12 text-center">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">BONUS 19</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				  		<div class="col-md-5 col-12">
					  		<img src="https://cdn.oppyo.com/launches/viddeyo/special-bonus/bonus19.webp" class="img-fluid mx-auto d-block " alt="Bonus19">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
					 	 	<div class="f-22 f-md-32 w600 lh150 bonus-title-color">
							  Latest Humans Stock Images-
							</div>
					  		<ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
							  <li>Human stock images have proven their worth and are widely used by marketers to garner higher audience attention without breaking their bank. </li>
							  <li>So, here’s an exciting package that includes package of stock photos that you can use for your marketing activities today.</li>
							  <li>Now combine this package with countless benefits that you get with Viddeyo and take your profits to the next level.</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #19 End -->

	<!-- Bonus #20 start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-12 xstext1">
					<div class="bonus-title-bg">
				   		<div class="f-22 f-md-28 lh120 w700">BONUS 20</div>
					</div>
			 	</div>
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-md-5 order-md-2 col-12">
					  		<img src="https://cdn.oppyo.com/launches/viddeyo/special-bonus/bonus20.webp" class="img-fluid mx-auto d-block " alt="Bonus20">
				   		</div>
				   		<div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
					  		<div class="f-22 f-md-32 w600 lh150 bonus-title-color">
							  Web Video Production-
							</div>
					  		<ul class="bonus-list f-18 f-md-20 w400 lh150 mt20 mt-md30 p0">
							  <li>It’s no scientific fact that videos are the best way to convey your marketing messages to a globally scattered audience in short span of time.</li>
							  <li>So, here are giving a very useful package that turns your “average” video into a professional-quality masterpiece that entices maximum audience.</li>
							  <li>So take action &amp; use this bonus with Viddeyo to become an enviable success story.</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #20 End -->

	<!-- Huge Woth Section Start -->
	<div class="huge-area mt30 mt-md10">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-12 text-center">
                    <div class="f-md-65 f-40 lh120 w700 white-clr">That’s Huge Worth of</div>
                    <br>
                    <div class="f-md-60 f-40 lh120 w800 gradient-clr">$2895!</div>
                </div>
            </div>
        </div>
    </div>
	<!-- Huge Worth Section End -->

	<!-- text Area Start -->
	<div class="white-section pb0">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-12 text-center">
                	<div class="f-md-36 f-25 lh140 w500">So what are you waiting for? You have a great opportunity <br class="hidden-xs"> ahead + <span class="w700 blue-clr">My 20 Bonus Products</span> are making it a <br class="hidden-xs"> <span class="w700 blue-clr">completely NO Brainer!!</span></div>
            	</div>
			</div>
		</div>
	</div>
	<!-- text Area End -->

	<!-- CTA Button Section Start -->
	<div class="cta-btn-section">
	   	<div class="container">
		 	<div class="row">
			 	<div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center">
					<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
						<span class="text-center">Grab Viddeyo + My 20 Exclusive Bonuses</span> <br>
					</a>
			 	</div>
			 
				<div class="col-12 mt15 mt-md20" align="center">
					<h3 class="f-md-30 f-20 w500 text-center black">My Exclusive Bonuses Are Expiring in...</h3>
				</div>
				<!-- Timer -->
				<div class="col-md-8 col-md-10 mx-auto col-12 text-center">
					<div class="countdown counter-black">
						<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
						<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
						<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
						<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
					</div>
				</div>
				<!-- Timer End -->
		  	</div>
	   	</div>
	</div>
	<!-- CTA Button Section End -->

	
	
	<!-- Footer Section Start -->
	<div class="footer-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 909.75 162.72" style="enable-background:new 0 0 909.75 162.72; max-height:45px;" xml:space="preserve">
                     <style type="text/css">
                        .st00{fill:#FFFFFF;}
                        .st11{fill:url(#SVGID_1_);}
                     </style>
                     <g>
                        <g>
                           <path class="st00" d="M216.23,71.07L201.6,97.26l-35.84-64.75l-3.16-5.7h29.13l0.82,1.49L216.23,71.07z M288.35,26.81l-3.16,5.7
                              l-56.4,103.68h-6.26l-11.71-22.25l14.63-26.18l0.01,0.02l2.73-4.94l4.81-8.68l25.38-45.87l0.82-1.49H288.35z"/>
                           <path class="st00" d="M329.85,26.61v109.57h-23.23V26.61H329.85z"/>
                           <path class="st00" d="M454.65,81.7c0,2.7-0.2,5.41-0.59,8.04c-3.58,24.36-23.15,43.45-47.59,46.42l-0.26,0.03h-50.93v-69.9h23.23
                              v46.67h26.13c12.93-2,23.4-11.91,26.09-24.74c0.45-2.14,0.67-4.33,0.67-6.51c0-2.11-0.21-4.22-0.62-6.26
                              c-1.52-7.54-5.76-14.28-11.95-18.97c-3.8-2.88-8.14-4.84-12.75-5.78c-1.58-0.32-3.19-0.52-4.83-0.6h-45.98V26.82L403.96,27
                              c11.89,0.9,23.21,5.67,32.17,13.61c9.78,8.65,16.15,20.5,17.96,33.36C454.46,76.49,454.65,79.1,454.65,81.7z"/>
                           <path class="st00" d="M572.18,81.7c0,2.7-0.2,5.41-0.59,8.04c-3.58,24.36-23.15,43.45-47.59,46.42l-0.26,0.03h-50.93v-69.9h23.23
                              v46.67h26.13c12.93-2,23.4-11.91,26.09-24.74c0.45-2.14,0.67-4.33,0.67-6.51c0-2.11-0.21-4.22-0.62-6.26
                              c-1.52-7.54-5.76-14.28-11.95-18.97c-3.8-2.88-8.14-4.84-12.75-5.78c-1.58-0.32-3.19-0.52-4.83-0.6h-45.98V26.82L521.5,27
                              c11.89,0.9,23.2,5.67,32.17,13.61c9.78,8.65,16.15,20.5,17.96,33.36C572,76.49,572.18,79.1,572.18,81.7z"/>
                           <path class="st00" d="M688.08,26.82v23.23h-97.72V31.18l0.01-4.36H688.08z M613.59,112.96h74.49v23.23h-97.72v-18.87l0.01-4.36
                              V66.1l23.23,0.14h64.74v23.23h-64.74V112.96z"/>
                           <path class="st00" d="M808.03,26.53l-5.07,6.93l-35.84,48.99l-1.08,1.48L766,83.98l-0.04,0.05l0,0.01l-0.83,1.14v51h-23.23V85.22
                              l-0.86-1.18l-0.01-0.01l-0.04-0.05l-0.04-0.06l-1.08-1.48l-35.84-48.98l-5.07-6.93h28.77l1.31,1.78l24.46,33.43l24.46-33.43
                              l1.31-1.78H808.03z"/>
                           <path class="st00" d="M909.75,81.69c0,30.05-24.45,54.49-54.49,54.49c-30.05,0-54.49-24.45-54.49-54.49S825.2,27.2,855.25,27.2
                              C885.3,27.2,909.75,51.65,909.75,81.69z M855.25,49.95c-17.51,0-31.75,14.24-31.75,31.75s14.24,31.75,31.75,31.75
                              c17.51,0,31.75-14.24,31.75-31.75C887,64.19,872.76,49.95,855.25,49.95z"/>
                        </g>
                     </g>
                     <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="0" y1="81.3586" x2="145.8602" y2="81.3586">
                        <stop  offset="0.4078" style="stop-color:#FFFFFF"/>
                        <stop  offset="1" style="stop-color:#FC6DAB"/>
                     </linearGradient>
                     <path class="st11" d="M11.88,0c41.54,18.27,81.87,39.12,121.9,60.41c7.38,3.78,12.24,11.99,12.07,20.32
                        c0.03,7.94-4.47,15.72-11.32,19.73c-27.14,17.07-54.59,33.74-82.21,50c-0.57,0.39-15.29,8.93-15.47,9.04
                        c-1.4,0.77-2.91,1.46-4.44,1.96c-14.33,4.88-30.25-4.95-32.19-19.99c-0.18-1.2-0.23-2.5-0.24-3.72c0.35-25.84,1.01-53.01,2.04-78.83
                        C2.57,42.41,20.3,32.08,34.79,40.31c8.16,4.93,16.31,9.99,24.41,15.03c2.31,1.49,7.96,4.89,10.09,6.48
                        c5.06,3.94,7.76,10.79,6.81,17.03c-0.68,4.71-3.18,9.07-7.04,11.77c-0.38,0.24-1.25,0.88-1.63,1.07c-3.6,2.01-7.5,4.21-11.11,6.17
                        c-9.5,5.22-19.06,10.37-28.78,15.27c11.29-9.81,22.86-19.2,34.53-28.51c3.54-2.55,4.5-7.53,1.83-10.96c-0.72-0.8-1.5-1.49-2.49-1.9
                        c-0.18-0.08-0.42-0.22-0.6-0.29c-11.27-5.17-22.82-10.58-33.98-15.95c0,0-0.22-0.1-0.22-0.1l-0.09-0.02l-0.18-0.04
                        c-0.21-0.08-0.43-0.14-0.66-0.15c-0.11-0.01-0.2-0.07-0.31-0.04c-0.21,0.03-0.4-0.04-0.6,0.03c-0.1,0.02-0.2,0.02-0.29,0.03
                        c-0.14,0.06-0.28,0.1-0.43,0.12c-0.13,0.06-0.27,0.14-0.42,0.17c-0.09,0.03-0.17,0.11-0.26,0.16c-0.08,0.06-0.19,0.06-0.26,0.15
                        c-0.37,0.25-0.66,0.57-0.96,0.9c-0.1,0.2-0.25,0.37-0.32,0.59c-0.04,0.08-0.1,0.14-0.1,0.23c-0.1,0.31-0.17,0.63-0.15,0.95
                        c-0.06,0.15,0.04,0.35,0.02,0.52c0,0.02,0,0.08-0.01,0.1c0,0,0,0.13,0,0.13l0.02,0.51c0.94,24.14,1.59,49.77,1.94,73.93
                        c0,0,0.06,4.05,0.06,4.05l0,0.12l0.01,0.02l0.01,0.03c0,0.05,0.02,0.11,0.02,0.16c0.08,0.23,0.16,0.29,0.43,0.47
                        c0.31,0.16,0.47,0.18,0.71,0.09c0.06-0.04,0.11-0.06,0.18-0.1c0.02-0.01-0.01,0.01,0.05-0.03l0.22-0.13l0.87-0.52
                        c32.14-19.09,64.83-37.83,97.59-55.81c0,0,0.23-0.13,0.23-0.13c0.26-0.11,0.51-0.26,0.69-0.42c1.15-0.99,0.97-3.11-0.28-4.01
                        c0,0-0.43-0.27-0.43-0.27l-1.7-1.1C84.73,51.81,47.5,27.03,11.88,0L11.88,0z"/>
                  </svg>
                  <div editabletype="text" class="f-16 f-md-16 w300 mt20 lh140 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-16 f-md-16 w300 lh140 white-clr text-xs-center">Copyright © Viddeyo</div>
                  <ul class="footer-ul w300 f-16 f-md-16 white-clr text-center text-md-right">
                     <li><a href="https://support.oppyo.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://viddeyo.co/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://viddeyo.co/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://viddeyo.co/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://viddeyo.co/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://viddeyo.co/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://viddeyo.co/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!--Footer Section End -->


	<!-- timer --->
  	<?php
	 	if ($now < $exp_date) {
	?>

  	<script type="text/javascript">
		// Count down milliseconds = server_end - server_now = client_end - client_now
		var server_end = <?php echo $exp_date; ?> * 1000;
		var server_now = <?php echo time(); ?> * 1000;
		var client_now = new Date().getTime();
		var end = server_end - server_now + client_now; // this is the real end time
		
		var noob = $('.countdown').length;
		
		var _second = 1000;
		var _minute = _second * 60;
		var _hour = _minute * 60;
		var _day = _hour * 24
		var timer;
		
		function showRemaining() {
		var now = new Date();
		var distance = end - now;
		if (distance < 0) {
			clearInterval(timer);
			document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
			return;
		}
		
		var days = Math.floor(distance / _day);
		var hours = Math.floor((distance % _day) / _hour);
		var minutes = Math.floor((distance % _hour) / _minute);
		var seconds = Math.floor((distance % _minute) / _second);
		if (days < 10) {
			days = "0" + days;
		}
		if (hours < 10) {
			hours = "0" + hours;
		}
		if (minutes < 10) {
			minutes = "0" + minutes;
		}
		if (seconds < 10) {
			seconds = "0" + seconds;
		}
		var i;
		var countdown = document.getElementsByClassName('countdown');
		for (i = 0; i < noob; i++) {
			countdown[i].innerHTML = '';
		
			if (days) {
				countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + days + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 smmltd">Days</span> </div>';
			}
		
			countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + hours + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>';
		
			countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">' + minutes + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>';
		
			countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">' + seconds + '</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>';
		}
		
		}
		timer = setInterval(showRemaining, 1000);
  	</script>
  	<?php
	 	} else {
	 	echo "Times Up";
	 	}
	?>
  <!--- timer end-->
</body>
</html>

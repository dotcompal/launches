<html>

<head>
    <title>Intellimate JV Invite</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=9">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&family=Play:wght@400;700&family=Plus+Jakarta+Sans:ital,wght@0,200;0,300;0,400;0,500;0,600;0,700;0,800;1,200;1,300;1,400;1,500;1,600;1,700;1,800&display=swap" rel="stylesheet">
    <meta name="description" content="Promote this Cutting-Edge Academy Builder That Lets You Create & Sell Courses Online in Just 7 Minutes">
    <meta property="og:image" content="https://www.intellimate.co/jv/thumbnail.png">
    <meta name="language" content="English">
    <meta name="revisit-after" content="1 days">
    <meta name="author" content="Ayush Jain">

    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:title" content="Intellimate JV Invite">
    <meta property="og:description" content="Promote this Cutting-Edge Academy Builder That Lets You Create & Sell Courses Online in Just 7 Minutes">
    <meta property="og:image" content="https://www.intellimate.co/jv/thumbnail.png">

    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:title" content="Intellimate JV Invite">
    <meta property="twitter:description" content="Promote this Cutting-Edge Academy Builder That Lets You Create & Sell Courses Online in Just 7 Minutes">
    <meta property="twitter:image" content="https://www.intellimate.co/jv/thumbnail.png">
    <!-- Start Editor required -->
    <link rel="stylesheet" href="../common_assets/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="../common_assets/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="../common_assets/css/general.css">
    <link rel="stylesheet" href="assets/css/style.css" type="text/css">
    <link rel="stylesheet" href="assets/css/timer.css" type="text/css">
    <script src="../common_assets/js/jquery.min.js"></script>
    <script src="../common_assets/js/popper.min.js"></script>
    <script src="../common_assets/js/bootstrap.min.js"></script>

    <script type="text/javascript">
        // Special handling for in-app browsers that don't always support new windows
        (function() {
            function browserSupportsNewWindows(userAgent) {
                var rules = [
                    'FBIOS',
                    'Twitter for iPhone',
                    'WebView',
                    '(iPhone|iPod|iPad)(?!.*Safari\/)',
                    'Android.*(wv|\.0\.0\.0)'
                ];
                var pattern = new RegExp('(' + rules.join('|') + ')', 'ig');
                return !pattern.test(userAgent);
            }

            if (!browserSupportsNewWindows(navigator.userAgent || navigator.vendor || window.opera)) {
                document.getElementById('af-form-1086371786').parentElement.removeAttribute('target');
            }
        })();
    </script>
    <script type="text/javascript">
        (function() {
            var IE = /*@cc_on!@*/ false;
            if (!IE) {
                return;
            }
            if (document.compatMode && document.compatMode == 'BackCompat') {
                if (document.getElementById("af-form-1086371786")) {
                    document.getElementById("af-form-1086371786").className = 'af-form af-quirksMode';
                }
                if (document.getElementById("af-body-1086371786")) {
                    document.getElementById("af-body-1086371786").className = "af-body inline af-quirksMode";
                }
                if (document.getElementById("af-header-1086371786")) {
                    document.getElementById("af-header-1086371786").className = "af-header af-quirksMode";
                }
                if (document.getElementById("af-footer-1086371786")) {
                    document.getElementById("af-footer-1086371786").className = "af-footer af-quirksMode";
                }
            }
        })();
    </script>
</head>

<body>

<!-- New Timer  Start-->
<?php
         $date = 'December 15 2023 10:00 AM EST';
         $exp_date = strtotime($date);
         $now = time();  
         /*
         
         $date = date('F d Y g:i:s A eO');
         $rand_time_add = rand(700, 1200);
         $exp_date = strtotime($date) + $rand_time_add;
         $now = time();*/
         
         if ($now < $exp_date) {
         ?>
      <?php
         } else {
          echo "Times Up";
         }
         ?>
      <!-- New Timer End -->

    <!-- Header Section Start -->
    <div class="header-section">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-3 col-12 text-center">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 742.13 152.79" style="max-height: 100px;"><defs><style>.cls-1{fill:#fff;}.cls-2{fill:url(#linear-gradient);}.cls-3{fill:#21a1ff;}.cls-4{fill:#2ddfff;}.cls-5{fill:url(#linear-gradient-2);}.cls-6{fill:#f1f1f2;}</style><linearGradient id="linear-gradient" x1="667.64" y1="93.3" x2="726.15" y2="93.3" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#2ee2ff"/><stop offset="1" stop-color="#219fff"/></linearGradient><linearGradient id="linear-gradient-2" x1="23.17" y1="76.39" x2="157.15" y2="76.39" xlink:href="#linear-gradient"/></defs><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1" d="M220,127.18H210V59.43h10Z"/><path class="cls-1" d="M264.32,103.36a14.48,14.48,0,1,0-28.95,0v23.82H226V103.36a23.83,23.83,0,1,1,47.65,0v23.82h-9.35Z"/><path class="cls-1" d="M289.05,89v19.2a9.7,9.7,0,0,0,9.75,9.75h4.53v9.35H298.8a19.1,19.1,0,0,1-19.1-19.1V67.37h9.35V79.63h14.28V89Z"/><path class="cls-1" d="M319.61,108.48a15.43,15.43,0,0,0,27,4h10.56a24.77,24.77,0,1,1,1.81-9.14,21.23,21.23,0,0,1-.61,5.12h-38.8Zm29.45-9.35a15.48,15.48,0,0,0-29.85,0Z"/><path class="cls-1" d="M374.4,108a9.78,9.78,0,0,0,9.75,9.85h4.52v9.35h-4.52a19.18,19.18,0,0,1-19.1-19.2V67.17h9.35Z"/><path class="cls-1" d="M404.05,108a9.78,9.78,0,0,0,9.75,9.85h4.52v9.35H413.8A19.18,19.18,0,0,1,394.7,108V67.17h9.35Z"/><path class="cls-1" d="M433.71,127.18h-9.35V79.63h9.35Z"/><path class="cls-1" d="M496.23,77.42l-23.32,24.83L449.69,77.42v49.76h-9.95V59.43h6.63l26.54,28.24,26.64-28.24h6.63v67.75h-9.95Z"/><path class="cls-1" d="M559.86,127.18h-9.35V103.36A14.48,14.48,0,1,0,536,117.83v9.35a23.88,23.88,0,1,1,23.82-23.82Z"/><path class="cls-1" d="M575.24,89v19.2a9.7,9.7,0,0,0,9.75,9.75h4.53v9.35H585a19.1,19.1,0,0,1-19.1-19.1V67.37h9.35V79.63h14.28V89Z"/><path class="cls-1" d="M605.8,108.48a15.43,15.43,0,0,0,27,4h10.55a24.77,24.77,0,1,1,1.81-9.14,21.74,21.74,0,0,1-.6,5.12H605.8Zm29.45-9.35a15.48,15.48,0,0,0-29.85,0Z"/><path class="cls-2" d="M693.48,59.43h6.83l25.84,67.75H715.49l-18.6-48.85L678.3,127.18H667.64Z"/><path class="cls-3" d="M742.13,127.18h-10V59.43h10Z"/><path class="cls-3" d="M161,69.48c0,11.86,0,22.69,0,34,3.94,1,10.59-1.4,14.61-6.34A17.06,17.06,0,0,0,177.32,78,16.86,16.86,0,0,0,161,69.48Z"/><path class="cls-4" d="M18.5,69.48c0,11.86,0,22.69,0,34-3.94,1-10.6-1.4-14.61-6.34A17,17,0,0,1,2.17,78,16.84,16.84,0,0,1,18.5,69.48Z"/><path class="cls-1" d="M90.13,118.12a23.33,23.33,0,0,1-16.67-6.7,2.78,2.78,0,0,1-.12-4.15,2.85,2.85,0,0,1,4.09.09,18.1,18.1,0,0,0,25.42,0,2.83,2.83,0,0,1,4.16-.13,2.78,2.78,0,0,1-.22,4.15,23,23,0,0,1-13.27,6.52c-.58.07-1.16.14-1.74.18S90.68,118.12,90.13,118.12Z"/><path class="cls-1" d="M66.14,76.4c-4.22,0-7.84,1.86-10.89,5.46a2.66,2.66,0,0,0-.08,3.38,1.62,1.62,0,0,0,2.67-.08c5.48-4.64,11.8-4.9,16.61,0a1.62,1.62,0,0,0,2.72.1A2.65,2.65,0,0,0,77,81.86a14.1,14.1,0,0,0-8.67-5.31,10.57,10.57,0,0,0-1.13-.14C66.86,76.38,66.5,76.4,66.14,76.4Z"/><path class="cls-1" d="M114.15,76.4c-4.22,0-7.84,1.86-10.89,5.46a2.64,2.64,0,0,0-.08,3.38,1.62,1.62,0,0,0,2.67-.08c5.47-4.64,11.8-4.9,16.61,0a1.61,1.61,0,0,0,2.71.1,2.65,2.65,0,0,0-.14-3.38,14.13,14.13,0,0,0-8.67-5.31,10.89,10.89,0,0,0-1.13-.14C114.87,76.38,114.51,76.4,114.15,76.4Z"/><path class="cls-5" d="M124.12,39.68h-1.39l5.74-20.2a9.79,9.79,0,1,0-5.88-1.61L116.4,39.68H63.93l-6.2-21.81a9.75,9.75,0,1,0-5.87,1.61l5.75,20.2H56.2a33.07,33.07,0,0,0-33,33v47a33.08,33.08,0,0,0,33,33h67.92a33.08,33.08,0,0,0,33-33v-47A33.07,33.07,0,0,0,124.12,39.68Zm5.74-33.76a2.76,2.76,0,1,1,1.94,3.4A2.78,2.78,0,0,1,129.86,5.92ZM48.52,9.32a2.76,2.76,0,1,1,1.94-3.38A2.75,2.75,0,0,1,48.52,9.32Zm89.19,110.43a13.61,13.61,0,0,1-13.59,13.6H56.2a13.61,13.61,0,0,1-13.6-13.6v-47a13.61,13.61,0,0,1,13.6-13.6h67.92a13.61,13.61,0,0,1,13.59,13.6Z"/><circle class="cls-6" cx="429.03" cy="70.88" r="4.75"/></g></g></svg>            
                </div>
      
                <div class="col-md-9 mt15 mt-md7">
                    <ul class="leader-ul f-20 w400 white-clr text-md-end text-center ">
                        <li>
                            <a href="https://docs.google.com/document/d/1gMeSUVGTf6kOLpgNS7b8YE_qRw4xFeSu/edit" target="_blank">JV Doc</a>
                        </li>
                        <li>
                            <a href="https://docs.google.com/document/d/1-ldOBgBQ3bFsrxSkM_af514SFRIuTyct/edit" target="_blank">Swipes</a>
                        </li>
                        <li>
                            <a href="#live-sec" class="affiliate-link-btn ml-md15 mt10 mt-md0"> Grab Your Affiliate Link</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row mt20 mt-md50">
                <div class="col-12 text-center">
                    <div class="pre-heading f-16 f-md-22 w500 lh140 white-clr">
                        Meet Your New Director of Client Engagement & Sales Optimization
                    </div>
                </div>

                <div class="col-12 f-md-50 f-28 w700 lh140 mt50 mt-md70 relative play">
                    <div class="gametext d-md-block white-clr">
                        Revolutionary
                    </div>
                    <div class="mainheadline text-center">
                        Self-Learning App Turns Any URL, Website or Doc File into an AI-Powered Chatbot… Working 24/7 to 
                        <span class="blue-gradient"></span> 
                    </div>
                </div>

                <div class="col-12 mt20 mt-md40 text-center">
                    <div class="f-16 f-md-22 w500 white-clr post-headline">
                        A HUGE Opportunity for Marketers, Agencies, Freelancers, & Entrepreneurs… 
                    </div>
                </div>
                <div class="col-12 mt-md30 mt20">
                    <div class="f-18 f-md-24 w500 text-center lh150 white-clr">
                       <span class="red-clr">No</span> Monthly Fee || <span class="red-clr">No</span> Freelancer || <span class="red-clr">No</span> Tech Skills || <span class="red-clr">No</span> Manual Work
                    </div>
                </div>

            </div>
            <div class="row mt20 mt-md50 align-items-center">
                <div class="col-md-8 min-md-video-width-left">
                    <!-- <div class="">
                        <img src="assets/images/video-thumb.webp" class="img-fluid d-block mx-auto">
                    </div> -->

                    <div class="responsive-video">
                        <iframe src="https://intellimate.dotcompal.com/video/embed/rb1d7ll1t8" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                    </div>
                </div>

                <div class="col-md-4 mt20 mt-md0 min-md-video-width-right">
                    <div class="launch-start-bg">
                        <div class="d-flex gap20 justify-content-center">
                            <img src="assets/images/airoplane.webp" alt="Airoplane" class="mx-auto d-block img-fluid">
                        </div>
                        <div class="mt20 f-36 f-md-50 w700 text-center lh150 white-clr date-sec play">
                            <span class="f-md-120 ">15</span><sup class="f-md-38 f28">th</sup> Dec
                        </div>
                        <div class="mt10 f-18 f-md-22 w500 text-center lh150 white-clr text-uppercase">
                           <span class="mr10"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                            <g clip-path="url(#clip0_6_6325)">
                              <path d="M12 22C17.5228 22 22 17.5228 22 12C22 6.47715 17.5228 2 12 2C6.47715 2 2 6.47715 2 12C2 17.5228 6.47715 22 12 22Z" stroke="white" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round"/>
                              <path d="M12 6V12L16 14" stroke="white" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round"/>
                            </g>
                            <defs>
                              <clipPath id="clip0_6_6325">
                                <rect width="24" height="24" fill="white"/>
                              </clipPath>
                            </defs>
                          </svg></span> AT 11 AM EDT
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="countdown counter-black mt15">
                        <div class="timer-label text-center"><span class="f-30 f-md-40 lh100 timerbg col-12">01</span><br><span class="f-14 w500">Days</span> </div>
                        <div class="timer-label text-center"><span class="f-30 f-md-40 lh100 timerbg">16</span><br><span class="f-14 w500">Hours</span> </div>
                        <div class="timer-label text-center"><span class="f-30 f-md-40 lh100 timerbg">59</span><br><span class="f-14 w500">Mins</span> </div>
                        <div class="timer-label text-center"><span class="f-30 f-md-40 lh100 timerbg">37</span><br><span class="f-14 w500">Sec</span> </div>
                     </div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- Live Section Start -->
<div class="live-section" id="live-sec">
   <div class="container">
      <div class="row">
         <div class="col-12 col-md-6">
            <div class="left-live-box">
               <div class="f-28 f-md-45 w700 text-center text-capitalize black-clr lh140">
                  Subscribe To Our <br class="d-none d-md-block"> JV List
               </div>
               <div class="f-20 f-md-22 w500 text-center text-capitalize black-clr lh140 mt10">
                  And Be The First to Know Our Special Contest, Events and Discounts
               </div>
               <!-- Aweber Form Code -->
               <div class="mt20 mt-md40">
                        <form method="post" class="af-form-wrapper mb-md5" accept-charset="UTF-8" action="https://www.aweber.com/scripts/addlead.pl">
                           <div style="display: none;">
                              <input type="hidden" name="meta_web_form_id" value="1703814699">
                              <input type="hidden" name="meta_split_id" value="">
                              <input type="hidden" name="listname" value="awlist6200898">
                              <input type="hidden" name="redirect" value="https://www.aweber.com/thankyou-coi.htm?m=text" id="redirect_3161ec97c09ca429c4183bf74a86fd90">
                              <input type="hidden" name="meta_adtracking" value="My_Web_Form">
                              <input type="hidden" name="meta_message" value="1">
                              <input type="hidden" name="meta_required" value="name,email">
                              <input type="hidden" name="meta_tooltip" value="">
                           </div>
                           <div id="af-form-1703814699" class="af-form">
                              <div id="af-body-1703814699" class="af-body af-standards row justify-content-center">
                                 <div class="af-element col-md-12">
                                    <label class="previewLabel" for="awf_field-115787791" style="display:none;">Name: </label>
                                    <div class="af-textWrap mb20 input-type">
                                       <input id="awf_field-115787791" class="frm-ctr-popup form-control input-field" type="text" name="name" placeholder="Your Name" value="" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="500">
                                    </div>
                                    <div class="af-clear"></div>
                                 </div>
                                 <div class="af-element mb20 col-md-12">
                                    <label class="previewLabel" for="awf_field-115787792" style="display:none;">Email: </label>
                                    <div class="af-textWrap input-type">
                                       <input class="text frm-ctr-popup form-control input-field" id="awf_field-115787792" type="text" name="email" placeholder="Your Email" value="" tabindex="501" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} ">
                                    </div>
                                    <div class="af-clear"></div>
                                 </div>
                                 <div class="af-element buttonContainer button-type form-btn white-clr col-md-12">
                                    <input name="submit" class="submit f-20 f-md-24 white-clr center-block" type="submit" value="Subscribe For JV Updates" tabindex="502">
                                    <div class="af-clear"></div>
                                 </div>
                              </div>
                           </div>
                           <div style="display: none;"><img src="https://forms.aweber.com/form/displays.htm?id=zIwcTIxMbMwc" alt="image"></div>
                        </form>
                     </div>
                     <!-- Aweber Form Code -->
            </div>
         </div>
         <div class="col-12 col-md-6 mt20 mt-md0 text-center">
            <div class="right-live-box">
               <div class="f-24 f-md-34 w700 text-center text-capitalize black-clr lh140">
                  Grab Your <span class="orange-clr">JVZoo</span> Affiliate Link to Jump on This Amazing Product Launch
               </div>
               <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/jvzoo.webp" class="img-fluid d-block mx-auto mt20 " alt="Jvzoo">
               <div class="request-affiliate mt20">
                  <a href="#" class="f-20 f-md-22 w600 mx-auto white-clr" target="_blank">Request FE Link</a>
               </div>
               <div class="request-affiliate mt20">
                  <a href="#" class="f-20 f-md-22 w600 mx-auto white-clr" target="_blank">Request Bundle Link</a>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- Live Section End -->

<section class="not-everyone-excels-sec">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="f-28 f-md-46 w700 lh140 text-center play">
                    <span class="red-clr">Not everyone excels in</span> <br class="d-none d-md-block">
                    <span class="sad-smily">
                        <span class="border-underline">
                        sales and marketing, adept at closing clients <br class="d-none d-md-block"> and securing big deals because
                        </span>
                    </span> 
                </div>
                <img src="assets/images/man-not-excels.webp" alt="Man Not Excels" class="d-block img-fluid mx-auto mt20 mt-md80">
                <div class="f-18 f-md-22 w500 lh140 mt20 mt-md50 text-center">
                    And If your customers are an agency owner, marketer, freelancer, or entrepreneur, they have <br class="d-none d-md-block"> likely encountered these challenges in business. 
                </div>
            </div>
            
        </div>
    </div>
</section>

<section class="eliminate-sec">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <div class="f-28 f-md-36 w400 lh140 play eliminated-head">
                    What if IntelliMate <span class="w700 star-smily">Eliminate sales challenges, supercharging business. Effortlessly acquire clients and close deals, transforming struggles into successes.</span>
                </div>
                <div class="f-18 f-md-22 w700 lh140 mt20 mt-md50 heired-right">
                    Yes, You Heired Right 
                </div>
            </div>
        </div>
        <div class="eliminate-wall mt20 mt-md50">
            <div class="row">
                <div class="col-12">
                    <div class="f-20 f-md-22 w500 lh140 text-center">
                        Elevate your customer’s business with IntelliMate's powerful capabilities. 
                    </div>
                    <img src="assets/images/customer-business.webp" alt="Customer Bussiness" class="mx-auto d-block img-fluid mt20 mt-md40">
                </div>
            </div>
        </div>
        <div class="row mt20 mt-md40">
            <div class="col-12">
                <div class="f-18 f-md-22 w500 lh140 text-center">
                    IntelliMate is a Trained Chat Bot on your Data, Address Critical Challenges Faced by Agency owners, <br class="d-none d-md-block"> Freelancers, Marketers and Individuals Who are in Money-Making Space. 
                </div>
            </div>
        </div>
    </div>
</section>

<section class="proudly-sec">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <div class="proudly-head">
                    <div class="f-28 f-md-50 w700 lh140 blue-gradient caveat ">Proudly Presenting…</div>
                </div>
                <div class="mt20 mt-md60">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 742.13 152.79" style="max-height: 120px;"><defs><style>.cls-1{fill:#fff;}.cls-2{fill:url(#linear-gradient);}.cls-3{fill:#21a1ff;}.cls-4{fill:#2ddfff;}.cls-5{fill:url(#linear-gradient-2);}.cls-6{fill:#f1f1f2;}</style><linearGradient id="linear-gradient" x1="667.64" y1="93.3" x2="726.15" y2="93.3" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#2ee2ff"/><stop offset="1" stop-color="#219fff"/></linearGradient><linearGradient id="linear-gradient-2" x1="23.17" y1="76.39" x2="157.15" y2="76.39" xlink:href="#linear-gradient"/></defs><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1" d="M220,127.18H210V59.43h10Z"/><path class="cls-1" d="M264.32,103.36a14.48,14.48,0,1,0-28.95,0v23.82H226V103.36a23.83,23.83,0,1,1,47.65,0v23.82h-9.35Z"/><path class="cls-1" d="M289.05,89v19.2a9.7,9.7,0,0,0,9.75,9.75h4.53v9.35H298.8a19.1,19.1,0,0,1-19.1-19.1V67.37h9.35V79.63h14.28V89Z"/><path class="cls-1" d="M319.61,108.48a15.43,15.43,0,0,0,27,4h10.56a24.77,24.77,0,1,1,1.81-9.14,21.23,21.23,0,0,1-.61,5.12h-38.8Zm29.45-9.35a15.48,15.48,0,0,0-29.85,0Z"/><path class="cls-1" d="M374.4,108a9.78,9.78,0,0,0,9.75,9.85h4.52v9.35h-4.52a19.18,19.18,0,0,1-19.1-19.2V67.17h9.35Z"/><path class="cls-1" d="M404.05,108a9.78,9.78,0,0,0,9.75,9.85h4.52v9.35H413.8A19.18,19.18,0,0,1,394.7,108V67.17h9.35Z"/><path class="cls-1" d="M433.71,127.18h-9.35V79.63h9.35Z"/><path class="cls-1" d="M496.23,77.42l-23.32,24.83L449.69,77.42v49.76h-9.95V59.43h6.63l26.54,28.24,26.64-28.24h6.63v67.75h-9.95Z"/><path class="cls-1" d="M559.86,127.18h-9.35V103.36A14.48,14.48,0,1,0,536,117.83v9.35a23.88,23.88,0,1,1,23.82-23.82Z"/><path class="cls-1" d="M575.24,89v19.2a9.7,9.7,0,0,0,9.75,9.75h4.53v9.35H585a19.1,19.1,0,0,1-19.1-19.1V67.37h9.35V79.63h14.28V89Z"/><path class="cls-1" d="M605.8,108.48a15.43,15.43,0,0,0,27,4h10.55a24.77,24.77,0,1,1,1.81-9.14,21.74,21.74,0,0,1-.6,5.12H605.8Zm29.45-9.35a15.48,15.48,0,0,0-29.85,0Z"/><path class="cls-2" d="M693.48,59.43h6.83l25.84,67.75H715.49l-18.6-48.85L678.3,127.18H667.64Z"/><path class="cls-3" d="M742.13,127.18h-10V59.43h10Z"/><path class="cls-3" d="M161,69.48c0,11.86,0,22.69,0,34,3.94,1,10.59-1.4,14.61-6.34A17.06,17.06,0,0,0,177.32,78,16.86,16.86,0,0,0,161,69.48Z"/><path class="cls-4" d="M18.5,69.48c0,11.86,0,22.69,0,34-3.94,1-10.6-1.4-14.61-6.34A17,17,0,0,1,2.17,78,16.84,16.84,0,0,1,18.5,69.48Z"/><path class="cls-1" d="M90.13,118.12a23.33,23.33,0,0,1-16.67-6.7,2.78,2.78,0,0,1-.12-4.15,2.85,2.85,0,0,1,4.09.09,18.1,18.1,0,0,0,25.42,0,2.83,2.83,0,0,1,4.16-.13,2.78,2.78,0,0,1-.22,4.15,23,23,0,0,1-13.27,6.52c-.58.07-1.16.14-1.74.18S90.68,118.12,90.13,118.12Z"/><path class="cls-1" d="M66.14,76.4c-4.22,0-7.84,1.86-10.89,5.46a2.66,2.66,0,0,0-.08,3.38,1.62,1.62,0,0,0,2.67-.08c5.48-4.64,11.8-4.9,16.61,0a1.62,1.62,0,0,0,2.72.1A2.65,2.65,0,0,0,77,81.86a14.1,14.1,0,0,0-8.67-5.31,10.57,10.57,0,0,0-1.13-.14C66.86,76.38,66.5,76.4,66.14,76.4Z"/><path class="cls-1" d="M114.15,76.4c-4.22,0-7.84,1.86-10.89,5.46a2.64,2.64,0,0,0-.08,3.38,1.62,1.62,0,0,0,2.67-.08c5.47-4.64,11.8-4.9,16.61,0a1.61,1.61,0,0,0,2.71.1,2.65,2.65,0,0,0-.14-3.38,14.13,14.13,0,0,0-8.67-5.31,10.89,10.89,0,0,0-1.13-.14C114.87,76.38,114.51,76.4,114.15,76.4Z"/><path class="cls-5" d="M124.12,39.68h-1.39l5.74-20.2a9.79,9.79,0,1,0-5.88-1.61L116.4,39.68H63.93l-6.2-21.81a9.75,9.75,0,1,0-5.87,1.61l5.75,20.2H56.2a33.07,33.07,0,0,0-33,33v47a33.08,33.08,0,0,0,33,33h67.92a33.08,33.08,0,0,0,33-33v-47A33.07,33.07,0,0,0,124.12,39.68Zm5.74-33.76a2.76,2.76,0,1,1,1.94,3.4A2.78,2.78,0,0,1,129.86,5.92ZM48.52,9.32a2.76,2.76,0,1,1,1.94-3.38A2.75,2.75,0,0,1,48.52,9.32Zm89.19,110.43a13.61,13.61,0,0,1-13.59,13.6H56.2a13.61,13.61,0,0,1-13.6-13.6v-47a13.61,13.61,0,0,1,13.6-13.6h67.92a13.61,13.61,0,0,1,13.59,13.6Z"/><circle class="cls-6" cx="429.03" cy="70.88" r="4.75"/></g></g></svg> 
                </div>
                <div class="mt20 mt-md60 f-28 f-md-50 w700 lh140 white-clr">
                    Self-Learning App Turns Any URL, Website or Doc File into an AI-Powered Chatbot… 
                </div>
                <img src="assets/images/pbox.webp" alt="Product Box" class="mx-auto d-block img-fluid mt20 mt-md50">
            </div>
        </div>
        <div class="product-box mt20 mt-md80">
            <div class="row">
                <div class="col-12">
                    <div class="f-24 f-md-32 w700 blue-gradient play">
                        Eliminate Any Obstacles That May Be Hindering Your Customers' Path to Success.
                    </div>
                    <ul class="mt20 white-clr f-20 f-md-22 w500 lh180">
                        <li>Your Customers Can Sell Online & Keep 100% of the Profit – Never Loose Traffic Again.</li>
                        <li>Switch To One Time Pricing & Save Huge</li>
                        <li>Done-for-Yow Selling Website Preloaded with 40+ Ready-to-Use Products to Start Selling Right Away.</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="earn-dollar-sec">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <div class="f-28 f-md-46 w700 play white-clr dollar-head">
                    Earn Thousands Of Dollars
                </div>
                <div class="f-28 f-md-46 w700 lh140 mt15">
                    Extra Recurring Every Month Using IntelliMate <br class="d-none d-md-block">
                    AI - Smart Chat Bots
                </div>
            </div>
        </div>
    </div>
    <div class="outer-container mt20 mt-md60">
        <div class="container-grey-box ">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-12 col-md-2">
                                <img src="assets/images/one.webp" alt="One" class="d-block img-fluid">
                            </div>
                            <div class="col-12 col-md-10 f-24 f-md-32 w700 lh140 mt10 mt-md0">
                                Effortless Lead Generation
                            </div>
                        </div>
                        <div class="f-18 f-md-20 w400 lh140 mt20">
                            IntelliMate Trained Smart Chatbot enables users to effortlessly convert website visitors into potential leads.
                        </div>
                        <div class="f-18 f-md-20 w400 lh140 mt20">
                        <span class="w700">By feeding relevant data through URLs or Doc files,</span> the chat bot generates precise responses that engage visitors, collect information, and qualify leads, streamlining the lead generation process for your customers.
                        </div>
                    </div>
                    <div class="col-md-6 mt20 mt-md0">
                        <img src="assets/images/effortless-lead-generation.webp" alt="Effortless Lead Generation" class="d-block img-fluid">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="outer-container ">
        <div class="container-white-box ">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-6 order-md-2">
                        <div class="row">
                            <div class="col-12 col-md-2">
                                <img src="assets/images/two.webp" alt="Two" class="d-block img-fluid">
                            </div>
                            <div class="col-12 col-md-10 f-24 f-md-32 w700 lh140 mt10 mt-md0" style="word-break:break-all;">
                                Product Recommendations
                            </div>
                        </div>
                        <div class="f-18 f-md-20 w400 lh140 mt20">
                            IntelliMate can be used to build a chatbot that assists customers with product recommendations.  
                        </div>
                        <div class="f-18 f-md-20 w400 lh140 mt20">
                            This personalized approach can help drive sales by suggesting relevant products based on customer preferences and needs.
                        </div>
                    </div>
                    <div class="col-md-6 mt20 mt-md0 order-md-1">
                        <img src="assets/images/product-recommendations.webp" alt="Product Recommendations" class="d-block img-fluid">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="outer-container">
        <div class="container-grey-box ">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-12 col-md-2">
                                <img src="assets/images/three.webp" alt="Three" class="d-block img-fluid">
                            </div>
                            <div class="col-12 col-md-10 f-24 f-md-32 w700 lh140 mt10 mt-md0">
                                Upselling and Cross-selling
                            </div>
                        </div>
                        <div class="f-18 f-md-20 w400 lh140 mt20">
                            By analyzing user interactions and understanding customer preferences, IntelliMate can identify opportunities for upselling and cross-selling.  
                        </div>
                        <div class="f-18 f-md-20 w400 lh140 mt20">
                            This can help sales teams maximize revenue by suggesting additional products or upgrades. 
                        </div>
                    </div>
                    <div class="col-md-6 mt20 mt-md0">
                        <img src="assets/images/upselling.webp" alt="Upselling and Cross-selling" class="d-block img-fluid">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="outer-container ">
        <div class="container-white-box ">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-6 order-md-2">
                        <div class="row">
                            <div class="col-12 col-md-2">
                                <img src="assets/images/four.webp" alt="Four" class="d-block img-fluid">
                            </div>
                            <div class="col-12 col-md-10 f-24 f-md-32 w700 lh140 mt10 mt-md0">
                                Time-Saving Automation for Freelancers
                            </div>
                        </div>
                        <div class="f-18 f-md-20 w400 lh140 mt20">
                            Freelancers can save valuable time with the automated capabilities of IntelliMate.
                        </div>
                        <div class="f-18 f-md-20 w400 lh140 mt20">
                            Instead of spending hours manually responding to inquiries or providing information, <br class="d-none d-md-block">
                            freelancers can focus on their core work, knowing that the IntelliMate is efficiently handling routine tasks and engaging website visitors on their behalf.
                        </div>
                    </div>
                    <div class="col-md-6 mt20 mt-md0 order-md-1">
                        <img src="assets/images/time-saving-automation.webp" alt="Time-Saving Automation for Freelancers" class="d-block img-fluid">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="outer-container">
        <div class="container-grey-box ">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-12 col-md-2">
                                <img src="assets/images/five.webp" alt="Five" class="d-block img-fluid">
                            </div>
                            <div class="col-12 col-md-10 f-24 f-md-32 w700 lh140 mt10 mt-md0">
                                Personalized Marketing for Marketers
                            </div>
                        </div>
                        <div class="f-18 f-md-20 w400 lh140 mt20">
                            Marketers can leverage the power of personalized interactions with IntelliMate.  
                        </div>
                        <div class="f-18 f-md-20 w400 lh140 mt20">
                            By tailoring responses based on user data and preferences, marketers can create a more engaging and targeted experience, increasing the effectiveness of marketing campaigns and driving higher conversion rates. 
                        </div>
                    </div>
                    <div class="col-md-6 mt20 mt-md0">
                        <img src="assets/images/personalized-marketing.webp" alt="Personalized Marketing for Marketers" class="d-block img-fluid">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="outer-container ">
        <div class="container-white-box ">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-6 order-md-2">
                        <div class="row">
                            <div class="col-12 col-md-2">
                                <img src="assets/images/six.webp" alt="Six" class="d-block img-fluid">
                            </div>
                            <div class="col-12 col-md-10 f-24 f-md-32 w700 lh140 mt10 mt-md0">
                                Enhanced Client Communication for Agency Owners
                            </div>
                        </div>
                        <div class="f-18 f-md-20 w400 lh140 mt20">
                            Agency owners can strengthen client communication through the intelligent responses generated by the IntelliMate.
                        </div>
                        <div class="f-18 f-md-20 w400 lh140 mt20">
                            The ability to provide quick and accurate information enhances the client experience, fostering stronger relationships and demonstrating the agency's commitment to effective communication.
                        </div>
                    </div>
                    <div class="col-md-6 mt20 mt-md0 order-md-1">
                        <img src="assets/images/client-communication.webp" alt="Enhanced Client Communication for Agency Owners" class="d-block img-fluid">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="outer-container">
        <div class="container-grey-box ">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-12 col-md-2">
                                <img src="assets/images/seven.webp" alt="Seven" class="d-block img-fluid">
                            </div>
                            <div class="col-12 col-md-10 f-24 f-md-32 w700 lh140 mt10 mt-md0">
                                Boosting Online Revenue Streams
                            </div>
                        </div>
                        <div class="f-18 f-md-20 w400 lh140 mt20">
                            For Make Money Online People, IntelliMate becomes a powerful tool to boost there revenue streams.
                        </div>
                        <div class="f-18 f-md-20 w400 lh140 mt20">
                            By converting website visitors into leads and sales, IntelliMate contributes to the overall monetization strategy, creating a more efficient and effective online business model.
                        </div>
                    </div>
                    <div class="col-md-6 mt20 mt-md0">
                        <img src="assets/images/personalized-marketing.webp" alt="Personalized Marketing for Marketers" class="d-block img-fluid">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="watch-sec">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <div class="watch-head">
                    <div class="f-28 f-md-46 w700 white-clr text-center">
                        Watch The Demo 
                            <span class="ml10">
                                <svg xmlns="http://www.w3.org/2000/svg" width="35" height="35" viewBox="0 0 35 35" fill="none">
                                    <path d="M17.5 0C7.83501 0 0 7.83501 0 17.5C0 27.165 7.83501 35 17.5 35C27.165 35 35 27.165 35 17.5C35 7.83501 27.165 0 17.5 0ZM12.7185 25.1115V9.88853L25.9019 17.5L12.7185 25.1115Z" fill="#F1F8FA"/>
                                </svg>
                            </span> <br class="d-none d-md-block">
                        Discover How Easy & Powerful It Is
                    </div>
                </div>
            </div>
            <div class="col-12 mt20 mt-md30">
                <img src="assets/images/down-arrow.webp" alt="Arrow" class="mx-auto d-block img-fluid vert-move">
            </div>
            <div class="col-12 mt20 mt-md30">
                <img src="assets/images/demo-img.webp" alt="DEmo" class="mx-auto d-block img-fluid">
            </div>
        </div>
    </div>
</section>

<section class="boost-business">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <div class="boost-head">
                    <div class="f-28 f-md-50 w700 lh140 play text-center">
                        Boost your business's bottom line <br class="d-none d-md-block">
                        with <span class="blue-gradient">IntelliMate AI</span> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

    <!-- timer --->
    <?php
    if ($now < $exp_date) {
    
    ?>
 <script type="text/javascript">
    // Count down milliseconds = server_end - server_now = client_end - client_now
    var server_end = <?php echo $exp_date; ?> * 1000;
    var server_now = <?php echo time(); ?> * 1000;
    var client_now = new Date().getTime();
    var end = server_end - server_now + client_now; // this is the real end time
    
    var noob = $('.countdown').length;
    
    var _second = 1000;
    var _minute = _second * 60;
    var _hour = _minute * 60;
    var _day = _hour * 24
    var timer;
    
    function showRemaining() {
        var now = new Date();
        var distance = end - now;
        if (distance < 0) {
            clearInterval(timer);
            document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
            return;
        }
    
        var days = Math.floor(distance / _day);
        var hours = Math.floor((distance % _day) / _hour);
        var minutes = Math.floor((distance % _hour) / _minute);
        var seconds = Math.floor((distance % _minute) / _second);
        if (days < 10) {
            days = "0" + days;
        }
        if (hours < 10) {
            hours = "0" + hours;
        }
        if (minutes < 10) {
            minutes = "0" + minutes;
        }
        if (seconds < 10) {
            seconds = "0" + seconds;
        }
        var i;
        var countdown = document.getElementsByClassName('countdown');
        for (i = 0; i < noob; i++) {
            countdown[i].innerHTML = '';
    
            if (days) {
                countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-30 f-md-40 timerbg">' + days + '</span><br><span class="f-16 w500">Days</span> </div>';
            }
    
            countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-30 f-md-40 timerbg">' + hours + '</span><br><span class="f-16 w500">Hours</span> </div>';
    
            countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-30 f-md-40 timerbg">' + minutes + '</span><br><span class="f-16 w500">Mins</span> </div>';
    
            countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-30 f-md-40 timerbg">' + seconds + '</span><br><span class="f-16 w500">Sec</span> </div>';
        }
    
    }
    timer = setInterval(showRemaining, 1000);
 </script>    

 <?php
    } else {
    echo "Times Up";
    }
    ?>

</body>

</html>
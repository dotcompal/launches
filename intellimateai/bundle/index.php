<!Doctype html>
<html>
   <head>
      <title>IntelliMate AI | Bundle Deal</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=9">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
      <!------Meta Tags-------->
      <meta name="title" content="IntelliMate AI Exclusive Bundle Deal">
      <meta name="description" content="Get IntelliMate AI With All The Upgrades For 64% Off & Save Over $650 When You Grab This Highly-Discounted Bundle Deal">
      <meta name="keywords" content="IntelliMate AI">
      <meta property="og:image" content="https://www.intellimateai.co/bundle/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Dr. Amit Pareek">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="IntelliMate AI Exclusive Bundle Deal">
      <meta property="og:description" content="Get IntelliMate AI With All The Upgrades For 64% Off & Save Over $650 When You Grab This Highly-Discounted Bundle Deal">
      <meta property="og:image" content="https://www.intellimateai.co/bundle/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="IntelliMate AI Exclusive Bundle Deal">
      <meta property="twitter:description" content="Get IntelliMate AI With All The Upgrades For 64% Off & Save Over $650 When You Grab This Highly-Discounted Bundle Deal">
      <meta property="twitter:image" content="https://www.intellimateai.co/bundle/thumbnail.png">
      <!------Meta Tags-------->
      <link rel="icon" href="https://cdn.dotcompaltest.com/uploads/launches/intellimateai/common_assets/images/favicon.png" type="image/png">
      <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&family=Play:wght@400;700&family=Plus+Jakarta+Sans:ital,wght@0,200;0,300;0,400;0,500;0,600;0,700;0,800;1,200;1,300;1,400;1,500;1,600;1,700;1,800&display=swap" rel="stylesheet">
      <!-- required -->
      <link rel="stylesheet" href="../common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style.css" type="text/css">
      <link rel="stylesheet" href="../common_assets/css/general.css" type="text/css">
      <script type='text/javascript' src="../common_assets/js/jquery.min.js"></script>
      <link rel="stylesheet" href="assets/css/timer.css">
      <script src=https://pexelapi.intellimateai.co/app/chat/chat.js id=1637 user_id=23 > </script>
      <!-- <script src="assets/js/timer.js"></script> -->

      <!-- New Timer  Start-->
         <style>
            .timer-header-top{background: #000000; padding-top:10px; padding-bottom:10px;}
         </style>  
      <script>
         (function(w, i, d, g, e, t, s) {
             if(window.businessDomain != undefined){
                 console.log("Your page have duplicate embed code. Please check it.");
                 return false;
             }
             businessDomain = 'IntelliMate AI';
             allowedDomain = 'intellimateai.co';
             if(!window.location.hostname.includes(allowedDomain)){
                 console.log("Your page have not authorized. Please check it.");
                 return false;
             }
             console.log("Your script is ready...");
             w[d] = w[d] || [];
             t = i.createElement(g);
             t.async = 1;
             t.src = e;
             s = i.getElementsByTagName(g)[0];
             s.parentNode.insertBefore(t, s);
         })(window, document, '_gscq', 'script', 'https://cdn.staticdcp.com/apps/engage/smart_engage/js/config-loader.js');
         </script>
          <style>
           
         </style>  
   </head>
      <div class="timer-header-top fixed-top">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-12 col-md-3">
                  <div class="tht-left f-14 f-md-16 white-clr w800 text-md-start text-center" style="word-spacing:3px;">
                      Coupon Code <span class="neon-clr">"INTELLIMATE"</span> <br class="d-block d-md-none"> for <span class="neon-clr"> $50 Discount </span> Ending In
                  </div>
               </div>
               <div class="col-7 col-md-6 text-center ">
                  <div class="countdown counter-white"><div class="timer-label text-center"><span class="f-24 f-md-42 timerbg oswald">00</span><br> &nbsp;&nbsp;&nbsp;&nbsp;<span class="f-14 f-md-14 w400 smmltd">Days</span> </div><div class="timer-label text-center">&nbsp;&nbsp;&nbsp;&nbsp;<span class="f-24 f-md-42 timerbg oswald">00</span><br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="f-14 f-md-14 w400 smmltd">Hours</span> </div><div class="timer-label text-center timer-mrgn"> &nbsp;&nbsp;&nbsp;&nbsp;<span class="f-24 f-md-42 timerbg oswald">00</span><br> &nbsp;&nbsp;&nbsp;&nbsp;<span class="f-14 f-md-14 w400 smmltd">Mins</span> </div><div class="timer-label text-center "> &nbsp;&nbsp;&nbsp;&nbsp; <span class="f-24 f-md-42 timerbg oswald">00</span><br><span class="f-14 f-md-14 w400 smmltd">Sec</span> </div></div>
               </div>
               <div class="col-5 col-md-3 text-md-end">
                  <div class="instant-btn1"><a href="#buybundle" class="t-decoration-none">Buy Now</a></div>
               </div>
            </div>
         </div>
      </div>
   <body>
      <!--1. Header Section Start -->
      <div class="header-section mt100">
         <div class="container">
            <div class="row">
               <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 742.13 152.79" style="max-height: 100px;"><defs><style>.cls-1{fill:#fff;}.cls-2{fill:url(#linear-gradient);}.cls-3{fill:#21a1ff;}.cls-4{fill:#2ddfff;}.cls-5{fill:url(#linear-gradient-2);}.cls-6{fill:#f1f1f2;}</style><linearGradient id="linear-gradient" x1="667.64" y1="93.3" x2="726.15" y2="93.3" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#2ee2ff"></stop><stop offset="1" stop-color="#219fff"></stop></linearGradient><linearGradient id="linear-gradient-2" x1="23.17" y1="76.39" x2="157.15" y2="76.39" xlink:href="#linear-gradient"></linearGradient></defs><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1" d="M220,127.18H210V59.43h10Z"></path><path class="cls-1" d="M264.32,103.36a14.48,14.48,0,1,0-28.95,0v23.82H226V103.36a23.83,23.83,0,1,1,47.65,0v23.82h-9.35Z"></path><path class="cls-1" d="M289.05,89v19.2a9.7,9.7,0,0,0,9.75,9.75h4.53v9.35H298.8a19.1,19.1,0,0,1-19.1-19.1V67.37h9.35V79.63h14.28V89Z"></path><path class="cls-1" d="M319.61,108.48a15.43,15.43,0,0,0,27,4h10.56a24.77,24.77,0,1,1,1.81-9.14,21.23,21.23,0,0,1-.61,5.12h-38.8Zm29.45-9.35a15.48,15.48,0,0,0-29.85,0Z"></path><path class="cls-1" d="M374.4,108a9.78,9.78,0,0,0,9.75,9.85h4.52v9.35h-4.52a19.18,19.18,0,0,1-19.1-19.2V67.17h9.35Z"></path><path class="cls-1" d="M404.05,108a9.78,9.78,0,0,0,9.75,9.85h4.52v9.35H413.8A19.18,19.18,0,0,1,394.7,108V67.17h9.35Z"></path><path class="cls-1" d="M433.71,127.18h-9.35V79.63h9.35Z"></path><path class="cls-1" d="M496.23,77.42l-23.32,24.83L449.69,77.42v49.76h-9.95V59.43h6.63l26.54,28.24,26.64-28.24h6.63v67.75h-9.95Z"></path><path class="cls-1" d="M559.86,127.18h-9.35V103.36A14.48,14.48,0,1,0,536,117.83v9.35a23.88,23.88,0,1,1,23.82-23.82Z"></path><path class="cls-1" d="M575.24,89v19.2a9.7,9.7,0,0,0,9.75,9.75h4.53v9.35H585a19.1,19.1,0,0,1-19.1-19.1V67.37h9.35V79.63h14.28V89Z"></path><path class="cls-1" d="M605.8,108.48a15.43,15.43,0,0,0,27,4h10.55a24.77,24.77,0,1,1,1.81-9.14,21.74,21.74,0,0,1-.6,5.12H605.8Zm29.45-9.35a15.48,15.48,0,0,0-29.85,0Z"></path><path class="cls-2" d="M693.48,59.43h6.83l25.84,67.75H715.49l-18.6-48.85L678.3,127.18H667.64Z"></path><path class="cls-3" d="M742.13,127.18h-10V59.43h10Z"></path><path class="cls-3" d="M161,69.48c0,11.86,0,22.69,0,34,3.94,1,10.59-1.4,14.61-6.34A17.06,17.06,0,0,0,177.32,78,16.86,16.86,0,0,0,161,69.48Z"></path><path class="cls-4" d="M18.5,69.48c0,11.86,0,22.69,0,34-3.94,1-10.6-1.4-14.61-6.34A17,17,0,0,1,2.17,78,16.84,16.84,0,0,1,18.5,69.48Z"></path><path class="cls-1" d="M90.13,118.12a23.33,23.33,0,0,1-16.67-6.7,2.78,2.78,0,0,1-.12-4.15,2.85,2.85,0,0,1,4.09.09,18.1,18.1,0,0,0,25.42,0,2.83,2.83,0,0,1,4.16-.13,2.78,2.78,0,0,1-.22,4.15,23,23,0,0,1-13.27,6.52c-.58.07-1.16.14-1.74.18S90.68,118.12,90.13,118.12Z"></path><path class="cls-1" d="M66.14,76.4c-4.22,0-7.84,1.86-10.89,5.46a2.66,2.66,0,0,0-.08,3.38,1.62,1.62,0,0,0,2.67-.08c5.48-4.64,11.8-4.9,16.61,0a1.62,1.62,0,0,0,2.72.1A2.65,2.65,0,0,0,77,81.86a14.1,14.1,0,0,0-8.67-5.31,10.57,10.57,0,0,0-1.13-.14C66.86,76.38,66.5,76.4,66.14,76.4Z"></path><path class="cls-1" d="M114.15,76.4c-4.22,0-7.84,1.86-10.89,5.46a2.64,2.64,0,0,0-.08,3.38,1.62,1.62,0,0,0,2.67-.08c5.47-4.64,11.8-4.9,16.61,0a1.61,1.61,0,0,0,2.71.1,2.65,2.65,0,0,0-.14-3.38,14.13,14.13,0,0,0-8.67-5.31,10.89,10.89,0,0,0-1.13-.14C114.87,76.38,114.51,76.4,114.15,76.4Z"></path><path class="cls-5" d="M124.12,39.68h-1.39l5.74-20.2a9.79,9.79,0,1,0-5.88-1.61L116.4,39.68H63.93l-6.2-21.81a9.75,9.75,0,1,0-5.87,1.61l5.75,20.2H56.2a33.07,33.07,0,0,0-33,33v47a33.08,33.08,0,0,0,33,33h67.92a33.08,33.08,0,0,0,33-33v-47A33.07,33.07,0,0,0,124.12,39.68Zm5.74-33.76a2.76,2.76,0,1,1,1.94,3.4A2.78,2.78,0,0,1,129.86,5.92ZM48.52,9.32a2.76,2.76,0,1,1,1.94-3.38A2.75,2.75,0,0,1,48.52,9.32Zm89.19,110.43a13.61,13.61,0,0,1-13.59,13.6H56.2a13.61,13.61,0,0,1-13.6-13.6v-47a13.61,13.61,0,0,1,13.6-13.6h67.92a13.61,13.61,0,0,1,13.59,13.6Z"></path><circle class="cls-6" cx="429.03" cy="70.88" r="4.75"></circle></g></g></svg>
            </div>
            <div class="row">
                  <!-- <div class="col-12 mt20 mt-md50 text-center">
                     <div class="preheadline f-18 f-md-22 w600 white-clr lh140">
                     <span class="preheadline-content white-clr"><u class="red-clr">WARNING :</u> <span class="uppercase">This Limited Time DISCOUNTED Bundle Offer Expires Soon</span></span>
                     </div> -->

               <div class="col-12 mt30 mt-md50 text-center">
                  <div class="preheadline f-16 f-md-17 w600 white-clr lh140">
                     <u class="red-clr">WARNING :</u> <span class="uppercase">THIS LIMITED TIME DISCOUNTED BUNDLE OFFER EXPIRES SOON</span>
                  </div>
               </div>
            </div>

            <div class="col-12 mt20 mt-md30">
               <!-- <div class="mainheadline f-28 f-md-50 lh140 play w700 text-center">
                  A NEW Revolutionary <span class="caveat">Self-Learning</span> App Turns Any Website File into a Conversational AI Chatbot… that works tirelessly 24/7 to Customer Happiness! Non-Stop
               </div> -->
               <div class="col-12 f-md-45 f-28 w700 lh140 mt30 mt-md50 relative play">
                  <div class="gametext white-clr">
                     FIRST-TO-MARKET
                  </div>
                  <div class="mainheadline text-center">
                     Instantly Create, Train, and Launch Self-Learning AI Chatbots in Any Language in Seconds <span class="w400">– No Tech Skills, No Hiring, No Monthly Fees!"</span>
                  </div>
              <!--    <div class="mainheadline text-center">-->
                      
              <!--           Instantly Create, Train, and Launch Self-Learning AI Chatbots in Any Language in Seconds <span class="w400">– No Tech Skills, No Hiring, No Monthly Fees!"</span>-->
                    
                 
              <!--</div>-->
            </div>
            <!-- <div class="col-12 mt70 mt-md70 relative p-md0 relative play">
               <div class="gametext white-clr">
                First-To-Market
              </div>
               <div class="mainheadline f-md-45 f-25 w400 text-center black-clr lh140">
               <span class="w700 neon-clr">#1 Super VA That Finish 100s of Marketing Tasks <span class="caveat">(Better &amp; Faster Than Any CMO Out There...)</span> </span> &amp; Trim Your Workload, Hiring/Firing Headaches, &amp; Monthly Bills
               </div>
            </div> -->

         <!-- <div class="col-12 mt30 mt-md50 relative p-md0">
            <div class="mainheadline f-md-42 f-25 w400 text-center white-clr lh140">
               <div class="headline-text">
                  <span class="w700 sky-blue">#1 Super VA That Finish 100s of Marketing Tasks <span class="caveat">(Better &amp; Faster Than Any CMO Out There...)</span> </span> &amp; Trim Your Workload, Hiring/Firing Headaches, &amp; Monthly Bills
               </div>
            </div>
         </div> -->
         <div class="col-12 mt20 mt-md30 text-center">
               <div class="f-18 f-md-26 w500 white-clr post-headline text-capitalize">
               Get IntelliMate AI With All The Upgrades For 73% Off & Save Over $805 When You Grab This Highly-Discounted Bundle Deal
               </div>
           </div>
               
               
               <!-- <div class="col-12 col-md-9 mx-auto mt-md30 mt20 f-18 f-md-26 w500 text-center lh140 white-clr text-capitalize">
                  Get IntelliMate AI With All The Upgrades For 78% Off & Save Over $1028 When You Grab This Highly-Discounted Bundle Deal
               </div> -->

               <div class="col-12 col-md-10 mt20 mt-md30 mx-auto">
                   <!-- <div class="responsive-video">
                     <iframe src="https://mavas.dotcompal.com/video/embed/s9vi6jzkmb" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                     box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                     </div> -->
                  <img src="assets/images/ProductBox.webp" class="img-fluid d-block mx-auto " alt="ProductBox">
               </div>
            </div>
         </div>
      </div>
      <!--1. Header Section End -->
      <!---New Section Start---->
      <div class="new-section content-mbl-space">
      <div class="container">
             <div class="row">
                 <div class="col-12 f-md-45 f-28 w500 text-center black-clr lh170 line-center ">
                     Save $805 RIGHT AWAY
                     <div class="gradient-clr w700 f-md-70 f-40">
                         Deal Ends Soon
                     </div>
                 </div>
             </div>
             <div class="row">
                 <div class="col-12 col-md-6">
                    <div class="f-16 f-md-18 lh140 w500">
                       <ul class="menu-list pl0 m0">
                          <li>GET Complete IntelliMate AI Package (FE + ALL Upgrades + Agency License) </li>
                          <li>No Monthly Payment Hassles- It's Just A One-Time Payment</li>
                          <li>GET Priority Support from Our Dedicated Support Engineers</li>
                       </ul>
                    </div>
                 </div>
                 <div class="col-12 col-md-6">
                    <div class="f-16 f-md-18 lh140 w500">
                       <ul class="menu-list pl0 m0">
                          <li>Provide Top-Notch Services to Your Clients</li>
                          <li>Grab All Benefits For A Low, ONE-TIME Discounted Price</li>
                          <li>GET 30-Days Money Back Guarantee</li>
                       </ul>
                    </div>
                 </div>
              </div>
             <div class="row">
                 <div class="col-12 mt20 mt-md40 f-md-26 f-22 lh140 w400 text-center black-clr">
                     <div class="f-24 f-md-36 w500 lh150 text-center black-clr">
                         <img src="assets/images/hurry.png " alt="Hurry " class="mr10 hurry-img">
                         <span class="red-clr ">Hurry!</span> Special One Time Price for Limited Time Only!
                     </div>
                     <div>
                         Use Coupon Code <span class="w600 neon-clr">"INTELLIMATE"</span> for an Additional <span class="w600 neon-clr">$50 Discount</span> 
                     </div>
                     <div class="mt20 red-clr w600">
                     <img src="assets/images/red-close.webp " alt="Hurry" class="mr10 close-img">
                        No Recurring | No Monthly | No Yearly
                     </div>
                     <div class="col-md-8 col-12 instant-btn mt20 f-24 f-md-30 mx-auto px0">
                         <a href="#buybundle">
                      GET Bundle Offer Now &nbsp;<i class="fa fa-angle-double-right f-32 f-md-42 angle-right" aria-hidden="true"></i>
                      </a>
                     </div>
                     <div class="col-12 mt-md20 mt20 f-18 f-md-18 w600 lh140 text-center black-clr">
                         No Download or Installation Required
                     </div>
                     <div class="col-12 mt20 mt-md20">
                         <img src="assets/images/compaitable-with.png" class="img-fluid mx-auto d-block">
                      </div>
                 </div>
             </div>
         </div>
      </div>
      <!--1. New Section End -->
      <!--2. Second Section Start -->
      <div class="section1">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-md-45 f-28 black-clr  text-center w400 lh140">
                  Here's What You GET With <span class="w700">Limited Time <br class="d-none d-md-block"> IntelliMate AI Discounted</span> Bundle Today
                  </div>
               </div>
               <!-- AGENCY BUNDLE START -->
               <div class="col-12  section-margin px-md-0">
                  <div class="col-12 plan1-shape">
                     <div class="col-md-10 col-12 px-md15 mt-md0 mt20 mx-auto">
                        <div class="pimg">
                           <img src="assets/images/commercial.webp" class="img-fluid d-block mx-auto" alt="Commercial">
                        </div>
                     </div>
                     <div class="row p0 mt20 mx-0 mt-md0">
                        <div class="col-12 p0 text-center mt-md50 mt20">
                           <div class="title-shape">
                              <div class="f-24 f-md-38 w600 lh150 white-clr text-center">
                                 IntelliMate AI Commercial ($37)
                              </div>
                           </div>
                        </div>
                        <div class="col-12 col-md-6 px0 px-md15">
                           <div class="f-20 f-md-20 lh150 w400 mt20 mt-md50">
                              <ul class="kaptick pl0">
                                 <li>
                                    Create 100 WorkSpace
                                 </li>
                                 <li>Collect 10000 Leads</li>
                                 <li>Train IntelliMate AI using Any Website URL, Doc File or with your own data</li>
                                 <li>Train IntelliMate AI using a list of Q&A's or FAQ's</li>
                                 <li>Embed IntelliMate AI on Any WebSite / Funnel / Page with Just one line of code.</li>
                                 <li>Seamless Integration With 20+ Top Autoresponders To Send Emails To Your Clients/Leads On Automation</li>
                                 <li>Natural Language Processing capabilities Generate Human-like or Better interactions</li>
                                 <li>Intent Recognition allows the chatbot to provide relevant and personalized responses.</li>
                                 <li>Contextual Understanding enables more meaningful and coherent interactions with users.</li>
                                 <li>Detailed analytics and insights.</li>
                                 <li>Customized to suit your specific needs and industry.</li>
                                 <li>Ability to Train Bots in Multiple Languages</li>
                                 <li>Ability to Update IntelliMate AI AI anytime</li>
                                 <li>Complete Visitor Analytics</li>
                                
                                 <!-- <li>Get FREE 1 TB Storage</li> -->
                              </ul>
                           </div>
                        </div>
                        <div class="col-12 col-md-6 px0 px-md15">
                           <div class="f-20 f-md-20 lh150 w400 mt0 mt-md50">
                              <ul class="kaptick pl0 p0 m0">
                              <li>128-Bit SSL Encryption for Maximum Security of your Data & Files</li>
                                 <li>Completely Cloud-Based Platform - No Domain, Hosting or Installation Required</li>
                                 <li>100% ADA, GDPR - SPAM Compliant</li>
                                 <li>Customized Drag & Drop Business Central Dashboard</li>
                                 <li>No Coding, Design or Tech Skills Needed</li>
                                 <li>Easy And Intuitive To Use Software With Step By Step Video Training</li>
                                 <li>100% Newbie friendly</li>
                                 <li>24*5 Customer Support</li>
                                 <li>Commercial license Included</li>
                                 <li>Unparallel Price</li>
                                 <li>BONUSES WORTH $1,988 FREE!!!</li>
                                 <li>Bonus #1 - Live Training - 0-10k a Month With Intellimate AI (First 1000 buyers only - $1000 Value)</li>
                                 <li>Bonus #2 - Video Training on How To Start Online Business</li>
                                 <li>Bonus #3 - Video Training on How to Train Your Bot</li>
                                 <li>Bonus #4 - Video Training on How to Boost your Sales with Bot</li>
                               
                              </ul>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- AGENCY BUNDLE END -->
               <!-- ELITE BUNDLE START -->
               <div class="col-12 px-md0  section-margin">
                  <div class="col-12 plan2-shape">
                     <div class="col-md-12 mx-auto mt-md0 mt20">
                        <div class="pimg">
                           <img src="assets/images/elite.webp" class="img-fluid d-block mx-auto" alt="Elite">
                        </div>
                     </div>
                     <div class="row mt-md0 mt20 mx-0">
                        <div class="col-12 p0 text-center mt-md50 mt20">
                           <div class="title-shape">
                              <div class="f-22 f-md-28 w600 white-clr lh150 text-center">
                                 Pro Upgrade 1- IntelliMate AI Elite ($77)
                              </div>
                           </div>
                        </div>
                        <div class="col-12 col-md-6 px0 px-md15">
                           <div class="f-20 f-md-20 lh150 w400 mt20 mt-md50">
                              <ul class="kaptick pl0">
                                 <li>UNLIMITED WorkSpace To Keep Each of Your & Client's Project Separate</li>
                                 <li>UNLIMITED Lead collection using IntelliMate AI Smart Lead Collection feature</li>
                                 <li>UNLIMITED Embed IntelliMate AI on UNLIMITED WebSites / Funnels / Pages. </li>
                                 <li>UNLIMITED Email Lists using IntelliMate AI</li>
                                 <li>UNLIMITED Contact Managmenet using Inbuilt Contact Management System</li>
                                 <li>Get Your Subscribers Auto-Registered For Your Webinars With Webinar Platform Integrations</li>
                                 <li>Activate IntelliMate AI in 50 More Languages</li> 
                                 <!--<li>Get Notifications of Every Lead you Receive so you can Approch Instant</li>       -->
                                 <li>Personalized Conversations with Your Visitors & Customers</li> 
                                 <li>Regenerate Response Generated By IntelliMate AI</li>
                                 <li>Keyword Triggered Responses</li>
                               
                              </ul>
                           </div>
                        </div>
                        <div class="col-12 col-md-6 px0 px-md15">
                           <div class="f-20 f-md-20 lh150 w400 mt0 mt-md50">
                              <ul class="kaptick pl0 p0 m0">
                                 <li>Manage Leads Effortlessly & make the most from them with our powerful lead management feature.</li>
                                 <li>Self Updating IntelliMate AI ( IntelliMate AI Stores the response & generate relevant response based on History for example - Ton/Language/Word Limit/Response Style )</li>
                                 <li>Fully Encrypted IntelliMate AI Chat Management </li>
                                 <li>100% Chat Privacy</li> 
                                 <li>Get the Desired Results using Trained Chat-Bots</li> 
                                 <li>Unparallel price</li>
                                 <li>Elite Upgrade Bonuses</li>
                                 <li>Bonus #1 - Buying Traffic to Generate Massive Website Visitors</li>
                                 <li>Bonus #2 - Blockbuster Affiliate Marketing</li>
                                 <li>Bonus #3 - WordPress Fast Track </li>
                                 <li>Bonus #4 - Webinar Mastery</li>
                                 <li>Bonus #5 - SEO And Tracking</li>
                              </ul>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- ELITE BUNDLE END -->
               <!-- ENTERPRISE BUNDLE START -->
               <div class="col-12 px-md-0 section-margin">
                  <div class="col-12 plan1-shape">
                     <div class="col-md-12 mx-auto px-md15 mt-md0 mt20">
                        <div class="pimg">
                           <img src="assets/images/enterprise.webp" class="img-fluid d-block mx-auto" alt="Enterprise">
                        </div>
                     </div>
                     <div class="row p0 mt20 mx-0 mt-md0">
                        <div class="col-12 p0 text-center mt-md50 mt20">
                           <div class="title-shape">
                              <div class="f-22 f-md-28 w600 white-clr lh150 text-center">
                                 Pro Upgrade 2- IntelliMate AI Enterprise – ( $97 )
                              </div>
                           </div>
                        </div>
                        <div class="col-12 col-md-6 px0 px-md15">
                           <div class="f-20 f-md-20 lh150 w400 mt20 mt-md50">
                              <ul class="kaptick pl0">
                                 <li>Rebrand IntelliMate AI & Make it Like Yours</li>
                                 <li>Rebrand IntelliMate AI on Any website/funnel/Page by Changing It's Name</li> 
                                 <li>Rebrand IntelliMate AI on Any website/funnel/Page by Changing It's Logo & Image</li>   
                                 <li>Rebrand IntelliMate AI on Any website/funnel/Page by Changing Header Theme Color</li>
                                 <li>Rebrand IntelliMate AI on Any website/funnel/Page by Changing Background Image</li>
                                 <li>Rebrand IntelliMate AI on Any website/funnel/Page by Changing Bot Text & Background Color</li>
                                 <li>Rebrand IntelliMate AI on Any website/funnel/Page by Changing User Text & Background Color</li>
                                 <li>Set up a Welcome Message on Your Own Branded Bot</li>
                                 <li>Set up a Triger & Response based Chat Funnel</li>
                                 <li>Set up Thank you Message at End of Conversation</li>
                                 <li>Time-based Chat Initiation</li>
                                 <li>Auto Pop-up Lead Collection form When Chat Starts or End</li>
                                 <li>Advanced Engagement Booster</li>
                                 <li>Keyword Triggered Responses</li>
                               
                              </ul>
                           </div>
                        </div>
                        <div class="col-12 col-md-6 px0 px-md15">
                           <div class="f-20 f-md-20 lh150 w400 mt0 mt-md50">
                              <ul class="kaptick pl0 p0 m0">
                                 <li>Time Delayed Chat Triggers</li>
                                 <li>Strengthen Your Relationship With Your Customers Using CRM Integrations</li>
                                 <li>Grow your Sales & Marketing with IntelliMate AI</li>
                                 <li>Collect Reviews on your Website </li>
                                 <li>Book Appointments & Meetings</li>
                                 <li>Survey & Feedbacks</li>
                                 <li>5 Satr Ratings</li>
                                 <li>Engage Leads</li>
                                 <li>Show Affiliate offers</li>
                                 <li>Unparallel price</li>
                                 <li>BONUSES WORTH $1,988 FREE!!!</li>
                                 <li>Bonus #1 - 100 Website Business Models</li>
                                 <li>Bonus #2 - 60 Photoshop Action Scripts</li>
                                 <li>Bonus #3 - Free Website Traffic Source</li>
                                 <li>Bonus #4 - 100 Video Transition Backgrounds</li>
                                 <li>Bonus #5 - AI YouTube Masterclass</li>
                              </ul>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- ENTERPRISE BUNDLE END -->
               <!-- BUSINESS DRIVE BUNDLE START -->
               <div class="col-12 px-md0  section-margin">
                  <div class="col-12 plan2-shape">
                     <div class="col-md-10 mx-auto mt-md0 mt20">
                        <div class="pimg">
                           <img src="assets/images/agency.webp" class="img-fluid d-block mx-auto" alt="Agency">
                        </div>
                     </div>
                     <div class="row mt-md0 mt20 mx-0">
                        <div class=" col-12 p0 text-center mt-md50 mt20">
                           <div class="title-shape">
                              <div class="f-22 f-md-28 w600 white-clr lh150 text-center">
                                 Pro Upgrade 3- IntelliMate AI Agency ($97)
                              </div>
                           </div>
                        </div>
                        <div class="col-12 col-md-6 px0 px-md15">
                           <div class="f-20 f-md-20 lh150 w400 mt20 mt-md50">
                              <ul class="kaptick pl0">
                                 <li>Directly Provide Top Notch IntelliMate AI AI Smart Bot Creation Services to Your Client's and Charge Monthly or Recurring Fee For 100% Profits</li>
                                 <li>Completely Cloud-Based Platform - No Domain, Hosting or Installation Required</span></li>
                                 <li>Assign Clients to a Specific Workspace</li>
                                 <li>Serve Unlimited Clients With Agency License</li>
                                 <li>Serve Unlimited Team Members</li>
                                 <li>Dashboard & Chatbot Footer White Labeling for You & Your Clients</li>
                                 <li>Accurate Analysis of Client's Activities For Effective Monitoring</li>
                                 <li>Dedicated Support To You & Your Clients - Chat Support Directly From Software</li>
                              </ul>
                           </div>
                        </div>
                        <div class="col-12 col-md-6 px0 px-md15">
                           <div class="f-20 f-md-20 lh150 w400 mt0 mt-md50">
                              <ul class="kaptick pl0 p0 m0">
                                
                                 <li>Business Management System To Manage Your Clients Plans</li>
                                 <li>No Recurring/ Monthly or Yearly Fee Ever</li>
                                 <li>Provide High In Demand Services</li>
                                 <li>Unparallel price</li>
                                 <li>Bonus #1 - SEO And Tracking</li>
                                 <li>Bonus #2 - How To Set Up Your First Website</li>
                                 <li>Bonus #3 - Free Business Videos</li>
                                 <li>Bonus #4 - Blogging Traffic Mantra </li>
                                 <li>Bonus #5 - Christmas Themed & Wordpress Themes</li>
                              </ul>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- BUSINESS DRIVE BUNDLE END -->

               <!-- sitesmart-Ai start -->
               <div class="col-12 px-md0  section-margin">
                  <div class="col-12 plan2-shape">
                     <div class="col-md-12 mx-auto mt-md0 mt20">
                        <div class="pimg">
                           <img src="assets/images/sitesmataI-product-Box.webp" class="img-fluid d-block mx-auto" alt="sitesmataI-product-Box">
                        </div>
                     </div>
                     <div class="row mt-md0 mt20 mx-0">
                        <div class="col-12 p0 text-center mt-md50 mt20">
                           <div class="title-shape">
                              <div class="f-22 f-md-28 w600 white-clr lh150 text-center">
                                 Pro Upgrade 4 - SiteSmart AI ($297) 
                              </div>
                           </div>
                        </div>
                        <div class="col-12 col-md-6 px0 px-md15">
                           <div class="f-20 f-md-20 lh150 w400 mt20 mt-md50">
                              <ul class="kaptick pl0">
                                 <li>Create unlimited websites to serve as many clients as you want Deploy stunning websites by leveraging chatgpt</li>
                                 <li>Ai websites prefilled with smoking hot, ai, human-like content</li>
                                 <li>Easily create & sell beautiful, highly professional website filled with unique content in 1000+ niches</li>
                                 <li>Auto publish own content and images to your website posts & pages all the day, without you ever having to log in ChatGPT</li>
                                 <li>Woo-commerce integration sites, affiliate review/niche sites, portfolio sites & more</li>
                                 <li>Commercial license included - Use for your clients</li>
                                 <li>White-labelling functionality for developers to show your developers details on theme without touching any code</li> 
                                 <li>Drive unlimited leads from website, blog, page & e-com sites</li>
                                 <!-- <li>Get Notifications of Every Lead you Receive so you can Approch Instant</li>       
                                 <li>Personalized Conversations with Your Visitors &amp; Customers</li> 
                                 <li>Regenerate Response Generated By IntelliMate AI</li>
                                 <li>Keyword Triggered Responses</li> -->
                               
                              </ul>
                           </div>
                        </div>
                        <div class="col-12 col-md-6 px0 px-md15">
                           <div class="f-20 f-md-20 lh150 w400 mt0 mt-md50">
                              <ul class="kaptick pl0 p0 m0">
                                
                                 <li>Get 400+ eye-catchy and customizable color-templates</li>
                                 <li>Cutting-edge autoresponder integration to send emails to your subscribers automatically</li>
                                 <li>Create even more engaging & beautiful websites with 1 million+ royalty free stock photos</li> 
                                 <li>Skilfully crafted unlimited logos templates with editable files</li> 
                                 <li>Share control of your business with unlimited team members</li>
                                 <li>Get Unlimited Agency license</li>
                                 <li>Sell it to anyone you want and keep 100% profits in your pocket</li>
                                 <li>No product, sales page, marketing material creation</li>
                                 <li>Rebranding by adding your own logo & brand name</li>
                                 <!-- <li>Bonus #4 - Webinar Mastery</li>
                                 <li>Bonus #5 - SEO And Tracking</li> -->
                              </ul>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!--  -->
               <!-- FAST ACTION BONUSES BUNDLE START -->
               <div class="col-12 px-md-0  section-margin">
                  <div class="col-12 plan1-shape">
                     <div class="mx-auto mt-md0 mt20">
                        <div class="pimg ">
                           <img src="assets/images/fast-action.webp" class="img-fluid d-block mx-auto" alt="Fast-Action">
                        </div>
                     </div>
                     <div class="row mt20">
                        <div class="col-12 p0 mt-md50 mt20 text-center">
                           <div class="title-shape1">
                              <div class="f-22 f-md-28 w600 white-clr lh150 text-center text-capitalize">
                                 And You're Also Getting These Premium Fast Action Bonuses Worth ($497)
                              </div>
                           </div>
                        </div>
                        <div class="col-12 col-md-6 px0 px-md15">
                           <div class="f-20 f-md-20 lh150 w600 mt20 mt-md50">
                              <ul class="kaptick pl0">
                                 <li>Bonus #1 - Live Training How to Make Money Using IntelliMate AI</li>
                                 <li>Bonus #2 -Video Training On How To Start Online Business</li>
                              </ul>
                           </div>
                        </div>
                        <div class="col-12 col-md-6 px0 px-md15">
                           <div class="f-20 f-md-20 lh150 w600 mt0 mt-md50">
                              <ul class="kaptick pl0 p0 m0">
                                 <li>Bonus #3 -IntelliMate AI Guide Workshop</li>
                                 <li>Bonus #4 -Video Training On Get Your Head Into The AI</li>
                              </ul>
                           </div>
                        </div>
                        <!-- <div class="col-12 col-md-6 px0 px-md15">
                           <div class="f-20 f-md-20 lh150 w600 mt20 mt-md50">
                               <ul class="kaptick pl0">
                                   <li>Super Value Bonus #5 – Video Training To Monetizing Your Website </li>
                                   <li>Super Value Bonus #6 – Training To Start Affiliate Marketing </li>
                                   <li>Super Value Bonus #7 – Video Training On Viral Marketing </li>
                                   <li>Super Value Bonus #8 – Video Marketing Profit Kit </li>
                               </ul>
                           </div>
                           </div> -->
                     </div>
                  </div>
               </div>
               <!-- FAST ACTION BONUSES BUNDLE END -->
            </div>
         </div>
      </div>
      <!--2. Second Section End -->
      <!--3. Third Section Start -->
      <div class="section2">
         <div class="container">
            <div class="row ">
               <div class="col-12 px0 px-md15" id="buybundle">
                  <div class="f-md-45 f-28 black-clr  text-center w700 lh140">
                     Grab This Exclusive BUNDLE DISCOUNT Today…
                  </div>
                  <div class="f-18 f-md-22 w400 lh150 text-center mt20 black-clr">
                     Use Coupon Code <span class="neon-clr w700">"INTELLIMATE"</span> for an Additional <span class="neon-clr w700"> $50</span> Discount
                  </div>
               </div>
            </div>
            <div class="row justify-content-center">
               <div class="col-md-6 col-12 mt25 mt-md50 px-md15">
                  <div class="tablebox1">
                     <div class="col-12 tablebox-inner-bg text-center">
                        <div class="relative col-12 pt20 pt-md0">
                       <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 742.13 152.79" style="max-height: 75px;"><defs><style>.cls-1{fill:#fff;}.cls-2{fill:url(#linear-gradient);}.cls-3{fill:#21a1ff;}.cls-4{fill:#2ddfff;}.cls-5{fill:url(#linear-gradient-2);}.cls-6{fill:#f1f1f2;}</style><linearGradient id="linear-gradient" x1="667.64" y1="93.3" x2="726.15" y2="93.3" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#2ee2ff"></stop><stop offset="1" stop-color="#219fff"></stop></linearGradient><linearGradient id="linear-gradient-2" x1="23.17" y1="76.39" x2="157.15" y2="76.39" xlink:href="#linear-gradient"></linearGradient></defs><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1" d="M220,127.18H210V59.43h10Z"></path><path class="cls-1" d="M264.32,103.36a14.48,14.48,0,1,0-28.95,0v23.82H226V103.36a23.83,23.83,0,1,1,47.65,0v23.82h-9.35Z"></path><path class="cls-1" d="M289.05,89v19.2a9.7,9.7,0,0,0,9.75,9.75h4.53v9.35H298.8a19.1,19.1,0,0,1-19.1-19.1V67.37h9.35V79.63h14.28V89Z"></path><path class="cls-1" d="M319.61,108.48a15.43,15.43,0,0,0,27,4h10.56a24.77,24.77,0,1,1,1.81-9.14,21.23,21.23,0,0,1-.61,5.12h-38.8Zm29.45-9.35a15.48,15.48,0,0,0-29.85,0Z"></path><path class="cls-1" d="M374.4,108a9.78,9.78,0,0,0,9.75,9.85h4.52v9.35h-4.52a19.18,19.18,0,0,1-19.1-19.2V67.17h9.35Z"></path><path class="cls-1" d="M404.05,108a9.78,9.78,0,0,0,9.75,9.85h4.52v9.35H413.8A19.18,19.18,0,0,1,394.7,108V67.17h9.35Z"></path><path class="cls-1" d="M433.71,127.18h-9.35V79.63h9.35Z"></path><path class="cls-1" d="M496.23,77.42l-23.32,24.83L449.69,77.42v49.76h-9.95V59.43h6.63l26.54,28.24,26.64-28.24h6.63v67.75h-9.95Z"></path><path class="cls-1" d="M559.86,127.18h-9.35V103.36A14.48,14.48,0,1,0,536,117.83v9.35a23.88,23.88,0,1,1,23.82-23.82Z"></path><path class="cls-1" d="M575.24,89v19.2a9.7,9.7,0,0,0,9.75,9.75h4.53v9.35H585a19.1,19.1,0,0,1-19.1-19.1V67.37h9.35V79.63h14.28V89Z"></path><path class="cls-1" d="M605.8,108.48a15.43,15.43,0,0,0,27,4h10.55a24.77,24.77,0,1,1,1.81-9.14,21.74,21.74,0,0,1-.6,5.12H605.8Zm29.45-9.35a15.48,15.48,0,0,0-29.85,0Z"></path><path class="cls-2" d="M693.48,59.43h6.83l25.84,67.75H715.49l-18.6-48.85L678.3,127.18H667.64Z"></path><path class="cls-3" d="M742.13,127.18h-10V59.43h10Z"></path><path class="cls-3" d="M161,69.48c0,11.86,0,22.69,0,34,3.94,1,10.59-1.4,14.61-6.34A17.06,17.06,0,0,0,177.32,78,16.86,16.86,0,0,0,161,69.48Z"></path><path class="cls-4" d="M18.5,69.48c0,11.86,0,22.69,0,34-3.94,1-10.6-1.4-14.61-6.34A17,17,0,0,1,2.17,78,16.84,16.84,0,0,1,18.5,69.48Z"></path><path class="cls-1" d="M90.13,118.12a23.33,23.33,0,0,1-16.67-6.7,2.78,2.78,0,0,1-.12-4.15,2.85,2.85,0,0,1,4.09.09,18.1,18.1,0,0,0,25.42,0,2.83,2.83,0,0,1,4.16-.13,2.78,2.78,0,0,1-.22,4.15,23,23,0,0,1-13.27,6.52c-.58.07-1.16.14-1.74.18S90.68,118.12,90.13,118.12Z"></path><path class="cls-1" d="M66.14,76.4c-4.22,0-7.84,1.86-10.89,5.46a2.66,2.66,0,0,0-.08,3.38,1.62,1.62,0,0,0,2.67-.08c5.48-4.64,11.8-4.9,16.61,0a1.62,1.62,0,0,0,2.72.1A2.65,2.65,0,0,0,77,81.86a14.1,14.1,0,0,0-8.67-5.31,10.57,10.57,0,0,0-1.13-.14C66.86,76.38,66.5,76.4,66.14,76.4Z"></path><path class="cls-1" d="M114.15,76.4c-4.22,0-7.84,1.86-10.89,5.46a2.64,2.64,0,0,0-.08,3.38,1.62,1.62,0,0,0,2.67-.08c5.47-4.64,11.8-4.9,16.61,0a1.61,1.61,0,0,0,2.71.1,2.65,2.65,0,0,0-.14-3.38,14.13,14.13,0,0,0-8.67-5.31,10.89,10.89,0,0,0-1.13-.14C114.87,76.38,114.51,76.4,114.15,76.4Z"></path><path class="cls-5" d="M124.12,39.68h-1.39l5.74-20.2a9.79,9.79,0,1,0-5.88-1.61L116.4,39.68H63.93l-6.2-21.81a9.75,9.75,0,1,0-5.87,1.61l5.75,20.2H56.2a33.07,33.07,0,0,0-33,33v47a33.08,33.08,0,0,0,33,33h67.92a33.08,33.08,0,0,0,33-33v-47A33.07,33.07,0,0,0,124.12,39.68Zm5.74-33.76a2.76,2.76,0,1,1,1.94,3.4A2.78,2.78,0,0,1,129.86,5.92ZM48.52,9.32a2.76,2.76,0,1,1,1.94-3.38A2.75,2.75,0,0,1,48.52,9.32Zm89.19,110.43a13.61,13.61,0,0,1-13.59,13.6H56.2a13.61,13.61,0,0,1-13.6-13.6v-47a13.61,13.61,0,0,1,13.6-13.6h67.92a13.61,13.61,0,0,1,13.59,13.6Z"></path><circle class="cls-6" cx="429.03" cy="70.88" r="4.75"></circle></g></g></svg>
                        </div>
                        <div class="col-12 mt-md40 mt20 table-headline-shape">
                           <div class="w700 f-md-28 f-20 text-center text-uppercase black-clr lh150 ">
                              Bundle Offer
                           </div>
                        </div>
                        <!-- PLAN 1 START -->
                        <div class="row mx-0 odd-shape mt-md40 mt20">
                           <div class="col-md-9 col-12 px-md15 px0">
                              <div class="w600 f-md-22 f-18 text-left text-xs-center text-uppercase white-clr lh150 mt8">
                                 COMMERCIAL
                              </div>
                           </div>
                           <div class="col-md-3 col-12 px-md15 px0">
                              <div class="col-12 price-shape">
                                 <div class="w600 f-md-28 f-20 text-center black-clr lh150">
                                    $37
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- PLAN 1 END -->
                        <!-- PLAN 2 START -->
                        <div class="row mx-0 even-shape">
                           <div class="col-md-9 col-12 px-md15 px0">
                              <div class="w600 f-md-22 f-18 text-left text-xs-center text-uppercase white-clr lh150 mt8">
                                 ELITE  
                              </div>
                           </div>
                           <div class="col-md-3 col-12 px-md15 px0">
                              <div class="col-12 price-shape">
                                 <div class="w600 f-md-28 f-20 text-center black-clr lh150">
                                    $77
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- PLAN 2 END -->
                        <!-- PLAN 3 START -->
                        <div class="row mx-0 odd-shape">
                           <div class="col-md-9 col-12 px-md15 px0">
                              <div class="w600 f-md-22 f-18 text-left text-xs-center text-uppercase white-clr lh150 mt8">
                                 ENTERPRISE  
                              </div>
                           </div>
                           <div class="col-md-3 col-12 px-md15 px0">
                              <div class="col-12 price-shape">
                                 <div class="w600 f-md-28 f-20 text-center black-clr lh150">
                                    $97
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- PLAN 3 END -->
                        <!-- PLAN 4 START -->
                        <div class="row mx-0 even-shape">
                           <div class="col-md-9 col-12 px-md15 px0">
                              <div class="w600 f-md-22 f-18 text-left text-xs-center text-uppercase white-clr lh150 mt8">
                                 AGENCY
                              </div>
                           </div>
                           <div class="col-md-3 col-12 px-md15 px0">
                              <div class="col-12 price-shape">
                                 <div class="w600 f-md-28 f-20 text-center black-clr lh150">
                                    $97
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- PLAN 4 END -->

                         <!-- PLAN 5 START -->
                         <div class="row mx-0 odd-shape">
                           <div class="col-md-9 col-12 px-md15 px0">
                              <div class="w600 f-md-22 f-18 text-left text-xs-center text-uppercase white-clr lh150 mt8">
                              SITESMART AI
                              </div>
                           </div>
                           <div class="col-md-3 col-12 px-md15 px0">
                              <div class="col-12 price-shape">
                                 <div class="w600 f-md-28 f-20 text-center black-clr lh150">
                                    $297
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- PLAN 5 END -->
                        <!-- PLAN 6 START -->
                        <!-- <div class="row mx-0 even-shape">
                           <div class="col-md-9 col-12 px-md15 px0">
                              <div class="w600 f-md-22 f-18 text-left text-xs-center text-uppercase white-clr lh150 mt8">
                              PREMIUM BONUS 2
                              </div>
                           </div>
                           <div class="col-md-3 col-12 px-md15 px0">
                              <div class="col-12 price-shape">
                                 <div class="w600 f-md-28 f-20 text-center black-clr lh150">
                                    $97
                                 </div>
                              </div>
                           </div>
                        </div> -->
                        <!-- PLAN 6 END -->
                        <!-- PLAN 7 START -->
                        <div class="row mx-0 even-shape">
                           <div class="col-md-9 col-12 px-md15 px0">
                              <div class="w600 f-md-22 f-18 text-left text-xs-center text-uppercase white-clr lh150 mt8">
                                 FAST ACTION BONUSES 
                              </div>
                           </div>
                           <div class="col-md-3 col-12 px-md15 px0">
                              <div class="col-12 price-shape">
                                 <div class="w600 f-md-28 f-20 text-center black-clr lh150">
                                    $497
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- PLAN 5 END -->
                        <div class="col-12 px-md0 px15 mt10 w500 f-md-28 f-18 white-clr text-center">
                           Regular <strike>$1102</strike>
                        </div>
                        <div class="row justify-content-center mx-0 ">
                           <div class="col-md-8 col-12 now-price mt10 w700 f-md-45 f-28 white-clr text-center">
                              Now $297
                           </div>
                        </div>
                        <div class="col-12 px-md0 px15 mt10 mt-md10 w500 f-md-25 f-20 white-clr text-center">
                           One Time Fee
                        </div>
                        <div class="mt10 red-clr w600 f-22 d-md-27 lh140">
                           <img src="assets/images/red-close.webp" alt="Hurry" class="mr10 close-img">
                              No Recurring | No Monthly | No Yearly
                           </div>
                        <div class="row justify-content-center mx-0 ">
                           <div class="myfeatures f-md-25 f-16 w400 text-center lh160">
                              <div class="hideme-button">
                                 <!-- <a href="https://www.jvzoo.com/b/109507/398672/2"><img src="https://i.jvzoo.com/109507/398672/2" alt="IntelliMate AI Bundle" border="0" class="img-fluid d-block mx-auto"/></a>  -->
                                 <a href="https://www.jvzoo.com/b/110349/402227/2"><img src="https://i.jvzoo.com/110349/402227/2" alt="IntelliMate AI Bundle" border="0" class="img-fluid d-block mx-auto"/></a>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--3. Third Section End -->
      <!----Second Section---->
      <div class="second-header-section">
         <div class="container ">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-md-45 f-24 w600 lh140 text-center white-clr">
                     TOP Reasons Why You Can't Ignore <br class="d-none d-md-block"> <span class="f-md-50 f-24 w700 "> IntelliMate AI Bundle Deal </span>
                  </div>
               </div>
            </div>
            <div class="row mt30 mt-md65">
               <div class="col-12 col-md-10 mx-auto">
                  <div class="step-shape">
                     <div class="f-20 f-md-22 w600 white-clr lh150">
                        Reason #1
                     </div>
                     <div class="f-18 f-md-18 w400 white-clr lh150 mt10 ">
                        Get All The Benefits Of IntelliMate AI & Premium Upgrades To Multiply Your Results... Save More Time, Work Like A Pro & Ultimately Boost Your Agency Profits
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt30">
                  <div class="step-shape">
                     <div class="f-20 f-md-22 w600 white-clr lh150">
                        Reason #2
                     </div>
                     <div class="f-18 f-md-18 w400 white-clr lh150 mt10 ">
                        Regular Price For IntelliMate AI, All Upgrades & Bonuses Is $1102. You Are Saving $805 Today When You Grab The Exclusive Bundle Deal Now at ONLY $297.
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt30">
                  <div class="step-shape">
                     <div class="f-20 f-md-22 w600 white-clr lh150">
                        Reason #3
                     </div>
                     <div class="f-18 f-md-18 w400 white-clr lh150 mt10 ">
                        This Limited Time Additional $50 Coupon Will Expires As Soon As Timer Hits Zero So Take Action Now.
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt30">
                  <div class="step-shape">
                     <div class="f-20 f-md-22 w600 white-clr lh150">
                        Reason #4
                     </div>
                     <div class="f-18 f-md-18 w400 white-clr lh150 mt10 ">
                        Get Priority Support From Our Dedicated Support Team to Get Assured Success.
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!----Second Section---->
      <div class="riskfree-section">
        <div class="container ">
            <div class="row align-items-center ">
                <div class="f-28 f-md-45 w700 black-clr text-center col-12 mb-md40">Test Drive <u>EVERYTHING</u> IntelliMate AI Has To Offer Risk Free For 30 Days
                </div>
                <div class="col-md-6 col-12 mt15 mt-md0">
                    <div class="f-md-18 f-18 w400 lh150 black-clr"><span class="w600">Your purchase is secured with our 30-day 100% money back guarantee.</span>
                        <br><br>
                        So, you can buy with full confidence and try it for yourself and for your customers risk free.&nbsp;If you face any issue or don't get the results you desire after using it, just raise a ticket on our support desk within 30 days and we'll refund you everything, down to the last penny.
                        <br><br>
                        However, we want to clearly state that we do NOT offer a “no questions asked” money back guarantee. You <span class="w700"> must </span> provide a <span class="w700"> genuine reason </span> when you contact our support and we will refund your hard-earned money.
                    </div>
                </div>
                <div class="col-md-6 col-12 mt20 mt-md0">
                    <img src="assets/images/riskfree-img.webp" class="img-fluid d-block mx-auto" alt="Money Back Guarantee">
                </div>
            </div>
        </div>
    </div>
      <!-- <div class="riskfree-section">
         <div class="container ">
            <div class="row align-items-center ">
               <div class="f-28 f-md-45 w700 white-clr text-center col-12 mb-md40">Test Drive IntelliMate AI Risk FREE For 30 Days
               </div>
               <div class="col-md-7 col-12 order-md-2">
                  <div class="f-md-18 f-18 w400 lh150 white-clr mt15 "><span class="w600">Your purchase is secured with our 30-day 100% money back guarantee.</span>
                     <br><br>
                     <span class="w600 ">So, you can buy with full confidence and try it for yourself and for your client’s risk free.</span> If you face any Issue or don’t get results you desired after using it, just raise a ticket on support desk
                     within 30 days and we'll refund you everything, down to the last penny.<br><br> We would like to clearly state that we do NOT offer a “no questions asked” money back guarantee. You must provide a genuine reason when you contact
                     our support and we will refund your hard-earned money.
                  </div>
               </div>
               <div class="col-md-5 order-md-1 col-12 mt20 mt-md0 ">
                  <img src="assets/images/riskfree-img.webp " class="img-fluid d-block mx-auto " alt="Risk Free">
               </div>
            </div>
         </div>
      </div> -->
      <!--3. Third Section Start -->
      <div class="section2">
         <div class="container">
            <div class="row ">
               <div class="col-12 px0 px-md15" id="buybundle">
                  <div class="f-md-45 f-28 f  text-center w700 lh140">
                     Limited Time Offer!
                  </div>
                  <div class="f-22 f-md-32 w700 lh150 text-center mt20 black-clr">
                     Get Complete Package of All <span class="w700">IntelliMate AI</span> Products<br class="d-md-block d-none"> for A Low One-Time Fee
                  </div>
               </div>
               <div class="col-12 f-22 f-md-22 w400 lh150 text-center mt20 black-clr">
                  Use Coupon Code <span class="neon-clr w700 ">"INTELLIMATE"</span> for an Additional <span class="neon-clr w700 "> $50</span> Discount
               </div>
            </div>
            <div class="row justify-content-center">
               <div class="col-md-6 col-12 mt25 mt-md50 px-md15">
                  <div class="tablebox1">
                     <div class="col-12 tablebox-inner-bg text-center">
                        <div class="relative col-12 p0">
                          <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 742.13 152.79" style="max-height: 75px;"><defs><style>.cls-1{fill:#fff;}.cls-2{fill:url(#linear-gradient);}.cls-3{fill:#21a1ff;}.cls-4{fill:#2ddfff;}.cls-5{fill:url(#linear-gradient-2);}.cls-6{fill:#f1f1f2;}</style><linearGradient id="linear-gradient" x1="667.64" y1="93.3" x2="726.15" y2="93.3" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#2ee2ff"></stop><stop offset="1" stop-color="#219fff"></stop></linearGradient><linearGradient id="linear-gradient-2" x1="23.17" y1="76.39" x2="157.15" y2="76.39" xlink:href="#linear-gradient"></linearGradient></defs><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1" d="M220,127.18H210V59.43h10Z"></path><path class="cls-1" d="M264.32,103.36a14.48,14.48,0,1,0-28.95,0v23.82H226V103.36a23.83,23.83,0,1,1,47.65,0v23.82h-9.35Z"></path><path class="cls-1" d="M289.05,89v19.2a9.7,9.7,0,0,0,9.75,9.75h4.53v9.35H298.8a19.1,19.1,0,0,1-19.1-19.1V67.37h9.35V79.63h14.28V89Z"></path><path class="cls-1" d="M319.61,108.48a15.43,15.43,0,0,0,27,4h10.56a24.77,24.77,0,1,1,1.81-9.14,21.23,21.23,0,0,1-.61,5.12h-38.8Zm29.45-9.35a15.48,15.48,0,0,0-29.85,0Z"></path><path class="cls-1" d="M374.4,108a9.78,9.78,0,0,0,9.75,9.85h4.52v9.35h-4.52a19.18,19.18,0,0,1-19.1-19.2V67.17h9.35Z"></path><path class="cls-1" d="M404.05,108a9.78,9.78,0,0,0,9.75,9.85h4.52v9.35H413.8A19.18,19.18,0,0,1,394.7,108V67.17h9.35Z"></path><path class="cls-1" d="M433.71,127.18h-9.35V79.63h9.35Z"></path><path class="cls-1" d="M496.23,77.42l-23.32,24.83L449.69,77.42v49.76h-9.95V59.43h6.63l26.54,28.24,26.64-28.24h6.63v67.75h-9.95Z"></path><path class="cls-1" d="M559.86,127.18h-9.35V103.36A14.48,14.48,0,1,0,536,117.83v9.35a23.88,23.88,0,1,1,23.82-23.82Z"></path><path class="cls-1" d="M575.24,89v19.2a9.7,9.7,0,0,0,9.75,9.75h4.53v9.35H585a19.1,19.1,0,0,1-19.1-19.1V67.37h9.35V79.63h14.28V89Z"></path><path class="cls-1" d="M605.8,108.48a15.43,15.43,0,0,0,27,4h10.55a24.77,24.77,0,1,1,1.81-9.14,21.74,21.74,0,0,1-.6,5.12H605.8Zm29.45-9.35a15.48,15.48,0,0,0-29.85,0Z"></path><path class="cls-2" d="M693.48,59.43h6.83l25.84,67.75H715.49l-18.6-48.85L678.3,127.18H667.64Z"></path><path class="cls-3" d="M742.13,127.18h-10V59.43h10Z"></path><path class="cls-3" d="M161,69.48c0,11.86,0,22.69,0,34,3.94,1,10.59-1.4,14.61-6.34A17.06,17.06,0,0,0,177.32,78,16.86,16.86,0,0,0,161,69.48Z"></path><path class="cls-4" d="M18.5,69.48c0,11.86,0,22.69,0,34-3.94,1-10.6-1.4-14.61-6.34A17,17,0,0,1,2.17,78,16.84,16.84,0,0,1,18.5,69.48Z"></path><path class="cls-1" d="M90.13,118.12a23.33,23.33,0,0,1-16.67-6.7,2.78,2.78,0,0,1-.12-4.15,2.85,2.85,0,0,1,4.09.09,18.1,18.1,0,0,0,25.42,0,2.83,2.83,0,0,1,4.16-.13,2.78,2.78,0,0,1-.22,4.15,23,23,0,0,1-13.27,6.52c-.58.07-1.16.14-1.74.18S90.68,118.12,90.13,118.12Z"></path><path class="cls-1" d="M66.14,76.4c-4.22,0-7.84,1.86-10.89,5.46a2.66,2.66,0,0,0-.08,3.38,1.62,1.62,0,0,0,2.67-.08c5.48-4.64,11.8-4.9,16.61,0a1.62,1.62,0,0,0,2.72.1A2.65,2.65,0,0,0,77,81.86a14.1,14.1,0,0,0-8.67-5.31,10.57,10.57,0,0,0-1.13-.14C66.86,76.38,66.5,76.4,66.14,76.4Z"></path><path class="cls-1" d="M114.15,76.4c-4.22,0-7.84,1.86-10.89,5.46a2.64,2.64,0,0,0-.08,3.38,1.62,1.62,0,0,0,2.67-.08c5.47-4.64,11.8-4.9,16.61,0a1.61,1.61,0,0,0,2.71.1,2.65,2.65,0,0,0-.14-3.38,14.13,14.13,0,0,0-8.67-5.31,10.89,10.89,0,0,0-1.13-.14C114.87,76.38,114.51,76.4,114.15,76.4Z"></path><path class="cls-5" d="M124.12,39.68h-1.39l5.74-20.2a9.79,9.79,0,1,0-5.88-1.61L116.4,39.68H63.93l-6.2-21.81a9.75,9.75,0,1,0-5.87,1.61l5.75,20.2H56.2a33.07,33.07,0,0,0-33,33v47a33.08,33.08,0,0,0,33,33h67.92a33.08,33.08,0,0,0,33-33v-47A33.07,33.07,0,0,0,124.12,39.68Zm5.74-33.76a2.76,2.76,0,1,1,1.94,3.4A2.78,2.78,0,0,1,129.86,5.92ZM48.52,9.32a2.76,2.76,0,1,1,1.94-3.38A2.75,2.75,0,0,1,48.52,9.32Zm89.19,110.43a13.61,13.61,0,0,1-13.59,13.6H56.2a13.61,13.61,0,0,1-13.6-13.6v-47a13.61,13.61,0,0,1,13.6-13.6h67.92a13.61,13.61,0,0,1,13.59,13.6Z"></path><circle class="cls-6" cx="429.03" cy="70.88" r="4.75"></circle></g></g></svg>
                        </div>
                        <div class="col-12 mt-md40 mt20 table-headline-shape">
                           <div class="w700 f-md-28 f-20 text-center text-uppercase black-clr lh150 ">
                              Bundle Offer
                           </div>
                        </div>
                        <!-- PLAN 1 START -->
                        <div class="row mx-0 odd-shape mt-md40 mt20">
                           <div class="col-md-9 col-12 px-md15 px0">
                              <div class="w600 f-md-22 f-18 text-left text-xs-center text-uppercase white-clr lh150 mt8">
                                 COMMERCIAL
                              </div>
                           </div>
                           <div class="col-md-3 col-12 px-md15 px0">
                              <div class="col-12 price-shape">
                                 <div class="w600 f-md-28 f-20 text-center black-clr lh150">
                                    $37
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- PLAN 1 END -->
                        <!-- PLAN 2 START -->
                        <div class="row mx-0 even-shape">
                           <div class="col-md-9 col-12 px-md15 px0">
                              <div class="w600 f-md-22 f-18 text-left text-xs-center text-uppercase white-clr lh150 mt8">
                                 ELITE  
                              </div>
                           </div>
                           <div class="col-md-3 col-12 px-md15 px0">
                              <div class="col-12 price-shape">
                                 <div class="w600 f-md-28 f-20 text-center black-clr lh150">
                                    $77
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- PLAN 2 END -->
                        <!-- PLAN 3 START -->
                        <div class="row mx-0 odd-shape">
                           <div class="col-md-9 col-12 px-md15 px0">
                              <div class="w600 f-md-22 f-18 text-left text-xs-center text-uppercase white-clr lh150 mt8">
                                 ENTERPRISE  
                              </div>
                           </div>
                           <div class="col-md-3 col-12 px-md15 px0">
                              <div class="col-12 price-shape">
                                 <div class="w600 f-md-28 f-20 text-center black-clr lh150">
                                    $97
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- PLAN 3 END -->
                        <!-- PLAN 4 START -->
                        <div class="row mx-0 even-shape">
                           <div class="col-md-9 col-12 px-md15 px0">
                              <div class="w600 f-md-22 f-18 text-left text-xs-center text-uppercase white-clr lh150 mt8">
                                 AGENCY
                              </div>
                           </div>
                           <div class="col-md-3 col-12 px-md15 px0">
                              <div class="col-12 price-shape">
                                 <div class="w600 f-md-28 f-20 text-center black-clr lh150">
                                    $97
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- PLAN 4 END -->

                         <!-- PLAN 5 START -->
                         <div class="row mx-0 odd-shape">
                           <div class="col-md-9 col-12 px-md15 px0">
                              <div class="w600 f-md-22 f-18 text-left text-xs-center text-uppercase white-clr lh150 mt8">
                              SiteSmart AI
                              </div>
                           </div>
                           <div class="col-md-3 col-12 px-md15 px0">
                              <div class="col-12 price-shape">
                                 <div class="w600 f-md-28 f-20 text-center black-clr lh150">
                                    $297
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- PLAN 5 END -->
                        <!-- PLAN 6 START -->
                        <!-- <div class="row mx-0 even-shape">
                           <div class="col-md-9 col-12 px-md15 px0">
                              <div class="w600 f-md-22 f-18 text-left text-xs-center text-uppercase white-clr lh150 mt8">
                              PREMIUM BONUS 2
                              </div>
                           </div>
                           <div class="col-md-3 col-12 px-md15 px0">
                              <div class="col-12 price-shape">
                                 <div class="w600 f-md-28 f-20 text-center black-clr lh150">
                                    $97
                                 </div>
                              </div>
                           </div>
                        </div> -->
                        <!-- PLAN 6 END -->
                        <!-- PLAN 7 START -->
                        <div class="row mx-0 even-shape">
                           <div class="col-md-9 col-12 px-md15 px0">
                              <div class="w600 f-md-22 f-18 text-left text-xs-center text-uppercase white-clr lh150 mt8">
                                 FAST ACTION BONUSES 
                              </div>
                           </div>
                           <div class="col-md-3 col-12 px-md15 px0">
                              <div class="col-12 price-shape">
                                 <div class="w600 f-md-28 f-20 text-center black-clr lh150">
                                    $497
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- PLAN 5 END -->
                        <div class="col-12 px-md0 px15 mt10 w500 f-md-28 f-18 white-clr text-center">
                           Regular <strike>$1102</strike>
                        </div>
                        <div class="row justify-content-center mx-0 ">
                           <div class="col-md-8 col-12 now-price mt10 w700 f-md-45 f-28 white-clr text-center">
                              Now $297
                           </div>
                        </div>
                        <div class="col-12 px-md0 px15 mt10 mt-md10 w500 f-md-25 f-20 white-clr text-center">
                           One Time Fee
                        </div>
                        <div class="mt10 red-clr w600 f-22 d-md-27 lh140">
                           <img src="assets/images/red-close.webp" alt="Hurry" class="mr10 close-img">
                              No Recurring | No Monthly | No Yearly
                           </div>
                        <div class="row justify-content-center mx-0">
                           <div class="myfeatures f-md-25 f-16 w400 text-center lh160">
                              <div class="hideme-button">
                                 <!-- <a href="https://www.jvzoo.com/b/109507/398672/2"><img src="https://i.jvzoo.com/109507/398672/2" alt="IntelliMate AI Bundle" border="0" class="img-fluid mx-auto d-block"/></a>  -->
                                 <a href="https://www.jvzoo.com/b/110349/402227/2"><img src="https://i.jvzoo.com/110349/402227/2" alt="IntelliMate AI Bundle" border="0" class="img-fluid d-block mx-auto"/></a>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt-md50 mt20">
                  <div class="f-md-36 f-24 black-clr  text-center w700 lh140">
                     We are always here by your side <br class="d-none d-md-block">
                     <span class="red-clr">Get in touch with us …</span>
                  </div>
               </div>
            </div>

            <div class="container-box mt20 mt-md80">
               <div class="row">
                  <div class="col-12 col-md-12 text-center">
                     <div class="f-28 f-md-45 w600 white-clr lh140 play">
                        Thanks for checking out IntelliMate AI!
                     </div>
                  </div>
               </div>
               <div class="row mt20 mt-md80 justify-content-between">
                  <div class="col-12 col-md-8 mx-auto">
                     <div class="row">
                        <div class="col-12 col-md-6">
                           <div class="contact-shape">
                              <img src="assets/images/amit-pareek-sir.webp" class="img-fluid d-block mx-auto minus " alt="Amit Pareek">
                              <div class="f-24 f-md-30 w600  lh140 text-center white-clr mt20 play">
                                 Dr. Amit Pareek
                              </div>
                              <!-- <div class="col-12 mt30 d-flex justify-content-center">
                                 <a href="http://facebook.com/Dr.AmitPareek" class="link-text mr20">
                                 <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/am-fb.webp" class="center-block " alt="Facebook">
                                 </a>
                                 <a href="skype:amit.pareek77" class="link-text">
                                    <div class="col-12 ">
                                       <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/am-skype.webp" class="center-block " alt="Skype">
                                    </div>
                                 </a>
                              </div> -->
                           </div>
                        </div>
                        <div class="col-12 col-md-6 mt20 mt-md0">
                           <div class="contact-shape">
                              <img src="assets/images/atul-parrek-sir.webp" class="img-fluid d-block mx-auto minus " alt=" Atul Pareek">
                              <div class="f-24 f-md-30 w600  lh140 text-center white-clr mt20 play">
                                  Atul Pareek
                              </div>
                              <!-- <div class="col-12 mt30 d-flex justify-content-center">
                                 <a href="https://www.facebook.com/profile.php?id=100076904623319" class="link-text mr20">
                                    <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/am-fb.webp" class="center-block " alt="Facebook">
                                 </a>
                                 <a href="skype:live:.cid.c3d2302c4a2815e0" class="link-text">
                                    <div class="col-12 ">
                                       <img src="https://cdn.dotcompaltest.com/uploads/launches/mavas/jv/am-skype.webp" class="center-block " alt="Skype">
                                    </div>
                                 </a>
                              </div> -->
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>

         </div>
      </div>
      <!--3. Third Section End -->
      <!--3. FAQ Section Start -->
      <div class="faq-section sec-pd sec-md-pd">
      <div class="container">
         <div class="row">
            <div class="col-12 f-28 f-md-45 lh140 w600 text-center black-clr">
               Frequently Asked <span class="gradient-clr">Questions</span>
            </div>
            <div class="col-12 mt20 mt-md30 ">
               <div class="row ">
                  <div class="col-md-6 col-12 ">
                     <div class="faq-list ">
                        <div class="f-md-22 f-20 w600 black-clr lh140 ">
                           Do I need to pay anyone any fee for what I charge my clients using IntelliMate?
                        </div>
                        <div class="f-18 w400 lh140 mt10 text-left ">
                           No, IntelliMate doesn't require you to pay additional fees or royalties for the services you provide using the platform. Once you get access to IntelliMate, you can use it to render services to your clients without any extra charges.

                        </div>
                     </div>
                     <div class="faq-list ">
                        <div class="f-md-22 f-20 w600 black-clr lh140 ">                              
                           How can IntelliMate benefit my business?
                        </div>
                        <div class="f-18 w400 lh140 mt10 text-left ">
                           Existing users are successfully using IntelliMate to enhance customer interactions, automate routine tasks, generate leads, drive MORE sales, and offer 24/7 support, all of which contribute to increased efficiency and growth.
                        </div>
                     </div>
                     <div class="faq-list ">
                        <div class="f-md-22 f-20 w600 black-clr lh140">
                           How does IntelliMate ensure privacy and security?
                        </div>
                        <div class="f-18 w400 lh140 mt10 text-left ">
                           IntelliMate takes your data seriously and keeps it safe. It uses strong security methods like encryption to protect your information. Plus, it follows privacy rules to make sure your data stays private and secure.
                        </div>
                     </div>
                     
                  </div>
                  <div class="col-md-6 col-12 ">
                     <div class="faq-list ">
                        <div class="f-md-22 f-20 w600 black-clr lh140 ">
                           Can I customize IntelliMate to suit my specific business needs?
                        </div>
                        <div class="f-18 w400 lh140 mt10 text-left ">
                           Sure, IntelliMate offers lots of ways to change how it looks and works. You can make it match your business needs by adjusting how it looks, what it does, and how it talks to customers. This makes sure IntelliMate fits perfectly into your way of doing things.
                        </div>
                     </div>
                     <div class="faq-list ">
                        <div class="f-md-22 f-20 w600 black-clr lh140 ">
                           What kind of tasks can IntelliMate handle?
                        </div>
                        <div class="f-18 w400 lh140 mt10 text-left ">
                           IntelliMate can do a lot for your business! It can handle customer questions, sort out leads, personalize conversations, give automated responses, help with online sales, suggest content, and more, all based on what your business needs.

                        </div>
                     </div>
                     <div class="faq-list ">
                        <div class="f-md-22 f-20 w600 black-clr lh140 ">
                           How can I be sure IntelliMate is the right fit for my business?
                        </div>
                        <div class="f-18 w400 lh140 mt10 text-left ">
                           We get it, picking the right solution matters. That's why we've got a 30-day money-back guarantee. You've got a whole month to see how awesome IntelliMate is. If it doesn't fit your business, we'll give your money back, no hassle.
                        </div>
                     </div>
                  
                  </div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md50 ">
               <div class="f-22 f-md-28 w600 black-clr px0 text-xs-center lh140 ">If you have any query, simply visit our  <a href="mailto:support@oppyo.com" class="blue-clr support-link">support@oppyo.com</a></div>
            </div>
         </div>
      </div>
   </div>
      <!--3. FAQ Section End -->
      
      <!-- Footer Section Start -->
      <footer>
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                 <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 742.13 152.79" style="max-height: 100px;"><defs><style>.cls-1{fill:#fff;}.cls-2{fill:url(#linear-gradient);}.cls-3{fill:#21a1ff;}.cls-4{fill:#2ddfff;}.cls-5{fill:url(#linear-gradient-2);}.cls-6{fill:#f1f1f2;}</style><linearGradient id="linear-gradient" x1="667.64" y1="93.3" x2="726.15" y2="93.3" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#2ee2ff"></stop><stop offset="1" stop-color="#219fff"></stop></linearGradient><linearGradient id="linear-gradient-2" x1="23.17" y1="76.39" x2="157.15" y2="76.39" xlink:href="#linear-gradient"></linearGradient></defs><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1" d="M220,127.18H210V59.43h10Z"></path><path class="cls-1" d="M264.32,103.36a14.48,14.48,0,1,0-28.95,0v23.82H226V103.36a23.83,23.83,0,1,1,47.65,0v23.82h-9.35Z"></path><path class="cls-1" d="M289.05,89v19.2a9.7,9.7,0,0,0,9.75,9.75h4.53v9.35H298.8a19.1,19.1,0,0,1-19.1-19.1V67.37h9.35V79.63h14.28V89Z"></path><path class="cls-1" d="M319.61,108.48a15.43,15.43,0,0,0,27,4h10.56a24.77,24.77,0,1,1,1.81-9.14,21.23,21.23,0,0,1-.61,5.12h-38.8Zm29.45-9.35a15.48,15.48,0,0,0-29.85,0Z"></path><path class="cls-1" d="M374.4,108a9.78,9.78,0,0,0,9.75,9.85h4.52v9.35h-4.52a19.18,19.18,0,0,1-19.1-19.2V67.17h9.35Z"></path><path class="cls-1" d="M404.05,108a9.78,9.78,0,0,0,9.75,9.85h4.52v9.35H413.8A19.18,19.18,0,0,1,394.7,108V67.17h9.35Z"></path><path class="cls-1" d="M433.71,127.18h-9.35V79.63h9.35Z"></path><path class="cls-1" d="M496.23,77.42l-23.32,24.83L449.69,77.42v49.76h-9.95V59.43h6.63l26.54,28.24,26.64-28.24h6.63v67.75h-9.95Z"></path><path class="cls-1" d="M559.86,127.18h-9.35V103.36A14.48,14.48,0,1,0,536,117.83v9.35a23.88,23.88,0,1,1,23.82-23.82Z"></path><path class="cls-1" d="M575.24,89v19.2a9.7,9.7,0,0,0,9.75,9.75h4.53v9.35H585a19.1,19.1,0,0,1-19.1-19.1V67.37h9.35V79.63h14.28V89Z"></path><path class="cls-1" d="M605.8,108.48a15.43,15.43,0,0,0,27,4h10.55a24.77,24.77,0,1,1,1.81-9.14,21.74,21.74,0,0,1-.6,5.12H605.8Zm29.45-9.35a15.48,15.48,0,0,0-29.85,0Z"></path><path class="cls-2" d="M693.48,59.43h6.83l25.84,67.75H715.49l-18.6-48.85L678.3,127.18H667.64Z"></path><path class="cls-3" d="M742.13,127.18h-10V59.43h10Z"></path><path class="cls-3" d="M161,69.48c0,11.86,0,22.69,0,34,3.94,1,10.59-1.4,14.61-6.34A17.06,17.06,0,0,0,177.32,78,16.86,16.86,0,0,0,161,69.48Z"></path><path class="cls-4" d="M18.5,69.48c0,11.86,0,22.69,0,34-3.94,1-10.6-1.4-14.61-6.34A17,17,0,0,1,2.17,78,16.84,16.84,0,0,1,18.5,69.48Z"></path><path class="cls-1" d="M90.13,118.12a23.33,23.33,0,0,1-16.67-6.7,2.78,2.78,0,0,1-.12-4.15,2.85,2.85,0,0,1,4.09.09,18.1,18.1,0,0,0,25.42,0,2.83,2.83,0,0,1,4.16-.13,2.78,2.78,0,0,1-.22,4.15,23,23,0,0,1-13.27,6.52c-.58.07-1.16.14-1.74.18S90.68,118.12,90.13,118.12Z"></path><path class="cls-1" d="M66.14,76.4c-4.22,0-7.84,1.86-10.89,5.46a2.66,2.66,0,0,0-.08,3.38,1.62,1.62,0,0,0,2.67-.08c5.48-4.64,11.8-4.9,16.61,0a1.62,1.62,0,0,0,2.72.1A2.65,2.65,0,0,0,77,81.86a14.1,14.1,0,0,0-8.67-5.31,10.57,10.57,0,0,0-1.13-.14C66.86,76.38,66.5,76.4,66.14,76.4Z"></path><path class="cls-1" d="M114.15,76.4c-4.22,0-7.84,1.86-10.89,5.46a2.64,2.64,0,0,0-.08,3.38,1.62,1.62,0,0,0,2.67-.08c5.47-4.64,11.8-4.9,16.61,0a1.61,1.61,0,0,0,2.71.1,2.65,2.65,0,0,0-.14-3.38,14.13,14.13,0,0,0-8.67-5.31,10.89,10.89,0,0,0-1.13-.14C114.87,76.38,114.51,76.4,114.15,76.4Z"></path><path class="cls-5" d="M124.12,39.68h-1.39l5.74-20.2a9.79,9.79,0,1,0-5.88-1.61L116.4,39.68H63.93l-6.2-21.81a9.75,9.75,0,1,0-5.87,1.61l5.75,20.2H56.2a33.07,33.07,0,0,0-33,33v47a33.08,33.08,0,0,0,33,33h67.92a33.08,33.08,0,0,0,33-33v-47A33.07,33.07,0,0,0,124.12,39.68Zm5.74-33.76a2.76,2.76,0,1,1,1.94,3.4A2.78,2.78,0,0,1,129.86,5.92ZM48.52,9.32a2.76,2.76,0,1,1,1.94-3.38A2.75,2.75,0,0,1,48.52,9.32Zm89.19,110.43a13.61,13.61,0,0,1-13.59,13.6H56.2a13.61,13.61,0,0,1-13.6-13.6v-47a13.61,13.61,0,0,1,13.6-13.6h67.92a13.61,13.61,0,0,1,13.59,13.6Z"></path><circle class="cls-6" cx="429.03" cy="70.88" r="4.75"></circle></g></g></svg>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-md-20 f-16 w400 lh140 white-clr text-xs-center">Copyright © IntelliMateAI 2023</div>
                  <ul class="footer-ul w400 f-md-20 f-16 white-clr text-center text-md-right">
                     <li><a href="https://support.oppyo.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://intellimateai.co/legal/privacy-policy.html " class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://intellimateai.co/legal/terms-of-service.html " class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://intellimateai.co/legal/disclaimer.html " class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://intellimateai.co/legal/gdpr.html " class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://intellimateai.co/legal/dmca.html " class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://intellimateai.co/legal/anti-spam.html " class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </footer>
      <!-- Footer Section End -->
   
<!-- timer --->
<script type="text/javascript">
   var noob = $('.countdown').length;
   var stimeInSecs;
   var tickers;

   function timerBegin(seconds) {   
      if(seconds>=0)  {
         stimeInSecs = parseInt(seconds);           
         showRemaining();
      }
      //tickers = setInterval("showRemaining()", 1000);
   }

   function showRemaining() {

      var seconds = stimeInSecs;
         
      if (seconds > 0) {         
         stimeInSecs--;       
      } else {        
         clearInterval(tickers);       
         //timerBegin(59 * 60); 
      }

      var days = Math.floor(seconds / 86400);

      seconds %= 86400;
      
      var hours = Math.floor(seconds / 3600);
      
      seconds %= 3600;
      
      var minutes = Math.floor(seconds / 60);
      
      seconds %= 60;


      if (days < 10) {
         days = "0" + days;
      }
      if (hours < 10) {
         hours = "0" + hours;
      }
      if (minutes < 10) {
         minutes = "0" + minutes;
      }
      if (seconds < 10) {
         seconds = "0" + seconds;
      }
      var i;
      var countdown = document.getElementsByClassName('countdown');

      for (i = 0; i < noob; i++) {
         countdown[i].innerHTML = '';
      
         if (days) {
            countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-24 f-md-42 timerbg oswald">' + days + '</span><br> &nbsp;&nbsp;&nbsp;&nbsp;<span class="f-14 f-md-14 w400 smmltd">Days</span> </div>';
         }
      
         countdown[i].innerHTML += '<div class="timer-label text-center">&nbsp;&nbsp;&nbsp;&nbsp;<span class="f-24 f-md-42 timerbg oswald">' + hours + '</span><br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="f-14 f-md-14 w400 smmltd">Hours</span> </div>';
      
         countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"> &nbsp;&nbsp;&nbsp;&nbsp;<span class="f-24 f-md-42 timerbg oswald">' + minutes + '</span><br> &nbsp;&nbsp;&nbsp;&nbsp;<span class="f-14 f-md-14 w400 smmltd">Mins</span> </div>';
      
         countdown[i].innerHTML += '<div class="timer-label text-center "> &nbsp;&nbsp;&nbsp;&nbsp; <span class="f-24 f-md-42 timerbg oswald">' + seconds + '</span><br><span class="f-14 f-md-14 w400 smmltd">Sec</span> </div>';
      }
   
   }

const givenTimestamp = 'Fri Dec 15 2023 20:30:00 GMT+0530'
const durationTime = 360;

const date = new Date(givenTimestamp);
const givenTime = date.getTime() / 1000;
const currentTime = new Date().getTime()



let cnt;
const matchInterval = setInterval(() => {
   if(Math.round(givenTime) === Math.round(new Date().getTime() /1000)){
   cnt = durationTime * 60;
   counter();
   clearInterval(matchInterval);
   }else if(Math.round(givenTime) < Math.round(new Date().getTime() /1000)){
      const timeGap = Math.round(new Date().getTime() /1000) - Math.round(givenTime)
      let difference = (durationTime * 60) - timeGap;
      if(difference >= 0){
         cnt = difference
         localStorage.mavasTimer = cnt;

      }else{
         
         difference = difference % (durationTime * 60);
         cnt = (durationTime * 60) + difference
         localStorage.mavasTimer = cnt;

      }

      counter();
      clearInterval(matchInterval);
   }
},1000)





function counter(){
   if(parseInt(localStorage.mavasTimer) > 0){
      cnt = parseInt(localStorage.mavasTimer);
   }
   cnt -= 1;
   localStorage.mavasTimer = cnt;
   if(localStorage.mavasTimer !== NaN){
      timerBegin(parseInt(localStorage.mavasTimer));
   }
   if(cnt>0){
      setTimeout(counter,1000);
  }else if(cnt === 0){
   cnt = durationTime * 60 
   counter()
  }
}
  </script> 
   </body>
</html>
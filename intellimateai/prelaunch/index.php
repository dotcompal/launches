<!Doctype html>
<html>

<head>
   <title>IntelliMate AI Prelaunch</title>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=9">
   <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
   <meta name="title" content="IntelliMate AI | Prelaunch">
   <meta name="description" content="Self-Learning App Turns Any URL Website or Doc File into an AI-Powered Chatbot… Working 24/7 Generate Leads, Drive Sales, & Ensure Customer Happiness! Non-Stop">
   <meta name="keywords" content="IntelliMate AI">
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   <meta property="og:image" content="https://www.intellimateai.co/prelaunch/thumbnail.png">
   <meta name="language" content="English">
   <meta name="revisit-after" content="1 days">
   <meta name="author" content="Dr. Amit Pareek">
   <!-- Open Graph / Facebook -->
   <meta property="og:type" content="website">
   <meta property="og:title" content="IntelliMate AI | Prelaunch">
   <meta property="og:description" content="Self-Learning App Turns Any URL Website or Doc File into an AI-Powered Chatbot… Working 24/7 Generate Leads, Drive Sales, & Ensure Customer Happiness! Non-Stop">
   <meta property="og:image" content="https://www.intellimateai.co/prelaunch/thumbnail.png">
   <!-- Twitter -->
   <meta property="twitter:card" content="summary_large_image">
   <meta property="twitter:title" content="IntelliMate AI | Prelaunch">
   <meta property="twitter:description" content="Self-Learning App Turns Any URL Website or Doc File into an AI-Powered Chatbot… Working 24/7 Generate Leads, Drive Sales, & Ensure Customer Happiness! Non-Stop">
   <meta property="twitter:image" content="https://www.intellimateai.co/prelaunch/thumbnail.png">
   <link rel="icon" href="https://cdn.dotcompaltest.com/uploads/launches/intellimateai/common_assets/images/favicon.png" type="image/png">
   <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&family=Play:wght@400;700&family=Plus+Jakarta+Sans:ital,wght@0,200;0,300;0,400;0,500;0,600;0,700;0,800;1,200;1,300;1,400;1,500;1,600;1,700;1,800&display=swap" rel="stylesheet">
   <!-- Start Editor required -->
      <link rel="stylesheet" href="../common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" href="../common_assets/css/general.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style.css" type="text/css">
      <link rel="stylesheet" href="assets/css/aweberform.css" type="text/css">
   
</head>

<body> 
   <!-- Header Section Start -->
   <div class="header-section">
      <div class="container">
         <div class="row">
            <div class="col-12 col-12 d-flex align-items-center justify-content-center justify-content-md-between flex-wrap flex-md-nowrap">           
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 742.13 152.79" style="max-height: 55px;"><defs><style>.cls-1{fill:#fff;}.cls-2{fill:url(#linear-gradient);}.cls-3{fill:#21a1ff;}.cls-4{fill:#2ddfff;}.cls-5{fill:url(#linear-gradient-2);}.cls-6{fill:#f1f1f2;}</style><linearGradient id="linear-gradient" x1="667.64" y1="93.3" x2="726.15" y2="93.3" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#2ee2ff"></stop><stop offset="1" stop-color="#219fff"></stop></linearGradient><linearGradient id="linear-gradient-2" x1="23.17" y1="76.39" x2="157.15" y2="76.39" xlink:href="#linear-gradient"></linearGradient></defs><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1" d="M220,127.18H210V59.43h10Z"></path><path class="cls-1" d="M264.32,103.36a14.48,14.48,0,1,0-28.95,0v23.82H226V103.36a23.83,23.83,0,1,1,47.65,0v23.82h-9.35Z"></path><path class="cls-1" d="M289.05,89v19.2a9.7,9.7,0,0,0,9.75,9.75h4.53v9.35H298.8a19.1,19.1,0,0,1-19.1-19.1V67.37h9.35V79.63h14.28V89Z"></path><path class="cls-1" d="M319.61,108.48a15.43,15.43,0,0,0,27,4h10.56a24.77,24.77,0,1,1,1.81-9.14,21.23,21.23,0,0,1-.61,5.12h-38.8Zm29.45-9.35a15.48,15.48,0,0,0-29.85,0Z"></path><path class="cls-1" d="M374.4,108a9.78,9.78,0,0,0,9.75,9.85h4.52v9.35h-4.52a19.18,19.18,0,0,1-19.1-19.2V67.17h9.35Z"></path><path class="cls-1" d="M404.05,108a9.78,9.78,0,0,0,9.75,9.85h4.52v9.35H413.8A19.18,19.18,0,0,1,394.7,108V67.17h9.35Z"></path><path class="cls-1" d="M433.71,127.18h-9.35V79.63h9.35Z"></path><path class="cls-1" d="M496.23,77.42l-23.32,24.83L449.69,77.42v49.76h-9.95V59.43h6.63l26.54,28.24,26.64-28.24h6.63v67.75h-9.95Z"></path><path class="cls-1" d="M559.86,127.18h-9.35V103.36A14.48,14.48,0,1,0,536,117.83v9.35a23.88,23.88,0,1,1,23.82-23.82Z"></path><path class="cls-1" d="M575.24,89v19.2a9.7,9.7,0,0,0,9.75,9.75h4.53v9.35H585a19.1,19.1,0,0,1-19.1-19.1V67.37h9.35V79.63h14.28V89Z"></path><path class="cls-1" d="M605.8,108.48a15.43,15.43,0,0,0,27,4h10.55a24.77,24.77,0,1,1,1.81-9.14,21.74,21.74,0,0,1-.6,5.12H605.8Zm29.45-9.35a15.48,15.48,0,0,0-29.85,0Z"></path><path class="cls-2" d="M693.48,59.43h6.83l25.84,67.75H715.49l-18.6-48.85L678.3,127.18H667.64Z"></path><path class="cls-3" d="M742.13,127.18h-10V59.43h10Z"></path><path class="cls-3" d="M161,69.48c0,11.86,0,22.69,0,34,3.94,1,10.59-1.4,14.61-6.34A17.06,17.06,0,0,0,177.32,78,16.86,16.86,0,0,0,161,69.48Z"></path><path class="cls-4" d="M18.5,69.48c0,11.86,0,22.69,0,34-3.94,1-10.6-1.4-14.61-6.34A17,17,0,0,1,2.17,78,16.84,16.84,0,0,1,18.5,69.48Z"></path><path class="cls-1" d="M90.13,118.12a23.33,23.33,0,0,1-16.67-6.7,2.78,2.78,0,0,1-.12-4.15,2.85,2.85,0,0,1,4.09.09,18.1,18.1,0,0,0,25.42,0,2.83,2.83,0,0,1,4.16-.13,2.78,2.78,0,0,1-.22,4.15,23,23,0,0,1-13.27,6.52c-.58.07-1.16.14-1.74.18S90.68,118.12,90.13,118.12Z"></path><path class="cls-1" d="M66.14,76.4c-4.22,0-7.84,1.86-10.89,5.46a2.66,2.66,0,0,0-.08,3.38,1.62,1.62,0,0,0,2.67-.08c5.48-4.64,11.8-4.9,16.61,0a1.62,1.62,0,0,0,2.72.1A2.65,2.65,0,0,0,77,81.86a14.1,14.1,0,0,0-8.67-5.31,10.57,10.57,0,0,0-1.13-.14C66.86,76.38,66.5,76.4,66.14,76.4Z"></path><path class="cls-1" d="M114.15,76.4c-4.22,0-7.84,1.86-10.89,5.46a2.64,2.64,0,0,0-.08,3.38,1.62,1.62,0,0,0,2.67-.08c5.47-4.64,11.8-4.9,16.61,0a1.61,1.61,0,0,0,2.71.1,2.65,2.65,0,0,0-.14-3.38,14.13,14.13,0,0,0-8.67-5.31,10.89,10.89,0,0,0-1.13-.14C114.87,76.38,114.51,76.4,114.15,76.4Z"></path><path class="cls-5" d="M124.12,39.68h-1.39l5.74-20.2a9.79,9.79,0,1,0-5.88-1.61L116.4,39.68H63.93l-6.2-21.81a9.75,9.75,0,1,0-5.87,1.61l5.75,20.2H56.2a33.07,33.07,0,0,0-33,33v47a33.08,33.08,0,0,0,33,33h67.92a33.08,33.08,0,0,0,33-33v-47A33.07,33.07,0,0,0,124.12,39.68Zm5.74-33.76a2.76,2.76,0,1,1,1.94,3.4A2.78,2.78,0,0,1,129.86,5.92ZM48.52,9.32a2.76,2.76,0,1,1,1.94-3.38A2.75,2.75,0,0,1,48.52,9.32Zm89.19,110.43a13.61,13.61,0,0,1-13.59,13.6H56.2a13.61,13.61,0,0,1-13.6-13.6v-47a13.61,13.61,0,0,1,13.6-13.6h67.92a13.61,13.61,0,0,1,13.59,13.6Z"></path><circle class="cls-6" cx="429.03" cy="70.88" r="4.75"></circle></g></g></svg>
               <div class=" mt20 mt-md5">
                  <ul class="leader-ul f-18 f-md-20 white-clr text-md-end text-center">
                     <li class="affiliate-link-btn"><a href="#book-seat" class="t-decoration-none ">Book Your Free Seat</a></li>
                  </ul>
               </div>
            </div>
            <div class="col-12 text-center  mt30 mt-md50">
                    <div class="preheadline f-16 f-md-22 w600 lh140 white-clr">
                    Register For This Value Packed Training; <span class="neon-clr"> All Attendees Receive FREE Gifts</span>
                    </div>
                </div>

            <!-- <div class="col-12 mt30 mt-md50 text-center">
               <div class="preheadline f-18 f-md-22 w600 white-clr lh140">
                  <div class="preheadline-content white-clr ">
                  Register For This Value Packed Training; <span class="neon-clr"> All Attendees Receive FREE Gifts</span>
                  </div>
               </div>
            </div> -->
            <!-- <div class="col-12 mt20 mt-md30">
               <div class="f-md-22 f-18 w700 text-center lh140 white-clr">
               [🎯Unlock Your TRUE Income Potential] Learn How to Make $2K Weekly Using Amazon S3's Rental Strategy!"
               </div>
            </div> -->

            <div class="col-12 f-md-50 f-28 w700 lh140 mt70 mt-md70 relative play">
                    <div class="gametext white-clr">
                        Revolutionary
                    </div>
                    <div class="mainheadline text-center">
                        <div class="d-md-flex align-items-center justify-content-center">
                            <div class="">
                                Self-Learning App Turns Any
                            </div>
                            <div style="100%">
                                <div class="ved_search_box">
                                    <div class="ved_search_placeholders blue-gradient">
                                        <h1 class=" f-md-50 f-28 w700">URL</h1>
                                        <h1 class=" f-md-50 f-28 w700">Website</h1>
                                        <h1 class=" f-md-50 f-28 w700">or Doc File</h1>  
                                    </div>
                                </div>
                            </div>
                        </div>
                        into an AI-Powered Chatbot… Working 24/7 <span class="caveat f-md-43">Generate Leads, Drive Sales, &amp; Ensure Customer Happiness! Non-Stop</span>
                    </div>
                </div>
            <!-- <div class="col-12 mt20 mt-md50 relative">
                  <div class="mainheadline f-md-40 f-28 w700 text-center white-clr lh140">
                  <div class="headline-text">
                     First-To-Market <span class="w700 sky-blue neon-clr">#1 Super VA That Finish Your 100s Type of Marketing Tasks <span class="caveat">(Better &amp; Faster Than Any CMO Out There...)</span> </span> AND Trim Your Workload, Hiring/Firing Headaches, &amp; Monthly Bills </div>
                  </div>
               </div>
            <div class="col-12 mt-md50 mt20 f-18 f-md-22 w400 text-center lh170 white-clr text-capitalize">
               <div class="postheadline">
               In This Free Training, we will also reveal, how you can Dominate on Social-Media, Drive Website Traffic, Create Ads, Optimize SEO, Generate Quality Leads, Create Compelling Content, and <span class="w700">Much More with IntelliMate AI</span>
               </div>
            </div> -->


            </div>
         
         <div class="row d-flex align-items-center flex-wrap mt20 mt-md80">
            <div class="col-md-7 col-12 min-md-video-width-left">
               <img src="assets/images/webinar-image.webp" class="img-fluid d-block mx-auto" alt="Prelaunch">
            </div>
            <div class="col-md-5 col-12 mt20 mt-md0 min-md-video-width-right" id="form">
               <div class="auto_responder_form inp-phold calendar-wrap" id="book-seat">
                  <div class="f">
                     <div class="f-20 f-md-24 w800 text-center lh180 orange-clr" editabletype="text" style="z-index:10;">
                        Register for Free Training <br>
                        <span class="f-20 f-md-24 w700 text-center black-clr" contenteditable="false">15<sup contenteditable="false">th</sup> Dec, 10 AM EST</span>
                     </div>
                     <div class="col-12 f-18 f-md-20 w500 lh140 text-center">
                        Book Your Seat now<br> (Limited to 100 Users)
                     </div>
                     <!-- Aweber Form Code -->
                     <div class="col-12 p0 mt15 mt-md20">
                        <form method="post" class="af-form-wrapper register-form-style1 " accept-charset="UTF-8" action="https://www.aweber.com/scripts/addlead.pl" style="z-index: 10;">
                           <div style="display: none;">
                              <input type="hidden" name="meta_web_form_id" value="857182195" />
                              <input type="hidden" name="meta_split_id" value="" />
                              <input type="hidden" name="listname" value="awlist6671958" />
                              <input type="hidden" name="redirect" value="https://www.intellimateai.co/prelaunch-thankyou" id="redirect_e2827ebc4298c416c54f740c0edcf22e" />
                              <input type="hidden" name="meta_adtracking" value="My_Web_Form" />
                              <input type="hidden" name="meta_message" value="1" />
                              <input type="hidden" name="meta_required" value="name,email" />
                              <input type="hidden" name="meta_tooltip" value="" />
                           </div>
                           <div id="af-form-857182195" class="af-form">
                              <div id="af-body-857182195" class="af-body af-standards">
                                 <div class="af-element width-form1">
                                    <label class="previewLabel" for="awf_field-116563941"></label>
                                    <div class="af-textWrap">
                                       <div class="input-type" style="z-index: 11;">
                                          <!--<span class="input-group-addon border1px"><i class="fa fa-user" aria-hidden="true"></i></span>-->
                                          <input id="awf_field-116563941" type="text" name="name" class="text form-control custom-input input-field" value="" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="500" placeholder="Your Name" />
                                       </div>
                                    </div>
                                    <div class="af-clear bottom-margin"></div>
                                 </div>

                                 
                                 <div class="af-element width-form1">
                                    <label class="previewLabel" for="awf_field-116563942"> </label>
                                    <div class="af-textWrap">
                                       <div class="input-type" style="z-index: 11;">
                                          <!--<span class="input-group-addon border1px"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>-->
                                          <input class="text form-control custom-input input-field" id="awf_field-116563942" type="text" name="email" value="" tabindex="501" onFocus=" if (this.value == '') { this.value = ''; }" onBlur="if (this.value == '') { this.value='';} " placeholder="Your Email" />
                                       </div>
                                    </div>
                                    <div class="af-clear bottom-margin"></div>
                                 </div>
                                 <input type="hidden" id="awf_field_aid" class="text" name="custom aid" value="" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="502" />
                                 <div class="af-element buttonContainer button-type register-btn1 f-22 f-md-21 mt-md0">
                                    <input name="submit" id="af-submit-image-348552691" type="submit" class="image" style="max-width: 100%;" alt="Submit Form" tabindex="502" value="Book Your Free Seat" />
                                    <div class="af-clear bottom-margin"></div>
                                 </div>
                              </div>
                           </div>
                           <div style="display: none;"><img src="https://forms.aweber.com/form/displays.htm?id=HKzsjBxMjJys" alt="Form" /></div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Header Section End -->
   <!-- Header Section Start -->
   <div class="second-section py-5">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="col-12">
                  <div class="row">
                     <div class="col-12">
                        <div class="f-md-36 f-24 w800 lh140 text-capitalize text-center gradient-clr">
                        Why Attend This Free Training Webinar?
                        </div>
                        <div class="f-20 f-md-22 w600 lh150 text-capitalize text-center black-clr mt20">
                        Our 19+ Years Of Experience Is Packaged In This State-Of-Art 1-Hour Webinar Where You’ll Learn:
                        </div>
                     </div>
                     <div class="col-12 mt20 mt-md50">
                        <ul class="features-list pl0 f-18 f-md-20 lh150 w400 black-clr text-capitalize">
                           <li>How We Have Made Over $9 Million by Selling & Securely Using the Power of IntelliMate AI. and  <span class="w700">You Can Follow The Same To Build A Profitable Business Online. </span></li>
                           <li>How you can Run Your Business WITHOUT Any Expertise/Hassles/Freelancer/Agency Fee</li>
                           <li>How We Are Earning $3000 Monthly Recurring Just by Renting IntelliMate AI Bots & You Can Follow The Same For Your Clients & Charge Them PREMIUM Fee</li>
                           <li>Earn Thousands of Dollars Extra Every Month Using IntelliMate AI - Smart Chat Bots</li>
                           <li>Get Paid a PREMIUM Fee for services rendered by IntelliMate AI without incurring any expenses in Freelancer/Agency. Keep 100% Profits with you.</li>
                           <!-- <li>A Sneak-Peak Of "IntelliMate AI", First-To-Market #1 Super VA That Finish Your 100s Type of Marketing Tasks (Better & Faster Than Any CMO Out There...) </li>
                           <li>During This Launch Special Deal, Get All Benefits at A Limited Low One-Time-Fee. </li>
                           <li>Lastly, Everyone Who Attends This Session Will Get an Assured Gift from Us. </li> -->
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>

   <!-- boost business section start -->

   <section class="boost-business">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <div class="boost-head">
                        <div class="f-28 f-md-50 w700 lh140 play text-center">
                            Boost your business's bottom line <br class="d-none d-md-block">
                            with <span class="blue-gradient">IntelliMate AI</span> 
                        </div>
                    </div>
                </div>
            </div>
            <img src="assets/images/blue-arrow.webp" alt="Blue Arrow" class="mx-auto d-block img-fluid">
            <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 justify-content-center mt20 mt-md0">
                <div class="col">
                    <div class="feature-list">
                        <img src="assets/images/lead-generation.webp" class="img-fluid mx-auto d-block" alt="Leads Without Optin Forms">
                        <div class="f-20 f-md-24 w700 lh140 mt15 mt-md30 play">
                            Leads Without Optin Forms
                        </div>
                        <div class="f-16 f-md-18 w400 lh140 mt10">
                            IntelliMate AI effortlessly generates leads without the need for cumber-some opt-in forms, streamlining the customer acquisition process.
                        </div>
                    </div>
                </div>
                <div class="col mt20 mt-sm0">
                    <div class="feature-list">
                        <img src="assets/images/boost-money.webp" class="img-fluid mx-auto d-block" alt="Boost Your Sales">
                        <div class="f-20 f-md-24 w700 lh140 mt15 mt-md30 play">
                            Boost Your Sales
                        </div>
                        <div class="f-16 f-md-18 w400 lh140 mt10">
                            Elevate your sales performance exponentially with IntelliMate AI, leveraging its smart features to enhance conversion rates and drive revenue growth.
                        </div>
                    </div>
                </div>
                <div class="col mt20 mt-md0">
                    <div class="feature-list">
                        <img src="assets/images/build-brand-value.webp" class="img-fluid mx-auto d-block" alt="Build Brand Value">
                        <div class="f-20 f-md-24 w700 lh140 mt15 mt-md30 play">
                            Build Brand Value
                        </div>
                        <div class="f-16 f-md-18 w400 lh140 mt10">
                            Establish and strengthen your brand value by providing a seamless and interactive user experience through the intelligent capabilities of IntelliMate AI.
                        </div>
                    </div>
                </div>  
                <div class="col mt20 mt-md50">
                    <div class="feature-list">
                        <img src="assets/images/increase-funnel.webp" class="img-fluid mx-auto d-block" alt="Increase Funnel Value">
                        <div class="f-20 f-md-24 w700 lh140 mt15 mt-md30 play">
                            Increase Funnel Value
                        </div>
                        <div class="f-16 f-md-18 w400 lh140 mt10">
                        IntelliMate AI enhances your sales funnel by optimizing customer interactions, ensuring a more efficient and valuable journey from awareness to conversion.
                        </div>
                    </div>
                </div>   
                <div class="col mt20 mt-md50">
                    <div class="feature-list">
                        <img src="assets/images/cut-cost.webp" class="img-fluid mx-auto d-block" alt="Cut the Cost">
                        <div class="f-20 f-md-24 w700 lh140 mt15 mt-md30 play">
                            Cut the Cost
                        </div>
                        <div class="f-16 f-md-18 w400 lh140 mt10">
                            Reduce operational costs with IntelliMate AI's automation, providing cost-effective solutions for customer engagement and support.
                        </div>
                    </div>
                </div>   
                <div class="col mt20 mt-md50">
                    <div class="feature-list">
                        <img src="assets/images/happy-customers.webp" class="img-fluid mx-auto d-block" alt="Happy Customers 24X7">
                        <div class="f-20 f-md-24 w700 lh140 mt15 mt-md30 play">
                            Happy Customers 24X7
                        </div>
                        <div class="f-16 f-md-18 w400 lh140 mt10">
                            Keep your customers satisfied around the clock with IntelliMate AI, delivering instant responses and support for enhanced customer satisfaction.
                        </div>
                    </div>
                </div>
                <div class="col mt20 mt-md50">
                    <div class="feature-list">
                        <img src="assets/images/rent-bots.webp" class="img-fluid mx-auto d-block" alt="Rent Bots for Profit">
                        <div class="f-20 f-md-24 w700 lh140 mt15 mt-md30 play">
                            Rent Bots for Profit
                        </div>
                        <div class="f-16 f-md-18 w400 lh140 mt10">
                            Explore additional revenue streams by renting out IntelliMate AI bots, extending the benefits of smart automation to other businesses for mutual profit.&nbsp;
                        </div>
                    </div>
                </div>
                <div class="col mt20 mt-md50">
                    <div class="feature-list">
                        <img src="assets/images/increase-profit.webp" class="img-fluid mx-auto d-block" alt="Maximize Buyer Conversion">
                        <div class="f-20 f-md-24 w700 lh140 mt15 mt-md30 play">
                            Maximize Buyer Conversion
                        </div>
                        <div class="f-16 f-md-18 w400 lh140 mt10">
                            IntelliMate AI maximizes buyer conversion by offering personalized recommendations and assistance, increasing the likelihood of successful transactions.&nbsp;
                        </div>
                    </div>
                </div>
                <div class="col mt20 mt-md50">
                    <div class="feature-list">
                        <img src="assets/images/reduce-bounce-rates.webp" class="img-fluid mx-auto d-block" alt="Reduce Bounce Rates">
                        <div class="f-20 f-md-24 w700 lh140 mt15 mt-md30 play">
                            Reduce Bounce Rates
                        </div>
                        <div class="f-16 f-md-18 w400 lh140 mt10">
                            Improve website engagement and user retention as IntelliMate AI minimizes bounce rates through interactive and tailored user experiences.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

   <!--boost business section end  -->
   <div class="second-section">
      <div class="container">
         <div class="row mt20">
            <div class="col-12 col-md-12 mx-auto" id="form">
               <div class="auto_responder_form inp-phold formbg" id="book-seat1">
                  <div class="f-24 f-md-40 w800 text-center lh160 white-clr" editabletype="text" style="z-index:10;">
                     So Make Sure You Do NOT Miss This Webinar
                  </div>
                  <div class="col-12 f-md-22 f-18 w600 lh160 mx-auto my20 text-center white-clr">
                     Register Now! And Join Us Live On <br class="d-none d-md-block"> <span class="orange-clr">December 15th, 10:00 AM EST </span><br>
                     <span class="w400">Book Your Seat now (Limited to 100 Users Only)</span>
                  </div>
                  <!-- Aweber Form Code -->
                  <div class="col-12 p0 mt15 mt-md25" editabletype="form" style="z-index:10">
                     <!-- Aweber Form Code -->
                     <form method="post" class="af-form-wrapper register-form-style1 " accept-charset="UTF-8" action="https://www.aweber.com/scripts/addlead.pl" style="z-index: 10;">
                        <div style="display: none;">
                           <input type="hidden" name="meta_web_form_id" value="857182195" />
                           <input type="hidden" name="meta_split_id" value="" />
                           <input type="hidden" name="listname" value="awlist6641005" />
                           <input type="hidden" name="redirect" value="https://www.intellimateai.co/prelaunch-thankyou" id="redirect_e2827ebc4298c416c54f740c0edcf22e" />
                           <input type="hidden" name="meta_adtracking" value="My_Web_Form" />
                           <input type="hidden" name="meta_message" value="1" />
                           <input type="hidden" name="meta_required" value="name,email" />
                           <input type="hidden" name="meta_tooltip" value="" />
                        </div>
                        <div id="af-form-857182195" class="af-form">
                           <div id="af-body-857182195" class="af-body af-standards">
                              <div class="af-element width-form">
                                 <label class="previewLabel" for="awf_field-116563941"></label>
                                 <div class="af-textWrap">
                                    <div class="input-type" style="z-index: 11;">
                                       <!--<span class="input-group-addon border1px"><i class="fa fa-user" aria-hidden="true"></i></span>-->
                                       <input id="awf_field-116563941" type="text" name="name" class="text form-control custom-input input-field" value="" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="500" placeholder="Your Name">
                                    </div>
                                 </div>
                                 <div class="af-clear bottom-margin"></div>
                              </div>

                             


                              <div class="af-element width-form">
                                 <label class="previewLabel" for="awf_field-116563942"> </label>
                                 <div class="af-textWrap">
                                    <div class="input-type" style="z-index: 11;">
                                       <!--<span class="input-group-addon border1px"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>-->
                                       <input class="text form-control custom-input input-field" id="awf_field-116563942" type="text" name="email" value="" tabindex="501" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " placeholder="Your Email">
                                    </div>
                                 </div>
                                 <div class="af-clear bottom-margin"></div>
                              </div>
                              <input type="hidden" id="awf_field_aid" class="text" name="custom aid" value="undefined" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="502">
                              <div class="af-element buttonContainer button-type register-btn1 f-22 f-md-21 width-form mt-md0">
                                 <input name="submit" id="af-submit-image-348552691" type="submit" class="image" style="max-width: 100%;" alt="Submit Form" tabindex="503" value="Book Your Free Seat">
                                 <div class="af-clear bottom-margin"></div>
                              </div>
                           </div>
                        </div>
                        <div style="display: none;"><img src="https://forms.aweber.com/form/displays.htm?id=jAxMbOwcLOyczA==" alt="Form"></div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Header Section End -->
   <!--Footer Section Start -->
   <div class="footer-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
               <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 742.13 152.79" style="max-height: 100px;"><defs><style>.cls-1{fill:#fff;}.cls-2{fill:url(#linear-gradient);}.cls-3{fill:#21a1ff;}.cls-4{fill:#2ddfff;}.cls-5{fill:url(#linear-gradient-2);}.cls-6{fill:#f1f1f2;}</style><linearGradient id="linear-gradient" x1="667.64" y1="93.3" x2="726.15" y2="93.3" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#2ee2ff"></stop><stop offset="1" stop-color="#219fff"></stop></linearGradient><linearGradient id="linear-gradient-2" x1="23.17" y1="76.39" x2="157.15" y2="76.39" xlink:href="#linear-gradient"></linearGradient></defs><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1" d="M220,127.18H210V59.43h10Z"></path><path class="cls-1" d="M264.32,103.36a14.48,14.48,0,1,0-28.95,0v23.82H226V103.36a23.83,23.83,0,1,1,47.65,0v23.82h-9.35Z"></path><path class="cls-1" d="M289.05,89v19.2a9.7,9.7,0,0,0,9.75,9.75h4.53v9.35H298.8a19.1,19.1,0,0,1-19.1-19.1V67.37h9.35V79.63h14.28V89Z"></path><path class="cls-1" d="M319.61,108.48a15.43,15.43,0,0,0,27,4h10.56a24.77,24.77,0,1,1,1.81-9.14,21.23,21.23,0,0,1-.61,5.12h-38.8Zm29.45-9.35a15.48,15.48,0,0,0-29.85,0Z"></path><path class="cls-1" d="M374.4,108a9.78,9.78,0,0,0,9.75,9.85h4.52v9.35h-4.52a19.18,19.18,0,0,1-19.1-19.2V67.17h9.35Z"></path><path class="cls-1" d="M404.05,108a9.78,9.78,0,0,0,9.75,9.85h4.52v9.35H413.8A19.18,19.18,0,0,1,394.7,108V67.17h9.35Z"></path><path class="cls-1" d="M433.71,127.18h-9.35V79.63h9.35Z"></path><path class="cls-1" d="M496.23,77.42l-23.32,24.83L449.69,77.42v49.76h-9.95V59.43h6.63l26.54,28.24,26.64-28.24h6.63v67.75h-9.95Z"></path><path class="cls-1" d="M559.86,127.18h-9.35V103.36A14.48,14.48,0,1,0,536,117.83v9.35a23.88,23.88,0,1,1,23.82-23.82Z"></path><path class="cls-1" d="M575.24,89v19.2a9.7,9.7,0,0,0,9.75,9.75h4.53v9.35H585a19.1,19.1,0,0,1-19.1-19.1V67.37h9.35V79.63h14.28V89Z"></path><path class="cls-1" d="M605.8,108.48a15.43,15.43,0,0,0,27,4h10.55a24.77,24.77,0,1,1,1.81-9.14,21.74,21.74,0,0,1-.6,5.12H605.8Zm29.45-9.35a15.48,15.48,0,0,0-29.85,0Z"></path><path class="cls-2" d="M693.48,59.43h6.83l25.84,67.75H715.49l-18.6-48.85L678.3,127.18H667.64Z"></path><path class="cls-3" d="M742.13,127.18h-10V59.43h10Z"></path><path class="cls-3" d="M161,69.48c0,11.86,0,22.69,0,34,3.94,1,10.59-1.4,14.61-6.34A17.06,17.06,0,0,0,177.32,78,16.86,16.86,0,0,0,161,69.48Z"></path><path class="cls-4" d="M18.5,69.48c0,11.86,0,22.69,0,34-3.94,1-10.6-1.4-14.61-6.34A17,17,0,0,1,2.17,78,16.84,16.84,0,0,1,18.5,69.48Z"></path><path class="cls-1" d="M90.13,118.12a23.33,23.33,0,0,1-16.67-6.7,2.78,2.78,0,0,1-.12-4.15,2.85,2.85,0,0,1,4.09.09,18.1,18.1,0,0,0,25.42,0,2.83,2.83,0,0,1,4.16-.13,2.78,2.78,0,0,1-.22,4.15,23,23,0,0,1-13.27,6.52c-.58.07-1.16.14-1.74.18S90.68,118.12,90.13,118.12Z"></path><path class="cls-1" d="M66.14,76.4c-4.22,0-7.84,1.86-10.89,5.46a2.66,2.66,0,0,0-.08,3.38,1.62,1.62,0,0,0,2.67-.08c5.48-4.64,11.8-4.9,16.61,0a1.62,1.62,0,0,0,2.72.1A2.65,2.65,0,0,0,77,81.86a14.1,14.1,0,0,0-8.67-5.31,10.57,10.57,0,0,0-1.13-.14C66.86,76.38,66.5,76.4,66.14,76.4Z"></path><path class="cls-1" d="M114.15,76.4c-4.22,0-7.84,1.86-10.89,5.46a2.64,2.64,0,0,0-.08,3.38,1.62,1.62,0,0,0,2.67-.08c5.47-4.64,11.8-4.9,16.61,0a1.61,1.61,0,0,0,2.71.1,2.65,2.65,0,0,0-.14-3.38,14.13,14.13,0,0,0-8.67-5.31,10.89,10.89,0,0,0-1.13-.14C114.87,76.38,114.51,76.4,114.15,76.4Z"></path><path class="cls-5" d="M124.12,39.68h-1.39l5.74-20.2a9.79,9.79,0,1,0-5.88-1.61L116.4,39.68H63.93l-6.2-21.81a9.75,9.75,0,1,0-5.87,1.61l5.75,20.2H56.2a33.07,33.07,0,0,0-33,33v47a33.08,33.08,0,0,0,33,33h67.92a33.08,33.08,0,0,0,33-33v-47A33.07,33.07,0,0,0,124.12,39.68Zm5.74-33.76a2.76,2.76,0,1,1,1.94,3.4A2.78,2.78,0,0,1,129.86,5.92ZM48.52,9.32a2.76,2.76,0,1,1,1.94-3.38A2.75,2.75,0,0,1,48.52,9.32Zm89.19,110.43a13.61,13.61,0,0,1-13.59,13.6H56.2a13.61,13.61,0,0,1-13.6-13.6v-47a13.61,13.61,0,0,1,13.6-13.6h67.92a13.61,13.61,0,0,1,13.59,13.6Z"></path><circle class="cls-6" cx="429.03" cy="70.88" r="4.75"></circle></g></g></svg>
                  
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-16 f-md-18 w400 lh140 white-clr text-xs-center">Copyright © IntelliMate AI 2023</div>
                  <ul class="footer-ul w400 f-16 f-md-18 white-clr text-center text-md-right">
                     <li><a href="https://support.oppyo.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://intellimateai.co/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://intellimateai.co/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://intellimateai.co/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://intellimateai.co/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://intellimateai.co/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://intellimateai.co/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!--Footer Section End -->
<script type="text/javascript">
// Special handling for in-app browsers that don't always support new windows
(function() {
    function browserSupportsNewWindows(userAgent) {
        var rules = [
            'FBIOS',
            'Twitter for iPhone',
            'WebView',
            '(iPhone|iPod|iPad)(?!.*Safari\/)',
            'Android.*(wv|\.0\.0\.0)'
        ];
        var pattern = new RegExp('(' + rules.join('|') + ')', 'ig');
        return !pattern.test(userAgent);
    }

    if (!browserSupportsNewWindows(navigator.userAgent || navigator.vendor || window.opera)) {
        document.getElementById('af-form-857182195').parentElement.removeAttribute('target');
    }
})();
</script><script type="text/javascript">
    <!--
    (function() {
        var IE = /*@cc_on!@*/false;
        if (!IE) { return; }
        if (document.compatMode && document.compatMode == 'BackCompat') {  
            if (document.getElementById("af-form-857182195")) {
                document.getElementById("af-form-857182195").className = 'af-form af-quirksMode';
            }
            if (document.getElementById("af-body-857182195")) {
                document.getElementById("af-body-857182195").className = "af-body inline af-quirksMode";
            }
            if (document.getElementById("af-header-857182195")) {
                document.getElementById("af-header-857182195").className = "af-header af-quirksMode";
            }
            if (document.getElementById("af-footer-857182195")) {
                document.getElementById("af-footer-857182195").className = "af-footer af-quirksMode";
            }
        }
    })();
    -->
</script>
</body>

</html>

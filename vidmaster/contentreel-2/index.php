<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <!-- Tell the browser to be responsive to screen width -->
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
      <meta name="title" content="Vidamaze Bonuses">
      <meta name="description" content="Vidamaze Bonuses">
      <meta name="keywords" content="Have A Sneak Peak of A Breakthrough Technology That Analyzes, Detects & Fix Deadly Legal Flaws on Your Website Just by Copy Pasting a Single Line of Code">
      <meta property="og:image" content="https://www.vidamaze.com/special-bonus/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Dr. Amit Pareek">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="Vidamaze Bonuses">
      <meta property="og:description" content="Have A Sneak Peak of A Breakthrough Technology That Analyzes, Detects & Fix Deadly Legal Flaws on Your Website Just by Copy Pasting a Single Line of Code">
      <meta property="og:image" content="https://www.vidamaze.com/special-bonus/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="Vidamaze Bonuses">
      <meta property="twitter:description" content="Have A Sneak Peak of A Breakthrough Technology That Analyzes, Detects & Fix Deadly Legal Flaws on Your Website Just by Copy Pasting a Single Line of Code">
      <meta property="twitter:image" content="https://www.vidamaze.com/special-bonus/thumbnail.png">
      <title>Vidamaze Bonuses</title>
      <!-- Shortcut Icon  -->
      <link rel="icon" href="assets/images/favicon.png" type="image/png">
      <!-- Css CDN Load Link -->
      <link rel="stylesheet" type="text/css" href="assets/css/bonus-style.css">
      
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&family=Lexend:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
      <!-- required -->
      <!-- Start Editor required -->
      <link rel="stylesheet" href="https://cdn.oppyo.com/launches/yoseller/common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style.css" type="text/css">
      <script src="https://cdn.oppyo.com/launches/writerarc/common_assets/js/jquery.min.js"></script>
      <script type="text/javascript" src="https://www.legelsuites.com/widget/ada/oppyo-Im7S-MNbL-QN75-NLLR" crossorigin="anonymous" embed-id="oppyo-Im7S-MNbL-QN75-NLLR"></script>
   </head>
   <body>
      <!-- New Timer  Start-->
      <?php
         $date = 'Jan 21 2023 11:00 PM EST';
         $exp_date = strtotime($date);
         $now = time();  
         /*
         
         $date = date('F d Y g:i:s A eO');
         $rand_time_add = rand(700, 1200);
         $exp_date = strtotime($date) + $rand_time_add;
         $now = time();*/
         
         if ($now < $exp_date) {
         ?>
      <?php
         } else {
         	echo "Times Up";
         }
         ?>
      <!-- New Timer End -->
      <?php
         if(!isset($_GET['afflink'])){
         $_GET['afflink'] = 'https://jvz8.com/c/10103/391130/';
         $_GET['name'] = 'Dr. Amit Pareek';      
         }
         ?>
         
     <!-- Header Section Start -->   
     <div class="main-header">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <img src="assets/images/vidmaze-logo.png" alt="vidamaze-logo" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-md-12 col-12 text-center mt30 mt-md50">
                  <div class="text-center">
                     <div class="f-md-32 f-20 lh140 w400 white-clr d-block d-md-flex align-items-center justify-content-center">
                        <span class="w600 orange-clr"><?php echo $_GET['name'];?>'s</span> &nbsp;special bonus for Vidamaze 
                     </div>
                  </div>
               </div>
            </div>
         
            <div class="row mt20">
               <div class="col-12  text-center">
                  <div class="f-16 f-md-20 w700 white-clr lh140 text-uppercase preheadline">                       
                     Experts Want This <span class=""><u>BANNED</u> </span> 
                  </div>
               </div>
               <div class="col-12 mt-md30 mt20 f-md-45 f-28 w700 text-center white-clr lh140">
                  Shocking New Video Technology Creates 
                  <br class="d-none d-md-block">
                  “Trigger Reels” That Light Up Part Of The Brain Which
               </div>

               <div class="col-12 mt-md30 mt20 f-md-45 f-28 w700 text-center pink-clr lh140">
                  Makes People Watch And Buy Like
                  <br class="d-none d-md-block">
                  You've Never Seen Before!
               </div>
            
               <div class="col-12 mt20 mt-md30 text-center">
                  <div class="f-18 f-md-20 w500 lh140 white-clr text-capitalize">               
                  Warning: Please Use Responsibly. This is NOT A Toy.
                  <br>
                  Do NOT Use For Unethical Or Illegal Purposes.
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md40">
               <div class="col-md-10 col-12 mx-auto">
                  <!-- <img src="assets/images/product-image.webp" class="img-fluid d-block mx-auto"> -->
                  <div class="col-12 responsive-video">
                     <iframe src="https://player.vimeo.com/video/718794592?loop=0&controls=1&muted=0&autoplay=0" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                  </div>
               </div>
            </div>
         
         </div>
      </div>

      <!-- Header Section End -->
      
      <!-- CTA Btn Start-->
      <div class="white-dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center">
                  <div class="f-md-24 f-20 text-center mt3 black black-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 black-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 black-clr mt-md30">
                     There is no coupon for this  as its  a <span class="pink-clr w700">"premium psychology weapon"</span> product
                  </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="https://jvz6.com/c/10103/391284/" class="text-center bonusbuy-btn">
                  <span class="text-center">Get Vidamaze  Bundle  + Get Vidamaze AI Pro Pro free</span>
                  </a>
               </div>
                  <!-- <div class="f-16 f-md-20 lh150 w400 text-center mt20 black-clr">
                     Use Coupon Code <span class="w700 pink-clr">"AMITVIP"</span> for an Additional <span class="w700 pink-clr"> Discount</span> on Commercial Licence
                  </div> -->
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Get Vidamaze  + Get Vidamaze AI Pro Pro free</span>
                  </a>
               </div>
               <div class="col-12 mt15 mt-md40">
                  <h3 class="f-md-22 f-20 w500 text-center black black-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-black black-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 smmltd">Days</span>
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Sec</span>
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Btn End -->


      <!-------AFFILIATE NINJA PRO----------->
      <!-------Exclusive Bonus----------->
      <div class="exclusive-bonus">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                  <div class="white-clr f-24 f-md-36 lh140 w600">
                     Exclusive Bonus #1 : AFFILIATE NINJA PRO  
                  </div>
                  <div class="f-18 f-md-20 w500 mt20 white-clr">
                     After using <span class="yellow-clr w700"> VIDAMAZE </span> you can have numerous of traffic you can use <span class="yellow-clr w700"> AFFILIATE NINJA PRO </span> where you can earn by affiliate marketing can make SEO optimize funnel to build a list and target your audience and earn a minimum of $1000 daily.
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="container mt30 mb30">
         <div class="row">
            <div class="col-12 text-center">
               <img src="assets/images/affiliate-ninjapro-logo.png" alt="Affiliate NinjaPro" class="mx-auto d-block img-fluid">
            </div>
         </div>
      </div>

      <div class="affiliate-header-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-md-22 f-16 w400 lh140 white-clr">Are you sick &amp; tired of wasting hours of your time setting up affiliate promos and paying a ton of money getting traffic on them, only to never see them get any sales?
                  </div>
               </div>
               <div class="col-12 mt-md35 mt20 f-md-45 f-28 w700 text-center white-clr lh140">
                  REVEALED: A Cloud-Based App That <span class="lightcolor"> Creates SEO-Optimized Affiliate Funnels to Build You a List &amp; Get Targeted Traffic...</span> 
               </div>
               <div class="col-12">
                  <img src="assets/images/affiliate-line.png" class="img-fluid d-block mx-auto mt30">
                  <div class="text-center f-md-22 f-18 lh140 w500 mt10  mb10 white-clr">
                     ...Completely done-for-you high converting affiliate funnels…
                  </div>
                  <img src="assets/images/affiliate-line.png" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
                  <img src="assets/images/affiliate-pbox.png" alt="Affiliate Product Box" Class="mx-auto d-block img-fluid">
               </div>
            </div>
         </div>
      </div>

      <div class="gr-1">
         <div class="container-fluid p0">
            <picture>
               <source media="(min-width:768px)" srcset="assets/images/affiliate-steps.webp">
               <source media="(min-width:320px)" srcset="assets/images/affiliate-mview-steps.webp" style="width:100%" class="img-fluid mx-auto">
               <img src="assets/images/affiliate-steps.webp" alt="Coursesify Steps" class="img-fluid" style="width: 100%;">
            </picture>
         </div>
      </div>
      <!-------AFFILIATE NINJA PRO END----------->

      <!-------Agenciez----------->
      <!-------Exclusive Bonus----------->
      <div class="exclusive-bonus">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                  <div class="white-clr f-24 f-md-36 lh140 w600">
                     Exclusive Bonus #2 : Agenciez   
                  </div>
                  <div class="f-18 f-md-20 w500 mt20 white-clr">
                     After using <span class="yellow-clr w700"> VIDAMAZE </span> you can have numerous of traffic you can use <span class="yellow-clr w700"> Agenciez </span>  to  Kickstart Your Own Profitable Website Agency and Tap into Real 284 B Market
                  </div>
               </div>
            </div>
         </div>
      </div>

      <div class="agenciez-header-section">
			<div class="container">
				<div class="row">
					<div class="col-12 text-center">
						<img src="assets/images/agencies-logo.png" class="img-fluid mx-auto d-block">
					</div>
					<div class="col-12 f-md-28 f-22 lh140 w400 text-center white-clr mt20 mt-md-65">
						<i>Kickstart Your Own <u>Profitable Website Agency</u> and Tap into Real 284 B Market <br class="d-none d-md-block">Right from Your Home.</i>
					</div>
					<div class="col-md-8 col-12">
						<div class="game-changer">
							<div class="f-24 f-md-36 w500 text-center black-clr lh120 text-roated">Game Changer</div>
						</div>
					</div>
					<div class="col-12">
						<div class="text-center">
							<div class="highlihgt-heading">
								<div class="f-28 f-md-45 w400 text-center white-clr lh140 ">
								<span class="green-clr">The World’s No.1 Pro Website Builder Lets You</span> <span class="w500">Create Beautiful Local WordPress Websites &amp; Ecom Stores for  Any Business in Just 7 Minutes!</span>
								</div>

							</div>
						</div>
					</div>
					<div class="col-12   f-24 f-md-30 w400 lh140 text-center white-clr mt20 mt-sm20">
						Easily create &amp; sell websites for big profits to retail stores, book shops, coaches, dentists, attorney, gyms, spas, restaurants, cafes, &amp; 100+ other niches...  	
					</div>
					
					<div class="col-12  f-22 f-md-28 w400 lh140 text-center green-clr mt20 mt-sm20">
						No Tech Skills Needed. No Monthly Fees.
					</div>

					<div class="col-12 mt20 mt-md-20">
                  <div class="row align-items-center">
                     <div class="col-12 col-sm-7">
                        <div class="responsive-video">
                           <iframe src="https://agenciez.dotcompal.com/video/embed/1xqnfd9akv" style="width:100%; height:100%" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                        </div>
                     </div>
                     <div class="col-12 col-sm-5 mt20 mt-sm0">
                        <div class="f-18 f-sm-20 lh150 w300">
                           <ul class="header-bordered-list pl0 white-clr">
                           <li>Tap Into HUGE $284 B Web Agency Industry  </li>
                           <li>Create Elegant Local Business Sites, Store, or Blogs with Ease.  </li>
                           <li>No. 1 WordPress Framework–Customize Themes in Just Few Click  </li>
                           <li>30+ Stunning Themes with 200+ Templates &amp; 2000+ Possible Designs  </li>
                           <li>100% SEO, Social-Media &amp; Mobile Friendly Sites  </li>
                           <li>FREE Commercial License - Build an Incredible Income Helping Clients!  </li>
                           </ul>
                        </div>
                     </div>
                  </div>
					</div>
				</div>
			</div>
		</div>

      <div class="gr-1">
         <div class="container-fluid p0">
            <picture>
               <source media="(min-width:768px)" srcset="assets/images/agencies-steps.webp">
               <source media="(min-width:320px)" srcset="assets/images/agencies-mview-steps.webp" style="width:100%" class="img-fluid mx-auto">
               <img src="assets/images/agencies-steps.webp" alt="Coursesify Steps" class="img-fluid" style="width: 100%;">
            </picture>
         </div>
      </div>

      <!-- Profit MOJO Start   -->
      <!-------Exclusive Bonus----------->
      <div class="exclusive-bonus">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                  <div class="white-clr f-24 f-md-36 lh140 w600">
                     Exclusive Bonus #3 : Profit MOJO   
                  </div>
                  <div class="f-18 f-md-20 w500 mt20 white-clr">
                     After using <span class="yellow-clr w700"> VIDAMAZE </span> you can have numerous of traffic you can use <span class="yellow-clr w700"> useProfit MOJO </span> to Create stunning promo pages, lead pages, webinar pages, bonus & review pages & much more in just a few minutes, 100% Cloud-Based 1-Click Landing Page Builder That Creates High Converting Marketing Pages And Also Drives Tons Of Traffic From Facebook Without Paying Any Fees.
                  </div>
               </div>
            </div>
         </div>
      </div>

      <div class="container mt30 mb30">
         <div class="row">
            <div class="col-12 text-center">
               <img src="assets/images/profit-mozo-logo.png" alt="Profit Mozo" class="mx-auto d-block img-fluid">
            </div>
         </div>
      </div>

      <div class="profit-mozo-header-section">
         <div class="profit-mozo-inner-section">
            <div class="container">
               <div class="row">
                  <div class="col-12 text-center">
                     <div class="f-md-22 f-16 w400 lh140 white-clr">Are you sick & tired of paying hundreds of dollars monthly to landing page builders that constantly fail to deliver?
                     </div>
                  </div>
                  <div class="col-12 mt-md35 mt20 f-md-37 f-28 w700 text-center orange lh140">
                     <span class="profitcolor">Revealed:</span> <span class="underline">Amazingly Fast And Easy</span>, <span class="profitcolor">100% Cloud-Based 1-Click Landing Page Builder That Creates High Converting Marketing Pages And Also Drives Tons Of Traffic From Facebook </span><span class="underline">Without Paying Any Monthly Fees Forever...</span>
                  </div>
                  <div class="col-12">
                     <div class="text-center f-md-22 f-18 lh140 w500 mt20  white-clr">
                        Create stunning promo pages, lead pages, webinar pages, bonus &amp; review pages &amp; much more in just a few minutes…
                     </div>
                  </div>
               </div>
            </div>
         </div> 
         <img src="assets/images/profit-mozo-bg-shadow.png" class="img-fluid d-block mx-auto" style="margin-top:-15px;">
         <div class="container mt20 mt-md30">
            <div class="row">
               <div class="col-12 col-md-8 mx-auto">
                  <div class="responsive-video">
                     <object data="https://www.youtube.com/embed/nvu791Og7KI?autoplay=1&amp;controls=0&amp;rel=0&amp;autohide=1&amp;nologo=1&amp;showinfo=0&amp;frameborder=0&amp;theme=light&amp;modestbranding=1"></object>
                  </div>
               </div>
            </div>
         </div>  
      </div>

      <div class="profit-mozo-salesarea">
	      <div class="container">
    	      <div class="row">
		         <div class="col-md-10 col-12 mx-auto">
			         <div class="text-center f-md-31 f-23 w700">
                     <span class="bluecolor">And the main thing is -</span><span class="themecolor"> NOW YOU CAN DO IT TOO</span>
                  </div>
			         <div class="col-12">
			            <img src="assets/images/profit-mozo-arrow-down.png" class="d-block img-fluid mx-auto">
                  </div>
                  <div class="col-12 mt20 mt-md50">
                     <div class="f-md-20 f-18  lh150 ">
                        So how does it work and how you too can get best results with it starting from today?<br><br>
                        I know it sounds very fancy, but it’s actually a reality now as you too can...
                     </div>
                  </div> 
               </div>
		         <div class="col-12 mt20 mt-md50">
                  <div class="text-center">
                     <span class="f-md-34 f-25 w700 lh150">Create Your Own High Converting &amp; Profitable Marketing Campaigns </span><br> <span class="f-md-29 f-21 lh150">In Just 3 Simple Steps!</span>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-12 col-md-10 mx-auto">
                  <div class="row mt20 mt-md50 align-items-center">
                     <div class="col-md-7 col-12">
                        <img src="assets/images/step1.png" class="mx-auto img-fluid d-none d-md-block">
                        <img src="assets/images/mobstep1.png" class="mx-auto img-fluid d-block d-md-none">
                     </div>
                     <div class="col-md-5 col-12 mt20 mt-md0">
                        <div class="profitcolor f-md-31 f-23 w600 text-center text-md-start">Choose a gorgeous template</div>
                        <div class="f-md-20 f-17 text-center text-md-start mt10 lh140">To begin with, just choose one of our high-converting, gorgeous and mobile-optimized templates</div>
                     </div>
                  </div>
                  <div class="row mt20 mt-md50 align-items-center">
                     <div class="col-md-7 col-12 order-md-2 mt20 mt-md-0">
                        <img src="assets/images/step2.png" class="mx-auto img-fluid d-none d-md-block">
                        <img src="assets/images/mobstep2.png" class="mx-auto img-fluid d-block d-md-none">
                     </div>
                     <div class="col-md-5 col-12 order-md-1">
                        <div class="profitcolor f-md-31 f-23 w600 text-center text-md-start">Edit</div>
                        <div class="f-md-20 f-17 text-center text-md-start mt10 lh140">Now customize it using our easy &amp; LIVE editor to add attractive images, content or insert CTA's etc.</div>
                     </div>
                  </div>
                  <div class="row mt20 mt-md50 align-items-center">
                     <div class="col-md-7 col-12">
                        <img src="assets/images/step3.png" class="mx-auto img-fluid d-none d-md-block">
                        <img src="assets/images/mobstep3.png" class="mx-auto img-fluid d-block d-md-none">
                     </div>
                     <div class="col-md-5 col-12 mt20 mt-md-0">
                        <div class="profitcolor f-md-31 f-23 w600 text-center text-md-start">Enjoy Viral Facebook Traffic</div>
                        <div class="f-md-20 f-17 text-center text-md-start mt10 lh140">Drive hordes of targeted traffic from Facebook with our inbuilt SocialMozo app and get direct views and leads for your offers. Now just sit back and relax and get success rolling in leaps and bounds.</div>
                     </div>	
                  </div>
               </div>
            </div>
         </div>
      </div>

      <div class="profit-mozo-bluestrip">
         <div class="container">
            <div class="col-12 text-center">
               <div class="col-md-8 mx-auto f-md-31 f-23 w600">In Fact, We Challenge You to create your First Profitable Marketing Page Right NOW.</div>
            </div>
         </div>
      </div>

      <div class="profit-mozo-presentingbg">
	      <div class="container">
	         <div class="row">
               <div class="col-md-12 col-12 text-center f-md-45 f-28 w700">
                  Proudly Presenting…
               </div>
               <div class="col-md-10 col-12 mx-auto mt20 mt-md50">
                  <img src="assets/images/profit-mozo-pbox.png" class="img-fluid d-block mx-auto">   
               </div>
               <div class="col-12 mt20 mt-md50">
                  <div class="f-md-20 f-18 lh160">
                     ProfitMozo is an amazingly fast and easy 100% cloud-based 1-click landing page builder that creates search engine friendly, mobile optimized high converting marketing pages and… <br><br>
                     Also, drives tons of high quality traffic for ANY offer in ANY niche from Facebook. <br><br>
                     So, you don’t need to turn your hair grey about boosting sales, leads and commissions.<br><br>
                     And the biggest catch is, you don't need to spend a fortune on purchasing any domain or hosting service. All you need is to create attractive pages right inside our software, and see results like never before. <br><br>
                     ProfitMozo will remove all the stress and complexities out of marketing page creation, and enable you to maximize returns in a cost-effective manner... And you won't even need a dollar-biting page designer!<br><br>
                     A highly engaging method that both captivates and converts any audience… without cost and without any grunt work.<br><br>

                     For getting all these mind-blowing benefits, you don’t need to mess around with any complicated <strong>codes, tedious installations or hosting issues...</strong>Our dead simple software is hosted on cloud, you just login and ENJOY all the amazing benefits.
                  </div>
               </div>
            </div>
         </div> 
      </div>

      <!-------Exclusive Bonus----------->
      <div class="exclusive-bonus">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                  <div class="white-clr f-24 f-md-36 lh140 w600">
                     Exclusive Bonus #4 : Entertainerz   
                  </div>
                  <div class="f-18 f-md-20 w500 mt20 white-clr">
                     After using <span class="yellow-clr w700"> VIDAMAZE </span> you can have numerous of traffic you can use <span class="yellow-clr w700"> Entertainerz </span> and create a new site so can have more passive income it is a SUPER SIMPLE Software Makes Us $528/Day Creating AUTO-Updating & Traffic-Pulling Entertainment News Sites in Just 60 Seconds
                  </div>
               </div>
            </div>
         </div>
      </div>

      <div class="entertainerz-header-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row">
                     <div class="col-md-12 text-center">
                        <svg id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 189.77 50" style="max-height:80px">
                           <defs>
                              <style>.cls-1{fill:url(#linear-gradient-2);}.cls-2{fill:#fff;}.cls-3{fill:#001730;}.cls-4{fill:url(#linear-gradient-3);}.cls-5{fill:url(#linear-gradient);}</style>
                              <linearGradient id="linear-gradient" x1="20.65" y1="26.83" x2="42.69" y2="26.83" gradientUnits="userSpaceOnUse">
                                 <stop offset="0" stop-color="#bf003b"></stop>
                                 <stop offset="1" stop-color="#e23940"></stop>
                              </linearGradient>
                              <linearGradient id="linear-gradient-2" x1=".12" y1="29.26" x2="25.92" y2="29.26" gradientUnits="userSpaceOnUse">
                                 <stop offset="0" stop-color="#f38227"></stop>
                                 <stop offset="1" stop-color="#fbb826"></stop>
                              </linearGradient>
                              <linearGradient id="linear-gradient-3" x1="3.09" y1="36.12" x2="22.33" y2="28.06" gradientUnits="userSpaceOnUse">
                                 <stop offset="0" stop-color="#3369e9"></stop>
                                 <stop offset="1" stop-color="#002b7f"></stop>
                              </linearGradient>
                           </defs>
                           <path class="cls-2" d="M22.29,0c-.94,0-1.69,.76-1.69,1.69,0,.75,.48,1.39,1.16,1.61l-2.95,10.01-.61,2.09,2.13,.26,.41-2.75v-.03s1.44-9.5,1.44-9.5c.03,0,.07,0,.11,0,.93,0,1.69-.76,1.69-1.7s-.76-1.69-1.69-1.69Z"></path>
                           <path class="cls-2" d="M16.2,15.76l-1.74,.96-5.3-6.31c-.29,.23-.64,.36-1.03,.36-.94,0-1.7-.75-1.7-1.69s.76-1.69,1.7-1.69,1.69,.76,1.69,1.69c0,.41-.15,.79-.4,1.08l6.78,5.6Z"></path>
                           <g>
                              <path class="cls-2" d="M63.68,34.98c0,.14-.04,.35-.11,.62-.08,.34-.13,.55-.15,.62-.03,.32-.09,.79-.19,1.41-.05,.16-.16,.24-.35,.24-.09,0-.26-.02-.51-.05-1.1-.16-2.82-.24-5.13-.24-.66,0-1.64,.02-2.96,.05s-2.31,.05-2.96,.05c-.3,0-.44-.13-.44-.39,0-1.01,.06-2.52,.17-4.53,.11-2.02,.17-3.53,.17-4.53s-.05-2.93-.16-5.72c-.02-.72-.07-1.79-.15-3.22v-.19c-.02-.21,.09-.31,.34-.31,.63,0,1.57,.02,2.83,.05,1.26,.04,2.2,.05,2.83,.05s1.5-.02,2.7-.05c1.2-.04,2.1-.05,2.7-.05,.42,0,.65,.13,.69,.39,.19,1.19,.28,1.92,.28,2.21,0,.21-.1,.31-.3,.31-.76,0-1.92-.04-3.47-.13-1.55-.09-2.71-.13-3.5-.13-.74,0-1.2,.1-1.39,.3-.19,.2-.29,.69-.29,1.47v2.01c0,.55,.01,.87,.04,.96,.08,.28,.31,.42,.7,.42,.11,0,.26,0,.46-.01h.46c1.76,0,3.34-.03,4.74-.09,.55-.02,1.19-.06,1.94-.13,.19-.02,.28,.06,.28,.24,0,.29-.03,.73-.1,1.32-.07,.59-.1,1.04-.1,1.33,0,.16-.13,.24-.4,.24-.11,0-.51-.03-1.2-.09-.5-.05-1.63-.08-3.39-.08-1.48,0-2.42,.02-2.82,.05-.36,.03-.57,.22-.62,.57,0-.06,.01,.28,.01,1.04v1.99c0,.93,.14,1.52,.43,1.75,.22,.19,.79,.28,1.68,.28h1.48c.63-.01,2.39-.12,5.29-.34h.2c.18-.03,.27,.08,.27,.32Z"></path>
                              <path class="cls-2" d="M77.61,37.42c0,.2-.09,.3-.27,.3-.3,0-.74,0-1.33-.01-.59,0-1.03-.01-1.33-.01-.16,0-.24-.13-.24-.39,0-.44,.01-1.11,.04-2.01,.03-.9,.04-1.57,.04-2.02,0-.36,0-.9-.01-1.61,0-.71-.01-1.25-.01-1.61,0-1.02-.15-1.75-.44-2.17-.36-.5-1.02-.75-1.99-.75-.45,0-1.07,.25-1.87,.75-.84,.53-1.27,1-1.27,1.41v8.08c0,.22-.09,.34-.26,.34-.29,0-.72,0-1.29-.01-.57,0-1.01-.01-1.29-.01-.19,0-.28-.1-.28-.31,0-.71,.01-1.77,.03-3.19,.02-1.42,.03-2.49,.03-3.21,0-1.91-.2-3.68-.61-5.29-.03-.08-.04-.14-.04-.18,0-.09,.06-.15,.18-.19,.06,0,.58-.09,1.54-.25,.97-.16,1.47-.24,1.52-.24,.05,0,.09,.07,.11,.22,.04,.56,.15,1.12,.31,1.68,.47-.37,1.08-.83,1.85-1.37,.94-.57,1.85-.86,2.73-.86,1.69,0,2.84,.49,3.46,1.47,.46,.72,.69,1.88,.69,3.49,0,.28,0,.7-.02,1.28-.01,.57-.02,1.01-.02,1.29,0,.6,0,1.5,.03,2.69,.02,1.19,.03,2.09,.03,2.69Z"></path>
                              <path class="cls-2" d="M87.67,35.21c0,.2-.02,.47-.05,.81-.05,.42-.07,.7-.08,.82,0,.16-.02,.26-.04,.3-.04,.06-.12,.13-.26,.19-.74,.35-1.89,.53-3.48,.53-2.1,0-3.15-.95-3.15-2.86,0-.76,.02-1.91,.05-3.44,.04-1.53,.05-2.68,.05-3.44,0-.37-.18-.57-.55-.59-.33,0-.66,0-.98-.01-.19-.04-.28-.41-.28-1.1,0-.26,.02-.53,.05-.82,.03-.22,.17-.35,.43-.39,.22,0,.44,0,.66-.01,.41-.02,.62-.2,.62-.55s-.01-.91-.03-1.64c-.02-.74-.03-1.29-.03-1.66,0-.66,.13-.98,.38-.98,.08,0,.92,.14,2.53,.42,.21,.03,.31,.13,.31,.3,0,.4-.03,.99-.09,1.78-.06,.79-.09,1.38-.09,1.78,0,.26,.12,.39,.36,.39h3.23c.15,0,.23,.05,.23,.16s-.02,.28-.05,.52c-.03,.24-.05,.42-.05,.53,0,.13,0,.34,.01,.62,0,.28,.01,.48,.01,.62,0,.17-.13,.26-.38,.26-.36,0-.9-.02-1.63-.07-.73-.05-1.27-.07-1.63-.07-.07,0-.13,.26-.16,.77-.05,.64-.07,1.53-.07,2.68v2.01c0,.77,.07,1.31,.22,1.62,.22,.47,.66,.7,1.33,.7,.28,0,.69-.06,1.23-.18s.94-.18,1.2-.18c.11,0,.16,.08,.16,.24Z"></path>
                              <path class="cls-2" d="M101.01,30.46c0,.66-.29,1.11-.86,1.33-.39,.14-3.17,.41-8.34,.79,.12,.79,.55,1.46,1.31,2.02,.72,.53,1.49,.79,2.32,.79,1.42,0,2.72-.46,3.91-1.37,.14-.12,.29-.23,.43-.35,.07,0,.12,0,.14,.03,.02,.02,.19,.33,.51,.94,.31,.61,.47,.93,.47,.98,0,.05-.07,.16-.22,.31-1.32,1.4-3.01,2.1-5.08,2.1s-3.74-.62-4.98-1.87c-1.24-1.24-1.86-2.9-1.86-4.98,0-1.85,.59-3.46,1.78-4.84,1.25-1.43,2.78-2.14,4.59-2.14,1.69,0,3.1,.62,4.23,1.86,1.1,1.2,1.66,2.67,1.66,4.39Zm-3.21-.82c0-.75-.29-1.41-.88-1.98s-1.25-.86-1.99-.86c-.83,0-1.56,.33-2.2,1-.64,.66-.96,1.41-.96,2.24,0,.19,.16,.28,.48,.28,1.33,0,2.92-.13,4.77-.4,.52-.07,.78-.16,.78-.27Z"></path>
                              <path class="cls-2" d="M110.6,25.04c0,.99-.07,1.89-.2,2.69-.03,.21-.12,.31-.27,.31-.19,0-.47-.02-.86-.06-.38-.04-.67-.06-.87-.06-1.13,0-1.85,.32-2.17,.96-.19,.39-.28,1.19-.28,2.41v1.48c0,.5,.02,1.27,.06,2.29,.04,1.02,.06,1.79,.06,2.29,0,.24-.1,.36-.31,.36-.28,0-.7,0-1.27-.01-.57,0-.99-.01-1.27-.01-.21,0-.31-.11-.31-.34,0-.73,.01-1.82,.03-3.29,.02-1.46,.03-2.56,.03-3.3,0-2.22-.21-4-.62-5.35-.02-.07-.03-.13-.03-.16,0-.11,.06-.18,.18-.23,.33-.04,.8-.09,1.4-.16,.97-.18,1.42-.27,1.35-.27,.14,0,.24,.26,.28,.79,.04,.53,.13,.79,.24,.79,.02,0,.04,0,.07-.03,.37-.23,.74-.47,1.1-.7,.4-.24,.82-.42,1.24-.54,.35-.11,.8-.16,1.35-.16,.7,0,1.05,.09,1.05,.28Z"></path>
                              <path class="cls-2" d="M119.58,35.21c0,.2-.02,.47-.05,.81-.05,.42-.07,.7-.08,.82,0,.16-.02,.26-.04,.3-.04,.06-.12,.13-.26,.19-.74,.35-1.89,.53-3.48,.53-2.1,0-3.15-.95-3.15-2.86,0-.76,.02-1.91,.05-3.44,.04-1.53,.05-2.68,.05-3.44,0-.37-.18-.57-.55-.59-.33,0-.66,0-.98-.01-.19-.04-.28-.41-.28-1.1,0-.26,.02-.53,.05-.82,.03-.22,.17-.35,.43-.39,.22,0,.44,0,.66-.01,.41-.02,.62-.2,.62-.55s-.01-.91-.03-1.64c-.02-.74-.03-1.29-.03-1.66,0-.66,.13-.98,.38-.98,.08,0,.92,.14,2.53,.42,.21,.03,.31,.13,.31,.3,0,.4-.03,.99-.09,1.78-.06,.79-.09,1.38-.09,1.78,0,.26,.12,.39,.36,.39h3.23c.15,0,.23,.05,.23,.16s-.02,.28-.05,.52c-.03,.24-.05,.42-.05,.53,0,.13,0,.34,.01,.62,0,.28,.01,.48,.01,.62,0,.17-.13,.26-.38,.26-.36,0-.9-.02-1.63-.07-.73-.05-1.27-.07-1.63-.07-.07,0-.13,.26-.16,.77-.05,.64-.07,1.53-.07,2.68v2.01c0,.77,.07,1.31,.22,1.62,.22,.47,.66,.7,1.33,.7,.28,0,.69-.06,1.23-.18s.94-.18,1.2-.18c.11,0,.16,.08,.16,.24Z"></path>
                              <path class="cls-2" d="M134.6,25.5s-.01,.12-.04,.24c-.35,2.06-.53,3.83-.53,5.32,0,.09,.11,2.11,.34,6.05v.19c.02,.26-.1,.39-.34,.39s-.62,.03-1.12,.09c-.5,.06-.87,.09-1.11,.09-.17,0-.3-.31-.39-.94-.09-.63-.18-.94-.26-.94-.05,0-.3,.18-.73,.53-.53,.43-1.02,.76-1.48,.98-.71,.36-1.42,.54-2.13,.54-1.74,0-3.21-.7-4.4-2.09-1.13-1.33-1.7-2.88-1.7-4.65,0-1.99,.57-3.64,1.71-4.94,1.19-1.36,2.77-2.03,4.71-2.03,1.37,0,2.57,.48,3.6,1.43,.15,.18,.39,.44,.71,.78,.03,.03,.05,.04,.08,.04,.05,0,.13-.28,.23-.85,.1-.57,.22-.85,.38-.85,.24,0,.7,.07,1.36,.2,.74,.16,1.1,.31,1.1,.43Zm-3.43,5.63c0-1.1-.33-2.05-.98-2.84-.69-.85-1.58-1.28-2.65-1.28s-1.98,.42-2.69,1.27c-.69,.81-1.04,1.76-1.04,2.86s.35,2.03,1.04,2.83c.72,.84,1.62,1.25,2.69,1.25s1.93-.43,2.64-1.28c.66-.8,1-1.73,1-2.8Z"></path>
                              <path class="cls-2" d="M140.3,21.58c0,1.1-.62,1.66-1.85,1.66s-1.87-.55-1.87-1.66c0-.49,.19-.9,.58-1.23,.36-.31,.79-.46,1.29-.46s.94,.16,1.3,.47c.36,.31,.55,.72,.55,1.21Zm-.09,3.65c0,.69-.04,1.74-.13,3.13-.09,1.4-.13,2.44-.13,3.15,0,.66,.01,1.64,.04,2.94,.03,1.31,.04,2.28,.04,2.93,0,.19-.08,.28-.24,.28h-2.63c-.19,0-.28-.18-.28-.55,0-.62,.02-1.55,.05-2.8,.03-1.24,.05-2.18,.05-2.81,0-.7-.05-1.75-.16-3.15s-.15-2.45-.15-3.15c0-.15,.09-.23,.28-.23,.16,0,.41,.02,.75,.07,.34,.04,.59,.07,.76,.07s.42-.02,.76-.07c.34-.04,.59-.07,.76-.07,.15,0,.23,.09,.23,.26Z"></path>
                              <path class="cls-2" d="M154.74,37.42c0,.2-.09,.3-.27,.3-.3,0-.74,0-1.33-.01-.59,0-1.03-.01-1.33-.01-.16,0-.24-.13-.24-.39,0-.44,.01-1.11,.04-2.01,.03-.9,.04-1.57,.04-2.02,0-.36,0-.9-.01-1.61,0-.71-.01-1.25-.01-1.61,0-1.02-.15-1.75-.44-2.17-.36-.5-1.02-.75-1.99-.75-.45,0-1.07,.25-1.87,.75-.84,.53-1.27,1-1.27,1.41v8.08c0,.22-.09,.34-.26,.34-.29,0-.72,0-1.29-.01-.57,0-1.01-.01-1.29-.01-.19,0-.28-.1-.28-.31,0-.71,.01-1.77,.03-3.19,.02-1.42,.03-2.49,.03-3.21,0-1.91-.2-3.68-.61-5.29-.03-.08-.04-.14-.04-.18,0-.09,.06-.15,.18-.19,.06,0,.58-.09,1.54-.25,.97-.16,1.47-.24,1.52-.24,.05,0,.09,.07,.11,.22,.04,.56,.15,1.12,.31,1.68,.47-.37,1.08-.83,1.85-1.37,.94-.57,1.85-.86,2.73-.86,1.69,0,2.84,.49,3.46,1.47,.46,.72,.69,1.88,.69,3.49,0,.28,0,.7-.02,1.28-.01,.57-.02,1.01-.02,1.29,0,.6,0,1.5,.03,2.69,.02,1.19,.03,2.09,.03,2.69Z"></path>
                              <path class="cls-2" d="M168.7,30.46c0,.66-.29,1.11-.86,1.33-.39,.14-3.17,.41-8.34,.79,.12,.79,.55,1.46,1.31,2.02,.72,.53,1.49,.79,2.32,.79,1.42,0,2.72-.46,3.91-1.37,.14-.12,.29-.23,.43-.35,.07,0,.12,0,.14,.03,.02,.02,.19,.33,.51,.94,.31,.61,.47,.93,.47,.98,0,.05-.07,.16-.22,.31-1.32,1.4-3.01,2.1-5.08,2.1s-3.74-.62-4.98-1.87c-1.24-1.24-1.86-2.9-1.86-4.98,0-1.85,.59-3.46,1.78-4.84,1.25-1.43,2.78-2.14,4.59-2.14,1.69,0,3.1,.62,4.23,1.86,1.1,1.2,1.66,2.67,1.66,4.39Zm-3.21-.82c0-.75-.29-1.41-.88-1.98s-1.25-.86-1.99-.86c-.83,0-1.56,.33-2.2,1-.64,.66-.96,1.41-.96,2.24,0,.19,.16,.28,.48,.28,1.33,0,2.92-.13,4.77-.4,.52-.07,.78-.16,.78-.27Z"></path>
                              <path class="cls-2" d="M178.29,25.04c0,.99-.07,1.89-.2,2.69-.03,.21-.12,.31-.27,.31-.19,0-.47-.02-.86-.06-.38-.04-.67-.06-.87-.06-1.13,0-1.85,.32-2.17,.96-.19,.39-.28,1.19-.28,2.41v1.48c0,.5,.02,1.27,.06,2.29s.06,1.79,.06,2.29c0,.24-.1,.36-.31,.36-.28,0-.7,0-1.27-.01-.57,0-.99-.01-1.27-.01-.21,0-.31-.11-.31-.34,0-.73,.01-1.82,.03-3.29,.02-1.46,.03-2.56,.03-3.3,0-2.22-.21-4-.62-5.35-.02-.07-.03-.13-.03-.16,0-.11,.06-.18,.18-.23,.33-.04,.8-.09,1.4-.16,.97-.18,1.42-.27,1.35-.27,.14,0,.24,.26,.28,.79,.04,.53,.13,.79,.24,.79,.02,0,.04,0,.07-.03,.37-.23,.74-.47,1.1-.7,.4-.24,.82-.42,1.24-.54,.35-.11,.8-.16,1.35-.16,.7,0,1.05,.09,1.05,.28Z"></path>
                              <path class="cls-2" d="M189.77,35.13s-.07,.36-.2,.96c-.14,.64-.22,1.03-.24,1.19-.04,.31-.12,.47-.24,.47-.31,0-.74-.02-1.27-.05-.6-.04-1.02-.07-1.25-.08-.6-.02-1.46-.03-2.59-.03-.87,0-1.84,.04-2.9,.11-1.33,.09-1.9,.13-1.71,.13-.19,0-.31-.08-.36-.24-.13-.43-.19-.93-.19-1.51,0-.08,0-.2,.01-.34s.01-.26,.01-.33c0-.13,.1-.28,.29-.45,.19-.18,1.15-1.19,2.88-3.05,.87-.91,2.11-2.3,3.7-4.19h-5.94c-.14,0-.22-.09-.22-.28,0-.26,.06-.65,.17-1.17,.11-.52,.17-.9,.17-1.15,0-.11,.13-.16,.38-.16,.5,0,1.25,.02,2.25,.07s1.75,.07,2.25,.07c2.46,0,3.85,.02,4.16,.05,.22,.03,.34,.12,.34,.28,0,1.36-.05,2.12-.16,2.29-.09,.13-.87,1.01-2.33,2.65-.94,1.07-2.37,2.66-4.27,4.77,2.99-.08,5.31-.12,6.95-.12,.21,0,.31,.04,.31,.12Z"></path>
                           </g>
                           <path class="cls-2" d="M16.61,49.17c.96-1.64,6.11-6.32,13.48-6.77,2.78-.17,5.12,.34,6.8,.88,0,0,.84,.29,.15,.94s-11.41-1.5-19.36,5.17c-1.6,1.37-1.2,.01-1.07-.22Z"></path>
                           <path class="cls-5" d="M42.69,31.94c-.01,2.52-.67,4.93-2.77,6.32-1.62,1.06-3.89,2.45-6.95,3.79,.63-1.27,.93-2.75,.99-4.28,.01-.21,.01-.43,.01-.64,.01-9.53-3.63-17.69-6.99-21.14-.16-.16-.32-.32-.5-.47-1.51-1.33-3.74-2.37-5.74-2.61-.03,0-.06-.01-.09-.01,.03-.01,.07-.01,.1-.02,3.12-.66,6.47-1.1,9.93-1.26,2.07-.09,4.7,1.12,6.16,2.62,2.81,2.89,5.86,9.72,5.85,17.7Z"></path>
                           <path class="cls-1" d="M33.97,37.13c0,.21,0,.43-.01,.64-.06,1.53-.36,3.01-.99,4.28-.27,.12-.54,.23-.82,.35-3.01,1.24-6.73,2.41-11.29,3.21v-.21c0-.47-.01-.95-.03-1.41-.02-.5-.04-.99-.07-1.48-.01-.04-.01-.07-.01-.11-.03-.46-.07-.91-.12-1.36-.02-.25-.05-.49-.08-.74-.08-.69-.17-1.36-.28-2.03-.07-.45-.15-.89-.23-1.32-.01-.08-.03-.17-.05-.25-.1-.54-.22-1.08-.34-1.6-.03-.13-.06-.26-.09-.39-.11-.44-.22-.87-.34-1.3-.11-.44-.24-.87-.37-1.29-.06-.2-.12-.4-.19-.6-.14-.44-.28-.88-.44-1.3-.18-.51-.37-1.01-.56-1.5-.25-.62-.51-1.22-.77-1.8-.6-1.29-1.23-2.47-1.88-3.53-.12-.2-.25-.4-.38-.59-.12-.2-.25-.39-.38-.57-.63-.92-1.28-1.72-1.92-2.38-.16-.16-.33-.33-.51-.48-.02-.03-.05-.05-.08-.08-.06-.05-.12-.1-.18-.15-.45-.38-.95-.74-1.48-1.07-.14-.09-.28-.17-.42-.25-.48-.28-.98-.53-1.49-.75,1.46-.71,3.09-1.4,4.87-2.04,.53-.18,1.07-.37,1.62-.54,1.32-.43,2.71-.82,4.15-1.18,.6-.14,1.22-.28,1.84-.41,.03,0,.06,.01,.09,.01,2,.24,4.23,1.28,5.74,2.61,.18,.15,.34,.31,.5,.47,3.36,3.45,7,11.61,6.99,21.14Z"></path>
                           <path class="cls-4" d="M20.86,45.61c-3.31,.58-7.05,.95-11.28,1.01-.83,.01-1.62-.36-2.33-.79-2.42-1.46-5.57-7.74-7.18-18.65-.38-2.58,.81-5,2.76-6.72,1.27-1.12,3.09-2.28,5.33-3.39,1.36,.59,2.64,1.4,3.65,2.3,.18,.16,.34,.32,.5,.48,3.71,3.82,7.71,12.34,8.42,22.55,.08,1.05,.11,2.12,.11,3.21Z"></path>
                           <path class="cls-2" d="M6.78,22.31c3.46-3.05,12.1-6.51,21.9-6.95,1.63-.07,3.7,.88,4.84,2.06,2.21,2.28,4.61,7.65,4.6,13.93,0,1.98-.64,3.72-2.18,4.97-3.42,2.78-10.62,7.14-23.86,6.57-.65-.03-1.28-.28-1.84-.62-1.9-1.15-4.38-6.09-5.65-14.67-.3-2.03,.64-3.93,2.17-5.29Z"></path>
                           <path class="cls-3" d="M26.11,26.61l-5.61-1.56c-1.31-.36-2.85,.26-3.44,1.38-.23,.43-.29,.9-.17,1.33l1.51,5.69c.3,1.12,1.67,1.73,3.08,1.36,.54-.14,1.04-.42,1.4-.8l4.1-4.13c.96-.96,.86-2.3-.21-2.99-.2-.13-.42-.22-.66-.29h0Z"></path>
                        </svg>
                     </div>
                  </div>
               </div>
               <div class="col-12 text-center lh150 mt20 mt-md50">
                  <div class="f-md-20 f-18 w600 lh150 white-clr">
                  <span class="orange-clr">Legally Exploit a Little-Known Loophole</span> to Instantly Get Access to <br class="d-none d-md-block"> 800 million+ YouTube Videos &amp; Entertainment News…
                  </div>
                  <img src="assets/images/entertainerz-preheadline.webp" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 mt-md30 mt20 f-md-40 f-28 w600 text-center white-clr lh150">
               SUPER SIMPLE Software <span class="orange-clr w800"> Makes Us $528/Day Creating AUTO-Updating &amp; Traffic-Pulling Entertainment News Sites </span> in Just 60 Seconds…
               </div>
               <div class="col-12 mt-md25 mt20 f-18 f-md-21 w600 text-center lh150 white-clr">
               No Content Creation. No Camera. No Tech Hassles Ever... 100% Beginner Friendly!
               </div>
            </div>
            <div class="row mt20 mt-md40 gx-md-5">
               <div class="col-md-7 col-12 mt0 mt-md20">
                  <!-- <img src="https://cdn.oppyo.com/launches/entertainerz/special/proudly.webp" class="img-fluid d-block mx-auto"> -->
                  <div class="col-12 responsive-video">
                     <iframe src="https://entertainerz.dotcompal.com/video/embed/u1o4dj3t3c" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                  </div>
               </div>
               <div class="col-md-5 col-12 mt20 mt-md0">
                  <div class="key-features-bg">
                     <ul class="list-head pl0 m0 f-16 f-md-18 lh160 w500 white-clr">
                     <li>Legally Use from 800 million+ Trending Content Created by Others</li>
                     <li>Set And Forget System with Single Keyword</li>
                     <li>AI-Powered Software Puts Most Profitable Links on Your Websites</li>
                     <li>Built-In Automated Traffic Generating System</li>
                     <li>Complete Social Media Automation</li>
                     <li>100% SEO Friendly Website And Built-In Remarketing System</li>
                     <li>Automatically Translate Your Sites In 15+ Language According For More Traffic</li>
                     <li>Integration With Major Autoresponders And Social Media Apps</li>
                     <li>In-Built Content Spinner To Make Your Content Fresh</li>
                     <li>Make 5K-10K With Commercial License</li>
                     <li>A-Z Complete Video Training Is Included</li>
                     <li>Limited-Time Special Bonuses Worth $2285 If You Buy Today</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
      
      <div class="gr-1">
         <div class="container-fluid p0">
            <picture>
               <source media="(min-width:768px)" srcset="assets/images/entertainerz-steps.webp">
               <source media="(min-width:320px)" srcset="assets/images/entertainerz-mview-steps.webp" style="width:100%" class="img-fluid mx-auto">
               <img src="assets/images/entertainerz-steps.webp" alt="Coursesify Steps" class="img-fluid" style="width: 100%;">
            </picture>
         </div>
      </div>

      <!-- Bonus Section Header Start -->
      <div class="bonus-header">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-10 mx-auto heading-bg text-center">
                  <div class="f-24 f-md-36 lh140 w700"> When You Purchase Vidamaze, You Also Get <br class="hidden-xs"> Instant Access To These 20 Exclusive Bonuses</div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus Section Header End -->

      <div class="container mt20 mt-md50">
         <div class="row">
            <div class="col-12 col-md-12 text-center">
               <div class="f-md-40 f-28 lh140 w700 white-clr feature-shape">
                  Super Vip bonus
               </div>
            </div>
         </div>
      </div>
          <!-- Bonus Section Header End -->
          <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS #1</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus1.webp" class="img-fluid mx-auto d-block" alt="Bonus1">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        SOCIAL COVER GRAPHICS
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           Get amazing social cover graphics for leading social sites that you can use to offer to your clients & get their foot in the door of your service.
                           <br><br>
                           Social covers are what visitors first look at when they see a product or service online, and these need to be stunning. So, to help you offer these to your clients as a service, we are giving you amazing DFY social cover graphics that your clients will absolutely want, and love. These graphics are for;
                           <li class="mt20">YouTube</li>
                           <li class="mb20">LinkedIn</li>
                           The biggest sites in broad niches!
                        </ul>
                        <div class="download-btn">
                           <a href="https://drive.google.com/drive/folders/1n3RsnTVKcMwnUeAFseUfM7fv3gWPMsZ2?usp=sharing" target="_blank " class="promo-link f-20 f-md-22 w400">Download Now 🎁</a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #1 Section End -->
      <!-- Bonus #2 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS #2</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50">
                  <div class="row">
                     <div class="col-md-6 col-12 order-md-2">
                        <img src="assets/images/bonus2.webp" class="img-fluid mx-auto d-block" alt="Bonus2">
                     </div>
                     <div class="col-md-6 col-12 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md0 text-capitalize">
                        5 LOCAL NICHE VIDEOS
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           With the growing popularity of video marketing, you are going to NEED to offer it to your clients to keep them interested and returning.
                           <br><br>
                           <span class="w600">These videos are in the following carefully picked niches: </span><br><br>
                           <li>Restaurant</li>
                           <li>Fitness</li>
                           <li>Salon</li>
                           <li>Child Care</li>
                           <li>Car Repair</li>
                        </ul>
                        <div class="download-btn">
                           <a href="https://drive.google.com/drive/folders/1Rk6zhO4DHFapuMH4QZHE9-l45IdIBNvD?usp=sharing" target="_blank " class="promo-link f-20 f-md-22 w400">Download Now 🎁</a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #2 Section End -->
      <!-- Bonus #3 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS #3</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus3.webp" class="img-fluid mx-auto d-block " alt="Bonus3">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        LOCAL NICHE ARTICLE PACK
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           With EZLeadz, you will have your own directory site up & running in no time, and blogs and articles are what go hand-in-hand with sites like these.
                           <br><br>
                           Don't worry, you won't need to write any blogs or articles because we are giving you a local niche article pack that contains a bunch of articles and blogs for you to add to your websites and make it look top-notch to keep your viewers interested.
                           <br><br>
                           These are spread across various niches and will be a very useful tool for your website.
                        </ul>
                        <div class="download-btn">
                           <a href="https://drive.google.com/drive/folders/1x9NUZ8cbVySBYUUmJuvAP0_Ppqi0KvYR?usp=sharing" target="_blank " class="promo-link f-20 f-md-22 w400">Download Now 🎁</a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #3 Section End -->
      <!-- Bonus #4 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS #4</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="assets/images/bonus4.webp" class="img-fluid mx-auto d-block " alt="Bonus4">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                           SUREFIRE LOCAL LAUNCHPAD
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           When you have your own local agency business, what is important for you, is to stand out from the crowd and get ahead of the competition to be the #1 authority figure to get clients.
                           <br><br>
                           <span class="w600">You will get:</span> <br><br>
                           <li>Quick Overview</li>
                           <li>Find Low Hanging Fruit Clients</li>
                           <li>Pinpoint Opportunities</li>
                           <li>Professional Bartering</li>
                           <li>WordPress Conversion</li>
                        </ul>
                        <div class="download-btn">
                           <a href="https://drive.google.com/drive/folders/1_hvs4b5OG9tGAV6SvW2fxGvIhBO_Uczn?usp=sharing" target="_blank " class="promo-link f-20 f-md-22 w400">Download Now 🎁</a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #4 End -->
      <!-- Bonus #5 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS #5</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus5.webp" class="img-fluid mx-auto d-block " alt="Bonus5">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                           LOGO KIT (10 LOGO)
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           The logo of a business is more than just a design, it is the mark, the sign of the business that the customer remembers the service or product by, and we don't need to mention that it needs to be great on order for clients to like and remember it.
                           <br><br>
                           Logo building isn't easy, so to make it easy for you and offer it as a service to local businesses around you, we are giving you a logo kit with DFY logos that can be repurposed as per the need to make your client look good, and you get to fill your pockets with a service they will love.
                           <br><br>
                           These logos are set to make your client professional, and trustworthy so their customers remember them forever!
                        </ul>
                        <div class="download-btn">
                           <a href="https://drive.google.com/drive/folders/16SvLZ-JBoZcGpwBf-lIha4kuRn9t6BXp?usp=sharing" target="_blank " class="promo-link f-20 f-md-22 w400">Download Now 🎁</a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #5 End -->

      <!-- CTA Button Section Start -->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                     There is no coupon for this  as its  a <span class="pink-clr w700">"premium psychology weapon"</span>  product
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="https://jvz6.com/c/10103/391284/" class="text-center bonusbuy-btn">
                  <span class="text-center">Get Vidamaze  Bundle  + Get Vidamaze AI Pro Pro free</span>
                  </a>
               </div>
                  <!-- <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 orange-clr">"AMITVIP"</span> for an Additional <span class="w700 orange-clr"> Discount</span> on Commercial Licence
                  </div> -->
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Get Vidamaze  + Get Vidamaze AI Pro Pro free</span>
                  </a>
               </div>
               <div class="col-12 mt15 mt-md40">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 smmltd">Days</span>
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Sec</span>
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->

      <div class="container mt20 mt-md50">
         <div class="row">
            <div class="col-12 col-md-12 text-center">
               <div class="f-md-40 f-28 lh140 w700 white-clr feature-shape">
                  VIP KIT 
               </div>
            </div>
         </div>
      </div>

      <!-- Bonus #6 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS #6</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="assets/images/bonus6.webp" class="img-fluid mx-auto d-block " alt="Bonus6">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        DEAL CLOSING SECRETS
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           Video series to teach viewers how to close deals with offline or online businesses.
                           <br><br>
                          <span class="w600">Topics Covered:</span><br><br> 
                           <li>Objections</li>
                           <li>Three Things They Want</li>
                           <li>The Method</li>
                           <li>Method Expanded</li>
                           <li>Go Narrow</li>
                        </ul>
                        <div class="download-btn">
                           <a href="https://drive.google.com/file/d/1BbcjE896v9Dr4y8xreAu2H98WkzuLJiv/view?usp=sharing" target="_blank " class="promo-link f-20 f-md-22 w400">Download Now 🎁</a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #6 End -->
      
      <!-- Bonus #7 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS #7</div>
                  </div>
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md50">        
               <div class="col-md-5 col-12">
                  <img src="assets/images/bonus7.webp" class="img-fluid mx-auto d-block " alt="Bonus7">
               </div>
               <div class="col-md-7 col-12 mt20 mt-md0">
                  <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                  HOW TO SPY ON LOCAL COMPETITION
                  </div>
                  <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                     Get ahead of the competition by knowing exactly what they are doing that makes them better.
                     <br><br>
                     If you have been struggling to improve your rankings and wonder why your competition always seems to outrank you, there are resources available that will unmask their strengths and weaknesses.
                     <br><br>
                     In this article, we will look at how to do this with Quantcast.
                  </ul>
                  <div class="download-btn">
                     <a href="https://drive.google.com/file/d/16hREwlm1gxtpfnRnxuSBPXaV_ieLep6A/view?usp=sharing" target="_blank " class="promo-link f-20 f-md-22 w400">Download Now 🎁</a>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #7 End -->
      <!-- Bonus #8 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS #8</div>
                  </div>
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md50">
               <div class="col-md-5 col-12 order-md-2">
                  <img src="assets/images/bonus8.webp" class="img-fluid mx-auto d-block " alt="Bonus8">
               </div>
               <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                  <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                     LOCAL LEAD SCRIBE V3
                  </div>
                  <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                     Use stunning and Professional-looking Whiteboard Animated Videos to convert your leads!
                     <br><br>
                     <span class="w600">You get videos in the following niches:</span> <br><br>
                     <li>Business Security Consultants</li>
                     <li>Chiropractors</li>
                     <li>Electricians</li>
                     <li>Handyman</li>
                     <li>Plumbers</li>
                     <li>Mechanics</li>
                  </ul>
                  <div class="download-btn">
                     <a href="https://drive.google.com/file/d/1HFpYfx1VsCpryG2pPZFaXWMw3z1EWsfs/view?usp=sharing" target="_blank " class="promo-link f-20 f-md-22 w400">Download Now 🎁</a>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #8 End -->
      <!-- Bonus #9 Start -->
      <div class="section-bonus">
         <div class="container">  
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS #9</div>
                  </div>
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md50">
               <div class="col-md-5 col-12">
                  <img src="assets/images/bonus9.webp" class="img-fluid mx-auto d-block " alt="Bonus9">
               </div>
               <div class="col-md-7 col-12 mt20 mt-md0">
                  <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                  SOCIAL MESSAGING APP FOR MARKETERS
                  </div>
                  <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                    
                     Every business knows the importance of social media for its marketing campaigns.
                     <br><br>
                     Social media is only one tool that the digital age provides us with when it comes to reaching large audiences and building deeper relationships with them.
                     <br><br>
                     Knowing how to use WhatsApp, Facebook Messenger, Skype, Instagram, and iMessage for your business is your biggest advantage.
                     
                     <div class="download-btn">
                        <a href="https://drive.google.com/file/d/1sxeyEOocAbcZOUDBBzqh3eLwcOq5DZQg/view?usp=sharing" target="_blank " class="promo-link f-20 f-md-22 w400 mt20">Download Now 🎁</a>
                     </div>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #9 End -->
      <!-- Bonus #10 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS #10</div>
                  </div>
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md50">
               <div class="col-md-5 col-12 order-md-2">
                  <img src="assets/images/bonus10.webp" class="img-fluid mx-auto d-block " alt="Bonus10">
               </div>
               <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                  <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                     LIST BUILDING ON A BUDGET
                  </div>
                  <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                     Creating a mailing list is one of the very best strategies ANYONE can use in order to increase engagement with your followers and of course – make more sales.
                     <br><br>
                     So, we are giving you a comprehensive guide to everything you need to know about building a huge mailing list on a budget. This is your first step toward building your own digital empire.
                  </ul>
                  <div class="download-btn">
                     <a href="https://drive.google.com/file/d/1VAT_Kq8KNl48RdKFZEZQx6H-YPKoRg_x/view?usp=sharing" target="_blank " class="promo-link f-20 f-md-22 w400">Download Now 🎁</a>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #10 End -->

      <!-- Huge Woth Section Start -->
      <div class="huge-area ">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="f-md-65 f-40 lh120 w700 white-clr">That's Huge Worth of</div>
                  <br class="d-none d-md-block">
                  <div class="f-md-60 f-40 lh120 w800 white-clr">$3300!</div>
               </div>
            </div>
         </div>
      </div>
      <!-- Huge Worth Section End -->
      <!-- text Area Start -->
      <div class="white-section pb0">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="f-md-36 f-25 lh140 w500">So what are you waiting for? You have a great opportunity <br class="hidden-xs"> ahead + <span class="w700 ">My 20 Bonus Products</span> are making it a <br class="hidden-xs"> <span class="w700 ">completely NO Brainer!!</span></div>
               </div>
            </div>
         </div>
      </div>
      <!-- text Area End -->
      <!-- CTA Button Section Start -->
      <div class="cta-btn-section">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center">
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 black-clr mt-md30">
                  There is no coupon for this  as its  a <span class="w700 orange-clr">"premium psychology weapon"</span>   product
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="https://jvz6.com/c/10103/391284/" class="text-center bonusbuy-btn">
                  <span class="text-center">Get Vidamaze  + Get Vidamaze AI Pro Pro free</span>
                  </a>
               </div>
               <!-- <div class="f-16 f-md-20 lh150 w400 text-center mt20 black-clr">
                     Use Coupon Code <span class="w700 orange-clr">"AMITVIP"</span> for an Additional <span class="w700 orange-clr"> Discount</span> on Commercial Licence
                  </div> -->
                  <a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn mt15 mt-md20">
                  <span class="text-center">Get Vidamaze  Bundle  + Get Vidamaze AI Pro Pro free</span> 
                  </a>
               </div>
               <div class="col-12 mt15 mt-md20" align="center">
                  <h3 class="f-md-28 f-20 w500 text-center black">My Exclusive Bonuses Are Expiring in...</h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 col-md-10 mx-auto col-12 text-center">
                  <div class="countdown counter-black">
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                     <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                     <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->
      <div class="footer-section" style="font-size: 14px;">
         <div class="container " style="font-size: 14px;">
            <div class="row" style="font-size: 14px;">
               <div class="col-12 text-center">
                  <img src="assets/images/vidmaze-logo.png" alt="vidamaze-logo" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 text-center" style="font-size: 14px;">
                  <div class="f-16 f-md-18 w300 mt20 lh150 white-clr text-center" style="font-size: 16px;">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40" style="font-size: 14px;">
                  <div class="f-16 f-md-18 w300 lh150 white-clr text-xs-center" style="font-size: 16px;">Copyright © Vidamaze 2023</div>
                  <ul class="footer-ul f-16 f-md-18 w300 white-clr text-center text-md-right">
                     <li style="font-size: 16px;"><a href="https://support.oppyo.com/hc/en-us" class="white-clr t-decoration-none" style="font-size: 16px;">Contact</a> <span class="px5" style="font-size: 16px;">|</span> </li>
                     <li style="font-size: 16px;"><a href="http://vidamaze.com/legal/privacy-policy.html" class="white-clr t-decoration-none" style="font-size: 16px;">Privacy</a> <span class="px5" style="font-size: 16px;">|</span> </li>
                     <li style="font-size: 16px;"><a href="http://vidamaze.com/legal/terms-of-service.html" class="white-clr t-decoration-none" style="font-size: 16px;">T&amp;C</a> <span class="px5" style="font-size: 16px;">|</span> </li>
                     <li style="font-size: 16px;"><a href="http://vidamaze.com/legal/disclaimer.html" class="white-clr t-decoration-none" style="font-size: 16px;">Disclaimer</a> <span class="px5" style="font-size: 16px;">|</span> </li>
                     <li style="font-size: 16px;"><a href="http://vidamaze.com/legal/gdpr.html" class="white-clr t-decoration-none" style="font-size: 16px;">GDPR</a> <span class="px5" style="font-size: 16px;">|</span> </li>
                     <li style="font-size: 16px;"><a href="http://vidamaze.com/legal/dmca.html" class="white-clr t-decoration-none" style="font-size: 16px;">DMCA</a> <span class="px5" style="font-size: 16px;">|</span> </li>
                     <li style="font-size: 16px;"><a href="http://vidamaze.com/legal/anti-spam.html" class="white-clr t-decoration-none" style="font-size: 16px;">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!-- timer --->
      <?php
         if ($now < $exp_date) {
         ?>
      <script type="text/javascript">
         // Count down milliseconds = server_end - server_now = client_end - client_now
         var server_end = <?php echo $exp_date; ?> * 1000;
         var server_now = <?php echo time(); ?> * 1000;
         var client_now = new Date().getTime();
         var end = server_end - server_now + client_now; // this is the real end time
         
         var noob = $('.countdown').length;
         
         var _second = 1000;
         var _minute = _second * 60;
         var _hour = _minute * 60;
         var _day = _hour * 24
         var timer;
         
         function showRemaining() {
         var now = new Date();
         var distance = end - now;
         if (distance < 0) {
         	clearInterval(timer);
         	document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
         	return;
         }
         
         var days = Math.floor(distance / _day);
         var hours = Math.floor((distance % _day) / _hour);
         var minutes = Math.floor((distance % _hour) / _minute);
         var seconds = Math.floor((distance % _minute) / _second);
         if (days < 10) {
         	days = "0" + days;
         }
         if (hours < 10) {
         	hours = "0" + hours;
         }
         if (minutes < 10) {
         	minutes = "0" + minutes;
         }
         if (seconds < 10) {
         	seconds = "0" + seconds;
         }
         var i;
         var countdown = document.getElementsByClassName('countdown');
         for (i = 0; i < noob; i++) {
         	countdown[i].innerHTML = '';
         
         	if (days) {
         		countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + days + '</span><span class="f-14 f-md-18 smmltd">Days</span> </div>';
         	}
         
         	countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + hours + '</span><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>';
         
         	countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">' + minutes + '</span><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>';
         
         	countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">' + seconds + '</span><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>';
         }
         
         }
         timer = setInterval(showRemaining, 1000);
         	
      </script>
      <?php
         } else {
         echo "Times Up";
         }
         ?>
      <!--- timer end-->
   </body>
</html>
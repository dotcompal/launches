<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <!-- Tell the browser to be responsive to screen width -->
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
      <meta name="title" content="ContentReel Bonuses">
      <meta name="description" content="ContentReel Bonuses">
      <meta name="keywords" content="Have A Sneak Peak of A Breakthrough Technology That Analyzes, Detects & Fix Deadly Legal Flaws on Your Website Just by Copy Pasting a Single Line of Code">
      <meta property="og:image" content="https://www.contentreel.co/special-bonus/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Dr. Amit Pareek">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="ContentReel Bonuses">
      <meta property="og:description" content="Have A Sneak Peak of A Breakthrough Technology That Analyzes, Detects & Fix Deadly Legal Flaws on Your Website Just by Copy Pasting a Single Line of Code">
      <meta property="og:image" content="https://www.contentreel.co/special-bonus/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="ContentReel Bonuses">
      <meta property="twitter:description" content="Have A Sneak Peak of A Breakthrough Technology That Analyzes, Detects & Fix Deadly Legal Flaws on Your Website Just by Copy Pasting a Single Line of Code">
      <meta property="twitter:image" content="https://www.contentreel.co/special-bonus/thumbnail.png">
      <title>ContentReel Bonuses</title>
      <!-- Shortcut Icon  -->
      <link rel="icon" href="assets/images/favicon.png" type="image/png">
      <!-- Css CDN Load Link -->
      <link rel="stylesheet" type="text/css" href="assets/css/bonus-style.css">
      
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&family=Lexend:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
      <!-- required -->
      <!-- Start Editor required -->
      <link rel="stylesheet" href="https://cdn.oppyo.com/launches/yoseller/common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style.css" type="text/css">
      <script src="https://cdn.oppyo.com/launches/writerarc/common_assets/js/jquery.min.js"></script>
      <script type="text/javascript" src="https://www.legelsuites.com/widget/ada/oppyo-Im7S-MNbL-QN75-NLLR" crossorigin="anonymous" embed-id="oppyo-Im7S-MNbL-QN75-NLLR"></script>
   </head>
   <body>
      <!-- New Timer  Start-->
      <?php
         $date = 'Jan 18 2023 11:00 PM EST';
         $exp_date = strtotime($date);
         $now = time();  
         /*
         
         $date = date('F d Y g:i:s A eO');
         $rand_time_add = rand(700, 1200);
         $exp_date = strtotime($date) + $rand_time_add;
         $now = time();*/
         
         if ($now < $exp_date) {
         ?>
      <?php
         } else {
         	echo "Times Up";
         }
         ?>
      <!-- New Timer End -->
      <?php
         if(!isset($_GET['afflink'])){
         $_GET['afflink'] = ' https://jvz1.com/c/10103/390951/';
         $_GET['name'] = 'Dr. Amit Pareek';      
         }
         ?>
         
     <!-- Header Section Start -->   
     <div class="main-header">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <img src="assets/images/contentreel-logo.png" alt=" contentreel-logo" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-md-12 col-12 text-center mt30 mt-md50">
                  <div class="text-center">
                     <div class="f-md-32 f-20 lh140 w400 white-clr d-block d-md-flex align-items-center justify-content-center">
                        <span class="w600 orange-clr"><?php echo $_GET['name'];?>'s</span> &nbsp;special bonus for ContentReel 
                     </div>
                  </div>
               </div>
            
         
         <div class="row mt20">
               <div class="col-12  text-center">
                  <div class="f-16 f-md-20 w700 orange-clr lh140 text-uppercase preheadline">                       
                     Grab Content Reel with  Content Reel Podium FREE + Exclusive bonuses 
                  </div>
               </div>
               <div class="col-12 mt-md30 mt20 f-md-42 f-26 w700 text-center white-clr lh140">
                  AI Assistant Lets You Dominate ANY Niche & Create Hundreds of Micro Content Videos In Minutes Using A Single Keyword!
               </div>
              
               <div class="col-12 mt20 mt-md30 text-center">
                  <div class="f-18 f-md-20 w500 lh140 white-clr text-capitalize">               
                  AI Will AUTOMATICALLY Select The Topics, Write The Script, & Convert To an Animated Video FOR YOU. Just Enter A Keyword & Hit Go!
                  <br> <br>
                  Enter a Keyword, Select Number Of Videos To Create & Click GO!
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md40">
               <div class="col-md-10 col-12 mx-auto">
                  <!-- <img src="assets/images/product-image.webp" class="img-fluid d-block mx-auto"> -->
                  <div class="col-12 responsive-video">
                     <iframe src="https://player.vimeo.com/video/782035442?h=ae41785836&title=0&byline=0&portrait=0%22" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                  </div>
               </div>
            </div>
         </div>
      </div>
      </div>

      <!-- Header Section End -->
      
      <!-- CTA Btn Start-->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                  Use Coupon Code <span class="w700 orange-clr">"AMITBUNDLE"</span> for an Additional <span class="w700 orange-clr"> Discount</span> on Bundle
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="https://jvz8.com/c/10103/390968/" class="text-center bonusbuy-btn">
                  <span class="text-center">Get Content Reel Bundle + Get Content Reel Podium Free</span>
                  </a>
               </div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 orange-clr">"AMITVIP"</span> for an Additional <span class="w700 orange-clr"> Discount</span> on Commercial Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Get Content Reel + Get Content Reel Podium Free</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 smmltd">Days</span>
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Sec</span>
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Btn End -->


           <!-------WriterArc Starts----------->
      <!-------Exclusive Bonus----------->
      <div class="exclusive-bonus">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                  <div class="white-clr f-24 f-md-36 lh140 w600">
                     Exclusive Bonus #1 : Coursesify 
                  </div>
                  <div class="f-18 f-md-20 w500 mt20 white-clr">
                     Create Unlimited High Quality Video Content On Any Niche Or Topic By Using <span class="black-clr w700">Content Reel</span> & Sell On Pre-Built Plateform <span class="black-clr w700"> Coursesify </span> & Earn Unlimited.
                  </div>
               </div>
            </div>
         </div>
      </div>

      <div class="coursesify-header-section">
         <div class="container">
            <div class="row">
               <div class="col-12 d-flex align-items-center justify-content-center flex-wrap flex-md-nowrap">
                  <svg id="Layer_1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 135.23 36" style="height:55px;"><defs><style>.cls-1,.cls-2{fill:#fff;}.cls-3,.cls-4{fill:#5e2bff;}.cls-2,.cls-4,.cls-5,.cls-6{fill-rule:evenodd;}.cls-5{fill:#958aff;}.cls-6{fill:#7861ff;}</style></defs><path class="cls-1" d="M40.05,28.2c-.9-.5-1.61-1.21-2.12-2.12s-.77-1.97-.77-3.17,.26-2.25,.79-3.17c.53-.91,1.25-1.62,2.16-2.12,.91-.5,1.94-.75,3.06-.75s2.15,.25,3.06,.75,1.64,1.21,2.16,2.12c.53,.92,.79,1.97,.79,3.17s-.27,2.25-.81,3.17-1.27,1.62-2.2,2.12c-.92,.5-1.95,.75-3.09,.75s-2.14-.25-3.04-.75h0Zm4.55-2.22c.47-.26,.85-.65,1.13-1.17,.28-.52,.42-1.15,.42-1.9,0-1.11-.29-1.97-.88-2.57-.58-.6-1.3-.9-2.14-.9s-1.55,.3-2.12,.9-.86,1.45-.86,2.57,.28,1.97,.83,2.57c.56,.6,1.26,.9,2.1,.9,.53,0,1.04-.13,1.51-.39h.01Z"></path><path class="cls-1" d="M62.41,17.06v7.77s.15,4.08-5.57,4.08c-.92,0-2.61-.19-3.31-.58s-1.26-.96-1.66-1.71c-.4-.75-.6-1.65-.6-2.69v-6.86h2.96v6.44c0,.93,.23,1.64,.7,2.14,.46,.5,1.1,.75,1.9,.75s1.46-.25,1.92-.75,.7-1.21,.7-2.14v-6.44h2.96Z"></path><path class="cls-1" d="M69.77,17.42c.61-.35,1.31-.53,2.1-.53v3.1h-.78c-.93,0-1.63,.22-2.1,.65-.47,.44-.71,1.2-.71,2.28v5.83h-2.96v-11.7h2.96v1.82c.38-.62,.88-1.1,1.49-1.46h0Z"></path><path class="cls-1" d="M75.75,28.43c-.76-.34-1.36-.81-1.81-1.4-.44-.59-.69-1.25-.73-1.96h2.98c.06,.45,.28,.82,.67,1.12,.39,.3,.87,.44,1.45,.44s1-.11,1.32-.34c.32-.22,.48-.51,.48-.87,0-.38-.19-.67-.58-.86s-1-.4-1.85-.62c-.87-.21-1.59-.43-2.14-.65-.56-.22-1.03-.57-1.44-1.03s-.6-1.09-.6-1.88c0-.65,.19-1.24,.56-1.77,.37-.53,.91-.96,1.6-1.27,.7-.31,1.52-.46,2.46-.46,1.39,0,2.51,.35,3.34,1.05,.83,.7,1.29,1.64,1.37,2.82h-2.83c-.04-.46-.24-.83-.58-1.11-.35-.27-.81-.41-1.38-.41-.54,0-.95,.1-1.24,.3-.29,.2-.43,.47-.43,.82,0,.39,.2,.69,.59,.9,.39,.2,1.01,.41,1.84,.62,.84,.21,1.54,.43,2.09,.65,.55,.23,1.02,.57,1.43,1.05,.4,.47,.61,1.09,.62,1.87,0,.68-.19,1.28-.56,1.82-.37,.54-.91,.95-1.6,1.26-.7,.3-1.51,.45-2.44,.45s-1.82-.17-2.58-.52v-.02Z"></path><path class="cls-1" d="M96.15,23.79h-8.55c.07,.84,.37,1.51,.89,1.98,.52,.48,1.16,.72,1.92,.72,1.1,0,1.88-.47,2.34-1.41h3.19c-.34,1.13-.99,2.05-1.94,2.78-.96,.73-2.13,1.09-3.53,1.09-1.13,0-2.14-.25-3.03-.75s-1.59-1.21-2.09-2.12-.75-1.97-.75-3.17,.25-2.27,.74-3.19c.49-.91,1.18-1.62,2.07-2.11s1.91-.74,3.06-.74,2.11,.24,2.99,.72,1.56,1.16,2.05,2.04c.49,.88,.73,1.89,.73,3.03,0,.42-.03,.8-.08,1.14h0Zm-2.98-1.99c-.01-.76-.29-1.37-.82-1.83-.54-.46-1.19-.69-1.96-.69-.73,0-1.35,.22-1.85,.66s-.81,1.06-.92,1.85h5.55Z"></path><path class="cls-1" d="M100.29,28.43c-.76-.34-1.36-.81-1.81-1.4-.44-.59-.69-1.25-.73-1.96h2.98c.06,.45,.28,.82,.67,1.12,.39,.3,.87,.44,1.45,.44s1-.11,1.32-.34c.32-.22,.47-.51,.47-.87,0-.38-.19-.67-.58-.86s-1-.4-1.85-.62c-.87-.21-1.59-.43-2.14-.65-.56-.22-1.03-.57-1.44-1.03-.4-.46-.6-1.09-.6-1.88,0-.65,.19-1.24,.56-1.77,.37-.53,.91-.96,1.6-1.27,.7-.31,1.52-.46,2.46-.46,1.39,0,2.51,.35,3.34,1.05,.83,.7,1.29,1.64,1.37,2.82h-2.83c-.04-.46-.24-.83-.58-1.11-.35-.27-.81-.41-1.38-.41-.54,0-.95,.1-1.24,.3-.29,.2-.43,.47-.43,.82,0,.39,.2,.69,.59,.9,.39,.2,1.01,.41,1.84,.62,.84,.21,1.54,.43,2.09,.65,.55,.23,1.02,.57,1.43,1.05,.4,.47,.61,1.09,.62,1.87,0,.68-.19,1.28-.56,1.82-.37,.54-.91,.95-1.6,1.26-.7,.3-1.51,.45-2.44,.45s-1.82-.17-2.58-.52v-.02Z"></path><path class="cls-1" d="M110.09,15.17c-.34-.33-.52-.74-.52-1.24s.17-.9,.52-1.24c.34-.33,.78-.5,1.3-.5s.95,.17,1.3,.5c.34,.33,.52,.74,.52,1.24s-.17,.9-.52,1.24c-.35,.33-.78,.5-1.3,.5s-.95-.17-1.3-.5Zm2.76,1.89v11.7h-2.96v-11.7s2.96,0,2.96,0Z"></path><path class="cls-3" d="M124.98,16.12h-5.9v12.63h-3v-12.63h-1.33v-2.43h1.33v-.59c0-1.44,.41-2.49,1.22-3.17,.82-.68,2.05-.99,3.69-.95v2.49c-.72-.01-1.22,.11-1.5,.36s-.42,.71-.42,1.37v.49h5.9v2.43h0Z"></path><path class="cls-1" d="M134.23,17.06l-7.24,17.23h-3.15l2.53-5.83-4.69-11.4h3.31l3.02,8.17,3.06-8.17h3.16Z"></path><polygon class="cls-5" points="8.66 17.9 9.8 25.1 7.24 18.44 8.66 17.9"></polygon><polygon class="cls-6" points="8.98 15.43 11.07 15.1 10.54 25.27 8.98 15.43"></polygon><polygon class="cls-4" points="14.57 12.98 11.24 25.43 11.9 12.84 14.57 12.98"></polygon><path class="cls-2" d="M27.86,20.55s-2.54,7.7-9.85,7.7c-2.34,0-4.5-.8-6.22-2.14l4.1-15.32,11.97,9.75h0Z"></path><path class="cls-3" d="M29.73,19.39l-.15,.79c-.9,4.75-4.54,8.43-9.29,9.37-1.48,.29-2.98,.3-4.47,.02-3.09-.58-5.77-2.34-7.54-4.94-1.78-2.6-2.43-5.74-1.85-8.82,.9-4.75,4.54-8.43,9.29-9.37,1.48-.29,2.98-.3,4.47-.02l.79,.15,.97-5.12-.79-.15c-2.17-.41-4.34-.4-6.48,.02-2.24,.45-4.34,1.33-6.25,2.64C4.67,6.52,2.14,10.39,1.3,14.85c-.84,4.46,.1,8.98,2.66,12.73,2.56,3.75,6.42,6.28,10.88,7.12,1.06,.2,2.13,.3,3.19,.3s2.2-.11,3.29-.32h0c2.24-.45,4.34-1.33,6.25-2.64,3.75-2.56,6.28-6.42,7.12-10.88l.15-.79-5.12-.97h0Z"></path></svg>
               </div>
               <div class="col-12 text-center lh150 mt20 mt-md50">
                  <div class="pre-heading f-16 f-md-18 w700 lh160 white-clr">
                  Setup A Profitable Academy Once &amp; Get Paid Forever without... <br class="d-none d-md-block"> Being on Camera or Writing A Single Word.
                    </div>
               </div>
               <div class="col-12 mt-md35 mt20 f-md-40 f-28 w700 text-center white-clr lh150">
                 <span class="yellow-clr">World’s FIRST Super Academy Builder </span> Lets Anyone Create UDEMY &amp; COURSERA Like Sites Preloaded with 400+ RED-HOT Video Courses, E-Books &amp; Reports in Just 7 Minutes…   
               </div>
               <div class="col-12 mt-md25 mt20">
                  <div class="f-20 f-md-22 w500 text-center lh150 white-clr2">
                   Welcome The Future of E-Learning with Cutting-Edge Features Like In-Built Selling System, Course Builder, Progress Bar, Ratings &amp; Certificates. 100% Beginner Friendly
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md50">
               <div class="col-md-7 col-12 min-md-video-width-left">
                  <!-- <img src="assets/images/product-box.webp" class="img-fluid d-block mx-auto"> -->
                  <div class="col-12 responsive-video border-video">
                  <iframe src="https://coursesify.dotcompal.com/video/embed/rb1d7ll1t8" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                  </div>
               </div>
               <div class="col-md-5 col-12 min-md-video-width-right mt20 mt-md0">
                  <ul class="list-head pl0 m0 f-16 f-md-18 lh160 w400 white-clr">
                     <li>Launch Your Beautiful Academy Site with <span class="w600"> Marketplace, Blog &amp; Members Area</span> </li>
                     <li> <span class="w600">Get Started Immediately with 400+ Done-For-You Courses, E-Books &amp; Reports</span> </li>
                     <li> <span class="w600">Create New Courses on Any Topic–</span> Add Unlimited Lessons, Videos and E-books</li>
                     <li> <span class="w600">Embrace Future of Learning with</span> Course Progress Bar, Review &amp; Ratings, and Course Completion Certificates</li>
                     <li> <span class="w600">Accept Payments Directly </span> with PayPal, Stripe, JVZoo, ClickBank, WarriorPlus Etc</li>
                     <li> <span class="w600">Have Unlimited Earning Potential –</span> Teach Anything or Everything Quick &amp; Easy</li>
                     <li> <span class="w600">Keep 100% Profits </span> No Traffic, Leads, or Profit Sharing</li>
                     <li> <span class="w600">In-Built Ready to Use Affiliate System</span> to Boost your Sales &amp; Profits</li>
                     <li> <span class="w600">100% SEO &amp; Mobile Responsive</span> Academy Site &amp; Marketplace</li>
                     <li> <span class="w600">FREE SSL Certificate, Domain &amp; Hosting</span>  for Your Academy Sites</li>
                     <li> <span class="w600">FREE UPGRADE -</span>  COMMERCIAL LICENCE IF YOU ACT TODAY</li>
                  </ul>
               </div>
            </div>
         </div>
      </div>

      <div class="gr-1">
         <div class="container-fluid p0">
            <picture>
               <source media="(min-width:768px)" srcset="assets/images/coursesify-steps.webp">
               <source media="(min-width:320px)" srcset="assets/images/coursesify-mview-steps.webp" style="width:100%" class="img-fluid mx-auto">
               <img src="assets/images/coursesify-steps.webp" alt="Coursesify Steps" class="img-fluid" style="width: 100%;">
            </picture>
         </div>
      </div>

      <!-- WriterArc End -->
      

      <!-- Bonus Section Header Start -->
      <div class="bonus-header">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-10 mx-auto heading-bg text-center">
                  <div class="f-24 f-md-36 lh140 w700"> When You Purchase ContentReel, You Also Get <br class="hidden-xs"> Instant Access To These 20 Exclusive Bonuses</div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus Section Header End -->
          <!-- Bonus Section Header End -->
          <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">Super Vip Kit 1</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus1.webp" class="img-fluid mx-auto d-block" alt="Bonus1">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Video Marketing Profit Kit
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <b>This course delivers the following crucial information:</b>
                           <li class="mt20">How to pick the right type of video to create </li>
                           <li>How to save money on video creation</li> 
                           <li>How to turbocharge the persuasive power of your marketing videos</li> 
                           <li>How to make money from your videos</li> 
                           <li>How to promote your videos the right way</li> 
                           <li>And much more!</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #1 Section End -->
      <!-- Bonus #2 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">Super Vip Kit 2</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30">
                  <div class="row">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="assets/images/bonus2.webp" class="img-fluid mx-auto d-block" alt="Bonus2">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md0 text-capitalize">
                        Express VidTraffic App
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li class="w600">Create motion animations and add animations to your videos with Express VidTraffic App.</li>
                           <li>Express VidTraffic App is an easy to use animation software that gives you the ability to animate shapes, text and imported images. Express Animate is simple and easy-to-use.</li>
                           <li>Express VidTraffic App is a powerful one-of-it’s kind animation software that lets you animate anything in your videos. There’s a lot you can do with this software such as add text, shapes, and more.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #2 Section End -->
      <!-- Bonus #3 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">Super Vip Kit 3</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus3.webp" class="img-fluid mx-auto d-block " alt="Bonus3">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Traffic Engine
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                          <li class="w600">Traffic Engine is designed to boost the website traffic of any online businesses passively. It is an all-in-one tool for search engine optimization, pay-per-click monitoring and general market research.</li>

                           <li>Traffic Engine is the ultimate traffic consultancy tool. It’s packed with a page analysis feature that examines crucial SEO parts of any website and returns an SEO "score" for the page.</li>

                           <li>It also instantly gives you suggestions on improving your SEO performance, generates a report of where your site is ranking in various keywords, while also keeping track of your previous positions. </li>

                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #3 Section End -->
      <!-- Bonus #4 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">Super Vip Kit 4</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="assets/images/bonus4.webp" class="img-fluid mx-auto d-block " alt="Bonus4">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        AnimaStudio
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li class="w600">AnimaStudio is compact image-editing software packed with tools for cropping, resizing, correcting colors, applying presets and effects, as well as drawing with a brush or pen.</li>

                           <li>AnimaStudio is a comprehensive graphic design package that can support layers and lets you create Web graphics, icons, images, animations, and much more.</li>

                           <li>It works perfectly for professional artists, designers, and programmers, but is also designed with layman users with any level of experience. It lets you work on and produce images in file formats such as GIF, JPEG, BMP, ICO, TGA, and AVI. </li>

                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #4 End -->
      <!-- Bonus #5 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">Super Vip Kit 5</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus5.webp" class="img-fluid mx-auto d-block " alt="Bonus5">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        AgencyManager
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>Enquiry/Lead. Track all your inquiries for various products, sources of your leads, and analyze sales performance in one glance. You can save all your customer contacts and organize them quickly for future use.</li>

                           <li>Follow-ups. Record the complete details of your lead so that your follow-ups are targeted and high-converting. You can log the mode of follow-up, date and time of follow-up, brief about the communication done, next follow-up details and so on. AgencyManager also gives you a bird’s eye view of the entire history of your lead’s follow-up.</li>

                           <li>Closure. Closing deals is a breeze with AgencyManager. It enables you to monitor and track overall sales performance, track the status of lead/inquiry, and record the status of your deal.</li>

                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #5 End -->
      <!-- Bonus #6 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">Super Vip Kit 6</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="assets/images/bonus6.webp" class="img-fluid mx-auto d-block " alt="Bonus4">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        InvoicePRO
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li class="w600">Organize, generate, and manage all your clients’ invoices in one powerful dashboard with InvoicePRO.</li>

                          <li>InvoicePRO lets you perform wide-ranging accounting-oriented tasks. You can create customized invoices, integrate various devices, and conveniently include multiple tax rates so you and your clients can leave the guesswork out of complicated financial tasks.</li> 

                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #6 End -->
      <!-- CTA Button Section Start -->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                  Use Coupon Code <span class="w700 orange-clr">"AMITBUNDLE"</span> for an Additional <span class="w700 orange-clr"> Discount</span> on Bundle
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="https://jvz8.com/c/10103/390968/" class="text-center bonusbuy-btn">
                  <span class="text-center">Get Content Reel Bundle + Get Content Reel Podium Free</span>
                  </a>
               </div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 orange-clr">"AMITVIP"</span> for an Additional <span class="w700 orange-clr"> Discount</span> on Commercial Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Get Content Reel + Get Content Reel Podium Free</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 smmltd">Days</span>
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Sec</span>
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->
      <!-- Bonus #6 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row align-items-center">        
               <div class="col-md-5 col-12 order-md-2">
                  <img src="assets/images/bonus7.webp" class="img-fluid mx-auto d-block " alt="Bonus6">
               </div>
               <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">Prime Bonus 1</div>
                  </div>
                  <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize  mt20 mt-md30">
                     Resellers Rights to VidRankNeos
                  </div>
                  <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                     <li>
                     Get Unlimited Traffic with Page #1 Ranking! With VidRankNeos You Are Guaranteed To Rank Your Videos And Once You’ve Done That, You Can Easy Track Your Rankings.
                     </li>
                     <li>
                     With Rank Tracker and Spy feature you can not only track your videos ranking and current positions but you can also spy on your competitors and see where they are ranking and what they are doing to keep ranking.
                     </li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #6 End -->
      <!-- Bonus #7 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-5 col-12">
                  <img src="assets/images/bonus8.webp" class="img-fluid mx-auto d-block " alt="Bonus7">
               </div>
               <div class="col-md-7 col-12 mt20 mt-md0">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">Prime Bonus 2</div>
                  </div>
                  <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize mt20 mt-md30">
                  Resellers Rights to VidNeos
                  </div>
                  <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                     <li>
                     VidNeos takes hundreds of thousands of dollars worth of real life, real business insights from real videos and real sales...and packs it all into a one-stop-shop software... Research, Create, Analyse, Optimise & Backlink (video and website) - Do EVERYTHING from one place.
                     </li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #7 End -->
      <!-- Bonus #8 Start -->
      <div class="section-bonus">
         <div class="container">  
            <div class="row align-items-center">
               <div class="col-md-5 order-md-2 col-12">
                  <img src="assets/images/bonus9.webp" class="img-fluid mx-auto d-block " alt="Bonus8">
               </div>
               <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">Prime Bonus 3</div>
                  </div>
                  <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize mt20 mt-md30">
                  Resellers Rights to WPDollar3 Pro
                  </div>
                  <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                     <li>
                        WP Dollar 3.0 is THE most powerful store builder on the market for Amazon. Go from Amazon Newbie to Autopilot Cash in 9.7 Minutes or less with this Revolutionary new Wordpress Plugin. Create full amazon stores on autopilot...no tech skills needed.
                     </li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #8 End -->
      <!-- Bonus #9 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-5 col-12">
                  <img src="assets/images/bonus10.webp" class="img-fluid mx-auto d-block " alt="Bonus9">
               </div>
               <div class="col-md-7 col-12 mt20 mt-md0">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">Prime Bonus 4</div>
                  </div>
                  <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize mt20 mt-md30">
                  Resellers Rights to SocialNeos Pro
                  </div>
                  <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                     <li>
                     SocialNeos rewards customers for sharing content, builds your list and creates a powerful Social Notification system just like Mobile Push Notifications - message your users any time, on any device!It's The Only Viral Traffic Plugin You'll Ever Need Turn Any Video or Content into a List Building Machine in 3 Easy Steps.
                     </li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #9 End -->
      <!-- Bonus #10 start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus11.webp" class="img-fluid mx-auto d-block " alt="Bonus10">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="bonus-title-bg">
                           <div class="f-22 f-md-28 lh120 w700">Prime Bonus 5</div>
                        </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize mt20 mt-md30">
                        Resellers Rights to AKBooster 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>
                           AK Booster Pro can help You Gain a Massive Advantage over your Competitors... And Mazimize Your Profits in a snap... Find profitable, easy to rank, Kindle niches in minute and promote your book to thousand of hungry buyers to rake in loads of traffic.
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #10 End -->
      <div class="section-bonus">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-5 col-12">
                  <img src="assets/images/bonus12.webp" class="img-fluid mx-auto d-block " alt="Bonus13">
               </div>
               <div class="col-md-7 col-12 mt20 mt-md0">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">Prime Bonus 6</div>
                  </div>
                  <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize mt20 mt-md30">
                  Resellers Rights to Upto 100 Ready-Made Niche Blogs
                  </div>
                  <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                     <li>
                     100 ready-made blogs, packed with all the content you’ll need to attract FREE search engine traffic, backlinks, visitors and buyers… Each blog comes wrapped inside a beautiful, high converting blog design and designed to work in perfect harmony with WP Dollar 3.
                     </li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #11 End -->
      <!-- Bonus #12 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-5 order-md-2 col-12">
                  <img src="assets/images/bonus13.webp" class="img-fluid mx-auto d-block " alt="Bonus12">
               </div>
               <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">Prime Bonus 7</div>
                  </div>
                  <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize mt20 mt-md30">
                  Resellers Rights to VidAgency WP Theme
                  </div>
                  <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                     <li>VidAgency Theme is one of its kind WordPress theme that is plug-n-play video agency website setup theme.All you need to do is hit the install button and the theme will do the rest. In fact, you don’t even have to create or write content yourself!</li>
                  </ul>
               </div>
            </div>    
         </div>
      </div>
      <!-- Bonus #12 End -->
      <!-- CTA Button Section Start -->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                  Use Coupon Code <span class="w700 orange-clr">"AMITBUNDLE"</span> for an Additional <span class="w700 orange-clr"> Discount</span> on Bundle
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="https://jvz8.com/c/10103/390968/" class="text-center bonusbuy-btn">
                  <span class="text-center">Get Content Reel Bundle + Get Content Reel Podium Free</span>
                  </a>
               </div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 orange-clr">"AMITVIP"</span> for an Additional <span class="w700 orange-clr"> Discount</span> on Commercial Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Get Content Reel + Get Content Reel Podium Free</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 smmltd">Days</span>
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Sec</span>
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->
   <!-- Bonus #11 start -->
   
      <!-- Bonus #13 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-5 col-12">
                  <img src="assets/images/bonus14.webp" class="img-fluid mx-auto d-block " alt="Bonus13">
               </div>
               <div class="col-md-7 col-12 mt20 mt-md0">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">Premium Bonus 1</div>
                  </div>
                  <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize mt20 mt-md30">
                  Whitelabel License to AppSpyPro
                  </div>
                  <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                     <li>AppSpyPro helps you find the most popular niches and keywords in Apple iOS store, Google Android Play Store and other mobile app stores...</li>
                  </ul>
               </div>
            </div>      
         </div>
      </div>
      <!-- Bonus #13 End -->
      <!-- Bonus #14 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-5 order-md-2 col-12">
                  <img src="assets/images/bonus15.webp" class="img-fluid mx-auto d-block " alt="Bonus15">
               </div>
               <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">Premium Bonus 2</div>
                  </div>
                  <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize mt20 mt-md30">
                  Whitelabel License to Adsense Dragon
                  </div>
                  <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                     <li>The AdSense Dragon is a detailed, step-by-step blueprint of the exact system used to dominate Adsense.</li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #14 End -->
      <!-- Bonus #15 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-5 col-12">
                  <img src="assets/images/bonus16.webp" class="img-fluid mx-auto d-block " alt="Bonus15">
               </div>
               <div class="col-md-7 col-12 mt20 mt-md0">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">Premium Bonus 3</div>
                  </div>
                  <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize mt20 mt-md30">
                  Whitelabel License to VideoAppMonarchy Theme + Plugin
                  </div>
                  <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                     <li>Video App Monarchy is a WP plugin that actually produces results for anyone who is struggling to generate traffic.</li> 
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #15 End -->
      <!-- Bonus #16 start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-5 order-md-2 col-12">
                  <img src="assets/images/bonus17.webp" class="img-fluid mx-auto d-block " alt="Bonus16">
               </div>
               <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">Premium Bonus 4</div>
                  </div>
                  <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize mt20 mt-md30">
                  Whitelabel License to VidNeos AutoVideo Theme
                  </div>
                  <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                     <li>VIDNEOS THEME takes all the guesswork out of Video Blogging and does all the heavy lifting for you, while making you tons of money from the massive video traffic that your blog will attract.. Create UNLIMITED Content Rich, Self-Updating & Auto-Traffic Video Blogs, In Minutes Without Ever Creating a Single Video!</li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #16 End -->
      <!-- Bonus #17 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-5 col-12">
                  <img src="assets/images/bonus18.webp" class="img-fluid mx-auto d-block " alt="Bonus17">
               </div>
               <div class="col-md-7 col-12 mt20 mt-md0">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">Premium Bonus 5</div>
                  </div>
                  <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize mt20 mt-md30">
                  Whitelabel License to SocialNeos Theme
                  </div>
                  <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                     <li>...Build Unlimited Beautiful Viral Video Blogs In Any Niche Using The Intelligent NeosTheme... ALL on Autopilot Without EVER Creating a Single Video!....</li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #17 End -->
      <!-- Bonus #18 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-5 order-md-2 col-12">
                  <img src="assets/images/bonus19.webp" class="img-fluid mx-auto d-block " alt="Bonus18">
               </div>
               <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">Premium Bonus 6</div>
                  </div>
                  <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize mt20 mt-md30">
                  Whitelabel License to CurationNeos Desktop App
                  </div>
                  <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                     <li>CurationNeos allows you to turn other people’s red hot content into your own unique, traffic-getting content in seconds! In just seconds, you can create your own unique, curated content that you can use to rake in daily free traffic from social media, Google, in your emails, and more!</li>
                  </ul>
               </div>      
            </div>
         </div>
      </div>
      <!-- Bonus #18 End -->
      <!-- Bonus #19 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row align-items-center">  
               <div class="col-md-5 col-12">
                  <img src="assets/images/bonus20.webp" class="img-fluid mx-auto d-block " alt="Bonus19">
               </div>
               <div class="col-md-7 col-12 mt20 mt-md0">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">Premium Bonus 7</div>
                  </div>
                  <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize mt20 mt-md30">
                  Whitelabel License to Pin Matrix Pro
                  </div>
                  <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                     <li>Pin Matrix pro is an Fully Automated Adobe Air Software Automates Pinterest Tasks . Mutlipe image pining, Setting Delays and automating the pins on different niches boards to dive niches targeted traffic</li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #19 End -->
      <!-- Bonus #20 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-6 col-12">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">Premium Bonus 8</div>
                  </div>
                  <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize mt20">
                     Website Builder
                  </div>
                  <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                     <li>EZSite Ninja is an interactive drag and drop website builder with pre-loaded templates in various niches. Your customers can now create beautiful websites in minutes without paying for expensive website builders. They get lifetime access to our website builder.</li> 
                  </ul>
               </div>
               <div class="col-md-6 col-12 mt20 mt-md0">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">Premium Bonus 9</div>
                  </div>
                  <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize mt20">
                  Link shortener
                  </div>
                  <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                     <li>A short link is a powerful marketing tool when you use it carefully. It is not just a link but a medium between your customer and their destination. A short link allows you to collect so much data about your customers and their behaviors.</li> 
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #20 End -->
      
      <!-- CTA Button Section Start -->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                  Use Coupon Code <span class="w700 orange-clr">"AMITBUNDLE"</span> for an Additional <span class="w700 orange-clr"> Discount</span> on Bundle
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="https://jvz8.com/c/10103/390968/" class="text-center bonusbuy-btn">
                  <span class="text-center">Get Content Reel Bundle + Get Content Reel Podium Free</span>
                  </a>
               </div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 orange-clr">"AMITVIP"</span> for an Additional <span class="w700 orange-clr"> Discount</span> on Commercial Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Get Content Reel + Get Content Reel Podium Free</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 smmltd">Days</span>
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Sec</span>
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->

      
      <div class="section-bonus">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-5 col-12">
                  <img src="assets/images/bonus21.webp" class="img-fluid mx-auto d-block " alt="Bonus16">
               </div>
               <div class="col-md-7 col-12 mt20 mt-md0">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">LifeTime Bonus 1</div>
                  </div>
                  <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize mt20 mt-md30">
                  Lifetime Access to CurationNeos WebApp
                  </div>
                  <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                     <li>Search, find, edit, and curate other people’s red hot content into your own unique, traffic- getting content in seconds.!</li>
                  </ul>
               </div>
            </div>
         </div>
      </div>

      <div class="section-bonus">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-5 order-md-2 col-12">
                  <img src="assets/images/bonus22.webp" class="img-fluid mx-auto d-block " alt="Bonus17">
               </div>
               <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">LifeTime Bonus 2</div>
                  </div>
                  <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize mt20 mt-md30">
                  Lifetime Access to Video Sales Authority
                  </div>
                  <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                     <li>Premium training (exclusive to customers), designed from the ground up to take you by the hand, and teach you how to copy my entire business marketing strategy. It is a multi-step system that leverages psychological "progressive agreements", to turn your business into a powerhouse.</li>
                  </ul>
               </div>
            </div>
         </div>
      </div>

      <div class="section-bonus">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-5 col-12">
                  <img src="assets/images/bonus23.webp" class="img-fluid mx-auto d-block " alt="Bonus16">
               </div>
               <div class="col-md-7 col-12 mt20 mt-md0">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">LifeTime Bonus 3</div>
                  </div>
                  <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize mt20 mt-md30">
                  Lifetime Access to Independent Authors Academy
                  </div>
                  <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                     <li>Independent Author's Academy is the "Lite Version" of the upgrade we just offered you... with a big price drop that will make you go "Ooo..." with temptation!</li>
                  </ul>
               </div>
            </div>
         </div>
      </div>

      <div class="section-bonus">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-5 order-md-2 col-12">
                  <img src="assets/images/bonus24.webp" class="img-fluid mx-auto d-block " alt="Bonus17">
               </div>
               <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">LifeTime Bonus 4</div>
                  </div>
                  <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize mt20 mt-md30">
                  Lifetime Access to VidRank Training
                  </div>
                  <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                     <li>A premium training designed from the ground up to take you by the hand and teach you how to copy an entire successful business marketing strategy to earn loads of profits.</li>
                  </ul>
               </div>
            </div>
         </div>
      </div>

      <!-- Huge Woth Section Start -->
      <div class="huge-area ">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="f-md-65 f-40 lh120 w700 white-clr">That's Huge Worth of</div>
                  <br>
                  <div class="f-md-60 f-40 lh120 w800 white-clr">$3300!</div>
               </div>
            </div>
         </div>
      </div>
      <!-- Huge Worth Section End -->
      <!-- text Area Start -->
      <div class="white-section pb0">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="f-md-36 f-25 lh140 w500">So what are you waiting for? You have a great opportunity <br class="hidden-xs"> ahead + <span class="w700 ">My 20 Bonus Products</span> are making it a <br class="hidden-xs"> <span class="w700 ">completely NO Brainer!!</span></div>
               </div>
            </div>
         </div>
      </div>
      <!-- text Area End -->
      <!-- CTA Button Section Start -->
      <div class="cta-btn-section">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center">
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 black-clr mt-md30">
                  Use Coupon Code <span class="w700 orange-clr">"AMITBUNDLE"</span> for an Additional <span class="w700 orange-clr"> Discount</span> on Bundle
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href=" https://jvz3.com/c/10103/390812/" class="text-center bonusbuy-btn">
                  <span class="text-center">Get Content Reel Bundle + Get Content Reel Podium Free</span>
                  </a>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 black-clr">
                     Use Coupon Code <span class="w700 orange-clr">"AMITVIP"</span> for an Additional <span class="w700 orange-clr"> Discount</span> on Commercial Licence
                  </div>
                  <a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn mt15 mt-md20">
                  <span class="text-center">Get Content Reel + Get Content Reel Podium Free</span> 
                  </a>
               </div>
               <div class="col-12 mt15 mt-md20" align="center">
                  <h3 class="f-md-28 f-20 w500 text-center black">My Exclusive Bonuses Are Expiring in...</h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 col-md-10 mx-auto col-12 text-center">
                  <div class="countdown counter-black">
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                     <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                     <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->
      <div class="footer-section" style="font-size: 14px;">
         <div class="container " style="font-size: 14px;">
            <div class="row" style="font-size: 14px;">
               <div class="col-12 text-center">
                  <img src="assets/images/contentreel-logo.png" alt=" contentreel-logo" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 text-center" style="font-size: 14px;">
                  <div class="f-16 f-md-18 w300 mt20 lh150 white-clr text-center" style="font-size: 16px;">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40" style="font-size: 14px;">
                  <div class="f-16 f-md-18 w300 lh150 white-clr text-xs-center" style="font-size: 16px;">Copyright © ContentReel 2023</div>
                  <ul class="footer-ul f-16 f-md-18 w300 white-clr text-center text-md-right">
                     <li style="font-size: 16px;"><a href="https://support.oppyo.com/hc/en-us" class="white-clr t-decoration-none" style="font-size: 16px;">Contact</a> <span class="px5" style="font-size: 16px;">|</span> </li>
                     <li style="font-size: 16px;"><a href="http://contentreel.co/legal/privacy-policy.html" class="white-clr t-decoration-none" style="font-size: 16px;">Privacy</a> <span class="px5" style="font-size: 16px;">|</span> </li>
                     <li style="font-size: 16px;"><a href="http://contentreel.co/legal/terms-of-service.html" class="white-clr t-decoration-none" style="font-size: 16px;">T&amp;C</a> <span class="px5" style="font-size: 16px;">|</span> </li>
                     <li style="font-size: 16px;"><a href="http://contentreel.co/legal/disclaimer.html" class="white-clr t-decoration-none" style="font-size: 16px;">Disclaimer</a> <span class="px5" style="font-size: 16px;">|</span> </li>
                     <li style="font-size: 16px;"><a href="http://contentreel.co/legal/gdpr.html" class="white-clr t-decoration-none" style="font-size: 16px;">GDPR</a> <span class="px5" style="font-size: 16px;">|</span> </li>
                     <li style="font-size: 16px;"><a href="http://contentreel.co/legal/dmca.html" class="white-clr t-decoration-none" style="font-size: 16px;">DMCA</a> <span class="px5" style="font-size: 16px;">|</span> </li>
                     <li style="font-size: 16px;"><a href="http://contentreel.co/legal/anti-spam.html" class="white-clr t-decoration-none" style="font-size: 16px;">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!-- timer --->
      <?php
         if ($now < $exp_date) {
         ?>
      <script type="text/javascript">
         // Count down milliseconds = server_end - server_now = client_end - client_now
         var server_end = <?php echo $exp_date; ?> * 1000;
         var server_now = <?php echo time(); ?> * 1000;
         var client_now = new Date().getTime();
         var end = server_end - server_now + client_now; // this is the real end time
         
         var noob = $('.countdown').length;
         
         var _second = 1000;
         var _minute = _second * 60;
         var _hour = _minute * 60;
         var _day = _hour * 24
         var timer;
         
         function showRemaining() {
         var now = new Date();
         var distance = end - now;
         if (distance < 0) {
         	clearInterval(timer);
         	document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
         	return;
         }
         
         var days = Math.floor(distance / _day);
         var hours = Math.floor((distance % _day) / _hour);
         var minutes = Math.floor((distance % _hour) / _minute);
         var seconds = Math.floor((distance % _minute) / _second);
         if (days < 10) {
         	days = "0" + days;
         }
         if (hours < 10) {
         	hours = "0" + hours;
         }
         if (minutes < 10) {
         	minutes = "0" + minutes;
         }
         if (seconds < 10) {
         	seconds = "0" + seconds;
         }
         var i;
         var countdown = document.getElementsByClassName('countdown');
         for (i = 0; i < noob; i++) {
         	countdown[i].innerHTML = '';
         
         	if (days) {
         		countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + days + '</span><span class="f-14 f-md-18 smmltd">Days</span> </div>';
         	}
         
         	countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + hours + '</span><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>';
         
         	countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">' + minutes + '</span><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>';
         
         	countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">' + seconds + '</span><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>';
         }
         
         }
         timer = setInterval(showRemaining, 1000);
         	
      </script>
      <?php
         } else {
         echo "Times Up";
         }
         ?>
      <!--- timer end-->
   </body>
</html>
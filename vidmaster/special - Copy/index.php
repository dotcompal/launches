<!Doctype html>
<html>
   <head>
      <title>VidMaster - Creates & Publish YouTube Shorts in Just 60 Seconds…</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=9">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
      <meta name="title" content="VidMaster Special">
      <meta name="description" content="Game-Changer AI APP Creates Stories, Reels, Boomerang, & Short Videos With Just One Keyword… For Floods Of FREE Traffic And Sales">
      <meta name="keywords" content="VidMaster">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta property="og:image" content="https://www.vidmaster.co/special/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Ayush Jain">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="VidMaster Special">
      <meta property="og:description" content="Game-Changer AI APP Creates Stories, Reels, Boomerang, & Short Videos With Just One Keyword… For Floods Of FREE Traffic And Sales">
      <meta property="og:image" content="https://www.vidmaster.co/special/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="VidMaster Special">
      <meta property="twitter:description" content="Game-Changer AI APP Creates Stories, Reels, Boomerang, & Short Videos With Just One Keyword… For Floods Of FREE Traffic And Sales">
      <meta property="twitter:image" content="https://www.vidmaster.co/special/thumbnail.png">
      <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
      <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
      <link rel="stylesheet" href="../common_assets/css/bootstrap.min.css"
         type="text/css">
      <link rel="stylesheet" href="assets/css/style.css" type="text/css">
      <link rel="stylesheet" href="assets/css/timer.css" type="text/css">
      <script src="../common_assets/js/jquery.min.js"></script>
   <script src="../common_assets/js/bootstrap.min.js"></script>

   <script>
                        (function(w, i, d, g, e, t, s) {
                            if(window.businessDomain != undefined){
                                console.log("Your page have duplicate embed code. Please check it.");
                                return false;
                            }
                            businessDomain = 'vidmaster';
                            allowedDomain = 'www.vidmaster.co';
                            if(!window.location.hostname.includes(allowedDomain)){
                                console.log("Your page have not authorized. Please check it.");
                                return false;
                            }
                            console.log("Your script is ready...");
                            w[d] = w[d] || [];
                            t = i.createElement(g);
                            t.async = 1;
                            t.src = e;
                            s = i.getElementsByTagName(g)[0];
                            s.parentNode.insertBefore(t, s);
                        })(window, document, '_gscq', 'script', 'https://cdn.staticdcp.com/apps/engage/smart_engage/js/config-loader.js');</script>

   </head>
   <body>
            <div style="background-color: #ffffffc7; padding: 30px 0px 30px 0px; height: 100px; width: 100%; position: fixed; top: 0; z-index: 1000;">
               <div  style="font-size: 20px; color:black; text-align:center; font-weight:500">
                  Game-Changer AI APP Creates Stories, Reels, Boomerang, & Short Videos with <span class="red-clr">Just One Keyword…</span> for Floods of FREE Traffic and Sales
               </div>
            </div>
           
      <!-- Header Section Start -->
      <div class="header-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center mt20">
                  <div class="pre-heading f-20 f-md-24 w500 lh140 white-clr">
                     Encash the Latest TRAFFIC Trend of 2023…
                  </div>
               </div>
               <div class="col-12 mt-md50 mt30 f-md-45 f-28 w700 text-center white-clr lh140 text-capitalize">
                  Game-Changer AI APP Creates Stories, Reels, Boomerang, & Short Videos with <span class="red-clr">Just One Keyword…</span> for Floods of FREE Traffic and Sales
               </div>
               <div class="col-12 mt-md25 mt20 f-16 f-md-20 w400 text-center lh140 white-clr text-capitalize">
               Even A Complete Beginner Can Drive Thousands of Visitors to Any Offer or Link  Instantly <br> |  No Camera | No Writing & Editing | No Paid Traffic Ever…
               </div>
            </div>
            <div class="row d-flex align-items-center flex-wrap mt20 mt-md40">
               <div class="col-md-7 col-12 min-md-video-width-left">
                  <!-- <img src="assets/images/product-image.webp" class="img-fluid mx-auto d-block"> -->
                  <div class="col-12 responsive-video border-video">
                     <iframe src="https://vidmaster.dotcompal.com/video/embed/8x6aokyyl7"
                        style="width:100%; height:100%" frameborder="0" allow="fullscreen"
                        allowfullscreen=""></iframe>
                     </div>
                  <div class="col-12 mt40 d-none d-md-block">
                     <div class="f-20 f-md-28 w700 lh140 text-center white-clr">
                        Bonus Agency License Included for Time!
                     </div>
                     <div class="f-18 f-md-20 w400 white-clr lh140 mt10">
                        Use Coupon Code <span class="red-clr w600">"VidMaster"</span>  for an Additional <span class="red-clr w600"> $3 Discount</span>
                     </div>
                     <div class="row">
                        <div class="col-md-11 mx-auto col-12 mt20 mt-md20 text-center">
                           <a href="#buynow" class="cta-link-btn d-block px0">Get Instant Access To VidMaster</a>
                        </div>
                     </div>
                     <div class="col-12 col-md-10 mx-auto d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                        <img src="assets/images/compaitable-with1.webp"
                           class="img-fluid mx-xs-center md-img-right" alt="visa">
                        <div class="d-md-block d-none visible-md px-md30"><img
                           src="assets/images/v-line.webp" class="img-fluid"
                           alt="line">
                        </div>
                        <img src="assets/images/days-gurantee1.webp"
                           class="img-fluid mx-xs-auto mt15 mt-md0" alt="30 days">
                     </div>
                     <div class="col-12 col-md-8 mx-auto mt30 d-none d-md-block">
                        <img src="assets/images/limited-time.webp" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
               <div class="col-md-5 col-12 mt20 mt-md0 min-md-video-width-right">
                  <div class="calendar-wrap mb20 mb-md0">
                     <ul class="list-head pl0 m0 f-18 lh140 w400 white-clr">
                     <li>Drive Tsunami of FREE Traffic in Any Niche</li>
                     <li>Create Reels & Shorts Using Just One Keyword </li>
                     <li>Create Boomerang Video to Engage Audience</li>
                     <li>Get Thousands of Visitors & Sales to Any Offer or Page</li>
                     <li>Make Tons of Affiliate Commissions or Ad Profits</li> 
                     <li>Create Short Videos from Any Video, Text, or Just a Keyword</li>
                     <li>Add Background Music and/or Voiceover to Any Video</li>
                     <li>Pre-designed 50+ Shorts Video templates</li>
                     <li>25+ Vector Images to Choose From</li>
                     <li>100+ Social Sharing Platforms ForFREE Viral Traffic</li>
                     <li>100% Newbie Friendly, No Prior Experience Or Tech Skills Needed</li>
                     <li>No Camera Recording, No Voice, Or Complex Editing Required</li>
                     <li>Free Commercial License To Sell Video Services For High Profits</li>
                     </ul>
                  </div>
                  <div class="d-md-none">
                     <div class="f-18 f-md-26 w700 lh140 text-center white-clr">
                        Free Commercial License + Low 1-Time Price For Launch Period Only 
                     </div>
                     <div class="row">
                        <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center">
                           <a href="#buynow" class="cta-link-btn d-block px0">Get Instant Access To VidMaster</a>
                        </div>
                     </div>
                     <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                        <img src="assets/images/compaitable-with1.webp"
                           class="img-responsive mx-xs-center md-img-right" alt="visa">
                        <div class="d-md-block d-none visible-md px-md30"><img
                           src="assets/images/v-line.webp" class="img-fluid"
                           alt="line">
                        </div>
                        <img src="assets/images/days-gurantee1.webp"
                           class="img-responsive mx-xs-auto mt15 mt-md0" alt="30 days">
                     </div>
                     <div class="col-12 col-md-8 mx-auto mt30 d-md-none">
                        <img src="assets/images/limited-time.webp" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Header Section End -->

      <!-- Step Section -->
      <div class="step-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-39 f-28 w600 text-center black-clr2 lh140">
                  Create Stunning Stories, Reels & Shorts for Massive Traffic To Any Offer, Page, or Link <span class="red-clr w700">in Just 3 Easy Steps</span>
               </div>
            </div>
            <div class="row mt20 mt-md50">
               <div class="col-12 col-md-4">
                  <div class="step-block">
                     <div class="step-title f-20 f-md-22 w700">Step 1</div>
                     <img src="assets/images/step-1.webp" class="img-fluid d-block mx-auto mt15 mt-md30">
                     <div class="step-content">
                        <div class="f-22 f-md-28 w600 black-clr2 mt10">Choose Platform</div>
                        <div class="w400 f-18 f-md-20 lh140 mt10 black-clr2">
                           Choose the social platform- YouTube, Instagram, TikTok, Facebook, or Snapchat you want to create Reel or Shorts for
                        </div>
                     </div>  
                  </div>
               </div>
               <div class="col-12 col-md-4 mt20 mt-md0">
                  <div class="step-block">
                     <div class="step-title f-20 f-md-22 w700">Step 2</div>
                     <img src="assets/images/step-2.webp" class="img-fluid d-block mx-auto mt15 mt-md30">
                     <div class="step-content">
                        <div class="f-22 f-md-28 w600 black-clr2 mt10">Enter a Keyword </div>
                        <div class="w400 f-18 f-md-20 lh140 mt10 black-clr2">
                           Enter the desired Keyword & you will get several short videos. Select any video & customize it by adding background, vectors, audio, & fonts to make it engaging
                        </div>
                     </div>  
                  </div>
               </div>
               <div class="col-12 col-md-4 mt20 mt-md0">
                  <div class="step-block">
                     <div class="step-title f-20 f-md-22 w700">Step 3</div>
                     <img src="assets/images/step-3.webp" class="img-fluid d-block mx-auto mt15 mt-md30">
                     <div class="step-content">
                        <div class="f-22 f-md-28 w600 black-clr2 mt10">Publish and Profit</div>
                        <div class="w400 f-18 f-md-20 lh140 mt10 black-clr2">
                           Now publish your Reels & Shortson different social platforms with a click to drive unlimited viral traffic & sales on your offer, page, or website.
                        </div>
                     </div>  
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md70">
               <div class="col-12 text-center">
                  <div class="f-20 f-md-24 w600 lh140">
                     No Download/Installation  |  No Prior Knowledge  |  100% Beginners Friendly
                  </div>
                  <div class="f-18 f-md-20 w400 mt15 mt-md30 lh140">
                     In just 60 seconds, you can create your first profitable Reels & Shorts to enjoy MASSIVE FREE Traffic, Sales & Profits coming into your bank account on autopilot.
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Step Section End -->

      <!-- Proof Section Start -->
      <div class="proof-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-40 f-28 w700 lh140 text-center black-clr2">
                  Got 34,309 Targeted Visitors in Last 30 Days…                
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
                  <img src="assets/images/proof.webp" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 mt20 mt-md50 f-md-40 f-28 w700 lh140 text-center black-clr2">   
                  And Making Consistent $535 In Profits <br class="d-none d-md-block"> Each & Every Day        
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
                  <img src="assets/images/proof1.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>
         </div>
      </div>
      <!-- Proof Section End -->

      <!-- Testimonials Section -->
      <div class="testimonial-section">
         <div class="container ">
            <div class="row ">
               <div class="f-md-44 f-28 w700 lh140 text-center black-clr2">
                  Checkout What VidMaster Early Users Have to SAY
               </div>
            </div>
            <div class="row mt25 mt-md50 align-items-center">
               <div class="col-12 col-md-10">
                  <div class="testi-block">
                     <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-12 col-md-3 order-md-2">
                           <img src="assets/images/c1.webp" class="img-fluid d-block mx-auto">
                           <div class="f-22 f-md-24 w700 lh120 red-clr mt20 text-center">Benjamin Davis</div>
                        </div>
                        <div class="col-12 col-md-9 order-md-1 mt20 mt-md0 text-center text-md-start">
                           <div class="f-22 f-md-28 w700 lh140">I got 2500+ Views & 509 Subscribers in 4 days</div>
                           <div class="f-20 w400 lh140 mt20 quote1">
                              Hi, I am from Australia and promote weight loss products on ClickBank & Amazon as an affiliate. I was struggling to get traffic on my offers and my YouTube channels have only 257 subscribers the reason is that I could not create and post Videos and Shorts regularly as it takes lots of effort and time. But recently, I got access to VidMaster, I created around 15 Short Videos with almost no effort, that was amazingly EASY. And <span class="w700">I got 2500+ Views & 509 Subscribers in 4 days</span> on my weight loss channel. Kudos to the team for creating a solution that can make the complete process fast & easy. 
                           </div> 
                         </div>
                     </div>
                 </div>
               </div>
               <div class="col-12 col-md-10 offset-md-2 mt20 mt-md40">
                  <div class="testi-block">
                     <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-12 col-md-3">
                           <img src="assets/images/c2.webp" class="img-fluid d-block mx-auto">
                           <div class="f-22 f-md-24 w700 red-clr mt20 text-center">Liam Smith</div>
                        </div>
                        <div class="col-12 col-md-9 mt20 mt-md0 text-center text-md-start">
                           <div class="f-22 f-md-28 w700 lh140">Best part about VidMaster is that it takes away all hassles</div>
                           <div class="f-20 w400 lh140 mt20 quote1">
                              Thanks for the early access, VidMaster Team. <span class="w700">The best part about VidMaster is that it takes away</span> all hassles like video recording or editing, voiceover recording, background music and driving traffic to your offers, etc. I am sure this will give you complete value for your money and help you get the desired success in the long run.
                           </div>  
                        </div>
                     </div>
                 </div>
               </div>
               <div class="col-12 col-md-10 mt20 mt-md40">
                  <div class="testi-block">
                     <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-12 col-md-3 order-md-2">
                           <img src="assets/images/c3.webp" class="img-fluid d-block mx-auto">
                           <div class="f-22 f-md-24 w700 lh120 red-clr mt20 text-center">Noah Brown</div>
                        </div>
                        <div class="col-12 col-md-9 order-md-1 mt20 mt-md0 text-center text-md-start">
                           <div class="f-22 f-md-28 w700 lh140">Create super engaging video shorts for YouTube</div>
                           <div class="f-20 w400 lh140 mt20 quote1">
                              VidMaster is the perfect platform to <span class="w700">create super engaging video shorts for YouTube and social media and drive tons of viral traffic in a stress-free manner.</span> I am enjoying all the functionalities of this amazing software. If someone asks me to give marks, I would love to give ten-on-ten to this fabulous product.
                           </div> 
                        </div>
                     </div>
                 </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Testimonials Section End -->

      <!-- CTA Section-->
      <div class="cta-section-new">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-18 f-md-28 w700 lh140 text-center white-clr">
                     Free Commercial License + Low 1-Time Price For Launch Period Only
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 text-center mt5 white-clr">
                     Use Discount Coupon <span class="red-clr w600"> "vidmaster" </span> for Instant <span class="red-clr w600"> $3 OFF </span>
                 </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 mt-md30 text-center">
                        <a href="#buynow" class="cta-link-btn">Get Instant Access To VidMaster</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                     <img src="assets/images/compaitable-with1.webp" class="img-fluid mx-xs-center md-img-right" alt="visa">
                     <div class="d-md-block d-none visible-md px-md30">
                        <img src="assets/images/v-line.webp" class="img-fluid" alt="line">
                     </div>
                     <img src="assets/images/days-gurantee1.webp" class="img-fluid mx-xs-auto mt15 mt-md0" alt="30 days">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- CTA Section End-->

      <!-- Reels Section Start -->
      <section class="reel-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-45 w700 lh140 black-clr2">
                     Checkout Some of Reels & Shorts <br class="d-none d-md-block">
                     Videos Created Using VidMaster
                  </div>
               </div>
            </div>
            <div class="row row-cols-md-3 row-cols-xl-3 row-cols-1  mt-md50">
               <div class="col mt20 mt-md100">
               <video width="100%" height="auto" loop autoplay muted="muted" class="mp4-border" controls>
                   <source src="assets/images/reel1.mp4" type="video/mp4">
               </video>
               </div>
               <div class="col mt20 mt-md0">
               <video width="100%" height="auto" loop autoplay muted="muted" class="mp4-border" controls>
                   <source src="assets/images/reel2.mp4" type="video/mp4">
               </video>
               </div>
               <div class="col mt20 mt-md60">
               <video width="100%" height="auto" loop autoplay muted="muted" class="mp4-border" controls>
                   <source src="assets/images/reel3.mp4" type="video/mp4">
               </video>
               </div>
               <div class="col mt20 mt-md30">
               <video width="100%" height="auto" loop autoplay muted="muted" class="mp4-border" controls>
                   <source src="assets/images/reel4.mp4" type="video/mp4">
               </video>
               </div>
               <div class="col mt20">
               <video width="100%" height="auto" loop autoplay muted="muted" class="mt-md-90 mp4-border" controls>
                   <source src="assets/images/reel5.mp4" type="video/mp4">
               </video>
               </div>
               <div class="col mt20 mt-md40">
               <video width="100%" height="auto" loop autoplay muted="muted" class="mt-md-50 mp4-border" controls>
                   <source src="assets/images/reel6.mp4" type="video/mp4">
               </video>
               </div>
            </div>
         </div>
      </section>
      <!-- Reels Section End -->

      <!-- Ground-Breaking Section Start -->
      <div class="ground-breaking">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-40 f-28 w700 text-center black-clr2 lh140">
                  VidMaster Is Packed with Tons of Cool Features
               </div>
            </div>
            <div class="row row-cols-md-3 row-cols-xl-3 row-cols-1">
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat1.webp" alt="Create Niche based short video" class="img-fluid">
                     <div class="sep-line">
                     </div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Create Niche based short video</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat2.webp" alt="Create Video from Text" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">
                        Create Video from Text
                     </div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat3.webp" alt="Create Video Using Video Clips" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Create Video Using Video Clips</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat4.webp" alt="Create Video from Images" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Create Video from Images</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat5.webp" alt="Create Boomerang Video" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Create Boomerang Video</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat6.webp" alt="Create Podcasts" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Create Podcasts</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat7.webp" alt="50+ Short Video Templates" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">50+ Short Video Templates</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat8.webp" alt="Add 25+ Vector Images" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Add 25+ Vector Images</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat9.webp" alt="Add Sound Waves in Video" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Add Sound Waves in Video</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat10.webp" alt="Add Voiceover to Any Video" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Add Voiceover to Any Video</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat11.webp" alt="Add Background Music to Any video" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Add Background Music to Any video</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat12.webp" alt="Create Voiceover in 150+ Voices in 30+ Languages" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Create Voiceover in 150+ Voices in 30+ Languages</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat13.webp" alt="Simple Video Editor" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Simple Video Editor</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat14.webp" alt="Add Logo &amp; Watermark" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Add Logo &amp; Watermark</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat15.webp" alt="Free Stock Images &amp; Videos" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Free Stock Images &amp; Videos</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat16.webp" alt="Multiple Video Quality (HD, 720p, 1080p, etc)" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Multiple Video Quality (HD, 720p, 1080p, etc)</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat17.webp" alt="Inbuilt Music Library" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Inbuilt Music Library</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat18.webp" alt="Built-in Biz Drive to Store &amp; Share Media" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Built-in Biz Drive to Store &amp; Share Media</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat19.webp" alt="Post Videos on YouTube" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Post Videos on YouTube</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat20.webp" alt=" 100+ Social Sharing Platforms" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500"> 100+ Social Sharing Platforms</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat21.webp" alt="Drive Unlimited Viral Traffic" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Drive Unlimited Viral Traffic</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat22.webp" alt="Capture Unlimited Leads" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Capture Unlimited Leads</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat23.webp" alt="100% Cloud-Based Software" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">100% Cloud-Based Software</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat24.webp" alt="100% Newbie Friendly" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">100% Newbie Friendly</div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Ground-Breaking Section Start -->

      <!-- CTA Section-->
      <div class="cta-section-new">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-18 f-md-28 w700 lh140 text-center white-clr">
                     Free Commercial License + Low 1-Time Price For Launch Period Only
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 text-center mt5 white-clr">
                     Use Discount Coupon <span class="red-clr w600"> "vidmaster" </span> for Instant <span class="red-clr w600"> $3 OFF </span>
                 </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 mt-md30 text-center">
                        <a href="#buynow" class="cta-link-btn">Get Instant Access To VidMaster</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                     <img src="assets/images/compaitable-with1.webp" class="img-fluid mx-xs-center md-img-right" alt="visa">
                     <div class="d-md-block d-none visible-md px-md30">
                        <img src="assets/images/v-line.webp" class="img-fluid" alt="line">
                     </div>
                     <img src="assets/images/days-gurantee1.webp" class="img-fluid mx-xs-auto mt15 mt-md0" alt="30 days">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- CTA Section End-->


      <!-- Billion Section Start-->
      <div class="billion-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-45 w600 white-clr lh140 heading-design">Did You Know ? YouTube Shorts Hit</div>
                  <div class="f-45 f-md-100 w600 black-clr2 mt20 lh140"> <span class="red-clr">30 Billion</span> View</div>
                  <div class="f-28 f-md-45 w600 lh140 black-clr2">Every Single Day!</div>
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-12 col-md-7">
                  <div class="f-18 f-md-24 w600 black-clr2 lh140">Yes, YouTube Shorts are Generating 30 Billion+ Views Per Day, A 4X Increase From This Time Last Year
                  </div>
                  <div class="f-18 f-md-24 w400 lh140 black-clr21 mt20">This clearly shows the trend is changing. Internet users prefer short videos to consume content instead of watching long videos like earlier…</div>
                  <div class="f-28 f-md-45 w600 lh140 red-clr mt20 mt-md50">No Doubt!</div>
                  <div class="f-20 f-md-24 w600 lh140 black-clr2 mt10">Reels & Shorts Are Present and Future of Video Marketing…</div>
               </div>
               <div class="col-12 col-md-5 mt20 mt-md0">
                  <img src="assets/images/girl.webp" class="img-fluid mx-auto d-block">
               </div>
            </div>
         </div>
         <div class="billion-wrap">
            <div class="container">
               <div class="row">
                  <div class="col-12 text-center">
                     <div class="f-28 f-md-45 w600 lh140 black-clr2">Here Are Some More Facts - <br class="d-none d-md-block">Why One Should Get Started with Reels & Shorts! </div>
                  </div>
               </div>
               <div class="row align-items-center mt20 mt-md80">
                  <div class="col-12 col-md-6">
                     <div class="f-20 f-md-24 w600 lh140 text-start black-clr2">YouTube has 2+ Bn Users & 70% of Videos are Viewed on Mobile</div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 ">
                     <img src="assets/images/facts1.webp" class="img-fluid d-block mx-auto">
                  </div>
               </div>
               <!-- <div class="row align-items-center mt20 mt-md80">
                  <div class="col-12 col-md-6 order-md-2">
                     <div class="f-20 f-md-24 w600 lh140 text-start black-clr2">YouTube Confirms Ads Are Coming to Shorts</div>
                     <div class="f-18 f-md-20 w400 lh140 mt10 text-start black-clr21"> Ads in Shorts would give creators in the YouTube Partner Program the ability to earn more revenue on the platform.
                     </div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
                     <img src="assets/images/facts2.webp" alt="YouTube Confirms Ads Are Coming to Shorts" class="mx-auto img-fluid d-block">
                  </div>
               </div> -->
               <div class="row align-items-center mt20 mt-md80">
                  <div class="col-12 col-md-6 order-md-2">
                     <div class="f-20 f-md-24 w600 lh140 text-start black-clr2">Instagram Reels have 2.5 Billion Monthly Active Users</div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
                     <img src="assets/images/facts3.webp" class="img-fluid d-block mx-auto" alt="Instagram Reels">
                  </div>
               </div>
               <div class="row align-items-center mt20 mt-md80">
                  <div class="col-12 col-md-6">
                     <div class="f-20 f-md-24 w600 lh140 text-start black-clr2">Tiktok has 50 Million Daily Active User in USA & 1 Bn Users Worldwide</div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0">
                     <img src="assets/images/facts4.webp" alt="Tiktok" class="mx-auto img-fluid d-block">
                  </div>
               </div>
               <div class="row align-items-center mt20 mt-md80">
                  <div class="col-12 col-md-6 order-md-2">
                     <div class="f-20 f-md-24 w600 lh140 text-start black-clr2">100 Million hours Daily watch Time by Facebook users.</div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
                     <img src="assets/images/facts5.webp" class="img-fluid d-block mx-auto" alt="Facebook Users">
                  </div>
               </div>
               <div class="row align-items-center mt20 mt-md80">
                  <div class="col-12 col-md-6">
                     <div class="f-20 f-md-24 w600 lh140 text-start black-clr2">Snapchat has over 360+ million daily active users</div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0">
                     <img src="assets/images/facts6.webp" alt="Website Analyser" class="mx-auto img-fluid d-block" alt="Snapchat Users">
                  </div>
               </div>
               <div class="row mt20 mt-md80">
                  <div class="col-12 text-center">
                     <div class="f-28 f-md-45 w600 lh140 ">So, Capitalising on Reels & Shorts in the Video Marketing Strategy of Any Business is the Need of The Hour.</div>
                     <div class="f-md-45 f-28 w600 lh140 mt20 mt-md30 black-clr2  border-cover">
                        <div class="f-22 f-md-26 w600 lh140 black-clr2">It Opens the Door to</div>
                        <div class=""> Tsunami <span class="red-clr"> of FREE </span>Traffic</div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Billion Section End -->

      <section class="prob-sec">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-20 f-md-32 w700 lh140 text-center black-clr2">
                     Now You’re Probably Wondering “Can Anyone Make a Full-Time Income With Short Videos?”                                  
                  </div>
                  <div class="f-20 f-md-32 w400 lh140 text-center mt15">
                  <span class="w700">Answer Is - Of Course, You Can!</span>   
                  </div>
               </div>
            </div>
         </div>
      </section>

      <!-- Power Section Start -->
      <div class="power-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-28 f-md-45 w700 lh140 text-center black-clr2">
                     See How People Are Making Crazy <br class="d-none d-md-block">
                     Money Using the POWER Of Reels & Short Videos
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md150">
                  <div class="power-shape">
                     <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-12 col-md-2">
                           <img src="assets/images/ryan.webp" class="img-fluid d-block mx-auto ryn-img">
                        </div>
                        <div class="col-12 col-md-10 mt30 mt-md0">
                           <div class="f-md-22 f-20 lh140 w400 text-center text-md-start pl-md60">
                              <span class="w700">Here’s “Ryan’s World” </span>– A 10-Year-Old Kid that has over <br class="d-none d-md-block"> 31.9M Subscribers with a whooping <span class="w700">Net Worth of $32M</span><br class="d-none d-md-block"> and growing… As he is now capitalizing on YouTube Shorts. <br><br>
                              Yes! You heard me right... He is just a 10-year-kid but making BIG.
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md150">
                  <div class="power-shape">
                     <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-12 col-md-2 order-md-2">
                           <img src="assets/images/huddakattan.webp" class="img-fluid d-block mx-auto ryn-img1">
                        </div>
                        <div class="col-12 col-md-10 mt30 mt-md0 order-md-1">
                           <div class="f-md-22 f-20 lh140 w400 text-center text-md-start pl-md30 pr-md40">
                              <span class="w700">“HuddaKattan” </span>is an American makeup artist, blogger & entrepreneur, Founder of cosmetic lines Huda Beauty on Instagram has over 51.8M Followers. She is using Instagram to interatct with here followers on daily basis.
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md150">
                  <div class="power-shape">
                     <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-12 col-md-2">
                           <img src="assets/images/khabane-lame.webp" class="img-fluid d-block mx-auto ryn-img">
                        </div>
                        <div class="col-12 col-md-10 mt30 mt-md0">
                           <div class="f-md-22 f-20 lh140 w400 text-center text-md-start pl-md60">
                           <span class="w700">“Khabane Lame”</span> is an Italian born social personality started uploading short video on dancing on Tiktok after laid off during Covid 19 and today he is most followed creator with 142.4M followers.
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md150">
                  <div class="power-shape">
                     <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-12 col-md-2 order-md-2">
                           <img src="assets/images/dj-khaled.webp" class="img-fluid d-block mx-auto ryn-img1">
                        </div>
                        <div class="col-12 col-md-10 mt30 mt-md0 order-md-1">
                           <div class="f-md-22 f-20 lh140 w400 text-center text-md-start pl-md60">
                              <span class="w700"> DJ Khaled known as  ‘King of Snapchat' </span> <br><br>
                              He is an American DJ, record executive, record producer and rapper. Now-a-days He teaches his audience by creating Reels that how anyone can get succeed on social media.
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row mt30 mt-md100">
               <div class="col-12 text-center">
                  <div class="but-design-border">
                     <div class="f-md-45 f-28 lh140 w900 white-clr button-shape">
                        Impressive right?
                     </div>
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md20">
               <div class="col-12 f-md-28 f-20 lh160 w400 text-center">
                  So, it can be safely stated that…<br>
                  <span class="f-22 f-md-32 w700 red-clr">Reels & Shorts Are The BIGGEST, Traffic and Income<br class="d-none d-md-block"> Opportunity Right Now! </span>
               </div>
            </div>
         </div>
      </div>
      <!-- Power Section End -->

      <!-- Affiliate Section Start -->
      <div class="affiliate-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-18 f-md-20 w400 lh140 black-clr2">
                  <span class="w700">The SECRET Is: </span><br><br>When you can combine selling top products, Reels & Shorts, and free viral traffic to make sales & profits, you’re the fastest way possible.
               </div>
               <div class="col-12 f-18 f-md-20 w500 lh140 black-clr2 mt20 mt-md50">
                  <img src="assets/images/fb-viral.webp" class="img-fluid d-block mx-auto" alt="Viral">
               </div>
               <div class="col-12 mt20 mt-md50">
                  <div class="f-md-40 f-24 w700 lh140 text-capitalize text-center black-clr2">
                     Checkout Free Traffic & Commissions <br class="d-none -md-block">
                     We’re Getting By Following This Proven System…
                  </div>
               </div>
               <div class="col-12 col-md-10 mt20 mt-md30 mx-auto">
                  <img src="assets/images/proof-02.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>
         </div>
      </div>
      <!-- Affiliate Section End -->

      <!-----Option Section Start------>
      <div class="option-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-24 f-md-45 w700 lh140 black-clr2 text-center">That’s A Real Deal But…</div>
                  <div class="f-28 f-md-65 w700 lh140 red-clr1 text-center">Here’s The Big Problem</div>
               </div>
               <div class="col-12 mt-md70 mt20 ">
                  <div class="row align-items-center">
                     <div class="col-md-6">
                        <div class="f-24 f-md-32 w700 red-clr lh140">
                           Creating Content Daily is Painful & Time Consuming!
                        </div>
                        <div class="f-18 w500 lh140 black-clr2 mt20">
                          <ul class="problem-list">
                           <li> You need to research, plan, and write content daily. And staying 
                           up-to-date with the latest niche trends needs lots of effort </li>

                           <li>You Need To Be On Camera. This can be a major blocker in the case 
                           you are shy or introverted and have a fear that people would judge 
                           and laugh at your videos.</li>                           
                          </ul>                        
                        </div>
                     </div>
                     <div class="col-md-6">
                        <img src="assets/images/opt1.webp" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
               <div class="col-12 mt-md70 mt20 ">
                  <div class="row align-items-center">
                     <div class="col-md-6 order-md-2">
                        <div class="f-24 f-md-32 w700 red-clr lh140">
                           Buying Expensive Equipment Can Leave Your Back Accounts Dry                        
                        </div>
                        <div class="f-18 w500 lh140 black-clr2 mt20">
                           To even get started with the first video, you need expensive equipment, like a<br><br>
                           <ul class="problem-list pl0">
                              <li>Nice camera</li>
                              <li>Microphone, and</li>
                              <li>Video-audio editing software That would cost you THOUSANDS of dollars.</li>
                           </ul>
                        </div>
                     </div>
                     <div class="col-md-6 order-md-1">
                        <img src="assets/images/opt2.webp" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>               
            </div>
            <div class="row mt-md70 mt20">
               <div class="col-12">
                  <div class="row align-items-center">
                     <div class="col-md-6">
                        <div class="f-24 f-md-32 w700 red-clr lh140">
                           You Need To Learn Complex Editing Tools
                        </div>
                        <div class="f-18 w500 lh140 black-clr2 mt20">
                           <ul class="problem-list pl0">
                              <li>You need to learn COMPLEX video & audio editing skills. 
                              Most software are complex and difficult to learn, especially 
                              if you are a non-technical guy like us.
                              </li>
                           </ul>
                        </div>
                     </div>
                     <div class="col-md-6">
                        <img src="assets/images/opt3.webp" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
               <div class="col-12 mt-md70 mt20">
                  <div class="row align-items-center">
                     <div class="col-md-6 order-md-2">
                        <div class="f-24 f-md-32 w700 red-clr lh140">
                           Even If You Outsource Any of the Above Tasks!
                        </div>
                        <div class="f-18 w500 lh140 black-clr2 mt20">
                           <ul class="problem-list pl0">
                              <li> 	
                                 Firstly, it can cost you 1000’s Dollars even for a 
                                 small project.                                  
                              </li>
                              <li> 	
                                 Finding a well skilled Freelancer who could handle 
                                 all your task is a big headache. 
                              </li>
                              <li> 	
                                 Even then you have to work for countless hours 
                                 finding ideas and explaining your requirement to them 
                              </li>
                           </ul>
                        </div>
                     </div>
                     <div class="col-md-6 order-md-1">
                        <img src="assets/images/opt4.webp" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>              
            </div>
            <div class="row mt-md50 mt20">
               <div class="col-12 f-md-22 f-20 w400 lh140 black-clr2">
                  <span class="w700"> Problem does not end here, after all these efforts, still there is no security that you will get the desired results.</span> <br><br>
                  There’re tons of areas where marketers can face challenges that we had faced in our journey before creating this masterpiece
               </div>
            </div>
            <div class="row mt-md50 mt20">
               <div class="col-12 col-md-10 mx-auto">
                  <div class="any-shape text-center">
                     <div class="f-md-45 f-28 lh140 orange-clr w700 text-uppercase">
                        BUT NOT ANYMORE!
                     </div>
                     <div class="f-18 lh140 white-clr w400 mt15">
                        Imagine if you had an ultimate ground-breaking beginner friendly technology that’s custom <br class="d-none d-md-block"> created keeping every marketer in mind, and that creates Short Videos for YouTube without<br class="d-none d-md-block"> having to spend money on costly equipment, without creating much content, and without <br class="d-none d-md-block">having to be on camera at all!          
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-----Option Section End------>

      <!-- Proudly Section -->
      <div class="proudly-sec" id="product">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center lh140">
                  <div class="f-28 f-md-32 w700 text-center orange-clr lh140 text-capitalize">Presenting…</div>
               </div>
               <div class="col-12 mt-md40 mt20 text-center">
                  <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 858.2 150.9" style="enable-background:new 0 0 858.2 150.9; max-height: 100px;" xml:space="preserve">
                     <style type="text/css">
                        .st0{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_1_);}
                        .st1{fill-rule:evenodd;clip-rule:evenodd;fill:#3C4650;}
                        .st2{fill-rule:evenodd;clip-rule:evenodd;fill:#1C262D;}
                        .st3{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_2_);}
                        .st4{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_3_);}
                        .st5{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_4_);}
                        .st6{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_5_);}
                        .st7{fill:#FFFFFF;}
                     </style>
                     <g id="Layer_1-2">
                        <g>
                           <g>
                              <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="25.2183" y1="-779.9527" x2="12.9283" y2="-954.9427" gradientTransform="matrix(1 0 0 -1 0 -776)">
                                 <stop offset="0" style="stop-color:#CCCCCB"></stop>
                                 <stop offset="0.32" style="stop-color:#3C4650"></stop>
                                 <stop offset="0.7" style="stop-color:#252D33"></stop>
                                 <stop offset="1" style="stop-color:#181D20"></stop>
                              </linearGradient>
                              <path class="st0" d="M40.1,3.1c-14.4-6-22.8-3.9-29.9,10.8C-0.2,35.8-2.6,80.4,3,112.9c5.4,20.7,22.1,25.1,32.2,15
                              c0.7-0.3,1.5-0.6,2.1-0.9c-4.3,1.9-9.1-0.3-10.5-4.8c-7.4-23.6-6.2-70,0.3-91.8C30.9,17.5,39.4,4,40.1,3.1L40.1,3.1z"></path>
                              <path class="st1" d="M31.6,0.4C1.2,18.4,7.9,102.5,18.1,129.5c5.3,1.4,11.3,0.9,17-1.6c0.7-0.3,1.5-0.6,2.1-0.9
                              c-4.3,1.9-9.1-0.3-10.5-4.8c-7.4-23.6-4.8-64.4,1.9-88.3C32.9,18.4,39.3,4.6,40.1,3.1c-3.2-1.3-3.4-1.4-6.2-2.2L31.6,0.4
                              L31.6,0.4z"></path>
                              <path class="st2" d="M2.4,109.5c0.2,1.2,0.3,2.3,0.6,3.4c3.4,15.5,18.4,20.7,32.2,15c0.7-0.3,1.5-0.6,2.1-0.9
                              c-4.3,1.9-9.1-0.3-10.5-4.8C13.8,125.2,5.6,121.7,2.4,109.5L2.4,109.5z"></path>
                              <linearGradient id="SVGID_2_" gradientUnits="userSpaceOnUse" x1="-5.0837" y1="-921.3506" x2="166.8963" y2="-831.7707" gradientTransform="matrix(1 0 0 -1 0 -776)">
                                 <stop offset="0" style="stop-color:#CCCCCB"></stop>
                                 <stop offset="0.32" style="stop-color:#3C4650"></stop>
                                 <stop offset="0.7" style="stop-color:#252D33"></stop>
                                 <stop offset="1" style="stop-color:#181D20"></stop>
                              </linearGradient>
                              <path class="st3" d="M118.5,101.8c15-13.2,4.7-28.9-11.3-33.6c6.2,5.9,5.8,10.5-1.3,16.6c-10.3,8.8-22.8,17-34.3,24.1
                              c-11.4,6.9-24,13.9-36.3,19c-13.8,5.7-28.8,0.5-32.2-15c1.7,9.8,4.1,18.4,7.2,25c7.1,14.8,18.7,15.5,33.1,9.6
                              C65.7,138.2,99.7,117.5,118.5,101.8L118.5,101.8L118.5,101.8z"></path>
                              <path class="st1" d="M120.4,83.8c-14.2,18.1-62,46.8-80.9,55.3c-11.8,5.2-25.1,7.1-31.1-5.5c0.6,1.6,1.1,2.9,1.8,4.3
                              c7.1,14.8,18.7,15.5,33.1,9.6c22.4-9.3,56.5-30,75.2-45.6C124.2,96.5,123.7,90.1,120.4,83.8L120.4,83.8L120.4,83.8z"></path>
                              <linearGradient id="SVGID_3_" gradientUnits="userSpaceOnUse" x1="-9.377" y1="-913.095" x2="162.593" y2="-823.515" gradientTransform="matrix(1 0 0 -1 0 -776)">
                                 <stop offset="0" style="stop-color:#CCCCCB"></stop>
                                 <stop offset="0.32" style="stop-color:#4E4F4F"></stop>
                                 <stop offset="0.7" style="stop-color:#383838"></stop>
                                 <stop offset="1" style="stop-color:#231F20"></stop>
                              </linearGradient>
                              <path class="st4" d="M35.2,127.9c-13.8,5.7-28.8,0.5-32.2-15c0.2,1.2,0.4,2.5,0.7,3.7c4.3,15.7,18.5,20.2,33,13.8
                              c24.1-10.6,47-27.5,57.7-36.5c-7.4,5.4-15.3,10.4-22.8,15C60.1,115.8,47.5,122.8,35.2,127.9L35.2,127.9L35.2,127.9z"></path>
                              <path class="st2" d="M115.3,104.5c1.1-0.9,2.1-1.8,3.2-2.6c11-10.1-0.8-24.2-11.3-33.6c4.9,4.8,5.7,8.7,2.1,13.1
                              C115.6,85.6,123.2,97.3,115.3,104.5L115.3,104.5L115.3,104.5z"></path>
                              <linearGradient id="SVGID_4_" gradientUnits="userSpaceOnUse" x1="39.6241" y1="-837.5" x2="136.5" y2="-837.5" gradientTransform="matrix(1 0 0 -1 0 -776)">
                                 <stop offset="0" style="stop-color:#ED2024"></stop>
                                 <stop offset="1" style="stop-color:#F05757"></stop>
                              </linearGradient>
                              <path class="st5" d="M60,105.5c-3.4-18.2-3.5-36.9-0.7-55.1c2.6,15.8,8,31.4,15.4,46.2c8.1-5.2,16.1-10.7,23.5-16.8
                              c0.2-6.1,0.6-12,1.2-17.9c10.3,7.9,33,27.2,19.1,40c0.4-0.3,0.7-0.7,1.2-1c22.4-19.2,22.4-30.6,0-49.9
                              c-9.9-8.4-21.3-16.1-32.4-23c-3.1,12.2-5.1,24.7-6.1,37.3c-5-16.1-5.8-28-5.8-44.5c-7.5-4.3-15.2-8.5-23-12.3
                              c-13.2,33.6-16.3,70.8-8.7,106C49.2,111.7,54.6,108.7,60,105.5L60,105.5L60,105.5z"></path>
                              <linearGradient id="SVGID_5_" gradientUnits="userSpaceOnUse" x1="144.1735" y1="-864.6838" x2="14.8335" y2="-839.7738" gradientTransform="matrix(1 0 0 -1 0 -776)">
                                 <stop offset="0" style="stop-color:#ED2024"></stop>
                                 <stop offset="1" style="stop-color:#F05757"></stop>
                              </linearGradient>
                              <path class="st6" d="M99.4,61.9c10.3,8,33,27.2,19.1,40l1.2-1c6.8-5.8,6.8-13,1.9-20.5C116.3,72.4,106.1,65.4,99.4,61.9
                              L99.4,61.9L99.4,61.9z"></path>
                           </g>
                           <g>
                              <path class="st7" d="M834,63.8V50h-15.5h-8.2c3.2,0,5.4,2.2,5.7,4.5c0,4.4,0.1,9.4,0.1,15.6v54H834V90.6
                              c0-19.8,9.2-26.9,24.2-26.8V50.1C845.7,50.3,837.5,54.8,834,63.8L834,63.8z"></path>
                              <path class="st7" d="M573.3,106.1c0,6.9,0.6,16.3,1,18.2h-17.1c-0.6-1.5-1-5.3-1.1-8.1c-2.7,4.4-8,9.8-21.5,9.8
                              c-17.7,0-25.3-11.6-25.3-23.1c0-16.8,13.4-24.5,35.2-24.5h11.3v-5.1c0-5.7-2-11.8-12.9-11.8c-9.9,0-11.9,4.5-13,10h-17.1
                              c1.1-12.2,8.6-23.2,30.7-23.1c19.3,0.1,29.8,7.7,29.8,25.1L573.3,106.1L573.3,106.1z M555.8,89.6h-9.6
                              c-13.2,0-18.9,3.9-18.9,12.1c0,6.2,4,11.1,11.9,11.1c14.7,0,16.5-10.1,16.5-21.1L555.8,89.6L555.8,89.6z"></path>
                              <path class="st7" d="M745.2,90.8c0,11.2,5.7,20.8,16.7,20.8c9.6,0,12.5-4.3,14.6-9.1h18c-2.7,9.2-10.8,23.4-33.1,23.4
                              c-24,0-34.3-18.5-34.3-37.8c0-22.8,11.7-39.8,35-39.8c24.9,0,33.3,18.7,33.3,36.3c0,2.4,0,4.1-0.3,6.2H745.2L745.2,90.8z
                              M777.4,79.4c-0.2-9.8-4.5-18-15.4-18s-15.5,7.6-16.5,18H777.4L777.4,79.4z"></path>
                              <path class="st7" d="M606.8,102.3c1.8,6.6,7,10.4,15.4,10.4s12-3.4,12-8.6s-3.3-7.8-15.1-10.7c-23.2-5.7-27.3-12.8-27.3-23.2
                              s7.7-21.9,29-21.9s29,11.9,29.8,22h-17.1c-0.8-3.4-3.2-9.1-13.5-9.1c-8,0-10.5,3.7-10.5,7.5c0,4.3,2.5,6.4,15.1,9.4
                              c24,5.6,27.8,13.8,27.8,24.7c0,12.5-9.7,23.1-30.9,23.1s-30.7-10.5-32.5-23.7L606.8,102.3L606.8,102.3z"></path>
                              <path class="st7" d="M471,48.3c-12.3,0-18.9,5.6-23.1,12c-2.7-6.5-9-12-19.6-12c-11.2,0-17.3,5.5-20.9,11.4l0.1-9.7h-23.6
                              c3.4,0,5.8,2.5,5.8,5l0,0c0.1,4.7,0.1,9.4,0.1,14.1v55h17.7V82.9c0-13.2,4.5-19.8,14.1-19.8s11.9,7,11.9,15.3v45.8h17.6V81.7
                              c0-12,4-18.8,13.9-18.8s12.1,7.4,12.1,14.7v46.5h17.5V75.5C494.4,56,483.6,48.3,471,48.3L471,48.3z"></path>
                              <path class="st7" d="M368.8,103.6V26.4h-17.9h-5.8c3.4,0,5.8,2.5,5.8,5l0,0v24.8c-1.8-3.6-7.5-7.9-19-7.9
                              c-20.8,0-33.5,16.7-33.5,39.5s11.9,38.1,30.7,38.1c11.4,0,18.2-3.9,21.8-10.4l0.1,8.7h18C368.8,117.3,368.8,110.5,368.8,103.6
                              L368.8,103.6z M334,111.4c-10.6,0-17.2-8.4-17.2-24.1s6.3-24.4,17.9-24.4c14.5,0,16.9,9.9,16.9,23.8
                              C351.5,99.2,349.2,111.3,334,111.4L334,111.4z"></path>
                              <g>
                                 <path class="st7" d="M265.6,50h-5.8c3.4,0,5.8,2.5,5.8,5v69.2h17.9V50H265.6z"></path>
                                 <path class="st7" d="M283.5,29.1v-2.7h-17.9V47l0,0C275.5,46.9,283.5,38.9,283.5,29.1L283.5,29.1z"></path>
                              </g>
                              <g>
                                 <path class="st7" d="M712.3,63.8V50.1h-15.1l0,0h-17.9l0,0h-11.9v13.8h11.9v41.5c0,12.8,4.9,19.9,18.4,19.9
                                    c3.9,0,9.1-0.1,12.9-1.4v-12.6c-1.7,0.3-3.9,0.4-5.3,0.4c-6.3,0-8-2.7-8-8.8V63.8H712.3L712.3,63.8z"></path>
                                 <path class="st7" d="M697,29.1v-2.7h-17.9V47l0,0C689,46.9,697,38.9,697,29.1L697,29.1z"></path>
                              </g>
                              <path class="st7" d="M231.3,25.9l-16.3,47c-4.8,14-9.3,27.1-11.3,35.9h-0.3c-2.2-9.7-6.2-22.3-10.8-36.3L177,25.9h-25.7
                              c5.1,0,7.6,4.6,8.6,7.4l32.4,90.9h21.8l36.1-98.3L231.3,25.9L231.3,25.9z"></path>
                           </g>
                        </g>
                     </g>
                  </svg>
               </div>
               <div class="col-12 f-md-40 f-28 mt-md30 mt20 w700 text-center white-clr lh140">
                  A Push-Button Technology Creates Reels & Short Videos to Publish on YouTube, 
                  Instagram, TikTok, Facebook & Snapchat using Just One Keyword and Drive Viral Traffic to Any of Your Offers
                  
               </div>
               <!-- <div class="col-12 f-16 f-md-18 lh140 w500 text-center white-clr mt20">
                  It enabled you to create Engaging, Highly Professional Short Videos, <br class="d-none d-md-block">
                  Add Voiceover or Background Music with Zero Tech Hassles. 
               </div> -->
            </div>
            <div class="row mt20 mt-md50 align-items-center">
               <div class="col-12">
                  <img src="assets/images/product-image.webp" class="img-fluid d-block mx-auto img-animation"
                     alt="Product">
               </div>
            </div>
         </div>
      </div>
      <!-- Proudly Section End -->

      <!-- Step Section Start -->
      <div class="step-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-39 f-28 w600 text-center black-clr2 lh140">
                  Create Stunning Stories, Reels & Shorts for Massive Traffic To Any Offer, Page, or Link <span class="red-clr w700">in Just 3 Easy Steps</span>
               </div>
            </div>
            <div class="row mt20 mt-md50">
               <div class="col-12 col-md-4">
                  <div class="step-block">
                     <div class="step-title f-20 f-md-22 w700">Step 1</div>
                     <img src="assets/images/step-1.webp" class="img-fluid d-block mx-auto mt15 mt-md30">
                     <div class="step-content">
                        <div class="f-22 f-md-28 w600 black-clr2 mt10">Choose Platform</div>
                        <div class="w400 f-18 f-md-20 lh140 mt10 black-clr2">
                           Choose the social platform- YouTube, Instagram, TikTok, Facebook, or Snapchat you want to create Reel or Shorts for
                        </div>
                     </div>  
                  </div>
               </div>
               <div class="col-12 col-md-4 mt20 mt-md0">
                  <div class="step-block">
                     <div class="step-title f-20 f-md-22 w700">Step 2</div>
                     <img src="assets/images/step-2.webp" class="img-fluid d-block mx-auto mt15 mt-md30">
                     <div class="step-content">
                        <div class="f-22 f-md-28 w600 black-clr2 mt10">Enter a Keyword </div>
                        <div class="w400 f-18 f-md-20 lh140 mt10 black-clr2">
                           Enter the desired Keyword & you will get several short videos. Select any video & customize it by adding background, vectors, audio, & fonts to make it engaging
                        </div>
                     </div>  
                  </div>
               </div>
               <div class="col-12 col-md-4 mt20 mt-md0">
                  <div class="step-block">
                     <div class="step-title f-20 f-md-22 w700">Step 3</div>
                     <img src="assets/images/step-3.webp" class="img-fluid d-block mx-auto mt15 mt-md30">
                     <div class="step-content">
                        <div class="f-22 f-md-28 w600 black-clr2 mt10">Publish and Profit</div>
                        <div class="w400 f-18 f-md-20 lh140 mt10 black-clr2">
                           Now publish your Reels & Shortson different social platforms with a click to drive unlimited viral traffic & sales on your offer, page, or website.
                        </div>
                     </div>  
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md70">
               <div class="col-12 text-center">
                  <div class="f-20 f-md-24 w600 lh140">
                     No Download/Installation  |  No Prior Knowledge  |  100% Beginners Friendly
                  </div>
                  <div class="f-18 f-md-20 w400 mt15 mt-md30 lh140">
                     In just 60 seconds, you can create your first profitable Reels & Shorts to enjoy MASSIVE FREE Traffic, Sales & Profits coming into your bank account on autopilot.
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Step Section End -->

      <!-- Demo Section  -->
      <div class="demo-sec" id="demo">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="white-clr f-md-45 f-28 w700 text-center">
                     Watch VidMaster in Action
                  </div>
               </div>
               <div class="col-12 col-md-9 mx-auto mt-md40 mt20">
                     <div class="responsive-video">
                     <iframe src="https://vidmaster.dotcompal.com/video/embed/jwjoicqx4d" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                        box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen"
                        allowfullscreen></iframe>
                     </div>
               </div>
               <div class="col-12 text-center mt20 mt-md40">
                  <div class="f-18 f-md-28 w700 lh140 text-center white-clr">
                     Free Commercial License + Low 1-Time Price For Launch Period Only
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 text-center mt20 white-clr">
                  Use Coupon Code <span class="red-clr w600">"VidMaster"</span> for an Additional <span class="red-clr w600">$3 Discount</span>
                  </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center">
                        <a href="#buynow" class="cta-link-btn">Get Instant Access To VidMaster</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                     <img src="assets/images/compaitable-with1.webp" class="img-responsive mx-xs-center md-img-right" alt="visa">
                     <div class="d-md-block d-none visible-md px-md30">
                        <img src="assets/images/v-line.webp" class="img-fluid" alt="line">
                     </div>
                     <img src="assets/images/days-gurantee1.webp" class="img-responsive mx-xs-auto mt15 mt-md0" alt="30 days">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Demo Section End -->

      

      <!-- Testimonials Section -->
      <div class="testimonial-section1">
         <div class="container ">
            <div class="row ">
               <div class="f-md-45 f-28 w700 lh140 text-center black-clr2">
                  Here's What REAL Users Have to <br class="d-none d-md-block"> Say About VidMaster
               </div>
            </div>
            <div class="row mt25 mt-md50 align-items-center">
               <div class="col-12 col-md-10 offset-md-2">
                  <div class="testi-block">
                     <div class="row d-flex align-items-center flex-wrap">
                         <div class="col-12 col-md-3">
                             <img src="assets/images/c4.webp" class="img-fluid d-block mx-auto">
                             <div class="f-22 f-md-24 w700 lh120 red-clr mt20 text-center">Lucas Rodriguez</div>
                         </div>
                         <div class="col-12 col-md-9 mt20 mt-md0 text-center text-md-start">
                             <div class="f-22 f-md-28 w700 lh140">Easy To Use Software with Training Included</div>
                             <div class="f-20 w400 lh140 mt20 quote1">
                              VidMaster is next-level quality software to full fill all my YouTube marketing needs. Also, it is  <span class="w700">so easy to use, and the training included makes it even easier</span> to create highly engaging and professional videos & shorts in a few minutes. I'd say that this is a MUST HAVE technology for marketers.
                             </div>
                            
                         </div>
                     </div>
                 </div>
               </div>
               <div class="col-12 col-md-10 mt20 mt-md40">
                  <div class="testi-block">
                     <div class="row d-flex align-items-center flex-wrap">
                         <div class="col-12 col-md-3 order-md-2">
                             <img src="assets/images/c5.webp" class="img-fluid d-block mx-auto">
                             <div class="f-22 f-md-24 w700 lh120 red-clr mt20 text-center">Theodore Miller</div>
                         </div>
                         <div class="col-12 col-md-9 order-md-1 mt20 mt-md0 text-center text-md-start">
                             <div class="f-22 f-md-28 w700 lh140">Revolutionary software comes at a one-time price</div>
                             <div class="f-20 w400 lh140 mt20 quote1">
                              VidMaster combines ease of use with  <span class="w700">super-powerful video creation technology.</span> I'm using it to create short videos for my YouTube channel that are well-suited to my audience.<br><br>
                              Trust me, it works like a breeze and the best part is, <span class="w700">that this revolutionary software comes at a one-time price.</span> It’s amazing! I am enjoying working with it. Great Job Ayush & Pranshu. 
                              
                             </div>
                           
                         </div>
                     </div>
                 </div>
               </div>
               <div class="col-12 col-md-10 offset-md-2 mt20 mt-md40">
                  <div class="testi-block">
                     <div class="row d-flex align-items-center flex-wrap">
                         <div class="col-12 col-md-3">
                             <img src="assets/images/c6.webp" class="img-fluid d-block mx-auto">
                             <div class="f-22 f-md-24 w700 lh120 red-clr mt20 text-center">Harper Garcia</div>
                         </div>
                         <div class="col-12 col-md-9 mt20 mt-md0 text-center text-md-start">
                             <div class="f-22 f-md-28 w700 lh140">I Can Make Money with Complete freedom</div>
                             <div class="f-20 w400 lh140 mt20 quote1">
                              I have been in the Video Marketing arena for quite some time now, and I must say that VidMaster will take the industry by storm.<br.<br>
                              The coolest part is that <span class="w700">it enables you to create video shorts in just a few minutes which usually takes me a long day to create even a single video</span> to boost engagement and traffic on my YouTube channel. Now, I can make money with Amazon/Affiliate offers, AdSense & my own offers with fewer efforts on video creation.<span class="w700"> Complete freedom is what it provides me.</span> Something to check on a serious note. 
                              
                             </div>
                            
                         </div>
                     </div>
                 </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Testimonials Section End -->

      <!-- <div class="feature-highlight" id="features">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-40 f-24 w700 text-center balck-clr2 lh140">
                  Here Are The GROUND - BREAKING Features<br class="d-none d-md-block"> 
                  That Makes VidMaster A CUT ABOVE The Rest  
               </div>
            </div>
         </div>
      </div> -->

      <!-- Features Section Star -->
      <div class="features-section-one">
         <div class="container">           
            <div class="row align-items-center">
               <div class="col-12 mb20 mb-md50">
                  <div class="f-md-45 f-28 w700 lh140 white-clr text-center">
                  Here Are The GROUND - BREAKING Features<br class="d-none d-md-block"> 
                  That Makes VidMaster A CUT ABOVE The Rest 
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="f-md-32 f-24 w700 lh140 white-clr">
                     Create Highly Engaging & Professional Reels & Short Videos 
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 white-clr mt15">
                     Create Reels & Shorts for YouTube, Instagram, TikTok, Facebook & SnapChat                  
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0">
                  <img src="assets/images/f1.webp" class="img-fluid d-block mx-auto" alt="Features">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-md-6 order-md-2">
                  <div class="f-md-32 f-24 w700 lh140 white-clr">
                     Create Shorts by One Keyword
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 white-clr mt15">
                     Create Reels & short videos by entering Just one keyword and you will get number of short videos to choose or upload your own.
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0 order-md-1">
                  <img src="assets/images/f2.webp" class="img-fluid d-block mx-auto" alt="Features">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-md-6">
                  <div class="f-md-32 f-24 w700 lh140 white-clr">
                     Create 9:16 Vertical Video
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 white-clr mt15">
                     Create highly professional & engaging 9:16 vertical video. Perfect for mobile viewing and guaranteed to entertain your audience.                  
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0">
                  <img src="assets/images/f3.webp" class="img-fluid d-block mx-auto" alt="Features">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-md-6 order-md-2">
                  <div class="f-md-32 f-24 w700 lh140 white-clr">
                     Create Boomerang
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 white-clr mt15">
                     Create boomerang short video loops that boomerang forward & reverse through the action.
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0 order-md-1">
                  <!-- <img src="assets/images/f4.webp" class="img-fluid d-block mx-auto" alt="Features"> -->
                  <video width="100%" height="auto" loop autoplay muted="muted" class="mp4-border" >
                   <source src="assets/images/f4.mp4" type="video/mp4">
               </video>
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-md-6">
                  <div class="f-md-32 f-24 w700 lh140 white-clr">
                     Create High-Quality Explanatory Whiteboard Video Shorts
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 white-clr mt15">
                     Create whiteboard video shorts by just adding your text and customizing font, color, and alignment. These videos are highly effective and informative.                    
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0">
                  <img src="assets/images/f5.webp" class="img-fluid d-block mx-auto" alt="Features">
               </div>
            </div>
         </div>
      </div>
      <div class="grey-section1">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-6">
                  <div class="f-md-32 f-24 w700 lh140 balck-clr2">
                     Create Videos by Images
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 balck-clr2 mt15">
                     Find images for videos by searching images with keywords inside VidMaster or upload your own images                   
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0">
                  <img src="assets/images/f6.webp" class="img-fluid d-block mx-auto" alt="Features">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-md-6 order-md-2">
                  <div class="f-md-32 f-24 w700 lh140 balck-clr2">
                     50+ Background Templates
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 balck-clr2 mt15">
                     Easily create Reels & shorts by using 50+ background templates to make your video attractive &get more & more engagement.                  
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0 order-md-1">
                  <img src="assets/images/f7.webp" class="img-fluid d-block mx-auto" alt="Features">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-md-6">
                  <div class="f-md-32 f-24 w700 lh140 balck-clr2">
                     25+ Vector Images
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 balck-clr2 mt15">
                     Create more engaging & attention grabbing Reels & Shorts by using 25+vector images. 
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0">
                  <img src="assets/images/f8.webp" class="img-fluid d-block mx-auto" alt="Features">
               </div>
            </div>
         </div>
      </div>
      <div class="white-feature">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-6 order-md-2">
                  <div class="f-md-32 f-24 w700 lh140 balck-clr2">
                  	100+ Stylish Fonts
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 balck-clr2 mt15">
                     Use 100+ different Stylish fonts to make your shorts more presentable
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0 order-md-1">
                  <img src="assets/images/f9.webp" class="img-fluid d-block mx-auto" alt="Features">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-md-6">
                  <div class="f-md-32 f-24 w700 lh140 balck-clr2">
                     Add Music Waves
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 balck-clr2 mt15">
                     Add Music Waves run with audio of different color to your Short videos to make your video engaging.
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0">
                  <!-- <img src="assets/images/f10.webp" class="img-fluid d-block mx-auto" alt="Features"> -->
                  <video width="100%" height="auto" loop autoplay muted="muted" class="mp4-border" >
                   <source src="assets/images/f10.mp4" type="video/mp4">
               </video>
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-md-6 order-md-2">
                  <div class="f-md-32 f-24 w700 lh140 balck-clr2">
                     Build Authority by Adding Your Branding
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 balck-clr2 mt15">
                     Build your authority in the audience by adding your brand watermarks and logos in the video.                   
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0 order-md-1">
                  <img src="assets/images/f11.webp" class="img-fluid d-block mx-auto" alt="Features">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-md-6">
                  <div class="f-md-32 f-24 w700 lh140 balck-clr2">
                     Share Shorts Directly to YouTube
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 balck-clr2 mt15">
                     You can easily share the video on YouTube or can download it is to use wherever you want.
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0">
                  <img src="assets/images/f12.webp" class="img-fluid d-block mx-auto" alt="Features">
               </div>
            </div>
         </div>
      </div>

      <div class="grey-section1">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-6 order-md-2">
                  <div class="f-md-32 f-24 w700 lh140 balck-clr2">
                     100+ Social Sharing Platforms
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 balck-clr2 mt15">
                     You can alsoshare these Reels & short videos t0  100+ social media platforms directly to get free viral traffic                            
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0 order-md-1">
                  <img src="assets/images/f13.webp" class="img-fluid d-block mx-auto" alt="Features">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md50">
               <div class="col-md-6">
                  <div class="f-md-32 f-24 w700 lh140 balck-clr2">
                     Completely Cloud-Based Software
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 balck-clr2 mt15">
                     Fully Cloud-Based Software with Zero Tech Hassles & 24*7 Customer Support          
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0">
                  <img src="assets/images/f14.webp" class="img-fluid d-block mx-auto" alt="Features">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md50">
               <div class="col-md-6 order-md-2">
                  <div class="f-md-32 f-24 w700 lh140 balck-clr2">
                     100% Newbie Friendly
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 balck-clr2 mt15">
                     Easy and Intuitive to Use Software. Also comes with Step-by-Step Video Training & PDF
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0 order-md-1">
                  <img src="assets/images/f15.webp" class="img-fluid d-block mx-auto" alt="Features">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md50">
               <div class="col-md-6">
                  <div class="f-md-32 f-24 w700 lh140 balck-clr2">
                     No Monthly Fees or Additional Charges
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0">
                  <img src="assets/images/f16.webp" class="img-fluid d-block mx-auto" alt="Features">
               </div>
            </div>
         </div>
      </div>
      <!-- Features Section End -->

      <!-- CTA Section -->
      <div class="cta-section-new">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-18 f-md-28 w700 lh140 text-center white-clr">
                     Free Commercial License + Low 1-Time Price For Launch Period Only
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 text-center mt20 white-clr">
                     Use Discount Coupon <span class="red-clr w600">"VidMaster"</span> for Instant <span class="red-clr w700">$3 OFF</span>
                 </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center">
                        <a href="#buynow" class="cta-link-btn">Get Instant Access To VidMaster</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                     <img src="assets/images/compaitable-with1.webp" class="img-responsive mx-xs-center md-img-right" alt="visa">
                     <div class="d-md-block d-none visible-md px-md30">
                        <img src="assets/images/v-line.webp" class="img-fluid" alt="line">
                     </div>
                     <img src="assets/images/days-gurantee1.webp" class="img-responsive mx-xs-auto mt15 mt-md0" alt="30 days">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- CTA Section End -->


            <!-- Imagine Section Start -->
            <section class="imagine-sec">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-12 col-md-6 f-18 f-md-20 w400 lh140 balck-clr2 order-md-2">
                  A new lightweight piece of technology that’s so incredibly sophisticated yet intuitive & easy to use that allows you to do it all, with literally ONE click!<br><br>
                  <span class="w700">That’s EXACTLY what we’ve designed VidMaster to do for you.</span>
                  <br><br>
                  So, if you want to build super engaging Short Videos for YouTube and other social media at the push of a button, and get viral traffic automatically and convert it into SALES & PROFITS, all from start to finish, then VidMaster is made for you!
               </div>
               <div class="col-md-6 col-12 order-md-1 mt20 mt-md0">
                  <img src="assets/images/imagine-box.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>
         </div>
      </section>
      <!-- Imagine Section End -->

      <div class="targeted-section">
         <div class="container">
             <div class="row">
                 <div class="col-12">
                     <div class="row d-flex align-items-center flex-wrap">
                         <div class="col-12 col-md-7">
                           <div class="f-18 f-md-20 w400 lh140 balck-clr2">
                              <ul class="noneed-listing pl0">
                                 <li>Create instant videos shorts for YouTube and other social media in just 3 clicks</li>
                                 <li>Drive tons of viral traffic from RED-HOT YouTube & other social media giants. </li>
                                 <li>Get the best results without getting into complex video creation & editing hassles</li>
                                 <li>Get more exposure for your offers in a hands-down manner</li>
                                 <li>Boost sales & profits without spending much</li>
                                 <li>Jump into the most trending video marketing strategy with YouTube Shorts.</li>
                              </ul>
                           </div>
                         </div>
                         <div class="col-12 col-md-5 mt20 mt-md0">
                             <img src="assets/images/target-arrow.webp" class="img-fluid d-block mx-auto">
 
                         </div>
                     </div>
                 </div>
                 <div class="col-12 mt-md30 mt20">
                     <div class="f-18 f-md-20 w400 lh140 balck-clr2">
                        <span class="w700">Seriously, having to do all video creation manually is expensive,</span> time-consuming and put frankly downright irritating, especially when you have to do this on a daily basis. <br><br>
                        That's exactly the reason so many people are afraid to even get started with YouTube Shorts and the same  <span class="w700">reason why out of all that DO get started, very few truly "make it".</span><br><br>
                        
                        We have done all the grunt work for you. So, you don’t need to worry at all
                        
 
                     </div>
                 </div>
             </div>
         </div>
     </div>
      <!-- Need Section Start -->

      <div class="need-marketing">
         <div class="container">
             <div class="row">
                 <div class="col-12">
                     <div class="f-md-45 f-28 lh140 w700 text-center balck-clr2">Who Can Benefit from VidMaster?
                     </div>
                 </div>
                 <div class="col-12 mt-md30 mt20">
                     <div class="f-20 f-md-22 w400 lh140 balck-clr2">
                       <span class="w700"> This is Simple.</span> It helps<br><br>
                     </div>
                 </div>
             
              <div class="col-12 mt0 mt-md20">
                     <div class="row flex-wrap">
                         <div class="col-12 col-md-5">
                             <div class="f-20 f-md-20 w400 lh140 balck-clr2">
                                 <ul class="noneed-listing pl0">
<li> <span class="w700">Marketers</span> to drive more traffic</li>
<li> <span class="w700">Affiliates,</span> make more commissions</li>
<li> <span class="w700">SEO marketers</span> get #1 ranking and traffic</li>
<li> <span class="w700">Social & Video Marketers</span> get free viral traffic</li>
<li> <span class="w700">Creators</span> can get more income handsfree without extra efforts in video creation.</li> 
                                    
 
                                 </ul>
                             </div>
                         </div>
                         <div class="col-12 col-md-7 mt0 mt-md0">
                            <div class="f-20 f-md-20 w400 lh140 balck-clr2">
                                 <ul class="noneed-listing pl0">
<li><span class="w700">Product sellers, </span> sell more with RED HOT traffic</li>
<li><span class="w700">List builders</span>  skyrocket their subscribers...</li>
<li><span class="w700">Freelancers and Agencies –</span>  Provide RED Hot service & make profits</li>
<li><span class="w700">Local Business Owners – </span> get more engagement & TRAFFIC</li>
<li>Weight Loss, Gaming, Real Estate, Health, Wealth –</span> <span class="w700">Any Kind of Business</span> </li>

 
                                 </ul>
                             </div>
                         </div>
                     </div>
                 </div>
             
              <div class="col-12 mt20 mt-md70 text-center">
                     
                         <div class="f-22 f-md-32 w700 lh140 red-clr">
                           It Works Seamlessly for ANY NICHE… All at the push of a button. 
                         </div>
                 </div>
             
                 <div class="col-12 mt20 mt-md50">
                     <div class="row align-items-center flex-wrap">
                         <div class="col-12 col-md-7">
                   <div class="f-20 f-md-26 w700 lh140 balck-clr2 text-center text-md-start">
                     VidMaster Is Designed to Meet Every Marketer’s Need:
                         </div>
                             <div class="f-20 f-md-20 w400 lh140 balck-clr2 mt20">
                      
                                 <ul class="noneed-listing pl0">
<li>Anyone who wants a nice PASSIVE income while focusing on bigger projects</li>
<li>Lazy people who want easy traffic & profits</li>
<li>Anyone who wants to value their business and money and is not ready to sacrifice them</li>
<li>People with products or services who want to kickstart online</li>                                    
 
                                 </ul>
                             </div>
                         </div>
                         <div class="col-12 col-md-5 mt20 mt-md0">
                             <img src="assets/images/platfrom.webp" class="img-fluid d-block mx-auto">
                         </div>
                     </div>
                 </div>
                 <div class="col-12 mt20 mt-md70 text-center">
                     <div class="look-shape">
                         <div class="f-22 f-md-30 w700 lh140 balck-clr2">
                           Look, it doesn't matter who you are or what you're doing.
                         </div>
                         <div class="f-20 f-md-22 w500 lh140 balck-clr2 mt10">
                           If you want to finally be successful online, make the money that you want and live in a way<br class="d-none d-md-block"> you dream of, VidMaster is for you.
                         </div>
                     </div>
                 </div>
                 <div class="col-12 mt20 mt-md70">
                     <div class="f-md-45 f-28 lh140 w700 text-center balck-clr2">Even BETTER: It’s Entirely Newbie Friendly
                     </div>
                 </div>
                 <div class="col-12 mt20 mt-md50">
                     <div class="row align-items-center flex-wrap">
                         <div class="col-12 col-md-7">
                             <div class="f-18 f-md-20 w400 lh140 balck-clr2">
                              Whatever your level of technical expertise, this amazing software enables 
                              you to create engaging & highly professional video shorts for any niche in 
                              the 7-minute flat. It really doesn’t matter.
                              <br><br>
                              <span class="w700">The ease of use for VidMaster is STUNNING,</span> allowing users to drive 
                              free viral traffic from YouTube, without ever having to turn their hair 
                              grey for complex video editing skills.
                              <br><br>
                              With VidMaster, not only do you get the BEST possible system for the LOWEST 
                              price around, but you can also sleep safe knowing you CONTROL every aspect of your business, from building a stunning video marketing strategy to promoting affiliate products, to traffic generation, monetization, list building, and TON more - no need to ever outsource those tasks again!
                              
                             </div>
                         </div>
                         <div class="col-12 col-md-5 mt20 mt-md0">
                             <img src="assets/images/better.webp" class="img-fluid d-block mx-auto">
                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </div>
      <!-- Need Section End -->
      <!-- CTA Section -->
      <div class="cta-section-new">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-18 f-md-26 w700 lh140 text-center white-clr">
                     Free Commercial License + Low 1-Time Price For Launch Period Only
                  </div>
                  <div class="f-20 f-md-24 w400 lh140 text-center mt20 white-clr">
                     Use Discount Coupon <span class="orange-clr w700">"VidMaster"</span> for Instant <span class="orange-clr w700">$3 OFF</span>
                 </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center">
                        <a href="#buynow" class="cta-link-btn">Get Instant Access To VidMaster</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                     <img src="assets/images/compaitable-with1.webp" class="img-responsive mx-xs-center md-img-right" alt="visa">
                     <div class="d-md-block d-none visible-md px-md30">
                        <img src="assets/images/v-line.webp" class="img-fluid" alt="line">
                     </div>
                     <img src="assets/images/days-gurantee1.webp" class="img-responsive mx-xs-auto mt15 mt-md0" alt="30 days">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- CTA Section End -->

      <!---Bonus Section Starts-->
      <div class="bonus-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="but-design-border">
                     <div class="f-md-45 f-28 lh140 w700 white-clr button-shape">
                        We’re not done Yet!
                     </div>
                  </div>
               </div>
               <div class="col-12 mt30 mt-md30 f-20 f-md-22 w500 balck-clr2 lh140 text-center">
                  When You Grab Your VidMaster Account Today, You’ll Also Get These <span class="w700">Fast Action Bonuses!</span>
               </div>
               <div class="col-12 ">
                  <div class="row ">
                     <!-------bonus 1------->
                     <div class="col-12 col-md-6 mt-md50 mt20 ">
                        <div class="bonus-section-shape ">
                           <div class="row align-items-center ">
                              <div class="col-md-6 mx-auto">
                                 <img src="assets/images/bonus-box.webp " class="img-fluid d-block mx-auto ">
                              </div>
                              <div class="col-12 mt20 mt-md40 ">
                                 <div class="w700 f-22 f-md-24 balck-clr2 lh140 ">
                                    DotcomPal- All in One Growth Platform for Entrepreneurs (That's PRICELESS)
                                 </div>
                                 <div class="f-18 f-md-20 w400 balck-clr2 lh140 mt15 ">
                                    All-in-one growth platform for Entrepreneurs & SMBs to generate more leads, bring more growth & sell more, more quickly. It’s got everything you need to convert more visitors and bring more growth at every customer touchpoint... without any designer, tech team, or the usual hassles. 
<br><br>
 This package is something that’s of huge worth for your business, but we’re offering it for free only with your investment in Cordova today. 
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-------bonus 2------->
                     <div class="col-12 col-md-6 mt-md50 mt20 ">
                        <div class="bonus-section-shape col-12 ">
                           <div class="row align-items-center ">
                              <div class="col-md-6 mx-auto col-12">
                                 <img src="assets/images/bonus-box.webp " class="img-fluid d-block mx-auto ">
                              </div>
                              <div class="col-12 mt20 mt-md40 ">
                                 <div class="w700 f-22 f-md-24 balck-clr2 lh140 ">
                                    Video Training on Viral Marketing Secrets
                                 </div>
                                 <div class="f-18 f-md-20 w400 balck-clr2 lh140 mt15 ">
                                    Believe it or not, people interested in whatever it is you are promoting are already congregating online. Social media platforms want you to succeed. The more popular your content becomes, the more traffic they get. 
<br><br>
                                    So, with this video training you will discover a shortcut to online viral marketing secrets. This will cover - The Two-Step Trick to Effective Viral Marketing, How Do You Find Hot Content? Maximize Niche Targeting for Your Curated Content, remember to protect yourself when sharing others’ content, how to share viral content on Facebook, how to share viral content on Twitter, Filter your content format to go viral on many platforms and much more. 
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt-md30 mt20 ">
                  <div class="row ">
                     <!-------bonus 3------->
                     <div class="col-12 col-md-6 ">
                        <div class="bonus-section-shape col-12 ">
                           <div class="row align-items-center ">
                              <div class="col-md-6 mx-auto">
                                 <img src="assets/images/bonus-box.webp " class="img-fluid d-block mx-auto ">
                              </div>
                              <div class="col-12 mt20 mt-md50">
                                 <div class="w700 f-22 f-md-24 balck-clr2 lh140 "> Modern Video Marketing
                                 </div>
                                 <div class="f-18 f-md-20 w400 balck-clr2 lh140 mt15 ">
                                    It's about time for you to learn the ins and outs of successful online video marketing! <br><br>

                                    Regardless of what you've heard, video marketing as a whole is not exactly a new phenomenon. Video marketing content is increasingly plugged into a larger marketing infrastructure.  
                                    <br><br>
                                    You have to wrap your mind around the fact that modern video marketing is both new and old. Depending on how you navigate these factors, you will either succeed or fail. Either your video is going to convert people into buyers or they're just going to sit there on YouTube getting zero views. 
                                    <br><br>
                                    So, with this video training you will discover the secrets of successful video marketing- The Modern and Effective Ways of Video Marketing, Modern Video Marketing Essentials, Types of Video Marketing and much more 
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-------bonus 4------->
                     <div class="col-12 col-md-6 mt20 mt-md0 ">
                        <div class="bonus-section-shape col-12 ">
                           <div class="row align-items-center ">
                              <div class="col-md-6 mx-auto">
                                 <img src="assets/images/bonus-box.webp " class="img-fluid d-block mx-auto ">
                              </div>
                              <div class="col-12 mt20 mt-md40">
                                 <div class="w700 f-22 f-md-24 balck-clr2 lh140 ">
                                    Youtube Authority  
                                 </div>
                                 <div class="f-18 f-md-20 w400 balck-clr2 lh140 mt15 ">
                                    Since its launch in 2005, YouTube has come a long way. It has become an extremely powerful tool for businesses to increase awareness of their brand, drive more traffic to their company sites, and reach a broad audience around the world.So, if you are isn’t already leveraging the power of YouTube there are some massive benefits that you’re missing out on. 
<br><br>
                                    With this video course you will: <br>
                                    <div class="f-20 f-md-20 w400 lh140 balck-clr2">
                      
                                       <ul class="noneed-listing pl0">
<li>A Clear understanding on starting a YouTube channel.  </li>
<li>Determine your target audience.  </li>
<li>Learn about the different types of videos  </li>
<li>Discover how you can increase engagement  </li>
<li>Learn the different avenues for monetizing your YouTube channel </li>
<li>Learn about the different mistakes that you can make on your YouTube channel     </li>                           
       
                                       </ul>
                                   </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt30 mt-md70">
                  <div class="f-24 f-md-50 w700 balck-clr2 lh140 text-center ">
                     That’s A Total Value of <br class="d-none d-md-block "> <span class="f-28 f-md-50 red-clr"> $2285</span>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--Bonus Section ends -->
<!-- License Section Start -->
<div class="license-section">
   <div class="container">
      <div class="row">

         <div class="col-12 text-center">
            <div class="w700 f-28 f-md-45 balck-clr2 text-center lh140">
               Also Get Our Free Commercial License When <br class="d-none d-md-block">
               You Get Access To VidMaster Today!
            </div>
         </div>
      </div>
      <div class="row align-items-center mt20 mt-md70">
         <div class="col-md-6 order-md-2">
            <div class="f-md-20 f-18 w400 lh140 balck-clr2">
               As we have shown you, there are tons of businesses & marketers – yoga gurus, fitness centers, social media marketers or anybody else who want to start online & Drive FREE Traffic, NEED YOUR SERVICE & would love pay you monthly for your services.<br><br>

               Build their branded & traffic generating video channels. We’ve taken care of everything so that you can deliver those services easily & 100% profits in your pocket.<br><br>
               
              <span class="w700"> Note:</span> This special commercial license is being included in the Advanced plan and ONLY during this launch. Take advantage of it now because it will never be offered again.
            </div>
         </div>
         <div class="col-md-6 col-10 mx-auto mt20 mt-md0 order-md-1">
            <img src="assets/images/commercial-license.webp" class="img-fluid d-block mx-auto" alt="Features">
         </div>
      </div>
   </div>
</div>
<!-- License Section End -->
  <!-- Guarantee Section Start -->
  <div class="riskfree-section ">
   <div class="container ">
      <div class="row align-items-center ">
         <div class="col-12 text-center mb20 mb-md40">
            <div class="f-24 f-md-36 w600 lh140 white-clr">
               We’re Backing Everything Up with An Iron Clad...
            </div>
            <div class="f-md-45 f-28 w700 lh140 white-clr">
               "30-Day Risk-Free Money Back Guarantee"
            </div>
         </div>
         <div class="col-md-7 col-12 order-md-2">
            <div class="f-md-20 f-18 w400 lh140 white-clr mt15">
               I'm 100% confident that VidMaster will help you take your online business to the next level. I'm so confident on it that I'm making this offer a risk-free investment for you.
<br><br>
               If you concluded that, HONESTLY nothing of this has helped you in any way, you can take advantage of our "30-day Money Back Guarantee" and simply ask for a refund within 30 days!
               <br><br>
               Note: For refund, we will require a valid reason along with the proof that you tried our system, but it didn't work for you!
               <br><br>
               I am considering your money to be kept safe on the table between us and waiting for you to APPLY and successfully use this product, and get best results, so then you can feel it is a great investment.
            </div>
         </div>
         <div class="col-md-5 order-md-1 col-12 mt20 mt-md0 ">
            <img src="assets/images/riskfree-img.webp " class="img-fluid d-block mx-auto ">
         </div>
      </div>
   </div>
</div>
<!-- Guarantee Section End -->

      <!-- Discounted Price Section Start -->
      <div class="table-section " id="buynow">
         <div class="container ">
            <div class="row ">
               <div class="col-12">
                  <div class="f-20 f-md-24 w500 text-center lh140">
                     Take Advantage of Our Limited Time Deal
                  </div>
                  <div class="f-md-48 f-28 w700 lh140 text-center mt10 ">
                     Get VidMaster For A Low One-Time-<br class="d-none d-md-block">
                     Price, No Monthly Fee.
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 text-center mt20 balck-clr">
                     Use Discount Coupon <span class="red-clr w700">"VidMaster"</span> for Instant <span class="red-clr w700">$3 OFF</span> on Commercial Plan
                 </div>
               </div>
               <div class="col-12 col-md-8 mx-auto mt20 mt-md50 ">
                  <div class="row ">
                     <div class="col-12 col-md-10 mx-auto mt20 mt-md0">
                        <div class="table-wrap1 ">
                           <div class="table-head1 ">
                           <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 858.2 150.9" style="enable-background:new 0 0 858.2 150.9; max-height: 60px;" xml:space="preserve">
                     <style type="text/css">
                        .st0{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_1_);}
                        .st1{fill-rule:evenodd;clip-rule:evenodd;fill:#3C4650;}
                        .st2{fill-rule:evenodd;clip-rule:evenodd;fill:#1C262D;}
                        .st3{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_2_);}
                        .st4{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_3_);}
                        .st5{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_4_);}
                        .st6{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_5_);}
                        .st7{fill:#FFFFFF;}
                     </style>
                     <g id="Layer_1-2">
                        <g>
                           <g>
                              <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="25.2183" y1="-779.9527" x2="12.9283" y2="-954.9427" gradientTransform="matrix(1 0 0 -1 0 -776)">
                                 <stop offset="0" style="stop-color:#CCCCCB"></stop>
                                 <stop offset="0.32" style="stop-color:#3C4650"></stop>
                                 <stop offset="0.7" style="stop-color:#252D33"></stop>
                                 <stop offset="1" style="stop-color:#181D20"></stop>
                              </linearGradient>
                              <path class="st0" d="M40.1,3.1c-14.4-6-22.8-3.9-29.9,10.8C-0.2,35.8-2.6,80.4,3,112.9c5.4,20.7,22.1,25.1,32.2,15
                              c0.7-0.3,1.5-0.6,2.1-0.9c-4.3,1.9-9.1-0.3-10.5-4.8c-7.4-23.6-6.2-70,0.3-91.8C30.9,17.5,39.4,4,40.1,3.1L40.1,3.1z"></path>
                              <path class="st1" d="M31.6,0.4C1.2,18.4,7.9,102.5,18.1,129.5c5.3,1.4,11.3,0.9,17-1.6c0.7-0.3,1.5-0.6,2.1-0.9
                              c-4.3,1.9-9.1-0.3-10.5-4.8c-7.4-23.6-4.8-64.4,1.9-88.3C32.9,18.4,39.3,4.6,40.1,3.1c-3.2-1.3-3.4-1.4-6.2-2.2L31.6,0.4
                              L31.6,0.4z"></path>
                              <path class="st2" d="M2.4,109.5c0.2,1.2,0.3,2.3,0.6,3.4c3.4,15.5,18.4,20.7,32.2,15c0.7-0.3,1.5-0.6,2.1-0.9
                              c-4.3,1.9-9.1-0.3-10.5-4.8C13.8,125.2,5.6,121.7,2.4,109.5L2.4,109.5z"></path>
                              <linearGradient id="SVGID_2_" gradientUnits="userSpaceOnUse" x1="-5.0837" y1="-921.3506" x2="166.8963" y2="-831.7707" gradientTransform="matrix(1 0 0 -1 0 -776)">
                                 <stop offset="0" style="stop-color:#CCCCCB"></stop>
                                 <stop offset="0.32" style="stop-color:#3C4650"></stop>
                                 <stop offset="0.7" style="stop-color:#252D33"></stop>
                                 <stop offset="1" style="stop-color:#181D20"></stop>
                              </linearGradient>
                              <path class="st3" d="M118.5,101.8c15-13.2,4.7-28.9-11.3-33.6c6.2,5.9,5.8,10.5-1.3,16.6c-10.3,8.8-22.8,17-34.3,24.1
                              c-11.4,6.9-24,13.9-36.3,19c-13.8,5.7-28.8,0.5-32.2-15c1.7,9.8,4.1,18.4,7.2,25c7.1,14.8,18.7,15.5,33.1,9.6
                              C65.7,138.2,99.7,117.5,118.5,101.8L118.5,101.8L118.5,101.8z"></path>
                              <path class="st1" d="M120.4,83.8c-14.2,18.1-62,46.8-80.9,55.3c-11.8,5.2-25.1,7.1-31.1-5.5c0.6,1.6,1.1,2.9,1.8,4.3
                              c7.1,14.8,18.7,15.5,33.1,9.6c22.4-9.3,56.5-30,75.2-45.6C124.2,96.5,123.7,90.1,120.4,83.8L120.4,83.8L120.4,83.8z"></path>
                              <linearGradient id="SVGID_3_" gradientUnits="userSpaceOnUse" x1="-9.377" y1="-913.095" x2="162.593" y2="-823.515" gradientTransform="matrix(1 0 0 -1 0 -776)">
                                 <stop offset="0" style="stop-color:#CCCCCB"></stop>
                                 <stop offset="0.32" style="stop-color:#4E4F4F"></stop>
                                 <stop offset="0.7" style="stop-color:#383838"></stop>
                                 <stop offset="1" style="stop-color:#231F20"></stop>
                              </linearGradient>
                              <path class="st4" d="M35.2,127.9c-13.8,5.7-28.8,0.5-32.2-15c0.2,1.2,0.4,2.5,0.7,3.7c4.3,15.7,18.5,20.2,33,13.8
                              c24.1-10.6,47-27.5,57.7-36.5c-7.4,5.4-15.3,10.4-22.8,15C60.1,115.8,47.5,122.8,35.2,127.9L35.2,127.9L35.2,127.9z"></path>
                              <path class="st2" d="M115.3,104.5c1.1-0.9,2.1-1.8,3.2-2.6c11-10.1-0.8-24.2-11.3-33.6c4.9,4.8,5.7,8.7,2.1,13.1
                              C115.6,85.6,123.2,97.3,115.3,104.5L115.3,104.5L115.3,104.5z"></path>
                              <linearGradient id="SVGID_4_" gradientUnits="userSpaceOnUse" x1="39.6241" y1="-837.5" x2="136.5" y2="-837.5" gradientTransform="matrix(1 0 0 -1 0 -776)">
                                 <stop offset="0" style="stop-color:#ED2024"></stop>
                                 <stop offset="1" style="stop-color:#F05757"></stop>
                              </linearGradient>
                              <path class="st5" d="M60,105.5c-3.4-18.2-3.5-36.9-0.7-55.1c2.6,15.8,8,31.4,15.4,46.2c8.1-5.2,16.1-10.7,23.5-16.8
                              c0.2-6.1,0.6-12,1.2-17.9c10.3,7.9,33,27.2,19.1,40c0.4-0.3,0.7-0.7,1.2-1c22.4-19.2,22.4-30.6,0-49.9
                              c-9.9-8.4-21.3-16.1-32.4-23c-3.1,12.2-5.1,24.7-6.1,37.3c-5-16.1-5.8-28-5.8-44.5c-7.5-4.3-15.2-8.5-23-12.3
                              c-13.2,33.6-16.3,70.8-8.7,106C49.2,111.7,54.6,108.7,60,105.5L60,105.5L60,105.5z"></path>
                              <linearGradient id="SVGID_5_" gradientUnits="userSpaceOnUse" x1="144.1735" y1="-864.6838" x2="14.8335" y2="-839.7738" gradientTransform="matrix(1 0 0 -1 0 -776)">
                                 <stop offset="0" style="stop-color:#ED2024"></stop>
                                 <stop offset="1" style="stop-color:#F05757"></stop>
                              </linearGradient>
                              <path class="st6" d="M99.4,61.9c10.3,8,33,27.2,19.1,40l1.2-1c6.8-5.8,6.8-13,1.9-20.5C116.3,72.4,106.1,65.4,99.4,61.9
                              L99.4,61.9L99.4,61.9z"></path>
                           </g>
                           <g>
                              <path class="st7" d="M834,63.8V50h-15.5h-8.2c3.2,0,5.4,2.2,5.7,4.5c0,4.4,0.1,9.4,0.1,15.6v54H834V90.6
                              c0-19.8,9.2-26.9,24.2-26.8V50.1C845.7,50.3,837.5,54.8,834,63.8L834,63.8z"></path>
                              <path class="st7" d="M573.3,106.1c0,6.9,0.6,16.3,1,18.2h-17.1c-0.6-1.5-1-5.3-1.1-8.1c-2.7,4.4-8,9.8-21.5,9.8
                              c-17.7,0-25.3-11.6-25.3-23.1c0-16.8,13.4-24.5,35.2-24.5h11.3v-5.1c0-5.7-2-11.8-12.9-11.8c-9.9,0-11.9,4.5-13,10h-17.1
                              c1.1-12.2,8.6-23.2,30.7-23.1c19.3,0.1,29.8,7.7,29.8,25.1L573.3,106.1L573.3,106.1z M555.8,89.6h-9.6
                              c-13.2,0-18.9,3.9-18.9,12.1c0,6.2,4,11.1,11.9,11.1c14.7,0,16.5-10.1,16.5-21.1L555.8,89.6L555.8,89.6z"></path>
                              <path class="st7" d="M745.2,90.8c0,11.2,5.7,20.8,16.7,20.8c9.6,0,12.5-4.3,14.6-9.1h18c-2.7,9.2-10.8,23.4-33.1,23.4
                              c-24,0-34.3-18.5-34.3-37.8c0-22.8,11.7-39.8,35-39.8c24.9,0,33.3,18.7,33.3,36.3c0,2.4,0,4.1-0.3,6.2H745.2L745.2,90.8z
                              M777.4,79.4c-0.2-9.8-4.5-18-15.4-18s-15.5,7.6-16.5,18H777.4L777.4,79.4z"></path>
                              <path class="st7" d="M606.8,102.3c1.8,6.6,7,10.4,15.4,10.4s12-3.4,12-8.6s-3.3-7.8-15.1-10.7c-23.2-5.7-27.3-12.8-27.3-23.2
                              s7.7-21.9,29-21.9s29,11.9,29.8,22h-17.1c-0.8-3.4-3.2-9.1-13.5-9.1c-8,0-10.5,3.7-10.5,7.5c0,4.3,2.5,6.4,15.1,9.4
                              c24,5.6,27.8,13.8,27.8,24.7c0,12.5-9.7,23.1-30.9,23.1s-30.7-10.5-32.5-23.7L606.8,102.3L606.8,102.3z"></path>
                              <path class="st7" d="M471,48.3c-12.3,0-18.9,5.6-23.1,12c-2.7-6.5-9-12-19.6-12c-11.2,0-17.3,5.5-20.9,11.4l0.1-9.7h-23.6
                              c3.4,0,5.8,2.5,5.8,5l0,0c0.1,4.7,0.1,9.4,0.1,14.1v55h17.7V82.9c0-13.2,4.5-19.8,14.1-19.8s11.9,7,11.9,15.3v45.8h17.6V81.7
                              c0-12,4-18.8,13.9-18.8s12.1,7.4,12.1,14.7v46.5h17.5V75.5C494.4,56,483.6,48.3,471,48.3L471,48.3z"></path>
                              <path class="st7" d="M368.8,103.6V26.4h-17.9h-5.8c3.4,0,5.8,2.5,5.8,5l0,0v24.8c-1.8-3.6-7.5-7.9-19-7.9
                              c-20.8,0-33.5,16.7-33.5,39.5s11.9,38.1,30.7,38.1c11.4,0,18.2-3.9,21.8-10.4l0.1,8.7h18C368.8,117.3,368.8,110.5,368.8,103.6
                              L368.8,103.6z M334,111.4c-10.6,0-17.2-8.4-17.2-24.1s6.3-24.4,17.9-24.4c14.5,0,16.9,9.9,16.9,23.8
                              C351.5,99.2,349.2,111.3,334,111.4L334,111.4z"></path>
                              <g>
                                 <path class="st7" d="M265.6,50h-5.8c3.4,0,5.8,2.5,5.8,5v69.2h17.9V50H265.6z"></path>
                                 <path class="st7" d="M283.5,29.1v-2.7h-17.9V47l0,0C275.5,46.9,283.5,38.9,283.5,29.1L283.5,29.1z"></path>
                              </g>
                              <g>
                                 <path class="st7" d="M712.3,63.8V50.1h-15.1l0,0h-17.9l0,0h-11.9v13.8h11.9v41.5c0,12.8,4.9,19.9,18.4,19.9
                                    c3.9,0,9.1-0.1,12.9-1.4v-12.6c-1.7,0.3-3.9,0.4-5.3,0.4c-6.3,0-8-2.7-8-8.8V63.8H712.3L712.3,63.8z"></path>
                                 <path class="st7" d="M697,29.1v-2.7h-17.9V47l0,0C689,46.9,697,38.9,697,29.1L697,29.1z"></path>
                              </g>
                              <path class="st7" d="M231.3,25.9l-16.3,47c-4.8,14-9.3,27.1-11.3,35.9h-0.3c-2.2-9.7-6.2-22.3-10.8-36.3L177,25.9h-25.7
                              c5.1,0,7.6,4.6,8.6,7.4l32.4,90.9h21.8l36.1-98.3L231.3,25.9L231.3,25.9z"></path>
                           </g>
                        </g>
                     </g>
                  </svg>
                              <div class="f-22 f-md-32 w700 lh140 text-center text-uppercase mt15 white-clr ">
                              Premium
                              </div>
                           </div>
                           <div class=" ">
                              <ul class="table-list1 pl0 f-18 f-md-20 lh140 w400 balck-clr2 mb0">
                                 
                              <li>Create Unlimited Reels & Shorts </li>
                              <li>Create Reels & Short Videos Just by Using One Keyword  </li>
                              <li>Create Boomerang Short Videos to Engage your Audience </li>
                              <li>Create Picture Video by Using Keyword Search  </li>
                              <li>Create Video Using Your Own Video Clips or Stock Videos </li>
                              <li>Create High-Quality Explanatory Whiteboard Video Shorts </li>
                              <li>Add VoiceOver to any Video</li>
                              <li>Add Background Music to any Video  </li>
                              <li>50 + Short frame Templates and 25+ Vector Images</li>
                              <li>100+ Stylish Fonts to make video more Presentable  </li>
                              <li>Eye Catchy Header Text Style to grab attention  </li>
                              <li>Add Waves with different color to your Short videos  </li>
                              <li>100+ social sharing platforms to share the videos for viral traffic </li>
                              <li>Add Your Brand Logo and/or Watermark to your Videos </li>
                              <li>Share Videos Directly to YouTube Shorts </li>
                              <li>Store up to 2 GB of Video, Audio, and other media files  </li>
                              <li>24*7 Customer Support</li>
                              <li>Commercial License Included</li>
                              <li>Provide High In-Demand Video Creation Services to your Clients </li>
                                 <li class="headline ">BONUSES WORTH $1,988 FREE!!!</li>
                                 <li>Fast Action Bonus 1 - DotcomPal</li>
                                 <li>Fast Action Bonus 2 - Viral Marketing Secrets</li>
                                 <li>Fast Action Bonus 3 - Modern Video Marketing</li>
                                 <li>Fast Action Bonus 4 - Youtube Authority</li>
                              </ul>
                           </div>
                           <div class="table-btn">
                              <div class="hideme-button ">
                                 <a href="https://warriorplus.com/o2/buy/jrrjv5/pdmlv2/ypzl19"><img src="https://warriorplus.com/o2/btn/fn200011000/jrrjv5/pdmlv2/332890" class="img-fluid mx-auto d-block"></a> 
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- <div class="col-12 mt30 ">
                  <div class="f-md-22 f-20 w400 lh140 text-center ">
                     *Commercial License Is ONLY Available With The Purchase Of Commercial Plan
                  </div>
               </div> -->
            </div>
         </div>
      </div>
      <!-- Discounted Price Section End -->
    
       <!--------To Your Awesome Section ----------->
       <div class="awesome-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-18 f-md-18 w400 lh140 white-clr text-xs-center">
                     <span class="w700">You can agree that the price we're asking is extremely low.</span> The price is rising with every few hours, so it won't be long until it's more than double what it is today!<br><br> We could easily charge hundreds of dollars for a revolutionary tool like this, but we want to offer you an attractive and affordable price that will finally help you build super engaging video channels in the best possible way - without wasting tons of money!
                     <br><br>  So, take action now... and I promise you won't be disappointed!
                  </div>
               </div>
              
               <div class="col-12 w700 f-md-28 f-22 text-start mt20 mt-md70 red-clr">To your success</div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
               <div class="row">
                        <div class="col-md-6 col-12 text-center mt20 mt-md0">
                           <img src="assets/images/pranshu-sir.webp" class="img-fluid d-block mx-auto " alt="Pranshu Gupta" style="max-height: 200px;"> 
                           <div class="f-24 f-md-24 w700 lh140 text-center white-clr mt20">
                              Pranshu Gupta
                           </div>
                        </div>
                        <div class="col-md-6 col-12 text-center mt30 mt-md0">
                           <img src="assets/images/cp-sir.webp" class="img-fluid d-block mx-auto " alt="Ayush Jain" style="max-height: 200px;">
                           <div class="f-24 f-md-24 w700 lh140 white-clr mt20">
                              Chandraprakash Kalwar
                           </div>
                        </div>
                     </div>
               </div>
               <div class="col-12 mt20 mt-md50">
                  <div class="f-18 f-md-18 w400 lh140 white-clr">
                     <span class="w700">P.S- You can try "VIDMASTER" for 30 days without any worries.</span><br><br> Listen, we know there are a lot of crappy software tools out there that will get you nowhere. Most of the software is overpriced and an absolute waste of money. So, if you're a bit sceptical, that's perfectly fine. I'm so sure you'll see the potential of this ground-breaking software that I'll let you try it out 100% risk-free.

                     <br><br> Just test it for 30 days and if you're not able to build jaw-dropping video channels packed with video content and we cannot help you in any way, you will be eligible for a refund - no tricks, no hassles.
                     <br><br>
                     <span class="w700">P.S.S Don't Procrastinate - Take Action NOW! Get your copy of VIDMASTER!  
                     </span><br><br> By now you should be really excited about all the wonderful benefits of such an amazing piece of software. You don't want to miss out on the wonderful opportunity presented today... And then regret later when it costs more than double... or it's even completely off the market!
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--------To Your Awesome Section End----------->
      <!--------Faq Section------------->
      <div class="faq-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-28 f-md-48 lh140 w700 text-center balck-clr2">
                  Frequently Asked <span class="red-clr">Questions</span>
               </div>
               <div class="col-12 mt20 mt-md30 ">
                  <div class="row">
                     <div class="col-md-6 col-12 ">
                        <div class="faq-list ">
                           <div class="f-md-22 f-20 w700 balck-clr2 lh140 ">
                              What exactly VidMaster is all about?
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-left ">
                              VidMaster is the ultimate push-button technology that creates engagin video shorts, drives FREE viral traffic & makes handsfree commissions, ad profits & sales.
                           </div>
                        </div>
                        <div class="faq-list ">
                           <div class="f-md-22 f-20 w700 balck-clr2 lh140">
                              Is my investment risk free?
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-left ">
                              We know the worth of your money. You can be rest assured that your investment is as safe as houses. However, we would like to clearly state that we don’t offer a no questions asked money back guarantee. You must provide a genuine reason and show us proof that you did everything before asking for a refund.
                           </div>
                        </div>
                        <div class="faq-list ">
                           <div class="f-md-22 f-20 w700 balck-clr2 lh140 ">
                              Do I have to install VidMaster?
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-left ">
                              NO! VidMaster is fully cloud based. Just create an account and you can get started immediately online. It is 100% web-based platform hosted on the cloud. This means you never have to download anything ever. You can access it at any time from any device that has an internet connection.
                           </div>
                        </div>
                        <div class="faq-list ">
                           <div class="f-md-22 f-20 w700 balck-clr2 lh140">
                              Is it ‘Newbie Friendly’?
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-left ">
                              Yep, my friend, VidMaster is 100% newbie friendly. We know that there are a lot of technical hassles that most software has, but our software is a cut above the rest, and everyone can use it with complete ease.
                           </div>
                        </div>
                        <div class="faq-list ">
                           <div class="f-md-22 f-20 w700 balck-clr2 lh140">
                              Do you charge any monthly fees?
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-left ">
                              NOT AT ALL. There are NO monthly fees to use VidMaster during the launch period. During this period, you pay once and never again. We always believe in providing complete value for your money.
                           </div>
                        </div>
                     </div>
                     <div class="col-md-6 col-12 ">
                        <div class="faq-list ">
                           <div class="f-md-22 f-20 w700 balck-clr2 lh140">
                              Is VidMaster easy to use?
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-left ">
                              I bet you it’s the easiest tool you might have seen so far. Our #1 priority during the development of the software was to make it simple and easy for everyone. There is nothing to install, just create your account and login to take a plunge into hugely profitable video marketing arena.
                           </div>
                        </div>
                        <div class="faq-list ">
                           <div class="f-md-22 f-20 w700 balck-clr2 lh140">
                              How do I know how well my campaigns are working?
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-left ">
                              You can see real-time reports for your campaigns in your account dashboard and make changes accordingly. But there’s a small catch, you need to upgrade your purchase to use these benefits.
                           </div>
                        </div>
                        <div class="faq-list ">
                           <div class="f-md-22 f-20 w700 balck-clr2 lh140">
                              Will I able to drive hordes of viral traffic from day one?
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-left ">
                              Well, that depends on how well you make the use of this ultimate software. We’ve created this from grounds up to make everything simple and easy and ensure that you move ahead without any hassles.
                           </div>
                        </div>
                        <div class="faq-list ">
                           <div class="f-md-22 f-20 w700 balck-clr2 lh140">
                              Will I get any training or support?
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-left ">
                              YES. We made detailed and step-by-step training videos that show you every step of how to get setup and you can access them in the member’s area.
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50 ">
                  <div class="f-22 f-md-32 w700 balck-clr2 px0 text-md-center lh140 ">If you have any query, simply reach to us at <a href="mailto:support@bizomart.com" class="red-clr">support@bizomart.com</a>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--------Faq Section End------------->
      <!------Final Section------->
      <div class="final-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="but-design-border">
                     <div class="f-md-45 f-28 lh140 w700 balck-clr2 button-shape">
                        FINAL CALL
                     </div>
                  </div>
               </div>
               <div class="col-12 passport-content">
                  <div class="col-12 f-24 f-md-32 lh140 w600 text-center balck-clr2 mt20">
                     You Still Have the Opportunity to Raise Your Game… <br> Remember, It’s Your Make-or-Break Time! 
                  </div>
               </div>
            </div>
            <!-- CTA Btn Section Start -->
            <div class="row mt20 mt-md40">
               <div class="col-12 text-center">
                  <div class="f-18 f-md-26 w700 lh140 text-center balck-clr2">
                     Free Commercial License + Low 1-Time Price For Launch Period Only
                  </div>
                  <div class="f-20 f-md-24 w400 lh140 text-center mt20 balck-clr2">
                     Use Discount Coupon <span class="red-clr w700">"VidMaster"</span> for Instant <span class="red-clr w700">$3 OFF</span>
                 </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center">
                        <a href="#buynow" class="cta-link-btn">Get Instant Access To VidMaster</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                     <img src="assets/images/compaitable-with.webp" class="img-responsive mx-xs-center md-img-right" alt="visa">
                     <div class="d-md-block d-none visible-md px-md30">
                        <img src="assets/images/v-line.webp" class="img-fluid" alt="line">
                     </div>
                     <img src="assets/images/days-gurantee.webp" class="img-responsive mx-xs-auto mt15 mt-md0" alt="30 days">
                  </div>
               </div>
            </div>
            <!-- CTA Btn Section End -->
         </div>
      </div>
      <!------Final Section End------->

      <!------Footer Section-------->
      <div class="footer-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 858.2 150.9" style="enable-background:new 0 0 858.2 150.9; max-height: 60px;" xml:space="preserve">
                     <style type="text/css">
                        .st0{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_1_);}
                        .st1{fill-rule:evenodd;clip-rule:evenodd;fill:#3C4650;}
                        .st2{fill-rule:evenodd;clip-rule:evenodd;fill:#1C262D;}
                        .st3{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_2_);}
                        .st4{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_3_);}
                        .st5{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_4_);}
                        .st6{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_5_);}
                        .st7{fill:#FFFFFF;}
                     </style>
                     <g id="Layer_1-2">
                        <g>
                           <g>
                              <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="25.2183" y1="-779.9527" x2="12.9283" y2="-954.9427" gradientTransform="matrix(1 0 0 -1 0 -776)">
                                 <stop offset="0" style="stop-color:#CCCCCB"></stop>
                                 <stop offset="0.32" style="stop-color:#3C4650"></stop>
                                 <stop offset="0.7" style="stop-color:#252D33"></stop>
                                 <stop offset="1" style="stop-color:#181D20"></stop>
                              </linearGradient>
                              <path class="st0" d="M40.1,3.1c-14.4-6-22.8-3.9-29.9,10.8C-0.2,35.8-2.6,80.4,3,112.9c5.4,20.7,22.1,25.1,32.2,15
                              c0.7-0.3,1.5-0.6,2.1-0.9c-4.3,1.9-9.1-0.3-10.5-4.8c-7.4-23.6-6.2-70,0.3-91.8C30.9,17.5,39.4,4,40.1,3.1L40.1,3.1z"></path>
                              <path class="st1" d="M31.6,0.4C1.2,18.4,7.9,102.5,18.1,129.5c5.3,1.4,11.3,0.9,17-1.6c0.7-0.3,1.5-0.6,2.1-0.9
                              c-4.3,1.9-9.1-0.3-10.5-4.8c-7.4-23.6-4.8-64.4,1.9-88.3C32.9,18.4,39.3,4.6,40.1,3.1c-3.2-1.3-3.4-1.4-6.2-2.2L31.6,0.4
                              L31.6,0.4z"></path>
                              <path class="st2" d="M2.4,109.5c0.2,1.2,0.3,2.3,0.6,3.4c3.4,15.5,18.4,20.7,32.2,15c0.7-0.3,1.5-0.6,2.1-0.9
                              c-4.3,1.9-9.1-0.3-10.5-4.8C13.8,125.2,5.6,121.7,2.4,109.5L2.4,109.5z"></path>
                              <linearGradient id="SVGID_2_" gradientUnits="userSpaceOnUse" x1="-5.0837" y1="-921.3506" x2="166.8963" y2="-831.7707" gradientTransform="matrix(1 0 0 -1 0 -776)">
                                 <stop offset="0" style="stop-color:#CCCCCB"></stop>
                                 <stop offset="0.32" style="stop-color:#3C4650"></stop>
                                 <stop offset="0.7" style="stop-color:#252D33"></stop>
                                 <stop offset="1" style="stop-color:#181D20"></stop>
                              </linearGradient>
                              <path class="st3" d="M118.5,101.8c15-13.2,4.7-28.9-11.3-33.6c6.2,5.9,5.8,10.5-1.3,16.6c-10.3,8.8-22.8,17-34.3,24.1
                              c-11.4,6.9-24,13.9-36.3,19c-13.8,5.7-28.8,0.5-32.2-15c1.7,9.8,4.1,18.4,7.2,25c7.1,14.8,18.7,15.5,33.1,9.6
                              C65.7,138.2,99.7,117.5,118.5,101.8L118.5,101.8L118.5,101.8z"></path>
                              <path class="st1" d="M120.4,83.8c-14.2,18.1-62,46.8-80.9,55.3c-11.8,5.2-25.1,7.1-31.1-5.5c0.6,1.6,1.1,2.9,1.8,4.3
                              c7.1,14.8,18.7,15.5,33.1,9.6c22.4-9.3,56.5-30,75.2-45.6C124.2,96.5,123.7,90.1,120.4,83.8L120.4,83.8L120.4,83.8z"></path>
                              <linearGradient id="SVGID_3_" gradientUnits="userSpaceOnUse" x1="-9.377" y1="-913.095" x2="162.593" y2="-823.515" gradientTransform="matrix(1 0 0 -1 0 -776)">
                                 <stop offset="0" style="stop-color:#CCCCCB"></stop>
                                 <stop offset="0.32" style="stop-color:#4E4F4F"></stop>
                                 <stop offset="0.7" style="stop-color:#383838"></stop>
                                 <stop offset="1" style="stop-color:#231F20"></stop>
                              </linearGradient>
                              <path class="st4" d="M35.2,127.9c-13.8,5.7-28.8,0.5-32.2-15c0.2,1.2,0.4,2.5,0.7,3.7c4.3,15.7,18.5,20.2,33,13.8
                              c24.1-10.6,47-27.5,57.7-36.5c-7.4,5.4-15.3,10.4-22.8,15C60.1,115.8,47.5,122.8,35.2,127.9L35.2,127.9L35.2,127.9z"></path>
                              <path class="st2" d="M115.3,104.5c1.1-0.9,2.1-1.8,3.2-2.6c11-10.1-0.8-24.2-11.3-33.6c4.9,4.8,5.7,8.7,2.1,13.1
                              C115.6,85.6,123.2,97.3,115.3,104.5L115.3,104.5L115.3,104.5z"></path>
                              <linearGradient id="SVGID_4_" gradientUnits="userSpaceOnUse" x1="39.6241" y1="-837.5" x2="136.5" y2="-837.5" gradientTransform="matrix(1 0 0 -1 0 -776)">
                                 <stop offset="0" style="stop-color:#ED2024"></stop>
                                 <stop offset="1" style="stop-color:#F05757"></stop>
                              </linearGradient>
                              <path class="st5" d="M60,105.5c-3.4-18.2-3.5-36.9-0.7-55.1c2.6,15.8,8,31.4,15.4,46.2c8.1-5.2,16.1-10.7,23.5-16.8
                              c0.2-6.1,0.6-12,1.2-17.9c10.3,7.9,33,27.2,19.1,40c0.4-0.3,0.7-0.7,1.2-1c22.4-19.2,22.4-30.6,0-49.9
                              c-9.9-8.4-21.3-16.1-32.4-23c-3.1,12.2-5.1,24.7-6.1,37.3c-5-16.1-5.8-28-5.8-44.5c-7.5-4.3-15.2-8.5-23-12.3
                              c-13.2,33.6-16.3,70.8-8.7,106C49.2,111.7,54.6,108.7,60,105.5L60,105.5L60,105.5z"></path>
                              <linearGradient id="SVGID_5_" gradientUnits="userSpaceOnUse" x1="144.1735" y1="-864.6838" x2="14.8335" y2="-839.7738" gradientTransform="matrix(1 0 0 -1 0 -776)">
                                 <stop offset="0" style="stop-color:#ED2024"></stop>
                                 <stop offset="1" style="stop-color:#F05757"></stop>
                              </linearGradient>
                              <path class="st6" d="M99.4,61.9c10.3,8,33,27.2,19.1,40l1.2-1c6.8-5.8,6.8-13,1.9-20.5C116.3,72.4,106.1,65.4,99.4,61.9
                              L99.4,61.9L99.4,61.9z"></path>
                           </g>
                           <g>
                              <path class="st7" d="M834,63.8V50h-15.5h-8.2c3.2,0,5.4,2.2,5.7,4.5c0,4.4,0.1,9.4,0.1,15.6v54H834V90.6
                              c0-19.8,9.2-26.9,24.2-26.8V50.1C845.7,50.3,837.5,54.8,834,63.8L834,63.8z"></path>
                              <path class="st7" d="M573.3,106.1c0,6.9,0.6,16.3,1,18.2h-17.1c-0.6-1.5-1-5.3-1.1-8.1c-2.7,4.4-8,9.8-21.5,9.8
                              c-17.7,0-25.3-11.6-25.3-23.1c0-16.8,13.4-24.5,35.2-24.5h11.3v-5.1c0-5.7-2-11.8-12.9-11.8c-9.9,0-11.9,4.5-13,10h-17.1
                              c1.1-12.2,8.6-23.2,30.7-23.1c19.3,0.1,29.8,7.7,29.8,25.1L573.3,106.1L573.3,106.1z M555.8,89.6h-9.6
                              c-13.2,0-18.9,3.9-18.9,12.1c0,6.2,4,11.1,11.9,11.1c14.7,0,16.5-10.1,16.5-21.1L555.8,89.6L555.8,89.6z"></path>
                              <path class="st7" d="M745.2,90.8c0,11.2,5.7,20.8,16.7,20.8c9.6,0,12.5-4.3,14.6-9.1h18c-2.7,9.2-10.8,23.4-33.1,23.4
                              c-24,0-34.3-18.5-34.3-37.8c0-22.8,11.7-39.8,35-39.8c24.9,0,33.3,18.7,33.3,36.3c0,2.4,0,4.1-0.3,6.2H745.2L745.2,90.8z
                              M777.4,79.4c-0.2-9.8-4.5-18-15.4-18s-15.5,7.6-16.5,18H777.4L777.4,79.4z"></path>
                              <path class="st7" d="M606.8,102.3c1.8,6.6,7,10.4,15.4,10.4s12-3.4,12-8.6s-3.3-7.8-15.1-10.7c-23.2-5.7-27.3-12.8-27.3-23.2
                              s7.7-21.9,29-21.9s29,11.9,29.8,22h-17.1c-0.8-3.4-3.2-9.1-13.5-9.1c-8,0-10.5,3.7-10.5,7.5c0,4.3,2.5,6.4,15.1,9.4
                              c24,5.6,27.8,13.8,27.8,24.7c0,12.5-9.7,23.1-30.9,23.1s-30.7-10.5-32.5-23.7L606.8,102.3L606.8,102.3z"></path>
                              <path class="st7" d="M471,48.3c-12.3,0-18.9,5.6-23.1,12c-2.7-6.5-9-12-19.6-12c-11.2,0-17.3,5.5-20.9,11.4l0.1-9.7h-23.6
                              c3.4,0,5.8,2.5,5.8,5l0,0c0.1,4.7,0.1,9.4,0.1,14.1v55h17.7V82.9c0-13.2,4.5-19.8,14.1-19.8s11.9,7,11.9,15.3v45.8h17.6V81.7
                              c0-12,4-18.8,13.9-18.8s12.1,7.4,12.1,14.7v46.5h17.5V75.5C494.4,56,483.6,48.3,471,48.3L471,48.3z"></path>
                              <path class="st7" d="M368.8,103.6V26.4h-17.9h-5.8c3.4,0,5.8,2.5,5.8,5l0,0v24.8c-1.8-3.6-7.5-7.9-19-7.9
                              c-20.8,0-33.5,16.7-33.5,39.5s11.9,38.1,30.7,38.1c11.4,0,18.2-3.9,21.8-10.4l0.1,8.7h18C368.8,117.3,368.8,110.5,368.8,103.6
                              L368.8,103.6z M334,111.4c-10.6,0-17.2-8.4-17.2-24.1s6.3-24.4,17.9-24.4c14.5,0,16.9,9.9,16.9,23.8
                              C351.5,99.2,349.2,111.3,334,111.4L334,111.4z"></path>
                              <g>
                                 <path class="st7" d="M265.6,50h-5.8c3.4,0,5.8,2.5,5.8,5v69.2h17.9V50H265.6z"></path>
                                 <path class="st7" d="M283.5,29.1v-2.7h-17.9V47l0,0C275.5,46.9,283.5,38.9,283.5,29.1L283.5,29.1z"></path>
                              </g>
                              <g>
                                 <path class="st7" d="M712.3,63.8V50.1h-15.1l0,0h-17.9l0,0h-11.9v13.8h11.9v41.5c0,12.8,4.9,19.9,18.4,19.9
                                    c3.9,0,9.1-0.1,12.9-1.4v-12.6c-1.7,0.3-3.9,0.4-5.3,0.4c-6.3,0-8-2.7-8-8.8V63.8H712.3L712.3,63.8z"></path>
                                 <path class="st7" d="M697,29.1v-2.7h-17.9V47l0,0C689,46.9,697,38.9,697,29.1L697,29.1z"></path>
                              </g>
                              <path class="st7" d="M231.3,25.9l-16.3,47c-4.8,14-9.3,27.1-11.3,35.9h-0.3c-2.2-9.7-6.2-22.3-10.8-36.3L177,25.9h-25.7
                              c5.1,0,7.6,4.6,8.6,7.4l32.4,90.9h21.8l36.1-98.3L231.3,25.9L231.3,25.9z"></path>
                           </g>
                        </g>
                     </g>
                  </svg>
                  <div editabletype="text" class="f-16 f-md-18 w400 lh140 mt20 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
                  <div class=" mt20 mt-md40 white-clr text-center"> <script type="text/javascript" src="https://warriorplus.com/o2/disclaimer/jrrjv5" defer=""></script><div class="wplus_spdisclaimer f-16 f-md-16 w400 lh140 white-clr"><span class="w600">Disclaimer : </span> WarriorPlus is used to help manage the sale of products on this site. While WarriorPlus helps facilitate the sale, all payments are made directly to the product vendor and NOT WarriorPlus. Thus, all product questions, support inquiries and/or refund requests must be sent to the vendor. WarriorPlus's role should not be construed as an endorsement, approval or review of these products or any claim, statement or opinion used in the marketing of these products.</div></div>
              </div>
              <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-16 f-md-18 w400 lh140 white-clr text-xs-center">Copyright © VidMaster</div>
                  <ul class="footer-ul f-16 f-md-18 w400 white-clr text-center text-md-right">
                      <li><a href="https://support.bizomart.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                      <li><a href="http://www.vidmaster.co/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                      <li><a href="http://www.vidmaster.co/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                      <li><a href="http://www.vidmaster.co/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                      <li><a href="http://www.vidmaster.co/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                      <li><a href="http://www.vidmaster.co/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                      <li><a href="http://www.vidmaster.co/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
              </div>
          </div>
      </div>
  </div>
      <!------Footer Section End-------->
         <!-- Exit Popup and Timer -->
   <?php include('timer.php'); ?>
   <!-- Exit Popup and Timer End -->
   </body>
</html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
   <!-- Tell the browser to be responsive to screen width -->
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
   <meta name="title" content="VidMaster Special Bonuses">
   <meta name="description" content="Game-Changer AI APP Creates Stories, Reels, Boomerang, & Short Videos With Just One Keyword… For Floods Of FREE Traffic And Sales">
   <meta name="keywords" content="">
   <meta property="og:image" content="https://www.vidmaster.co/special-bonus/thumbnail.png">
   <meta name="language" content="English">
   <meta name="revisit-after" content="1 days">
   <meta name="author" content="Dr. Amit Pareek">
   <!-- Open Graph / Facebook -->
   <meta property="og:type" content="website">
   <meta property="og:title" content="VidMaster Special Bonuses">
   <meta property="og:description" content="Game-Changer AI APP Creates Stories, Reels, Boomerang, & Short Videos With Just One Keyword… For Floods Of FREE Traffic And Sales">
   <meta property="og:image" content="https://www.vidmaster.co/special-bonus/thumbnail.png">
   <!-- Twitter -->
   <meta property="twitter:card" content="summary_large_image">
   <meta property="twitter:title" content="VidMaster Special Bonuses">
   <meta property="twitter:description" content="Game-Changer AI APP Creates Stories, Reels, Boomerang, & Short Videos With Just One Keyword… For Floods Of FREE Traffic And Sales">
   <meta property="twitter:image" content="https://www.vidmaster.co/special-bonus/thumbnail.png">
   <title>VidMaster Special Bonuses</title>

   <link rel="preconnect" href="https://fonts.googleapis.com">
   <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
   <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
   
   <!-- Shortcut Icon  -->
   <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
   <!-- Css CDN Load Link -->
   <link rel="stylesheet" href="../common_assets/css/bootstrap.min.css" type="text/css">
   <link rel="stylesheet" type="text/css" href="assets/css/bonus-style.css">
   <link rel="stylesheet" href="assets/css/style.css" type="text/css">
   <script src="../common_assets/js/jquery.min.js"></script>
</head>

<body>
   <!-- New Timer  Start-->
   <?php
   $date = 'Jan 25 2023 11:00 AM EST';
   $exp_date = strtotime($date);
   $now = time();
   /*
         
         $date = date('F d Y g:i:s A eO');
         $rand_time_add = rand(700, 1200);
         $exp_date = strtotime($date) + $rand_time_add;
         $now = time();*/

   if ($now < $exp_date) {
   ?>
   <?php
   } else {
      echo "Times Up";
   }
   ?>
   <!-- New Timer End -->
   <?php
   if (!isset($_GET['afflink'])) {
      $_GET['afflink'] = ' https://warriorplus.com/o2/a/vpt9gc/0';
      $_GET['name'] = 'Dr. Amit Pareek';
   }
   ?>
<div class="header-nav">
        <div class="conatiner">
            <div class="row">
                <div class="col-12 text-center">
                <div class="text-center">
                  <div class="f-md-32 f-20 lh140 w500 black-clr d-block d-md-flex align-items-center justify-content-center">
                     <span class="w600 red-clr"><?php echo $_GET['name']; ?>'s</span> &nbsp;special bonus for &nbsp;
                     <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 858.2 150.9" style="enable-background:new 0 0 858.2 150.9; max-height: 55px;" xml:space="preserve">
                        <style type="text/css">
                           .st0a{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_1_);}
                           .st1a{fill-rule:evenodd;clip-rule:evenodd;fill:#3C4650;}
                           .st2a{fill-rule:evenodd;clip-rule:evenodd;fill:#1C262D;}
                           .st3a{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_2_);}
                           .st4a{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_3_);}
                           .st5a{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_4_);}
                           .st6a{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_5_);}
                           .st7a{fill:#1C262D;}
                        </style>
                        <g id="Layer_1-2">
                           <g>
                              <g>
                                 
                                    <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="25.1812" y1="148.0499" x2="12.8912" y2="-26.9401" gradientTransform="matrix(1 0 0 -1 0 152)">
                                    <stop offset="0" style="stop-color:#CCCCCB"></stop>
                                    <stop offset="0.32" style="stop-color:#3C4650"></stop>
                                    <stop offset="0.7" style="stop-color:#252D33"></stop>
                                    <stop offset="1" style="stop-color:#181D20"></stop>
                                 </linearGradient>
                                 <path class="st0a" d="M40.1,3.1c-14.4-6-22.8-3.9-29.9,10.8c-10.4,21.9-12.8,66.5-7.2,99c5.4,20.7,22.1,25.1,32.2,15
                                    c0.7-0.3,1.5-0.6,2.1-0.9c-4.3,1.9-9.1-0.3-10.5-4.8c-7.4-23.6-6.2-70,0.3-91.8C30.9,17.5,39.4,4,40.1,3.1L40.1,3.1z"></path>
                                 <path class="st1" d="M31.6,0.4C1.2,18.4,7.9,102.5,18.1,129.5c5.3,1.4,11.3,0.9,17-1.6c0.7-0.3,1.5-0.6,2.1-0.9
                                    c-4.3,1.9-9.1-0.3-10.5-4.8c-7.4-23.6-4.8-64.4,1.9-88.3C32.9,18.4,39.3,4.6,40.1,3.1c-3.2-1.3-3.4-1.4-6.2-2.2L31.6,0.4
                                    L31.6,0.4z"></path>
                                 <path class="st2a" d="M2.4,109.5c0.2,1.2,0.3,2.3,0.6,3.4c3.4,15.5,18.4,20.7,32.2,15c0.7-0.3,1.5-0.6,2.1-0.9
                                    c-4.3,1.9-9.1-0.3-10.5-4.8C13.8,125.2,5.6,121.7,2.4,109.5C2.4,109.5,2.4,109.5,2.4,109.5z"></path>
                                 
                                    <linearGradient id="SVGID_2_" gradientUnits="userSpaceOnUse" x1="-5.1028" y1="6.6861" x2="166.8772" y2="96.2661" gradientTransform="matrix(1 0 0 -1 0 152)">
                                    <stop offset="0" style="stop-color:#CCCCCB"></stop>
                                    <stop offset="0.32" style="stop-color:#3C4650"></stop>
                                    <stop offset="0.7" style="stop-color:#252D33"></stop>
                                    <stop offset="1" style="stop-color:#181D20"></stop>
                                 </linearGradient>
                                 <path class="st3a" d="M118.5,101.8c15-13.2,4.7-28.9-11.3-33.6c6.2,5.9,5.8,10.5-1.3,16.6c-10.3,8.8-22.8,17-34.3,24.1
                                    c-11.4,6.9-24,13.9-36.3,19c-13.8,5.7-28.8,0.5-32.2-15c1.7,9.8,4.1,18.4,7.2,25c7.1,14.8,18.7,15.5,33.1,9.6
                                    C65.7,138.2,99.7,117.5,118.5,101.8L118.5,101.8L118.5,101.8z"></path>
                                 <path class="st1a" d="M120.4,83.8c-14.2,18.1-62,46.8-80.9,55.3c-11.8,5.2-25.1,7.1-31.1-5.5c0.6,1.6,1.1,2.9,1.8,4.3
                                    c7.1,14.8,18.7,15.5,33.1,9.6c22.4-9.3,56.5-30,75.2-45.6C124.2,96.5,123.7,90.1,120.4,83.8L120.4,83.8L120.4,83.8z"></path>
                                 
                                    <linearGradient id="SVGID_3_" gradientUnits="userSpaceOnUse" x1="-9.3881" y1="14.9264" x2="162.5819" y2="104.5064" gradientTransform="matrix(1 0 0 -1 0 152)">
                                    <stop offset="0" style="stop-color:#CCCCCB"></stop>
                                    <stop offset="0.32" style="stop-color:#4E4F4F"></stop>
                                    <stop offset="0.7" style="stop-color:#383838"></stop>
                                    <stop offset="1" style="stop-color:#231F20"></stop>
                                 </linearGradient>
                                 <path class="st4a" d="M35.2,127.9c-13.8,5.7-28.8,0.5-32.2-15c0.2,1.2,0.4,2.5,0.7,3.7c4.3,15.7,18.5,20.2,33,13.8
                                    c24.1-10.6,47-27.5,57.7-36.5c-7.4,5.4-15.3,10.4-22.8,15C60.1,115.8,47.5,122.8,35.2,127.9L35.2,127.9L35.2,127.9z"></path>
                                 <path class="st2" d="M115.3,104.5c1.1-0.9,2.1-1.8,3.2-2.6c11-10.1-0.8-24.2-11.3-33.6c4.9,4.8,5.7,8.7,2.1,13.1
                                    C115.6,85.6,123.2,97.3,115.3,104.5L115.3,104.5L115.3,104.5z"></path>
                                 
                                    <linearGradient id="SVGID_4_" gradientUnits="userSpaceOnUse" x1="39.5473" y1="90.515" x2="136.3825" y2="90.515" gradientTransform="matrix(1 0 0 -1 0 152)">
                                    <stop offset="0" style="stop-color:#ED2024"></stop>
                                    <stop offset="1" style="stop-color:#F05757"></stop>
                                 </linearGradient>
                                 <path class="st5" d="M60,105.5c-3.4-18.2-3.5-36.9-0.7-55.1c2.6,15.8,8,31.4,15.4,46.2c8.1-5.2,16.1-10.7,23.5-16.8
                                    c0.2-6.1,0.6-12,1.2-17.9c10.3,7.9,33,27.2,19.1,40c0.4-0.3,0.7-0.7,1.2-1c22.4-19.2,22.4-30.6,0-49.9
                                    c-9.9-8.4-21.3-16.1-32.4-23c-3.1,12.2-5.1,24.7-6.1,37.3c-5-16.1-5.8-28-5.8-44.5c-7.5-4.3-15.2-8.5-23-12.3
                                    c-13.2,33.6-16.3,70.8-8.7,106C49.2,111.7,54.6,108.7,60,105.5L60,105.5L60,105.5z"></path>
                                 
                                    <linearGradient id="SVGID_5_" gradientUnits="userSpaceOnUse" x1="144.1806" y1="63.3529" x2="14.8406" y2="88.2629" gradientTransform="matrix(1 0 0 -1 0 152)">
                                    <stop offset="0" style="stop-color:#ED2024"></stop>
                                    <stop offset="1" style="stop-color:#F05757"></stop>
                                 </linearGradient>
                                 <path class="st6a" d="M99.4,61.9c10.3,8,33,27.2,19.1,40l1.2-1c6.8-5.8,6.8-13,1.9-20.5C116.3,72.4,106.1,65.4,99.4,61.9
                                    L99.4,61.9L99.4,61.9z"></path>
                              </g>
                              <g>
                                 <path class="st7a" d="M834,63.8V50h-15.5h-8.2c3.2,0,5.4,2.2,5.7,4.5c0,4.4,0.1,9.4,0.1,15.6v54H834V90.6
                                    c0-19.8,9.2-26.9,24.2-26.8V50.1C845.7,50.3,837.5,54.8,834,63.8L834,63.8z"></path>
                                 <path class="st7a" d="M573.3,106.1c0,6.9,0.6,16.3,1,18.2h-17.1c-0.6-1.5-1-5.3-1.1-8.1c-2.7,4.4-8,9.8-21.5,9.8
                                    c-17.7,0-25.3-11.6-25.3-23.1c0-16.8,13.4-24.5,35.2-24.5h11.3v-5.1c0-5.7-2-11.8-12.9-11.8c-9.9,0-11.9,4.5-13,10h-17.1
                                    c1.1-12.2,8.6-23.2,30.7-23.1c19.3,0.1,29.8,7.7,29.8,25.1V106.1L573.3,106.1z M555.8,89.6h-9.6c-13.2,0-18.9,3.9-18.9,12.1
                                    c0,6.2,4,11.1,11.9,11.1c14.7,0,16.5-10.1,16.5-21.1L555.8,89.6L555.8,89.6z"></path>
                                 <path class="st7a" d="M745.2,90.8c0,11.2,5.7,20.8,16.7,20.8c9.6,0,12.5-4.3,14.6-9.1h18c-2.7,9.2-10.8,23.4-33.1,23.4
                                    c-24,0-34.3-18.5-34.3-37.8c0-22.8,11.7-39.8,35-39.8c24.9,0,33.3,18.7,33.3,36.3c0,2.4,0,4.1-0.3,6.2H745.2L745.2,90.8z
                                     M777.4,79.4c-0.2-9.8-4.5-18-15.4-18s-15.5,7.6-16.5,18H777.4L777.4,79.4z"></path>
                                 <path class="st7a" d="M606.8,102.3c1.8,6.6,7,10.4,15.4,10.4s12-3.4,12-8.6s-3.3-7.8-15.1-10.7c-23.2-5.7-27.3-12.8-27.3-23.2
                                    s7.7-21.9,29-21.9s29,11.9,29.8,22h-17.1c-0.8-3.4-3.2-9.1-13.5-9.1c-8,0-10.5,3.7-10.5,7.5c0,4.3,2.5,6.4,15.1,9.4
                                    c24,5.6,27.8,13.8,27.8,24.7c0,12.5-9.7,23.1-30.9,23.1s-30.7-10.5-32.5-23.7L606.8,102.3L606.8,102.3z"></path>
                                 <path class="st7a" d="M471,48.3c-12.3,0-18.9,5.6-23.1,12c-2.7-6.5-9-12-19.6-12c-11.2,0-17.3,5.5-20.9,11.4l0.1-9.7h-23.6
                                    c3.4,0,5.8,2.5,5.8,5l0,0c0.1,4.7,0.1,9.4,0.1,14.1v55h17.7V82.9c0-13.2,4.5-19.8,14.1-19.8s11.9,7,11.9,15.3v45.8h17.6V81.7
                                    c0-12,4-18.8,13.9-18.8s12.1,7.4,12.1,14.7v46.5h17.5V75.5C494.4,56,483.6,48.3,471,48.3L471,48.3z"></path>
                                 <path class="st7a" d="M368.8,103.6V26.4h-17.9h-5.8c3.4,0,5.8,2.5,5.8,5l0,0v24.8c-1.8-3.6-7.5-7.9-19-7.9
                                    c-20.8,0-33.5,16.7-33.5,39.5s11.9,38.1,30.7,38.1c11.4,0,18.2-3.9,21.8-10.4l0.1,8.7h18C368.8,117.3,368.8,110.5,368.8,103.6
                                    L368.8,103.6z M334,111.4c-10.6,0-17.2-8.4-17.2-24.1s6.3-24.4,17.9-24.4c14.5,0,16.9,9.9,16.9,23.8
                                    C351.5,99.2,349.2,111.3,334,111.4L334,111.4z"></path>
                                 <g>
                                    <path class="st7a" d="M265.6,50h-5.8c3.4,0,5.8,2.5,5.8,5v69.2h17.9V50H265.6z"></path>
                                    <path class="st7a" d="M283.5,29.1v-2.7h-17.9v20.6c0,0,0,0,0,0C275.5,46.9,283.5,38.9,283.5,29.1L283.5,29.1z"></path>
                                 </g>
                                 <g>
                                    <path class="st7a" d="M712.3,63.8V50.1h-15.1v0h-17.9v0h-11.9v13.8h11.9v41.5c0,12.8,4.9,19.9,18.4,19.9c3.9,0,9.1-0.1,12.9-1.4
                                       v-12.6c-1.7,0.3-3.9,0.4-5.3,0.4c-6.3,0-8-2.7-8-8.8V63.8H712.3L712.3,63.8z"></path>
                                    <path class="st7a" d="M697,29.1v-2.7h-17.9v20.6c0,0,0,0,0,0C689,46.9,697,38.9,697,29.1L697,29.1z"></path>
                                 </g>
                                 <path class="st7a" d="M231.3,25.9l-16.3,47c-4.8,14-9.3,27.1-11.3,35.9h-0.3c-2.2-9.7-6.2-22.3-10.8-36.3L177,25.9h-25.7
                                    c5.1,0,7.6,4.6,8.6,7.4l32.4,90.9h21.8l36.1-98.3H231.3L231.3,25.9z"></path>
                              </g>
                           </g>
                        </g>
                    </svg>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="main-header">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-12 text-center">    
               <div class="col-12 text-center">
               <div class="pre-heading f-16 f-md-20 w400 lh140 white-clr">
                     Grab My 20 Exclusive Bonuses Before the Deal Ends…
                  </div>
               </div>
               <div class="col-12 mt-md50 mt30 f-md-45 f-28 w700 text-center white-clr lh140 text-capitalize">
               Game-Changer AI APP Creates Stories, Reels, Boomerang, & Short Videos With <span class="red-clr">Just One Keyword…</span> 
               For Floods Of FREE Traffic And Sales
               </div>
              
                  <div class="col-12 mt-md40 mt20 f-16 f-md-20 w600 text-center lh140 white-clr text-capitalize">
                     Watch My Quick Review Video
                  </div>      
            </div>
         </div>
         <div class="row mt20 mt-md30">
            <div class="col-12 col-md-10 mx-auto">
               <img src="assets/images/productbox.webp" class="img-fluid mx-auto d-block" alt="ProductBox">
                <!-- <div class="responsive-video">
                  <iframe src=" https://vidboxs.dotcompal.com/video/embed/w4rbh264oa" style=" position: absolute;top: 0;left: 0;width: 100%;
                  height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; 
                  fullscreen" allowfullscreen=""></iframe>
               </div>  -->
            </div>
         </div>
      </div>
   </div>
   <!-- Header Section Start -->
   
   
   <div class="list-section">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="row header-list-block">
                  <div class="col-12 col-md-6">
                     <div class="f-18 f-md-20 lh140 w600">
                     <ul class="bonus-list pl0">
                        <li>Tap Into Fast-Growing $200 Billion+ Video Industry & Bank BIG. </li>
                        <li>Create Unlimited Reels & Short Videos for any Niche. </li>
                        <li>Create Reels, Stories & Shorts Using Just One keyword </li>
                        <li>Create Boomerang Video to Engage Audience  </li>
                        <li>Drive Unlimited Traffic & Sales to Any Offer or Page </li>
                        <li>Add Background Music and/or Voiceover to Any Video </li>
                        </ul>
                     </div>
                  </div>
                  <div class="col-12 col-md-6">
                     <div class="f-18 f-md-20 lh140 w500">
                        <ul class="bonus-list pl0">
                        <li>Create Reels & Short Videos from Any Video or Text in 3 Simple Clicks </li>
                        <li>Make Tons of Affiliate Commissions or Ad Profits </li>
                        <li>Pre-designed 50+ Shorts Video templates & 25+ vector images</li>
                        <li>Inbuilt Voiceover Creator with 150+ Human Voice in 30+ Languages </li>
                        <li>100% Newbie Friendly, Required No Prior Experience or Tech Skills </li>  
                        <li>No Camera Recording, No Voice, or Complex Editing Required</li>
                        </ul>
                     </div>
                  </div>
                  <!-- <div class="col-12 col-md-12">
                     <div class="f-18 f-md-20 lh140 w600">
                        <ul class="bonus-list pl0">
                        
                  </ul>
               </div> -->
            </div>
            </div>
            </div>
         </div>
      </div>
   </div>

   <!-- CTA Section Start -->
   <div class="dark-cta-sec">
      <div class="container-fluid">
         <div class="row">
            <div class="col-md-12 col-md-12 col-12 text-center ">
               <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
               <div class="f-md-36 f-22 text-center  lh120 w600 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
               <div class="f-18 f-md-22 lh150 w400 text-center mt20 white-clr">

                  Use Coupon Code <span class="w600 red-clr">"AMITVID"</span> for an Additional <span class="w600 red-clr">$5 Discount</span> on Commercial Licence
               </div>
            </div>
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab VidMaster + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
               <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-20 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
               <div class="countdown counter-white white-clr"><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w600">00</span><br><span class="f-14 f-md-18 ">Days</span> </div><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w600">10</span><br><span class="f-14 f-md-18 w500 ">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald w600">28</span><br><span class="f-14 f-md-18 w500 ">Mins</span> </div><div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald w600">20</span><br><span class="f-14 f-md-18 w500">Sec</span> </div></div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>
   <!-- CTA Section End -->


   <!-- Step Section -->
   <div class="step-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-39 f-28 w600 text-center black-clr2 lh140">
                  Create Stunning Stories, Reels &amp; Shorts for Massive Traffic To Any Offer, Page, or Link <span class="red-clr w700">in Just 3 Easy Steps</span>
               </div>
            </div>
            <div class="row mt20 mt-md50">
               <div class="col-12 col-md-4">
                  <div class="step-block">
                     <div class="step-title f-20 f-md-22 w700">Step 1</div>
                     <img src="assets/images/step-2.webp" class="img-fluid d-block mx-auto mt15 mt-md30">
                     <div class="step-content">
                        <div class="f-22 f-md-28 w600 black-clr2 mt10">Choose Platform</div>
                        <div class="w400 f-18 f-md-20 lh140 mt10 black-clr2">
                           Choose the social platform- YouTube, Instagram, TikTok, Facebook, or Snapchat you want to create Reel or Shorts for
                        </div>
                     </div>  
                  </div>
               </div>
               <div class="col-12 col-md-4 mt20 mt-md0">
                  <div class="step-block">
                     <div class="step-title f-20 f-md-22 w700">Step 2</div>
                     <img src="assets/images/step-1.webp" class="img-fluid d-block mx-auto mt15 mt-md30">
                     <div class="step-content">
                        <div class="f-22 f-md-28 w600 black-clr2 mt10">Enter a Keyword</div>
                        <div class="w400 f-18 f-md-20 lh140 mt10 black-clr2">
                           Enter the desired Keyword &amp; you will get several short videos. Select any video &amp; customize it by adding background, vectors, audio, &amp; fonts to make it engaging
                        </div>
                     </div>  
                  </div>
               </div>
               <div class="col-12 col-md-4 mt20 mt-md0">
                  <div class="step-block">
                     <div class="step-title f-20 f-md-22 w700">Step 3</div>
                     <img src="assets/images/step-3.webp" class="img-fluid d-block mx-auto mt15 mt-md30">
                     <div class="step-content">
                        <div class="f-22 f-md-28 w600 black-clr2 mt10">Publish and Profit</div>
                        <div class="w400 f-18 f-md-20 lh140 mt10 black-clr2">
                           Now publish your Reels &amp; Shortson different social platforms with a click to drive unlimited viral traffic &amp; sales on your offer, page, or website.
                        </div>
                     </div>  
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md70">
               <div class="col-12 text-center">
                  <div class="f-20 f-md-24 w600 lh140">
                     No Download/Installation  |  No Prior Knowledge  |  100% Beginners Friendly
                  </div>
                  <div class="f-18 f-md-20 w400 mt15 mt-md30 lh140">
                     In just 60 seconds, you can create your first profitable Reels &amp; Shorts to enjoy MASSIVE FREE Traffic, Sales &amp; Profits coming into your bank account on autopilot.
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Step Section End -->
      <div class="proof-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-40 f-28 w700 lh140 text-center black-clr">
                  Got 34,309 Targeted Visitors in Last 30 Days… .                  
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
                  <img src="https://cdn.oppyo.com/launches/vidboxs/fe/proof.webp" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 mt20 mt-md50 f-md-40 f-28 w700 lh140 text-center black-clr">
                  Consistently Making Average $535 In Profits <br class="d-none d-md-block"> Each & Every Day              
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
                  <img src="https://cdn.oppyo.com/launches/vidboxs/fe/proof1.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>
         </div>
      </div>
     <!-- Testimonials Section -->
     <div class="testimonial-section">
         <div class="container ">
            <div class="row ">
               <div class="f-md-44 f-28 w700 lh140 text-center black-clr2">
                  Checkout What VidMaster Early Users Have to SAY  
               </div>
            </div>
            <div class="row mt25 mt-md50 align-items-center">
               <div class="col-12 col-md-10">
                  <div class="testi-block">
                     <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-12 col-md-3 order-md-2">
                           <img src="assets/images/c1.webp" class="img-fluid d-block mx-auto">
                           <div class="f-22 f-md-24 w700 lh120 red-clr mt20 text-center">Benjamin Davis</div>
                        </div>
                        <div class="col-12 col-md-9 order-md-1 mt20 mt-md0 text-center text-md-start">
                           <div class="f-22 f-md-28 w700 lh140">I got 2500+ Views &amp; 509 Subscribers in 4 days</div>
                           <div class="f-20 w400 lh140 mt20 quote1">
                              Hi, I am from Australia and promote weight loss products on ClickBank &amp; Amazon as an affiliate. I was struggling to get traffic on my offers and my YouTube channels have only 257 subscribers the reason is that I could not create and post Videos and Shorts regularly as it takes lots of effort and time. But recently, I got access to VidMaster, I created around 15 Short Videos with almost no effort, that was amazingly EASY. And <span class="w700">I got 2500+ Views &amp; 509 Subscribers in 4 days</span> on my weight loss channel. Kudos to the team for creating a solution that can make the complete process fast &amp; easy. 
                           </div> 
                         </div>
                     </div>
                 </div>
               </div>
               <div class="col-12 col-md-10 offset-md-2 mt20 mt-md40">
                  <div class="testi-block">
                     <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-12 col-md-3">
                           <img src="assets/images/c2.webp" class="img-fluid d-block mx-auto">
                           <div class="f-22 f-md-24 w700 red-clr mt20 text-center">Liam Smith</div>
                        </div>
                        <div class="col-12 col-md-9 mt20 mt-md0 text-center text-md-start">
                           <div class="f-22 f-md-28 w700 lh140">Best part about VidMaster is that it takes away all hassles</div>
                           <div class="f-20 w400 lh140 mt20 quote1">
                              Thanks for the early access, VidMaster Team. <span class="w700">The best part about VidMaster is that it takes away</span> all hassles like video recording or editing, voiceover recording, background music and driving traffic to your offers, etc. I am sure this will give you complete value for your money and help you get the desired success in the long run.
                           </div>  
                        </div>
                     </div>
                 </div>
               </div>
               <div class="col-12 col-md-10 mt20 mt-md40">
                  <div class="testi-block">
                     <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-12 col-md-3 order-md-2">
                           <img src="assets/images/c3.webp" class="img-fluid d-block mx-auto">
                           <div class="f-22 f-md-24 w700 lh120 red-clr mt20 text-center">Noah Brown</div>
                        </div>
                        <div class="col-12 col-md-9 order-md-1 mt20 mt-md0 text-center text-md-start">
                           <div class="f-22 f-md-28 w700 lh140">Create super engaging video shorts for YouTube</div>
                           <div class="f-20 w400 lh140 mt20 quote1">
                              VidMaster is the perfect platform to <span class="w700">create super engaging video shorts for YouTube and social media and drive tons of viral traffic in a stress-free manner.</span> I am enjoying all the functionalities of this amazing software. If someone asks me to give marks, I would love to give ten-on-ten to this fabulous product.
                           </div> 
                        </div>
                     </div>
                 </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Testimonials Section End -->

   <!-- CTA Section Start -->
   <div class="dark-cta-sec">
      <div class="container-fluid">
         <div class="row">
            <div class="col-md-12 col-md-12 col-12 text-center ">
               <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
               <div class="f-md-36 f-22 text-center  lh120 w600 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
               <div class="f-18 f-md-22 lh140 w400 text-center mt20 white-clr">
                  Use Coupon Code <span class="w600 red-clr">"AMITVID"</span> for an Additional <span class="w600 red-clr">$5 Discount</span> on Commercial Licence
               </div>
            </div>
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab VidMaster + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
               <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-20 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
               <div class="countdown counter-white white-clr"><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w600">00</span><br><span class="f-14 f-md-18 ">Days</span> </div><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w600">10</span><br><span class="f-14 f-md-18 w500 ">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald w600">28</span><br><span class="f-14 f-md-18 w500 ">Mins</span> </div><div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald w600">20</span><br><span class="f-14 f-md-18 w500">Sec</span> </div></div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>
   <!-- CTA Section End -->

   <div class="billion-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-45 w600 white-clr lh140 heading-design">Did You Know ? YouTube Shorts Hit</div>
                  <div class="f-45 f-md-100 w600 black-clr2 mt20 lh140"> <span class="red-clr">30 Billion</span> View</div>
                  <div class="f-28 f-md-45 w600 lh140 black-clr2">Every Single Day!</div>
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-12 col-md-7">
                  <div class="f-18 f-md-24 w600 black-clr2 lh140">Yes, YouTube Shorts are Generating 30 Billion+ Views Per Day, A 4X Increase From This Time Last Year
                  </div>
                  <div class="f-18 f-md-24 w400 lh140 black-clr21 mt20">This clearly shows the trend is changing. Internet users prefer short videos to consume content instead of watching long videos like earlier…</div>
                  <div class="f-28 f-md-45 w600 lh140 red-clr mt20 mt-md50">No Doubt!</div>
                  <div class="f-20 f-md-24 w600 lh140 black-clr2 mt10">Reels &amp; Shorts Are Present and Future of Video Marketing…</div>
               </div>
               <div class="col-12 col-md-5 mt20 mt-md0">
                  <img src="assets/images/girl.webp" class="img-fluid mx-auto d-block">
               </div>
            </div>
         </div>
         <div class="billion-wrap">
            <div class="container">
               <div class="row">
                  <div class="col-12 text-center">
                     <div class="f-28 f-md-45 w600 lh140 black-clr2">Here Are Some More Facts - <br class="d-none d-md-block">Why One Should Get Started with Reels &amp; Shorts! </div>
                  </div>
               </div>
               <div class="row align-items-center mt20 mt-md80">
                  <div class="col-12 col-md-6">
                     <div class="f-20 f-md-24 w600 lh140 text-start black-clr2">YouTube has 2+ Bn Users &amp; 70% of Videos are Viewed on Mobile</div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 ">
                     <img src="assets/images/facts1.webp" class="img-fluid d-block mx-auto">
                  </div>
               </div>
               <!-- <div class="row align-items-center mt20 mt-md80">
                  <div class="col-12 col-md-6 order-md-2">
                     <div class="f-20 f-md-24 w600 lh140 text-start black-clr2">YouTube Confirms Ads Are Coming to Shorts</div>
                     <div class="f-18 f-md-20 w400 lh140 mt10 text-start black-clr21"> Ads in Shorts would give creators in the YouTube Partner Program the ability to earn more revenue on the platform.
                     </div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
                     <img src="assets/images/facts2.webp" alt="YouTube Confirms Ads Are Coming to Shorts" class="mx-auto img-fluid d-block">
                  </div>
               </div> -->
               <div class="row align-items-center mt20 mt-md80">
                  <div class="col-12 col-md-6 order-md-2">
                     <div class="f-20 f-md-24 w600 lh140 text-start black-clr2">Instagram Reels have 2.5 Billion Monthly Active Users</div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
                     <img src="assets/images/facts3.webp" class="img-fluid d-block mx-auto" alt="Instagram Reels">
                  </div>
               </div>
               <div class="row align-items-center mt20 mt-md80">
                  <div class="col-12 col-md-6">
                     <div class="f-20 f-md-24 w600 lh140 text-start black-clr2">Tiktok has 50 Million Daily Active User in USA &amp; 1 Bn Users Worldwide</div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0">
                     <img src="assets/images/facts4.webp" alt="Tiktok" class="mx-auto img-fluid d-block">
                  </div>
               </div>
               <div class="row align-items-center mt20 mt-md80">
                  <div class="col-12 col-md-6 order-md-2">
                     <div class="f-20 f-md-24 w600 lh140 text-start black-clr2">100 Million hours Daily watch Time by Facebook users.</div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
                     <img src="assets/images/facts5.webp" class="img-fluid d-block mx-auto" alt="Facebook Users">
                  </div>
               </div>
               <div class="row align-items-center mt20 mt-md80">
                  <div class="col-12 col-md-6">
                     <div class="f-20 f-md-24 w600 lh140 text-start black-clr2">Snapchat has over 360+ million daily active users</div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0">
                     <img src="assets/images/facts6.webp" alt="Website Analyser" class="mx-auto img-fluid d-block">
                  </div>
               </div>
               <div class="row mt20 mt-md80">
                  <div class="col-12 text-center">
                     <div class="f-28 f-md-45 w600 lh140 ">So, Capitalising on Reels &amp; Shorts in the Video Marketing Strategy of Any Business is the Need of The Hour.</div>
                     <div class="f-md-45 f-28 w600 lh140 mt20 mt-md30 black-clr2  border-cover">
                        <div class="f-22 f-md-26 w600 lh140 black-clr2">It Opens the Door to</div>
                        <div class=""> Tsunami <span class="red-clr"> of FREE </span>Traffic</div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
  <!-- Proudly Section -->
  <div class="next-gen-sec" id="product">
        <div class="presenting-wrap">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        <div class="f-28 f-md-50 w600 text-center white-clr lh140 text-capitalize presenting-head">Introducing…</div>
                    </div>
                    <div class="col-12 mt-md20 mt20 text-center">
                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 858.2 150.9" style="enable-background:new 0 0 858.2 150.9; max-height: 100px;" xml:space="preserve">
             <style type="text/css">
                .st0{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_1_);}
                .st1{fill-rule:evenodd;clip-rule:evenodd;fill:#3C4650;}
                .st2{fill-rule:evenodd;clip-rule:evenodd;fill:#1C262D;}
                .st3{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_2_);}
                .st4{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_3_);}
                .st5{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_4_);}
                .st6{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_5_);}
                .st7{fill:#FFFFFF;}
             </style>
             <g id="Layer_1-2">
                <g>
                   <g>
                      
                         <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="25.2183" y1="-779.9527" x2="12.9283" y2="-954.9427" gradientTransform="matrix(1 0 0 -1 0 -776)">
                         <stop offset="0" style="stop-color:#CCCCCB"></stop>
                         <stop offset="0.32" style="stop-color:#3C4650"></stop>
                         <stop offset="0.7" style="stop-color:#252D33"></stop>
                         <stop offset="1" style="stop-color:#181D20"></stop>
                      </linearGradient>
                      <path class="st0" d="M40.1,3.1c-14.4-6-22.8-3.9-29.9,10.8C-0.2,35.8-2.6,80.4,3,112.9c5.4,20.7,22.1,25.1,32.2,15
                         c0.7-0.3,1.5-0.6,2.1-0.9c-4.3,1.9-9.1-0.3-10.5-4.8c-7.4-23.6-6.2-70,0.3-91.8C30.9,17.5,39.4,4,40.1,3.1L40.1,3.1z"></path>
                      <path class="st1" d="M31.6,0.4C1.2,18.4,7.9,102.5,18.1,129.5c5.3,1.4,11.3,0.9,17-1.6c0.7-0.3,1.5-0.6,2.1-0.9
                         c-4.3,1.9-9.1-0.3-10.5-4.8c-7.4-23.6-4.8-64.4,1.9-88.3C32.9,18.4,39.3,4.6,40.1,3.1c-3.2-1.3-3.4-1.4-6.2-2.2L31.6,0.4
                         L31.6,0.4z"></path>
                      <path class="st2" d="M2.4,109.5c0.2,1.2,0.3,2.3,0.6,3.4c3.4,15.5,18.4,20.7,32.2,15c0.7-0.3,1.5-0.6,2.1-0.9
                         c-4.3,1.9-9.1-0.3-10.5-4.8C13.8,125.2,5.6,121.7,2.4,109.5L2.4,109.5z"></path>
                      
                         <linearGradient id="SVGID_2_" gradientUnits="userSpaceOnUse" x1="-5.0837" y1="-921.3506" x2="166.8963" y2="-831.7707" gradientTransform="matrix(1 0 0 -1 0 -776)">
                         <stop offset="0" style="stop-color:#CCCCCB"></stop>
                         <stop offset="0.32" style="stop-color:#3C4650"></stop>
                         <stop offset="0.7" style="stop-color:#252D33"></stop>
                         <stop offset="1" style="stop-color:#181D20"></stop>
                      </linearGradient>
                      <path class="st3" d="M118.5,101.8c15-13.2,4.7-28.9-11.3-33.6c6.2,5.9,5.8,10.5-1.3,16.6c-10.3,8.8-22.8,17-34.3,24.1
                         c-11.4,6.9-24,13.9-36.3,19c-13.8,5.7-28.8,0.5-32.2-15c1.7,9.8,4.1,18.4,7.2,25c7.1,14.8,18.7,15.5,33.1,9.6
                         C65.7,138.2,99.7,117.5,118.5,101.8L118.5,101.8L118.5,101.8z"></path>
                      <path class="st1" d="M120.4,83.8c-14.2,18.1-62,46.8-80.9,55.3c-11.8,5.2-25.1,7.1-31.1-5.5c0.6,1.6,1.1,2.9,1.8,4.3
                         c7.1,14.8,18.7,15.5,33.1,9.6c22.4-9.3,56.5-30,75.2-45.6C124.2,96.5,123.7,90.1,120.4,83.8L120.4,83.8L120.4,83.8z"></path>
                      
                         <linearGradient id="SVGID_3_" gradientUnits="userSpaceOnUse" x1="-9.377" y1="-913.095" x2="162.593" y2="-823.515" gradientTransform="matrix(1 0 0 -1 0 -776)">
                         <stop offset="0" style="stop-color:#CCCCCB"></stop>
                         <stop offset="0.32" style="stop-color:#4E4F4F"></stop>
                         <stop offset="0.7" style="stop-color:#383838"></stop>
                         <stop offset="1" style="stop-color:#231F20"></stop>
                      </linearGradient>
                      <path class="st4" d="M35.2,127.9c-13.8,5.7-28.8,0.5-32.2-15c0.2,1.2,0.4,2.5,0.7,3.7c4.3,15.7,18.5,20.2,33,13.8
                         c24.1-10.6,47-27.5,57.7-36.5c-7.4,5.4-15.3,10.4-22.8,15C60.1,115.8,47.5,122.8,35.2,127.9L35.2,127.9L35.2,127.9z"></path>
                      <path class="st2" d="M115.3,104.5c1.1-0.9,2.1-1.8,3.2-2.6c11-10.1-0.8-24.2-11.3-33.6c4.9,4.8,5.7,8.7,2.1,13.1
                         C115.6,85.6,123.2,97.3,115.3,104.5L115.3,104.5L115.3,104.5z"></path>
                      
                         <linearGradient id="SVGID_4_" gradientUnits="userSpaceOnUse" x1="39.6241" y1="-837.5" x2="136.5" y2="-837.5" gradientTransform="matrix(1 0 0 -1 0 -776)">
                         <stop offset="0" style="stop-color:#ED2024"></stop>
                         <stop offset="1" style="stop-color:#F05757"></stop>
                      </linearGradient>
                      <path class="st5" d="M60,105.5c-3.4-18.2-3.5-36.9-0.7-55.1c2.6,15.8,8,31.4,15.4,46.2c8.1-5.2,16.1-10.7,23.5-16.8
                         c0.2-6.1,0.6-12,1.2-17.9c10.3,7.9,33,27.2,19.1,40c0.4-0.3,0.7-0.7,1.2-1c22.4-19.2,22.4-30.6,0-49.9
                         c-9.9-8.4-21.3-16.1-32.4-23c-3.1,12.2-5.1,24.7-6.1,37.3c-5-16.1-5.8-28-5.8-44.5c-7.5-4.3-15.2-8.5-23-12.3
                         c-13.2,33.6-16.3,70.8-8.7,106C49.2,111.7,54.6,108.7,60,105.5L60,105.5L60,105.5z"></path>
                      
                         <linearGradient id="SVGID_5_" gradientUnits="userSpaceOnUse" x1="144.1735" y1="-864.6838" x2="14.8335" y2="-839.7738" gradientTransform="matrix(1 0 0 -1 0 -776)">
                         <stop offset="0" style="stop-color:#ED2024"></stop>
                         <stop offset="1" style="stop-color:#F05757"></stop>
                      </linearGradient>
                      <path class="st6" d="M99.4,61.9c10.3,8,33,27.2,19.1,40l1.2-1c6.8-5.8,6.8-13,1.9-20.5C116.3,72.4,106.1,65.4,99.4,61.9
                         L99.4,61.9L99.4,61.9z"></path>
                   </g>
                   <g>
                      <path class="st7" d="M834,63.8V50h-15.5h-8.2c3.2,0,5.4,2.2,5.7,4.5c0,4.4,0.1,9.4,0.1,15.6v54H834V90.6
                         c0-19.8,9.2-26.9,24.2-26.8V50.1C845.7,50.3,837.5,54.8,834,63.8L834,63.8z"></path>
                      <path class="st7" d="M573.3,106.1c0,6.9,0.6,16.3,1,18.2h-17.1c-0.6-1.5-1-5.3-1.1-8.1c-2.7,4.4-8,9.8-21.5,9.8
                         c-17.7,0-25.3-11.6-25.3-23.1c0-16.8,13.4-24.5,35.2-24.5h11.3v-5.1c0-5.7-2-11.8-12.9-11.8c-9.9,0-11.9,4.5-13,10h-17.1
                         c1.1-12.2,8.6-23.2,30.7-23.1c19.3,0.1,29.8,7.7,29.8,25.1L573.3,106.1L573.3,106.1z M555.8,89.6h-9.6
                         c-13.2,0-18.9,3.9-18.9,12.1c0,6.2,4,11.1,11.9,11.1c14.7,0,16.5-10.1,16.5-21.1L555.8,89.6L555.8,89.6z"></path>
                      <path class="st7" d="M745.2,90.8c0,11.2,5.7,20.8,16.7,20.8c9.6,0,12.5-4.3,14.6-9.1h18c-2.7,9.2-10.8,23.4-33.1,23.4
                         c-24,0-34.3-18.5-34.3-37.8c0-22.8,11.7-39.8,35-39.8c24.9,0,33.3,18.7,33.3,36.3c0,2.4,0,4.1-0.3,6.2H745.2L745.2,90.8z
                          M777.4,79.4c-0.2-9.8-4.5-18-15.4-18s-15.5,7.6-16.5,18H777.4L777.4,79.4z"></path>
                      <path class="st7" d="M606.8,102.3c1.8,6.6,7,10.4,15.4,10.4s12-3.4,12-8.6s-3.3-7.8-15.1-10.7c-23.2-5.7-27.3-12.8-27.3-23.2
                         s7.7-21.9,29-21.9s29,11.9,29.8,22h-17.1c-0.8-3.4-3.2-9.1-13.5-9.1c-8,0-10.5,3.7-10.5,7.5c0,4.3,2.5,6.4,15.1,9.4
                         c24,5.6,27.8,13.8,27.8,24.7c0,12.5-9.7,23.1-30.9,23.1s-30.7-10.5-32.5-23.7L606.8,102.3L606.8,102.3z"></path>
                      <path class="st7" d="M471,48.3c-12.3,0-18.9,5.6-23.1,12c-2.7-6.5-9-12-19.6-12c-11.2,0-17.3,5.5-20.9,11.4l0.1-9.7h-23.6
                         c3.4,0,5.8,2.5,5.8,5l0,0c0.1,4.7,0.1,9.4,0.1,14.1v55h17.7V82.9c0-13.2,4.5-19.8,14.1-19.8s11.9,7,11.9,15.3v45.8h17.6V81.7
                         c0-12,4-18.8,13.9-18.8s12.1,7.4,12.1,14.7v46.5h17.5V75.5C494.4,56,483.6,48.3,471,48.3L471,48.3z"></path>
                      <path class="st7" d="M368.8,103.6V26.4h-17.9h-5.8c3.4,0,5.8,2.5,5.8,5l0,0v24.8c-1.8-3.6-7.5-7.9-19-7.9
                         c-20.8,0-33.5,16.7-33.5,39.5s11.9,38.1,30.7,38.1c11.4,0,18.2-3.9,21.8-10.4l0.1,8.7h18C368.8,117.3,368.8,110.5,368.8,103.6
                         L368.8,103.6z M334,111.4c-10.6,0-17.2-8.4-17.2-24.1s6.3-24.4,17.9-24.4c14.5,0,16.9,9.9,16.9,23.8
                         C351.5,99.2,349.2,111.3,334,111.4L334,111.4z"></path>
                      <g>
                         <path class="st7" d="M265.6,50h-5.8c3.4,0,5.8,2.5,5.8,5v69.2h17.9V50H265.6z"></path>
                         <path class="st7" d="M283.5,29.1v-2.7h-17.9V47l0,0C275.5,46.9,283.5,38.9,283.5,29.1L283.5,29.1z"></path>
                      </g>
                      <g>
                         <path class="st7" d="M712.3,63.8V50.1h-15.1l0,0h-17.9l0,0h-11.9v13.8h11.9v41.5c0,12.8,4.9,19.9,18.4,19.9
                            c3.9,0,9.1-0.1,12.9-1.4v-12.6c-1.7,0.3-3.9,0.4-5.3,0.4c-6.3,0-8-2.7-8-8.8V63.8H712.3L712.3,63.8z"></path>
                         <path class="st7" d="M697,29.1v-2.7h-17.9V47l0,0C689,46.9,697,38.9,697,29.1L697,29.1z"></path>
                      </g>
                      <path class="st7" d="M231.3,25.9l-16.3,47c-4.8,14-9.3,27.1-11.3,35.9h-0.3c-2.2-9.7-6.2-22.3-10.8-36.3L177,25.9h-25.7
                         c5.1,0,7.6,4.6,8.6,7.4l32.4,90.9h21.8l36.1-98.3L231.3,25.9L231.3,25.9z"></path>
                   </g>
                </g>
             </g>
             </svg>
                    </div>
                </div>
                <div class="row mt20 mt-md50 align-items-center">
                    <div class="col-12 col-md-10 mx-auto">
                        <img src="assets/images/productbox.webp" class="img-fluid d-block mx-auto img-animation" alt="Product">
                    </div>
                    <!-- <div class="col-12 mt20 mt-md50 f-18 f-md-20 lh140 w400 white-clr text-center lh140">
                        This is An Exclusive Deal for <span class="w600 red-clr">"VidMaster"</span> Users Only...
                    </div>
                    <div class="col-md-10 mx-auto col-12 mt30 mt-md30">
                        <div class="f-18 f-md-36 text-center probtn1" editabletype="button" style="z-index: 10;">
                            <a href="#buynow" class="text-center">Upgrade to VidMaster Agency Now</a>
                        </div>
                    </div> -->
                </div>
           </div>
        </div>
     </div>
      <!-- Proudly Section End -->

   <!-------Exclusive Bonus----------->
   <div class="exclusive-bonus">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <div class="white-clr f-24 f-md-36 lh140 w600">
                  Exclusive Bonus #1 : AppZilo
               </div>
               <div class="f-18 f-md-20 w500 mt20 white-clr">
               On Purchasing fe you will get 20 accounts as a RESELLER for the  AppZilo to resell it. And on purchasing any OTO you will get 200 accounts as a RESELLER for the  AppZilo to resell it. 
               </div>
            </div>
         </div>
      </div>
   </div>
   <!------Vidvee Section------>
   <div class="header-section-appzilo">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-md-left text-center">
                  <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 650.9 161.1" style="enable-background:new 0 0 650.9 161.1; max-height:50px;" xml:space="preserve">
                     <style type="text/css">
                        .st0aa{fill:#FFFFFF;}
                        .st1aa{fill-rule:evenodd;clip-rule:evenodd;fill:#2089EA;}
                        .st2aa{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_1_);}
                        .st3aa{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_00000001627455377407541100000004222602826157794230_);}
                     </style>
                     <g>
                        <path class="st0aa" d="M221,39.2c4.4-2.4,9.6-3.6,15.5-3.6c6.9,0,13.2,1.8,18.9,5.3c5.6,3.5,10.1,8.5,13.4,15.1
                           c3.3,6.5,4.9,14.1,4.9,22.8c0,8.6-1.6,16.3-4.9,22.9c-3.3,6.6-7.7,11.7-13.3,15.2c-5.6,3.6-11.9,5.4-18.9,5.4
                           c-5.8,0-11-1.2-15.5-3.6c-4.5-2.4-8-5.5-10.5-9.4l0,51.9l-25.8,0l0-124.3l25.8,0l0,11.9C213.1,44.7,216.5,41.6,221,39.2z M242,63.5
                           c-3.6-3.7-8-5.5-13.2-5.5c-5.1,0-9.5,1.9-13,5.6c-3.6,3.7-5.4,8.8-5.4,15.2c0,6.4,1.8,11.5,5.4,15.2c3.6,3.7,7.9,5.6,13,5.6
                           s9.5-1.9,13.1-5.7c3.6-3.8,5.4-8.9,5.4-15.3C247.4,72.2,245.6,67.2,242,63.5z"></path>
                        <path class="st0aa" d="M323.4,39.2c4.4-2.4,9.6-3.6,15.5-3.6c6.9,0,13.2,1.8,18.9,5.3c5.6,3.5,10.1,8.5,13.4,15.1
                           c3.3,6.5,4.9,14.1,4.9,22.8c0,8.6-1.6,16.3-4.9,22.9c-3.3,6.6-7.7,11.7-13.3,15.2c-5.6,3.6-11.9,5.4-18.9,5.4
                           c-5.8,0-11-1.2-15.5-3.6c-4.5-2.4-8-5.5-10.5-9.4l0,51.9l-25.8,0l0-124.3l25.8,0l0,11.9C315.5,44.7,319,41.6,323.4,39.2z
                           M344.4,63.5c-3.6-3.7-8-5.5-13.2-5.5c-5.1,0-9.5,1.9-13,5.6c-3.6,3.7-5.4,8.8-5.4,15.2c0,6.4,1.8,11.5,5.4,15.2
                           c3.6,3.7,7.9,5.6,13,5.6c5.1,0,9.5-1.9,13.1-5.7c3.6-3.8,5.4-8.9,5.4-15.3C349.8,72.2,348,67.2,344.4,63.5z"></path>
                        <path class="st0aa" d="M417.1,99.8l45.6,0l0,21.1l-74.8,0l0-19.6l45.2-65.2l-45.3,0l0-21.1l74.8,0l0,19.6L417.1,99.8z"></path>
                        <path class="st0aa" d="M481.4,24c-2.9-2.7-4.3-6-4.3-9.9c0-4,1.4-7.4,4.3-10c2.9-2.7,6.6-4,11.1-4c4.4,0,8.1,1.3,10.9,4
                           c2.9,2.7,4.3,6,4.3,10c0,3.9-1.4,7.2-4.3,9.9c-2.9,2.7-6.5,4-10.9,4C488,28,484.3,26.6,481.4,24z M505.3,36.7l0,84.2l-25.8,0
                           l0-84.2L505.3,36.7z"></path>
                        <path class="st0aa" d="M549.8,9.3l0,111.6l-25.8,0l0-111.6L549.8,9.3z"></path>
                        <path class="st0aa" d="M584.6,116.8c-6.6-3.5-11.8-8.5-15.5-15.1c-3.8-6.5-5.7-14.2-5.7-22.9c0-8.6,1.9-16.3,5.7-22.9
                           c3.8-6.6,9-11.6,15.7-15.2c6.6-3.5,14.1-5.3,22.3-5.3c8.2,0,15.7,1.8,22.3,5.3c6.6,3.5,11.9,8.6,15.7,15.2
                           c3.8,6.6,5.7,14.2,5.7,22.9c0,8.6-1.9,16.3-5.8,22.9c-3.9,6.6-9.2,11.6-15.8,15.2c-6.7,3.5-14.2,5.3-22.4,5.3
                           C598.6,122.1,591.2,120.3,584.6,116.8z M619.4,94.3c3.5-3.6,5.2-8.8,5.2-15.5c0-6.7-1.7-11.9-5.1-15.5c-3.4-3.6-7.5-5.4-12.4-5.4
                           c-5,0-9.2,1.8-12.5,5.4c-3.3,3.6-5,8.8-5,15.6c0,6.7,1.6,11.9,4.9,15.5c3.3,3.6,7.4,5.4,12.3,5.4C611.8,99.8,616,98,619.4,94.3z"></path>
                     </g>
                     <g>
                        <path class="st1aa" d="M94.8,75.9l-4.6,9.3l-13.3,26.9l-17.1,34.6c-4.4,8.9-12.5,14.4-21.4,14.4H0l41.5-83.6l20-40.3
                           c4.4-0.2,8.8,1.4,12.8,4.6c3.7,2.9,7,7.1,9.7,12.3l0.8,1.7c0,0.1,0.1,0.1,0.1,0.2l3.1,6.2L94.8,75.9z"></path>
                        <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="28.8665" y1="152.9736" x2="86.1242" y2="43.0589">
                           <stop offset="0.5251" style="stop-color:#2089EA"></stop>
                           <stop offset="0.6138" style="stop-color:#1D7ADE"></stop>
                           <stop offset="0.7855" style="stop-color:#1751C0"></stop>
                           <stop offset="1" style="stop-color:#0D1793"></stop>
                        </linearGradient>
                        <path class="st2aa" d="M94.8,75.9l-4.6,9.3l-13.3,26.9l-17.1,34.6c-4.4,8.9-12.5,14.4-21.4,14.4H0l41.5-83.6l20-40.3
                           c4.4-0.2,8.8,1.4,12.8,4.6c3.7,2.9,7,7.1,9.7,12.3l0.8,1.7c0,0.1,0.1,0.1,0.1,0.2l3.1,6.2L94.8,75.9z"></path>
                        <linearGradient id="SVGID_00000110437950689616847500000012451418664872649149_" gradientUnits="userSpaceOnUse" x1="116.6752" y1="141.5592" x2="116.6752" y2="0">
                           <stop offset="0.3237" style="stop-color:#F4833F"></stop>
                           <stop offset="0.6981" style="stop-color:#F39242"></stop>
                           <stop offset="1" style="stop-color:#F2A246"></stop>
                        </linearGradient>
                        <path style="fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_00000110437950689616847500000012451418664872649149_);" d="
                           M171.9,141.6h-28.8c-10.9,0-20.9-6.8-26.4-17.7L90.8,71.4l-6-12.2l-2.7-5.4c0-0.1,0-0.1-0.1-0.2c-0.2-0.5-0.5-1-0.7-1.5
                           c-2.3-4.5-5.3-8.2-8.5-10.8c-3.5-2.8-7.4-4.3-11.3-4.1l6.2-12.5l5.9-11.8l1.7-3.4c0,0,0,0,0,0c2.7-4.6,6.5-7.9,11.1-9.1
                           c8.2-2.1,16.6,3.3,21.8,12.8c0,0,0,0,0,0l1.4,2.8c0,0,0,0,0,0l4.3,8.6L171.9,141.6z"></path>
                     </g>
                  </svg>
               </div>
               
               <div class="col-12 text-center mt20 mt-md50">
                  <div class="pre-heading-appzilo f-md-22 f-16 w500 lh140 white-clr">
                  World's #1 &amp; Fastest 1-Click iOS/Android Mobile App Builder At A Low One Time Price!
                  </div>
               </div>
               <div class="col-12 mt-md30 mt20 f-md-38 f-28 w600 text-center white-clr lh140">
               Brand New 1-Click Cloud App LETS YOU CONVERT YOUR EXISTING WEBSITE INTO AN iOS &amp; Android APP + UNLIMITED MOBILE APPS FOR IOS &amp; ANDROID FROM <u class="stats-headline1">SCRATCH</u>
               </div>
               <div class="col-12 mt-md30 mt20 text-center">
                  <div class="post-heading f-16 f-md-24 w500 text-center lh140 white-clr text-capitalize">
                     Create &amp; Sell Mobile Apps for BIG Profits to Affiliates, Coaches, Attorney, Dentists,<br class="d-none d-md-block"> Gyms, &amp; 10,000+ Other Niches | No Coding or Prior Experience Needed...
                  </div>
               </div>
               <div class="col-12 mt20 mt-md20">
                  <div class="row">
                     <div class="col-md-6 col-12  mt-md20">
                        <!-- <img src="https://cdn.oppyo.com/launches/appzilo/special/product-box.webp" class="img-fluid d-block mx-auto"> -->
                        <div class="col-12 responsive-video">
                           <iframe src="https://appzilo.dotcompal.com/video/embed/7fu418e5f4" style="width:100%; height:100%" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                        </div> 
                    </div>
                     <div class="col-md-6 col-12 mt20 mt-md0 ">
                        <div class="key-features-bg-appzilo">
                           <ul class="list-head pl0 m0 f-18 lh150 w400 white-clr">
                              <li>Create Unlimited iOS &amp; Android Apps </li>
                              <li>Comes With Simple Drag &amp; Drop Editor </li>
                              <li>Sell Limitless Apps to Online &amp; Local Businesses </li>
                              <li>Fastest and bug-free delivery.</li>
                              <li>Turn Any Website Into Fully Fledged Apps in Seconds </li>
                              <li>No App Store &amp; Play Store Approval </li>
                              <li>Comes With Pre-Built Templates </li>
                              <li>No Technical Skills Required </li>
                              <li>Google Ads Monetization </li>
                              <li>100% Newbie Friendly </li>
                              <li>One Time Price and Use Forever </li>
                              <li>Work For ANY Business in ANY Niche </li>
                              <li>Comes With Unlimited Commercial License </li>
                              <li>Ultra-Fast Customer Support </li>
                              <li>$200 Refund If Doesn’t Work for You </li>
                           </ul>
                        </div>
                        <div class="col-12 mt20 mt-md60 d-md-none">
                           <div class="f-20 f-md-24 w500 lh140 text-center white-clr">
                           (Get Free 30 Reseller License + Commercial License + Special Bonuses)
                           </div>
                           <div class="row">
                              <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center ">
                                 <a href="https://warriorplus.com/o2/buy/lbptdw/h6yjdz/dnp22z" class="cta-link-btn px0 d-block">Get Instant Access To AppZilo</a>
                              </div>
                           </div>
                           <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30 ">
                              <img src="https://cdn.oppyo.com/launches/appzilo/special/compaitable-with1.webp " class="img-fluid d-block mx-xs-center md-img-right ">
                              <div class="d-md-block d-none px-md30 "><img src="https://cdn.oppyo.com/launches/appzilo/special/v-line.webp " class="img-fluid "></div>
                              <img src="https://cdn.oppyo.com/launches/appzilo/special/days-gurantee1.webp " class="img-fluid d-block mx-xs-auto mt15 mt-md0 ">
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="gr-1">
      <div class="container-fluid p0">
         <picture>
            <source media="(min-width:768px)" srcset="assets/images/appzilo-ss.webp">
            <source media="(min-width:320px)" srcset="assets/images/appzilo-mview.png">
            <img src="assets/images/appzilo-ss.webp" alt="Steps" style="width:100%;">
         </picture>
      </div>
   </div>
   <!------Vidvee Section------>
   <!----------Buziffy ---------->
   <div class="exclusive-bonus">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                  <div class="white-clr f-24 f-md-36 lh140 w600">
                     Exclusive Bonus #2 : MailZilo 
                  </div>
                  <div class="f-18 f-md-20 w500 mt20 white-clr">
                  On Purchasing fe you will get 20 accounts as a RESELLER for the  MailZilo to resell it. And on purchasing any OTO you will get 200 accounts as a RESELLER for the  MailZilo to resell it. 
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="header-section-mailzilo">
         <div class="container">
            <div class="row">
               <div class="col-md-12  text-center">
                  <svg version="1.1" x="0px" y="0px" viewBox="0 0 1501.16 276.9" style="enable-background:new 0 0 1501.16 276.9; max-height:40px;" xml:space="preserve">
                     <style type="text/css">
                        .st0z{fill:#FFFFFF;}
                     </style>
                     <g>
                        <path class="st0z" d="M122.38,159.77l82.88-67.64h39.5v182.43h-45.44V160.68l-73.53,57.2h-6.82l-73.52-57.2v113.87H0V92.13h39.46
                           L122.38,159.77z"></path>
                        <path class="st0z" d="M391.87,92.13l81.03,182.43h-48.87L409,238.51h-81.7l-13.62,36.04h-48.16l73.69-182.43H391.87z M395.71,206.65
                           l-29.57-70.91l-26.8,70.91H395.71z"></path>
                        <path class="st0z" d="M540.93,92.13v182.43h-45.44V92.13H540.93z"></path>
                        <path class="st0z" d="M722.06,241.12v33.43H582.98V92.13h45.44v149H722.06z"></path>
                        <path class="st0z" d="M913.75,92.13l-99.48,149h99.48v33.43H736.68l100.26-149H743.6V92.13H913.75z"></path>
                        <path class="st0z" d="M989.75,92.13v182.43h-45.44V92.13H989.75z"></path>
                        <path class="st0z" d="M1170.88,241.12v33.43H1031.8V92.13h45.44v149H1170.88z"></path>
                        <path class="st0z" d="M1332.43,209.97c-2.33,5.08-5.42,9.64-9.28,13.71c-9.7,10.23-22.48,15.35-38.35,15.35
                           c-15.94,0-28.8-5.12-38.53-15.35c-9.74-10.22-14.61-23.66-14.61-40.28c0-16.72,4.87-30.17,14.61-40.35
                           c5.49-5.75,11.97-9.87,19.44-12.37l-26.14-31.06c-9.05,4.52-17.19,10.6-24.43,18.22c-17.75,18.72-26.63,40.57-26.63,65.56
                           c0,25.41,8.94,47.36,26.81,65.81c17.87,18.45,41.03,27.68,69.49,27.68c28.13,0,51.18-9.26,69.13-27.81
                           c2.07-2.15,4.04-4.34,5.87-6.58L1332.43,209.97z"></path>
                        <path class="st0z" d="M1501.16,0l-114.63,241.95l-71.11-84.51l-31.99,29.36l2.17-64.79c67.33-7.59,111.49-65.77,112.17-66.67
                           c-41.92,31-125.97,49.4-125.97,49.4l-28.6-33.1L1501.16,0z"></path>
                     </g>
                  </svg>
               </div>
               
               <div class="col-12 text-center mt20 mt-md50">
                  <div class="pre-heading-mailzilo f-md-22 f-16 w500 lh140 white-clr">
                  Are you Sick &amp; Tired of Paying 100s of Dollars To Old School Autoresponders That Never Deliver?
                  </div>
               </div>
               <div class="col-12 mt-md30 mt20 f-md-45 f-28 w600 text-center white-clr lh140">
               Get 4X Email Opens &amp; Traffic Using <span class="yellow-clr">the Powerful Tagging Based Email Marketing Platform &amp; 90,000+ Done-For-You Proven Swipes</span> with No Monthly Fee Ever!
               </div>
               <div class="col-12 mt-md30 mt20 text-center">
                  <div class="post-heading-mailzilo f-16 f-md-24 w500 text-center lh140 white-clr text-capitalize">
                  100% Newbie Friendly Platform to Collect &amp; Manage Leads | Send Unlimited Emails <br class="d-none d-md-block">to Get Tons of Traffic, Affiliate Commissions, &amp; Sales on Autopilot
                  </div>
               </div>
               <div class="col-12 mt20 mt-md40">
                  <div class="row">
                     <div class="col-md-6 col-12 mt20 mt-md60">
                        <!-- <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/product-box.webp" class="img-fluid d-block mx-auto"> -->
                        <div class="col-12 responsive-video">
                           <iframe src="https://mailzilo.dotcompal.com/video/embed/caepa779qt" style="width:100%; height:100%" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                        </div>
                        
                     </div>
                     <div class="col-md-6 col-12 mt20 mt-md0 ">
                        <div class="key-features-bg-mailzilo">
                           <ul class="list-head pl0 m0 f-18 lh150 w400 white-clr">
                              <li><span class="list-highlight">Send Unlimited Emails</span> to Unlimited Subscribers</li>
                              <li><span class="list-highlight">Collect Unlimited Leads</span> with Built-In Lead Form</li>
                              <li><span class="list-highlight">Free SMTP</span> for unrestricted Mailing</li>
                              <li><span class="list-highlight">Upload Unlimited List</span> with Zero Restrictions</li>
                              <li><span class="list-highlight">Get 4X More ROI</span> and Profits than Ever</li>
                              <li><span class="list-highlight">Smart Tagging</span> for Lead Personalisation</li>
                              <li><span class="list-highlight">100% Control</span> on your online business </li>
                              <li><span class="list-highlight">Built-in Inline</span> Editor to Craft Beautiful Emails</li>
                              <li><span class="list-highlight">Hassle-Free</span> Subscribers Management</li>
                              <li><span class="list-highlight">100+ High Converting</span> Templates for Webforms &amp; Email </li>
                              <li><span class="list-highlight">GDPR &amp; Can-Spam Compliant</span></li>
                              <li><span class="list-highlight">100% Newbie Friendly</span> &amp; Easy to Use</li>
                              <li><span class="list-highlight">Commercial License Included</span></li>
                           </ul>
                        </div>
                        <div class="col-12 mt20 mt-md60 d-md-none">
                           <div class="f-20 f-md-24 w500 lh140 text-center white-clr">
                              Get Free 30 Reseller License + Commercial License <br class="d-none d-md-block"> If You Buy Today!! 
                           </div>
                           <div class="row">
                              <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center ">
                                 <a href="https://warriorplus.com/o2/buy/xgwy6m/y4pjnq/nh90r1" class="cta-link-btn px0 d-block">Get Instant Access To MailZilo</a>
                              </div>
                           </div>
                           <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30 ">
                              <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/compaitable-with1.webp " class="img-fluid d-block mx-xs-center md-img-right ">
                              <div class="d-md-block d-none px-md30 "><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/v-line.webp " class="img-fluid "></div>
                              <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/days-gurantee1.webp " class="img-fluid d-block mx-xs-auto mt15 mt-md0 ">
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="gr-1">
      <div class="container-fluid p0">
         <picture>
            <source media="(min-width:768px)" srcset="assets/images/mailzilo-ss.webp">
            <source media="(min-width:320px)" srcset="assets/images/mailzilo-mview.webp">
            <img src="assets/images/mailzilo-ss.webp" alt="Steps" style="width:100%;">
         </picture>
      </div>
   </div>
   <!----------Buziffy ---------->
   <!------Ninjakash--------->
   <div class="exclusive-bonus">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <div class="white-clr f-24 f-md-36 lh140 w600">
                  Exclusive Bonus #3 : Ninja Kash
               </div>
               <div class="f-18 f-md-20 w500 mt20 white-clr">
               On Purchasing fe you will get 20 accounts as a RESELLER for the  Ninja Kash to resell it. And on purchasing any OTO you will get 200 accounts as a RESELLER for the  Ninja Kash to resell it. 
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="grand-logo-bg">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <img src="assets/images/logo-gr.webp" class="img-fluid mx-auto d-block" alt="Ninjakash Logo">
            </div>
         </div>
      </div>
   </div>
   <div class="header-section-n">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="mt20 mt-md50">
                  <div class="f-md-24 f-20 w500 lh140 text-center black-clr post-heading-n">
                     Weird System Pays Us $500-1000/Day for Just Copying and Pasting...
                  </div>
               </div>
            </div>
            <div class="col-12">
               <div class="col-12 mt-md30 mt20">
                  <div class="f-28 f-md-50 w500 text-center white-clr lh140">								
                     <span class="w600"> Break-Through App Creates a Ninja Affiliate Funnel In 5 Minutes Flat</span> That Drives FREE Traffic &amp; Sales on 100% Autopilot
                  </div>
               </div>
               <div class="col-12 f-22 f-md-28 w600 text-center  orange lh140 mt-md30 mt20">
                  NO Worries For Finding Products | NO Paid Traffic | No Tech Skills
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-md-7 col-12 mt-md40 mt20 px-md15 min-md-video-width-left">
               <!--<div>
                  <img src="assets/images/video-bg.png" class="img-responsive center-block"> 
                     </div>-->
               <div class="col-12 mt-md15">
                  <div class="col-12 responsive-video">
                     <iframe src="https://ninjakash.dotcompal.com/video/embed/t4spbhm1vi" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                        box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                  </div>
               </div>
            </div>
            <div class="col-md-5 col-12 f-20 f-md-20 lh140 w400 white-clr mt-md40 mt20 pl-md15 min-md-video-width-right">
               <ul class="list-head-b pl0 m0">
                  <li>Kickstart with 50 Hand-Picked Products</li>
                  <li>Promote Any Offer in Any niche HANDS FREE</li>
                  <li>SEO Optimized, Mobile Responsive Affiliate Funnels</li>
                  <li>Drive TONS OF Social &amp; Viral traffic</li>
                  <li>Build Your List and Convert AFFILIATE Sales in NO Time</li>
                  <li>No Monthly Fees…EVER</li>
               </ul>
            </div>
         </div>
      </div>
   </div>
   <div class="gr-1">
      <div class="container-fluid p0">
         <picture>
            <source media="(min-width:768px)" srcset="assets/images/gr2.webp">
            <source media="(min-width:320px)" srcset="assets/images/gr2a.webp">
            <img src="assets/images/gr2.webp" alt="Flowers" style="width:100%;">
         </picture>
         <picture>
            <source media="(min-width:768px)" srcset="assets/images/gr1.webp">
            <source media="(min-width:320px)" srcset="assets/images/gr1a.webp">
            <img src="assets/images/gr1.webp" alt="Flowers" style="width:100%;">
         </picture>
      </div>
   </div>
   <!------Ninjakash--------->

   <!-- Jobiin Section Start -->
   <!-------Exclusive Bonus----------->
   <div class="exclusive-bonus">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <div class="white-clr f-24 f-md-36 lh140 w600">
                  Exclusive Bonus #4 : JOBiin
               </div>
               <div class="f-18 f-md-20 w500 mt20 white-clr">
               On Purchasing fe you will get 20 accounts as a RESELLER for the  JOBiin to resell it. And on purchasing any OTO you will get 200 accounts as a RESELLER for the  JOBiin to resell it. 
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-------Exclusive Bonus----------->

   <div class="jobiin-header-section">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <svg id="Layer_1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 128.19 38" style="max-height:55px;">
                  <defs>
                     <style>
                        .cls-1,
                        .cls-2 {
                           fill: #fff;
                        }

                        .cls-2 {
                           fill-rule: evenodd;
                        }

                        .cls-3 {
                           fill: #0a52e2;
                        }
                     </style>
                  </defs>
                  <rect class="cls-1" x="79.21" width="48.98" height="38" rx="4" ry="4"></rect>
                  <path class="cls-1" d="M18.64,4.67V24.72c0,1.29-.21,2.47-.63,3.54-.42,1.07-1.03,1.97-1.84,2.71-.81,.74-1.79,1.32-2.93,1.74-1.15,.42-2.45,.63-3.9,.63s-2.64-.19-3.7-.57c-1.07-.38-1.98-.92-2.75-1.62-.77-.7-1.39-1.54-1.88-2.51S.19,26.6,0,25.41l5.74-1.13c.46,2.45,1.63,3.68,3.52,3.68,.89,0,1.65-.28,2.28-.85,.63-.57,.95-1.46,.95-2.67V9.6H3.92V4.67h14.72Z"></path>
                  <path class="cls-1" d="M54.91,33.37V4.63h9.71c3.38,0,6.02,.66,7.92,1.97,1.9,1.32,2.84,3.28,2.84,5.9,0,1.33-.35,2.52-1.06,3.56-.7,1.05-1.73,1.83-3.07,2.36,1.72,.37,3.02,1.16,3.88,2.37,.86,1.21,1.29,2.61,1.29,4.21,0,2.75-.91,4.83-2.72,6.25-1.82,1.42-4.39,2.12-7.72,2.12h-11.08Zm5.76-16.7h4.15c1.54,0,2.72-.32,3.55-.95,.83-.63,1.24-1.55,1.24-2.76,0-1.33-.42-2.31-1.25-2.94-.84-.63-2.08-.95-3.74-.95h-3.95v7.6Zm0,3.99v8.27h5.31c1.53,0,2.69-.33,3.49-.99,.8-.66,1.2-1.64,1.2-2.94,0-1.4-.34-2.48-1.03-3.22-.68-.74-1.76-1.11-3.24-1.11h-5.75Z"></path>
                  <g>
                     <path class="cls-1" d="M46.84,4.19C42.31-.12,36.87-1.08,31.16,1.2c-5.82,2.33-9.1,6.88-9.52,13.21-.41,6.17,2.57,10.9,6.84,15.15,.95-.84,1.22-1.22,2.01-2.05-.79-.65-1.41-1.09-2.43-2.11-5.86-6.15-5.78-15.66,1.04-20.46,5-3.52,11.71-3.13,16.37,.95,4.29,3.75,5.54,10.44,2.59,15.61-1.2,2.11-3.09,3.84-4.86,5.98,.28,.45,.72,1.18,1.44,2.35,1.77-2.01,3.11-3.72,4.38-5.63,4.39-6.6,3.56-14.59-2.17-20.02Z"></path>
                     <g>
                        <path class="cls-2" d="M39.67,27.34c-2.49,.37-4.9,.52-7.23-.46-.54-.23-1.13-1.06-1.18-1.65-.3-3.69,1.25-5.27,4.98-5.26,.31,0,.62,0,.92,.01,5.31-.01,5.96,6.85,2.51,7.36Z"></path>
                        <path class="cls-2" d="M39.49,16.69c-.04,1.69-1.37,2.98-3.06,2.98-1.6,0-3.01-1.43-3.01-3.06s1.39-3.04,3.02-3.03c1.75,0,3.1,1.38,3.06,3.12Z"></path>
                     </g>
                     <g>
                        <g>
                           <path class="cls-2" d="M42.93,26.23c-.67-3.46-.93-5.25-3.46-7.18,.63-1.21,1.3-1.83,2.1-2,2.43-.53,3.98,.21,4.84,1.97,.66,1.34,.78,2.29-.37,3.77-1.03,1.17-1.85,2.21-3.11,3.43Z"></path>
                           <path class="cls-2" d="M44.94,13.4c.01,1.6-1.27,2.91-2.88,2.92-1.6,.01-2.91-1.28-2.92-2.88-.01-1.6,1.28-2.9,2.88-2.92,1.6-.01,2.91,1.28,2.92,2.88Z"></path>
                        </g>
                        <g>
                           <path class="cls-2" d="M30.1,26.23c.67-3.46,.93-5.25,3.46-7.18-.63-1.21-1.3-1.83-2.1-2-2.43-.53-3.98,.21-4.84,1.97-.66,1.34-.88,2.01,.27,3.49,.96,1.3,1.88,2.44,3.21,3.71Z"></path>
                           <path class="cls-2" d="M28.08,13.4c0,1.6,1.28,2.91,2.88,2.92,1.6,.01,2.91-1.28,2.92-2.88,.01-1.6-1.27-2.9-2.88-2.92-1.6-.01-2.91,1.28-2.92,2.88Z"></path>
                        </g>
                     </g>
                     <path class="cls-1" d="M42.02,27.74c-.7,.41-6.95,2.1-10.73,.08l-2.22,2.57c.55,.61,5.12,5.27,7.55,7.6,2.22-2.17,7.2-7.37,7.2-7.37l-1.8-2.89Z"></path>
                  </g>
                  <g>
                     <rect class="cls-3" x="85.05" y="4.63" width="5.38" height="5.4" rx="2.69" ry="2.69"></rect>
                     <rect class="cls-3" x="85.05" y="13.43" width="5.38" height="19.94"></rect>
                     <rect class="cls-3" x="95.13" y="4.63" width="5.38" height="5.4" rx="2.69" ry="2.69"></rect>
                     <rect class="cls-3" x="95.13" y="13.43" width="5.38" height="19.94"></rect>
                     <path class="cls-3" d="M109.85,13.43l.24,2.86c.66-1.02,1.48-1.81,2.45-2.38,.97-.56,2.06-.85,3.26-.85,2.01,0,3.59,.63,4.72,1.9,1.13,1.27,1.7,3.25,1.7,5.95v12.46h-5.4v-12.47c0-1.34-.27-2.29-.81-2.85-.54-.56-1.36-.84-2.45-.84-.71,0-1.35,.14-1.92,.43-.57,.29-1.04,.7-1.42,1.23v14.5h-5.38V13.43h5.01Z"></path>
                  </g>
               </svg>
            </div>
            <div class="col-12 text-center mt20 mt-md50 lh130">
               <div class=" f-md-20 f-16 w600 lh130 headline-highlight">
                  Copy &amp; Paste Our SECRET FORMULA That Makes Us $328/Day Over &amp; Over Again!
               </div>
            </div>
            <div class="col-12 mt-md25 mt20 f-md-46 f-26 w600 text-center white-clr lh140">
               EXPOSED–A Breakthrough Software That<span class="w600 red-clr"> Creates <span class="tm">
                     Indeed<sup class="f-20 w500 lh200">TM</sup>
                  </span> Like JOB Search Sites in Next 60 Seconds</span> with 100% Autopilot Job Listings, FREE Traffic &amp; Passive Commissions
            </div>
            <div class="col-12 mt-md25 mt20 f-18 f-md-22 w400 text-center lh140 grey-clr">
               Pre-Loaded with 10 million+ Job Listings I Earn Commissions on Every Click on Listings &amp; Banners | <br class="d-none d-md-block"> No Content Writing I No Tech Hassel Ever… Even A Beginner Can Start Right Away
            </div>
         </div>
         <div class="row d-flex align-items-center flex-wrap mt20 mt-md40">
            <div class="col-md-10 col-12 mx-auto">
               <div class="responsive-video">
                  <iframe src="https://jobiin.dotcompal.com/video/embed/x83jxcd4vl" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                        box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
               </div>
            </div>
            <div class="col-12 f-20 f-md-26 w600 lh150 text-center white-clr mt-md50 mt20">
               LIMITED TIME - FREE COMMERCIAL LICENSE INCLUDED
            </div>
         </div>
      </div>
      <img src="assets/images/header-left-element.webp" alt="Header Element " class="img-fluid d-none header-left-element ">
      <img src="assets/images/header-right-element.webp" alt="Header Element " class="img-fluid d-none header-right-element ">
   </div>

   <div class="jobiin-second-section">
      <div class="container">
         <div class="row">
            <div class="col-12 z-index-9">
               <div class="row jobiin-header-list-block">
                  <div class="col-12 col-md-6">
                     <ul class="bonus-list pl0 f-18 f-md-20 lh150 w500 black-clr text-capitalize">
                        <li><span class="w600">Tap into the $212 Billion Recruitment Industry</span></li>
                        <li><span class="w600">Create Multiple UNIQUE Job Sites</span> – Niche Focused (Automotive, Banking Jobs etc.) or City Focused (New York, London Jobs etc.)</li>
                        <li><span class="w600">100% Automated Job Posting &amp; Updates</span> on Your Site from 10 million+ Open Jobs</li>
                        <li>Get Job Listed from TOP Brands &amp; Global Giants <span class="w600">Like Amazon, Walmart, Costco, etc.</span></li>
                        <li>Create Website in <span class="w600">13 Different Languages &amp; Target 30+ Top Countries</span></li>
                        <li class="w600">Built-In FREE Search and Social Traffic from Google &amp; 80+ Social Media Platforms</li>
                        <li class="w600">Built-In SEO Optimised Fresh Content &amp; Blog Articles</li>
                     </ul>
                  </div>
                  <div class="col-12 col-md-6 mt10 mt-md0">
                     <ul class="bonus-list pl0 f-18 f-md-20 lh150 w500 black-clr text-capitalize">
                        <li class="w600">Make Commissions from Top Recruitment Companies on Every Click of Job Listings</li>
                        <li>Promote TOP Affiliate Offers Through <span class="w600">Banner and Text Ads for Extra Commission</span></li>
                        <li>Even Apply Yourself for Any <span class="w600">Good Part-Time Job to Make Extra Income</span></li>
                        <li>Easy And Intuitive to Use Software with <span class="w600">Step-by-Step Video Training</span></li>
                        <li><span class="w600">Get Started Immediately</span> – Launch Done-for-You Job Site and Start Making Money Right Away</li>
                        <li><span class="w600">Made For Absolute Newbies</span> &amp; Experienced Marketers</li>
                        <li class="w600">PLUS, FREE COMMERCIAL LICENSE IF YOU Start TODAY!</li>

                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>

   <div class="gr-1">
      <div class="container-fluid p0">
         <picture>
            <source media="(min-width:768px)" srcset="assets/images/jobiin-steps.webp">
            <source media="(min-width:320px)" srcset="assets/images/jobiin-steps-mview.webp">
            <img src="assets/images/jobiin-steps.webp" alt="Steps" style="width:100%;">
         </picture>
         <picture>
            <source media="(min-width:768px)" srcset="assets/images/jobiin-big-job.webp">
            <source media="(min-width:320px)" srcset="assets/images/jobiin-big-job-mview.webp">
            <img src="assets/images/jobiin-big-job.webp" alt="Real Result" style="width:100%;">
         </picture>
         <picture>
            <source media="(min-width:768px)" srcset="assets/images/jobiin-impressive.webp">
            <source media="(min-width:320px)" srcset="assets/images/jobiin-impressive-mview.webp">
            <img src="assets/images/jobiin-impressive.webp" alt="Real Result" style="width:100%;">
         </picture>
         <picture>
            <source media="(min-width:768px)" srcset="assets/images/jobiin-proudly-presenting.webp">
            <source media="(min-width:320px)" srcset="assets/images/jobiin-proudly-presenting-mview.webp">
            <img src="assets/images/jobiin-proudly-presenting.webp" alt="Real Result" style="width:100%;">
         </picture>
      </div>
   </div>
   <!-- Jobiin Section End -->



   <!-- Bonus Section Header Start -->
   <div class="bonus-header">
      <div class="container">
         <div class="row">
            <div class="col-12 col-md-10 mx-auto heading-bg text-center">
               <div class="f-24 f-md-36 lh140 w600"> When You Purchase VidMaster, You Also Get <br class="hidden-xs"> Instant Access To These 20 Exclusive Bonuses</div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus Section Header End -->

   <!-- Bonus #1 Section Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w600">BONUS 1</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus1.webp" class="img-fluid mx-auto d-block" alt="Bonus1">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w600 lh140 bonus-title-color">Resilience Video Upgrade  </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li class="w600">With this video course you will learn how to build mental strength to boost resilience and overcome 
                           any difficult situation in life.</li>
                        <li>Learning how to bounce back is teachable and learnable. </li>
                        <li>It is imperative to fully understand what resilience is, learn about its components, and learn about resilience 
                           boosting techniques.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #1 Section End -->

   <!-- Bonus #2 Section Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w600">BONUS 2</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30">
               <div class="row">
                  <div class="col-md-5 col-12 order-md-2">
                     <img src="assets/images/bonus2.webp" class="img-fluid mx-auto d-block" alt="Bonus2">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                     <div class="f-22 f-md-32 w600 lh140 bonus-title-color mt20 mt-md0">
                     Facebook Ad Secrets Advance 
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li class="w600">While the FB Ad Secrets was made for beginners just starting in Facebook advertising, these videos answer 
                           the coming challenges you will face as you scale to spending hundreds or thousands of dollars per day while 
                           remaining profitable. </li>
                        <li>For beginners, you will usually start with a low budget to run your Facebook ads. But eventually when the time comes 
                           for you to scale your offer with a bigger budget, you will come across problems such as your ad account being disabled,
                            profit margin becomes thinner and things like when and how to split test your ads. </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #2 Section End -->

   <!-- Bonus #3 Section Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w600">BONUS 3</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus3.webp" class="img-fluid mx-auto d-block " alt="Bonus3">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w600 lh140 bonus-title-color">
                     Modern Niche Marketing Video Upgrade 
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li class="w600">With this video guide you will learn everything that you need to know to properly assess niche ideas for 
                           their viability and how to drive targeted traffic to your niche website.</li>
                        <li>There is more competition these days which means that you really have to be smart to succeed. If you follow the advice 
                           in this guide then you will provide yourself with the maximum chance of success. Don’t believe the hype – niche marketing 
                           does work if you do it correctly.</li>
                        <li>There are niche websites being sold every day for large amounts of money. These websites are generating regular income 
                           and have a good following. </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #3 Section End -->

   <!-- Bonus #4 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w600">BONUS 4</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12 order-md-2">
                     <img src="assets/images/bonus4.webp" class="img-fluid mx-auto d-block " alt="Bonus4">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                     <div class="f-22 f-md-32 w600 lh140 bonus-title-color">
                     Web Traffic Excellence
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li class="w600">
                        You can have the best product or the best service in the world but if you have no traffic – it’s all completely 
                        worthless.</li>
                        <li>This 5 - part video course will help new and experienced marketers generate huge amount of traffic from five 
                           different sources.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #4 End -->

   <!-- Bonus #5 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w600">BONUS 5</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus5.webp" class="img-fluid mx-auto d-block " alt="Bonus5">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w600 lh140 bonus-title-color">
                     Video Marketing Blueprint v2 
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Who Else Wants To Use Videos To Market Their Business And Generate Massive Leads ... Starting Today?</li>
                        <li>Video marketing has become one of the highest rising and most popular traffic generations. You?ve made the right decision 
                           in investing in this course where you?ll learn how to get torrents of traffic in just a few easy steps.</li>
                        <li>Video Marketing Blueprint is the key to your marketing success. With it, you will find out how to grow your mailing 
                           list, generate massive traffic to your website, increase the conversion and purchase rate, of course, so much more.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #5 End -->

   <!-- CTA Button Section Start -->
   <div class="dark-cta-sec">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-md-12 col-12 text-center ">
               <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
               <div class="f-md-36 f-22 text-center  lh120 w600 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
               <div class="f-18 f-md-22 lh140 w400 text-center mt20 white-clr">
                  Use Coupon Code <span class="w600 red-clr">"AMITVID"</span> for an Additional <span class="w600 red-clr">$5 Discount</span> 
                  on Commercial Licence
               </div>
            </div>
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab VidMaster + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
               <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-20 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
               <div class="countdown counter-white white-clr">
                  <div class="timer-label text-center">
                     <span class="f-31 f-md-60 timerbg oswald w600">01</span>
                     <span class="f-14 f-md-18 w500 ">Days</span>
                  </div>
                  <div class="timer-label text-center">
                     <span class="f-31 f-md-60 timerbg oswald w600">16</span>
                     <span class="f-14 f-md-18 w500 ">Hours</span>
                  </div>
                  <div class="timer-label text-center timer-mrgn">
                     <span class="f-31 f-md-60 timerbg oswald w600">59</span>
                     <span class="f-14 f-md-18 w500 ">Mins</span>
                  </div>
                  <div class="timer-label text-center ">
                     <span class="f-31 f-md-60 timerbg oswald w600">37</span>
                     <span class="f-14 f-md-18 w500 ">Sec</span>
                  </div>
               </div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>
   <!-- CTA Button Section End -->

   <!-- Bonus #6 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w600">BONUS 6</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12 order-md-2">
                     <img src="assets/images/bonus6.webp" class="img-fluid mx-auto d-block " alt="Bonus6">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                     <div class="f-22 f-md-32 w600 lh140 bonus-title-color">
                     Site Speed Secrets Video Upgrade 
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Discover how you can finally speed up your website and increase your conversion rates. All the information you need is 
                           here, and nothing has been left out in this course! </li>
                        <li>Not all web hosts are created equal – make sure you sign up for a fast web host that will help you reach your business 
                           goals.  </li>
                        <li>Know how to use and analyze the results of the best website speed testing tools out there.  </li>
                        <li>Never underestimate the power of caching ever again – it will help your site load much faster than ever before. </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #6 End -->

   <!-- Bonus #7 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w600">BONUS 7</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus7.webp" class="img-fluid mx-auto d-block " alt="Bonus7">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w600 lh140 bonus-title-color">
                     Shopify Traffic
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Compared to 10 years ago, starting an online e-store is not as hard anymore today. Driven by the dream of having an internet
                            lifestyle, and the rewarding monthly income, everyone is triggered to own a business for a better life.</li>
                        <li>With all the platforms and opportunities available, it's easy to kick start an online business anytime you want. Even if you
                            have little budget, you can start an online shop on a small scale. </li>
                        <li>Shopify Traffic is a series of training course that will teach you how to generate traffic to your Shopify e-store with
                            effective methods and platform from personal experience and culmination of researches together with years of studies. </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #7 End -->

   <!-- Bonus #8 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w600">BONUS 8</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="assets/images/bonus8.webp" class="img-fluid mx-auto d-block " alt="Bonus8">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w600 lh140 bonus-title-color">
                     Content Syndication 
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Content Syndication Videos is a series of 40 On-Screen, Easy-To-Follow Video Tutorials on how to market and publish your 
                           content expertly.</li>
                        <li>The Internet is full of 'me too marketers' and what makes this frustrating is that even if you're a genuine expert, it's not
                            a guarantee you'll be successful with marketing yourself. </li>
                        <li>This course will help you to get an unfair advantage and stay ahead of the competition. Lessons are short, simple and
                            direct.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #8 End -->

   <!-- Bonus #9 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w600">BONUS 9</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus9.webp" class="img-fluid mx-auto d-block " alt="Bonus9">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w600 lh140 bonus-title-color">
                     Kick Ass Affiliate 
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Kick Ass Affiliate is a video course that will enlighten you how to become a super affiliate even if you don’t have a 
                           huge email list or you don’t have a huge following on your blog or in your social media circle.</li>
                        <li>If you are thinking that you can’t beat those deadbeat affiliates that keep on topping the leader board, inside the 
                           course are you will learn how to change that mentality. </li>
                        <li>You are also about to learn the principle of offering crazy bonuses and how to set this up effectively in your affiliate
                            marketing campaign. If you already have a mailing list, learn how to create a persuasive approach that will make your
                             subscribers buy the product you offer </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #9 End -->

   <!-- Bonus #10 start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w600">BONUS 10</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="assets/images/bonus10.webp" class="img-fluid mx-auto d-block " alt="Bonus10">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w600 lh140 bonus-title-color">
                     100 Split Tests 
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>100 Split Tests is the result of performing over thousands of different split tests across all selling and advertising mediums online - and we've distilled it into one hundred tweaks and 'hacks' you can do to immediately boost your conversion rates!</li>
                        <li>Split testing has been known to drive some pretty compelling results, but that all assumes one thing: that it’s done correctly. There are numerous aspects that you can look into for split testing, and they can get complicated.</li>
                        <li>Is your goal to drive traffic? Is it clicks? Or even opt-ins? A clear result is determined by setting a clear goal at the start. An email subject line that wins at open rate may be different than the winner for click-through rates. </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #10 End -->

   <!-- CTA Button Section Start -->
   <div class="dark-cta-sec">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-md-12 col-12 text-center ">
               <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
               <div class="f-md-36 f-22 text-center  lh120 w600 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
               <div class="f-18 f-md-22 lh140 w400 text-center mt20 white-clr">
                  Use Coupon Code <span class="w600 red-clr">"AMITVID"</span> for an Additional <span class="w600 red-clr">$5 Discount</span> on Commercial Licence
               </div>
            </div>
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab VidMaster + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
               <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-20 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
               <div class="countdown counter-white white-clr">
                  <div class="timer-label text-center">
                     <span class="f-31 f-md-60 timerbg oswald w600">01</span>
                     <span class="f-14 f-md-18 w500 ">Days</span>
                  </div>
                  <div class="timer-label text-center">
                     <span class="f-31 f-md-60 timerbg oswald w600">16</span>
                     <span class="f-14 f-md-18 w500 ">Hours</span>
                  </div>
                  <div class="timer-label text-center timer-mrgn">
                     <span class="f-31 f-md-60 timerbg oswald w600">59</span>
                     <span class="f-14 f-md-18 w500 ">Mins</span>
                  </div>
                  <div class="timer-label text-center ">
                     <span class="f-31 f-md-60 timerbg oswald w600">37</span>
                     <span class="f-14 f-md-18 w500 ">Sec</span>
                  </div>
               </div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>
   <!-- CTA Button Section End -->

   <!-- Bonus #11 start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w600">BONUS 11</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus11.webp" class="img-fluid mx-auto d-block " alt="Bonus11">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w600 lh140 bonus-title-color">
                     Copy Cash Secrets 
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li><b>Now You Can Write Compelling Sales Letters That Effectively Generate A Call-To-Action And Potentially Increase Sales Leads On A Regular Basis!</b></li>
                        <li>Words is powerful tool if utilized properly by anyone in marketing or sales. Keyword for ‘guarantee’ is an essential component for most good eCommerce marketing copy. </li>
                        <li>The word “Guaranteed” appeals to consumer emotional triggers like safety and trust. The word works like a safety net which is a promise made by a retailer to a consumer that they will be pleased with their purchase. </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #11 End -->

   <!-- Bonus #12 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w600">BONUS 12</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="assets/images/bonus12.webp" class="img-fluid mx-auto d-block " alt="Bonus12">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w600 lh140 bonus-title-color">
                     Traffic Babylon
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li><b>If You Are Looking To Send Your Website Traffic Even Further Through The Roof, then here's a short, 12 part video series that reveals to you 6 paid and 6 free traffic sources to SKYROCKET your traffic!</b></li>
                        <li>YES! Traffic is indeed the life-blood of your website. But the question is that, how many traffic generating ideas you have for you to perform on your campaign?</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #12 End -->

   <!-- Bonus #13 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w600">BONUS 13</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus13.webp" class="img-fluid mx-auto d-block " alt="Bonus13">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w600 lh140 bonus-title-color">
                     Viral Profit Machine 
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Discover How YOU Can Make MAXIMUM Profits With Minimum Effort By Getting Other People To Willingly Market For You!</li>
                        <li>Unlock The Secrets To Getting Other People To Talk About You And Your Business Willingly Whether They're Paid To Do It Or Not!</li>
                        <li>Announcing: Viral Profit Machine = Here's a sneak peak of what you'll find in this 50 minutes 54 seconds exclusive audio session:</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #13 End -->

   <!-- Bonus #14 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w600">BONUS 14</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="assets/images/bonus14.webp" class="img-fluid mx-auto d-block " alt="Bonus14">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w600 lh140 bonus-title-color">
                     Public Domain Profits 
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>If you're sick and tired of not making any money online then you need to discover the... The Lazy Internet Marketer's Way To Riches</li>
                        <li>Now you too can become a money making internet marketer without busting your butt with super hard work or extra long hours...</li>
                        <li>If you've always dreamed of making more money while being able to do less and less work, THIS is what you've been searching for...</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #14 End -->

   <!-- Bonus #15 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w600">BONUS 15</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus15.webp" class="img-fluid mx-auto d-block " alt="Bonus15">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w600 lh140 bonus-title-color">
                     Keyword Goldrush 
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Keywords are absolutely vital for you to succeed in making money online.</li>
                        <li>Your keywords are how your websites are found by your potential customers using the search engines.</li>
                        <li>If you are not optimised for the right keywords, and you do not rank high for these keywords, then you are going to struggle to get the traffic that you want for making money.</li>
                        <li>This program is all about how to find the keywords you want to target, how to use them, and most importantly, how to profit from them.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #15 End -->

   <!-- CTA Button Section Start -->
   <div class="dark-cta-sec">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-md-12 col-12 text-center ">
               <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
               <div class="f-md-36 f-22 text-center  lh120 w600 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
               <div class="f-18 f-md-22 lh140 w400 text-center mt20 white-clr">
                  Use Coupon Code <span class="w600 red-clr">"AMITVID"</span> for an Additional <span class="w600 red-clr">$5 Discount</span> on Commercial Licence
               </div>
            </div>
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab VidMaster + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
               <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-20 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
               <div class="countdown counter-white white-clr">
                  <div class="timer-label text-center">
                     <span class="f-31 f-md-60 timerbg oswald w600">01</span>
                     <span class="f-14 f-md-18 w500 ">Days</span>
                  </div>
                  <div class="timer-label text-center">
                     <span class="f-31 f-md-60 timerbg oswald w600">16</span>
                     <span class="f-14 f-md-18 w500 ">Hours</span>
                  </div>
                  <div class="timer-label text-center timer-mrgn">
                     <span class="f-31 f-md-60 timerbg oswald w600">59</span>
                     <span class="f-14 f-md-18 w500 ">Mins</span>
                  </div>
                  <div class="timer-label text-center ">
                     <span class="f-31 f-md-60 timerbg oswald w600">37</span>
                     <span class="f-14 f-md-18 w500 ">Sec</span>
                  </div>
               </div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>
   <!-- CTA Button Section End -->

   <!-- Bonus #16 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w600">BONUS 16</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="assets/images/bonus16.webp" class="img-fluid mx-auto d-block " alt="Bonus16">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w600 lh140 bonus-title-color">
                     SEO Reborn
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Finally A Fool Proof Way Of Ranking On The First Page Of Google Has Been Cracked And Revealed By A Former Internet Newbie Turned SEO King!</li>
                        <li>Introducing A Complete Underground Secret SEO Solution In Ranking Your Website On The First Page Of Google Without Paying A Dime For Traffic!</li>
                        <li>Finally Rank Higher On The Search Engines Without Having To Worry About Paid Traffic. Take Advantage Of First Page Google Rankings And Easily Beat Your Competition! It Is Time To DOMINATE The Search Engines And Start Being The BIG Player In Your Market!</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #16 End -->

   <!-- Bonus #17 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w600">BONUS 17</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus17.webp" class="img-fluid mx-auto d-block " alt="Bonus17">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w600 lh140 bonus-title-color">
                     Membership Mastery
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>The Lazy Man's Guide To Creating A Membership Site From Scratch : Now You Can Kick The Technical Roadblocks Out of Your Way And Focus On Building Your Own Membership Site - the Right Way! Look over the shoulder of a real membership owner to see how you can setup a membership site quickly from scratch.</li>
                        <li>On the Internet and in the world around you, members only clubs are sprouting everywhere, whether paid or free. This is because Membership sites generally provide access to something that is unique from other sites. They also build communities of people alike. In fact most people want to improve themselves, improve their business... you get the point. So they're willing to join membership sites that help them do that.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #17 End -->

   <!-- Bonus #18 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w600">BONUS 18</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="assets/images/bonus18.webp" class="img-fluid mx-auto d-block " alt="Bonus18">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w600 lh140 bonus-title-color">
                     Marketing Stomp 
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Discover the Jealously Guarded Secrets of Seemingly Ordinary Folks Generating Extraordinary Incomes With Just a Computer and Internet Connection!</li>
                        <li>There are many ways to make money online and one of these profitable concepts is by selling your own digital products that many people needs.</li>
                        <li>If you are new to internet marketing or online entrepreneurship, having a guide in doing this for the first time is a huge help.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #18 End -->

   <!-- Bonus #19 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w600">BONUS 19</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus19.webp" class="img-fluid mx-auto d-block " alt="Bonus19">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w600 lh140 bonus-title-color">
                     Customer Product Portals 
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>See how easy it is to put together a central repository where your customers can access all of the products they have purchased from you. At the same time market all of your other products to your buyers passively!</li>
                        <li>Indeed, there are so many ways to make money online. And of those effective ways is selling digital goods because of it's several advantages in terms of delivering the product.</li>
                        <li>But the thing though is that, selling these digital products also has a flaw and that is it can easily be shared or steal by some hackers. </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #19 End -->

   <!-- Bonus #20 start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w600">BONUS 20</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="assets/images/bonus20.webp" class="img-fluid mx-auto d-block " alt="Bonus20">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w600 lh140 bonus-title-color">
                     Support Desk System 
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li><b>How To Set Up A Professional Email Support Ticket System!</b></li>
                        <li>Reduce Your Refund Rates, Keep Your Buyers Happy And Never Worry About Not Answering Your Customer's Emails Again!</li>
                        <li>Now You Can Side Aside The Mundane Task Of Answering Emails, Set Up A Support Desk For Free And Give Yourself Time To Progress Your Business!</li>
                        <li>8-part video tutorials reveal how you can install a customer support system in just less than an hour and start using it to keep in contact with your prospects and customers!</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #20 End -->

   <!-- Huge Woth Section Start -->
   <div class="huge-area mt30 mt-md10">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-12 text-center">
               <div class="f-md-65 f-40 lh120 w600 white-clr">That’s Huge Worth of</div>
               <br>
               <div class="f-md-60 f-40 lh120 w600 red-clr">$2285!</div>
            </div>
         </div>
      </div>
   </div>
   <!-- Huge Worth Section End -->

   <!-- text Area Start -->
   <div class="white-section pb0">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-12 text-center">
            <div class="f-md-35 f-28 lh140 w600">So what are you waiting for? You have a great opportunity ahead + My 20 Bonus Products are making it's a completely NO Brainer!!</div>
            </div>
         </div>
      </div>
   </div>
   <!-- text Area End -->

   <!-- CTA Button Section Start -->
   <div class="cta-btn-section">
      <div class="container">
         <div class="row">
         <div class="f-18 f-md-22 lh140 w500 text-center mt20 black-clr">
                  Use Coupon Code <span class="w600 red-clr">"AMITVID"</span> for an Additional <span class="w600 red-clr">$5 Discount</span> on Commercial Licence
               </div>
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md30">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab VidMaster + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-20 f-20 w500 text-center black">My Exclusive Bonuses Are Expiring in...</h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 col-md-10 mx-auto col-12 text-center">
               <div class="countdown counter-black">
                  <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w600">01</span><br><span class="f-14 f-md-18 w500  ">Days</span> </div>
                  <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w600">16</span><br><span class="f-14 f-md-18 w500 ">Hours</span> </div>
                  <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald w600">59</span><br><span class="f-14 f-md-18 w500 ">Mins</span> </div>
                  <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald w600">37</span><br><span class="f-14 f-md-18 w500 ">Sec</span> </div>
               </div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>
   <!-- CTA Button Section End -->

   <!--Footer Section Start -->
   <div class="footer-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 858.2 150.9" style="enable-background:new 0 0 858.2 150.9; max-height: 60px;" xml:space="preserve">
                     <style type="text/css">
                        .st0{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_1_);}
                        .st1{fill-rule:evenodd;clip-rule:evenodd;fill:#3C4650;}
                        .st2{fill-rule:evenodd;clip-rule:evenodd;fill:#1C262D;}
                        .st3{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_2_);}
                        .st4{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_3_);}
                        .st5{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_4_);}
                        .st6{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_5_);}
                        .st7{fill:#FFFFFF;}
                     </style>
                     <g id="Layer_1-2">
                        <g>
                           <g>
                              <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="25.2183" y1="-779.9527" x2="12.9283" y2="-954.9427" gradientTransform="matrix(1 0 0 -1 0 -776)">
                                 <stop offset="0" style="stop-color:#CCCCCB"></stop>
                                 <stop offset="0.32" style="stop-color:#3C4650"></stop>
                                 <stop offset="0.7" style="stop-color:#252D33"></stop>
                                 <stop offset="1" style="stop-color:#181D20"></stop>
                              </linearGradient>
                              <path class="st0" d="M40.1,3.1c-14.4-6-22.8-3.9-29.9,10.8C-0.2,35.8-2.6,80.4,3,112.9c5.4,20.7,22.1,25.1,32.2,15
                              c0.7-0.3,1.5-0.6,2.1-0.9c-4.3,1.9-9.1-0.3-10.5-4.8c-7.4-23.6-6.2-70,0.3-91.8C30.9,17.5,39.4,4,40.1,3.1L40.1,3.1z"></path>
                              <path class="st1" d="M31.6,0.4C1.2,18.4,7.9,102.5,18.1,129.5c5.3,1.4,11.3,0.9,17-1.6c0.7-0.3,1.5-0.6,2.1-0.9
                              c-4.3,1.9-9.1-0.3-10.5-4.8c-7.4-23.6-4.8-64.4,1.9-88.3C32.9,18.4,39.3,4.6,40.1,3.1c-3.2-1.3-3.4-1.4-6.2-2.2L31.6,0.4
                              L31.6,0.4z"></path>
                              <path class="st2" d="M2.4,109.5c0.2,1.2,0.3,2.3,0.6,3.4c3.4,15.5,18.4,20.7,32.2,15c0.7-0.3,1.5-0.6,2.1-0.9
                              c-4.3,1.9-9.1-0.3-10.5-4.8C13.8,125.2,5.6,121.7,2.4,109.5L2.4,109.5z"></path>
                              <linearGradient id="SVGID_2_" gradientUnits="userSpaceOnUse" x1="-5.0837" y1="-921.3506" x2="166.8963" y2="-831.7707" gradientTransform="matrix(1 0 0 -1 0 -776)">
                                 <stop offset="0" style="stop-color:#CCCCCB"></stop>
                                 <stop offset="0.32" style="stop-color:#3C4650"></stop>
                                 <stop offset="0.7" style="stop-color:#252D33"></stop>
                                 <stop offset="1" style="stop-color:#181D20"></stop>
                              </linearGradient>
                              <path class="st3" d="M118.5,101.8c15-13.2,4.7-28.9-11.3-33.6c6.2,5.9,5.8,10.5-1.3,16.6c-10.3,8.8-22.8,17-34.3,24.1
                              c-11.4,6.9-24,13.9-36.3,19c-13.8,5.7-28.8,0.5-32.2-15c1.7,9.8,4.1,18.4,7.2,25c7.1,14.8,18.7,15.5,33.1,9.6
                              C65.7,138.2,99.7,117.5,118.5,101.8L118.5,101.8L118.5,101.8z"></path>
                              <path class="st1" d="M120.4,83.8c-14.2,18.1-62,46.8-80.9,55.3c-11.8,5.2-25.1,7.1-31.1-5.5c0.6,1.6,1.1,2.9,1.8,4.3
                              c7.1,14.8,18.7,15.5,33.1,9.6c22.4-9.3,56.5-30,75.2-45.6C124.2,96.5,123.7,90.1,120.4,83.8L120.4,83.8L120.4,83.8z"></path>
                              <linearGradient id="SVGID_3_" gradientUnits="userSpaceOnUse" x1="-9.377" y1="-913.095" x2="162.593" y2="-823.515" gradientTransform="matrix(1 0 0 -1 0 -776)">
                                 <stop offset="0" style="stop-color:#CCCCCB"></stop>
                                 <stop offset="0.32" style="stop-color:#4E4F4F"></stop>
                                 <stop offset="0.7" style="stop-color:#383838"></stop>
                                 <stop offset="1" style="stop-color:#231F20"></stop>
                              </linearGradient>
                              <path class="st4" d="M35.2,127.9c-13.8,5.7-28.8,0.5-32.2-15c0.2,1.2,0.4,2.5,0.7,3.7c4.3,15.7,18.5,20.2,33,13.8
                              c24.1-10.6,47-27.5,57.7-36.5c-7.4,5.4-15.3,10.4-22.8,15C60.1,115.8,47.5,122.8,35.2,127.9L35.2,127.9L35.2,127.9z"></path>
                              <path class="st2" d="M115.3,104.5c1.1-0.9,2.1-1.8,3.2-2.6c11-10.1-0.8-24.2-11.3-33.6c4.9,4.8,5.7,8.7,2.1,13.1
                              C115.6,85.6,123.2,97.3,115.3,104.5L115.3,104.5L115.3,104.5z"></path>
                              <linearGradient id="SVGID_4_" gradientUnits="userSpaceOnUse" x1="39.6241" y1="-837.5" x2="136.5" y2="-837.5" gradientTransform="matrix(1 0 0 -1 0 -776)">
                                 <stop offset="0" style="stop-color:#ED2024"></stop>
                                 <stop offset="1" style="stop-color:#F05757"></stop>
                              </linearGradient>
                              <path class="st5" d="M60,105.5c-3.4-18.2-3.5-36.9-0.7-55.1c2.6,15.8,8,31.4,15.4,46.2c8.1-5.2,16.1-10.7,23.5-16.8
                              c0.2-6.1,0.6-12,1.2-17.9c10.3,7.9,33,27.2,19.1,40c0.4-0.3,0.7-0.7,1.2-1c22.4-19.2,22.4-30.6,0-49.9
                              c-9.9-8.4-21.3-16.1-32.4-23c-3.1,12.2-5.1,24.7-6.1,37.3c-5-16.1-5.8-28-5.8-44.5c-7.5-4.3-15.2-8.5-23-12.3
                              c-13.2,33.6-16.3,70.8-8.7,106C49.2,111.7,54.6,108.7,60,105.5L60,105.5L60,105.5z"></path>
                              <linearGradient id="SVGID_5_" gradientUnits="userSpaceOnUse" x1="144.1735" y1="-864.6838" x2="14.8335" y2="-839.7738" gradientTransform="matrix(1 0 0 -1 0 -776)">
                                 <stop offset="0" style="stop-color:#ED2024"></stop>
                                 <stop offset="1" style="stop-color:#F05757"></stop>
                              </linearGradient>
                              <path class="st6" d="M99.4,61.9c10.3,8,33,27.2,19.1,40l1.2-1c6.8-5.8,6.8-13,1.9-20.5C116.3,72.4,106.1,65.4,99.4,61.9
                              L99.4,61.9L99.4,61.9z"></path>
                           </g>
                           <g>
                              <path class="st7" d="M834,63.8V50h-15.5h-8.2c3.2,0,5.4,2.2,5.7,4.5c0,4.4,0.1,9.4,0.1,15.6v54H834V90.6
                              c0-19.8,9.2-26.9,24.2-26.8V50.1C845.7,50.3,837.5,54.8,834,63.8L834,63.8z"></path>
                              <path class="st7" d="M573.3,106.1c0,6.9,0.6,16.3,1,18.2h-17.1c-0.6-1.5-1-5.3-1.1-8.1c-2.7,4.4-8,9.8-21.5,9.8
                              c-17.7,0-25.3-11.6-25.3-23.1c0-16.8,13.4-24.5,35.2-24.5h11.3v-5.1c0-5.7-2-11.8-12.9-11.8c-9.9,0-11.9,4.5-13,10h-17.1
                              c1.1-12.2,8.6-23.2,30.7-23.1c19.3,0.1,29.8,7.7,29.8,25.1L573.3,106.1L573.3,106.1z M555.8,89.6h-9.6
                              c-13.2,0-18.9,3.9-18.9,12.1c0,6.2,4,11.1,11.9,11.1c14.7,0,16.5-10.1,16.5-21.1L555.8,89.6L555.8,89.6z"></path>
                              <path class="st7" d="M745.2,90.8c0,11.2,5.7,20.8,16.7,20.8c9.6,0,12.5-4.3,14.6-9.1h18c-2.7,9.2-10.8,23.4-33.1,23.4
                              c-24,0-34.3-18.5-34.3-37.8c0-22.8,11.7-39.8,35-39.8c24.9,0,33.3,18.7,33.3,36.3c0,2.4,0,4.1-0.3,6.2H745.2L745.2,90.8z
                              M777.4,79.4c-0.2-9.8-4.5-18-15.4-18s-15.5,7.6-16.5,18H777.4L777.4,79.4z"></path>
                              <path class="st7" d="M606.8,102.3c1.8,6.6,7,10.4,15.4,10.4s12-3.4,12-8.6s-3.3-7.8-15.1-10.7c-23.2-5.7-27.3-12.8-27.3-23.2
                              s7.7-21.9,29-21.9s29,11.9,29.8,22h-17.1c-0.8-3.4-3.2-9.1-13.5-9.1c-8,0-10.5,3.7-10.5,7.5c0,4.3,2.5,6.4,15.1,9.4
                              c24,5.6,27.8,13.8,27.8,24.7c0,12.5-9.7,23.1-30.9,23.1s-30.7-10.5-32.5-23.7L606.8,102.3L606.8,102.3z"></path>
                              <path class="st7" d="M471,48.3c-12.3,0-18.9,5.6-23.1,12c-2.7-6.5-9-12-19.6-12c-11.2,0-17.3,5.5-20.9,11.4l0.1-9.7h-23.6
                              c3.4,0,5.8,2.5,5.8,5l0,0c0.1,4.7,0.1,9.4,0.1,14.1v55h17.7V82.9c0-13.2,4.5-19.8,14.1-19.8s11.9,7,11.9,15.3v45.8h17.6V81.7
                              c0-12,4-18.8,13.9-18.8s12.1,7.4,12.1,14.7v46.5h17.5V75.5C494.4,56,483.6,48.3,471,48.3L471,48.3z"></path>
                              <path class="st7" d="M368.8,103.6V26.4h-17.9h-5.8c3.4,0,5.8,2.5,5.8,5l0,0v24.8c-1.8-3.6-7.5-7.9-19-7.9
                              c-20.8,0-33.5,16.7-33.5,39.5s11.9,38.1,30.7,38.1c11.4,0,18.2-3.9,21.8-10.4l0.1,8.7h18C368.8,117.3,368.8,110.5,368.8,103.6
                              L368.8,103.6z M334,111.4c-10.6,0-17.2-8.4-17.2-24.1s6.3-24.4,17.9-24.4c14.5,0,16.9,9.9,16.9,23.8
                              C351.5,99.2,349.2,111.3,334,111.4L334,111.4z"></path>
                              <g>
                                 <path class="st7" d="M265.6,50h-5.8c3.4,0,5.8,2.5,5.8,5v69.2h17.9V50H265.6z"></path>
                                 <path class="st7" d="M283.5,29.1v-2.7h-17.9V47l0,0C275.5,46.9,283.5,38.9,283.5,29.1L283.5,29.1z"></path>
                              </g>
                              <g>
                                 <path class="st7" d="M712.3,63.8V50.1h-15.1l0,0h-17.9l0,0h-11.9v13.8h11.9v41.5c0,12.8,4.9,19.9,18.4,19.9
                                    c3.9,0,9.1-0.1,12.9-1.4v-12.6c-1.7,0.3-3.9,0.4-5.3,0.4c-6.3,0-8-2.7-8-8.8V63.8H712.3L712.3,63.8z"></path>
                                 <path class="st7" d="M697,29.1v-2.7h-17.9V47l0,0C689,46.9,697,38.9,697,29.1L697,29.1z"></path>
                              </g>
                              <path class="st7" d="M231.3,25.9l-16.3,47c-4.8,14-9.3,27.1-11.3,35.9h-0.3c-2.2-9.7-6.2-22.3-10.8-36.3L177,25.9h-25.7
                              c5.1,0,7.6,4.6,8.6,7.4l32.4,90.9h21.8l36.1-98.3L231.3,25.9L231.3,25.9z"></path>
                           </g>
                        </g>
                     </g>
                  </svg>
                  <div editabletype="text" class="f-16 f-md-18 w400 lh140 mt20 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
                  <div class=" mt20 mt-md40 white-clr text-center"> <script type="text/javascript" src="https://warriorplus.com/o2/disclaimer/jrrjv5" defer=""></script><div class="wplus_spdisclaimer f-16 f-md-16 w400 lh140 white-clr"><span class="w600">Disclaimer : </span> WarriorPlus is used to help manage the sale of products on this site. While WarriorPlus helps facilitate the sale, all payments are made directly to the product vendor and NOT WarriorPlus. Thus, all product questions, support inquiries and/or refund requests must be sent to the vendor. WarriorPlus's role should not be construed as an endorsement, approval or review of these products or any claim, statement or opinion used in the marketing of these products.</div></div>
              </div>
              <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-16 f-md-18 w400 lh140 white-clr text-xs-center">Copyright © VidMaster</div>
                  <ul class="footer-ul f-16 f-md-18 w400 white-clr text-center text-md-right">
                      <li><a href="https://support.bizomart.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                      <li><a href="http://www.vidmaster.co/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                      <li><a href="http://www.vidmaster.co/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                      <li><a href="http://www.vidmaster.co/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                      <li><a href="http://www.vidmaster.co/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                      <li><a href="http://www.vidmaster.co/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                      <li><a href="http://www.vidmaster.co/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
              </div>
          </div>
      </div>
  </div>
   <!-- Back to top button -->
   <a id="button">
      <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 300.003 300.003" style="enable-background:new 0 0 300.003 300.003;" xml:space="preserve">
         <g>
            <g>
               <path d="M150,0C67.159,0,0.001,67.159,0.001,150c0,82.838,67.157,150.003,149.997,150.003S300.002,232.838,300.002,150    C300.002,67.159,232.842,0,150,0z M217.685,189.794c-5.47,5.467-14.338,5.47-19.81,0l-48.26-48.27l-48.522,48.516    c-5.467,5.467-14.338,5.47-19.81,0c-2.731-2.739-4.098-6.321-4.098-9.905s1.367-7.166,4.103-9.897l56.292-56.297    c0.539-0.838,1.157-1.637,1.888-2.368c2.796-2.796,6.476-4.142,10.146-4.077c3.662-0.062,7.348,1.281,10.141,4.08    c0.734,0.729,1.349,1.528,1.886,2.365l56.043,56.043C223.152,175.454,223.156,184.322,217.685,189.794z" />
            </g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
      </svg>
   </a>
   <!--Footer Section End -->
   <!-- timer --->
   <?php
   if ($now < $exp_date) {
   ?>
      <script type="text/javascript">
         // Count down milliseconds = server_end - server_now = client_end - client_now
         var server_end = <?php echo $exp_date; ?> * 1000;
         var server_now = <?php echo time(); ?> * 1000;
         var client_now = new Date().getTime();
         var end = server_end - server_now + client_now; // this is the real end time

         var noob = $('.countdown').length;

         var _second = 1000;
         var _minute = _second * 60;
         var _hour = _minute * 60;
         var _day = _hour * 24
         var timer;

         function showRemaining() {
            var now = new Date();
            var distance = end - now;
            if (distance < 0) {
               clearInterval(timer);
               document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
               return;
            }

            var days = Math.floor(distance / _day);
            var hours = Math.floor((distance % _day) / _hour);
            var minutes = Math.floor((distance % _hour) / _minute);
            var seconds = Math.floor((distance % _minute) / _second);
            if (days < 10) {
               days = "0" + days;
            }
            if (hours < 10) {
               hours = "0" + hours;
            }
            if (minutes < 10) {
               minutes = "0" + minutes;
            }
            if (seconds < 10) {
               seconds = "0" + seconds;
            }
            var i;
            var countdown = document.getElementsByClassName('countdown');
            for (i = 0; i < noob; i++) {
               countdown[i].innerHTML = '';

               if (days) {
                  countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w600">' + days + '</span><br><span class="f-14 f-md-18 ">Days</span> </div>';
               }

               countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w600">' + hours + '</span><br><span class="f-14 f-md-18 w500 ">Hours</span> </div>';

               countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald w600">' + minutes + '</span><br><span class="f-14 f-md-18 w500 ">Mins</span> </div>';

               countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald w600">' + seconds + '</span><br><span class="f-14 f-md-18 w500">Sec</span> </div>';
            }

         }
         timer = setInterval(showRemaining, 1000);
      </script>
   <?php
   } else {
      echo "Times Up";
   }
   ?>
   <!--- timer end-->

   <!-- scroll Top to Bottom -->
   <script>
      var btn = $('#button');

      $(window).scroll(function() {
         if ($(window).scrollTop() > 300) {
            btn.addClass('show');
         } else {
            btn.removeClass('show');
         }
      });

      btn.on('click', function(e) {
         e.preventDefault();
         $('html, body').animate({
            scrollTop: 0
         }, '300');
      });
   </script>
</body>

</html>
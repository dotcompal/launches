<!Doctype html>
<html>
   <head>
      <title>VidMaster Prelaunch</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=9">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
      <meta name="title" content="VidMaster Prelaunch">
      <meta name="description" content="Creates YouTube Short Videos, Drive Tons of Free Viral Traffic & Profit">
      <meta name="keywords" content="VidMaster">
      <meta property="og:image" content="https://www.vidmaster.co/prelaunch/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Ayush Jain">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="VidMaster Prelaunch">
      <meta property="og:description" content="Creates YouTube Short Videos, Drive Tons of Free Viral Traffic & Profit">
      <meta property="og:image" content="https://www.vidmaster.co/prelaunch/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="VidMaster Prelaunch">
      <meta property="twitter:description" content="Creates YouTube Short Videos, Drive Tons of Free Viral Traffic & Profit">
      <meta property="twitter:image" content="https://www.vidmaster.co/prelaunch/thumbnail.png">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
      <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
      <link rel="stylesheet" href="../common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style.css" type="text/css">
      <link rel="stylesheet" href="assets/css/aweberform.css" type="text/css">
      <script src="../common_assets/js/jquery.min.js"></script>
      <script>
         function getUrlVars() {
             var vars = [],
                 hash;
             var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
             for (var i = 0; i < hashes.length; i++) {
                 hash = hashes[i].split('=');
                 vars.push(hash[0]);
                 vars[hash[0]] = hash[1];
             }
             return vars;
         }
         var first = getUrlVars()["aid"];
         //alert(first);
         document.addEventListener('DOMContentLoaded', (event) => {
                 document.getElementById('awf_field_aid').setAttribute('value', first);
             })
             //document.getElementById('myField').value = first;
      </script>
   </head>
   <body>
      <div class="header-nav">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-7 text-md-start text-center">
               <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 858.2 150.9" style="enable-background:new 0 0 858.2 150.9; max-height: 55px;" xml:space="preserve">
                     <style type="text/css">
                        .st0a{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_1_);}
                        .st1a{fill-rule:evenodd;clip-rule:evenodd;fill:#3C4650;}
                        .st2a{fill-rule:evenodd;clip-rule:evenodd;fill:#1C262D;}
                        .st3a{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_2_);}
                        .st4a{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_3_);}
                        .st5a{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_4_);}
                        .st6a{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_5_);}
                        .st7a{fill:#1C262D;}
                     </style>
                     <g id="Layer_1-2">
                        <g>
                           <g>
                              
                                 <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="25.1812" y1="148.0499" x2="12.8912" y2="-26.9401" gradientTransform="matrix(1 0 0 -1 0 152)">
                                 <stop offset="0" style="stop-color:#CCCCCB"></stop>
                                 <stop offset="0.32" style="stop-color:#3C4650"></stop>
                                 <stop offset="0.7" style="stop-color:#252D33"></stop>
                                 <stop offset="1" style="stop-color:#181D20"></stop>
                              </linearGradient>
                              <path class="st0a" d="M40.1,3.1c-14.4-6-22.8-3.9-29.9,10.8c-10.4,21.9-12.8,66.5-7.2,99c5.4,20.7,22.1,25.1,32.2,15
                                 c0.7-0.3,1.5-0.6,2.1-0.9c-4.3,1.9-9.1-0.3-10.5-4.8c-7.4-23.6-6.2-70,0.3-91.8C30.9,17.5,39.4,4,40.1,3.1L40.1,3.1z"></path>
                              <path class="st1" d="M31.6,0.4C1.2,18.4,7.9,102.5,18.1,129.5c5.3,1.4,11.3,0.9,17-1.6c0.7-0.3,1.5-0.6,2.1-0.9
                                 c-4.3,1.9-9.1-0.3-10.5-4.8c-7.4-23.6-4.8-64.4,1.9-88.3C32.9,18.4,39.3,4.6,40.1,3.1c-3.2-1.3-3.4-1.4-6.2-2.2L31.6,0.4
                                 L31.6,0.4z"></path>
                              <path class="st2a" d="M2.4,109.5c0.2,1.2,0.3,2.3,0.6,3.4c3.4,15.5,18.4,20.7,32.2,15c0.7-0.3,1.5-0.6,2.1-0.9
                                 c-4.3,1.9-9.1-0.3-10.5-4.8C13.8,125.2,5.6,121.7,2.4,109.5C2.4,109.5,2.4,109.5,2.4,109.5z"></path>
                              
                                 <linearGradient id="SVGID_2_" gradientUnits="userSpaceOnUse" x1="-5.1028" y1="6.6861" x2="166.8772" y2="96.2661" gradientTransform="matrix(1 0 0 -1 0 152)">
                                 <stop offset="0" style="stop-color:#CCCCCB"></stop>
                                 <stop offset="0.32" style="stop-color:#3C4650"></stop>
                                 <stop offset="0.7" style="stop-color:#252D33"></stop>
                                 <stop offset="1" style="stop-color:#181D20"></stop>
                              </linearGradient>
                              <path class="st3a" d="M118.5,101.8c15-13.2,4.7-28.9-11.3-33.6c6.2,5.9,5.8,10.5-1.3,16.6c-10.3,8.8-22.8,17-34.3,24.1
                                 c-11.4,6.9-24,13.9-36.3,19c-13.8,5.7-28.8,0.5-32.2-15c1.7,9.8,4.1,18.4,7.2,25c7.1,14.8,18.7,15.5,33.1,9.6
                                 C65.7,138.2,99.7,117.5,118.5,101.8L118.5,101.8L118.5,101.8z"></path>
                              <path class="st1a" d="M120.4,83.8c-14.2,18.1-62,46.8-80.9,55.3c-11.8,5.2-25.1,7.1-31.1-5.5c0.6,1.6,1.1,2.9,1.8,4.3
                                 c7.1,14.8,18.7,15.5,33.1,9.6c22.4-9.3,56.5-30,75.2-45.6C124.2,96.5,123.7,90.1,120.4,83.8L120.4,83.8L120.4,83.8z"></path>
                              
                                 <linearGradient id="SVGID_3_" gradientUnits="userSpaceOnUse" x1="-9.3881" y1="14.9264" x2="162.5819" y2="104.5064" gradientTransform="matrix(1 0 0 -1 0 152)">
                                 <stop offset="0" style="stop-color:#CCCCCB"></stop>
                                 <stop offset="0.32" style="stop-color:#4E4F4F"></stop>
                                 <stop offset="0.7" style="stop-color:#383838"></stop>
                                 <stop offset="1" style="stop-color:#231F20"></stop>
                              </linearGradient>
                              <path class="st4a" d="M35.2,127.9c-13.8,5.7-28.8,0.5-32.2-15c0.2,1.2,0.4,2.5,0.7,3.7c4.3,15.7,18.5,20.2,33,13.8
                                 c24.1-10.6,47-27.5,57.7-36.5c-7.4,5.4-15.3,10.4-22.8,15C60.1,115.8,47.5,122.8,35.2,127.9L35.2,127.9L35.2,127.9z"></path>
                              <path class="st2" d="M115.3,104.5c1.1-0.9,2.1-1.8,3.2-2.6c11-10.1-0.8-24.2-11.3-33.6c4.9,4.8,5.7,8.7,2.1,13.1
                                 C115.6,85.6,123.2,97.3,115.3,104.5L115.3,104.5L115.3,104.5z"></path>
                              
                                 <linearGradient id="SVGID_4_" gradientUnits="userSpaceOnUse" x1="39.5473" y1="90.515" x2="136.3825" y2="90.515" gradientTransform="matrix(1 0 0 -1 0 152)">
                                 <stop offset="0" style="stop-color:#ED2024"></stop>
                                 <stop offset="1" style="stop-color:#F05757"></stop>
                              </linearGradient>
                              <path class="st5" d="M60,105.5c-3.4-18.2-3.5-36.9-0.7-55.1c2.6,15.8,8,31.4,15.4,46.2c8.1-5.2,16.1-10.7,23.5-16.8
                                 c0.2-6.1,0.6-12,1.2-17.9c10.3,7.9,33,27.2,19.1,40c0.4-0.3,0.7-0.7,1.2-1c22.4-19.2,22.4-30.6,0-49.9
                                 c-9.9-8.4-21.3-16.1-32.4-23c-3.1,12.2-5.1,24.7-6.1,37.3c-5-16.1-5.8-28-5.8-44.5c-7.5-4.3-15.2-8.5-23-12.3
                                 c-13.2,33.6-16.3,70.8-8.7,106C49.2,111.7,54.6,108.7,60,105.5L60,105.5L60,105.5z"></path>
                              
                                 <linearGradient id="SVGID_5_" gradientUnits="userSpaceOnUse" x1="144.1806" y1="63.3529" x2="14.8406" y2="88.2629" gradientTransform="matrix(1 0 0 -1 0 152)">
                                 <stop offset="0" style="stop-color:#ED2024"></stop>
                                 <stop offset="1" style="stop-color:#F05757"></stop>
                              </linearGradient>
                              <path class="st6a" d="M99.4,61.9c10.3,8,33,27.2,19.1,40l1.2-1c6.8-5.8,6.8-13,1.9-20.5C116.3,72.4,106.1,65.4,99.4,61.9
                                 L99.4,61.9L99.4,61.9z"></path>
                           </g>
                           <g>
                              <path class="st7a" d="M834,63.8V50h-15.5h-8.2c3.2,0,5.4,2.2,5.7,4.5c0,4.4,0.1,9.4,0.1,15.6v54H834V90.6
                                 c0-19.8,9.2-26.9,24.2-26.8V50.1C845.7,50.3,837.5,54.8,834,63.8L834,63.8z"></path>
                              <path class="st7a" d="M573.3,106.1c0,6.9,0.6,16.3,1,18.2h-17.1c-0.6-1.5-1-5.3-1.1-8.1c-2.7,4.4-8,9.8-21.5,9.8
                                 c-17.7,0-25.3-11.6-25.3-23.1c0-16.8,13.4-24.5,35.2-24.5h11.3v-5.1c0-5.7-2-11.8-12.9-11.8c-9.9,0-11.9,4.5-13,10h-17.1
                                 c1.1-12.2,8.6-23.2,30.7-23.1c19.3,0.1,29.8,7.7,29.8,25.1V106.1L573.3,106.1z M555.8,89.6h-9.6c-13.2,0-18.9,3.9-18.9,12.1
                                 c0,6.2,4,11.1,11.9,11.1c14.7,0,16.5-10.1,16.5-21.1L555.8,89.6L555.8,89.6z"></path>
                              <path class="st7a" d="M745.2,90.8c0,11.2,5.7,20.8,16.7,20.8c9.6,0,12.5-4.3,14.6-9.1h18c-2.7,9.2-10.8,23.4-33.1,23.4
                                 c-24,0-34.3-18.5-34.3-37.8c0-22.8,11.7-39.8,35-39.8c24.9,0,33.3,18.7,33.3,36.3c0,2.4,0,4.1-0.3,6.2H745.2L745.2,90.8z
                                  M777.4,79.4c-0.2-9.8-4.5-18-15.4-18s-15.5,7.6-16.5,18H777.4L777.4,79.4z"></path>
                              <path class="st7a" d="M606.8,102.3c1.8,6.6,7,10.4,15.4,10.4s12-3.4,12-8.6s-3.3-7.8-15.1-10.7c-23.2-5.7-27.3-12.8-27.3-23.2
                                 s7.7-21.9,29-21.9s29,11.9,29.8,22h-17.1c-0.8-3.4-3.2-9.1-13.5-9.1c-8,0-10.5,3.7-10.5,7.5c0,4.3,2.5,6.4,15.1,9.4
                                 c24,5.6,27.8,13.8,27.8,24.7c0,12.5-9.7,23.1-30.9,23.1s-30.7-10.5-32.5-23.7L606.8,102.3L606.8,102.3z"></path>
                              <path class="st7a" d="M471,48.3c-12.3,0-18.9,5.6-23.1,12c-2.7-6.5-9-12-19.6-12c-11.2,0-17.3,5.5-20.9,11.4l0.1-9.7h-23.6
                                 c3.4,0,5.8,2.5,5.8,5l0,0c0.1,4.7,0.1,9.4,0.1,14.1v55h17.7V82.9c0-13.2,4.5-19.8,14.1-19.8s11.9,7,11.9,15.3v45.8h17.6V81.7
                                 c0-12,4-18.8,13.9-18.8s12.1,7.4,12.1,14.7v46.5h17.5V75.5C494.4,56,483.6,48.3,471,48.3L471,48.3z"></path>
                              <path class="st7a" d="M368.8,103.6V26.4h-17.9h-5.8c3.4,0,5.8,2.5,5.8,5l0,0v24.8c-1.8-3.6-7.5-7.9-19-7.9
                                 c-20.8,0-33.5,16.7-33.5,39.5s11.9,38.1,30.7,38.1c11.4,0,18.2-3.9,21.8-10.4l0.1,8.7h18C368.8,117.3,368.8,110.5,368.8,103.6
                                 L368.8,103.6z M334,111.4c-10.6,0-17.2-8.4-17.2-24.1s6.3-24.4,17.9-24.4c14.5,0,16.9,9.9,16.9,23.8
                                 C351.5,99.2,349.2,111.3,334,111.4L334,111.4z"></path>
                              <g>
                                 <path class="st7a" d="M265.6,50h-5.8c3.4,0,5.8,2.5,5.8,5v69.2h17.9V50H265.6z"></path>
                                 <path class="st7a" d="M283.5,29.1v-2.7h-17.9v20.6c0,0,0,0,0,0C275.5,46.9,283.5,38.9,283.5,29.1L283.5,29.1z"></path>
                              </g>
                              <g>
                                 <path class="st7a" d="M712.3,63.8V50.1h-15.1v0h-17.9v0h-11.9v13.8h11.9v41.5c0,12.8,4.9,19.9,18.4,19.9c3.9,0,9.1-0.1,12.9-1.4
                                    v-12.6c-1.7,0.3-3.9,0.4-5.3,0.4c-6.3,0-8-2.7-8-8.8V63.8H712.3L712.3,63.8z"></path>
                                 <path class="st7a" d="M697,29.1v-2.7h-17.9v20.6c0,0,0,0,0,0C689,46.9,697,38.9,697,29.1L697,29.1z"></path>
                              </g>
                              <path class="st7a" d="M231.3,25.9l-16.3,47c-4.8,14-9.3,27.1-11.3,35.9h-0.3c-2.2-9.7-6.2-22.3-10.8-36.3L177,25.9h-25.7
                                 c5.1,0,7.6,4.6,8.6,7.4l32.4,90.9h21.8l36.1-98.3H231.3L231.3,25.9z"></path>
                           </g>
                        </g>
                     </g>
                     </svg>
               </div>
               <div class="col-md-5  mt-md0 mt15">
                  <ul class="leader-ul f-16 f-md-18 w400 white-clr text-md-end text-center ">
                     <li>
                        <a href="#book-seat1" class="affiliate-link-btn">Subscribe To EarlyBird VIP List</a>
                     </li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!--Header Section Start -->
      <div class="header-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center mt20 mt-md50">
                  <div class="pre-heading f-16 f-md-20 w400 lh140 white-clr">
                     Copy Our Secret Formula To PULL Free Viral Traffic From YouTube Every Day!
                  </div>
               </div>
               <div class="col-12 mt-md50 mt30 f-md-45 f-28 w700 text-center white-clr lh140 text-capitalize">
                  A Push-Button Technology <span class="red-clr"> Creates <br class="d-none d-md-block">  YouTube Short Videos, Drive Tons of Free <br class="d-none d-md-block"> Viral Traffic & Profit</span>
               </div>
               <div class="col-12 mt-md25 mt20 f-16 f-md-20 w600 text-center lh140 white-clr text-capitalize">
                  Create Unlimited YouTube Shorts | No Camera & Recording | No Complex Editing Needed |<br class="d-none d-md-block"> No Paid Traffic | No Tech Hassles | Commercial License Included | 100% Newbie Friendly
               </div>
            </div>
            <div class="row d-flex align-items-center flex-wrap mt20 mt-md40">
               <div class="col-md-10 col-12 mx-auto">
			         <img src="assets/images/product-box.webp" alt="ProductBOX " class="img-fluid d-block mx-auto" />
                  <!--<div class="responsive-video">
                     <iframe src="https://webpull.dotcompal.com/video/embed/9vxq6c88sw" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                        box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                  </div>-->
               </div>
            </div>
         </div>
      </div>
      <!--Header Section End -->

      <!-- Second-Section Start -->
      <div class="second-section">
         <div class="container">
            <div class="row">
               <div class="col-12 z-index-9">
                  <div class="row header-list-block">
                     <div class="col-12 col-md-6">
                        <ul class="features-list pl0 f-18 f-md-20 lh140 w400 black-clr text-capitalize">
                           <li>
                              Tap into the <span class="w700"> Fast-Growing $200 Billion Video Marketing Industry </span> to Make Easy Income
                           </li>
                           <li>
                              <span class="w700">Create Unlimited YouTube Short Videos</span> for Any Niche and Offer
                           </li>
                           <li>
                              <span class="w700">Create Videos from Any Text or Script</span> in just 3 Simple Clicks
                           </li>
                           <li>
                              Create Videos <span class="w700">Using your Own Images or Search Images</span> with keywords
                           </li>
                           <li>
                              <span class="w700">Drive Unlimited Traffic & Sales</span> to Any Offer or Page
                           </li>
                        </ul>
                     </div>
                     <div class="col-12 col-md-6 mt10 mt-md0">
                        <ul class="features-list pl0 f-18 f-md-20 lh140 w400 black-clr text-capitalize">
                           <li>Create Videos by <span class="w700">Using Your Own Video Clips</span> or Use Stock Videos</li>
                           <li><span class="w700">Add Background Music and/or Voiceover</span> to Any Video</li>
                           <li>Inbuilt <span class="w700">Voiceover Creator</span> with 150+ Human Voice in 30+ Languages</li>
                           <li><span class="w700">No Camera</span>  Recording, <span class="w700">No Voice</span> , and <span class="w700">No Complex Editing</span> Required</li>
                           <li><span class="w700">100% Newbie Friendly,</span>  Required No Prior Experience or Tech Skills</li>
                        </ul>
                     </div>
                     <div class="col-12 mt10 mt-md20">
                        <ul class="features-list pl0 f-18 f-md-20 lh140 w400 black-clr text-capitalize">
                           <li><span class="w700">Free Commercial License</span> to Sell Video Services for High Profits</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div> 
            <div class="row mt20 mt-md70">
               <div class="col-12">
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 text-center">
                        <a href="#book-seat1" class="cta-link-btn">Subscribe To EarlyBird VIP List</a>
                     </div>
                  </div>
                  <div class="col-12 mx-auto mt20 mt-md40 f-18 f-md-20 w400 black-clr text-left lh160 text-md-center">
                     <ul class="ul-tick-icon1 p0 m0">
                        <li>Don't Pay $47/M, Get All Benefits For $17 One Time Only</li>
                        <li>Also Get A Chance To Win 10 FREE Copies</li>
                     </ul>
                </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Second-Second-End -->

      <!-------Form Section Start------------->
      <div class="form-sec mb30 mb-md100">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-12 mx-auto" id="form">
                  <div class="auto_responder_form inp-phold formbg" id="book-seat1">
                     <div class="f-28 f-md-40 w800 text-center lh140 black-clr" editabletype="text" style="z-index:10;">
                        Awesome! You're Almost Done.							
                     </div>
                     <div class="col-12 f-md-20 f-16 w600 lh140 mx-auto my20 text-center">
                        Fill up the details below
                     </div>
                     <!-- Aweber Form Code -->
                     <div class="col-12 p0 mt15 mt-md25" editabletype="form" style="z-index:10">
                        <!-- Aweber Form Code -->
                        <form method="post" class="af-form-wrapper register-form-style1 " accept-charset="UTF-8" action="https://www.aweber.com/scripts/addlead.pl" style="z-index: 10;">
                           <div style="display: none;">
                              <input type="hidden" name="meta_web_form_id" value="1914364641">
                              <input type="hidden" name="meta_split_id" value="">
                              <input type="hidden" name="listname" value="awlist6318712">
                              <input type="hidden" name="redirect" value="https://www.vidmaster.com/prelaunch-thankyou" id="redirect_6cd30a0aed0c2d7c98670b17f7027610">
                              <input type="hidden" name="meta_adtracking" value="My_Web_Form_2">
                              <input type="hidden" name="meta_message" value="1">
                              <input type="hidden" name="meta_required" value="name,email">
                              <input type="hidden" name="meta_tooltip" value="">
                           </div>
                           <div id="af-form-1914364641" class="af-form">
                              <div id="af-body-1914364641" class="af-body af-standards">
                                 <div class="af-element width-form">
                                    <label class="previewLabel" for="awf_field-114582649"></label>
                                    <div class="af-textWrap">
                                       <div class="input-type" style="z-index: 11;">
                                          <!--<span class="input-group-addon border1px"><i class="fa fa-user" aria-hidden="true"></i></span>-->
                                          <input id="awf_field-114582649" type="text" name="name" class="text form-control custom-input input-field" value="" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="500" placeholder="Your Name">
                                       </div>
                                    </div>
                                    <div class="af-clear bottom-margin"></div>
                                 </div>
                                 <div class="af-element width-form">
                                    <label class="previewLabel" for="awf_field-114582650"> </label>
                                    <div class="af-textWrap">
                                       <div class="input-type" style="z-index: 11;">
                                          <!--<span class="input-group-addon border1px"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>-->
                                          <input class="text form-control custom-input input-field" id="awf_field-114582650" type="text" name="email" value="" tabindex="501" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " placeholder="Your Email">
                                       </div>
                                    </div>
                                    <div class="af-clear bottom-margin"></div>
                                 </div>
                                 <input type="hidden" id="awf_field_aid" class="text" name="custom aid" value="undefined" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="502">
                                 <div class="af-element buttonContainer button-type register-btn1 f-18 f-md-21 width-form mt-md0">
                                    <input name="submit" id="af-submit-image-348552691" type="submit" class="image" style="max-width: 100%;" alt="Submit Form" tabindex="503" value="Subscribe To EarlyBird VIP List">
                                    <div class="af-clear bottom-margin"></div>
                                 </div>
                              </div>
                           </div>
                           <div style="display: none;"><img src="https://forms.aweber.com/form/displays.htm?id=7OycjOwsrEw=" alt=""></div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
     <!-------Form Section End------------->

     <!-- Footer-Section-Start -->
     <div class="footer-section">
      <div class="container">
          <div class="row">
              <div class="col-12 text-center">
               <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 858.2 150.9" style="enable-background:new 0 0 858.2 150.9; max-height: 55px;" xml:space="preserve">
           <style type="text/css">
              .st0{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_1_);}
              .st1{fill-rule:evenodd;clip-rule:evenodd;fill:#3C4650;}
              .st2{fill-rule:evenodd;clip-rule:evenodd;fill:#1C262D;}
              .st3{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_2_);}
              .st4{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_3_);}
              .st5{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_4_);}
              .st6{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_5_);}
              .st7{fill:#FFFFFF;}
           </style>
           <g id="Layer_1-2">
              <g>
                 <g>
                    
                       <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="25.2183" y1="-779.9527" x2="12.9283" y2="-954.9427" gradientTransform="matrix(1 0 0 -1 0 -776)">
                       <stop offset="0" style="stop-color:#CCCCCB"></stop>
                       <stop offset="0.32" style="stop-color:#3C4650"></stop>
                       <stop offset="0.7" style="stop-color:#252D33"></stop>
                       <stop offset="1" style="stop-color:#181D20"></stop>
                    </linearGradient>
                    <path class="st0" d="M40.1,3.1c-14.4-6-22.8-3.9-29.9,10.8C-0.2,35.8-2.6,80.4,3,112.9c5.4,20.7,22.1,25.1,32.2,15
                       c0.7-0.3,1.5-0.6,2.1-0.9c-4.3,1.9-9.1-0.3-10.5-4.8c-7.4-23.6-6.2-70,0.3-91.8C30.9,17.5,39.4,4,40.1,3.1L40.1,3.1z"></path>
                    <path class="st1" d="M31.6,0.4C1.2,18.4,7.9,102.5,18.1,129.5c5.3,1.4,11.3,0.9,17-1.6c0.7-0.3,1.5-0.6,2.1-0.9
                       c-4.3,1.9-9.1-0.3-10.5-4.8c-7.4-23.6-4.8-64.4,1.9-88.3C32.9,18.4,39.3,4.6,40.1,3.1c-3.2-1.3-3.4-1.4-6.2-2.2L31.6,0.4
                       L31.6,0.4z"></path>
                    <path class="st2" d="M2.4,109.5c0.2,1.2,0.3,2.3,0.6,3.4c3.4,15.5,18.4,20.7,32.2,15c0.7-0.3,1.5-0.6,2.1-0.9
                       c-4.3,1.9-9.1-0.3-10.5-4.8C13.8,125.2,5.6,121.7,2.4,109.5L2.4,109.5z"></path>
                    
                       <linearGradient id="SVGID_2_" gradientUnits="userSpaceOnUse" x1="-5.0837" y1="-921.3506" x2="166.8963" y2="-831.7707" gradientTransform="matrix(1 0 0 -1 0 -776)">
                       <stop offset="0" style="stop-color:#CCCCCB"></stop>
                       <stop offset="0.32" style="stop-color:#3C4650"></stop>
                       <stop offset="0.7" style="stop-color:#252D33"></stop>
                       <stop offset="1" style="stop-color:#181D20"></stop>
                    </linearGradient>
                    <path class="st3" d="M118.5,101.8c15-13.2,4.7-28.9-11.3-33.6c6.2,5.9,5.8,10.5-1.3,16.6c-10.3,8.8-22.8,17-34.3,24.1
                       c-11.4,6.9-24,13.9-36.3,19c-13.8,5.7-28.8,0.5-32.2-15c1.7,9.8,4.1,18.4,7.2,25c7.1,14.8,18.7,15.5,33.1,9.6
                       C65.7,138.2,99.7,117.5,118.5,101.8L118.5,101.8L118.5,101.8z"></path>
                    <path class="st1" d="M120.4,83.8c-14.2,18.1-62,46.8-80.9,55.3c-11.8,5.2-25.1,7.1-31.1-5.5c0.6,1.6,1.1,2.9,1.8,4.3
                       c7.1,14.8,18.7,15.5,33.1,9.6c22.4-9.3,56.5-30,75.2-45.6C124.2,96.5,123.7,90.1,120.4,83.8L120.4,83.8L120.4,83.8z"></path>
                    
                       <linearGradient id="SVGID_3_" gradientUnits="userSpaceOnUse" x1="-9.377" y1="-913.095" x2="162.593" y2="-823.515" gradientTransform="matrix(1 0 0 -1 0 -776)">
                       <stop offset="0" style="stop-color:#CCCCCB"></stop>
                       <stop offset="0.32" style="stop-color:#4E4F4F"></stop>
                       <stop offset="0.7" style="stop-color:#383838"></stop>
                       <stop offset="1" style="stop-color:#231F20"></stop>
                    </linearGradient>
                    <path class="st4" d="M35.2,127.9c-13.8,5.7-28.8,0.5-32.2-15c0.2,1.2,0.4,2.5,0.7,3.7c4.3,15.7,18.5,20.2,33,13.8
                       c24.1-10.6,47-27.5,57.7-36.5c-7.4,5.4-15.3,10.4-22.8,15C60.1,115.8,47.5,122.8,35.2,127.9L35.2,127.9L35.2,127.9z"></path>
                    <path class="st2" d="M115.3,104.5c1.1-0.9,2.1-1.8,3.2-2.6c11-10.1-0.8-24.2-11.3-33.6c4.9,4.8,5.7,8.7,2.1,13.1
                       C115.6,85.6,123.2,97.3,115.3,104.5L115.3,104.5L115.3,104.5z"></path>
                    
                       <linearGradient id="SVGID_4_" gradientUnits="userSpaceOnUse" x1="39.6241" y1="-837.5" x2="136.5" y2="-837.5" gradientTransform="matrix(1 0 0 -1 0 -776)">
                       <stop offset="0" style="stop-color:#ED2024"></stop>
                       <stop offset="1" style="stop-color:#F05757"></stop>
                    </linearGradient>
                    <path class="st5" d="M60,105.5c-3.4-18.2-3.5-36.9-0.7-55.1c2.6,15.8,8,31.4,15.4,46.2c8.1-5.2,16.1-10.7,23.5-16.8
                       c0.2-6.1,0.6-12,1.2-17.9c10.3,7.9,33,27.2,19.1,40c0.4-0.3,0.7-0.7,1.2-1c22.4-19.2,22.4-30.6,0-49.9
                       c-9.9-8.4-21.3-16.1-32.4-23c-3.1,12.2-5.1,24.7-6.1,37.3c-5-16.1-5.8-28-5.8-44.5c-7.5-4.3-15.2-8.5-23-12.3
                       c-13.2,33.6-16.3,70.8-8.7,106C49.2,111.7,54.6,108.7,60,105.5L60,105.5L60,105.5z"></path>
                    
                       <linearGradient id="SVGID_5_" gradientUnits="userSpaceOnUse" x1="144.1735" y1="-864.6838" x2="14.8335" y2="-839.7738" gradientTransform="matrix(1 0 0 -1 0 -776)">
                       <stop offset="0" style="stop-color:#ED2024"></stop>
                       <stop offset="1" style="stop-color:#F05757"></stop>
                    </linearGradient>
                    <path class="st6" d="M99.4,61.9c10.3,8,33,27.2,19.1,40l1.2-1c6.8-5.8,6.8-13,1.9-20.5C116.3,72.4,106.1,65.4,99.4,61.9
                       L99.4,61.9L99.4,61.9z"></path>
                 </g>
                 <g>
                    <path class="st7" d="M834,63.8V50h-15.5h-8.2c3.2,0,5.4,2.2,5.7,4.5c0,4.4,0.1,9.4,0.1,15.6v54H834V90.6
                       c0-19.8,9.2-26.9,24.2-26.8V50.1C845.7,50.3,837.5,54.8,834,63.8L834,63.8z"></path>
                    <path class="st7" d="M573.3,106.1c0,6.9,0.6,16.3,1,18.2h-17.1c-0.6-1.5-1-5.3-1.1-8.1c-2.7,4.4-8,9.8-21.5,9.8
                       c-17.7,0-25.3-11.6-25.3-23.1c0-16.8,13.4-24.5,35.2-24.5h11.3v-5.1c0-5.7-2-11.8-12.9-11.8c-9.9,0-11.9,4.5-13,10h-17.1
                       c1.1-12.2,8.6-23.2,30.7-23.1c19.3,0.1,29.8,7.7,29.8,25.1L573.3,106.1L573.3,106.1z M555.8,89.6h-9.6
                       c-13.2,0-18.9,3.9-18.9,12.1c0,6.2,4,11.1,11.9,11.1c14.7,0,16.5-10.1,16.5-21.1L555.8,89.6L555.8,89.6z"></path>
                    <path class="st7" d="M745.2,90.8c0,11.2,5.7,20.8,16.7,20.8c9.6,0,12.5-4.3,14.6-9.1h18c-2.7,9.2-10.8,23.4-33.1,23.4
                       c-24,0-34.3-18.5-34.3-37.8c0-22.8,11.7-39.8,35-39.8c24.9,0,33.3,18.7,33.3,36.3c0,2.4,0,4.1-0.3,6.2H745.2L745.2,90.8z
                        M777.4,79.4c-0.2-9.8-4.5-18-15.4-18s-15.5,7.6-16.5,18H777.4L777.4,79.4z"></path>
                    <path class="st7" d="M606.8,102.3c1.8,6.6,7,10.4,15.4,10.4s12-3.4,12-8.6s-3.3-7.8-15.1-10.7c-23.2-5.7-27.3-12.8-27.3-23.2
                       s7.7-21.9,29-21.9s29,11.9,29.8,22h-17.1c-0.8-3.4-3.2-9.1-13.5-9.1c-8,0-10.5,3.7-10.5,7.5c0,4.3,2.5,6.4,15.1,9.4
                       c24,5.6,27.8,13.8,27.8,24.7c0,12.5-9.7,23.1-30.9,23.1s-30.7-10.5-32.5-23.7L606.8,102.3L606.8,102.3z"></path>
                    <path class="st7" d="M471,48.3c-12.3,0-18.9,5.6-23.1,12c-2.7-6.5-9-12-19.6-12c-11.2,0-17.3,5.5-20.9,11.4l0.1-9.7h-23.6
                       c3.4,0,5.8,2.5,5.8,5l0,0c0.1,4.7,0.1,9.4,0.1,14.1v55h17.7V82.9c0-13.2,4.5-19.8,14.1-19.8s11.9,7,11.9,15.3v45.8h17.6V81.7
                       c0-12,4-18.8,13.9-18.8s12.1,7.4,12.1,14.7v46.5h17.5V75.5C494.4,56,483.6,48.3,471,48.3L471,48.3z"></path>
                    <path class="st7" d="M368.8,103.6V26.4h-17.9h-5.8c3.4,0,5.8,2.5,5.8,5l0,0v24.8c-1.8-3.6-7.5-7.9-19-7.9
                       c-20.8,0-33.5,16.7-33.5,39.5s11.9,38.1,30.7,38.1c11.4,0,18.2-3.9,21.8-10.4l0.1,8.7h18C368.8,117.3,368.8,110.5,368.8,103.6
                       L368.8,103.6z M334,111.4c-10.6,0-17.2-8.4-17.2-24.1s6.3-24.4,17.9-24.4c14.5,0,16.9,9.9,16.9,23.8
                       C351.5,99.2,349.2,111.3,334,111.4L334,111.4z"></path>
                    <g>
                       <path class="st7" d="M265.6,50h-5.8c3.4,0,5.8,2.5,5.8,5v69.2h17.9V50H265.6z"></path>
                       <path class="st7" d="M283.5,29.1v-2.7h-17.9V47l0,0C275.5,46.9,283.5,38.9,283.5,29.1L283.5,29.1z"></path>
                    </g>
                    <g>
                       <path class="st7" d="M712.3,63.8V50.1h-15.1l0,0h-17.9l0,0h-11.9v13.8h11.9v41.5c0,12.8,4.9,19.9,18.4,19.9
                          c3.9,0,9.1-0.1,12.9-1.4v-12.6c-1.7,0.3-3.9,0.4-5.3,0.4c-6.3,0-8-2.7-8-8.8V63.8H712.3L712.3,63.8z"></path>
                       <path class="st7" d="M697,29.1v-2.7h-17.9V47l0,0C689,46.9,697,38.9,697,29.1L697,29.1z"></path>
                    </g>
                    <path class="st7" d="M231.3,25.9l-16.3,47c-4.8,14-9.3,27.1-11.3,35.9h-0.3c-2.2-9.7-6.2-22.3-10.8-36.3L177,25.9h-25.7
                       c5.1,0,7.6,4.6,8.6,7.4l32.4,90.9h21.8l36.1-98.3L231.3,25.9L231.3,25.9z"></path>
                 </g>
              </g>
           </g>
           </svg>
                  <div editabletype="text" class="f-16 f-md-18 w400 lh140 mt20 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
              </div>
              <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-16 f-md-18 w400 lh140 white-clr text-xs-center">Copyright © VidMaster</div>
                  <ul class="footer-ul f-16 f-md-18 w400 white-clr text-center text-md-right">
                      <li><a href="https://support.bizomart.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                      <li><a href="http://www.vidmaster.co/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                      <li><a href="http://www.vidmaster.co/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                      <li><a href="https://warriorplus.com/o2/disclaimer/jrrjv5" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                      <li><a href="http://www.vidmaster.co/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                      <li><a href="http://www.vidmaster.co/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                      <li><a href="http://www.vidmaster.co/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
              </div>
          </div>
      </div>
  </div>
      <!-- Footer-Section-End -->
      <script type="text/javascript">
      // Special handling for in-app browsers that don't always support new windows
      (function() {
         function browserSupportsNewWindows(userAgent) {
            var rules = [
                  'FBIOS',
                  'Twitter for iPhone',
                  'WebView',
                  '(iPhone|iPod|iPad)(?!.*Safari\/)',
                  'Android.*(wv|\.0\.0\.0)'
            ];
            var pattern = new RegExp('(' + rules.join('|') + ')', 'ig');
            return !pattern.test(userAgent);
         }

         if (!browserSupportsNewWindows(navigator.userAgent || navigator.vendor || window.opera)) {
            document.getElementById('af-form-1914364641').parentElement.removeAttribute('target');
         }
      })();
      </script>
      <script type="text/javascript">

         (function() {
            var IE = /*@cc_on!@*/false;
            if (!IE) { return; }
            if (document.compatMode && document.compatMode == 'BackCompat') {
                  if (document.getElementById("af-form-1914364641")) {
                     document.getElementById("af-form-1914364641").className = 'af-form af-quirksMode';
                  }
                  if (document.getElementById("af-body-1914364641")) {
                     document.getElementById("af-body-1914364641").className = "af-body inline af-quirksMode";
                  }
                  if (document.getElementById("af-header-1914364641")) {
                     document.getElementById("af-header-1914364641").className = "af-header af-quirksMode";
                  }
                  if (document.getElementById("af-footer-1914364641")) {
                     document.getElementById("af-footer-1914364641").className = "af-footer af-quirksMode";
                  }
            }
         })();

      </script>


   </body>
</html>
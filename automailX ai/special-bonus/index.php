<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <!-- Tell the browser to be responsive to screen width -->
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
      <meta name="title" content="AutoMailX Bonuses">
      <meta name="description" content="AutoMailX Bonuses">
      <meta name="keywords" content="World’s First ChatGPT AI Powered Email Marketing App Writes, Design & Send Unlimited Profit Pulling Emails and Messages with Just One Keyword for Tons of Traffic, Commissions, & Sales on Autopilot.">
      <meta property="og:image" content="https://automailxai.com/special/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Dr. Amit Pareek">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="AutoMailX Bonuses">
      <meta property="og:description" content="World’s First ChatGPT AI Powered Email Marketing App Writes, Design & Send Unlimited Profit Pulling Emails and Messages with Just One Keyword for Tons of Traffic, Commissions, & Sales on Autopilot.">
      <meta property="og:image" content="https://automailxai.com/special/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="AutoMailX Bonuses">
      <meta property="twitter:description" content="World’s First ChatGPT AI Powered Email Marketing App Writes, Design & Send Unlimited Profit Pulling Emails and Messages with Just One Keyword for Tons of Traffic, Commissions, & Sales on Autopilot.">
      <meta property="twitter:image" content="https://automailxai.com/special-bonus/thumbnail.png">
      <title>AutoMailX Bonuses</title>
      <!-- Shortcut Icon  -->
      <meta property="og:image" content="https://automailxai.com/special/thumbnail.png">
      <!-- Css CDN Load Link -->
      <link rel="stylesheet" type="text/css" href="assets/css/style.css">
      <link rel="stylesheet" type="text/css" href="assets/css/bonus-style.css">
      <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
      <link rel="preconnect" href="https://fonts.googleapis.com">
       <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&family=Inter:wght@300;400;500;600;700;800;900&display=swap" rel="stylesheet">
      <link rel="stylesheet" href="../common_assets/css/bootstrap.min.css" type="text/css">
      <script src="https://cdn.oppyo.com/launches/writerarc/common_assets/js/jquery.min.js"></script>
   </head>
   <body>
   
      <!-- New Timer  Start-->
      <?php
         $date = 'November 5 2023 11:59 AM EST';
         $exp_date = strtotime($date);
         $now = time();  
         /*
         
         $date = date('F d Y g:i:s A eO');
         $rand_time_add = rand(700, 1200);
         $exp_date = strtotime($date) + $rand_time_add;
         $now = time();*/
         
         if ($now < $exp_date) {
         ?>
      <?php
         } else {
         	echo "Times Up";
         }
         ?>
      <!-- New Timer End -->
      <?php
         if(!isset($_GET['afflink'])){
         $_GET['afflink'] = 'https://automailxai.com/special/';
         $_GET['name'] = 'Pranshu Gupta';      
         }
         ?>

      <!-- Header Section Start -->   
      <div class="main-header">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="text-center">
                     <div class="f-md-32 f-20 lh140 w400 white-clr d-block d-md-flex align-items-center justify-content-center">
                        <span class="w600"><?php echo $_GET['name'];?>'s</span> &nbsp;special bonus for &nbsp;
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 1164.92 192.13" style="max-height: 55px;"><defs><style>.cls-1wh,.cls-2wh,.cls-3wh,.cls-4wh,.cls-5wh{fill-rule:evenodd;}.cls-1wh{fill:url(#linear-gradient);}.cls-2wh{fill:url(#linear-gradient-2);}.cls-3wh{fill:url(#linear-gradient-3);}.cls-4wh{fill:url(#linear-gradient-4);}.cls-5wh{fill:url(#linear-gradient-5);}.cls-6wh{fill:#fff;}.cls-7wh{fill:url(#linear-gradient-6);}</style><linearGradient id="linear-gradient" x1="82.05" y1="127.34" x2="150.42" y2="127.34" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#624ef2"></stop><stop offset="1" stop-color="#1e9af7"></stop></linearGradient><linearGradient id="linear-gradient-2" x1="7.14" y1="79.25" x2="123.43" y2="79.25" xlink:href="#linear-gradient"></linearGradient><linearGradient id="linear-gradient-3" x1="0" y1="116.36" x2="45.71" y2="116.36" xlink:href="#linear-gradient"></linearGradient><linearGradient id="linear-gradient-4" x1="3.94" y1="157.47" x2="142.12" y2="157.47" xlink:href="#linear-gradient"></linearGradient><linearGradient id="linear-gradient-5" x1="3.58" y1="75.02" x2="182.4" y2="75.02" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#ff2d2d"></stop><stop offset="1" stop-color="#ffbc00"></stop></linearGradient><linearGradient id="linear-gradient-6" x1="971.74" y1="90.91" x2="1164.92" y2="90.91" xlink:href="#linear-gradient"></linearGradient></defs><title>AutomailX White</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1wh" d="M150.42,67.57V177.2a15.41,15.41,0,0,1-3.38,9.92q-20.67-19.74-39-37.28l-26-24.94Z"></path><path class="cls-2wh" d="M123.43,48.09l-62.5,62.32Q47.16,98.7,33.43,86.92q-8.17-7-16.31-14.11c-3.34-2.91-6.75-5.85-10-8.88a6.08,6.08,0,0,1,1.32-.33l24.25-3.27,28.14-3.8,33-4.45Z"></path><path class="cls-3wh" d="M45.71,119.33,25.4,139.11Q15.2,149,5.81,158.19c-2.33,3-5.83,2.65-5.81-2.51V76.9c0-5.16,3.5-5.32,5.81-2.2l.39.41h0c.73,1.66,1.4,3.32,2,4.81l.15.38Z"></path><path class="cls-4wh" d="M142.12,190.87a14.7,14.7,0,0,1-8.88,1l-39.42-7.34-33-6.14-28.14-5.24L8.44,168.61a8.2,8.2,0,0,1-4.5-2.68L25.82,145q11.1-10.61,23.23-22.21l15.81,16.51L78,128.3q12.35,12.06,25.78,25.16Q121.84,171.09,142.12,190.87Z"></path><path class="cls-5wh" d="M10.15,79.16,65,136.41l92.87-81.5,3.93,12.24L182.4,13.62,128.57,32.19l11.14,5.9L61.05,116.53s-54.7-47.67-57.47-50c1.16,1.75,6.57,12.68,6.57,12.68Z"></path><path class="cls-6wh" d="M283,50.49H247.2L201.86,181.82h29.75l9.74-29.95h47.42l9.73,29.95h29.75ZM248.4,130.2l16.17-49.7h1l16.14,49.7Z"></path><path class="cls-6wh" d="M406.68,139.88V83.32H434v98.5H407.77V163.93h-1a29.1,29.1,0,0,1-11.06,13.91q-7.73,5.26-18.82,5.26a33.23,33.23,0,0,1-17.38-4.49,30.61,30.61,0,0,1-11.7-12.76q-4.2-8.26-4.27-19.81V83.32h27.32v57.84q.06,8.73,4.68,13.79T387.89,160a19.37,19.37,0,0,0,9.23-2.27,17.53,17.53,0,0,0,7-6.77A21,21,0,0,0,406.68,139.88Z"></path><path class="cls-6wh" d="M510,180.6q-2.06.65-5.78,1.51a48.09,48.09,0,0,1-9,1.06,41.39,41.39,0,0,1-17.28-2.63,24.12,24.12,0,0,1-11.51-9.36q-4.11-6.36-4-16v-51.3H448.87V83.32h13.47V59.72h27.32v23.6h18.53v20.52H489.66v47.71a12.24,12.24,0,0,0,1.15,5.87,6.34,6.34,0,0,0,3.24,2.92,13.08,13.08,0,0,0,4.84.83,21.79,21.79,0,0,0,3.85-.35l2.95-.55Z"></path><path class="cls-6wh" d="M569.43,183.74q-14.94,0-25.81-6.38a43,43,0,0,1-16.77-17.83Q521,148.09,521,133t5.9-26.71a43,43,0,0,1,16.77-17.82Q554.5,82,569.43,82t25.81,6.38A43,43,0,0,1,612,106.24q5.91,11.46,5.9,26.71T612,159.53a43,43,0,0,1-16.77,17.83Q584.38,183.75,569.43,183.74Zm.13-21.16a16.88,16.88,0,0,0,11.35-3.88,23.82,23.82,0,0,0,6.89-10.61,46.31,46.31,0,0,0,2.35-15.33,46.31,46.31,0,0,0-2.35-15.33,24.09,24.09,0,0,0-6.89-10.64,16.8,16.8,0,0,0-11.35-3.91,17.26,17.26,0,0,0-11.51,3.91,23.74,23.74,0,0,0-7,10.64,46.58,46.58,0,0,0-2.34,15.33,46.58,46.58,0,0,0,2.34,15.33,23.48,23.48,0,0,0,7,10.61A17.35,17.35,0,0,0,569.56,162.58Z"></path><path class="cls-6wh" d="M640.74,50.49h19l44.63,109h1.54l44.64-109h19V181.82H754.63V82h-1.28l-41,99.78H697.94L656.9,82h-1.28v99.78H640.74Z"></path><path class="cls-6wh" d="M828.25,184.13a39.77,39.77,0,0,1-17-3.56,28.82,28.82,0,0,1-12.12-10.33q-4.49-6.77-4.49-16.38,0-8.46,3.34-13.76a23.63,23.63,0,0,1,8.91-8.3,47.46,47.46,0,0,1,12.35-4.52q6.76-1.52,13.62-2.41,9-1.16,14.59-1.76t8.21-2.08q2.6-1.48,2.6-5.13v-.52q0-9.48-5.16-14.75t-15.62-5.25q-10.85,0-17,4.74a28,28,0,0,0-8.66,10.13l-14.37-5.13a34.69,34.69,0,0,1,10.3-14,38.56,38.56,0,0,1,14.11-7A58.86,58.86,0,0,1,837,82a61.89,61.89,0,0,1,10.93,1.12,35.92,35.92,0,0,1,12,4.58,26.84,26.84,0,0,1,9.65,10.46q3.86,7,3.85,18.72v64.9H858.26V168.48h-.77a26.84,26.84,0,0,1-5.13,6.86,29.6,29.6,0,0,1-9.56,6.22A36.78,36.78,0,0,1,828.25,184.13Zm2.3-13.6q9,0,15.17-3.53a24.19,24.19,0,0,0,9.36-9.1,23.36,23.36,0,0,0,3.18-11.74V132.31q-1,1.16-4.2,2.09a64,64,0,0,1-7.44,1.6q-4.2.67-8.15,1.15c-2.63.32-4.75.59-6.38.81a58.57,58.57,0,0,0-11,2.46,19.26,19.26,0,0,0-8.21,5.07q-3.12,3.36-3.11,9.14,0,7.89,5.87,11.89T830.55,170.53Z"></path><path class="cls-6wh" d="M911.79,66.9a10.65,10.65,0,0,1-7.6-3,9.86,9.86,0,0,1,0-14.49,11.07,11.07,0,0,1,15.2,0,9.86,9.86,0,0,1,0,14.49A10.65,10.65,0,0,1,911.79,66.9Zm-7.7,114.92V83.32h15.14v98.5Z"></path><path class="cls-6wh" d="M962.06,50.49V181.82H946.93V50.49Z"></path><path class="cls-7wh" d="M1045.34,0l26.37,62h1.42l47.14-62h44.65l-70.4,90.91L1136,181.82h-43.32l-27-62.06h-1.42l-47.59,62.06H971.74l72.18-90.91L1003,0Z"></path></g></g></svg>
                     </div>
                  </div>
               </div>
               <div class="col-12 text-center mt20 mt-md60">
                  <div class="f-md-22 f-18 w600 text-center white-clr pre-heading lh150">
                  Zero Manual Work. Zero List Building Hassles. Zero Downtime. 
                  </div>
               </div>
               <div class="col-12 mt-md40 mt20 ">
                  <div class="main-heading text-center">
                     <div class="f-md-38 f-28 w700 text-center black-clr lh140">   
                        <span class="blue-gradient">Revealing “MailChimp” Killer AI-Powered App That</span> Writes &amp; Delivers Unlimited Stunning Emails, And Drives Tons of Clicks, Sales, and Commissions
                        <span>
                      </span></div>
                      <div class="f-18 f-md-22 lh140 w600 text-center white-clr heading-blue-shape mt10">
                      Let AI Do It All. No Tech Hassles. No Monthly Fee Ever...
                      </div>
                   </div>
                </div>

                <div class="col-12 mt-md50 mt20 text-center">
                  <div class="f-20 f-md-26 w500 text-center lh150 white-clr post-heading">
                  <span class="orange-clr w700">Done-For-You Leads, Built-In High-Speed SMTP</span> (Email Delivery Servers), <span class="orange-clr w700">Smart Tagging, Unlimited List Importing &amp; Mailing</span> - All Within an Easy Cloud Based App.
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md30">
               <div class="col-md-10 col-12 mx-auto">
                   <img src="assets/images/product-box.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>
         </div>
      </div>
      <!-- Header Section End -->

      <!-- header List Section Start -->
      <div class="header-list-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row header-list-block" >
                     <div class="col-12 col-md-6">
                        <div class="f-16 f-md-18 lh140 w400">
                           <ul class="bonus-list pl0 m0">
                              <li><span class="w600">Create 100s of High-Quality Emails and Messages</span> With Just One Keyword.</li>
                              <li><span class="w600">Create Engaging Emails </span> for Marketing, Sales, Promotion and many more...</li>
                              <li> <span class="w600">AI Enabled Smart Tagging </span>for Lead Personalization & Traffic.</li>
                              <li><span class="w600">Send Unlimited Emails </span>to Unlimited Subscribers.</li>
                              <li><span class="w600">Collect Unlimited Leads </span> with Built-In Lead Form.</li>
                              <li><span class="w600">Free SMTP </span> for unrestricted Mailing.</li>
                              <li><span class="w600">Upload Unlimited List </span> with Zero Restrictions.</li>
                           </ul>
                        </div>
                     </div>
                     <div class="col-12 col-md-6">
                        <div class="f-16 f-md-18 lh140 w400">
                           <ul class="bonus-list pl0 m0">
                              <li><span class="w600">Get 4X More ROI </span> and Profits than Ever.</li>
                              <li><span class="w600">Advance Inbuilt AI Text & Inline Editor </span> to Craft Beautiful Email.</li>
                              <li><span class="w600">100% Control on your online business</span> GDPR & Can-Spam Compliant.</li>
                              <li><span class="w600">100+ High Converting </span> Templates for Webforms & Email.</span></li>
                              <li><span class="w600">No Monthly Fees, Pay One Time Only.</span></li>
                              <li><span class="w600">Commercial License Included.</li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md70">
                <div class="col-md-12 col-md-12 col-12 text-center ">
                    <div class="f-md-24 f-20 text-center mt3 black-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                    <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 black-clr">TAKE ACTION NOW!</div>
                    <div class="f-16 f-md-20 lh150 w400 text-center mt20 black-clr">
                        Use Coupon Code <span class="w700 blue-gradient">"ADMINMAILX"</span> for an Additional <span class="w700 blue-gradient">35% Discount</span> on Commercial Licence
                    </div>
                </div>
                <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                    <a href="https://automailxai.com/special/" class="text-center bonusbuy-btn">
                        <span class="text-center">Grab AutoMailX + My 20 Exclusive Bonuses</span>
                    </a>
                </div>
                <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                    <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
                </div>
                <div class="col-12 mt15 mt-md40" align="center">
                    <h3 class="f-md-22 f-20 w500 text-center black-clr">Coupon Is Expiring In... </h3>
                </div>
                <!-- Timer -->
                <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                    <div class="countdown counter-black black-clr"><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01</span><span class="f-14 f-md-18 smmltd">Days</span> </div><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01</span><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">06</span><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div><div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">50</span><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div></div>
                </div>
                <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- header List Section End -->

 
      <!-- Step Section End -->
      <div class="step-section">
         <div class="container ">
            <div class="row ">
               <div class="col-12 f-28 f-md-45 w600 lh140 black-clr text-center">
               Create & Send Profit-Pulling Emails with <br class="d-none d-md-block"><span class="blue-gradient d-inline-grid"> AutoMailX in Just 3 Simple Clicks
                  <img src="assets/images/let-line.webp" class="img-fluid mx-auto"></span>
               </div>
               <div class="col-12 mt30 mt-md80">
                  <div class="row align-items-center">
                     <div class="col-12 col-md-5 relative">
                        <img src="assets/images/step1.webp" class="img-fluid">
                        <div class=" step1 f-28 f-md-50 w600 mt15">
                        Log in, Upload your List, or Use Ready-To-Convert Email List -
                        </div>
                        <div class="f-18 f-md-20 w400 lh140 mt15">
                        Login to your AutoMailX account &amp; Import list upto 1 million+ Leads with No Limitation
                        </div>
                        <img src="assets/images/left-arrow.webp" class="img-fluid d-none d-md-block ms-auto step-arrow1">
                     </div>
                     <div class="col-12 col-md-7 mt20 mt-md0 ">
                     <video width="100%" height="auto" loop="" autoplay="" muted="muted" class="gif-bg">
                        <source src="assets/images/step1.mp4" type="video/mp4">
                     </video>
                  </div>
                  </div>
               </div>
               <div class="col-12 mt-md150">
                  <div class="row align-items-center ">
                     <div class="col-12 col-md-5 order-md-2 relative">
                        <img src="assets/images/step2.webp" class="img-fluid">
                        <div class=" step2 f-28 f-md-50 w600 mt15">
                        Entry Keyword & Generate
                        </div>
                        <div class="f-18 f-md-20 w400 lh140 mt15">
                        AutoMailX, AI generates profitable, engaging & high-quality email for you in seconds.
                        </div>
                        <img src="assets/images/right-arrow.webp" class="img-fluid d-none d-md-block step-arrow2">
                     </div>
                     <div class="col-md-7 mt20 mt-md0 order-md-1 ">
                     <video width="100%" height="auto" loop="" autoplay="" muted="muted" class="gif-bg">
                        <source src="assets/images/step2.mp4" type="video/mp4">
                     </video>
                  </div>
                  </div>
               </div>
               <div class="col-12 mt-md150">
                  <div class="row align-items-center">
                     <div class="col-12 col-md-5">
                        <img src="assets/images/step3.webp" class="img-fluid">
                        <div class=" step3 f-28 f-md-50 w600 mt15">
                        Send &amp; Profit:
                        </div>
                        <div class="f-18 f-md-20 w400 lh140 mt15">
                        Send unlimited emails directly into inbox for tons of autopilot profits with just push of a button.
                        </div>
                     </div>
                     <div class="col-12 col-md-7 mt20 mt-md0">
                     <video width="100%" height="auto" loop="" autoplay="" muted="muted" class="gif-bg" >
                   <source src="assets/images/step3.mp4" type="video/mp4">
               </video>
                     </div>
                  </div>
               </div>

               <div class="col-12 f-md-24 f-20 w700 lh140 text-center mt20 mt-md40 aos-init aos-animate" data-aos="fade-up">
               <div class="smile-text">It Just Takes Less than 60 Seconds...</div>
            </div>
            <div class="col-12 text-center">
               
               <div class="d-flex gap-2 gap-md-1 justify-content-center flex-wrap mt20 mt-md30">
                  <div class="d-flex align-items-center gap-2 justify-content-center">
                     <img src="assets/images/red-cross.webp" alt="Cross" class="img-fluid d-block">
                     <div class="f-md-28 w700 lh140 f-20 red-clr"> No Email Writing &nbsp; </div>
                  </div>
                  <div class="d-flex align-items-center gap-2 justify-content-center">
                     <img src="assets/images/red-cross.webp" alt="Cross" class="img-fluid d-block">
                     <div class="f-md-28 w700 lh140 f-20 red-clr"> No Designer &nbsp;</div>
                  </div>
                  <div class="d-flex align-items-center gap-2 justify-content-center">
                     <img src="assets/images/red-cross.webp" alt="Cross" class="img-fluid d-block">
                     <div class="f-md-28 w700 lh140 f-20 red-clr">No Freelancer &nbsp;</div>
                  </div>
                  <div class="d-flex align-items-center gap-2 justify-content-center">
                     <img src="assets/images/red-cross.webp" alt="Cross" class="img-fluid d-block">
                     <div class="f-md-28 w700 lh140 f-20 red-clr">No 3rd Party Apps &nbsp;</div>
                  </div>
                  <div class="d-flex align-items-center gap-2 justify-content-center">
                     <img src="assets/images/red-cross.webp" alt="Cross" class="img-fluid d-block">
                     <div class="f-md-28 w700 lh140 f-20 red-clr">No SMTP </div>
                  </div>
               </div>
            </div>
            <div class="col-12 f-18 f-md-24 w400 lh140 black-clr text-center mt20 mt-md30">
                  Plus, with included free commercial license, this is the easiest &amp; fastest way to start 6 figure business and help desperate businesses in no time! 
               </div>
            </div>
         </div>
      </div>
<!-- CTA Btn Start-->
<div class="cta-section-new">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 blue-gradient">"ADMINMAILX"</span> for an Additional <span class="w700 blue-gradient">35% Discount</span> on Commercial Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                     <span class="text-center">Grab AutoMailX + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                           <br>
                        <span class="f-14 f-md-18 smmltd">Days</span>
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Sec</span>
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Btn End -->
      

      <!-- Proudly Section Start -->
      <div class="next-gen-sec" id="product">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <!-- <div class="pre-heading f-md-32 f-24 w600 text-center white-clr lh140">
                     Proudly Presenting... 
                  </div> -->
                  <div class="proudly-sec f-26 w700 text-center white-clr">
                     Proudly <br> Presenting...
                  </div>
               </div>
               <div class="col-12 mt-md20 mt20  mt-md50 text-center">
                 <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 1164.92 192.13" style="max-height: 55px;"><defs><style>.cls-1wh,.cls-2wh,.cls-3wh,.cls-4wh,.cls-5wh{fill-rule:evenodd;}.cls-1wh{fill:url(#linear-gradient);}.cls-2wh{fill:url(#linear-gradient-2);}.cls-3wh{fill:url(#linear-gradient-3);}.cls-4wh{fill:url(#linear-gradient-4);}.cls-5wh{fill:url(#linear-gradient-5);}.cls-6wh{fill:#fff;}.cls-7wh{fill:url(#linear-gradient-6);}</style><linearGradient id="linear-gradient" x1="82.05" y1="127.34" x2="150.42" y2="127.34" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#624ef2"></stop><stop offset="1" stop-color="#1e9af7"></stop></linearGradient><linearGradient id="linear-gradient-2" x1="7.14" y1="79.25" x2="123.43" y2="79.25" xlink:href="#linear-gradient"></linearGradient><linearGradient id="linear-gradient-3" x1="0" y1="116.36" x2="45.71" y2="116.36" xlink:href="#linear-gradient"></linearGradient><linearGradient id="linear-gradient-4" x1="3.94" y1="157.47" x2="142.12" y2="157.47" xlink:href="#linear-gradient"></linearGradient><linearGradient id="linear-gradient-5" x1="3.58" y1="75.02" x2="182.4" y2="75.02" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#ff2d2d"></stop><stop offset="1" stop-color="#ffbc00"></stop></linearGradient><linearGradient id="linear-gradient-6" x1="971.74" y1="90.91" x2="1164.92" y2="90.91" xlink:href="#linear-gradient"></linearGradient></defs><title>AutomailX White</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1wh" d="M150.42,67.57V177.2a15.41,15.41,0,0,1-3.38,9.92q-20.67-19.74-39-37.28l-26-24.94Z"></path><path class="cls-2wh" d="M123.43,48.09l-62.5,62.32Q47.16,98.7,33.43,86.92q-8.17-7-16.31-14.11c-3.34-2.91-6.75-5.85-10-8.88a6.08,6.08,0,0,1,1.32-.33l24.25-3.27,28.14-3.8,33-4.45Z"></path><path class="cls-3wh" d="M45.71,119.33,25.4,139.11Q15.2,149,5.81,158.19c-2.33,3-5.83,2.65-5.81-2.51V76.9c0-5.16,3.5-5.32,5.81-2.2l.39.41h0c.73,1.66,1.4,3.32,2,4.81l.15.38Z"></path><path class="cls-4wh" d="M142.12,190.87a14.7,14.7,0,0,1-8.88,1l-39.42-7.34-33-6.14-28.14-5.24L8.44,168.61a8.2,8.2,0,0,1-4.5-2.68L25.82,145q11.1-10.61,23.23-22.21l15.81,16.51L78,128.3q12.35,12.06,25.78,25.16Q121.84,171.09,142.12,190.87Z"></path><path class="cls-5wh" d="M10.15,79.16,65,136.41l92.87-81.5,3.93,12.24L182.4,13.62,128.57,32.19l11.14,5.9L61.05,116.53s-54.7-47.67-57.47-50c1.16,1.75,6.57,12.68,6.57,12.68Z"></path><path class="cls-6wh" d="M283,50.49H247.2L201.86,181.82h29.75l9.74-29.95h47.42l9.73,29.95h29.75ZM248.4,130.2l16.17-49.7h1l16.14,49.7Z"></path><path class="cls-6wh" d="M406.68,139.88V83.32H434v98.5H407.77V163.93h-1a29.1,29.1,0,0,1-11.06,13.91q-7.73,5.26-18.82,5.26a33.23,33.23,0,0,1-17.38-4.49,30.61,30.61,0,0,1-11.7-12.76q-4.2-8.26-4.27-19.81V83.32h27.32v57.84q.06,8.73,4.68,13.79T387.89,160a19.37,19.37,0,0,0,9.23-2.27,17.53,17.53,0,0,0,7-6.77A21,21,0,0,0,406.68,139.88Z"></path><path class="cls-6wh" d="M510,180.6q-2.06.65-5.78,1.51a48.09,48.09,0,0,1-9,1.06,41.39,41.39,0,0,1-17.28-2.63,24.12,24.12,0,0,1-11.51-9.36q-4.11-6.36-4-16v-51.3H448.87V83.32h13.47V59.72h27.32v23.6h18.53v20.52H489.66v47.71a12.24,12.24,0,0,0,1.15,5.87,6.34,6.34,0,0,0,3.24,2.92,13.08,13.08,0,0,0,4.84.83,21.79,21.79,0,0,0,3.85-.35l2.95-.55Z"></path><path class="cls-6wh" d="M569.43,183.74q-14.94,0-25.81-6.38a43,43,0,0,1-16.77-17.83Q521,148.09,521,133t5.9-26.71a43,43,0,0,1,16.77-17.82Q554.5,82,569.43,82t25.81,6.38A43,43,0,0,1,612,106.24q5.91,11.46,5.9,26.71T612,159.53a43,43,0,0,1-16.77,17.83Q584.38,183.75,569.43,183.74Zm.13-21.16a16.88,16.88,0,0,0,11.35-3.88,23.82,23.82,0,0,0,6.89-10.61,46.31,46.31,0,0,0,2.35-15.33,46.31,46.31,0,0,0-2.35-15.33,24.09,24.09,0,0,0-6.89-10.64,16.8,16.8,0,0,0-11.35-3.91,17.26,17.26,0,0,0-11.51,3.91,23.74,23.74,0,0,0-7,10.64,46.58,46.58,0,0,0-2.34,15.33,46.58,46.58,0,0,0,2.34,15.33,23.48,23.48,0,0,0,7,10.61A17.35,17.35,0,0,0,569.56,162.58Z"></path><path class="cls-6wh" d="M640.74,50.49h19l44.63,109h1.54l44.64-109h19V181.82H754.63V82h-1.28l-41,99.78H697.94L656.9,82h-1.28v99.78H640.74Z"></path><path class="cls-6wh" d="M828.25,184.13a39.77,39.77,0,0,1-17-3.56,28.82,28.82,0,0,1-12.12-10.33q-4.49-6.77-4.49-16.38,0-8.46,3.34-13.76a23.63,23.63,0,0,1,8.91-8.3,47.46,47.46,0,0,1,12.35-4.52q6.76-1.52,13.62-2.41,9-1.16,14.59-1.76t8.21-2.08q2.6-1.48,2.6-5.13v-.52q0-9.48-5.16-14.75t-15.62-5.25q-10.85,0-17,4.74a28,28,0,0,0-8.66,10.13l-14.37-5.13a34.69,34.69,0,0,1,10.3-14,38.56,38.56,0,0,1,14.11-7A58.86,58.86,0,0,1,837,82a61.89,61.89,0,0,1,10.93,1.12,35.92,35.92,0,0,1,12,4.58,26.84,26.84,0,0,1,9.65,10.46q3.86,7,3.85,18.72v64.9H858.26V168.48h-.77a26.84,26.84,0,0,1-5.13,6.86,29.6,29.6,0,0,1-9.56,6.22A36.78,36.78,0,0,1,828.25,184.13Zm2.3-13.6q9,0,15.17-3.53a24.19,24.19,0,0,0,9.36-9.1,23.36,23.36,0,0,0,3.18-11.74V132.31q-1,1.16-4.2,2.09a64,64,0,0,1-7.44,1.6q-4.2.67-8.15,1.15c-2.63.32-4.75.59-6.38.81a58.57,58.57,0,0,0-11,2.46,19.26,19.26,0,0,0-8.21,5.07q-3.12,3.36-3.11,9.14,0,7.89,5.87,11.89T830.55,170.53Z"></path><path class="cls-6wh" d="M911.79,66.9a10.65,10.65,0,0,1-7.6-3,9.86,9.86,0,0,1,0-14.49,11.07,11.07,0,0,1,15.2,0,9.86,9.86,0,0,1,0,14.49A10.65,10.65,0,0,1,911.79,66.9Zm-7.7,114.92V83.32h15.14v98.5Z"></path><path class="cls-6wh" d="M962.06,50.49V181.82H946.93V50.49Z"></path><path class="cls-7wh" d="M1045.34,0l26.37,62h1.42l47.14-62h44.65l-70.4,90.91L1136,181.82h-43.32l-27-62.06h-1.42l-47.59,62.06H971.74l72.18-90.91L1003,0Z"></path></g></g></svg>
                </div>
               
               <div class="col-12 mt-md40 mt20 ">
                  <div class="main-heading text-center">
                     <div class="f-md-38 f-28 w700 text-center black-clr lh140">   
                        <span class="blue-gradient">Revealing “MailChimp” Killer AI-Powered App That</span> Writes &amp; Delivers Unlimited Stunning Emails, And Drives Tons of Clicks, Sales, and Commissions
                        <span>
                      </span></div>
                      <div class="f-18 f-md-22 lh140 w600 text-center white-clr heading-blue-shape mt10">
                      Let AI Do It All. No Tech Hassles. No Monthly Fee Ever...
                      </div>
                   </div>
                </div>

                <div class="col-12 mt-md50 mt20 text-center">
                  <div class="f-20 f-md-26 w500 text-center lh150 white-clr post-heading">
                  <span class="orange-clr w700">Done-For-You Leads, Built-In High-Speed SMTP</span> (Email Delivery Servers), <span class="orange-clr w700">Smart Tagging, Unlimited List Importing &amp; Mailing</span> - All Within an Easy Cloud Based App.
                  </div>
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md50">
               <div class="col-12 col-md-10 mx-auto">
                  <img src="assets/images/product-box.webp" class="img-fluid d-block mx-auto" alt="Product">
               </div>
            </div>
         </div>
      </div>
      <!-- Proudly Section End  -->

      <!-- Bonus Section Header Start -->
      <div class="bonus-header">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-10 mx-auto heading-bg text-center">
                  <div class="f-24 f-md-36 lh140 w700"> When You Purchase AutoMailX, You Also Get <br class="hidden-xs"> Instant Access To These 20 Exclusive Bonuses</div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus Section Header End -->

      <!-- Bonus #1 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-12 col-md-5 text-center">
                  <img src="assets/images/bonus1.webp" class="img-fluid mx-auto d-block" alt="Bonus1">
               </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                         <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 1</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize mt-md30 mt20">
                        WP Email Timer Plus
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                       <b> <li>WP Email Timer Plus is a plugin that allows you to create beautiful countdown timers even INSIDE your emails!</b></li>
                       <li> This will help to increase conversions, sales and also clickthrough rate inside your emails because the moment someone opens your email, they immediately see the timer ticking to zero and urging them to take action right away.</li>
                       <li> Other than email, you will have the option to add the countdown timer to your blogs/websites as a widget.</li>
                        </ul>
                     </div>
                  </div>
         </div>
      </div>
      <!-- Bonus #1 Section End -->

      <!-- Bonus #2 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row align-items-center">
                     <div class="col-md-7 col-12 mt20 mt-md0 order-2 order-md-2">
                         <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 2</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize mt-md30 mt20">
                        Traffic Beast
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">Whether you have a personal blog, business website, or are making money through online advertising, today's currency of success relies, almost exclusively, on the science of cultivating more significant traffic to your website. </span> </li>
                           <li>The traffic that you bring to your website is crucial because it helps you increase your rankings on the various search engines, which is how potential customers can find your company. </li>
                           <li>Unfortunately bringing more traffic to your site these days can be a challenge. With millions of competing websites, it can be difficult for potential customers to find your site.</li>
                        </ul>
                     </div>
                     <div class="col-12 col-md-5 text-center order-1 order-md-2">
               <img src="assets/images/bonus2.webp" class="img-fluid mx-auto d-block" alt="Bonus2">
               </div>
                  </div>
         </div>
      </div>
      <!-- Bonus #2 Section End -->

      <!-- Bonus #3 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-12 col-md-5 text-center">
                  <img src="assets/images/bonus3.webp" class="img-fluid mx-auto d-block" alt="Bonus1">
               </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                         <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 3</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize mt-md30 mt20">
                        Email Marketing Success 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">Email marketing for business isn’t a new concept, and it has been proven to be one of the best marketing tactics for return on investment. </span></li>
                           <li>With more than 205 billion emails being sent and received every day if your business isn't taking advantage of this powerful and massive marketing channel, then you are missing out on a highly effective way to reach your target audience. </li>
                           <li>Creating a successful email marketing campaign isn’t difficult, but it does require you to do more than just send out an occasional newsletter. </li>
                        </ul>
                     </div>
                  </div>
         </div>
      </div>
      <!-- Bonus #3 Section End -->

      <!-- Bonus #4 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row align-items-center">
                     <div class="col-md-7 col-12 mt20 mt-md0 order-2 order-md-2">
                         <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 4</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize mt-md30 mt20">
                        Email Monetizer
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">Turning your email list into a passive income money maker isn’t as difficult, or time consuming as you may think.</span> </li>
                           <li>Every day, thousands of online marketers are transforming their mailing lists into powerful cash funnels, and quite often, they don’t even have their own product line!</li>
                           <li>This special report will make it easy for you to start making money with your subscriber base even if you’re just starting out.</li>
                           <li>It will show you how you can join the ranks of successful list builders quickly and easily, while increasing engagement, building your tribe and positioning yourself as a thought leader in your market.</li>
                        </ul>
                     </div>
                     <div class="col-12 col-md-5 text-center order-1 order-md-2">
               <img src="assets/images/bonus4.webp" class="img-fluid mx-auto d-block" alt="Bonus2">
               </div>
                  </div>
         </div>
      </div>
      <!-- Bonus #4 End -->

      <!-- Bonus #5 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-12 col-md-5 text-center">
                  <img src="assets/images/bonus5.webp" class="img-fluid mx-auto d-block" alt="Bonus1">
               </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                         <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 5</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize mt-md30 mt20">
                        Modern Email Marketing and Segmentation 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">        
                           <li class="w600">Failure in e-marketing comes in many different forms because people try many different things.</li>
                           <li>Social medial marketing faces many challenges because of the evolving algorithms of platforms like Facebook. It’s getting worse and worse with each passing year.</li>
                           <li class="w600">This is a step-by-step guide to start earning REAL list marketing money with modern email marketing and segmentation techniques.</li>
                        </ul>
                     </div>
                  </div>
         </div>
      </div>
      <!-- Bonus #5 End -->

      <!-- CTA Button Section Start -->
      <div class="cta-section-new">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 blue-gradient">"ADMINMAILX"</span> for an Additional <span class="w700 blue-gradient">35% Discount</span> on Commercial Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                     <span class="text-center">Grab AutoMailX + My 20 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                        <span class="f-14 f-md-18 w500 ">Days</span>
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                        <span class="f-14 f-md-18 w500 ">Hours</span>
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                        <span class="f-14 f-md-18 w500 ">Mins</span>
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                        <span class="f-14 f-md-18 w500 ">Sec</span>
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->

      <!-- Bonus #6 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row align-items-top">
                     <div class="col-md-7 col-12 mt20 mt-md0 order-2 order-md-2">
                         <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 6</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize mt-md30 mt20">
                        Market Storm Magazines 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">This is a collection of Internet Marketing Magazines with 380+ pages of quality content!</span></li>
                           <li>You can start your own monthly or annual magazine program and make 100% passive income. </li>
                        </ul>
                     </div>
                     <div class="col-12 col-md-5 text-center order-1 order-md-2">
               <img src="assets/images/bonus6.webp" class="img-fluid mx-auto d-block" alt="Bonus2">
               </div>
                  </div>
         </div>
                     </div>
      <!-- Bonus #6 End -->

      <!-- Bonus #7 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-12 col-md-5 text-center">
                  <img src="assets/images/bonus7.webp" class="img-fluid mx-auto d-block" alt="Bonus1">
               </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                         <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 7</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize mt-md30 mt20">
                        Find Your Niche 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">Owning a business has many advantages from being able to set your own hours to have the control to sell what you want.  </span></li>
                           <li>While it isn't for lack of effort, those looking to start a new online business fail to complete the crucial first step; they fail to research to find a viable and profitable niche.</li>
                           <li>This comprehensive guide covers everything you need to know for finding your niche so you can stand out and create success faster.</li>
                        </ul>
                     </div>
                  </div>
         </div>
      </div>
      <!-- Bonus #7 End -->

      <!-- Bonus #8 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row align-items-top">
                     <div class="col-md-7 col-12 mt20 mt-md0 order-2 order-md-2">
                         <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 8</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize mt-md30 mt20">
                        WP Email Countdown Plugin 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">With this plugin you can create unlimited email countdown optin pages. It works in Wordpress and any WP theme.</span> </li>
                           <li>Collect leads with your countdown page using only the HTML for any auto-responder service. Paste auto-responder code and it will automatically connect to your page.</li>
                           <li>Countdown to any date with a live text countdown that will redirect to any URL after and on the date that you choose. </li>
                           <li>Use the wordpress meta options panel to have complete control over your email countdown page. Edit a variety of options, including your logo or banner image.</li>     
                        </ul>
                     </div>
                     <div class="col-12 col-md-5 text-center order-1 order-md-2">
               <img src="assets/images/bonus8.webp" class="img-fluid mx-auto d-block" alt="Bonus2">
               </div>
                  </div>
         </div>
                     </div>
      <!-- Bonus #8 End -->

      <!-- Bonus #9 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-12 col-md-5 text-center">
                  <img src="assets/images/bonus9.webp" class="img-fluid mx-auto d-block" alt="Bonus1">
               </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                         <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 9</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize mt-md30 mt20">
                        Email List Management Secrets 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">List maintenance is one of the most important subjects in online marketing. Your list is your number one and most basic bottom level output for your promotions.</span> </li>
                           <li>It’s expensive and time consuming to gather, but forms one of the most powerful resources and profit potential you have.</li>
                           <li>Depending on your business, there are several solutions that might be right for you. With this ebook you will learn the big five solutions to allow you to decide which one is going to make you the most cash.</li>
                        </ul>
                     </div>
                  </div>
         </div>
      </div>
      <!-- Bonus #9 End -->

      <!-- Bonus #10 start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row align-items-top">
                     <div class="col-md-7 col-12 mt20 mt-md0 order-2 order-md-2">
                         <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 10</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize mt-md30 mt20">
                        Boost Your Productivity 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">We need to be more proficient in our work to accomplish more. </span></li>
                           <li>In many cases, people fail to be productive because they lose focus and let their minds wander, leading to a loss in productivity. </li>
                           <li>This quick guide will reveal you basic ingredients of productivity and tehniques how to better manage your time.</li>
                           <li>This product contains all the features for building your list: List Building Report, 'Mobile Responsive' Minisite, Confirmation + Thank You Page etc..</li>
                        </ul>
                     </div>
                     <div class="col-12 col-md-5 text-center order-1 order-md-2">
               <img src="assets/images/bonus10.webp" class="img-fluid mx-auto d-block" alt="Bonus2">
               </div>
                  </div>
         </div>
                     </div>
      <!-- Bonus #10 End -->

      <!-- CTA Button Section Start -->
      <div class="cta-section-new">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 blue-gradient">"ADMINMAILX"</span> for an Additional <span class="w700 blue-gradient">35% Discount</span> on Commercial Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                     <span class="text-center">Grab AutoMailX + My 20 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr"><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 smmltd">Days</span> </div><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">0300</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div><div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div></div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->

      <!-- Bonus #11 start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-12 col-md-5 text-center">
                  <img src="assets/images/bonus11.webp" class="img-fluid mx-auto d-block" alt="Bonus1">
               </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                         <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 11</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize mt-md30 mt20">
                        OptiRoi
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>OptiROI will help you maximize profits regardless of what niche you're in!</li>
                           <li>You can also use this technology to build bigger email lists, which equates to much more future revenue! If you want to outsmart and dominate your competition in today's crowded and highly-competitive landscape, then you need to be proactive with your marketing. </li>
                        </ul>
                     </div>
                  </div>
         </div>
      </div>
      <!-- Bonus #11 End -->

      <!-- Bonus #12 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row align-items-top">
                     <div class="col-md-7 col-12 mt20 mt-md0 order-2 order-md-2">
                         <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 12</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize mt-md30 mt20">
                        How to Keep Your Email Subscribers 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">Learn How to Keep Your Email Subscribers! </span></li>
                           <li>Indeed, the money is in the list. That's why you decided to build your own email list but as you go along, building a list is not just your task that you have to take care of.</li>
                           <li>There is the concern of how to make your list conversion increase and most of all how to keep your list intact or at least you have low number of attrition. </li>
                           <li>Well, if you will look to other business model, attrition is normal but if you will handle your list quite well, you can decrease it numbers and make more money from it.</li>
                        </ul>
                     </div>
                     <div class="col-12 col-md-5 text-center order-1 order-md-2">
               <img src="assets/images/bonus12.webp" class="img-fluid mx-auto d-block" alt="Bonus2">
               </div>
                  </div>
         </div>
                     </div>
      <!-- Bonus #12 End -->

      <!-- Bonus #13 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-12 col-md-5 text-center">
                  <img src="assets/images/bonus13.webp" class="img-fluid mx-auto d-block" alt="Bonus1">
               </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                         <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 13</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize mt-md30 mt20">
                        Double Your Email Conversions 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li class="w600">Learn How to Double Your Email Conversions!</li>
                        <li>The money is in the list. You may already have heard this from many successful online entrepreneurs.</li>
                        <li>If you have been building your email list, your next challenge is how you will be able to make your email conversions higher.</li>
                        <li>Fortunately, inside this product is a podcast that you will give you the proven system that will guide you how to increase your email conversion rate and eventually make sales.</li>
                        </ul>
                     </div>
                  </div>
         </div>
      </div>
      <!-- Bonus #13 End -->

      <!-- Bonus #14 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row align-items-center">
                     <div class="col-md-7 col-12 mt20 mt-md0 order-2 order-md-2">
                         <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 14</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize mt-md30 mt20">
                        Email Marketing Basics Video Course 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">Discover How to Set Up Your Email Autoresponder withGetResponse So That You Can Grow an Email List That Gets Clicks and Converts into Sales…Starting Today!</span> </li>
                           <li>This video course will take you behind the scenes to help you understand how to build a relationship with your list… </li>
                           <li>It will show you how to plan out your email series, but also how to take the series and set them up on GetResponse.com. </li>
                        </ul>
                     </div>
                     <div class="col-12 col-md-5 text-center order-1 order-md-2">
               <img src="assets/images/bonus14.webp" class="img-fluid mx-auto d-block" alt="Bonus2">
               </div>
                  </div>
         </div>
                     </div>
      <!-- Bonus #14 End -->

      <!-- Bonus #15 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-12 col-md-5 text-center">
                  <img src="assets/images/bonus15.webp" class="img-fluid mx-auto d-block" alt="Bonus1">
               </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                         <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 15</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize mt-md30 mt20">
                        Xyber Email Assistant Software 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li class="w600">Do Your Customer Support with Ease Using Xyber Email Assistant! </li>
                           <li>If you are a current online business, customer support is necessary. This is because you can't be so sure that your business will work perfectly!</li>
                           <li>The good news though is that, you can now turbo-charge the growth of your business, while freeing Up Your Time With Reduced Customer Support Hassles! Stay On Top Of Your Business With The Ability To Instantly Respond To Email For More Satisfied Customers, Affiliates, and Partners!</li>
                        </ul>
                     </div>
                  </div>
         </div>
      </div>
      <!-- Bonus #15 End -->

      <!-- CTA Button Section Start -->
      <div class="cta-section-new">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 blue-gradient">"ADMINMAILX"</span> for an Additional <span class="w700 blue-gradient">35% Discount</span> on Commercial Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                     <span class="text-center">Grab AutoMailX + My 20 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <span class="f-14 f-md-18 smmltd">Days</span>
                      </div>
                      <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        
                        <span class="f-14 f-md-18 w500 smmltd">Hours</span>
                      </div>
                      <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        
                        <span class="f-14 f-md-18 w500 smmltd">Mins</span>
                      </div>
                      <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        
                        <span class="f-14 f-md-18 w500 smmltd">Sec</span> 
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->

      <!-- Bonus #16 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row align-items-top">
                     <div class="col-md-7 col-12 mt20 mt-md0 order-2 order-md-2">
                         <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 16</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize mt-md30 mt20">
                        Make Your Subject Lines Standout 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">Learn the Techniques to Make Your Email Subject Line Stand Out Multi Media!</span></li>
                           <li>The money is in the list. And if you are building your email list today, the next question is that, are your email series get opens?</li>
                           <li>Your subject line will certainly stand out, and your email will be opened if you make your email unique, useful to the reader, and focused on what the reader either needs to know or wants to know.</li>
                           <li>Maximize your email marketing efforts by simply having the highest results that you haven't experience before.</li>
                        </ul>
                     </div>
                     <div class="col-12 col-md-5 text-center order-1 order-md-2">
               <img src="assets/images/bonus16.webp" class="img-fluid mx-auto d-block" alt="Bonus2">
               </div>
                  </div>
         </div>
                     </div>
      <!-- Bonus #16 End -->

      <!-- Bonus #17 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-12 col-md-5 text-center">
                  <img src="assets/images/bonus17.webp" class="img-fluid mx-auto d-block" alt="Bonus1">
               </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                         <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 17</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize mt-md30 mt20">
                        Viral List Autopilot 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>Finally, Discover How to Build a Highly Profitable List By Using This Untapped Viral Strategy!  </li>
                           <li>Starting Today! This video course will take you behind the scenes to help you understand how to build a higher converting list by leveraging other people’s lists!</li>
                           <li>Indeed, the money is in the list and if you are not implementing into your blog or in your business, you are missing a lot of potential customers.</li>
                        </ul>
                     </div>
                  </div>
         </div>
      </div>
      <!-- Bonus #17 End -->

      <!-- Bonus #18 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row align-items-top">
                     <div class="col-md-7 col-12 mt20 mt-md0 order-2 order-md-2">
                         <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 18</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize mt-md30 mt20">
                        Rapid Lead Magnets 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li><span class="w600">How to create quick & easy 'Lead Magnet' funnels you can use to build targeted lists and attract buyers!</span></li>
                        <li>Lead Magnets are basically things that you'll give away for free in exchange for an email address so that you can follow up with a visitor or subscriber and ultimately get them to build a relationship with you and build rapport. In that way, you will be able to sell them your front and offers. </li>
                        <li>As long as everything is congruent and related to each other, you should have very high conversion. Think quality oer the amounts of the lead magnets that you have.</li>
                        </ul>
                     </div>
                     <div class="col-12 col-md-5 text-center order-1 order-md-2">
               <img src="assets/images/bonus18.webp" class="img-fluid mx-auto d-block" alt="Bonus2">
               </div>
                  </div>
         </div>
                     </div>
      <!-- Bonus #18 End -->

      <!-- Bonus #19 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-12 col-md-5 text-center">
                  <img src="assets/images/bonus19.webp" class="img-fluid mx-auto d-block" alt="Bonus1">
               </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                         <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 19</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize mt-md30 mt20">
                        CPA Email Marketing 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>
                              <span class="w600">Learn From This Audio, Give It Away To Build Your Email List & Sell The Whole Product With MRR</span> 
                           </li>
                           <li>One of the ways you can organize and automate your CPA network offers is through automatic email campaigns. If you already have a list of subscribers to some websites or blogs you own, you already have a means to do an email campaign with CPA offers. This approach also lends itself well to doing a “hands off” system approach that can work behind the scenes to generate cash, even when you are asleep.</li>
                        </ul>
                     </div>
                  </div>
         </div>
      </div>
      <!-- Bonus #19 End -->

      <!-- Bonus #20 start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row align-items-top">
                     <div class="col-md-7 col-12 mt20 mt-md0 order-2 order-md-2">
                         <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 20</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize mt-md30 mt20">
                        Email Marketing Expert 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li class="w600">Doing business is more than a full-time job.</li>
                           <li>Your days are spent selling and procuring products, ensuring customer satisfaction and when you are home, you have to work on new products, ideas to improve your service, track finances and do the research to grow your business.</li>
                           <li>This leaves little or no time to learn new things. </li>
                           <li>This course has everything you need to know to boost your online reputation and GET HUNDREDS OF PEOPLE SINGING UP TO YOUR LISTS EVERY WEEK.</li>
                        </ul>
                     </div>
                     <div class="col-12 col-md-5 text-center order-1 order-md-2">
               <img src="assets/images/bonus20.webp" class="img-fluid mx-auto d-block" alt="Bonus2">
               </div>
                  </div>
         </div>
                     </div>
      <!-- Bonus #20 End -->

      <!-- Huge Woth Section Start -->
      <div class="huge-area mt30 mt-md10">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="f-md-65 f-40 lh120 w700 white-clr">That's Huge Worth of</div>
                  <br>
                  <div class="f-md-60 f-40 lh120 w800 blue-gradient">$3300!</div>
               </div>
            </div>
         </div>
      </div>
      <!-- Huge Worth Section End -->

      <!-- text Area Start -->
      <div class="white-section pb0">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="f-md-36 f-25 lh140 w500">So what are you waiting for? You have a great opportunity <br class="hidden-xs"> ahead + <span class="w700 ">My 20 Bonus Products</span> are making it a <br class="hidden-xs"> <span class="w700 ">completely NO Brainer!!</span></div>
               </div>
            </div>
         </div>
      </div>
      <!-- text Area End -->

      <!-- CTA Button Section Start -->
      <div class="cta-btn-section">
         <div class="container">
            <div class="row">
            <div class="col-12 f-16 f-md-20 lh150 w500 text-center black-clr">
                  Use Coupon Code <span class="w700 blue-clr">"ADMINMAILX"</span> for an Additional <span class="w700 blue-clr">35% Discount</span> on Commercial Licence
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab AutoMailX + My 20 Exclusive Bonuses</span> 
                  </a>
               </div>
               <div class="col-12 mt15 mt-md20" align="center">
                  <h3 class="f-md-28 f-20 w500 text-center black">My Exclusive Bonuses Are Expiring in...</h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 col-md-10 mx-auto col-12 text-center">
                  <div class="countdown counter-black">
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                     <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                     <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->
      <div class="footer-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                 <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 1164.92 192.13" style="max-height: 55px;"><defs><style>.cls-1wh,.cls-2wh,.cls-3wh,.cls-4wh,.cls-5wh{fill-rule:evenodd;}.cls-1wh{fill:url(#linear-gradient);}.cls-2wh{fill:url(#linear-gradient-2);}.cls-3wh{fill:url(#linear-gradient-3);}.cls-4wh{fill:url(#linear-gradient-4);}.cls-5wh{fill:url(#linear-gradient-5);}.cls-6wh{fill:#fff;}.cls-7wh{fill:url(#linear-gradient-6);}</style><linearGradient id="linear-gradient" x1="82.05" y1="127.34" x2="150.42" y2="127.34" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#624ef2"></stop><stop offset="1" stop-color="#1e9af7"></stop></linearGradient><linearGradient id="linear-gradient-2" x1="7.14" y1="79.25" x2="123.43" y2="79.25" xlink:href="#linear-gradient"></linearGradient><linearGradient id="linear-gradient-3" x1="0" y1="116.36" x2="45.71" y2="116.36" xlink:href="#linear-gradient"></linearGradient><linearGradient id="linear-gradient-4" x1="3.94" y1="157.47" x2="142.12" y2="157.47" xlink:href="#linear-gradient"></linearGradient><linearGradient id="linear-gradient-5" x1="3.58" y1="75.02" x2="182.4" y2="75.02" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#ff2d2d"></stop><stop offset="1" stop-color="#ffbc00"></stop></linearGradient><linearGradient id="linear-gradient-6" x1="971.74" y1="90.91" x2="1164.92" y2="90.91" xlink:href="#linear-gradient"></linearGradient></defs><title>AutomailX White</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1wh" d="M150.42,67.57V177.2a15.41,15.41,0,0,1-3.38,9.92q-20.67-19.74-39-37.28l-26-24.94Z"></path><path class="cls-2wh" d="M123.43,48.09l-62.5,62.32Q47.16,98.7,33.43,86.92q-8.17-7-16.31-14.11c-3.34-2.91-6.75-5.85-10-8.88a6.08,6.08,0,0,1,1.32-.33l24.25-3.27,28.14-3.8,33-4.45Z"></path><path class="cls-3wh" d="M45.71,119.33,25.4,139.11Q15.2,149,5.81,158.19c-2.33,3-5.83,2.65-5.81-2.51V76.9c0-5.16,3.5-5.32,5.81-2.2l.39.41h0c.73,1.66,1.4,3.32,2,4.81l.15.38Z"></path><path class="cls-4wh" d="M142.12,190.87a14.7,14.7,0,0,1-8.88,1l-39.42-7.34-33-6.14-28.14-5.24L8.44,168.61a8.2,8.2,0,0,1-4.5-2.68L25.82,145q11.1-10.61,23.23-22.21l15.81,16.51L78,128.3q12.35,12.06,25.78,25.16Q121.84,171.09,142.12,190.87Z"></path><path class="cls-5wh" d="M10.15,79.16,65,136.41l92.87-81.5,3.93,12.24L182.4,13.62,128.57,32.19l11.14,5.9L61.05,116.53s-54.7-47.67-57.47-50c1.16,1.75,6.57,12.68,6.57,12.68Z"></path><path class="cls-6wh" d="M283,50.49H247.2L201.86,181.82h29.75l9.74-29.95h47.42l9.73,29.95h29.75ZM248.4,130.2l16.17-49.7h1l16.14,49.7Z"></path><path class="cls-6wh" d="M406.68,139.88V83.32H434v98.5H407.77V163.93h-1a29.1,29.1,0,0,1-11.06,13.91q-7.73,5.26-18.82,5.26a33.23,33.23,0,0,1-17.38-4.49,30.61,30.61,0,0,1-11.7-12.76q-4.2-8.26-4.27-19.81V83.32h27.32v57.84q.06,8.73,4.68,13.79T387.89,160a19.37,19.37,0,0,0,9.23-2.27,17.53,17.53,0,0,0,7-6.77A21,21,0,0,0,406.68,139.88Z"></path><path class="cls-6wh" d="M510,180.6q-2.06.65-5.78,1.51a48.09,48.09,0,0,1-9,1.06,41.39,41.39,0,0,1-17.28-2.63,24.12,24.12,0,0,1-11.51-9.36q-4.11-6.36-4-16v-51.3H448.87V83.32h13.47V59.72h27.32v23.6h18.53v20.52H489.66v47.71a12.24,12.24,0,0,0,1.15,5.87,6.34,6.34,0,0,0,3.24,2.92,13.08,13.08,0,0,0,4.84.83,21.79,21.79,0,0,0,3.85-.35l2.95-.55Z"></path><path class="cls-6wh" d="M569.43,183.74q-14.94,0-25.81-6.38a43,43,0,0,1-16.77-17.83Q521,148.09,521,133t5.9-26.71a43,43,0,0,1,16.77-17.82Q554.5,82,569.43,82t25.81,6.38A43,43,0,0,1,612,106.24q5.91,11.46,5.9,26.71T612,159.53a43,43,0,0,1-16.77,17.83Q584.38,183.75,569.43,183.74Zm.13-21.16a16.88,16.88,0,0,0,11.35-3.88,23.82,23.82,0,0,0,6.89-10.61,46.31,46.31,0,0,0,2.35-15.33,46.31,46.31,0,0,0-2.35-15.33,24.09,24.09,0,0,0-6.89-10.64,16.8,16.8,0,0,0-11.35-3.91,17.26,17.26,0,0,0-11.51,3.91,23.74,23.74,0,0,0-7,10.64,46.58,46.58,0,0,0-2.34,15.33,46.58,46.58,0,0,0,2.34,15.33,23.48,23.48,0,0,0,7,10.61A17.35,17.35,0,0,0,569.56,162.58Z"></path><path class="cls-6wh" d="M640.74,50.49h19l44.63,109h1.54l44.64-109h19V181.82H754.63V82h-1.28l-41,99.78H697.94L656.9,82h-1.28v99.78H640.74Z"></path><path class="cls-6wh" d="M828.25,184.13a39.77,39.77,0,0,1-17-3.56,28.82,28.82,0,0,1-12.12-10.33q-4.49-6.77-4.49-16.38,0-8.46,3.34-13.76a23.63,23.63,0,0,1,8.91-8.3,47.46,47.46,0,0,1,12.35-4.52q6.76-1.52,13.62-2.41,9-1.16,14.59-1.76t8.21-2.08q2.6-1.48,2.6-5.13v-.52q0-9.48-5.16-14.75t-15.62-5.25q-10.85,0-17,4.74a28,28,0,0,0-8.66,10.13l-14.37-5.13a34.69,34.69,0,0,1,10.3-14,38.56,38.56,0,0,1,14.11-7A58.86,58.86,0,0,1,837,82a61.89,61.89,0,0,1,10.93,1.12,35.92,35.92,0,0,1,12,4.58,26.84,26.84,0,0,1,9.65,10.46q3.86,7,3.85,18.72v64.9H858.26V168.48h-.77a26.84,26.84,0,0,1-5.13,6.86,29.6,29.6,0,0,1-9.56,6.22A36.78,36.78,0,0,1,828.25,184.13Zm2.3-13.6q9,0,15.17-3.53a24.19,24.19,0,0,0,9.36-9.1,23.36,23.36,0,0,0,3.18-11.74V132.31q-1,1.16-4.2,2.09a64,64,0,0,1-7.44,1.6q-4.2.67-8.15,1.15c-2.63.32-4.75.59-6.38.81a58.57,58.57,0,0,0-11,2.46,19.26,19.26,0,0,0-8.21,5.07q-3.12,3.36-3.11,9.14,0,7.89,5.87,11.89T830.55,170.53Z"></path><path class="cls-6wh" d="M911.79,66.9a10.65,10.65,0,0,1-7.6-3,9.86,9.86,0,0,1,0-14.49,11.07,11.07,0,0,1,15.2,0,9.86,9.86,0,0,1,0,14.49A10.65,10.65,0,0,1,911.79,66.9Zm-7.7,114.92V83.32h15.14v98.5Z"></path><path class="cls-6wh" d="M962.06,50.49V181.82H946.93V50.49Z"></path><path class="cls-7wh" d="M1045.34,0l26.37,62h1.42l47.14-62h44.65l-70.4,90.91L1136,181.82h-43.32l-27-62.06h-1.42l-47.59,62.06H971.74l72.18-90.91L1003,0Z"></path></g></g></svg>
                 <div class="f-14 f-md-16 w400 mt20 lh150 white-clr text-center"><span class="w600">Note:</span> This site is not a part of the Facebook website or Facebook Inc. Additionally, this site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc.<br>
                  </div>
                  <div class="f-16 f-md-18 w600 mt20 mt-md20 white-clr text-center"> <script type="text/javascript"
                    src="https://warriorplus.com/o2/disclaimer/qy9f4y" defer></script><div class="wplus_spdisclaimer"></div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-md-16 f-16 w400 lh140 white-clr text-xs-center">Copyright © AutoMailX 2023</div>
                  <ul class="footer-ul w400 f-md-16 f-16 white-clr text-center text-md-right">
                     <li><a href="https://support.bizomart.com/hc/en-us " class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://automailxai.com/legal/privacy-policy.html " class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://automailxai.com/legal/terms-of-service.html " class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://automailxai.com/legal/disclaimer.html " class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://automailxai.com/legal/gdpr.html " class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://automailxai.com/legal/dmca.html " class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://automailxai.com/legal/anti-spam.html " class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!-- timer --->
      <?php
         if ($now < $exp_date) {
         ?>
      <script type="text/javascript">
         // Count down milliseconds = server_end - server_now = client_end - client_now
         var server_end = <?php echo $exp_date; ?> * 1000;
         var server_now = <?php echo time(); ?> * 1000;
         var client_now = new Date().getTime();
         var end = server_end - server_now + client_now; // this is the real end time
         
         var noob = $('.countdown').length;
         
         var _second = 1000;
         var _minute = _second * 60;
         var _hour = _minute * 60;
         var _day = _hour * 24
         var timer;
         
         function showRemaining() {
         var now = new Date();
         var distance = end - now;
         if (distance < 0) {
         	clearInterval(timer);
         	document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
         	return;
         }
         
         var days = Math.floor(distance / _day);
         var hours = Math.floor((distance % _day) / _hour);
         var minutes = Math.floor((distance % _hour) / _minute);
         var seconds = Math.floor((distance % _minute) / _second);
         if (days < 10) {
         	days = "0" + days;
         }
         if (hours < 10) {
         	hours = "0" + hours;
         }
         if (minutes < 10) {
         	minutes = "0" + minutes;
         }
         if (seconds < 10) {
         	seconds = "0" + seconds;
         }
         var i;
         var countdown = document.getElementsByClassName('countdown');
         for (i = 0; i < noob; i++) {
         	countdown[i].innerHTML = '';
         
         	if (days) {
         		countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + days + '</span><span class="f-14 f-md-18 smmltd">Days</span> </div>';
         	}
         
         	countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + hours + '</span><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>';
         
         	countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">' + minutes + '</span><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>';
         
         	countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">' + seconds + '</span><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>';
         }
         
         }
         timer = setInterval(showRemaining, 1000);
         	
      </script>
      <?php
         } else {
         echo "Times Up";
         }
         ?>
      <!--- timer end-->
   </body>
</html>
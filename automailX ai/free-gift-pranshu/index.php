<!Doctype html>
<html lang="en">
   <head>
   <head>
    <title>AutomailX | Free Gift</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=9">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta name="title" content="AutomailX | Free Gift">
    <meta name="description" content="World's First A.I App Builds Us Done-For-You Stunning Websites in 1000s of Niches Prefilled With Smoking Hot Content…">
    <meta name="keywords" content="AutomailX | Free Gift">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta property="og:image" content="https://automailxai.com/free-gift/thumbnail.png">
    <meta name="language" content="English">
    <meta name="revisit-after" content="1 days">
    <meta name="author" content="Pranshu Gupta">
    
    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:title" content="AutomailX | Free Gift">
    <meta property="og:description" content="World's First A.I App Builds Us Done-For-You Stunning Websites in 1000s of Niches Prefilled With Smoking Hot Content…">
    <meta property="og:image" content="https://automailxai.com/free-gift/thumbnail.png">

    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:title" content="AutomailX | Free Gift">
    <meta property="twitter:description" content="World's First A.I App Builds Us Done-For-You Stunning Websites in 1000s of Niches Prefilled With Smoking Hot Content…">
    <meta property="twitter:image" content="https://automailxai.com/free-gift/thumbnail.png">

   <!-- Font-Family -->
   <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">

    
    <!-- Start Editor required -->
    <link rel="stylesheet" href="../common_assets/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="assets/css/style.css" type="text/css">
    <link rel="stylesheet" href="assets/css/aweberform.css" type="text/css">
    <script src="https://cdn.oppyotest.com/launches/sellero/common_assets/js/jquery.min.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- End -->
</head>
<body>
	
	<?php
		if(!isset($_GET['afflink'])){
		$_GET['afflink'] = 'https://automailxai.com/special/';
		$_GET['name'] = 'Pranshu Gupta';      
		}
	?>

	<div class="header-sec" id="product">
        <div class="container">
            <div class="row">
				<div class="col-12 text-center">
                    <div class="white-clr f-20 f-md-32">
                        Free Gifts by <span class="w600 gradient-clr">Pranshu Gupta's</span> for SuperVIP Customers
                    </div>
                    <div class="uy mt20 mt-md40">
                        <div class="f-20 f-md-24 lh160 w500 black-clr">
                            <span class="f-20 f-md-24 gradient-clr w600">Congratulations!</span> you have also <span class="gradient-clr w600">unlocked Special Exclusive coupon <br class="d-none d-md-block">  "ADMINMAILX" </span> for my Lightning Fast Software <span class="gradient-clr w600">"AutomailX"</span> launch on <br class="d-none d-md-block"> <span class="gradient-clr w600">5th November 2023  @ 11:00 AM EDT</span>
                        </div>
                        <div class="f-20 f-md-24 lh130 w600 mt20 gn-box">Good News :</div>
                        <div class="f-20 f-md-24 lh160 w400 mt20 mt-md30 black-clr">
                            If you want any other <b>BONUSES</b> that are relevant for your business <br class="d-none d-md-block"> or <b>any niche</b> to increase more of your profits then, <b>please feel free <br class="d-none d-md-block"> to email</b> us at <a href="https://support.bizomart.com" target="_blank" class="gradient-clr">https://support.bizomart.com</a>
                        </div>
                    </div>
                </div>
               	<div class="col-12 mt-md50 mt20 text-center">
				   <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 1164.92 192.13" style="max-height:55px"><defs><style>.cls-x1,.cls-x2,.cls-x3,.cls-x4,.cls-x5{fill-rule:evenodd;}.cls-x1{fill:url(#linear-gradient);}.cls-x2{fill:url(#linear-gradient-2);}.cls-x3{fill:url(#linear-gradient-3);}.cls-x4{fill:url(#linear-gradient-4);}.cls-x5{fill:url(#linear-gradient-5);}.cls-x6{fill:#fff;}.cls-x7{fill:url(#linear-gradient-6);}</style><linearGradient id="linear-gradient" x1="82.05" y1="127.34" x2="150.42" y2="127.34" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#624ef2"/><stop offset="1" stop-color="#1e9af7"/></linearGradient><linearGradient id="linear-gradient-2" x1="7.14" y1="79.25" x2="123.43" y2="79.25" xlink:href="#linear-gradient"/><linearGradient id="linear-gradient-3" x1="0" y1="116.36" x2="45.71" y2="116.36" xlink:href="#linear-gradient"/><linearGradient id="linear-gradient-4" x1="3.94" y1="157.47" x2="142.12" y2="157.47" xlink:href="#linear-gradient"/><linearGradient id="linear-gradient-5" x1="3.58" y1="75.02" x2="182.4" y2="75.02" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#ff2d2d"/><stop offset="1" stop-color="#ffbc00"/></linearGradient><linearGradient id="linear-gradient-6" x1="971.74" y1="90.91" x2="1164.92" y2="90.91" xlink:href="#linear-gradient"/></defs><title>AutomailX White</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-x1" d="M150.42,67.57V177.2a15.41,15.41,0,0,1-3.38,9.92q-20.67-19.74-39-37.28l-26-24.94Z"/><path class="cls-x2" d="M123.43,48.09l-62.5,62.32Q47.16,98.7,33.43,86.92q-8.17-7-16.31-14.11c-3.34-2.91-6.75-5.85-10-8.88a6.08,6.08,0,0,1,1.32-.33l24.25-3.27,28.14-3.8,33-4.45Z"/><path class="cls-x3" d="M45.71,119.33,25.4,139.11Q15.2,149,5.81,158.19c-2.33,3-5.83,2.65-5.81-2.51V76.9c0-5.16,3.5-5.32,5.81-2.2l.39.41h0c.73,1.66,1.4,3.32,2,4.81l.15.38Z"/><path class="cls-x4" d="M142.12,190.87a14.7,14.7,0,0,1-8.88,1l-39.42-7.34-33-6.14-28.14-5.24L8.44,168.61a8.2,8.2,0,0,1-4.5-2.68L25.82,145q11.1-10.61,23.23-22.21l15.81,16.51L78,128.3q12.35,12.06,25.78,25.16Q121.84,171.09,142.12,190.87Z"/><path class="cls-x5" d="M10.15,79.16,65,136.41l92.87-81.5,3.93,12.24L182.4,13.62,128.57,32.19l11.14,5.9L61.05,116.53s-54.7-47.67-57.47-50c1.16,1.75,6.57,12.68,6.57,12.68Z"/><path class="cls-x6" d="M283,50.49H247.2L201.86,181.82h29.75l9.74-29.95h47.42l9.73,29.95h29.75ZM248.4,130.2l16.17-49.7h1l16.14,49.7Z"/><path class="cls-x6" d="M406.68,139.88V83.32H434v98.5H407.77V163.93h-1a29.1,29.1,0,0,1-11.06,13.91q-7.73,5.26-18.82,5.26a33.23,33.23,0,0,1-17.38-4.49,30.61,30.61,0,0,1-11.7-12.76q-4.2-8.26-4.27-19.81V83.32h27.32v57.84q.06,8.73,4.68,13.79T387.89,160a19.37,19.37,0,0,0,9.23-2.27,17.53,17.53,0,0,0,7-6.77A21,21,0,0,0,406.68,139.88Z"/><path class="cls-x6" d="M510,180.6q-2.06.65-5.78,1.51a48.09,48.09,0,0,1-9,1.06,41.39,41.39,0,0,1-17.28-2.63,24.12,24.12,0,0,1-11.51-9.36q-4.11-6.36-4-16v-51.3H448.87V83.32h13.47V59.72h27.32v23.6h18.53v20.52H489.66v47.71a12.24,12.24,0,0,0,1.15,5.87,6.34,6.34,0,0,0,3.24,2.92,13.08,13.08,0,0,0,4.84.83,21.79,21.79,0,0,0,3.85-.35l2.95-.55Z"/><path class="cls-x6" d="M569.43,183.74q-14.94,0-25.81-6.38a43,43,0,0,1-16.77-17.83Q521,148.09,521,133t5.9-26.71a43,43,0,0,1,16.77-17.82Q554.5,82,569.43,82t25.81,6.38A43,43,0,0,1,612,106.24q5.91,11.46,5.9,26.71T612,159.53a43,43,0,0,1-16.77,17.83Q584.38,183.75,569.43,183.74Zm.13-21.16a16.88,16.88,0,0,0,11.35-3.88,23.82,23.82,0,0,0,6.89-10.61,46.31,46.31,0,0,0,2.35-15.33,46.31,46.31,0,0,0-2.35-15.33,24.09,24.09,0,0,0-6.89-10.64,16.8,16.8,0,0,0-11.35-3.91,17.26,17.26,0,0,0-11.51,3.91,23.74,23.74,0,0,0-7,10.64,46.58,46.58,0,0,0-2.34,15.33,46.58,46.58,0,0,0,2.34,15.33,23.48,23.48,0,0,0,7,10.61A17.35,17.35,0,0,0,569.56,162.58Z"/><path class="cls-x6" d="M640.74,50.49h19l44.63,109h1.54l44.64-109h19V181.82H754.63V82h-1.28l-41,99.78H697.94L656.9,82h-1.28v99.78H640.74Z"/><path class="cls-x6" d="M828.25,184.13a39.77,39.77,0,0,1-17-3.56,28.82,28.82,0,0,1-12.12-10.33q-4.49-6.77-4.49-16.38,0-8.46,3.34-13.76a23.63,23.63,0,0,1,8.91-8.3,47.46,47.46,0,0,1,12.35-4.52q6.76-1.52,13.62-2.41,9-1.16,14.59-1.76t8.21-2.08q2.6-1.48,2.6-5.13v-.52q0-9.48-5.16-14.75t-15.62-5.25q-10.85,0-17,4.74a28,28,0,0,0-8.66,10.13l-14.37-5.13a34.69,34.69,0,0,1,10.3-14,38.56,38.56,0,0,1,14.11-7A58.86,58.86,0,0,1,837,82a61.89,61.89,0,0,1,10.93,1.12,35.92,35.92,0,0,1,12,4.58,26.84,26.84,0,0,1,9.65,10.46q3.86,7,3.85,18.72v64.9H858.26V168.48h-.77a26.84,26.84,0,0,1-5.13,6.86,29.6,29.6,0,0,1-9.56,6.22A36.78,36.78,0,0,1,828.25,184.13Zm2.3-13.6q9,0,15.17-3.53a24.19,24.19,0,0,0,9.36-9.1,23.36,23.36,0,0,0,3.18-11.74V132.31q-1,1.16-4.2,2.09a64,64,0,0,1-7.44,1.6q-4.2.67-8.15,1.15c-2.63.32-4.75.59-6.38.81a58.57,58.57,0,0,0-11,2.46,19.26,19.26,0,0,0-8.21,5.07q-3.12,3.36-3.11,9.14,0,7.89,5.87,11.89T830.55,170.53Z"/><path class="cls-x6" d="M911.79,66.9a10.65,10.65,0,0,1-7.6-3,9.86,9.86,0,0,1,0-14.49,11.07,11.07,0,0,1,15.2,0,9.86,9.86,0,0,1,0,14.49A10.65,10.65,0,0,1,911.79,66.9Zm-7.7,114.92V83.32h15.14v98.5Z"/><path class="cls-x6" d="M962.06,50.49V181.82H946.93V50.49Z"/><path class="cls-x7" d="M1045.34,0l26.37,62h1.42l47.14-62h44.65l-70.4,90.91L1136,181.82h-43.32l-27-62.06h-1.42l-47.59,62.06H971.74l72.18-90.91L1003,0Z"/></g></g></svg>
				</div>

				<div class="col-12 text-center mt20 mt-md60">
                  <div class="f-md-22 f-18 w600 text-center white-clr pre-heading lh150">
                  Zero Manual Work. Zero List Building Hassles. Zero Downtime. 
                  </div>
               </div>
               <div class="col-12 mt-md40 mt20 ">
                  <div class="main-heading text-center">
                     <div class="f-md-38 f-28 w700 text-center black-clr lh140">   
                        <span class="blue-gradient">Revealing “MailChimp” Killer AI-Powered App That</span> Writes &amp; Delivers Unlimited Stunning Emails, And Drives Tons of Clicks, Sales, and Commissions
                        <span>
                      </span></div>
                      <div class="f-18 f-md-22 lh140 w600 text-center white-clr heading-blue-shape mt10">
                      Let AI Do It All. No Tech Hassles. No Monthly Fee Ever...
                      </div>
                   </div>
                </div>

				<div class="col-12 mt-md50 mt20 text-center">
                  <div class="f-20 f-md-26 w500 text-center lh150 white-clr post-heading">
                  <span class="orange-clr w700">Done-For-You Leads, Built-In High-Speed SMTP</span> (Email Delivery Servers), <span class="orange-clr w700">Smart Tagging, Unlimited List Importing &amp; Mailing</span> - All Within an Easy Cloud Based App.
                  </div>
               </div>
				<div class="col-12 white-clr text-center">
					<div class="f-md-36 f-28 lh160 w700 mt30 white-clr">
						Download Your Free Gifts Below
					</div>
					<div class="mt10 f-20 f-md-32 lh160 ">
						<span class="tde">Enjoy Your Extra Bonus Below</span>
					</div>
				</div>
				<div class="col-12 col-md-10 mt20 mt-md50 mx-auto">
					<img src="assets/images/product-box.webp" class="img-fluid d-block mx-auto img-animation" alt="Proudly Presenting...">
				</div>
			</div>
		</div>
	</div>

	<div class="exclusive-bonus">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                  <div class="white-clr f-22 f-md-36 lh140 w600">
				  🎁 Special Premium Gift 🎁
                  </div>
               </div>
            </div>
         </div>
      </div>
<!--1. Header Section Start --> 	
	<div class="section-Profitable">
        <div class="container">
            <div class="row">
               	<div class="col-md-12 text-center">
               		<svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 645.02 151.26" style="max-height:70px">
                                    <defs>
                                      <style>
                                        .cls-n1 {
                                          fill: url(#linear-gradient-2);
                                        }
                                  
                                        .cls-n2 {
                                          fill: #fff;
                                        }
                                  
                                        .cls-n3 {
                                          fill-rule: evenodd;
                                        }
                                  
                                        .cls-n3, .cls-n4 {
                                          fill: #ff813b;
                                        }
                                  
                                        .cls-n5 {
                                          fill: url(#linear-gradient-3);
                                        }
                                  
                                        .cls-n6 {
                                          fill: url(#linear-gradient);
                                        }
                                  
                                        .cls-n7 {
                                          fill: url(#linear-gradient-4);
                                        }
                                      </style>
                                      <linearGradient id="linear-gradient" x1="47.81" y1="75" x2="185.48" y2="75" gradientUnits="userSpaceOnUse">
                                        <stop offset="0" stop-color="#b956ff"></stop>
                                        <stop offset="1" stop-color="#6e33ff"></stop>
                                      </linearGradient>
                                      <linearGradient id="linear-gradient-2" x1="0" y1="75" x2="85.18" y2="75" gradientUnits="userSpaceOnUse">
                                        <stop offset="0" stop-color="#b956ff"></stop>
                                        <stop offset="1" stop-color="#a356fa"></stop>
                                      </linearGradient>
                                      <linearGradient id="linear-gradient-3" x1="55.61" y1="62.54" x2="62.86" y2="62.54" gradientUnits="userSpaceOnUse">
                                        <stop offset="0" stop-color="#b956ff"></stop>
                                        <stop offset="1" stop-color="#ac59fa"></stop>
                                      </linearGradient>
                                      <linearGradient id="linear-gradient-4" x1="54.62" y1="79" x2="61.87" y2="79" gradientTransform="translate(19.64 -11.24) rotate(13.24)" xlink:href="#linear-gradient-3"></linearGradient>
                                    </defs>
                                    <g id="Layer_1-2" data-name="Layer 1">
                                      <g>
                                        <g>
                                          <g>
                                            <path class="cls-n6" d="m181.49,58.53h-10.72c-1.4-5.13-3.43-9.99-6.01-14.51l7.58-7.58c1.56-1.56,1.56-4.09,0-5.65l-17.65-17.65c-1.56-1.56-4.09-1.56-5.65,0l-7.58,7.58c-4.51-2.58-9.38-4.61-14.51-6.01V4c0-2.21-1.79-4-4-4h-24.96c-2.21,0-4,1.79-4,4v10.72c-.16.04-.31.09-.47.14-.27.08-.53.15-.8.23-.27.08-.55.16-.82.24-.55.17-1.09.35-1.63.53-.11.04-.22.08-.33.12-.48.17-.96.34-1.44.52-.12.04-.24.09-.36.14-.52.2-1.04.4-1.56.62-.04.02-.08.03-.12.05-2.41,1-4.74,2.15-6.99,3.43l-7.57-7.57c-1.56-1.56-4.09-1.56-5.65,0l-17.65,17.65c-.36.36-.63.76-.82,1.2h61.99v.02c.23,0,.45-.02.68-.02,23.76,0,43.01,19.26,43.01,43.01s-19.26,43.01-43.01,43.01c-.5,0-1-.02-1.5-.04v.04h-61.17c.19.43.46.84.82,1.2l17.65,17.65c1.56,1.56,4.09,1.56,5.65,0l7.57-7.57c2.25,1.28,4.58,2.43,6.99,3.43.04.02.08.03.12.05.51.21,1.03.42,1.55.62.12.05.24.09.36.14.47.18.95.35,1.43.52.11.04.23.08.34.12.54.18,1.08.36,1.63.53.27.08.55.16.83.25.26.08.53.16.79.23.16.04.31.09.47.14v10.72c0,2.21,1.79,4,4,4h24.96c2.21,0,4-1.79,4-4v-10.72c5.13-1.4,9.99-3.43,14.51-6.01l7.58,7.58c1.56,1.56,4.09,1.56,5.65,0l17.65-17.65c1.56-1.56,1.56-4.09,0-5.65l-7.58-7.58c2.58-4.51,4.61-9.38,6.01-14.51h10.72c2.21,0,4-1.79,4-4v-24.96c0-2.21-1.79-4-4-4Z"></path>
                                            <path class="cls-n1" d="m76.64,101.54c-5.74-7.31-9.18-16.52-9.18-26.54s3.44-19.23,9.18-26.54c2.45-3.13,5.33-5.9,8.53-8.24h-48.39c-1.31-1.89-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.13h17.07c-1.51,3.21-2.76,6.58-3.71,10.07h3.1c1.31-1.89,3.49-3.13,5.97-3.13,4.01,0,7.25,3.25,7.25,7.25s-3.22,7.23-7.21,7.25h-.04c-2.48,0-4.66-1.24-5.97-3.13H13.22c-.44-.64-.98-1.19-1.59-1.65-1.21-.92-2.73-1.48-4.38-1.48-4,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.14h38.82c1.31-1.89,3.49-3.13,5.97-3.13.43,0,.84.05,1.25.12,3.41.59,6,3.56,6,7.14,0,2.78-1.56,5.19-3.86,6.41-1.01.53-2.16.84-3.39.84-2.48,0-4.66-1.24-5.97-3.13h-17.85c-1.31-1.9-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.13h15.96c.95,3.48,2.2,6.85,3.71,10.07h-5.63c-1.31-1.89-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25,0,1.53.48,2.95,1.28,4.12,1.31,1.9,3.49,3.14,5.97,3.14s4.66-1.24,5.97-3.14h36.95c-3.21-2.34-6.08-5.11-8.53-8.24ZM30.64,47.97c-2,0-3.62-1.62-3.62-3.62s1.62-3.62,3.62-3.62,3.63,1.62,3.63,3.62-1.63,3.62-3.63,3.62Zm-23.39,26.47c-2,0-3.62-1.63-3.62-3.62s1.63-3.63,3.62-3.63,3.63,1.63,3.63,3.63-1.63,3.62-3.63,3.62Zm20.97,16.45c-2,0-3.63-1.63-3.63-3.63s1.63-3.62,3.63-3.62,3.62,1.63,3.62,3.62-1.63,3.63-3.62,3.63Zm14.08,18.39c-2,0-3.62-1.62-3.62-3.62s1.62-3.62,3.62-3.62,3.62,1.62,3.62,3.62-1.62,3.62-3.62,3.62Z"></path>
                                            <circle class="cls-n5" cx="59.23" cy="62.54" r="3.63"></circle>
                                            <circle class="cls-n7" cx="58.24" cy="79" r="3.63" transform="translate(-16.54 15.44) rotate(-13.24)"></circle>
                                          </g>
                                          <g>
                                            <g>
                                              <path class="cls-n3" d="m104.63,78.16c-9.44,6-14.52,2.86-15.25-9.4,4.93,3.38,10.02,6.52,15.25,9.4Z"></path>
                                              <path class="cls-n3" d="m131.01,68.76c-.72,12.26-5.81,15.4-15.25,9.4,5.24-2.88,10.32-6.02,15.25-9.4Z"></path>
                                            </g>
                                            <path class="cls-n4" d="m110.16,40.13c-.89,0-1.78.03-2.66.1-.08,0-.16,0-.24.02-9.59.79-18.07,5.45-23.89,12.42-.25.3-.49.59-.73.89-.14.19-.29.38-.43.57-4.02,5.37-6.52,11.93-6.88,19.07,0,.01,0,.03,0,.04-.01.22-.02.45-.03.68-.01.3-.02.6-.02.9v.18c0,.28,0,.55,0,.83.44,18.64,15.5,33.66,34.15,34.04.24,0,.48,0,.72,0,.3,0,.6,0,.9,0,18.85-.48,33.98-15.9,33.98-34.87s-15.61-34.88-34.88-34.88Zm.19,39.35c-14.41,14.37-29.15,1.23-29.15-9.45s13.05,0,29.15,0,29.15-10.67,29.15,0-14.74,23.81-29.15,9.45Z"></path>
                                          </g>
                                        </g>
                                        <g>
                                          <path class="cls-n2" d="m286.65,118.51h-17.37l-39.32-59.42v59.42h-17.37V31.8h17.37l39.32,59.54V31.8h17.37v86.71Z"></path>
                                          <path class="cls-n2" d="m304.95,38.69c-2.03-1.94-3.04-4.36-3.04-7.26s1.01-5.31,3.04-7.26c2.03-1.94,4.57-2.92,7.63-2.92s5.6.97,7.63,2.92c2.02,1.94,3.04,4.36,3.04,7.26s-1.02,5.31-3.04,7.26c-2.03,1.94-4.57,2.91-7.63,2.91s-5.6-.97-7.63-2.91Zm16.19,11.1v68.72h-17.37V49.79h17.37Z"></path>
                                          <path class="cls-n2" d="m396.18,56.55c5.04,5.17,7.57,12.38,7.57,21.65v40.31h-17.36v-37.96c0-5.46-1.37-9.65-4.1-12.59-2.73-2.94-6.45-4.4-11.16-4.4s-8.58,1.47-11.35,4.4c-2.77,2.94-4.15,7.13-4.15,12.59v37.96h-17.37V49.79h17.37v8.56c2.31-2.98,5.27-5.31,8.87-7.01,3.6-1.69,7.55-2.54,11.85-2.54,8.19,0,14.8,2.59,19.85,7.75Z"></path>
                                          <path class="cls-n2" d="m437.61,129.8c0,7.61-1.88,13.09-5.64,16.44-3.77,3.35-9.16,5.02-16.19,5.02h-7.69v-14.76h4.96c2.64,0,4.51-.52,5.58-1.55,1.07-1.03,1.61-2.71,1.61-5.02V49.79h17.37v80.01Zm-16.31-91.11c-2.03-1.94-3.04-4.36-3.04-7.26s1.01-5.31,3.04-7.26c2.02-1.94,4.61-2.92,7.75-2.92s5.58.97,7.57,2.92c1.98,1.94,2.98,4.36,2.98,7.26s-.99,5.31-2.98,7.26c-1.98,1.94-4.51,2.91-7.57,2.91s-5.73-.97-7.75-2.91Z"></path>
                                          <path class="cls-n2" d="m454.42,65.42c2.77-5.37,6.53-9.51,11.29-12.4,4.76-2.89,10.07-4.34,15.94-4.34,5.13,0,9.61,1.04,13.46,3.1,3.85,2.07,6.92,4.67,9.24,7.82v-9.8h17.49v68.72h-17.49v-10.05c-2.23,3.23-5.31,5.89-9.24,8-3.93,2.11-8.46,3.16-13.58,3.16-5.79,0-11.06-1.49-15.82-4.47-4.76-2.98-8.52-7.17-11.29-12.59-2.77-5.41-4.16-11.64-4.16-18.67s1.38-13.11,4.16-18.48Zm47.45,7.88c-1.65-3.02-3.89-5.33-6.7-6.95-2.81-1.61-5.83-2.42-9.06-2.42s-6.2.79-8.93,2.36c-2.73,1.57-4.94,3.87-6.64,6.88-1.69,3.02-2.54,6.6-2.54,10.73s.85,7.75,2.54,10.85c1.69,3.1,3.93,5.48,6.7,7.13,2.77,1.66,5.72,2.48,8.87,2.48s6.24-.81,9.06-2.42c2.81-1.61,5.04-3.93,6.7-6.95,1.65-3.02,2.48-6.64,2.48-10.85s-.83-7.83-2.48-10.85Z"></path>
                                          <path class="cls-n4" d="m591.93,102.01h-34.48l-5.71,16.5h-18.24l31.14-86.71h20.22l31.13,86.71h-18.36l-5.71-16.5Zm-4.71-13.89l-12.53-36.22-12.53,36.22h25.06Z"></path>
                                          <path class="cls-n4" d="m645.02,31.93v86.58h-17.37V31.93h17.37Z"></path>
                                        </g>
                                      </g>
                                    </g>
                                  </svg>
               </div>
               <div class="col-12 text-center lh150 mt20 mt-md70">
                  <div class="heading f-18 f-md-22 w600 lh140 white-clr">
                     Legally... Swipe Guru's Profitable Funnels with Our “ChatGPT4 Secret Agent”
                  </div>
               </div>
               <div class="col-12 mt-md30 mt20 f-md-42 f-28 w400 text-center white-clr lh140">
                  World's First Google-Killer Ai App Builds Us <br class="d-none d-md-block">
                  <span class="white-clr w700">DFY Profitable Affiliate Funnel Sites, Prefilled with Content, Reviews &amp; Lead Magnet in Just 60 Seconds.</span>
               </div>
               <div class="col-12 mt-md15 mt15 text-center">
                  <div class="f-22 f-md-28 w700 lh140 black-clr yellow-brush">
                     Then Promote Them to Our Secret Buyers Network of 496 million Users...
                  </div>
               </div>
               <div class="col-12 mt20 text-center">
                  <div class="f-22 f-md-28 w400 lh140 white-clr">
                     &amp; Making Us <span class="w600 underline">$955.45 Commissions Daily</span>  with Literally Zero Work                 
                  </div>
               </div>
               <div class="col-12 mt20 text-center">
                  <div class="f-20 f-md-22 w400 lh140 red-bg white-clr">
                     No Designing, No Copywriting, &amp; No Hosting…. Even A 100% Beginner Can Do It.
                  </div>
               </div>
            </div>

            <div class="row mt20 mt-md40 gx-md-5">
               <div class="col-md-7 col-12">
                  <div class="col-12">
                     <div class="f-18 f-md-19 w500 lh140 white-clr shape-head">
                     Watch How We Swiped A Profitable Funnel With Just One Click And Made $30,345.34 Per Month On Complete Autopilot…
                     </div>
                     
                     <div class="video-box mt20 mt-md30">
                        <!-- <img src="assets/images/video-thumb.webp" alt="Video" class="mx-auto d-block img-fluid"> -->
                        <div style="padding-bottom: 56.25%;position: relative;"><iframe src="https://ninjaai.dotcompal.com/video/embed/q5429rqlji" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe></div>
                     </div>
                  </div>
               </div>
               <div class="col-md-5 col-12 mt20 mt-md0">
                  <div class="key-features-bg">
                     <ul class="list-head pl0 m0 f-16 f-md-18 lh160 w400 white-clr">
                        <li><span class="w600">Eliminate All The Guesswork</span> And Jump Straight to Results</li>
                        <li><span class="w600">Let Ai Find</span> Hundreds Of The Guru's Profitable Funnels</li>
                        <li><span class="w600">Let Ai Create</span> Proven Money Making Funnels On Autopilot</li>
                        <li class="w600">All The Copywriting And Designing Is Done For You</li>
                        <li>Generate 100% <span class="w600">SEO &amp; Mobile Optimized</span> Funnels</li>
                        <li><span class="w600">Instant High Converting Traffic For 100% Free…</span> No Need To Run Ads</li>
                        <li>98% Of Beta Testers Made At Least One Sale <span class="w600 underline">Within 24 Hours</span> Of Using Ninja AI.</li>
                        <li><span class="w600">A True Ai App</span> That Leverages ChatGPT4</li>
                        <li>Connect Your Favourite Autoresponders And <span class="w600">Build Your List FAST.</span></li>
                        <li>Seamless Integration With Any Payment Processor You Want</li>
                        <li class="w600">ZERO Upfront Cost</li>
                        <li>No Complicated Setup - Get Up And Running In 2 Minutes</li>
                        <li class="w600">So Easy, Anyone Can Do it.</li>
                        <li>30 Days Money-Back Guarantee</li>
                        <li><span class="w600">Free Commercial License Included -</span> That's Priceless</li>
                     </ul>
                  </div>
               </div>
            </div> 

			<div class="row mt20 mt-md40">
				<div class="col-md-12 mx-auto col-12 mt20 mt-md20 text-center affiliate-link-btn1">
					<a href="https://www.getninjaai.com/signup"> Please Signup For Special Gift </a>
				</div>
            </div>
        </div>
    </div>
<!--1. Header Section End -->
	
	<div class="free-gifts">
		<div class="container">
			<!-- bonus-1 -->
			<div class="row align-items-center">
					<div class="col-md-6">
						<div class="f-22 f-md-28 lh140 w700 white-clr">
							<div class="option-design mr10 mr-md20">Bonus 1</div> 
						</div>
						<div class="f-25 f-md-32 w600 lh140 black-clr mt20 mt-md30">
						ChatGPT Blueprint
						</div>
						<div class="f-20 w600 lh140 black-clr mt20 mt-md30">
						This course is based on over 15 years of building a business online and using that knowledge to utilize ChatGPT. 
						</div>
						<div class="f-20 w400 lh140 black-clr mt20">
						Some of these are the most effective strategies that are already being used by successful entrepreneurs worldwide.
						</div>
						<div class="f-md-22 w700 lh160 d-btn mt20 mt-md30">
						<a href="https://tinyurl.com/2v5rhebb" target="_blank">Download Now 🎁</a>
						</div>
					</div>
					<div class="col-md-6 mt20 mt-md0">
						<img src="assets/images/bonus1.webp" alt="Bonus 1" class="mx-auto d-block img-fluid">
					</div>
            	</div>

			<!-- bonus-2 -->
				<div class="row mt20 mt-md0 align-items-center">
					<div class="col-md-6 order-md-1">
						<div class="f-22 f-md-28 lh140 w700 white-clr">
							<div class="option-design mr10 mr-md20">Bonus 2</div> 
						</div>
						<div class="f-25 f-md-32 w600 lh140 black-clr mt20 mt-md30">
						The Opt-In Funnel Crash Course
						</div>
						<div class="f-20 w600 lh140 black-clr mt20 mt-md30">
						Every industry leader has an email list. It allows you for widespread distribution of information to your subscribers. 
						</div>
						<div class="f-20 w400 lh140 black-clr mt20">
						The success of your email marketing efforts largely depends on the size and quality of your email list.
						</div>
						<div class="f-md-22 w700 lh160 d-btn mt20 mt-md30">
						<a href="https://tinyurl.com/yc3knn44" target="_blank">Download Now 🎁</a>
						</div>
					</div>
					<div class="col-md-6 mt20 mt-md0">
						<img src="assets/images/bonus2.webp" alt="Bonus 2" class="mx-auto d-block img-fluid">
					</div>
            	</div>

			<!-- bonus-3 -->
				<div class="row mt20 mt-md0 align-items-center">
					<div class="col-md-6">
						<div class="f-22 f-md-28 lh140 w700 white-clr">
							<div class="option-design mr10 mr-md20">Bonus 3</div> 
						</div>
						<div class="f-25 f-md-32 w600 lh140 black-clr mt20 mt-md30">
						Blockchain Power
						</div>
						<div class="f-20 w400 lh140 black-clr mt20">
						Blockchain Power is a new powerful report that many business owners will want to have. You can use this report as a very appealing lead magnet as many businesses want to know more about blockchain. In the report readers will learn what blockchain really is and what it isn’t. They will also be provided with a non-technical overview of how blockchain works.
						</div>
						<div class="f-md-22 w700 lh160 d-btn mt20 mt-md30">
						<a href="https://tinyurl.com/4skb9nsz" target="_blank">Download Now 🎁</a>
						</div>
					</div>
					<div class="col-md-6 mt20 mt-md0">
						<img src="assets/images/bonus3.webp" alt="Bonus 3" class="mx-auto d-block img-fluid blockchaine-image">
					</div>
            	</div>

			<!-- bonus-4 -->
				<div class="row mt20 mt-md100 align-items-center">
					<div class="col-md-6 order-md-1">
						<div class="f-22 f-md-28 lh140 w700 white-clr">
							<div class="option-design mr10 mr-md20">Bonus 4</div> 
						</div>
						<div class="f-25 f-md-32 w600 lh140 black-clr mt20 mt-md30">
						Artificial Intelligence In Digital Marketing
						</div>
						<div class="f-20 w600 lh140 black-clr mt20 mt-md30">
						Being smart in business means knowing what’s just around the corner. It means thinking ahead and preparing for inevitable changes that will impact the way business is conducted. 
						</div>
						<div class="f-20 w400 lh140 black-clr mt20">
						This is what allows a business to be resilient and to thrive in a changing environment.
						</div>
						<div class="f-md-22 w700 lh160 d-btn mt20 mt-md30">
						<a href="https://tinyurl.com/4mjhdajm" target="_blank">Download Now 🎁</a>
						</div>
					</div>
					<div class="col-md-6 mt40 mt-md0">
						<img src="assets/images/bonus4.webp" alt="Bonus 4" class="mx-auto d-block img-fluid Artificial-image">
					</div>
            	</div>

			<!-- bonus-5 -->
				<div class="row mt40 mt-md100 align-items-center">
					<div class="col-md-6">
						<div class="f-22 f-md-28 lh140 w700 white-clr">
							<div class="option-design mr10 mr-md20">Bonus 5</div> 
						</div>
						<div class="f-25 f-md-32 w600 lh140 black-clr mt20 mt-md30">
						Infographics E-Commerce Expansion
						</div>
						<div class="f-20 w400 lh140 black-clr mt20">
						Infographics is one of the best marketing materials nowadays if you want to attract more customers or leads into your offers. The fact is that information graphics have many uses both for marketing and building brands.
						</div>
						<div class="f-md-22 w700 lh160 d-btn mt20 mt-md30">
						<a href="https://tinyurl.com/4kb6f925" target="_blank">Download Now 🎁</a>
						</div>
					</div>
					<div class="col-md-6 mt20 mt-md0">
						<img src="assets/images/bonus5.webp" alt="Bonus 5" class="mx-auto d-block img-fluid">
					</div>
            	</div>

			<!-- bonus-6 -->
				<div class="row mt40 mt-md100 align-items-center">
					<div class="col-md-6 order-md-1">
						<div class="f-22 f-md-28 lh140 w700 white-clr">
							<div class="option-design mr10 mr-md20">Bonus 6</div> 
						</div>
						<div class="f-25 f-md-32 w600 lh140 black-clr mt20 mt-md30">
						Sales Presentation Graphics Pack
						</div>
						<div class="f-20 w600 lh140 black-clr mt20 mt-md30">
							This graphics pack includes 50 images of men and women giving sales presentations.
							<span class="w400"> You can use it to add appeal to blog posts, marketing, PLR content and so much more.</span> 
						</div>
						<div class="f-md-22 w700 lh160 d-btn mt20 mt-md30">
						<a href="https://tinyurl.com/277u29wd" target="_blank">Download Now 🎁</a>
						</div>
					</div>
					<div class="col-md-6 mt20 mt-md0">
						<img src="assets/images/bonus6.webp" alt="Bonus 4" class="mx-auto d-block img-fluid">
					</div>
            	</div>

			<!-- bonus-7 -->
				<div class="row mt20 mt-md100 align-items-center">
					<div class="col-md-6">
						<div class="f-22 f-md-28 lh140 w700 white-clr">
							<div class="option-design mr10 mr-md20">Bonus 7</div> 
						</div>
						<div class="f-25 f-md-32 w600 lh140 black-clr mt20 mt-md30">
						Graphics Black Box V3
						</div>
						<div class="f-20 w600 lh140 black-clr mt20 mt-md30">
						Break All The Rules In Graphics Design! Get More Sales! Command More Fees! Make More Money! 
						</div>
						<div class="f-20 w400 lh140 black-clr mt20">
						Get More Sales and Commad More Fees and Eventually Make More Money Online Using Flat Design in Your Marketing Pages.
						</div>
						<div class="f-md-22 w700 lh160 d-btn mt20 mt-md30">
						<a href="https://tinyurl.com/2rzduhxb" target="_blank">Download Now 🎁</a>
						</div>
					</div>
					<div class="col-md-6 mt20 mt-md0">
						<img src="assets/images/bonus7.webp" alt="Bonus 7" class="mx-auto d-block img-fluid">
					</div>
            	</div>

			<!-- bonus-8 -->
				<div class="row mt20 mt-md100 align-items-center">
					<div class="col-md-6 order-md-1">
						<div class="f-22 f-md-28 lh140 w700 white-clr">
							<div class="option-design mr10 mr-md20">Bonus 8</div> 
						</div>
						<div class="f-25 f-md-32 w600 lh140 black-clr mt20 mt-md30">
						Color Wave Video Loops Pack
						</div>
						<div class="f-20 w600 lh140 black-clr mt20 mt-md30">
						Professional High Quality Motion Graphic Background For You To Use! 
						</div>
						<div class="f-20 w400 lh140 black-clr mt20">
						Videos are one of the best tool or media to make your website visitors stay longer on your page or deliver the information you want to share to your readers.
						</div>
						<div class="f-md-22 w700 lh160 d-btn mt20 mt-md30">
						<a href="https://tinyurl.com/4s72swh9" target="_blank">Download Now 🎁</a>
						</div>
					</div>
					<div class="col-md-6 mt20 mt-md0">
						<img src="assets/images/bonus8.webp" alt="Bonus 8" class="mx-auto d-block img-fluid">
					</div>
            	</div>

			<!-- bonus-9 -->
				<div class="row mt20 mt-md100 align-items-center">
					<div class="col-md-6">
						<div class="f-22 f-md-28 lh140 w700 white-clr">
							<div class="option-design mr10 mr-md20">Bonus 9</div> 
						</div>
						<div class="f-25 f-md-32 w600 lh140 black-clr mt20 mt-md30">
						Amazing Infographics
						</div>
						<div class="f-20 w600 lh140 black-clr mt20 mt-md30">
						Over 250 very cool info-graphics for you to use in your marketing efforts! 
						</div>
						<div class="f-20 w400 lh140 black-clr mt20">
						This new set of Graphics will surely full of information you can use in your very own marketing efforts and ideas. 
						</div>
						<div class="f-md-22 w700 lh160 d-btn mt20 mt-md30">
						<a href="https://tinyurl.com/mr2vr456" target="_blank">Download Now 🎁</a>
						</div>
					</div>
					<div class="col-md-6 mt20 mt-md0">
						<img src="assets/images/bonus9.webp" alt="Bonus 9" class="mx-auto d-block img-fluid">
					</div>
            	</div>

			<!-- bonus-10 -->
				<div class="row mt20 mt-md100 align-items-center">
					<div class="col-md-6 order-md-1">
						<div class="f-22 f-md-28 lh140 w700 white-clr">
							<div class="option-design mr10 mr-md20">Bonus 10</div> 
						</div>
						<div class="f-25 f-md-32 w600 lh140 black-clr mt20 mt-md30">
						Outsourcing Templates
						</div>
						<div class="f-20 w600 lh140 black-clr mt20 mt-md30">
						Outsourcing can be helpful for business owners of every level.<span class="w400"> You can assign a task to someone else and have it done for you. After the job is completed to your satisfaction, you simply pay the person you hired and the work is yours to keep.</span>
						</div>
						<div class="f-md-22 w700 lh160 d-btn mt20 mt-md30">
						<a href="https://tinyurl.com/mz3tp42w" target="_blank">Download Now 🎁</a>
						</div>
					</div>
					<div class="col-md-6 mt20 mt-md0">
						<img src="assets/images/bonus10.webp" alt="Bonus 10" class="mx-auto d-block img-fluid">
					</div>
            	</div>
		</div>
	</div>
	
	<!--Footer Section Start -->
	<div class="footer-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
			   		<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 1164.92 192.13" style="max-height:55px"><defs><style>.cls-x1,.cls-x2,.cls-x3,.cls-x4,.cls-x5{fill-rule:evenodd;}.cls-x1{fill:url(#linear-gradient);}.cls-x2{fill:url(#linear-gradient-2);}.cls-x3{fill:url(#linear-gradient-3);}.cls-x4{fill:url(#linear-gradient-4);}.cls-x5{fill:url(#linear-gradient-5);}.cls-x6{fill:#fff;}.cls-x7{fill:url(#linear-gradient-6);}</style><linearGradient id="linear-gradient" x1="82.05" y1="127.34" x2="150.42" y2="127.34" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#624ef2"/><stop offset="1" stop-color="#1e9af7"/></linearGradient><linearGradient id="linear-gradient-2" x1="7.14" y1="79.25" x2="123.43" y2="79.25" xlink:href="#linear-gradient"/><linearGradient id="linear-gradient-3" x1="0" y1="116.36" x2="45.71" y2="116.36" xlink:href="#linear-gradient"/><linearGradient id="linear-gradient-4" x1="3.94" y1="157.47" x2="142.12" y2="157.47" xlink:href="#linear-gradient"/><linearGradient id="linear-gradient-5" x1="3.58" y1="75.02" x2="182.4" y2="75.02" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#ff2d2d"/><stop offset="1" stop-color="#ffbc00"/></linearGradient><linearGradient id="linear-gradient-6" x1="971.74" y1="90.91" x2="1164.92" y2="90.91" xlink:href="#linear-gradient"/></defs><title>AutomailX White</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-x1" d="M150.42,67.57V177.2a15.41,15.41,0,0,1-3.38,9.92q-20.67-19.74-39-37.28l-26-24.94Z"/><path class="cls-x2" d="M123.43,48.09l-62.5,62.32Q47.16,98.7,33.43,86.92q-8.17-7-16.31-14.11c-3.34-2.91-6.75-5.85-10-8.88a6.08,6.08,0,0,1,1.32-.33l24.25-3.27,28.14-3.8,33-4.45Z"/><path class="cls-x3" d="M45.71,119.33,25.4,139.11Q15.2,149,5.81,158.19c-2.33,3-5.83,2.65-5.81-2.51V76.9c0-5.16,3.5-5.32,5.81-2.2l.39.41h0c.73,1.66,1.4,3.32,2,4.81l.15.38Z"/><path class="cls-x4" d="M142.12,190.87a14.7,14.7,0,0,1-8.88,1l-39.42-7.34-33-6.14-28.14-5.24L8.44,168.61a8.2,8.2,0,0,1-4.5-2.68L25.82,145q11.1-10.61,23.23-22.21l15.81,16.51L78,128.3q12.35,12.06,25.78,25.16Q121.84,171.09,142.12,190.87Z"/><path class="cls-x5" d="M10.15,79.16,65,136.41l92.87-81.5,3.93,12.24L182.4,13.62,128.57,32.19l11.14,5.9L61.05,116.53s-54.7-47.67-57.47-50c1.16,1.75,6.57,12.68,6.57,12.68Z"/><path class="cls-x6" d="M283,50.49H247.2L201.86,181.82h29.75l9.74-29.95h47.42l9.73,29.95h29.75ZM248.4,130.2l16.17-49.7h1l16.14,49.7Z"/><path class="cls-x6" d="M406.68,139.88V83.32H434v98.5H407.77V163.93h-1a29.1,29.1,0,0,1-11.06,13.91q-7.73,5.26-18.82,5.26a33.23,33.23,0,0,1-17.38-4.49,30.61,30.61,0,0,1-11.7-12.76q-4.2-8.26-4.27-19.81V83.32h27.32v57.84q.06,8.73,4.68,13.79T387.89,160a19.37,19.37,0,0,0,9.23-2.27,17.53,17.53,0,0,0,7-6.77A21,21,0,0,0,406.68,139.88Z"/><path class="cls-x6" d="M510,180.6q-2.06.65-5.78,1.51a48.09,48.09,0,0,1-9,1.06,41.39,41.39,0,0,1-17.28-2.63,24.12,24.12,0,0,1-11.51-9.36q-4.11-6.36-4-16v-51.3H448.87V83.32h13.47V59.72h27.32v23.6h18.53v20.52H489.66v47.71a12.24,12.24,0,0,0,1.15,5.87,6.34,6.34,0,0,0,3.24,2.92,13.08,13.08,0,0,0,4.84.83,21.79,21.79,0,0,0,3.85-.35l2.95-.55Z"/><path class="cls-x6" d="M569.43,183.74q-14.94,0-25.81-6.38a43,43,0,0,1-16.77-17.83Q521,148.09,521,133t5.9-26.71a43,43,0,0,1,16.77-17.82Q554.5,82,569.43,82t25.81,6.38A43,43,0,0,1,612,106.24q5.91,11.46,5.9,26.71T612,159.53a43,43,0,0,1-16.77,17.83Q584.38,183.75,569.43,183.74Zm.13-21.16a16.88,16.88,0,0,0,11.35-3.88,23.82,23.82,0,0,0,6.89-10.61,46.31,46.31,0,0,0,2.35-15.33,46.31,46.31,0,0,0-2.35-15.33,24.09,24.09,0,0,0-6.89-10.64,16.8,16.8,0,0,0-11.35-3.91,17.26,17.26,0,0,0-11.51,3.91,23.74,23.74,0,0,0-7,10.64,46.58,46.58,0,0,0-2.34,15.33,46.58,46.58,0,0,0,2.34,15.33,23.48,23.48,0,0,0,7,10.61A17.35,17.35,0,0,0,569.56,162.58Z"/><path class="cls-x6" d="M640.74,50.49h19l44.63,109h1.54l44.64-109h19V181.82H754.63V82h-1.28l-41,99.78H697.94L656.9,82h-1.28v99.78H640.74Z"/><path class="cls-x6" d="M828.25,184.13a39.77,39.77,0,0,1-17-3.56,28.82,28.82,0,0,1-12.12-10.33q-4.49-6.77-4.49-16.38,0-8.46,3.34-13.76a23.63,23.63,0,0,1,8.91-8.3,47.46,47.46,0,0,1,12.35-4.52q6.76-1.52,13.62-2.41,9-1.16,14.59-1.76t8.21-2.08q2.6-1.48,2.6-5.13v-.52q0-9.48-5.16-14.75t-15.62-5.25q-10.85,0-17,4.74a28,28,0,0,0-8.66,10.13l-14.37-5.13a34.69,34.69,0,0,1,10.3-14,38.56,38.56,0,0,1,14.11-7A58.86,58.86,0,0,1,837,82a61.89,61.89,0,0,1,10.93,1.12,35.92,35.92,0,0,1,12,4.58,26.84,26.84,0,0,1,9.65,10.46q3.86,7,3.85,18.72v64.9H858.26V168.48h-.77a26.84,26.84,0,0,1-5.13,6.86,29.6,29.6,0,0,1-9.56,6.22A36.78,36.78,0,0,1,828.25,184.13Zm2.3-13.6q9,0,15.17-3.53a24.19,24.19,0,0,0,9.36-9.1,23.36,23.36,0,0,0,3.18-11.74V132.31q-1,1.16-4.2,2.09a64,64,0,0,1-7.44,1.6q-4.2.67-8.15,1.15c-2.63.32-4.75.59-6.38.81a58.57,58.57,0,0,0-11,2.46,19.26,19.26,0,0,0-8.21,5.07q-3.12,3.36-3.11,9.14,0,7.89,5.87,11.89T830.55,170.53Z"/><path class="cls-x6" d="M911.79,66.9a10.65,10.65,0,0,1-7.6-3,9.86,9.86,0,0,1,0-14.49,11.07,11.07,0,0,1,15.2,0,9.86,9.86,0,0,1,0,14.49A10.65,10.65,0,0,1,911.79,66.9Zm-7.7,114.92V83.32h15.14v98.5Z"/><path class="cls-x6" d="M962.06,50.49V181.82H946.93V50.49Z"/><path class="cls-x7" d="M1045.34,0l26.37,62h1.42l47.14-62h44.65l-70.4,90.91L1136,181.82h-43.32l-27-62.06h-1.42l-47.59,62.06H971.74l72.18-90.91L1003,0Z"/></g></g></svg>
					<div class="f-14 f-md-16 w400 mt20 lh150 white-clr text-center" ><span class="w600">Note:</span> This site is not a part of the Facebook website or Facebook Inc. Additionally, this site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc.
               </div>
			   <div class="f-16 f-md-18 w600 mt20 mt-md20 white-clr text-center"> <script type="text/javascript"
                    src="https://warriorplus.com/o2/disclaimer/qy9f4y" defer></script><div class="wplus_spdisclaimer"></div>
                  </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-md-18 f-16 w400 lh140 white-clr text-xs-center">Copyright © AutomailX 2023</div>
                  <ul class="footer-ul w400 f-md-18 f-16 white-clr text-center text-md-right">
                     <li><a href="https://support.bizomart.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://automailxai.com/legal/privacy-policy.html " class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://automailxai.com/legal/terms-of-service.html " class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://automailxai.com/legal/disclaimer.html " class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://automailxai.com/legal/gdpr.html " class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://automailxai.com/legal/dmca.html " class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://automailxai.com/legal/anti-spam.html " class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
    <!--Footer Section End -->


</body>
</html>

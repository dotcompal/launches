<!-- New Timer  Start-->
<script type="text/javascript">
   var noob = $('.countdown').length;
   var stimeInSecs;
   var tickers;

   function timerBegin(seconds) {   
      if(seconds>=0)  {
         stimeInSecs = parseInt(seconds);           
         showRemaining();
      }
      //tickers = setInterval("showRemaining()", 1000);
   }

   function showRemaining() {

      var seconds = stimeInSecs;
         
      if (seconds > 0) {         
         stimeInSecs--;       
      } else {        
         clearInterval(tickers);       
         //timerBegin(59 * 60); 
      }

      var days = Math.floor(seconds / 86400);

      seconds %= 86400;
      
      var hours = Math.floor(seconds / 3600);
      
      seconds %= 3600;
      
      var minutes = Math.floor(seconds / 60);
      
      seconds %= 60;


      if (days < 10) {
         days = "0" + days;
      }
      if (hours < 10) {
         hours = "0" + hours;
      }
      if (minutes < 10) {
         minutes = "0" + minutes;
      }
      if (seconds < 10) {
         seconds = "0" + seconds;
      }
      var i;
      var countdown = document.getElementsByClassName('countdown');

      for (i = 0; i < noob; i++) {
         countdown[i].innerHTML = '';
      
         if (days) {
            countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-24 f-md-28 timerbg oswald">' + days + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-14 w400 smmltd">Days</span> </div>';
         }
      
         countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-24 f-md-28 timerbg oswald">' + hours + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-14 w400 smmltd">Hours</span> </div>';
      
         countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"><span class="f-24 f-md-28 timerbg oswald">' + minutes + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-14 w400 smmltd">Mins</span> </div>';
      
         countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-24 f-md-28 timerbg oswald">' + seconds + '</span><br><span class="f-14 f-md-14 w400 smmltd">Sec</span> </div>';
      }
   
   }

const givenTimestamp = 'Fri Nov 03 2023 00:50:10 GMT+0530'
const durationTime = 30;

const date = new Date(givenTimestamp);
const givenTime = date.getTime() / 1000;
const currentTime = new Date().getTime()



let cnt;
const matchInterval = setInterval(() => {
   if(Math.round(givenTime) === Math.round(new Date().getTime() /1000)){
   cnt = durationTime * 60;
   counter();
   clearInterval(matchInterval);
   }else if(Math.round(givenTime) < Math.round(new Date().getTime() /1000)){
      const timeGap = Math.round(new Date().getTime() /1000) - Math.round(givenTime)
      let difference = (durationTime * 60) - timeGap;
      if(difference >= 0){
         cnt = difference
         localStorage.mavasTimer = cnt;

      }else{
         
         difference = difference % (durationTime * 60);
         cnt = (durationTime * 60) + difference
         localStorage.mavasTimer = cnt;

      }

      counter();
      clearInterval(matchInterval);
   }
},1000)





function counter(){
   if(parseInt(localStorage.mavasTimer) > 0){
      cnt = parseInt(localStorage.mavasTimer);
   }
   cnt -= 1;
   localStorage.mavasTimer = cnt;
   if(localStorage.mavasTimer !== NaN){
      timerBegin(parseInt(localStorage.mavasTimer));
   }
   if(cnt>0){
      setTimeout(counter,1000);
  }else if(cnt === 0){
   cnt = durationTime * 60 
   counter()
  }
}

</script>
<!-- Exit Popup -->	
<script type="text/javascript" src="assets/js/ouibounce.min.js"></script>
<div id="ouibounce-modal" style="z-index:9999999; display:none;">
<div class="underlay"></div>
<div class="modal-wrapper" style="display:block;">
   <div class="modal-bg">
      <button type="button" class="close" onclick="document.getElementById('ouibounce-modal').style.display = 'none';">
      X 
      </button>
      <div class="model-header">
         <div class="d-flex justify-content-center align-items-center gap-3">
            <img src="assets/images/hold.png" alt="" class="img-fluid d-block m131">
            <div>
               <div class="f-md-56 f-24 w700 lh130 orange-clr">
                  WAIT! HOLD ON!!
               </div>
               <div class="f-md-28 f-18 w700 lh130 white-clr mt10 mt-md0">
                  Don't Leave Empty Handed
               </div>
            </div>
         </div>
      </div>
      <div class="col-12">
         <div class="copun-line f-md-22 f-16 w500 lh130 text-center black-clr mt10">
            Use Coupon Code <span class="w700 timer-clr">AutoMailX</span>  to Get <span class="w700 timer-clr">$3 Off</span> Instantly.
         </div>
         <div class="mt-md20 mt10 text-center ">
            <a href="https://warriorplus.com/o2/buy/qy9f4y/nb5h2m/lvzzql" class="cta-link-btn1">Grab SPECIAL Discount Now!
            <br>
            <span class="f-12 white-clr w500 text-uppercase">Act Now! Limited to First 100 Buyers Only.</span> </a>
         </div>
         <div class="mt-md20 mt10 text-center f-md-26 f-16 w900 lh150 text-center red-clr text-uppercase">
            Coupon Code EXPIRES IN:
         </div>
         <div class="timer-new">
            <div class="days" class="timer-label">
               <span id="days"></span> <span class="text">Days</span>
            </div>
            <div class="hours" class="timer-label">
               <span id="hours"></span> <span  class="text">Hours</span>
            </div>
            <div class="days" class="timer-label">
               <span id="mins" ></span> <span class="text">Min</span>
            </div>
            <div class="secs" class="timer-label">
               <span id="secs" ></span> <span class="text">Sec</span>
            </div>
         </div>
      </div>
   </div>
</div>
<
   var _ouibounce = ouibounce(document.getElementById('ouibounce-modal'),{
   aggressive: true, //Making this true makes ouibounce not to obey "once per visitor" rule
   });
</script>
<!-- Exit Popup Ends-->

<!--- timer end-->
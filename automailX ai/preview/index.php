<!DOCTYPE html>
<html>
   <head>
      <title>AutomailX Special</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=9">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
      <meta name="title" content="AutomailX Special">
      <meta name="description" content="Most Special and Powerful Tag & Segment Based Email Software That Gets Us 4X More Opening & Click without Any Monthly Fee Ever!">
      <meta name="keywords" content="AutomailX Special">
      <meta property="og:image" content="https://automailxai.com/special/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Pranshu Gupta">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="AutomailX Special">
      <meta property="og:description" content="Most Special and Powerful Tag & Segment Based Email Software That Gets Us 4X More Opening & Click without Any Monthly Fee Ever!">
      <meta property="og:image" content="https://automailxai.com/special/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="AutomailX Special">
      <meta property="twitter:description" content="Most Special and Powerful Tag & Segment Based Email Software That Gets Us 4X More Opening & Click without Any Monthly Fee Ever!">
      <meta property="twitter:image" content="https://automailxai.com/special/thumbnail.png">
      <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
      <link rel="preconnect" href="https://fonts.googleapis.com">
       <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&family=Inter:wght@300;400;500;600;700;800;900&display=swap" rel="stylesheet">
      <link rel="stylesheet" href="../common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style.css" type="text/css" />
      <link rel="stylesheet" href="assets/css/timer.css" type="text/css" />
      <script src="https://cdn.oppyo.com/launches/webpull/common_assets/js/jquery.min.js"></script>   


      <script>
      (function(w, i, d, g, e, t, s) {
            if(window.businessDomain != undefined){
               console.log("Your page have duplicate embed code. Please check it.");
               return false;
            }
            businessDomain = 'AutomailX';
            allowedDomain = 'automailxai.com';
            if(!window.location.hostname.includes(allowedDomain)){
               console.log("Your page have not authorized. Please check it.");
               return false;
            }
            console.log("Your script is ready...");
            w[d] = w[d] || [];
            t = i.createElement(g);
            t.async = 1;
            t.src = e;
            s = i.getElementsByTagName(g)[0];
            s.parentNode.insertBefore(t, s);
      })(window, document, '_gscq', 'script', 'https://cdn.staticdcp.com/apps/engage/smart_engage/js/config-loader.js');
      </script>
   </head>
   <body>

   <!-- <div class="timer-header-top fixed-top">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-12 col-md-3">
                  <div class="tht-left f-14 f-md-16 text-md-start text-center white-clr">
                     Use Coupon Code <span class="neon-clr">"AutoMailX"</span>  for <br class="d-block d-md-none">Extra <span class="neon-clr"> $3 Discount </span>
                  </div>
               </div>
               <div class="col-8 col-md-6">
               <div class="countdown counter-white text-center">
                     <div class="timer-label text-center"><span class="f-24 f-md-28 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
                     <div class="timer-label text-center"><span class="f-24 f-md-28 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                     <div class="timer-label text-center timer-mrgn"><span class="f-24 f-md-28 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                     <div class="timer-label text-center "><span class="f-24 f-md-28 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                  </div>
               </div>
               <div class="col-4 col-md-3 text-center text-md-right">
                  <div class="affiliate-link-btn"><a href="#buynow" class="t-decoration-none">Buy Now</a></div>
               </div>
            </div>
         </div>
      </div>	 -->
      <!-- Header Section Start -->
      <div class="header-section">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                  <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 1164.92 192.13" style="max-height:55px"><defs><style>.cls-1,.cls-2,.cls-3,.cls-4,.cls-5{fill-rule:evenodd;}.cls-1{fill:url(#linear-gradient);}.cls-2{fill:url(#linear-gradient-2);}.cls-3{fill:url(#linear-gradient-3);}.cls-4{fill:url(#linear-gradient-4);}.cls-5{fill:url(#linear-gradient-5);}.cls-6{fill:#fff;}.cls-7{fill:url(#linear-gradient-6);}</style><linearGradient id="linear-gradient" x1="82.05" y1="127.34" x2="150.42" y2="127.34" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#624ef2"/><stop offset="1" stop-color="#1e9af7"/></linearGradient><linearGradient id="linear-gradient-2" x1="7.14" y1="79.25" x2="123.43" y2="79.25" xlink:href="#linear-gradient"/><linearGradient id="linear-gradient-3" x1="0" y1="116.36" x2="45.71" y2="116.36" xlink:href="#linear-gradient"/><linearGradient id="linear-gradient-4" x1="3.94" y1="157.47" x2="142.12" y2="157.47" xlink:href="#linear-gradient"/><linearGradient id="linear-gradient-5" x1="3.58" y1="75.02" x2="182.4" y2="75.02" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#ff2d2d"/><stop offset="1" stop-color="#ffbc00"/></linearGradient><linearGradient id="linear-gradient-6" x1="971.74" y1="90.91" x2="1164.92" y2="90.91" xlink:href="#linear-gradient"/></defs><title>AutomailX White</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1" d="M150.42,67.57V177.2a15.41,15.41,0,0,1-3.38,9.92q-20.67-19.74-39-37.28l-26-24.94Z"/><path class="cls-2" d="M123.43,48.09l-62.5,62.32Q47.16,98.7,33.43,86.92q-8.17-7-16.31-14.11c-3.34-2.91-6.75-5.85-10-8.88a6.08,6.08,0,0,1,1.32-.33l24.25-3.27,28.14-3.8,33-4.45Z"/><path class="cls-3" d="M45.71,119.33,25.4,139.11Q15.2,149,5.81,158.19c-2.33,3-5.83,2.65-5.81-2.51V76.9c0-5.16,3.5-5.32,5.81-2.2l.39.41h0c.73,1.66,1.4,3.32,2,4.81l.15.38Z"/><path class="cls-4" d="M142.12,190.87a14.7,14.7,0,0,1-8.88,1l-39.42-7.34-33-6.14-28.14-5.24L8.44,168.61a8.2,8.2,0,0,1-4.5-2.68L25.82,145q11.1-10.61,23.23-22.21l15.81,16.51L78,128.3q12.35,12.06,25.78,25.16Q121.84,171.09,142.12,190.87Z"/><path class="cls-5" d="M10.15,79.16,65,136.41l92.87-81.5,3.93,12.24L182.4,13.62,128.57,32.19l11.14,5.9L61.05,116.53s-54.7-47.67-57.47-50c1.16,1.75,6.57,12.68,6.57,12.68Z"/><path class="cls-6" d="M283,50.49H247.2L201.86,181.82h29.75l9.74-29.95h47.42l9.73,29.95h29.75ZM248.4,130.2l16.17-49.7h1l16.14,49.7Z"/><path class="cls-6" d="M406.68,139.88V83.32H434v98.5H407.77V163.93h-1a29.1,29.1,0,0,1-11.06,13.91q-7.73,5.26-18.82,5.26a33.23,33.23,0,0,1-17.38-4.49,30.61,30.61,0,0,1-11.7-12.76q-4.2-8.26-4.27-19.81V83.32h27.32v57.84q.06,8.73,4.68,13.79T387.89,160a19.37,19.37,0,0,0,9.23-2.27,17.53,17.53,0,0,0,7-6.77A21,21,0,0,0,406.68,139.88Z"/><path class="cls-6" d="M510,180.6q-2.06.65-5.78,1.51a48.09,48.09,0,0,1-9,1.06,41.39,41.39,0,0,1-17.28-2.63,24.12,24.12,0,0,1-11.51-9.36q-4.11-6.36-4-16v-51.3H448.87V83.32h13.47V59.72h27.32v23.6h18.53v20.52H489.66v47.71a12.24,12.24,0,0,0,1.15,5.87,6.34,6.34,0,0,0,3.24,2.92,13.08,13.08,0,0,0,4.84.83,21.79,21.79,0,0,0,3.85-.35l2.95-.55Z"/><path class="cls-6" d="M569.43,183.74q-14.94,0-25.81-6.38a43,43,0,0,1-16.77-17.83Q521,148.09,521,133t5.9-26.71a43,43,0,0,1,16.77-17.82Q554.5,82,569.43,82t25.81,6.38A43,43,0,0,1,612,106.24q5.91,11.46,5.9,26.71T612,159.53a43,43,0,0,1-16.77,17.83Q584.38,183.75,569.43,183.74Zm.13-21.16a16.88,16.88,0,0,0,11.35-3.88,23.82,23.82,0,0,0,6.89-10.61,46.31,46.31,0,0,0,2.35-15.33,46.31,46.31,0,0,0-2.35-15.33,24.09,24.09,0,0,0-6.89-10.64,16.8,16.8,0,0,0-11.35-3.91,17.26,17.26,0,0,0-11.51,3.91,23.74,23.74,0,0,0-7,10.64,46.58,46.58,0,0,0-2.34,15.33,46.58,46.58,0,0,0,2.34,15.33,23.48,23.48,0,0,0,7,10.61A17.35,17.35,0,0,0,569.56,162.58Z"/><path class="cls-6" d="M640.74,50.49h19l44.63,109h1.54l44.64-109h19V181.82H754.63V82h-1.28l-41,99.78H697.94L656.9,82h-1.28v99.78H640.74Z"/><path class="cls-6" d="M828.25,184.13a39.77,39.77,0,0,1-17-3.56,28.82,28.82,0,0,1-12.12-10.33q-4.49-6.77-4.49-16.38,0-8.46,3.34-13.76a23.63,23.63,0,0,1,8.91-8.3,47.46,47.46,0,0,1,12.35-4.52q6.76-1.52,13.62-2.41,9-1.16,14.59-1.76t8.21-2.08q2.6-1.48,2.6-5.13v-.52q0-9.48-5.16-14.75t-15.62-5.25q-10.85,0-17,4.74a28,28,0,0,0-8.66,10.13l-14.37-5.13a34.69,34.69,0,0,1,10.3-14,38.56,38.56,0,0,1,14.11-7A58.86,58.86,0,0,1,837,82a61.89,61.89,0,0,1,10.93,1.12,35.92,35.92,0,0,1,12,4.58,26.84,26.84,0,0,1,9.65,10.46q3.86,7,3.85,18.72v64.9H858.26V168.48h-.77a26.84,26.84,0,0,1-5.13,6.86,29.6,29.6,0,0,1-9.56,6.22A36.78,36.78,0,0,1,828.25,184.13Zm2.3-13.6q9,0,15.17-3.53a24.19,24.19,0,0,0,9.36-9.1,23.36,23.36,0,0,0,3.18-11.74V132.31q-1,1.16-4.2,2.09a64,64,0,0,1-7.44,1.6q-4.2.67-8.15,1.15c-2.63.32-4.75.59-6.38.81a58.57,58.57,0,0,0-11,2.46,19.26,19.26,0,0,0-8.21,5.07q-3.12,3.36-3.11,9.14,0,7.89,5.87,11.89T830.55,170.53Z"/><path class="cls-6" d="M911.79,66.9a10.65,10.65,0,0,1-7.6-3,9.86,9.86,0,0,1,0-14.49,11.07,11.07,0,0,1,15.2,0,9.86,9.86,0,0,1,0,14.49A10.65,10.65,0,0,1,911.79,66.9Zm-7.7,114.92V83.32h15.14v98.5Z"/><path class="cls-6" d="M962.06,50.49V181.82H946.93V50.49Z"/><path class="cls-7" d="M1045.34,0l26.37,62h1.42l47.14-62h44.65l-70.4,90.91L1136,181.82h-43.32l-27-62.06h-1.42l-47.59,62.06H971.74l72.18-90.91L1003,0Z"/></g></g></svg>
				
               </div>
               <!--<div class="col-md-7 mt20 mt-md0">-->
               <!--   <ul class="leader-ul f-16 f-md-18 w500 white-clr text-md-end text-center">-->
               <!--      <li><a href="#features" class="white-clr">Features</a></li>-->
               <!--      <li><a href="#demo" class="white-clr">Demo</a></li>-->
               <!--      <li class="d-md-none affiliate-link-btn"><a href="https://warriorplus.com/o2/buy/d8j629/hy9rz2/d410t1" class="white-clr">Buy Now</a></li>-->
               <!--   </ul>-->
               <!--</div>-->
               <!--<div class="col-md-2 affiliate-link-btn text-md-end text-center mt15 mt-md0 d-none d-md-block f-18">-->
               <!--   <a href="https://warriorplus.com/o2/buy/d8j629/hy9rz2/d410t1">Buy Now</a>-->
               <!--</div>-->
               <div class="col-12 text-center mt20 mt-md60">
                  <div class="f-md-22 f-18 w600 text-center white-clr pre-heading lh150">
                  Zero Manual Work. Zero List Building Hassles. Zero Downtime. 
                  </div>
               </div>
               <div class="col-12 mt-md40 mt20 ">
                  <div class="main-heading text-center">
                     <div class="f-md-38 f-28 w700 text-center black-clr lh140">   
                        <span class="blue-gradient">Revealing “MailChimp” Killer AI-Powered App That</span> Writes & Delivers Unlimited Stunning Emails, And Drives Tons of Clicks, Sales, and Commissions
                        <span>
                      </div>
                      <div class="f-18 f-md-22 lh140 w600 text-center white-clr heading-blue-shape mt10">
                      Let AI Do It All. No Tech Hassles. No Monthly Fee Ever...
                      </div>
                   </div>
                </div>
               <div class="col-12 mt-md50 mt20 text-center">
                  <div class="f-20 f-md-26 w500 text-center lh150 white-clr post-heading">
                  <span class="orange-clr w700">Done-For-You Leads, Built-In High-Speed SMTP</span> (Email Delivery Servers), <span class="orange-clr w700">Smart Tagging, Unlimited List Importing & Mailing</span> - All Within an Easy Cloud Based App.
                  </div>
               </div>
               <!--<div class="col-12 mt-md30 mt20 text-center">-->
               <!--   <div class="f-16 f-md-24 w500 text-center lh140 white-clr text-capitalize">-->
               <!--   Say No To Expensive Email Marketing Tools / Autoresponders, Email Writers & Designers-->
               <!--   </div>-->
               <!--</div>-->
               <div class="col-12 mt20 mt-md80">
                  <div class="row align-items-center">
                     <div class="col-md-7 col-12">
                        <!-- <img src="assets/images/product-box.webp" class="img-fluid d-block mx-auto"> -->
                         <div class="col-12 responsive-video">
                           <iframe src="https://automailx.dotcompal.com/video/embed/y2zcl3jd5x" style="width:100%; height:100%" frameborder="0" allow="fullscreen" allowfullscreen ></iframe>
                        </div> 
                        <div class="col-12 mt20 mt-md40 d-none d-md-block">
                        <div class="border-dashed">
                           <div class="f-18 f-md-20 w500 lh140 text-center white-clr">
                           <span class="yellow-clr">Free Gift : </span> First 99 Action Taker Get Free 30 Reseller License + FREE Commercial License <span class="yellow-clr">(Worth 1,997)</span>
                           </div>
                           
                        </div>
                           <div class="f-22 f-md-28 mt20 mt-md40 w500 lh140 text-center white-clr">
                           Get AutoMailX For Low One Time Fee… Just Pay Once Instead Of <strike> $99 Monthly</strike> 
                           </div>
                           <div class="f-18 f-md-20 w400 lh140 text-center white-clr mt20">
                           (Get Free 30 Reseller License + FREE Commercial License)
                           </div>
                           <div class="row">
                              <div class="col-md-11 mx-auto col-12 mt20 mt-md30 text-center ">
                                 <a href="#buynow" class="cta-link-btn px0 d-block">Get Instant Access To AutomailX</a>
                              </div>
                           </div>
                           <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30 ">
                              <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/compaitable-with1.webp " class="img-fluid d-block mx-xs-center md-img-right ">
                              <div class="d-md-block d-none px-md30 "><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/v-line.webp " class="img-fluid "></div>
                              <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/days-gurantee1.webp " class="img-fluid d-block mx-xs-auto mt15 mt-md0 ">
                           </div>
                        </div>
                     </div>
                     <div class="col-md-5 col-12 mt20 mt-md0">
                        <div class="key-features-bg">
                           <ul class="list-head pl0 m0 f-20 lh150 w400 white-clr">
                           <li> Send Unlimited Emails - No Limits At All</li>
                          <li> Let AI Plan And Write Profit-Pulling Email Content</li>
                          
                          <li> AI Will Find Thousands of Targeted Leads in Any Niche</li>
                          <li> Built-In High-Speed SMTP to Deliver Emails Directly into Inbox</li>
                          <li> Zero Server Downtime Headaches Ever</li>
                          <li> AI Enabled Smart Tagging For Personalization & 4X Traffic</li>
                          <li> Design Newsletters & Autoresponders Using The Power Of AI</li>
                          <li> Upload Unlimited List with Zero Restrictions</li>
                          <li> 100+ High Converting Templates For Webforms & Email</li>
                          <li> 100% Mobile Responsive Emails for Maximum Reach</li>
                          <li> Have 100% Control on Your Online Business</li>
                          <li> No Monthly Fee Forever. Pay One Time</li>
                          <li> User Friendly, Cloud Based App. Start within A Minute</li>
                          <li> GDPR & Can-Spam Compliant</li>
                          <li> FREE Commercial License Included</li>
                           </ul>
                        </div>
                        <div class="col-12 mt20 mt-md60 d-md-none">
                           <div class="f-20 f-md-24 w500 lh140 text-center white-clr">
                              Get Free 30 Reseller License + Commercial License <br class="d-none d-md-block"> If You Buy Today!! 
                           </div>
                           <div class="row">
                              <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center ">
                                 <a href="#buynow" class="cta-link-btn px0 d-block">Get Instant Access To AutomailX</a>
                              </div>
                           </div>
                           <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30 ">
                              <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/compaitable-with1.webp " class="img-fluid d-block mx-xs-center md-img-right ">
                              <div class="d-md-block d-none px-md30 "><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/v-line.webp " class="img-fluid "></div>
                              <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/days-gurantee1.webp " class="img-fluid d-block mx-xs-auto mt15 mt-md0 ">
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- <div class="revolutionary-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-28 f-md-45 w700 lh140 black-clr text-center">
                  AutomailX Is Disrupting the Email Industry 
               </div>
               <div class="col-12 f-20 f-md-28 w400 lh140 black-clr text-center mt20 mt-md30">
                  Join the club of 6,458 expert marketers & business owners who are leveraging the power of this unbeatable, fully ChatGPT AI driven technology for…
               </div>
               <div class="col-md-8 col-12 mx-auto f-20 f-md-28 w600 lh140 white-clr blue-bg text-center mt20 mt-md30">
                 Saving 100s of Dollars Monthly & Getting Tons of Traffic, Commissions & Sales on Autopilot
               </div>
               <div class="mt20 mt-md50">
                  <img src="assets/images/arrow.png" class="img-fluid mx-auto d-block downarrow">
               </div>
            </div>
            <div class="row mt30">
               <div class="col-12 col-md-6 mt30">
                  <div class="testimonial-box">
                     <div>
                        <img src="assets/images/quotes.png" class="img-fluid mx-auto d-block">
                     </div>
                     <div class="mt20 f-18 f-md-20 w400 lh140 white-clr text-center">
                     As someone who is always on the go, I often struggle to find the time and energy to craft thoughtful and engaging emails. That's why AutomailX has been a game-changer for me. The AI-powered app is incredibly intuitive and has allowed me to streamline my communication like never before. I highly recommend AutomailX.
                     </div>
                     <div class="mt20 mt-md30">
                        <img src="assets/images/testi1.webp" class="img-fluid mx-auto d-block">
                     </div>
                     <div class="mt20 mt-md30 f-18 f-md-24 w400 lh140 white-clr text-center">
                     Robert John  
                     </div>
                     <div class="mt20 mt-md30">
                        <img src="assets/images/five-star.png" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>
            
               <div class="col-12 col-md-6 mt30">
                  <div class="testimonial-box">
                     <div>
                        <img src="assets/images/quotes.png" class="img-fluid mx-auto d-block">
                     </div>
                     <div class="mt20 f-18 f-md-20 w400 lh140 white-clr text-center">
                     Finally, I am getting my emails delivered, clicked, and opened. Yes, <span class="w600">AutomailX is one of the BEST tools </span> I’ve used for my business yet! No more dependency on other email software. Stupidly simple to use. AutomailX made me go crazy.<span class="w600"> Five Stars from my side for this product…</span> </div>
                     <div class="mt20 mt-md30">
                        <img src="assets/images/testi2.webp" class="img-fluid mx-auto d-block">
                     </div>
                     <div class="mt20 mt-md30 f-18 f-md-24 w400 lh140 white-clr text-center">
                     William Smith 
                     </div>
                     <div class="mt20 mt-md30">
                        <img src="assets/images/five-star.png" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>
               
           
               <div class="col-12 col-md-6 mt30">
                  <div class="testimonial-box">
                     <div>
                        <img src="assets/images/quotes.png" class="img-fluid mx-auto d-block">
                     </div>
                     <div class="mt20 f-18 f-md-20 w400 lh140 white-clr text-center">
                          Importing Lists without losing any Leads? DONE! Sending UNLIMITED Emails? DONE! Want to Generate More Leads? DONE! Automating Email Marketing Campaigns? That’s DONE as well! <br>
                        This software really impressed me with its amazing features. A great option for anyone looking to make the most from email marketing...
                        
                   </div>
                     <div class="mt20 mt-md30">
                        <img src="assets/images/testi3.webp" class="img-fluid mx-auto d-block">
                     </div>
                     <div class="mt20 mt-md30 f-18 f-md-24 w400 lh140 white-clr text-center">
                     Rodney Paul 
                     </div>
                     <div class="mt20 mt-md30">
                        <img src="assets/images/five-star.png" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>

               <div class="col-12 col-md-6 mt30">
                  <div class="testimonial-box">
                     <div>
                        <img src="assets/images/quotes.png" class="img-fluid mx-auto d-block">
                     </div>
                     <div class="mt20 f-18 f-md-20 w400 lh140 white-clr text-center">
                     AutomailX will surely take the industry by storm and will help marketers to generate more leads from any Blog, eCommerce, or WordPress site. I like its latest smart tag feature which enables me to build targeted marketing campaigns. Yet another killer product from Team Pranshu & Chandraprakash...
                     </div>
                     <div class="mt20 mt-md30">
                        <img src="assets/images/testi4.webp" class="img-fluid mx-auto d-block">
                     </div>
                     <div class="mt20 mt-md30 f-18 f-md-24 w400 lh140 white-clr text-center">
                     Alex Holland
                     </div>
                     <div class="mt20 mt-md30">
                        <img src="assets/images/five-star.png" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>

               <div class="col-12 col-md-6 mt30">
                  <div class="testimonial-box">
                     <div>
                        <img src="assets/images/quotes.png" class="img-fluid mx-auto d-block">
                     </div>
                     <div class="mt20 f-18 f-md-20 w400 lh140 white-clr text-center">
                       BINGO, I've tested AutomailX for myself, and man, it delivered exactly as promised. I just recently sent mail to my entire list and got 20% more open rates with this super-amazing Email Marketing Technology. Two-Thumbs up for this one…
                     </div>
                     <div class="mt20 mt-md30">
                        <img src="assets/images/testi5.webp" class="img-fluid mx-auto d-block">
                     </div>
                     <div class="mt20 mt-md30 f-18 f-md-24 w400 lh140 white-clr text-center">
                     Tom Garfield 
                     </div>
                     <div class="mt20 mt-md30">
                        <img src="assets/images/five-star.png" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>

               <div class="col-12 col-md-6 mt30">
                  <div class="testimonial-box">
                     <div>
                        <img src="assets/images/quotes.png" class="img-fluid mx-auto d-block">
                     </div>
                     <div class="mt20 f-18 f-md-20 w400 lh140 white-clr text-center">
                     I always struggle to find the time to write the perfect email. That's where AutomailX comes in - this AI-powered app has completely changed My Way of Writing Mail! It helps me write professional and Attractive emails that truly Interact with my audience.
                      With AutomailX, I never have to worry about the stress of crafting the perfect message .</div>
                     <div class="mt20 mt-md30">
                        <img src="assets/images/testi6.webp" class="img-fluid mx-auto d-block">
                     </div>
                     <div class="mt20 mt-md30 f-18 f-md-24 w400 lh140 white-clr text-center">
                     Andrew Hardy 
                     </div>
                     <div class="mt20 mt-md30">
                        <img src="assets/images/five-star.png" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>
         </div>
      </div>
   </div> -->
   <div class="step-section">
      <div class="container ">
         <div class="row ">
            <div class="col-12 f-28 f-md-45 w700 lh140 black-clr text-center">
            Start Building Lists & Turning Them Into Profits in <span class="blue-gradient"> Just 3 Easy Steps
               </span>
            </div>
            <img src="assets/images/let-line.webp" class="img-fluid mx-auto d-block" style="width:400px;">
            <div class="col-12 f-22 f-md-30 w400 text-center lh140 black-clr mt20 mt-md20">
            Stop Paying 100s of Dollars Monthly to Old-School Email Marketing Software!
            </div>
            <div class="col-12 mt30 mt-md80">
               <div class="row align-items-center">
                  <div class="col-12 col-md-5 relative">
                     <img src="assets/images/step1.webp" class="img-fluid">
                     <div class=" step1 f-28 f-md-50 w600 mt15">
                     Log-In to AutoMailX
                     </div>
                     <div class="f-18 f-md-20 w400 lh140 mt15">
                     Login to your AutoMailX account. Import List with No Limitation or Let AI find thousands of leads in any niche
                     </div>
                     <img src="assets/images/left-arrow.webp" class="img-fluid d-none d-md-block ms-auto step-arrow1">
                  </div>
                  <div class="col-12 col-md-7 mt20 mt-md0 ">
                     <video width="100%" height="auto" loop="" autoplay="" muted="muted" class="gif-bg" >
                        <source src="assets/images/step1.mp4" type="video/mp4" >
                     </video>
                  </div>
               </div>
            </div>
            <div class="col-12 mt-md150 ">
               <div class="row align-items-center ">
                  <div class="col-12 col-md-5 order-md-2 relative">
                     <img src="assets/images/step2.webp" class="img-fluid mt20">
                     <div class=" step2 f-28 f-md-50 w600 mt15">
                     Just Enter Keyword & Your Link
                     </div>
                     <div class="f-18 f-md-20 w400 lh140 mt15">
                     AutoMailX, AI generates profitable & engaging email with your link ready to blast within seconds.

                     </div>
                     <img src="assets/images/right-arrow.webp" class="img-fluid d-none d-md-block step-arrow2">
                  </div>
                  <div class="col-md-7 mt20 mt-md0 order-md-1 ">
                     <video width="100%" height="auto" loop="" autoplay="" muted="muted" class="gif-bg" >
                        <source src="assets/images/step2.mp4" type="video/mp4" >
                     </video>
                  </div>
               </div>
            </div>
            <div class="col-12 mt-md150">
               <div class="row align-items-center mt-md50">
                  <div class="col-12 col-md-5">
                     <img src="assets/images/step3.webp" class="img-fluid mt20">
                     <div class=" step3 f-28 f-md-50 w600 mt15">
                     Send & Profit:
                     </div>
                     <div class="f-18 f-md-20 w400 lh140 mt15">
                     Send unlimited emails directly into inbox for tons of traffic, sales & commissions with just push of a button.

                     </div>
                  </div>
                  <div class="col-12 col-md-7 mt20 mt-md0">
                     <video width="100%" height="auto" loop="" autoplay="" muted="muted" class="gif-bg" >
                        <source src="assets/images/step3.mp4" type="video/mp4" >
                     </video>
                  </div>
               </div>
            </div>
             <div class="col-12 f-md-24 f-20 w700 lh140 text-center mt20 mt-md40 aos-init aos-animate" data-aos="fade-up">
               <div class="smile-text">It Just Takes Less than 60 Seconds...</div>
            </div>
            
            <div class="col-12 text-center">
               
               <div class="d-flex gap-2 gap-md-1 justify-content-center flex-wrap mt20 mt-md30">
                  <div class="d-flex align-items-center gap-2 justify-content-center">
                     <img src="assets/images/red-cross.webp" alt="Cross" class="img-fluid d-block">
                     <div class="f-md-28 w700 lh140 f-20 red-clr"> No Email Writing &nbsp; </div>
                  </div>
                  <div class="d-flex align-items-center gap-2 justify-content-center">
                     <img src="assets/images/red-cross.webp" alt="Cross" class="img-fluid d-block">
                     <div class="f-md-28 w700 lh140 f-20 red-clr"> No Designer &nbsp;</div>
                  </div>
                  <div class="d-flex align-items-center gap-2 justify-content-center">
                     <img src="assets/images/red-cross.webp" alt="Cross" class="img-fluid d-block">
                     <div class="f-md-28 w700 lh140 f-20 red-clr">No Freelancer &nbsp;</div>
                  </div>
                  <div class="d-flex align-items-center gap-2 justify-content-center">
                     <img src="assets/images/red-cross.webp" alt="Cross" class="img-fluid d-block">
                     <div class="f-md-28 w700 lh140 f-20 red-clr">No 3rd Party Apps &nbsp;</div>
                  </div>
                  <div class="d-flex align-items-center gap-2 justify-content-center">
                     <img src="assets/images/red-cross.webp" alt="Cross" class="img-fluid d-block">
                     <div class="f-md-28 w700 lh140 f-20 red-clr">No SMTP </div>
                  </div>
               </div>
            </div>
           <div class="col-12 f-18 f-md-24 w400 lh140 black-clr text-center mt20 mt-md30">
                  Plus, with included free commercial license, this is the easiest & fastest way to start 6 figure business and help desperate businesses in no time! 
               </div>
         </div>
      </div>
   </div>
<div class="cta-section-new">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <!-- <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/limited-time-offer-banner.webp" class="img-fluid d-block mx-auto"> -->
                  <div class="f-22 f-md-34 w500 lh140 text-center white-clr">
                           Grab AutomailX Now for <strike>$99/Month,</strike> Today Only 1-Time $17
                           </div>
                  <div class="f-18 f-md-20 w400 lh140 text-center white-clr mt20">
                     (Get Free 30 Reseller License + FREE Commercial License)
                  </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 text-center">
                        <a href="#buynow" class="cta-link-btn">Get Instant Access To AutomailX</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20">
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/compaitable-with1.webp" class="img-fluid d-block mx-xs-center md-img-right">
                     <div class="d-md-block d-none px-md30"><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/v-line.webp" class="img-fluid"></div>
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/days-gurantee1.webp" class="img-fluid d-block mx-xs-auto mt15 mt-md0">
                  </div>
               </div>
            </div>
         </div>
      </div>
      
      <div class="warning">
         <div class="container">
            <div class="row text-center">
                 <div class="col-md-12 d-flex align-items-center justify-content-center flex-wrap text-center">
                    <img src="assets/images/warning.png" class="img-fluid d-block ml-auto mr10">
                    <div class="f-22 f-md-28 w400 lh140 text-center white-clr ">
                  <span class="w700">WARNING:</span>   Grab The Offer Before Timer Hits Zero</div>
                    </div>
                </div>
            </div>
        </div>
   
   <!-- <section class="future-section">
      <div class="container">
         <div class="row">
               <div class="col-12 f-28 f-md-45 w700 lh130 black-clr text-center thunder">
                  AutomailX Is FUTURE Of Email Marketing
                  <img src="assets/images/double-blue-line.png" alt="line" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 f-18 f-md-28 w400 lh140 black-clr text-center mt20 mt-md30">
                  Here you Can See What you Can Do With This Fully ChatGPT & AI Cutting<br class="d-none d-md-block"> Edge Software.
               </div>
               <div class="col-md-12 mt-md80 mt20">
               <img src="assets/images/future-img.webp" alt="" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-md-12 mt-md80 mt20">
                  <img src="assets/images/yes-you-can.png" alt="" class="img-fluid d-block mx-auto">
               </div>
               <div class="f-18 f-md-28 w400 lh150 black-clr text-center mt20 mt-md60">
                 <span class="blue-gradient1 under w600"> The Best Thing is You Just Need to Enter a Keyword </span> And AutomailX Will Create, <br class="d-none d-md-block">Design & Send Emails For You In Just 60 Seconds Flat.
               </div>
         </div>
      </div>
   </section>  -->

   <div class="profitwall-sec">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-50 w700 lh140 text-center white-clr">
               All It Takes Is 3 Fail-Proof Steps
               </div>
               
            </div>
            <div class="col-12 f-24 f-md-32 lh140 w400 text-center white-clr mt10">
            Start Creating & Sending Beautiful Emails for Floods of Sales… 
            </div>
         </div>
         <div class="row row-cols-1 row-cols-sm-2 row-cols-md-2 gap30 mt20 mt-md10">
            <div class="col">
               <div class="text-center">
                  <img src="assets/images/no-coding.webp" alt="No Coding" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
                     No Coding
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     You don't need to write a single line of code with SiteSmartAi… You won't even see any codes…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     AI does it all for you…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     And there is NO need for any coding or programming of any sort… 
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="assets/images/no-server-configurations.webp" alt="No Server Configurations" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
                     No Server Configurations
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Server management is not an easy task.
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Luckily, you don't have to worry about it even a bit…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Cause AI will handle all of it for you in the background.
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="assets/images/no-content-writing.webp" alt="No Content Writing" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
                     No Content Writing
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     You don't have to write a word with SiteSmartAi…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     It will prefill your website with hundreds of smoking-hot, human-like content
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     That will turn any viewers into profit…
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="assets/images/no-optimizing.webp" alt="No Optimizing" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
                     No Optimizing
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Every article, video, and pictures that will be posted on your website…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Is 100% optimized…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     It will even optimize it for SEO. so you can rank better in Google and other search engines.
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="assets/images/no-designing.webp" alt="No Designing" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
                     No Designing
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     SiteSmartAi eliminates the need to hire a designer…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Or even use an expensive app to design…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     It comes with a built-in feature that will allow you to turn any keyword…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Into a stunning design…
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="assets/images/no-translating.webp" alt="No Translating" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
                     No Translating
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Imagine if you can have your website in 28 different languages…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     That means getting 28x more traffic and sales…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     With SiteSmartAi you don't have to imagine… 
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Because it will do that for you…
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="assets/images/no-paid-ads.webp" alt="No Paid Ads" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
                     No Paid Ads
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Everyone says that you need to run ads to get traffic…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     But that's not the case with SiteSmartAi…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Because it will do that for you…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     It will drive thousands of buyers clicks to you for 100% free…
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="assets/images/no-spam.webp" alt="No Spam" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
                     No Spam
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     You don't need to spam social media with SiteSmartAi…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Because there is no need for that…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     It will create a social strategy for you that does not need any spamming…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     And will give you 10x more clicks to your website…
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="assets/images/fast-result.webp" alt="Fast Result" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
                     Fast Result
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     No need to wait months to see results…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     Because SiteSmartAi is not relying on SEO. The results are BLAZING FAST…
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
                  <img src="assets/images/zero-hidden-fee.webp" alt="Zero Hidden Fee" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
                     Zero Hidden Fee
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     There is no other app needed. There is no other setup needed…
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center text-md-start">
                     You will not pay a single dollar extra when you are using SiteSmartAi
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>

      
      <!-- Proof Section -->
      <div class="proof-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-42 w700 lh140 black-clr ">
                     Checkout How We’ve Got 20,000+ Clicks & Made Over $21,273 Income by Using the Power of AutomailX
                  </div>
                  <div class="mt20 mt-md30">
                     <img src="assets/images/arrow.png" class="img-fluid mx-auto d-block downarrow">
                  </div>
               </div>
               <div class="col-md-6 col-sm-6 col-12 mt20 mt-md40">
                  <img src="assets/images/proof-1.webp" class="img-fluid mx-auto d-block">
               </div>
               <div class="col-md-6 col-sm-6 col-12 mt20 mt-md40">
                  <img src="assets/images/proof-2.webp" class="img-fluid mx-auto d-block">
               </div>

               <div class="col-md-6 col-sm-6 col-12 mt20 mt-md40">
                  <img src="assets/images/proof-3.webp" class="img-fluid mx-auto d-block">
               </div>
               <div class="col-md-6 col-sm-6 col-12 mt20 mt-md40">
                  <img src="assets/images/proof-4.webp" class="img-fluid mx-auto d-block">
               </div>
            </div>

            <div class="row mt-md70 mt30 align-items-center">
               <div class="col-md-10 offset-md-1 f-md-32 f-22 w600 text-center">
                  Got 20K+ Visitors in Just 15 Days on My Affiliate Offers Using the Power of Emails  
               </div>
               <div class="col-md-10 offset-md-1 mt-md30 mt20">
                  <img src="assets/images/proof1.webp" alt="" class="img-fluid d-block mx-auto">
               </div>
            </div>
            <div class="row mt-md70 mt30 align-items-center">
               <div class="col-md-6 f-md-32 f-22 w600 order-md-2 text-center text-md-start">
                  And Let’s Check Out the Crazy Open And Click Rates 
                  We’ve Got for A Simple Email I Sent Using AutomailX.
               </div>
               <div class="col-md-6 mt-md0 mt20 order-md-1">
                  <img src="assets/images/proof2.webp" alt="" class="img-fluid d-block mx-auto">
               </div>
            </div>
         </div>
      </div>
      <!-- Proof Section End --> 
     
     
      <!-- Cta Section -->
      <div class="cta-section-new">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <!-- <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/limited-time-offer-banner.webp" class="img-fluid d-block mx-auto"> -->
                  <div class="f-22 f-md-34 w500 lh140 text-center white-clr">
                           Grab AutomailX Now for <strike>$99/Month,</strike> Today Only 1-Time $17
                           </div>
                  <div class="f-18 f-md-20 w400 lh140 text-center white-clr mt20">
                     (Get Free 30 Reseller License + FREE Commercial License)
                  </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 text-center">
                        <a href="#buynow" class="cta-link-btn">Get Instant Access To AutomailX</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20">
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/compaitable-with1.webp" class="img-fluid d-block mx-xs-center md-img-right">
                     <div class="d-md-block d-none px-md30"><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/v-line.webp" class="img-fluid"></div>
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/days-gurantee1.webp" class="img-fluid d-block mx-xs-auto mt15 mt-md0">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="warning">
         <div class="container">
            <div class="row text-center">
                 <div class="col-md-12 d-flex align-items-center justify-content-center flex-wrap text-center">
                    <img src="assets/images/warning.png" class="img-fluid d-block ml-auto mr10">
                    <div class="f-22 f-md-28 w400 lh140 text-center white-clr ">
                  <span class="w700">WARNING:</span>   Grab The Offer Before Timer Hits Zero</div>
                    </div>
                </div>
            </div>
        </div>
      <!-- <div class="unstoppable-section">
         <div class="container">
            <div class="row">
               <div class=" col-12 text-center mx-auto">
                  <div class="f-22 f-md-55 w700 lh140 text-center white-clr unstoppable ">
                     ChatGPT is UNSTOPPABLE 
                  </div>
                  <div class="f-28 f-md-45 w500 lh140 white-clr mt20 mt-md30">
                  And It’s Bigger After Acquisition & Investment of 10 Billion Dollars By Microsoft 
                  </div>
                  <div class="mt20 mt-md30 chatgpt-head-design">
                     <div class="f-32 f-md-42 w700 black-clr lh140">
                     ChatGPT Is The Game Changer in 2023 & Beyond
                     </div>
                  </div>
                  <div class="f-28 f-md-45 w700 lh160 mt20 mt-md30 white-clr">
                  <span class="w400 white-clr">And It Instantly Became The </span> <span class="blue-gradient1 under">Undisputed Ruler Of The Digital World... </span> 
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md50 align-items-center">
               <div class="col-12 col-md-7">
                  <div class="f-24 f-md-32 w600 white-clr lh140">
                 It solves all your tedious writing & designing problem like you have now as 
                  </div>
                  <ul class="problem-list pl0 m0 f-20 f-md-22 lh150 w500 white-clr mt20 mt-md30">
                     <li>	You want to write content for Emails</li>
                     <li>	Want to translate Language.</li>
                     <li>	You want to generate graphics & images.</li>
                     <li>	You want to create videos</li>
                     <li>	Want to write high quality content for Websites & blogs</li>
                     <li>	Want to write marketing material</li>
                  </ul>
               </div>
               <div class="col-12 col-md-5 relative mt20 mt-md0">
                  <img src="assets/images/game-changer-robot.webp" alt="Robot Question" class="img-fluid mx-auto d-block robot-ques">
               </div>
            </div>
         </div>
      </div> -->

<!--research section starts-->
<div class="research-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="reason-box">
                     <div class="f-28 f-md-45 w700 lh140 black-clr">
                        MONEY IS IN THE LIST
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50">
                  <div class="row align-items-center">
                     <div class="col-12 col-md-6">
                        <div class="f-20 f-md-24 w500 lh140 text-center text-md-start">
                        Yes, possibly you heard that, <span class="w700">MONEY IS IN THE LIST!</span><br>

                        And, to take out your BIG share out of it,
                        you don’t even need to create any product or offer a service.<br>

                        You can simply tap into NO LIMIT Multi-Billion Dollar Affiliate
                        Marketing Industry without any product, services or any headache that has no ends so,
                        </div>
                     </div>
                     <div class="col-12 col-md-6 mt20 mt-md0">
                        <img src="assets/images/investment.webp" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md70">
                  <div class="row">
                     <div class="col-12 col-md-7 order-md-2">
                        <div class="f-28 f-md-45 w700 lh140 text-center text-md-start green-clr">
                           That’s Right…
                        </div>
                        <div class="f-20 f-md-70 w700 lh140 text-center text-md-start mt10 caveat blue-gradient">
                        SKY IS THE LIMIT!
                        </div>
                     </div>
                     <div class="col-12 col-md-5 order-md-1 mt20 mt-md0 relative d-none d-md-block">
                        <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/girl.webp" class="img-fluid d-block mx-auto girl-img">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      
      <section class="still-think-section">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 px-md30">
                  <div class="f-28 f-md-40 lh140 w700 text-center white-clr">
                  <span class="orange-clr">Here’re some real-</span>life examples of people who are literally exploiting affiliate marketing to make a fortune every year
                  </div>
                  
                 
               </div>
            </div>
            <div class="row">
               <div class="col-12 col-md-12 mt20 mt-md150">
                  <div class="still-think-shape">
                     <div class="row">
                        <div class="col-md-4 col-12">
                           <img src="assets/images/pat-flynn.webp" class="img-fluid mx-auto d-block still-section-image">
                        </div>
                        <div class="col-md-8 col-12">
                           <div class="f-24 f-md-32 lh140 w400 white-clr mt20 mt-md20">
                           Pat Flynn - Made <span class="neon-clr w700">$53,000+ Last Month</span>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-12 col-md-12 mt20 mt-md200">
                  <div class="still-think-shape1">
                     <div class="row">
                        <div class="col-md-4 col-12 order-md-2">
                           <img src="assets/images/john-chow.webp" class="img-fluid mx-auto d-block still-section-image1">
                        </div>
                        <div class="col-md-8 col-12 order-md-1 ">
                           <div class="f-24 f-md-32 lh140 w400 white-clr mt20 mt-md0">
                           John Chow - Makes  <span class="neon-clr w700">$40,000 to $50,000 Every month</span>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-12 col-md-12 mt20 mt-md200">
                  <div class="still-think-shape">
                     <div class="row">
                        <div class="col-md-4 col-12">
                           <img src="assets/images/jeremy.webp" class="img-fluid mx-auto d-block still-section-image">
                        </div>
                        <div class="col-md-8 col-12">
                           <div class="f-24 f-md-32 lh140 w400 white-clr mt20 mt-md0">
                           Jeremy Schoemaker - Estimated earnings <span class="neon-clr w700">in 5 Years: $2,000,000 to $4,000,000</span>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-12 col-md-12 mt20 mt-md200">
                  <div class="still-think-shape1">
                     <div class="row">
                        <div class="col-md-3 col-12 order-md-2">
                           <img src="assets/images/darren.webp" class="img-fluid mx-auto d-block still-section-image1">
                        </div>
                        <div class="col-md-9 col-12 order-md-1 px-md0">
                           <div class="f-24 f-md-32 lh140 w400 white-clr mt20 mt-md0">
                           Darren Rowse - Estimated Earnings:  <span class="neon-clr w700">$40,000 to $45,000 per month</span>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-12 col-md-12 mt20 mt-md200">
                  <div class="still-think-shape">
                     <div class="row">
                        <div class="col-md-4 col-12">
                           <img src="assets/images/finch.webp" class="img-fluid mx-auto d-block still-section-image">
                        </div>
                        <div class="col-md-8 col-12">
                           <div class="f-24 f-md-32 lh140 w400 white-clr mt20 mt-md0">
                           Finch Sells - Estimated Earnings: <span class="neon-clr w700">$15,000 to $25,000</span>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-12 col-md-12 mt20 mt-md200">
                  <div class="still-think-shape1">
                     <div class="row">
                        <div class="col-md-3 col-12 order-md-2">
                           <img src="assets/images/rafael.webp" class="img-fluid mx-auto d-block still-section-image1">
                        </div>
                        <div class="col-md-9 col-12 order-md-1 px-md0">
                           <div class="f-24 f-md-32 lh140 w400 white-clr mt20 mt-md0">
                           Rafael Zelik - Estimated Earnings: <span class="neon-clr w700">$40,000 to $50,000 approximately</span>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-12 col-md-12 mt20 mt-md200">
                  <div class="still-think-shape">
                     <div class="row">
                        <div class="col-md-4 col-12">
                           <img src="assets/images/missy.webp" class="img-fluid mx-auto d-block still-section-image">
                        </div>
                        <div class="col-md-8 col-12">
                           <div class="f-24 f-md-32 lh140 w400 white-clr mt20 mt-md0">
                           Missy Ward - Estimated Earnings: <span class="neon-clr w700">$500,000 per Year</span> approx
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-12 col-md-12 mt20 mt-md200">
                  <div class="still-think-shape1">
                     <div class="row">
                        <div class="col-md-3 col-12 order-md-2">
                           <img src="assets/images/lorenzo.webp" class="img-fluid mx-auto d-block still-section-image1">
                        </div>
                        <div class="col-md-9 col-12 order-md-1 px-md0">
                           <div class="f-24 f-md-32 lh140 w400 white-clr mt20 mt-md0">
                           Lorenzo of Mr. Green - Estimated Earnings:  <span class="neon-clr w700">$10,000 to $15,000</span>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-md-12 col-12 mt20 mt-md150">
               <div class="f-24 f-md-32 lh140 w400 text-center white-clr">
                  <span class="w700">And with email marketing,</span> you can promote lots of affiliate products in any niche in any country those pay healthy recurring commissions for life. Tons of such great products from top vendors are available to promote on:
               </div>
               <img src="assets/images/logo-images.webp" class="img-fluid mx-auto d-block mt20 mt-md50">
            </div>
         </div>
      </section>

      <div class="simple-section">
         <div class="container">
            <div class="row">
               <div class="col-md-11 mx-auto col-12 text-center">
                  <div class="f-28 f-md-42 lh140 w700 text-center black-clr">
                  Emails Are Everywhere - It Brings More Traffic, More Sales And Build Better Customer Relation…
                  </div>
                  <div class="f-22 f-md-28 lh140 w700 text-center black-clr mt20 mt-md30 its-simple">
                     It’s POWERFUL -
                  </div>
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md40">
               <div class="col-md-5 col-sm-5 col-12">
                  <img src="assets/images/ems1.webp" class="img-fluid mx-auto d-block">
               </div>
               <div class="col-md-7 col-sm-6 col-12">
                  <div class="alone-bg f-22 f-md-32 lh140 w400 black-clr">
                        <span class="w700"> Email Marketing is 3 times more POWERFUL</span> than social media
                  </div>
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md40">
               <div class="col-md-5 col-sm-5 col-12 order-md-2">
                  <img src="assets/images/ems2.webp" class="img-fluid mx-auto d-block">
               </div>
               <div class="col-md-7 col-sm-6 col-12 order-md-1">
                  <div class="alone-bg f-22 f-md-32 lh140 w400 black-clr">
                        <span class="w700"> Traffic from email converts better</span> than any cold ads, search or social traffic
                  </div>
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md40">
               <div class="col-md-5 col-sm-5 col-12">
                  <img src="assets/images/ems3.webp" class="img-fluid mx-auto d-block">
               </div>
               <div class="col-md-7 col-sm-6 col-12">
                  <div class="alone-bg f-22 f-md-32 lh140 w400 black-clr">
                        <span class="w700"> Google keeps changing their algorithm</span> and make us lose rankings and traffic!
                  </div>
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md40">
               <div class="col-md-5 col-sm-5 col-12 order-md-2">
                  <img src="assets/images/ems4.webp" class="img-fluid mx-auto d-block">
               </div>
               <div class="col-md-7 col-sm-6 col-12 order-md-1">
                  <div class="alone-bg f-22 f-md-32 lh140 w400 black-clr">
                        <span class="w700"> Social Networks like Facebook, Instagram bans</span> your account and don’t let you promote affiliate offers!
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-md-12 col-12">
                  <div class="f-24 f-md-30 lh140 text-center black-clr mt20 mt-md50">
                     <span class="w700">More Importantly,</span> with email marketing, you can promote various offers to the same subscribers without any extra costs and enjoy tons of sales & commissions at will.
                  </div>
               </div>
            </div>
         </div>
      </div>

      <div class="stats-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-45 w700 lh140 text-center white-clr stats-headline">If You’re Not Using Emails, You’re <br class="d-none d-md-block">Actually Losing Money! </div>
               </div>
            </div>
            <div class="row row-cols-1 row-cols-md-2 gap30 mt0 t-md20">
               <div class="col">
                  <div class="stats-block">
                     <div><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/mail.webp" class="img-fluid d-block mx-auto"></div>
                     <div class="f-20 f-md-24 w400 lh140 white-clr">There are <span class="orange-clr w700">4.5+ Billion</span> Daily Email Users</div>
                  </div>
               </div>
               <div class="col">
                  <div class="stats-block">
                     <div><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/mail.webp" class="img-fluid d-block mx-auto"></div>
                     <div class="f-20 f-md-24 w400 lh140 white-clr"><span class="orange-clr w700">350+ Billion Emails</span> are Circulated
                        Every Single Day
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="stats-block">
                     <div><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/mail.webp" class="img-fluid d-block mx-auto"></div>
                     <div class="f-20 f-md-24 w400 lh140 white-clr"><span class="orange-clr w700">$400 Million</span> are Spent Every Year
                        on Email Marketing in US Alone
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="stats-block">
                     <div><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/mail.webp" class="img-fluid d-block mx-auto"></div>
                     <div class="f-20 f-md-24 w400 lh140 white-clr"><span class="orange-clr w700">95% of Email</span> Users Check
                        Their Emails Daily
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="stats-block">
                     <div><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/mail.webp" class="img-fluid d-block mx-auto"></div>
                     <div class="f-20 f-md-24 w400 lh140 white-clr"><span class="orange-clr w700">85% of Businesses</span> Believe that
                        Email Marketing Improves
                        Customer Retention
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="stats-block">
                     <div><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/mail.webp" class="img-fluid d-block mx-auto"></div>
                     <div class="f-20 f-md-24 w400 lh140 white-clr"><span class="orange-clr w700">80% of Youth</span> Prefer Business
                        Communications via Email
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="stats-block">
                     <div><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/mail.webp" class="img-fluid d-block mx-auto"></div>
                     <div class="f-20 f-md-24 w400 lh140 white-clr"><span class="orange-clr w700">90% of Marketers</span> Use Email
                        as the Primary Channel
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="stats-block">
                     <div><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/mail.webp" class="img-fluid d-block mx-auto"></div>
                     <div class="f-20 f-md-24 w400 lh140 white-clr"><span class="orange-clr w700">Since 10+ Years,</span> Email Marketing
                        Generates the Highest ROI for
                        Marketers.
                     </div>
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md80">
               <div class="col-12 f-24 f-md-36 w600 white-clr lh140 text-center">  This Is Why 1000s Of Small & Big Businesses/Marketers  
               <br class="d-none d-md-block"> Are Capitalizing On Email Marketing
               </div>
            </div>
         </div>
      </div>
<!--research section ends-->

      <!-- Unique Twist Section -->
      <!-- <section class="unique-sec"> 
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-45 w800 lh140 white-clr">
                     Here’s The Unique TWIST Comes
                  </div>
                  <div class="mt10">
                     <img src="assets/images/hline.webp" alt="" class="mx-auto d-block img-fluid">
                  </div>
                  <div class="f-20 f-md-24 w500 lh140 white-clr mt20 mt-md50">
                  Now we all know that ChatGPT is the future of online world & emails are the most <br class="d-none d-md-block"> powerful and low-cost marketing method. 
                  </div>
                  <div class="f-20 f-md-28 w500 lh140 white-clr mt20 mt-md50">
                  When we combine these tow two powerful tools, it brings tons of Traffic,<br class="d-none d-md-block"> commission, and sales on 100% autopilot mode. 
                  </div>
                  <div class="mt20 mt-md30 lh140 w500 white-clr f-20 f-md-24 w500 unique-text">
                    So Here we comes with
                  </div>
                  <div class="col-md-10 mx-auto col-12 combile-automailx Ai mt20 mt-md70">
                     <img src="assets/images/automailx.webp" alt="AutomailX" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-24 f-md-38 w500 lh140 white-clr">
                   With AutomailX, You Need Just One Keyword To Write, Design, And Send Profitable Emails for...
                  </div>
                  <div class="mt20 mt-md30">
                     <img src="assets/images/arrow.png" class="img-fluid mx-auto d-block downarrow">
                  </div>
               </div>
            </div>
            <div class="row row-cols-1 row-cols-md-3 gap30  justify-content-center mt0 mt-md50">
               <div class="col">
                  <img src="assets/images/u1.webp" class="img-fluid mx-auto d-block">
               </div>
               <div class="col">
                  <img src="assets/images/u2.webp" class="img-fluid mx-auto d-block">
               </div>
               <div class="col">
                  <img src="assets/images/u3.webp" class="img-fluid mx-auto d-block">
               </div>
               <div class="col">
                  <img src="assets/images/u4.webp" class="img-fluid mx-auto d-block">
               </div>
               <div class="col">
                  <img src="assets/images/u5.webp" class="img-fluid mx-auto d-block">
               </div>
               <div class="col">
                  <img src="assets/images/u6.webp" class="img-fluid mx-auto d-block">
               </div>
               <div class="col">
                  <img src="assets/images/u7.webp" class="img-fluid mx-auto d-block">
               </div>
               <div class="col">
                  <img src="assets/images/u8.webp" class="img-fluid mx-auto d-block">    
               </div>
               <div class="col">
                  <img src="assets/images/u9.webp" class="img-fluid mx-auto d-block">
               </div>
              
            </div>
         </div>
      </section> -->

      <!-- Cta Section Start -->
      <div class="cta-section-new">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <!-- <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/limited-time-offer-banner.webp" class="img-fluid d-block mx-auto"> -->
                 <div class="f-22 f-md-34 w500 lh140 text-center white-clr">
                           Grab AutomailX Now for <strike>$99/Month,</strike> Today Only 1-Time $17
                           </div>
                  <div class="f-18 f-md-20 w400 lh140 text-center white-clr mt20">
                     (Get Free 30 Reseller License + FREE Commercial License)
                  </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 text-center">
                        <a href="#buynow" class="cta-link-btn">Get Instant Access To AutomailX</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20">
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/compaitable-with1.webp" class="img-fluid d-block mx-xs-center md-img-right">
                     <div class="d-md-block d-none px-md30"><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/v-line.webp" class="img-fluid"></div>
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/days-gurantee1.webp" class="img-fluid d-block mx-xs-auto mt15 mt-md0">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Cta Section End -->

      <div class="warning">
         <div class="container">
            <div class="row text-center">
                 <div class="col-md-12 d-flex align-items-center justify-content-center flex-wrap text-center">
                    <img src="assets/images/warning.png" class="img-fluid d-block ml-auto mr10">
                    <div class="f-22 f-md-28 w400 lh140 text-center white-clr ">
                  <span class="w700">WARNING:</span>   Grab The Offer Before Timer Hits Zero</div>
                    </div>
                </div>
            </div>
        </div>

      <!-- <div class="power-section">
         <div class="container ">
            <div class="row align-items-center">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-43 w700 lh140 black-clr">
                     And Do You Know How Much People Are Making <br>
                     <span class="blue-gradient1">Using the POWER of Emails Marketing?</span>
                  </div>
                  <img src="assets/images/let-line.webp" class="img-fluid mx-auto">
               </div>
               <div class="col-12 col-md-11 offset-md-1 mt20 mt-md50">
                  <div class="power-block">
                     <div class="f-28 f-md-36 w700 orange-clr lh140 text-center text-md-start">Tom Sather</div>
                     <div class="f-md-20 f-18 w400 lh140 white-clr mt10 text-center text-md-start">
                        He is known for his work as the Director of Return Path which has an Annual Revenue of $10+ Million. Return Path, with emphasis on Emails Marketing, offers unparalleled insights on why emails end up undeliverable and where they end up bouncing.
                     </div>
                     <img src="assets/images/tom.webp" class="img-fluid d-block mx-auto left-img mt20 mt-md0">
                  </div>
               </div>
               <div class="col-12">
                  <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/connect.webp" class="img-fluid d-block mx-auto connect-line">
               </div>
               <div class="col-12 col-md-11 mt20">
                  <div class="power-block right">
                     <div class="f-28 f-md-36 w700 orange-clr lh140 text-center text-md-start">Matthew Lloyd Smith</div>
                     <div class="f-md-20 f-18 w400 lh140 white-clr mt10 text-center text-md-start">Matthew Lloyd Smith created the insanely helpful website Really Good Emails, which showcases the best in email marketing on the internet. Really Good Emails has a Revenue of over $5 Million.
                     </div>
                     <img src="assets/images/matthew.webp" class="img-fluid d-block mx-auto right-img mt20 mt-md0">
                  </div>
               </div>
               <div class="col-12">
                  <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/connect1.webp" class="img-fluid d-block mx-auto connect-line">
               </div>
               <div class="col-12 col-md-11 offset-md-1 mt20">
                  <div class="power-block">
                     <div class="f-28 f-md-36 w700 orange-clr lh140 text-center text-md-start">Kath Pay</div>
                     <div class="f-md-20 f-18 w400 lh140 white-clr mt10 text-center text-md-start">
                        Kath Pay has been in the email game for over 19 years. She is an email marketing consultant and founder of Holistic Email Marketing. She’s been named one of the top email influencers in the world and makes annual revenue of over $8 Million.
                     </div>
                     <img src="assets/images/kath.webp" class="img-fluid d-block mx-auto left-img mt20 mt-md0">
                  </div>
               </div>
               <div class="col-12 mt20 mt-md100">
                  <div class="f-20 f-md-32 w400 lh140 text-center">And all of that just by using the Power of ChatGPT Ai Powered Email Marketing Solution </div>
                  <div class="f-32 f-md-55 w700 lh140 text-center green-clr my20">Impressive, Right?</div>
                  <div class="f-20 f-md-32 w400 lh140 text-center">So, it can be safely stated that…<br>
                     <span class="w600">Chat GPT AI + Email Marketing Together is the BIGGEST Income Opportunity Right Now!</span>
                  </div>
               </div>
            </div>
         </div>
      </div>  -->

      <section class="still-think">
         <div class="container">
            <div class="row">
                 <div class="f-24 f-md-28 w400 lh140 black-clr text-center">
                     If Emails Are So Great, Why Not Everyone Is Using It & Reaping the Benefits?
                  </div>
               <div class="col-12 text-center">
                  <div class="f-28 f-md-45 w700 lh140 black-clr mt20">
                     Because Expensive 3rd Party Apps are Charging Monthly & Sucking Your Profits 
                  </div>
                  <div class="mt10">
                     <img src="assets/images/hline.webp" alt="" class="mx-auto d-block img-fluid">
                  </div>
                 
                  <div class="f-24 f-md-28 w700 lh140 white-clr still-headline mt20 mt-md50">
                     Content Writing Platform 
                  </div>
               </div>
            </div>
            <div class="row row-cols-1 row-cols-md-3 gap30  justify-content-center mt20 mt-md20">
               <div class="col">
                  <img src="assets/images/email1.webp" class="img-fluid mx-auto d-block">
               </div>
               <div class="col">
                  <img src="assets/images/email2.webp" class="img-fluid mx-auto d-block">
               </div>
               <div class="col">
                  <img src="assets/images/email3.webp" class="img-fluid mx-auto d-block">
               </div>
            </div>
            <div class="row mt20 mt-md80">
               <div class="col-12 text-center">
                  <div class="f-24 f-md-28 w700 lh140 white-clr still-headline">
                  Email Marketing Platform 
                  </div>
               </div>
            </div>
            <div class="row row-cols-1 row-cols-md-3 gap30  justify-content-center mt20 mt-md50">
               <div class="col">
                  <img src="assets/images/email-marketing1.webp" class="img-fluid mx-auto d-block">
               </div>
               <div class="col">
                  <img src="assets/images/email-marketing2.webp" class="img-fluid mx-auto d-block">
               </div>
               <div class="col">
                  <img src="assets/images/email-marketing3.webp" class="img-fluid mx-auto d-block">
               </div>
               <div class="col">
                  <img src="assets/images/email-marketing4.webp" class="img-fluid mx-auto d-block">
               </div>
               <div class="col">
                  <img src="assets/images/email-marketing5.webp" class="img-fluid mx-auto d-block">
               </div>
               <div class="col">
                  <img src="assets/images/email-marketing6.webp" class="img-fluid mx-auto d-block">
               </div>
            </div>
            <div class="row mt20 mt-md80">
               <div class="col-12 text-center">
                  <div class="f-24 f-md-28 w700 lh140 white-clr still-headline">
                     SMTP Provider Platforms
                  </div>
               </div>
            </div>
            <div class="row row-cols-1 row-cols-md-3 gap30  justify-content-center mt20 mt-md50">
               <div class="col">
                  <img src="assets/images/smtp1.webp" class="img-fluid mx-auto d-block">
               </div>
               <div class="col">
                  <img src="assets/images/smtp2.webp" class="img-fluid mx-auto d-block">
               </div>
               <div class="col">
                  <img src="assets/images/smtp3.webp" class="img-fluid mx-auto d-block">
               </div>
               <div class="col">
                  <img src="assets/images/smtp4.webp" class="img-fluid mx-auto d-block">
               </div>
               <div class="col">
                  <img src="assets/images/smtp5.webp" class="img-fluid mx-auto d-block">
               </div>
               <div class="col">
                  <img src="assets/images/smtp6.webp" class="img-fluid mx-auto d-block">
               </div>
            </div>
         </div>
      </section>


      <section class="problem-app">
         <div class="container">
            <div class="row">
               <div class="col-md-11 mx-auto col-12 px-md30 text-center">
                  <div class="f-25 f-md-38 lh140 w700 text-center white-clr red-brush emojii-icon" style="position:relative;">
                  And Problems with Those Apps Keep Going…
                  </div>
               </div>
               <div class="col-md-12 col-12 mt20 mt-md50">
                  <div class="problem-shape f-16 f-md-20 lh140 w600 white-clr">
                  You Need To Write Email Manually Which Is Time Consuming. And If You Don’t Even Have Good Writing Skills, It's A BIG Headache.
                  </div>
                  <div class="problem-shape f-16 f-md-20 lh140 w600 white-clr mt20 mt-md30">
                  <div class="mt0 mt-md10 mb-md10">They Have Access To Your Valuable List, So Data Leak Is Also A BIG Risk.</div>
                  </div>
                  <div class="problem-shape f-16 f-md-20 lh140 w600 white-clr mt20 mt-md30">
                  It’s Almost Impossible To Import Your Subscriber List And If You Succeed, You Still May Lose <br class="d-none d-md-block"> 20-30% Of Your Leads.
                  </div>
                  <div class="problem-shape f-16 f-md-20 lh140 w600 white-clr mt20 mt-md30">
                  Constant Downtimes, Spam Complaints, Bounces, And Unexplained Delays Due To Someone Else’s Mistake.
                  </div>
                  <div class="problem-shape f-16 f-md-20 lh140 w600 white-clr mt20 mt-md30">
                  That’s Not All. They Can Shut Down Your Account Anytime Without Any Prior Notice & You Can Lose Your Entire Business Overnight.
                  </div>
                  <div class="f-24 f-md-36 lh140 w700 text-center neon-clr mt20 mt-md50">
                  Even after that, there is still no guarantee that you’ll get the desired ROI from your campaigns as you expected.
                  </div>
               </div>
            </div>
         </div>
      </section>

      <!-- <div class="problem-bg-section1">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-12 col-md-6 order-md-2">
                  <div class="f-24 f-md-38 w600 blue-gradient lh100">Let me tell you my story: 
                  <img src="assets/images/let-line.webp" class="img-fluid"></div>
                  <div class="f-18 w400 lh160 mt20 black-clr">
                     3 years back, I was searching for a better solution for my email marketing.  
                     So, after doing my research and numerous brainstorming sessions,  
                     I decided to choose MailChimp for sending emails for boosting sales and profits. 
                     <br><br>
                     Things were going quite smooth, my emails were getting opened and click-through  
                     rates were also decent enough. So, I felt like a king, and my dreams clouds started  
                     showering rains of overnight fame and success.
                  </div>
               </div>
               <div class="col-12 col-md-5 mt20 mt-md0 order-md-1">
                  <img src="assets/images/problem1.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>
            <div class="row mt20 mt-md50">
               <div class="col-12">
                  <div class="f-20 f-md-28 w500 text-center white-clr">But destiny had something else in the store, and one morning while sipping my daily cappuccino, my eyes were split wide open to read this horrendous email- </div>
               </div>
            </div>
         </div>
      </div>

      <div class="problem-bg-section2">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-12">
                  <img src="assets/images/suspended.webp" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 f-18 f-md-20 w400 lh160 mt20 mt-md50 white-clr mb20 mb-md50">
                  These were literally the first words that echoed in my mind after I checked that mail. After all, I didn’t do anything wrong or unethical. I wasn’t spamming, nor was I sending fishy emails to my subscribers. <br><br>
                  I felt completely done and dusted. I couldn’t do anything as I had ZERO control and had to rely on a 3rd party for sending mails and earning my daily bread. 
               </div>
               <div class="col-12 col-md-5 order-md-2">
                  <img src="assets/images/problem2.webp" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 col-md-7 order-md-1 mt20 mt-md0">
                  <div class="f-18 w400 lh160 white-clr">
                     I gathered some courage and got in touch with their support team. And you’ll be
                     amazed to know that I couldn’t get a justifiable and believable reply from their 
                     end, and ignored any contact attempts that I made after that. They kept playing 
                     with me like a football, kicking me mercilessly from one corner to another.
                     <br><br>
                     I am still amazed why it happened. It sounds as difficult as a Greek puzzle.
                     <br><br>
                     My business was dying a slow, painful death, and I wasn't giving my 
                     subscribers what they needed... they were not getting due worth for 
                     their faith in me.
                     <br><br>
                     If you’ve already gone through all this, there’s no doubt you’ve experienced 
                     frustration and the loss of profits. And so, all that came to my mind is that 
                     I had to prepare for a hard and bumpy ride.
                  </div>
               </div>
            </div>
         </div>
      </div> -->

      <div class="problem-bg-section3">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-12 text-center">
                  <div class="f-24 f-md-38 w600 black-clr shape-headline lh140">
                     I NEED FULLControl Over My Online Business
                  </div>
               </div>
               <div class="col-12 f-18 f-md-20  w400 lh160 mt20 mt-md50 black-clr">
                  That’s it, I told myself. I decided from now on, I won’t put my faith in these money-sucking service providers that are sitting with a big crocodile’s mouth open to eat you once and for all.<br><br>
                  So, I put everything else aside, got my technical team into a huddle, and decided that we will create a cloud-based email marketing platform that gives better and trackable results time and time again.<br><br>
                  After lots of hard work and burning my midnight lamps for countless nights, I came up with this magnificent feature-packed Email marketing system that gives me complete freedom of work &amp; control over my business.
               </div>
            </div>
         </div>
      </div>

      <!--Proudly Introducing Start -->
     <div class="next-gen-sec" id="product">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="proudly-sec f-26 w700 text-center white-clr">
                     Proudly <br> Presenting...
                  </div>
               </div>
               <div class="col-12 mt-md20 mt20  mt-md50 text-center">
                  <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 1164.92 192.13" style="max-height:55px"><defs><style>.cls-1,.cls-2,.cls-3,.cls-4,.cls-5{fill-rule:evenodd;}.cls-1{fill:url(#linear-gradient);}.cls-2{fill:url(#linear-gradient-2);}.cls-3{fill:url(#linear-gradient-3);}.cls-4{fill:url(#linear-gradient-4);}.cls-5{fill:url(#linear-gradient-5);}.cls-6{fill:#fff;}.cls-7{fill:url(#linear-gradient-6);}</style><linearGradient id="linear-gradient" x1="82.05" y1="127.34" x2="150.42" y2="127.34" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#624ef2"/><stop offset="1" stop-color="#1e9af7"/></linearGradient><linearGradient id="linear-gradient-2" x1="7.14" y1="79.25" x2="123.43" y2="79.25" xlink:href="#linear-gradient"/><linearGradient id="linear-gradient-3" x1="0" y1="116.36" x2="45.71" y2="116.36" xlink:href="#linear-gradient"/><linearGradient id="linear-gradient-4" x1="3.94" y1="157.47" x2="142.12" y2="157.47" xlink:href="#linear-gradient"/><linearGradient id="linear-gradient-5" x1="3.58" y1="75.02" x2="182.4" y2="75.02" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#ff2d2d"/><stop offset="1" stop-color="#ffbc00"/></linearGradient><linearGradient id="linear-gradient-6" x1="971.74" y1="90.91" x2="1164.92" y2="90.91" xlink:href="#linear-gradient"/></defs><title>AutomailX White</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1" d="M150.42,67.57V177.2a15.41,15.41,0,0,1-3.38,9.92q-20.67-19.74-39-37.28l-26-24.94Z"/><path class="cls-2" d="M123.43,48.09l-62.5,62.32Q47.16,98.7,33.43,86.92q-8.17-7-16.31-14.11c-3.34-2.91-6.75-5.85-10-8.88a6.08,6.08,0,0,1,1.32-.33l24.25-3.27,28.14-3.8,33-4.45Z"/><path class="cls-3" d="M45.71,119.33,25.4,139.11Q15.2,149,5.81,158.19c-2.33,3-5.83,2.65-5.81-2.51V76.9c0-5.16,3.5-5.32,5.81-2.2l.39.41h0c.73,1.66,1.4,3.32,2,4.81l.15.38Z"/><path class="cls-4" d="M142.12,190.87a14.7,14.7,0,0,1-8.88,1l-39.42-7.34-33-6.14-28.14-5.24L8.44,168.61a8.2,8.2,0,0,1-4.5-2.68L25.82,145q11.1-10.61,23.23-22.21l15.81,16.51L78,128.3q12.35,12.06,25.78,25.16Q121.84,171.09,142.12,190.87Z"/><path class="cls-5" d="M10.15,79.16,65,136.41l92.87-81.5,3.93,12.24L182.4,13.62,128.57,32.19l11.14,5.9L61.05,116.53s-54.7-47.67-57.47-50c1.16,1.75,6.57,12.68,6.57,12.68Z"/><path class="cls-6" d="M283,50.49H247.2L201.86,181.82h29.75l9.74-29.95h47.42l9.73,29.95h29.75ZM248.4,130.2l16.17-49.7h1l16.14,49.7Z"/><path class="cls-6" d="M406.68,139.88V83.32H434v98.5H407.77V163.93h-1a29.1,29.1,0,0,1-11.06,13.91q-7.73,5.26-18.82,5.26a33.23,33.23,0,0,1-17.38-4.49,30.61,30.61,0,0,1-11.7-12.76q-4.2-8.26-4.27-19.81V83.32h27.32v57.84q.06,8.73,4.68,13.79T387.89,160a19.37,19.37,0,0,0,9.23-2.27,17.53,17.53,0,0,0,7-6.77A21,21,0,0,0,406.68,139.88Z"/><path class="cls-6" d="M510,180.6q-2.06.65-5.78,1.51a48.09,48.09,0,0,1-9,1.06,41.39,41.39,0,0,1-17.28-2.63,24.12,24.12,0,0,1-11.51-9.36q-4.11-6.36-4-16v-51.3H448.87V83.32h13.47V59.72h27.32v23.6h18.53v20.52H489.66v47.71a12.24,12.24,0,0,0,1.15,5.87,6.34,6.34,0,0,0,3.24,2.92,13.08,13.08,0,0,0,4.84.83,21.79,21.79,0,0,0,3.85-.35l2.95-.55Z"/><path class="cls-6" d="M569.43,183.74q-14.94,0-25.81-6.38a43,43,0,0,1-16.77-17.83Q521,148.09,521,133t5.9-26.71a43,43,0,0,1,16.77-17.82Q554.5,82,569.43,82t25.81,6.38A43,43,0,0,1,612,106.24q5.91,11.46,5.9,26.71T612,159.53a43,43,0,0,1-16.77,17.83Q584.38,183.75,569.43,183.74Zm.13-21.16a16.88,16.88,0,0,0,11.35-3.88,23.82,23.82,0,0,0,6.89-10.61,46.31,46.31,0,0,0,2.35-15.33,46.31,46.31,0,0,0-2.35-15.33,24.09,24.09,0,0,0-6.89-10.64,16.8,16.8,0,0,0-11.35-3.91,17.26,17.26,0,0,0-11.51,3.91,23.74,23.74,0,0,0-7,10.64,46.58,46.58,0,0,0-2.34,15.33,46.58,46.58,0,0,0,2.34,15.33,23.48,23.48,0,0,0,7,10.61A17.35,17.35,0,0,0,569.56,162.58Z"/><path class="cls-6" d="M640.74,50.49h19l44.63,109h1.54l44.64-109h19V181.82H754.63V82h-1.28l-41,99.78H697.94L656.9,82h-1.28v99.78H640.74Z"/><path class="cls-6" d="M828.25,184.13a39.77,39.77,0,0,1-17-3.56,28.82,28.82,0,0,1-12.12-10.33q-4.49-6.77-4.49-16.38,0-8.46,3.34-13.76a23.63,23.63,0,0,1,8.91-8.3,47.46,47.46,0,0,1,12.35-4.52q6.76-1.52,13.62-2.41,9-1.16,14.59-1.76t8.21-2.08q2.6-1.48,2.6-5.13v-.52q0-9.48-5.16-14.75t-15.62-5.25q-10.85,0-17,4.74a28,28,0,0,0-8.66,10.13l-14.37-5.13a34.69,34.69,0,0,1,10.3-14,38.56,38.56,0,0,1,14.11-7A58.86,58.86,0,0,1,837,82a61.89,61.89,0,0,1,10.93,1.12,35.92,35.92,0,0,1,12,4.58,26.84,26.84,0,0,1,9.65,10.46q3.86,7,3.85,18.72v64.9H858.26V168.48h-.77a26.84,26.84,0,0,1-5.13,6.86,29.6,29.6,0,0,1-9.56,6.22A36.78,36.78,0,0,1,828.25,184.13Zm2.3-13.6q9,0,15.17-3.53a24.19,24.19,0,0,0,9.36-9.1,23.36,23.36,0,0,0,3.18-11.74V132.31q-1,1.16-4.2,2.09a64,64,0,0,1-7.44,1.6q-4.2.67-8.15,1.15c-2.63.32-4.75.59-6.38.81a58.57,58.57,0,0,0-11,2.46,19.26,19.26,0,0,0-8.21,5.07q-3.12,3.36-3.11,9.14,0,7.89,5.87,11.89T830.55,170.53Z"/><path class="cls-6" d="M911.79,66.9a10.65,10.65,0,0,1-7.6-3,9.86,9.86,0,0,1,0-14.49,11.07,11.07,0,0,1,15.2,0,9.86,9.86,0,0,1,0,14.49A10.65,10.65,0,0,1,911.79,66.9Zm-7.7,114.92V83.32h15.14v98.5Z"/><path class="cls-6" d="M962.06,50.49V181.82H946.93V50.49Z"/><path class="cls-7" d="M1045.34,0l26.37,62h1.42l47.14-62h44.65l-70.4,90.91L1136,181.82h-43.32l-27-62.06h-1.42l-47.59,62.06H971.74l72.18-90.91L1003,0Z"/></g></g></svg>
               </div>
               <div class="col-12 mt-md40 mt20 ">
                  <div class="main-heading text-center">
                     <div class="f-md-38 f-28 w700 text-center black-clr lh140">   
                        <span class="blue-gradient">Revealing “MailChimp” Killer AI-Powered App That</span> Writes & Delivers Unlimited Stunning Emails, And Drives Tons of Clicks, Sales, and Commissions
                        <span>
                      </div>
                      <div class="f-18 f-md-22 lh140 w600 text-center white-clr heading-blue-shape mt10">
                      Let AI Do It All. No Tech Hassles. No Monthly Fee Ever...
                      </div>
                   </div>
                </div>
               <div class="col-12 mt-md50 mt20 text-center">
                  <div class="f-20 f-md-26 w500 text-center lh150 white-clr post-heading">
                  <span class="orange-clr w700">Done-For-You Leads, Built-In High-Speed SMTP</span> (Email Delivery Servers), <span class="orange-clr w700">Smart Tagging, Unlimited List Importing & Mailing</span> - All Within an Easy Cloud Based App.
                  </div>
               </div>
                <div class="row align-items-center mt20 mt-md50">
               <div class="col-12 col-md-10 mx-auto">
                  <img src="assets/images/product-box.webp" class="img-fluid d-block mx-auto" alt="Product">
               </div>
            </div>
            </div>
         </div>
      </div> 
      <!--Proudly Introducing End -->

      <!-- <div class="cta-section-new">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  
                  <div class="f-22 f-md-28 w700 lh140 text-center white-clr mt20">
                  Grab AutomailX Now for <strike>$99/Month,</strike> Today Only 1-Time $17
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 text-center white-clr mt20">
                          (Get Free 30 Reseller License + FREE Commercial License)
                           </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 text-center">
                        <a href="#buynow" class="cta-link-btn">Get Instant Access To AutomailX</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20">
                     <img src="assets/images/compaitable-with1.webp" class="img-fluid d-block mx-xs-center md-img-right">
                     <div class="d-md-block d-none px-md30"><img src="assets/images/v-line.webp" class="img-fluid"></div>
                     <img src="assets/images/days-gurantee1.webp" class="img-fluid d-block mx-xs-auto mt15 mt-md0">
                  </div>
               </div>
            </div>
         </div>
      </div> -->

       <div class="warning">
         <div class="container">
            <div class="row text-center">
                 <div class="col-md-12 d-flex align-items-center justify-content-center flex-wrap text-center">
                    <img src="assets/images/warning.png" class="img-fluid d-block ml-auto mr10">
                    <div class="f-22 f-md-28 w400 lh140 text-center white-clr ">
                  <span class="w700">WARNING:</span>   Grab The Offer Before Timer Hits Zero</div>
                    </div>
                </div>
            </div>
        </div>

      <!-- <div class="zero">-->
      <!--   <div class="container">-->
      <!--      <div class="row">-->
      <!--         <div class="col-12">-->
      <!--            <div class="f-24 f-md-32 w600 lh140 text-center">Use this Intelligent BUT Stupid SIMPLE Software to Create High Quality Emails, get more leads & higher Inboxing again and again… With ZERO Writing Skill, ZERO Technical Skills, ZERO Headache, and ZERO Grunt Work... -->
      <!--            </div>-->
      <!--         </div>-->
      <!--      </div>-->
      <!--      <div class="row row-cols-1 row-cols-md-3 mt0 gap30">-->
      <!--         <div class="col">-->
      <!--            <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/z1.webp" class="img-fluid d-block mx-auto">-->
      <!--            <div class="f-20 w400 lh140 text-center mt20">Generate More Leads from any-->
      <!--               blog, ecommerce or WordPress site-->
      <!--            </div>-->
      <!--         </div>-->
      <!--         <div class="col">-->
      <!--            <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/z2.webp" class="img-fluid d-block mx-auto">-->
      <!--            <div class="f-20 w400 lh140 text-center mt20">Get mails delivered straight-->
      <!--               to INBOX-->
      <!--            </div>-->
      <!--         </div>-->
      <!--         <div class="col">-->
      <!--            <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/z3.webp" class="img-fluid d-block mx-auto">-->
      <!--            <div class="f-20 w400 lh140 text-center mt20">Send Unlimited Mails to-->
      <!--               Unlimited subscribers-->
      <!--            </div>-->
      <!--         </div>-->
      <!--         <div class="col">-->
      <!--            <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/z4.webp" class="img-fluid d-block mx-auto">-->
      <!--            <div class="f-20 w400 lh140 text-center mt20">Use smart tags to segment your-->
      <!--               subscribers & Send exclusive emails-->
      <!--            </div>-->
      <!--         </div>-->
      <!--         <div class="col">-->
      <!--            <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/z5.webp" class="img-fluid d-block mx-auto">-->
      <!--            <div class="f-20 w400 lh140 text-center mt20">Have Complete Control On Your-->
      <!--               Business – don’t rely on 3rd party services-->
      <!--            </div>-->
      <!--         </div>-->
      <!--         <div class="col">-->
      <!--            <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/z6.webp" class="img-fluid d-block mx-auto">-->
      <!--            <div class="f-20 w400 lh140 text-center mt20">No need to Pay Huge-->
      <!--               Monthly Fees-->
      <!--            </div>-->
      <!--         </div>-->
      <!--      </div>-->
      <!--   </div>-->
      <!--</div>-->
      <div class="step-section">
      <div class="container ">
         <div class="row ">
            <div class="col-12 f-28 f-md-45 w700 lh140 black-clr text-center">
            Start Building Lists & Turning Them Into Profits in <span class="blue-gradient"> Just 3 Easy Steps
               </span>
            </div>
            <img src="assets/images/let-line.webp" class="img-fluid mx-auto d-block" style="width:400px;">
            <div class="col-12 f-22 f-md-30 w400 text-center lh140 black-clr mt20 mt-md20">
            Stop Paying 100s of Dollars Monthly to Old-School Email Marketing Software!
            </div>
            <div class="col-12 mt30 mt-md80">
               <div class="row align-items-center">
                  <div class="col-12 col-md-5 relative">
                     <img src="assets/images/step1.webp" class="img-fluid">
                     <div class=" step1 f-28 f-md-50 w600 mt15">
                     Log-In to AutoMailX
                     </div>
                     <div class="f-18 f-md-20 w400 lh140 mt15">
                     Login to your AutoMailX account. Import List with No Limitation or Let AI find thousands of leads in any niche
                     </div>
                     <img src="assets/images/left-arrow.webp" class="img-fluid d-none d-md-block ms-auto step-arrow1">
                  </div>
                  <div class="col-12 col-md-7 mt20 mt-md0 ">
                     <video width="100%" height="auto" loop="" autoplay="" muted="muted" class="gif-bg" >
                        <source src="assets/images/step1.mp4" type="video/mp4" >
                     </video>
                  </div>
               </div>
            </div>
            <div class="col-12 mt-md150 ">
               <div class="row align-items-center ">
                  <div class="col-12 col-md-5 order-md-2 relative">
                     <img src="assets/images/step2.webp" class="img-fluid mt20">
                     <div class=" step2 f-28 f-md-50 w600 mt15">
                     Just Enter Keyword & Your Link
                     </div>
                     <div class="f-18 f-md-20 w400 lh140 mt15">
                     AutoMailX, AI generates profitable & engaging email with your link ready to blast within seconds.

                     </div>
                     <img src="assets/images/right-arrow.webp" class="img-fluid d-none d-md-block step-arrow2">
                  </div>
                  <div class="col-md-7 mt20 mt-md0 order-md-1 ">
                     <video width="100%" height="auto" loop="" autoplay="" muted="muted" class="gif-bg" >
                        <source src="assets/images/step2.mp4" type="video/mp4" >
                     </video>
                  </div>
               </div>
            </div>
            <div class="col-12 mt-md150">
               <div class="row align-items-center mt-md50">
                  <div class="col-12 col-md-5">
                     <img src="assets/images/step3.webp" class="img-fluid mt20">
                     <div class=" step3 f-28 f-md-50 w600 mt15">
                     Send & Profit:
                     </div>
                     <div class="f-18 f-md-20 w400 lh140 mt15">
                     Send unlimited emails directly into inbox for tons of traffic, sales & commissions with just push of a button.

                     </div>
                  </div>
                  <div class="col-12 col-md-7 mt20 mt-md0">
                     <video width="100%" height="auto" loop="" autoplay="" muted="muted" class="gif-bg" >
                        <source src="assets/images/step3.mp4" type="video/mp4" >
                     </video>
                  </div>
               </div>
            </div>
             <div class="col-12 f-md-24 f-20 w700 lh140 text-center mt20 mt-md40 aos-init aos-animate" data-aos="fade-up">
               <div class="smile-text">It Just Takes Less than 60 Seconds...</div>
            </div>
            
            <div class="col-12 text-center">
               
               <div class="d-flex gap-2 gap-md-1 justify-content-center flex-wrap mt20 mt-md30">
                  <div class="d-flex align-items-center gap-2 justify-content-center">
                     <img src="assets/images/red-cross.webp" alt="Cross" class="img-fluid d-block">
                     <div class="f-md-28 w700 lh140 f-20 red-clr"> No Email Writing &nbsp; </div>
                  </div>
                  <div class="d-flex align-items-center gap-2 justify-content-center">
                     <img src="assets/images/red-cross.webp" alt="Cross" class="img-fluid d-block">
                     <div class="f-md-28 w700 lh140 f-20 red-clr"> No Designer &nbsp;</div>
                  </div>
                  <div class="d-flex align-items-center gap-2 justify-content-center">
                     <img src="assets/images/red-cross.webp" alt="Cross" class="img-fluid d-block">
                     <div class="f-md-28 w700 lh140 f-20 red-clr">No Freelancer &nbsp;</div>
                  </div>
                  <div class="d-flex align-items-center gap-2 justify-content-center">
                     <img src="assets/images/red-cross.webp" alt="Cross" class="img-fluid d-block">
                     <div class="f-md-28 w700 lh140 f-20 red-clr">No 3rd Party Apps &nbsp;</div>
                  </div>
                  <div class="d-flex align-items-center gap-2 justify-content-center">
                     <img src="assets/images/red-cross.webp" alt="Cross" class="img-fluid d-block">
                     <div class="f-md-28 w700 lh140 f-20 red-clr">No SMTP </div>
                  </div>
               </div>
            </div>
           <div class="col-12 f-18 f-md-24 w400 lh140 black-clr text-center mt20 mt-md30">
                  Plus, with included free commercial license, this is the easiest & fastest way to start 6 figure business and help desperate businesses in no time! 
               </div>
         </div>
      </div>
   </div>
      <!-- Demo Video Section Start -->
       <div class="demo-section-bg" id="demo">
         <div class="container ">
            <div class="row ">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-45 w700 lh140 orange-clr not-believe-text">
                  Watch This Short Demo Video
                  </div>
               </div>
               <div class="col-12 f-28 f-md-45 w700 lh140 white-clr text-capitalize text-center mt20">
                 & See How Easy And POWERFUL AutomailX Is...
               </div>
               <div class="mt20 mt-md50">
                     <img src="assets/images/arrow.png" class="img-fluid mx-auto d-block downarrow">
                  </div>
               <div class="col-12 col-md-9 mx-auto mt20 mt-md50">
                  <!--<img src="assets/images/demo-img.webp" alt="" class="d-block mx-auto img-fluid">-->
                   <div class="responsive-video ">
                     <iframe src="https://automailx.dotcompal.com/video/embed/9ea5pbi7fo" style="width: 100%; height: 100%; " frameborder="0 " allow="fullscreen " allowfullscreen class="gif-bg"></iframe>
                  </div> 
               </div>
               <div class="col-12 mt30">
                 <div class="f-22 f-md-34 w500 lh140 text-center white-clr">
                           Grab AutomailX Now for <strike>$99/Month,</strike> Today Only 1-Time $17
                           </div>
                  <div class="f-18 f-md-20 w400 lh140 text-center white-clr mt20">
                     (Get Free 30 Reseller License + FREE Commercial License)
                  </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 text-center">
                        <a href="#buynow" class="cta-link-btn">Get Instant Access To AutomailX</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20">
                     <img src="assets/images/compaitable-with1.webp" class="img-fluid d-block mx-xs-center md-img-right">
                     <div class="d-md-block d-none px-md30"><img src="assets/images/v-line.webp" class="img-fluid"></div>
                     <img src="assets/images/days-gurantee1.webp" class="img-fluid d-block mx-xs-auto mt15 mt-md0">
                  </div>
               </div>
            </div>
         </div>
      </div> 
      <!-- Demo Video Section End -->
      
      <section class="blue-section1">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-45 w700 lh140 white-clr still-headline">
                     Checkout The Ground-Breaking Features
                  </div>
                  <div class="f-28 f-md-45 w700 lh140 black-clr mt10">
                     That Make AutomailX A Cut Above The Rest 
                  </div>
               </div>
            </div>

      
         <div class="container">
            <div class="row align-items-center mt20 mt-md30">
               <div class="col-md-6 relative">
                  <div class="f-24 f-md-30 lh140 black-clr w700 features-title">
                  <img src="../common_assets/images/1.webp" class="img-responsive size">
                     A Fully AI Powered Cloud Based Platform 
                  </div>
                  <div class="f-20 lh140 black-clr w400 mt30">
                     AutomailX as an advanced technology platform. Its artificial intelligence designed to generate natural language email responses. 
                  </div>
                  <!-- <img src="assets/images/arrow-left.webp" class="img-fluid d-none ms-md-auto d-md-block" style="position: absolute; right: -160px; top: 261px;"> -->
               </div>
               <div class="col-md-6 mt20 mt-md0">
                  <img src="assets/images/f1.webp" alt="Build Huge" class="mx-auto d-block img-fluid">
               </div>
            </div>
         </div>
      </section>
      <section class="white-section">
         <div class="container">
            <div class="row align-items-center ">
            <div class="f-24 f-md-30 lh140 black-clr w700 features-title">
                  <img src="../common_assets/images/2.webp" class="img-responsive size">
                  Built-In High-Speed SMTP to Deliver Emails Directly into Inbox - 
               </div>
               <div class="col-md-9 mt30 mx-auto">
               <video width="100%" height="auto" loop="" autoplay="" muted="muted" class="gif-bg" >
                        <source src="assets/images/step2.mp4" type="video/mp4" >
                     </video>
               </div>
               <div class="col-12 f-20 lh140 black-clr w400 mt30">
               AutoMailX offers Built-In High-Speed SMTP, ensuring lightning-fast email delivery directly to the inbox. Say goodbye to third-party SMTP services – just upload your list and start mailing instantly!
               </div>
            </div>
         </div>
      </section>
      <section class="blue-section1">
         <div class="container">
            <div class="row align-items-center ">
               <div class="col-md-6 relative">
               <div class="f-24 f-md-30 lh140 black-clr w700 features-title">
                  <img src="../common_assets/images/3.webp" class="img-responsive size">
                  Done-Foe-You Leads for Immediate Profits - 
                  </div>
                  <div class="f-18  lh140 black-clr w400 mt30">
                  Don't have an email list? No worries – we provide you with ready-to-use email leads. Let AI Build your list to start generating profits instantly.
                  </div>
                  
               </div>
               <div class="col-md-6 mt20 mt-md0">
                  <img src="assets/images/f3.webp" alt="Build Huge" class="mx-auto d-block img-fluid">
               </div>
            </div>
            </div>
      </section>
      <section class="white-section">
         <div class="container">
            <div class="row align-items-center ">
            <div class="f-24 f-md-30 lh140 black-clr w700 features-title">
                  <img src="../common_assets/images/4.webp" class="img-responsive size">
                  Create High-Quality Emails & Messages With Just One Keyword 
               </div>
               <div class="col-md-9 mt30 mx-auto">
               <video width="100%" height="auto" loop="" autoplay="" muted="muted" class="gif-bg" >
                        <source src="assets/images/step2.mp4" type="video/mp4" >
                     </video>
               </div>
               <div class="col-12 f-20 lh140 black-clr w400 mt30">
                  AutomailX AI creates an engaging, high quality & personalized email & subject line using the keyword on which want to craft email for your campaign. 
               </div>
            </div>
         </div>
      </section>
      <section class="blue-section1">
         <div class="container">
            <div class="row align-items-center ">
               <div class="col-md-6 relative">
               <div class="f-24 f-md-30 lh140 black-clr w700 features-title">
                  <img src="../common_assets/images/5.webp" class="img-responsive size">
                     	Send Unlimited Mails Instantly Or Schedule Them For Later Date/Time
                  </div>
                  <div class="f-18  lh140 black-clr w400 mt30">
                  Yes, the sky is the limit for mailing with AutomailX. It allows you to send unlimited emails or newsletters and get rid of endless complications of email marketing. You can send emails right away or schedule them for a later date and time. 
                  </div>
                  
               </div>
               <div class="col-md-6 mt20 mt-md0">
                  <img src="assets/images/f3.webp" alt="Build Huge" class="mx-auto d-block img-fluid">
               </div>
            </div>
            </div>
      </section>
      <section class="white-section">
         <div class="container">
            <div class="row align-items-center ">
               <div class="col-md-6 order-md-2 relative">
               <div class="f-24 f-md-30 lh140 black-clr w700 features-title">
                  <img src="../common_assets/images/6.webp" class="img-responsive size">
                  	AI Enabled Smart Tagging For Lead Personalization & Traffic
                  </div>
                  <div class="f-20 lh140 black-clr w400 mt30">
                  This is our masterpiece. Using this Latest & VERY Powerful feature, you can assign tags to your subscribers and segment them in a very simple manner. Now you can send emails exclusively to the subscribers related to any smart Tag or group. 
                  </div>
                  
               </div>
               <div class="col-md-6 mt20 mt-md0 order-md-1">
                  <img src="assets/images/f4.webp" alt="Sell" class="mx-auto d-block img-fluid">
               </div>
            </div>
            </div>  
      </section>
      <section class="blue-section1">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-6 relative">
               <div class="f-24 f-md-30 lh140 black-clr w700 features-title">
                  <img src="../common_assets/images/7.webp" class="img-responsive size">
                     	Boost Email Delivery, Click And Open Rate with Engaging Emails
                  </div>
                  <div class="f-20 lh140 black-clr w400 mt30">
                     Opens and clicks give you an accurate idea about how your campaign is performing. Today all the marketers want to get their mails delivered in inbox, clicked and opened on time and that is what AutomailX is made for. 
                        <br><br>
                     It increases your delivery, click and open rate and gives you full control on your campaigns. 
                  </div>
                  
               </div>
               <div class="col-md-6 mt20 mt-md0">
                  <img src="assets/images/f5.webp" alt="Build Huge" class="mx-auto d-block img-fluid">
               </div>
            </div>
            </div>  
      </section>
            <section class="white-section">
         <div class="container">

         <div class="row align-items-center ">
         <div class="f-24 f-md-30 lh140 black-clr w700 features-title">
                  <img src="../common_assets/images/8.webp" class="img-responsive size">
               Design Beautiful Newsletters & Autoresponders Using AI 
               </div>
               <div class="col-md-12 mt30 mt-md60 mx-auto">
                  <img src="assets/images/f6.webp" class="img-fluid d-block mx-auto scaleimg">
               </div>
               <div class="col-12 f-20 lh140 black-clr w400 mt30">
               Create HUNDREDS of stellar combinations with incredibly magnificent email templates. You are leveraged with premium templates for the HOTTEST of niches. They are completely done-for-you and all you need to do is just get them in sync with your best offers and start funnelling the valuable prospects. 
               </div>
            </div>
         </div>  
      </section>

      <section class="blue-section1">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-6 relative">
               <div class="f-24 f-md-30 lh140 black-clr w700 features-title">
                  <img src="../common_assets/images/9.webp" class="img-responsive size">
                     	Import Unlimited Subscribers with Just 1 Click
                  </div>
                  <div class="f-20 lh140 black-clr w400 mt30">
                        AutomailX enables you to import unlimited subscribers list and the best part is that you won't lose even a single ID in the process. So, you can mail freely to your subscribers without any restrictions whatsoever. 
                        <br><br>
                        Most email marketing service providers charge a hefty fees for importing your lists so stop paying a huge monthly rental just to keep your list even if you don't mail them. 
                        <br><br>
                        They also need double optin before allowing to import so you lose anywhere from 20-30% of your subscribers. But with AutomailX, all that will become a case of the bygone era. 
                  </div>
                  
               </div>
               <div class="col-md-6 mt20 mt-md0">
                  <img src="assets/images/f7.webp" alt="Build Huge" class="mx-auto d-block img-fluid">
               </div>
            </div>
            </div>  
      </section>
            <section class="white-section">
         <div class="container">

         <div class="row align-items-center ">
         <div class="f-24 f-md-30 lh140 black-clr w700 features-title">
                  <img src="../common_assets/images/10.webp" class="img-responsive size">
               Generate Tons of Subscribers With Our Attractive In-Built Lead Form -
               </div>
               <div class="col-md-12 mt30 mt-md60 mx-auto">
                  <img src="assets/images/f8.webp" class="img-fluid d-block mx-auto scaleimg">
               </div>
               <div class="col-12 f-20 lh140 black-clr w400 mt30">
               Lead forms are the simplest way to generate qualified leads and build a huge base of cash paying customers for your business. 
                        <br><br>
                        Keeping this in mind, AutomailX helps you to grab attention of more and more subscribers on your blog, e-commerce sites or WordPress sites with an eye-catchy lead generation form. 
                        <br><br>
                        All you need to do is: copy one line of code and paste it on your site.
               </div>
            </div>
            </div>  
      </section>
      <section class="blue-section1">
         <div class="container">
         <div class="row align-items-center ">
         <div class="f-24 f-md-30 lh140 black-clr w700 features-title">
                  <img src="../common_assets/images/11.webp" class="img-responsive size">
               Inbuilt AI Text & Inline Editor To Craft Best Emails - 
               </div>
               <div class="col-md-10 col-12 mt30 mt-md60 mx-auto">
                  <img src="assets/images/f9.webp" class="img-fluid d-block mx-auto scaleimg">
               </div>
               
               <div class="col-12 f-20 lh140 black-clr w400 mt30">
               You can create simple text emails or html emails with our LIVE Inline editor feature to send best emails for maximum engagement. This is all built to attract, capture, nurture and convert your potential prospect. 
               </div>
            </div>
            </div>  
      </section>
            <section class="white-section">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-6 order-md-2 relative">
               <div class="f-24 f-md-30 lh140 black-clr w700 features-title text-capitalize ">
                  <img src="../common_assets/images/12.webp" class="img-responsive size">
                     Reduce bounce rate 
                  </div>
                  <div class="f-20 lh140 black-clr w400 mt30">
                     Higher bounce spoils your image and get murky with every bounce. With AutomailX, you can get rid of all the bounced and spammed mails. AutomailX automatically remove mails that were counted as bounce and make your list clear without any grunt work. 
                  </div>
                  
               </div>
               <div class="col-md-6 mt20 mt-md0 order-md-1">
                  <img src="assets/images/f10.webp" alt="Sell" class="mx-auto d-block img-fluid">
               </div>
            </div>
            </div>  
      </section>
      <section class="blue-section1">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-6 relative">
               <div class="f-24 f-md-30 lh140 black-clr w700 features-title text-capitalize">
                  <img src="../common_assets/images/13.webp" class="img-responsive size">
                     AutomailX is 100% CAN SPAM Compliant - 
                  </div>
                  <div class="f-20 lh140 black-clr w400 mt30">
                     It provide one-click unsubscribe feature that is user-friendly and helps to greatly reduce spam complaints and built your better send reputation. 
                  </div>
                  
               </div>
               <div class="col-md-6 mt20 mt-md0">
                  <img src="assets/images/f11.webp" alt="Build Huge" class="mx-auto d-block img-fluid">
               </div>
            </div>
            </div>  
      </section>
            <section class="white-section">
         <div class="container">
         <div class="row align-items-center ">
         <div class="f-24 f-md-30 lh140 black-clr w700 features-title text-capitalize">
                  <img src="../common_assets/images/14.webp" class="img-responsive size">
               Manage your subscribers hassle-free
               </div>
               <div class="col-md-10 col-12 mt30 mt-md60 mx-auto">
                  <img src="assets/images/f12.webp" class="img-fluid d-block mx-auto scaleimg">
               </div>
               
               <div class="col-12 f-20 lh140 black-clr w400 mt30">
               AutomailX offers you the easiest way to find, filter or clean your subscribers in never-ending lists. You can find out a subscriber out of a list of thousands with just 1 click. 
                        <br><br>
                  Track duplicate entries and create a backup of your list in no time with AutomailX
               </div>
            </div>
            </div>  
      </section>
      <section class="blue-section1">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-6 relative">
               <div class="f-24 f-md-30 lh140 black-clr w700 features-title text-capitalize">
                  <img src="../common_assets/images/15.webp" class="img-responsive size">
                     All-in-one cloud based email marketing software -
                  </div>
                  <div class="f-20 lh140 black-clr w400 mt30">
                  AutomailX is built on the idea to deliver maximum quality, ease and efficiency. And to make it simpler we made it a cloud based platform. 
                  <br><br>
                  Guys you will be thrilled by numbers of features that we are offering to you to make your email marketing simple and fun. 
                  </div>
                  
               </div>
               <div class="col-md-6 mt20 mt-md0">
                  <img src="assets/images/f13.webp" alt="Build Huge" class="mx-auto d-block img-fluid">
               </div>
            </div>
            </div>  
      </section>
            <section class="white-section">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-6 order-md-2 relative">
               <div class="f-24 f-md-30 lh140 black-clr w700 features-title text-capitalize">
                  <img src="../common_assets/images/16.webp" class="img-responsive size">
                     Boost sales and increase revenue -
                  </div>
                  <div class="f-20 lh140 black-clr w400 mt30">
                     Email marketing is taking the world by storm and if you are searching the right way to send best emails to attract more customers, AutomailX is the best among rest
                     <br><br>
                     You could send a free whitepaper, more information on your products, "subscriber-only" discount, or even personize mails to contend your customers for taking action. Your imagination and creativity really are the only limits and you have endless opportunities to get success. 
                  </div>
                  
               </div>
               <div class="col-md-6 mt20 mt-md0 order-md-1">
                  <img src="assets/images/f14.webp" alt="Sell" class="mx-auto d-block img-fluid">
               </div>
            </div>
            </div>  
      </section>
      <section class="blue-section1">
         <div class="container">


         <div class="row align-items-center ">
         <div class="f-24 f-md-30 lh140 black-clr w700 features-title text-capitalize">
                  <img src="../common_assets/images/17.webp" class="img-responsive size">
               Personalize your mails to get high opening rates 
               </div>
               <div class="col-md-10 col-12 mt30 mt-md60 mx-auto">
                  <img src="assets/images/f15.webp" class="img-fluid d-block mx-auto scaleimg">
               </div>
               
               <div class="f-20 lh140 black-clr w400 mt30 mt-md60 ">
                     Now this is something that will really prove to be of great worth for your marketing efforts. Personalization of emails is the best and easiest way to get attention of your subscribers, with AutomailX, you can personalize your every email to every subscriber to get high open rates. 
                  </div>
            </div>



            </div>  
      </section>
            <section class="white-section">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-6 order-md-2 relative">
               <div class="f-24 f-md-30 lh140 black-clr w700 features-title text-capitalize">
                  <img src="../common_assets/images/18.webp" class="img-responsive size">
                     Create long-term relationships with your subscribers with beautiful newsletters -
                  </div>
                  <div class="f-20 lh140 black-clr w400 mt30">
                     Always try to keep in touch with your subscribers to create a strong relationship with them. You can send newsletters to update them about your products or upcoming launches. 
                     <br><br>
                     You can redirect them on your blog or website to keep your brand in their mind. 
                  </div>
                  
               </div>
               <div class="col-md-6 mt20 mt-md0 order-md-1">
                  <img src="assets/images/f16.webp" alt="Sell" class="mx-auto d-block img-fluid">
               </div>
            </div>
            </div>  
      </section>
      <section class="blue-section1">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-6 relative">
               <div class="f-24 f-md-30 lh140 black-clr w700 features-title text-capitalize">
                  <img src="../common_assets/images/19.webp" class="img-responsive size">
                     100% newbie friendly and fully automated
                  </div>
                  <div class="f-20 lh140 black-clr w400 mt30">
                  Effective email marketing is all about sending right email to right people at right time and our software team has got everything covered and made it no hands software.
                        <br><br>
                  With the robust features of AutomailX, you have the complete freedom to automate your email marketing. The software delivers automated, one-to-one messages across all of your marketing channels and even a newbie can manage his email marketing campaign without any hassle.
                  </div>
                  
               </div>
               <div class="col-md-6 mt20 mt-md0">
                  <img src="assets/images/f17.webp" alt="Build Huge" class="mx-auto d-block img-fluid">
               </div>
            </div>
            </div>  
      </section>
            <section class="white-section">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-6 order-md-2 relative">
               <div class="f-24 f-md-30 lh140 black-clr w700 features-title text-capitalize">
                  <img src="../common_assets/images/20.webp" class="img-responsive size">
                  No monthly fees or additional charges
                  </div>
                  <div class="f-20 lh140 black-clr w400 mt30">
                        If you are a newbie and starting out with email marketing, then sending mails can be a costly affair. They charge you like wildfire and you have to provide a heavy monthly fee just to reach out to your email list.
                        <br><br>
                        But AutomailX is hands down the best email marketing software available today that charges no recurring fee and allows you to send unlimited emails with just one click.
                  </div>
                  
               </div>
               <div class="col-md-6 mt20 mt-md0 order-md-1">
                  <img src="assets/images/f18.webp" alt="Sell" class="mx-auto d-block img-fluid">
               </div>
            </div>
            </div>  
      </section>
      <section class="blue-section1">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-6 relative">
               <div class="f-24 f-md-30 lh140 black-clr w700 features-title text-capitalize">
                  <img src="../common_assets/images/21.webp" class="img-responsive size">
                  Brand new system- Absolutely NO rehashes
                  </div>
                  <div class="f-20 lh140 black-clr w400 mt30">
                  We always believe in giving you something that's packed with latest features and which is simply not an add-on to a pre-existing product.
                        <br><br>
                        So, AutomailX is packed with great features and it's the ultimate email marketing technology that's never been seen before.
                  </div>
                  
               </div>
               <div class="col-md-6 mt20 mt-md0">
                  <img src="assets/images/f19.webp" alt="Build Huge" class="mx-auto d-block img-fluid">
               </div>
            </div>
            </div>  
      </section>
      <section class="white-section">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-6 order-md-2 relative">
               <div class="f-24 f-md-30 lh140 black-clr w700 features-title text-capitalize">
                  <img src="../common_assets/images/22.webp" class="img-responsive size">
                  Designed by Marketers for Marketers
                  </div>
                  <div class="f-20 lh140 black-clr w400 mt30">
                  AutomailX has been built from the ground up to be A-Z marketer-friendly, meaning you can upload your list of subscribers straight into the software with no technical hassles, and get best results without any complications.
                  </div>
                  
               </div>
               <div class="col-md-6 mt20 mt-md0 order-md-1">
                  <img src="assets/images/f20.webp" alt="Sell" class="mx-auto d-block img-fluid">
               </div>
            </div>
            </div>  
      </section>
      <section class="blue-section1">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-6 relative">
               <div class="f-24 f-md-30 lh140 black-clr w700 features-title text-capitalize">
                  <img src="../common_assets/images/23.webp" class="img-responsive size">
                  Step-By-Step Training to Make Everything Easy For You...
                  </div>
                  <div class="f-20 lh140 black-clr w400 mt30">
                  Yep, we know software can get complex. And while AutomailX is DEAD easy to use, we wanted to make 100% sure it's accessible to everyone and everyone can earn with it. That's why we did 2 things:
                        <br><br>
                        #1 We've added in-depth video training for every feature so you can always look at the RIGHT way to do things
                        <br><br>
                        #2 We're also offering 24*7 on-going support so you're always just a message away from having your problem solved.
                  </div>
                  
               </div>
               <div class="col-md-6 mt20 mt-md0">
                  <img src="assets/images/f21.webp" alt="Build Huge" class="mx-auto d-block img-fluid">
               </div>
            </div>
         </div>  
      </section>

      
      
      <div class="cta-section-new">
         <div class="container">
            <div class="row">
               <div class="col-12">
                 <div class="f-22 f-md-34 w500 lh140 text-center white-clr">
                           Grab AutomailX Now for <strike>$99/Month,</strike> Today Only 1-Time $17
                           </div>
                  <div class="f-18 f-md-20 w400 lh140 text-center white-clr mt20">
                     (Get Free 30 Reseller License + FREE Commercial License)
                  </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 text-center">
                        <a href="#buynow" class="cta-link-btn">Get Instant Access To AutomailX</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20">
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/compaitable-with1.webp" class="img-fluid d-block mx-xs-center md-img-right">
                     <div class="d-md-block d-none px-md30"><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/v-line.webp" class="img-fluid"></div>
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/days-gurantee1.webp" class="img-fluid d-block mx-xs-auto mt15 mt-md0">
                  </div>
               </div>
            </div>
         </div>
      </div>
      
      <div class="warning">
         <div class="container">
            <div class="row text-center">
                 <div class="col-md-12 d-flex align-items-center justify-content-center flex-wrap text-center">
                    <img src="assets/images/warning.png" class="img-fluid d-block ml-auto mr10">
                    <div class="f-22 f-md-28 w400 lh140 text-center white-clr ">
                  <span class="w700">WARNING:</span>   Grab The Offer Before Timer Hits Zero</div>
                    </div>
                </div>
            </div>
        </div>
      
    <section class="comp-table">
         <div class="comparetable-section ">
            <div class="container ">
               <div class="row ">
                  <div class="f-md-45 f-28 w700 lh140 black-clr text-center">
                    There is Nothing Like AutomailX in The Market
                  </div>
                  <!--<div class="col-12">-->
                  <!--   <div class="f-md-45 f-28 w700 lh140 black-clr text-center ">-->
                  <!--      No Other Software Even Comes Close!-->
                  <!--   </div>-->
                  <!--</div>-->
                  <div class="col-12 mt10 d-none d-md-block">
                     
                     <div class="row g-0 mt70 mt-md100">
                        <div class="col-md-2 col-2">
                           <div class="fist-row">
                              <ul class="f-md-18 f-14 w500 lh120">
                                 <li class="f-md-28 f-16 w700 justify-content-start justify-content-md-center">Features</li>
                                 <li class="f-md-32 w600">No. of contacts </li>
                                 <li>Ai Writer</li>
                                 <li>Smart Tagging </li>
                                 <li>Send Unlimited Mails </li>
                                 <li>SMTP Integration </li>
                                 <li>Automation</li>
                                 <li>ChatGPT Enabled </li>
                                 <li>One Time Pay </li>
                                 <li>Ai Graphics </li>
                              </ul>
                           </div>
                        </div>
                        <div class="col-md-2 col-2">
                           <div class="second-row">
                              <ul class="f-md-16 f-14 w600 white-clr ttext-center lh120">
                                 <li class="f-md-20 f-16 white-clr">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 1164.92 192.13" style="max-height:55px"><defs><style>.cls-1,.cls-2,.cls-3,.cls-4,.cls-5{fill-rule:evenodd;}.cls-1{fill:url(#linear-gradient);}.cls-2{fill:url(#linear-gradient-2);}.cls-3{fill:url(#linear-gradient-3);}.cls-4{fill:url(#linear-gradient-4);}.cls-5{fill:url(#linear-gradient-5);}.cls-6{fill:#fff;}.cls-7{fill:url(#linear-gradient-6);}</style><linearGradient id="linear-gradient" x1="82.05" y1="127.34" x2="150.42" y2="127.34" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#624ef2"/><stop offset="1" stop-color="#1e9af7"/></linearGradient><linearGradient id="linear-gradient-2" x1="7.14" y1="79.25" x2="123.43" y2="79.25" xlink:href="#linear-gradient"/><linearGradient id="linear-gradient-3" x1="0" y1="116.36" x2="45.71" y2="116.36" xlink:href="#linear-gradient"/><linearGradient id="linear-gradient-4" x1="3.94" y1="157.47" x2="142.12" y2="157.47" xlink:href="#linear-gradient"/><linearGradient id="linear-gradient-5" x1="3.58" y1="75.02" x2="182.4" y2="75.02" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#ff2d2d"/><stop offset="1" stop-color="#ffbc00"/></linearGradient><linearGradient id="linear-gradient-6" x1="971.74" y1="90.91" x2="1164.92" y2="90.91" xlink:href="#linear-gradient"/></defs><title>AutomailX White</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1" d="M150.42,67.57V177.2a15.41,15.41,0,0,1-3.38,9.92q-20.67-19.74-39-37.28l-26-24.94Z"/><path class="cls-2" d="M123.43,48.09l-62.5,62.32Q47.16,98.7,33.43,86.92q-8.17-7-16.31-14.11c-3.34-2.91-6.75-5.85-10-8.88a6.08,6.08,0,0,1,1.32-.33l24.25-3.27,28.14-3.8,33-4.45Z"/><path class="cls-3" d="M45.71,119.33,25.4,139.11Q15.2,149,5.81,158.19c-2.33,3-5.83,2.65-5.81-2.51V76.9c0-5.16,3.5-5.32,5.81-2.2l.39.41h0c.73,1.66,1.4,3.32,2,4.81l.15.38Z"/><path class="cls-4" d="M142.12,190.87a14.7,14.7,0,0,1-8.88,1l-39.42-7.34-33-6.14-28.14-5.24L8.44,168.61a8.2,8.2,0,0,1-4.5-2.68L25.82,145q11.1-10.61,23.23-22.21l15.81,16.51L78,128.3q12.35,12.06,25.78,25.16Q121.84,171.09,142.12,190.87Z"/><path class="cls-5" d="M10.15,79.16,65,136.41l92.87-81.5,3.93,12.24L182.4,13.62,128.57,32.19l11.14,5.9L61.05,116.53s-54.7-47.67-57.47-50c1.16,1.75,6.57,12.68,6.57,12.68Z"/><path class="cls-6" d="M283,50.49H247.2L201.86,181.82h29.75l9.74-29.95h47.42l9.73,29.95h29.75ZM248.4,130.2l16.17-49.7h1l16.14,49.7Z"/><path class="cls-6" d="M406.68,139.88V83.32H434v98.5H407.77V163.93h-1a29.1,29.1,0,0,1-11.06,13.91q-7.73,5.26-18.82,5.26a33.23,33.23,0,0,1-17.38-4.49,30.61,30.61,0,0,1-11.7-12.76q-4.2-8.26-4.27-19.81V83.32h27.32v57.84q.06,8.73,4.68,13.79T387.89,160a19.37,19.37,0,0,0,9.23-2.27,17.53,17.53,0,0,0,7-6.77A21,21,0,0,0,406.68,139.88Z"/><path class="cls-6" d="M510,180.6q-2.06.65-5.78,1.51a48.09,48.09,0,0,1-9,1.06,41.39,41.39,0,0,1-17.28-2.63,24.12,24.12,0,0,1-11.51-9.36q-4.11-6.36-4-16v-51.3H448.87V83.32h13.47V59.72h27.32v23.6h18.53v20.52H489.66v47.71a12.24,12.24,0,0,0,1.15,5.87,6.34,6.34,0,0,0,3.24,2.92,13.08,13.08,0,0,0,4.84.83,21.79,21.79,0,0,0,3.85-.35l2.95-.55Z"/><path class="cls-6" d="M569.43,183.74q-14.94,0-25.81-6.38a43,43,0,0,1-16.77-17.83Q521,148.09,521,133t5.9-26.71a43,43,0,0,1,16.77-17.82Q554.5,82,569.43,82t25.81,6.38A43,43,0,0,1,612,106.24q5.91,11.46,5.9,26.71T612,159.53a43,43,0,0,1-16.77,17.83Q584.38,183.75,569.43,183.74Zm.13-21.16a16.88,16.88,0,0,0,11.35-3.88,23.82,23.82,0,0,0,6.89-10.61,46.31,46.31,0,0,0,2.35-15.33,46.31,46.31,0,0,0-2.35-15.33,24.09,24.09,0,0,0-6.89-10.64,16.8,16.8,0,0,0-11.35-3.91,17.26,17.26,0,0,0-11.51,3.91,23.74,23.74,0,0,0-7,10.64,46.58,46.58,0,0,0-2.34,15.33,46.58,46.58,0,0,0,2.34,15.33,23.48,23.48,0,0,0,7,10.61A17.35,17.35,0,0,0,569.56,162.58Z"/><path class="cls-6" d="M640.74,50.49h19l44.63,109h1.54l44.64-109h19V181.82H754.63V82h-1.28l-41,99.78H697.94L656.9,82h-1.28v99.78H640.74Z"/><path class="cls-6" d="M828.25,184.13a39.77,39.77,0,0,1-17-3.56,28.82,28.82,0,0,1-12.12-10.33q-4.49-6.77-4.49-16.38,0-8.46,3.34-13.76a23.63,23.63,0,0,1,8.91-8.3,47.46,47.46,0,0,1,12.35-4.52q6.76-1.52,13.62-2.41,9-1.16,14.59-1.76t8.21-2.08q2.6-1.48,2.6-5.13v-.52q0-9.48-5.16-14.75t-15.62-5.25q-10.85,0-17,4.74a28,28,0,0,0-8.66,10.13l-14.37-5.13a34.69,34.69,0,0,1,10.3-14,38.56,38.56,0,0,1,14.11-7A58.86,58.86,0,0,1,837,82a61.89,61.89,0,0,1,10.93,1.12,35.92,35.92,0,0,1,12,4.58,26.84,26.84,0,0,1,9.65,10.46q3.86,7,3.85,18.72v64.9H858.26V168.48h-.77a26.84,26.84,0,0,1-5.13,6.86,29.6,29.6,0,0,1-9.56,6.22A36.78,36.78,0,0,1,828.25,184.13Zm2.3-13.6q9,0,15.17-3.53a24.19,24.19,0,0,0,9.36-9.1,23.36,23.36,0,0,0,3.18-11.74V132.31q-1,1.16-4.2,2.09a64,64,0,0,1-7.44,1.6q-4.2.67-8.15,1.15c-2.63.32-4.75.59-6.38.81a58.57,58.57,0,0,0-11,2.46,19.26,19.26,0,0,0-8.21,5.07q-3.12,3.36-3.11,9.14,0,7.89,5.87,11.89T830.55,170.53Z"/><path class="cls-6" d="M911.79,66.9a10.65,10.65,0,0,1-7.6-3,9.86,9.86,0,0,1,0-14.49,11.07,11.07,0,0,1,15.2,0,9.86,9.86,0,0,1,0,14.49A10.65,10.65,0,0,1,911.79,66.9Zm-7.7,114.92V83.32h15.14v98.5Z"/><path class="cls-6" d="M962.06,50.49V181.82H946.93V50.49Z"/><path class="cls-7" d="M1045.34,0l26.37,62h1.42l47.14-62h44.65l-70.4,90.91L1136,181.82h-43.32l-27-62.06h-1.42l-47.59,62.06H971.74l72.18-90.91L1003,0Z"/></g></g></svg>
                                 </li>
                                 <li class="f-md-24 w600" style="line-height:36px;"> Unlimited </li>
                                 <li><img src="assets/images/thumbsup.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                                 <li><img src="assets/images/thumbsup.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                                 <li class="w600">Unlimited</li>
                                 <li><img src="assets/images/thumbsup.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                                 <li><img src="assets/images/thumbsup.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                                 <li><img src="assets/images/thumbsup.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                                 <li><img src="assets/images/thumbsup.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                                 <li><img src="assets/images/thumbsup.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                              </ul>
                           </div>
                        </div>
                        <div class="col-md-2 col-2">
                           <div class="third-row">
                              <ul class="f-md-16 f-14 w500 lh120">
                                 <li class="f-md-28 f-16 w700"><span>GetResponse</span></li>
                                 <li class="f-md-24 w600">Unlimited </li>
                                 <li><img src="assets/images/thumbsdown.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                 <li><img src="assets/images/thumbsdown.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                 <li><img src="assets/images/thumbsup.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                                 <li><img src="assets/images/thumbsdown.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                 <li><img src="assets/images/thumbsup.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                                 <li><img src="assets/images/thumbsdown.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                 <li><img src="assets/images/thumbsdown.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                 <li><img src="assets/images/thumbsdown.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                              </ul>
                           </div>
                        </div>
                        <div class="col-md-2 col-2">
                           <div class="third-row">
                              <ul class="f-md-16 f-14 w500 lh120">
                                 <li class="f-md-28 f-16 w700"><span>Aweber</span></li>
                                 <li class="f-md-24 w600">Unlimited</li>
                                 <li><img src="assets/images/thumbsdown.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                 <li><img src="assets/images/thumbsdown.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                 <li><img src="assets/images/thumbsup.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                                 <li><img src="assets/images/thumbsdown.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                 <li><img src="assets/images/thumbsup.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                                 <li><img src="assets/images/thumbsdown.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                 <li><img src="assets/images/thumbsdown.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                 <li><img src="assets/images/thumbsdown.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                              </ul>
                           </div>
                        </div>
                        <div class="col-md-2 col-2">
                           <div class="third-row">
                              <ul class="f-md-16 f-14 w500 lh120">
                                 <li class="f-md-28 f-16 w700"><span>Convertkit</span></li>
                                 <li class="f-md-24 w600">Unlimited</li>
                                 <li><img src="assets/images/thumbsdown.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                 <li><img src="assets/images/thumbsdown.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                 <li><img src="assets/images/thumbsup.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                                 <li><img src="assets/images/thumbsdown.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                 <li><img src="assets/images/thumbsup.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                                 <li><img src="assets/images/thumbsdown.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                 <li><img src="assets/images/thumbsdown.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                 <li><img src="assets/images/thumbsdown.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                              </ul>
                           </div>
                        </div>
                        <div class="col-md-2 col-2">
                        <div class="third-row">
                        <ul class="f-md-16 f-14 w500 lh120">
                                 <li class="f-md-28 f-16 w700"><span>SendinBlue</span></li>
                                 <li class="f-md-24 w600">Unlimited</li>
                                 <li><img src="assets/images/thumbsdown.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                 <li><img src="assets/images/thumbsdown.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                 <li><img src="assets/images/thumbsup.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                                 <li><img src="assets/images/thumbsup.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                                 <li><img src="assets/images/thumbsup.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                                 <li><img src="assets/images/thumbsdown.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                 <li><img src="assets/images/thumbsdown.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                 <li><img src="assets/images/thumbsdown.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                              </ul>
                        </div>
                        </div>
                     </div>
                     <div class="mt-md30 mt20 f-16 f-md-18 w400 lh140 "><span class="w700 ">Note :</span> All the features mentioned in the above table are bifurcated in different upgrade options according to the need of individual users and the features that you will get with purchase of AutomailX Start
                        or Pro Commercial plan are mentioned in the pricing table below on this page.
                     </div>
                  </div>
                  <div class="col-12">
                     <img src="assets/images/comp-table.webp" alt="comptable" class="mx-auto d-block img-fluid d-md-none">
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- <section class="revolution-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-45 w700 lh140 white-clr">
                  AutomailX Is The Revolution In Email Marketing World
                  </div>
                  <div class="mt10">
                     <img src="assets/images/revolution-line.webp" alt="" class="mx-auto d-block img-fluid">
                  </div>
                  <div class="mt20 mt-md30 white-clr f-20 f-md-24 w400 lh140">
                    That is going to change the future of email marketing, writing & designing for “Better”.
                  </div>
                  <div class="mt20 mt-md80">
                     <img src="assets/images/automail-img.webp" alt="" class="img-fluid mx-auto d-block">
                  </div>
               </div>
            </div>
         </div>
      </section> -->

      <div class="powerfull-ai">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               
               <div class="f-28 f-md-50 w700 lh140  black-clr">
               AutomailX Is The Revolution In Email Marketing World
               </div>
            </div>
         </div>
         <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 gap30 mt20 mt-md10">
            <div class="col">
               <div class="figure">
                  <img src="assets/images/dfy1.webp" alt="DFY Websites" class="mx-auto d-block img-fluid ">
                  <div class="figure-caption text-center">
                     <div class="f-18 f-md-22 w500 lh140 mt20 black-clr">Welcome To This Futuristic AI Powered Email Marketing Solution</div>
                  </div>
               </div>
            </div>

            <div class="col">
               <div class="figure">
               <img src="assets/images/dfy2.webp" alt="DFY Websites" class="mx-auto d-block img-fluid ">
                  <div class="figure-caption text-center">
                     <div class="f-18 f-md-22 w500 lh140 mt20 black-clr">Built-In High-Speed SMTP to Deliver Emails Directly into Inbox</div>
                  </div>
               </div>
            </div>

            <div class="col">
               <div class="figure">
               <img src="assets/images/dfy2.webp" alt="DFY Websites" class="mx-auto d-block img-fluid ">
                  <div class="figure-caption text-center">
                     <div class="f-18 f-md-22 w500 lh140 mt20 black-clr">Done-For-You Leads to Kickstart Your Email Marketing Journey & Enjoy Instant Profit</div>
                  </div>
               </div>
            </div>

            <div class="col">
               <div class="figure">
               <img src="assets/images/dfy2.webp" alt="DFY Websites" class="mx-auto d-block img-fluid ">
                  <div class="figure-caption text-center">
                     <div class="f-18 f-md-22 w500 lh140 mt20 black-clr">Send & Scheduled Unlimited Emails With Inboxing</div>
                  </div>
               </div>
            </div>

            <div class="col">
               <div class="figure">
               <img src="assets/images/dfy8.webp" alt="DFY Websites" class="mx-auto d-block img-fluid ">
                  <div class="figure-caption text-center">
                     <div class="f-18 f-md-22 w500 lh140 mt20 black-clr">Say Goodbye To Expensive <br class="d-none d-md-block"> Email Writers.</div>
                  </div>
               </div>
            </div>

            <div class="col">
               <div class="figure">
               <img src="assets/images/dfy3.webp" alt="DFY Websites" class="mx-auto d-block img-fluid ">
                  <div class="figure-caption text-center">
                     <div class="f-18 f-md-22 w500 lh140 mt20 black-clr">Create High Quality Emails & Messages With Just Single Keyword</div>
                  </div>
               </div>
            </div>

            <div class="col">
               <div class="figure">
               <img src="assets/images/dfy4.webp" alt="DFY Websites" class="mx-auto d-block img-fluid ">
                  <div class="figure-caption text-center">
                     <div class="f-18 f-md-22 w500 lh140 mt20 black-clr">Stop Paying For Expensive Autoresponder Tools.</div>
                  </div>
               </div>
            </div>

            <!-- <div class="col">
               <div class="figure">
               <img src="assets/images/dfy5.webp" alt="DFY Websites" class="mx-auto d-block img-fluid ">
                  <div class="figure-caption text-center">
                     <div class="f-18 f-md-22 w500 lh140 mt20 black-clr">Automailx AI Is One Stop Solution For All Your Email Marketing Needs</div>
                  </div>
               </div>
            </div> -->

            <div class="col ">
               <div class="figure">
               <img src="assets/images/dfy6.webp" alt="DFY Websites" class="mx-auto d-block img-fluid ">
                  <div class="figure-caption text-center">
                     <div class="f-18 f-md-22 w500 lh140 mt20 black-clr">Stop Paying Hundreds Of Dollars For Expensive AI Tools</div>
                  </div>
               </div>
            </div>

            <div class="col">
               <div class="figure">
               <img src="assets/images/dfy7.webp" alt="DFY Websites" class="mx-auto d-block img-fluid ">
                  <div class="figure-caption text-center">
                     <div class="f-18 f-md-22 w500 lh140 mt20 black-clr">Commercial License Included To Sell & Earn Extra Profits</div>
                  </div>
               </div>
            </div>

            
         </div>
      </div>
   </div>

      
      <div class="cta-section-new">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <!-- <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/limited-time-offer-banner.webp" class="img-fluid d-block mx-auto"> -->
                 <div class="f-22 f-md-34 w500 lh140 text-center white-clr">
                           Grab AutomailX Now for <strike>$99/Month,</strike> Today Only 1-Time $17
                           </div>
                  <div class="f-18 f-md-20 w400 lh140 text-center white-clr mt20">
                     (Get Free 30 Reseller License + FREE Commercial License)
                  </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 text-center">
                        <a href="#buynow" class="cta-link-btn">Get Instant Access To AutomailX</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20">
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/compaitable-with1.webp" class="img-fluid d-block mx-xs-center md-img-right">
                     <div class="d-md-block d-none px-md30"><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/v-line.webp" class="img-fluid"></div>
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/days-gurantee1.webp" class="img-fluid d-block mx-xs-auto mt15 mt-md0">
                  </div>
               </div>
            </div>
         </div>
      </div>

     <div class="warning">
         <div class="container">
            <div class="row text-center">
                 <div class="col-md-12 d-flex align-items-center justify-content-center flex-wrap text-center">
                    <img src="assets/images/warning.png" class="img-fluid d-block ml-auto mr10">
                    <div class="f-22 f-md-28 w400 lh140 text-center white-clr ">
                  <span class="w700">WARNING:</span>   Grab The Offer Before Timer Hits Zero</div>
                    </div>
                </div>
            </div>
        </div>

      <section class="unlock-opportunities-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="black-clr w700 f-28 f-md-35 unlimited-head">
                     <div class="unlimited-blub">
                        With AutomailX, Tap into Endless Earning Opportunities...
                     </div>
                  </div>
               </div>
            </div>
            <div class="row row-cols-1 row-cols-md-3 gap30  justify-content-center mt20 mt-md50">
               <div class="col">
                  <img src="assets/images/op3.webp" class="img-fluid mx-auto d-block">
                  <div class="balck-clr f-18 f-md-20 w700 lh140 mt20 text-center">
                     	Promote Any Product as An Affiliate from Warrior+, JVZoo, ClickBank
                  </div>
               </div>
               <div class="col">
                  <img src="assets/images/op5.webp" class="img-fluid mx-auto d-block">
                  <div class="balck-clr f-18 f-md-20 w700 lh140 mt20 text-center">
                     Create And Sell Your Own Courses, E-Book, or Digital Products
                  </div>
               </div>
               <div class="col">
                  <img src="assets/images/op9.webp" class="img-fluid mx-auto d-block">
                  <div class="balck-clr f-18 f-md-20 w700 lh140 mt20 text-center">
                     Earn More with Cross-Selling Similar Product/Services with Emails
                  </div>
               </div>
               <div class="col">
                  <img src="assets/images/op2.webp" class="img-fluid mx-auto d-block">
                  <div class="balck-clr f-18 f-md-20 w700 lh140 mt20 text-center">
                     Start Email Marketing Agency & Charge Customers for Your Services
                  </div>
               </div>
               <div class="col">
                  <img src="assets/images/op8.webp" class="img-fluid mx-auto d-block">
                  <div class="balck-clr f-18 f-md-20 w700 lh140 mt20 text-center">
                    	Offer Paid Newsletter Services by Charging Subscription Fee 
                  </div>
               </div>
               <div class="col">
                  <img src="assets/images/op7.webp" class="img-fluid mx-auto d-block">
                  <div class="balck-clr f-18 f-md-20 w700 lh140 mt20 text-center">
                     	Craft Personalized, Engaging, High Converting Email Swipes and Sell Them
                  </div>
               </div>
               
            </div>
         </div>
      </section>

    
      <div class="benefit-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-28 f-md-45 w700 lh140 text-center white-clr">So, Who Can Benefit From AutomailX? </div>
               </div>
            </div>
            <div class="row row-cols-1 gap30 mt0 mt-md20">
               <div class="col">
                  
               </div>
            </div>
            <div class="row row-cols-1 row-cols-md-3 gap30 mt0">
               <div class="col">
                  <div class="fea-white">
                     <img src="assets/images/b1.webp" class="img-fluid mx-auto d-block" alt="">
                     <div class="f-18 f-md-20 w600 lh140 grey-clr mt10">
                        	Any Internet Marketer, Regardless of The Niche. AutomailX Helps Them Boost Inboxing, Clicks & Sales.
                     </div>
                  </div>
               </div>
               <div class="col">
                   <div class="fea-white">
                     <img src="assets/images/b2.webp" class="img-fluid mx-auto d-block" alt="">
                     <div class="f-18 f-md-20 w600 lh140 grey-clr mt10">
                        		List Builders Skyrocket Their Subscribers’ Numbers... All At The Push Of A Button.
                     </div>
                  </div>
               </div>
               <div class="col">
                   <div class="fea-white">
                     <img src="assets/images/b3.webp" class="img-fluid mx-auto d-block" alt="">
                     <div class="f-18 f-md-20 w600 lh140 grey-clr mt10">
                        	Anyone Who Wants To Streamline Their Business While Focusing On Bigger Projects.
                     </div>
                  </div>
               </div>
            </div>
            
            
            <div class="row row-cols-1 row-cols-md-3 gap30 mt0">
               <div class="col">
                  <div class="fea-white">
                     <img src="assets/images/b4.webp" class="img-fluid mx-auto d-block" alt="">
                     <div class="f-18 f-md-20 w600 lh140 grey-clr mt10">
                        	Affiliate Marketers Who Don't Have Any Product Or Service To Sell
                     </div>
                  </div>
               </div>
               <div class="col">
                   <div class="fea-white">
                     <img src="assets/images/b5.webp" class="img-fluid mx-auto d-block" alt="">
                     <div class="f-18 f-md-20 w600 lh140 grey-clr mt10">
                        	Product/Service/Course Sellers To Boost Their Sales & Customer Satisfaction
                     </div>
                  </div>
               </div>
               <div class="col">
                   <div class="fea-white">
                     <img src="assets/images/b6.webp" class="img-fluid mx-auto d-block" alt="">
                     <div class="f-18 f-md-20 w600 lh140 grey-clr mt10">
                    	Online Marketing Agency To Provide Services To Local Businesses & Build Income
                     </div>
                  </div>
               </div>
            </div>
            
            <div class="row row-cols-1 row-cols-md-3 gap30 mt0">
               <div class="col">
                  <div class="fea-white">
                     <img src="assets/images/b7.webp" class="img-fluid mx-auto d-block" alt="">
                     <div class="f-18 f-md-20 w600 lh140 grey-clr mt10">
                    	Local Businesses To Automate Their Client Communication.
                     </div>
                  </div>
               </div>
               <div class="col">
                   <div class="fea-white">
                     <img src="assets/images/b8.webp" class="img-fluid mx-auto d-block" alt="">
                     <div class="f-18 f-md-20 w600 lh140 grey-clr mt10">
                    	Lazy People Who Are Reluctant To Work
                     </div>
                  </div>
               </div>
               <div class="col">
                   <div class="fea-white">
                     <img src="assets/images/b9.webp" class="img-fluid mx-auto d-block" alt="">
                     <div class="f-18 f-md-20 w600 lh140 grey-clr mt10">
                        Anyone Who Wants To Value Their Business And Money 
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

      
      <!-- Cta Section -->
       <div class="cta-section-new">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <!-- <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/limited-time-offer-banner.webp" class="img-fluid d-block mx-auto"> -->
                 <div class="f-22 f-md-34 w500 lh140 text-center white-clr">
                           Grab AutomailX Now for <strike>$99/Month,</strike> Today Only 1-Time $17
                           </div>
                  <div class="f-18 f-md-22 w500 lh140 text-center blue-gradient mt10">
                     (Get Free 30 Reseller License + FREE Commercial License)
                  </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 text-center">
                        <a href="#buynow" class="cta-link-btn">Get Instant Access To AutomailX</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20">
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/compaitable-with1.webp" class="img-fluid d-block mx-xs-center md-img-right">
                     <div class="d-md-block d-none px-md30"><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/v-line.webp" class="img-fluid"></div>
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/days-gurantee1.webp" class="img-fluid d-block mx-xs-auto mt15 mt-md0">
                  </div>
               </div>
            </div>
         </div>
      </div> 
      <div class="warning">
         <div class="container">
            <div class="row text-center">
                 <div class="col-md-12 d-flex align-items-center justify-content-center flex-wrap text-center">
                    <img src="assets/images/warning.png" class="img-fluid d-block ml-auto mr10">
                    <div class="f-22 f-md-28 w400 lh140 text-center white-clr ">
                  <span class="w700">WARNING:</span>   Grab The Offer Before Timer Hits Zero</div>
                    </div>
                </div>
            </div>
        </div>
      <!-- Cta Section End -->
    
    
     
      <section class="still-think">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-43 w700 lh140 black-clr">
                  Say GoodBye to Expensive & Complex Software
                  </div>
                  <div class="mt10">
                     <img src="assets/images/hline.webp" alt="" class="mx-auto d-block img-fluid">
                  </div>
                  <div class="f-24 f-md-28 w400 lh140 black-clr mt20">
                  Save Thousands of Dollars Per Month Easily with AutomailX rather than spending on expensive 3rd party tools
                  </div>
                  <div class="f-24 f-md-28 w700 lh140 white-clr still-headline mt20 mt-md50">
                     Content Creation Platform&nbsp;
                  </div>
               </div>
            </div>

            <div class="row row-cols-1 row-cols-md-3 gap30  justify-content-center mt20 mt-md20">
               <div class="col">
                  <img src="assets/images/email1.webp" class="img-fluid mx-auto d-block">
               </div>
               <div class="col">
                  <img src="assets/images/email2.webp" class="img-fluid mx-auto d-block">
               </div>
               <div class="col">
                  <img src="assets/images/email3.webp" class="img-fluid mx-auto d-block">
               </div>
            </div>
            <div class="row mt20 mt-md80">
               <div class="col-12 text-center">
                  <div class="f-24 f-md-28 w700 lh140 white-clr still-headline">
                  Email Marketing Platform
                  </div>
               </div>
            </div>
            <div class="row row-cols-1 row-cols-md-3 gap30  justify-content-center mt20 mt-md50">
               <div class="col">
                  <img src="assets/images/email-marketing1.webp" class="img-fluid mx-auto d-block">
               </div>
               <div class="col">
                  <img src="assets/images/email-marketing2.webp" class="img-fluid mx-auto d-block">
               </div>
               <div class="col">
                  <img src="assets/images/email-marketing3.webp" class="img-fluid mx-auto d-block">
               </div>
               <div class="col">
                  <img src="assets/images/email-marketing4.webp" class="img-fluid mx-auto d-block">
               </div>
               <div class="col">
                  <img src="assets/images/email-marketing5.webp" class="img-fluid mx-auto d-block">
               </div>
               <div class="col">
                  <img src="assets/images/email-marketing6.webp" class="img-fluid mx-auto d-block">
               </div>
            </div>
            <div class="row mt20 mt-md80">
               <div class="col-12 text-center">
                  <div class="f-24 f-md-28 w700 lh140 white-clr still-headline">
                  SMTP Provider Platforms
                  </div>
               </div>
            </div>
            <div class="row row-cols-1 row-cols-md-3 gap30  justify-content-center mt20 mt-md50">
               <div class="col">
                  <img src="assets/images/smtp1.webp" class="img-fluid mx-auto d-block">
               </div>
               <div class="col">
                  <img src="assets/images/smtp2.webp" class="img-fluid mx-auto d-block">
               </div>
               <div class="col">
                  <img src="assets/images/smtp3.webp" class="img-fluid mx-auto d-block">
               </div>
               <div class="col">
                  <img src="assets/images/smtp4.webp" class="img-fluid mx-auto d-block">
               </div>
               <div class="col">
                  <img src="assets/images/smtp5.webp" class="img-fluid mx-auto d-block">
               </div>
               <div class="col">
                  <img src="assets/images/smtp6.webp" class="img-fluid mx-auto d-block">
               </div>
            </div>
         </div>
      </section>
      <div class="cta-section-new">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-22 f-md-34 w500 lh140 text-center white-clr">
                           Grab AutomailX Now for <strike>$99/Month,</strike> Today Only 1-Time $17
                           </div>
                  <div class="f-18 f-md-20 w400 lh140 text-center white-clr mt20">
                     (Get Free 30 Reseller License + FREE Commercial License)
                  </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 text-center">
                        <a href="#buynow" class="cta-link-btn">Get Instant Access To AutomailX</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20">
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/compaitable-with1.webp" class="img-fluid d-block mx-xs-center md-img-right">
                     <div class="d-md-block d-none px-md30"><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/v-line.webp" class="img-fluid"></div>
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/days-gurantee1.webp" class="img-fluid d-block mx-xs-auto mt15 mt-md0">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="warning">
         <div class="container">
            <div class="row text-center">
                 <div class="col-md-12 d-flex align-items-center justify-content-center flex-wrap text-center">
                    <img src="assets/images/warning.png" class="img-fluid d-block ml-auto mr10">
                    <div class="f-22 f-md-28 w400 lh140 text-center white-clr ">
                  <span class="w700">WARNING:</span>   Grab The Offer Before Timer Hits Zero</div>
                    </div>
                </div>
            </div>
        </div>
      <section class="comp-sec">
         <div class="container">
            <div class="row ">
               <div class="col-12">
                  <div class="f-md-55 f-28 w700 lh140 text-capitalize text-center black-clr">
                     Now Is the Time to Take Decision.
                  </div>
                  <div class="f-md-36 f-20 w500 lh140 text-capitalize text-center black-clr mt20">
                    Make Your Choice. Your Future...
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md50 row-cols-1 row-cols-md-2">
                  <div class="col relative">
                     <div class="compare-red-wrap">
                        <div class="f-md-32 f-22 w700 lh140 text-center"> <u>Without AutomailX</u> </div>
                        <br>
                       <div class="f-md-24 f-20 w600 lh140 text-center"> Keep struggling with multiple software - spend your hundreds of dollars and hours of time.</div>
                        <ul class="compare-red pl0 f-20 f-md-22 w400 lh160 black-clr text-capitalize mt20">
                      <li>  Keep Paying Big Amount To Expensive Email Writers...</li>
                       <li> Continuously managing costly monthly bills for SMTP services.</li>
                      <li>  Stuck in the slow and time-consuming process of list building, slowing down your business growth.</li>
                      <li>  Keep Paying Monthly To Hefty Autoresponders With Limited Email Sending Plans</li>
                      <li>  Get Sued For Violating Copyright Laws If Try To Copy Content...</li>
                      <li>  Investing Countless Hours In Research And Information Gathering</li>
                       <li> Crafting Personalized Email Is Difficult...</li>
                      <li>  Hiring Experts To Setup SMTP, Email Scheduling, & Writing Emails Cost Thousands.</li>
                       <li> It’s Quite Time-Consuming & Painful To Do All The Tasks Manually</li>

                           
                        </ul>                        
                     </div>
                  </div>
                  <div class="col mt20 mt-md0 relative">
                     <div class="compare-red-wrap1">
                        <div class="f-md-32 f-22 w700 lh140 text-center"> <u>With AutomailX</u> </div> <br>
                         <div class="f-md-24 f-20 w600 lh140 text-center"> Pay Only One-Time for Automated Content Writing, DFY Leads, Inbuilt SMTP, Unlimited Mailing & Sales for Lifetime.</div>
                        <ul class="compare-red1 pl0 f-20 f-md-24 w500 lh160 black-clr text-capitalize mt20">
                      <li>  Get This Fully AI Driven Cloud Based Platform Paying One Time Only</li>
                      <li>  Effortlessly Create Engaging Emails Using Ai With Just A Single Keyword</li>
                     <li>  Send Your Emails Using Built-In High-Speed SMTP For Immediate Mailing Upon List Upload.</li>
                     <li>   Done-For- You Leads to Kickstart Your Email Marketing Journey & Enjoy Instant Profit</li>
                      <li>  Send & Scheduled Unlimited Emails By Paying One Time Only</li>
                       <li> Get Fully Copyright Free Content And Can Be Used Anywhere</li>
                      
                       <li> Leverage  AI To Craft Personalized Emails Easily In Few Seconds</li>
                       <li> No Need To Hire Anyone. AutomailX Is One Stop Solution For Email Marketing</li>
                       <li> Enjoy You Time And Live Your Dream Laptop Lifestyle</li>
                           
                        </ul>
                     </div>
                  </div>
               </div>
         </div>
      </section>
      
      <div class="cta-section-new">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-22 f-md-34 w500 lh140 text-center white-clr">
                           Grab AutomailX Now for <strike>$99/Month,</strike> Today Only 1-Time $17
                           </div>
                  <div class="f-18 f-md-20 w400 lh140 text-center white-clr mt20">
                     (Get Free 30 Reseller License + FREE Commercial License)
                  </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 text-center">
                        <a href="#buynow" class="cta-link-btn">Get Instant Access To AutomailX</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20">
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/compaitable-with1.webp" class="img-fluid d-block mx-xs-center md-img-right">
                     <div class="d-md-block d-none px-md30"><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/v-line.webp" class="img-fluid"></div>
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/days-gurantee1.webp" class="img-fluid d-block mx-xs-auto mt15 mt-md0">
                  </div>
               </div>
            </div>
         </div>
      </div>
      
       <div class="warning">
         <div class="container">
            <div class="row text-center">
                 <div class="col-md-12 d-flex align-items-center justify-content-center flex-wrap text-center">
                    <img src="assets/images/warning.png" class="img-fluid d-block ml-auto mr10">
                    <div class="f-22 f-md-28 w400 lh140 text-center white-clr ">
                  <span class="w700">WARNING:</span>   Grab The Offer Before Timer Hits Zero</div>
                    </div>
                </div>
            </div>
        </div>
      
       <div class="travel-section">
        
              <img src="assets/images/travel-section.png" class="img-fluid d-block mx-auto"></div>
             </div>
             
     <div class="cta-section-new">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-22 f-md-34 w500 lh140 text-center white-clr">
                           Grab AutomailX Now for <strike>$99/Month,</strike> Today Only 1-Time $17
                           </div>
                  <div class="f-18 f-md-20 w400 lh140 text-center white-clr mt20">
                     (Get Free 30 Reseller License + FREE Commercial License)
                  </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 text-center">
                        <a href="#buynow" class="cta-link-btn">Get Instant Access To AutomailX</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20">
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/compaitable-with1.webp" class="img-fluid d-block mx-xs-center md-img-right">
                     <div class="d-md-block d-none px-md30"><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/v-line.webp" class="img-fluid"></div>
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/days-gurantee1.webp" class="img-fluid d-block mx-xs-auto mt15 mt-md0">
                  </div>
               </div>
            </div>
         </div>
      </div>
      
    <div class="warning">
         <div class="container">
            <div class="row text-center">
                 <div class="col-md-12 d-flex align-items-center justify-content-center flex-wrap text-center">
                    <img src="assets/images/warning.png" class="img-fluid d-block ml-auto mr10">
                    <div class="f-22 f-md-28 w400 lh140 text-center white-clr ">
                  <span class="w700">WARNING:</span>   Grab The Offer Before Timer Hits Zero</div>
                    </div>
                </div>
            </div>
        </div>
      <div class="bonus-section">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-12">
                  <img src="assets/images/bonus-shape.webp" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 col-md-12 mt15 mt-md20">
                  <div class="f-18 f-md-32 w500 lh140 text-center">
                      
                     When You Grab Your AutomailX Account Today, <br class="d-none d-md-block">You'll Also Get These <span class="w600">Fast Action Bonuses!</span>
                  </div>
               </div>
               <div class="col-12 col-md-12 mt60 mt-md100">
                  <div class="bonus-section-shape text-center">
                     <div class="col-12 margin-t-60">
                        <div class="bonus-headline">
                           <div class="f-md-24 f-20 w700 white-clr text-nowrap text-center">
                              Bonus #1
                           </div>
                        </div>
                     </div>
                     <div class="row d-flex align-items-center flex-wrap mt15 mt-md30">
                        <div class="col-12 col-md-7 order-md-2">
                           <div class="w700 f-22 f-md-24 black-clr lh140 text-start">
                              Email Marketing DFY Business (with PLR Rights)
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-start">
                              Both online and offline marketers can make a killing using this up-to-date Email Marketing DFY Business. Our step-by-step Email Marketing exclusive training is going to take you and your customers by the hand and show you how to get some amazing marketing results in the shortest time ever. </div>
                        <ul class="revo-list1 pl0">
                                          <li>Top quality training to sell under your name!</li>
                                          <li>Hottest &amp; proven-seller topic on the web!</li>
                                          <li>Ready-to-go sales material to start selling today!</li>
                                          <li>Sell unlimited copies!</li>
                                       </ul>
                        </div>  
                        
                        <div class="col-12 col-md-5 order-md-1 mt20 mt-md0">
                           <img src="assets/images/bonus-box.webp" class="img-fluid d-block mx-auto max-h450" alt="Bonus">
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-12 mt60 mt-md100">
                  <div class="bonus-section-shape text-center">
                     <div class="col-12 margin-t-60">
                        <div class="bonus-headline">
                           <div class="f-md-24 f-20 w700 white-clr text-nowrap text-center">
                              Bonus #2
                           </div>
                        </div>
                     </div>
                     <div class="row d-flex align-items-center flex-wrap mt15 mt-md30">
                        <div class="col-12 col-md-7">
                           <div class="w700 f-22 f-md-24 black-clr lh140 text-start">
                              Progressive List Building DFY Business (with PLR Rights)
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-start">
                             Both online and offline marketers can make a killing using this up-to-date Progressive List Building DFY Business training. Our step-by-step Progressive List Building DFY Business exclusive training is going to take you and your customers by the hand and show you how to get some amazing marketing results in the shortest time ever.
                            <ul class="revo-list1 pl0">
                                          <li>Top quality training to sell under your name!</li>
                                          <li>Hottest &amp; proven-seller topic on the web!</li>
                                          <li>Ready-to-go sales material to start selling today!</li>
                                          <li>Sell unlimited copies!</li>
                                       </ul>
                           </div>
                        </div>
                        <div class="col-12 col-md-5 mt20 mt-md0">
                           <img src="assets/images/bonus-box.webp" class="img-fluid d-block mx-auto max-h450" alt="Bonus">
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-12 mt60 mt-md100">
                  <div class="bonus-section-shape text-center">
                     <div class="col-12 margin-t-60">
                        <div class="bonus-headline">
                           <div class="f-md-24 f-20 w700 white-clr text-nowrap text-center">
                              Bonus #3
                           </div>
                        </div>
                     </div>
                     <div class="row d-flex align-items-center flex-wrap mt15 mt-md30">
                        <div class="col-12 col-md-7 order-md-2">
                           <div class="w700 f-22 f-md-24 black-clr lh140 text-start">
                              Affiliate Marketing Made Easy
                           </div>
                           <div class="f-18 f-md-20 w400 lh140 mt10 text-start">
                              Marketers do not want to waste their money, time and effort just for not doing, and even worse, for not knowing how to do Affiliate Marketing!
<br><br>
Our step-by-step Affiliate Marketing Training System is going to take you and your customers by the hand and show you how to make as much money as possible, in the shortest time ever with Affiliate Marketing online. </div>
                        </div>
                        <div class="col-12 col-md-5 mt20 mt-md0 order-md-1">
                           <img src="assets/images/bonus-box.webp" class="img-fluid d-block mx-auto max-h450" alt="Bonus">
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-12 mt60 mt-md100">
                  <div class="bonus-section-shape text-center">
                     <div class="col-12 margin-t-60">
                        <div class="bonus-headline">
                           <div class="f-md-24 f-20 w700 white-clr text-nowrap text-center">
                              Bonus #4
                           </div>
                        </div>
                     </div>
                     <div class="row d-flex align-items-center flex-wrap mt15 mt-md30">
                        <div class="col-12 col-md-7">
                           <div class="w700 f-22 f-md-24 black-clr lh140 text-start">
                            Auto Video Creator - Create your own professional videos!
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-start">
                              Uncover the secrets to create your own professional videos in minutes with this easy to use software. You don't even have to speak ... the software will do it for you!
Auto Video Creator - Create your own professional videos in a snap! </div>
                        </div>
                        <div class="col-12 col-md-5 mt20 mt-md0">
                           <img src="assets/images/bonus-box.webp" class="img-fluid d-block mx-auto max-h450" alt="Bonus">
                        </div>
                     </div>
                  </div>
               </div>
              
               <div class="col-12 text-center">
                  <div class="bonus-gradient">
                     <div class="f-22 f-md-36 w700 white-clr lh240 text-center">
                        That's A Total Value of <br class="d-none d-md-block"><span class="f-60 f-md-100 w700">$3300</span>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--Bonus Section ends -->
     
      <!-- License Section Start -->
      <div class="license-section">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-12 col-md-6">
                  <div class="f-28 f-md-45 w700 lh140 black-clr text-center text-md-start">
                     Also, Get Free Commercial License 
                  </div>
                  <div class="f-md-20 f-18 w400 lh140 black-clr mt20 text-center text-md-start">
                     AutomailX comes with Commercial use license so that you can use AutomailX for commercial purposes and sell services to your clients.
                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                  <img src="assets/images/commercial-license.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>
         </div>
      </div>
       
        <!-- Cta Section -->
       <div class="cta-section-new">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <!-- <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/limited-time-offer-banner.webp" class="img-fluid d-block mx-auto"> -->
                 <div class="f-22 f-md-34 w500 lh140 text-center white-clr">
                           Grab AutomailX Now for <strike>$99/Month,</strike> Today Only 1-Time $17
                           </div>
                  <div class="f-18 f-md-22 w500 lh140 text-center blue-gradient mt10">
                     (Get Free 30 Reseller License + FREE Commercial License)
                  </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 text-center">
                        <a href="#buynow" class="cta-link-btn">Get Instant Access To AutomailX</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20">
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/compaitable-with1.webp" class="img-fluid d-block mx-xs-center md-img-right">
                     <div class="d-md-block d-none px-md30"><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/v-line.webp" class="img-fluid"></div>
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/days-gurantee1.webp" class="img-fluid d-block mx-xs-auto mt15 mt-md0">
                  </div>
               </div>
            </div>
         </div>
      </div> 
      <div class="warning">
         <div class="container">
            <div class="row text-center">
                 <div class="col-md-12 d-flex align-items-center justify-content-center flex-wrap text-center">
                    <img src="assets/images/warning.png" class="img-fluid d-block ml-auto mr10">
                    <div class="f-22 f-md-28 w400 lh140 text-center white-clr ">
                  <span class="w700">WARNING:</span>   Grab The Offer Before Timer Hits Zero</div>
                    </div>
                </div>
            </div>
        </div>
      <!-- Cta Section End -->
        <section class="best-part-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-45 w700 lh140 black-clr best-text  d-flex align-items-center justify-content-center">
                  This Is Your Chance To Turn Worries into<br class="d-none d-md-block"> Income Opportunity!
                  <img src="assets/images/not-believe-smile.webp" alt="" class="img-fluid d-block ml15">
                  </div>
                  <img src="assets/images/best-line.webp" alt="Best Line" class="mx-auto d-block img-fluid mt20">
                  <div class="mt20 mt-md30 lh140 f-22 f-md-28 w600">
                  Just 3 Clicks It Takes to Start Profiting with the Power of Artificial Intelligence… 
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md90">
               <div class="col-md-6">
                  <ul class="f-18 f-md-20 lh140 black-clr best-list pl0 m0 w400">
                     <li><span class="w600">	No Need To Pay </span>To Expensive Content Creation Platforms...</li>
                     <li><span class="w600">	No More Wasting Time</span> With The Old-School Autoresponder Tools</li>
                     <li><span class="w600">	No More Feeling Bankrupt</span> Paying Money Month After Month To Email Marketing Service Providers That Cost A Fortune</li>
                     <li><span class="w600">	No More Losing Your List</span> While Importing So Don't Lose Money Instead Make More Profits</li>
                     <li><span class="w600">	Stop Paying Monthly </span>for High Price SMTP’s</li>
                     <li><span class="w600">	No More Waiting Game </span>To Get Authentic Results</li>
                     <li><span class="w600">	No Limits on Sending Emails </span>So Send Emails As Often As You’d Like With NO Downtime.</li>
                     <li><span class="w600">	It’s Very Easy To Setup</span> Regardless Of Prior Technical Experience.</li>
                     <li>	It’s Your Chance to Take Control of Your Life - <span class="w600">AutomailX Puts You on Driver Seat</span></li>
                     <li><span class="w600">	Finally Become Your Own BOSS</span></li>
                  </ul>
               </div>
               <div class="col-md-6">
                  <img src="assets/images/Say-good-bye.webp" alt="Say Good Bye" class="img-fluid d-block mx-auto">
               </div>
            </div>
            <div class="mt20 mt-md30 lh140 f-18 f-md-24 w300 text-center">
                <i> So, what are you waiting for? Click the button below to grab your copy of AutomailX now… </i>
                  </div>
         </div>
      </section>
      
       
        <!-- Cta Section -->
       <div class="cta-section-new">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <!-- <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/limited-time-offer-banner.webp" class="img-fluid d-block mx-auto"> -->
                 <div class="f-22 f-md-34 w500 lh140 text-center white-clr">
                           Grab AutomailX Now for <strike>$99/Month,</strike> Today Only 1-Time $17
                           </div>
                  <div class="f-18 f-md-22 w500 lh140 text-center blue-gradient mt10">
                     (Get Free 30 Reseller License + FREE Commercial License)
                  </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 text-center">
                        <a href="#buynow" class="cta-link-btn">Get Instant Access To AutomailX</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20">
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/compaitable-with1.webp" class="img-fluid d-block mx-xs-center md-img-right">
                     <div class="d-md-block d-none px-md30"><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/v-line.webp" class="img-fluid"></div>
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/days-gurantee1.webp" class="img-fluid d-block mx-xs-auto mt15 mt-md0">
                  </div>
               </div>
            </div>
         </div>
      </div> 
      <div class="warning">
         <div class="container">
            <div class="row text-center">
                 <div class="col-md-12 d-flex align-items-center justify-content-center flex-wrap text-center">
                    <img src="assets/images/warning.png" class="img-fluid d-block ml-auto mr10">
                    <div class="f-22 f-md-28 w400 lh140 text-center white-clr ">
                  <span class="w700">WARNING:</span>   Grab The Offer Before Timer Hits Zero</div>
                    </div>
                </div>
            </div>
        </div>
      <!-- Cta Section End -->
      <!-- Guarantee Section Start -->
     <div class="riskfree-section ">
         <div class="container ">
            <div class="row align-items-center ">
               <div class="f-28 f-md-45 w400 lh140 white-clr text-center col-12 mb-md40">
                  We’re Backing Everything Up with An Iron Clad... <br class="d-none d-md-block"><span class="w600 blue-gradient">"30-Day Risk-Free Money Back Guarantee"</span>
               </div>
               <div class="col-md-7 col-12 order-md-2">
                  <div class="f-md-20 f-18 w400 lh140 white-clr mt15 ">
                     I'm 100% confident that AutomailX will help you take your online business to the next level. I'm so confident on it that I'm making this offer a risk-free investment for you.
                     <br><br>
                     If you concluded that, HONESTLY nothing of this has helped you in any way, you can take advantage of our "30-day Money Back Guarantee" and simply ask for a refund within 30 days!
                     <br><br>
                     Note: For refund, we will require a valid reason along with the proof that you tried our system, but it didn't work for you!
                     <br><br>
                     I am considering your money to be kept safe on the table between us and waiting for you to APPLY and successfully use this product, and get best results, so then you can feel it is a great investment.
                  </div>
               </div>
               <div class="col-md-5 order-md-1 col-12 mt20 mt-md0 ">
                  <img src="assets/images/risk-free.webp " class="img-fluid d-block mx-auto ">
               </div>
            </div>
         </div>
      </div>
      <!-- Guarantee Section End -->
      <!-- Discounted Price Section Start -->
     <div class="table-section" id="buynow">
         <div class="container">
            <div class="row ">
               <div class="col-12 ">
                  <div class="f-20 f-md-24 w400 text-center lh140">
                     Take Advantage of Our Limited Time Deal
                  </div>
                  <div class="f-md-45 f-28 w600 lh140 text-center mt10 ">
                     Get AutomailX for A Low One-Time-
                     <br class="d-none d-md-block "> Price, No Monthly Fee.
                  </div>
               </div>
               <div class="col-12 col-md-9 mx-auto mt20 mt-md50 ">
                  <div class="row ">
                     <div class="col-12 col-md-12 mt20 mt-md0">
                        <div class="table-wrap1 ">
                           <div class="table-head1 " id="buynow">
                           <div class="f-28 f-md-45 lh140 white-clr w700">
                              Today with AutomailX, You're Getting
                            </div>
                           </div>
                           <div class=" ">
                              <ul class="table-list1 pl0 f-18 f-md-20 w500 text-center lh140 black-clr mt15">
                            <li>  Complete Email Marketing Solution With Zero Recurring Fees - <span class="w700">Worth $997</span></li>
                            <li>  Done-For-You Leads for Instant Mailing & Profit - <span class="w700">Worth $997</span></li>
                             <li> Built-In High-Speed SMTP to Deliver Emails Directly into Inbox - <span class="w700">Worth $697</span></li>
                            <li>  Zero Server Downtime Headaches Ever -<span class="w700"> Worth $497</span></li>
                            <li>  AI-Powered Email Copywriter for High-Converting & Profit Pulling Email Content-<span class="w700"> Worth $997</span></li>
                            <li>  AI Enabled Smart Tagging For Lead Personalization & Traffic -<span class="w700"> Worth $497</span></li>
                            <li>  Upload Unlimited List with Zero Restrictions - <span class="w700">Worth $497</span></li>
                            <li>  Design Newsletters & Autoresponders Using The Power Of AI - <span class="w700">Worth $497</span></li>
                            <li>  Collect Unlimited Leads With Built-In Lead Form -<span class="w700"> Worth $297</span></li>
                            <li>  Boost Email Delivery, Click And Open Rate - <span class="w700">Worth $297</span></li>
                            <li>  100+ High Converting Templates For Webforms & Email -<span class="w700"> Worth $497</span></li>
                            <li>  100% Mobile Responsive Emails for Maximum Reach -<span class="w700"> Worth $497</span></li>
                             <li> Have 100% Control on Your Online Business  -<span class="w700"> Beyond A Value</span></li>
                            <li>  User Friendly, Cloud Based App. Start within A Minute -<span class="w700"> That's Priceless</span></li>
                            <li>  GDPR & Can-Spam Compliant - <span class="w700">Worth $297</span></li>
                            <li>  Pay Once & Get Profit Forever With No Restrictions -<span class="w700"> Beyond A Value</span></li>
                            <li>  100% Newbie Friendly -<span class="w700"> Beyond A Value</span></li>
                            <li>  Step-By-Step Training Videos -<span class="w700"> Worth $497</span></li>
                            <li>  Round-The-Clock World-Class Support - <span class="w700">(Priceless – It's Worth A LOT)</span></li>
                             <li> Commercial License included</li>
                              </ul>
                           </div>
                           <div class="table-btn ">
                              <div class="f-22 f-md-28 w400 lh140 black-clr">
                                 Total Value of Everything
                              </div>
                              <div class="f-26 f-md-36 w700 lh140 black-clr mt12">
                                 YOU GET TODAY:
                              </div>
                              <div class="f-28 f-md-60 w700 red-clr mt12 l140">
                                 $8,058.00
                              </div>
                              <div class="f-22 f-md-28 w400 lh140 black-clr mt20">
                                 For Limited Time Only, Grab It For:
                              </div>
                              <div class="f-21 f-md-40 w700 lh140 red-clr mt20">
                              <del>$99 Monthly</del> 
                              </div>
                              <div class="f-24 f-md-34 w700 lh140 black-clr mt20">
                                 Today, <span class="blue-clr">Just $17 One-Time</span> 
                              </div>
                              
                              <div class="col-12 mx-auto text-center">
                              <a href="https://warriorplus.com/o2/buy/qy9f4y/nb5h2m/w61hlz"  id="buyButton" class="cta-link-btn mt20"> Get Instant Access To AutomailX</a>
                                    </div>
                              
                           </div>
                           
                           <div class="table-border-content mt20">
                              <div class="tb-check d-flex align-items-center f-18">
                                 <label class="checkbox inline">
                                    <img src="https://assets.clickfunnels.com/templates/listhacking-sales/images/arrow-flash-small.gif" alt="" style="margin-top:-5px;">
                                    <input type="checkbox" id="check" onclick="myFunction()" style="width:16px !important; height:16px !important; margin-top:5px;">
                                 </label>
                                 <label class="checkbox inline">
                                    <h5>Yes, Unlock Secret $2k/Day AI Traffic System</h5>
                                 </label>
                              </div>
                              <div class="p20">
                                 <p class="f-18 text-center text-md-start"><b class="red-clr underline">Limited to the First 30 Buyers:</b> Enhance your order with an AI-powered traffic source, generating an additional $2k/day using FREE Traffic + Supercharge Your Website with a Faster Server for Quick Publishing, Traffic Flow, and Increased Profits.
                                 <br> <br><span class="w700">(97% of Customers Grab This and Witness Immediate Results)</span></p>
                                 <p class="f-18 text-center text-md-start"> Get it now for just<span class="green-clr w600">  $9.97 One Time Only</span> <span class="black-clr"><strike>(Normally $247)</strike></span>
                              
                              </p>
                                 
                              </div>
                           </div>
                        
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt30 ">
                  <div class="f-md-22 f-20 w400 lh140 text-center ">
                     *Commercial License Is ONLY Available With The Purchase Of Commercial Plan
                  </div>
               </div>
            </div>
         </div>
      </div> 
      <!-- Discounted Price Section End -->
      <!-- To your awesome section -->
      <div class="awesome-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-18 f-md-18 w400 lh140 white-clr text-xs-center">
                     <span class="w700">You can agree that the price we're asking is extremely low.</span> The price is rising with every few hours, so it won't be long until it's more than double what it is today!<br><br> We could easily charge hundreds of dollars for a revolutionary tool like this, but we want to offer you an attractive and affordable price that will finally help you build super engaging video channels in the best possible way - without wasting tons of money!
                     <br><br>  So, take action now... and I promise you won't be disappointed!
                  </div>
               </div>
              
               <div class="col-12 w700 f-md-28 f-22 text-start mt20 mt-md70 red-clr">To your success</div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
                  <div class="row">
                     <div class="col-md-6 col-12 text-center">
                        <img src="assets/images/pranshu-sir.webp" class="img-fluid d-block mx-auto " alt="Pranshu Gupta" style="max-height: 200px;"> 
                           <div class="f-24 f-md-28 w700 lh140 text-center white-clr mt20">
                              Pranshu Gupta
                           </div>
                           
                        </div>
                        <div class="col-md-6 col-12 text-center mt20 mt-md0">
                           <img src="assets/images/bizomart.webp" class="img-fluid d-block mx-auto " alt="Bizomart" style="max-height: 200px;">
                           <div class="f-24 f-md-28 w700 lh140 white-clr mt20">
                              Bizomart
                           </div>
                           
                        </div>
                     </div>
                     </div>
               <div class="col-12 mt20 mt-md50">
                  <div class="f-18 f-md-18 w400 lh140 white-clr">
                     <span class="w700">P.S- You can try "AutomailX" for 30 days without any worries.</span><br><br> Listen, we know there are a lot of crappy software tools out there that will get you nowhere. Most of the software is overpriced and an absolute waste of money. So, if you're a bit sceptical, that's perfectly fine. I'm so sure you'll see the potential of this ground-breaking software that I'll let you try it out 100% risk-free.

                     <br><br> Just test it for 30 days and if you're not able to build jaw-dropping video channels packed with video content and we cannot help you in any way, you will be eligible for a refund - no tricks, no hassles.
                     <br><br>
                     <span class="w700">P.S.S Don't Procrastinate - Take Action NOW! Get your copy of AutomailX!  
                     </span><br><br> By now you should be really excited about all the wonderful benefits of such an amazing piece of software. You don't want to miss out on the wonderful opportunity presented today... And then regret later when it costs more than double... or it's even completely off the market!
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- To your awesome section End -->
       
        <!-- Cta Section -->
       <div class="cta-section-new">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <!-- <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/limited-time-offer-banner.webp" class="img-fluid d-block mx-auto"> -->
                 <div class="f-22 f-md-34 w500 lh140 text-center white-clr">
                           Grab AutomailX Now for <strike>$99/Month,</strike> Today Only 1-Time $17
                           </div>
                  <div class="f-18 f-md-22 w500 lh140 text-center blue-gradient mt10">
                     (Get Free 30 Reseller License + FREE Commercial License)
                  </div>
                  <div>
                  <a href="https://warriorplus.com/o2/buy/qy9f4y/nb5h2m/w61hlz"><img src="https://warriorplus.com/o2/btn/fn200011000/qy9f4y/nb5h2m/370882" class="img-fluid mx-auto d-block"></a> 
                        </div>
                  </div>
                  <!-- <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 text-center">
                        <a href="#buynow" class="cta-link-btn">Get Instant Access To AutomailX</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20">
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/compaitable-with1.webp" class="img-fluid d-block mx-xs-center md-img-right">
                     <div class="d-md-block d-none px-md30"><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/v-line.webp" class="img-fluid"></div>
                     <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/days-gurantee1.webp" class="img-fluid d-block mx-xs-auto mt15 mt-md0">
                  </div> -->
               </div>
            </div>
         </div>
      </div> 
     <div class="warning">
         <div class="container">
            <div class="row text-center">
                 <div class="col-md-12 d-flex align-items-center justify-content-center flex-wrap text-center">
                    <img src="assets/images/warning.png" class="img-fluid d-block ml-auto mr10">
                    <div class="f-22 f-md-28 w400 lh140 text-center white-clr ">
                  <span class="w700">WARNING:</span>   Grab The Offer Before Timer Hits Zero</div>
                    </div>
                </div>
            </div>
        </div>
      <!-- Cta Section End -->
      <!-- FAQ Section Start -->
      <div class="faq-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-45 f-28 w600 lh150 text-center black-clr">
                  Frequently Asked <span class="orange-clr">Questions</span>
               </div>
               <div class="col-12 mt20 mt-md30">
                  <div class="row">
                     <div class="col-md-6 col-12">
                        <div class="faq-list">
                           <div class="f-md-22 f-20 w600 black-clr lh150">
                              What exactly AutomailX is all about? 
                           </div>
                           <div class="f-18 w400 lh150 mt10 text-start">
                              AutomailX is the ultimate push-button software that creates self-updating websites packed with awesome &amp; TRENDY content, drives FREE viral traffic &amp; makes handsfree commissions, ad profits &amp; sales. 
                           </div>
                        </div>
                        <div class="faq-list">
                           <div class="f-md-22 f-20 w600 black-clr lh150">
                              Is my investment risk free?
                           </div>
                           <div class="f-18 w400 lh150 mt10 text-start">
                              AutomailX is the ultimate push-button software that creates self-updating websites packed with awesome &amp; TRENDY content, drives FREE viral traffic &amp; makes handsfree commissions, ad profits &amp; sales. 
                           </div>
                        </div>
                        <div class="faq-list">
                           <div class="f-md-22 f-20 w600 black-clr lh150">
                              Do I have to install AutomailX?
                           </div>
                           <div class="f-18 w400 lh150 mt10 text-start">
                              NO! AutomailX is fully cloud based. Just create an account and you can get started immediately online. It is 100% web-based platform hosted on the cloud. This means you never have to download anything ever. You can access it at any time from any device that
                              has an internet connection.
                           </div>
                        </div>
                        <div class="faq-list">
                           <div class="f-md-22 f-20 w600 black-clr lh150">
                              Is it ‘Newbie Friendly’?
                           </div>
                           <div class="f-18 w400 lh150 mt10 text-start">
                              Yep, my friend, AutomailX is 100% newbie friendly. We know that there are a lot of technical hassles that most software has, but our software is a cut above the rest, and everyone can use it with complete ease.
                           </div>
                        </div>
                        <div class="faq-list">
                           <div class="f-md-22 f-20 w600 black-clr lh150">
                              Do you charge any monthly fees?
                           </div>
                           <div class="f-18 w400 lh150 mt10 text-start">
                              NOT AT ALL. There are NO monthly fees to use AutomailX during the launch period. During this period, you pay once and never again. We always believe in providing complete value for your money.
                           </div>
                        </div>
                     </div>
                     <div class="col-md-6 col-12">
                        <div class="faq-list">
                           <div class="f-md-22 f-20 w600 black-clr lh150">
                              Is AutomailX easy to use?
                           </div>
                           <div class="f-18 w400 lh150 mt10 text-start">
                              I bet you it’s the easiest tool you might have seen so far. Our #1 priority during the development of the software was to make it simple and easy for everyone. There is nothing to install, just create your account and login to take a plunge into hugely
                              profitable video marketing arena.
                           </div>
                        </div>
                        <div class="faq-list">
                           <div class="f-md-22 f-20 w600 black-clr lh150">
                              How do I know how well my campaigns are working?
                           </div>
                           <div class="f-18 w400 lh150 mt10 text-start">
                              You can see real-time reports for your campaigns in your account dashboard and make changes accordingly. But there’s a small catch, you need to upgrade your purchase to use these benefits.
                           </div>
                        </div>
                        <div class="faq-list">
                           <div class="f-md-22 f-20 w600 black-clr lh150">
                              Will I able to drive hordes of viral traffic from day one?
                           </div>
                           <div class="f-18 w400 lh150 mt10 text-start">
                              Well, that depends on how well you make the use of this ultimate software. We’ve created this from grounds up to make everything simple and easy and ensure that you move ahead without any hassles.
                           </div>
                        </div>
                        <div class="faq-list">
                           <div class="f-md-22 f-20 w600 black-clr lh150">
                              Will I get any training or support?
                           </div>
                           <div class="f-18 w400 lh150 mt10 text-start">
                              YES. We made detailed and step-by-step training videos that show you every step of how to get setup and you can access them in the member’s area.
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50">
                  <div class="f-22 f-md-26 w600 black-clr px0 text-xs-center lh150">If you have any query, simply reach to us at <a href="mailto:support@bizomart.com" class="blue-clr">support@bizomart.com</a></div>
               </div>
            </div>
         </div>
      </div>    
      <!-- FAQ Section End -->

      <!--final section-->
      <div class="final-section ">
         <div class="container ">
            <div class="row">
               <div class="col-12">
                  <div class="final-block">
                     <div class="text-center">
                        <div class="f-45 f-md-45 lh140 w700 text-center black-clr red-shape2">
                           FINAL CALL
                        </div>
                     </div>
                     <div class="col-12 f-22 f-md-28 lh140 w500 text-center white-clr mt20 p0">
                        You Still Have the Opportunity To Raise Your Game… <br class="d-none d-md-block">
                        Remember, It’s Your Make-or-Break Time!
                     </div>
                     <!-- CTA Btn Section Start -->
                     <div class="col-12 mt40 mt-md40">
                        <div class="f-22 f-md-34 w500 lh140 text-center white-clr">
                           Grab AutomailX Now for <strike>$99/Month,</strike> Today Only 1-Time $27
                           </div>
                        <div class="f-18 f-md-22 w500 lh140 text-center blue-gradient mt10">
                           (Get Free 30 Reseller License + FREE Commercial License)
                        </div>
                        <div>
                        <a href="https://warriorplus.com/o2/buy/qy9f4y/nb5h2m/lvzzql"><img src="https://warriorplus.com/o2/btn/fn200011000/qy9f4y/nb5h2m/370900" class="img-fluid mx-auto d-block"></a> 
                        </div>
                        <!-- <div class="row">
                           <div class="col-md-10 mx-auto col-12 mt20 text-center">
                              <a href="#buynow" class="cta-link-btn">Get Instant Access To AutomailX</a>
                           </div>
                        </div>
                        <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20">
                           <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/compaitable-with1.webp" class="img-fluid d-block mx-xs-center md-img-right">
                           <div class="d-md-block d-none px-md30"><img src="https://cdn.oppyo.com/launches/mailzilo/advanced/v-line.webp" class="img-fluid"></div>
                           <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/days-gurantee1.webp" class="img-fluid d-block mx-xs-auto mt15 mt-md0">
                        </div> -->
                     </div>
                     <!-- CTA Btn Section End -->
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--final section-->
      <!--Footer Section Start -->
      <div class="footer-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 1164.92 192.13" style="max-height:55px"><defs><style>.cls-1,.cls-2,.cls-3,.cls-4,.cls-5{fill-rule:evenodd;}.cls-1{fill:url(#linear-gradient);}.cls-2{fill:url(#linear-gradient-2);}.cls-3{fill:url(#linear-gradient-3);}.cls-4{fill:url(#linear-gradient-4);}.cls-5{fill:url(#linear-gradient-5);}.cls-6{fill:#fff;}.cls-7{fill:url(#linear-gradient-6);}</style><linearGradient id="linear-gradient" x1="82.05" y1="127.34" x2="150.42" y2="127.34" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#624ef2"/><stop offset="1" stop-color="#1e9af7"/></linearGradient><linearGradient id="linear-gradient-2" x1="7.14" y1="79.25" x2="123.43" y2="79.25" xlink:href="#linear-gradient"/><linearGradient id="linear-gradient-3" x1="0" y1="116.36" x2="45.71" y2="116.36" xlink:href="#linear-gradient"/><linearGradient id="linear-gradient-4" x1="3.94" y1="157.47" x2="142.12" y2="157.47" xlink:href="#linear-gradient"/><linearGradient id="linear-gradient-5" x1="3.58" y1="75.02" x2="182.4" y2="75.02" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#ff2d2d"/><stop offset="1" stop-color="#ffbc00"/></linearGradient><linearGradient id="linear-gradient-6" x1="971.74" y1="90.91" x2="1164.92" y2="90.91" xlink:href="#linear-gradient"/></defs><title>AutomailX White</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1" d="M150.42,67.57V177.2a15.41,15.41,0,0,1-3.38,9.92q-20.67-19.74-39-37.28l-26-24.94Z"/><path class="cls-2" d="M123.43,48.09l-62.5,62.32Q47.16,98.7,33.43,86.92q-8.17-7-16.31-14.11c-3.34-2.91-6.75-5.85-10-8.88a6.08,6.08,0,0,1,1.32-.33l24.25-3.27,28.14-3.8,33-4.45Z"/><path class="cls-3" d="M45.71,119.33,25.4,139.11Q15.2,149,5.81,158.19c-2.33,3-5.83,2.65-5.81-2.51V76.9c0-5.16,3.5-5.32,5.81-2.2l.39.41h0c.73,1.66,1.4,3.32,2,4.81l.15.38Z"/><path class="cls-4" d="M142.12,190.87a14.7,14.7,0,0,1-8.88,1l-39.42-7.34-33-6.14-28.14-5.24L8.44,168.61a8.2,8.2,0,0,1-4.5-2.68L25.82,145q11.1-10.61,23.23-22.21l15.81,16.51L78,128.3q12.35,12.06,25.78,25.16Q121.84,171.09,142.12,190.87Z"/><path class="cls-5" d="M10.15,79.16,65,136.41l92.87-81.5,3.93,12.24L182.4,13.62,128.57,32.19l11.14,5.9L61.05,116.53s-54.7-47.67-57.47-50c1.16,1.75,6.57,12.68,6.57,12.68Z"/><path class="cls-6" d="M283,50.49H247.2L201.86,181.82h29.75l9.74-29.95h47.42l9.73,29.95h29.75ZM248.4,130.2l16.17-49.7h1l16.14,49.7Z"/><path class="cls-6" d="M406.68,139.88V83.32H434v98.5H407.77V163.93h-1a29.1,29.1,0,0,1-11.06,13.91q-7.73,5.26-18.82,5.26a33.23,33.23,0,0,1-17.38-4.49,30.61,30.61,0,0,1-11.7-12.76q-4.2-8.26-4.27-19.81V83.32h27.32v57.84q.06,8.73,4.68,13.79T387.89,160a19.37,19.37,0,0,0,9.23-2.27,17.53,17.53,0,0,0,7-6.77A21,21,0,0,0,406.68,139.88Z"/><path class="cls-6" d="M510,180.6q-2.06.65-5.78,1.51a48.09,48.09,0,0,1-9,1.06,41.39,41.39,0,0,1-17.28-2.63,24.12,24.12,0,0,1-11.51-9.36q-4.11-6.36-4-16v-51.3H448.87V83.32h13.47V59.72h27.32v23.6h18.53v20.52H489.66v47.71a12.24,12.24,0,0,0,1.15,5.87,6.34,6.34,0,0,0,3.24,2.92,13.08,13.08,0,0,0,4.84.83,21.79,21.79,0,0,0,3.85-.35l2.95-.55Z"/><path class="cls-6" d="M569.43,183.74q-14.94,0-25.81-6.38a43,43,0,0,1-16.77-17.83Q521,148.09,521,133t5.9-26.71a43,43,0,0,1,16.77-17.82Q554.5,82,569.43,82t25.81,6.38A43,43,0,0,1,612,106.24q5.91,11.46,5.9,26.71T612,159.53a43,43,0,0,1-16.77,17.83Q584.38,183.75,569.43,183.74Zm.13-21.16a16.88,16.88,0,0,0,11.35-3.88,23.82,23.82,0,0,0,6.89-10.61,46.31,46.31,0,0,0,2.35-15.33,46.31,46.31,0,0,0-2.35-15.33,24.09,24.09,0,0,0-6.89-10.64,16.8,16.8,0,0,0-11.35-3.91,17.26,17.26,0,0,0-11.51,3.91,23.74,23.74,0,0,0-7,10.64,46.58,46.58,0,0,0-2.34,15.33,46.58,46.58,0,0,0,2.34,15.33,23.48,23.48,0,0,0,7,10.61A17.35,17.35,0,0,0,569.56,162.58Z"/><path class="cls-6" d="M640.74,50.49h19l44.63,109h1.54l44.64-109h19V181.82H754.63V82h-1.28l-41,99.78H697.94L656.9,82h-1.28v99.78H640.74Z"/><path class="cls-6" d="M828.25,184.13a39.77,39.77,0,0,1-17-3.56,28.82,28.82,0,0,1-12.12-10.33q-4.49-6.77-4.49-16.38,0-8.46,3.34-13.76a23.63,23.63,0,0,1,8.91-8.3,47.46,47.46,0,0,1,12.35-4.52q6.76-1.52,13.62-2.41,9-1.16,14.59-1.76t8.21-2.08q2.6-1.48,2.6-5.13v-.52q0-9.48-5.16-14.75t-15.62-5.25q-10.85,0-17,4.74a28,28,0,0,0-8.66,10.13l-14.37-5.13a34.69,34.69,0,0,1,10.3-14,38.56,38.56,0,0,1,14.11-7A58.86,58.86,0,0,1,837,82a61.89,61.89,0,0,1,10.93,1.12,35.92,35.92,0,0,1,12,4.58,26.84,26.84,0,0,1,9.65,10.46q3.86,7,3.85,18.72v64.9H858.26V168.48h-.77a26.84,26.84,0,0,1-5.13,6.86,29.6,29.6,0,0,1-9.56,6.22A36.78,36.78,0,0,1,828.25,184.13Zm2.3-13.6q9,0,15.17-3.53a24.19,24.19,0,0,0,9.36-9.1,23.36,23.36,0,0,0,3.18-11.74V132.31q-1,1.16-4.2,2.09a64,64,0,0,1-7.44,1.6q-4.2.67-8.15,1.15c-2.63.32-4.75.59-6.38.81a58.57,58.57,0,0,0-11,2.46,19.26,19.26,0,0,0-8.21,5.07q-3.12,3.36-3.11,9.14,0,7.89,5.87,11.89T830.55,170.53Z"/><path class="cls-6" d="M911.79,66.9a10.65,10.65,0,0,1-7.6-3,9.86,9.86,0,0,1,0-14.49,11.07,11.07,0,0,1,15.2,0,9.86,9.86,0,0,1,0,14.49A10.65,10.65,0,0,1,911.79,66.9Zm-7.7,114.92V83.32h15.14v98.5Z"/><path class="cls-6" d="M962.06,50.49V181.82H946.93V50.49Z"/><path class="cls-7" d="M1045.34,0l26.37,62h1.42l47.14-62h44.65l-70.4,90.91L1136,181.82h-43.32l-27-62.06h-1.42l-47.59,62.06H971.74l72.18-90.91L1003,0Z"/></g></g></svg>
                  <div class="f-14 f-md-16 w400 mt20 lh150 white-clr text-center" ><span class="w600">Note:</span> This site is not a part of the Facebook website or Facebook Inc. Additionally, this site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc.<br><br>
                  <div class="f-16 f-md-18 w600 mt20 mt-md20 white-clr text-center"> <script type="text/javascript" 
                    src="https://warriorplus.com/o2/disclaimer/qy9f4y" defer></script><div class="wplus_spdisclaimer"></div>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-md-16 f-16 w400 lh140 white-clr text-xs-center">Copyright © AutomailX 2023</div>
                  <ul class="footer-ul w400 f-md-16 f-16 white-clr text-center text-md-right">
                     <li><a href="https://support.bizomart.com/hc/en-us " class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://automailxai.com/legal/privacy-policy.html " class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://automailxai.com/legal/terms-of-service.html " class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://automailxai.com/legal/disclaimer.html " class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://automailxai.com/legal/gdpr.html " class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://automailxai.com/legal/dmca.html " class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://automailxai.com/legal/anti-spam.html " class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!--Footer Section End -->

      <!-- Exit Popup and Timer -->
      <!-- <?php include('timer.php'); ?> -->
      <!-- Exit Popup and Timer End -->

      <script>
         const typedTextSpan = document.querySelector(".typed-text");
const cursorSpan = document.querySelector(".cursor");

const textArray = ["Emails and Messages", "Newsletter", "Autoresponder"];
const typingDelay = 200;
const erasingDelay = 100;
const newTextDelay = 2000; // Delay between current and next text
let textArrayIndex = 0;
let charIndex = 0;

function type() {
  if (charIndex < textArray[textArrayIndex].length) {
    if(!cursorSpan.classList.contains("typing")) cursorSpan.classList.add("typing");
    typedTextSpan.textContent += textArray[textArrayIndex].charAt(charIndex);
    charIndex++;
    setTimeout(type, typingDelay);
  } 
  else {
    cursorSpan.classList.remove("typing");
  	setTimeout(erase, newTextDelay);
  }
}

function erase() {
	if (charIndex > 0) {
    if(!cursorSpan.classList.contains("typing")) cursorSpan.classList.add("typing");
    typedTextSpan.textContent = textArray[textArrayIndex].substring(0, charIndex-1);
    charIndex--;
    setTimeout(erase, erasingDelay);
  } 
  else {
    cursorSpan.classList.remove("typing");
    textArrayIndex++;
    if(textArrayIndex>=textArray.length) textArrayIndex=0;
    setTimeout(type, typingDelay + 1100);
  }
}

document.addEventListener("DOMContentLoaded", function() { // On DOM Load initiate the effect
  if(textArray.length) setTimeout(type, newTextDelay + 250);
});
      </script>

<script> 
function myFunction() {
  var checkBox = document.getElementById("check");
  var buybutton = document.getElementById("buyButton");
  if (checkBox.checked == true){
 
	    buybutton.getAttribute("href");
	    buybutton.setAttribute("href","https://warriorplus.com/o2/buy/qy9f4y/nb5h2m/lvzzql");
	} else {
		 buybutton.getAttribute("href");
		 buybutton.setAttribute("href","https://warriorplus.com/o2/buy/qy9f4y/nb5h2m/w61hlz");
	  }
}
</script>
   </body>
</html>

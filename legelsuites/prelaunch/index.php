<!Doctype html>
<html>
   <head>
      <title>LegelSuites Prelaunch</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=9">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
      <meta name="title" content="LegelSuites | Prelaunch">
      <meta name="description" content="Discover How to Analyse, Detects & Fix Deadly Legal Flaws & Secure Your Website from Being Sued and Fined! ">
      <meta name="keywords" content="LegelSuites">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta property="og:image" content="https://www.legelsuites.co/prelaunch/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Dr. Amit Pareek">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="LegelSuites | Prelaunch">
      <meta property="og:description" content="Discover How to Analyse, Detects & Fix Deadly Legal Flaws & Secure Your Website from Being Sued and Fined! ">
      <meta property="og:image" content="https://www.legelsuites.co/prelaunch/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="LegelSuites | Prelaunch">
      <meta property="twitter:description" content="Discover How to Analyse, Detects & Fix Deadly Legal Flaws & Secure Your Website from Being Sued and Fined! ">
      <meta property="twitter:image" content="https://www.legelsuites.co/prelaunch/thumbnail.png">
      <link rel="icon" href="https://cdn.oppyo.com/launches/legelsuites/common_assets/images/favicon.png" type="image/png">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&family=Lexend:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
      <script type="text/javascript" src="https://www.legelsuites.com/widget/ada/oppyo-GTtd-wozS-Rm7H-DqGe" crossorigin="anonymous" embed-id="oppyo-GTtd-wozS-Rm7H-DqGe"></script>
      <!-- Start Editor required -->
      <link rel="stylesheet" href="https://cdn.oppyo.com/launches/legelsuites/common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style.css" type="text/css">
      <link rel="stylesheet" href="assets/css/aweberform.css" type="text/css">
      <script>
      function getUrlVars() {
         var vars = [],
            hash;
         var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
         for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
         }
         return vars;
      }
      var first = getUrlVars()["aid"];
      //alert(first);
      document.addEventListener('DOMContentLoaded', (event) => {
         document.getElementById('awf_field_aid').setAttribute('value', first);
      })
      //document.getElementById('myField').value = first;
   </script>
   </head>
   <body>
      <!-- Header Section Start -->
      <div class="header-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row align-items-center">
                     <div class="col-md-3 text-center text-md-start">
                     <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 495.7 104.2" style="enable-background:new 0 0 495.7 104.2; max-height:60px;" xml:space="preserve">
                     <style type="text/css">
                        .st0{fill:#FFFFFF;}
                        .st1{fill:#FFA829;}
                        .st2{fill:url(#SVGID_1_);}
                        .st3{fill:url(#SVGID_00000132794433514585357610000014266780508499585408_);}
                     </style>
                     <g>
                        <path class="st0" d="M96.9,82.6c-7.5,2.8-15.7,4.4-24.2,4.8c0.2,0.7,0,1.4-0.5,1.9c-3.2,3.4-6.4,6.1-9.9,8.2
                           c-5.4,3.4-11.5,5.6-17.9,6.5l-0.3,0.1c-0.2,0-0.3,0.1-0.5,0.1c-0.2,0-0.3,0-0.5-0.1l-0.3-0.1c-6.4-0.9-12.4-3.1-17.9-6.6
                           C11.6,89,3.4,74.5,3.2,58.7c0-0.9,0.6-1.7,1.4-2c0,0,0.1,0,0.1,0C1.2,50.6-0.5,43.9,0.1,37C1.8,19.4,18.3,4.9,42.3,0
                           c1-0.2,2,0.4,2.3,1.4c0.3,1-0.2,2.1-1.2,2.4c-5.4,2-10.2,4.6-14.2,7.7l14-3.6c0.3-0.1,0.7-0.1,1,0l38.4,9.9c0.9,0.2,1.5,1,1.5,1.9
                           v38.1c0,7.9-2,15.6-5.7,22.4c5.9,0.1,11.7-0.4,17.4-1.5c1-0.2,2,0.4,2.3,1.4C98.4,81.2,97.9,82.2,96.9,82.6z"></path>
                        <g>
                           <path class="st0" d="M291.8,67.6l2.5-5.7c3.2,2.5,8.2,4.3,13.1,4.3c6.2,0,8.8-2.2,8.8-5.1c0-8.6-23.6-3-23.6-17.7
                              c0-6.4,5.1-11.8,16-11.8c4.8,0,9.7,1.3,13.2,3.5l-2.3,5.7c-3.6-2.1-7.5-3.1-10.9-3.1c-6.1,0-8.6,2.4-8.6,5.4
                              c0,8.4,23.5,3,23.5,17.5c0,6.3-5.1,11.8-16,11.8C301.2,72.4,295,70.4,291.8,67.6z"></path>
                           <path class="st0" d="M464,67.6l2.5-5.7c3.2,2.5,8.2,4.3,13.1,4.3c6.2,0,8.8-2.2,8.8-5.1c0-8.6-23.6-3-23.6-17.7
                              c0-6.4,5.1-11.8,16-11.8c4.8,0,9.7,1.3,13.2,3.5l-2.3,5.7c-3.6-2.1-7.5-3.1-10.9-3.1c-6.1,0-8.6,2.4-8.6,5.4
                              c0,8.4,23.5,3,23.5,17.5c0,6.3-5.1,11.8-16,11.8C473.4,72.4,467.2,70.4,464,67.6z"></path>
                           <path class="st0" d="M330.3,54.5V32.2h7.3v22.1c0,8.1,3.7,11.7,10.1,11.7s10-3.5,10-11.7V32.2h7.2v22.3c0,11.6-6.5,17.8-17.3,17.8
                              C336.9,72.4,330.3,66.1,330.3,54.5z"></path>
                           <path class="st0" d="M375.3,32.2h7.3v39.6h-7.3V32.2z"></path>
                           <path class="st0" d="M401.4,38.4h-13.1v-6.2h33.6v6.2h-13.1v33.4h-7.3V38.4H401.4z"></path>
                           <path class="st0" d="M457.2,65.6v6.2h-29.7V32.2h28.9v6.2h-21.6v10.3H454v6.1h-19.1v10.9L457.2,65.6L457.2,65.6z"></path>
                        </g>
                        <g>
                           <path class="st1" d="M96.2,80.7c-10.5,3.9-23,5.6-36.1,4.4C25.9,81.9,0,60.5,2.1,37.2C3.7,20,20.3,6.5,42.7,2
                              C26.1,8.2,14.6,19.8,13.2,34.1C11.1,57.4,37,78.8,71.1,81.9C79.9,82.7,88.4,82.2,96.2,80.7z"></path>
                           <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="20.0931" y1="1069.2449" x2="47.1737" y2="1042.1643" gradientTransform="matrix(1 0 0 1 0 -978)">
                              <stop offset="0" style="stop-color:#0055FF"></stop>
                              <stop offset="1" style="stop-color:#008CFF"></stop>
                           </linearGradient>
                           <path class="st2" d="M68.3,87.8c-2,0-3.9-0.1-5.9-0.2c-1.3,1.1-2.7,2.1-4.2,3.1c-4.5,2.8-9.3,4.6-14.5,5.4
                              c-5.1-0.8-10-2.6-14.5-5.4c-8.7-5.4-14.6-13.8-17-23.4c-2.8-2.7-5.1-5.6-7-8.6c0.2,15.2,8,29,20.9,37.1c5.3,3.3,11,5.4,17.2,6.3
                              l0.4,0.1l0.4-0.1c6.1-0.9,11.9-3,17.2-6.3c3.6-2.2,6.7-4.9,9.5-7.9C70,87.8,69.1,87.8,68.3,87.8z"></path>
                           <linearGradient id="SVGID_00000129182319601907477880000006569859738938224028_" gradientUnits="userSpaceOnUse" x1="33.5187" y1="1042.8949" x2="80.3658" y2="996.0477" gradientTransform="matrix(1 0 0 1 0 -978)">
                              <stop offset="0" style="stop-color:#0055FF"></stop>
                              <stop offset="1" style="stop-color:#008CFF"></stop>
                           </linearGradient>
                           <path style="fill:url(#SVGID_00000129182319601907477880000006569859738938224028_);" d="M43.7,9.9l-17.5,4.5
                              c-2,1.9-21.6,31.5,10.2,51.6l2.2-13c-2.2-1.6-3.6-4.2-3.6-7c0-4.8,3.9-8.7,8.7-8.7s8.7,3.9,8.7,8.7c0,2.9-1.4,5.4-3.6,7l3.4,20.5
                              C59,76,67,78.1,76.4,79.8c3.7-6.5,5.7-14,5.7-21.9V19.8L43.7,9.9z M40.3,17.5l-0.2,2.2l-1.6-1.4l-2.1,0.5l0.9-2l-1.1-1.9l2.2,0.2
                              l1.4-1.6l0.4,2.1l2,0.8L40.3,17.5z M59.9,22.7l-0.2,2.5l-1.8-1.7l-2.4,0.6l1-2.2l-1.3-2.1l2.5,0.3l1.6-1.9l0.5,2.4l2.3,1
                              L59.9,22.7z M74.2,71.3L74,74.6l-2.5-2.3l-3.3,0.8l1.4-3.1l-1.8-2.9l3.4,0.4l2.2-2.5l0.7,3.3l3.1,1.3L74.2,71.3z M74.8,51.1
                              l-0.2,3.1l-2.3-2.1l-3,0.7l1.3-2.8L69,47.4l3.1,0.3l2-2.4l0.6,3l2.9,1.2L74.8,51.1z M75.4,31l-0.2,2.8l-2.1-1.9l-2.7,0.6l1.1-2.5
                              l-1.4-2.4l2.7,0.3l1.8-2.1l0.6,2.7l2.6,1.1L75.4,31z"></path>
                        </g>
                        <g>
                           <path class="st0" d="M105.1,32.2h11.2v30.7h18.9v8.9h-30.1V32.2z"></path>
                           <path class="st0" d="M171.5,63.1v8.7h-31.8V32.2h31.1v8.7h-20v6.7h17.6V56h-17.6v7.2L171.5,63.1L171.5,63.1z"></path>
                           <path class="st0" d="M204.2,51.2h9.9v16.4c-4.6,3.3-10.9,5-16.6,5c-12.6,0-21.8-8.6-21.8-20.6s9.2-20.6,22.1-20.6
                              c7.4,0,13.4,2.5,17.3,7.2L208,45c-2.7-3-5.8-4.4-9.6-4.4c-6.8,0-11.3,4.5-11.3,11.3c0,6.7,4.5,11.3,11.2,11.3
                              c2.1,0,4.1-0.4,6.1-1.3L204.2,51.2L204.2,51.2z"></path>
                           <path class="st0" d="M253,63.1v8.7h-31.8V32.2h31.1v8.7h-20v6.7h17.6V56h-17.6v7.2L253,63.1L253,63.1z"></path>
                           <path class="st0" d="M259.2,32.2h11.2v30.7h18.9v8.9h-30.1V32.2z"></path>
                        </g>
                     </g>
                  </svg>
                     </div>
                     <div class="col-md-9 mt20 mt-md5">
                        <ul class="leader-ul f-18 f-md-20 white-clr text-md-end text-center">
                           <li class="affiliate-link-btn"><a href="#book-seat" class="t-decoration-none ">Book Your Free Seat</a></li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
               
      
      
      <div class="header-wrap">
            <div class="container">
               <div class="row">
                  <div class="col-12 text-center">
                     <div class="preheadline f-16 f-md-22 w400 white-clr lh150">
                        Register for One Time Only Value Packed Free Training & Get an Assured Gift + 10 Free Licenses
                     </div>
                  </div>
                  <div class="col-12 mt20 mt-md30 f-md-45 f-28 w700 text-center cyan-clr lh150">
                  Discover How to Analyse, Detects & Fix Deadly Legal Flaws & Secure Your Website from Being Sued and Fined!
                  </div>
                  <div class="col-12 mt20 mt-md30 text-center f-18 f-md-26 w500 text-center lh130 white-clr text-capitalize">
                     In This Free Training, We Will Also Reveal, How You Can Start an Agency & Serve Clients for Website Legal Compliant Services and Can Get Started Easily Without Any Tech Hassles.
                     <img src="assets/images/h-line.webp" alt="H-Line" class="mx-auto img-fluid d-none d-md-block mt15">
                  </div>
               </div>
               <div class="row d-flex align-items-center flex-wrap mt20 mt-md40">
               <div class="col-md-7 col-12 min-md-video-width-left">
                  <img src="assets/images/video-thumb.webp" class="img-fluid d-block mx-auto" alt="Prelaunch">
               </div>
               <div class="col-md-5 col-12 mt20 mt-md0 min-md-video-width-right" id="form">
                  <div class="auto_responder_form inp-phold calendar-wrap" id="book-seat">
                     <div class="f-20 f-md-24 w700 text-center lh180 blue-clr" editabletype="text" style="z-index:10;">
                        Register for Free Training <br>
                        <span class="f-20 f-md-24 w700 text-center black-clr" contenteditable="false">17<sup contenteditable="false">th</sup> November, 10 AM EST</span>
                     </div>
                     <div class="col-12 f-18 f-md-20 w500 lh150 text-center">
                        Book Your Seat now<br> (Limited to 100 Users)
                     </div>
                     <!-- Aweber Form Code -->
                     <div class="col-12 p0 mt15 mt-md20">
                        <form method="post" class="af-form-wrapper register-form-style1 " accept-charset="UTF-8" action="https://www.aweber.com/scripts/addlead.pl" style="z-index: 10;">
                           <div style="display: none;">
                              <input type="hidden" name="meta_web_form_id" value="1402426371" />
                              <input type="hidden" name="meta_split_id" value="" />
                              <input type="hidden" name="listname" value="awlist6381806" />
                              <input type="hidden" name="redirect" value="https://www.legelsuites.co/prelaunch-thankyou" id="redirect_f08aa5ab4c41007a2dd94c8dbe05d33d" />
                              <input type="hidden" name="meta_adtracking" value="Prela_Form_Code_(Hardcoded)" />
                              <input type="hidden" name="meta_message" value="1" />
                              <input type="hidden" name="meta_required" value="name,email" />
                              <input type="hidden" name="meta_tooltip" value="" />
                           </div>
                           <div id="af-form-1402426371" class="af-form">
                              <div id="af-body-1402426371" class="af-body af-standards">
                                 <div class="af-element width-form1">
                                    <label class="previewLabel" for="awf_field-114988469"></label>
                                    <div class="af-textWrap">
                                       <div class="input-type" style="z-index: 11;">
                                          <!--<span class="input-group-addon border1px"><i class="fa fa-user" aria-hidden="true"></i></span>-->
                                          <input id="awf_field-114988469" type="text" name="name" class="text form-control custom-input input-field" value="" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="500" placeholder="Your Name" />
                                       </div>
                                    </div>
                                    <div class="af-clear bottom-margin"></div>
                                 </div>
                                 <div class="af-element width-form1">
                                    <label class="previewLabel" for="awf_field-114988470"> </label>
                                    <div class="af-textWrap">
                                       <div class="input-type" style="z-index: 11;">
                                          <!--<span class="input-group-addon border1px"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>-->
                                          <input class="text form-control custom-input input-field" id="awf_field-114988470" type="text" name="email" value="" tabindex="501" onFocus=" if (this.value == '') { this.value = ''; }" onBlur="if (this.value == '') { this.value='';} " placeholder="Your Email" />
                                       </div>
                                    </div>
                                    <div class="af-clear bottom-margin"></div>
                                 </div>
                                 <input type="hidden" id="awf_field_aid" class="text" name="custom aid" value="" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="502" />
                                 <div class="af-element buttonContainer button-type register-btn1 f-22 f-md-21 mt-md0">
                                    <input name="submit" id="af-submit-image-348552691" type="submit" class="image" style="max-width: 100%;" alt="Submit Form" tabindex="503" value="Book Your Free Seat" />
                                    <div class="af-clear bottom-margin"></div>
                                 </div>
                              </div>
                           </div>
                           <div style="display: none;"><img src="https://forms.aweber.com/form/displays.htm?id=TIws7ExsjCwMzA==" alt="Form" /></div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
            </div>
         </div>
      </div>
      <!-- Header Section End -->
      <!-- Second Section Start -->
      <div class="second-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="col-12">
                     <div class="row">
                        <div class="col-12">
                           <div class="f-md-34 f-24 w700 lh150 text-capitalize text-center gradient-clr">
                              Why Attend This Free Training Webinar & What Will You Learn?
                           </div>
                           <div class="f-20 f-md-22 w600 lh150 text-capitalize text-center black-clr mt20">
                              Our 20+ Years Of Experience Is Packaged In This State-Of-Art 1-Hour Webinar Where You’ll Learn:
                           </div>
                        </div>
                        <div class="col-12 mt20 mt-md50">
                           <ul class="features-list pl0 f-18 f-md-18 lh150 w400 black-clr text-capitalize">
                             <li>How We Have Built A 6-Figure Profitable Agency Business Online.</li> 
                             <li> How You Can Detect and Fix International Legal Compliant Issues By Using An All-In-One Tool. </li> 
                             <li> How To Save Website Owners from being Legally Sued or Fined due to Non-Compliant Websites. </li> 
                             <li> How To Tap Into The $285 Billion Hot Freelancing Industry. </li> 
                             <li> How To Set Up Stunning Agency Websites Quickly and Easily. </li> 
                             <li> Lastly, Everyone Who Attends This Session Will Get an Assured Gift from Us Plus We Are Giving 10 Licenses Of Our Premium Solution – LegelSuites </li> 
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md70">
               <div class="col-12 col-md-12 mx-auto" id="form">
                  <div class="auto_responder_form inp-phold formbg" id="book-seat1">
                     <div class="f-24 f-md-40 w700 text-center lh150 white-clr" editabletype="text" style="z-index:10;">
                        So Make Sure You Do NOT Miss This Webinar
                     </div>
                     <div class="col-12 f-md-22 f-18 w600 lh150 mx-auto my20 text-center white-clr">
                        Register Now! And Join Us Live on November 17th, 10:00 AM EST
                     </div>
                      <!-- Aweber Form Code -->
                  <div class="col-12 p0 mt15 mt-md25" editabletype="form" style="z-index:10">
                     <!-- Aweber Form Code -->
                     <form method="post" class="af-form-wrapper register-form-style1 " accept-charset="UTF-8" action="https://www.aweber.com/scripts/addlead.pl" style="z-index: 10;">
                        <div style="display: none;">
                           <input type="hidden" name="meta_web_form_id" value="1402426371">
                           <input type="hidden" name="meta_split_id" value="">
                           <input type="hidden" name="listname" value="awlist6381806">
                           <input type="hidden" name="redirect" value="https://www.legelsuites.co/prelaunch-thankyou" id="redirect_f08aa5ab4c41007a2dd94c8dbe05d33d">
                           <input type="hidden" name="meta_adtracking" value="Prela_Form_Code_(Hardcoded)">
                           <input type="hidden" name="meta_message" value="1">
                           <input type="hidden" name="meta_required" value="name,email">
                           <input type="hidden" name="meta_tooltip" value="">
                        </div>
                        <div id="af-form-1402426371" class="af-form">
                           <div id="af-body-1402426371" class="af-body af-standards">
                              <div class="af-element width-form">
                                 <label class="previewLabel" for="awf_field-114988469"></label>
                                 <div class="af-textWrap">
                                    <div class="input-type" style="z-index: 11;">
                                       <!--<span class="input-group-addon border1px"><i class="fa fa-user" aria-hidden="true"></i></span>-->
                                       <input id="awf_field-114988469" type="text" name="name" class="text form-control custom-input input-field" value="" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="500" placeholder="Your Name">
                                    </div>
                                 </div>
                                 <div class="af-clear bottom-margin"></div>
                              </div>
                              <div class="af-element width-form">
                                 <label class="previewLabel" for="awf_field-114988470"> </label>
                                 <div class="af-textWrap">
                                    <div class="input-type" style="z-index: 11;">
                                       <!--<span class="input-group-addon border1px"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>-->
                                       <input class="text form-control custom-input input-field" id="awf_field-114988470" type="text" name="email" value="" tabindex="501" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " placeholder="Your Email">
                                    </div>
                                 </div>
                                 <div class="af-clear bottom-margin"></div>
                              </div>
                              <input type="hidden" id="awf_field_aid" class="text" name="custom aid" value="undefined" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="502">
                              <div class="af-element buttonContainer button-type register-btn1 f-22 f-md-21 width-form mt-md0">
                                 <input name="submit" id="af-submit-image-348552691" type="submit" class="image" style="max-width: 100%;" alt="Submit Form" tabindex="503" value="Book Your Free Seat">
                                 <div class="af-clear bottom-margin"></div>
                              </div>
                           </div>
                        </div>
                        <div style="display: none;"><img src="https://forms.aweber.com/form/displays.htm?id=TIws7ExsjCwMzA==" alt="Form"></div>
                     </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Header Section End -->
      <!--Footer Section Start -->
      <div class="footer-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
               <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 495.7 104.2" style="enable-background:new 0 0 495.7 104.2; max-height:60px;" xml:space="preserve">
                     <style type="text/css">
                        .st0{fill:#FFFFFF;}
                        .st1{fill:#FFA829;}
                        .st2{fill:url(#SVGID_1_);}
                        .st3{fill:url(#SVGID_00000132794433514585357610000014266780508499585408_);}
                     </style>
                     <g>
                        <path class="st0" d="M96.9,82.6c-7.5,2.8-15.7,4.4-24.2,4.8c0.2,0.7,0,1.4-0.5,1.9c-3.2,3.4-6.4,6.1-9.9,8.2
                           c-5.4,3.4-11.5,5.6-17.9,6.5l-0.3,0.1c-0.2,0-0.3,0.1-0.5,0.1c-0.2,0-0.3,0-0.5-0.1l-0.3-0.1c-6.4-0.9-12.4-3.1-17.9-6.6
                           C11.6,89,3.4,74.5,3.2,58.7c0-0.9,0.6-1.7,1.4-2c0,0,0.1,0,0.1,0C1.2,50.6-0.5,43.9,0.1,37C1.8,19.4,18.3,4.9,42.3,0
                           c1-0.2,2,0.4,2.3,1.4c0.3,1-0.2,2.1-1.2,2.4c-5.4,2-10.2,4.6-14.2,7.7l14-3.6c0.3-0.1,0.7-0.1,1,0l38.4,9.9c0.9,0.2,1.5,1,1.5,1.9
                           v38.1c0,7.9-2,15.6-5.7,22.4c5.9,0.1,11.7-0.4,17.4-1.5c1-0.2,2,0.4,2.3,1.4C98.4,81.2,97.9,82.2,96.9,82.6z"></path>
                        <g>
                           <path class="st0" d="M291.8,67.6l2.5-5.7c3.2,2.5,8.2,4.3,13.1,4.3c6.2,0,8.8-2.2,8.8-5.1c0-8.6-23.6-3-23.6-17.7
                              c0-6.4,5.1-11.8,16-11.8c4.8,0,9.7,1.3,13.2,3.5l-2.3,5.7c-3.6-2.1-7.5-3.1-10.9-3.1c-6.1,0-8.6,2.4-8.6,5.4
                              c0,8.4,23.5,3,23.5,17.5c0,6.3-5.1,11.8-16,11.8C301.2,72.4,295,70.4,291.8,67.6z"></path>
                           <path class="st0" d="M464,67.6l2.5-5.7c3.2,2.5,8.2,4.3,13.1,4.3c6.2,0,8.8-2.2,8.8-5.1c0-8.6-23.6-3-23.6-17.7
                              c0-6.4,5.1-11.8,16-11.8c4.8,0,9.7,1.3,13.2,3.5l-2.3,5.7c-3.6-2.1-7.5-3.1-10.9-3.1c-6.1,0-8.6,2.4-8.6,5.4
                              c0,8.4,23.5,3,23.5,17.5c0,6.3-5.1,11.8-16,11.8C473.4,72.4,467.2,70.4,464,67.6z"></path>
                           <path class="st0" d="M330.3,54.5V32.2h7.3v22.1c0,8.1,3.7,11.7,10.1,11.7s10-3.5,10-11.7V32.2h7.2v22.3c0,11.6-6.5,17.8-17.3,17.8
                              C336.9,72.4,330.3,66.1,330.3,54.5z"></path>
                           <path class="st0" d="M375.3,32.2h7.3v39.6h-7.3V32.2z"></path>
                           <path class="st0" d="M401.4,38.4h-13.1v-6.2h33.6v6.2h-13.1v33.4h-7.3V38.4H401.4z"></path>
                           <path class="st0" d="M457.2,65.6v6.2h-29.7V32.2h28.9v6.2h-21.6v10.3H454v6.1h-19.1v10.9L457.2,65.6L457.2,65.6z"></path>
                        </g>
                        <g>
                           <path class="st1" d="M96.2,80.7c-10.5,3.9-23,5.6-36.1,4.4C25.9,81.9,0,60.5,2.1,37.2C3.7,20,20.3,6.5,42.7,2
                              C26.1,8.2,14.6,19.8,13.2,34.1C11.1,57.4,37,78.8,71.1,81.9C79.9,82.7,88.4,82.2,96.2,80.7z"></path>
                           <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="20.0931" y1="1069.2449" x2="47.1737" y2="1042.1643" gradientTransform="matrix(1 0 0 1 0 -978)">
                              <stop offset="0" style="stop-color:#0055FF"></stop>
                              <stop offset="1" style="stop-color:#008CFF"></stop>
                           </linearGradient>
                           <path class="st2" d="M68.3,87.8c-2,0-3.9-0.1-5.9-0.2c-1.3,1.1-2.7,2.1-4.2,3.1c-4.5,2.8-9.3,4.6-14.5,5.4
                              c-5.1-0.8-10-2.6-14.5-5.4c-8.7-5.4-14.6-13.8-17-23.4c-2.8-2.7-5.1-5.6-7-8.6c0.2,15.2,8,29,20.9,37.1c5.3,3.3,11,5.4,17.2,6.3
                              l0.4,0.1l0.4-0.1c6.1-0.9,11.9-3,17.2-6.3c3.6-2.2,6.7-4.9,9.5-7.9C70,87.8,69.1,87.8,68.3,87.8z"></path>
                           <linearGradient id="SVGID_00000129182319601907477880000006569859738938224028_" gradientUnits="userSpaceOnUse" x1="33.5187" y1="1042.8949" x2="80.3658" y2="996.0477" gradientTransform="matrix(1 0 0 1 0 -978)">
                              <stop offset="0" style="stop-color:#0055FF"></stop>
                              <stop offset="1" style="stop-color:#008CFF"></stop>
                           </linearGradient>
                           <path style="fill:url(#SVGID_00000129182319601907477880000006569859738938224028_);" d="M43.7,9.9l-17.5,4.5
                              c-2,1.9-21.6,31.5,10.2,51.6l2.2-13c-2.2-1.6-3.6-4.2-3.6-7c0-4.8,3.9-8.7,8.7-8.7s8.7,3.9,8.7,8.7c0,2.9-1.4,5.4-3.6,7l3.4,20.5
                              C59,76,67,78.1,76.4,79.8c3.7-6.5,5.7-14,5.7-21.9V19.8L43.7,9.9z M40.3,17.5l-0.2,2.2l-1.6-1.4l-2.1,0.5l0.9-2l-1.1-1.9l2.2,0.2
                              l1.4-1.6l0.4,2.1l2,0.8L40.3,17.5z M59.9,22.7l-0.2,2.5l-1.8-1.7l-2.4,0.6l1-2.2l-1.3-2.1l2.5,0.3l1.6-1.9l0.5,2.4l2.3,1
                              L59.9,22.7z M74.2,71.3L74,74.6l-2.5-2.3l-3.3,0.8l1.4-3.1l-1.8-2.9l3.4,0.4l2.2-2.5l0.7,3.3l3.1,1.3L74.2,71.3z M74.8,51.1
                              l-0.2,3.1l-2.3-2.1l-3,0.7l1.3-2.8L69,47.4l3.1,0.3l2-2.4l0.6,3l2.9,1.2L74.8,51.1z M75.4,31l-0.2,2.8l-2.1-1.9l-2.7,0.6l1.1-2.5
                              l-1.4-2.4l2.7,0.3l1.8-2.1l0.6,2.7l2.6,1.1L75.4,31z"></path>
                        </g>
                        <g>
                           <path class="st0" d="M105.1,32.2h11.2v30.7h18.9v8.9h-30.1V32.2z"></path>
                           <path class="st0" d="M171.5,63.1v8.7h-31.8V32.2h31.1v8.7h-20v6.7h17.6V56h-17.6v7.2L171.5,63.1L171.5,63.1z"></path>
                           <path class="st0" d="M204.2,51.2h9.9v16.4c-4.6,3.3-10.9,5-16.6,5c-12.6,0-21.8-8.6-21.8-20.6s9.2-20.6,22.1-20.6
                              c7.4,0,13.4,2.5,17.3,7.2L208,45c-2.7-3-5.8-4.4-9.6-4.4c-6.8,0-11.3,4.5-11.3,11.3c0,6.7,4.5,11.3,11.2,11.3
                              c2.1,0,4.1-0.4,6.1-1.3L204.2,51.2L204.2,51.2z"></path>
                           <path class="st0" d="M253,63.1v8.7h-31.8V32.2h31.1v8.7h-20v6.7h17.6V56h-17.6v7.2L253,63.1L253,63.1z"></path>
                           <path class="st0" d="M259.2,32.2h11.2v30.7h18.9v8.9h-30.1V32.2z"></path>
                        </g>
                     </g>
                  </svg>
                  <div class="f-16 f-md-16 w300 mt20 lh150 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-16 f-md-16 w300 lh150 white-clr text-xs-center">Copyright © LegelSuites</div>
                  <ul class="footer-ul w300 f-16 f-md-16 white-clr text-center text-md-right">
                     <li><a href="https://support.oppyo.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://legelsuites.co/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://legelsuites.co/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://legelsuites.co/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://legelsuites.co/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://legelsuites.co/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://legelsuites.co/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!--Footer Section End -->
      <script type="text/javascript">
         // Special handling for in-app browsers that don't always support new windows
         (function() {
             function browserSupportsNewWindows(userAgent) {
                 var rules = [
                     'FBIOS',
                     'Twitter for iPhone',
                     'WebView',
                     '(iPhone|iPod|iPad)(?!.*Safari\/)',
                     'Android.*(wv|\.0\.0\.0)'
                 ];
                 var pattern = new RegExp('(' + rules.join('|') + ')', 'ig');
                 return !pattern.test(userAgent);
             }
         
             if (!browserSupportsNewWindows(navigator.userAgent || navigator.vendor || window.opera)) {
                 document.getElementById('af-form-1316343921').parentElement.removeAttribute('target');
             }
         })();
      </script><script type="text/javascript">
         (function() {
             var IE = /*@cc_on!@*/false;
             if (!IE) { return; }
             if (document.compatMode && document.compatMode == 'BackCompat') {
                 if (document.getElementById("af-form-1316343921")) {
                     document.getElementById("af-form-1316343921").className = 'af-form af-quirksMode';
                 }
                 if (document.getElementById("af-body-1316343921")) {
                     document.getElementById("af-body-1316343921").className = "af-body inline af-quirksMode";
                 }
                 if (document.getElementById("af-header-1316343921")) {
                     document.getElementById("af-header-1316343921").className = "af-header af-quirksMode";
                 }
                 if (document.getElementById("af-footer-1316343921")) {
                     document.getElementById("af-footer-1316343921").className = "af-footer af-quirksMode";
                 }
             }
         })();
      </script>
   </body>
</html>
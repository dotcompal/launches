<!DOCTYPE html>
<html>
   <head>
      <title>Bonus Landing Page Generator</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=9">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
      <meta name="title" content="LegelSuites Special Bonuses">
      <meta name="description" content="LegelSuites Special Bonuses">
      <meta name="keywords" content="LegelSuites Special Bonuses">
      <meta property="og:image" content="https://www.legelsuites.co/special-bonus/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Dr. Amit Pareek">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="LegelSuites Special Bonuses">
      <meta property="og:description" content="LegelSuites Special Bonuses">
      <meta property="og:image" content="https://www.legelsuites.co/special-bonus/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="LegelSuites Special Bonuses">
      <meta property="twitter:description" content="LegelSuites Special Bonuses">
      <meta property="twitter:image" content="https://www.legelsuites.co/special-bonus/thumbnail.png">
      <!-- Shortcut Icon  -->
      <link rel="icon" href="https://cdn.oppyo.com/launches/legelsuites/common_assets/images/favicon.png" type="image/png">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&family=Lexend:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
      <!--Load External CSS -->
      <link rel="stylesheet" href="https://cdn.oppyo.com/launches/legelsuites/common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" href="assets/css/bonus-style.css" type="text/css" />
      <script src="https://cdn.oppyo.com/launches/legelsuites/common_assets/js/jquery.min.js"></script>
   </head>
   <body>
      <div class="whitesection">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
               <svg version="1.1" id="Layer_11" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                  viewBox="0 0 493.7 100" style="enable-background:new 0 0 493.7 100; max-height:60px;" xml:space="preserve">
                  <style type="text/css">
                     .st00{fill:#FFA829;}
                     .st11{fill:url(#SVGID_1_);}
                     .st22{fill:url(#SVGID_00000154400198293151545150000005639196215602350980_);}
                     .st33{fill:#0055FF;}
                     .st44{fill:#07132B;}
                  </style>
                  <path class="st00" d="M94.2,78.7c-10.5,3.9-23,5.6-36.1,4.4C23.9,79.9-2,58.5,0.1,35.2C1.7,18,18.3,4.5,40.7,0
                  C24.1,6.2,12.6,17.8,11.2,32.1C9.1,55.4,35,76.8,69.1,79.9C77.9,80.7,86.4,80.2,94.2,78.7z"/>
                     <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="18.0771" y1="1067.2471" x2="45.1577" y2="1040.1664" gradientTransform="matrix(1 0 0 1 0 -978)">
                        <stop  offset="0" style="stop-color:#0055FF"/>
                        <stop  offset="1" style="stop-color:#008CFF"/>
                     </linearGradient>
                     <path class="st11" d="M66.3,85.8c-2,0-3.9-0.1-5.9-0.2c-1.3,1.1-2.7,2.1-4.2,3.1c-4.5,2.8-9.3,4.6-14.5,5.4c-5.1-0.8-10-2.6-14.5-5.4
                        c-8.7-5.4-14.6-13.8-17-23.4c-2.8-2.7-5.1-5.6-7-8.6c0.2,15.2,8,29,20.9,37.1c5.3,3.3,11,5.4,17.2,6.3l0.4,0.1l0.4-0.1
                        c6.1-0.9,11.9-3,17.2-6.3c3.6-2.2,6.7-4.9,9.5-7.9C68,85.8,67.1,85.8,66.3,85.8z"/>
                     <linearGradient id="SVGID_00000124841396298535227280000000355205612494478977_" gradientUnits="userSpaceOnUse" x1="31.5027" y1="1040.897" x2="78.3498" y2="994.0498" gradientTransform="matrix(1 0 0 1 0 -978)">
                        <stop  offset="0" style="stop-color:#0055FF"/>
                        <stop  offset="1" style="stop-color:#008CFF"/>
                     </linearGradient>
                     <path style="fill:url(#SVGID_00000124841396298535227280000000355205612494478977_);" d="M41.7,7.9l-17.5,4.5
                     c-2,1.9-21.6,31.5,10.2,51.6l2.2-13c-2.2-1.6-3.6-4.2-3.6-7c0-4.8,3.9-8.7,8.7-8.7s8.7,3.9,8.7,8.7c0,2.9-1.4,5.4-3.6,7l3.4,20.5
                     C57,74,65,76.1,74.4,77.8c3.7-6.5,5.7-14,5.7-21.9V17.8L41.7,7.9z M38.3,15.5l-0.2,2.2l-1.6-1.4l-2.1,0.5l0.9-2l-1.1-1.9l2.2,0.2
                     l1.4-1.6l0.4,2.1l2,0.8L38.3,15.5z M57.9,20.7l-0.2,2.5l-1.8-1.7l-2.4,0.6l1-2.2l-1.3-2.1l2.5,0.3l1.6-1.9l0.5,2.4l2.3,1L57.9,20.7z
                     M72.2,69.3L72,72.6l-2.5-2.3l-3.3,0.8l1.4-3.1l-1.8-2.9l3.4,0.4l2.2-2.5l0.7,3.3l3.1,1.3L72.2,69.3z M72.8,49.1l-0.2,3.1l-2.3-2.1
                     l-3,0.7l1.3-2.8L67,45.4l3.1,0.3l2-2.4l0.6,3l2.9,1.2L72.8,49.1z M73.4,29l-0.2,2.8l-2.1-1.9l-2.7,0.6l1.1-2.5l-1.4-2.4l2.7,0.3
                     l1.8-2.1l0.6,2.7l2.6,1.1L73.4,29z"/>
                     <path class="st33" d="M103.1,30.2h11.2v30.7h18.9v8.9h-30.1V30.2z"/>
                     <path class="st33" d="M169.5,61.1v8.7h-31.8V30.2h31.1v8.7h-20v6.7h17.6V54h-17.6v7.2L169.5,61.1L169.5,61.1z"/>
                     <path class="st33" d="M202.2,49.2h9.9v16.4c-4.6,3.3-10.9,5-16.6,5c-12.6,0-21.8-8.6-21.8-20.6s9.2-20.6,22.1-20.6
                     c7.4,0,13.4,2.5,17.3,7.2L206,43c-2.7-3-5.8-4.4-9.6-4.4c-6.8,0-11.3,4.5-11.3,11.3c0,6.7,4.5,11.3,11.2,11.3c2.1,0,4.1-0.4,6.1-1.3
                     L202.2,49.2L202.2,49.2z"/>
                     <path class="st33" d="M251,61.1v8.7h-31.8V30.2h31.1v8.7h-20v6.7h17.6V54h-17.6v7.2L251,61.1L251,61.1z"/>
                     <path class="st33" d="M257.2,30.2h11.2v30.7h18.9v8.9h-30.1V30.2z"/>
                     <path class="st44" d="M289.8,65.6l2.5-5.7c3.2,2.5,8.2,4.3,13.1,4.3c6.2,0,8.8-2.2,8.8-5.1c0-8.6-23.6-3-23.6-17.7
                     c0-6.4,5.1-11.8,16-11.8c4.8,0,9.7,1.3,13.2,3.5l-2.3,5.7c-3.6-2.1-7.5-3.1-10.9-3.1c-6.1,0-8.6,2.4-8.6,5.4c0,8.4,23.5,3,23.5,17.5
                     c0,6.3-5.1,11.8-16,11.8C299.2,70.4,293,68.4,289.8,65.6z"/>
                     <path class="st44" d="M462,65.6l2.5-5.7c3.2,2.5,8.2,4.3,13.1,4.3c6.2,0,8.8-2.2,8.8-5.1c0-8.6-23.6-3-23.6-17.7
                     c0-6.4,5.1-11.8,16-11.8c4.8,0,9.7,1.3,13.2,3.5l-2.3,5.7c-3.6-2.1-7.5-3.1-10.9-3.1c-6.1,0-8.6,2.4-8.6,5.4c0,8.4,23.5,3,23.5,17.5
                     c0,6.3-5.1,11.8-16,11.8C471.4,70.4,465.2,68.4,462,65.6z"/>
                     <path class="st44" d="M328.3,52.5V30.2h7.3v22.1c0,8.1,3.7,11.7,10.1,11.7s10-3.5,10-11.7V30.2h7.2v22.3c0,11.6-6.5,17.8-17.3,17.8
                     C334.9,70.4,328.3,64.1,328.3,52.5z"/>
                     <path class="st44" d="M373.3,30.2h7.3v39.6h-7.3V30.2z"/>
                     <path class="st44" d="M399.4,36.4h-13.1v-6.2h33.6v6.2h-13.1v33.4h-7.3V36.4H399.4z"/>
                     <path class="st44" d="M455.2,63.6v6.2h-29.7V30.2h28.9v6.2h-21.6v10.3H452v6.1h-19.1v10.9L455.2,63.6L455.2,63.6z"/>
                  </svg>
               </div>
               <div class="col-12 f-md-45 f-28 w700 text-center black-clr lh150 mt20">
                  Here's How You Can Generate <br class="d-none d-md-block"> Your Own Bonus Page Easily
               </div>
            </div>
         </div>
      </div>
      <div class="formsection">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-6 col-12">
                  <img src="assets/images/product-image.webp" class="img-fluid mx-auto d-block" alt="ProductBox">
               </div>
               <div class="col-md-6 col-12 mt20 mt-md0">
                  <?php //echo "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI']; ?>
                  <?php //echo "http://" . $_SERVER['SERVER_NAME'] ."/". basename(__DIR__)."/index.php?name=".$name."&affid=".$affid."&pic=".$filename;  ?>
                  <?php
                     if(isset($_POST['submit'])) 
                     {
                        $name=$_REQUEST['name'];
                        $afflink=$_REQUEST['afflink'];
                        //$picname=$_REQUEST['pic'];
                        /*$tmpname=$_FILES['pic']['tmp_name'];
                        $type=$_FILES['pic']['type'];
                        $size=$_FILES['pic']['size']/1024;
                        $rand=rand(1111111,9999999999);*/
                        if($afflink=="")
                        {
                           echo 'Please fill the details.';
                        }
                        else
                        {
                           /*if(($type!="image/jpg" && $type!="image/jpeg" && $type!="image/png" && $type!="image/gif" && $type!="image/bmp") or $size>1024)
                           {
                              echo "Please upload image file (size must be less than 1 MB)";	
                           }
                           else
                           {*/
                              //$filename=$rand."-".$picname;
                              //move_uploaded_file($tmpname,"images/".$filename);
                              $url="https://www.legelsuites.co/special-bonus/?afflink=".trim($afflink)."&name=".urlencode(trim($name));
                              echo "<a target='_blank' href=".$url.">".$url."</a><br>";
                              //header("Location:$url");
                           //}	
                        }
                     }
                     ?>
                  <form class="row" action="" method="post" enctype="multipart/form-data">
                     <div class="col-12 form_area ">
                        <div class="col-12 clear">
                           <input type="text" name="name" placeholder="Your Name..." required>
                        </div>
                        <div class="col-12 mt20 clear">
                           <input type="text" name="afflink" placeholder="Your Affiliate Link..." required>
                        </div>
                        <div class="col-12 f-24 f-md-30 white-clr center-block mt10 mt-md20 w500 clear">
                           <input type="submit" value="Generate Page" name="submit" class="f-md-30 f-24" />
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
      <!--Footer Section Start -->
      <div class="footer-section" style="font-size: 14px;">
         <div class="container " style="font-size: 14px;">
            <div class="row" style="font-size: 14px;">
               <div class="col-12 text-center" style="font-size: 14px;">
               <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 495.7 104.2" style="enable-background:new 0 0 495.7 104.2; max-height:60px;" xml:space="preserve">
                              <style type="text/css">
                                 .st0{fill:#FFFFFF;}
                                 .st1{fill:#FFA829;}
                                 .st2{fill:url(#SVGID_1_);}
                                 .st3{fill:url(#SVGID_00000132794433514585357610000014266780508499585408_);}
                              </style>
                              <g>
                                 <path class="st0" d="M96.9,82.6c-7.5,2.8-15.7,4.4-24.2,4.8c0.2,0.7,0,1.4-0.5,1.9c-3.2,3.4-6.4,6.1-9.9,8.2
                                    c-5.4,3.4-11.5,5.6-17.9,6.5l-0.3,0.1c-0.2,0-0.3,0.1-0.5,0.1c-0.2,0-0.3,0-0.5-0.1l-0.3-0.1c-6.4-0.9-12.4-3.1-17.9-6.6
                                    C11.6,89,3.4,74.5,3.2,58.7c0-0.9,0.6-1.7,1.4-2c0,0,0.1,0,0.1,0C1.2,50.6-0.5,43.9,0.1,37C1.8,19.4,18.3,4.9,42.3,0
                                    c1-0.2,2,0.4,2.3,1.4c0.3,1-0.2,2.1-1.2,2.4c-5.4,2-10.2,4.6-14.2,7.7l14-3.6c0.3-0.1,0.7-0.1,1,0l38.4,9.9c0.9,0.2,1.5,1,1.5,1.9
                                    v38.1c0,7.9-2,15.6-5.7,22.4c5.9,0.1,11.7-0.4,17.4-1.5c1-0.2,2,0.4,2.3,1.4C98.4,81.2,97.9,82.2,96.9,82.6z"></path>
                                 <g>
                                    <path class="st0" d="M291.8,67.6l2.5-5.7c3.2,2.5,8.2,4.3,13.1,4.3c6.2,0,8.8-2.2,8.8-5.1c0-8.6-23.6-3-23.6-17.7
                                       c0-6.4,5.1-11.8,16-11.8c4.8,0,9.7,1.3,13.2,3.5l-2.3,5.7c-3.6-2.1-7.5-3.1-10.9-3.1c-6.1,0-8.6,2.4-8.6,5.4
                                       c0,8.4,23.5,3,23.5,17.5c0,6.3-5.1,11.8-16,11.8C301.2,72.4,295,70.4,291.8,67.6z"></path>
                                    <path class="st0" d="M464,67.6l2.5-5.7c3.2,2.5,8.2,4.3,13.1,4.3c6.2,0,8.8-2.2,8.8-5.1c0-8.6-23.6-3-23.6-17.7
                                       c0-6.4,5.1-11.8,16-11.8c4.8,0,9.7,1.3,13.2,3.5l-2.3,5.7c-3.6-2.1-7.5-3.1-10.9-3.1c-6.1,0-8.6,2.4-8.6,5.4
                                       c0,8.4,23.5,3,23.5,17.5c0,6.3-5.1,11.8-16,11.8C473.4,72.4,467.2,70.4,464,67.6z"></path>
                                    <path class="st0" d="M330.3,54.5V32.2h7.3v22.1c0,8.1,3.7,11.7,10.1,11.7s10-3.5,10-11.7V32.2h7.2v22.3c0,11.6-6.5,17.8-17.3,17.8
                                       C336.9,72.4,330.3,66.1,330.3,54.5z"></path>
                                    <path class="st0" d="M375.3,32.2h7.3v39.6h-7.3V32.2z"></path>
                                    <path class="st0" d="M401.4,38.4h-13.1v-6.2h33.6v6.2h-13.1v33.4h-7.3V38.4H401.4z"></path>
                                    <path class="st0" d="M457.2,65.6v6.2h-29.7V32.2h28.9v6.2h-21.6v10.3H454v6.1h-19.1v10.9L457.2,65.6L457.2,65.6z"></path>
                                 </g>
                                 <g>
                                    <path class="st1" d="M96.2,80.7c-10.5,3.9-23,5.6-36.1,4.4C25.9,81.9,0,60.5,2.1,37.2C3.7,20,20.3,6.5,42.7,2
                                       C26.1,8.2,14.6,19.8,13.2,34.1C11.1,57.4,37,78.8,71.1,81.9C79.9,82.7,88.4,82.2,96.2,80.7z"></path>
                                    <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="20.0931" y1="1069.2449" x2="47.1737" y2="1042.1643" gradientTransform="matrix(1 0 0 1 0 -978)">
                                       <stop offset="0" style="stop-color:#0055FF"></stop>
                                       <stop offset="1" style="stop-color:#008CFF"></stop>
                                    </linearGradient>
                                    <path class="st2" d="M68.3,87.8c-2,0-3.9-0.1-5.9-0.2c-1.3,1.1-2.7,2.1-4.2,3.1c-4.5,2.8-9.3,4.6-14.5,5.4
                                       c-5.1-0.8-10-2.6-14.5-5.4c-8.7-5.4-14.6-13.8-17-23.4c-2.8-2.7-5.1-5.6-7-8.6c0.2,15.2,8,29,20.9,37.1c5.3,3.3,11,5.4,17.2,6.3
                                       l0.4,0.1l0.4-0.1c6.1-0.9,11.9-3,17.2-6.3c3.6-2.2,6.7-4.9,9.5-7.9C70,87.8,69.1,87.8,68.3,87.8z"></path>
                                    <linearGradient id="SVGID_00000129182319601907477880000006569859738938224028_" gradientUnits="userSpaceOnUse" x1="33.5187" y1="1042.8949" x2="80.3658" y2="996.0477" gradientTransform="matrix(1 0 0 1 0 -978)">
                                       <stop offset="0" style="stop-color:#0055FF"></stop>
                                       <stop offset="1" style="stop-color:#008CFF"></stop>
                                    </linearGradient>
                                    <path style="fill:url(#SVGID_00000129182319601907477880000006569859738938224028_);" d="M43.7,9.9l-17.5,4.5
                                       c-2,1.9-21.6,31.5,10.2,51.6l2.2-13c-2.2-1.6-3.6-4.2-3.6-7c0-4.8,3.9-8.7,8.7-8.7s8.7,3.9,8.7,8.7c0,2.9-1.4,5.4-3.6,7l3.4,20.5
                                       C59,76,67,78.1,76.4,79.8c3.7-6.5,5.7-14,5.7-21.9V19.8L43.7,9.9z M40.3,17.5l-0.2,2.2l-1.6-1.4l-2.1,0.5l0.9-2l-1.1-1.9l2.2,0.2
                                       l1.4-1.6l0.4,2.1l2,0.8L40.3,17.5z M59.9,22.7l-0.2,2.5l-1.8-1.7l-2.4,0.6l1-2.2l-1.3-2.1l2.5,0.3l1.6-1.9l0.5,2.4l2.3,1
                                       L59.9,22.7z M74.2,71.3L74,74.6l-2.5-2.3l-3.3,0.8l1.4-3.1l-1.8-2.9l3.4,0.4l2.2-2.5l0.7,3.3l3.1,1.3L74.2,71.3z M74.8,51.1
                                       l-0.2,3.1l-2.3-2.1l-3,0.7l1.3-2.8L69,47.4l3.1,0.3l2-2.4l0.6,3l2.9,1.2L74.8,51.1z M75.4,31l-0.2,2.8l-2.1-1.9l-2.7,0.6l1.1-2.5
                                       l-1.4-2.4l2.7,0.3l1.8-2.1l0.6,2.7l2.6,1.1L75.4,31z"></path>
                                 </g>
                                 <g>
                                    <path class="st0" d="M105.1,32.2h11.2v30.7h18.9v8.9h-30.1V32.2z"></path>
                                    <path class="st0" d="M171.5,63.1v8.7h-31.8V32.2h31.1v8.7h-20v6.7h17.6V56h-17.6v7.2L171.5,63.1L171.5,63.1z"></path>
                                    <path class="st0" d="M204.2,51.2h9.9v16.4c-4.6,3.3-10.9,5-16.6,5c-12.6,0-21.8-8.6-21.8-20.6s9.2-20.6,22.1-20.6
                                       c7.4,0,13.4,2.5,17.3,7.2L208,45c-2.7-3-5.8-4.4-9.6-4.4c-6.8,0-11.3,4.5-11.3,11.3c0,6.7,4.5,11.3,11.2,11.3
                                       c2.1,0,4.1-0.4,6.1-1.3L204.2,51.2L204.2,51.2z"></path>
                                    <path class="st0" d="M253,63.1v8.7h-31.8V32.2h31.1v8.7h-20v6.7h17.6V56h-17.6v7.2L253,63.1L253,63.1z"></path>
                                    <path class="st0" d="M259.2,32.2h11.2v30.7h18.9v8.9h-30.1V32.2z"></path>
                                 </g>
                              </g>
                           </svg>
                  <div class="f-16 f-md-18 w300 mt20 lh150 white-clr text-center" style="font-size: 16px;">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40" style="font-size: 14px;">
                  <div class="f-16 f-md-18 w300 lh150 white-clr text-xs-center" style="font-size: 16px;">Copyright © LegelSuites 2022</div>
                  <ul class="footer-ul f-16 f-md-18 w300 white-clr text-center text-md-right">
                     <li style="font-size: 16px;"><a href="https://support.oppyo.com/hc/en-us" class="white-clr t-decoration-none" style="font-size: 16px;">Contact</a> <span class="px5" style="font-size: 16px;">|</span> </li>
                     <li style="font-size: 16px;"><a href="http://legelsuites.co/legal/privacy-policy.html" class="white-clr t-decoration-none" style="font-size: 16px;">Privacy</a> <span class="px5" style="font-size: 16px;">|</span> </li>
                     <li style="font-size: 16px;"><a href="http://legelsuites.co/legal/terms-of-service.html" class="white-clr t-decoration-none" style="font-size: 16px;">T&amp;C</a> <span class="px5" style="font-size: 16px;">|</span> </li>
                     <li style="font-size: 16px;"><a href="http://legelsuites.co/legal/disclaimer.html" class="white-clr t-decoration-none" style="font-size: 16px;">Disclaimer</a> <span class="px5" style="font-size: 16px;">|</span> </li>
                     <li style="font-size: 16px;"><a href="http://legelsuites.co/legal/gdpr.html" class="white-clr t-decoration-none" style="font-size: 16px;">GDPR</a> <span class="px5" style="font-size: 16px;">|</span> </li>
                     <li style="font-size: 16px;"><a href="http://legelsuites.co/legal/dmca.html" class="white-clr t-decoration-none" style="font-size: 16px;">DMCA</a> <span class="px5" style="font-size: 16px;">|</span> </li>
                     <li style="font-size: 16px;"><a href="http://legelsuites.co/legal/anti-spam.html" class="white-clr t-decoration-none" style="font-size: 16px;">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!--Footer Section End -->
   </body>
</html>
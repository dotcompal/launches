<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <!-- Tell the browser to be responsive to screen width -->
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
      <meta name="title" content="LegelSuites Bonuses">
      <meta name="description" content="LegelSuites Bonuses">
      <meta name="keywords" content="Have A Sneak Peak of A Breakthrough Technology That Analyzes, Detects & Fix Deadly Legal Flaws on Your Website Just by Copy Pasting a Single Line of Code">
      <meta property="og:image" content="https://www.legelsuites.co/special-bonus/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Atul">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="LegelSuites Bonuses">
      <meta property="og:description" content="Have A Sneak Peak of A Breakthrough Technology That Analyzes, Detects & Fix Deadly Legal Flaws on Your Website Just by Copy Pasting a Single Line of Code">
      <meta property="og:image" content="https://www.legelsuites.co/special-bonus/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="LegelSuites Bonuses">
      <meta property="twitter:description" content="Have A Sneak Peak of A Breakthrough Technology That Analyzes, Detects & Fix Deadly Legal Flaws on Your Website Just by Copy Pasting a Single Line of Code">
      <meta property="twitter:image" content="https://www.legelsuites.co/special-bonus/thumbnail.png">
      <title>LegelSuites Bonuses</title>
      <!-- Shortcut Icon  -->
      <link rel="icon" href="https://cdn.oppyo.com/launches/legelsuites/common_assets/images/favicon.png" type="image/png">
      <!-- Css CDN Load Link -->
      <link rel="stylesheet" href="https://cdn.oppyo.com/launches/legelsuites/common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" type="text/css" href="assets/css/bonus-style.css">

      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&family=Lexend:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">

      <link href="https://fonts.googleapis.com/css2?family=Pacifico&display=swap" rel="stylesheet">
      <script src="https://cdn.oppyo.com/launches/legelsuites/common_assets/js/jquery.min.js"></script>
      <script type="text/javascript" src="https://www.legelsuites.com/widget/ada/oppyo-GTtd-wozS-Rm7H-DqGe" crossorigin="anonymous" embed-id="oppyo-GTtd-wozS-Rm7H-DqGe"></script>
   </head>
   <body>
      <!-- New Timer  Start-->
      <?php
         $date = 'Nov 17 2022 02:00 PM EST';
         $exp_date = strtotime($date);
         $now = time();  
         /*
         
         $date = date('F d Y g:i:s A eO');
         $rand_time_add = rand(700, 1200);
         $exp_date = strtotime($date) + $rand_time_add;
         $now = time();*/
         
         if ($now < $exp_date) {
         ?>
      <?php
         } else {
         	echo "Times Up";
         }
         ?>
      <!-- New Timer End -->
      <?php
         if(!isset($_GET['afflink'])){
         $_GET['afflink'] = 'https://jvz1.com/c/47069/388896/';
         $_GET['name'] = 'Pranshu';      
         }
         ?>
      
      <!-- Header Section Start -->   
      <div class="main-header">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="text-center">
                     <div class="f-md-32 f-20 lh140 w400 white-clr d-block d-md-flex align-items-center justify-content-center">
                        <span class="w600 orange-clr"><?php echo $_GET['name'];?>'s</span> &nbsp;special bonus for &nbsp;
                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 495.7 104.2" style="enable-background:new 0 0 495.7 104.2; max-height:60px;" xml:space="preserve">
                           <style type="text/css">
                              .st0{fill:#FFFFFF;}
                              .st1{fill:#FFA829;}
                              .st2{fill:url(#SVGID_1_);}
                              .st3{fill:url(#SVGID_00000132794433514585357610000014266780508499585408_);}
                           </style>
                           <g>
                              <path class="st0" d="M96.9,82.6c-7.5,2.8-15.7,4.4-24.2,4.8c0.2,0.7,0,1.4-0.5,1.9c-3.2,3.4-6.4,6.1-9.9,8.2
                              c-5.4,3.4-11.5,5.6-17.9,6.5l-0.3,0.1c-0.2,0-0.3,0.1-0.5,0.1c-0.2,0-0.3,0-0.5-0.1l-0.3-0.1c-6.4-0.9-12.4-3.1-17.9-6.6
                              C11.6,89,3.4,74.5,3.2,58.7c0-0.9,0.6-1.7,1.4-2c0,0,0.1,0,0.1,0C1.2,50.6-0.5,43.9,0.1,37C1.8,19.4,18.3,4.9,42.3,0
                              c1-0.2,2,0.4,2.3,1.4c0.3,1-0.2,2.1-1.2,2.4c-5.4,2-10.2,4.6-14.2,7.7l14-3.6c0.3-0.1,0.7-0.1,1,0l38.4,9.9c0.9,0.2,1.5,1,1.5,1.9
                              v38.1c0,7.9-2,15.6-5.7,22.4c5.9,0.1,11.7-0.4,17.4-1.5c1-0.2,2,0.4,2.3,1.4C98.4,81.2,97.9,82.2,96.9,82.6z"></path>
                              <g>
                                 <path class="st0" d="M291.8,67.6l2.5-5.7c3.2,2.5,8.2,4.3,13.1,4.3c6.2,0,8.8-2.2,8.8-5.1c0-8.6-23.6-3-23.6-17.7
                                 c0-6.4,5.1-11.8,16-11.8c4.8,0,9.7,1.3,13.2,3.5l-2.3,5.7c-3.6-2.1-7.5-3.1-10.9-3.1c-6.1,0-8.6,2.4-8.6,5.4
                                 c0,8.4,23.5,3,23.5,17.5c0,6.3-5.1,11.8-16,11.8C301.2,72.4,295,70.4,291.8,67.6z"></path>
                                 <path class="st0" d="M464,67.6l2.5-5.7c3.2,2.5,8.2,4.3,13.1,4.3c6.2,0,8.8-2.2,8.8-5.1c0-8.6-23.6-3-23.6-17.7
                                 c0-6.4,5.1-11.8,16-11.8c4.8,0,9.7,1.3,13.2,3.5l-2.3,5.7c-3.6-2.1-7.5-3.1-10.9-3.1c-6.1,0-8.6,2.4-8.6,5.4
                                 c0,8.4,23.5,3,23.5,17.5c0,6.3-5.1,11.8-16,11.8C473.4,72.4,467.2,70.4,464,67.6z"></path>
                                 <path class="st0" d="M330.3,54.5V32.2h7.3v22.1c0,8.1,3.7,11.7,10.1,11.7s10-3.5,10-11.7V32.2h7.2v22.3c0,11.6-6.5,17.8-17.3,17.8
                                 C336.9,72.4,330.3,66.1,330.3,54.5z"></path>
                                 <path class="st0" d="M375.3,32.2h7.3v39.6h-7.3V32.2z"></path>
                                 <path class="st0" d="M401.4,38.4h-13.1v-6.2h33.6v6.2h-13.1v33.4h-7.3V38.4H401.4z"></path>
                                 <path class="st0" d="M457.2,65.6v6.2h-29.7V32.2h28.9v6.2h-21.6v10.3H454v6.1h-19.1v10.9L457.2,65.6L457.2,65.6z"></path>
                              </g>
                              <g>
                                 <path class="st1" d="M96.2,80.7c-10.5,3.9-23,5.6-36.1,4.4C25.9,81.9,0,60.5,2.1,37.2C3.7,20,20.3,6.5,42.7,2
                                 C26.1,8.2,14.6,19.8,13.2,34.1C11.1,57.4,37,78.8,71.1,81.9C79.9,82.7,88.4,82.2,96.2,80.7z"></path>
                                 <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="20.0931" y1="1069.2449" x2="47.1737" y2="1042.1643" gradientTransform="matrix(1 0 0 1 0 -978)">
                                    <stop offset="0" style="stop-color:#0055FF"></stop>
                                    <stop offset="1" style="stop-color:#008CFF"></stop>
                                 </linearGradient>
                                 <path class="st2" d="M68.3,87.8c-2,0-3.9-0.1-5.9-0.2c-1.3,1.1-2.7,2.1-4.2,3.1c-4.5,2.8-9.3,4.6-14.5,5.4
                                 c-5.1-0.8-10-2.6-14.5-5.4c-8.7-5.4-14.6-13.8-17-23.4c-2.8-2.7-5.1-5.6-7-8.6c0.2,15.2,8,29,20.9,37.1c5.3,3.3,11,5.4,17.2,6.3
                                 l0.4,0.1l0.4-0.1c6.1-0.9,11.9-3,17.2-6.3c3.6-2.2,6.7-4.9,9.5-7.9C70,87.8,69.1,87.8,68.3,87.8z"></path>
                                 <linearGradient id="SVGID_00000129182319601907477880000006569859738938224028_" gradientUnits="userSpaceOnUse" x1="33.5187" y1="1042.8949" x2="80.3658" y2="996.0477" gradientTransform="matrix(1 0 0 1 0 -978)">
                                    <stop offset="0" style="stop-color:#0055FF"></stop>
                                    <stop offset="1" style="stop-color:#008CFF"></stop>
                                 </linearGradient>
                                 <path style="fill:url(#SVGID_00000129182319601907477880000006569859738938224028_);" d="M43.7,9.9l-17.5,4.5
                                 c-2,1.9-21.6,31.5,10.2,51.6l2.2-13c-2.2-1.6-3.6-4.2-3.6-7c0-4.8,3.9-8.7,8.7-8.7s8.7,3.9,8.7,8.7c0,2.9-1.4,5.4-3.6,7l3.4,20.5
                                 C59,76,67,78.1,76.4,79.8c3.7-6.5,5.7-14,5.7-21.9V19.8L43.7,9.9z M40.3,17.5l-0.2,2.2l-1.6-1.4l-2.1,0.5l0.9-2l-1.1-1.9l2.2,0.2
                                 l1.4-1.6l0.4,2.1l2,0.8L40.3,17.5z M59.9,22.7l-0.2,2.5l-1.8-1.7l-2.4,0.6l1-2.2l-1.3-2.1l2.5,0.3l1.6-1.9l0.5,2.4l2.3,1
                                 L59.9,22.7z M74.2,71.3L74,74.6l-2.5-2.3l-3.3,0.8l1.4-3.1l-1.8-2.9l3.4,0.4l2.2-2.5l0.7,3.3l3.1,1.3L74.2,71.3z M74.8,51.1
                                 l-0.2,3.1l-2.3-2.1l-3,0.7l1.3-2.8L69,47.4l3.1,0.3l2-2.4l0.6,3l2.9,1.2L74.8,51.1z M75.4,31l-0.2,2.8l-2.1-1.9l-2.7,0.6l1.1-2.5
                                 l-1.4-2.4l2.7,0.3l1.8-2.1l0.6,2.7l2.6,1.1L75.4,31z"></path>
                              </g>
                              <g>
                                 <path class="st0" d="M105.1,32.2h11.2v30.7h18.9v8.9h-30.1V32.2z"></path>
                                 <path class="st0" d="M171.5,63.1v8.7h-31.8V32.2h31.1v8.7h-20v6.7h17.6V56h-17.6v7.2L171.5,63.1L171.5,63.1z"></path>
                                 <path class="st0" d="M204.2,51.2h9.9v16.4c-4.6,3.3-10.9,5-16.6,5c-12.6,0-21.8-8.6-21.8-20.6s9.2-20.6,22.1-20.6
                                 c7.4,0,13.4,2.5,17.3,7.2L208,45c-2.7-3-5.8-4.4-9.6-4.4c-6.8,0-11.3,4.5-11.3,11.3c0,6.7,4.5,11.3,11.2,11.3
                                 c2.1,0,4.1-0.4,6.1-1.3L204.2,51.2L204.2,51.2z"></path>
                                 <path class="st0" d="M253,63.1v8.7h-31.8V32.2h31.1v8.7h-20v6.7h17.6V56h-17.6v7.2L253,63.1L253,63.1z"></path>
                                 <path class="st0" d="M259.2,32.2h11.2v30.7h18.9v8.9h-30.1V32.2z"></path>
                              </g>
                           </g>
                        </svg>
                     </div>
                  </div>
               </div>
            </div>
         </div>

         <div class="header-wrap">
            <div class="container">
               <div class="row">
                  <div class="col-12 text-center">
                     <div class="preheadline f-16 f-md-22 w400 white-clr lh140">
                        Grab My 20 Exclusive Bonuses Before the Deal Ends...
                     </div>
                  </div>

                  <div class="col-12 mt-md30 mt20 f-md-45 f-28 w700 text-center cyan-clr lh140">
                  Have A Sneak Peak of A Breakthrough Technology That Analyzes, Detects & Fix Deadly Legal Flaws
                  on Your Website Just by Copy Pasting a Single Line of Code
                  </div>

                  <div class="col-12 mt-md30 mt20 f-18 f-md-26 w500 text-center lh130 white-clr text-capitalize">
                     <div class="f-18 f-md-26 w500 text-center lh130 white-clr text-capitalize">               
                        Fix Deadly Law Issues Like ADA, GDPR, T&C & Much More | Comes with<br class="d-none d-md-block"> Done-For-You Agency Website to Quick Start
                        <img src="assets/images/h-line.webp" alt="H-Line" class="mx-auto img-fluid d-none d-md-block mt15">
                     </div>
                  </div>

                  <div class="col-12 mt20 mt-md40">
                     <div class="f-16 f-md-22 w400 text-center lh130 white-clr text-capitalize">               
                     Watch My Quick LegelSuites Review
                     </div>
                  </div>
                  <div class="row mt20 mt-md40">
                     <div class="col-md-10 col-12 mx-auto">
                        <div class="col-12 responsive-video border-video">
                           <iframe src="https://legelsuites.dotcompal.com/video/embed/mcsmui8s77" style="width:100%; height:100%" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                        </div>
                        <!-- <img src="assets/images/product-image.webp" class="img-fluid mx-auto d-block" alt="ProductBox"> -->
                     </div>
                  </div>

                  <div class="row row-cols-md-2 row-cols-xl-2 row-cols-1 mt20 mt-md40 calendar-wrap mx-auto">
                     <div class="col">
                        <ul class="list-head pl0 m0 f-18 f-md-18 lh140 w300 white-clr">
                        <li><span class="w500">Quick-Start with Built-in Agency Website Builder</span> with Drag & Drop Editor to Start Selling Services Starting Today</li>
                        </ul>
                     </div>
                     <div class="col">
                        <ul class="list-head pl0 m0 f-18 f-md-18 lh140 w300  white-clr">
                        <li><span class="w500">Scans Any Website with LegelSuites Analyzer</span> to detect Legal Flaws on your Website like GDPR, ADA, Privacy Policy, T&C, etc</li>
                        </ul>
                     </div>
                     <div class="col">
                        <ul class="list-head pl0 m0 f-18 f-md-18 lh140 w300  white-clr">
                           <li><span class="w500">Help You Make Your Website ADA Compliant</span> by Copy-Pasting a Single Line of Code</li>
                        </ul>
                     </div>
                     <div class="col">
                        <ul class="list-head pl0 m0 f-18 f-md-18 lh140 w300 white-clr">
                        <li><span class="w500">Get Your Website GDPR & Privacy Laws Comply</span> as per International Laws</li>
                        </ul>
                     </div>
                     <div class="col">
                        <ul class="list-head pl0 m0 f-18 f-md-18 lh140 w300 white-clr">
                        <li><span class="w500">Generate Cookies Policies & Cookies Widgets</span> with an inbuilt GDPR Tool</li>
                        </ul>
                     </div>
                     <div class="col">
                        <ul class="list-head pl0 m0 f-18 f-md-18 lh140 w300 white-clr">
                        <li><span class="w500">Copy Paste a Single Line Of Code</span> To Make Any Of Your Business Websites 100% Compliance</li>
                        </ul>
                     </div>
                     <div class="col">
                        <ul class="list-head pl0 m0 f-18 f-md-18 lh140 w300 white-clr">
                        <li><span class="w500">Get Tailor-Made Legal Policies</span> and Other Legal Documents For Your's And Your Client's Websites</li>
                        </ul>
                     </div>
                     <div class="col">
                        <ul class="list-head pl0 m0 f-18 f-md-18 lh140 w300 white-clr">
                        <li><span class="w500">Tap into Hot Website Agency Services to Make Huge Profits</span></li>
                        </ul>
                     </div>
                     <div class="col">
                        <ul class="list-head pl0 m0 f-18 f-md-18 lh140 w300 white-clr">
                        <li><span class="w500">Website Widget to add on your Agency Website</span> to Offer Free Legal & Compliance Tests to quickly generate leads</li>
                        </ul>
                     </div>
                     <div class="col">
                        <ul class="list-head pl0 m0 f-18 f-md-18 lh140 w300 white-clr">
                        <li><span class="w500">Step-by-Step Training</span> is Included to get you Started Today</li>
                        </ul>
                     </div>
                     <div class="col">
                        <ul class="list-head pl0 m0 f-18 f-md-18 lh140 w300 white-clr">
                        <li><span class="w500">Very Limited Agency License Included</span> to Serve Your Clients and Make Huge Profits</li>
                        </ul>
                     </div>
                  </div>

                  <div class="row mt20 mt-md50">
                     <div class="col-md-12 col-md-12 col-12 text-center mt20 mt-md20">
                        <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                        <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                        <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                        Use Coupon Code <span class="w700 cyan-clr">"PRANSHUVIP"</span> for an Additional <span class="w700 cyan-clr">$4 Discount</span> on Agency Licence</div>
                     </div>
                     <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                        <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                           <span class="text-center">Grab LegelSuites + My 20 Exclusive Bonuses</span>
                        </a>
                     </div>
                     <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                        Use Coupon Code <span class="w700 cyan-clr">"LEGELBUNDLE"</span> for an Additional <span class="w700 cyan-clr">$50 Discount</span> on Agency Licence
                     </div>
                     <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                        <a href="https://jvz4.com/c/47069/388898/" class="text-center bonusbuy-btn">
                        <span class="text-center">Grab LegelSuites Bundle + My 20 Exclusive Bonuses</span>
                        </a>
                     </div>
                     <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                        <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
                     </div>
                     <div class="col-12 mt15 mt-md40" align="center">
                        <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
                     </div>
                     <!-- Timer -->
                     <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                        <div class="countdown counter-white white-clr">
                           <div class="timer-label text-center">
                              <span class="f-31 f-md-60 timerbg oswald">00</span>
                              <br>
                              <span class="f-14 f-md-18 smmltd">Days</span>
                           </div>
                           <div class="timer-label text-center">
                              <span class="f-31 f-md-60 timerbg oswald">00</span>
                              <br>
                              <span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                           </div>
                           <div class="timer-label text-center timer-mrgn">
                              <span class="f-31 f-md-60 timerbg oswald">00</span>
                              <br>
                              <span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                           </div>
                           <div class="timer-label text-center ">
                              <span class="f-31 f-md-60 timerbg oswald">00</span>
                              <br>
                              <span class="f-14 f-md-18 w500 smmltd">Sec</span>
                           </div>
                        </div>
                     </div>
                     <!-- Timer End -->
                  </div>
               </div>
            </div>
         </div> 
      </div>
      <!-- Header Section End -->

      <!-- Step Section Start -->
      <div class="step-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-md-45 f-28 w700 lh140 text-center black-clr">
                  Make Any Website Legally Compliant <br> 
                  <span class="blue-clr">in Just  3 Single Steps </span>                     
                  </div>
               </div>
            </div>
            <div class="row mt30 mt-md80">
               <div class="col-md-4 col-12">
                  <div class="step-box">
                     <div class="step-title f-20 f-md-22 w700">Step 1</div>
                     <img src="assets/images/step-1.webp" class="img-fluid d-block mx-auto my20">
                     <div class="step-content">
                        <div class="f-24 f-md-36 w700 white-clr">Login </div>
                        <div class="w400 f-18 f-md-22 lh140 mt15 white-clr">
                           Login to Legelsuites Dashboard
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-4 col-12 mt30 mt-md0">
                  <div class="step-box">
                     <div class="step-title f-20 f-md-22 w700">Step 2</div>
                     <img src="assets/images/step-2.webp" class="img-fluid d-block mx-auto my20">
                     <div class="step-content">
                        <div class="f-24 f-md-36 w700 white-clr">Analyse &amp; Detect</div>
                        <div class="w400 f-18 f-md-22 lh140 mt15 white-clr">
                           Analyze your website with LegelSuites Website Checker and get the legal compliance issues detected.
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-4 col-12 mt30 mt-md0">
                  <div class="step-box">
                     <div class="step-title f-20 f-md-22 w700">Step 3</div>
                     <img src="assets/images/step-3.webp" class="img-fluid d-block mx-auto my20">
                     <div class="step-content">
                        <div class="f-24 f-md-36 w700 white-clr">Fix Legal Flaws</div>
                        <div class="w400 f-18 f-md-22 lh140 mt15 white-clr">
                           Get the legal flaws fixed on your website. LegelSuites Provide Fixes to 5+ International Laws for Websites.  
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md60">
               <div class="col-12 f-md-20 f-18 w400 lh140 black-clr text-center">
                  It only takes one line of code to add to your website and LegelSuites will get the rest of the work done for you, from attracting hot leads, get the client’s website fixed which will eventually make you a handsome profit.             
               </div>
               <div class="col-12 f-md-24 f-20 w700 lh140 black-clr text-center mt20">
                  No Installation, No Coding, and No Other Techie Stuff.           
               </div>
               <div class="col-12 f-md-24 f-20 w700 lh140 blue-clr text-center mt20">
                  It’s EASY, FAST, and BUILT with Love.  
               </div>
            </div>
         </div>
      </div>
      <!-- Step Section End -->

      <!-- CTA Btn Start-->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center mt20 mt-md20">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 cyan-clr">"PRANSHUVIP"</span> for an Additional <span class="w700 cyan-clr">$4 Discount</span> on Agency Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab LegelSuites + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                  Use Coupon Code <span class="w700 cyan-clr">"LEGELBUNDLE"</span> for an Additional <span class="w700 cyan-clr">$50 Discount</span> on Agency Licence
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="https://jvz4.com/c/47069/388898/" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab LegelSuites Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 smmltd">Days</span>
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Sec</span>
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Btn End -->
      
      <!-- Proven Section -->
      <div class="proof-section">
         <div class="container">
            <div class="row">
               <div class="f-28 f-md-45 w700 lh140 text-capitalize text-center black-clr col-12">                 
                  The Website Legal Compliance Service Is So High-In-Demand That We Got 20%+ Conversion From Our Existing Customer Base
               </div>
               <div class="col-12 mt20 mt-md50">
                  <img src="https://cdn.oppyo.com/launches/legelsuites/special/proof.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md50">
               <div class="col-12 col-md-6 f-md-32 f-22 w700 lh140 black-clr">
               Also Got 900+ New Leads on My Agency Website in Just 10 Days.                   
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                  <img src="https://cdn.oppyo.com/launches/legelsuites/special/proof1.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>
            <div class="row mt20 mt-md60">
               <div class="col-12 f-md-22 f-20 w400 lh140 black-clr text-center">
               To attract new leads of potential customers you just need to add a FREE website analyser (Comes with LegalSuite) on your website and LegalSuites will grab potential leads for you 24*7.          
               </div>
            </div>
         </div>
      </div>
      <!-- Proven Section Ends-->

      <!-- <section class="analyzer-sec">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-26 f-md-45 w700 lh140 text-center">
                     Test Drive The LegelSuites Website Analyzer
                  </div>
                  <img src="assets/images/zig-zag.webp" alt="Zig Zag Line" class="mx-auto img-fluid d-block mb20 mb-md30">
                  <script type="text/javascript" src="https://www.legelsuites.com/widget/checker/oppyo-JoU0-q3qS-XQsN-Gbz5" crossorigin="anonymous" embed-id="oppyo-JoU0-q3qS-XQsN-Gbz5"></script>
               </div>
            </div>
         </div>
      </section> -->


      <section class="didyouknow-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-36 f-md-45 w800 blue-clr text-center lh140">Did You Know?</div>
                  <img src="assets/images/know-line.webp" class="img-fluid d-none d-md-block mx-auto mt5">
                  <div class="youknow-block mt20 mt-md40">
                     <div class="f-24 f-md-38 w800 black-clr text-center lh140">
                        Over 100,000 Website Owners Have been Fined<br class="d-none d-md-block"> or Sued 
                        for Non-Compliance in the Last<br class="d-none d-md-block"> Couple of Years!                           
                     </div>
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md20 align-items-center">
               <div class="col-12 col-md-6">
                  <div class="f-22 f-md-28 w400 lh140 black-clr text-center text-md-start">                    
                     So, if you are a Website Owner, you can be targeted 
                     for issues on your website and you can get indulge 
                     in serious Law Problems or can be Fined.                      
                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                  <img src="assets/images/trillion.webp" class="img-fluid mx-auto d-block">
               </div>
            </div>
         </div>
      </section>

      <section class="compliance-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-45 w700 black-clr text-center lh140">Breaching Website Compliance Law Like <span class="underline">ADA,
                     GDPR, Privacy Policies, Etc.</span> Can Cost You
                     Handsomely
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md80">
               <div class="col-12 col-md-6">
                  <div class="compliance-block">
                     <div class="compliance-title">
                        <div class="f-22 f-md-28 w500 lh140 black-clr text-center">                    
                           Supreme Court hands victory to a <br class="d-none d-md-block"> blind man who sued Domino’s over<br class="d-none d-md-block"> site accessibility   
                        </div>
                     </div>
                     <div class="compliance-image">
                        <img src="assets/images/supreme.webp" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                  <div class="compliance-block">
                     <div class="compliance-title">
                        <div class="f-22 f-md-28 w500 lh140 black-clr text-center">                    
                           Google and YouTube Will Pay<br class="d-none d-md-block"> Record $170 Million for Alleged<br class="d-none d-md-block"> Violations of Privacy Law
                        </div>
                     </div>
                     <div class="compliance-image">
                        <img src="assets/images/record.webp" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>

      <div class="stats-sec">
         <div class="stats-wrap">
            <div class="container">
               <div class="row">
                  <div class="col-12 f-28 f-md-45 white-clr text-center w700">
                     Here Are Some More Eye Openers
                  </div>
               </div>
               <div class="row mt40 mt-md60 gx-md-5 align-items-center">
                  <div class="col-12 col-md-6">
                     <div class="f-md-24 f-20 w400 lh140 white-clr text-center text-md-start">
                        The website must be ADS Compliance so that it can
                        be accessible to people with disabilities.                            
                     </div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
                     <img src="https://cdn.oppyo.com/launches/legelsuites/special/stats1.webp" class="img-fluid d-block mx-auto">
                  </div>
               </div>
               <div class="row mt40 mt-md60 gx-md-5 align-items-center">
                  <div class="col-12 col-md-6 order-md-2">
                     <div class="f-md-24 f-20 w400 lh140 white-clr text-center text-md-start">
                        Website must be GDPR Compliant and should have the 
                        consent of users if you are collecting cookies.                      
                     </div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
                     <img src="https://cdn.oppyo.com/launches/legelsuites/special/stats2.webp" class="img-fluid d-block mx-auto">
                  </div>
               </div>
               <div class="row mt40 mt-md60 gx-md-5 align-items-center">
                  <div class="col-12 col-md-6">
                     <div class="f-md-24 f-20 w400 lh140 white-clr text-center text-md-start">
                        Privacy Policy is a must in case you are collecting 
                        users’ personal information on your website. 
                     </div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0">
                     <img src="https://cdn.oppyo.com/launches/legelsuites/special/stats3.webp" class="img-fluid d-block mx-auto">
                  </div>
               </div>
               <div class="row mt40 mt-md60 gx-md-5 align-items-center">
                  <div class="col-12 col-md-6 order-md-2">
                     <div class="f-md-24 f-20 w400 lh140 white-clr text-center text-md-start">
                        More than 11,400 Websites filed an ADA lawsuit in 2021— A 320% increase since 2013. It’s time to get your website under compliance now!  
                     </div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
                     <img src="https://cdn.oppyo.com/launches/legelsuites/special/stats4.webp" class="img-fluid d-block mx-auto">
                  </div>
               </div>
               <div class="row mt40 mt-md80">
                  <div class="col-12">
                     <div class="f-md-24 f-20 w400 lh140 white-clr text-center">                   
                        Failing to fulfill this Legal Compliance can not only get you to indulge in legal<br class="d-none d-md-block"> problems but it will also cause your website not to get ranked on<br class="d-none d-md-block"> Google and other Search Engines.
                     </div>
                  </div>
                  <div class="col-12 mt20 mt-md40">
                     <div class="f-md-38 f-24 w700 lh140 cyan-clr text-center">   
                        So, Having Your Website 100% Compliant <br class="d-none d-md-block">
                        with International Law is VITAL                
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <section class="giants-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-36 w800 black-clr text-center lh140">
                     Here are some more examples of <span class="underline">how top giants are been<br class="d-none d-md-block">
                     sued or fined due to missing law compliance</span> around <br class="d-none d-md-block">
                     the globe.
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md80 align-items-center">
               <div class="col-12 col-md-6">
                  <div class="compliance-block">
                     <div class="compliance-title">
                        <div class="f-22 f-md-28 w500 lh140 black-clr text-center">                    
                           The National Association of the Deaf brought a lawsuit against the streaming service Netflix   
                        </div>
                     </div>
                     <div class="compliance-image">
                        <img src="assets/images/g1.webp" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                  <div class="compliance-block">
                     <div class="compliance-title">
                        <div class="f-22 f-md-28 w500 lh140 black-clr text-center">                    
                           ADA lawsuit against the meal kit service Blue Apron
                        </div>
                     </div>
                     <div class="compliance-image">
                        <img src="assets/images/g2.webp" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md30">
                  <div class="compliance-block">
                     <div class="compliance-title">
                        <div class="f-22 f-md-28 w500 lh140 black-clr text-center">                    
                           Nike came under fire in 2017 because both websites it operates Nike.com and Converse.com – were inaccessible to visually impaired users
                        </div>
                     </div>
                     <div class="compliance-image">
                        <img src="assets/images/g3.webp" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                  <div class="compliance-block">
                     <div class="compliance-title">
                        <div class="f-22 f-md-28 w500 lh140 black-clr text-center">                    
                           A class-action lawsuit was filed against Beyonce Knowles’ Company 
                        </div>
                     </div>
                     <div class="compliance-image">
                        <img src="assets/images/g4.webp" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>

      <!-- Proudly Section Start -->
      <div class="proudly-sec" id="product">
         <div class="proudly-inner-sec relative">
            <div class="container">
               <div class="row">
                  <div class="col-12">
                     <div class="row">
                        <div class="col-12 text-center">
                           <div class="f-36 f-md-50 w700 lh140 orange-clr proudly-shape">
                              Introducing
                           </div>
                        </div>
                        <div class="col-12 mt30 mt-md50 text-center">
                           <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 495.7 104.2" style="enable-background:new 0 0 495.7 104.2; max-height:120px;" xml:space="preserve">
                              <style type="text/css">
                                 .st0{fill:#FFFFFF;}
                                 .st1{fill:#FFA829;}
                                 .st2{fill:url(#SVGID_1_);}
                                 .st3{fill:url(#SVGID_00000132794433514585357610000014266780508499585408_);}
                              </style>
                              <g>
                                 <path class="st0" d="M96.9,82.6c-7.5,2.8-15.7,4.4-24.2,4.8c0.2,0.7,0,1.4-0.5,1.9c-3.2,3.4-6.4,6.1-9.9,8.2
                                    c-5.4,3.4-11.5,5.6-17.9,6.5l-0.3,0.1c-0.2,0-0.3,0.1-0.5,0.1c-0.2,0-0.3,0-0.5-0.1l-0.3-0.1c-6.4-0.9-12.4-3.1-17.9-6.6
                                    C11.6,89,3.4,74.5,3.2,58.7c0-0.9,0.6-1.7,1.4-2c0,0,0.1,0,0.1,0C1.2,50.6-0.5,43.9,0.1,37C1.8,19.4,18.3,4.9,42.3,0
                                    c1-0.2,2,0.4,2.3,1.4c0.3,1-0.2,2.1-1.2,2.4c-5.4,2-10.2,4.6-14.2,7.7l14-3.6c0.3-0.1,0.7-0.1,1,0l38.4,9.9c0.9,0.2,1.5,1,1.5,1.9
                                    v38.1c0,7.9-2,15.6-5.7,22.4c5.9,0.1,11.7-0.4,17.4-1.5c1-0.2,2,0.4,2.3,1.4C98.4,81.2,97.9,82.2,96.9,82.6z"></path>
                                 <g>
                                    <path class="st0" d="M291.8,67.6l2.5-5.7c3.2,2.5,8.2,4.3,13.1,4.3c6.2,0,8.8-2.2,8.8-5.1c0-8.6-23.6-3-23.6-17.7
                                       c0-6.4,5.1-11.8,16-11.8c4.8,0,9.7,1.3,13.2,3.5l-2.3,5.7c-3.6-2.1-7.5-3.1-10.9-3.1c-6.1,0-8.6,2.4-8.6,5.4
                                       c0,8.4,23.5,3,23.5,17.5c0,6.3-5.1,11.8-16,11.8C301.2,72.4,295,70.4,291.8,67.6z"></path>
                                    <path class="st0" d="M464,67.6l2.5-5.7c3.2,2.5,8.2,4.3,13.1,4.3c6.2,0,8.8-2.2,8.8-5.1c0-8.6-23.6-3-23.6-17.7
                                       c0-6.4,5.1-11.8,16-11.8c4.8,0,9.7,1.3,13.2,3.5l-2.3,5.7c-3.6-2.1-7.5-3.1-10.9-3.1c-6.1,0-8.6,2.4-8.6,5.4
                                       c0,8.4,23.5,3,23.5,17.5c0,6.3-5.1,11.8-16,11.8C473.4,72.4,467.2,70.4,464,67.6z"></path>
                                    <path class="st0" d="M330.3,54.5V32.2h7.3v22.1c0,8.1,3.7,11.7,10.1,11.7s10-3.5,10-11.7V32.2h7.2v22.3c0,11.6-6.5,17.8-17.3,17.8
                                       C336.9,72.4,330.3,66.1,330.3,54.5z"></path>
                                    <path class="st0" d="M375.3,32.2h7.3v39.6h-7.3V32.2z"></path>
                                    <path class="st0" d="M401.4,38.4h-13.1v-6.2h33.6v6.2h-13.1v33.4h-7.3V38.4H401.4z"></path>
                                    <path class="st0" d="M457.2,65.6v6.2h-29.7V32.2h28.9v6.2h-21.6v10.3H454v6.1h-19.1v10.9L457.2,65.6L457.2,65.6z"></path>
                                 </g>
                                 <g>
                                    <path class="st1" d="M96.2,80.7c-10.5,3.9-23,5.6-36.1,4.4C25.9,81.9,0,60.5,2.1,37.2C3.7,20,20.3,6.5,42.7,2
                                       C26.1,8.2,14.6,19.8,13.2,34.1C11.1,57.4,37,78.8,71.1,81.9C79.9,82.7,88.4,82.2,96.2,80.7z"></path>
                                    <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="20.0931" y1="1069.2449" x2="47.1737" y2="1042.1643" gradientTransform="matrix(1 0 0 1 0 -978)">
                                       <stop offset="0" style="stop-color:#0055FF"></stop>
                                       <stop offset="1" style="stop-color:#008CFF"></stop>
                                    </linearGradient>
                                    <path class="st2" d="M68.3,87.8c-2,0-3.9-0.1-5.9-0.2c-1.3,1.1-2.7,2.1-4.2,3.1c-4.5,2.8-9.3,4.6-14.5,5.4
                                       c-5.1-0.8-10-2.6-14.5-5.4c-8.7-5.4-14.6-13.8-17-23.4c-2.8-2.7-5.1-5.6-7-8.6c0.2,15.2,8,29,20.9,37.1c5.3,3.3,11,5.4,17.2,6.3
                                       l0.4,0.1l0.4-0.1c6.1-0.9,11.9-3,17.2-6.3c3.6-2.2,6.7-4.9,9.5-7.9C70,87.8,69.1,87.8,68.3,87.8z"></path>
                                    <linearGradient id="SVGID_00000129182319601907477880000006569859738938224028_" gradientUnits="userSpaceOnUse" x1="33.5187" y1="1042.8949" x2="80.3658" y2="996.0477" gradientTransform="matrix(1 0 0 1 0 -978)">
                                       <stop offset="0" style="stop-color:#0055FF"></stop>
                                       <stop offset="1" style="stop-color:#008CFF"></stop>
                                    </linearGradient>
                                    <path style="fill:url(#SVGID_00000129182319601907477880000006569859738938224028_);" d="M43.7,9.9l-17.5,4.5
                                       c-2,1.9-21.6,31.5,10.2,51.6l2.2-13c-2.2-1.6-3.6-4.2-3.6-7c0-4.8,3.9-8.7,8.7-8.7s8.7,3.9,8.7,8.7c0,2.9-1.4,5.4-3.6,7l3.4,20.5
                                       C59,76,67,78.1,76.4,79.8c3.7-6.5,5.7-14,5.7-21.9V19.8L43.7,9.9z M40.3,17.5l-0.2,2.2l-1.6-1.4l-2.1,0.5l0.9-2l-1.1-1.9l2.2,0.2
                                       l1.4-1.6l0.4,2.1l2,0.8L40.3,17.5z M59.9,22.7l-0.2,2.5l-1.8-1.7l-2.4,0.6l1-2.2l-1.3-2.1l2.5,0.3l1.6-1.9l0.5,2.4l2.3,1
                                       L59.9,22.7z M74.2,71.3L74,74.6l-2.5-2.3l-3.3,0.8l1.4-3.1l-1.8-2.9l3.4,0.4l2.2-2.5l0.7,3.3l3.1,1.3L74.2,71.3z M74.8,51.1
                                       l-0.2,3.1l-2.3-2.1l-3,0.7l1.3-2.8L69,47.4l3.1,0.3l2-2.4l0.6,3l2.9,1.2L74.8,51.1z M75.4,31l-0.2,2.8l-2.1-1.9l-2.7,0.6l1.1-2.5
                                       l-1.4-2.4l2.7,0.3l1.8-2.1l0.6,2.7l2.6,1.1L75.4,31z"></path>
                                 </g>
                                 <g>
                                    <path class="st0" d="M105.1,32.2h11.2v30.7h18.9v8.9h-30.1V32.2z"></path>
                                    <path class="st0" d="M171.5,63.1v8.7h-31.8V32.2h31.1v8.7h-20v6.7h17.6V56h-17.6v7.2L171.5,63.1L171.5,63.1z"></path>
                                    <path class="st0" d="M204.2,51.2h9.9v16.4c-4.6,3.3-10.9,5-16.6,5c-12.6,0-21.8-8.6-21.8-20.6s9.2-20.6,22.1-20.6
                                       c7.4,0,13.4,2.5,17.3,7.2L208,45c-2.7-3-5.8-4.4-9.6-4.4c-6.8,0-11.3,4.5-11.3,11.3c0,6.7,4.5,11.3,11.2,11.3
                                       c2.1,0,4.1-0.4,6.1-1.3L204.2,51.2L204.2,51.2z"></path>
                                    <path class="st0" d="M253,63.1v8.7h-31.8V32.2h31.1v8.7h-20v6.7h17.6V56h-17.6v7.2L253,63.1L253,63.1z"></path>
                                    <path class="st0" d="M259.2,32.2h11.2v30.7h18.9v8.9h-30.1V32.2z"></path>
                                 </g>
                              </g>
                           </svg>
                        </div>
                     </div>
                     <div class="col-12 col-md-12 mt20 mt-md30">
                        <div class="f-24 f-md-36 w700 text-center lh140 cyan-clr text-capitalize">
                           All in One Platform to Start Serving Website Owners &amp; Developers for the Website’s Legal Compliance as per International Laws to save them from Law Suits.
                        </div>
                     </div>
                     <div class="col-12 col-md-12 mt20 mt-md30">
                        <div class="f-22 f-md-24 w600 lh140 white-clr text-center">
                           Generate Legal Compliance Like GDPR, ADA, Privacy Policy, and other Legal Documents<br class="d-none d-md-block"> with just a click of a Button in 60 Second Flat. 
                        </div>
                     </div>
                     <div class="row mt30 mt-md50 align-items-center">
                        <div class="col-12 col-md-10 mx-auto">
                           <img src="assets/images/product-image.webp" class="img-fluid d-block mx-auto img-animation">
                        </div>
                     </div>
                     <img src="assets/images/proudly-icon1.webp" alt="Proudly Icon Right" class="d-none d-md-block" style="position:absolute; top:40px; right:64px;">
                     <img src="assets/images/proudly-icon2.webp" alt="Proudly Icon Right" class="d-none d-md-block" style="position:absolute; bottom:47px; left:51px;">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Proudly Section End  -->

      <!-------WriterArc Starts----------->
      <!-------Exclusive Bonus----------->
      <div class="exclusive-bonus">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                  <div class="white-clr f-24 f-md-36 lh140 w600">
                     Exclusive Bonus #1 : WriterArc
                  </div>
                  <div class="f-18 f-md-20 w500 mt20 white-clr">
                     20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
                  </div>
               </div>
            </div>
         </div>
      </div>

      <div class="writer-header-section">
         <div class="container">
            <div class="row">
               <div class="col-12 d-flex align-items-center justify-content-center flex-wrap flex-md-nowrap">
                  <svg viewBox="0 0 247 41" fill="none" xmlns="http://www.w3.org/2000/svg" style="height:30px;">
                     <path d="M46.7097 6.24429L37.4161 39.478H29.5526L23.3077 15.8145L16.778 39.478L8.96167 39.5251L0 6.24429H7.15048L13.0122 32.0507L19.7796 6.24429H27.2149L33.6011 31.9074L39.5121 6.24429H46.7097Z" fill="white"></path>
                     <path d="M61.0803 13.9091C62.4632 13.1149 64.0429 12.7179 65.8234 12.7179V19.7175H64.0593C61.9613 19.7175 60.3816 20.2107 59.3162 21.1931C58.2508 22.1776 57.7202 23.8906 57.7202 26.3344V39.476H51.0471V13.1006H57.7202V17.196C58.5786 15.8002 59.6973 14.7052 61.0803 13.9111V13.9091Z" fill="white"></path>
                     <path d="M70.7549 8.83947C69.9764 8.09448 69.5871 7.1653 69.5871 6.05396C69.5871 4.94262 69.9764 4.01548 70.7549 3.26845C71.5335 2.52347 72.5108 2.14893 73.6868 2.14893C74.8629 2.14893 75.8381 2.52142 76.6187 3.26845C77.3973 4.01548 77.7866 4.94262 77.7866 6.05396C77.7866 7.1653 77.3973 8.09448 76.6187 8.83947C75.8402 9.5865 74.8629 9.959 73.6868 9.959C72.5108 9.959 71.5335 9.5865 70.7549 8.83947ZM76.9752 13.1006V39.476H70.3021V13.1006H76.9752Z" fill="white"></path>
                     <path d="M91.3213 18.5755V31.3364C91.3213 32.2247 91.5364 32.8673 91.9646 33.2644C92.3928 33.6614 93.1161 33.8599 94.1323 33.8599H97.2302V39.478H93.0362C87.4121 39.478 84.599 36.7478 84.599 31.2893V18.5775H81.454V13.1027H84.599V6.57996H91.3192V13.1027H97.2302V18.5775H91.3192L91.3213 18.5755Z" fill="white"></path>
                     <path d="M126.402 28.2889H107.097C107.255 30.1944 107.923 31.6864 109.099 32.765C110.275 33.8436 111.72 34.3839 113.437 34.3839C115.916 34.3839 117.678 33.3217 118.727 31.1931H125.924C125.162 33.7331 123.699 35.8186 121.54 37.4539C119.378 39.0892 116.725 39.9058 113.58 39.9058C111.037 39.9058 108.757 39.343 106.741 38.2152C104.723 37.0896 103.149 35.4932 102.022 33.4301C100.894 31.3671 100.33 28.9868 100.33 26.2893C100.33 23.5918 100.885 21.1645 101.998 19.0994C103.11 17.0364 104.667 15.4502 106.669 14.3389C108.671 13.2275 110.974 12.6729 113.58 12.6729C116.186 12.6729 118.337 13.2132 120.325 14.2918C122.31 15.3704 123.851 16.9033 124.949 18.8866C126.045 20.8698 126.594 23.1477 126.594 25.7183C126.594 26.67 126.531 27.5276 126.404 28.2889H126.402ZM119.681 23.8129C119.649 22.0998 119.03 20.7265 117.823 19.695C116.614 18.6635 115.137 18.1477 113.389 18.1477C111.736 18.1477 110.347 18.6471 109.218 19.6479C108.089 20.6487 107.399 22.0364 107.145 23.8149H119.679L119.681 23.8129Z" fill="white"></path>
                     <path d="M141.485 13.9091C142.868 13.1149 144.448 12.7179 146.228 12.7179V19.7175H144.464C142.366 19.7175 140.787 20.2107 139.721 21.1931C138.656 22.1776 138.125 23.8906 138.125 26.3344V39.476H131.452V13.1006H138.125V17.196C138.984 15.8002 140.102 14.7052 141.485 13.9111V13.9091Z" fill="white"></path>
                     <path d="M213.892 14.0034C215.275 13.2093 216.854 12.8122 218.635 12.8122V19.8118H216.871C214.773 19.8118 213.193 20.3051 212.128 21.2875C211.062 22.2719 210.532 23.985 210.532 26.4287V39.5703H203.858V13.1929H210.532V17.2883C211.39 15.8925 212.509 14.7975 213.892 14.0034Z" fill="white"></path>
                     <path d="M223.066 19.2162C224.179 17.1696 225.72 15.5813 227.691 14.4557C229.66 13.33 231.915 12.7651 234.458 12.7651C237.73 12.7651 240.441 13.5818 242.584 15.217C244.729 16.8523 246.165 19.1446 246.897 22.0979H239.699C239.318 20.9559 238.675 20.0594 237.769 19.4086C236.863 18.7578 235.743 18.4323 234.409 18.4323C232.503 18.4323 230.993 19.1221 229.881 20.5036C228.768 21.8851 228.213 23.8437 228.213 26.3836C228.213 28.9235 228.768 30.8351 229.881 32.2166C230.993 33.5981 232.501 34.2879 234.409 34.2879C237.109 34.2879 238.873 33.0824 239.699 30.6694H246.897C246.165 33.5265 244.721 35.7962 242.559 37.4786C240.398 39.161 237.697 40.0021 234.456 40.0021C231.913 40.0021 229.658 39.4393 227.689 38.3116C225.718 37.1859 224.177 35.5977 223.064 33.5511C221.952 31.5044 221.397 29.1159 221.397 26.3857C221.397 23.6554 221.952 21.267 223.064 19.2203L223.066 19.2162Z" fill="white"></path>
                     <path d="M187.664 20.0164C184.058 18.8846 180.219 18.2726 176.24 18.2726C171.331 18.2726 166.639 19.2018 162.33 20.8923L174.404 0V9.42898C173.372 9.7851 172.628 10.7675 172.628 11.9218C172.628 13.377 173.81 14.5579 175.269 14.5579C176.728 14.5579 177.91 13.377 177.91 11.9218C177.91 10.7695 177.172 9.79124 176.142 9.43307V0.0818666L187.662 20.0164H187.664Z" fill="#5055BE"></path>
                     <path d="M198.911 39.474C192.278 28.3013 180.084 20.8105 166.137 20.8105C164.83 20.8105 163.537 20.876 162.263 21.0049C162.164 21.0152 162.064 21.0254 161.966 21.0356C158.315 21.4327 154.817 22.3475 151.549 23.7024C153.113 23.7024 154.655 23.7966 156.169 23.9808C157.518 24.1425 158.843 24.3758 160.142 24.6766L151.584 39.4822C157.862 34.5068 165.748 31.4675 174.337 31.2689C174.636 31.2628 174.935 31.2587 175.234 31.2587C184.196 31.2587 192.434 34.3512 198.939 39.5272L198.911 39.4761V39.474Z" fill="white"></path>
                  </svg>
               </div>
               <div class="col-12 mt20 mt-md40 text-center">
                  <div class="f-20 f-md-22 w500 blue-clr1 lh140">
                     Never Waste Your Time &amp; Money on Manually Writing Boring Content Again…
                  </div>
               </div>
               <div class="col-12 mt-md30 mt20 f-md-50 f-28 w700 text-center white-clr lh140">
                 <span style="border-bottom:4px solid #fff">Futuristic A.I. Technology</span>  Creates Stunning Content for <span class="gradient-orange"> Any Local or Online Niche 10X Faster…Just by Using a Keyword</span>
               </div>
               <div class="col-12 mt-md30 mt20 text-center">
                  <div class="post-headline f-18 f-md-22 w700 text-center lh160 white-clr text-capitalize">
                     You Sit Back &amp; Relax, WriterArc Will Create Top Converting Content for You <br class="d-none d-md-block">  No Prior Skill Needed | No Monthly Fee Ever…
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md50">
               <div class="col-md-7 col-12 min-md-video-width-left">
                  <!-- <img src="assets/images/product-box.webp" class="img-fluid d-block mx-auto"> -->
                  <div class="col-12 responsive-video border-video">
                     <iframe src="https://writerarc.dotcompal.com/video/embed/spru84q6w8" style="width:100%; height:100%" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                  </div>
               </div>
               <div class="col-md-5 col-12 min-md-video-width-right mt20 mt-md0">
                  <ul class="list-head pl0 m0 f-18 f-md-20 lh150 w500 white-clr">
                     <li><span class="orange-clr1">Create Content</span> for Any Local or Online Business</li>
                     <li><span class="orange-clr1"> Let Our Super Powerful</span> A.I. Engine Do the Heavy Lifting</li> 
                     <li><span class="orange-clr1">Preloaded with 50+ Copywriting Templates, </span> like Ads, Video Scripts, Website Content, and much more.</li> 
                     <li><span class="orange-clr1">Works in 35+ Languages</span>  and 22+ Tons of Writing</li>
                     <li> <span class="orange-clr1">Download Your Content</span> in Word/PDF Format</li>
                     <li><span class="orange-clr1">Free Commercial License – </span>Sell Service to Local Clients for BIG Profits</li>
                  </ul>
               </div>
            </div>
         </div>
      </div>

      <div class="gr-1">
         <div class="container-fluid p0">
            <picture>
               <source media="(min-width:768px)" srcset="assets/images/writerarc-steps.webp">
               <source media="(min-width:320px)" srcset="assets/images/writerarc-mview-steps.webp" style="width:100%" class="img-fluid mx-auto">
               <img src="assets/images/writerarc-steps.webp" alt="WriterArc Steps" class="img-fluid" style="width: 100%;">
            </picture>
         </div>
      </div>

      <!-- WriterArc End -->

      <!-- Vocalic Section Start -->
      <div class="exclusive-bonus">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                  <div class="white-clr f-24 f-md-36 lh140 w600">
                     Exclusive Bonus #2 : Vocalic
                  </div>
                  <div class="f-18 f-md-20 w500 mt20 white-clr">
                     20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="header-section-vc">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row align-items-center">
                     <div class="col-md-12 text-center">
                        <svg xmlns="http://www.w3.org/2000/svg" id="Layer_2" viewBox="0 0 290.61 100" style="max-height:56px;">
                           <defs>
                              <style>
                                 .cls-1 {
                                 fill: #fff;
                                 }
                                 .cls-1,
                                 .cls-2 {
                                 fill-rule: evenodd;
                                 }
                                 .cls-2,
                                 .cls-3 {
                                 fill: #00aced;
                                 }
                              </style>
                           </defs>
                           <g id="Layer_1-2">
                              <g>
                                 <g>
                                    <path class="cls-3" d="M104.76,79.86l-5.11-6.34c-.16-.2-.45-.23-.65-.07-.37,.3-.75,.59-1.14,.87-3.69,2.66-7.98,4.34-12.49,4.9-1.11,.14-2.23,.21-3.35,.21s-2.24-.07-3.35-.21c-4.52-.56-8.8-2.24-12.49-4.9-.39-.28-.77-.57-1.14-.87-.2-.16-.49-.13-.65,.07l-5.11,6.34c-.16,.2-.13,.49,.07,.65,5.24,4.22,11.53,6.88,18.21,7.71,.37,.05,.74,.09,1.11,.12v11.67h6.7v-11.67c.37-.03,.74-.07,1.11-.12,6.68-.83,12.96-3.48,18.21-7.71,.2-.16,.23-.45,.07-.65Z">
                                    </path>
                                    <path class="cls-3" d="M82.02,74.47c10.57,0,19.13-7.45,19.13-16.65V31.08c0-4.6-2.14-8.76-5.6-11.77-3.46-3.01-8.24-4.88-13.53-4.88-10.57,0-19.13,7.45-19.13,16.65v26.74c0,9.2,8.57,16.65,19.13,16.65Zm-11.14-41.53c0-5.35,4.99-9.69,11.14-9.69,3.08,0,5.86,1.08,7.87,2.84,2.01,1.75,3.26,4.18,3.26,6.85v23.02c0,5.35-4.99,9.69-11.14,9.69s-11.14-4.34-11.14-9.69v-23.02Z">
                                    </path>
                                    <path class="cls-3" d="M82.02,38.89c1.71,0,3.1-1.39,3.1-3.1s-1.39-3.1-3.1-3.1-3.1,1.39-3.1,3.1,1.39,3.1,3.1,3.1Z">
                                    </path>
                                    <path class="cls-3" d="M82.02,47.54c1.71,0,3.1-1.39,3.1-3.1s-1.39-3.1-3.1-3.1-3.1,1.39-3.1,3.1,1.39,3.1,3.1,3.1Z">
                                    </path>
                                    <path class="cls-3" d="M82.02,56.2c1.71,0,3.1-1.39,3.1-3.1s-1.39-3.1-3.1-3.1-3.1,1.39-3.1,3.1,1.39,3.1,3.1,3.1Z">
                                    </path>
                                 </g>
                                 <path class="cls-1" d="M25.02,58.54L10.61,26.14c-.08-.19-.25-.29-.45-.29H.5c-.18,0-.32,.08-.42,.23-.1,.15-.11,.31-.04,.47l20.92,46.71c.08,.19,.25,.29,.45,.29h8.22c.2,0,.37-.1,.45-.29L60.4,15.13c.07-.16,.06-.32-.03-.47-.1-.15-.24-.23-.42-.23h-9.67c-.2,0-.37,.11-.45,.29L26.07,58.54c-.1,.21-.29,.34-.53,.34-.23,0-.43-.13-.53-.34h0Z">
                                 </path>
                                 <path class="cls-1" d="M117.96,52c-.13-.79-.2-1.58-.2-2.38s.07-1.6,.2-2.38c1.15-6.95,7.2-12.05,14.24-12.05,3.84,0,7.49,1.51,10.21,4.23l1.1,1.1c.19,.19,.49,.19,.68,0l6.03-6.03c.19-.19,.19-.49,0-.68l-1.1-1.1c-4.5-4.5-10.56-7.01-16.92-7.01-12.06,0-22.27,8.99-23.75,20.97-.12,.98-.18,1.97-.18,2.96s.06,1.98,.18,2.96c1.48,11.97,11.69,20.97,23.75,20.97,6.37,0,12.42-2.51,16.92-7.01l1.1-1.1c.19-.19,.19-.49,0-.68l-6.03-6.03c-.19-.19-.49-.19-.68,0l-1.1,1.1c-2.72,2.72-6.37,4.23-10.21,4.23-7.04,0-13.09-5.1-14.24-12.05h0Z">
                                 </path>
                                 <path class="cls-1" d="M258.21,52c-.13-.79-.2-1.58-.2-2.38s.07-1.6,.2-2.38c1.15-6.95,7.2-12.05,14.24-12.05,3.84,0,7.49,1.51,10.21,4.23l1.1,1.1c.19,.19,.49,.19,.68,0l6.03-6.03c.19-.19,.19-.49,0-.68l-1.1-1.1c-4.5-4.5-10.56-7.01-16.92-7.01-12.06,0-22.27,8.99-23.75,20.97-.12,.98-.18,1.97-.18,2.96s.06,1.98,.18,2.96c1.48,11.97,11.69,20.97,23.75,20.97,6.37,0,12.42-2.51,16.92-7.01l1.1-1.1c.19-.19,.19-.49,0-.68l-6.03-6.03c-.19-.19-.49-.19-.68,0l-1.1,1.1c-2.72,2.72-6.37,4.23-10.21,4.23-7.04,0-13.09-5.1-14.24-12.05h0Z">
                                 </path>
                                 <path class="cls-1" d="M174.91,25.87c-11.97,1.48-20.97,11.69-20.97,23.75s8.99,22.27,20.97,23.75c.98,.12,1.97,.18,2.96,.18s1.98-.06,2.96-.18c2.14-.26,4.24-.82,6.23-1.65,.19-.08,.3-.24,.3-.44v-9.77c0-.19-.09-.35-.27-.43-.17-.09-.35-.07-.5,.05-1.86,1.41-4.03,2.35-6.34,2.73-.79,.13-1.58,.2-2.38,.2s-1.6-.07-2.38-.2l-.2-.03c-6.86-1.23-11.85-7.24-11.85-14.21s5.1-13.09,12.05-14.24c.79-.13,1.58-.2,2.38-.2s1.59,.07,2.38,.2l.19,.03c6.87,1.23,11.87,7.24,11.87,14.21v22.7c0,.26,.22,.48,.48,.48h8.53c.26,0,.48-.22,.48-.48v-22.7c0-12.06-8.99-22.27-20.97-23.75-.98-.12-1.97-.18-2.96-.18s-1.98,.06-2.96,.18h0Z">
                                 </path>
                                 <path class="cls-1" d="M210.57,0c-.33,0-.59,.27-.59,.59V72.96c0,.33,.27,.59,.59,.59h10.5c.33,0,.59-.27,.59-.59V.59c0-.33-.27-.59-.59-.59h-10.5Z">
                                 </path>
                                 <path class="cls-1" d="M229.84,25.39c-.33,0-.59,.27-.59,.59v46.97c0,.33,.27,.59,.59,.59h10.5c.33,0,.59-.27,.59-.59V25.98c0-.33-.27-.59-.59-.59h-10.5Z">
                                 </path>
                                 <path class="cls-2" d="M229.84,8.96c-.33,0-.59,.07-.59,.15v11.94c0,.08,.27,.15,.59,.15h10.5c.33,0,.59-.07,.59-.15V9.11c0-.08-.27-.15-.59-.15h-10.5Z">
                                 </path>
                              </g>
                           </g>
                        </svg>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50 text-center">
                  <div class="preheadline f-18 f-md-22 w800 blue-clr lh140">
                     <i><span class="red-clr">Attention:</span> Futuristic A.I. Technology Do All The Work For You!</i>
                  </div>
               </div>
               <div class="col-12 mt-md30 mt20 f-md-50 f-28 w600 text-center white-clr lh140">
                  <span class="w800"> Create Profit-Pulling Videos with Voiceovers   </span><br>
                  <span class="w800 f-30 f-md-55 highlight-heading">In Just 60 Seconds <u>without</u>...</span> <br>
                  Being in Front of Camera, Speaking A Single Word or Hiring Anyone Ever
               </div>
               <div class="col-12 mt-md35 mt20  text-center">
                  <div class="post-heading f-18 f-md-24 w600 text-center lh150 white-clr">
                     Create &amp; Sell Videos &amp; Voiceovers In ANY LANGUAGE For Big Profits To Dentists, Gyms, Spas, Cafes, Retail Stores, Book Shops, Affiliate Marketers, Coaches &amp; 100+ Other Niches… 
                  </div>
               </div>
            </div>
            <div class="row d-flex flex-wrap mt20 mt-md40">
               <div class="col-md-7 col-12 min-md-video-width-left mt0 mt-md40">
                  <div class="col-12 responsive-video border-video">
                     <iframe src="https://vocalic.dotcompal.com/video/embed/g1mdnyzmub" style="width:100%; height:100%" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                  </div>
               
               </div>
               <div class="col-md-5 col-12 mt20 mt-md0 min-md-video-width-right">
                  <div class="calendar-wrap mb20 mb-md0">
                     <ul class="list-head pl0 m0 f-18 lh150 w400 white-clr">
                        <li>Tap Into Fast-Growing <span class="w700">$200 Billion Video &amp; Voiceover Industry</span></li>
                        <li><span class="w700">Create Stunning Videos In ANY Niche</span> With In-Built Video Creator</li>
                        <li><span class="w700">Create &amp; Publish YouTube Shorts</span> For Tons Of FREE Traffic</li>
                        <li><span class="w700">Make Passive Affiliate Commissions</span> Creating Product Review Videos</li>
                        <li>Real Human-Like Voiceover In <span class="w700">150+ Unique Voices And 30+ Languages</span></li>
                        <li>Convert Any Text Script Into A <span class="w700">Whiteboard &amp; Explainer Videos</span></li>
                        <li><span class="w700">Create Videos Using Just A Keyword</span> With Ready-To-Use Stock Images</li>
                        <li><span class="w700">Boost Engagement &amp; Sales </span> Using Videos</li>
                        <li><span class="w700">Add Background Music</span> To Your Videos.</li>
                        <li><span class="w700">Built-In Content Spinner</span> - Make Unique Scripts</li>
                        <li><span class="w700">Store Media Files </span> With Integrated MyDrive</li>
                        <li><span class="w700">100% Newbie Friendly </span> - No Tech Hassles Ever</li>
                        <li><span class="w700">Agency License Included</span> To Build On Incredible Income Helping Clients</li>
                        <li>FREE Training To<span class="w700"> Find Tons Of Clients</span></li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="gr-1">
         <div class="container-fluid p0">
            <picture>
               <source media="(min-width:768px)" srcset="assets/images/vocalic1.webp">
               <source media="(min-width:320px)" srcset="assets/images/vocalic1-mview.webp" style="width:100%" class="vidvee-mview">
               <img src="assets/images/vocalic1.webp" alt="Vidvee Steps" class="img-fluid" style="width: 100%;">
            </picture>
            <picture>
               <source media="(min-width:768px)" srcset="assets/images/vocalic2.webp">
               <source media="(min-width:320px)" srcset="assets/images/vocalic2-mview.webp">
               <img src="assets/images/vocalic2.webp" alt="Flowers" style="width:100%;">
            </picture>
         </div>
      </div>
      <!-- Vocalic Section ENds -->

      <!-------Exclusive Bonus----------->
      <div class="exclusive-bonus">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                  <div class="white-clr f-24 f-md-36 lh140 w600">
                     Exclusive Bonus #3 : Viddeyo
                  </div>
                  <div class="f-18 f-md-20 w500 mt20 white-clr">
                     20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="viddeyo-header-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row align-items-center">
                     <div class="col-md-12 text-center">
                        <svg version="1.1" id="Layer_1a" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 909.75 162.72" style="enable-background:new 0 0 909.75 162.72; max-height:56px;" xml:space="preserve">
                           <style type="text/css">
                              .st00{fill:#FFFFFF;}
                              .st11{fill:url(#SVGID_1a_);}
                           </style>
                           <g>
                              <g>
                                 <path class="st00" d="M216.23,71.07L201.6,97.26l-35.84-64.75l-3.16-5.7h29.13l0.82,1.49L216.23,71.07z M288.35,26.81l-3.16,5.7
                                    l-56.4,103.68h-6.26l-11.71-22.25l14.63-26.18l0.01,0.02l2.73-4.94l4.81-8.68l25.38-45.87l0.82-1.49H288.35z"></path>
                                 <path class="st00" d="M329.85,26.61v109.57h-23.23V26.61H329.85z"></path>
                                 <path class="st00" d="M454.65,81.7c0,2.7-0.2,5.41-0.59,8.04c-3.58,24.36-23.15,43.45-47.59,46.42l-0.26,0.03h-50.93v-69.9h23.23
                                    v46.67h26.13c12.93-2,23.4-11.91,26.09-24.74c0.45-2.14,0.67-4.33,0.67-6.51c0-2.11-0.21-4.22-0.62-6.26
                                    c-1.52-7.54-5.76-14.28-11.95-18.97c-3.8-2.88-8.14-4.84-12.75-5.78c-1.58-0.32-3.19-0.52-4.83-0.6h-45.98V26.82L403.96,27
                                    c11.89,0.9,23.21,5.67,32.17,13.61c9.78,8.65,16.15,20.5,17.96,33.36C454.46,76.49,454.65,79.1,454.65,81.7z"></path>
                                 <path class="st00" d="M572.18,81.7c0,2.7-0.2,5.41-0.59,8.04c-3.58,24.36-23.15,43.45-47.59,46.42l-0.26,0.03h-50.93v-69.9h23.23
                                    v46.67h26.13c12.93-2,23.4-11.91,26.09-24.74c0.45-2.14,0.67-4.33,0.67-6.51c0-2.11-0.21-4.22-0.62-6.26
                                    c-1.52-7.54-5.76-14.28-11.95-18.97c-3.8-2.88-8.14-4.84-12.75-5.78c-1.58-0.32-3.19-0.52-4.83-0.6h-45.98V26.82L521.5,27
                                    c11.89,0.9,23.2,5.67,32.17,13.61c9.78,8.65,16.15,20.5,17.96,33.36C572,76.49,572.18,79.1,572.18,81.7z"></path>
                                 <path class="st00" d="M688.08,26.82v23.23h-97.72V31.18l0.01-4.36H688.08z M613.59,112.96h74.49v23.23h-97.72v-18.87l0.01-4.36
                                    V66.1l23.23,0.14h64.74v23.23h-64.74V112.96z"></path>
                                 <path class="st00" d="M808.03,26.53l-5.07,6.93l-35.84,48.99l-1.08,1.48L766,83.98l-0.04,0.05l0,0.01l-0.83,1.14v51h-23.23V85.22
                                    l-0.86-1.18l-0.01-0.01l-0.04-0.05l-0.04-0.06l-1.08-1.48l-35.84-48.98l-5.07-6.93h28.77l1.31,1.78l24.46,33.43l24.46-33.43
                                    l1.31-1.78H808.03z"></path>
                                 <path class="st00" d="M909.75,81.69c0,30.05-24.45,54.49-54.49,54.49c-30.05,0-54.49-24.45-54.49-54.49S825.2,27.2,855.25,27.2
                                    C885.3,27.2,909.75,51.65,909.75,81.69z M855.25,49.95c-17.51,0-31.75,14.24-31.75,31.75s14.24,31.75,31.75,31.75
                                    c17.51,0,31.75-14.24,31.75-31.75C887,64.19,872.76,49.95,855.25,49.95z"></path>
                              </g>
                           </g>
                           <linearGradient id="SVGID_1a_" gradientUnits="userSpaceOnUse" x1="0" y1="81.3586" x2="145.8602" y2="81.3586">
                              <stop offset="0.4078" style="stop-color:#FFFFFF"></stop>
                              <stop offset="1" style="stop-color:#FC6DAB"></stop>
                           </linearGradient>
                           <path class="st11" d="M11.88,0c41.54,18.27,81.87,39.12,121.9,60.41c7.38,3.78,12.24,11.99,12.07,20.32
                              c0.03,7.94-4.47,15.72-11.32,19.73c-27.14,17.07-54.59,33.74-82.21,50c-0.57,0.39-15.29,8.93-15.47,9.04
                              c-1.4,0.77-2.91,1.46-4.44,1.96c-14.33,4.88-30.25-4.95-32.19-19.99c-0.18-1.2-0.23-2.5-0.24-3.72c0.35-25.84,1.01-53.01,2.04-78.83
                              C2.57,42.41,20.3,32.08,34.79,40.31c8.16,4.93,16.31,9.99,24.41,15.03c2.31,1.49,7.96,4.89,10.09,6.48
                              c5.06,3.94,7.76,10.79,6.81,17.03c-0.68,4.71-3.18,9.07-7.04,11.77c-0.38,0.24-1.25,0.88-1.63,1.07c-3.6,2.01-7.5,4.21-11.11,6.17
                              c-9.5,5.22-19.06,10.37-28.78,15.27c11.29-9.81,22.86-19.2,34.53-28.51c3.54-2.55,4.5-7.53,1.83-10.96c-0.72-0.8-1.5-1.49-2.49-1.9
                              c-0.18-0.08-0.42-0.22-0.6-0.29c-11.27-5.17-22.82-10.58-33.98-15.95c0,0-0.22-0.1-0.22-0.1l-0.09-0.02l-0.18-0.04
                              c-0.21-0.08-0.43-0.14-0.66-0.15c-0.11-0.01-0.2-0.07-0.31-0.04c-0.21,0.03-0.4-0.04-0.6,0.03c-0.1,0.02-0.2,0.02-0.29,0.03
                              c-0.14,0.06-0.28,0.1-0.43,0.12c-0.13,0.06-0.27,0.14-0.42,0.17c-0.09,0.03-0.17,0.11-0.26,0.16c-0.08,0.06-0.19,0.06-0.26,0.15
                              c-0.37,0.25-0.66,0.57-0.96,0.9c-0.1,0.2-0.25,0.37-0.32,0.59c-0.04,0.08-0.1,0.14-0.1,0.23c-0.1,0.31-0.17,0.63-0.15,0.95
                              c-0.06,0.15,0.04,0.35,0.02,0.52c0,0.02,0,0.08-0.01,0.1c0,0,0,0.13,0,0.13l0.02,0.51c0.94,24.14,1.59,49.77,1.94,73.93
                              c0,0,0.06,4.05,0.06,4.05l0,0.12l0.01,0.02l0.01,0.03c0,0.05,0.02,0.11,0.02,0.16c0.08,0.23,0.16,0.29,0.43,0.47
                              c0.31,0.16,0.47,0.18,0.71,0.09c0.06-0.04,0.11-0.06,0.18-0.1c0.02-0.01-0.01,0.01,0.05-0.03l0.22-0.13l0.87-0.52
                              c32.14-19.09,64.83-37.83,97.59-55.81c0,0,0.23-0.13,0.23-0.13c0.26-0.11,0.51-0.26,0.69-0.42c1.15-0.99,0.97-3.11-0.28-4.01
                              c0,0-0.43-0.27-0.43-0.27l-1.7-1.1C84.73,51.81,47.5,27.03,11.88,0L11.88,0z"></path>
                        </svg>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50 text-center">
                  <div class="viddeyo-preheadline f-18 f-md-20 w400 white-clr lh140">
                     <span class="w600 text-uppercase">Warning:</span> 1000s of Business Websites are Leaking Profit and Sales by Uploading Videos to YouTube
                  </div>
               </div>
               <div class="col-12 mt-md15 mt10 f-md-48 f-28 w400 text-center white-clr lh140">
                  Start Your Own <span class="w700 highlight-viddeyo">Video Hosting Agency with Unlimited Videos with Unlimited Bandwidth</span> at Lightning Fast Speed with No Monthly FEE Ever...
               </div>
               <div class="col-12 mt20 mt-md20 f-md-21 f-18 w400 white-clr text-center lh170">
                  Yes! Total control over your videos with zero delay, no ads and no traffic leak. Agency License Included
               </div>
            </div>
            <div class="row d-flex align-items-center flex-wrap mt20 mt-md40">
               <div class="col-md-7 col-12 min-md-video-width-left">
                  <!-- <img src="https://cdn.oppyo.com/launches/viddeyo/special/proudly.webp" class="img-fluid d-block mx-auto" alt="proudly"> -->
                  <div class="col-12 responsive-video border-video">
                     <iframe src="https://launch.oppyo.com/video/embed/xo6b4lwdid" style="width:100%; height:100%" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                  </div>
               </div>
               <div class="col-md-5 col-12 mt20 mt-md0 min-md-video-width-right">
                  <div class="calendar-wrap">
                     <div class="calendar-wrap-inline">
                        <ul class="list-head pl0 m0 f-18 f-md-18 lh150 w500 black-clr">
                           <li>100% Control on Your Video Traffic &amp; Channels</li>
                           <li>Close More Prospects for Your Agency Services</li>
                           <li>Boost Commissions, Customer Satisfaction &amp; Sales Online</li>
                           <li>Tap Into $398 Billion E-Learning Industry</li>
                           <li>Tap Into Huge 82% Of Overall Traffic on Internet </li>
                           <li>Boost Your Clients Sales-Agency License Included</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      </div>
      <div class="viddeyo-second-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row viddeyo-header-list-block">
                     <div class="col-12 col-md-6">
                        <ul class="viddeyo-features-list pl0 f-18 f-md-20 lh150 w400 black-clr text-capitalize">
                           <li><span class="w600">Upload Unlimited Videos</span> - Sales, E-Learning, Training, Product Demo, or ANY Video</li>
                           <li><span class="w600">Super-Fast Delivery (After All, Time Is Money!)</span> No Buffering. No Delay </li>
                           <li><span class="w600">Get Maximum Visitors Engagement</span>-No Traffic Leakage with Unwanted Ads or Video Suggestions</li>
                           <li>Create 100% Mobile &amp; SEO Optimized <span class="w600">Video Channels &amp; Playlists</span></li>
                           <li><span class="w600">Stunning Promo &amp; Social Ads Templates</span> for Monetization &amp; Social Traffic</li>
                           <li>Fully Customizable Drag and Drop <span class="w600">WYSIWYG Video Ads Editor</span></li>
                           <li><span class="w600">Intelligent Analytics</span> to Measure the Performance </li>
                        </ul>
                     </div>
                     <div class="col-12 col-md-6 mt10 mt-md0">
                        <ul class="viddeyo-features-list pl0 f-18 f-md-20 lh150 w400 black-clr text-capitalize">
                           <li class="w600">Tap Into Fast-Growing $398 Billion E-Learning, Video Marketing &amp; Video Agency Industry. </li>
                           <li>Manage All the Videos, Courses, &amp; Clients Hassle-Free, <span class="w600">All-In-One Central Dashboard.</span> </li>
                           <li>Robust Solution That Has <span class="w600">Served Over 69 million Video Views for Customers</span></li>
                           <li>HLS Player- Optimized to <span class="w600">Work Beautifully with All Browsers, Devices &amp; Page Builders</span> </li>
                           <li>Connect With Your Favourite Tools. <span class="w600">20+ Integrations</span> </li>
                           <li><span class="w600">Completely Cloud-Based</span> &amp; Step-By-Step Video Training Included </li>
                           <li>PLUS, YOU'LL RECEIVE - <span class="w600">A LIMITED AGENCY LICENSE IF YOU BUY TODAY</span> </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      </div>
      <!-- Header Viddeyo End -->
      <div class="gr-1">
         <div class="container-fluid p0">
            <picture>
               <source media="(min-width:768px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/viddeyo-steps.webp">
               <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/viddeyo-steps-mview.webp" style="width:100%" class="vidvee-mview">
               <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/vidvee-steps.webp" alt="Vidvee Steps" class="img-fluid" style="width: 100%;">
            </picture>
            <picture>
               <source media="(min-width:768px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/viddeyo-letsfaceit.webp">
               <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/viddeyo-letsfaceit-mview.webp">
               <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/viddeyo-letsfaceit.webp" alt="Flowers" style="width:100%;">
            </picture>
            <picture>
               <source media="(min-width:768px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/viddeyo-proudly.webp">
               <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/viddeyo-proudly-mview.webp">
               <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/viddeyo-proudly.webp" alt="Flowers" style="width:100%;">
            </picture>
         </div>
      </div>
      <!--Viddeyo ends-->

      <!-------Coursova Starts----------->
      <!-------Exclusive Bonus----------->
      <div class="exclusive-bonus">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                  <div class="white-clr f-24 f-md-36 lh140 w600">
                     Exclusive Bonus #4 : Coursova
                  </div>
                  <div class="f-18 f-md-20 w500 mt20 white-clr">
                     20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="coursova-bg">
         <div class="container">
            <div class="row inner-content">
               <div class="col-12">
                  <div class="col-12 text-center">
                     <img src="assets/images/logo-co-white.png" class="img-responsive mx-xs-center logo-height">
                  </div>
               </div>
               <div class="col-12 text-center px-sm15">
                  <div class="mt20 mt-md50 mb20 mb-md50">
                     <div class="f-md-21 f-20 w500 lh140 text-center white-clr">
                        Turn your passion, hobby, or skill into <u>a profitable e-learning business</u> right from your home…
                     </div>
                  </div>
               </div>
               <div class="col-12">
                  <div class="col-md-8 col-sm-3 ml-md70 mt20">
                     <div class="game-changer col-md-12">
                        <div class="f-md-24 f-20 w600 text-center  black-clr lh120">Game-Changer</div>
                     </div>
                  </div>
                  <div class="col-12 heading-co-bg">
                     <div class="f-24 f-md-42 w500 text-center black-clr lh140  line-center rotate-5">								
                        <span class="w700"> Create and Sell Courses on Your Own Branded <br class="d-md-block d-none">E-Learning Marketplace</span>  without Any <br class="visible-lg"> Tech Hassles or Prior Skills
                     </div>
                  </div>
                  <div class="col-12 p0 f-18 f-md-24 w400 text-center  white-clr lh140 mt-md40 mt20">
                     Sell your own courses or <u>choose from 10 done-for-you RED-HOT HD <br class="d-md-block d-none">video courses to start earning right away</u>
                  </div>
               </div>
            </div>
            <div class="row align-items-center">
               <div class="col-md-7 col-12 mt-sm100 mt20 px-sm15 min-sm-video-width-left">
                  <div class="col-12 p0 mt-md15">
                     <div class="col-12 responsive-video" editabletype="video" style="z-index: 10;">
                        <iframe src="https://coursova.dotcompal.com/video/embed/n56ppjcyr4" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                           box-shadow: none !important;" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                     </div>
                  </div>
               </div>
               <div class="col-md-5 col-12 f-20 f-md-20 worksans lh140 w400 white-clr mt-md37 mt20 pl15 min-sm-video-width-right">
                  <ul class="list-head list-head-co pl0 m0">
                     <li>Tap Into Fast-Growing $398 Billion Industry Today</li>
                     <li>Build Beautiful E-Learning Sites with Marketplace, Blog &amp; Members Area in Minutes</li>
                     <li>Easily Create Courses - Add Unlimited Lessons (Video, E-Book &amp; Reports)</li>
                     <li>Accept Payments with PayPal, JVZoo, ClickBank, W+ Etc.</li>
                     <li>No Leads or Profit Sharing With 3rd Party Marketplaces</li>
                     <li>Get Agency License and Charge Clients Monthly (Yoga Classes, Gym Etc.)</li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <div class="gr-1">
         <div class="container-fluid p0">
            <picture>
               <img src="assets/images/coursova-gr1.png" alt="Flowers" style="width:100%;">
            </picture>
            <picture>
               <img src="assets/images/coursova-gr2.png" alt="Flowers" style="width:100%;">
            </picture>
            <picture>
               <img src="assets/images/coursova-gr3.png" alt="Flowers" style="width:100%;">
            </picture>
         </div>
      </div>
      <!------Coursova Section ends------>
      
      
      <!-------MaxDrive Starts----------->
      <!-------Exclusive Bonus----------->
      <div class="exclusive-bonus">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                  <div class="white-clr f-24 f-md-36 lh140 w600">
                     Exclusive Bonus #5 : MaxDrive
                  </div>
                  <div class="f-18 f-md-20 w500 mt20 white-clr">
                     20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
                  </div>
               </div>
            </div>
         </div>
      </div>

      <div class="maxdrive-logo">
         <img src="assets/images/maxdrive-logo.png" alt="MaxDrive Logo" class="img-fluid mx-auto d-block">
      </div>

      <div class="maxdrive-header-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-18 f-md-24 w700 text-center white-clr mb-md34 mb10 lh160">
                     Say goodbye to multiple monthly charging, slow loading & complex <br class="d-none d-md-block"> cloud storage services today
                  </div>
               </div>
               <div class="col-12">
                  <div class="col-md-8 col-sm-3 ml-md70 mt20">
                     <div class="game-changer col-md-12">
                        <div class="f-md-24 f-20 w600 text-center  white-clr lh120">Game-Changer</div>
                     </div>
                  </div>
               </div>
               <div class="col-12 heading-co-bg">
                  <div class="f-24 f-md-42 w400 text-center black-clr lh140  line-center rotate-5">								
                     One Of A Kind Platform To <span class="w700">Store & Deliver All Your Videos, Audios, Images & Media Files At Lightning Fast Speed...</span>  For A Low One Time Fee!
                  </div>
               </div>
               <div class="col-12 p0 f-22 f-md-29 w400 text-center  white-clr lh140 mt-md40 mt20">
                  And supercharge all your courses, videos, client projects, websites & <span class="d-none d-md-block"></span> landing pages to generate tons of leads, sales & profits online…
               </div>
            </div>

            <div class="row align-items-center">
               <div class="col-md-6 col-12 mt-sm100 mt20 px-sm15">
                  <div class="col-12 p0 mt-md15">
                     <div  editabletype="image" style="z-index: 10;"> <img src="assets/images/maxdrive-product.png" class="img-fluid mx-auto d-block"> </div>
                  </div>
               </div>
               <div class="col-md-6 col-12 f-20 f-md-20 lh140 w400 white-clr mt-md37 mt20 pl15">
                  <div class="row">
                     <div class="col-12 col-md-2">
                        <img src="assets/images/maxdrive-n1.png" alt="MaxDrive n1" class="img-fluid">
                     </div>
                     <div class="col-12 col-md-10">
                        <div class="f-22 f-md-28 w600 white-clr">
                           An All-In-One Platform
                        </div>
                        <div class="f-18 w400 lh140 white-clr mt10">
                        Host, Manage & Publish All Your Videos, Pdfs, Docs, Audios, Zip Files Or Any Other Marketing & Training Files
                        </div>
                     </div>
                  </div>

                  <div class="row mt30">
                     <div class="col-12 col-md-2">
                        <img src="assets/images/maxdrive-n2.png" alt="MaxDrive n1" class="img-fluid">
                     </div>
                     <div class="col-12 col-md-10">
                        <div class="f-22 f-md-28 w600 white-clr">
                           Lightning Fast & Easy
                        </div>
                        <div class="f-18 w400 lh140 white-clr mt10">
                        Supercharge All Your Websites, Pages, E-Com Stores & Mobile Apps By Delivering All Your Files At Lightning Fast Speed With Fast Cdns
                        </div>
                     </div>
                  </div>

                  <div class="row mt30">
                     <div class="col-12 col-md-2">
                        <img src="assets/images/maxdrive-n3.png" alt="MaxDrive n1" class="img-fluid">
                     </div>
                     <div class="col-12 col-md-10">
                        <div class="f-22 f-md-28 w600 white-clr">
                        A Robust & Proven Solution
                        </div>
                        <div class="f-18 w400 lh140 white-clr mt10">
                        Powerful Battle-Tested Architecture That’s Happily Served 133 Million+ File Views & Downloads And Counting…
                        </div>
                     </div>
                  </div>

                  <div class="row mt30">
                     <div class="col-12 col-md-2">
                        <img src="assets/images/maxdrive-n4.png" alt="MaxDrive n1" class="img-fluid">
                     </div>
                     <div class="col-12 col-md-10">
                        <div class="f-22 f-md-28 w600 white-clr">
                        Worry Free With 100% Security
                        </div>
                        <div class="f-18 w400 lh140 white-clr mt10">
                        Online Back-Up & 30 Days File Recovery
                        </div>
                     </div>
                  </div>

                  <div class="row mt30">
                     <div class="col-12 col-md-2">
                        <img src="assets/images/maxdrive-n5.png" alt="MaxDrive n1" class="img-fluid">
                     </div>
                     <div class="col-12 col-md-10">
                        <div class="f-22 f-md-28 w600 white-clr">
                        Loaded With 50+ More Cool Features
                        </div>
                        <div class="f-18 w400 lh140 white-clr mt10">
                        No Other Cloud Service Comes Close. These Features Will Absolutely Amaze You!
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <div class="container-fluid p0">
         <picture>
            <source media="(min-width:768px)" srcset="assets/images/maxdrive-steps.webp">
            <source media="(min-width:320px)" srcset="assets/images/maxdrive-steps-mview.webp" style="width:100%" class="MaxDrive-mview">
            <img src="assets/images/maxdrive-steps.webp" alt="MaxDrive Steps" class="img-fluid" style="width: 100%;">
         </picture>
      </div>
      <!-- MaxDrive Section End-->

      <!-------Agencies Starts----------->
      <!-------Exclusive Bonus----------->
      <div class="exclusive-bonus">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                  <div class="white-clr f-24 f-md-36 lh140 w600">
                     Exclusive Bonus #6 : Agencies
                  </div>
                  <div class="f-18 f-md-20 w500 mt20 white-clr">
                     20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
                  </div>
               </div>
            </div>
         </div>
      </div>

      <div class="space-section agencies-header-section" editabletype="space">
			<div class="container">
				<div class="row inner-content">
					<div class="col-xs-12">
						<div class="col-12" editabletype="image" style="z-index: 10;">
                     <img src="assets/images/agencies-logo.png" alt="Agencies Logo" class="img-fluid mx-auto d-block">
                  </div>
					</div>
					<div class="col-xs-12 f-sm-28 f-22 lh140 w300 text-center jost-font white-clr mt20 mt-sm65" editabletype="text" style="z-index: 10;">
						<i>Kickstart Your Own <u>Profitable Website Agency</u> and Tap into Real 284 B Market <br class="visible-lg">Right from Your Home.</i>
					</div>
					<div class="col-xs-8 col-xs-offset-2 col-sm-offset-0 col-sm-4 col-sm4 mt20 mt-sm25 pl-sm16">
						<div class="game-changer col-xs-12" editabletype="shape" style="z-index: 12;">
							<div class="f-24 f-md-36 w500 text-center black-clr lh120 jost-font text-nowrap text-roated" editabletype="text" style="z-index: 13;">Game Changer</div>
						</div>
					</div>
					<div class="col-xs-12 pl-sm35 mt-smm30px">
						<div class="text-center">
							<div class="highlihgt-heading" editabletype="shape" style="z-index: 9;">
								<div class="f-28 f-md-45 w400 text-center white-clr lh140 jost-font" editabletype="text" style="z-index: 10;">
								<span class="green-clr">The World’s No.1 Pro Website Builder Lets You</span> <span class="w500">Create Beautiful Local WordPress Websites &amp; Ecom Stores for  Any Business in Just 7 Minutes!</span>
								</div>

							</div>
						</div>
					</div>
					<div class="col-xs-12  f-24 f-sm-30 w400 lh140 text-center white-clr jost-font mt20 mt-sm20" editabletype="text" style="z-index: 10;">
						Easily create &amp; sell websites for big profits to retail stores, book shops, coaches, dentists, attorney, gyms, spas, restaurants, cafes, &amp; 100+ other niches...  	
					</div>
					
					<div class="col-xs-12 f-22 f-sm-28 w400 lh140 text-center green-clr jost-font mt20 mt-sm20" editabletype="text" style="z-index: 10;">
						No Tech Skills Needed. No Monthly Fees.
					</div>

					<div class="col-xs-12">
                  <div class="row align-items-center">
                     <div class="col-xs-12 col-md-7 mt15 mt-sm100 ">
                        <!-- <div class="" editabletype="image" style="z-index: 10;"><img src="assets/images/demo-video-image.png" class="img-responsive center-block" /></div> -->
                        <div class="responsive-video" editabletype="video" style="z-index: 10;">
                           <iframe src="https://agenciez.dotcompal.com/video/embed/1xqnfd9akv" style="width:100%; height:100%" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                        </div>
                     </div>
                     <div class="col-xs-12 col-md-5 mt20 mt-md40">
                        <div class="f-18 f-md-20 lh150 w300" editabletype="text" style="z-index: 10;">
                           <ul class="header-bordered-list pl0 white-clr">
                           <li>Tap Into HUGE $284 B Web Agency Industry  </li>
                           <li>Create Elegant Local Business Sites, Store, or Blogs with Ease.  </li>
                           <li>No. 1 WordPress Framework–Customize Themes in Just Few Click  </li>
                           <li>30+ Stunning Themes with 200+ Templates &amp; 2000+ Possible Designs  </li>
                           <li>100% SEO, Social-Media &amp; Mobile Friendly Sites  </li>
                           <li>FREE Commercial License - Build an Incredible Income Helping Clients!  </li>
                           </ul>
                        </div>
                     </div>
                  </div>
					</div>
				</div>
			</div>
		</div>

      <div class="container-fluid p0">
         <picture>
            <source media="(min-width:768px)" srcset="assets/images/agencies-steps.webp">
            <source media="(min-width:320px)" srcset="assets/images/agencies-steps-mview.webp" style="width:100%" class="MaxDrive-mview">
            <img src="assets/images/agencies-steps.webp" alt="MaxDrive Steps" class="img-fluid" style="width: 100%;">
         </picture>
      </div>
      <!-- Agencies Section End -->
      

      <!-- Bonus Section Header Start -->
      <div class="bonus-header">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-10 mx-auto heading-bg text-center">
                  <div class="f-24 f-md-36 lh140 w700"> When You Purchase LegelSuites, You Also Get <br class="hidden-xs"> Instant Access To These 20 Exclusive Bonuses</div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus Section Header End -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 1</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus1.webp" class="img-fluid mx-auto d-block" alt="Bonus1">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                           Social Traffic Plan
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>Online digital marketing has numerous advantages for your business.</b></li>
                           <li>Not only is it a cost-effective way to increase awareness about your brand, but the information you post on the Internet travels fast and has no geographical boundaries.</li>
                           <li>
                           Learning about the effectively utilize the most popular social media platforms is the first step to driving more traffic to your site and finding success.
                           </li>
                           <li>
                           This simple guide will show you what you need to do to boost your targeted website traffic using social media. 
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #1 Section End -->
      <!-- Bonus #2 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 2</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30">
                  <div class="row">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="assets/images/bonus2.webp" class="img-fluid mx-auto d-block" alt="Bonus2">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md0 text-capitalize">
                        Traffic Machine
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>Are you a small business that has been struggling to increase the traffic to your website?</b></li>
                           <li><b>Did you spend your valuable time building a website only to have it go unseen?</b></li>
                           <li><b>If your website isn’t bringing in the visitors like it should, then you may need to think your content marketing strategy.</b></li>
                           <li>Everyone has struggled with increasing traffic to their site and one point in time or another. Those who have become success, have discovered the secrets to turning their site into a traffic machine.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #2 Section End -->
      <!-- Bonus #3 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 3</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus3.webp" class="img-fluid mx-auto d-block " alt="Bonus3">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Content Syndication
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>Content Syndication Videos is a series of 40 On-Screen, Easy-To-Follow Video Tutorials on how to market and publish your content expertly.</b></li>
                          <li>The Internet is full of 'me too marketers' and what makes this frustrating is that even if you're a genuine expert, it's not a guarantee you'll be successful with marketing yourself.</li>
                          <li>
                          This course will help you to get an unfair advantage and stay ahead of the competition. Lessons are short, simple and direct.
                          </li>
                          <li>
                          You will learn how to get more visitors, attract more leads and close more sales!
                          </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #3 Section End -->
      <!-- Bonus #4 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 4</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="assets/images/bonus4.webp" class="img-fluid mx-auto d-block " alt="Bonus4">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Hashtag Traffic Secrets
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li> <b> Get Laser-Targeted Social Media Traffic Using The Power Of Hashtags! </b> </li>
                           <li>
                           There are plenty of different ways to drive traffic to your website today, but some are definitely more effective – and attractive – than others. 
                           </li>
                           <li>
                           Social media marketing, more specifically “hashtag marketing”, is definitely one of the hottest drivers of organic traffic today and you have to make sure that you are making the most of the hashtags you create online right now to build your business from the ground up.
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #4 End -->
      <!-- Bonus #5 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 5</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus5.webp" class="img-fluid mx-auto d-block " alt="Bonus5">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Web Amcom Pro
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>This easy to use software inserts ads into your web pages, showing a selection of the latest eBay auctions relating to your niche. </b> Ads are updated automatically every day, so your website always shows the very latest ads for your niche.</li>
                           <li>
                           All the ads include your Amazon affiliate ID, so you earn commissions on any resulting sales - giving your website an instant profit boost. The software can be used to add Amazon ads to any website in minutes.
                           </li>
                           <li>
                           The software is licensed for use on an unlimited number of websites, so you can use it on every site you own, without paying any extra.  
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #5 End -->
      <!-- CTA Button Section Start -->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 cyan-clr">"PRANSHUVIP"</span> for an Additional <span class="w700 cyan-clr">$4 Discount</span> on Agency Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab LegelSuites + My 20 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                  Use Coupon Code <span class="w700 cyan-clr">"LEGELBUNDLE"</span> for an Additional <span class="w700 cyan-clr">$50 Discount</span> on Agency Licence
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="https://jvz4.com/c/47069/388898/" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab LegelSuites Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                        <span class="f-14 f-md-18 w500 ">Days</span>
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                        <span class="f-14 f-md-18 w500 ">Hours</span>
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                        <span class="f-14 f-md-18 w500 ">Mins</span>
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                        <span class="f-14 f-md-18 w500 ">Sec</span>
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->
      <!-- Bonus #6 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 6</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="assets/images/bonus6.webp" class="img-fluid mx-auto d-block " alt="Bonus6">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Web Baycom Pro
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li> <b>This easy to use software inserts ads into your web pages, showing a selection of the latest eBay auctions relating to your niche.</b> These ads update automatically every hour, so your website always shows the very latest auctions for your niche.</li> 

                           <li>The software uses the eBay auction 'RSS feeds' to get the latest auctions. These provide a self-updating selection of the latest auctions matching any keyword.</li>

                           <li>The software can be used to add eBay auctions to any website in minutes.</li> 

                           <li>You don't need to add any complicated code to your web pages. You just need to insert some special text on each page where you want to feed to appear.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #6 End -->
      <!-- Bonus #7 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 7</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus7.webp" class="img-fluid mx-auto d-block " alt="Bonus7">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Turbo GIF Animator
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>If you are digital marketer, chances are you may already had use graphics in promoting your products or services online</li>

                           <li>The thing is that, one of the best type of images that engage more viewers in social media is the images that are moving or simply an animated images in GIF format</li>

                           <li>Now, it's Time To Get CURRENT & Begin Informing Your Audience About Your New Product(s) In a More Interesting & Appealing Way, In Just A Few Seconds!</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #7 End -->
      <!-- Bonus #8 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 8</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus8.webp" class="img-fluid mx-auto d-block " alt="Bonus8">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Viral Content Crusher
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>
                              <b>Just one highly effective viral marketing campaign can be more significant than years of regular advertising. </b> In this guide you are going to learn a number of things concerning making viral content. The aim is to give you a comprehensive knowledge on the subject matter that will help you come up with content that will go viral.
                           </li>
                           <li>What is the definition of viral content and why do people share it?</li>
                           <li>Understanding the customer and then charming the viewers with your content</li>
                           <li>Reviews of case studies of the past</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #8 End -->
      <!-- Bonus #9 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 9</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus9.webp" class="img-fluid mx-auto d-block " alt="Bonus9">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Lead Generation On Demand
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>It doesn’t matter what kind of business you’re in, if you aren’t able to generate new leads and turn them into paying customers, your company will never succeed. </li> 
                           <li>You need to be constantly bringing in new customers if you want your business to thrive.</li> 
                           <li>Generating more leads is anything but easy and if you don’t have a solid marketing strategy that will drive more traffic to your website, you’ll never be able to generate the leads you need for your business to succeed. </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #9 End -->
      <!-- Bonus #10 start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 10</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus10.webp" class="img-fluid mx-auto d-block " alt="Bonus10">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        WP SEO Track Plugin
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>With this simple plugin you can get the true insight on your web traffic efforts in only seconds!</b> Watch as your social network shares increase, your google PageRank and more.</li>
                           <li>Get a clear vision on what you need to start focusing on with your SEO efforts. Start more effective backlinks from Facebook, Twitter, Google and more.</li>
                           <li>You will get all of the most important stats that you need to know for your SEO web traffic.</li>
                           <li>Focus on the amount of shares you have on popular social sites like Facebook, Twitter, Stumble Upon and more.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #10 End -->
      <!-- CTA Button Section Start -->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 cyan-clr">"PRANSHUVIP"</span> for an Additional <span class="w700 cyan-clr">$4 Discount</span> on Agency Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab LegelSuites + My 20 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                  Use Coupon Code <span class="w700 cyan-clr">"LEGELBUNDLE"</span> for an Additional <span class="w700 cyan-clr">$50 Discount</span> on Agency Licence
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="https://jvz4.com/c/47069/388898/" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab LegelSuites Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 smmltd">Days</span> </div>
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">0300</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                     <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                     <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->
      <!-- Bonus #11 start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 11</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus11.webp" class="img-fluid mx-auto d-block " alt="Bonus13">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Authority Site Blueprint
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>
                              <b> This 4-part audio course will teach you how to create an authority site and rank it highly on search engine to get traffic.</b> An authority site is a very high quality website that is respected by knowledgeable people in its industry. 
                           </li>   
                           <li>
                           This guide will walk you through the easy to use technology and useful content strategies. 
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #11 End -->
      <!-- Bonus #12 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 12</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus12.webp" class="img-fluid mx-auto d-block " alt="Bonus12">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        The Internet Marketer's Toolkit
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>Internet marketing means getting word out on the web and it means creating brilliant content.</b></li>
                           <li>The only problem is that very few people have any idea what makes the web tick. Even if you know the basics of internet marketing, there’s a good chance that you don’t have all of the advanced skills you need to really make any project into a success.</li>
                           <li>Learn the basics of internet marketing 101 even if you have no prior experience.</li>
                           <li>Learn how to turn any idea or concept into a reality.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #12 End -->
      <!-- Bonus #13 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 13</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus13.webp" class="img-fluid mx-auto d-block " alt="Bonus13">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Buying Traffic to Generate Massive Website Visitors
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>An intrinsic understanding of your competition and how to better them is the most important component of any digital marketing strategy. Today, there is such small distances between you and your competition - it's unlikely that you're offering anything that cannot be bought on some other website.</li> 
                           <li>   
                           Now, when a consumer wants and needs a product or information, all they must do is input their desires into the Google search and peruse the search engine results page.
                           </li>
                           <li>
                           Standing out in the search results is by far the most important part of driving traffic to your site. In this ebook, you will learn about the main and most popular techniques to drive traffic to your website.
                           </li>
                           <li>
                           Also, you will learn all the terms you need to know for a proper understanding of the web marketing industry.
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #13 End -->
      <!-- Bonus #14 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 14</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus14.webp" class="img-fluid mx-auto d-block " alt="Bonus14">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Foolproof Traffic System Gold
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                          <li> <b>Many internet marketers overlook how important traffic is when it comes to making product sales.</b> Traffic is the lifeblood of your business. There are many ways to get traffic to your website, some of them are free methods, and others are paid. It makes sense to use both ways of getting traffic to build and grow your business.</li>
                          <li>
                           You will learn the difference between quantity and quality of traffic.</li>
                          <li>You will learn about the importance of quality content and the power of search engine optimization (SEO), viral marketing, and social media marketing.</li> 
                          <li>
                           You will also learn about how Google Analytics can help you learn more about the traffic that is coming to your site.
                          </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #14 End -->
      <!-- Bonus #15 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 15</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus15.webp" class="img-fluid mx-auto d-block " alt="Bonus15">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Foolproof Traffic System
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                          <li><b> The bottom line is that you need to get traffic to your offers, or you will have no sales.</b> That traffic needs to be quality traffic, the target market/audience that will be interested in your offers.</li> 

                          <li>This step by step guide will help you to get more traffic on your site! It’s easier than you think with following this guide!</li> 

                          <li>Includes sales page, graphics, social media posts and much more!</li> 
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #15 End -->
      
      <!-- CTA Button Section Start -->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 cyan-clr">"PRANSHUVIP"</span> for an Additional <span class="w700 cyan-clr">$4 Discount</span> on Agency Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab LegelSuites + My 20 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                  Use Coupon Code <span class="w700 cyan-clr">"LEGELBUNDLE"</span> for an Additional <span class="w700 cyan-clr">$50 Discount</span> on Agency Licence
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="https://jvz4.com/c/47069/388898/" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab LegelSuites Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 smmltd">Days</span> </div>
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">0300</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                     <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                     <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->

      <!-- Bonus #16 start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 16</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus16.webp" class="img-fluid mx-auto d-block " alt="Bonus16">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        X-Treme List Build Plugin
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>
                              <b>Start creating engagement with your new leads by making your landing page memorable and enjoyable!</b>
                           </li>   
                           <li>
                           Just upload, click activate and you are ready to create unlimited awesome pages!
                           </li>
                           <li>
                           Edit every detail on the fly with the simple options panel for each page
                           </li>
                           <li>
                           Customize all of the content areas that are designed to be readable
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #16 End -->
      <!-- Bonus #17 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 17</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus17.webp" class="img-fluid mx-auto d-block " alt="Bonus17">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        WP Youtube Leads Plugin
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>With this plugin you can enhance the user engagement of your Youtube videos and increase your mailing list.</li>
                           <li>Integrate any YouTube video and start converting right away. Use the time-stamps with the video to maximize interest and action.</li>
                           <li>Create custom headlines and fill out the content the way you want</li>
                           <li>Customize the color and design to perfect the pesentation</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #17 End -->
      <!-- Bonus #18 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 18</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus18.webp" class="img-fluid mx-auto d-block " alt="Bonus18">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        WP Survey Creator
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li> <b>WP Survey Creator is a WordPress plugin that allows you to incorporate a survey feature into your WordPress-powered website.</b> With this plugin you can create surveys with different types of questions and control how it appears on the page. What’s more, you’ll have the ability to gather important results and statistics of the answers supplied by the respondents.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #18 End -->
      <!-- Bonus #19 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 19</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus19.webp" class="img-fluid mx-auto d-block " alt="Bonus19">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Power Tools Video Site Builder
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                          <li> <b>With this software you can instantly create your own complete moneymaking video site featuring Adsense and Amazon ads, unique web pages and SEO Solutions!</b> To create your complete video site, you need to activate the Site Builder Tool, enter a few details into the simple form and click a button. A complete website is instantly built, ready to upload.</li>
                          <li>120 videos sourced from YouTube. When you use the software, it fetches the latest selection of most popular videos for this particular niche. So when you build your site, you can be sure it will be fully up to date with the very latest videos.</li>
                          <li>Content provided by extracting random snippets from a set of 20 private label articles, ensuring that your pages are unique and contain niche-targeted content. (Most other video site builders scrape the content from YouTube, so the pages are just duplicates of the YouTube pages and rated as worthless by search engines).</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #19 End -->
      <!-- Bonus #20 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 20</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus20.webp" class="img-fluid mx-auto d-block " alt="Bonus20">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Social Media Boom Software
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                          <li><b> Turn tour Wordpress blog into a social powerhouse! The simple and fast way to increase social conversions.</b></li> 

                          <li>Take the social features of some of the highest shared websites like Buzzfeed or UpWorthy and add them to your blog posts.</li> 

                          <li>No matter what theme you are using you can add these shortcodes to get all the social share features you need to have viral blog posts.</li> 
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #20 End -->

 
      <!-- Huge Woth Section Start -->
      <div class="huge-area mt30 mt-md10">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="f-md-65 f-40 lh120 w700 white-clr">That's Huge Worth of</div>
                  <br>
                  <div class="f-md-60 f-40 lh120 w800 yellow-clr">$3300!</div>
               </div>
            </div>
         </div>
      </div>
      <!-- Huge Worth Section End -->
      <!-- text Area Start -->
      <div class="white-section pb0">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="f-md-36 f-25 lh140 w500">So what are you waiting for? You have a great opportunity <br class="hidden-xs"> ahead + <span class="w700">My 20 Bonus Products</span> are making it a <br class="hidden-xs"> <span class="w700">completely NO Brainer!!</span></div>
               </div>
            </div>
         </div>
      </div>
      <!-- text Area End -->
      <!-- CTA Button Section Start -->
      <div class="cta-btn-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-16 f-md-20 lh150 w500 text-center black-clr">
                  Use Coupon Code <span class="w700 blue-clr">"PRANSHUVIP"</span> for an Additional <span class="w700 blue-clr">$4 Discount</span> on Agency Licence
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn px-md80">
                  Grab LegelSuites + My 20 Exclusive Bonuses
                  </a>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 black-clr mt-md30">
                  Use Coupon Code <span class="w700 blue-clr">"LEGELBUNDLE"</span> for an Additional <span class="w700 blue-clr">$50 Discount</span> on Agency Licence
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="https://jvz4.com/c/47069/388898/" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab LegelSuites Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-12 mt15 mt-md20" align="center">
                  <h3 class="f-md-28 f-20 w500 text-center black">My Exclusive Bonuses Are Expiring in...</h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 col-md-10 mx-auto col-12 text-center">
                  <div class="countdown counter-black">
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                     <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                     <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->
      <!-- Footer Section Start -->
      <div class="footer-section">
         <div class="container ">
            <div class="row">
               <div class="col-12 text-center">
               <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 495.7 104.2" style="enable-background:new 0 0 495.7 104.2; max-height:60px;" xml:space="preserve">
                              <style type="text/css">
                                 .st0{fill:#FFFFFF;}
                                 .st1{fill:#FFA829;}
                                 .st2{fill:url(#SVGID_1_);}
                                 .st3{fill:url(#SVGID_00000132794433514585357610000014266780508499585408_);}
                              </style>
                              <g>
                                 <path class="st0" d="M96.9,82.6c-7.5,2.8-15.7,4.4-24.2,4.8c0.2,0.7,0,1.4-0.5,1.9c-3.2,3.4-6.4,6.1-9.9,8.2
                                    c-5.4,3.4-11.5,5.6-17.9,6.5l-0.3,0.1c-0.2,0-0.3,0.1-0.5,0.1c-0.2,0-0.3,0-0.5-0.1l-0.3-0.1c-6.4-0.9-12.4-3.1-17.9-6.6
                                    C11.6,89,3.4,74.5,3.2,58.7c0-0.9,0.6-1.7,1.4-2c0,0,0.1,0,0.1,0C1.2,50.6-0.5,43.9,0.1,37C1.8,19.4,18.3,4.9,42.3,0
                                    c1-0.2,2,0.4,2.3,1.4c0.3,1-0.2,2.1-1.2,2.4c-5.4,2-10.2,4.6-14.2,7.7l14-3.6c0.3-0.1,0.7-0.1,1,0l38.4,9.9c0.9,0.2,1.5,1,1.5,1.9
                                    v38.1c0,7.9-2,15.6-5.7,22.4c5.9,0.1,11.7-0.4,17.4-1.5c1-0.2,2,0.4,2.3,1.4C98.4,81.2,97.9,82.2,96.9,82.6z"></path>
                                 <g>
                                    <path class="st0" d="M291.8,67.6l2.5-5.7c3.2,2.5,8.2,4.3,13.1,4.3c6.2,0,8.8-2.2,8.8-5.1c0-8.6-23.6-3-23.6-17.7
                                       c0-6.4,5.1-11.8,16-11.8c4.8,0,9.7,1.3,13.2,3.5l-2.3,5.7c-3.6-2.1-7.5-3.1-10.9-3.1c-6.1,0-8.6,2.4-8.6,5.4
                                       c0,8.4,23.5,3,23.5,17.5c0,6.3-5.1,11.8-16,11.8C301.2,72.4,295,70.4,291.8,67.6z"></path>
                                    <path class="st0" d="M464,67.6l2.5-5.7c3.2,2.5,8.2,4.3,13.1,4.3c6.2,0,8.8-2.2,8.8-5.1c0-8.6-23.6-3-23.6-17.7
                                       c0-6.4,5.1-11.8,16-11.8c4.8,0,9.7,1.3,13.2,3.5l-2.3,5.7c-3.6-2.1-7.5-3.1-10.9-3.1c-6.1,0-8.6,2.4-8.6,5.4
                                       c0,8.4,23.5,3,23.5,17.5c0,6.3-5.1,11.8-16,11.8C473.4,72.4,467.2,70.4,464,67.6z"></path>
                                    <path class="st0" d="M330.3,54.5V32.2h7.3v22.1c0,8.1,3.7,11.7,10.1,11.7s10-3.5,10-11.7V32.2h7.2v22.3c0,11.6-6.5,17.8-17.3,17.8
                                       C336.9,72.4,330.3,66.1,330.3,54.5z"></path>
                                    <path class="st0" d="M375.3,32.2h7.3v39.6h-7.3V32.2z"></path>
                                    <path class="st0" d="M401.4,38.4h-13.1v-6.2h33.6v6.2h-13.1v33.4h-7.3V38.4H401.4z"></path>
                                    <path class="st0" d="M457.2,65.6v6.2h-29.7V32.2h28.9v6.2h-21.6v10.3H454v6.1h-19.1v10.9L457.2,65.6L457.2,65.6z"></path>
                                 </g>
                                 <g>
                                    <path class="st1" d="M96.2,80.7c-10.5,3.9-23,5.6-36.1,4.4C25.9,81.9,0,60.5,2.1,37.2C3.7,20,20.3,6.5,42.7,2
                                       C26.1,8.2,14.6,19.8,13.2,34.1C11.1,57.4,37,78.8,71.1,81.9C79.9,82.7,88.4,82.2,96.2,80.7z"></path>
                                    <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="20.0931" y1="1069.2449" x2="47.1737" y2="1042.1643" gradientTransform="matrix(1 0 0 1 0 -978)">
                                       <stop offset="0" style="stop-color:#0055FF"></stop>
                                       <stop offset="1" style="stop-color:#008CFF"></stop>
                                    </linearGradient>
                                    <path class="st2" d="M68.3,87.8c-2,0-3.9-0.1-5.9-0.2c-1.3,1.1-2.7,2.1-4.2,3.1c-4.5,2.8-9.3,4.6-14.5,5.4
                                       c-5.1-0.8-10-2.6-14.5-5.4c-8.7-5.4-14.6-13.8-17-23.4c-2.8-2.7-5.1-5.6-7-8.6c0.2,15.2,8,29,20.9,37.1c5.3,3.3,11,5.4,17.2,6.3
                                       l0.4,0.1l0.4-0.1c6.1-0.9,11.9-3,17.2-6.3c3.6-2.2,6.7-4.9,9.5-7.9C70,87.8,69.1,87.8,68.3,87.8z"></path>
                                    <linearGradient id="SVGID_00000129182319601907477880000006569859738938224028_" gradientUnits="userSpaceOnUse" x1="33.5187" y1="1042.8949" x2="80.3658" y2="996.0477" gradientTransform="matrix(1 0 0 1 0 -978)">
                                       <stop offset="0" style="stop-color:#0055FF"></stop>
                                       <stop offset="1" style="stop-color:#008CFF"></stop>
                                    </linearGradient>
                                    <path style="fill:url(#SVGID_00000129182319601907477880000006569859738938224028_);" d="M43.7,9.9l-17.5,4.5
                                       c-2,1.9-21.6,31.5,10.2,51.6l2.2-13c-2.2-1.6-3.6-4.2-3.6-7c0-4.8,3.9-8.7,8.7-8.7s8.7,3.9,8.7,8.7c0,2.9-1.4,5.4-3.6,7l3.4,20.5
                                       C59,76,67,78.1,76.4,79.8c3.7-6.5,5.7-14,5.7-21.9V19.8L43.7,9.9z M40.3,17.5l-0.2,2.2l-1.6-1.4l-2.1,0.5l0.9-2l-1.1-1.9l2.2,0.2
                                       l1.4-1.6l0.4,2.1l2,0.8L40.3,17.5z M59.9,22.7l-0.2,2.5l-1.8-1.7l-2.4,0.6l1-2.2l-1.3-2.1l2.5,0.3l1.6-1.9l0.5,2.4l2.3,1
                                       L59.9,22.7z M74.2,71.3L74,74.6l-2.5-2.3l-3.3,0.8l1.4-3.1l-1.8-2.9l3.4,0.4l2.2-2.5l0.7,3.3l3.1,1.3L74.2,71.3z M74.8,51.1
                                       l-0.2,3.1l-2.3-2.1l-3,0.7l1.3-2.8L69,47.4l3.1,0.3l2-2.4l0.6,3l2.9,1.2L74.8,51.1z M75.4,31l-0.2,2.8l-2.1-1.9l-2.7,0.6l1.1-2.5
                                       l-1.4-2.4l2.7,0.3l1.8-2.1l0.6,2.7l2.6,1.1L75.4,31z"></path>
                                 </g>
                                 <g>
                                    <path class="st0" d="M105.1,32.2h11.2v30.7h18.9v8.9h-30.1V32.2z"></path>
                                    <path class="st0" d="M171.5,63.1v8.7h-31.8V32.2h31.1v8.7h-20v6.7h17.6V56h-17.6v7.2L171.5,63.1L171.5,63.1z"></path>
                                    <path class="st0" d="M204.2,51.2h9.9v16.4c-4.6,3.3-10.9,5-16.6,5c-12.6,0-21.8-8.6-21.8-20.6s9.2-20.6,22.1-20.6
                                       c7.4,0,13.4,2.5,17.3,7.2L208,45c-2.7-3-5.8-4.4-9.6-4.4c-6.8,0-11.3,4.5-11.3,11.3c0,6.7,4.5,11.3,11.2,11.3
                                       c2.1,0,4.1-0.4,6.1-1.3L204.2,51.2L204.2,51.2z"></path>
                                    <path class="st0" d="M253,63.1v8.7h-31.8V32.2h31.1v8.7h-20v6.7h17.6V56h-17.6v7.2L253,63.1L253,63.1z"></path>
                                    <path class="st0" d="M259.2,32.2h11.2v30.7h18.9v8.9h-30.1V32.2z"></path>
                                 </g>
                              </g>
                           </svg>
                  <div class="f-16 f-md-18 w300 mt20 lh150 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-16 f-md-18 w300 lh150 white-clr text-xs-center">Copyright © LegelSuites 2022</div>
                  <ul class="footer-ul f-16 f-md-18 w300 white-clr text-center text-md-right">
                     <li><a href="https://support.oppyo.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://legelsuites.co/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://legelsuites.co/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://legelsuites.co/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://legelsuites.co/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://legelsuites.co/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://legelsuites.co/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!--Footer Section End -->
      <!-- timer --->
      <?php
         if ($now < $exp_date) {
         ?>
      <script type="text/javascript">
         // Count down milliseconds = server_end - server_now = client_end - client_now
         var server_end = <?php echo $exp_date; ?> * 1000;
         var server_now = <?php echo time(); ?> * 1000;
         var client_now = new Date().getTime();
         var end = server_end - server_now + client_now; // this is the real end time
         
         var noob = $('.countdown').length;
         
         var _second = 1000;
         var _minute = _second * 60;
         var _hour = _minute * 60;
         var _day = _hour * 24
         var timer;
         
         function showRemaining() {
         var now = new Date();
         var distance = end - now;
         if (distance < 0) {
         	clearInterval(timer);
         	document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
         	return;
         }
         
         var days = Math.floor(distance / _day);
         var hours = Math.floor((distance % _day) / _hour);
         var minutes = Math.floor((distance % _hour) / _minute);
         var seconds = Math.floor((distance % _minute) / _second);
         if (days < 10) {
         	days = "0" + days;
         }
         if (hours < 10) {
         	hours = "0" + hours;
         }
         if (minutes < 10) {
         	minutes = "0" + minutes;
         }
         if (seconds < 10) {
         	seconds = "0" + seconds;
         }
         var i;
         var countdown = document.getElementsByClassName('countdown');
         for (i = 0; i < noob; i++) {
         	countdown[i].innerHTML = '';
         
         	if (days) {
         		countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + days + '</span><span class="f-14 f-md-18 smmltd">Days</span> </div>';
         	}
         
         	countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + hours + '</span><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>';
         
         	countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">' + minutes + '</span><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>';
         
         	countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">' + seconds + '</span><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>';
         }
         
         }
         timer = setInterval(showRemaining, 1000);
         	
      </script>
      <?php
         } else {
         echo "Times Up";
         }
         ?>
      <!--- timer end-->
   </body>
</html>
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <!-- Tell the browser to be responsive to screen width -->
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
      <meta name="title" content="LegelSuites Bonuses">
      <meta name="description" content="LegelSuites Bonuses">
      <meta name="keywords" content="Have A Sneak Peak of A Breakthrough Technology That Analyzes, Detects & Fix Deadly Legal Flaws on Your Website Just by Copy Pasting a Single Line of Code">
      <meta property="og:image" content="https://www.legelsuites.co/special-bonus/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Dr. Amit Pareek">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="LegelSuites Bonuses">
      <meta property="og:description" content="Have A Sneak Peak of A Breakthrough Technology That Analyzes, Detects & Fix Deadly Legal Flaws on Your Website Just by Copy Pasting a Single Line of Code">
      <meta property="og:image" content="https://www.legelsuites.co/special-bonus/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="LegelSuites Bonuses">
      <meta property="twitter:description" content="Have A Sneak Peak of A Breakthrough Technology That Analyzes, Detects & Fix Deadly Legal Flaws on Your Website Just by Copy Pasting a Single Line of Code">
      <meta property="twitter:image" content="https://www.legelsuites.co/special-bonus/thumbnail.png">
      <title>LegelSuites Bonuses</title>
      <!-- Shortcut Icon  -->
      <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
      <!-- Css CDN Load Link -->
      <link rel="stylesheet" type="text/css" href="assets/css/bonus-style.css">
      
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&family=Lexend:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
      <!-- required -->
      <!-- Start Editor required -->
      <link rel="stylesheet" href="https://cdn.oppyo.com/launches/yoseller/common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style.css" type="text/css">
      <script src="https://cdn.oppyo.com/launches/writerarc/common_assets/js/jquery.min.js"></script>
      <script type="text/javascript" src="https://www.legelsuites.com/widget/ada/oppyo-Im7S-MNbL-QN75-NLLR" crossorigin="anonymous" embed-id="oppyo-Im7S-MNbL-QN75-NLLR"></script>
   </head>
   <body>
      <!-- New Timer  Start-->
      <?php
         $date = 'Nov 17 2022 02:00 PM EST';
         $exp_date = strtotime($date);
         $now = time();  
         /*
         
         $date = date('F d Y g:i:s A eO');
         $rand_time_add = rand(700, 1200);
         $exp_date = strtotime($date) + $rand_time_add;
         $now = time();*/
         
         if ($now < $exp_date) {
         ?>
      <?php
         } else {
         	echo "Times Up";
         }
         ?>
      <!-- New Timer End -->
      <?php
         if(!isset($_GET['afflink'])){
         $_GET['afflink'] = 'https://jvz6.com/c/10103/388896/';
         $_GET['name'] = 'Dr. Amit Pareek';      
         }
         ?>
         
      <!-- Header Section Start -->   
      <div class="main-header">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="text-center">
                     <div class="f-md-32 f-20 lh140 w400 white-clr d-block d-md-flex align-items-center justify-content-center">
                        <span class="w600 orange-clr"><?php echo $_GET['name'];?>'s</span> &nbsp;special bonus for &nbsp;
                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 495.7 104.2" style="enable-background:new 0 0 495.7 104.2; max-height:60px;" xml:space="preserve">
                           <style type="text/css">
                              .st0{fill:#FFFFFF;}
                              .st1{fill:#FFA829;}
                              .st2{fill:url(#SVGID_1_);}
                              .st3{fill:url(#SVGID_00000132794433514585357610000014266780508499585408_);}
                           </style>
                           <g>
                              <path class="st0" d="M96.9,82.6c-7.5,2.8-15.7,4.4-24.2,4.8c0.2,0.7,0,1.4-0.5,1.9c-3.2,3.4-6.4,6.1-9.9,8.2
                              c-5.4,3.4-11.5,5.6-17.9,6.5l-0.3,0.1c-0.2,0-0.3,0.1-0.5,0.1c-0.2,0-0.3,0-0.5-0.1l-0.3-0.1c-6.4-0.9-12.4-3.1-17.9-6.6
                              C11.6,89,3.4,74.5,3.2,58.7c0-0.9,0.6-1.7,1.4-2c0,0,0.1,0,0.1,0C1.2,50.6-0.5,43.9,0.1,37C1.8,19.4,18.3,4.9,42.3,0
                              c1-0.2,2,0.4,2.3,1.4c0.3,1-0.2,2.1-1.2,2.4c-5.4,2-10.2,4.6-14.2,7.7l14-3.6c0.3-0.1,0.7-0.1,1,0l38.4,9.9c0.9,0.2,1.5,1,1.5,1.9
                              v38.1c0,7.9-2,15.6-5.7,22.4c5.9,0.1,11.7-0.4,17.4-1.5c1-0.2,2,0.4,2.3,1.4C98.4,81.2,97.9,82.2,96.9,82.6z"></path>
                              <g>
                                 <path class="st0" d="M291.8,67.6l2.5-5.7c3.2,2.5,8.2,4.3,13.1,4.3c6.2,0,8.8-2.2,8.8-5.1c0-8.6-23.6-3-23.6-17.7
                                 c0-6.4,5.1-11.8,16-11.8c4.8,0,9.7,1.3,13.2,3.5l-2.3,5.7c-3.6-2.1-7.5-3.1-10.9-3.1c-6.1,0-8.6,2.4-8.6,5.4
                                 c0,8.4,23.5,3,23.5,17.5c0,6.3-5.1,11.8-16,11.8C301.2,72.4,295,70.4,291.8,67.6z"></path>
                                 <path class="st0" d="M464,67.6l2.5-5.7c3.2,2.5,8.2,4.3,13.1,4.3c6.2,0,8.8-2.2,8.8-5.1c0-8.6-23.6-3-23.6-17.7
                                 c0-6.4,5.1-11.8,16-11.8c4.8,0,9.7,1.3,13.2,3.5l-2.3,5.7c-3.6-2.1-7.5-3.1-10.9-3.1c-6.1,0-8.6,2.4-8.6,5.4
                                 c0,8.4,23.5,3,23.5,17.5c0,6.3-5.1,11.8-16,11.8C473.4,72.4,467.2,70.4,464,67.6z"></path>
                                 <path class="st0" d="M330.3,54.5V32.2h7.3v22.1c0,8.1,3.7,11.7,10.1,11.7s10-3.5,10-11.7V32.2h7.2v22.3c0,11.6-6.5,17.8-17.3,17.8
                                 C336.9,72.4,330.3,66.1,330.3,54.5z"></path>
                                 <path class="st0" d="M375.3,32.2h7.3v39.6h-7.3V32.2z"></path>
                                 <path class="st0" d="M401.4,38.4h-13.1v-6.2h33.6v6.2h-13.1v33.4h-7.3V38.4H401.4z"></path>
                                 <path class="st0" d="M457.2,65.6v6.2h-29.7V32.2h28.9v6.2h-21.6v10.3H454v6.1h-19.1v10.9L457.2,65.6L457.2,65.6z"></path>
                              </g>
                              <g>
                                 <path class="st1" d="M96.2,80.7c-10.5,3.9-23,5.6-36.1,4.4C25.9,81.9,0,60.5,2.1,37.2C3.7,20,20.3,6.5,42.7,2
                                 C26.1,8.2,14.6,19.8,13.2,34.1C11.1,57.4,37,78.8,71.1,81.9C79.9,82.7,88.4,82.2,96.2,80.7z"></path>
                                 <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="20.0931" y1="1069.2449" x2="47.1737" y2="1042.1643" gradientTransform="matrix(1 0 0 1 0 -978)">
                                    <stop offset="0" style="stop-color:#0055FF"></stop>
                                    <stop offset="1" style="stop-color:#008CFF"></stop>
                                 </linearGradient>
                                 <path class="st2" d="M68.3,87.8c-2,0-3.9-0.1-5.9-0.2c-1.3,1.1-2.7,2.1-4.2,3.1c-4.5,2.8-9.3,4.6-14.5,5.4
                                 c-5.1-0.8-10-2.6-14.5-5.4c-8.7-5.4-14.6-13.8-17-23.4c-2.8-2.7-5.1-5.6-7-8.6c0.2,15.2,8,29,20.9,37.1c5.3,3.3,11,5.4,17.2,6.3
                                 l0.4,0.1l0.4-0.1c6.1-0.9,11.9-3,17.2-6.3c3.6-2.2,6.7-4.9,9.5-7.9C70,87.8,69.1,87.8,68.3,87.8z"></path>
                                 <linearGradient id="SVGID_00000129182319601907477880000006569859738938224028_" gradientUnits="userSpaceOnUse" x1="33.5187" y1="1042.8949" x2="80.3658" y2="996.0477" gradientTransform="matrix(1 0 0 1 0 -978)">
                                    <stop offset="0" style="stop-color:#0055FF"></stop>
                                    <stop offset="1" style="stop-color:#008CFF"></stop>
                                 </linearGradient>
                                 <path style="fill:url(#SVGID_00000129182319601907477880000006569859738938224028_);" d="M43.7,9.9l-17.5,4.5
                                 c-2,1.9-21.6,31.5,10.2,51.6l2.2-13c-2.2-1.6-3.6-4.2-3.6-7c0-4.8,3.9-8.7,8.7-8.7s8.7,3.9,8.7,8.7c0,2.9-1.4,5.4-3.6,7l3.4,20.5
                                 C59,76,67,78.1,76.4,79.8c3.7-6.5,5.7-14,5.7-21.9V19.8L43.7,9.9z M40.3,17.5l-0.2,2.2l-1.6-1.4l-2.1,0.5l0.9-2l-1.1-1.9l2.2,0.2
                                 l1.4-1.6l0.4,2.1l2,0.8L40.3,17.5z M59.9,22.7l-0.2,2.5l-1.8-1.7l-2.4,0.6l1-2.2l-1.3-2.1l2.5,0.3l1.6-1.9l0.5,2.4l2.3,1
                                 L59.9,22.7z M74.2,71.3L74,74.6l-2.5-2.3l-3.3,0.8l1.4-3.1l-1.8-2.9l3.4,0.4l2.2-2.5l0.7,3.3l3.1,1.3L74.2,71.3z M74.8,51.1
                                 l-0.2,3.1l-2.3-2.1l-3,0.7l1.3-2.8L69,47.4l3.1,0.3l2-2.4l0.6,3l2.9,1.2L74.8,51.1z M75.4,31l-0.2,2.8l-2.1-1.9l-2.7,0.6l1.1-2.5
                                 l-1.4-2.4l2.7,0.3l1.8-2.1l0.6,2.7l2.6,1.1L75.4,31z"></path>
                              </g>
                              <g>
                                 <path class="st0" d="M105.1,32.2h11.2v30.7h18.9v8.9h-30.1V32.2z"></path>
                                 <path class="st0" d="M171.5,63.1v8.7h-31.8V32.2h31.1v8.7h-20v6.7h17.6V56h-17.6v7.2L171.5,63.1L171.5,63.1z"></path>
                                 <path class="st0" d="M204.2,51.2h9.9v16.4c-4.6,3.3-10.9,5-16.6,5c-12.6,0-21.8-8.6-21.8-20.6s9.2-20.6,22.1-20.6
                                 c7.4,0,13.4,2.5,17.3,7.2L208,45c-2.7-3-5.8-4.4-9.6-4.4c-6.8,0-11.3,4.5-11.3,11.3c0,6.7,4.5,11.3,11.2,11.3
                                 c2.1,0,4.1-0.4,6.1-1.3L204.2,51.2L204.2,51.2z"></path>
                                 <path class="st0" d="M253,63.1v8.7h-31.8V32.2h31.1v8.7h-20v6.7h17.6V56h-17.6v7.2L253,63.1L253,63.1z"></path>
                                 <path class="st0" d="M259.2,32.2h11.2v30.7h18.9v8.9h-30.1V32.2z"></path>
                              </g>
                           </g>
                        </svg>
                     </div>
                  </div>
               </div>
            </div>
         </div>

         <div class="header-wrap">
            <div class="container">
               <div class="row">
                  <div class="col-12 text-center">
                     <div class="preheadline f-16 f-md-22 w400 white-clr lh140">
                        Grab My 20 Exclusive Bonuses Before the Deal Ends...
                     </div>
                  </div>

                  <div class="col-12 mt-md30 mt20 f-md-45 f-28 w700 text-center cyan-clr lh140">
                  Have A Sneak Peak of A Breakthrough Technology That Analyzes, Detects & Fix Deadly Legal Flaws
                  on Your Website Just by Copy Pasting a Single Line of Code
                  </div>

                  <div class="col-12 mt-md30 mt20 f-18 f-md-26 w500 text-center lh130 white-clr text-capitalize">
                     <div class="f-18 f-md-26 w500 text-center lh130 white-clr text-capitalize">               
                        Fix Deadly Law Issues Like ADA, GDPR, T&C & Much More | Comes with<br class="d-none d-md-block"> Done-For-You Agency Website to Quick Start
                        <img src="assets/images/h-line.webp" alt="H-Line" class="mx-auto img-fluid d-none d-md-block mt15">
                     </div>
                  </div>
                  <div class="col-12 mt20 mt-md40">
                     <div class="f-16 f-md-22 w400 text-center lh130 white-clr text-capitalize">               
                     Watch My Quick LegelSuites Review
                     </div>
                  </div>
                  <div class="row mt20 mt-md40">
                     <div class="col-md-10 col-12 mx-auto">
                        <div class="col-12 responsive-video border-video">
                           <iframe src="https://legelsuites.dotcompal.com/video/embed/mcsmui8s77" style="width:100%; height:100%" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                        </div>
                        <!-- <img src="assets/images/product-image.webp" class="img-fluid mx-auto d-block" alt="ProductBox"> -->
                     </div>
                  </div>

                  <div class="row row-cols-md-2 row-cols-xl-2 row-cols-1 mt20 mt-md40 calendar-wrap mx-auto">
                     <div class="col">
                        <ul class="list-head pl0 m0 f-18 f-md-18 lh140 w300 white-clr">
                        <li><span class="w500">Quick-Start with Built-in Agency Website Builder</span> with Drag & Drop Editor to Start Selling Services Starting Today</li>
                        </ul>
                     </div>
                     <div class="col">
                        <ul class="list-head pl0 m0 f-18 f-md-18 lh140 w300  white-clr">
                        <li><span class="w500">Scans Any Website with LegelSuites Analyzer</span> to detect Legal Flaws on your Website like GDPR, ADA, Privacy Policy, T&C, etc</li>
                        </ul>
                     </div>
                     <div class="col">
                        <ul class="list-head pl0 m0 f-18 f-md-18 lh140 w300  white-clr">
                           <li><span class="w500">Help You Make Your Website ADA Compliant</span> by Copy-Pasting a Single Line of Code</li>
                        </ul>
                     </div>
                     <div class="col">
                        <ul class="list-head pl0 m0 f-18 f-md-18 lh140 w300 white-clr">
                        <li><span class="w500">Get Your Website GDPR & Privacy Laws Comply</span> as per International Laws</li>
                        </ul>
                     </div>
                     <div class="col">
                        <ul class="list-head pl0 m0 f-18 f-md-18 lh140 w300 white-clr">
                        <li><span class="w500">Generate Cookies Policies & Cookies Widgets</span> with an inbuilt GDPR Tool</li>
                        </ul>
                     </div>
                     <div class="col">
                        <ul class="list-head pl0 m0 f-18 f-md-18 lh140 w300 white-clr">
                        <li><span class="w500">Copy Paste a Single Line Of Code</span> To Make Any Of Your Business Websites 100% Compliance</li>
                        </ul>
                     </div>
                     <div class="col">
                        <ul class="list-head pl0 m0 f-18 f-md-18 lh140 w300 white-clr">
                        <li><span class="w500">Get Tailor-Made Legal Policies</span> and Other Legal Documents For Your's And Your Client's Websites</li>
                        </ul>
                     </div>
                     <div class="col">
                        <ul class="list-head pl0 m0 f-18 f-md-18 lh140 w300 white-clr">
                        <li><span class="w500">Tap into Hot Website Agency Services to Make Huge Profits</span></li>
                        </ul>
                     </div>
                     <div class="col">
                        <ul class="list-head pl0 m0 f-18 f-md-18 lh140 w300 white-clr">
                        <li><span class="w500">Website Widget to add on your Agency Website</span> to Offer Free Legal & Compliance Tests to quickly generate leads</li>
                        </ul>
                     </div>
                     <div class="col">
                        <ul class="list-head pl0 m0 f-18 f-md-18 lh140 w300 white-clr">
                        <li><span class="w500">Step-by-Step Training</span> is Included to get you Started Today</li>
                        </ul>
                     </div>
                     <div class="col">
                        <ul class="list-head pl0 m0 f-18 f-md-18 lh140 w300 white-clr">
                        <li><span class="w500">Very Limited Agency License Included</span> to Serve Your Clients and Make Huge Profits</li>
                        </ul>
                     </div>
                  </div>

                  <div class="row mt20 mt-md50">
                     <div class="col-md-12 col-md-12 col-12 text-center">
                        <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                        <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                        <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                           Use Coupon Code <span class="w700 cyan-clr">"LEGELSUITES"</span> for an Additional <span class="w700 cyan-clr">$3 Discount</span> on Agency Licence
                        </div>
                     </div>
                     <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                        <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                        <span class="text-center">Grab LegelSuites + My 20 Exclusive Bonuses</span>
                        </a>
                     </div>
                     <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                        <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
                     </div>
                     <div class="col-12 mt15 mt-md40">
                        <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
                     </div>
                     <!-- Timer -->
                     <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                        <div class="countdown counter-white white-clr">
                           <div class="timer-label text-center">
                              <span class="f-31 f-md-60 timerbg oswald">00</span>
                              <br>
                              <span class="f-14 f-md-18 smmltd">Days</span>
                           </div>
                           <div class="timer-label text-center">
                              <span class="f-31 f-md-60 timerbg oswald">00</span>
                              <br>
                              <span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                           </div>
                           <div class="timer-label text-center timer-mrgn">
                              <span class="f-31 f-md-60 timerbg oswald">00</span>
                              <br>
                              <span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                           </div>
                           <div class="timer-label text-center ">
                              <span class="f-31 f-md-60 timerbg oswald">00</span>
                              <br>
                              <span class="f-14 f-md-18 w500 smmltd">Sec</span>
                           </div>
                        </div>
                     </div>
                     <!-- Timer End -->
                  </div>
               </div>
            </div>
         </div> 
      </div>
      <!-- Header Section End -->

      <!-- Step Section Start -->
      <div class="step-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-md-45 f-28 w700 lh140 text-center black-clr">
                  Make Any Website Legally Compliant <br> 
                  <span class="blue-clr">in Just  3 Single Steps </span>                    
                  </div>
               </div>
            </div>
            <div class="row mt30 mt-md80">
               <div class="col-md-4 col-12">
                  <div class="step-box">
                     <div class="step-title f-20 f-md-22 w700">Step 1</div>
                     <img src="assets/images/step-1.webp" class="img-fluid d-block mx-auto my20">
                     <div class="step-content">
                        <div class="f-24 f-md-36 w700 white-clr">Login </div>
                        <div class="w400 f-18 f-md-22 lh140 mt15 white-clr">
                           Login to Legelsuites Dashboard
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-4 col-12 mt30 mt-md0">
                  <div class="step-box">
                     <div class="step-title f-20 f-md-22 w700">Step 2</div>
                     <img src="assets/images/step-2.webp" class="img-fluid d-block mx-auto my20">
                     <div class="step-content">
                        <div class="f-24 f-md-36 w700 white-clr">Analyse &amp; Detect</div>
                        <div class="w400 f-18 f-md-22 lh140 mt15 white-clr">
                           Analyze your website with LegelSuites Website Checker and get the legal compliance issues detected.
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-4 col-12 mt30 mt-md0">
                  <div class="step-box">
                     <div class="step-title f-20 f-md-22 w700">Step 3</div>
                     <img src="assets/images/step-3.webp" class="img-fluid d-block mx-auto my20">
                     <div class="step-content">
                        <div class="f-24 f-md-36 w700 white-clr">Fix Legal Flaws</div>
                        <div class="w400 f-18 f-md-22 lh140 mt15 white-clr">
                           Get the legal flaws fixed on your website. LegelSuites Provide Fixes to 5+ International Laws for Websites.  
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md60">
               <div class="col-12 f-md-20 f-18 w400 lh140 black-clr text-center">
                  It only takes one line of code to add to your website and LegelSuites will get the rest of the work done for you, from attracting hot leads, get the client's website fixed which will eventually make you a handsome profit.             
               </div>
               <div class="col-12 f-md-24 f-20 w700 lh140 black-clr text-center mt20">
                  No Installation, No Coding, and No Other Techie Stuff.           
               </div>
               <div class="col-12 f-md-24 f-20 w700 lh140 blue-clr text-center mt20">
                  It's EASY, FAST, and BUILT with Love.  
               </div>
            </div>
         </div>
      </div>
      <!-- Step Section End -->
      <!-- CTA Btn Start-->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 cyan-clr">"LEGELSUITES"</span> for an Additional <span class="w700 cyan-clr">$3 Discount</span> on Agency Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab LegelSuites + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 smmltd">Days</span>
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Sec</span>
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Btn End -->

      <!-- Proven Section -->
      <div class="proof-section">
         <div class="container">
            <div class="row">
               <div class="f-28 f-md-45 w700 lh140 text-capitalize text-center black-clr col-12">                 
               The Website Legal Compliance Service Is So High-In-Demand That We Got 20%+ Conversion From Our Existing Customer Base
               </div>
               <div class="col-12 mt20 mt-md50">
                  <img src="https://cdn.oppyo.com/launches/legelsuites/special/proof.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md50">
               <div class="col-12 col-md-6 f-md-32 f-22 w700 lh140 black-clr">
               Also Got 900+ New Leads on My Agency Website in Just 10 Days.               
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                  <img src="https://cdn.oppyo.com/launches/legelsuites/special/proof1.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>
            <div class="row mt20 mt-md60">
               <div class="col-12 f-md-24 f-20 w400 lh140 black-clr text-center">
               To attract new leads of potential customers you just need to add a FREE website analyser (Comes with  LegalSuite) on your website and LegalSuites will grab potential leads for you 24*7.    
               </div>
            </div>
         </div>
      </div>
      <!-- Proven Section Ends-->

      <!-- <section class="analyzer-sec">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-26 f-md-45 w700 lh140 text-center">
                     Test Drive The LegelSuites Website Analyzer
                  </div>
                  <img src="assets/images/zig-zag.webp" alt="Zig Zag Line" class="mx-auto img-fluid d-block mb20 mb-md30">
                  <script type="text/javascript" src="https://www.legelsuites.com/widget/checker/oppyo-JoU0-q3qS-XQsN-Gbz5" crossorigin="anonymous" embed-id="oppyo-JoU0-q3qS-XQsN-Gbz5"></script>
               </div>
            </div>
         </div>
      </section> -->


      <section class="didyouknow-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-36 f-md-45 w800 blue-clr text-center lh140">Did You Know?</div>
                  <img src="assets/images/know-line.webp" class="img-fluid d-none d-md-block mx-auto mt5">
                  <div class="youknow-block mt20 mt-md40">
                     <div class="f-24 f-md-38 w800 black-clr text-center lh140">
                        Over 100,000 Website Owners Have been Fined<br class="d-none d-md-block"> or Sued 
                        for Non-Compliance in the Last<br class="d-none d-md-block"> Couple of Years!                           
                     </div>
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md20 align-items-center">
               <div class="col-12 col-md-6">
                  <div class="f-22 f-md-28 w400 lh140 black-clr text-center text-md-start">                    
                     So, if you are a Website Owner, you can be targeted 
                     for issues on your website and you can get indulge 
                     in serious Law Problems or can be Fined.                      
                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                  <img src="assets/images/trillion.webp" class="img-fluid mx-auto d-block">
               </div>
            </div>
         </div>
      </section>

      <section class="compliance-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-45 w700 black-clr text-center lh140">Breaching Website Compliance Law Like <span class="underline">ADA,
                     GDPR, Privacy Policies, Etc.</span> Can Cost You
                     Handsomely
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md80">
               <div class="col-12 col-md-6">
                  <div class="compliance-block">
                     <div class="compliance-title">
                        <div class="f-22 f-md-28 w500 lh140 black-clr text-center">                    
                           Supreme Court hands victory to a <br class="d-none d-md-block"> blind man who sued Domino's over<br class="d-none d-md-block"> site accessibility   
                        </div>
                     </div>
                     <div class="compliance-image">
                        <img src="assets/images/supreme.webp" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                  <div class="compliance-block">
                     <div class="compliance-title">
                        <div class="f-22 f-md-28 w500 lh140 black-clr text-center">                    
                           Google and YouTube Will Pay<br class="d-none d-md-block"> Record $170 Million for Alleged<br class="d-none d-md-block"> Violations of Privacy Law
                        </div>
                     </div>
                     <div class="compliance-image">
                        <img src="assets/images/record.webp" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>

      <div class="stats-sec">
         <div class="stats-wrap">
            <div class="container">
               <div class="row">
                  <div class="col-12 f-28 f-md-45 white-clr text-center w700">
                     Here Are Some More Eye Openers
                  </div>
               </div>
               <div class="row mt40 mt-md60 gx-md-5 align-items-center">
                  <div class="col-12 col-md-6">
                     <div class="f-md-24 f-20 w400 lh140 white-clr text-center text-md-start">
                        The website must be ADS Compliance so that it can
                        be accessible to people with disabilities.                            
                     </div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
                     <img src="https://cdn.oppyo.com/launches/legelsuites/special/stats1.webp" class="img-fluid d-block mx-auto">
                  </div>
               </div>
               <div class="row mt40 mt-md60 gx-md-5 align-items-center">
                  <div class="col-12 col-md-6 order-md-2">
                     <div class="f-md-24 f-20 w400 lh140 white-clr text-center text-md-start">
                        Website must be GDPR Compliant and should have the 
                        consent of users if you are collecting cookies.                      
                     </div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
                     <img src="https://cdn.oppyo.com/launches/legelsuites/special/stats2.webp" class="img-fluid d-block mx-auto">
                  </div>
               </div>
               <div class="row mt40 mt-md60 gx-md-5 align-items-center">
                  <div class="col-12 col-md-6">
                     <div class="f-md-24 f-20 w400 lh140 white-clr text-center text-md-start">
                        Privacy Policy is a must in case you are collecting 
                        users' personal information on your website. 
                     </div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0">
                     <img src="https://cdn.oppyo.com/launches/legelsuites/special/stats3.webp" class="img-fluid d-block mx-auto">
                  </div>
               </div>
               <div class="row mt40 mt-md60 gx-md-5 align-items-center">
                  <div class="col-12 col-md-6 order-md-2">
                     <div class="f-md-24 f-20 w400 lh140 white-clr text-center text-md-start">
                        More than 11,400 Websites filed an ADA lawsuit in 2021— A 320% increase since 2013. It's time to get your website under compliance now!  
                     </div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
                     <img src="https://cdn.oppyo.com/launches/legelsuites/special/stats4.webp" class="img-fluid d-block mx-auto">
                  </div>
               </div>
               <div class="row mt40 mt-md80">
                  <div class="col-12">
                     <div class="f-md-24 f-20 w400 lh140 white-clr text-center">                   
                        Failing to fulfill this Legal Compliance can not only get you to indulge in legal<br class="d-none d-md-block"> problems but it will also cause your website not to get ranked on<br class="d-none d-md-block"> Google and other Search Engines.
                     </div>
                  </div>
                  <div class="col-12 mt20 mt-md40">
                     <div class="f-md-38 f-24 w700 lh140 cyan-clr text-center">   
                        So, Having Your Website 100% Compliant <br class="d-none d-md-block">
                        with International Law is VITAL                
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <section class="giants-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-36 w800 black-clr text-center lh140">
                     Here are some more examples of <span class="underline">how top giants are been<br class="d-none d-md-block">
                     sued or fined due to missing law compliance</span> around <br class="d-none d-md-block">
                     the globe.
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md80 align-items-center">
               <div class="col-12 col-md-6">
                  <div class="compliance-block">
                     <div class="compliance-title">
                        <div class="f-22 f-md-28 w500 lh140 black-clr text-center">                    
                           The National Association of the Deaf brought a lawsuit against the streaming service Netflix   
                        </div>
                     </div>
                     <div class="compliance-image">
                        <img src="assets/images/g1.webp" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                  <div class="compliance-block">
                     <div class="compliance-title">
                        <div class="f-22 f-md-28 w500 lh140 black-clr text-center">                    
                           ADA lawsuit against the meal kit service Blue Apron
                        </div>
                     </div>
                     <div class="compliance-image">
                        <img src="assets/images/g2.webp" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md30">
                  <div class="compliance-block">
                     <div class="compliance-title">
                        <div class="f-22 f-md-28 w500 lh140 black-clr text-center">                    
                           Nike came under fire in 2017 because both websites it operates Nike.com and Converse.com – were inaccessible to visually impaired users
                        </div>
                     </div>
                     <div class="compliance-image">
                        <img src="assets/images/g3.webp" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                  <div class="compliance-block">
                     <div class="compliance-title">
                        <div class="f-22 f-md-28 w500 lh140 black-clr text-center">                    
                           A class-action lawsuit was filed against Beyonce Knowles' Company 
                        </div>
                     </div>
                     <div class="compliance-image">
                        <img src="assets/images/g4.webp" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      
      <!-- Testimonial Section Starts -->
      <!-- <div class="testimonial-section">
         <div class="container ">
            <div class="row ">
               <div class="col-12">
                  <div class="f-md-45 f-28 lh140 w400 text-center black-clr">
                     Check Out What Our Beta Testers Are <br class="d-none d-md-block">
                    <span class="w700"> Saying About LegelSuites!  </span>
                     
                  </div>
               </div>
            </div>
            <div class="row gx-md-5">
               <div class="col-12 col-md-6 mt20 mt-md50">
                  <div class="testimonials-block-border">
                     <div class="testimonials-block f-18 w400 lh140">
                        LegelSuites is something <span class="w600">you must get your hands on to complete with the BIG BOYS</span> in the industry. This is what I was searching for a long time to get desired results with my marketing efforts. With a small one-time investment, all my doubts were cleared and <span class="w600">I'm sure this will be a BLOCKBUSTER as well</span>... 
                            
                     <div class="author-wrap mt20 mt-md20">
                        <img src="assets/images/testi-img5.webp" class="img-fluid">
                        <div class="f-22 w600 lh140 pl15">Dewis Nichole</div>
                     </div>	             							
                     </div>	                     								
                  </div>							
               </div>
               <div class="col-12 col-md-6 mt20 mt-md50">
                  <div class="testimonials-block-border">
                     <div class="testimonials-block f-18 w400 lh140">                      
                     LegelSuites is something that you can't overlook at any cost. The <span class="w600">features you're getting amazed me completely</span> &amp; I'm sure you'll also be in love with them. This is something that'll be of <span class="w600">immense worth for your online selling business</span>... 
                     <div class="author-wrap mt20 mt-md20">
                        <img src="assets/images/testi-img6.webp" class="img-fluid">
                        <div class="f-22 w600 lh140 pl15">Riley Green</div>
                     </div>	             							
                     </div>	                     								
                  </div>								
               </div>
               <div class="col-12 col-md-6 mt20 mt-md30">
                  <div class="testimonials-block-border">
                     <div class="testimonials-block f-18 w400 lh140">                       
                        A <span class="w600">proven &amp; tested, all-in-one E-Selling Technology</span> packed with <span class="w600">100+ proven converting</span>, mobile responsive &amp; ready-to-go Landing Page, Website &amp; Membership templates for the price of just a cappuccino seemed unreal, but LegelSuites made it a complete reality.<br><br>
                        Kudos to the team for creating a solution that can <span class="w600">make the complete online selling process fast &amp; easy.</span> Truly speechless. Rock solid and in complete awe with its features. … 
                        
         
                     <div class="author-wrap mt20 mt-md20">
                        <img src="assets/images/testi-img7.webp" class="img-fluid">
                        <div class="f-22 w600 lh140 pl15">Alexander Ramirez</div>
                     </div>	             							
                     </div>	                     								
                  </div>								
               </div>
               <div class="col-12 col-md-6 mt20 mt-md30">
                  <div class="testimonials-block-border">
                     <div class="testimonials-block f-18 w400 lh140">
                        LegelSuites is next-level quality software. <span class="w600">It's so easy to use, and the training included</span> makes it even easier to build a website or landing page to <span class="w600">sell Digital Products in any niche in a few minutes.</span><br><br>
                        I love the fact that you can create high-converting websites &amp; landing pages without actually being a technical nerd, all without any special skills. It's said that this is a <span class="w600">MUST-HAVE software</span> for marketers!... 
                        
                     <div class="author-wrap mt20 mt-md20">
                        <img src="assets/images/testi-img8.webp" class="img-fluid">
                        <div class="f-22 w600 lh140 pl15">Benjamin Perez</div>
                     </div>	             							
                     </div>	                     								
                  </div>								
               </div>
            </div>
         </div>
         </div> -->
      <!-- Testimonial Section Ends -->

      <div class="proudly-sec" id="product">
         <div class="proudly-inner-sec relative">
            <div class="container">
               <div class="row">
                  <div class="col-12">
                     <div class="row">
                        <div class="col-12 text-center">
                           <div class="f-36 f-md-50 w700 lh140 orange-clr proudly-shape">
                              Introducing
                           </div>
                        </div>
                        <div class="col-12 mt30 mt-md50 text-center">
                           <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 495.7 104.2" style="enable-background:new 0 0 495.7 104.2; max-height:120px;" xml:space="preserve">
                              <style type="text/css">
                                 .st0{fill:#FFFFFF;}
                                 .st1{fill:#FFA829;}
                                 .st2{fill:url(#SVGID_1_);}
                                 .st3{fill:url(#SVGID_00000132794433514585357610000014266780508499585408_);}
                              </style>
                              <g>
                                 <path class="st0" d="M96.9,82.6c-7.5,2.8-15.7,4.4-24.2,4.8c0.2,0.7,0,1.4-0.5,1.9c-3.2,3.4-6.4,6.1-9.9,8.2
                                    c-5.4,3.4-11.5,5.6-17.9,6.5l-0.3,0.1c-0.2,0-0.3,0.1-0.5,0.1c-0.2,0-0.3,0-0.5-0.1l-0.3-0.1c-6.4-0.9-12.4-3.1-17.9-6.6
                                    C11.6,89,3.4,74.5,3.2,58.7c0-0.9,0.6-1.7,1.4-2c0,0,0.1,0,0.1,0C1.2,50.6-0.5,43.9,0.1,37C1.8,19.4,18.3,4.9,42.3,0
                                    c1-0.2,2,0.4,2.3,1.4c0.3,1-0.2,2.1-1.2,2.4c-5.4,2-10.2,4.6-14.2,7.7l14-3.6c0.3-0.1,0.7-0.1,1,0l38.4,9.9c0.9,0.2,1.5,1,1.5,1.9
                                    v38.1c0,7.9-2,15.6-5.7,22.4c5.9,0.1,11.7-0.4,17.4-1.5c1-0.2,2,0.4,2.3,1.4C98.4,81.2,97.9,82.2,96.9,82.6z"></path>
                                 <g>
                                    <path class="st0" d="M291.8,67.6l2.5-5.7c3.2,2.5,8.2,4.3,13.1,4.3c6.2,0,8.8-2.2,8.8-5.1c0-8.6-23.6-3-23.6-17.7
                                       c0-6.4,5.1-11.8,16-11.8c4.8,0,9.7,1.3,13.2,3.5l-2.3,5.7c-3.6-2.1-7.5-3.1-10.9-3.1c-6.1,0-8.6,2.4-8.6,5.4
                                       c0,8.4,23.5,3,23.5,17.5c0,6.3-5.1,11.8-16,11.8C301.2,72.4,295,70.4,291.8,67.6z"></path>
                                    <path class="st0" d="M464,67.6l2.5-5.7c3.2,2.5,8.2,4.3,13.1,4.3c6.2,0,8.8-2.2,8.8-5.1c0-8.6-23.6-3-23.6-17.7
                                       c0-6.4,5.1-11.8,16-11.8c4.8,0,9.7,1.3,13.2,3.5l-2.3,5.7c-3.6-2.1-7.5-3.1-10.9-3.1c-6.1,0-8.6,2.4-8.6,5.4
                                       c0,8.4,23.5,3,23.5,17.5c0,6.3-5.1,11.8-16,11.8C473.4,72.4,467.2,70.4,464,67.6z"></path>
                                    <path class="st0" d="M330.3,54.5V32.2h7.3v22.1c0,8.1,3.7,11.7,10.1,11.7s10-3.5,10-11.7V32.2h7.2v22.3c0,11.6-6.5,17.8-17.3,17.8
                                       C336.9,72.4,330.3,66.1,330.3,54.5z"></path>
                                    <path class="st0" d="M375.3,32.2h7.3v39.6h-7.3V32.2z"></path>
                                    <path class="st0" d="M401.4,38.4h-13.1v-6.2h33.6v6.2h-13.1v33.4h-7.3V38.4H401.4z"></path>
                                    <path class="st0" d="M457.2,65.6v6.2h-29.7V32.2h28.9v6.2h-21.6v10.3H454v6.1h-19.1v10.9L457.2,65.6L457.2,65.6z"></path>
                                 </g>
                                 <g>
                                    <path class="st1" d="M96.2,80.7c-10.5,3.9-23,5.6-36.1,4.4C25.9,81.9,0,60.5,2.1,37.2C3.7,20,20.3,6.5,42.7,2
                                       C26.1,8.2,14.6,19.8,13.2,34.1C11.1,57.4,37,78.8,71.1,81.9C79.9,82.7,88.4,82.2,96.2,80.7z"></path>
                                    <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="20.0931" y1="1069.2449" x2="47.1737" y2="1042.1643" gradientTransform="matrix(1 0 0 1 0 -978)">
                                       <stop offset="0" style="stop-color:#0055FF"></stop>
                                       <stop offset="1" style="stop-color:#008CFF"></stop>
                                    </linearGradient>
                                    <path class="st2" d="M68.3,87.8c-2,0-3.9-0.1-5.9-0.2c-1.3,1.1-2.7,2.1-4.2,3.1c-4.5,2.8-9.3,4.6-14.5,5.4
                                       c-5.1-0.8-10-2.6-14.5-5.4c-8.7-5.4-14.6-13.8-17-23.4c-2.8-2.7-5.1-5.6-7-8.6c0.2,15.2,8,29,20.9,37.1c5.3,3.3,11,5.4,17.2,6.3
                                       l0.4,0.1l0.4-0.1c6.1-0.9,11.9-3,17.2-6.3c3.6-2.2,6.7-4.9,9.5-7.9C70,87.8,69.1,87.8,68.3,87.8z"></path>
                                    <linearGradient id="SVGID_00000129182319601907477880000006569859738938224028_" gradientUnits="userSpaceOnUse" x1="33.5187" y1="1042.8949" x2="80.3658" y2="996.0477" gradientTransform="matrix(1 0 0 1 0 -978)">
                                       <stop offset="0" style="stop-color:#0055FF"></stop>
                                       <stop offset="1" style="stop-color:#008CFF"></stop>
                                    </linearGradient>
                                    <path style="fill:url(#SVGID_00000129182319601907477880000006569859738938224028_);" d="M43.7,9.9l-17.5,4.5
                                       c-2,1.9-21.6,31.5,10.2,51.6l2.2-13c-2.2-1.6-3.6-4.2-3.6-7c0-4.8,3.9-8.7,8.7-8.7s8.7,3.9,8.7,8.7c0,2.9-1.4,5.4-3.6,7l3.4,20.5
                                       C59,76,67,78.1,76.4,79.8c3.7-6.5,5.7-14,5.7-21.9V19.8L43.7,9.9z M40.3,17.5l-0.2,2.2l-1.6-1.4l-2.1,0.5l0.9-2l-1.1-1.9l2.2,0.2
                                       l1.4-1.6l0.4,2.1l2,0.8L40.3,17.5z M59.9,22.7l-0.2,2.5l-1.8-1.7l-2.4,0.6l1-2.2l-1.3-2.1l2.5,0.3l1.6-1.9l0.5,2.4l2.3,1
                                       L59.9,22.7z M74.2,71.3L74,74.6l-2.5-2.3l-3.3,0.8l1.4-3.1l-1.8-2.9l3.4,0.4l2.2-2.5l0.7,3.3l3.1,1.3L74.2,71.3z M74.8,51.1
                                       l-0.2,3.1l-2.3-2.1l-3,0.7l1.3-2.8L69,47.4l3.1,0.3l2-2.4l0.6,3l2.9,1.2L74.8,51.1z M75.4,31l-0.2,2.8l-2.1-1.9l-2.7,0.6l1.1-2.5
                                       l-1.4-2.4l2.7,0.3l1.8-2.1l0.6,2.7l2.6,1.1L75.4,31z"></path>
                                 </g>
                                 <g>
                                    <path class="st0" d="M105.1,32.2h11.2v30.7h18.9v8.9h-30.1V32.2z"></path>
                                    <path class="st0" d="M171.5,63.1v8.7h-31.8V32.2h31.1v8.7h-20v6.7h17.6V56h-17.6v7.2L171.5,63.1L171.5,63.1z"></path>
                                    <path class="st0" d="M204.2,51.2h9.9v16.4c-4.6,3.3-10.9,5-16.6,5c-12.6,0-21.8-8.6-21.8-20.6s9.2-20.6,22.1-20.6
                                       c7.4,0,13.4,2.5,17.3,7.2L208,45c-2.7-3-5.8-4.4-9.6-4.4c-6.8,0-11.3,4.5-11.3,11.3c0,6.7,4.5,11.3,11.2,11.3
                                       c2.1,0,4.1-0.4,6.1-1.3L204.2,51.2L204.2,51.2z"></path>
                                    <path class="st0" d="M253,63.1v8.7h-31.8V32.2h31.1v8.7h-20v6.7h17.6V56h-17.6v7.2L253,63.1L253,63.1z"></path>
                                    <path class="st0" d="M259.2,32.2h11.2v30.7h18.9v8.9h-30.1V32.2z"></path>
                                 </g>
                              </g>
                           </svg>
                        </div>
                     </div>
                     <div class="col-12 col-md-12 mt20 mt-md30">
                        <div class="f-24 f-md-36 w700 text-center lh140 cyan-clr text-capitalize">
                           All in One Platform to Start Serving Website Owners &amp; Developers for the Website's Legal Compliance as per International Laws to save them from Law Suits.
                        </div>
                     </div>
                     <div class="col-12 col-md-12 mt20 mt-md30">
                        <div class="f-22 f-md-24 w600 lh140 white-clr text-center">
                           Generate Legal Compliance Like GDPR, ADA, Privacy Policy, and other Legal Documents<br class="d-none d-md-block"> with just a click of a Button in 60 Second Flat. 
                        </div>
                     </div>
                     <div class="row mt30 mt-md50 align-items-center">
                        <div class="col-12 col-md-10 mx-auto">
                           <img src="assets/images/product-image.webp" class="img-fluid d-block mx-auto img-animation">
                        </div>
                     </div>
                     <img src="assets/images/proudly-icon1.webp" alt="Proudly Icon Right" class="d-none d-md-block" style="position:absolute; top:40px; right:64px;">
                     <img src="assets/images/proudly-icon2.webp" alt="Proudly Icon Right" class="d-none d-md-block" style="position:absolute; bottom:47px; left:51px;">
                  </div>
               </div>
            </div>
         </div>
      </div>

      <!-- Bonus Section Header Start -->
      <div class="bonus-header">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-10 mx-auto heading-bg text-center">
                  <div class="f-24 f-md-36 lh140 w700"> When You Purchase LegelSuites, You Also Get <br class="hidden-xs"> Instant Access To These 20 Exclusive Bonuses</div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus Section Header End -->
      <!-- Bonus #1 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 1</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus1.webp" class="img-fluid mx-auto d-block" alt="Bonus1">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                           Social Traffic Plan
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>Online digital marketing has numerous advantages for your business.</b></li>
                           <li>Not only is it a cost-effective way to increase awareness about your brand, but the information you post on the Internet travels fast and has no geographical boundaries.</li>
                           <li>
                           Learning about the effectively utilize the most popular social media platforms is the first step to driving more traffic to your site and finding success.
                           </li>
                           <li>
                           This simple guide will show you what you need to do to boost your targeted website traffic using social media. 
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #1 Section End -->
      <!-- Bonus #2 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 2</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30">
                  <div class="row">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="assets/images/bonus2.webp" class="img-fluid mx-auto d-block" alt="Bonus2">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md0 text-capitalize">
                        Traffic Machine
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>Are you a small business that has been struggling to increase the traffic to your website?</b></li>
                           <li><b>Did you spend your valuable time building a website only to have it go unseen?</b></li>
                           <li><b>If your website isn't bringing in the visitors like it should, then you may need to think your content marketing strategy.</b></li>
                           <li>Everyone has struggled with increasing traffic to their site and one point in time or another. Those who have become success, have discovered the secrets to turning their site into a traffic machine.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #2 Section End -->
      <!-- Bonus #3 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 3</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus3.webp" class="img-fluid mx-auto d-block " alt="Bonus3">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Content Syndication
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>Content Syndication Videos is a series of 40 On-Screen, Easy-To-Follow Video Tutorials on how to market and publish your content expertly.</b></li>
                          <li>The Internet is full of 'me too marketers' and what makes this frustrating is that even if you're a genuine expert, it's not a guarantee you'll be successful with marketing yourself.</li>
                          <li>
                          This course will help you to get an unfair advantage and stay ahead of the competition. Lessons are short, simple and direct.
                          </li>
                          <li>
                          You will learn how to get more visitors, attract more leads and close more sales!
                          </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #3 Section End -->
      <!-- Bonus #4 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 4</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="assets/images/bonus4.webp" class="img-fluid mx-auto d-block " alt="Bonus4">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Hashtag Traffic Secrets
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li> <b> Get Laser-Targeted Social Media Traffic Using The Power Of Hashtags! </b> </li>
                           <li>
                           There are plenty of different ways to drive traffic to your website today, but some are definitely more effective – and attractive – than others. 
                           </li>
                           <li>
                           Social media marketing, more specifically “hashtag marketing”, is definitely one of the hottest drivers of organic traffic today and you have to make sure that you are making the most of the hashtags you create online right now to build your business from the ground up.
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #4 End -->
      <!-- Bonus #5 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 5</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus5.webp" class="img-fluid mx-auto d-block " alt="Bonus5">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Web Amcom Pro
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>This easy to use software inserts ads into your web pages, showing a selection of the latest eBay auctions relating to your niche. </b> Ads are updated automatically every day, so your website always shows the very latest ads for your niche.</li>
                           <li>
                           All the ads include your Amazon affiliate ID, so you earn commissions on any resulting sales - giving your website an instant profit boost. The software can be used to add Amazon ads to any website in minutes.
                           </li>
                           <li>
                           The software is licensed for use on an unlimited number of websites, so you can use it on every site you own, without paying any extra.  
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #5 End -->
      <!-- CTA Button Section Start -->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 cyan-clr">"LEGELSUITES"</span> for an Additional <span class="w700 cyan-clr">$3 Discount</span> on Agency Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab LegelSuites + My 20 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                        <span class="f-14 f-md-18 w500 ">Days</span>
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                        <span class="f-14 f-md-18 w500 ">Hours</span>
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                        <span class="f-14 f-md-18 w500 ">Mins</span>
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                        <span class="f-14 f-md-18 w500 ">Sec</span>
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->
      <!-- Bonus #6 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 6</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="assets/images/bonus6.webp" class="img-fluid mx-auto d-block " alt="Bonus6">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Web Baycom Pro
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li> <b>This easy to use software inserts ads into your web pages, showing a selection of the latest eBay auctions relating to your niche.</b> These ads update automatically every hour, so your website always shows the very latest auctions for your niche.</li> 

                           <li>The software uses the eBay auction 'RSS feeds' to get the latest auctions. These provide a self-updating selection of the latest auctions matching any keyword.</li>

                           <li>The software can be used to add eBay auctions to any website in minutes.</li> 

                           <li>You don't need to add any complicated code to your web pages. You just need to insert some special text on each page where you want to feed to appear.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #6 End -->
      <!-- Bonus #7 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 7</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus7.webp" class="img-fluid mx-auto d-block " alt="Bonus7">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Turbo GIF Animator
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>If you are digital marketer, chances are you may already had use graphics in promoting your products or services online</li>

                           <li>The thing is that, one of the best type of images that engage more viewers in social media is the images that are moving or simply an animated images in GIF format</li>

                           <li>Now, it's Time To Get CURRENT & Begin Informing Your Audience About Your New Product(s) In a More Interesting & Appealing Way, In Just A Few Seconds!</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #7 End -->
      <!-- Bonus #8 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 8</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus8.webp" class="img-fluid mx-auto d-block " alt="Bonus8">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Viral Content Crusher
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>
                              <b>Just one highly effective viral marketing campaign can be more significant than years of regular advertising. </b> In this guide you are going to learn a number of things concerning making viral content. The aim is to give you a comprehensive knowledge on the subject matter that will help you come up with content that will go viral.
                           </li>
                           <li>What is the definition of viral content and why do people share it?</li>
                           <li>Understanding the customer and then charming the viewers with your content</li>
                           <li>Reviews of case studies of the past</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #8 End -->
      <!-- Bonus #9 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 9</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus9.webp" class="img-fluid mx-auto d-block " alt="Bonus9">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Lead Generation On Demand
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>It doesn't matter what kind of business you're in, if you aren't able to generate new leads and turn them into paying customers, your company will never succeed. </li> 
                           <li>You need to be constantly bringing in new customers if you want your business to thrive.</li> 
                           <li>Generating more leads is anything but easy and if you don't have a solid marketing strategy that will drive more traffic to your website, you'll never be able to generate the leads you need for your business to succeed. </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #9 End -->
      <!-- Bonus #10 start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 10</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus10.webp" class="img-fluid mx-auto d-block " alt="Bonus10">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        WP SEO Track Plugin
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>With this simple plugin you can get the true insight on your web traffic efforts in only seconds!</b> Watch as your social network shares increase, your google PageRank and more.</li>
                           <li>Get a clear vision on what you need to start focusing on with your SEO efforts. Start more effective backlinks from Facebook, Twitter, Google and more.</li>
                           <li>You will get all of the most important stats that you need to know for your SEO web traffic.</li>
                           <li>Focus on the amount of shares you have on popular social sites like Facebook, Twitter, Stumble Upon and more.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #10 End -->
      <!-- CTA Button Section Start -->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 cyan-clr">"LEGELSUITES"</span> for an Additional <span class="w700 cyan-clr">$3 Discount</span> on Agency Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab LegelSuites + My 20 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 smmltd">Days</span> </div>
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">0300</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                     <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                     <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->
      <!-- Bonus #11 start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 11</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus11.webp" class="img-fluid mx-auto d-block " alt="Bonus13">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Authority Site Blueprint
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>
                              <b> This 4-part audio course will teach you how to create an authority site and rank it highly on search engine to get traffic.</b> An authority site is a very high quality website that is respected by knowledgeable people in its industry. 
                           </li>   
                           <li>
                           This guide will walk you through the easy to use technology and useful content strategies. 
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #11 End -->
      <!-- Bonus #12 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 12</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus12.webp" class="img-fluid mx-auto d-block " alt="Bonus12">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        The Internet Marketer's Toolkit
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>Internet marketing means getting word out on the web and it means creating brilliant content.</b></li>
                           <li>The only problem is that very few people have any idea what makes the web tick. Even if you know the basics of internet marketing, there's a good chance that you don't have all of the advanced skills you need to really make any project into a success.</li>
                           <li>Learn the basics of internet marketing 101 even if you have no prior experience.</li>
                           <li>Learn how to turn any idea or concept into a reality.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #12 End -->
      <!-- Bonus #13 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 13</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus13.webp" class="img-fluid mx-auto d-block " alt="Bonus13">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Buying Traffic to Generate Massive Website Visitors
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>An intrinsic understanding of your competition and how to better them is the most important component of any digital marketing strategy. Today, there is such small distances between you and your competition - it's unlikely that you're offering anything that cannot be bought on some other website.</li> 
                           <li>   
                           Now, when a consumer wants and needs a product or information, all they must do is input their desires into the Google search and peruse the search engine results page.
                           </li>
                           <li>
                           Standing out in the search results is by far the most important part of driving traffic to your site. In this ebook, you will learn about the main and most popular techniques to drive traffic to your website.
                           </li>
                           <li>
                           Also, you will learn all the terms you need to know for a proper understanding of the web marketing industry.
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #13 End -->
      <!-- Bonus #14 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 14</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus14.webp" class="img-fluid mx-auto d-block " alt="Bonus14">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Foolproof Traffic System Gold
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                          <li> <b>Many internet marketers overlook how important traffic is when it comes to making product sales.</b> Traffic is the lifeblood of your business. There are many ways to get traffic to your website, some of them are free methods, and others are paid. It makes sense to use both ways of getting traffic to build and grow your business.</li>
                          <li>
                           You will learn the difference between quantity and quality of traffic.</li>
                          <li>You will learn about the importance of quality content and the power of search engine optimization (SEO), viral marketing, and social media marketing.</li> 
                          <li>
                           You will also learn about how Google Analytics can help you learn more about the traffic that is coming to your site.
                          </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #14 End -->
      <!-- Bonus #15 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 15</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus15.webp" class="img-fluid mx-auto d-block " alt="Bonus15">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Foolproof Traffic System
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                          <li><b> The bottom line is that you need to get traffic to your offers, or you will have no sales.</b> That traffic needs to be quality traffic, the target market/audience that will be interested in your offers.</li> 

                          <li>This step by step guide will help you to get more traffic on your site! It's easier than you think with following this guide!</li> 

                          <li>Includes sales page, graphics, social media posts and much more!</li> 
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #15 End -->

      <!-- CTA Button Section Start -->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 cyan-clr">"LEGELSUITES"</span> for an Additional <span class="w700 cyan-clr">$3 Discount</span> on Agency Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab LegelSuites + My 20 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 smmltd">Days</span> </div>
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">0300</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                     <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                     <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->

            <!-- Bonus #16 start -->
            <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 16</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus16.webp" class="img-fluid mx-auto d-block " alt="Bonus16">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        X-Treme List Build Plugin
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>
                              <b>Start creating engagement with your new leads by making your landing page memorable and enjoyable!</b>
                           </li>   
                           <li>
                           Just upload, click activate and you are ready to create unlimited awesome pages!
                           </li>
                           <li>
                           Edit every detail on the fly with the simple options panel for each page
                           </li>
                           <li>
                           Customize all of the content areas that are designed to be readable
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #16 End -->
      <!-- Bonus #17 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 17</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus17.webp" class="img-fluid mx-auto d-block " alt="Bonus17">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        WP Youtube Leads Plugin
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>With this plugin you can enhance the user engagement of your Youtube videos and increase your mailing list.</li>
                           <li>Integrate any YouTube video and start converting right away. Use the time-stamps with the video to maximize interest and action.</li>
                           <li>Create custom headlines and fill out the content the way you want</li>
                           <li>Customize the color and design to perfect the pesentation</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #17 End -->
      <!-- Bonus #18 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 18</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus18.webp" class="img-fluid mx-auto d-block " alt="Bonus18">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        WP Survey Creator
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li> <b>WP Survey Creator is a WordPress plugin that allows you to incorporate a survey feature into your WordPress-powered website.</b> With this plugin you can create surveys with different types of questions and control how it appears on the page. What's more, you'll have the ability to gather important results and statistics of the answers supplied by the respondents.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #18 End -->
      <!-- Bonus #19 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 19</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus19.webp" class="img-fluid mx-auto d-block " alt="Bonus19">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Power Tools Video Site Builder
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                          <li> <b>With this software you can instantly create your own complete moneymaking video site featuring Adsense and Amazon ads, unique web pages and SEO Solutions!</b> To create your complete video site, you need to activate the Site Builder Tool, enter a few details into the simple form and click a button. A complete website is instantly built, ready to upload.</li>
                          <li>120 videos sourced from YouTube. When you use the software, it fetches the latest selection of most popular videos for this particular niche. So when you build your site, you can be sure it will be fully up to date with the very latest videos.</li>
                          <li>Content provided by extracting random snippets from a set of 20 private label articles, ensuring that your pages are unique and contain niche-targeted content. (Most other video site builders scrape the content from YouTube, so the pages are just duplicates of the YouTube pages and rated as worthless by search engines).</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #19 End -->
      <!-- Bonus #20 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 20</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus20.webp" class="img-fluid mx-auto d-block " alt="Bonus20">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Social Media Boom Software
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                          <li><b> Turn tour Wordpress blog into a social powerhouse! The simple and fast way to increase social conversions.</b></li> 

                          <li>Take the social features of some of the highest shared websites like Buzzfeed or UpWorthy and add them to your blog posts.</li> 

                          <li>No matter what theme you are using you can add these shortcodes to get all the social share features you need to have viral blog posts.</li> 
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #20 End -->
      
      <!-- Huge Woth Section Start -->
      <div class="huge-area mt30 mt-md10">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="f-md-65 f-40 lh120 w700 white-clr">That's Huge Worth of</div>
                  <br>
                  <div class="f-md-60 f-40 lh120 w800 yellow-clr">$3300!</div>
               </div>
            </div>
         </div>
      </div>
      <!-- Huge Worth Section End -->
      <!-- text Area Start -->
      <div class="white-section pb0">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="f-md-36 f-25 lh140 w500">So what are you waiting for? You have a great opportunity <br class="hidden-xs"> ahead + <span class="w700 ">My 20 Bonus Products</span> are making it a <br class="hidden-xs"> <span class="w700 ">completely NO Brainer!!</span></div>
               </div>
            </div>
         </div>
      </div>
      <!-- text Area End -->
      <!-- CTA Button Section Start -->
      <div class="cta-btn-section">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center">
                  <a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab LegelSuites + My 20 Exclusive Bonuses</span> 
                  </a>
               </div>
               <div class="col-12 mt15 mt-md20" align="center">
                  <h3 class="f-md-28 f-20 w500 text-center black">My Exclusive Bonuses Are Expiring in...</h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 col-md-10 mx-auto col-12 text-center">
                  <div class="countdown counter-black">
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                     <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                     <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->
      <div class="footer-section" style="font-size: 14px;">
         <div class="container " style="font-size: 14px;">
            <div class="row" style="font-size: 14px;">
               <div class="col-12 text-center" style="font-size: 14px;">
               <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 495.7 104.2" style="enable-background:new 0 0 495.7 104.2; max-height:60px;" xml:space="preserve">
                              <style type="text/css">
                                 .st0{fill:#FFFFFF;}
                                 .st1{fill:#FFA829;}
                                 .st2{fill:url(#SVGID_1_);}
                                 .st3{fill:url(#SVGID_00000132794433514585357610000014266780508499585408_);}
                              </style>
                              <g>
                                 <path class="st0" d="M96.9,82.6c-7.5,2.8-15.7,4.4-24.2,4.8c0.2,0.7,0,1.4-0.5,1.9c-3.2,3.4-6.4,6.1-9.9,8.2
                                    c-5.4,3.4-11.5,5.6-17.9,6.5l-0.3,0.1c-0.2,0-0.3,0.1-0.5,0.1c-0.2,0-0.3,0-0.5-0.1l-0.3-0.1c-6.4-0.9-12.4-3.1-17.9-6.6
                                    C11.6,89,3.4,74.5,3.2,58.7c0-0.9,0.6-1.7,1.4-2c0,0,0.1,0,0.1,0C1.2,50.6-0.5,43.9,0.1,37C1.8,19.4,18.3,4.9,42.3,0
                                    c1-0.2,2,0.4,2.3,1.4c0.3,1-0.2,2.1-1.2,2.4c-5.4,2-10.2,4.6-14.2,7.7l14-3.6c0.3-0.1,0.7-0.1,1,0l38.4,9.9c0.9,0.2,1.5,1,1.5,1.9
                                    v38.1c0,7.9-2,15.6-5.7,22.4c5.9,0.1,11.7-0.4,17.4-1.5c1-0.2,2,0.4,2.3,1.4C98.4,81.2,97.9,82.2,96.9,82.6z"></path>
                                 <g>
                                    <path class="st0" d="M291.8,67.6l2.5-5.7c3.2,2.5,8.2,4.3,13.1,4.3c6.2,0,8.8-2.2,8.8-5.1c0-8.6-23.6-3-23.6-17.7
                                       c0-6.4,5.1-11.8,16-11.8c4.8,0,9.7,1.3,13.2,3.5l-2.3,5.7c-3.6-2.1-7.5-3.1-10.9-3.1c-6.1,0-8.6,2.4-8.6,5.4
                                       c0,8.4,23.5,3,23.5,17.5c0,6.3-5.1,11.8-16,11.8C301.2,72.4,295,70.4,291.8,67.6z"></path>
                                    <path class="st0" d="M464,67.6l2.5-5.7c3.2,2.5,8.2,4.3,13.1,4.3c6.2,0,8.8-2.2,8.8-5.1c0-8.6-23.6-3-23.6-17.7
                                       c0-6.4,5.1-11.8,16-11.8c4.8,0,9.7,1.3,13.2,3.5l-2.3,5.7c-3.6-2.1-7.5-3.1-10.9-3.1c-6.1,0-8.6,2.4-8.6,5.4
                                       c0,8.4,23.5,3,23.5,17.5c0,6.3-5.1,11.8-16,11.8C473.4,72.4,467.2,70.4,464,67.6z"></path>
                                    <path class="st0" d="M330.3,54.5V32.2h7.3v22.1c0,8.1,3.7,11.7,10.1,11.7s10-3.5,10-11.7V32.2h7.2v22.3c0,11.6-6.5,17.8-17.3,17.8
                                       C336.9,72.4,330.3,66.1,330.3,54.5z"></path>
                                    <path class="st0" d="M375.3,32.2h7.3v39.6h-7.3V32.2z"></path>
                                    <path class="st0" d="M401.4,38.4h-13.1v-6.2h33.6v6.2h-13.1v33.4h-7.3V38.4H401.4z"></path>
                                    <path class="st0" d="M457.2,65.6v6.2h-29.7V32.2h28.9v6.2h-21.6v10.3H454v6.1h-19.1v10.9L457.2,65.6L457.2,65.6z"></path>
                                 </g>
                                 <g>
                                    <path class="st1" d="M96.2,80.7c-10.5,3.9-23,5.6-36.1,4.4C25.9,81.9,0,60.5,2.1,37.2C3.7,20,20.3,6.5,42.7,2
                                       C26.1,8.2,14.6,19.8,13.2,34.1C11.1,57.4,37,78.8,71.1,81.9C79.9,82.7,88.4,82.2,96.2,80.7z"></path>
                                    <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="20.0931" y1="1069.2449" x2="47.1737" y2="1042.1643" gradientTransform="matrix(1 0 0 1 0 -978)">
                                       <stop offset="0" style="stop-color:#0055FF"></stop>
                                       <stop offset="1" style="stop-color:#008CFF"></stop>
                                    </linearGradient>
                                    <path class="st2" d="M68.3,87.8c-2,0-3.9-0.1-5.9-0.2c-1.3,1.1-2.7,2.1-4.2,3.1c-4.5,2.8-9.3,4.6-14.5,5.4
                                       c-5.1-0.8-10-2.6-14.5-5.4c-8.7-5.4-14.6-13.8-17-23.4c-2.8-2.7-5.1-5.6-7-8.6c0.2,15.2,8,29,20.9,37.1c5.3,3.3,11,5.4,17.2,6.3
                                       l0.4,0.1l0.4-0.1c6.1-0.9,11.9-3,17.2-6.3c3.6-2.2,6.7-4.9,9.5-7.9C70,87.8,69.1,87.8,68.3,87.8z"></path>
                                    <linearGradient id="SVGID_00000129182319601907477880000006569859738938224028_" gradientUnits="userSpaceOnUse" x1="33.5187" y1="1042.8949" x2="80.3658" y2="996.0477" gradientTransform="matrix(1 0 0 1 0 -978)">
                                       <stop offset="0" style="stop-color:#0055FF"></stop>
                                       <stop offset="1" style="stop-color:#008CFF"></stop>
                                    </linearGradient>
                                    <path style="fill:url(#SVGID_00000129182319601907477880000006569859738938224028_);" d="M43.7,9.9l-17.5,4.5
                                       c-2,1.9-21.6,31.5,10.2,51.6l2.2-13c-2.2-1.6-3.6-4.2-3.6-7c0-4.8,3.9-8.7,8.7-8.7s8.7,3.9,8.7,8.7c0,2.9-1.4,5.4-3.6,7l3.4,20.5
                                       C59,76,67,78.1,76.4,79.8c3.7-6.5,5.7-14,5.7-21.9V19.8L43.7,9.9z M40.3,17.5l-0.2,2.2l-1.6-1.4l-2.1,0.5l0.9-2l-1.1-1.9l2.2,0.2
                                       l1.4-1.6l0.4,2.1l2,0.8L40.3,17.5z M59.9,22.7l-0.2,2.5l-1.8-1.7l-2.4,0.6l1-2.2l-1.3-2.1l2.5,0.3l1.6-1.9l0.5,2.4l2.3,1
                                       L59.9,22.7z M74.2,71.3L74,74.6l-2.5-2.3l-3.3,0.8l1.4-3.1l-1.8-2.9l3.4,0.4l2.2-2.5l0.7,3.3l3.1,1.3L74.2,71.3z M74.8,51.1
                                       l-0.2,3.1l-2.3-2.1l-3,0.7l1.3-2.8L69,47.4l3.1,0.3l2-2.4l0.6,3l2.9,1.2L74.8,51.1z M75.4,31l-0.2,2.8l-2.1-1.9l-2.7,0.6l1.1-2.5
                                       l-1.4-2.4l2.7,0.3l1.8-2.1l0.6,2.7l2.6,1.1L75.4,31z"></path>
                                 </g>
                                 <g>
                                    <path class="st0" d="M105.1,32.2h11.2v30.7h18.9v8.9h-30.1V32.2z"></path>
                                    <path class="st0" d="M171.5,63.1v8.7h-31.8V32.2h31.1v8.7h-20v6.7h17.6V56h-17.6v7.2L171.5,63.1L171.5,63.1z"></path>
                                    <path class="st0" d="M204.2,51.2h9.9v16.4c-4.6,3.3-10.9,5-16.6,5c-12.6,0-21.8-8.6-21.8-20.6s9.2-20.6,22.1-20.6
                                       c7.4,0,13.4,2.5,17.3,7.2L208,45c-2.7-3-5.8-4.4-9.6-4.4c-6.8,0-11.3,4.5-11.3,11.3c0,6.7,4.5,11.3,11.2,11.3
                                       c2.1,0,4.1-0.4,6.1-1.3L204.2,51.2L204.2,51.2z"></path>
                                    <path class="st0" d="M253,63.1v8.7h-31.8V32.2h31.1v8.7h-20v6.7h17.6V56h-17.6v7.2L253,63.1L253,63.1z"></path>
                                    <path class="st0" d="M259.2,32.2h11.2v30.7h18.9v8.9h-30.1V32.2z"></path>
                                 </g>
                              </g>
                           </svg>
                  <div class="f-16 f-md-18 w300 mt20 lh150 white-clr text-center" style="font-size: 16px;">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40" style="font-size: 14px;">
                  <div class="f-16 f-md-18 w300 lh150 white-clr text-xs-center" style="font-size: 16px;">Copyright © LegelSuites 2022</div>
                  <ul class="footer-ul f-16 f-md-18 w300 white-clr text-center text-md-right">
                     <li style="font-size: 16px;"><a href="https://support.oppyo.com/hc/en-us" class="white-clr t-decoration-none" style="font-size: 16px;">Contact</a> <span class="px5" style="font-size: 16px;">|</span> </li>
                     <li style="font-size: 16px;"><a href="http://legelsuites.co/legal/privacy-policy.html" class="white-clr t-decoration-none" style="font-size: 16px;">Privacy</a> <span class="px5" style="font-size: 16px;">|</span> </li>
                     <li style="font-size: 16px;"><a href="http://legelsuites.co/legal/terms-of-service.html" class="white-clr t-decoration-none" style="font-size: 16px;">T&amp;C</a> <span class="px5" style="font-size: 16px;">|</span> </li>
                     <li style="font-size: 16px;"><a href="http://legelsuites.co/legal/disclaimer.html" class="white-clr t-decoration-none" style="font-size: 16px;">Disclaimer</a> <span class="px5" style="font-size: 16px;">|</span> </li>
                     <li style="font-size: 16px;"><a href="http://legelsuites.co/legal/gdpr.html" class="white-clr t-decoration-none" style="font-size: 16px;">GDPR</a> <span class="px5" style="font-size: 16px;">|</span> </li>
                     <li style="font-size: 16px;"><a href="http://legelsuites.co/legal/dmca.html" class="white-clr t-decoration-none" style="font-size: 16px;">DMCA</a> <span class="px5" style="font-size: 16px;">|</span> </li>
                     <li style="font-size: 16px;"><a href="http://legelsuites.co/legal/anti-spam.html" class="white-clr t-decoration-none" style="font-size: 16px;">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!-- timer --->
      <?php
         if ($now < $exp_date) {
         ?>
      <script type="text/javascript">
         // Count down milliseconds = server_end - server_now = client_end - client_now
         var server_end = <?php echo $exp_date; ?> * 1000;
         var server_now = <?php echo time(); ?> * 1000;
         var client_now = new Date().getTime();
         var end = server_end - server_now + client_now; // this is the real end time
         
         var noob = $('.countdown').length;
         
         var _second = 1000;
         var _minute = _second * 60;
         var _hour = _minute * 60;
         var _day = _hour * 24
         var timer;
         
         function showRemaining() {
         var now = new Date();
         var distance = end - now;
         if (distance < 0) {
         	clearInterval(timer);
         	document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
         	return;
         }
         
         var days = Math.floor(distance / _day);
         var hours = Math.floor((distance % _day) / _hour);
         var minutes = Math.floor((distance % _hour) / _minute);
         var seconds = Math.floor((distance % _minute) / _second);
         if (days < 10) {
         	days = "0" + days;
         }
         if (hours < 10) {
         	hours = "0" + hours;
         }
         if (minutes < 10) {
         	minutes = "0" + minutes;
         }
         if (seconds < 10) {
         	seconds = "0" + seconds;
         }
         var i;
         var countdown = document.getElementsByClassName('countdown');
         for (i = 0; i < noob; i++) {
         	countdown[i].innerHTML = '';
         
         	if (days) {
         		countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + days + '</span><span class="f-14 f-md-18 smmltd">Days</span> </div>';
         	}
         
         	countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + hours + '</span><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>';
         
         	countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">' + minutes + '</span><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>';
         
         	countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">' + seconds + '</span><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>';
         }
         
         }
         timer = setInterval(showRemaining, 1000);
         	
      </script>
      <?php
         } else {
         echo "Times Up";
         }
         ?>
      <!--- timer end-->
   </body>
</html>
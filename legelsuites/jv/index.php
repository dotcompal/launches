<html>
   <head>
      <title>JV Page - LegelSuites JV Invite</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=9">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
      <meta name="title" content="LegelSuites | JV">
      <meta name="description" content="Breakthrough Technology That Scans Websites for Legal Flaws & Generate Fixes to Save Website Owners from Law Suits in 60 Sec Flat!">
      <meta name="keywords" content="LegelSuites">
      <meta property="og:image" content="https://www.legelsuites.co/jv/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Dr. Amit Pareek">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="LegelSuites | JV">
      <meta property="og:description" content="Breakthrough Technology That Scans Websites for Legal Flaws & Generate Fixes to Save Website Owners from Law Suits in 60 Sec Flat!">
      <meta property="og:image" content="https://www.legelsuites.co/jv/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="LegelSuites | JV">
      <meta property="twitter:description" content="Breakthrough Technology That Scans Websites for Legal Flaws & Generate Fixes to Save Website Owners from Law Suits in 60 Sec Flat!">
      <meta property="twitter:image" content="https://www.legelsuites.co/jv/thumbnail.png">
      <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&family=Lexend:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
      <!-- required -->
      <link rel="stylesheet" href="https://cdn.oppyo.com/launches/yoseller/common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style.css" type="text/css">
      <link rel="stylesheet" href="assets/css/timer.css" type="text/css">
      <script src="https://cdn.oppyo.com/launches/yoseller/common_assets/js/jquery.min.js"></script>
      <script type="text/javascript" src="https://www.legelsuites.com/widget/ada/oppyo-GTtd-wozS-Rm7H-DqGe" crossorigin="anonymous" embed-id="oppyo-GTtd-wozS-Rm7H-DqGe"></script>
      <script type="text/javascript" src="https://www.legelsuites.com/load-gdpr/5d9132b263ea11ed" crossorigin="anonymous" embed-id="5d9132b263ea11ed"></script>
   </head>
  <body>
      <!-- New Timer  Start-->
      <?php
         $date = 'Nov 17 2022 11:00 AM EST';
         $exp_date = strtotime($date);
         $now = time();  
         /*
         
         $date = date('F d Y g:i:s A eO');
         $rand_time_add = rand(700, 1200);
         $exp_date = strtotime($date) + $rand_time_add;
         $now = time();*/
         
         if ($now < $exp_date) {
         ?>
      <?php
         } else {
          echo "Times Up";
         }
         ?>
      <!-- New Timer End -->
      <!-- Header Section Start -->
      <div class="header-section">
         <div class="container">
            <div class="row">
               <div class="col-12 d-flex align-items-center justify-content-center justify-content-md-between flex-wrap flex-md-nowrap">
               <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                              viewBox="0 0 495.7 104.2" style="enable-background:new 0 0 495.7 104.2; max-height:60px;" xml:space="preserve">
                              <style type="text/css">
                                 .st0{fill:#FFFFFF;}
                                 .st1{fill:#FFA829;}
                                 .st2{fill:url(#SVGID_1_);}
                                 .st3{fill:url(#SVGID_00000132794433514585357610000014266780508499585408_);}
                              </style>
                              <g>
                                 <path class="st0" d="M96.9,82.6c-7.5,2.8-15.7,4.4-24.2,4.8c0.2,0.7,0,1.4-0.5,1.9c-3.2,3.4-6.4,6.1-9.9,8.2
                                    c-5.4,3.4-11.5,5.6-17.9,6.5l-0.3,0.1c-0.2,0-0.3,0.1-0.5,0.1c-0.2,0-0.3,0-0.5-0.1l-0.3-0.1c-6.4-0.9-12.4-3.1-17.9-6.6
                                    C11.6,89,3.4,74.5,3.2,58.7c0-0.9,0.6-1.7,1.4-2c0,0,0.1,0,0.1,0C1.2,50.6-0.5,43.9,0.1,37C1.8,19.4,18.3,4.9,42.3,0
                                    c1-0.2,2,0.4,2.3,1.4c0.3,1-0.2,2.1-1.2,2.4c-5.4,2-10.2,4.6-14.2,7.7l14-3.6c0.3-0.1,0.7-0.1,1,0l38.4,9.9c0.9,0.2,1.5,1,1.5,1.9
                                    v38.1c0,7.9-2,15.6-5.7,22.4c5.9,0.1,11.7-0.4,17.4-1.5c1-0.2,2,0.4,2.3,1.4C98.4,81.2,97.9,82.2,96.9,82.6z"/>
                                 <g>
                                    <path class="st0" d="M291.8,67.6l2.5-5.7c3.2,2.5,8.2,4.3,13.1,4.3c6.2,0,8.8-2.2,8.8-5.1c0-8.6-23.6-3-23.6-17.7
                                       c0-6.4,5.1-11.8,16-11.8c4.8,0,9.7,1.3,13.2,3.5l-2.3,5.7c-3.6-2.1-7.5-3.1-10.9-3.1c-6.1,0-8.6,2.4-8.6,5.4
                                       c0,8.4,23.5,3,23.5,17.5c0,6.3-5.1,11.8-16,11.8C301.2,72.4,295,70.4,291.8,67.6z"/>
                                    <path class="st0" d="M464,67.6l2.5-5.7c3.2,2.5,8.2,4.3,13.1,4.3c6.2,0,8.8-2.2,8.8-5.1c0-8.6-23.6-3-23.6-17.7
                                       c0-6.4,5.1-11.8,16-11.8c4.8,0,9.7,1.3,13.2,3.5l-2.3,5.7c-3.6-2.1-7.5-3.1-10.9-3.1c-6.1,0-8.6,2.4-8.6,5.4
                                       c0,8.4,23.5,3,23.5,17.5c0,6.3-5.1,11.8-16,11.8C473.4,72.4,467.2,70.4,464,67.6z"/>
                                    <path class="st0" d="M330.3,54.5V32.2h7.3v22.1c0,8.1,3.7,11.7,10.1,11.7s10-3.5,10-11.7V32.2h7.2v22.3c0,11.6-6.5,17.8-17.3,17.8
                                       C336.9,72.4,330.3,66.1,330.3,54.5z"/>
                                    <path class="st0" d="M375.3,32.2h7.3v39.6h-7.3V32.2z"/>
                                    <path class="st0" d="M401.4,38.4h-13.1v-6.2h33.6v6.2h-13.1v33.4h-7.3V38.4H401.4z"/>
                                    <path class="st0" d="M457.2,65.6v6.2h-29.7V32.2h28.9v6.2h-21.6v10.3H454v6.1h-19.1v10.9L457.2,65.6L457.2,65.6z"/>
                                 </g>
                                 <g>
                                    <path class="st1" d="M96.2,80.7c-10.5,3.9-23,5.6-36.1,4.4C25.9,81.9,0,60.5,2.1,37.2C3.7,20,20.3,6.5,42.7,2
                                       C26.1,8.2,14.6,19.8,13.2,34.1C11.1,57.4,37,78.8,71.1,81.9C79.9,82.7,88.4,82.2,96.2,80.7z"/>
                                    <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="20.0931" y1="1069.2449" x2="47.1737" y2="1042.1643" gradientTransform="matrix(1 0 0 1 0 -978)">
                                       <stop  offset="0" style="stop-color:#0055FF"/>
                                       <stop  offset="1" style="stop-color:#008CFF"/>
                                    </linearGradient>
                                    <path class="st2" d="M68.3,87.8c-2,0-3.9-0.1-5.9-0.2c-1.3,1.1-2.7,2.1-4.2,3.1c-4.5,2.8-9.3,4.6-14.5,5.4
                                       c-5.1-0.8-10-2.6-14.5-5.4c-8.7-5.4-14.6-13.8-17-23.4c-2.8-2.7-5.1-5.6-7-8.6c0.2,15.2,8,29,20.9,37.1c5.3,3.3,11,5.4,17.2,6.3
                                       l0.4,0.1l0.4-0.1c6.1-0.9,11.9-3,17.2-6.3c3.6-2.2,6.7-4.9,9.5-7.9C70,87.8,69.1,87.8,68.3,87.8z"/>
                                    <linearGradient id="SVGID_00000129182319601907477880000006569859738938224028_" gradientUnits="userSpaceOnUse" x1="33.5187" y1="1042.8949" x2="80.3658" y2="996.0477" gradientTransform="matrix(1 0 0 1 0 -978)">
                                       <stop  offset="0" style="stop-color:#0055FF"/>
                                       <stop  offset="1" style="stop-color:#008CFF"/>
                                    </linearGradient>
                                    <path style="fill:url(#SVGID_00000129182319601907477880000006569859738938224028_);" d="M43.7,9.9l-17.5,4.5
                                       c-2,1.9-21.6,31.5,10.2,51.6l2.2-13c-2.2-1.6-3.6-4.2-3.6-7c0-4.8,3.9-8.7,8.7-8.7s8.7,3.9,8.7,8.7c0,2.9-1.4,5.4-3.6,7l3.4,20.5
                                       C59,76,67,78.1,76.4,79.8c3.7-6.5,5.7-14,5.7-21.9V19.8L43.7,9.9z M40.3,17.5l-0.2,2.2l-1.6-1.4l-2.1,0.5l0.9-2l-1.1-1.9l2.2,0.2
                                       l1.4-1.6l0.4,2.1l2,0.8L40.3,17.5z M59.9,22.7l-0.2,2.5l-1.8-1.7l-2.4,0.6l1-2.2l-1.3-2.1l2.5,0.3l1.6-1.9l0.5,2.4l2.3,1
                                       L59.9,22.7z M74.2,71.3L74,74.6l-2.5-2.3l-3.3,0.8l1.4-3.1l-1.8-2.9l3.4,0.4l2.2-2.5l0.7,3.3l3.1,1.3L74.2,71.3z M74.8,51.1
                                       l-0.2,3.1l-2.3-2.1l-3,0.7l1.3-2.8L69,47.4l3.1,0.3l2-2.4l0.6,3l2.9,1.2L74.8,51.1z M75.4,31l-0.2,2.8l-2.1-1.9l-2.7,0.6l1.1-2.5
                                       l-1.4-2.4l2.7,0.3l1.8-2.1l0.6,2.7l2.6,1.1L75.4,31z"/>
                                 </g>
                                 <g>
                                    <path class="st0" d="M105.1,32.2h11.2v30.7h18.9v8.9h-30.1V32.2z"/>
                                    <path class="st0" d="M171.5,63.1v8.7h-31.8V32.2h31.1v8.7h-20v6.7h17.6V56h-17.6v7.2L171.5,63.1L171.5,63.1z"/>
                                    <path class="st0" d="M204.2,51.2h9.9v16.4c-4.6,3.3-10.9,5-16.6,5c-12.6,0-21.8-8.6-21.8-20.6s9.2-20.6,22.1-20.6
                                       c7.4,0,13.4,2.5,17.3,7.2L208,45c-2.7-3-5.8-4.4-9.6-4.4c-6.8,0-11.3,4.5-11.3,11.3c0,6.7,4.5,11.3,11.2,11.3
                                       c2.1,0,4.1-0.4,6.1-1.3L204.2,51.2L204.2,51.2z"/>
                                    <path class="st0" d="M253,63.1v8.7h-31.8V32.2h31.1v8.7h-20v6.7h17.6V56h-17.6v7.2L253,63.1L253,63.1z"/>
                                    <path class="st0" d="M259.2,32.2h11.2v30.7h18.9v8.9h-30.1V32.2z"/>
                                 </g>
                              </g>
                           </svg>
                  <div class="d-flex align-items-center flex-wrap justify-content-center justify-content-md-end text-center text-md-end mt20 mt-md0">
                     <ul class="leader-ul f-16 f-md-18">
                        <li>
                           <a href="https://docs.google.com/document/d/1WDKziCDpP1qQSxt_Yoo2WIRhCYnR0ZyH/edit?usp=sharing&ouid=117555941573669202344&rtpof=true&sd=true" target="_blank" class="active">JV Doc </a>
                        </li>
                        <li>
                           <a href="https://docs.google.com/document/d/1AJuSEYVTcwOQ10s1bBbLOVtXiZyef9_Q/edit?usp=sharing&ouid=117555941573669202344&rtpof=true&sd=true" target="_blank">Swipes & Bonuses </a>
                        </li>
                        <li>
                           <a href="#funnel">SP Preview</a>
                        </li>
                     </ul>
                     <a href="https://www.jvzoo.com/affiliate/affiliateinfonew/index/388896" class="affiliate-link-btn ml-md15 mt25 mt-md0" target="_blank"> Grab Your Affiliate Link</a>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50">
                  <div class="header-wrap">
                     <div class="row">
                        <div class="col-12 text-center">
                           <div class="preheadline f-16 f-md-22 w400 white-clr lh140">
                              <div>Make Your Subscribers Fix Own Website & Kick Start  Simple 7 Minutes Agency Services</div>
                           </div>
                        </div>
                        <div class="col-12 mt-md30 mt20 f-md-45 f-26 w700 text-center white-clr lh140">
                           Analyse, Detects & Fix Deadly Legal Flaws <br class="d-none d-md-block"> on Your Own Website & For Clients Just by <br class="d-none d-md-block"> <span class="cyan-clr">Copy Pasting a Single Line of Code</span> 
                        </div>
                        <div class="col-12 mt20 mt-md30 text-center">
                           <div class="f-18 f-md-26 w500 text-center lh130 white-clr text-capitalize">               
                              Fixes Deadly Law issues Like ADA, GDPR, TOC & Much More | DFY Editable <br class="d-none d-md-block"> Agency Website to Quick Start | Agency License Included
                           </div>
                           <img src="assets/images/h-line.webp" alt="H-Line" class="mx-auto img-fluid d-none d-md-block mt15">
                        </div>
                     </div>
                     <div class="row align-items-center mt20 mt-md70">
                        <div class="col-12 col-md-7">
                           <img src="assets/images/product-image.webp" alt="Video Image" class="img-fluid mx-auto d-block">
                           <!-- <div style="padding-bottom: 56.25%;position: relative;"><iframe src="https://yoseller.dotcompal.com/video/embed/7s9ou23ndl" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div> -->
                        </div>
                        <div class="col-12 col-md-5 mt20 mt-md0">
                           <img src="assets/images/date.webp" alt="Date" class="img-fluid mx-auto d-block">
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="list-section">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-6">
                  <div class="f-16 f-md-18 lh140 w400">
                     <ul class="bonus-list pl0 m0">
                        <li>Your Subscribers Can Tap into Hot Website Agency Services to Make Huge Profits  </li>
                        <li>Scans Any Webstie with LegelSuites Analyzer to detect Legal Flaws on your Website like GDPR, ADA, Privacy Policy, T&C, etc</li>
                        <li>Help you Make Your Webite ADA Compliant by Copy Pasting Single Line of Code</li>
                        <li>Get Your Website GDPR & Privacy Laws Comply as per Internation Laws</li>
                        <li>Generate Cookies Policies & Cookies Widgets with inbuilt GDPR Tool</li>
                        <li>Get Tailor Made Legal Policies and Other Legal Documents for your's and client's Websites</li>
                     </ul>
                  </div>
               </div>
               <div class="col-12 col-md-6">
                  <div class="f-16 f-md-18 lh140 w400">
                     <ul class="bonus-list pl0 m0">
                        <li>Copy Past Single line of code to make any of your business website 100% Compliance for International Laws</li>
                        <li>Built-in Agency Website Builder to Create Stunning Agency Website for your Self and for Clients</li>
                        <li>Website Widge to add on your Agency Website to Offer Legal & Compliance Test to quicly generate leads</li>
                        <li>Agency License Included to Serve Your Clients and Make Huge Profits</li>
                        <li>Step-by-Step Training Included to get your Subscribers get Started Today</li>
                     </ul>
                  </div>
               </div>
            </div>
            <div class="row mt30 mt-md80">
                  <div class="col-12 col-md-12">
                     <div class="auto_responder_form inp-phold formbg col-12">
                        <div class="f-md-45 f-26 lh140 w800 text-center white-clr">
                           Subscribe To Our JV List
                        </div>
                        <div class="f-20 f-md-24 w400 text-center mt10 white-clr">
                           and Be The First to Know Our Special Contest, Events and Discounts                         
                        </div>
                        <div class="col-12 col-md-8 mx-auto mt20 mt-md30">
                           <div class="white-form-box">
                              <form method="post" class="af-form-wrapper mb-md5" accept-charset="UTF-8" action="https://www.aweber.com/scripts/addlead.pl"  >
                                 <div style="display: none;">
                                    <input type="hidden" name="meta_web_form_id" value="575032983" />
                                    <input type="hidden" name="meta_split_id" value="" />
                                    <input type="hidden" name="listname" value="awlist6381803" />
                                    <input type="hidden" name="redirect" value="https://www.aweber.com/thankyou.htm?m=default" id="redirect_7b92e88171d07e4175363bf3dbdbc1db" />
                                    <input type="hidden" name="meta_adtracking" value="My_Web_Form" />
                                    <input type="hidden" name="meta_message" value="1" />
                                    <input type="hidden" name="meta_required" value="name,email" />
                                    <input type="hidden" name="meta_tooltip" value="" />
                                 </div>
                                 <div id="af-form-575032983" class="af-form">
                                    <div id="af-body-575032983" class="af-body af-standards row justify-content-center">
                                       <!-- <div class="af-element col-md-4">
                                          <label class="previewLabel" for="awf_field-114790966" style="display:none;">Name: </label>
                                          <div class="af-textWrap mb15 mb-md15 input-type">
                                             <input id="awf_field-114790966" class="frm-ctr-popup form-control input-field" type="text" name="name" placeholder="Your Name" class="text" value=""  onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="500" />
                                          </div>
                                          <div class="af-clear"></div>
                                          </div> -->
                                       <div class="af-element mb15 mb-md0  col-md-6">
                                          <label class="previewLabel" for="awf_field-114955111" style="display:none;">Email: </label>
                                          <div class="af-textWrap input-type">
                                             <input class="text frm-ctr-popup form-control input-field" id="awf_field-114955111" type="text" name="email" placeholder="Your Email" value=""  tabindex="501" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " />
                                          </div>
                                          <div class="af-clear"></div>
                                       </div>
                                       <div class="af-element buttonContainer button-type form-btn white-clr col-md-6">
                                          <input name="submit" class="submit f-18 f-md-20 white-clr center-block" type="submit" value="Subscribe For JV Updates" tabindex="502" />
                                          <div class="af-clear"></div>
                                       </div>
                                    </div>
                                 </div>
                                 <div style="display: none;"><img src="https://forms.aweber.com/form/displays.htm?id=rOysDMxMnBzM" alt="" /></div>
                              </form>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row mt20 mt-md80">
               <div class="col-12 col-md-12">
                  <div class="f-26 f-md-45 w800 text-center lh140">
                     Grab Your JVZoo Affiliate Link to Jump on <br class="d-none d-md-block">
                     This <span class="blue-clr1">Amazing Product Launch</span> 
                  </div>
                  <div class="relative">
                     <img src="assets/images/curve-line.webp" alt="Curve Line" class="mx-auto d-none d-md-block img-fluid" style="position:absolute; right:215px;">
                  </div>
               </div>
               <div class="col-md-12 mx-auto mt20 mt-md50">
                  <div class="row justify-content-center align-items-center">
                     <div class="col-12 col-md-3">
                        <img src="assets/images/jvzoo.webp" class="img-fluid d-block mx-auto" alt="Jvzoo"/>
                     </div>
                     <div class="col-12 col-md-4 affiliate-btn  mt-md19 mt15">
                        <a href="https://www.jvzoo.com/affiliate/affiliateinfonew/index/388896" class="f-18 f-md-20 w600 mx-auto" target="_blank">Request Affiliate Link</a>
                     </div>
                  </div>
               </div>
               </div>
         </div>
         <!-- Header Section End -->
      </div>
      <!-- Header Section End -->
      <!-- exciting-launch -->
      <div class="exciting-launch">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="exciting-inner-sec">
                     <div class="row">
                        <div class="col-12">
                           <div class="f-md-44 f-28 black-clr text-center w700 lh140">
                              This Exciting Launch Event Is Divided <br class="d-none d-md-block"> <span class="blue-clr1">INFO 2 PHASES</span> 
                              <img src="assets/images/exciting-curve.webp" alt="Exciting Curve" class="mx-auto d-block img-fluid">
                           </div>
                        </div>
                        <div class="col-12 col-md-12 mx-auto mt-md90 mt30">
                           <div class="row gx-8">
                              <div class="col-md-6 col-12 relative">
                                 <div class="exciting-launch-left">
                                    <div class="f-md-32 f-26 w600 lh140 text-center white-clr exciting-right-head">
                                       Pre-Launch with webinar
                                    </div>
                                    <div class="f-20 f-md-24 w400 lh140 black-clr mt15 mt-md30 text-center">
                                       <span class="w700">15th Nov'22</span> 10:00 AM EST to<br>
                                       <span class="w700">17th Nov'22</span> 10:00 AM EST
                                    </div>
                                    <div class="blue-clr1 f-20 f-md-24 w600 t15 mt15 mt-md20">
                                       To Make You Max Commissions
                                    </div>
                                    <ul class="exciting-list f-18 w400 lh140 pl0 black-clr mt20 mt-md30">
                                       <li>All Leads Are Hardcoded</li>
                                       <li>Exciting $2000 Pre-Launch Contest</li>
                                       <li>We'll Re-Market Your Leads Heavily</li>
                                       <li>Pitch Bundle Offer on webinars.</li>
                                    </ul>
                                 </div>
                                 <img src="assets/images/connect.webp" alt="Connect" class="d-none d-md-block" style="position:absolute; right:-80px; top:190px; z-index:1;">
                              </div>
                              <div class="col-md-6 col-12 mt-md100 mt60 relative">
                                 <div class="exciting-launch-right">
                                    <div class="f-md-32 f-26 w600 lh140 text-center white-clr exciting-right-head">
                                       7 Days Launch Event
                                    </div>
                                    <div class="f-20 f-md-24 w400 lh140 white-clr mt15 mt-md30 text-center">
                                       <span class="w700 orange-clr">Cart Open 17th Nov</span> at 11 AM EST<br>
                                       <span class="w700 orange-clr">Cart Closes 23rd Nov</span> at 11:59 PM EST
                                    </div>
                                    <ul class="exciting-list f-18 w400 lh140 pl0 white-clr mt20 mt-md30">
                                       <li> Big Opening Contest & Bundle Offer </li>
                                       <li> High in Demand Product with Top Conversion </li>
                                       <li> Deep Funnel to Make You Double-Digit EPCs</li>
                                       <li> Earn up to $348/Sale</li>
                                       <li> Huge $10K JV Prizes</li>
                                       <li> We'll Re-Market Your Visitors Heavily</li>
                                    </ul>
                                 </div>
                                 <img src="assets/images/business-startup.webp" alt="Bussiness StartUp" class="" style="position:absolute; top:-55px; left:-33px;">
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- exciting-launch end -->
      <div class="hello-awesome">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="awesome-feature-shape">
                     <div class="row">
                        <div class="col-12 text-center">
                           <div class="f-md-45 f-26 lh140 w700 white-clr hello-headling">
                              Hello Awesome JV’s
                           </div>
                        </div>
                        <div class="col-12 mt20 mt-md50">
                           <div class="row align-items-center">
                              <div class="col-12 col-md-6">
                                 <div class="f-18 lh140 w400 black-clr">
                                 It's Dr. Amit Pareek (Techpreneur) along with my Partner Atul Pareek (Entrepreneur & Product Creator) .
                                    <br><br>
                                    We have delivered 25+ 6-Figure Blockbuster Launches, Sold Software Products of Over $9 Million, and paid over $4.5 Million in commission to our Affiliates.
                                    <br><br>
                                    With the combined experience of 25+ years, we are coming back with another Top Notch and High in Demand product that will provide a complete solution for all your cloud storage, media content delivery, and video hosting needs under one dashboard.
                                    <br><br>
                                    Check out the incredible features of this amazing technology that will blow away your mind. And we guarantee that this offer will convert to Hot Cakes starting from 17th November'22 at 11:00 AM EST! Get Ready!!
                                 </div>
                              </div>
                              <div class="col-md-6 col-12 text-center mt20 mt-md0">
                                 <div class="row">
                                    <div class="col-12"> <img src="assets/images/amit-pareek.webp" class="img-fluid d-block mx-auto"></div>
                                    <div class="col-12 col-md-6 mt20 mt-md30">                                      
                                       <img src="assets/images/atul-parrek.webp" class="img-fluid d-block mx-auto">
                                    </div>
                                    <!-- <div class="col-12 col-md-6 mt20 mt-md30">
                                       <img src="assets/images/achal-goswami.webp" class="img-fluid d-block mx-auto">
                                    </div> -->
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 text-center">
                  <div class="awesome-shape text-start">
                     <div class="row gx-0">
                        <div class="col-12 f-md-24 f-20 lh140 w600 text-center">
                           Also, here are some stats from our previous launches:
                        </div>
                        <div class="col-12 col-md-6 f-18 lh140 w400 mt20">
                           <ul class="awesome-list">
                              <li>Over 100 Pick of The Day Awards</li>
                              <li>Over $4Mn In Affiliate Sales for Partners</li>
                           </ul>
                        </div>
                        <div class="col-12 col-md-6 f-18 lh140 w400 mt20">
                           <ul class="awesome-list">
                              <li>Top 10 Affiliate & Seller (High Performance Leader)</li>
                              <li>Always in Top-10 of JVZoo Top Sellers</li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- next-generation -->
      <div class="proudly-sec" id="product">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="proudly-inner-sec relative">
                     <div class="row">
                        <div class="col-12 text-center">
                           <div class="f-36 f-md-50 w700 lh140 orange-clr intro-bg">
                              Presenting…
                           </div>
                        </div>
                        <div class="col-12 mt-md0 mt20 text-center">
                           <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                              viewBox="0 0 495.7 104.2" style="enable-background:new 0 0 495.7 104.2; max-height:120px;" xml:space="preserve">
                              <style type="text/css">
                                 .st0{fill:#FFFFFF;}
                                 .st1{fill:#FFA829;}
                                 .st2{fill:url(#SVGID_1_);}
                                 .st3{fill:url(#SVGID_00000132794433514585357610000014266780508499585408_);}
                              </style>
                              <g>
                                 <path class="st0" d="M96.9,82.6c-7.5,2.8-15.7,4.4-24.2,4.8c0.2,0.7,0,1.4-0.5,1.9c-3.2,3.4-6.4,6.1-9.9,8.2
                                    c-5.4,3.4-11.5,5.6-17.9,6.5l-0.3,0.1c-0.2,0-0.3,0.1-0.5,0.1c-0.2,0-0.3,0-0.5-0.1l-0.3-0.1c-6.4-0.9-12.4-3.1-17.9-6.6
                                    C11.6,89,3.4,74.5,3.2,58.7c0-0.9,0.6-1.7,1.4-2c0,0,0.1,0,0.1,0C1.2,50.6-0.5,43.9,0.1,37C1.8,19.4,18.3,4.9,42.3,0
                                    c1-0.2,2,0.4,2.3,1.4c0.3,1-0.2,2.1-1.2,2.4c-5.4,2-10.2,4.6-14.2,7.7l14-3.6c0.3-0.1,0.7-0.1,1,0l38.4,9.9c0.9,0.2,1.5,1,1.5,1.9
                                    v38.1c0,7.9-2,15.6-5.7,22.4c5.9,0.1,11.7-0.4,17.4-1.5c1-0.2,2,0.4,2.3,1.4C98.4,81.2,97.9,82.2,96.9,82.6z"/>
                                 <g>
                                    <path class="st0" d="M291.8,67.6l2.5-5.7c3.2,2.5,8.2,4.3,13.1,4.3c6.2,0,8.8-2.2,8.8-5.1c0-8.6-23.6-3-23.6-17.7
                                       c0-6.4,5.1-11.8,16-11.8c4.8,0,9.7,1.3,13.2,3.5l-2.3,5.7c-3.6-2.1-7.5-3.1-10.9-3.1c-6.1,0-8.6,2.4-8.6,5.4
                                       c0,8.4,23.5,3,23.5,17.5c0,6.3-5.1,11.8-16,11.8C301.2,72.4,295,70.4,291.8,67.6z"/>
                                    <path class="st0" d="M464,67.6l2.5-5.7c3.2,2.5,8.2,4.3,13.1,4.3c6.2,0,8.8-2.2,8.8-5.1c0-8.6-23.6-3-23.6-17.7
                                       c0-6.4,5.1-11.8,16-11.8c4.8,0,9.7,1.3,13.2,3.5l-2.3,5.7c-3.6-2.1-7.5-3.1-10.9-3.1c-6.1,0-8.6,2.4-8.6,5.4
                                       c0,8.4,23.5,3,23.5,17.5c0,6.3-5.1,11.8-16,11.8C473.4,72.4,467.2,70.4,464,67.6z"/>
                                    <path class="st0" d="M330.3,54.5V32.2h7.3v22.1c0,8.1,3.7,11.7,10.1,11.7s10-3.5,10-11.7V32.2h7.2v22.3c0,11.6-6.5,17.8-17.3,17.8
                                       C336.9,72.4,330.3,66.1,330.3,54.5z"/>
                                    <path class="st0" d="M375.3,32.2h7.3v39.6h-7.3V32.2z"/>
                                    <path class="st0" d="M401.4,38.4h-13.1v-6.2h33.6v6.2h-13.1v33.4h-7.3V38.4H401.4z"/>
                                    <path class="st0" d="M457.2,65.6v6.2h-29.7V32.2h28.9v6.2h-21.6v10.3H454v6.1h-19.1v10.9L457.2,65.6L457.2,65.6z"/>
                                 </g>
                                 <g>
                                    <path class="st1" d="M96.2,80.7c-10.5,3.9-23,5.6-36.1,4.4C25.9,81.9,0,60.5,2.1,37.2C3.7,20,20.3,6.5,42.7,2
                                       C26.1,8.2,14.6,19.8,13.2,34.1C11.1,57.4,37,78.8,71.1,81.9C79.9,82.7,88.4,82.2,96.2,80.7z"/>
                                    <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="20.0931" y1="1069.2449" x2="47.1737" y2="1042.1643" gradientTransform="matrix(1 0 0 1 0 -978)">
                                       <stop  offset="0" style="stop-color:#0055FF"/>
                                       <stop  offset="1" style="stop-color:#008CFF"/>
                                    </linearGradient>
                                    <path class="st2" d="M68.3,87.8c-2,0-3.9-0.1-5.9-0.2c-1.3,1.1-2.7,2.1-4.2,3.1c-4.5,2.8-9.3,4.6-14.5,5.4
                                       c-5.1-0.8-10-2.6-14.5-5.4c-8.7-5.4-14.6-13.8-17-23.4c-2.8-2.7-5.1-5.6-7-8.6c0.2,15.2,8,29,20.9,37.1c5.3,3.3,11,5.4,17.2,6.3
                                       l0.4,0.1l0.4-0.1c6.1-0.9,11.9-3,17.2-6.3c3.6-2.2,6.7-4.9,9.5-7.9C70,87.8,69.1,87.8,68.3,87.8z"/>
                                    <linearGradient id="SVGID_00000129182319601907477880000006569859738938224028_" gradientUnits="userSpaceOnUse" x1="33.5187" y1="1042.8949" x2="80.3658" y2="996.0477" gradientTransform="matrix(1 0 0 1 0 -978)">
                                       <stop  offset="0" style="stop-color:#0055FF"/>
                                       <stop  offset="1" style="stop-color:#008CFF"/>
                                    </linearGradient>
                                    <path style="fill:url(#SVGID_00000129182319601907477880000006569859738938224028_);" d="M43.7,9.9l-17.5,4.5
                                       c-2,1.9-21.6,31.5,10.2,51.6l2.2-13c-2.2-1.6-3.6-4.2-3.6-7c0-4.8,3.9-8.7,8.7-8.7s8.7,3.9,8.7,8.7c0,2.9-1.4,5.4-3.6,7l3.4,20.5
                                       C59,76,67,78.1,76.4,79.8c3.7-6.5,5.7-14,5.7-21.9V19.8L43.7,9.9z M40.3,17.5l-0.2,2.2l-1.6-1.4l-2.1,0.5l0.9-2l-1.1-1.9l2.2,0.2
                                       l1.4-1.6l0.4,2.1l2,0.8L40.3,17.5z M59.9,22.7l-0.2,2.5l-1.8-1.7l-2.4,0.6l1-2.2l-1.3-2.1l2.5,0.3l1.6-1.9l0.5,2.4l2.3,1
                                       L59.9,22.7z M74.2,71.3L74,74.6l-2.5-2.3l-3.3,0.8l1.4-3.1l-1.8-2.9l3.4,0.4l2.2-2.5l0.7,3.3l3.1,1.3L74.2,71.3z M74.8,51.1
                                       l-0.2,3.1l-2.3-2.1l-3,0.7l1.3-2.8L69,47.4l3.1,0.3l2-2.4l0.6,3l2.9,1.2L74.8,51.1z M75.4,31l-0.2,2.8l-2.1-1.9l-2.7,0.6l1.1-2.5
                                       l-1.4-2.4l2.7,0.3l1.8-2.1l0.6,2.7l2.6,1.1L75.4,31z"/>
                                 </g>
                                 <g>
                                    <path class="st0" d="M105.1,32.2h11.2v30.7h18.9v8.9h-30.1V32.2z"/>
                                    <path class="st0" d="M171.5,63.1v8.7h-31.8V32.2h31.1v8.7h-20v6.7h17.6V56h-17.6v7.2L171.5,63.1L171.5,63.1z"/>
                                    <path class="st0" d="M204.2,51.2h9.9v16.4c-4.6,3.3-10.9,5-16.6,5c-12.6,0-21.8-8.6-21.8-20.6s9.2-20.6,22.1-20.6
                                       c7.4,0,13.4,2.5,17.3,7.2L208,45c-2.7-3-5.8-4.4-9.6-4.4c-6.8,0-11.3,4.5-11.3,11.3c0,6.7,4.5,11.3,11.2,11.3
                                       c2.1,0,4.1-0.4,6.1-1.3L204.2,51.2L204.2,51.2z"/>
                                    <path class="st0" d="M253,63.1v8.7h-31.8V32.2h31.1v8.7h-20v6.7h17.6V56h-17.6v7.2L253,63.1L253,63.1z"/>
                                    <path class="st0" d="M259.2,32.2h11.2v30.7h18.9v8.9h-30.1V32.2z"/>
                                 </g>
                              </g>
                           </svg>
                        </div>
                        <div class="col-12 f-md-42 f-26 mt-md30 mt20 w700 text-center cyan-clr lh140">
                           All in One Platform to Start Serving Website Owners 
                           & Developers for the Website’s Legal Compliance as per
                           International Laws to save them from Law Suite.
                        </div>
                        <div class="f-22 f-md-26 w500 lh140 white-clr text-center mt20 mt-md40">
                           Generate Legal Compliance Like GDPR, ADA, Privacy Policy, and other Legal Documents with just a click of a Button in 60 Second Flat.
                        </div>
                     </div>
                     <div class="row mt30 mt-md50 align-items-center">
                        <div class="col-12 col-md-12">
                           <img src="assets/images/product-image.webp" class="img-fluid d-block mx-auto img-animation">
                        </div>
                     </div>
                     <img src="assets/images/proudly-icon1.webp" alt="Proudly Icon Right" class="d-none d-md-block"style="position:absolute; top:40px; right:64px;">
                     <img src="assets/images/proudly-icon2.webp" alt="Proudly Icon Right" class="d-none d-md-block"style="position:absolute; bottom:47px; left:51px;">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- next-generation end -->
      <!-- FEATURE LIST SECTION START -->
      <div class="feature-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-md-45 f-28 w700 text-center">
                     <span class="yellow-bg white-clr">LegelSuites is Packed with Super Powerful</span><br> Features That Makes It A Cut Above The Rest
                  </div>
               </div>
            </div>
            <div class="row align-items-center mt40 mt-md100">
               <div class="col-md-6 col-12">
                  <blockquote cite="Website">
                     <div class="f-md-28 f-22 w300 lh140 zindex9 text-center text-md-start"><span class="w700">Website Analyser to Check any Website</span> for Potential issues in the Website.</div>
                  </blockquote>
               </div>
               <div class="col-md-6 col-12 mt-md0 mt20">
                  <img src="assets/images/f1.webp" class="img-fluid mx-auto d-block">
               </div>
            </div>
            <div class="row align-items-center mt40 mt-md100">
               <div class="col-md-6 col-12 order-md-2">
                  <blockquote cite="Widget">
                     <div class="f-md-28 f-22 w300 lh140 zindex9 text-center text-md-start"><span class="w700">Website Widget to add to your Agency Website,</span> so that 
                        you can offer a legal & compliance test on your website visitors and generate leads of potential clients for getting your services. 
                     </div>
                  </blockquote>
               </div>
               <div class="col-md-6 col-12 mt-md0 mt20 order-md-1">
                  <img src="assets/images/f2.webp" class="img-fluid mx-auto d-block">
               </div>
            </div>
            <div class="row align-items-center mt40 mt-md100">
               <div class="col-md-6 col-12">
                  <blockquote cite="Privacy">
                     <div class="f-md-28 f-22 w300 lh140 zindex9 text-center text-md-start"><span class="w700">Privacy Policy Generator to Quickly Generate</span> Privacy Policy 
                        for any Website with digging deep into research and writing policy by yourself. 
                     </div>
                  </blockquote>
               </div>
               <div class="col-md-6 col-12 mt-md0 mt20">
                  <img src="assets/images/f3.webp" class="img-fluid mx-auto d-block">
               </div>
            </div>
            <div class="row align-items-center mt40 mt-md100">
               <div class="col-md-6 col-12 order-md-2">
                  <blockquote cite="Disclaimer">
                     <div class="f-md-28 f-22 w300 lh140 zindex9 text-center text-md-start"><span class="w700">Disclaimer Generator for any website</span> which is selling any product 
                        or service. It is a must-have thing for any website to save owners from any liabilities and lawsuits. 
                     </div>
                  </blockquote>
               </div>
               <div class="col-md-6 col-12 mt-md0 mt20 order-md-1">
                  <img src="assets/images/f4.webp" class="img-fluid mx-auto d-block">
               </div>
            </div>
            <div class="row align-items-center mt40 mt-md100">
               <div class="col-md-6 col-12">
                  <blockquote cite="Cookie">
                     <div class="f-md-28 f-22 w300 lh140 zindex9 text-center text-md-start"><span class="w700">Cookie Consent Generator to Create Cookie Consent Badge</span>  for 
                        any of your’s or your client’s websites with just a few clicks of a button. 
                     </div>
                  </blockquote>
               </div>
               <div class="col-md-6 col-12 mt-md0 mt20">
                  <img src="assets/images/f5.webp" class="img-fluid mx-auto d-block">
               </div>
            </div>
            <div class="row align-items-center mt40 mt-md100">
               <div class="col-md-6 col-12 order-md-2">
                  <blockquote cite="T&C">
                     <div class="f-md-28 f-22 w300 lh140 zindex9 text-center text-md-start"><span class="w700">Terms and Conditions Generator to create Legal Documents</span> 
                        regarding the website’s terms and conditions, which is a must-have for any website around the Globe. 
                     </div>
                  </blockquote>
               </div>
               <div class="col-md-6 col-12 mt-md0 mt20 order-md-1">
                  <img src="assets/images/f6.webp" class="img-fluid mx-auto d-block">
               </div>
            </div>
            <div class="row align-items-center mt40 mt-md100">
               <div class="col-md-6 col-12">
                  <blockquote cite="Cookie">
                     <div class="f-md-28 f-22 w300 lh140 zindex9 text-center text-md-start"><span class="w700">Cookie Widget Generator</span> to Help you generate and add Cookie Widget instantly on any website.
                     </div>
                  </blockquote>
               </div>
               <div class="col-md-6 col-12 mt-md0 mt20">
                  <img src="assets/images/f7.webp" class="img-fluid mx-auto d-block">
               </div>
            </div>
            <div class="row align-items-center mt40 mt-md100">
               <div class="col-md-6 col-12 order-md-2">
                  <blockquote cite="Return">
                     <div class="f-md-28 f-22 w300 lh140 zindex9 text-center text-md-start"><span class="w700">Return and Refund Policy Generator</span> to Get Ready Made Return & Refund Policy for any Ecom website.
                     </div>
                  </blockquote>
               </div>
               <div class="col-md-6 col-12 mt-md0 mt20 order-md-1">
                  <img src="assets/images/f8.webp" class="img-fluid mx-auto d-block">
               </div>
            </div>
            <div class="row align-items-center mt40 mt-md100">
               <div class="col-md-6 col-12">
                  <blockquote cite="ADA">
                     <div class="f-md-28 f-22 w300 lh140 zindex9 text-center text-md-start">
                        <span class="w700">ADA Generator– This built-in tool will find the issues in 
                        your website</span>  which is not as per ADA regulations and 
                        create a Fix for those issues in just a few minutes by 
                        adding one line of code to your website.
                     </div>
                  </blockquote>
               </div>
               <div class="col-md-6 col-12 mt-md0 mt20">
                  <img src="assets/images/f9.webp" class="img-fluid mx-auto d-block">
               </div>
            </div>
         </div>
      </div>
      <div class="feature-list">
         <div class="container">
            <!-- <div class="row">
               <div class="col-12 f-md-45 f-28 w700 lh140 text-center white-clr">
                  LegelSuites is Packed with GROUND-BREAKING
                  Features That Make It A Cut Above the Rest
               </div>
               </div> -->
            <div class="row row-cols-1 row-cols-md-3 gap30">
               <div class="col">
                  <div class="feature-list-box">
                     <img src="assets/images/fr1.webp" class="img-fluid mx-auto d-block">
                     <p class="description">Send Emails to Clients
                     </p>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box">
                     <img src="assets/images/fr2.webp" class="img-fluid mx-auto d-block">
                     <p class="description">List Management & Tagging</span> 
                     </p>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box">
                     <img src="assets/images/fr3.webp" class="img-fluid mx-auto d-block">
                     <p class="description">List Segmentation & Suppression</p>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box">
                     <img src="assets/images/fr4.webp" class="img-fluid mx-auto d-block">
                     <p class="description">Audience Management</p>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box">
                     <img src="assets/images/fr5.webp" class="img-fluid mx-auto d-block">
                     <p class="description">
                        Advance Analytics
                     </p>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box">
                     <img src="assets/images/fr6.webp" class="img-fluid mx-auto d-block">
                     <p class="description">Advance Integration</p>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box">
                     <img src="assets/images/fr7.webp" class="img-fluid mx-auto d-block">
                     <p class="description">
                        Built-in Business Drive to Store Manage & Share Files
                     </p>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box">
                     <img src="assets/images/fr8.webp" class="img-fluid mx-auto d-block">
                     <p class="description">Create Unlimited Business/Domains</p>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box">
                     <img src="assets/images/fr9.webp" class="img-fluid mx-auto d-block">
                     <p class="description">Custom Domains</p>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box">
                     <img src="assets/images/fr10.webp" class="img-fluid mx-auto d-block">
                     <p class="description">Single Dashboard to Manage All Business & Clients</p>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box">
                     <img src="assets/images/fr11.webp" class="img-fluid mx-auto d-block">
                     <p class="description">100% Cloud Based – <br class="d-none d-md-block"> No Hosting/Installation Required</p>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box">
                     <img src="assets/images/fr12.webp" class="img-fluid mx-auto d-block">
                     <p class="description">Easy & Intuitive<br class="d-none d-md-block"> to Use Software</p>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box">
                     <img src="assets/images/fr13.webp" class="img-fluid mx-auto d-block">
                     <p class="description">Complete Step-by-Step Training Included</p>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box">
                     <img src="assets/images/fr14.webp" class="img-fluid mx-auto d-block">
                     <p class="description">No Coding, Design or Technical Skills Required</p>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box">
                     <img src="assets/images/fr15.webp" class="img-fluid mx-auto d-block">
                     <p class="description">Regular Updates</p>
                  </div>
               </div>
            </div>
            <div class="row mt0 mt-md20">
               <div class="col-12 text-center">
                  <p class="more">And Much More…</p>
               </div>
            </div>
         </div>
      </div>
      <!-- DEMO SECTION START -->
      <div class="demo-sec">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="demo-inner-sec">
                     <div class="row align-items-center">
                        <div class="col-12 col-md-6 relative">
                           <div class="f-md-45 f-28 w700 lh140 white-clr text-center text-md-start">
                              <span class="cyan-clr watch-text">Watch The Demo</span>
                              <br class="d-none d-md-block">
                              Discover How Easy & Powerful It Is
                           </div>
                           <img src="assets/images/demo-arrow.webp" class="img-fluid d-none d-md-block" alt="Semo Arrow" style="position:absolute; top:-26px; right:20px;">
                        </div>
                        <div class="col-12 col-md-6 mt-md40 mt20">
                           <!-- <img src="assets/images/demo-video-poster.webp" class="img-fluid d-block mx-auto"> -->
                           <div style="padding-bottom: 56.25%;position: relative;"><iframe src="https://legelsuites.dotcompal.com/video/embed/wy7c7hy9ak" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                           </div> 
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- DEMO SECTION END -->
      <section class="analyzer-sec">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="test-derive">
                     <div class="f-26 f-md-45 w700 lh150 text-center">
                        Test Drive The LegelSuites Website Analyzer
                     </div>
                     <img src="assets/images/zig-zag-line.webp" alt="Zig Zag Line" class="mx-auto img-fluid d-block mb20 mb-md30">
                     <script type="text/javascript" src="https://www.legelsuites.com/widget/checker/oppyo-xYlk-Jd1w-qBAz-d3a9" crossorigin="anonymous" embed-id="oppyo-xYlk-Jd1w-qBAz-d3a9"></script>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <div class="deep-funnel" id="funnel">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-45 f-28 w700 text-center lh140">
                  Our Deep & High Converting Sales Funnel
               </div>
               <div class="col-12 col-md-12 mt20 mt-md70">
                  <div>
                     <img src="assets/images/funnel.webp" class="img-fluid d-block mx-auto">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="prize-value">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-50 f-28 w600 text-center white-clr lh140">
                  Get Ready to Grab Your Share of
               </div>
               <div class="col-12 f-md-72 f-40 w800 text-center cyan-clr lh140">
                  $12000 JV Prizes
               </div>
            </div>
         </div>
      </div>
      <div class="contest-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-45 f-28 w700 text-center black-clr">
                  Pre-Launch Lead Contest
               </div>
               <div class="col-12 f-md-18 f-18 w400 lh140 mt20 text-center">
                  Contest Starts on 15th November'22 at 10:00 AM EST and Ends on 17th November at 10:00 AM EST
               </div>
               <div class="col-12 f-md-18 f-18 w400 lh140 mt10 text-center">
                  (Get Flat <span class="w700"> $0.50c </span> For Every Lead You Send for <span class="w700"> Pre-Launch Webinar)</span>
               </div>
               <div class="col-12 mt20 mt-md50">
                  <div class="row align-items-center">
                     <div class="col-12 col-md-5">
                        <img src="assets/images/happy-man.webp" class="img-fluid d-none d-md-block mx-auto" alt="Contest">
                     </div>
                     <div class="col-12 col-md-7">
                        <img src="assets/images/contest-img2.webp" class="img-fluid d-block mx-auto" alt="Contest">
                     </div>
                  </div>
               </div>
               <div class="col-12 f-18 f-md-18 lh140 mt-md40 mt20 w400 text-center black-clr">
                  *(Eligibility – All leads should have at least 1% conversion on launch and should have min 100 leads)
               </div>
            </div>
         </div>
      </div>

      <div class="prize-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="prize-inner-sec">
                     <div class="row">
                        <div class="col-12">
                           <div class="f-md-65 f-30 white-clr text-center w700 lh140">
                           $10,000 Launch Contest
                              <img src="assets/images/launch.webp" alt="Exciting Curve" class="mx-auto d-block img-fluid">
                           </div>
                        </div>
                        <div class="col-12 col-md-12 mx-auto mt-md90 mt30">
                           <div class="row gx-8">
                              <div class="col-md-6 col-12 relative">
                                 <div class="prize-launch-left">
                                    <div class="f-md-45 f-28 w700 lh140 text-center orange-clr cavet-font prize-right-left">
                                    Opening Contest
                                    </div>
                                    <div class="f-18 w600 lh140 black-clr mt20 mt-md30 text-center">
                                    Contest Runs From 17th Nov. 22, 11 AM EST <br>
to 19th Nov. 22, 11:59 PM EST
                                    </div>
                                    <img src="assets/images/prize-img1.webp" class="img-fluid d-block mx-auto mt20">
                                    <div class="f-14 w400 mt20">
                                    1. Terms of maximum two is allowed.<br><br>
2. To be eligible to win one of the sales leaderboard
prizes, you must have made commissions equal to
or greater than the value of the prize. If this criterion
is not met, then you will be eligible for the next prize.
                                    </div>                                    
                                 </div>
                              </div>
                              <div class="col-md-6 col-12 mt-md100 mt30 relative">
                                 <div class="prize-launch-right text-center">
                                    <div class="f-md-24 f-20 w600 lh140 text-center white-clr prize-right-head">
                                    Additional Surprise Contest For
                                    </div>
                                    <img src="assets/images/prize-img2.webp" class="img-fluid d-block mx-auto mt20 mt-md40">
                                    <div class="f-20 w700 lh140 white-clr mt20 mt-md30 text-center">
                                    Will Be Announced During The Launch...
                                    </div>
                                    <div class="f-24 f-md-32 w700 lh140 mt20 black-clr text-center note">
                                    Note
                           </div>
                                    <div class="f-18 f-md-20 w400 lh140 mt20 white-clr text-center">
                                    We will announce the winners on <span class="w600">24th Nov</span><br class="d-none d-md-block">
& Prizes will be distributed through<br class="d-none d-md-block">
Payoneer from <span class="w600">25th Nov Onwards.</span>
                           </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>      
      <div class="reciprocate-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-md-45 f-28 lh140 w700 black-clr">
                     Our Solid Track Record of <br class="d-md-block d-none"> <span class="blue-clr1"> Launching Top Converting Products</span>
                  </div>
               </div>
               <div class="col-12 col-md-12 mt20">
                  <img src="assets/images/product-logo.webp" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 text-center f-md-45 f-28 lh140 w700 white-clr mt20 mt-md70 blue-clr1">
                  Do We Reciprocate?
               </div>
               <div class="col-12 f-18 f-md-18 lh140 mt20 w400 text-center black-clr">
                  We've been in top positions on various launch leaderboards & sent huge sales for our valued JVs.<br><br>  
                  So, if you have a top-notch product with top conversions that fits our list, we would love to drive loads of sales for you. Here are just some results from our recent promotions. 
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
                  <div class="logos-effect">
                     <img src="assets/images/logos.png" class="img-fluid d-block mx-auto">
                  </div>
               </div>
               <div class="col-12 f-md-28 f-24 lh140 mt20 mt-md50 w600 text-center">
                  And The List Goes On And On...
               </div>
            </div>
         </div>
      </div>
      <div class="contact-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="contact-block mx-auto">
                     <div class="row gx-md-5 mx-auto">
                        <div class="col-12 mb20 mb-md50">
                           <div class="f-md-45 f-28 w700 lh140 text-center white-clr">
                              Have any Query? Contact us Anytime
                           </div>
                        </div>
                        <div class="col-md-6 col-12 text-center">
                           <div class="contact-shape">
                              <img src="assets/images/amit-pareek-sir.webp" class="img-fluid d-block mx-auto minus">
                              <div class="f-24 f-md-30 w600  lh140 text-center white-clr mt20">
                                 Dr Amit Pareek
                              </div>
                              <div class="f-14 w400 lh140 text-center white-clr">
                                 (Techpreneur & Marketer)
                              </div>
                              <div class="col-12 mt30 d-flex justify-content-center">
                                 <a href="http://facebook.com/Dr.AmitPareek" class="link-text mr20">
                                 <img src="https://cdn.oppyo.com/launches/webpull/jv/am-fb.webp" class="center-block">
                                 </a>
                                 <a href="skype:amit.pareek77" class="link-text">
                                    <div class="col-12 ">
                                       <img src="https://cdn.oppyo.com/launches/webpull/jv/am-skype.webp" class="center-block">
                                    </div>
                                 </a>
                              </div>
                           </div>
                        </div>
                        <div class="col-md-6 col-12 text-center mt20 mt-md0">
                           <div class="contact-shape">
                              <img src="assets/images/atul-parrek-sir.webp" class="img-fluid d-block mx-auto minus">
                              <div class="f-24 f-md-30 w600  lh140 text-center white-clr mt20">
                                 Atul Pareek
                              </div>
                              <div class="f-14 w400 lh140 text-center white-clr">
                                 (Entrepreneur & Product Creator)
                              </div>
                              <div class="col-12 mt30 d-flex justify-content-center">
                                 <a href="https://www.facebook.com/profile.php?id=100076904623319" class="link-text mr20">
                                 <img src="https://cdn.oppyo.com/launches/webpull/jv/am-fb.webp" class="center-block">
                                 </a>
                                 <a href="skype:live:.cid.c3d2302c4a2815e0" class="link-text">
                                    <div class="col-12 ">
                                       <img src="https://cdn.oppyo.com/launches/webpull/jv/am-skype.webp" class="center-block">
                                    </div>
                                 </a>
                              </div>
                           </div>
                        </div>
                        <!-- <div class="col-md-4 col-12 text-center mt20 mt-md0">
                           <div class="contact-shape">
                              <img src="assets/images/achal-goswami-sir.webp" class="img-fluid d-block mx-auto minus">
                              <div class="f-24 f-md-30 w600  lh140 text-center white-clr mt20">
                                 Achal Goswami
                              </div>
                              <div class="f-14 w400 lh140 text-center white-clr">
                                 (Entrepreneur & Internet Marketer)
                              </div>
                              <div class="col-12 mt30 d-flex justify-content-center">
                                 <a href="https://www.facebook.com/dcp.ambassador.achal/" class="link-text mr20">
                                 <img src="https://cdn.oppyo.com/launches/webpull/jv/am-fb.webp" class="center-block">
                                 </a>
                                 <a href="skype:live:.cid.78f368e20e6d5afa" class="link-text">
                                    <div class="col-12 ">
                                       <img src="https://cdn.oppyo.com/launches/webpull/jv/am-skype.webp" class="center-block">
                                    </div>
                                 </a>
                              </div>
                           </div>
                        </div> -->
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="terms-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-50 f-28 w700 lh140 text-center">
                  Affiliate Promotions Terms & Conditions
               </div>
               <div class="col-md-12 col-12 f-18 lh140 p-md0 mt-md20 mt20 w400 text-center">
                  We request you to read this section carefully before requesting for your affiliate link and being a part of this launch. Once you are approved to be a part of this launch and promote this product, you must abide by the instructions listed below: 
               </div>
               <div class="col-md-12 col-12 px-md-0 mt15 mt-md50">
                  <ul class="terms-list m0 f-16 lh140 w400">
                     <li>
                        Please be sure to NOT send spam of any kind. This includes cheap traffic methods in any way whatsoever. In case anyone does so, they will be banned from all future promotion with us without any reason whatsoever. No exceptions will be entertained.
                     </li>
                     <li>
                        Please ensure you or your members DON’T use negative words such as 'scam' in any of your promotional campaigns in any way. If this comes to our knowledge that you have violated it, your affiliate account will be removed from our system with immediate effect.
                     </li>
                     <li>
                        Please make it a point to not OFFER any cash incentives (such as rebates), cash backs etc or any such malpractices to people buying through your affiliate link.
                     </li>
                     <li>
                        Please note that you are not AUTHORIZED to create social media pages or run any such campaigns that may be detrimental using our product or brand name in any way possible. Doing this may result in serious consequences.
                     </li>
                     <li>
                        We reserve the right to TERMINATE any affiliate with immediate effect if they’re found to breach any/all of the conditions mentioned above in any manner.
                     </li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!--Footer Section Start -->
      <div class="footer-section">
         <div class="container ">
            <div class="row">
               <div class="col-12 text-center">
               <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                              viewBox="0 0 495.7 104.2" style="enable-background:new 0 0 495.7 104.2; max-height:60px;" xml:space="preserve">
                              <style type="text/css">
                                 .st0{fill:#FFFFFF;}
                                 .st1{fill:#FFA829;}
                                 .st2{fill:url(#SVGID_1_);}
                                 .st3{fill:url(#SVGID_00000132794433514585357610000014266780508499585408_);}
                              </style>
                              <g>
                                 <path class="st0" d="M96.9,82.6c-7.5,2.8-15.7,4.4-24.2,4.8c0.2,0.7,0,1.4-0.5,1.9c-3.2,3.4-6.4,6.1-9.9,8.2
                                    c-5.4,3.4-11.5,5.6-17.9,6.5l-0.3,0.1c-0.2,0-0.3,0.1-0.5,0.1c-0.2,0-0.3,0-0.5-0.1l-0.3-0.1c-6.4-0.9-12.4-3.1-17.9-6.6
                                    C11.6,89,3.4,74.5,3.2,58.7c0-0.9,0.6-1.7,1.4-2c0,0,0.1,0,0.1,0C1.2,50.6-0.5,43.9,0.1,37C1.8,19.4,18.3,4.9,42.3,0
                                    c1-0.2,2,0.4,2.3,1.4c0.3,1-0.2,2.1-1.2,2.4c-5.4,2-10.2,4.6-14.2,7.7l14-3.6c0.3-0.1,0.7-0.1,1,0l38.4,9.9c0.9,0.2,1.5,1,1.5,1.9
                                    v38.1c0,7.9-2,15.6-5.7,22.4c5.9,0.1,11.7-0.4,17.4-1.5c1-0.2,2,0.4,2.3,1.4C98.4,81.2,97.9,82.2,96.9,82.6z"/>
                                 <g>
                                    <path class="st0" d="M291.8,67.6l2.5-5.7c3.2,2.5,8.2,4.3,13.1,4.3c6.2,0,8.8-2.2,8.8-5.1c0-8.6-23.6-3-23.6-17.7
                                       c0-6.4,5.1-11.8,16-11.8c4.8,0,9.7,1.3,13.2,3.5l-2.3,5.7c-3.6-2.1-7.5-3.1-10.9-3.1c-6.1,0-8.6,2.4-8.6,5.4
                                       c0,8.4,23.5,3,23.5,17.5c0,6.3-5.1,11.8-16,11.8C301.2,72.4,295,70.4,291.8,67.6z"/>
                                    <path class="st0" d="M464,67.6l2.5-5.7c3.2,2.5,8.2,4.3,13.1,4.3c6.2,0,8.8-2.2,8.8-5.1c0-8.6-23.6-3-23.6-17.7
                                       c0-6.4,5.1-11.8,16-11.8c4.8,0,9.7,1.3,13.2,3.5l-2.3,5.7c-3.6-2.1-7.5-3.1-10.9-3.1c-6.1,0-8.6,2.4-8.6,5.4
                                       c0,8.4,23.5,3,23.5,17.5c0,6.3-5.1,11.8-16,11.8C473.4,72.4,467.2,70.4,464,67.6z"/>
                                    <path class="st0" d="M330.3,54.5V32.2h7.3v22.1c0,8.1,3.7,11.7,10.1,11.7s10-3.5,10-11.7V32.2h7.2v22.3c0,11.6-6.5,17.8-17.3,17.8
                                       C336.9,72.4,330.3,66.1,330.3,54.5z"/>
                                    <path class="st0" d="M375.3,32.2h7.3v39.6h-7.3V32.2z"/>
                                    <path class="st0" d="M401.4,38.4h-13.1v-6.2h33.6v6.2h-13.1v33.4h-7.3V38.4H401.4z"/>
                                    <path class="st0" d="M457.2,65.6v6.2h-29.7V32.2h28.9v6.2h-21.6v10.3H454v6.1h-19.1v10.9L457.2,65.6L457.2,65.6z"/>
                                 </g>
                                 <g>
                                    <path class="st1" d="M96.2,80.7c-10.5,3.9-23,5.6-36.1,4.4C25.9,81.9,0,60.5,2.1,37.2C3.7,20,20.3,6.5,42.7,2
                                       C26.1,8.2,14.6,19.8,13.2,34.1C11.1,57.4,37,78.8,71.1,81.9C79.9,82.7,88.4,82.2,96.2,80.7z"/>
                                    <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="20.0931" y1="1069.2449" x2="47.1737" y2="1042.1643" gradientTransform="matrix(1 0 0 1 0 -978)">
                                       <stop  offset="0" style="stop-color:#0055FF"/>
                                       <stop  offset="1" style="stop-color:#008CFF"/>
                                    </linearGradient>
                                    <path class="st2" d="M68.3,87.8c-2,0-3.9-0.1-5.9-0.2c-1.3,1.1-2.7,2.1-4.2,3.1c-4.5,2.8-9.3,4.6-14.5,5.4
                                       c-5.1-0.8-10-2.6-14.5-5.4c-8.7-5.4-14.6-13.8-17-23.4c-2.8-2.7-5.1-5.6-7-8.6c0.2,15.2,8,29,20.9,37.1c5.3,3.3,11,5.4,17.2,6.3
                                       l0.4,0.1l0.4-0.1c6.1-0.9,11.9-3,17.2-6.3c3.6-2.2,6.7-4.9,9.5-7.9C70,87.8,69.1,87.8,68.3,87.8z"/>
                                    <linearGradient id="SVGID_00000129182319601907477880000006569859738938224028_" gradientUnits="userSpaceOnUse" x1="33.5187" y1="1042.8949" x2="80.3658" y2="996.0477" gradientTransform="matrix(1 0 0 1 0 -978)">
                                       <stop  offset="0" style="stop-color:#0055FF"/>
                                       <stop  offset="1" style="stop-color:#008CFF"/>
                                    </linearGradient>
                                    <path style="fill:url(#SVGID_00000129182319601907477880000006569859738938224028_);" d="M43.7,9.9l-17.5,4.5
                                       c-2,1.9-21.6,31.5,10.2,51.6l2.2-13c-2.2-1.6-3.6-4.2-3.6-7c0-4.8,3.9-8.7,8.7-8.7s8.7,3.9,8.7,8.7c0,2.9-1.4,5.4-3.6,7l3.4,20.5
                                       C59,76,67,78.1,76.4,79.8c3.7-6.5,5.7-14,5.7-21.9V19.8L43.7,9.9z M40.3,17.5l-0.2,2.2l-1.6-1.4l-2.1,0.5l0.9-2l-1.1-1.9l2.2,0.2
                                       l1.4-1.6l0.4,2.1l2,0.8L40.3,17.5z M59.9,22.7l-0.2,2.5l-1.8-1.7l-2.4,0.6l1-2.2l-1.3-2.1l2.5,0.3l1.6-1.9l0.5,2.4l2.3,1
                                       L59.9,22.7z M74.2,71.3L74,74.6l-2.5-2.3l-3.3,0.8l1.4-3.1l-1.8-2.9l3.4,0.4l2.2-2.5l0.7,3.3l3.1,1.3L74.2,71.3z M74.8,51.1
                                       l-0.2,3.1l-2.3-2.1l-3,0.7l1.3-2.8L69,47.4l3.1,0.3l2-2.4l0.6,3l2.9,1.2L74.8,51.1z M75.4,31l-0.2,2.8l-2.1-1.9l-2.7,0.6l1.1-2.5
                                       l-1.4-2.4l2.7,0.3l1.8-2.1l0.6,2.7l2.6,1.1L75.4,31z"/>
                                 </g>
                                 <g>
                                    <path class="st0" d="M105.1,32.2h11.2v30.7h18.9v8.9h-30.1V32.2z"/>
                                    <path class="st0" d="M171.5,63.1v8.7h-31.8V32.2h31.1v8.7h-20v6.7h17.6V56h-17.6v7.2L171.5,63.1L171.5,63.1z"/>
                                    <path class="st0" d="M204.2,51.2h9.9v16.4c-4.6,3.3-10.9,5-16.6,5c-12.6,0-21.8-8.6-21.8-20.6s9.2-20.6,22.1-20.6
                                       c7.4,0,13.4,2.5,17.3,7.2L208,45c-2.7-3-5.8-4.4-9.6-4.4c-6.8,0-11.3,4.5-11.3,11.3c0,6.7,4.5,11.3,11.2,11.3
                                       c2.1,0,4.1-0.4,6.1-1.3L204.2,51.2L204.2,51.2z"/>
                                    <path class="st0" d="M253,63.1v8.7h-31.8V32.2h31.1v8.7h-20v6.7h17.6V56h-17.6v7.2L253,63.1L253,63.1z"/>
                                    <path class="st0" d="M259.2,32.2h11.2v30.7h18.9v8.9h-30.1V32.2z"/>
                                 </g>
                              </g>
                           </svg>
                  <div class="f-16 f-md-18 w300 mt20 lh150 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-16 f-md-18 w300 lh150 white-clr text-xs-center">Copyright © LegelSuites 2022</div>
                  <ul class="footer-ul f-16 f-md-18 w300 white-clr text-center text-md-right">
                     <li><a href="https://support.oppyo.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://legelsuites.co/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://legelsuites.co/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://legelsuites.co/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://legelsuites.co/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://legelsuites.co/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://legelsuites.co/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!--Footer Section End -->
      <!-- timer --->
      <?php
         if ($now < $exp_date) {
         
         ?>
      <script type="text/javascript">
         // Count down milliseconds = server_end - server_now = client_end - client_now
         var server_end = <?php echo $exp_date; ?> * 1000;
         var server_now = <?php echo time(); ?> * 1000;
         var client_now = new Date().getTime();
         var end = server_end - server_now + client_now; // this is the real end time
         
         var noob = $('.countdown').length;
         
         var _second = 1000;
         var _minute = _second * 60;
         var _hour = _minute * 60;
         var _day = _hour * 24
         var timer;
         
         function showRemaining() {
             var now = new Date();
             var distance = end - now;
             if (distance < 0) {
                 clearInterval(timer);
                 document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
                 return;
             }
         
             var days = Math.floor(distance / _day);
             var hours = Math.floor((distance % _day) / _hour);
             var minutes = Math.floor((distance % _hour) / _minute);
             var seconds = Math.floor((distance % _minute) / _second);
             if (days < 10) {
                 days = "0" + days;
             }
             if (hours < 10) {
                 hours = "0" + hours;
             }
             if (minutes < 10) {
                 minutes = "0" + minutes;
             }
             if (seconds < 10) {
                 seconds = "0" + seconds;
             }
             var i;
             var countdown = document.getElementsByClassName('countdown');
             for (i = 0; i < noob; i++) {
                 countdown[i].innerHTML = '';
         
                 if (days) {
                     countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-30 f-md-40 timerbg">' + days + '</span><br><span class="f-16 w500">Days</span> </div>';
                 }
         
                 countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-30 f-md-40 timerbg">' + hours + '</span><br><span class="f-16 w500">Hours</span> </div>';
         
                 countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-30 f-md-40 timerbg">' + minutes + '</span><br><span class="f-16 w500">Mins</span> </div>';
         
                 countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-30 f-md-40 timerbg">' + seconds + '</span><br><span class="f-16 w500">Sec</span> </div>';
             }
         
         }
         timer = setInterval(showRemaining, 1000);
      </script>    
      <?php
         } else {
         echo "Times Up";
         }
         ?>
      <script type="text/javascript">
// Special handling for in-app browsers that don't always support new windows
(function() {
    function browserSupportsNewWindows(userAgent) {
        var rules = [
            'FBIOS',
            'Twitter for iPhone',
            'WebView',
            '(iPhone|iPod|iPad)(?!.*Safari\/)',
            'Android.*(wv|\.0\.0\.0)'
        ];
        var pattern = new RegExp('(' + rules.join('|') + ')', 'ig');
        return !pattern.test(userAgent);
    }

    if (!browserSupportsNewWindows(navigator.userAgent || navigator.vendor || window.opera)) {
        document.getElementById('af-form-575032983').parentElement.removeAttribute('target');
    }
})();
</script><script type="text/javascript">
    (function() {
        var IE = /*@cc_on!@*/false;
        if (!IE) { return; }
        if (document.compatMode && document.compatMode == 'BackCompat') {
            if (document.getElementById("af-form-575032983")) {
                document.getElementById("af-form-575032983").className = 'af-form af-quirksMode';
            }
            if (document.getElementById("af-body-575032983")) {
                document.getElementById("af-body-575032983").className = "af-body inline af-quirksMode";
            }
            if (document.getElementById("af-header-575032983")) {
                document.getElementById("af-header-575032983").className = "af-header af-quirksMode";
            }
            if (document.getElementById("af-footer-575032983")) {
                document.getElementById("af-footer-575032983").className = "af-footer af-quirksMode";
            }
        }
    })();
</script>

<!-- /AWeber Web Form Generator 3.0.1 -->
   </body>
</html>
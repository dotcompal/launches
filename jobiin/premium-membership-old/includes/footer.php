<!----------- Footer Section-------------->
<footer class="footer-section">
	<div class="container container-1170">
		<div class="row">
		
			<!--- Footer Logo & Links ----->
			<div class="col-12 col-md-5 text-center">
				<div class="col-12 p0 clearfix">
				<img src="https://cdn.staticdcp.com/apps/website/assets/images/footer-logo.webp" class="img-fluid d-block mx-auto float-md-left">
				</div>	
				<p class="f-14 f-lg-16 w400 d-flex align-items-center justify-content-center justify-content-md-start mt15 mt-md25">Copyright <span class="f-20">&nbsp;&copy;&nbsp;</span> 2022, Saglus Info Pvt Ltd. </p>
			</div>
			<div class="col-12 col-md-7 d-flex align-items-end justify-content-center justify-content-md-end mt10 mt-md0 text-center pl-md0">
				<ul class="footer-bottom-links f-14 f-lg-16 w400">
					<li><a target="_blank" href="https://www.dotcompal.com/terms-condition" title="Terms & Conditions">Terms & Conditions</a></li>
					<li>|</li>
					<li><a target="_blank" href="https://www.dotcompal.com/privacy-policy" title="Privacy Policy">Privacy Policy</a></li>
					<li>|</li>
					<li><a target="_blank" href="https://www.dotcompal.com/dmca" title="DMCA Policy">DMCA Policy</a></li>
					<li>|</li>
					<li><a target="_blank" href="https://www.dotcompal.com/disclaimer" title="Cookies">Disclaimer</a></li>
				</ul>
			</div>
		</div>
	</div>
</footer>
<!----------- Footer Section end ------------->

<!---- Common Jquery Files ------> 
<script type='text/javascript' src="<?php echo $basedir; ?>assets/vendors/jquery/jquery.min.js"></script>
<script type='text/javascript' src="<?php echo $basedir; ?>assets/vendors/bootstrap/popper.min.js"></script> 
<script type='text/javascript' src="<?php echo $basedir; ?>assets/vendors/bootstrap/bootstrap.min.js"></script> 

<link rel="stylesheet" href="<?php echo $basedir; ?>assets/css/owl.carousel.min.css"/>
<script type='text/javascript' src="<?php echo $basedir; ?>assets/js/owl.carousel.min.js"></script>
<script type='text/javascript' src="<?php echo $basedir; ?>assets/js/carousel-slider.js"></script>

<script type='text/javascript' src="<?php echo $basedir; ?>assets/js/step-carousel.js"></script>
<!---- Add Custom Javacscript File ------> 
<script type='text/javascript' src="<?php echo $basedir; ?>assets/js/script.js"></script>
<!---- Custom Scrollbar Jquery File ------>

</body>
</html>

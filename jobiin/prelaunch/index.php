<!Doctype html>
<html>
   <head>
      <title>JOBiin Prelaunch</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=9">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
      <meta name="title" content="JOBiin Prelaunch">
      <meta name="description" content="Brand New, 1-Click Software That Makes Us $620/Day by Creating Automatic Job Search Sites in JUST 60 Seconds">
      <meta name="keywords" content="JOBiin">
      <meta property="og:image" content="https://www.getjobiin.co/prelaunch/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Ayush Jain">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="JOBiin Prelaunch">
      <meta property="og:description" content="Brand New, 1-Click Software That Makes Us $620/Day by Creating Automatic Job Search Sites in JUST 60 Seconds">
      <meta property="og:image" content="https://www.getjobiin.co/prelaunch/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="JOBiin Prelaunch">
      <meta property="twitter:description" content="Brand New, 1-Click Software That Makes Us $620/Day by Creating Automatic Job Search Sites in JUST 60 Seconds">
      <meta property="twitter:image" content="https://www.getjobiin.co/prelaunch/thumbnail.png">
      <link rel="icon" href="https://cdn.oppyo.com/launches/jobiin/common_assets/images/favicon.png" type="image/png">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Pacifico&display=swap" rel="stylesheet">
      <link rel="stylesheet" href="https://cdn.oppyo.com/launches/jobiin/common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style.css" type="text/css">
      <link rel="stylesheet" href="assets/css/aweberform.css" type="text/css">
      <script src="https://cdn.oppyo.com/launches/jobiin/common_assets/js/jquery.min.js"></script>
      <script>
         function getUrlVars() {
             var vars = [],
                 hash;
             var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
             for (var i = 0; i < hashes.length; i++) {
                 hash = hashes[i].split('=');
                 vars.push(hash[0]);
                 vars[hash[0]] = hash[1];
             }
             return vars;
         }
         var first = getUrlVars()["aid"];
         //alert(first);
         document.addEventListener('DOMContentLoaded', (event) => {
                 document.getElementById('awf_field_aid').setAttribute('value', first);
             })
             //document.getElementById('myField').value = first;
      </script>
   </head>
   <body>
      <!--Header Section Start -->
      <div class="header-section">
         <div class="container">
            <div class="row">
               <div class="col-md-3 text-md-start text-center">
                  <svg id="Layer_1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 128.19 38" style="max-height:55px;">
                     <defs>
                        <style>.cls-1,.cls-2{fill:#fff;}.cls-2{fill-rule:evenodd;}.cls-3{fill:#0a52e2;}</style>
                     </defs>
                     <rect class="cls-1" x="79.21" width="48.98" height="38" rx="4" ry="4"></rect>
                     <path class="cls-1" d="M18.64,4.67V24.72c0,1.29-.21,2.47-.63,3.54-.42,1.07-1.03,1.97-1.84,2.71-.81,.74-1.79,1.32-2.93,1.74-1.15,.42-2.45,.63-3.9,.63s-2.64-.19-3.7-.57c-1.07-.38-1.98-.92-2.75-1.62-.77-.7-1.39-1.54-1.88-2.51S.19,26.6,0,25.41l5.74-1.13c.46,2.45,1.63,3.68,3.52,3.68,.89,0,1.65-.28,2.28-.85,.63-.57,.95-1.46,.95-2.67V9.6H3.92V4.67h14.72Z"></path>
                     <path class="cls-1" d="M54.91,33.37V4.63h9.71c3.38,0,6.02,.66,7.92,1.97,1.9,1.32,2.84,3.28,2.84,5.9,0,1.33-.35,2.52-1.06,3.56-.7,1.05-1.73,1.83-3.07,2.36,1.72,.37,3.02,1.16,3.88,2.37,.86,1.21,1.29,2.61,1.29,4.21,0,2.75-.91,4.83-2.72,6.25-1.82,1.42-4.39,2.12-7.72,2.12h-11.08Zm5.76-16.7h4.15c1.54,0,2.72-.32,3.55-.95,.83-.63,1.24-1.55,1.24-2.76,0-1.33-.42-2.31-1.25-2.94-.84-.63-2.08-.95-3.74-.95h-3.95v7.6Zm0,3.99v8.27h5.31c1.53,0,2.69-.33,3.49-.99,.8-.66,1.2-1.64,1.2-2.94,0-1.4-.34-2.48-1.03-3.22-.68-.74-1.76-1.11-3.24-1.11h-5.75Z"></path>
                     <g>
                        <path class="cls-1" d="M46.84,4.19C42.31-.12,36.87-1.08,31.16,1.2c-5.82,2.33-9.1,6.88-9.52,13.21-.41,6.17,2.57,10.9,6.84,15.15,.95-.84,1.22-1.22,2.01-2.05-.79-.65-1.41-1.09-2.43-2.11-5.86-6.15-5.78-15.66,1.04-20.46,5-3.52,11.71-3.13,16.37,.95,4.29,3.75,5.54,10.44,2.59,15.61-1.2,2.11-3.09,3.84-4.86,5.98,.28,.45,.72,1.18,1.44,2.35,1.77-2.01,3.11-3.72,4.38-5.63,4.39-6.6,3.56-14.59-2.17-20.02Z"></path>
                        <g>
                           <path class="cls-2" d="M39.67,27.34c-2.49,.37-4.9,.52-7.23-.46-.54-.23-1.13-1.06-1.18-1.65-.3-3.69,1.25-5.27,4.98-5.26,.31,0,.62,0,.92,.01,5.31-.01,5.96,6.85,2.51,7.36Z"></path>
                           <path class="cls-2" d="M39.49,16.69c-.04,1.69-1.37,2.98-3.06,2.98-1.6,0-3.01-1.43-3.01-3.06s1.39-3.04,3.02-3.03c1.75,0,3.1,1.38,3.06,3.12Z"></path>
                        </g>
                        <g>
                           <g>
                              <path class="cls-2" d="M42.93,26.23c-.67-3.46-.93-5.25-3.46-7.18,.63-1.21,1.3-1.83,2.1-2,2.43-.53,3.98,.21,4.84,1.97,.66,1.34,.78,2.29-.37,3.77-1.03,1.17-1.85,2.21-3.11,3.43Z"></path>
                              <path class="cls-2" d="M44.94,13.4c.01,1.6-1.27,2.91-2.88,2.92-1.6,.01-2.91-1.28-2.92-2.88-.01-1.6,1.28-2.9,2.88-2.92,1.6-.01,2.91,1.28,2.92,2.88Z"></path>
                           </g>
                           <g>
                              <path class="cls-2" d="M30.1,26.23c.67-3.46,.93-5.25,3.46-7.18-.63-1.21-1.3-1.83-2.1-2-2.43-.53-3.98,.21-4.84,1.97-.66,1.34-.88,2.01,.27,3.49,.96,1.3,1.88,2.44,3.21,3.71Z"></path>
                              <path class="cls-2" d="M28.08,13.4c0,1.6,1.28,2.91,2.88,2.92,1.6,.01,2.91-1.28,2.92-2.88,.01-1.6-1.27-2.9-2.88-2.92-1.6-.01-2.91,1.28-2.92,2.88Z"></path>
                           </g>
                        </g>
                        <path class="cls-1" d="M42.02,27.74c-.7,.41-6.95,2.1-10.73,.08l-2.22,2.57c.55,.61,5.12,5.27,7.55,7.6,2.22-2.17,7.2-7.37,7.2-7.37l-1.8-2.89Z"></path>
                     </g>
                     <g>
                        <rect class="cls-3" x="85.05" y="4.63" width="5.38" height="5.4" rx="2.69" ry="2.69"></rect>
                        <rect class="cls-3" x="85.05" y="13.43" width="5.38" height="19.94"></rect>
                        <rect class="cls-3" x="95.13" y="4.63" width="5.38" height="5.4" rx="2.69" ry="2.69"></rect>
                        <rect class="cls-3" x="95.13" y="13.43" width="5.38" height="19.94"></rect>
                        <path class="cls-3" d="M109.85,13.43l.24,2.86c.66-1.02,1.48-1.81,2.45-2.38,.97-.56,2.06-.85,3.26-.85,2.01,0,3.59,.63,4.72,1.9,1.13,1.27,1.7,3.25,1.7,5.95v12.46h-5.4v-12.47c0-1.34-.27-2.29-.81-2.85-.54-.56-1.36-.84-2.45-.84-.71,0-1.35,.14-1.92,.43-.57,.29-1.04,.7-1.42,1.23v14.5h-5.38V13.43h5.01Z"></path>
                     </g>
                  </svg>
               </div>
               <div class="col-md-9  mt-md0 mt15">
                  <ul class="leader-ul f-16 f-md-18 w400 white-clr text-md-end text-center ">
                     <li>
                        <a href="#book-seat1" class="affiliate-link-btn">Subscribe To EarlyBird VIP List</a>
                     </li>
                  </ul>
               </div>
               <div class="col-12 text-center mt20 mt-md50 lh130">
                  <div class="pre-heading f-md-20 f-16 w600 lh130">
                  Tap into This HUGE Part-Time or Full Time Income Opportunity with NO Extra Efforts!
                  </div>
               </div>
               <div class="col-12 mt-md25 mt20 f-md-50 f-26 w500 text-center white-clr lh130">
               A Dead-Simple Software
                  <span class="orange-clr w700"> That Makes Us $328/Day Commissions By Creating Simple, Self Updating Job Sites</span>in Just 60 Seconds...
               </div>
               <div class="col-12 mt-md25 mt20 f-18 f-md-24 w500 text-center lh130 white-clr">
               No Manual Job Posting Ever... Automatic post from 1o Mn Jobs I Earn Commissions From 3 Income Sources| No Tech Skill or Prior Knowledge| No Tech Skill or Prior Knowledge Required
               </div>
            </div>
            <div class="row d-flex align-items-center flex-wrap mt20 mt-md40">
               <div class="col-md-10 col-12 mx-auto">
			   <img src="assets/images/product-box.webp" alt="ProductBOX " class="img-fluid d-block mx-auto" />
                  <!--<div class="responsive-video">
                     <iframe src="https://jobiin.dotcompal.com/video/embed/9vxq6c88sw" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                        box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                  </div>-->
               </div>
               <!-- <div class="col-md-5 col-12 mt20 mt-md0 min-md-video-width-right">
                  <div class="features-wrap">
                     <ul class="list-head pl0 m0 f-18 lh150 w500 white-clr">
                        <li>100% Control on Your Video Traffic &amp; Channels</li>
                        <li>Close More Prospects for Your Agency Services</li>
                        <li>Boost Commissions, Customer Satisfaction &amp; Sales Online</li>
                        <li>Tap Into $398 Billion E-Learning Industry</li>
                        <li>Tap Into Huge 82% Of Overall Traffic on Internet </li>
                        <li>Boost Your Clients Sales–Commercial License Included</li>
                     </ul>
                  </div>
                  </div>-->
            </div>
         </div>
         <img src="https://cdn.oppyo.com/launches/jobiin/common_assets/images/header-left-element.webp" alt="Header Element " class="img-fluid d-none header-left-element "/>
         <img src="https://cdn.oppyo.com/launches/jobiin/common_assets/images/header-right-element.webp" alt="Header Element " class="img-fluid d-none header-right-element " />
      </div>
      <!--Header Section End -->
      <div class="second-section">
         <div class="container">
            <div class="row">
               <div class="col-12 z-index-9">
                  <div class="row header-list-block">
                     <div class="col-12 col-md-6">
                        <ul class="features-list pl0 f-18 f-md-20 lh150 w500 black-clr text-capitalize">
                           <li>Create Automated Job Search Site and <span class="w700">Tap into the $212 Billion Recruitment Industry</span> </li>
                           <li class="w700">Automatically Update Job Listing on your Site Every Hour from 10+ Million Jobs </li>
                           <li>Get Job Listings from <span class="w700">Global Giants Like Amazon, Walmart, Costco, etc.</span> </li>
                           <li><span class="w700">Get SEO Optimised Blog Articles</span> and Fresh Content Built-In.</li>
                           <li>Made For Newbies & Experienced Marketers- <span class="w700">Even Non-Techies Can Start Benefiting Right Away</span></li>
                           <li>Get Started Immediately – <span class="w700">Launch Done for You Job Site and Start Making Money Right Away</span> </li>
                        </ul>
                     </div>
                     <div class="col-12 col-md-6 mt10 mt-md0">
                        <ul class="features-list pl0 f-18 f-md-20 lh150 w500 black-clr text-capitalize">
                           <li><span class="w700">Get Affiliate Commissions Paid on Every Click</span> of Job Listings from Top Recruitment Companies</li>
                           <li><span class="w700">ZERO Tech Or Marketing Experience Required</span> For You To Make Money Online…</li>
                           <li><span class="w700">Promote Yours & Affiliate Offers through Banner and Text Ads</span> to Get Tons in Commission</li>
                           <li><span class="w700">Get Unlimited Free Search and Social Traffic </span>on Your Website and Offers </li>
                           <li>Easy And Intuitive To Use Software With <span class="w700">Step By Step Video Training</span></li>
                           <li><span class="w700">100% Newbie Friendly with No Installation,</span> No Tech Hassle, and No Prior Skills Required </li>
                        </ul>
                     </div>
                     <div class="col-12 mt10 mt-md20">
                        <ul class="features-list pl0 f-18 f-md-20 lh150 w500 black-clr text-capitalize">
                           <li class="w700">PLUS, FREE COMMERCIAL LICENSE IF YOU ACT TODAY!</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div> 
         </div>
      </div>
      <!---CTA-->
      <div class="cta-section">
         <div class="container">
           <div class="row">
               <div class="col-12">
                  <!-- <div class="f-22 f-md-28 w700 lh150 text-center white-clr">
                     Limited Free Commercial License. No Monthly Fee
                  </div>
                  <div class="f-18 f-md-20 lh150 w500 text-center mt10 white-clr">
                  Use Coupon Code <span class="w700 orange-clr">"JOBiin" </span> for Additional <span class="w700 orange-clr">$3 Discount</span> on Entire Funnel
                  </div> -->
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center">
                        <a href="#book-seat1" class="cta-link-btn">Subscribe To EarlyBird VIP List</a>
                     </div>
                  </div>
                  <div class="col-12 mx-auto mt20 mt-md40 f-16 f-md-18 w400 white-clr text-left lh160 text-md-start">
                    <ul class="ul-tick-icon1 p0 m0">
                        <li>Don't Pay $17/M. Get All Benefits For $17 One Time Only</li>
                        <li>Also Get A Chance To Win 10 FREE Copies</li>
                    </ul>
                </div>
               </div>
            </div>
         </div>
      </div>
     <!-------Form------------->

     <div class="form-sec">
         <div class="container">
            <div class="row mt20 mt-md70 mb20 mb-md70">
               <div class="col-12 col-md-12 mx-auto" id="form">
                  <div class="auto_responder_form inp-phold formbg" id="book-seat1">
                     <div class="f-24 f-md-32 w600 text-center lh150 black-clr" editabletype="text" style="z-index:10;">
                        So Make Sure You Do NOT Miss This Webinar 							
                     </div>
                     <div class="col-12 f-md-22 f-20 w600 lh140 mx-auto my20 text-center">
                        Register Now! And Join Us Live On June 15th, 10:00 AM EST
                     </div>
                     <!-- Aweber Form Code -->
                     <div class="col-12 p0 mt15 mt-md25" editabletype="form" style="z-index:10">
                        <!-- Aweber Form Code -->
                        <form method="post" class="af-form-wrapper register-form-style1 " accept-charset="UTF-8" action="https://www.aweber.com/scripts/addlead.pl" style="z-index: 10;">
                           <div style="display: none;">
                              <input type="hidden" name="meta_web_form_id" value="1357456490">
                              <input type="hidden" name="meta_split_id" value="">
                              <input type="hidden" name="listname" value="awlist6287876">
                              <input type="hidden" name="redirect" value="https://www.getjobiin.co/prelaunch-thankyou" id="redirect_f29b27d1bf0dc9d5936b571b0ffb0da5">
                              <input type="hidden" name="meta_adtracking" value="Prela_Form_Code_(Hardcoded)">
                              <input type="hidden" name="meta_message" value="1">
                              <input type="hidden" name="meta_required" value="name,email">
                              <input type="hidden" name="meta_tooltip" value="">
                           </div>
                           <div id="af-form-1357456490" class="af-form">
                              <div id="af-body-1357456490" class="af-body af-standards">
                                 <div class="af-element width-form">
                                    <label class="previewLabel" for="awf_field-114268442"></label>
                                    <div class="af-textWrap">
                                       <div class="input-type" style="z-index: 11;">
                                          <!--<span class="input-group-addon border1px"><i class="fa fa-user" aria-hidden="true"></i></span>-->
                                          <input id="awf_field-114268442" type="text" name="name" class="text form-control custom-input input-field" value="" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="500" placeholder="Your Name">
                                       </div>
                                    </div>
                                    <div class="af-clear bottom-margin"></div>
                                 </div>
                                 <div class="af-element width-form">
                                    <label class="previewLabel" for="awf_field-114268443"> </label>
                                    <div class="af-textWrap">
                                       <div class="input-type" style="z-index: 11;">
                                          <!--<span class="input-group-addon border1px"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>-->
                                          <input class="text form-control custom-input input-field" id="awf_field-114268443" type="text" name="email" value="" tabindex="501" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " placeholder="Your Email">
                                       </div>
                                    </div>
                                    <div class="af-clear bottom-margin"></div>
                                 </div>
                                 <input type="hidden" id="awf_field_aid" class="text" name="custom aid" value="undefined" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="502">
                                 <div class="af-element buttonContainer button-type register-btn1 f-22 f-md-21 width-form mt-md0">
                                    <input name="submit" id="af-submit-image-348552691" type="submit" class="image" style="max-width: 100%;" alt="Submit Form" tabindex="503" value="Subscribe To EarlyBird VIP List">
                                    <div class="af-clear bottom-margin"></div>
                                 </div>
                              </div>
                           </div>
                           <div style="display: none;"><img src="https://forms.aweber.com/form/displays.htm?id=TIws7ExsjCwMzA==" alt=""></div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
     <!-------Form Section------------->
      <!-- FAQ Section End -->
      <div class="footer-section">
         <div class="container ">
            <div class="row">
               <div class="col-12 text-center">
                  <svg id="Layer_1 " xmlns="http://www.w3.org/2000/svg " viewBox="0 0 128.19 38 " style="max-height:55px;">
                     <defs>
                        <style>.cls-1,.cls-2{fill:#fff;}.cls-2{fill-rule:evenodd;}.cls-3{fill:#0a52e2;}</style>
                     </defs>
                     <rect class="cls-1
                        " x="79.21 " width="48.98 " height="38 " rx="4 " ry="4 "/>
                     <path class="cls-1 " d="M18.64,4.67V24.72c0,1.29-.21,2.47-.63,3.54-.42,1.07-1.03,1.97-1.84,2.71-.81,.74-1.79,1.32-2.93,1.74-1.15,.42-2.45,.63-3.9,.63s-2.64-.19-3.7-.57c-1.07-.38-1.98-.92-2.75-1.62-.77-.7-1.39-1.54-1.88-2.51S.19,26.6,0,25.41l5.74-1.13c.46,2.45,1.63,3.68,3.52,3.68,.89,0,1.65-.28,2.28-.85,.63-.57,.95-1.46,.95-2.67V9.6H3.92V4.67h14.72Z
                        "/>
                     <path class="cls-1 " d="M54.91,33.37V4.63h9.71c3.38,0,6.02,.66,7.92,1.97,1.9,1.32,2.84,3.28,2.84,5.9,0,1.33-.35,2.52-1.06,3.56-.7,1.05-1.73,1.83-3.07,2.36,1.72,.37,3.02,1.16,3.88,2.37,.86,1.21,1.29,2.61,1.29,4.21,0,2.75-.91,4.83-2.72,6.25-1.82,1.42-4.39,2.12-7.72,2.12h-11.08Zm5.76-16.7h4.15c1.54,0,2.72-.32,3.55-.95,.83-.63,1.24-1.55,1.24-2.76,0-1.33-.42-2.31-1.25-2.94-.84-.63-2.08-.95-3.74-.95h-3.95v7.6Zm0,3.99v8.27h5.31c1.53,0,2.69-.33,3.49-.99,.8-.66,1.2-1.64,1.2-2.94,0-1.4-.34-2.48-1.03-3.22-.68-.74-1.76-1.11-3.24-1.11h-5.75Z
                        "/>
                     <g>
                        <path class="cls-1 " d="M46.84,4.19C42.31-.12,36.87-1.08,31.16,1.2c-5.82,2.33-9.1,6.88-9.52,13.21-.41,6.17,2.57,10.9,6.84,15.15,.95-.84,1.22-1.22,2.01-2.05-.79-.65-1.41-1.09-2.43-2.11-5.86-6.15-5.78-15.66,1.04-20.46,5-3.52,11.71-3.13,16.37,.95,4.29,3.75,5.54,10.44,2.59,15.61-1.2,2.11-3.09,3.84-4.86,5.98,.28,.45,.72,1.18,1.44,2.35,1.77-2.01,3.11-3.72,4.38-5.63,4.39-6.6,3.56-14.59-2.17-20.02Z
                           "/>
                        <g>
                           <path class="cls-2 " d="M39.67,27.34c-2.49,.37-4.9,.52-7.23-.46-.54-.23-1.13-1.06-1.18-1.65-.3-3.69,1.25-5.27,4.98-5.26,.31,0,.62,0,.92,.01,5.31-.01,5.96,6.85,2.51,7.36Z "/>
                           <path class="cls-2 " d="M39.49,16.69c-.04,1.69-1.37,2.98-3.06,2.98-1.6,0-3.01-1.43-3.01-3.06s1.39-3.04,3.02-3.03c1.75,0,3.1,1.38,3.06,3.12Z
                              "/>
                        </g>
                        <g>
                           <g>
                              <path class="cls-2 " d="M42.93,26.23c-.67-3.46-.93-5.25-3.46-7.18,.63-1.21,1.3-1.83,2.1-2,2.43-.53,3.98,.21,4.84,1.97,.66,1.34,.78,2.29-.37,3.77-1.03,1.17-1.85,2.21-3.11,3.43Z "/>
                              <path class="cls-2 " d="M44.94,13.4c.01,1.6-1.27,2.91-2.88,2.92-1.6,.01-2.91-1.28-2.92-2.88-.01-1.6,1.28-2.9,2.88-2.92,1.6-.01,2.91,1.28,2.92,2.88Z
                                 "/>
                           </g>
                           <g>
                              <path class="cls-2 " d="M30.1,26.23c.67-3.46,.93-5.25,3.46-7.18-.63-1.21-1.3-1.83-2.1-2-2.43-.53-3.98,.21-4.84,1.97-.66,1.34-.88,2.01,.27,3.49,.96,1.3,1.88,2.44,3.21,3.71Z "/>
                              <path class="cls-2 " d="M28.08,13.4c0,1.6,1.28,2.91,2.88,2.92,1.6,.01,2.91-1.28,2.92-2.88,.01-1.6-1.27-2.9-2.88-2.92-1.6-.01-2.91,1.28-2.92,2.88Z
                                 "/>
                           </g>
                        </g>
                        <path class="cls-1 " d="M42.02,27.74c-.7,.41-6.95,2.1-10.73,.08l-2.22,2.57c.55,.61,5.12,5.27,7.55,7.6,2.22-2.17,7.2-7.37,7.2-7.37l-1.8-2.89Z "/>
                     </g>
                     <g>
                        <rect class="cls-3 " x="85.05 " y="4.63 " width="5.38 " height="5.4 " rx="2.69
                           " ry="2.69 "/>
                        <rect class="cls-3 " x="85.05 " y="13.43 " width="5.38 " height="19.94 "/>
                        <rect class="cls-3 " x="95.13 " y="4.63 " width="5.38 " height="5.4 " rx="2.69 " ry="2.69 "/>
                        <rect class="cls-3 " x="95.13 " y="13.43
                           " width="5.38 " height="19.94 "/>
                        <path class="cls-3 " d="M109.85,13.43l.24,2.86c.66-1.02,1.48-1.81,2.45-2.38,.97-.56,2.06-.85,3.26-.85,2.01,0,3.59,.63,4.72,1.9,1.13,1.27,1.7,3.25,1.7,5.95v12.46h-5.4v-12.47c0-1.34-.27-2.29-.81-2.85-.54-.56-1.36-.84-2.45-.84-.71,0-1.35,.14-1.92,.43-.57,.29-1.04,.7-1.42,1.23v14.5h-5.38V13.43h5.01Z "/>
                     </g>
                  </svg>
                  <div class="f-14 f-md-16 w500 mt20 lh150 white-clr text-start" style="color:#7d8398;"><span class="w700">Note:</span> This site is not a part of the Facebook website or Facebook Inc. Additionally, this site is NOT endorsed by Facebook in any way. FACEBOOK & INSTAGRAM are the trademarks of FACEBOOK, Inc.<br><br>
                     <span class="w700">Earning Disclaimer:</span> Please make your purchase decision wisely. There is no promise or representation that you will make a certain amount of money, or any money, or not lose money, as a result of using
                     our products and services. Any stats, conversion, earnings, or income statements are strictly estimates. There is no guarantee that you will make these levels for yourself. As with any business, your results will vary and will
                     be based on your personal abilities, experience, knowledge, capabilities, level of desire, and an infinite number of variables beyond our control, including variables we or you have not anticipated. There are no guarantees concerning
                     the level of success you may experience. Each person’s results will vary. There are unknown risks in any business, particularly with the Internet where advances and changes can happen quickly. The use of the plug-in and the given
                     information should be based on your own due diligence, and you agree that we are not liable for your success or failure.
                  </div>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-md-18 f-16 w400 lh130 white-clr text-xs-center">Copyright © JOBiin 2022</div>
                  <ul class="footer-ul w400 f-md-18 f-16 white-clr text-center text-md-right">
                     <li><a href="https://support.bizomart.com/hc/en-us " class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://getjobiin.co/legal/privacy-policy.html " class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://getjobiin.co/legal/terms-of-service.html " class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://getjobiin.co/legal/disclaimer.html " class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://getjobiin.co/legal/gdpr.html " class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://getjobiin.co/legal/dmca.html " class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://getjobiin.co/legal/anti-spam.html " class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div><script type="text/javascript">
// Special handling for in-app browsers that don't always support new windows
(function() {
    function browserSupportsNewWindows(userAgent) {
        var rules = [
            'FBIOS',
            'Twitter for iPhone',
            'WebView',
            '(iPhone|iPod|iPad)(?!.*Safari\/)',
            'Android.*(wv|\.0\.0\.0)'
        ];
        var pattern = new RegExp('(' + rules.join('|') + ')', 'ig');
        return !pattern.test(userAgent);
    }

    if (!browserSupportsNewWindows(navigator.userAgent || navigator.vendor || window.opera)) {
        document.getElementById('af-form-1357456490').parentElement.removeAttribute('target');
    }
})();
</script>
<script type="text/javascript">
    <!--
    (function() {
        var IE = /*@cc_on!@*/false;
        if (!IE) { return; }
        if (document.compatMode && document.compatMode == 'BackCompat') {
            if (document.getElementById("af-form-1357456490")) {
                document.getElementById("af-form-1357456490").className = 'af-form af-quirksMode';
            }
            if (document.getElementById("af-body-1357456490")) {
                document.getElementById("af-body-1357456490").className = "af-body inline af-quirksMode";
            }
            if (document.getElementById("af-header-1357456490")) {
                document.getElementById("af-header-1357456490").className = "af-header af-quirksMode";
            }
            if (document.getElementById("af-footer-1357456490")) {
                document.getElementById("af-footer-1357456490").className = "af-footer af-quirksMode";
            }
        }
    })();
    -->
</script>
   </body>
</html>
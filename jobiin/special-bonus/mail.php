<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <!-- Tell the browser to be responsive to screen width -->
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
      <meta name="title" content="JOBiin Bonuses">
      <meta name="description" content="JOBiin Bonuses">
      <meta name="keywords" content="">
      <meta property="og:image" content="https://getjobiin.co/special-bonus/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Ayush Jain">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="JOBiin Bonuses">
      <meta property="og:description" content="">
      <meta property="og:image" content="https://getjobiin.co/special-bonus/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="JOBiin Bonuses">
      <meta property="twitter:description" content="">
      <meta property="twitter:image" content="https://getjobiin.co/special-bonus/thumbnail.png">
      <title>JOBiin Bonuses</title>
      <link rel="icon" href="https://cdn.oppyo.com/launches/jobiin/common_assets/images/favicon.png" type="image/png">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700;800;900&display=swap" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css2?family=Jost:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&amp;family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&amp;display=swap" rel="stylesheet">
      <link rel="stylesheet" href="https://cdn.oppyo.com/launches/jobiin/common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" href="assets/css/timer.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style.css" type="text/css">
      <link rel="stylesheet" href="assets/css/bonus-style.css" type="text/css">
      <script src="https://cdn.oppyo.com/launches/jobiin/common_assets/js/jquery.min.js"></script>
   </head>
   <body>
      <!-- New Timer  Start-->
      <?php
         $date = 'June 16 2022 11:00 AM EST';
         $exp_date = strtotime($date);
         $now = time();  
         /*
         
         $date = date('F d Y g:i:s A eO');
         $rand_time_add = rand(700, 1200);
         $exp_date = strtotime($date) + $rand_time_add;
         $now = time();*/
         
         if ($now < $exp_date) {
         ?>
      <?php
         } else {
             echo "Times Up";
         }
         ?>
      <!-- New Timer End -->
      <?php
         if(!isset($_GET['afflink'])){
         $_GET['afflink'] = 'https://cutt.ly/NJ8Aq6H';
         $_GET['name'] = 'Atul & Achal';      
         }
         ?>
      <div class="main-header">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="text-center">
                     <div class="f-md-32 f-20 lh140 w500 white-clr d-block d-md-flex align-items-center justify-content-center">
                        <span class="w700"><?php echo $_GET['name'];?>'s</span> &nbsp;special bonus for &nbsp;
                        <svg id="Layer_1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 128.19 38" style="max-height:55px;">
                           <defs>
                              <style>.cls-1,.cls-2{fill:#fff;}.cls-2{fill-rule:evenodd;}.cls-3{fill:#0a52e2;}</style>
                           </defs>
                           <rect class="cls-1" x="79.21" width="48.98" height="38" rx="4" ry="4"></rect>
                           <path class="cls-1" d="M18.64,4.67V24.72c0,1.29-.21,2.47-.63,3.54-.42,1.07-1.03,1.97-1.84,2.71-.81,.74-1.79,1.32-2.93,1.74-1.15,.42-2.45,.63-3.9,.63s-2.64-.19-3.7-.57c-1.07-.38-1.98-.92-2.75-1.62-.77-.7-1.39-1.54-1.88-2.51S.19,26.6,0,25.41l5.74-1.13c.46,2.45,1.63,3.68,3.52,3.68,.89,0,1.65-.28,2.28-.85,.63-.57,.95-1.46,.95-2.67V9.6H3.92V4.67h14.72Z"></path>
                           <path class="cls-1" d="M54.91,33.37V4.63h9.71c3.38,0,6.02,.66,7.92,1.97,1.9,1.32,2.84,3.28,2.84,5.9,0,1.33-.35,2.52-1.06,3.56-.7,1.05-1.73,1.83-3.07,2.36,1.72,.37,3.02,1.16,3.88,2.37,.86,1.21,1.29,2.61,1.29,4.21,0,2.75-.91,4.83-2.72,6.25-1.82,1.42-4.39,2.12-7.72,2.12h-11.08Zm5.76-16.7h4.15c1.54,0,2.72-.32,3.55-.95,.83-.63,1.24-1.55,1.24-2.76,0-1.33-.42-2.31-1.25-2.94-.84-.63-2.08-.95-3.74-.95h-3.95v7.6Zm0,3.99v8.27h5.31c1.53,0,2.69-.33,3.49-.99,.8-.66,1.2-1.64,1.2-2.94,0-1.4-.34-2.48-1.03-3.22-.68-.74-1.76-1.11-3.24-1.11h-5.75Z"></path>
                           <g>
                              <path class="cls-1" d="M46.84,4.19C42.31-.12,36.87-1.08,31.16,1.2c-5.82,2.33-9.1,6.88-9.52,13.21-.41,6.17,2.57,10.9,6.84,15.15,.95-.84,1.22-1.22,2.01-2.05-.79-.65-1.41-1.09-2.43-2.11-5.86-6.15-5.78-15.66,1.04-20.46,5-3.52,11.71-3.13,16.37,.95,4.29,3.75,5.54,10.44,2.59,15.61-1.2,2.11-3.09,3.84-4.86,5.98,.28,.45,.72,1.18,1.44,2.35,1.77-2.01,3.11-3.72,4.38-5.63,4.39-6.6,3.56-14.59-2.17-20.02Z"></path>
                              <g>
                                 <path class="cls-2" d="M39.67,27.34c-2.49,.37-4.9,.52-7.23-.46-.54-.23-1.13-1.06-1.18-1.65-.3-3.69,1.25-5.27,4.98-5.26,.31,0,.62,0,.92,.01,5.31-.01,5.96,6.85,2.51,7.36Z"></path>
                                 <path class="cls-2" d="M39.49,16.69c-.04,1.69-1.37,2.98-3.06,2.98-1.6,0-3.01-1.43-3.01-3.06s1.39-3.04,3.02-3.03c1.75,0,3.1,1.38,3.06,3.12Z"></path>
                              </g>
                              <g>
                                 <g>
                                    <path class="cls-2" d="M42.93,26.23c-.67-3.46-.93-5.25-3.46-7.18,.63-1.21,1.3-1.83,2.1-2,2.43-.53,3.98,.21,4.84,1.97,.66,1.34,.78,2.29-.37,3.77-1.03,1.17-1.85,2.21-3.11,3.43Z"></path>
                                    <path class="cls-2" d="M44.94,13.4c.01,1.6-1.27,2.91-2.88,2.92-1.6,.01-2.91-1.28-2.92-2.88-.01-1.6,1.28-2.9,2.88-2.92,1.6-.01,2.91,1.28,2.92,2.88Z"></path>
                                 </g>
                                 <g>
                                    <path class="cls-2" d="M30.1,26.23c.67-3.46,.93-5.25,3.46-7.18-.63-1.21-1.3-1.83-2.1-2-2.43-.53-3.98,.21-4.84,1.97-.66,1.34-.88,2.01,.27,3.49,.96,1.3,1.88,2.44,3.21,3.71Z"></path>
                                    <path class="cls-2" d="M28.08,13.4c0,1.6,1.28,2.91,2.88,2.92,1.6,.01,2.91-1.28,2.92-2.88,.01-1.6-1.27-2.9-2.88-2.92-1.6-.01-2.91,1.28-2.92,2.88Z"></path>
                                 </g>
                              </g>
                              <path class="cls-1" d="M42.02,27.74c-.7,.41-6.95,2.1-10.73,.08l-2.22,2.57c.55,.61,5.12,5.27,7.55,7.6,2.22-2.17,7.2-7.37,7.2-7.37l-1.8-2.89Z"></path>
                           </g>
                           <g>
                              <rect class="cls-3" x="85.05" y="4.63" width="5.38" height="5.4" rx="2.69" ry="2.69"></rect>
                              <rect class="cls-3" x="85.05" y="13.43" width="5.38" height="19.94"></rect>
                              <rect class="cls-3" x="95.13" y="4.63" width="5.38" height="5.4" rx="2.69" ry="2.69"></rect>
                              <rect class="cls-3" x="95.13" y="13.43" width="5.38" height="19.94"></rect>
                              <path class="cls-3" d="M109.85,13.43l.24,2.86c.66-1.02,1.48-1.81,2.45-2.38,.97-.56,2.06-.85,3.26-.85,2.01,0,3.59,.63,4.72,1.9,1.13,1.27,1.7,3.25,1.7,5.95v12.46h-5.4v-12.47c0-1.34-.27-2.29-.81-2.85-.54-.56-1.36-.84-2.45-.84-.71,0-1.35,.14-1.92,.43-.57,.29-1.04,.7-1.42,1.23v14.5h-5.38V13.43h5.01Z"></path>
                           </g>
                        </svg>
                     </div>
                  </div>
                  <div class="col-12 mt20 mt-md50 text-center">
                     <div class="pre-heading f-md-20 f-16 w600 lh130">
                        Grab My Exclusive Bonuses Before the Deal Ends… 
                     </div>
                  </div>
                  <div class="col-12 mt-md25 mt20 f-md-40 f-26 w500 text-center white-clr lh140">
                        Revealing A Breakthrough Software That <br class="d-none d-md-block"> <span class="w700 orange-clr f-md-45"> Makes Consistent $328/Day in Commissions</span><br class="d-none d-md-block"> by Creating 100% Automated JOB Search Sites Pre-Loaded with 10 Million+ Jobs in Just 60 Seconds...
                        </div>
                  <div class="col-12 mt-md25 mt20 f-18 f-md-24 w600 text-center lh130 white-clr">
                     Watch My Quick Review Video
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md30">
               <div class="col-12 col-md-10 mx-auto">
                    <!-- <img src="https://cdn.oppyo.com/launches/jobiin/special-bonus/product-box.webp" class="img-fluid mx-auto d-block" alt="ProductBox"> -->
                    <div class="responsive-video">
                     <iframe src="https://jobiin.dotcompal.com/video/embed/4jho6ppp1q" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                        box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                     </div>
               </div>
            </div>
         </div>
         <img src="https://cdn.oppyo.com/launches/jobiin/common_assets/images/header-left-element.webp" alt="Header Element " class="img-fluid d-none header-left-element " />
         <img src="https://cdn.oppyo.com/launches/jobiin/common_assets/images/header-right-element.webp" alt="Header Element " class="img-fluid d-none header-right-element " />
      </div>
      <!-- Header Section Start -->
      <div class="second-section">
            <div class="container">
            <div class="row">
               <div class="col-12 z-index-9">
                  <div class="row header-list-block">
                     <div class="col-12 col-md-6">
                     <ul class="features-list pl0 f-18 f-md-20 lh150 w500 black-clr text-capitalize">
                        <li><span class="w700">Tap into the $212 Billion Recruitment Industry</span></li>
<li><span class="w700">Create Multiple UNIQUE Job Sites</span> – Niche Focused (Automotive, Banking Jobs etc.) or City Focused (New York, London Jobs etc.)</li>
<li><span class="w700">100% Automated Job Posting & Updates</span> on Your Site from 10 million+ Open Jobs</li>
<li>Get Job Listed from TOP Brands & Global Giants <span class="w700">Like Amazon, Walmart, Costco, etc.</span></li>
<li>Create Website in <span class="w700">13 Different Languages & Target 30+ Top Countries</span></li>
<li class="w700">Built-In FREE Search and Social Traffic from Google & 80+ Social Media Platforms</li>
<li class="w700">Built-In SEO Optimised Fresh Content & Blog Articles</li>
                        </ul>
                     </div>
                     <div class="col-12 col-md-6 mt10 mt-md0">
                     <ul class="features-list pl0 f-18 f-md-20 lh150 w500 black-clr text-capitalize">
                        <li class="w700">Make Commissions from Top Recruitment Companies on Every Click of Job Listings</li>
<li>Promote TOP Affiliate Offers Through <span class="w700">Banner and Text Ads for Extra Commission</span></li>
<li>Even Apply Yourself for Any <span class="w700">Good Part-Time Job to Make Extra Income</span></li>
<li>Easy And Intuitive to Use Software with <span class="w700">Step-by-Step Video Training</span></li>
<li><span class="w700">Get Started Immediately</span> – Launch Done-for-You Job Site and Start Making Money Right Away</li>
<li><span class="w700">Made For Absolute Newbies</span> & Experienced Marketers</li>
<li class="w700">PLUS, FREE COMMERCIAL LICENSE IF YOU Start TODAY!</li>

                        </ul>
                     </div>
                  </div>
               </div>
            </div>
                <div class="row mt20 mt-md70">
                    <!-- CTA Btn Section Start -->
                    <div class="col-md-12 col-md-12 col-12 text-center ">
                        <div class="f-md-24 f-20 text-center mt3 black black-clr lh120 w500">Don't wait for the time when I will pull these bonuses away…</div>
                        <div class="f-md-36 f-22 text-center  lh120 w700 mt20 mt-md20 black-clr">TAKE ACTION NOW!</div>
                        <div class="f-18 f-md-22 lh150 w500 text-center mt20 black-clr">
                            Use Coupon Code <span class="w700 blue-clr">"JOBiin"</span> for Additional <span class="w700 blue-clr">$3 Discount</span> on Entire Funnel
                        </div>
                    </div>
                    <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                        <a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
                            <span class="text-center">Grab JOBiin + My 20 Exclusive Bonuses</span> <br>
                        </a>
                    </div>
                    <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
                        <img src="https://cdn.oppyo.com/launches/jobiin/special-bonus/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
                    </div>
                    <div class="col-12 mt15 mt-md20 text-center" >
                        <h3 class="f-md-35 f-md-28 f-20 w600 text-center black">Coupon Is Expiring In... </h3>
                    </div>
                    <!-- Timer -->
                    <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                        <div class="countdown counter-black">
                            <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w600 smmltd">Days</span> </div>
                            <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w600 smmltd">Hours</span> </div>
                            <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w600 smmltd">Mins</span> </div>
                            <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w600 smmltd">Sec</span> </div>
                        </div>
                    </div>
                    <!-- Timer End -->
                    <!-- CTA Button Section End   -->
                </div>
            </div>
        </div>
        <!-- Header Section End -->

         <!-- Step Section -->
      <div class="step-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-32 f-24 lh140 w500 black-clr text-center">					
               Create Your 100% Automated & Profitable Job Sites  
               </div>
               <div class="col-12 f-md-50 f-28 lh140 w700 black-clr text-center">				
               in Just 3 Easy Steps 
               </div>
            </div>
            <div class="row">
               <div class="col-md-4 col-12 mt-md50 mt20">
                  <div class="step-bg text-center">
                  <div class="step-shape f-22 f-md-28 w700 black-clr lh140">Step 1</div>
                     <img src="https://cdn.oppyo.com/launches/jobiin/special/step-icon1.webp" class="img-fluid d-block mx-auto mt20" alt="Step One">
                     <div class="f-26 f-md-30 w700 lh140 mt20 text-shadow1">
                      Add Your Details 
                     </div>                    
                  </div>
               </div>
               <div class="col-md-4 col-12 mt-md50 mt20 ">
                  <div class="step-bg text-center">
                  <div class="step-shape f-22 f-md-28 w700 black-clr lh140">Step 2</div>
                     <img src="https://cdn.oppyo.com/launches/jobiin/special/step-icon2.webp" class="img-fluid d-block mx-auto mt20" alt="Step Two">
                     <div class="f-26 f-md-30 w700 lh140 mt20 text-shadow1">
                     Enter Affiliate IDs 
                     </div>
                   
                  </div>
               </div>
               <div class="col-md-4 col-12 mt-md50 mt20 ">
               <div class="step-bg text-center">
                  <div class="step-shape f-22 f-md-28 w700 black-clr lh140">Step 3</div>
                     <img src="https://cdn.oppyo.com/launches/jobiin/special/step-icon3.webp" class="img-fluid d-block mx-auto mt20" alt="Step Three">
                     <div class="f-26 f-md-30 w700 lh140 mt20 text-shadow1">
                      Publish & Profit 
                     </div>                   
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Step Section End -->
      <!-- Proof Section -->
      <div class="proof-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-40 f-28 lh140 w700 black-clr text-center">				
               JOBiin Site Is Making $328/ Day Over and Over Again  
               </div>
               <div class="col-12 col-md-12 mx-auto mt20 mt-md50">
                  <img src="https://cdn.oppyo.com/launches/jobiin/special-bonus/proof.webp" class="img-fluid d-block mx-auto" alt="Proof">
               </div>
            </div>
         </div>
      </div>
      <!-- Proof Section End -->
      <!-- Testimonials Section  -->
      <div class="testimonial-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-42 f-28 lh150 w700 text-center black-clr"> 
               Checkout What JOBiin Early Users Have To Say 
               </div>
            </div>
            <div class="row row-cols-md-2 row-cols-1 mt-md70 mt20">
               <div class="col p-md-0 mt20">
                  <img src="https://cdn.oppyo.com/launches/jobiin/special/test-1.webp" class="img-fluid d-block mx-auto" alt="Testimonial">
               </div>
               <div class="col p-md-0 mt20">
                  <img src="https://cdn.oppyo.com/launches/jobiin/special/test-2.webp" class="img-fluid d-block mx-auto" alt="Testimonial">
               </div>
               <div class="col p-md-0 mt20">
                  <img src="https://cdn.oppyo.com/launches/jobiin/special/test-3.webp" class="img-fluid d-block mx-auto" alt="Testimonial">
               </div>
               <div class="col p-md-0 mt20">
                  <img src="https://cdn.oppyo.com/launches/jobiin/special/test-4.webp" class="img-fluid d-block mx-auto" alt="Testimonial">
               </div>
            </div>
         </div>
      </div>
      <!-- Testimonials Section End -->
      <!-- Let Face Section Start -->
      <div class="letface-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-30 f-md-40 w800 lh140 let-shape text-uppercase">
                     <i>Did You Know! </i>
                  </div>
               </div>
               <div class="col-12 f-md-50 f-28 w700 lh130 text-center white-clr mt10">
               More Than 10.7 million People Are Currently Searching for Jobs in USA Alone! 
               </div>
               <div class="col-12 f-18 w500 lh150 white-clr mt25">
                  <div class="row align-items-center">
                     <div class="col-12 col-md-6">
                     And that’s just for the USA. If you add the numbers from the other top 30 countries of the world, <br><br>

                     you won’t be able to believe what you see. <br><br>

                     As the after-effects of Covid 19, approx. 9.6 million (57 percent) were unable to work because their employer closed or lost business due to the pandemic <br><br>

                     Just to add on to this, <span class="w700">the Global Pandemic has forced almost 78% Americans to either quit their existing job or search for a better one in hindsight.</span> And that’s one of the biggest reasons why the job search industry will continue to rise for months & years to come… <br><br>

                     <span class="w700">Yes! With more and more people looking to quit their jobs & settle into the remote work culture,</span> this industry will be at BOOM in this decade & shows no signs of slowing down. 
                     </div>
                     <div class="col-12 col-md-6"><img src="https://cdn.oppyo.com/launches/jobiin/special/sad.webp" class="img-fluid d-block mx-auto"></div>
                  </div>
               </div>
               <div class="col-12 f-md-50 f-28 w700 lh130 text-center white-clr mt20 mt-md50">
                  And Therefore, Job Search Industry Presents<br class="d-none d-md-block"> A HUGE Growth Opportunity! 
               </div>
               <div class="col-12 f-18 w500 lh150  white-clr mt25">
                  <div class="row align-items-center">
                     <div class="col-12 col-md-6 order-md-2">
                        <div class="f-22 f-md-28 w700 lh150 text-center text-md-start white-clr mt15 mt-md25">
                           <span class="highlight1">Yes, It’s Very High In-Demand!  </span>
                        </div>
                        <div class="mt20">
                        <span class="w700"> Thus, people are hungrily searching for an additional source of income that helps them to curb the ever-increasing inflation</span> as well as have sufficient financial resources to plan for any unavoidable contingencies. <br><br>

And that’s what we’re addressing here… <br><br>

<span class="w700"> There is a HUGE untapped opportunity for you to help millions of people by creating your own UNIQUE job sites</span> as well as have a second source of income without any huge investment of time and money or without having any prior knowledge or expertise.  <br><br>

And there is no limit in sight!  <br><br>

You can create job sites & you’ll have millions flocking on them in search on their dream jobs. The best part, you get paid as soon as they click on the desired job listing. 
                        </div>
                     </div>
                     <div class="col-12 col-md-6 order-md-1"><img src="https://cdn.oppyo.com/launches/jobiin/special/happy.webp" class="img-fluid d-block mx-auto"></div>
                  </div>
               </div>
            </div>
            <div class="impressive-shape col-md-10 mx-auto mt20 mt-md40">
               <div class="row">
               <div class="col-12 f-18 f-md-24 lh150 w600 text-center white-clr lh150 mt20">
               So, it is the perfect time to get in and profit from this exponential growth!
                  </div>
                  <div class="col-12 text-center mt20">
                     <div class="f-36 f-md-60 w800 lh120 orange-clr">
                        <i>Impressive, right? </i>
                     </div>
                  </div>
                 
                  <div class="col-12  f-22 f-md-32 lh140 w700 text-center white-clr lh150 mt20 mt-md30">
                  So, how would you like to get your share? 
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Let Face Section End -->
 <!-- Proof Section -->
 <div class="proof-section1">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-50 f-28 w500 lh130 text-center black-clr">
                 <span class="w700"> Atul & Achal
 Made $7,975 In Commissions in Last 30 Days…
</span> Following This Proven Trend… 
               </div>
               <div class="col-12 col-md-12 mx-auto mt20 mt-md50">
                  <img src="https://cdn.oppyo.com/launches/jobiin/special/proof1.webp" class="img-fluid d-block mx-auto" alt="Proof">
               </div>
               <!-- <div class="col-12 col-md-12 mx-auto mt20 mt-md50">
                  <img src="https://cdn.oppyo.com/launches/jobiin/special/proof2.webp" class="img-fluid d-block mx-auto" alt="Proof">
               </div> -->
            </div>
         </div>
      </div>
      <!-- Proof Section End -->
      <!-- Look Section -->
      <div class="look-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-40 f-28 lh140 w700 black-clr text-center">				
               Look At These Early Jobinn Users Working 30 Min a Day And… Making Good Additional Income Every Month 
               </div>
              <div class="col-12 col-md-10 mx-auto mt20 mt-md70">
                  <div class="look-shapes">
                     <div class="row align-items-center">
                        <div class="col-md-4 col-12 order-md-2">
                           <img src="https://cdn.oppyo.com/launches/jobiin/special/john-fabians.webp" class="img-fluid d-block mx-auto mx-md-0 ms-md-auto" alt="John Fabians">
                        </div>
                        <div class="col-md-8 col-12 order-md-1 mt20 mt-md50">
                           <div class="col-md-6 col-8 line-5 mx-auto mx-md-0"></div>
                           <div class="f-md-28 f-22 lh140 mt10 w500 text-md-start text-center black-clr">
                              John Fabians Happily <span class="w700">Made $1875 In Commissions</span>  with His First Job Search Website 
                           </div>
                        </div>
                     </div>
                  </div>
                  </div>
               <!-- <div class="col-12 col-md-10 mx-auto mt20 mt-md70">
                  <div class="look-shapes left">
                     <div class="row align-items-center">
                        <div class="col-md-4 col-12">
                           <img src="https://cdn.oppyo.com/launches/jobiin/special/ben-lewis.webp" class="img-fluid d-block mx-auto mx-md-0 me-md-auto" alt="Ben Lewis">
                        </div>
                        <div class="col-md-8 col-12 mt20 mt-md40">
                           <div class="col-md-6 col-8 line-5 mx-auto mx-md-0"></div>
                           <div class="f-md-28 f-22 lh140 mt10 w500 text-md-start text-center black-clr">
                              Ben Lewis Happily <span class="w700">Makes $585 Every Month</span>, By Creating Well Optimized Job Search Sites
                           </div>
                        </div>
                     </div>
                  </div>
               </div> -->
               <div class="col-12 col-md-10 mx-auto mt20 mt-md70">
                  <div class="look-shapes left pb-md0">
                     <div class="row align-items-center">
                        <div class="col-md-4 col-12">
                           <img src="https://cdn.oppyo.com/launches/jobiin/special/zenith.webp" class="img-fluid d-block mx-auto mx-md-0 ms-md-auto" alt="Jack & Zenith">
                        </div>
                        <div class="col-md-8 col-12 mt20 mt-md40">
                           <div class="col-md-6 col-8 line-5 mx-auto mx-md-0"></div>
                           <div class="f-md-28 f-22 lh140 mt10 w500 text-md-start text-center black-clr">
                              Jack & Zenith Never Thought They Could Earn Anywhere <span class="w700">Between $515-$985 Each Month</span> with Their Job Search Site Creation Skills 
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Look Section End -->
<!-- Produly Section Start-->
<div class="proudly-section">
         <div class="container">
            <div class="row d-flex flex-wrap align-items-center">
               <div class="col-md-6">
                  <div class="f-md-28 f-22 w600 white-clr lh130 text-center text-md-start">
                     Proudly Presenting…
                  </div>
                  <div class="mt-md30 mt20 text-center text-md-start">
                     <svg id="Layer_1" xmlns="http://www.w3.org/2000/svg " viewBox="0 0 128.19 38 " style="max-height:100px;">
                        <defs>
                           <style>.cls-1,.cls-2{fill:#fff;}.cls-2{fill-rule:evenodd;}.cls-3{fill:#0a52e2;}</style>
                        </defs>
                        <rect class="cls-1
                           " x="79.21 " width="48.98 " height="38 " rx="4 " ry="4 "></rect>
                        <path class="cls-1 " d="M18.64,4.67V24.72c0,1.29-.21,2.47-.63,3.54-.42,1.07-1.03,1.97-1.84,2.71-.81,.74-1.79,1.32-2.93,1.74-1.15,.42-2.45,.63-3.9,.63s-2.64-.19-3.7-.57c-1.07-.38-1.98-.92-2.75-1.62-.77-.7-1.39-1.54-1.88-2.51S.19,26.6,0,25.41l5.74-1.13c.46,2.45,1.63,3.68,3.52,3.68,.89,0,1.65-.28,2.28-.85,.63-.57,.95-1.46,.95-2.67V9.6H3.92V4.67h14.72Z
                           "></path>
                        <path class="cls-1 " d="M54.91,33.37V4.63h9.71c3.38,0,6.02,.66,7.92,1.97,1.9,1.32,2.84,3.28,2.84,5.9,0,1.33-.35,2.52-1.06,3.56-.7,1.05-1.73,1.83-3.07,2.36,1.72,.37,3.02,1.16,3.88,2.37,.86,1.21,1.29,2.61,1.29,4.21,0,2.75-.91,4.83-2.72,6.25-1.82,1.42-4.39,2.12-7.72,2.12h-11.08Zm5.76-16.7h4.15c1.54,0,2.72-.32,3.55-.95,.83-.63,1.24-1.55,1.24-2.76,0-1.33-.42-2.31-1.25-2.94-.84-.63-2.08-.95-3.74-.95h-3.95v7.6Zm0,3.99v8.27h5.31c1.53,0,2.69-.33,3.49-.99,.8-.66,1.2-1.64,1.2-2.94,0-1.4-.34-2.48-1.03-3.22-.68-.74-1.76-1.11-3.24-1.11h-5.75Z
                           "></path>
                        <g>
                           <path class="cls-1 " d="M46.84,4.19C42.31-.12,36.87-1.08,31.16,1.2c-5.82,2.33-9.1,6.88-9.52,13.21-.41,6.17,2.57,10.9,6.84,15.15,.95-.84,1.22-1.22,2.01-2.05-.79-.65-1.41-1.09-2.43-2.11-5.86-6.15-5.78-15.66,1.04-20.46,5-3.52,11.71-3.13,16.37,.95,4.29,3.75,5.54,10.44,2.59,15.61-1.2,2.11-3.09,3.84-4.86,5.98,.28,.45,.72,1.18,1.44,2.35,1.77-2.01,3.11-3.72,4.38-5.63,4.39-6.6,3.56-14.59-2.17-20.02Z
                              "></path>
                           <g>
                              <path class="cls-2 " d="M39.67,27.34c-2.49,.37-4.9,.52-7.23-.46-.54-.23-1.13-1.06-1.18-1.65-.3-3.69,1.25-5.27,4.98-5.26,.31,0,.62,0,.92,.01,5.31-.01,5.96,6.85,2.51,7.36Z "></path>
                              <path class="cls-2 " d="M39.49,16.69c-.04,1.69-1.37,2.98-3.06,2.98-1.6,0-3.01-1.43-3.01-3.06s1.39-3.04,3.02-3.03c1.75,0,3.1,1.38,3.06,3.12Z
                                 "></path>
                           </g>
                           <g>
                              <g>
                                 <path class="cls-2 " d="M42.93,26.23c-.67-3.46-.93-5.25-3.46-7.18,.63-1.21,1.3-1.83,2.1-2,2.43-.53,3.98,.21,4.84,1.97,.66,1.34,.78,2.29-.37,3.77-1.03,1.17-1.85,2.21-3.11,3.43Z "></path>
                                 <path class="cls-2 " d="M44.94,13.4c.01,1.6-1.27,2.91-2.88,2.92-1.6,.01-2.91-1.28-2.92-2.88-.01-1.6,1.28-2.9,2.88-2.92,1.6-.01,2.91,1.28,2.92,2.88Z
                                    "></path>
                              </g>
                              <g>
                                 <path class="cls-2 " d="M30.1,26.23c.67-3.46,.93-5.25,3.46-7.18-.63-1.21-1.3-1.83-2.1-2-2.43-.53-3.98,.21-4.84,1.97-.66,1.34-.88,2.01,.27,3.49,.96,1.3,1.88,2.44,3.21,3.71Z "></path>
                                 <path class="cls-2 " d="M28.08,13.4c0,1.6,1.28,2.91,2.88,2.92,1.6,.01,2.91-1.28,2.92-2.88,.01-1.6-1.27-2.9-2.88-2.92-1.6-.01-2.91,1.28-2.92,2.88Z
                                    "></path>
                              </g>
                           </g>
                           <path class="cls-1 " d="M42.02,27.74c-.7,.41-6.95,2.1-10.73,.08l-2.22,2.57c.55,.61,5.12,5.27,7.55,7.6,2.22-2.17,7.2-7.37,7.2-7.37l-1.8-2.89Z "></path>
                        </g>
                        <g>
                           <rect class="cls-3 " x="85.05 " y="4.63 " width="5.38 " height="5.4 " rx="2.69
                              " ry="2.69 "></rect>
                           <rect class="cls-3 " x="85.05 " y="13.43 " width="5.38 " height="19.94 "></rect>
                           <rect class="cls-3 " x="95.13 " y="4.63 " width="5.38 " height="5.4 " rx="2.69 " ry="2.69 "></rect>
                           <rect class="cls-3 " x="95.13 " y="13.43
                              " width="5.38 " height="19.94 "></rect>
                           <path class="cls-3 " d="M109.85,13.43l.24,2.86c.66-1.02,1.48-1.81,2.45-2.38,.97-.56,2.06-.85,3.26-.85,2.01,0,3.59,.63,4.72,1.9,1.13,1.27,1.7,3.25,1.7,5.95v12.46h-5.4v-12.47c0-1.34-.27-2.29-.81-2.85-.54-.56-1.36-.84-2.45-.84-.71,0-1.35,.14-1.92,.43-.57,.29-1.04,.7-1.42,1.23v14.5h-5.38V13.43h5.01Z "></path>
                        </g>
                     </svg>
                  </div>
                  <div class="f-md-32 f-24 w700 white-clr lh130 mt20 text-center text-md-start">
                  The Break-through Software That Creates Beautiful & 100% Automated JOB Search Sites Pre-Loaded with 10 million+ Jobs in Just 60 Seconds… 
                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">                 
                  <img src="https://cdn.oppyo.com/launches/jobiin/special/product-box.webp" class="img-fluid d-block mx-auto img-animation" alt="Product">
               </div>
            </div>
         </div>
      </div>
      <!-- Produly Section End-->
      <div class="exclusive-bonus">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                  <div class="white-clr f-24 f-md-36 lh140 w600">
                     Exclusive Bonus #1 : Agenciez
                  </div>
                  <div class="f-18 f-md-20 w500 mt20 white-clr">
                     20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="agenciz-header">
         <div class="container">
            <div class="row">
               <div class="col-md-3 text-center mx-auto"><img src="https://cdn.oppyo.com/launches/jobiin/special-bonus/agen-logo.webp" class="img-fluid d-block mx-auto mr-md-auto"></div>
               <div class="col-12 f-md-28 f-22 lh140 w400 text-center jost-font white-clr mt20 mt-md65">
                  <i>Kickstart Your Own <u>Profitable Website Agency</u> and Tap into Real 284 B Market <br class="d-none d-md-block">Right from Your Home.</i>
               </div>
            </div>
            <div class="row">
               <div class="col-8 col-md-4 mt20 mt-md25 text-md-start text-center">
                  <div class="game-changer">
                     <div class="f-24 f-md-36 w500 text-center black-clr lh120 jost-font text-nowrap text-roated">Game Changer</div>
                  </div>
               </div>
               <div class="col-12 pl-md35">
                  <div class="text-center">
                     <div class="highlihgt-heading">
                        <div class="f-28 f-md-45 w400 text-center white-clr lh140 jost-font">
                           <span class="green-clr">The World’s No.1 Pro Website Builder Lets You</span> <span class="w500">Create Beautiful Local WordPress Websites &amp; Ecom Stores <br class="d-none d-md-block"> for  Any Business in Just 7 Minutes!</span>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 f-24 f-md-30 w400 lh140 text-center white-clr jost-font mt20">
                  Easily create &amp; sell websites for big profits to retail stores, book shops, coaches, dentists, attorney, gyms, spas, restaurants, cafes, &amp; 100+ other niches...  	
               </div>
               <div class="col-12 f-22 f-md-28 w400 lh140 text-center green-clr jost-font mt20">
                  No Tech Skills Needed. No Monthly Fees.
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md40">
               <div class="col-12 col-md-7 min-video-width-left">
                  <div class="responsive-video">
                     <iframe src="https://agenciez.dotcompal.com/video/embed/1xqnfd9akv" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                        box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                  </div>
               </div>
               <div class="col-12 col-md-5  min-video-width-right">
                  <div class="f-18 f-md-20 lh150 w300">
                     <ul class="header-bordered-list pl0 white-clr">
                        <li>Tap Into HUGE $284 B Web Agency Industry  </li>
                        <li>Create Elegant Local Business Sites, Store, or Blogs with Ease.  </li>
                        <li>No. 1 WordPress Framework–Customize Themes in Just Few Click  </li>
                        <li>30+ Stunning Themes with 200+ Templates &amp; 2000+ Possible Designs  </li>
                        <li>100% SEO, Social-Media &amp; Mobile Friendly Sites  </li>
                        <li>FREE Commercial License - Build an Incredible Income Helping Clients!  </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="agen-all">
         <picture>
            <source media="(min-width:767px)" srcset="https://cdn.oppyo.com/launches/jobiin/special-bonus/three-dt.webp" class="img-fluid d-block mx-auto">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/jobiin/special-bonus/three-mb.webp" class="img-fluid d-block mx-auto">
            <img src="https://cdn.oppyo.com/launches/jobiin/special-bonus/three-dt.webp" alt="Steps" class="img-fluid d-block mx-auto">
         </picture>
         <picture>
            <source media="(min-width:767px)" srcset="https://cdn.oppyo.com/launches/jobiin/special-bonus/agen-dt.webp" class="img-fluid d-block mx-auto">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/jobiin/special-bonus/agen-mb.webp" class="img-fluid d-block mx-auto">
            <img src="https://cdn.oppyo.com/launches/jobiin/special-bonus/agen-dt.webp" alt="Steps" class="img-fluid d-block mx-auto">
         </picture>
         <picture>
            <source media="(min-width:767px)" srcset="https://cdn.oppyo.com/launches/jobiin/special-bonus/intro-dt.webp" class="img-fluid d-block mx-auto">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/jobiin/special-bonus/intro-mb.webp" class="img-fluid d-block mx-auto">
            <img src="https://cdn.oppyo.com/launches/jobiin/special-bonus/intro-dt.webp" alt="Steps" class="img-fluid d-block mx-auto">
         </picture>
      </div>
      <div class="exclusive-bonus">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                  <div class="white-clr f-24 f-md-36 lh140 w600">
                     Exclusive Bonus #2 : Trendio
                  </div>
                  <div class="f-18 f-md-20 w500 mt20 white-clr">
                     20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="tr-header-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row">
                     <div class="col-md-3 text-center mx-auto">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 912.39 246.83" style="enable-background:new 0 0 912.39 246.83; max-height:50px;color: #023047;" xml:space="preserve">
                           <style type="text/css">
                              .st0{fill:#FFFFFF;}
                              .st1{opacity:0.3;}
                              .st2{fill:url(#SVGID_1_);}
                              .st3{fill:url(#SVGID_00000092439463549223389810000006963263784374171300_);}
                              .st4{fill:url(#SVGID_00000015332009897132114970000017423936041397956233_);}
                              .st5{fill:#023047;}
                           </style>
                           <g>
                              <g>
                                 <path class="st0" d="M18.97,211.06L89,141.96l2.57,17.68l10.86,74.64l2.12-1.97l160.18-147.9l5.24-4.84l6.8-6.27l5.24-4.85 l-25.4-27.51l-12.03,11.11l-5.26,4.85l-107.95,99.68l-2.12,1.97l-11.28-77.58l-2.57-17.68L0,177.17c0.31,0.72,0.62,1.44,0.94,2.15 c0.59,1.34,1.2,2.67,1.83,3.99c0.48,1.03,0.98,2.06,1.5,3.09c0.37,0.76,0.76,1.54,1.17,2.31c2.57,5.02,5.43,10,8.57,14.93 c0.58,0.89,1.14,1.76,1.73,2.65L18.97,211.06z"></path>
                              </g>
                              <g>
                                 <g>
                                    <polygon class="st0" points="328.54,0 322.97,17.92 295.28,106.98 279.91,90.33 269.97,79.58 254.51,62.82 244.58,52.05 232.01,38.45 219.28,24.66 "></polygon>
                                 </g>
                              </g>
                           </g>
                           <g class="st1">
                              <g>
                                 <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="17.428" y1="42.82" x2="18.9726" y2="42.82" gradientTransform="matrix(1 0 0 -1 0 252.75)">
                                    <stop offset="0" style="stop-color:#000000"></stop>
                                    <stop offset="1" style="stop-color:#333333"></stop>
                                 </linearGradient>
                                 <path class="st2" d="M17.43,208.8l1.54,2.26c-0.37-0.53-0.73-1.05-1.09-1.58c-0.02-0.02-0.02-0.03-0.03-0.03 c-0.06-0.09-0.11-0.17-0.17-0.27C17.6,209.06,17.51,208.92,17.43,208.8z"></path>
                                 <linearGradient id="SVGID_00000177457867953882822120000005144526232562103955_" gradientUnits="userSpaceOnUse" x1="18.98" y1="70.45" x2="91.57" y2="70.45" gradientTransform="matrix(1 0 0 -1 0 252.75)">
                                    <stop offset="0" style="stop-color:#000000"></stop>
                                    <stop offset="1" style="stop-color:#333333"></stop>
                                 </linearGradient>
                                 <path style="fill:url(#SVGID_00000177457867953882822120000005144526232562103955_);" d="M89,141.96l2.57,17.68l-63.83,63 c-1.19-1.45-2.34-2.9-3.46-4.35c-1.84-2.4-3.6-4.81-5.3-7.22L89,141.96z"></path>
                                 <linearGradient id="SVGID_00000010989203515549635820000018393619450353154703_" gradientUnits="userSpaceOnUse" x1="104.55" y1="120.005" x2="333.22" y2="120.005" gradientTransform="matrix(1 0 0 -1 0 252.75)">
                                    <stop offset="0" style="stop-color:#000000"></stop>
                                    <stop offset="1" style="stop-color:#333333"></stop>
                                 </linearGradient>
                                 <polygon style="fill:url(#SVGID_00000010989203515549635820000018393619450353154703_);" points="333.22,15.61 299.96,122.58 274.65,95.18 107.11,249.88 104.55,232.31 264.73,84.41 269.97,79.58 279.91,90.33 295.28,106.98 322.97,17.92 "></polygon>
                              </g>
                           </g>
                           <g>
                              <g>
                                 <g>
                                    <path class="st5" d="M229.46,241.94c-12.28,5.37-23.49,8.06-33.57,8.06c-9.63,0-18.21-2.44-25.71-7.35 c-23.05-15.07-23.72-46.17-23.72-49.67V61.67h32.2v30h39.27v32.2h-39.27v69.04c0.07,4.46,1.88,18.1,9.2,22.83 c5.51,3.55,15.7,2.38,28.68-3.3L229.46,241.94z"></path>
                                 </g>
                              </g>
                           </g>
                           <g>
                              <g>
                                 <path class="st5" d="M250.4,164.29c0-15.97-0.24-27.35-0.97-38h25.9l0.97,22.51h0.97c5.81-16.7,19.61-25.17,32.19-25.17 c2.9,0,4.6,0.24,7.02,0.73v28.08c-2.42-0.48-5.08-0.97-8.71-0.97c-14.28,0-23.96,9.2-26.63,22.51c-0.48,2.66-0.97,5.81-0.97,9.2 v61H250.4V164.29z"></path>
                              </g>
                           </g>
                           <g>
                              <g>
                                 <path class="st5" d="M459.28,161.39c0-13.55-0.24-24.93-0.97-35.1h26.14l1.45,17.67h0.73c5.08-9.2,17.91-20.33,37.52-20.33 c20.57,0,41.87,13.31,41.87,50.59v69.95h-29.77v-66.56c0-16.94-6.29-29.77-22.51-29.77c-11.86,0-20.09,8.47-23.24,17.43 c-0.97,2.66-1.21,6.29-1.21,9.68v69.23h-30.01V161.39z"></path>
                              </g>
                           </g>
                           <g>
                              <g>
                                 <path class="st5" d="M706.41,72.31V211c0,12.1,0.48,25.17,0.97,33.16h-26.63l-1.21-18.64h-0.48c-7.02,13.07-21.3,21.3-38.49,21.3 c-28.08,0-50.35-23.96-50.35-60.27c-0.24-39.45,24.45-62.93,52.77-62.93c16.22,0,27.84,6.78,33.16,15.49h0.48v-66.8 C676.63,72.31,706.41,72.31,706.41,72.31z M676.64,175.43c0-2.42-0.24-5.33-0.73-7.75c-2.66-11.62-12.1-21.06-25.66-21.06 c-19.12,0-29.77,16.94-29.77,38.97c0,21.54,10.65,37.28,29.53,37.28c12.1,0,22.75-8.23,25.66-21.06c0.73-2.66,0.97-5.57,0.97-8.71 V175.43z"></path>
                              </g>
                           </g>
                           <path class="st0" d="M769.68,89.95c-0.11-0.53-0.24-1.04-0.39-1.55c-0.12-0.39-0.25-0.76-0.39-1.14c-0.12-0.32-0.25-0.64-0.39-0.95 c-0.12-0.27-0.25-0.54-0.39-0.81c-0.12-0.24-0.25-0.47-0.39-0.7c-0.12-0.21-0.25-0.42-0.39-0.63c-0.13-0.19-0.26-0.38-0.39-0.57 c-0.13-0.17-0.26-0.35-0.39-0.51s-0.26-0.32-0.39-0.47s-0.26-0.3-0.39-0.44s-0.26-0.28-0.39-0.41c-0.13-0.13-0.26-0.25-0.39-0.37 s-0.26-0.23-0.39-0.35c-0.13-0.11-0.26-0.22-0.39-0.33c-0.13-0.1-0.26-0.2-0.39-0.3c-0.13-0.1-0.26-0.19-0.39-0.28 s-0.26-0.18-0.39-0.27c-0.13-0.08-0.26-0.16-0.39-0.24c-0.13-0.08-0.26-0.16-0.39-0.24c-0.13-0.07-0.26-0.14-0.39-0.21 s-0.26-0.14-0.39-0.2s-0.26-0.13-0.39-0.19c-0.13-0.06-0.26-0.12-0.39-0.18s-0.26-0.11-0.39-0.17c-0.13-0.05-0.26-0.1-0.39-0.15 s-0.26-0.1-0.39-0.14s-0.26-0.09-0.39-0.13s-0.26-0.08-0.39-0.12s-0.26-0.07-0.39-0.11c-0.13-0.03-0.26-0.07-0.39-0.1 s-0.26-0.06-0.39-0.09s-0.26-0.05-0.39-0.08s-0.26-0.05-0.39-0.07s-0.26-0.04-0.39-0.06s-0.26-0.04-0.39-0.06s-0.26-0.03-0.39-0.04 s-0.26-0.03-0.39-0.04s-0.26-0.02-0.39-0.03s-0.26-0.02-0.39-0.03s-0.26-0.01-0.39-0.02c-0.13,0-0.26-0.01-0.39-0.01 s-0.26-0.01-0.39-0.01s-0.26,0.01-0.39,0.01s-0.26,0-0.39,0.01c-0.13,0-0.26,0.01-0.39,0.02c-0.13,0.01-0.26,0.02-0.39,0.03 s-0.26,0.02-0.39,0.03s-0.26,0.03-0.39,0.04c-0.13,0.02-0.26,0.03-0.39,0.04c-0.13,0.02-0.26,0.04-0.39,0.06s-0.26,0.04-0.39,0.06 s-0.26,0.05-0.39,0.08s-0.26,0.05-0.39,0.08s-0.26,0.06-0.39,0.09s-0.26,0.07-0.39,0.1c-0.13,0.04-0.26,0.07-0.39,0.11 s-0.26,0.08-0.39,0.12s-0.26,0.09-0.39,0.13c-0.13,0.05-0.26,0.09-0.39,0.14s-0.26,0.1-0.39,0.15s-0.26,0.11-0.39,0.16 c-0.13,0.06-0.26,0.12-0.39,0.18s-0.26,0.12-0.39,0.19s-0.26,0.14-0.39,0.21s-0.26,0.14-0.39,0.21c-0.13,0.08-0.26,0.16-0.39,0.24 c-0.13,0.08-0.26,0.16-0.39,0.24c-0.13,0.09-0.26,0.18-0.39,0.27s-0.26,0.19-0.39,0.28c-0.13,0.1-0.26,0.2-0.39,0.3 c-0.13,0.11-0.26,0.22-0.39,0.33s-0.26,0.23-0.39,0.34c-0.13,0.12-0.26,0.24-0.39,0.37c-0.13,0.13-0.26,0.27-0.39,0.4 c-0.13,0.14-0.26,0.28-0.39,0.43s-0.26,0.3-0.39,0.46c-0.13,0.16-0.26,0.33-0.39,0.5c-0.13,0.18-0.26,0.37-0.39,0.55 c-0.13,0.2-0.26,0.4-0.39,0.61c-0.13,0.22-0.26,0.45-0.39,0.68c-0.14,0.25-0.27,0.51-0.39,0.77c-0.14,0.3-0.27,0.6-0.39,0.9 c-0.14,0.36-0.27,0.73-0.39,1.1c-0.15,0.48-0.28,0.98-0.39,1.48c-0.25,1.18-0.39,2.42-0.39,3.7c0,1.27,0.14,2.49,0.39,3.67 c0.11,0.5,0.24,0.99,0.39,1.47c0.12,0.37,0.24,0.74,0.39,1.1c0.12,0.3,0.25,0.6,0.39,0.89c0.12,0.26,0.25,0.51,0.39,0.76 c0.12,0.23,0.25,0.45,0.39,0.67c0.12,0.2,0.25,0.41,0.39,0.6c0.13,0.19,0.25,0.37,0.39,0.55c0.13,0.17,0.25,0.34,0.39,0.5 c0.13,0.15,0.26,0.3,0.39,0.45c0.13,0.14,0.26,0.28,0.39,0.42c0.13,0.13,0.26,0.26,0.39,0.39c0.13,0.12,0.26,0.25,0.39,0.37 c0.13,0.11,0.26,0.22,0.39,0.33s0.26,0.21,0.39,0.32c0.13,0.1,0.26,0.2,0.39,0.3c0.13,0.09,0.26,0.18,0.39,0.27s0.26,0.18,0.39,0.26 s0.26,0.16,0.39,0.24c0.13,0.08,0.26,0.15,0.39,0.23c0.13,0.07,0.26,0.14,0.39,0.21s0.26,0.13,0.39,0.19 c0.13,0.06,0.26,0.13,0.39,0.19c0.13,0.06,0.26,0.11,0.39,0.17s0.26,0.11,0.39,0.17c0.13,0.05,0.26,0.1,0.39,0.14 c0.13,0.05,0.26,0.1,0.39,0.14s0.26,0.08,0.39,0.12s0.26,0.08,0.39,0.12s0.26,0.07,0.39,0.1s0.26,0.07,0.39,0.1s0.26,0.06,0.39,0.08 c0.13,0.03,0.26,0.06,0.39,0.08c0.13,0.02,0.26,0.05,0.39,0.07s0.26,0.04,0.39,0.06s0.26,0.04,0.39,0.05 c0.13,0.02,0.26,0.03,0.39,0.04s0.26,0.03,0.39,0.04s0.26,0.02,0.39,0.03s0.26,0.02,0.39,0.03s0.26,0.01,0.39,0.01 s0.26,0.01,0.39,0.01c0.05,0,0.1,0,0.15,0c0.08,0,0.16,0,0.24-0.01c0.13,0,0.26,0,0.39-0.01c0.13,0,0.26,0,0.39-0.01 s0.26-0.02,0.39-0.03s0.26-0.01,0.39-0.03c0.13-0.01,0.26-0.02,0.39-0.04c0.13-0.01,0.26-0.03,0.39-0.04 c0.13-0.02,0.26-0.03,0.39-0.05c0.13-0.02,0.26-0.04,0.39-0.06s0.26-0.04,0.39-0.06s0.26-0.05,0.39-0.08s0.26-0.05,0.39-0.08 s0.26-0.06,0.39-0.09s0.26-0.06,0.39-0.1s0.26-0.07,0.39-0.11s0.26-0.08,0.39-0.12s0.26-0.08,0.39-0.13 c0.13-0.04,0.26-0.09,0.39-0.14s0.26-0.1,0.39-0.15s0.26-0.11,0.39-0.16c0.13-0.06,0.26-0.11,0.39-0.17s0.26-0.12,0.39-0.18 s0.26-0.13,0.39-0.2s0.26-0.14,0.39-0.21s0.26-0.15,0.39-0.23s0.26-0.15,0.39-0.24c0.13-0.08,0.26-0.17,0.39-0.26 c0.13-0.09,0.26-0.18,0.39-0.27c0.13-0.1,0.26-0.19,0.39-0.29s0.26-0.21,0.39-0.32s0.26-0.22,0.39-0.33 c0.13-0.12,0.26-0.24,0.39-0.36c0.13-0.13,0.26-0.26,0.39-0.39c0.13-0.14,0.26-0.28,0.39-0.42c0.13-0.15,0.26-0.3,0.39-0.45 c0.13-0.16,0.26-0.33,0.39-0.49c0.13-0.18,0.26-0.36,0.39-0.54c0.13-0.2,0.26-0.4,0.39-0.6c0.14-0.22,0.26-0.44,0.39-0.67 c0.14-0.25,0.27-0.5,0.39-0.76c0.14-0.29,0.27-0.58,0.39-0.88c0.14-0.36,0.27-0.72,0.39-1.1c0.15-0.48,0.28-0.97,0.39-1.46 c0.25-1.17,0.39-2.4,0.39-3.66C770.03,92.19,769.9,91.05,769.68,89.95z"></path>
                           <g>
                              <g>
                                 <rect x="738.36" y="126.29" class="st5" width="30.01" height="117.88"></rect>
                              </g>
                           </g>
                           <g>
                              <g>
                                 <path class="st5" d="M912.39,184.14c0,43.33-30.5,62.69-60.51,62.69c-33.4,0-59.06-22.99-59.06-60.75 c0-38.73,25.41-62.45,61-62.45C888.91,123.63,912.39,148.32,912.39,184.14z M823.56,185.35c0,22.75,11.13,39.94,29.29,39.94 c16.94,0,28.8-16.7,28.8-40.42c0-18.4-8.23-39.45-28.56-39.45C832.03,145.41,823.56,165.74,823.56,185.35z"></path>
                              </g>
                           </g>
                           <g>
                              <path class="st5" d="M386,243.52c-2.95,0-5.91-0.22-8.88-0.66c-15.75-2.34-29.65-10.67-39.13-23.46 c-9.48-12.79-13.42-28.51-11.08-44.26c2.34-15.75,10.67-29.65,23.46-39.13c12.79-9.48,28.51-13.42,44.26-11.08 s29.65,10.67,39.13,23.46l7.61,10.26l-55.9,41.45l-15.21-20.51l33.41-24.77c-3.85-2.36-8.18-3.94-12.78-4.62 c-9-1.34-17.99,0.91-25.3,6.33s-12.07,13.36-13.41,22.37c-1.34,9,0.91,17.99,6.33,25.3c5.42,7.31,13.36,12.07,22.37,13.41 c9,1.34,17.99-0.91,25.3-6.33c4.08-3.02,7.35-6.8,9.72-11.22l22.5,12.08c-4.17,7.77-9.89,14.38-17.02,19.66 C411,239.48,398.69,243.52,386,243.52z"></path>
                           </g>
                        </svg>
                     </div>
                  </div>
               </div>
               <div class="col-12 text-center lh150 mt20 mt-md50 px15 px-md0">
                  <div class="trpre-heading f-20 f-md-22 w500 lh150 yellow-clr">
                     <span>
                     It’s Time to Get Over Boring Content &amp; Cash into The Latest Trending Topics in 2022 
                     </span>
                  </div>
               </div>
               <div class="col-12 mt-md25 mt20 f-md-45 f-28 w500 text-center black-clr lh150">
                  Breakthrough A.I. Technology <span class="w700 blue-clr">Creates Traffic Pulling Websites Packed with Trendy Content &amp; Videos from Just One Keyword</span> in 60 Seconds... 
               </div>
               <div class="col-12 mt-md25 mt20 f-20 f-md-22 w500 text-center lh150 black-clr">
                  Anyone can easily create &amp; sell traffic generating websites for dentists, attorney, affiliates,<br class="d-none d-md-block"> coaches, SAAS, retail stores, book shops, gyms, spas, restaurants, &amp; 100+ other niches...
               </div>
            </div>
            <div class="row d-flex align-items-center flex-wrap mt20 mt-md40">
               <div class="col-md-7 col-12 min-md-video-width-left">
                  <div class="col-12 responsive-video border-video">
                     <iframe src="https://trendio.dotcompal.com/video/embed/2p8pv1cndk" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                        box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                  </div>
               </div>
               <div class="col-md-5 col-12 mt20 mt-md0 min-md-video-width-right">
                  <div class="key-features-bg1">
                     <ul class="list-head1 pl0 m0 f-16 lh150 w500 black-clr">
                        <li>Works Seamlessly in Any Niche or Topic – <span class="w600">Just One Keyword</span> </li>
                        <li>All of That Without Content Creation, Editing, Camera, or Tech Hassles Ever </li>
                        <li>Drive Tons of FREE Viral Traffic Using the POWER Of Trendy Content </li>
                        <li>Grab More Authority, Engagement, &amp; Leads on your Business Website </li>
                        <li>Monetize Other’s Content Legally with Banner Ads, Affiliate Offers &amp; AdSense </li>
                        <li class="w600">FREE COMMERCIAL LICENSE INCLUDED TO BUILD AN INCREDIBLE INCOME </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="tr-all">
         <picture>
            <source media="(min-width:767px)" srcset="https://cdn.oppyo.com/launches/jobiin/special-bonus/tr-lst-dt.webp" class="img-fluid d-block mx-auto">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/jobiin/special-bonus/tr-lst-mb.webp" class="img-fluid d-block mx-auto">
            <img src="https://cdn.oppyo.com/launches/jobiin/special-bonus/tr-lst-dt.webp" alt="Steps" class="img-fluid d-block mx-auto">
         </picture>
         <picture>
            <source media="(min-width:767px)" srcset="https://cdn.oppyo.com/launches/jobiin/special-bonus/tr-dt.webp" class="img-fluid d-block mx-auto">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/jobiin/special-bonus/tr-mb.webp" class="img-fluid d-block mx-auto">
            <img src="https://cdn.oppyo.com/launches/jobiin/special-bonus/tr-dt.webp" alt="Steps" class="img-fluid d-block mx-auto">
         </picture>
         <picture>
            <source media="(min-width:767px)" srcset="https://cdn.oppyo.com/launches/jobiin/special-bonus/pr-dt.webp" class="img-fluid d-block mx-auto">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/jobiin/special-bonus/pr-mb.webp" class="img-fluid d-block mx-auto">
            <img src="https://cdn.oppyo.com/launches/jobiin/special-bonus/pr-dt.webp" alt="Steps" class="img-fluid d-block mx-auto">
         </picture>
      </div>
      <div class="exclusive-bonus">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                  <div class="white-clr f-24 f-md-36 lh140 w600">
                     Exclusive Bonus #3 : SociDesk
                  </div>
                  <div class="f-18 f-md-20 w500 mt20 white-clr">
                     20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="top-header">
         <div class="container">
            <div class="col-12">
               <img src="https://cdn.oppyo.com/launches/jobiin/special-bonus/soci-logo.webp" alt="Steps" class="img-fluid d-block mx-auto">
            </div>
         </div>
      </div>
      <div class="banner">
         <div class="container">
            <div class="row">
               <div class="col-md-10 mx-auto">
                  <p class="text-center f-md-22 f-20 w300 lh150 white-clr highlite">
                     Stop wasting time &amp; money in just posting on social media without any results…
                  </p>
               </div>
               <div class="col-m-12 mt-md30 mt20">
                  <div class="text-center f-md-45 f-25 white lh120 w400">
                     Convert Your Social Conversations into <strong>LEADS</strong> and <strong>SALES</strong> and Drive Tons of Social <strong>TRAFFIC</strong> 
                  </div>
                  <br>
               </div>
               <div class="col-md-10 mx-auto">
                  <p class="text-center f-md-22 f-18 w300 lh150 white">Cloud based software. No Complicated Installation. No Hosting. No Technical hassles</p>
               </div>
               <div class="col-md-8 mx-auto mt-md40 mt20">
                  <img src="https://cdn.oppyo.com/launches/jobiin/special-bonus/soproductbox.webp" class="img-fluid d-block mx-auto">  		
               </div>
            </div>
         </div>
      </div>
      <div class="sp-all">
         <picture>
            <source media="(min-width:767px)" srcset="https://cdn.oppyo.com/launches/jobiin/special-bonus/so-mb.webp" class="img-fluid d-block mx-auto">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/jobiin/special-bonus/so-dt.webp" class="img-fluid d-block mx-auto">
            <img src="https://cdn.oppyo.com/launches/jobiin/special-bonus/so-mb.webp" alt="Steps" class="img-fluid d-block mx-auto">
         </picture>
         <picture>
            <source media="(min-width:767px)" srcset="https://cdn.oppyo.com/launches/jobiin/special-bonus/sp-dt.webp" class="img-fluid d-block mx-auto">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/jobiin/special-bonus/sp-mb.webp" class="img-fluid d-block mx-auto">
            <img src="https://cdn.oppyo.com/launches/jobiin/special-bonus/sp-dt.webp" alt="Steps" class="img-fluid d-block mx-auto">
         </picture>
      </div>
      <div class="exclusive-bonus">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                  <div class="white-clr f-24 f-md-36 lh140 w600">
                     Exclusive Bonus #4 : Ninja Kash
                  </div>
                  <div class="f-18 f-md-20 w500 mt20 white-clr">
                     My Recent Launch Ninja Kash - With Reseller License (worth $11999)
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="grand-logo-bg">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <img src="assets/images/logo-gr.webp" class="img-fluid mx-auto d-block">
               </div>
            </div>
         </div>
      </div>
      <div class="header-section-n">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="mt20 mt-md50">
                     <div class="f-md-24 f-20 w500 lh140 text-center black-clr post-heading-n">
                        Weird System Pays Us $500-1000/Day for Just Copying and Pasting...
                     </div>
                  </div>
               </div>
               <div class="col-12">
                  <div class="col-12 mt-md30 mt20">
                     <div class="f-28 f-md-50 w500 text-center white-clr lh140">								
                        <span class="w700"> Break-Through App Creates a Ninja Affiliate Funnel In 5 Minutes Flat</span> That Drives FREE Traffic &amp; Sales on 100% Autopilot
                     </div>
                  </div>
                  <div class="col-12 f-22 f-md-28 w600 text-center  orange lh140 mt-md30 mt20">
                     NO Worries For Finding Products | NO Paid Traffic | No Tech Skills
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-md-7 col-12 mt-md40 mt20 px-md15 min-md-video-width-left">
                  <!--<div>
                     <img src="assets/images/video-bg.png" class="img-responsive center-block"> 
                      </div>-->
                  <div class="col-12 mt-md15">
                     <div class="col-12 responsive-video">
                        <iframe src="https://ninjakash.dotcompal.com/video/embed/t4spbhm1vi" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                           box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                     </div>
                  </div>
               </div>
               <div class="col-md-5 col-12 f-20 f-md-20 lh140 w400 white-clr mt-md40 mt20 pl-md15 min-md-video-width-right">
                  <ul class="list-head pl0 m0">
                     <li>Kickstart with 50 Hand-Picked Products</li>
                     <li>Promote Any Offer in Any niche HANDS FREE</li>
                     <li>SEO Optimized, Mobile Responsive Affiliate Funnels</li>
                     <li>Drive TONS OF Social &amp; Viral traffic</li>
                     <li>Build Your List and Convert AFFILIATE Sales in NO Time</li>
                     <li>No Monthly Fees…EVER</li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <div class="gr-1">
         <div class="container-fluid p0">
            <picture>
               <source media="(min-width:768px)" srcset="assets/images/gr2.webp">
               <source media="(min-width:320px)" srcset="assets/images/gr2a.webp">
               <img src="assets/images/gr2.webp" alt="Flowers" style="width:100%;">
            </picture>
            <picture>
               <source media="(min-width:768px)" srcset="assets/images/gr1.webp">
               <source media="(min-width:320px)" srcset="assets/images/gr1a.webp">
               <img src="assets/images/gr1.webp" alt="Flowers" style="width:100%;">
            </picture>
         </div>
      </div>
      <div class="exclusive-bonus">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                  <div class="white-clr f-24 f-md-36 lh140 w600">
                     Exclusive Bonus #5 : Buzzify 
                  </div>
                  <div class="f-18 f-md-20 w500 mt20 white-clr">
                  20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS
If you already have this bonus, you will get more licenses to sell (really great to have more)
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="buz-header-section">
         <div class="container">
            <div class="row">
		
               <div class="col-12">
                  <div class="row">
                     <div class="col-md-3 text-center mx-auto">
                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 973.3 276.9" style="enable-background:new 0 0 973.3 276.9; max-height:40px;" xml:space="preserve">
                           <style type="text/css">
                              .st0{fill:#FFFFFF;}
                           </style>
                           <g>
                              <path class="st0" d="M0,212.5V12.1h98.6c19.3,0,34.4,4.2,45.3,12.7c10.9,8.5,16.4,21,16.4,37.7c0,20.8-8.6,35.5-25.7,44.1
                                 c14,2.2,24.8,7.6,32.2,16.1c7.5,8.5,11.2,19.7,11.2,33.6c0,11.7-2.6,21.8-7.9,30.2c-5.3,8.4-12.8,14.8-22.5,19.3
                                 c-9.7,4.5-21.2,6.7-34.5,6.7H0z M39.2,93.3h50.3c10.7,0,18.8-2.2,24.5-6.6c5.6-4.4,8.4-10.7,8.4-19c0-8.3-2.8-14.6-8.3-18.8
                                 c-5.5-4.2-13.7-6.3-24.6-6.3H39.2V93.3z M39.2,181.9h60.6c12.3,0,21.6-2.2,27.7-6.7c6.2-4.5,9.3-11.2,9.3-20.2
                                 c0-8.9-3.2-15.8-9.5-20.6c-6.4-4.8-15.5-7.2-27.5-7.2H39.2V181.9z"></path>
                              <path class="st0" d="M268.1,216.4c-22.8,0-39.1-4.5-49.1-13.5c-10-9-15-23.6-15-44V88.2h37.3v61.9c0,13.1,2,22.4,6,27.7
                                 c4,5.4,10.9,8.1,20.8,8.1c9.7,0,16.6-2.7,20.6-8.1c4-5.4,6.1-14.6,6.1-27.7V88.2h37.3V159c0,20.3-5,35-14.9,44
                                 C307.2,211.9,290.9,216.4,268.1,216.4z"></path>
                              <path class="st0" d="M356.2,212.5l68.6-95.9h-58.7V88.2h121.6L419,184.1h64.5v28.4H356.2z"></path>
                              <path class="st0" d="M500.3,212.5l68.6-95.9h-58.7V88.2h121.6l-68.7,95.9h64.5v28.4H500.3z"></path>
                              <path class="st0" d="M679.8,62.3c-4.1,0-7.8-1-11.1-3c-3.4-2-6-4.7-8-8.1s-3-7.1-3-11.2c0-4,1-7.7,3-11c2-3.3,4.7-6,8-8
                                 c3.3-2,7-3,11.1-3c4,0,7.7,1,11.1,3c3.3,2,6,4.6,8,8c2,3.3,3,7,3,11c0,4.1-1,7.8-3,11.2c-2,3.4-4.7,6.1-8,8.1
                                 C687.5,61.3,683.8,62.3,679.8,62.3z M661.2,212.5V88.2h37.3v124.4H661.2z"></path>
                              <path class="st0" d="M747.9,212.5v-96.1h-16.8V88.2h16.8V57.3c0-11.7,1.8-21.9,5.5-30.5c3.6-8.6,8.9-15.2,15.7-19.9
                                 c6.8-4.7,15-7,24.6-7c5.9,0,11.7,0.9,17.5,2.7c5.7,1.8,10.7,4.4,14.8,7.6l-12.9,26.1c-3.8-2.3-8.1-3.5-12.9-3.5
                                 c-4.2,0-7.4,1.1-9.6,3.2c-2.2,2.1-3.7,5.1-4.4,8.9c-0.8,3.8-1.2,8.2-1.2,13.1v30.1h29.4v28.3h-29.4v96.1H747.9z"></path>
                              <path class="st0" d="M835.6,275.7l40.1-78.7l-59-108.8h42.7l38,72.9l33.2-72.9h42.7l-95,187.5H835.6z"></path>
                           </g>
                        </svg>
                     </div>
                  </div>
               </div>
			   	<!-- <div class="col-12 mt20 mt-md50"> <img src="assets/images/demo-vid1.png" class="img-fluid d-block mx-auto"></div> -->
               <div class="col-12 text-center lh150 mt20 mt-md30">
                  <div class="pre-heading-b f-md-20 f-18 w600 lh150">
                     <div class="skew-con d-flex gap20 align-items-center ">
                        It’s Time to Get Over Boring Content and Cash into The Latest Trending Topics in 2022 
                     </div>
                  </div>
               </div>
               <div class="col-12 mt-md30 mt20 f-md-40 f-28 w600 text-center white-clr lh150">
				  Breakthrough 3-Click Software Uses a <span class="under yellow-clr w800"> Secret Method to Make Us $528/Day Over and Over</span>  Again Using the Power of Trending Content &amp; Videos 
               </div>
               <div class="col-12 mt-md25 mt20 f-18 f-md-21 w600 text-center lh150 yellow-clr">
                 All of That Without Content Creation, Editing, Camera, or Tech Hassles Ever! Even Newbies Can Get Unlimited FREE Traffic and Steady PASSIVE Income…Every Day
               </div>
            </div>
            <div class="row d-flex align-items-center flex-wrap mt20 mt-md40">
               <div class="col-md-10 mx-auto col-12 mt20 mt-md0">
                  <div class="col-12 responsive-video">
                     <iframe src="https://buzzify.dotcompal.com/video/embed/satfj9nvaq" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                        box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="bsecond-section">
         <div class="container">
            <div class="row">
               <div class="col-12 p-md0">
                  <div class="col-12 key-features-bg-b d-flex align-items-center flex-wrap">
                     <div class="row">
                        <div class="col-12 col-md-6">
                           <ul class="list-head pl0 m0 f-16 f-md-18 lh160 w400 white-clr text-capitalize">
								<li><span class="w600">Creates Beautiful &amp; Self-Updating Sites </span>with Hot Trending Content &amp; Videos </li>
								<li class="w600">Built-In 1-Click Traffic Generating System </li>
								<li class="w600">Puts Most Profitable Links on Your Websites using AI </li>
								<li>Legally Use Other’s Trending Content To Generate Automated Profits- <span class="w600">Works in Any Niche or TOPIC </span></li>
								<li>1-Click Social Media Automation – <span class="w600">People ONLY WANT TRENDY Topics These Days To Click.</span> </li>
								<li><span class="w600">100% SEO Friendly </span> Website and Built-In Remarketing System </li>
								<li>Complete Newbie Friendly And <span class="w600">Works Even If You Have No Prior Experience And Technical Skills</span> </li>
                           </ul>
                        </div>
                        <div class="col-12 col-md-6">
                           <ul class="list-head pl0 m0 f-16 f-md-18 lh160 w400 white-clr text-capitalize">                            							  
							<li><span class="w600">Set and Forget System</span> With Single Keyword – Set Rules To Find &amp; Publish Trending Posts</li>
							<li><span class="w600">Automatically Translate Your Sites In 15+ Language</span> According To Location For More Traffic</li>
							<li><span class="w600">Make 5K-10K With Commercial License </span>That Allows You To Serve Your Clients &amp; Charge Them</li>
							<li><span class="w600">Integration with Major Platforms</span> Including Autoresponders And Social Media Apps </li>
							<li><span class="w600">In-Built Content Spinner</span> to Make your Content Fresh and More Engaging </li>
							<li><span class="w600">A-Z Complete Video Training </span>Is Included To Make It Easier For You </li>
							<li><span class="w600">Limited-Time Special</span> Bonuses Worth $2285 If You Buy Today </li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            
         </div>
      </div>
      <div class="gr-1">
         <div class="container-fluid p0">
            <picture>
               <source media="(min-width:768px)" srcset="assets/images/b3d.webp">
               <source media="(min-width:320px)" srcset="assets/images/b3m.webp">
               <img src="assets/images/b3d.webp" alt="Flowers" style="width:100%;">
            </picture>
            <picture>
               <source media="(min-width:768px)" srcset="assets/images/bnd.webp">
               <source media="(min-width:320px)" srcset="assets/images/bnm.webp">
               <img src="assets/images/bnd.webp" alt="Flowers" style="width:100%;">
            </picture>
            <picture>
               <source media="(min-width:768px)" srcset="assets/images/bpd.webp">
               <source media="(min-width:320px)" srcset="assets/images/bpm.webp">
               <img src="assets/images/bpd.webp" alt="Flowers" style="width:100%;">
            </picture>
         </div>
      </div>

      <div class="exclusive-bonus">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                  <div class="white-clr f-24 f-md-36 lh140 w600">
                     Exclusive Bonus #6 : Vidvee 
                  </div>
                  <div class="f-18 f-md-20 w500 mt20 white-clr">
                        20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS
                        If you already have this bonus, you will get more licenses to sell (really great to have more)
                  </div>
               </div>
            </div>
         </div>
      </div>


      <!-- Vidvee Header Section Start -->

      <div class="vidvee-header-section">
            <div class="container">
                  <div class="row">
                        <div class="col-12">
                              <img src="assets/images/vidvee.logo.png" alt="" class="d-block mx-auto img-fluid">
                        </div>
                        <div class="col-12 text-center mt20 mt-md50 lh130">
                              <div class="vidvee-pre-heading f-md-24 f-20 w600 lh130">
				            1 Click Exploits YouTube Loophole to Legally Use Their 800 Million+<br class="d-none d-md-block">
                                    Awesome Videos for Making Us $528 Every Day Again & Again...
                              </div>
                        </div>
                        <div class="col-12 mt-md25 mt20 text-center">
                              <div class="f-md-50 f-30 w500 white-clr lh130 line-center">
			                  Breakthrough Software <span class="w700 vidvee-orange-clr">Creates Self-Growing Beautiful Video Channels Packed With RED-HOT Videos And Drives FREE Autopilot Traffic On Any Topic </span>
                                    In Just 7 Minutes Flat…
                              </div>
                              <div class="mt-md25 mt20 f-20 f-md-24 w500 text-center lh130 white-clr">
                                    No Content Creation. No Camera or Editing. No Tech Hassles Ever... 100% Beginner Friendly!	
                              </div>
                              <div class="row d-flex align-items-center flex-wrap mt20 mt-md40">
                                    <div class="col-md-10 mx-auto col-12 mt20 mt-md0">
                                          <div class="col-12 responsive-video">
                                                <iframe src="https://vidvee.dotcompal.com/video/embed/xcz62wags4" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                                                box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                                          </div> 
						</div>
                                    <div class="col-12 mt20 mt-md40 vidvee-key-features-bg">
                                          <div class="row flex-wrap">
                                                <div class="col-12 col-md-6">
                                                      <ul class="list-head pl0 m0 f-20 f-md-20 lh130 w400 white-clr">
                                                            <li>Complete Video & YouTube Business Builder</li>
                                                            <li>Ground-Breaking Method Brings Unlimited Traffic To Any Offer, Page Or Link</li>
                                                            <li>Tap Into 800 Million YouTube Videos & Make Any Video Yours Legally</li>
                                                            <li>Build Beautiful & Branded Video Channels on Any Topic in Any Niche Within Minutes</li>
                                                      </ul>
                                                </div>
                                                <div class="col-12 col-md-6">
                                                      <ul class="list-head pl0 m0 f-20 f-md-20 lh130 w400 white-clr">
                                                            <li>Can Upload, Host & Play Your Own Videos</li>
                                                            <li>100% FREE Viral, Social & SEO Traffic (Powerful)</li>
                                                            <li>Make Tons of Sales, Amazon/Affiliate Commissions & Leads.</li>
                                                            <li><u>FREE Commercial License</u> - Build An Incredible Income Offering Services & Keep 100%!</li>
                                                      </ul>
                                                </div>
                                          </div>
                                    </div>

                              </div>                        
                        </div>      
                  </div>
            </div>
      </div>
   
      <!--Vidvee Header Section End -->


      <div class="gr-1">
         <div class="container-fluid p0">
            <picture>
               <source media="(min-width:768px)" srcset="assets/images/vidvee-steps.png">
               <source media="(min-width:320px)" srcset="assets/images/vidvee-steps-mview.png" style="width:100%" class="vidvee-mview">
               <img src="assets/images/vidvee-steps.png" alt="Vidvee Steps" class="img-fluid">
            </picture>
            <picture>
               <source media="(min-width:768px)" srcset="assets/images/no-doubt-vidvee.png">
               <source media="(min-width:320px)" srcset="assets/images/no-doubt-mview-vidvee.png">
               <img src="assets/images/no-doubt-vidvee.png" alt="Flowers" style="width:100%;">
            </picture>
            <picture>
               <source media="(min-width:768px)" srcset="assets/images/proudly-vidvee.png">
               <source media="(min-width:320px)" srcset="assets/images/proudly-mview-vidvee.png">
               <img src="assets/images/proudly-vidvee.png" alt="Flowers" style="width:100%;">
            </picture>
         </div>
      </div>

      <!-- Bonus Section Header Start -->
      <div class="bonus-header">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-10 mx-auto heading-bg text-center">
                  <div class="f-24 f-md-36 lh140 w700"> When You Purchase JOBiin, You Also Get <br class="hidden-xs"> Instant Access To These 20 Exclusive Bonuses</div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus Section Header End -->
       <!-- Bonus #1 Section Start -->
       <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 1</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="https://cdn.oppyo.com/launches/jobiin/special-bonus/bonus1.webp" class="img-fluid mx-auto d-block" alt="Bonus1">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh150 bonus-title-color">Speak With Confidence</div>
                        <ul class="bonus-list f-18 f-md-20 w500 lh150 mt20 mt-md30 p0">
                           <li>
                           Speaking with confidence is a must for every successful business owner. This guide will teach you the skills and techniques you need to finally give a great speech and impress everyone in your audience. 
                           </li>
                           <li>
                           Get this Bonus with JOBiin and sell it to your subscribers to build huge authority by using it on their job sites. 
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #1 Section End -->
      <!-- Bonus #2 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 2</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30">
                  <div class="row">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="https://cdn.oppyo.com/launches/jobiin/special-bonus/bonus2.webp" class="img-fluid mx-auto d-block" alt="Bonus2">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w700 lh150 bonus-title-color mt20 mt-md0">
                           Successful Job Interview
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w500 lh150 mt20 mt-md30 p0">
                           <li>
                           Having a successful job interview is a pre-requisite for a satisfied finincial future ahead. So, this guide includes tips how to get job and succeed in interview easily.  
                           </li>
                           <li>
                           Get this Bonus with JOBiin and sell it to your subscribers to make extra income wihtout any extra efforts.  
                         </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #2 Section End -->
      <!-- Bonus #3 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 3</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="https://cdn.oppyo.com/launches/jobiin/special-bonus/bonus3.webp" class="img-fluid mx-auto d-block " alt="Bonus3">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh150 bonus-title-color">
                           Creating A Stream of Traffic And How To Maintain It
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w500 lh150 mt20 mt-md30 p0">
                           <li>Traffic is the lifestream of every successful business online.  With this package, learn how helping others benefits you and how you can begin accomplishing powerful goals in the process.   </li>
                           <li>
                           This Bonus will help you drive targeted traffic on the job sites created with JOBiin.   </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #3 Section End -->
      <!-- Bonus #4 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 4</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="https://cdn.oppyo.com/launches/jobiin/special-bonus/bonus4.webp" class="img-fluid mx-auto d-block " alt="Bonus4">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w700 lh150 bonus-title-color">
                           How To Start a Freelance Business
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w500 lh150 mt20 mt-md30 p0">
                           <li>							  
                           Starting a freelance business is a dream of every marketer. This ebook is full of all the tips, tricks, and tried and tested strategies that you will need to start a successful freelance business.  
                           </li>
                           <li>
                           This Bonus will help you build a successful Freelancing Hiring Agency.  
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #4 End -->
      <!-- Bonus #5 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 5</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="https://cdn.oppyo.com/launches/jobiin/special-bonus/bonus5.webp" class="img-fluid mx-auto d-block " alt="Bonus5">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh150 bonus-title-color">
                           The Internet Marketer's Toolkit
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w500 lh150 mt20 mt-md30 p0">
                           <li>
                           Learn the basics of internet marketing 101 even if you have no prior experience. 
                           </li>
                           <li>
                           This Bonus will help you viral your Job Search Sites & get you huge revenue to kickstart your business.  
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #5 End -->
      <!-- CTA Button Section Start -->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w500">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-22 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-18 f-md-22 lh150 w500 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 orange-clr">"JOBiin"</span> for Additional <span class="w700 orange-clr">$3 Discount</span> on Entire Funnel
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab JOBiin + My 20 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
                  <img src="https://cdn.oppyo.com/launches/jobiin/special-bonus/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md20" align="center">
                  <h3 class="f-md-35 f-md-28 f-20 w600 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w600 smmltd">Days</span> </div>
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w600 smmltd">Hours</span> </div>
                     <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w600 smmltd">Mins</span> </div>
                     <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w600 smmltd">Sec</span> </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End   -->
      <!-- Bonus #6 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 6</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="https://cdn.oppyo.com/launches/jobiin/special-bonus/bonus6.webp" class="img-fluid mx-auto d-block " alt="Bonus6">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w700 lh150 bonus-title-color">
                           Traffic Beast
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w500 lh150 mt20 mt-md30 p0">
                           <li>
                           Driving traffic is vital for every success hungry marketer. Use this bonus to learn the advanced techniques of traffic generation like Content Creation, SEO and Leveraging Analytics.  
                           </li>
                           <li>
                           This Bonus will leverage your Job Sites to the next level and make you a successful profitable business for months and years to come.
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #6 End -->
      <!-- Bonus #7 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 7</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="https://cdn.oppyo.com/launches/jobiin/special-bonus/bonus7.webp" class="img-fluid mx-auto d-block " alt="Bonus7">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh150 bonus-title-color">
                           BIZ Landing Page Plugin
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w500 lh150 mt20 mt-md30 p0">
                           <li>
                           With this useful WordPress plugin, you can create an all-in-one website that will pull in multiple sources and display in one place.  
                           </li>
                           <li>
                           Use this exciting Bonus with JOBiin to get best results in a cost effective manner.  
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #7 End -->
      <!-- Bonus #8 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 8</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="https://cdn.oppyo.com/launches/jobiin/special-bonus/bonus8.webp" class="img-fluid mx-auto d-block " alt="Bonus8">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh150 bonus-title-color">
                           Success in Business
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w500 lh150 mt20 mt-md30 p0">
                           <li>
                           This Bonus Package includes source ebook document, 25 PLR articles, Product Analysis PDF and a Fast Action Ideas PDF.   
                           </li>
                           <li>
                           Dominate Your Competition with Success in Business PLR Content Package!  
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #8 End -->
      <!-- Bonus #9 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 9</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="https://cdn.oppyo.com/launches/jobiin/special-bonus/bonus9.webp" class="img-fluid mx-auto d-block " alt="Bonus9">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh150 bonus-title-color">
                           Job Breakthrough
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w500 lh150 mt20 mt-md30 p0">
                           <li>
                           Secret tips and techniques to get your resume noticed and get your phone ringing for job interviews.
                           </li>
                           <li>
                           Get this Bonus with JOBiin and use it as lead magnet to generate huge list.  
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #9 End -->
      <!-- Bonus #10 start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 10</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="https://cdn.oppyo.com/launches/jobiin/special-bonus/bonus10.webp" class="img-fluid mx-auto d-block " alt="Bonus10">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh150 bonus-title-color">
                           How To Ace Any Job Interview
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w500 lh150 mt20 mt-md30 p0">
                           <li>
                           Learn how to shake those interview jitters and sail through all your interviews with these simple and effective tips.  
                           </li>
                           <li>
                           Get this Bonus with JOBiin and use it to make some extra income by selling it.  
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #10 End -->
      <!-- CTA Button Section Start -->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w500">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-22 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-18 f-md-22 lh150 w500 text-center mt20 white-clr">
                  Use Coupon Code <span class="w700 orange-clr">"JOBiin"</span> for Additional <span class="w700 orange-clr">$3 Discount</span> on Entire Funnel
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab JOBiin + My 20 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
                  <img src="https://cdn.oppyo.com/launches/jobiin/special-bonus/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md20" align="center">
                  <h3 class="f-md-35 f-md-28 f-20 w600 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w600 smmltd">Days</span> </div>
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w600 smmltd">Hours</span> </div>
                     <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w600 smmltd">Mins</span> </div>
                     <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w600 smmltd">Sec</span> </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->
      <!-- Bonus #11 start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 11</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="https://cdn.oppyo.com/launches/jobiin/special-bonus/bonus11.webp" class="img-fluid mx-auto d-block " alt="Bonus11">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh150 bonus-title-color">
                           100 Resume Tips
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w500 lh150 mt20 mt-md30 p0">
                           <li>
                           Get your copy of "100 Resume Tips" to nail your dream job with your resume.   </li>
                           <li>
                           Get this Bonus with JOBiin and use it to make some extra income by selling it.  
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #11 End -->
      <!-- Bonus #12 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 12</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="https://cdn.oppyo.com/launches/jobiin/special-bonus/bonus12.webp" class="img-fluid mx-auto d-block " alt="Bonus12">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh150 bonus-title-color">
                           Interview Techniques
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w500 lh150 mt20 mt-md30 p0">
                           <li>Learn the advanced secrets to crack any kind of interview.  </li>
                           <li>Get this Bonus with JOBiin and sell it to your subscribers to build huge authority.   </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #12 End -->
      <!-- Bonus #13 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 13</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="https://cdn.oppyo.com/launches/jobiin/special-bonus/bonus13.webp" class="img-fluid mx-auto d-block " alt="Bonus13">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh150 bonus-title-color">
                           Resume Writing Secrets
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w500 lh150 mt20 mt-md30 p0">
                           <li>This guide will help you to get on the right track to write a professional resume for finding your dream job.   </li>
                           <li>Get this Bonus with JOBiin and sell it to your subscribers to make extra income.   </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #13 End -->
      <!-- Bonus #14 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 14</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="https://cdn.oppyo.com/launches/jobiin/special-bonus/bonus14.webp" class="img-fluid mx-auto d-block " alt="Bonus14">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh150 bonus-title-color">
                           Social Media Marketing Made Easy Video Upgrade
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w500 lh150 mt20 mt-md30 p0">
                           <li>This video course will give you the knowledge and tools you need to scale your business with social media marketing.</li>
                           <li>Use this business to learn social media marketing and build a huge viral social media traffic for your Job Sites.  </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #14 End -->
      <!-- Bonus #15 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 15</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="https://cdn.oppyo.com/launches/jobiin/special-bonus/bonus15.webp" class="img-fluid mx-auto d-block " alt="Bonus15">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh150 bonus-title-color">
                           Boost Your Website Traffic
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w500 lh150 mt20 mt-md30 p0">
                           <li>Having a stream of fresh traffic on your offers is a must for every marketer. With this package, discover how to utilize social media networks to gain more followers and direct more traffic to your website.  </li>
                           <li>This Bonus will help you viral your Job Search Sites & get you huge revenue to kickstart your business.   </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #15 End -->
      <!-- CTA Button Section Start -->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w500">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-22 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-18 f-md-22 lh150 w500 text-center mt20 white-clr">
                  Use Coupon Code <span class="w700 orange-clr">"JOBiin"</span> for Additional <span class="w700 orange-clr">$3 Discount</span> on Entire Funnel
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab JOBiin + My 20 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
                  <img src="https://cdn.oppyo.com/launches/jobiin/special-bonus/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md20" align="center">
                  <h3 class="f-md-35 f-md-28 f-20 w600 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w600 smmltd">Days</span> </div>
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w600 smmltd">Hours</span> </div>
                     <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w600 smmltd">Mins</span> </div>
                     <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w600 smmltd">Sec</span> </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->
      <!-- Bonus #16 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 16</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="https://cdn.oppyo.com/launches/jobiin/special-bonus/bonus16.webp" class="img-fluid mx-auto d-block " alt="Bonus16">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh150 bonus-title-color">
                           Content Marketing Formula
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w500 lh150 mt20 mt-md30 p0">
                           <li>In this guide you will learn how content marketing works & a complete content marketing formula that you can adapt to your own brand. </li>
                           <li>This Bonus will leverage your branding and enhance your marketing in front of your customers. </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #16 End -->
      <!-- Bonus #17 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 17</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="https://cdn.oppyo.com/launches/jobiin/special-bonus/bonus17.webp" class="img-fluid mx-auto d-block " alt="Bonus17">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh150 bonus-title-color">
                           Surfire Chat Secrets
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w500 lh150 mt20 mt-md30 p0">
                           <li>A step-by-step course to teach you how to build a Facebook messenger chatbot to generate leads without learning how to code.   </li>
                           <li>This Bonus will automate your customer support system by helping you build a facebook messenger chatbot.  </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #17 End -->
      <!-- Bonus #18 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 18</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="https://cdn.oppyo.com/launches/jobiin/special-bonus/bonus18.webp" class="img-fluid mx-auto d-block " alt="Bonus18">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh150 bonus-title-color">
                           Instant Infographics Creator Review Pack
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w500 lh150 mt20 mt-md30 p0">
                           <li>Infographics have proved their worth in order to attract customer attention & drive more traffic on offers. It's an Infographics Creator that creates beautiful and converting niche-specific Infographics in minutes.  </li>
                           <li>This Bonus will help you capture your audience's attention by creating instant infographics on job sites created with JOBiin.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #18 End -->
      <!-- Bonus #19 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 19</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="https://cdn.oppyo.com/launches/jobiin/special-bonus/bonus19.webp" class="img-fluid mx-auto d-block " alt="Bonus19">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh150 bonus-title-color">
                           Facebook Ad Secrets
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w500 lh150 mt20 mt-md30 p0">
                           <li>FB Ad Secrets is designed for beginners who want to learn and for existing marketers who want to sharpen their FB advertising skills.  </li>
                           <li>This Bonus will help you drive huge valuable traffic from Facebook, & use it to boost commissions on your job sites.  </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #19 End -->
      <!-- Bonus #20 start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 20</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="https://cdn.oppyo.com/launches/jobiin/special-bonus/bonus20.webp" class="img-fluid mx-auto d-block " alt="Bonus20">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh150 bonus-title-color">
                           Ace Any Job Interview
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w500 lh150 mt20 mt-md30 p0">
                           <li>This booklet is for anyone who would like to improve their confidence and skills in preparing for and performing well in job interviews.  </li>
                           <li>Grab this Bonus with JOBiin and use it as a strong lead magnet to attract high converting leads.  </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #20 End -->
      <!-- Huge Woth Section Start -->
      <div class="huge-area mt30 mt-md10">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="f-md-65 f-40 lh120 w700 white-clr">That’s Huge Worth of</div>
                  <br>
                  <div class="f-md-60 f-40 lh120 w800 white-clr">$2795!</div>
               </div>
            </div>
         </div>
      </div>
      <!-- Huge Worth Section End -->
      <!-- text Area Start -->
      <div class="white-section pb0">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="f-md-36 f-25 lh140 w600">So what are you waiting for? You have a great opportunity <br class="hidden-xs"> ahead + <span class="w700 blue-clr">My 20 Bonus Products</span> are making it a <br class="hidden-xs"> <span class="w700 blue-clr">completely NO Brainer!!</span></div>
               </div>
            </div>
         </div>
      </div>
      <!-- text Area End -->
      <!-- CTA Button Section Start -->
      <div class="cta-btn-section">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center">
                  <a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab JOBiin + My 20 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               <div class="col-12 mt15 mt-md20" align="center">
                  <h3 class="f-md-30 f-20 w600 text-center black">My Exclusive Bonuses Are Expiring in...</h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 col-md-10 mx-auto col-12 text-center">
                  <div class="countdown counter-black">
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w600 smmltd ">Days</span> </div>
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w600 smmltd">Hours</span> </div>
                     <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w600 smmltd">Mins</span> </div>
                     <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w600 smmltd">Sec</span> </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->
      <!--Footer Section Start -->
      <div class="footer-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <svg id="Layer_1 " xmlns="http://www.w3.org/2000/svg " viewBox="0 0 128.19 38 " style="max-height:55px;">
                     <defs>
                        <style>.cls-1,.cls-2{fill:#fff;}.cls-2{fill-rule:evenodd;}.cls-3{fill:#0a52e2;}</style>
                     </defs>
                     <rect class="cls-1
                        " x="79.21 " width="48.98 " height="38 " rx="4 " ry="4 "/>
                     <path class="cls-1 " d="M18.64,4.67V24.72c0,1.29-.21,2.47-.63,3.54-.42,1.07-1.03,1.97-1.84,2.71-.81,.74-1.79,1.32-2.93,1.74-1.15,.42-2.45,.63-3.9,.63s-2.64-.19-3.7-.57c-1.07-.38-1.98-.92-2.75-1.62-.77-.7-1.39-1.54-1.88-2.51S.19,26.6,0,25.41l5.74-1.13c.46,2.45,1.63,3.68,3.52,3.68,.89,0,1.65-.28,2.28-.85,.63-.57,.95-1.46,.95-2.67V9.6H3.92V4.67h14.72Z
                        "/>
                     <path class="cls-1 " d="M54.91,33.37V4.63h9.71c3.38,0,6.02,.66,7.92,1.97,1.9,1.32,2.84,3.28,2.84,5.9,0,1.33-.35,2.52-1.06,3.56-.7,1.05-1.73,1.83-3.07,2.36,1.72,.37,3.02,1.16,3.88,2.37,.86,1.21,1.29,2.61,1.29,4.21,0,2.75-.91,4.83-2.72,6.25-1.82,1.42-4.39,2.12-7.72,2.12h-11.08Zm5.76-16.7h4.15c1.54,0,2.72-.32,3.55-.95,.83-.63,1.24-1.55,1.24-2.76,0-1.33-.42-2.31-1.25-2.94-.84-.63-2.08-.95-3.74-.95h-3.95v7.6Zm0,3.99v8.27h5.31c1.53,0,2.69-.33,3.49-.99,.8-.66,1.2-1.64,1.2-2.94,0-1.4-.34-2.48-1.03-3.22-.68-.74-1.76-1.11-3.24-1.11h-5.75Z
                        "/>
                     <g>
                        <path class="cls-1 " d="M46.84,4.19C42.31-.12,36.87-1.08,31.16,1.2c-5.82,2.33-9.1,6.88-9.52,13.21-.41,6.17,2.57,10.9,6.84,15.15,.95-.84,1.22-1.22,2.01-2.05-.79-.65-1.41-1.09-2.43-2.11-5.86-6.15-5.78-15.66,1.04-20.46,5-3.52,11.71-3.13,16.37,.95,4.29,3.75,5.54,10.44,2.59,15.61-1.2,2.11-3.09,3.84-4.86,5.98,.28,.45,.72,1.18,1.44,2.35,1.77-2.01,3.11-3.72,4.38-5.63,4.39-6.6,3.56-14.59-2.17-20.02Z
                           "/>
                        <g>
                           <path class="cls-2 " d="M39.67,27.34c-2.49,.37-4.9,.52-7.23-.46-.54-.23-1.13-1.06-1.18-1.65-.3-3.69,1.25-5.27,4.98-5.26,.31,0,.62,0,.92,.01,5.31-.01,5.96,6.85,2.51,7.36Z "/>
                           <path class="cls-2 " d="M39.49,16.69c-.04,1.69-1.37,2.98-3.06,2.98-1.6,0-3.01-1.43-3.01-3.06s1.39-3.04,3.02-3.03c1.75,0,3.1,1.38,3.06,3.12Z
                              "/>
                        </g>
                        <g>
                           <g>
                              <path class="cls-2 " d="M42.93,26.23c-.67-3.46-.93-5.25-3.46-7.18,.63-1.21,1.3-1.83,2.1-2,2.43-.53,3.98,.21,4.84,1.97,.66,1.34,.78,2.29-.37,3.77-1.03,1.17-1.85,2.21-3.11,3.43Z "/>
                              <path class="cls-2 " d="M44.94,13.4c.01,1.6-1.27,2.91-2.88,2.92-1.6,.01-2.91-1.28-2.92-2.88-.01-1.6,1.28-2.9,2.88-2.92,1.6-.01,2.91,1.28,2.92,2.88Z
                                 "/>
                           </g>
                           <g>
                              <path class="cls-2 " d="M30.1,26.23c.67-3.46,.93-5.25,3.46-7.18-.63-1.21-1.3-1.83-2.1-2-2.43-.53-3.98,.21-4.84,1.97-.66,1.34-.88,2.01,.27,3.49,.96,1.3,1.88,2.44,3.21,3.71Z "/>
                              <path class="cls-2 " d="M28.08,13.4c0,1.6,1.28,2.91,2.88,2.92,1.6,.01,2.91-1.28,2.92-2.88,.01-1.6-1.27-2.9-2.88-2.92-1.6-.01-2.91,1.28-2.92,2.88Z
                                 "/>
                           </g>
                        </g>
                        <path class="cls-1 " d="M42.02,27.74c-.7,.41-6.95,2.1-10.73,.08l-2.22,2.57c.55,.61,5.12,5.27,7.55,7.6,2.22-2.17,7.2-7.37,7.2-7.37l-1.8-2.89Z "/>
                     </g>
                     <g>
                        <rect class="cls-3 " x="85.05 " y="4.63 " width="5.38 " height="5.4 " rx="2.69
                           " ry="2.69 "/>
                        <rect class="cls-3 " x="85.05 " y="13.43 " width="5.38 " height="19.94 "/>
                        <rect class="cls-3 " x="95.13 " y="4.63 " width="5.38 " height="5.4 " rx="2.69 " ry="2.69 "/>
                        <rect class="cls-3 " x="95.13 " y="13.43
                           " width="5.38 " height="19.94 "/>
                        <path class="cls-3 " d="M109.85,13.43l.24,2.86c.66-1.02,1.48-1.81,2.45-2.38,.97-.56,2.06-.85,3.26-.85,2.01,0,3.59,.63,4.72,1.9,1.13,1.27,1.7,3.25,1.7,5.95v12.46h-5.4v-12.47c0-1.34-.27-2.29-.81-2.85-.54-.56-1.36-.84-2.45-.84-.71,0-1.35,.14-1.92,.43-.57,.29-1.04,.7-1.42,1.23v14.5h-5.38V13.43h5.01Z "/>
                     </g>
                  </svg>
                  <div editabletype="text " class="f-16 f-md-18 w400 mt20 lh130 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-md-18 f-16 w400 lh130 white-clr text-xs-center">Copyright © JOBiin 2022</div>
                  <ul class="footer-ul w400 f-md-18 f-16 white-clr text-center text-md-right">
                     <li><a href="https://support.bizomart.com/hc/en-us " class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://getjobiin.co/legal/privacy-policy.html " class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://getjobiin.co/legal/terms-of-service.html " class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://getjobiin.co/legal/disclaimer.html " class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://getjobiin.co/legal/gdpr.html " class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://getjobiin.co/legal/dmca.html " class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://getjobiin.co/legal/anti-spam.html " class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!--Footer Section End -->
      <!-- timer --->
      <?php
         if ($now < $exp_date) {
         ?>
      <script type="text/javascript">
         // Count down milliseconds = server_end - server_now = client_end - client_now
         var server_end = <?php echo $exp_date; ?> * 1000;
         var server_now = <?php echo time(); ?> * 1000;
         var client_now = new Date().getTime();
         var end = server_end - server_now + client_now; // this is the real end time
         
         var noob = $('.countdown').length;
         
         var _second = 1000;
         var _minute = _second * 60;
         var _hour = _minute * 60;
         var _day = _hour * 24
         var timer;
         
         function showRemaining() {
         var now = new Date();
         var distance = end - now;
         if (distance < 0) {
         	clearInterval(timer);
         	document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
         	return;
         }
         
         var days = Math.floor(distance / _day);
         var hours = Math.floor((distance % _day) / _hour);
         var minutes = Math.floor((distance % _hour) / _minute);
         var seconds = Math.floor((distance % _minute) / _second);
         if (days < 10) {
         	days = "0" + days;
         }
         if (hours < 10) {
         	hours = "0" + hours;
         }
         if (minutes < 10) {
         	minutes = "0" + minutes;
         }
         if (seconds < 10) {
         	seconds = "0" + seconds;
         }
         var i;
         var countdown = document.getElementsByClassName('countdown');
         for (i = 0; i < noob; i++) {
         	countdown[i].innerHTML = '';
         
         	if (days) {
         		countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + days + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 smmltd">Days</span> </div>';
         	}
         
         	countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + hours + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w600 smmltd">Hours</span> </div>';
         
         	countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">' + minutes + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w600 smmltd">Mins</span> </div>';
         
         	countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">' + seconds + '</span><br><span class="f-14 f-md-18 w600 smmltd">Sec</span> </div>';
         }
         
         }
         timer = setInterval(showRemaining, 1000);
         	
      </script>
      <?php
         } else {
         echo "Times Up";
         }
         ?>
      <!--- timer end-->
   </body>
</html>
/*--------------------- Copyright (c) 2021 -----------------------
[Master Javascript]
-------------------------------------------------------------------*/
(function ($) {
  "use strict";
  var photographer = {
    initialised: false,
    version: 1.0,
    mobile: false,
    init: function () {
      if (!this.initialised) {
        this.initialised = true;
      } else {
        return;
      }
      /*-------------- Design Functions Calling ---------------------------------------------------
      ------------------------------------------------------------------------------------------------*/
      this.pgloader();
      this.pgtoggle();
   
      this.pgbottom_top();
   
   


    },

    /*-------------- Design Functions Calling ---------------------------------------------------
    --------------------------------------------------------------------------------------------------*/

    // loader           
    pgloader: function () {
      jQuery(window).on('load', function () {
        $(".pg-loader").fadeOut();
        $(".pg-spinner").delay(500).fadeOut("slow");
      });
    },
    // loader

    //Toggle
    pgtoggle: function () {
      $(".jv-toggle-btn").on('click', function (e) {
        e.stopPropagation();
        $("body").toggleClass("menu-open");
      });
    },


    // Bottom To Top
    pgbottom_top: function () {
      if ($('#button').length > 0) {

        var btn = $('#button');

        $(window).scroll(function () {
          if ($(window).scrollTop() > 300) {
            btn.addClass('show');
          } else {
            btn.removeClass('show');
          }
        });

        btn.on('click', function (e) {
          e.preventDefault();
          $('html, body').animate({
            scrollTop: 0
          }, '300');
        });


      }
    },
    // Bottom To Top

    




  };
  photographer.init();

}(jQuery));









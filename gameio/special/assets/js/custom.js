/*--------------------- Copyright (c) 2022 -----------------------
[Master Javascript]
-------------------------------------------------------------------*/
(function ($) {
  "use strict";
  var jvpage = {
    initialised: false,
    version: 1.0,
    mobile: false,
    init: function () {
      if (!this.initialised) {
        this.initialised = true;
      } else {
        return;
      }
      /*-------------- Design Functions Calling ---------------------------------------------------
      ------------------------------------------------------------------------------------------------*/

      this.pgtoggle();
         this.pgbottom_top();
    },

    /*-------------- Design Functions Calling ---------------------------------------------------
    --------------------------------------------------------------------------------------------------*/


    //Toggle
    pgtoggle: function () {
      $(".jv-toggle-btn").on('click', function (e) {
        e.stopPropagation();
        $("body").toggleClass("menu-open");
      });
    },


    // Bottom To Top
    pgbottom_top: function () {
      if ($('#button').length > 0) {

        var btn = $('#button');

        $(window).scroll(function () {
          if ($(window).scrollTop() > 300) {
            btn.addClass('show');
          } else {
            btn.removeClass('show');
          }
        });

        btn.on('click', function (e) {
          e.preventDefault();
          $('html, body').animate({
            scrollTop: 0
          }, '300');
        });


      }
    },
    // Bottom To Top

    




  };
  jvpage.init();

}(jQuery));









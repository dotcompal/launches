<!DOCTYPE html>
<html>
   <head>
   <title>WEBPULL Special</title>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=9">
   <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
   <meta name="title" content="WEBPULL Special">
   <meta name="description" content="3-Click Website Builder That Makes Us $525/Day Creating Premium Business Websites Preloaded with 400+ Done For You Themes in Hot Niches Ready To Sell In Just 60 Seconds">
   <meta name="keywords" content="WEBPULL Special">
   <meta property="og:image" content="https://www.webpull.co/special/thumbnail.png">
   <meta name="language" content="English">
   <meta name="revisit-after" content="1 days">
   <meta name="author" content="Ayush Jain">
   <!-- Open Graph / Facebook -->
   <meta property="og:type" content="website">
   <meta property="og:title" content="WEBPULL Special">
   <meta property="og:description" content="3-Click Website Builder That Makes Us $525/Day Creating Premium Business Websites Preloaded with 400+ Done For You Themes in Hot Niches Ready To Sell In Just 60 Seconds">
   <meta property="og:image" content="https://www.webpull.co/special/thumbnail.png">
   <!-- Twitter -->
   <meta property="twitter:card" content="summary_large_image">
   <meta property="twitter:title" content="WEBPULL Special">
   <meta property="twitter:description" content="3-Click Website Builder That Makes Us $525/Day Creating Premium Business Websites Preloaded with 400+ Done For You Themes in Hot Niches Ready To Sell In Just 60 Seconds">
   <meta property="twitter:image" content="https://www.webpull.co/special/thumbnail.png">
      <link rel="icon" href="https://cdn.oppyo.com/launches/webpull/common_assets/images/favicon.png" type="image/png">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
      <!-- Start Editor required -->
      <link rel="stylesheet" href="https://cdn.oppyo.com/launches/webpull/common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style.css" type="text/css" />
      <link rel="stylesheet" href="assets/css/timer.css" type="text/css" />
      <script src="https://cdn.oppyo.com/launches/webpull/common_assets/js/jquery.min.js"></script>
      <script>
         (function(w, i, d, g, e, t, s) {
             if (window.businessDomain != undefined) {
                 console.log("Your page have duplicate embed code. Please check it.");
                 return false;
             }
             businessDomain = 'webpull';
             allowedDomain = 'webpull.co';
             if (!window.location.hostname.includes(allowedDomain)) {
                 console.log("Your page have not authorized. Please check it.");
                 return false;
             }
             console.log("Your script is ready...");
             w[d] = w[d] || [];
             t = i.createElement(g);
             t.async = 1;
             t.src = e;
             s = i.getElementsByTagName(g)[0];
             s.parentNode.insertBefore(t, s);
         })(window, document, '_gscq', 'script', 'https://cdn.staticdcp.com/apps/engage/smart_engage/js/config-loader.js');
      </script>
   </head>
   <body>
      <!-- Header Section Start -->
      <div class="header-section">
         <div class="container">
            <div class="row">
               <div class="col-md-3 text-md-left text-center">
                  <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 394.2 100" style="enable-background:new 0 0 394.2 100; max-height:60px" xml:space="preserve">
                     <style type="text/css">
                        .st00 {
                        fill: #FFFFFF;
                        }
                        .st11 {
                        fill: url(#SVGID_1_);
                        }
                        .st22 {
                        fill: url(#SVGID_00000021093791325859232720000015953972570224053676_);
                        }
                        .st33 {
                        fill: #FFC802;
                        }
                        .st44 {
                        fill: url(#SVGID_00000003068462962921929030000010945183465240898967_);
                        }
                     </style>
                     <path class="st00"
                        d="M117.7,37.9l-5,12.4H92.9v12.4h18.6l-5,12.4H92.9v12.4h24.8l-5,12.5H92.9H80.5V75.1V62.7V37.9H117.7z">
                     </path>
                     <path class="st00" d="M323.2,37.9v49.7H348V100h-37.3V37.9H323.2z"></path>
                     <path class="st00" d="M369.4,37.9v49.7h24.8V100h-37.3V37.9H369.4z"></path>
                     <g>
                        <g>
                           <polygon class="st00" points="59.1,71.4 52.4,54.4 59.1,37.9 72.6,37.9   "></polygon>
                           <polygon class="st00" points="29.4,54.7 36.3,37.9 54.5,82.9 47.7,100   "></polygon>
                           <polygon class="st00" points="24.8,100 31.6,82.8 13.4,37.9 0,37.9   "></polygon>
                        </g>
                     </g>
                     <path class="st00"
                        d="M222.8,44.3c-4.3-4.3-9.4-6.4-15.4-6.4h-9.3h-12.5v21.7v21.7V100h12.5V81.3h0V68.9h0v-9.3v-9.3h9.3  c2.6,0,4.8,0.9,6.6,2.8c1.8,1.8,2.7,4,2.7,6.6c0,2.6-0.9,4.8-2.7,6.6c-1.8,1.8-4,2.7-6.6,2.7h-0.7v12.4h0.7c6,0,11.1-2.1,15.4-6.4  c4.3-4.3,6.4-9.4,6.4-15.4C229.2,53.6,227.1,48.5,222.8,44.3z">
                     </path>
                     <rect x="252.1" y="31.8" transform="matrix(0.4226 -0.9063 0.9063 0.4226 114.0209 255.0236)"
                        class="st00" width="10.2" height="12.5"></rect>
                     <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="249.3741" y1="97.3262"
                        x2="270.7371" y2="53.5256">
                        <stop offset="0" style="stop-color:#FA8460"></stop>
                        <stop offset="1" style="stop-color:#FCB121"></stop>
                     </linearGradient>
                     <path class="st11"
                        d="M271.7,80.3c-1.4,3.1-3.8,5.2-7,6.4c-3.2,1.2-6.4,1-9.5-0.4c-3.1-1.4-5.2-3.8-6.4-7c-1.2-3.2-1-6.4,0.4-9.5  l9.9-21.2l-11.3-5.3l-9.9,21.2c-2.9,6.2-3.2,12.6-0.8,19c2.3,6.5,6.6,11.1,12.9,14c6.2,2.9,12.5,3.2,19,0.8  c6.5-2.3,11.2-6.6,14.1-12.8l9.9-21.2l-11.3-5.3L271.7,80.3z">
                     </path>
                     <rect x="285.9" y="47.6" transform="matrix(0.4226 -0.9063 0.9063 0.4226 119.2494 294.738)"
                        class="st00" width="10.2" height="12.5"></rect>
                     <path class="st00"
                        d="M173.3,76.3c-0.8-1.7-1.7-3.2-2.9-4.5c-1.2-1.3-2.5-2.3-4-3.1c-1.5-0.8-3-1.2-4.5-1.3c1.5-0.1,2.8-0.5,4.1-1.3  c1.2-0.8,2.3-1.8,3.2-3c0.9-1.2,1.6-2.5,2.1-3.9c0.5-1.4,0.8-2.8,0.8-4c0-3.5-0.5-6.4-1.4-8.6c-1-2.2-2.3-4-3.9-5.3  c-1.6-1.3-3.6-2.2-5.8-2.6c-2.2-0.5-4.6-0.7-7-0.7h-27.3v10.5h11.9v0h15.4c0.8,0,1.7,0.2,2.4,0.6c0.8,0.4,1.4,0.9,2,1.5  c0.6,0.6,1,1.4,1.4,2.2c0.3,0.9,0.5,1.7,0.5,2.6c0,0.9-0.2,1.8-0.5,2.8c-0.3,0.9-0.8,1.8-1.4,2.5c-0.6,0.7-1.3,1.3-2.2,1.8  c-0.9,0.5-1.9,0.7-3.1,0.7h-9.1v10.5h10c2.7,0,4.9,0.7,6.4,2.1c1.6,1.4,2.3,3.2,2.3,5.3c0,1-0.2,2-0.6,3c-0.4,1-0.9,1.9-1.6,2.7  c-0.7,0.8-1.5,1.5-2.4,2c-0.9,0.5-1.9,0.7-2.9,0.7h-16.8h0V73.7h0V63.2h0v-9.3h-11.9V100h9.6h2.3h16.8c2.7,0,5.2-0.3,7.5-0.9  c2.3-0.6,4.4-1.6,6.2-3.1c1.8-1.4,3.1-3.3,4.2-5.7c1-2.3,1.5-5.2,1.5-8.6C174.4,79.8,174,78,173.3,76.3z">
                     </path>
                     <g>
                        <linearGradient id="SVGID_00000142155450205615942230000010409392281687221429_"
                           gradientUnits="userSpaceOnUse" x1="270.7616" y1="25.6663" x2="296.63" y2="25.6663">
                           <stop offset="0" style="stop-color:#FA8460"></stop>
                           <stop offset="4.692155e-02" style="stop-color:#FA9150"></stop>
                           <stop offset="0.1306" style="stop-color:#FBA43A"></stop>
                           <stop offset="0.2295" style="stop-color:#FBB229"></stop>
                           <stop offset="0.3519" style="stop-color:#FCBC1E"></stop>
                           <stop offset="0.5235" style="stop-color:#FCC117"></stop>
                           <stop offset="1" style="stop-color:#FCC315"></stop>
                        </linearGradient>
                        <path style="fill:url(#SVGID_00000142155450205615942230000010409392281687221429_);"
                           d="M289.6,14.2c-2.1-1.1-4.3-1.5-6.5-1.4   c-0.1,0-0.1,0-0.2,0c-0.1,0-0.2,0-0.2,0c0,0-0.1,0-0.1,0c-0.1,0-0.2,0-0.2,0c-0.1,0-0.2,0-0.3,0c-0.1,0-0.2,0-0.2,0   c-0.3,0-0.5,0.1-0.8,0.1c-0.1,0-0.2,0-0.3,0.1c-0.1,0-0.2,0-0.3,0.1c-0.1,0-0.3,0.1-0.4,0.1c-0.1,0-0.2,0-0.2,0.1   c-0.1,0-0.2,0.1-0.3,0.1c-0.2,0.1-0.5,0.2-0.7,0.3c0,0,0,0,0,0c-0.3,0.1-0.6,0.3-0.9,0.4c-0.3,0.1-0.6,0.3-0.9,0.5   c-0.1,0.1-0.3,0.2-0.4,0.2c-0.1,0.1-0.2,0.2-0.4,0.2c-0.1,0.1-0.2,0.1-0.2,0.2c0,0-0.1,0-0.1,0.1c-0.1,0.1-0.2,0.1-0.2,0.2   c-0.1,0-0.1,0.1-0.2,0.2c-0.1,0.1-0.2,0.2-0.3,0.3c-0.1,0-0.1,0.1-0.2,0.2c-0.5,0.4-0.9,0.9-1.3,1.4c-0.1,0.1-0.1,0.1-0.2,0.2   c0,0-0.1,0.1-0.1,0.1c-0.1,0.1-0.1,0.2-0.2,0.3c0,0-0.1,0.1-0.1,0.1c-0.1,0.1-0.1,0.2-0.2,0.3c0,0,0,0.1-0.1,0.1   c-0.1,0.1-0.1,0.2-0.2,0.3c0,0,0,0.1-0.1,0.1c-0.1,0.1-0.1,0.2-0.2,0.3c-0.1,0.1-0.2,0.3-0.2,0.4c-3.3,6.3-0.8,14.1,5.6,17.4   c1.9,1,3.9,1.4,5.9,1.4c0.1,0,0.3,0,0.4,0c0.1,0,0.3,0,0.4,0c0.1,0,0.1,0,0.2,0c0.1,0,0.2,0,0.3,0c0,0,0,0,0.1,0c0.1,0,0.2,0,0.3,0   c0.1,0,0.2,0,0.2,0c0.1,0,0.2,0,0.3-0.1c0.1,0,0.3,0,0.4-0.1c0.1,0,0.3-0.1,0.4-0.1c0.1,0,0.2-0.1,0.3-0.1c0.2,0,0.4-0.1,0.5-0.2   c0.1,0,0.3-0.1,0.4-0.1c0.1,0,0.3-0.1,0.4-0.1c0,0,0,0,0,0c0.1,0,0.2-0.1,0.4-0.1c0,0,0,0,0,0c0,0,0.1,0,0.1,0c0,0,0.1,0,0.1-0.1   c0,0,0,0,0.1,0c0,0,0.1,0,0.1,0c0,0,0,0,0.1,0c0.1,0,0.2-0.1,0.3-0.2c0,0,0,0,0.1,0c0.1-0.1,0.2-0.1,0.3-0.2   c0.1-0.1,0.3-0.1,0.4-0.2c0.4-0.2,0.7-0.5,1.1-0.7c0.2-0.2,0.5-0.4,0.7-0.6c0.2-0.2,0.4-0.4,0.7-0.6c0.2-0.2,0.4-0.4,0.6-0.6   c0.2-0.2,0.4-0.5,0.6-0.7c0.1-0.1,0.2-0.2,0.3-0.4c0,0,0,0,0,0c0.1-0.1,0.2-0.2,0.3-0.4c0.1-0.1,0.2-0.3,0.2-0.4   c0.1-0.1,0.1-0.2,0.2-0.3c0-0.1,0.1-0.1,0.1-0.2c0.1-0.1,0.1-0.2,0.2-0.3C298.5,25.3,296,17.5,289.6,14.2z M279.6,26.7L279.6,26.7   l0.3,0.6c0.4,0.7,1,1.3,1.8,1.7c0.3,0.2,0.7,0.3,1,0.3c0.3,0,0.5-0.1,0.7-0.2c0.1,0,0.1-0.1,0.2-0.2c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0-0.1c0.2-0.3,0.2-0.7-0.1-1.3c0,0,0,0,0,0   c-0.1-0.2-0.2-0.4-0.4-0.6c-0.1-0.1-0.2-0.2-0.3-0.3c-0.4-0.4-0.7-0.8-0.9-1.2c-0.1-0.1-0.2-0.2-0.2-0.4c0,0,0,0,0-0.1   c0,0,0-0.1-0.1-0.1c0,0,0-0.1-0.1-0.1c-0.1-0.2-0.1-0.3-0.2-0.5c-0.1-0.2-0.1-0.4-0.2-0.6c0,0,0-0.1,0-0.1c0-0.1,0-0.2,0-0.3v-0.1   c0-0.1,0-0.1,0-0.2c0,0,0-0.1,0-0.1c0-0.1,0-0.2,0-0.3c0.1-0.3,0.2-0.6,0.3-0.8c0,0,0,0,0-0.1c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0-0.1c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0l0,0c0.4-0.5,0.9-0.9,1.5-1.1c0.7-0.2,1.4-0.3,2.1-0.1l0.9-1.7l1.9,1l-0.8,1.6c0.7,0.4,1.2,1,1.6,1.6   l0.2,0.4l-1.8,1.6l-0.3-0.5c-0.1-0.1-0.3-0.4-0.5-0.7c-0.1-0.1-0.3-0.3-0.5-0.4c0,0-0.1-0.1-0.1-0.1c-0.1-0.1-0.3-0.2-0.4-0.2   c0,0-0.1,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0   c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0h0c0,0,0,0-0.1,0h0   c0,0,0,0-0.1,0h0c0,0,0,0,0,0h0c0,0,0,0,0,0h0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c-0.2,0.1-0.4,0.3-0.5,0.5c-0.3,0.5-0.2,0.9,1,2.3c0.8,0.9,1.2,1.6,1.4,2.4   c0.2,0.8,0.1,1.6-0.3,2.5c-0.8,1.5-2.4,2.2-4.2,1.8l-0.8,1.6l-0.1,0.2l-2-1l0.9-1.7c-0.1-0.1-0.2-0.1-0.3-0.2   c-0.3-0.2-0.5-0.4-0.8-0.6c0,0-0.1-0.1-0.1-0.1c-0.3-0.3-0.5-0.6-0.7-1l-0.2-0.4L279.6,26.7z">
                        </path>
                        <path class="st33"
                           d="M278,37c-6.3-3.3-8.8-11.1-5.6-17.4c1.3-2.4,3.2-4.3,5.4-5.5c-2.4,1.2-4.4,3.1-5.7,5.6   c-3.3,6.3-0.8,14.1,5.6,17.4c3.9,2,8.4,1.9,12-0.1C286.2,38.9,281.8,39,278,37z">
                        </path>
                     </g>
                     <g>
                        <linearGradient id="SVGID_00000016062571573767098340000005260099975199807150_"
                           gradientUnits="userSpaceOnUse" x1="79.734" y1="-130.471" x2="91.3973" y2="-130.471"
                           gradientTransform="matrix(0.8624 -0.5063 0.5063 0.8624 260.6894 161.6657)">
                           <stop offset="0" style="stop-color:#FA8460"></stop>
                           <stop offset="4.692155e-02" style="stop-color:#FA9150"></stop>
                           <stop offset="0.1306" style="stop-color:#FBA43A"></stop>
                           <stop offset="0.2295" style="stop-color:#FBB229"></stop>
                           <stop offset="0.3519" style="stop-color:#FCBC1E"></stop>
                           <stop offset="0.5235" style="stop-color:#FCC117"></stop>
                           <stop offset="1" style="stop-color:#FCC315"></stop>
                        </linearGradient>
                        <path style="fill:url(#SVGID_00000016062571573767098340000005260099975199807150_);"
                           d="M268.1,0c-1,0.1-2,0.4-2.8,0.9   c0,0-0.1,0-0.1,0c0,0-0.1,0-0.1,0.1c0,0,0,0,0,0c0,0-0.1,0-0.1,0.1c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0-0.1,0.1   c-0.1,0.1-0.2,0.1-0.3,0.2c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0.1-0.1,0.1   c-0.1,0.1-0.1,0.2-0.2,0.3c0,0,0,0,0,0c-0.1,0.1-0.2,0.2-0.2,0.4c-0.1,0.1-0.2,0.3-0.2,0.4c0,0.1-0.1,0.1-0.1,0.2   c0,0.1-0.1,0.1-0.1,0.2c0,0,0,0.1-0.1,0.1c0,0,0,0,0,0.1c0,0,0,0.1-0.1,0.1c0,0,0,0.1,0,0.1c0,0.1,0,0.1-0.1,0.2c0,0,0,0.1,0,0.1   c-0.1,0.3-0.1,0.5-0.2,0.8c0,0,0,0.1,0,0.1c0,0,0,0.1,0,0.1c0,0.1,0,0.1,0,0.2c0,0,0,0,0,0.1c0,0.1,0,0.1,0,0.2c0,0,0,0,0,0.1   c0,0.1,0,0.1,0,0.2c0,0,0,0,0,0.1c0,0.1,0,0.1,0,0.2c0,0.1,0,0.1,0,0.2c0.2,3.2,2.9,5.7,6.1,5.5c1-0.1,1.9-0.3,2.6-0.8   c0.1,0,0.1-0.1,0.2-0.1c0.1,0,0.1-0.1,0.1-0.1c0,0,0,0,0.1,0c0,0,0.1,0,0.1-0.1c0,0,0,0,0,0c0,0,0.1-0.1,0.1-0.1c0,0,0.1,0,0.1-0.1   c0,0,0.1-0.1,0.1-0.1c0,0,0.1-0.1,0.1-0.1c0,0,0.1-0.1,0.1-0.1c0,0,0.1-0.1,0.1-0.1c0.1-0.1,0.1-0.1,0.2-0.2c0,0,0.1-0.1,0.1-0.1   c0,0,0.1-0.1,0.1-0.1c0,0,0,0,0,0c0,0,0.1-0.1,0.1-0.1c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0-0.1c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0.1-0.1,0.1-0.1c0,0,0,0,0,0c0,0,0.1-0.1,0.1-0.1c0-0.1,0.1-0.1,0.1-0.2c0.1-0.2,0.2-0.4,0.3-0.5c0.1-0.1,0.1-0.2,0.1-0.4   c0-0.1,0.1-0.3,0.1-0.4c0-0.1,0.1-0.3,0.1-0.4c0-0.1,0-0.3,0.1-0.4c0-0.1,0-0.1,0-0.2c0,0,0,0,0,0c0-0.1,0-0.1,0-0.2   c0-0.1,0-0.1,0-0.2c0-0.1,0-0.1,0-0.2c0,0,0-0.1,0-0.1c0-0.1,0-0.1,0-0.2C274.1,2.3,271.3-0.2,268.1,0z M267.1,7.2L267.1,7.2   l0.3,0.2c0.3,0.2,0.7,0.3,1.1,0.3c0.2,0,0.3-0.1,0.4-0.1c0.1-0.1,0.2-0.1,0.2-0.3c0,0,0-0.1,0-0.1c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0-0.2-0.1-0.3-0.3-0.5c0,0,0,0,0,0   c-0.1,0-0.2-0.1-0.3-0.1c-0.1,0-0.1,0-0.2-0.1c-0.2-0.1-0.4-0.2-0.6-0.2c-0.1,0-0.1-0.1-0.2-0.1c0,0,0,0,0,0c0,0,0,0-0.1,0   c0,0,0,0-0.1,0c-0.1,0-0.1-0.1-0.2-0.1c-0.1-0.1-0.1-0.1-0.2-0.2c0,0,0,0,0,0c0,0-0.1-0.1-0.1-0.1l0,0c0,0,0,0,0-0.1c0,0,0,0,0,0   c0,0,0-0.1-0.1-0.1c0-0.1-0.1-0.3-0.1-0.4c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0l0,0   c0-0.3,0.2-0.5,0.3-0.8c0.2-0.2,0.5-0.4,0.8-0.5l0-0.9l1-0.1l0,0.8c0.4,0,0.7,0.1,1,0.3l0.2,0.1l-0.3,1l-0.3-0.1   c-0.1,0-0.2-0.1-0.4-0.1c-0.1,0-0.2,0-0.3-0.1c0,0-0.1,0-0.1,0c-0.1,0-0.1,0-0.2,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0l0,0c0,0,0,0,0,0l0,0c0,0,0,0,0,0l0,0c0,0,0,0,0,0l0,0c0,0,0,0,0,0l0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c-0.1,0.1-0.1,0.2-0.1,0.3c0,0.2,0.1,0.4,0.9,0.7c0.5,0.2,0.8,0.4,1.1,0.6c0.3,0.3,0.4,0.6,0.4,1c0,0.8-0.4,1.4-1.2,1.7l0,0.8   l0,0.1l-1,0.1l0-0.8c-0.1,0-0.1,0-0.2,0c-0.2,0-0.3,0-0.4-0.1c0,0-0.1,0-0.1,0c-0.2-0.1-0.4-0.1-0.5-0.2l-0.2-0.1L267.1,7.2z">
                        </path>
                        <path class="st33"
                           d="M268.8,11.5c-3.2,0.2-6-2.3-6.1-5.5c-0.1-1.2,0.3-2.4,0.9-3.4c-0.7,1-1,2.2-0.9,3.5c0.2,3.2,2.9,5.7,6.1,5.5   c2-0.1,3.7-1.2,4.6-2.8C272.4,10.4,270.7,11.4,268.8,11.5z">
                        </path>
                     </g>
                     <polygon class="st00"
                        points="297.8,43.3 296.4,42.7 294.6,46.7 295.4,41.2 296.8,41.8 298.7,37.9 "></polygon>
                     <polygon class="st00" points="264.2,27.2 262.9,26.4 260.7,30.2 262,24.8 263.3,25.6 265.5,21.8 ">
                     </polygon>
                  </svg>
               </div>
               <div class="col-md-7">
                  <ul class="leader-ul f-md-20 f-18 w500 white-clr text-md-end text-center">
                     <li><a href="#features" class="white-clr t-decoration-none">Features</a><span class="pl9 white-clr">|</span></li>
                     <li><a href="#demo" class="white-clr t-decoration-none">Demo</a><span class="pl9 white-clr d-md-none">|</span></li>
                     <li class="d-md-none"><a href="#buynow" class="white-clr t-decoration-none">Buy Now</a></li>
                  </ul>
               </div>
               <div class="col-md-2 affiliate-link-btn text-md-end text-center mt15 mt-md0 d-none d-md-block">
                  <a href="#buynow">Buy Now</a>
               </div>
               <div class="col-12 text-center mt20 mt-md50">
                  <div class="pre-heading f-md-21 f-20 w500 lh140 white-clr">
                     Simple Agency Services That Get Daily Clients and Make You Huge Profits (Anyone Can Do This)
                  </div>
               </div>
               <div class="col-12 mt-md25 mt20 f-md-45 f-28 w700 text-center white-clr lh140 text-shadow">
                  Breakthrough 3-Click Website Builder That <span class="orange-clr">Makes Us $525/Day Creating Premium Business Websites</span> Preloaded with 400+ Done For You Themes in Hot Niches Ready To Sell In Just 60 Seconds
               </div>
               <div class="col-12 mt-md25 mt20 f-18 f-md-24 w500 text-center lh140 white-clr text-capitalize">
                  Tap into the $284 Bn Industry to Make Easy Profits | Easily Create & Sell Websites in 100+ Niches | Create Local Business Website, Affiliate Site & Ecom Stores | 100% Newbie Friendly
               </div>
               <div class="col-12 mt20 mt-md40">
                  <div class="row d-flex align-items-center flex-wrap">
                     <div class="col-md-10 mx-auto col-12">
                        <img src="https://cdn.oppyo.com/launches/webpull/jv/product-box.webp" class="img-fluid d-block mx-auto" alt="Product">
                        <!-- <div class="col-12 responsive-video">
                           <iframe src="https://webpull.dotcompal.com/video/embed/8ghfrcnhp5" style="width:100%; height:100%" frameborder="0" allow="fullscreen" allowfullscreen></iframe>
                           </div> -->
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50">
                  <div class="f-md-32 f-22 w700 lh140 text-capitalize text-center white-clr">
                     LIMITED TIME - FREE COMMERCIAL LICENSE INCLUDED
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="list-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row header-list-block">
                     <div class="col-12 col-md-6">
                        <div class="f-18 f-md-20 lh140 w400">
                           <ul class="header-bordered-list pl0">
                              <li><span class="w600">Tap Into Fast-Growing $284 Billion Website </span>Creation Industry to Make Easy Income</li>
                              <li> <span class="w600">Create Beautiful, Fast loading, SEO Friendly, and 100% Mobile Responsive Website</span> in Just 60 Seconds
                              </li>
                              <li> <span class="w600">Instantly Create Local Business Website, E-com Sites, and Blogs</span> for any Business or Niche
                              </li>
                              <li> <span class="w600">Accept Online Payments For Your Services & Products</span> with Seamless WooCommerce Integration</li>
                              <li>
                                 <span class="w600">
                                    Commercial License Included to Build an Incredible Income Helping Clients!
                              </li>
                           </ul>
                        </div>
                     </div>
                     <div class="col-12 col-md-6">
                     <div class="f-18 f-md-20 lh140 w400">
                     <ul class="header-bordered-list pl0">
                     <li> <span class="w600">Help Local Businesses Go Digital in Their Hard Time (IN TODAYS DIGITAL AGE)</span> and They Will Happily Bank You Huge Profits</li>
                     <li> <span class="w600">reloaded with 400+ Done For You Themes</span> P for Local and Online Niches with Ready Content</li>
                     <li> <span class="w600">Customize and Update Themes on Live Website</span> Easily without Touching Any Code</li>
                     <li><span class="w600">Analytics & Remarketing Ready Website</span></li>
                     <li><span class="w600">100% Newbie-Friendly, No Tech Skills Needed, No Monthly Fees Ever.</span></li>
                     </ul>
                     </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt40 mt-md60">
               <!-- <div class="f-24 f-md-34 w700 lh140 text-center black-clr animate-charcter">
                  <img src=" ./assets/images/hurry.webp " alt="Hurry " class="mr10 hurry-img">
                  <span class="red-clr ">Hurry!</span> Limited Time Only. No Monthly Fees
                  </div>
                  
                  <div class="f-20 f-md-24 w400 lh140 text-center mt20 black-clr ">
                  Use Coupon Code <span class="red-clr w600 ">“web10”</span> for an Additional <span class="red-clr w600 ">10% Discount</span>
                  </div> -->
               <div class="f-22 f-md-28 w700 lh140 text-center black-clr">
               (Free Commercial License + Low 1-Time Price For Launch Period Only)
               </div>
               <div class="row">
               <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center ">
               <a href="#buynow " class="cta-link-btn ">Get Instant Access To WEBPULL</a>
               </div>
               </div>
               <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30 ">
               <img src="assets/images/compaitable-with.webp " class="img-fluid d-block mx-xs-center md-img-right ">
               <div class="d-md-block d-none px-md30 "><img src="assets/images/v-line.webp " class="img-fluid "></div>
               <img src="assets/images/days-gurantee.webp " class="img-fluid d-block mx-xs-auto mt15 mt-md0 ">
               </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Header Section End -->
      <!-- Header Second Section -->
      <div class="second-header-section">
      <div class="container ">
      <div class="row">
      <div class="col-12 text-center ">
      <div class="f-28 f-md-38 w700 lh140 black-clr text-center">
      Create Stunning Website or Ecom Store For You or <br class="d-none d-md-block"> Your Clients in <span class="orange-clr">Just 3 Simple Steps</span>
      </div>
      </div>
      </div>
      <div class="row mt50 mt-md65">
      <div class="col-12 col-md-4">
      <div class="step-shape">
      <div class="step-border">
      <div class="step-shape1 f-md-32 f-24 w600 white-clr lh140 text-center">
      STEP 1
      </div>
      <div class="mt20 f-md-30 f-24 w700 lh140 text-center text-uppercase">
      Choose
      </div>
      <div class="step-image mt20 mt-md30">
      <img src="assets/images/step1.webp " class="img-fluid d-block mx-auto ">
      </div>
      <div class="f-18 f-md-20 w400 black-clr lh140 mt20 text-center">
      Select the Niche-Specific Theme from the Proven Converting 400+ Stunning Done-For-You Business Themes
      </div>
      </div>
      </div>
      </div>
      <div class="col-12 col-md-4 mt50 mt-md0">
      <div class="step-shape">
      <div class="step-border">
      <div class="step-shape1 f-md-32 f-24 w600 white-clr lh140 text-center">
      STEP 2
      </div>
      <div class="mt20 f-md-30 f-24 w700 lh140 text-center text-uppercase">
      Customize
      </div>
      <div class="step-image mt20 mt-md30">
      <img src="assets/images/step2.webp " class="img-fluid d-block mx-auto ">
      </div>
      <div class="f-18 f-md-20 w400 black-clr lh140 mt20 text-center ">
      Upload Your Logo, Choose from 1000's of Possible Design Combinations & Customize it as Per Your Need in Just a Few Seconds.
      </div>
      </div>
      </div>
      </div>
      <div class="col-12 col-md-4 mt50 mt-md0 relative">
      <div class="step-shape">
      <div class="step-border">
      <div class="step-shape1 f-md-32 f-24 w600 white-clr lh140 text-center">
      STEP 3
      </div>
      <div class="mt20 f-md-30 f-24 w700 lh140 text-center text-uppercase">
      Publish & Profit
      </div>
      <div class="step-image mt20 mt-md30">
      <img src="assets/images/step3.webp " class="img-fluid d-block mx-auto ">
      </div>
      <div class="f-18 f-md-20 w400 black-clr lh140 mt20 text-center ">
      Now, Just Publish Your Website with a Click to Use it for Your Business or Sell Website Development Services in 100+ Niches to Earn Huge Profits.
      </div>
      </div>
      </div>
      </div>
      <div class="col-12 mt20 mt-md70">
      <div class="f-24 f-md-38 w600 text-center black-clr lh140 ">
      And That’s It!
      </div>
      <div class="f-20 f-md-24 w500 text-center black-clr lh140 mt5">
      It is That Easy! And you will be able to make huge profits from your own offers or by <br class="d-none d-md-block"> helping clients get in front of their target market in no time!
      </div>
      </div>
      </div>
      <!-- <div class="row mt-md100 mt20">
         <div class="col-12 f-28 f-md-45 w700 text-center black-clr lh140">
             WEBPULL Is Built for Everyone Across All Levels
         </div>
         </div> -->
      <div class="row mt-md40 mt0">
      <div class="col-md-4 mt-md0 mt20">
      <img src="./assets/images/icon1.webp" alt="" class="img-fluid d-block mx-auto">
      <div class="f-20 w700 black-clr text-center mt20">
      No Prior Experience Needed
      </div>
      </div>
      <div class="col-md-4 mt-md0 mt20">
      <img src="./assets/images/icon2.webp" alt="" class="img-fluid d-block mx-auto">
      <div class="f-20 w700 black-clr text-center mt20">
      Simple But Powerful Builder - <br class="d-none d-md-block"> No Coding At All
      </div>
      </div>
      <div class="col-md-4 mt-md0 mt20">
      <img src="./assets/images/icon3.webp" alt="" class="img-fluid d-block mx-auto">
      <div class="f-20 w700 black-clr text-center mt20">
      Newbie-Friendly with No <br class="d-none d-md-block"> Learning Curve
      </div>
      </div>
      </div>
      </div>
      </div>
      <!--2. Second Section End --->
      <!-- Premium Websites Section -->
      <div class="premium-sec">
      <div class="container">
      <div class="row">
      <div class="col-12 ">
      <div class="f-28 f-md-45 w700 lh140 black-clr text-center">
      See How Much We Have Made by Selling Premium Websites Built with WEBPULL
      </div>
      </div>
      <div class="col-12 mt20 mt-md40">
      <img src="assets/images/engage-convert.webp" alt=" Premium Website" class="img-fluid d-block mx-auto">
      </div>
      </div>
      </div>
      </div>
      <!-- Premium Websites Section End -->
      <!-- Testimonial Section Start -->
      <div class="testimonial-section2 ">
      <div class="container ">
      <div class="row ">
      <div class="col-12 f-28 f-md-45 w700 lh140 black-clr text-center ">Even Our Beta Users Are Super Excited With The Results They Get With WEBPULL
      </div>
      </div>
      <div class="row mt30 mt-md70 align-items-center">
      <div class="col-md-6 p-md0">
      <img src="./assets/images/testi-img4.webp" alt="" class="img-fluid d-block mx-auto">
      <img src="./assets/images/testi-img5.webp" alt="" class="img-fluid d-block mx-auto">
      <img src="./assets/images/testi-img6.webp" alt="" class="img-fluid d-block mx-auto">
      </div>
      <div class="col-md-6 p-md0">
      <img src="./assets/images/testi-img7.webp" alt="" class="img-fluid d-block mx-auto">
      <img src="./assets/images/testi-img8.webp" alt="" class="img-fluid d-block mx-auto">
      </div>
      </div>
      </div>
      </div>
      <!-- Testimonial Section End -->
      <!-- Over Section Start -->
      <div class="over-section">
      <div class="container">
      <div class="row">
      <div class="col-12">
      <img src="./assets/images/over-img.webp" alt="" class="img-fluid d-block mx-auto">
      <img src="./assets/images/siteefy.webp" alt="" class="img-fluid d-block mx-auto mt-md50 mt20">
      </div>
      </div>
      </div>
      </div>
      <!-- Over Section End -->
      <!-- Research Section Start -->
      <div class="research-section">
      <div class="container">
      <div class="row align-items-center">
      <div class="col-12">
      <div class="f-20 f-md-22 w400 lh140 black-clr text-center">
      As, in this never-ending pandemic era, every business needs to switch to digital to survive, and having a<br class="d-none d-md-block"> website for any business is not an option anymore…
      </div>
      <div class="f-28 f-md-45 w700 orange-clr1  text-center my10">
      It’s a Compulsion Today!
      </div>
      <div class="f-20 f-md-22 w400 lh140 black-clr text-center">
      Because no business owner would like to lose those visitors & big sales from all the digital sources.
      </div>
      </div>
      </div>
      <div class="row mt20 mt-md0 align-items-center">
      <div class="col-12 col-md-6 f-24 f-md-34 w600 lh140 black-clr order-md-2">
      As When You Have an SEO & Mobile Optimized Website Then You
      Seamlessly Get Customers from All 360-Degree Directions.
      </div>
      <div class="col-12 col-md-6 order-md-1 mt20 mt-md50">
      <img src="assets/images/f-image.webp " class="img-fluid d-block mx-autos ms-md-auto" />
      </div>
      </div>
      <div class="row mt20 mt-md0 align-items-center">
      <div class="col-12 col-md-5 f-24 f-md-34 w600 lh140 black-clr">
      <span class="d-flex justify-content-md-end">And You Drive Non-Stop Traffic, Leads
      & Profits 24 by 7 - 100% Hands-Free</span>
      </div>
      <div class="col-12 col-md-7 mt20 mt-md50">
      <img src="assets/images/h-image.webp " class="img-fluid d-block mx-autos" />
      </div>
      </div>
      <div class="row mt20 mt-md50">                
      <div class="col-12 f-20 f-md-24 w500 lh140 black-clr text-center"><span class="w700">So, when it comes to doing a Business, Websites are the Present and The Future! </span> <br class="d-none d-md-block">In Easy Words, Every Business Need a Website Today!
      </div>
      <div class="col-12 mt-md50 mt20 f-md-28 f-22 w600 blue-clr lh140 text-center">
      And that opens a door to
      </div>
      <div class="col-12 mt-md30 mt20 f-md-50 f-28 w700 white-clr lh140 text-center">
      <div class="over-shape">
      BIG $284 Billion Opportunity.
      </div>
      </div>
      </div>
      </div>
      </div>
      <!-- Research Section End -->
      <!-----this open new----->
      <div class="this-open-section">
      <div class="container">
      <div class="row">
      <div class="col-12 f-md-34 f-24 w700 lh140 black-clr text-center">
      Look At Some Real-Life Examples of How Much An Average Freelancer is Anywhere From $500-$2000 For Building Client’s Websites On Platforms Like Fiverr, Odesk, Freelancers, etc. 
      </div>
      </div>
      <div class="row mt-md70 mt20 align-items-center">
      <div class="col-md-5">
      <img src="./assets/images/boomer-smith.webp" alt="" class="img-fluid d-block mx-auto">
      </div>
      <div class="col-md-7 f-md-20 f-18 w400 mt-md0 mt20">
      <span class="w600">Boomer Smith</span> has made <span class="w600">$20,400 on Fiverr in just 4
      months</span> of his start in Dec’21 & still counting. After quitting his Tele calling Job which he never liked, he started searching for any Freelancing opportunity on the internet where he encountered Fiverr and found 2 Big words
      Wed Design, and WordPress.
      <br><br> Now, he is charging $1,200 per project and has completed almost 17 projects in just 4 months which made him $20,400 in income.
      </div>
      </div>
      <div class="row mt-md70 mt20 align-items-center">
      <div class="col-md-5 order-md-2">
      <img src="./assets/images/brain.webp" alt="" class="img-fluid d-block mx-auto">
      </div>
      <div class="col-md-7 f-md-20 f-18 w400 mt-md0 mt20 order-md-1">
      Meet <span class="w600">Brain C.</span> ‘Rising Talent’ (official tag given by Upwork) earned almost
      <span class="w600">$17,495 in just 47 Days on Upwork.</span>
      <br><br> His journey started on Upwork with Graphic Designing in 2019. But as he came to know about many newbies earning huge just by servicing WordPress Website development, he gave it a shot and started earning more than his graphic
      design service.
      <br><br> He started his Website Development Services for the Lawyers & charges them <span class="w600">$3,499 per website on Upwork, which Lawyers are happy to pay.</span>
      </div>
      </div>
      <div class="row mt-md70 mt20 align-items-center">
      <div class="col-md-6">
      <img src="./assets/images/bright.webp" alt="" class="img-fluid d-block mx-auto">
      </div>
      <div class="col-md-6 f-md-20 f-18 w400 mt-md0 mt20">
      <span class="w600">Bright Dock</span> has made almost <span class="w600">$183k (till yet) on
      Freelancer</span> and has completed Over 180 successfully completed Freelancer projects in under 2 years.
      <br><br> He started his WordPress Designing Freelancing journey 2 years ago on Freelancer and now has a freelancing team of 40 digital designers to work for him, so he just focuses on growth. He is charging <span class="w600">$100 per Hr. and people are ready to pay for his awesome job.</span>
      </div>
      </div>
      </div>
      </div>
      <!-----this open new----->
      <div class="course-section">
      <div class="container">
      <div class="row">
      <div class="col-12 f-20 f-md-28 w400 lh140 text-center">
      <span class="w700 f-24 f-md-36"> Of Course, You Can! </span><br>  See How Newbies Made Thousands of Dollars from Website Services  
      </div>
      <div class="col-12 col-md-12 mx-auto mt20 mt-md50">
      <div class="row">
      <div class="col-12 col-md-6 mt-md0 p-md-0">
      <img src="assets/images/proofimg1.webp" class="img-fluid d-block mx-auto">
      </div>
      <div class="col-12 col-md-6 mt20 mt-md0  p-md-0">
      <img src="assets/images/proofimg2.webp" class="img-fluid d-block mx-auto">
      </div>
      <div class="col-12 col-md-6 p-md-0">
      <img src="assets/images/proofimg3.webp" class="img-fluid d-block mx-auto">
      </div>
      <div class="col-12 col-md-6 p-md-0">
      <img src="assets/images/proofimg4.webp" class="img-fluid d-block mx-auto">
      </div>
      </div>
      </div>
      <div class="col-12 f-20 f-md-28 w400 lh140 text-center mt20 mt-md30">
      So, It is the perfect time to get in and profit from this exponential growth! 
      </div>
      </div>
      <div class="row mt30 mt-md70">
      <div class="col-12 text-center">
      <div class="presenting-shape f-md-45 f-28 w700 text-center white-clr lh140 text-uppercase">  Impressive, right?</div>
      </div>
      </div>
      <div class="row mt20 mt-md30">
      <div class="col-12 f-20 f-md-24 lh140 w500 text-center black-clr lh150">
      It can be stated that… <br>
      <span class="mt10 d-block">
      Website Creation Service is the Biggest Income Opportunity Right Now! 
      </span>
      </div>
      <div class="col-12  f-24 f-md-36 lh140 w600 text-center black-clr lh140 mt20 mt-md30">
      So, How would you like to Get Your Share? 
      </div>
      </div>
      </div>
      </div>
      <!-- Amit Pareek Section End -->
      <div class="hithere-section ">
      <div class="container ">
      <div class="row align-items-center">
      <div class="col-12 mb20">
      <div class="f-22 f-md-28 w600 lh140 black-clr">
      Dear Struggling Marketer,
      </div>
      <div class="f-24 f-md-36 w700 lh140 blue-clr">
      From the desk of Ayush Jain and Pranshu Gupta 
      </div>
      </div>
      <div class="col-md-6">
      <div class="f-md-18 f-18 w400 lh140 black-clr">
      I’m Ayush Jain along with my friend Pranshu Gupta. 
      <br><br>
      We are happily living the internet lifestyle for the last 10 years with full flexibility of working from anywhere at any time. <br><br>
      And while we don’t say this to brag, 
      we do want to make one thing clear: <br><br>
      <span class="w600">Whatever business you do online, you need a website! </span><br><br>
      And if you browse the internet, you will find a website for everything be it for a <br><br>
      <div class="f-18 lh140 w400 mb-md30">
      <ul class="revo-list1 pl0">
      <li>Local business, </li>
      <li>Online business selling services, </li>
      <li>E-com stores that sell physical products, </li>
      <li>Software products, </li>
      <li>Info-products, </li>
      <li>Affiliate products,  </li>
      <li>Or even if it is a list-building business.  </li>
      </ul>
      </div>
      <span class="w600">And when you tap into its immense power and use it for your business, the results can be true freedom and explosive income all at the same time.</span>
      </div>
      </div>
      <div class="col-md-6 mt20 mt-md0">
      <img src="./assets/images/ayush-pranshu.webp" alt="" class="img-fluid d-block mx-auto">
      </div>
      </div>
      </div>
      </div>
      <div class="earned-money-section">
      <div class="container">
      <div class="row">
      <div class="col-12 f-28 f-md-45 w600 lh140 text-center">
      Wala! We Made $3,660 In Sales in Last 15 Days… Following This Proven System
      </div>
      <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
      <img src="assets/images/proof6.webp" class="img-fluid d-block mx-auto">
      <img src="assets/images/proof5.webp" class="img-fluid d-block mx-auto mt20">
      </div>
      </div>
      </div>
      </div>
      <div class="eleraning-section">
      <div class="container">
      <div class="row">
      <div class="col-12 f-md-32 f-24 w500 lh140 text-center black-clr">
      <span class="w700">Every Random Joe & Jane Can Get Results Like These </span><br class="d-none d-md-block"> WITHOUT Quitting Their Current Job, WITHOUT Any Prior Skills, Or WITHOUT Risking Their Hard Earned Money. 
      </div>
      <div class="col-12 col-md-12 mx-auto mt20 mt-md30">
      <img src="assets/images/earned-money.webp" class="img-fluid d-block mx-auto">
      </div>
      <div class="col-12 f-md-22 f-18 w600 lh150 text-center black-clr mt20 mt-md30">
      Yep, you read that right.<br><br>
      Now, even you can start getting REAL profits like this easily IF YOU DO IT RIGHT! It’s a REAL <br class="d-none d-md-block"> DEAL but if you go in the wrong direction, it can waste your months of time & 100s of dollars. 
      </div>
      </div>
      </div>
      </div>
      <!-- <div class="big-question-bg">
         <div class="container">
             <div class="row align-items-center">
                 <div class="col-md-7 order-md-2">
                     <div class="f-28 f-md-50 w700 black-clr">
                         Here’s A <span class="orange-clr1">Big Question…</span>
         
                     </div>
                     <div class="f-20 w400 lh140 black-clr mt20">
                         Even after the fact that websites are the need of the hour and the current Covid-19 pandemic has forced businesses to move online, which has created a huge demand in the market…<br><br> It is surprising to see that not many people
                         are jumping in, to <span class="w600">Tap the $284 Billion Industry and Bridge this Huge Demand
                             and Supply Gap.</span>
                     </div>
                 </div>
                 <div class="col-md-5 order-md-1 mt-md0 mt20 ">
                     <img src="./assets/images/bigquestion-shape.webp" alt="" class="img-fluid d-block mx-auto">
                 </div>
             </div>
         </div>
         </div> -->
      <div class="problem-bg-section">
      <div class="container">
      <div class="row">
      <div class="col-12 px40 text-center">
      <div class="f-24 f-md-36 w600 lh140 text-center black-clr">
      That’s A REAL Deal but 
      </div>
      <div class="f-35 f-md-65 w700 lh140 text-center red-clr">
      But Here’s the Problem… 
      </div>
      <div class="f-20 f-md-24 w500 lh140 text-center black-clr">
      Website development can be a serious pain, it requires time, money, and loads of hard work. So, if you want to create a website for your own business or looking forward to starting a website building agency, you have the following options… 
      </div>
      </div>
      </div>
      <div class="row mt20 mt-md30">
      <div class="col-12">
      <img src="assets/images/option1.webp" class="img-fluid d-block mx-auto">
      </div>
      </div>
      <div class="row g-0 d-flex align-items-center flex-wrap mt20 mt-md30">
      <div class="col-12 col-md-4 z-index9">
      <img src="assets/images/problem-1.webp" class="img-fluid d-block mx-auto">
      </div>
      <div class="col-12 col-md-8 mt-md0 mt20">
      <div class="problme-area-shape">
      <div class="f-20 f-md-24 w600 lh140 black-clr">
      Problem 1
      </div>
      <div class="f-24 f-md-38 w700 lh140 red-clr">
      It is Time-Consuming and Frustrating
      </div>
      <div class="f-18 w400 lh140 black-clr">
      You should know Designing, HTML, JAVA Script, CSS Coding, and testing to build a robust website. And learning all these skills from scratch can take months and years
      </div>
      </div>
      </div>
      </div>
      <div class="row g-0 d-flex align-items-center flex-wrap mt20 mt-md60">
      <div class="col-12 col-md-4 z-index9">
      <img src="assets/images/problem-2.webp" class="img-fluid d-block mx-auto">
      </div>
      <div class="col-12 col-md-8 mt-md0 mt20">
      <div class="problme-area-shape">
      <div class="f-20 f-md-24 w600 lh140 black-clr">
      Problem 2
      </div>
      <div class="f-24 f-md-38 w700 lh140 red-clr">
      Still A Big Learning Curve
      </div>
      <div class="f-18 w400 lh140 black-clr">
      You Need Marketing Expertise to Build an Engaging and SEO Optimised design and content for your website.
      </div>
      </div>
      </div>
      </div>
      <div class="row g-0 d-flex align-items-center flex-wrap mt20 mt-md60">
      <div class="col-12 col-md-4 z-index9">
      <img src="assets/images/problem-3.webp" class="img-fluid d-block mx-auto">
      </div>
      <div class="col-12 col-md-8 mt-md0 mt20">
      <div class="problme-area-shape">
      <div class="f-20 f-md-24 w600 lh140 black-clr">
      Problem 3
      </div>
      <div class="f-24 f-md-38 w700 lh140 red-clr">
      Need Additional Features? Buy Multiple Theme and Plugins.
      </div>
      <div class="f-18 w400 lh140 black-clr">
      There are a bunch of extra things which you or your clients may need on the website, but these are not generally available, you need to buy a lot of plugins and themes for these additional features and it is going to cost you heavily.
      </div>
      </div>
      </div>
      </div>
      </div>
      </div>
      <div class="problem-bg-section1">
      <div class="container">
      <div class="row">
      <div class="col-12">
      <img src="assets/images/option2.webp" class="img-fluid d-block mx-auto">
      </div>
      </div>
      <div class="row g-0 d-flex align-items-center flex-wrap mt20 mt-md50">
      <div class="col-12 col-md-4 z-index9 order-md-2">
      <img src="assets/images/cost-you-thousands.webp" class="img-fluid d-block mx-auto">
      </div>
      <div class="col-12 col-md-8 order-md-1 mt-md0 mt20">
      <div class="problme-area-shape1">
      <div class="f-24 f-md-38 w700 lh140 red-clr">
      That’s An Easy Way but Cost You Thousands
      </div>
      <div class="f-18 w400 lh140 black-clr">
      You can hire professionals to help you start this highly lucrative business. But be prepared to spend a Huge Amount of Money even before you earn anything. Website designers and developers either charge on an hourly basis or a flat fee per website project.
      </div>
      </div>
      </div>
      </div>
      <div class="row mx15 mt40 mt-md70">
      <div class="col-12 problme-area-shape2">
      <div class="row align-items-center">
      <div class="col-12 col-md-6">
      <div class="f-24 f-md-38 w700 lh140 white-clr text-uppercase text-center">
      HOURLY RATE
      </div>
      <div class="f-18 w400 lh140 white-clr text-center mt10">
      Hiring someone on an hourly basis from platforms like Upwork, Freelances, etc will cost you somewhere between $50 to $200 per hour.
      </div>
      <img src="assets/images/hourly-rate.webp" class="img-fluid d-block mx-auto mt20">
      </div>
      <div class="col-12 col-md-6 sep-line mt20 mt-md0">
      <div class="f-24 f-md-38 w700 lh140 white-clr text-uppercase text-center">
      FLAT RATE
      </div>
      <div class="f-18 w400 lh140 white-clr text-center mt10">
      Hiring someone on a flat rate per project will cost you somewhere in between $2000 to $6000.
      </div>
      <img src="assets/images/flat-rate.webp" class="img-fluid d-block mx-auto mt20 mt-md45">
      </div>
      </div>
      </div>
      </div>
      <div class="row mt50 mt-md60">
      <div class="col-12">
      <div class="f-18 w400 lh140 black-clr text-center">
      OR you can even hire a full-time employee, but the medium annual salary for a website developer is up to $150,000
      </div>
      <div class="f-24 f-md-28 w600 lh140 black-clr text-center mt10">
      Are you Ready to Pay this…?
      </div>
      <div class="mt20">
      <img src="assets/images/ready-to-pay.webp" class="img-fluid d-block mx-auto">
      </div>
      </div>
      </div>
      </div>
      </div>
      <div class="problem-bg-section2">
      <div class="container ">
      <div class="row">
      <div class="col-12 text-center">
      <img src="./assets/images/option-3.webp" alt="" class="d-block mx-auto img-fluid">
      </div>
      <div class="col-12 f-md-34 f-24 w600 lh140 black-clr text-center mt-md40 mt20">
      Getting Costly Web builder Apps or taking service from Website building companies that charge you $100 to $1000 per month or more without developer’s rights.
      </div>
      <div class="col-12 f-18 w400 lh140 black-clr text-center mt20">
      So, if you have multiple clients for website development you must arrange multiple accounts, that is a big investment upfront. And still, you may not be satisfied with the limited features and no flexibility in using.
      </div>
      <div class="col-12 mx-auto mt-md50 mt20">
      <img src="./assets/images/acquiring.webp" alt="" class="img-fluid d-block mx-auto">
      </div>
      <div class="col-12 f-18 w400 lh140 black-clr text-center mt-md50 mt20">
      And even after spending so much time and money, there is no guarantee that you will get a good result.
      <br><br><span class="w600">Problems like this force 85% of the entrepreneurs to leave their business
      in dreams or within just the first year of starting.</span>
      </div>
      </div>
      <div class="row mx0">
      <div class="col-12 mt80 mt-md100 ">
      <div class="problme-shape1">
      <div class="text-center mtm50">
      <div class="shape-testi6">
      <div class="f-28 f-md-45 w700 lh140 text-center white-clr skew-normal">
      Isn’t It Scary…?
      </div>
      </div>
      </div>
      <div class="row d-flex align-items-center flex-wrap">
      <div class="col-12 mx-auto">
      <div class="f-18 w400 lh140 black-clr">
      <span class="w700 f-20 f-md-24">YES IT IS. But Not Anymore!</span><br><br> I and my partners started working to find the solution and after years of learning, planning, designing, coding, debugging & real-user-testing…<br><br>
      <span class="w600">We’ve got it down to a science where we can have a site ready for
      a client in less than 7 minutes.</span><br><br> We are excited to release our solution that will make creating and selling websites easier and faster than ever…
      </div>
      </div>
      </div>
      </div>
      </div>
      </div>
      </div>
      </div>
      <!--Proudly Introducing Start -->
      <div class="next-gen-sec" id="product">
      <div class="container">
      <div class="row">
      <div class="col-12 text-center">
      <div class="presenting-shape f-md-38 f-24 w600 text-center white-clr lh140"> 
      Proudly Presenting... </div>
      </div>
      <div class="col-12 mt-md20 mt20 text-center">
      <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 394.2 100" style="enable-background:new 0 0 394.2 100; max-height:111px" xml:space="preserve">
      <style type="text/css">
      .st00{fill:#FFFFFF;}
      .st11{fill:url(#SVGID_1_);}
      .st22{fill:url(#SVGID_00000021093791325859232720000015953972570224053676_);}
      .st33{fill:#FFC802;}
      .st44{fill:url(#SVGID_00000003068462962921929030000010945183465240898967_);}
      </style>
      <path class="st00" d="M117.7,37.9l-5,12.4H92.9v12.4h18.6l-5,12.4H92.9v12.4h24.8l-5,12.5H92.9H80.5V75.1V62.7V37.9H117.7z"></path>
      <path class="st00" d="M323.2,37.9v49.7H348V100h-37.3V37.9H323.2z"></path>
      <path class="st00" d="M369.4,37.9v49.7h24.8V100h-37.3V37.9H369.4z"></path>
      <g>
      <g>
      <polygon class="st00" points="59.1,71.4 52.4,54.4 59.1,37.9 72.6,37.9   "></polygon>
      <polygon class="st00" points="29.4,54.7 36.3,37.9 54.5,82.9 47.7,100   "></polygon>
      <polygon class="st00" points="24.8,100 31.6,82.8 13.4,37.9 0,37.9   "></polygon>
      </g>
      </g>
      <path class="st00" d="M222.8,44.3c-4.3-4.3-9.4-6.4-15.4-6.4h-9.3h-12.5v21.7v21.7V100h12.5V81.3h0V68.9h0v-9.3v-9.3h9.3  c2.6,0,4.8,0.9,6.6,2.8c1.8,1.8,2.7,4,2.7,6.6c0,2.6-0.9,4.8-2.7,6.6c-1.8,1.8-4,2.7-6.6,2.7h-0.7v12.4h0.7c6,0,11.1-2.1,15.4-6.4  c4.3-4.3,6.4-9.4,6.4-15.4C229.2,53.6,227.1,48.5,222.8,44.3z"></path>
      <rect x="252.1" y="31.8" transform="matrix(0.4226 -0.9063 0.9063 0.4226 114.0209 255.0236)" class="st00" width="10.2" height="12.5"></rect>
      <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="249.3741" y1="97.3262" x2="270.7371" y2="53.5256">
      <stop offset="0" style="stop-color:#FA8460"></stop>
      <stop offset="1" style="stop-color:#FCB121"></stop>
      </linearGradient>
      <path class="st11" d="M271.7,80.3c-1.4,3.1-3.8,5.2-7,6.4c-3.2,1.2-6.4,1-9.5-0.4c-3.1-1.4-5.2-3.8-6.4-7c-1.2-3.2-1-6.4,0.4-9.5  l9.9-21.2l-11.3-5.3l-9.9,21.2c-2.9,6.2-3.2,12.6-0.8,19c2.3,6.5,6.6,11.1,12.9,14c6.2,2.9,12.5,3.2,19,0.8  c6.5-2.3,11.2-6.6,14.1-12.8l9.9-21.2l-11.3-5.3L271.7,80.3z"></path>
      <rect x="285.9" y="47.6" transform="matrix(0.4226 -0.9063 0.9063 0.4226 119.2494 294.738)" class="st00" width="10.2" height="12.5"></rect>
      <path class="st00" d="M173.3,76.3c-0.8-1.7-1.7-3.2-2.9-4.5c-1.2-1.3-2.5-2.3-4-3.1c-1.5-0.8-3-1.2-4.5-1.3c1.5-0.1,2.8-0.5,4.1-1.3  c1.2-0.8,2.3-1.8,3.2-3c0.9-1.2,1.6-2.5,2.1-3.9c0.5-1.4,0.8-2.8,0.8-4c0-3.5-0.5-6.4-1.4-8.6c-1-2.2-2.3-4-3.9-5.3  c-1.6-1.3-3.6-2.2-5.8-2.6c-2.2-0.5-4.6-0.7-7-0.7h-27.3v10.5h11.9v0h15.4c0.8,0,1.7,0.2,2.4,0.6c0.8,0.4,1.4,0.9,2,1.5  c0.6,0.6,1,1.4,1.4,2.2c0.3,0.9,0.5,1.7,0.5,2.6c0,0.9-0.2,1.8-0.5,2.8c-0.3,0.9-0.8,1.8-1.4,2.5c-0.6,0.7-1.3,1.3-2.2,1.8  c-0.9,0.5-1.9,0.7-3.1,0.7h-9.1v10.5h10c2.7,0,4.9,0.7,6.4,2.1c1.6,1.4,2.3,3.2,2.3,5.3c0,1-0.2,2-0.6,3c-0.4,1-0.9,1.9-1.6,2.7  c-0.7,0.8-1.5,1.5-2.4,2c-0.9,0.5-1.9,0.7-2.9,0.7h-16.8h0V73.7h0V63.2h0v-9.3h-11.9V100h9.6h2.3h16.8c2.7,0,5.2-0.3,7.5-0.9  c2.3-0.6,4.4-1.6,6.2-3.1c1.8-1.4,3.1-3.3,4.2-5.7c1-2.3,1.5-5.2,1.5-8.6C174.4,79.8,174,78,173.3,76.3z"></path>
      <g>
      <linearGradient id="SVGID_00000142155450205615942230000010409392281687221429_" gradientUnits="userSpaceOnUse" x1="270.7616" y1="25.6663" x2="296.63" y2="25.6663">
      <stop offset="0" style="stop-color:#FA8460"></stop>
      <stop offset="4.692155e-02" style="stop-color:#FA9150"></stop>
      <stop offset="0.1306" style="stop-color:#FBA43A"></stop>
      <stop offset="0.2295" style="stop-color:#FBB229"></stop>
      <stop offset="0.3519" style="stop-color:#FCBC1E"></stop>
      <stop offset="0.5235" style="stop-color:#FCC117"></stop>
      <stop offset="1" style="stop-color:#FCC315"></stop>
      </linearGradient>
      <path style="fill:url(#SVGID_00000142155450205615942230000010409392281687221429_);" d="M289.6,14.2c-2.1-1.1-4.3-1.5-6.5-1.4   c-0.1,0-0.1,0-0.2,0c-0.1,0-0.2,0-0.2,0c0,0-0.1,0-0.1,0c-0.1,0-0.2,0-0.2,0c-0.1,0-0.2,0-0.3,0c-0.1,0-0.2,0-0.2,0   c-0.3,0-0.5,0.1-0.8,0.1c-0.1,0-0.2,0-0.3,0.1c-0.1,0-0.2,0-0.3,0.1c-0.1,0-0.3,0.1-0.4,0.1c-0.1,0-0.2,0-0.2,0.1   c-0.1,0-0.2,0.1-0.3,0.1c-0.2,0.1-0.5,0.2-0.7,0.3c0,0,0,0,0,0c-0.3,0.1-0.6,0.3-0.9,0.4c-0.3,0.1-0.6,0.3-0.9,0.5   c-0.1,0.1-0.3,0.2-0.4,0.2c-0.1,0.1-0.2,0.2-0.4,0.2c-0.1,0.1-0.2,0.1-0.2,0.2c0,0-0.1,0-0.1,0.1c-0.1,0.1-0.2,0.1-0.2,0.2   c-0.1,0-0.1,0.1-0.2,0.2c-0.1,0.1-0.2,0.2-0.3,0.3c-0.1,0-0.1,0.1-0.2,0.2c-0.5,0.4-0.9,0.9-1.3,1.4c-0.1,0.1-0.1,0.1-0.2,0.2   c0,0-0.1,0.1-0.1,0.1c-0.1,0.1-0.1,0.2-0.2,0.3c0,0-0.1,0.1-0.1,0.1c-0.1,0.1-0.1,0.2-0.2,0.3c0,0,0,0.1-0.1,0.1   c-0.1,0.1-0.1,0.2-0.2,0.3c0,0,0,0.1-0.1,0.1c-0.1,0.1-0.1,0.2-0.2,0.3c-0.1,0.1-0.2,0.3-0.2,0.4c-3.3,6.3-0.8,14.1,5.6,17.4   c1.9,1,3.9,1.4,5.9,1.4c0.1,0,0.3,0,0.4,0c0.1,0,0.3,0,0.4,0c0.1,0,0.1,0,0.2,0c0.1,0,0.2,0,0.3,0c0,0,0,0,0.1,0c0.1,0,0.2,0,0.3,0   c0.1,0,0.2,0,0.2,0c0.1,0,0.2,0,0.3-0.1c0.1,0,0.3,0,0.4-0.1c0.1,0,0.3-0.1,0.4-0.1c0.1,0,0.2-0.1,0.3-0.1c0.2,0,0.4-0.1,0.5-0.2   c0.1,0,0.3-0.1,0.4-0.1c0.1,0,0.3-0.1,0.4-0.1c0,0,0,0,0,0c0.1,0,0.2-0.1,0.4-0.1c0,0,0,0,0,0c0,0,0.1,0,0.1,0c0,0,0.1,0,0.1-0.1   c0,0,0,0,0.1,0c0,0,0.1,0,0.1,0c0,0,0,0,0.1,0c0.1,0,0.2-0.1,0.3-0.2c0,0,0,0,0.1,0c0.1-0.1,0.2-0.1,0.3-0.2   c0.1-0.1,0.3-0.1,0.4-0.2c0.4-0.2,0.7-0.5,1.1-0.7c0.2-0.2,0.5-0.4,0.7-0.6c0.2-0.2,0.4-0.4,0.7-0.6c0.2-0.2,0.4-0.4,0.6-0.6   c0.2-0.2,0.4-0.5,0.6-0.7c0.1-0.1,0.2-0.2,0.3-0.4c0,0,0,0,0,0c0.1-0.1,0.2-0.2,0.3-0.4c0.1-0.1,0.2-0.3,0.2-0.4   c0.1-0.1,0.1-0.2,0.2-0.3c0-0.1,0.1-0.1,0.1-0.2c0.1-0.1,0.1-0.2,0.2-0.3C298.5,25.3,296,17.5,289.6,14.2z M279.6,26.7L279.6,26.7   l0.3,0.6c0.4,0.7,1,1.3,1.8,1.7c0.3,0.2,0.7,0.3,1,0.3c0.3,0,0.5-0.1,0.7-0.2c0.1,0,0.1-0.1,0.2-0.2c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0-0.1c0.2-0.3,0.2-0.7-0.1-1.3c0,0,0,0,0,0   c-0.1-0.2-0.2-0.4-0.4-0.6c-0.1-0.1-0.2-0.2-0.3-0.3c-0.4-0.4-0.7-0.8-0.9-1.2c-0.1-0.1-0.2-0.2-0.2-0.4c0,0,0,0,0-0.1   c0,0,0-0.1-0.1-0.1c0,0,0-0.1-0.1-0.1c-0.1-0.2-0.1-0.3-0.2-0.5c-0.1-0.2-0.1-0.4-0.2-0.6c0,0,0-0.1,0-0.1c0-0.1,0-0.2,0-0.3v-0.1   c0-0.1,0-0.1,0-0.2c0,0,0-0.1,0-0.1c0-0.1,0-0.2,0-0.3c0.1-0.3,0.2-0.6,0.3-0.8c0,0,0,0,0-0.1c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0-0.1c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0l0,0c0.4-0.5,0.9-0.9,1.5-1.1c0.7-0.2,1.4-0.3,2.1-0.1l0.9-1.7l1.9,1l-0.8,1.6c0.7,0.4,1.2,1,1.6,1.6   l0.2,0.4l-1.8,1.6l-0.3-0.5c-0.1-0.1-0.3-0.4-0.5-0.7c-0.1-0.1-0.3-0.3-0.5-0.4c0,0-0.1-0.1-0.1-0.1c-0.1-0.1-0.3-0.2-0.4-0.2   c0,0-0.1,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0   c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0h0c0,0,0,0-0.1,0h0   c0,0,0,0-0.1,0h0c0,0,0,0,0,0h0c0,0,0,0,0,0h0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c-0.2,0.1-0.4,0.3-0.5,0.5c-0.3,0.5-0.2,0.9,1,2.3c0.8,0.9,1.2,1.6,1.4,2.4   c0.2,0.8,0.1,1.6-0.3,2.5c-0.8,1.5-2.4,2.2-4.2,1.8l-0.8,1.6l-0.1,0.2l-2-1l0.9-1.7c-0.1-0.1-0.2-0.1-0.3-0.2   c-0.3-0.2-0.5-0.4-0.8-0.6c0,0-0.1-0.1-0.1-0.1c-0.3-0.3-0.5-0.6-0.7-1l-0.2-0.4L279.6,26.7z"></path>
      <path class="st33" d="M278,37c-6.3-3.3-8.8-11.1-5.6-17.4c1.3-2.4,3.2-4.3,5.4-5.5c-2.4,1.2-4.4,3.1-5.7,5.6   c-3.3,6.3-0.8,14.1,5.6,17.4c3.9,2,8.4,1.9,12-0.1C286.2,38.9,281.8,39,278,37z"></path>
      </g>
      <g>
      <linearGradient id="SVGID_00000016062571573767098340000005260099975199807150_" gradientUnits="userSpaceOnUse" x1="79.734" y1="-130.471" x2="91.3973" y2="-130.471" gradientTransform="matrix(0.8624 -0.5063 0.5063 0.8624 260.6894 161.6657)">
      <stop offset="0" style="stop-color:#FA8460"></stop>
      <stop offset="4.692155e-02" style="stop-color:#FA9150"></stop>
      <stop offset="0.1306" style="stop-color:#FBA43A"></stop>
      <stop offset="0.2295" style="stop-color:#FBB229"></stop>
      <stop offset="0.3519" style="stop-color:#FCBC1E"></stop>
      <stop offset="0.5235" style="stop-color:#FCC117"></stop>
      <stop offset="1" style="stop-color:#FCC315"></stop>
      </linearGradient>
      <path style="fill:url(#SVGID_00000016062571573767098340000005260099975199807150_);" d="M268.1,0c-1,0.1-2,0.4-2.8,0.9   c0,0-0.1,0-0.1,0c0,0-0.1,0-0.1,0.1c0,0,0,0,0,0c0,0-0.1,0-0.1,0.1c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0-0.1,0.1   c-0.1,0.1-0.2,0.1-0.3,0.2c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0.1-0.1,0.1   c-0.1,0.1-0.1,0.2-0.2,0.3c0,0,0,0,0,0c-0.1,0.1-0.2,0.2-0.2,0.4c-0.1,0.1-0.2,0.3-0.2,0.4c0,0.1-0.1,0.1-0.1,0.2   c0,0.1-0.1,0.1-0.1,0.2c0,0,0,0.1-0.1,0.1c0,0,0,0,0,0.1c0,0,0,0.1-0.1,0.1c0,0,0,0.1,0,0.1c0,0.1,0,0.1-0.1,0.2c0,0,0,0.1,0,0.1   c-0.1,0.3-0.1,0.5-0.2,0.8c0,0,0,0.1,0,0.1c0,0,0,0.1,0,0.1c0,0.1,0,0.1,0,0.2c0,0,0,0,0,0.1c0,0.1,0,0.1,0,0.2c0,0,0,0,0,0.1   c0,0.1,0,0.1,0,0.2c0,0,0,0,0,0.1c0,0.1,0,0.1,0,0.2c0,0.1,0,0.1,0,0.2c0.2,3.2,2.9,5.7,6.1,5.5c1-0.1,1.9-0.3,2.6-0.8   c0.1,0,0.1-0.1,0.2-0.1c0.1,0,0.1-0.1,0.1-0.1c0,0,0,0,0.1,0c0,0,0.1,0,0.1-0.1c0,0,0,0,0,0c0,0,0.1-0.1,0.1-0.1c0,0,0.1,0,0.1-0.1   c0,0,0.1-0.1,0.1-0.1c0,0,0.1-0.1,0.1-0.1c0,0,0.1-0.1,0.1-0.1c0,0,0.1-0.1,0.1-0.1c0.1-0.1,0.1-0.1,0.2-0.2c0,0,0.1-0.1,0.1-0.1   c0,0,0.1-0.1,0.1-0.1c0,0,0,0,0,0c0,0,0.1-0.1,0.1-0.1c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0-0.1c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0.1-0.1,0.1-0.1c0,0,0,0,0,0c0,0,0.1-0.1,0.1-0.1c0-0.1,0.1-0.1,0.1-0.2c0.1-0.2,0.2-0.4,0.3-0.5c0.1-0.1,0.1-0.2,0.1-0.4   c0-0.1,0.1-0.3,0.1-0.4c0-0.1,0.1-0.3,0.1-0.4c0-0.1,0-0.3,0.1-0.4c0-0.1,0-0.1,0-0.2c0,0,0,0,0,0c0-0.1,0-0.1,0-0.2   c0-0.1,0-0.1,0-0.2c0-0.1,0-0.1,0-0.2c0,0,0-0.1,0-0.1c0-0.1,0-0.1,0-0.2C274.1,2.3,271.3-0.2,268.1,0z M267.1,7.2L267.1,7.2   l0.3,0.2c0.3,0.2,0.7,0.3,1.1,0.3c0.2,0,0.3-0.1,0.4-0.1c0.1-0.1,0.2-0.1,0.2-0.3c0,0,0-0.1,0-0.1c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0-0.2-0.1-0.3-0.3-0.5c0,0,0,0,0,0   c-0.1,0-0.2-0.1-0.3-0.1c-0.1,0-0.1,0-0.2-0.1c-0.2-0.1-0.4-0.2-0.6-0.2c-0.1,0-0.1-0.1-0.2-0.1c0,0,0,0,0,0c0,0,0,0-0.1,0   c0,0,0,0-0.1,0c-0.1,0-0.1-0.1-0.2-0.1c-0.1-0.1-0.1-0.1-0.2-0.2c0,0,0,0,0,0c0,0-0.1-0.1-0.1-0.1l0,0c0,0,0,0,0-0.1c0,0,0,0,0,0   c0,0,0-0.1-0.1-0.1c0-0.1-0.1-0.3-0.1-0.4c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0l0,0   c0-0.3,0.2-0.5,0.3-0.8c0.2-0.2,0.5-0.4,0.8-0.5l0-0.9l1-0.1l0,0.8c0.4,0,0.7,0.1,1,0.3l0.2,0.1l-0.3,1l-0.3-0.1   c-0.1,0-0.2-0.1-0.4-0.1c-0.1,0-0.2,0-0.3-0.1c0,0-0.1,0-0.1,0c-0.1,0-0.1,0-0.2,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0l0,0c0,0,0,0,0,0l0,0c0,0,0,0,0,0l0,0c0,0,0,0,0,0l0,0c0,0,0,0,0,0l0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c-0.1,0.1-0.1,0.2-0.1,0.3c0,0.2,0.1,0.4,0.9,0.7c0.5,0.2,0.8,0.4,1.1,0.6c0.3,0.3,0.4,0.6,0.4,1c0,0.8-0.4,1.4-1.2,1.7l0,0.8   l0,0.1l-1,0.1l0-0.8c-0.1,0-0.1,0-0.2,0c-0.2,0-0.3,0-0.4-0.1c0,0-0.1,0-0.1,0c-0.2-0.1-0.4-0.1-0.5-0.2l-0.2-0.1L267.1,7.2z"></path>
      <path class="st33" d="M268.8,11.5c-3.2,0.2-6-2.3-6.1-5.5c-0.1-1.2,0.3-2.4,0.9-3.4c-0.7,1-1,2.2-0.9,3.5c0.2,3.2,2.9,5.7,6.1,5.5   c2-0.1,3.7-1.2,4.6-2.8C272.4,10.4,270.7,11.4,268.8,11.5z"></path>
      </g>
      <polygon class="st00" points="297.8,43.3 296.4,42.7 294.6,46.7 295.4,41.2 296.8,41.8 298.7,37.9 "></polygon>
      <polygon class="st00" points="264.2,27.2 262.9,26.4 260.7,30.2 262,24.8 263.3,25.6 265.5,21.8 "></polygon>
      </svg>
      </div>
      <div class="f-md-32 f-22 w600 white-clr lh140 col-12 mt20 text-center">
      Brand New 3-Click Website Builder That Lets You Create and Sell Website with Zero Hassles, Preloaded with 400+ Done For You Themes Ready To Sell.  
      </div> 
      </div>
      <div class="row align-items-end mt20 mt-md50 gx-4">
      <div class="col-12 col-md-6">
      <img src="https://cdn.oppyo.com/launches/webpull/jv/product-box.webp" class="img-fluid d-block mx-auto" alt="Product">
      </div>
      <div class="col-12 col-md-6 mt20 mt-md0">
      <div class="proudly-list-bg">
      <div class="col-12">
      <ul class="proudly-tick pl0 m0 f-18 f-md-20 w400 black-clr lh140">
      <li><span class="w700">Create Any Type Of Site</span>
      <br>
      Create Stunning Local Sites, Marketplace,  
      E-Com Stores, Affiliate Niche Sites or Blogs Easily. 
      </li>
      <li><span class="w700">Super-Customizable & Easy To Use 
      </span><br>
      Preloaded with 400+ Done-For-You  
      Stunning Themes To Create Site In Any Niche </li>
      <li><span class="w700">No Worries of Paying Monthly </span>
      <br>During This Launch Special Deal,  
      Get All Benefits At Limited Low One-Time-Fee.</li>
      <li><span class="w700">No Limits - Use for Yourself Or Clients 
      </span><br>
      Sell Your Own Products, Services or Affiliate  
      Offers or Charge Your Clients for Elegant Websites. 
      </li>    
      <li style="padding-bottom:0px"><span class="w700">50+ More Features</span>
      <br>We’ve Left No Stone Unturned to Give  
      You an Unmatched Experience </li> 
      </ul>
      </div>
      </div>
      </div>   
      </div>
      <div class="row mt20 mt-md50">
      <div class="col-12 f-18 lh140 w400 text-center white-clr">
      It has never been easier to create professional, SEO optimized, and mobile responsive business websites that drive customers 360 degrees but with WEBPULL, it is EASY & FAST. 
      <br><br>
      Replace your old-school website builder with the amazing framework that helps ANYONE create stunning, lightning-fast, and clean websites. 
      </div>
      </div>
      </div>
      </div>
      <!--Proudly Introducing End -->
      <!-- 3 Steps Section Start -->
      <div class="step-section1">
      <div class="container ">
      <div class="row ">
      <div class="col-12 f-28 f-md-50 w600 lh140 black-clr text-center ">
      Create Stunning Websites & Stores For <br class="d-none d-md-block">
      Any Business In <span class="orange-clr">Just 3 Simple Steps… </span>
      </div>
      <div class="col-12 mt30 mt-md50">
      <div class="row align-items-center ">
      <div class="col-12 col-md-6">                           
      <div class="step-shapes f-24 f-md-32 w600">
      Step 1
      </div>
      <div class="f-24 f-md-38 w600 lh140 mt15 mt-md25">
      Choose
      </div>
      <div class="f-18 f-md-20 w400 lh140 mt5 mt-md15">
      Select the Niche-Specific Theme from proven converting  400+ stunning done-for-you Business Themes. 
      </div>
      <img src="assets/images/left-arrow.webp" class="img-fluid d-none d-md-block ms-auto step-arrow1">
      </div>
      <div class="col-12 col-md-6 mt20 mt-md0 ">
      <img src="assets/images/choose.webp " class="img-fluid d-block mx-auto " />
      </div>
      </div>
      </div>
      <div class="col-12 mt30 mt-md50">
      <div class="row align-items-center ">
      <div class="col-12 col-md-6 order-md-2">                           
      <div class="step-shapes f-24 f-md-32 w600">
      Step 2
      </div>
      <div class="f-24 f-md-38 w600 lh140 mt15 mt-md25">
      Customize
      </div>
      <div class="f-18 f-md-20 w400 lh140 mt5 mt-md15">
      Upload the logo, choose from thousands of possible design combinations & customize it as per your business needs.
      </div>
      <img src="assets/images/right-arrow.webp" class="img-fluid d-none d-md-block step-arrow1">
      </div>
      <div class="col-md-6 col-md-pull-6 mt20 mt-md0 order-md-1 ">
      <img src="assets/images/edit.webp " class="img-fluid d-block mx-auto " />
      </div>
      </div>
      </div>
      <div class="col-12 mt30 mt-md50">
      <div class="row align-items-center ">
      <div class="col-12 col-md-6">                           
      <div class="step-shapes f-24 f-md-32 w600">
      Step 3
      </div>
      <div class="f-24 f-md-38 w600 lh140 mt15 mt-md25">
      Publish, Sell, and Profit
      </div>
      <div class="f-18 f-md-20 w400 lh140 mt5 mt-md15">
      Now, just publish your website with a click to use it for your  
      business or sell website development services in 100+ niches. 
      </div>
      </div>
      <div class="col-12 col-md-6 mt20 mt-md0 ">
      <img src="assets/images/publish-report.webp " class="img-fluid d-block mx-auto " />
      </div>
      </div>
      </div>
      </div>
      </div>
      </div>
      <!-- 3 Steps Section End -->
      <!-- Demo Video Section Start -->
      <div class="demo-section-bg " id="demo">
      <div class="container ">
      <div class="row ">
      <div class="col-12 f-28 f-md-45 w700 lh140 white-clr text-capitalize text-center ">Watch WEBPULL In Action:                  
      </div>
      <div class="col-12 mt20 mt-md50">
      <img src="https://cdn.oppyo.com/launches/webpull/jv/product-box.webp" class="img-fluid d-block mx-auto" alt="Product">
      </div>
      <!-- <div class="col-12 col-md-10 mx-auto mt20 mt-md40 ">
         <div class="responsive-video video-shadow ">
             <iframe src="https://webpull.dotcompal.com/video/embed/ydruowx7ha" style="width: 100%; height: 100%; " frameborder="0 " allow="fullscreen " allowfullscreen></iframe>
         </div>
         </div> -->
      <!-- CTA Button -->
      <div class="col-12 mt20 mt-md40">
      <!-- <div class="f-24 f-md-34 w700 lh140 text-center white-clr animate-charcter2">
         <img src=" ./assets/images/hurry.webp " alt="Hurry " class="mr10 hurry-img">
         <span class="red-clr ">Hurry!</span> Limited Time Only. No Monthly Fees
         </div>
         <div class="f-20 f-md-24 w400 lh140 text-center mt20 white-clr ">
         Use Coupon Code <span class="org-clr w600 ">“web10”</span> for an Additional <span class="org-clr w600 ">10% Discount</span>
         </div> -->
      <div class="f-22 f-md-28 w700 lh140 text-center white-clr">
      (Free Commercial License + Low 1-Time Price For Launch Period Only)
      </div>
      <div class="row">
      <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center ">
      <a href="#buynow " class="cta-link-btn ">Get Instant Access To WEBPULL</a>
      </div>
      </div>
      <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30 ">
      <img src="assets/images/compaitable-with1.webp " class="img-fluid d-block mx-xs-center md-img-right ">
      <div class="d-md-block d-none px-md30 "><img src="assets/images/v-line.webp " class="img-fluid "></div>
      <img src="assets/images/days-gurantee1.webp " class="img-fluid d-block mx-xs-auto mt15 mt-md0 ">
      </div>
      </div>
      </div>
      </div>
      </div>
      <!-- Demo Video Section End -->
      <div class="testimonial-section2 ">
      <div class="container ">
      <div class="row ">
      <div class="col-12 f-28 f-md-45 w700 lh140 black-clr text-center ">And Here's What Some More <br class="d-none d-md-block">
      Happy Users Say About WEBPULL 
      </div>
      </div>
      <div class="row mt30 mt-md70 align-items-center mb-md50">
      <div class="col-md-6 p-md0">
      <img src="./assets/images/testi-img4.webp" alt="" class="img-fluid d-block mx-auto">
      <img src="./assets/images/testi-img5.webp" alt="" class="img-fluid d-block mx-auto">
      <img src="./assets/images/testi-img6.webp" alt="" class="img-fluid d-block mx-auto">
      </div>
      <div class="col-md-6 p-md0">
      <img src="./assets/images/testi-img7.webp" alt="" class="img-fluid d-block mx-auto">
      <img src="./assets/images/testi-img8.webp" alt="" class="img-fluid d-block mx-auto">
      </div>
      </div>
      </div>
      </div>
      <!-- Features Part -->
      <!-- Features Section Start -->
      <div class="white-section pt-md0" id="features">
      <div class="container ">
      <div class="row ">
      <div class="col-12 text-center minus60">
      <div class="presenting-shape1 f-md-38 f-24 w600 text-center white-clr lh140"> 
      Checkout The Ground Breaking Features That  
      Make WEBPULL A Cut Above The Rest  </div>
      </div>
      </div>
      <div class="row mt-md90 mt20 align-items-center ">
      <div class="col-12 col-md-6 ">
      <div class="f-24 f-md-32 w600 lh140 black-clr ">
      Highly Converting Designs With All Important Business Sections Included 
      </div>
      <div class="f-18 f-md-20 w400 black-clr lh140 mt20 ">
      ShapeThe Webpull preloaded themes will allow you to create clean sites  
      that look dynamic. You’ll be able to create multiple business layouts  
      with all the custom designs included. </span>
      </div>
      <div class="f-18 f-md-20 mt-md10 mb-md20 mb20 w400 black-clr lh140 ">
      These themes are created having multiple business requirements  
      in mind to create a stunning website for yourself or your clients  
      is already included. 
      <br><br><span class="w600 ">Just choose a theme and create any scenario you want such as: </span>
      </div>
      <div class="col-12 p0 ">
      <div class="row ">
      <div class="col-12 col-md-6 ">
      <div class="f-md-20 f-18 w600 black-clr clear ">
      <ul class="p0 revo-list m0 ">
      <li>Product/Services,</li>
      <li>Home page</li>
      <li>Services</li>
      <li>Portfolio</li>
      </ul>
      </div>
      </div>
      <div class="col-12 col-md-6 pl-md15 mt15 mt-md0 ">
      <div class="f-md-20 f-18 w600 black-clr clear m0 ">
      <ul class="p0 revo-list ">
      <li>Testimonials</li>
      <li>Contact us</li>
      <li>Full width pages</li>
      <li>Other pages…</li>
      </ul>
      </div>
      </div>
      </div>
      </div>
      </div>
      <div class="col-12 col-md-6 mt20 mt-md0 ">
      <img src="assets/images/fnl1.webp " class="img-fluid d-block mx-auto " />
      </div>
      </div>
      </div>
      </div>
      <div class="grey-section">
         <div class="container">
            <div class="row">
               <div class="col-12 ">
                  <div class="f-28 f-md-45 w600 lh140 black-clr text-capitalize text-center">
                     400+ Eye-Catching And Customizable Business Themes with Multiple  
                     Color Templates To Captivate Audience & Maximize Engagement 
                  </div>
                  <div class="f-18 f-md-20 w400 black-clr lh140 mt20 mt-md30 text-center">
                     Capturing your audience’s attention takes a lot of time, expertise,  
                     and effort. We’re giving you mobile optimized and elegant templates  
                     for almost all types of business websites to <span class="w600">get max attention,  
                     max conversions, max sales and more importantly max profits.</span>
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md40">
                  <img src="assets/images/funnel-planner-and-builder.webp" class="img-fluid d-block mx-auto " />
               </div>
               <div class="col-12 mt20 mt-md40 ">
                  <div class="f-18 f-md-20 w400 black-clr lh140 text-center">                      
                     <span class="w600 ">The best part is</span> that you can get up and running quickly  
                     with these ready-to-use templates and start creating  
                     mind-blowing websites in absolutely no time. 
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="theme-section ">
         <div class="container ">
            <div class="row ">
               <div class="col-12 f-28 f-md-45 w600 lh140 white-clr text-capitalize text-center ">
                  Instantly Create Stunning Professional Websites For Any Business Need. 
               </div>
               <div class="col-12 f-18 f-md-20 w400 mt-md20 mt15 text-center white-clr lh140 ">
                  Creating a super engaging website has never been easier. Once you know the direction for your own business or that of your client, simply choose a template and start creating a site in only minutes. 
               </div>
               <div class="col-12 mt-md50 mt20 ">
                  <div class="row ">
                     <div class="col-12 col-md-4 mt20 mt-md0 ">
                        <div class="shp-blc ">
                           <img src="assets/images/icnm7.webp " class="img-fluid d-block mx-auto " />
                        </div>
                        <div class="icn-shape ">
                           <div class="text-center f-22 f-md-26 w600 lh140 ">
                              Local Business Websites
                           </div>
                           <!-- <div class="text-center f-md-20 f-20 mt10 mt-md12 w400 lh140 ">
                              Easily create <span class="w600 ">local business websites</span> for businesses like spas, daycare, restaurants, nursing homes, etc. <span class="w600 ">Appointments
                                  booking sites</span> for dentists, chiropractors, local consultants, etc. Or <span class="w600 ">create portfolio </span>
                              </div> -->
                        </div>
                     </div>
                     <div class="col-12 col-md-4 mt20 mt-md0 ">
                        <div class="shp-blc ">
                           <img src="assets/images/icnm6.webp " class="img-fluid d-block mx-auto " />
                        </div>
                        <div class="icn-shape ">
                           <div class="text-center f-22 f-md-26 w600 lh140 ">
                              Marketplace
                           </div>
                           <!-- <div class="text-center f-md-20 f-20 mt10 mt-md15 w400 lh140 ">
                              Showcase your work beautifully & close more services like a Pro. Do not pay cut of your sales or monthly fee to 3rd party marketplaces. <br> Eg. Sites for website developers, video marketers, graphics designers etc.
                              </div> -->
                        </div>
                     </div>
                     <div class="col-12 col-md-4 mt20 mt-md0 ">
                        <div class="shp-blc ">
                           <img src="assets/images/icnm4.webp " class="img-fluid d-block mx-auto " />
                        </div>
                        <div class="icn-shape ">
                           <div class="text-center f-22 f-md-26 w600 lh140 ">
                              E-com Stores
                           </div>
                           <!-- <div class="text-center f-md-20 f-20 mt10 mt-md15 w400 lh140 ">
                              Sell more products on your own store. Have full control on your business. <br>Eg. E-com sites for Info-Product Sellers, Software Sellers, Physical Product Sellers like Florists, Cake Shops, etc
                              </div> -->
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt-md100 ">
                  <div class="row ">
                     <div class="col-12 col-md-4 mt20 mt-md0 ">
                        <div class="shp-blc ">
                           <img src="assets/images/icnm5.webp " class="img-fluid d-block mx-auto " />
                        </div>
                        <div class="icn-shape ">
                           <div class="text-center f-22 f-md-26 w600 lh140 ">
                              Affiliate Review/Niche Sites
                           </div>
                           <!-- <div class="text-center f-md-20 f-20 mt10 mt-md15 w400 lh140 ">
                              Create niche content sites or product review sites and get more traffic, build list & earn affiliate commissions.
                              </div> -->
                        </div>
                     </div>
                     <div class="col-12 col-md-4 mt20 mt-md0 ">
                        <div class="shp-blc ">
                           <img src="assets/images/icnm1.webp " class="img-fluid d-block mx-auto " />
                        </div>
                        <div class="icn-shape ">
                           <div class="text-center f-22 f-md-26 w600 lh140 ">
                              CMS Blogs
                           </div>
                           <!-- <div class="text-center f-md-20 f-20 mt10 mt-md15 w400 lh140 ">
                              Regularly Updating Sites That Generate Tons Of Traffic. Eg. Health Experts, Matchmakers, Travel Writers, Business Coaches & Marketing Blogs.
                              </div> -->
                        </div>
                     </div>
                     <div class="col-12 col-md-4 mt20 mt-md0 ">
                        <div class="shp-blc ">
                           <img src="assets/images/icnm2.webp " class="img-fluid d-block mx-auto " />
                        </div>
                        <div class="icn-shape ">
                           <div class="text-center f-22 f-md-26 w600 lh140 ">
                              Lead Generation Sites & Much More…
                           </div>
                           <!-- <div class="text-center f-md-20 f-20 mt10 mt-md15 w400 lh140 ">
                              Generate More Business Leads. Eg. Websites for Insurance Companies, Loan Providers, Car Dealers, Wedding Planners, B2B Businesses, etc.
                              </div> -->
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="white-section ">
         <div class="container ">
            <div class="row ">
               <!-- Landing Grid 1 -->
               <div class="col-12 ">
                  <div class="row align-items-center ">
                     <div class="col-md-6 col-12 ">
                        <div class="f-24 f-md-32 w600 lh140 ">
                           Every Site You Create With WEBPULL Is Fully SEO-Optimized
                        </div>
                        <div class="f-18 f-md-20 w400 mt20 lh140 ">
                           We’ve designed this platform to match all your on-page SEO needs. 
                        </div>
                     </div>
                     <div class="col-md-6 col-12 mt20 mt-md0 ">
                        <img src="assets/images/f3-a.webp " class="img-fluid d-block mx-auto " />
                     </div>
                  </div>
               </div>
               <!-- Landing Grid 2 -->
               <div class="col-12 mt-md90 mt20 ">
                  <div class="row align-items-center ">
                     <div class="col-md-6 col-12 order-md-2 ">
                        <div class="f-24 f-md-32 w600 lh140 ">
                           Get More Likes, Shares & Viral Traffic With In-Built Social Media Tools 
                        </div>
                        <div class="f-18 f-md-20 w400 mt20 lh140 ">
                           Because most of the people you serve to use social media to  
                           review your business, you’ll need to make sure that your  
                           business looks up to par on every social media platform. 
                        </div>
                     </div>
                     <div class="col-md-6 col-12 order-md-1 mt20 mt-md0 ">
                        <img src="assets/images/f3-b.webp " class="img-fluid d-block mx-auto " />
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="grey-section ">
         <div class="container ">
            <div class="row ">
               <div class="col-12 f-28 f-md-45 w600 lh140 black-clr text-center ">
                  Accept Online Payments For Your Services with Seamless WooCommerce Integration
               </div>
               <div class="col-12 mt-md45 mt20 ">
                  <img src="assets/images/at.webp " class="img-fluid d-block mx-auto " />
               </div>
               <div class="col-12 f-18 f-md-20 w400 lh140 mt20 mt-md50 text-center ">
                  WEBPULL comes with Seamless WooCommerce Integration used by around 2.3 million eCommerce shops worldwide, WooCommerce is arguably the best payment plugin for websites that helps merchants sell anything easily.
               </div>
            </div>
         </div>
      </div>
      <div class="green-bg-section ">
         <div class="container ">
            <div class="row ">
               <div class="col-12 f-28 f-md-45 w600 lh140 white-clr text-center ">
                  And we’re only getting started!<br class="d-none d-md-block "> There’s still a lot more in store for you…
               </div>
            </div>
         </div>
      </div>
      <div class="theme-section ">
         <div class="container ">
            <div class="row ">
               <div class="col-12 f-28 f-md-45 w600 lh140 white-clr text-capitalize text-center ">
                  Super-Powerful Mobile Websites  
               </div>
            </div>
            <div class="row align-items-center">
               <div class="col-12 col-md-6 order-md-2 mt-md55 mt20 ">
                  <div class="f-18 f-md-20 w400 lh140 white-clr ">
                     Webpull creates fully responsive and ultra-fast loading mobile  
                     websites which you can manage even from your phones.  
                     <br><br>
                     As per the latest industry figures, in the first quarter of 2022, mobile  
                     devices (excluding tablets) generated 51.92% of the global website  
                     traffic. So, it is important to have mobile-friendly websites.  
                  </div>
               </div>
               <div class="col-12 col-md-6 order-md-1 mt-md70 mt20 ">
                  <img src="assets/images/website-sol.webp " class="img-fluid d-block mx-auto " />
               </div>
            </div>
            <div class="row mt-md70 mt20 ">
               <div class="col-12 col-md-4 mt-md0 mt10 ">
                  <div class="shp-blc ">
                     <img src="assets/images/icnm10.webp " class="img-fluid d-block mx-auto " />
                  </div>
                  <div class="icn-shape ">
                     <div class="text-center f-22 f-md-26 w600 lh140 ">
                        100% Responsive On All Devices
                     </div>
                     <!-- <div class="text-center f-md-20 f-20 mt10 mt-md15 w400 lh140 ">
                        It is fully responsive so works great on all the Mobile devices, Tablets & all desktop sizes to engage visitors 360* directions.
                        </div> -->
                  </div>
               </div>
               <div class="col-12 col-md-4 mt-md0 mt30 ">
                  <div class="shp-blc ">
                     <img src="assets/images/icnm11.webp " class="img-fluid d-block mx-auto " />
                  </div>
                  <div class="icn-shape ">
                     <div class="text-center f-22 f-md-26 w600 lh140 ">
                        Ultra Fast Mobile Sites
                     </div>
                     <!-- <div class="text-center f-md-20 f-20 mt10 mt-md15 w400 lh140 ">
                        Latest Google updates give top rankings only to the websites that load fast. You can customize mobile sites using our theme so heavy coding doesn’t hamper your user experience.
                        </div> -->
                  </div>
               </div>
               <div class="col-12 col-md-4 mt-md0 mt30 ">
                  <div class="shp-blc ">
                     <img src="assets/images/icnm12.webp " class="img-fluid d-block mx-auto " />
                  </div>
                  <div class="icn-shape ">
                     <div class="text-center f-22 f-md-26 w600 lh140 ">
                        Manage Websites Even From Your Phone
                     </div>
                     <!-- <div class="text-center f-md-20 f-20 mt10 mt-md15 w400 lh140 ">
                        Isn’t that COOL? <br><br> Be free to manage anything anytime from your mobile device even while you’re on the go.
                        </div> -->
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="white-section ">
         <div class="container ">
            <div class="row align-items-center">
               <div class="col-md-6 col-12 order-md-2 ">
                  <div class="f-24 f-md-32 w600 lh140 text-md-start text-center ">
                     Engage & Convert More of Your Traffic, Into Brand Loyalists
                  </div>
                  <div class="f-18 f-md-20 w400 mt20 lh140 text-md-start text-center ">
                     When you combine red-hot traffic with super engaging business 
                     websites, the result can be massive sales for your 
                     business. <br><br>
                     <span class="w600">That’s why you’ll love WEBPULL </span>
                  </div>
               </div>
               <div class="col-md-6 col-12 order-md-1 mt-md0 mt20 ">
                  <img src="assets/images/ns1.webp " class="img-fluid d-block mx-auto " />
               </div>
            </div>
         </div>
      </div>
      <div class="grey-section ">
         <div class="container ">
            <div class="row ">
               <div class="col-md-12 col-12 ">
                  <div class="f-28 f-md-45 w600 lh140 black-clr text-center ">
                     Get 20+ Beautiful Slider Combinations To Engage & Wow Visitors 
                  </div>
               </div>
               <div class="col-md-10 col-12 mt-md45 mt15 mx-auto ">
                  <img src="assets/images/engage-convert.webp " class="img-fluid d-block mx-auto " />
               </div>
               <div class="col-12 f-18 f-md-20 w400 lh140 mt20 mt-md40 text-center px-md30 ">
                  <span class="w600">You can customize anything on this beautiful slider.</span> From lead generation forms, images, calls to action text or buttons, headlines, background images and even use videos in almost all 20 combinations. This will help grab your visitor’s attention and skyrocket your conversions at will, period.  
               </div>
            </div>
         </div>
      </div>
      <div class="white-section ">
         <div class="container ">
            <div class="row align-items-center ">
               <div class="col-md-6 col-12 ">
                  <div class="f-24 f-md-32 w600 lh140 text-md-start text-center ">
                     Change Website Width & Layout Instantly  
                     Without Touching Any Code 
                  </div>
                  <!-- <div class="f-18 f-md-20 w400 mt20 lh140 text-md-start text-center ">
                     Evenly customize website layout right from dashboard. Set website width, divide it in 2, 3 or 4 columns & decide number of footer widgets etc. in just few clicks.
                     <br><br> With this level of flexibility, you’ll be able to create more unique & branded websites effortlessly.
                     </div> -->
               </div>
               <div class="col-md-6 col-12 mt20 mt-md0 ">
                  <img src="assets/images/c1-a.webp " class="img-fluid d-block mx-auto ">
               </div>
            </div>
            <div class="row mt-md100 mt30 align-items-center ">
               <div class="col-md-6 col-12 order-md-2 ">
                  <div class="f-24 f-md-32 w600 lh140 text-md-start text-center ">
                     Fully Customizable Typography Gives Every Website  
                     You Create A Unique Look and Feel.  
                  </div>
                  <!-- <div class="f-18 f-md-20 w400 mt20 lh140 text-md-start text-center ">
                     The right choice of words depicts your brand personality. They enable you to convert visitors into customers. <br><br> That's why we’re giving you full control on font, size & colour of headlines, text, and links from the dashboard
                     itself without the need for any coding skills. <br><br> Our theme supports 100+ Google fonts. You literally can create 1000s of unique & beautiful website combinations for your clients with ease.
                     </div> -->
               </div>
               <div class="col-md-6 col-12 order-md-1 mt20 mt-md0 ">
                  <img src="assets/images/c1-b.webp " class="img-fluid d-block mx-auto ">
               </div>
            </div>
         </div>
      </div>
      <div class="grey-section ">
         <div class="container ">
            <div class="row ">
               <div class="col-12 f-28 f-md-45 w600 lh140 black-clr text-center ">
                  Convert Any Website Into A Profit Generating Machine  
                  That Gets Tons of Leads, Engagement, And Sales  
               </div>
               <div class="col-md-10 mx-auto mt20 mt-md45 ">
                  <img src="assets/images/ns2.webp " class="img-fluid d-block mx-auto " />
               </div>
            </div>
         </div>
      </div>
      <div class="theme-section ">
         <div class="container ">
            <div class="row ">
               <div class="col-12 f-28 f-md-45 w600 lh140 white-clr text-capitalize text-center ">
                  Built On World's Best Website Builder Framework For BUSINESSES - The WEBPULL 
               </div>
               <div class="col-12 f-18 f-md-20 w400 mt-md30 mt15 text-center white-clr lh140 ">
                  The WEBPULL Framework will empower your customers to build incredible websites quickly and easily without any technical hassles.  
               </div>
            </div>
            <div class="row mt-md50 mt20 ">
               <div class="col-12 col-md-4 mt-md0 mt10 ">
                  <div class="shp-blc ">
                     <img src="assets/images/icnm13.webp " class="img-fluid d-block mx-auto " />
                  </div>
                  <div class="icn-shape ">
                     <div class="text-center f-22 f-md-26 w600 lh140 ">
                        Simple User Interface So Even Grandma Could Use It
                     </div>
                     <!-- <div class="text-center f-md-20 f-20 mt10 mt-md15 w400 lh140 ">
                        It is so easy to use that even a newbie can create sites without any problem.
                        </div> -->
                  </div>
               </div>
               <div class="col-12 col-md-4 mt-md0 mt30 ">
                  <div class="shp-blc ">
                     <img src="assets/images/icnm14.webp " class="img-fluid d-block mx-auto " />
                  </div>
                  <div class="icn-shape ">
                     <div class="text-center f-22 f-md-26 w600 lh140 ">
                        Create Lightning FAST Websites
                     </div>
                     <!-- <div class="text-center f-md-20 f-20 mt10 mt-md15 w400 lh140 ">
                        Proper balancing code built on the latest web designing technologies makes this theme lighting fast to give best User experience.
                        </div> -->
                  </div>
               </div>
               <div class="col-12 col-md-4 mt-md0 mt30 ">
                  <div class="shp-blc ">
                     <img src="assets/images/icnm15.webp " class="img-fluid d-block mx-auto " />
                  </div>
                  <div class="icn-shape ">
                     <div class="text-center f-22 f-md-26 w600 lh140 ">
                        100% Secured Websites
                     </div>
                     <!-- <div class="text-center f-md-20 f-20 mt10 mt-md15 w400 lh140 ">
                        No need to worry about Security issues. These sites come complete with the latest technology & intelligent coding to make it a totally secure platform.
                        </div> -->
                  </div>
               </div>
            </div>
            <div class="row mt-md55 ">
               <div class="col-12 col-md-4 mt-md0 mt30 ">
                  <div class="shp-blc ">
                     <img src="assets/images/icnm16.webp " class="img-fluid d-block mx-auto " />
                  </div>
                  <div class="icn-shape ">
                     <div class="text-center f-22 f-md-26 w600 lh140 ">
                        WEBPULL Also Support Custom CSS For Developers
                     </div>
                     <!-- <div class="text-center f-md-20 f-20 mt10 mt-md15 w400 lh140 ">
                        If you are a developer & want to use custom css, you can do it as well. Simply use hooks to add your custom code.
                        </div> -->
                  </div>
               </div>
               <div class="col-12 col-md-4 mt-md0 mt30 ">
                  <div class="shp-blc ">
                     <img src="assets/images/icnm17.webp " class="img-fluid d-block mx-auto " />
                  </div>
                  <div class="icn-shape ">
                     <div class="text-center f-22 f-md-26 w600 lh140 ">
                        10+ Ready To Use Short Codes
                     </div>
                     <!-- <div class="text-center f-md-20 f-20 mt10 mt-md15 w400 lh140 ">
                        These short codes make your working on site easy & fast.
                        </div> -->
                  </div>
               </div>
               <div class="col-12 col-md-4 mt-md0 mt30 ">
                  <div class="shp-blc ">
                     <img src="assets/images/icnm18.webp " class="img-fluid d-block mx-auto " />
                  </div>
                  <div class="icn-shape ">
                     <div class="text-center f-22 f-md-26 w600 lh140 ">
                        FREE Updates – Limited Time Offer
                     </div>
                     <!-- <div class="text-center f-md-20 f-20 mt10 mt-md15 w400 lh140 ">
                        When you sign up during this launch special, we’ll upgrade you to free updates for life. That means you’ll always have access to the latest version of WEBPULL at no additional charge.
                        </div> -->
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="white-section ">
         <div class="container ">
            <div class="row align-items-center">
               <!-- Landing Grid 1 -->
               <div class="col-md-6 col-12 mt-md0 ">
                  <div class="f-24 f-md-32 w600 lh140 text-md-start text-center ">
                     Inbuilt Lead Management Feature Lets You  
                     Maximize Lead Generation Results.  
                  </div>
                  <!-- <div class="f-18 f-md-20 w400 mt20 lh140 text-md-start text-center ">
                     Tap into ready to use & beautiful Lead forms in 8 different colours. Use these forms to collect maximum leads & keep them organized in your very own lead management panel.
                     </div> -->
               </div>
               <div class="col-md-6 col-12 mt20 mt-md0 ">
                  <img src="assets/images/ns4.webp " class="img-fluid d-block mx-auto " />
               </div>
               <!-- Landing Grid 2 -->
               <div class="col-12 mt-md80 mt30 p-md0 ">
                  <div class="row align-items-center ">
                     <div class="col-md-6 col-12 order-md-2 ">
                        <div class="f-24 f-md-32 w600 lh140 text-md-start text-center ">
                           Skyrocket User Engagement, Conversions & Sales with  
                           CTA Management, Google Maps, and Feature Rich Slider
                        </div>
                        <!-- <div class="f-18 f-md-20 w400 mt20 lh140 ">
                           The topmost priority is to engage users on your business website. To make it easier, we have-
                           <br><br>
                           <span class="w600 ">CTA Management –</span> This will help motivate your visitors to take action on your forms which can result in more leads and sales faster.<br><br>
                           <span class="w600 ">Google Maps –</span> Allows mobile to get directions that helps them reach businesses easily.<br><br>
                           <span class="w600 ">Feature Rich Slider -</span> Highly customizable sliders to meet your requirements.
                           </div> -->
                     </div>
                     <div class="col-md-6 col-12 order-md-1 mt20 mt-md0 ">
                        <img src="assets/images/ns5.webp " class="img-fluid d-block mx-auto " />
                     </div>
                  </div>
               </div>
               <!-- Landing Grid 3 -->
               <div class="col-12 mt-md70 mt30 p-md0 ">
                  <div class="row align-items-center ">
                     <div class="col-md-6 col-12 mt-md0 ">
                        <div class="f-24 f-md-32 w600 lh140 text-md-start text-center ">
                           Analytics & Remarketing Ready 
                        </div>
                        <!-- <div class="f-18 f-md-20 w400 mt20 lh140 text-md-start text-center ">
                           Make the best use of Google Analytics and FB Remarketing Pixels or any other third-party code without investing any extra time. Just put these codes in your dashboard and track all the data you need for your campaigns.
                           </div> -->
                     </div>
                     <div class="col-md-6 col-12 mt20 mt-md0 ">
                        <img src="assets/images/ns6.webp " class="img-fluid d-block mx-auto " />
                     </div>
                  </div>
               </div>
               <!-- Landing Grid 4 -->
               <div class="col-12 mt-md80 mt30 p-md0 ">
                  <div class="row align-items-center ">
                     <div class="col-md-6 col-12 order-md-2 mt-md0 ">
                        <div class="f-24 f-md-32 w600 lh140 text-md-start text-center ">
                           Ready-To-Use Widgets 
                        </div>
                        <!-- <div class="f-18 f-md-20 w400 mt20 lh140 text-md-start text-center ">
                           Allows you to add or remove Google maps, Lead forms, CTAs, and Custom TABs easy and fast.
                           </div> -->
                     </div>
                     <div class="col-md-6 col-12 order-md-1 mt20 mt-md0 ">
                        <div>
                           <img src="assets/images/ns7.webp " class="img-fluid d-block mx-auto " />
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Features Part End-->
      <!-- Suport Section Start -->
      <div class="support-section">
         <div class="container ">
            <div class="row ">
               <div class="col-12 ">
                  <div class="f-md-45 f-28 w600 lh140 white-clr text-center ">
                     Here’re Some More Features
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50 ">
                  <div class="row g-0 ">
                     <!-- first -->
                     <div class="col-12 col-md-4 ">
                        <div class="boxbg boxborder-rb boxborder top-first ">
                           <img src="assets/images/k1.webp " class="img-fluid d-block mx-auto ">
                           <div class="f-18 f-md-20 w400 lh140 mt15 mt-md40 text-center white-clr ">
                              Round-the-Clock Support
                           </div>
                        </div>
                     </div>
                     <!-- second -->
                     <div class="col-12 col-md-4 ">
                        <div class="boxbg boxborder-rb boxborder ">
                           <img src="assets/images/k2.webp " class="img-fluid d-block mx-auto ">
                           <div class="f-18 f-md-20 w400 lh140 mt15 mt-md40 text-center white-clr ">
                              No Coding, Design or Technical
                              <br class="visible-lg "> Skills Required
                           </div>
                        </div>
                     </div>
                     <!-- third -->
                     <div class="col-12 col-md-4 ">
                        <div class="boxbg boxborder-b boxborder ">
                           <img src="assets/images/k3.webp " class="img-fluid d-block mx-auto ">
                           <div class="f-18 f-md-20 w400 lh140 mt15 mt-md40 text-center white-clr ">
                              Regular Updates
                           </div>
                        </div>
                     </div>
                     <!-- fourth -->
                     <div class="col-12 col-md-4 ">
                        <div class="boxbg boxborder-r boxborder ">
                           <img src="assets/images/k4.webp " class="img-fluid d-block mx-auto ">
                           <div class="f-18 f-md-20 w400 lh140 mt15 mt-md40 text-center white-clr ">
                              Complete Step-by-Step Video
                              <br class="visible-lg ">Training and Tutorials Included
                           </div>
                        </div>
                     </div>
                     <!-- fifth -->
                     <div class="col-12 col-md-4 ">
                        <div class="boxbg boxborder-r boxborder ">
                           <img src="assets/images/k5.webp " class="img-fluid d-block mx-auto ">
                           <div class="f-18 f-md-20 w400 lh140 mt15 mt-md40 text-center white-clr ">
                              Newbie Friendly & Easy To Use
                           </div>
                        </div>
                     </div>
                     <!-- sixth -->
                     <div class="col-12 col-md-4 ">
                        <div class="boxbg boxborder ">
                           <img src="assets/images/k6.webp " class="img-fluid d-block mx-auto ">
                           <div class="f-18 f-md-20 w400 lh140 mt15 mt-md40 text-center white-clr ">
                              Limited Commercial License
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Suport Section End -->
      <!-- Compare Table Section -->
      <section class="comp-table">
         <div class="comparetable-section ">
            <div class="container ">
               <div class="row ">
                  <div class="col-12 f-20 f-md-24 w500 lh140 black-clr text-center ">
                     WEBPULL Is A Complete Pro Agency Builder.
                  </div>
                  <div class="col-12 mt10 ">
                     <div class="f-md-45 f-28 w700 lh140 black-clr text-center ">
                        No Other Software Even Comes Close!
                     </div>
                     <div class="row g-0 mt70 mt-md100">
                        <div class="col-md-4 col-4">
                           <div class="fist-row">
                              <ul class="f-md-18 f-14 w500 lh120">
                                 <li class="f-md-28 f-16 w700 justify-content-start justify-content-md-center">Features
                                 </li>
                                 <li class="f-md-28 w700">Price </li>
                                 <li><span class="w700">Beautiful Design</span> & Full Customization</li>
                                 <li class="text-start">No. of<span class="w700"> Templates</span></li>
                                 <li>No. of Themes with <span class="w700"> 1 Purchase</span></li>
                                 <li class="f-md-16">1 WP Framework to create any type of site- "Local Sites, Marketplaces, Affiliate Review Sites, Blogs and E-com Sites"
                                 </li>
                                 <li><span class="w700">Theme/Template</span> change on website</li>
                                 <li>You can<span class="w700"> Serve To Clients</span></li>
                                 <li>Responsive &<span class="w700"> Mobile Friendly</span></li>
                                 <li>Inbuilt<span class="w700"> SEO Management</span></li>
                                 <li>Supports<span class="w700"> Custom Widgets</span></li>
                                 <li class="w700">Sidebar Layouts</li>
                                 <li>
                                    <span class="w700">
                                       Woocommerce supported
                                 </li>
                                 <li>Advance<span class="w700"> Inbuilt Sliders</span> Management</li>
                                 <li><span class="w700">Done-for-you local niche content -</span> Blogs Services, Portfolio Etc.</li>
                                 <li class="w700">User Friendly</span>
                                 </li>
                                 <li>Inbulilt <span class="w700">Header Layouts</span></li>
                                 <li class="w700">Multiple Blog Layouts</li>
                                 <li class="w600">Multiple Post Layouts</li>
                                 <li>Inbuilt<span class="w700"> Lead Management</span></li>
                                 <li>Inbuilt<span class="w700"> CTA Management</span></li>
                                 <li class="w700">Inbuilt Google Map</li>
                                 <li class="w700">Analytics Ready</li>
                                 <li><span class="w700">Remarketing Ready</span></li>
                                 <li><span class="w600">Support Hooks, Custom CSS</span> for Developer</li>
                                 <li class="text-start">Comes with<span class="w700"> short codes & widgets</span></li>
                                 <li class="text-start"><span class="w700">Typography support</span></li>
                                 <li><span class="w700">100% Secured Sites</span></li>
                                 <li class="text-start">Inbuilt<span class="w700"> Autoresponder Integration</span></li>
                                 <li class="w700">Social Media Ready</li>
                                 <li><span class="w700">White-labelling Functionality</span> for developers</li>
                                 <li><span class="w700">Library Integration</span> when over 1 Millons Photos</li>
                                 <li class="text-start">Regular<span class="w700"> FREE Updates</span></span>
                                 </li>
                                 <li>Round-the-Clock<span class="w700">chat support</span></li>
                                 <li class="text-start">FREE<span class="w700"> Commercial License</span></li>
                              </ul>
                           </div>
                        </div>
                        <div class="col-md-2 col-2">
                           <div class="second-row">
                              <ul class="f-md-16 f-14 w600 white-clr ttext-center lh120">
                                 <li class="f-md-20 f-16 white-clr">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 394.2 100" style="enable-background:new 0 0 394.2 100; max-height:60px" xml:space="preserve">
                                       <style type="text/css">
                                          .st00 {
                                          fill: #FFFFFF;
                                          }
                                          .st11 {
                                          fill: url(#SVGID_1_);
                                          }
                                          .st22 {
                                          fill: url(#SVGID_00000021093791325859232720000015953972570224053676_);
                                          }
                                          .st33 {
                                          fill: #FFC802;
                                          }
                                          .st44 {
                                          fill: url(#SVGID_00000003068462962921929030000010945183465240898967_);
                                          }
                                       </style>
                                       <path class="st00"
                                          d="M117.7,37.9l-5,12.4H92.9v12.4h18.6l-5,12.4H92.9v12.4h24.8l-5,12.5H92.9H80.5V75.1V62.7V37.9H117.7z">
                                       </path>
                                       <path class="st00" d="M323.2,37.9v49.7H348V100h-37.3V37.9H323.2z">
                                       </path>
                                       <path class="st00" d="M369.4,37.9v49.7h24.8V100h-37.3V37.9H369.4z">
                                       </path>
                                       <g>
                                          <g>
                                             <polygon class="st00"
                                                points="59.1,71.4 52.4,54.4 59.1,37.9 72.6,37.9   ">
                                             </polygon>
                                             <polygon class="st00"
                                                points="29.4,54.7 36.3,37.9 54.5,82.9 47.7,100   ">
                                             </polygon>
                                             <polygon class="st00"
                                                points="24.8,100 31.6,82.8 13.4,37.9 0,37.9   "></polygon>
                                          </g>
                                       </g>
                                       <path class="st00"
                                          d="M222.8,44.3c-4.3-4.3-9.4-6.4-15.4-6.4h-9.3h-12.5v21.7v21.7V100h12.5V81.3h0V68.9h0v-9.3v-9.3h9.3  c2.6,0,4.8,0.9,6.6,2.8c1.8,1.8,2.7,4,2.7,6.6c0,2.6-0.9,4.8-2.7,6.6c-1.8,1.8-4,2.7-6.6,2.7h-0.7v12.4h0.7c6,0,11.1-2.1,15.4-6.4  c4.3-4.3,6.4-9.4,6.4-15.4C229.2,53.6,227.1,48.5,222.8,44.3z">
                                       </path>
                                       <rect x="252.1" y="31.8"
                                          transform="matrix(0.4226 -0.9063 0.9063 0.4226 114.0209 255.0236)"
                                          class="st00" width="10.2" height="12.5"></rect>
                                       <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse"
                                          x1="249.3741" y1="97.3262" x2="270.7371" y2="53.5256">
                                          <stop offset="0" style="stop-color:#FA8460"></stop>
                                          <stop offset="1" style="stop-color:#FCB121"></stop>
                                       </linearGradient>
                                       <path class="st11"
                                          d="M271.7,80.3c-1.4,3.1-3.8,5.2-7,6.4c-3.2,1.2-6.4,1-9.5-0.4c-3.1-1.4-5.2-3.8-6.4-7c-1.2-3.2-1-6.4,0.4-9.5  l9.9-21.2l-11.3-5.3l-9.9,21.2c-2.9,6.2-3.2,12.6-0.8,19c2.3,6.5,6.6,11.1,12.9,14c6.2,2.9,12.5,3.2,19,0.8  c6.5-2.3,11.2-6.6,14.1-12.8l9.9-21.2l-11.3-5.3L271.7,80.3z">
                                       </path>
                                       <rect x="285.9" y="47.6"
                                          transform="matrix(0.4226 -0.9063 0.9063 0.4226 119.2494 294.738)"
                                          class="st00" width="10.2" height="12.5"></rect>
                                       <path class="st00"
                                          d="M173.3,76.3c-0.8-1.7-1.7-3.2-2.9-4.5c-1.2-1.3-2.5-2.3-4-3.1c-1.5-0.8-3-1.2-4.5-1.3c1.5-0.1,2.8-0.5,4.1-1.3  c1.2-0.8,2.3-1.8,3.2-3c0.9-1.2,1.6-2.5,2.1-3.9c0.5-1.4,0.8-2.8,0.8-4c0-3.5-0.5-6.4-1.4-8.6c-1-2.2-2.3-4-3.9-5.3  c-1.6-1.3-3.6-2.2-5.8-2.6c-2.2-0.5-4.6-0.7-7-0.7h-27.3v10.5h11.9v0h15.4c0.8,0,1.7,0.2,2.4,0.6c0.8,0.4,1.4,0.9,2,1.5  c0.6,0.6,1,1.4,1.4,2.2c0.3,0.9,0.5,1.7,0.5,2.6c0,0.9-0.2,1.8-0.5,2.8c-0.3,0.9-0.8,1.8-1.4,2.5c-0.6,0.7-1.3,1.3-2.2,1.8  c-0.9,0.5-1.9,0.7-3.1,0.7h-9.1v10.5h10c2.7,0,4.9,0.7,6.4,2.1c1.6,1.4,2.3,3.2,2.3,5.3c0,1-0.2,2-0.6,3c-0.4,1-0.9,1.9-1.6,2.7  c-0.7,0.8-1.5,1.5-2.4,2c-0.9,0.5-1.9,0.7-2.9,0.7h-16.8h0V73.7h0V63.2h0v-9.3h-11.9V100h9.6h2.3h16.8c2.7,0,5.2-0.3,7.5-0.9  c2.3-0.6,4.4-1.6,6.2-3.1c1.8-1.4,3.1-3.3,4.2-5.7c1-2.3,1.5-5.2,1.5-8.6C174.4,79.8,174,78,173.3,76.3z">
                                       </path>
                                       <g>
                                          <linearGradient
                                             id="SVGID_00000142155450205615942230000010409392281687221429_"
                                             gradientUnits="userSpaceOnUse" x1="270.7616" y1="25.6663"
                                             x2="296.63" y2="25.6663">
                                             <stop offset="0" style="stop-color:#FA8460"></stop>
                                             <stop offset="4.692155e-02" style="stop-color:#FA9150"></stop>
                                             <stop offset="0.1306" style="stop-color:#FBA43A"></stop>
                                             <stop offset="0.2295" style="stop-color:#FBB229"></stop>
                                             <stop offset="0.3519" style="stop-color:#FCBC1E"></stop>
                                             <stop offset="0.5235" style="stop-color:#FCC117"></stop>
                                             <stop offset="1" style="stop-color:#FCC315"></stop>
                                          </linearGradient>
                                          <path
                                             style="fill:url(#SVGID_00000142155450205615942230000010409392281687221429_);"
                                             d="M289.6,14.2c-2.1-1.1-4.3-1.5-6.5-1.4   c-0.1,0-0.1,0-0.2,0c-0.1,0-0.2,0-0.2,0c0,0-0.1,0-0.1,0c-0.1,0-0.2,0-0.2,0c-0.1,0-0.2,0-0.3,0c-0.1,0-0.2,0-0.2,0   c-0.3,0-0.5,0.1-0.8,0.1c-0.1,0-0.2,0-0.3,0.1c-0.1,0-0.2,0-0.3,0.1c-0.1,0-0.3,0.1-0.4,0.1c-0.1,0-0.2,0-0.2,0.1   c-0.1,0-0.2,0.1-0.3,0.1c-0.2,0.1-0.5,0.2-0.7,0.3c0,0,0,0,0,0c-0.3,0.1-0.6,0.3-0.9,0.4c-0.3,0.1-0.6,0.3-0.9,0.5   c-0.1,0.1-0.3,0.2-0.4,0.2c-0.1,0.1-0.2,0.2-0.4,0.2c-0.1,0.1-0.2,0.1-0.2,0.2c0,0-0.1,0-0.1,0.1c-0.1,0.1-0.2,0.1-0.2,0.2   c-0.1,0-0.1,0.1-0.2,0.2c-0.1,0.1-0.2,0.2-0.3,0.3c-0.1,0-0.1,0.1-0.2,0.2c-0.5,0.4-0.9,0.9-1.3,1.4c-0.1,0.1-0.1,0.1-0.2,0.2   c0,0-0.1,0.1-0.1,0.1c-0.1,0.1-0.1,0.2-0.2,0.3c0,0-0.1,0.1-0.1,0.1c-0.1,0.1-0.1,0.2-0.2,0.3c0,0,0,0.1-0.1,0.1   c-0.1,0.1-0.1,0.2-0.2,0.3c0,0,0,0.1-0.1,0.1c-0.1,0.1-0.1,0.2-0.2,0.3c-0.1,0.1-0.2,0.3-0.2,0.4c-3.3,6.3-0.8,14.1,5.6,17.4   c1.9,1,3.9,1.4,5.9,1.4c0.1,0,0.3,0,0.4,0c0.1,0,0.3,0,0.4,0c0.1,0,0.1,0,0.2,0c0.1,0,0.2,0,0.3,0c0,0,0,0,0.1,0c0.1,0,0.2,0,0.3,0   c0.1,0,0.2,0,0.2,0c0.1,0,0.2,0,0.3-0.1c0.1,0,0.3,0,0.4-0.1c0.1,0,0.3-0.1,0.4-0.1c0.1,0,0.2-0.1,0.3-0.1c0.2,0,0.4-0.1,0.5-0.2   c0.1,0,0.3-0.1,0.4-0.1c0.1,0,0.3-0.1,0.4-0.1c0,0,0,0,0,0c0.1,0,0.2-0.1,0.4-0.1c0,0,0,0,0,0c0,0,0.1,0,0.1,0c0,0,0.1,0,0.1-0.1   c0,0,0,0,0.1,0c0,0,0.1,0,0.1,0c0,0,0,0,0.1,0c0.1,0,0.2-0.1,0.3-0.2c0,0,0,0,0.1,0c0.1-0.1,0.2-0.1,0.3-0.2   c0.1-0.1,0.3-0.1,0.4-0.2c0.4-0.2,0.7-0.5,1.1-0.7c0.2-0.2,0.5-0.4,0.7-0.6c0.2-0.2,0.4-0.4,0.7-0.6c0.2-0.2,0.4-0.4,0.6-0.6   c0.2-0.2,0.4-0.5,0.6-0.7c0.1-0.1,0.2-0.2,0.3-0.4c0,0,0,0,0,0c0.1-0.1,0.2-0.2,0.3-0.4c0.1-0.1,0.2-0.3,0.2-0.4   c0.1-0.1,0.1-0.2,0.2-0.3c0-0.1,0.1-0.1,0.1-0.2c0.1-0.1,0.1-0.2,0.2-0.3C298.5,25.3,296,17.5,289.6,14.2z M279.6,26.7L279.6,26.7   l0.3,0.6c0.4,0.7,1,1.3,1.8,1.7c0.3,0.2,0.7,0.3,1,0.3c0.3,0,0.5-0.1,0.7-0.2c0.1,0,0.1-0.1,0.2-0.2c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0-0.1c0.2-0.3,0.2-0.7-0.1-1.3c0,0,0,0,0,0   c-0.1-0.2-0.2-0.4-0.4-0.6c-0.1-0.1-0.2-0.2-0.3-0.3c-0.4-0.4-0.7-0.8-0.9-1.2c-0.1-0.1-0.2-0.2-0.2-0.4c0,0,0,0,0-0.1   c0,0,0-0.1-0.1-0.1c0,0,0-0.1-0.1-0.1c-0.1-0.2-0.1-0.3-0.2-0.5c-0.1-0.2-0.1-0.4-0.2-0.6c0,0,0-0.1,0-0.1c0-0.1,0-0.2,0-0.3v-0.1   c0-0.1,0-0.1,0-0.2c0,0,0-0.1,0-0.1c0-0.1,0-0.2,0-0.3c0.1-0.3,0.2-0.6,0.3-0.8c0,0,0,0,0-0.1c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0-0.1c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0l0,0c0.4-0.5,0.9-0.9,1.5-1.1c0.7-0.2,1.4-0.3,2.1-0.1l0.9-1.7l1.9,1l-0.8,1.6c0.7,0.4,1.2,1,1.6,1.6   l0.2,0.4l-1.8,1.6l-0.3-0.5c-0.1-0.1-0.3-0.4-0.5-0.7c-0.1-0.1-0.3-0.3-0.5-0.4c0,0-0.1-0.1-0.1-0.1c-0.1-0.1-0.3-0.2-0.4-0.2   c0,0-0.1,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0   c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0h0c0,0,0,0-0.1,0h0   c0,0,0,0-0.1,0h0c0,0,0,0,0,0h0c0,0,0,0,0,0h0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c-0.2,0.1-0.4,0.3-0.5,0.5c-0.3,0.5-0.2,0.9,1,2.3c0.8,0.9,1.2,1.6,1.4,2.4   c0.2,0.8,0.1,1.6-0.3,2.5c-0.8,1.5-2.4,2.2-4.2,1.8l-0.8,1.6l-0.1,0.2l-2-1l0.9-1.7c-0.1-0.1-0.2-0.1-0.3-0.2   c-0.3-0.2-0.5-0.4-0.8-0.6c0,0-0.1-0.1-0.1-0.1c-0.3-0.3-0.5-0.6-0.7-1l-0.2-0.4L279.6,26.7z">
                                          </path>
                                          <path class="st33"
                                             d="M278,37c-6.3-3.3-8.8-11.1-5.6-17.4c1.3-2.4,3.2-4.3,5.4-5.5c-2.4,1.2-4.4,3.1-5.7,5.6   c-3.3,6.3-0.8,14.1,5.6,17.4c3.9,2,8.4,1.9,12-0.1C286.2,38.9,281.8,39,278,37z">
                                          </path>
                                       </g>
                                       <g>
                                          <linearGradient
                                             id="SVGID_00000016062571573767098340000005260099975199807150_"
                                             gradientUnits="userSpaceOnUse" x1="79.734" y1="-130.471"
                                             x2="91.3973" y2="-130.471"
                                             gradientTransform="matrix(0.8624 -0.5063 0.5063 0.8624 260.6894 161.6657)">
                                             <stop offset="0" style="stop-color:#FA8460"></stop>
                                             <stop offset="4.692155e-02" style="stop-color:#FA9150"></stop>
                                             <stop offset="0.1306" style="stop-color:#FBA43A"></stop>
                                             <stop offset="0.2295" style="stop-color:#FBB229"></stop>
                                             <stop offset="0.3519" style="stop-color:#FCBC1E"></stop>
                                             <stop offset="0.5235" style="stop-color:#FCC117"></stop>
                                             <stop offset="1" style="stop-color:#FCC315"></stop>
                                          </linearGradient>
                                          <path
                                             style="fill:url(#SVGID_00000016062571573767098340000005260099975199807150_);"
                                             d="M268.1,0c-1,0.1-2,0.4-2.8,0.9   c0,0-0.1,0-0.1,0c0,0-0.1,0-0.1,0.1c0,0,0,0,0,0c0,0-0.1,0-0.1,0.1c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0-0.1,0.1   c-0.1,0.1-0.2,0.1-0.3,0.2c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0.1-0.1,0.1   c-0.1,0.1-0.1,0.2-0.2,0.3c0,0,0,0,0,0c-0.1,0.1-0.2,0.2-0.2,0.4c-0.1,0.1-0.2,0.3-0.2,0.4c0,0.1-0.1,0.1-0.1,0.2   c0,0.1-0.1,0.1-0.1,0.2c0,0,0,0.1-0.1,0.1c0,0,0,0,0,0.1c0,0,0,0.1-0.1,0.1c0,0,0,0.1,0,0.1c0,0.1,0,0.1-0.1,0.2c0,0,0,0.1,0,0.1   c-0.1,0.3-0.1,0.5-0.2,0.8c0,0,0,0.1,0,0.1c0,0,0,0.1,0,0.1c0,0.1,0,0.1,0,0.2c0,0,0,0,0,0.1c0,0.1,0,0.1,0,0.2c0,0,0,0,0,0.1   c0,0.1,0,0.1,0,0.2c0,0,0,0,0,0.1c0,0.1,0,0.1,0,0.2c0,0.1,0,0.1,0,0.2c0.2,3.2,2.9,5.7,6.1,5.5c1-0.1,1.9-0.3,2.6-0.8   c0.1,0,0.1-0.1,0.2-0.1c0.1,0,0.1-0.1,0.1-0.1c0,0,0,0,0.1,0c0,0,0.1,0,0.1-0.1c0,0,0,0,0,0c0,0,0.1-0.1,0.1-0.1c0,0,0.1,0,0.1-0.1   c0,0,0.1-0.1,0.1-0.1c0,0,0.1-0.1,0.1-0.1c0,0,0.1-0.1,0.1-0.1c0,0,0.1-0.1,0.1-0.1c0.1-0.1,0.1-0.1,0.2-0.2c0,0,0.1-0.1,0.1-0.1   c0,0,0.1-0.1,0.1-0.1c0,0,0,0,0,0c0,0,0.1-0.1,0.1-0.1c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0-0.1c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0.1-0.1,0.1-0.1c0,0,0,0,0,0c0,0,0.1-0.1,0.1-0.1c0-0.1,0.1-0.1,0.1-0.2c0.1-0.2,0.2-0.4,0.3-0.5c0.1-0.1,0.1-0.2,0.1-0.4   c0-0.1,0.1-0.3,0.1-0.4c0-0.1,0.1-0.3,0.1-0.4c0-0.1,0-0.3,0.1-0.4c0-0.1,0-0.1,0-0.2c0,0,0,0,0,0c0-0.1,0-0.1,0-0.2   c0-0.1,0-0.1,0-0.2c0-0.1,0-0.1,0-0.2c0,0,0-0.1,0-0.1c0-0.1,0-0.1,0-0.2C274.1,2.3,271.3-0.2,268.1,0z M267.1,7.2L267.1,7.2   l0.3,0.2c0.3,0.2,0.7,0.3,1.1,0.3c0.2,0,0.3-0.1,0.4-0.1c0.1-0.1,0.2-0.1,0.2-0.3c0,0,0-0.1,0-0.1c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0-0.2-0.1-0.3-0.3-0.5c0,0,0,0,0,0   c-0.1,0-0.2-0.1-0.3-0.1c-0.1,0-0.1,0-0.2-0.1c-0.2-0.1-0.4-0.2-0.6-0.2c-0.1,0-0.1-0.1-0.2-0.1c0,0,0,0,0,0c0,0,0,0-0.1,0   c0,0,0,0-0.1,0c-0.1,0-0.1-0.1-0.2-0.1c-0.1-0.1-0.1-0.1-0.2-0.2c0,0,0,0,0,0c0,0-0.1-0.1-0.1-0.1l0,0c0,0,0,0,0-0.1c0,0,0,0,0,0   c0,0,0-0.1-0.1-0.1c0-0.1-0.1-0.3-0.1-0.4c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0l0,0   c0-0.3,0.2-0.5,0.3-0.8c0.2-0.2,0.5-0.4,0.8-0.5l0-0.9l1-0.1l0,0.8c0.4,0,0.7,0.1,1,0.3l0.2,0.1l-0.3,1l-0.3-0.1   c-0.1,0-0.2-0.1-0.4-0.1c-0.1,0-0.2,0-0.3-0.1c0,0-0.1,0-0.1,0c-0.1,0-0.1,0-0.2,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0l0,0c0,0,0,0,0,0l0,0c0,0,0,0,0,0l0,0c0,0,0,0,0,0l0,0c0,0,0,0,0,0l0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c-0.1,0.1-0.1,0.2-0.1,0.3c0,0.2,0.1,0.4,0.9,0.7c0.5,0.2,0.8,0.4,1.1,0.6c0.3,0.3,0.4,0.6,0.4,1c0,0.8-0.4,1.4-1.2,1.7l0,0.8   l0,0.1l-1,0.1l0-0.8c-0.1,0-0.1,0-0.2,0c-0.2,0-0.3,0-0.4-0.1c0,0-0.1,0-0.1,0c-0.2-0.1-0.4-0.1-0.5-0.2l-0.2-0.1L267.1,7.2z">
                                          </path>
                                          <path class="st33"
                                             d="M268.8,11.5c-3.2,0.2-6-2.3-6.1-5.5c-0.1-1.2,0.3-2.4,0.9-3.4c-0.7,1-1,2.2-0.9,3.5c0.2,3.2,2.9,5.7,6.1,5.5   c2-0.1,3.7-1.2,4.6-2.8C272.4,10.4,270.7,11.4,268.8,11.5z">
                                          </path>
                                       </g>
                                       <polygon class="st00"
                                          points="297.8,43.3 296.4,42.7 294.6,46.7 295.4,41.2 296.8,41.8 298.7,37.9 ">
                                       </polygon>
                                       <polygon class="st00"
                                          points="264.2,27.2 262.9,26.4 260.7,30.2 262,24.8 263.3,25.6 265.5,21.8 ">
                                       </polygon>
                                    </svg>
                                 </li>
                                 <li class="w600"><span class="w700 orange-clr f-md-28"> $17</span> <br>One Time During Launch Only</li>
                                 <li>
                                    <img src="assets/images/right-tick.webp" class="img-fluid mx-auto d-block" alt="tick">
                                 </li>
                                 <li>
                                    400+
                                 </li>
                                 <li>
                                    30+
                                 </li>
                                 <li>
                                    <img src="assets/images/right-tick.webp" class="img-fluid mx-auto d-block" alt="tick">
                                 </li>
                                 <li>
                                    Cloud Based
                                 </li>
                                 <li>
                                    <img src="assets/images/right-tick.webp" class="img-fluid mx-auto d-block" alt="tick">
                                 </li>
                                 <li>
                                    <img src="assets/images/right-tick.webp" class="img-fluid mx-auto d-block" alt="tick">
                                 </li>
                                 <li>
                                    <img src="assets/images/right-tick.webp" class="img-fluid mx-auto d-block" alt="tick">
                                 </li>
                                 <li>
                                    <img src="assets/images/right-tick.webp" class="img-fluid mx-auto d-block" alt="tick">
                                 </li>
                                 <li>
                                    <img src="assets/images/right-tick.webp" class="img-fluid mx-auto d-block" alt="tick">
                                 </li>
                                 <li>
                                    <img src="assets/images/right-tick.webp" class="img-fluid mx-auto d-block" alt="tick">
                                 </li>
                                 <li>
                                    <img src="assets/images/right-tick.webp" class="img-fluid mx-auto d-block" alt="tick">
                                 </li>
                                 <li>
                                    <img src="assets/images/right-tick.webp" class="img-fluid mx-auto d-block" alt="tick">
                                 </li>
                                 <li>
                                    <img src="assets/images/right-tick.webp" class="img-fluid mx-auto d-block" alt="tick">
                                 </li>
                                 <li><img src="assets/images/right-tick.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                                 <li><img src="assets/images/right-tick.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                                 <li><img src="assets/images/right-tick.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                                 <li><img src="assets/images/right-tick.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                                 <li><img src="assets/images/right-tick.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                                 <li><img src="assets/images/right-tick.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                                 <li><img src="assets/images/right-tick.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                                 <li><img src="assets/images/right-tick.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                                 <li><img src="assets/images/right-tick.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                                 <li><img src="assets/images/right-tick.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                                 <li><img src="assets/images/right-tick.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                                 <li><img src="assets/images/right-tick.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                                 <li><img src="assets/images/right-tick.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                                 <li><img src="assets/images/right-tick.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                                 <li><img src="assets/images/right-tick.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                                 <li><img src="assets/images/right-tick.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                                 <li><img src="assets/images/right-tick.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                                 <li><img src="assets/images/right-tick.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                                 <li><img src="assets/images/right-tick.webp" class="img-fluid mx-auto d-block" alt="tick"></li>
                              </ul>
                           </div>
                        </div>
                        <div class="col-md-2 col-2">
                           <div class="third-row">
                              <ul class="f-md-16 f-14 w500 lh120">
                                 <li class="f-md-28 f-16 w700"><span>Studio<br class="d-md-none">Press</span></li>
                                 <li class="w600"><span class="w700 f-md-28"> $499</span> <br>One Time
                                 </li>
                                 <li>
                                    <img src="assets/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="right arrow">
                                 </li>
                                 <li class="w700">
                                    60+
                                 </li>
                                 <li class="w700">
                                    Only 1
                                 </li>
                                 <li>
                                    <img src="assets/images/red-cross.webp" class="img-fluid mx-auto d-block" alt="cross">
                                 </li>
                                 <li class="w700">
                                    Manual Upload
                                 </li>
                                 <li class="w700">
                                    Buy License
                                 </li>
                                 <li>
                                    <img src="assets/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="cross">
                                 </li>
                                 <li>
                                    <img src="assets/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="cross">
                                 </li>
                                 <li><img src="assets/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                 <li><img src="assets/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="right arrow"></li>
                                 <li><img src="assets/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="right arrow"></li>
                                 <li><img src="assets/images/red-cross.webp" class="img-fluid mx-auto d-block" alt="right arrow"></li>
                                 <li><img src="assets/images/red-cross.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                 <li><img src="assets/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="right arrow"></li>
                                 <li><img src="assets/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="right arrow"></li>
                                 <li><img src="assets/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="right arrow"></li>
                                 <li><img src="assets/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="right arrow"></li>
                                 <li><img src="assets/images/red-cross.webp" class="img-fluid mx-auto d-block" alt="right arrow"></li>
                                 <li><img src="assets/images/red-cross.webp" class="img-fluid mx-auto d-block" alt="right arrow"></li>
                                 <li><img src="assets/images/red-cross.webp" class="img-fluid mx-auto d-block" alt="right arrow"></li>
                                 <li><img src="assets/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="right arrow"></li>
                                 <li><img src="assets/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="right arrow"></li>
                                 <li><img src="assets/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="right arrow"></li>
                                 <li><img src="assets/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="right arrow"></li>
                                 <li><img src="assets/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                 <li><img src="assets/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                 <li><img src="assets/images/red-cross.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                 <li><img src="assets/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                 <li><img src="assets/images/red-cross.webp" class="img-fluid mx-auto d-block" alt="right arrow">
                                 </li>
                                 <li><img src="assets/images/red-cross.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                 <li><img src="assets/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="right arrow"></li>
                                 <li><img src="assets/images/red-cross.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                 <li><img src="assets/images/red-cross.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                              </ul>
                           </div>
                        </div>
                        <div class="col-md-2 col-2">
                           <div class="forth-row">
                              <ul class="f-md-16 f-14 w500 lh120">
                                 <li class="f-md-28 f-16 w700"><span>Astra</span></li>
                                 <li class="w600"><span class="w700 f-md-28"> $249</span> <br>One Time
                                 </li>
                                 <li>
                                    <img src="assets/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="right arrow">
                                 </li>
                                 <li class="w700"> 100+</li>
                                 <li class="w700">Only 1</li>
                                 <li>
                                    <img src="assets/images/red-cross.webp" class="img-fluid mx-auto d-block" alt="right arrow">
                                 </li>
                                 <li class="w700">Manual Upload</li>
                                 <li class="w700">Buy License</li>
                                 <li>
                                    <img src="assets/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="cross">
                                 </li>
                                 <li>
                                    <img src="assets/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="cross">
                                 </li>
                                 <li><img src="assets/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                 <li><img src="assets/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="right arrow"></li>
                                 <li><img src="assets/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="right arrow"></li>
                                 <li><img src="assets/images/red-cross.webp" class="img-fluid mx-auto d-block" alt="right arrow"></li>
                                 <li><img src="assets/images/red-cross.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                 <li><img src="assets/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="right arrow"></li>
                                 <li><img src="assets/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="right arrow"></li>
                                 <li><img src="assets/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="right arrow"></li>
                                 <li><img src="assets/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="right arrow"></li>
                                 <li><img src="assets/images/red-cross.webp" class="img-fluid mx-auto d-block" alt="right arrow"></li>
                                 <li><img src="assets/images/red-cross.webp" class="img-fluid mx-auto d-block" alt="right arrow"></li>
                                 <li><img src="assets/images/red-cross.webp" class="img-fluid mx-auto d-block" alt="right arrow"></li>
                                 <li><img src="assets/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="right arrow"></li>
                                 <li><img src="assets/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="right arrow"></li>
                                 <li><img src="assets/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="right arrow"></li>
                                 <li><img src="assets/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="right arrow"></li>
                                 <li><img src="assets/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                 <li><img src="assets/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                 <li><img src="assets/images/red-cross.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                 <li><img src="assets/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                 <li><img src="assets/images/red-cross.webp" class="img-fluid mx-auto d-block" alt="right arrow">
                                 </li>
                                 <li><img src="assets/images/red-cross.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                 <li><img src="assets/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="right arrow"></li>
                                 <li><img src="assets/images/red-cross.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                 <li><img src="assets/images/red-cross.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                              </ul>
                           </div>
                        </div>
                        <div class="col-md-2 col-2">
                           <div class="fifth-row">
                              <ul class="f-md-16 f-14">
                                 <li class="f-md-28 f-16 w700"><span>Divi</span></li>
                                 <li class="w600"><span class="w700 f-md-28"> $249</span> <br>One Time
                                 </li>
                                 <li>
                                    <img src="assets/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="right arrow">
                                 </li>
                                 <li class="w700"> 100+</li>
                                 <li class="w700">Only 1</li>
                                 <li>
                                    <img src="assets/images/red-cross.webp" class="img-fluid mx-auto d-block" alt="right arrow">
                                 </li>
                                 <li class="w700">Manual Upload</li>
                                 <li class="w700">Buy License</li>
                                 <li>
                                    <img src="assets/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="cross">
                                 </li>
                                 <li>
                                    <img src="assets/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="cross">
                                 </li>
                                 <li><img src="assets/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                 <li><img src="assets/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="right arrow"></li>
                                 <li><img src="assets/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="right arrow"></li>
                                 <li><img src="assets/images/red-cross.webp" class="img-fluid mx-auto d-block" alt="right arrow"></li>
                                 <li><img src="assets/images/red-cross.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                 <li><img src="assets/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="right arrow"></li>
                                 <li><img src="assets/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="right arrow"></li>
                                 <li><img src="assets/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="right arrow"></li>
                                 <li><img src="assets/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="right arrow"></li>
                                 <li><img src="assets/images/red-cross.webp" class="img-fluid mx-auto d-block" alt="right arrow"></li>
                                 <li><img src="assets/images/red-cross.webp" class="img-fluid mx-auto d-block" alt="right arrow"></li>
                                 <li><img src="assets/images/red-cross.webp" class="img-fluid mx-auto d-block" alt="right arrow"></li>
                                 <li><img src="assets/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="right arrow"></li>
                                 <li><img src="assets/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="right arrow"></li>
                                 <li><img src="assets/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="right arrow"></li>
                                 <li><img src="assets/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="right arrow"></li>
                                 <li><img src="assets/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                 <li><img src="assets/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                 <li><img src="assets/images/red-cross.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                 <li><img src="assets/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                 <li><img src="assets/images/red-cross.webp" class="img-fluid mx-auto d-block" alt="right arrow">
                                 </li>
                                 <li><img src="assets/images/red-cross.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                 <li><img src="assets/images/tick-icon.webp" class="img-fluid mx-auto d-block" alt="right arrow"></li>
                                 <li><img src="assets/images/red-cross.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                                 <li><img src="assets/images/red-cross.webp" class="img-fluid mx-auto d-block" alt="cross"></li>
                              </ul>
                           </div>
                        </div>
                     </div>
                     <div class="mt-md30 mt20 f-16 f-md-16 w400 lh140 "><span class="w700 ">Note :</span> All the features mentioned in the above table are bifurcated in different upgrade options according to the need of individual users and the features that you will get with purchase of WEBPULL Start
                        or Pro Commercial plan are mentioned in the pricing table below on this page.
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- Compare Table Section End -->
      <!-- No Comparsion Section -->
      <div class="nocomparsion-section ">
         <div class="container ">
            <div class="row ">
               <div class="col-12 col-md-12 no-comparison-shape ">
                  <div class="text-capitalize f-md-45 f-28 w400 lh120 black-clr text-center text-md-start ">
                     There Is <span class="w600 ">ABSOLUTELY NO Comparison Of WEBPULL </span>At <br class="d-none d-md-block "> Any Price.
                  </div>
                  <div class="f-20 f-md-20 w400 lh140 mt15 text-center text-md-start ">
                     That’s correct! There’s absolutely no match to the power packed WEBPULL platform. It’s built with years of experience, designing, coding, debugging & real-user-testing to help get you maximum results.<br><br> When it comes to creating
                     super engaging websites for any business NEED, no other software even comes close! WEBPULL gives you the power to literally crush your competition so…<br><br> Grab it today for this low one-time price because
                     <span class="w600 ">it will never be available again after launch at this price.</span>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- No Comparsion Section End -->
      <div class="cta-section-new">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <!-- <div class="f-24 f-md-34 w700 lh140 text-center white-clr animate-charcter2">
                     <img src=" ./assets/images/hurry.webp " alt="Hurry " class="mr10 hurry-img">
                     <span class="red-clr ">Hurry!</span> Limited Time Only. No Monthly Fees
                     </div>
                     <div class="f-20 f-md-24 w400 lh140 text-center mt20 white-clr ">
                     Use Coupon Code <span class="org-clr w600 ">“web10”</span> for an Additional <span class="org-clr w600 ">10% Discount</span>
                     </div> -->
                  <div class="f-22 f-md-28 w700 lh140 text-center white-clr">
                     (Free Commercial License + Low 1-Time Price For Launch Period Only)
                  </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center ">
                        <a href="#buynow " class="cta-link-btn ">Get Instant Access To WEBPULL</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30 ">
                     <img src="assets/images/compaitable-with1.webp " class="img-fluid d-block mx-xs-center md-img-right ">
                     <div class="d-md-block d-none px-md30 "><img src="assets/images/v-line.webp " class="img-fluid "></div>
                     <img src="assets/images/days-gurantee1.webp " class="img-fluid d-block mx-xs-auto mt15 mt-md0 ">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Silver Platter Section Start -->
      <div class="silver-platter-section ">
         <div class="container ">
            <div class="row mx-0 ">
               <div class="col-12">
                  <div class="huge-opportunity-shape ">
                     <div class="f-28 f-md-45 w600 lh140 black-clr text-center ">
                        WEBPULL Brings You<br class="d-none d-md-block ">A HUGE Opportunity On A Silver Platter
                     </div>
                     <div class="f-24 f-md-38 w500 lh140 black-clr text-center mt15 ">
                        You Just Need To Capitalize On It – The Easier Way.No Competition With Others.
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Silver Platter Section End -->
      <!-- Silver Platter Section End -->
      <div class="wearenot-alone-section ">
         <div class="container ">
            <div class="row mx-0 ">
               <div class="col-12 col-md-10 mx-auto">
                  <div class="alone-bg">
                     <div class="row ">
                        <div class="col-md-1 ">
                           <img src="assets/images/dollars-image.webp " class="img-fluid d-block dollars-sm-image " />
                        </div>
                        <div class="col-md-11 f-28 f-md-45 w400 lh140 black-clr text-center mt15 mt-md0 ">You can build a profitable 6-figure Agency by just serving 1 client a day.
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 px10 f-28 f-md-45 w600 lh140 black-clr text-center mt20 mt-md80 ">Here’s a simple math
               </div>
               <div class="col-12 mt20 mt-md50 ">
                  <img src="assets/images/make-icon.webp " class="img-fluid d-block mx-auto " />
               </div>
               <div class="col-12 mt20 mt-md70">
                  <div class="f-md-45 f-28 w600 lh140 text-center white-clr">
                     <span class="red-shape">And It’s Not A 1-Time Income</span>
                  </div>
               </div>
               <div class="col-12 f-20 f-md-26 w400 lh160 mt20 text-center ">
                  You can do this for life and charge your new as well as existing clients for your <br class="d-none d-md-block ">services again and again forever.
               </div>
            </div>
         </div>
      </div>
      <!-- Silver Platter Section End -->
      <!-- MaxFunnels Is EVERYTHING Section end-->
      <div class="potential-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-md-45 f-24 w600 text-center white-clr">
                     Here’s a List of
                  </div>
                  <div class="f-md-50 f-24 w700 lh140 text-center white-clr niche-bg">
                     All Potential Niche
                  </div>
                  <div class="f-md-45 f-24 w600 text-center white-clr">
                     You Can Start Getting Clients &amp; Collecting Checks
                  </div>
               </div>
            </div>
            <div class="row mt-md70 mt30">
               <div class="col-md-3 col-6">
                  <div class="">
                     <img src="assets/images/pn1.webp" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-24 f-16 lh140 w600 white-clr text-center mt20">
                     Architect
                  </div>
               </div>
               <div class="col-md-3 col-6 mt-md0 mt0">
                  <div class="">
                     <img src="assets/images/pn2.webp" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-24 f-16 lh140 w600 white-clr text-center mt20">
                     Financial Adviser
                  </div>
               </div>
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn3.webp" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-24 f-16 lh140 w600 white-clr text-center mt20">
                     Painters &amp; Decorators
                  </div>
               </div>
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn4.webp" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-24 f-16 lh140 w600 white-clr text-center mt20">
                     Counsellors
                  </div>
               </div>
            </div>
            <div class="row mt-md50 mt0">
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn5.webp" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-24 f-16 lh140 w600 white-clr text-center mt20">
                     Divorce Lawyers
                  </div>
               </div>
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn6.webp" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-24 f-16 lh140 w600 white-clr text-center mt20">
                     Carpet Cleaner
                  </div>
               </div>
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn7.webp" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-24 f-16 lh140 w600 white-clr text-center mt20">
                     Hotels
                  </div>
               </div>
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn8.webp" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-24 f-16 lh140 w600 white-clr text-center mt20">
                     Car Rental &amp; Taxi
                  </div>
               </div>
            </div>
            <div class="row mt-md50 mt0">
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn9.webp" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-24 f-16 lh140 w600 white-clr text-center mt20">
                     Florist
                  </div>
               </div>
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn10.webp" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-24 f-16 lh140 w600 white-clr text-center mt20">
                     Attorney
                  </div>
               </div>
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn11.webp" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-24 f-16 lh140 w600 white-clr text-center mt20">
                     Lock Smith
                  </div>
               </div>
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn12.webp" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-24 f-16 lh140 w600 white-clr text-center mt20">
                     Real Estate
                  </div>
               </div>
            </div>
            <div class="row mt-md50 mt0">
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn13.webp" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-24 f-16 lh140 w600 white-clr text-center mt20">
                     Home Security
                  </div>
               </div>
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn14.webp" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-24 f-16 lh140 w600 white-clr text-center mt20">
                     Plumbers
                  </div>
               </div>
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn15.webp" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-24 f-16 lh140 w600 white-clr text-center mt20">
                     Chiropractors
                  </div>
               </div>
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn16.webp" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-24 f-16 lh140 w600 white-clr text-center mt20">
                     Dentists
                  </div>
               </div>
            </div>
            <div class="row mt-md50 mt0">
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn17.webp" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-24 f-16 lh140 w600 white-clr text-center mt20">
                     Home Tutors
                  </div>
               </div>
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn18.webp" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-24 f-16 lh140 w600 white-clr text-center mt20">
                     Dermatologists
                  </div>
               </div>
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn19.webp" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-24 f-16 lh140 w600 white-clr text-center mt20">
                     Veterinarians
                  </div>
               </div>
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn20.webp" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-24 f-16 lh140 w600 white-clr text-center mt20">
                     Motor Garage
                  </div>
               </div>
            </div>
            <div class="row mt-md50 mt0">
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn21.webp" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-24 f-16 lh140 w600 white-clr text-center mt20">
                     Carpenters
                  </div>
               </div>
               <div class="col-md-3 col-6 mt-md0 mt30">
                  <div class="">
                     <img src="assets/images/pn22.webp" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-md-24 f-16 lh140 w600 white-clr text-center mt20">
                     Computer Repair
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- MaxFunnels Is EVERYTHING Section end-->
      <!-- Testimonial Section Start -->
      <div class="testimonial-section">
         <div class="container ">
            <div class="row ">
               <div class="col-12 f-28 f-md-45 w600 lh140 black-clr text-center ">
                  See What Our Early Users & Backdoor <br class="d-none d-md-block"> Customers Are Saying About WEBPULL!
               </div>
            </div>
            <!-- <div class="row mt25 mt-md75 align-items-center ">
               <div class="col-12 col-md-10 pr-md5 f-18 f-md-20 w400 lh140 ">
                   <div class=" testi-block1 ">
                       <span class="w500 ">WEBPULL is something fresh and really exciting.</span> It's so easy to use, and the training included makes it even easier to take complete control of your business without falling prey to money sucking third
                       parties.
                       <br><br> I love the fact that it enables you to drive targeted traffic to your offers without actually being a technical nerd...all without any special skills. Absolutely brilliant guys. Keep up the great work…
                   </div>
               </div>
               <div class="col-md-2 pl-md20 mt20 mt-md0 ">
                   <img src="assets/images/david-kirby.webp " class="img-fluid d-block mx-auto testi-radius " />
                   <div class="f-18 f-md-20 w600 lh140 black-clr text-center mt15 ">David Kirby</div>
               </div>
               </div> -->
            <!-- <div class="row mt30 mt-md70 align-items-center ">
               <div class="col-md-10 f-18 f-md-20 w400 lh140 order-md-2 ">
                   <div class=" testi-block1 ">
                       I don't write testimonials very often but couldn't help but write one after seeing the immense value you're going to get out of WEBPULL. This saves a lot of time and money as it <span class="w500 ">includes everything that one needs to be a part of the ever-growing website building and online service selling industry</span>                        without doing anything myself! Something that you must give a serious look…
                   </div>
               </div>
               <div class="col-md-2 pr-md20 mt20 mt-md0 order-md-1 ">
                   <img src="assets/images/john-zakaria.webp " class="img-fluid d-block mx-auto testi-radius " />
                   <div class="f-18 f-md-20 w600 lh140 black-clr text-center mt15 ">John Zakaria</div>
               </div>
               </div> -->
            <div class="row  mt25 mt-md75 align-items-center">
               <div class="col-md-6 p-md0">
                  <img src="./assets/images/testi-img1.webp" alt="" class="img-fluid d-block mx-auto">
                  <img src="./assets/images/testi-img2.webp" alt="" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-md-6 p-md0">
                  <img src="./assets/images/testi-img3.webp" alt="" class="img-fluid d-block mx-auto">
               </div>
            </div>
         </div>
      </div>
      <!-- Testimonial Section End -->
      <!---Bonus Section Starts-->
      <div class="bonus-section ">
         <div class="container ">
            <div class="row ">
               <div class="col-12 text-center f-28 f-md-45 w700 lh140 black-clr text-center ">
                  Plus You’ll Also Receive These Amazing Bonuses With Your Purchase Today…
               </div>
               <div class="col-12 mt20 mt-md20 f-20 f-md-22 w400 black-clr lh140 text-center ">
                  WEBPULL is already worth 10X the price we’re charging today. But just to sweeten the pot a bit more and over deliver, we’re going to include these special bonuses that will enhance your investment in WEBPULL:
               </div>
               <div class="col-12 ">
                  <div class="row ">
                     <!-------bonus 1------->
                     <div class="col-12 col-md-6 mt-md30 mt20 ">
                        <div class="bonus-section-shape ">
                           <div class="row align-items-center ">
                              <div class="col-md-6 mx-auto">
                                 <img src="assets/images/bonus-box.webp " class="img-fluid d-block mx-auto ">
                              </div>
                              <div class="col-12 mt20 mt-md40 ">
                                 <div class="w600 f-22 f-md-24 black-clr lh140 ">
                                    [LIVE TRAINING] Discover How to Find Pre-Qualify Local Leads Worldwide & Close More Clients for your Website Building Services -(Worth $1497)
                                 </div>
                                 <div class="f-18 f-md-18 w400 black-clr lh140 mt15 ">
                                    Join this Live Training Webinar and discover the secrets behind Finding the Perfect Leads & Close them as long-term clients without wasted hours in trials and errors. <br><br> You’ll be able to go from ZERO
                                    to a Fully Certified Website Agency Expert without any Sales Experience or Complicated Sales Process & Even If you’re a one-man team!
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-------bonus 2------->
                     <div class="col-12 col-md-6 mt-md30 mt20 ">
                        <div class="bonus-section-shape col-12 ">
                           <div class="row align-items-center ">
                              <div class="col-md-6 mx-auto col-12">
                                 <img src="assets/images/bonus-box.webp " class="img-fluid d-block mx-auto ">
                              </div>
                              <div class="col-12 mt20 mt-md40 ">
                                 <div class="w600 f-22 f-md-24 black-clr lh140 ">
                                    ProfitFox - Most Powerful Notification System That Shows Your Perfect Offer to Visitors According to What They Need! – (Worth $297)
                                 </div>
                                 <div class="f-18 f-md-18 w400 black-clr lh140 mt15 ">
                                    This Exclusive WEBPULL Customers only bonus will help you to Boost Conversions on your Websites. With this, you can identify your targeted audience by setting up a simple rule in our software & it will present your targeted offer to targeted visitors
                                    and you just sit back relax & enjoy new leads and sales. Ultimately, you can use the power of engagement and target your audience based on their actions, interests, and behavior.
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt-md30 mt20 ">
                  <div class="row ">
                     <!-------bonus 3------->
                     <div class="col-12 col-md-6 ">
                        <div class="bonus-section-shape col-12 ">
                           <div class="row align-items-center ">
                              <div class="col-md-6 mx-auto">
                                 <img src="assets/images/bonus-box.webp " class="img-fluid d-block mx-auto ">
                              </div>
                              <div class="col-12 mt20 mt-md50">
                                 <div class="w600 f-22 f-md-24 black-clr lh140 ">100 Website Business Models – (Worth $199)
                                 </div>
                                 <div class="f-18 f-md-18 w400 black-clr lh140 mt15 ">
                                    This E-book will give you $100K Website Business Models with Step-By-Step easy instructions. <br><br> This Bonus eases your way to building professional websites for various niches using WebPull’s DFY Themes
                                    and Marketing Templates.
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-------bonus 4------->
                     <div class="col-12 col-md-6 mt20 mt-md0 ">
                        <div class="bonus-section-shape col-12 ">
                           <div class="row align-items-center ">
                              <div class="col-md-6 mx-auto">
                                 <img src="assets/images/bonus-box.webp " class="img-fluid d-block mx-auto ">
                              </div>
                              <div class="col-12 mt20 mt-md40">
                                 <div class="w600 f-22 f-md-24 black-clr lh140 ">
                                    WP Site Launcher Review Pack – (Worth $210)
                                 </div>
                                 <div class="f-18 f-md-18 w400 black-clr lh140 mt15 ">
                                    This plugin is a 1-click plugin that sets up the entire site for you. You just need to select the settings once and then you can use the same settings to build other websites.
                                    <br><br> This Blockbuster Bonus along with WebPull’s Niche-based DFY-Themes will create an entire website for you within a click.
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt30 mt-md70 ">
                  <div class="f-24 f-md-38 w700 black-clr lh140 text-center ">
                     That’s A Total Value of <br class="visible-lg "><span class="f-30 f-md-50">$2200</span>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--Bonus Section ends -->
      <!-- Amazing Deal Section -->
      <div class="amazing-tool-bg ">
         <div class="container ">
            <div class="row ">
               <div class="col-12 text-center ">
                  <div class="f-md-45 f-28 w600 lh140 text-center white-clr">
                     <span class="red-shape">Wow! That Sounds An Amazing Deal....</span>
                  </div>
               </div>
               <div class="col-12 text-center mt20 mt-md20 ">
                  <div class="w400 f-24 f-md-38 black-clr lh140 ">
                     But “I’m Just Getting Started Online. Can WEBPULL <br class="visible-lg ">Help Me Build An Online Business Too?”
                  </div>
               </div>
               <div class="col-12 mt20 mt-md70 ">
                  <div class="row align-items-center ">
                     <div class="col-12 col-md-8 ">
                        <div class="f-20 w400 lh140 text-start ">
                           <span class="w400 f-24 f-md-38 "><u>Absolutely,</u></span><br> You easily can tap into the huge website building service selling industry right from the comfort of your home. Help local businesses to get online during this
                           pandemic and make a sustainable income stream. <br><br>
                           <span class="w500 ">You can literally create a highly engaging business websites your
                           own and start an online business today</span> or for local clients without any tech hassles. Even better, you won’t go bankrupt in the process by hiring third party money sucking service
                           providers. <br><br>
                           <span class="w500 ">But even with, we want to offer you something great today that will
                           <u>open a HUGE Business Opportunity For YOU!</u></span>
                        </div>
                     </div>
                     <div class="col-12 col-md-4 mt20 mt-md0 ">
                        <img src="assets/images/sounds-great-img.webp " class="img-fluid d-block mx-auto ">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Amazing Deal Section end  -->
      <!-- License Section Start -->
      <div class="license-section ">
         <div class="container ">
            <div class="row ">
               <div class="col-12 f-28 f-md-45 w600 lh140 black-clr text-center ">
                  <u>Alert! FREE Limited Time Upgrade:</u><br> Get Commercial License To Use WEBPULL To Create An Incredible Income!
               </div>
               <div class="col-12 mt20 mt-md30 f-md-24 f-20 w400 lh140 black-clr text-center ">
                  As we have shown you, there are tons of businesses enter in the online market every day – yoga gurus, fitness centers or anybody else who want to grow online, desperately need your services & eager to pay you BIG for your services.
               </div>
               <div class="col-12 mt20 mt-md70 ">
                  <div class="row align-items-center ">
                     <div class="col-12 col-md-6">
                        <img src="assets/images/commercial-license-img.webp " class="img-fluid d-block mx-auto ">
                     </div>
                     <div class="col-12 col-md-6 mt20 mt-md0 ">
                        <div class="f-md-20 f-20 w400 lh140 black-clr ">Build their branded and professional websites to them get more exposure, boost their authority and profits online. They will pay top dollar to you when you deliver top notch services to them. We’ve taken care of everything
                           so that you can deliver those services easily.<br><br>
                           <span class="w500 ">Notice: </span>This special commercial license is being included in the Pro plan and ONLY during this launch.<span class="w500 "> Take advantage of it now
                           because it will never be offered again.</span>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Guarantee Section Start -->
      <div class="riskfree-section ">
         <div class="container ">
            <div class="row align-items-center ">
               <div class="f-28 f-md-45 w600 lh140 white-clr text-center col-12 mb-md40">Test Drive WEBPULL Risk FREE For 30 Days
               </div>
               <div class="col-md-7 col-12 order-md-2">
                  <div class="f-md-20 f-18 w400 lh140 white-clr mt15 "><span class="w600 ">Your purchase is secured
                     with our 30-day 100% money back guarantee.</span>
                     <br><br>
                     <span class="w600 ">So, you can buy with full confidence and try it for yourself and for your
                     client’s risk free.</span> If you face any Issue or don’t get results you desired after using it, just raise a ticket on support desk within 30 days and we'll refund you everything, down to the last
                     penny.
                     <br><br> We would like to clearly state that we do NOT offer a “no questions asked” money back guarantee. You must provide a genuine reason when you contact our support and we will refund your hard-earned money.
                  </div>
               </div>
               <div class="col-md-5 order-md-1 col-12 mt20 mt-md0 ">
                  <img src="assets/images/riskfree-img.webp " class="img-fluid d-block mx-auto ">
               </div>
            </div>
         </div>
      </div>
      <!-- Guarantee Section End -->
      <!-- Discounted Price Section Start -->
      <div class="table-section " id="buynow">
         <div class="container ">
            <div class="row ">
               <div class="col-12 ">
                  <div class="f-20 f-md-24 w400 text-center lh140">
                     Take Advantage Of Our Limited Time Deal
                  </div>
                  <div class="f-md-45 f-28 w600 lh140 text-center mt10 ">
                     Get WEBPULL For A Low One-Time-
                     <br class="d-none d-md-block "> Price, No Monthly Fee.
                  </div>
                  <div class="f-20 f-md-20 w400 lh140 text-center mt20 black-clr">
                     (Free Commercial License + Low 1-Time Price For Launch Period Only)
                  </div>
               </div>
               <div class="col-12 col-md-8 mx-auto mt20 mt-md50 ">
                  <div class="row ">
                     <!-- <div class="col-12 col-md-6 ">
                        <div class="table-wrap ">
                            <div class="table-head text-center ">
                                <div class="text-center">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 394.2 100" style="enable-background:new 0 0 394.2 100; max-height:60px" xml:space="preserve">
                                        <style type="text/css">
                                            .st0 {
                                                fill: url(#SVGID_1_);
                                            }
                        
                                            .st1 {
                                                fill: url(#SVGID_00000013884957388662945840000008381380006846104982_);
                                            }
                        
                                            .st2 {
                                                fill: url(#SVGID_00000103238645058858935420000009018919042447175073_);
                                            }
                        
                                            .st3 {
                                                fill: url(#SVGID_00000094589957027515491610000008304791246048428196_);
                                            }
                        
                                            .st4 {
                                                fill: url(#SVGID_00000150062470572363633260000014522019953476311697_);
                                            }
                        
                                            .st5 {
                                                fill: url(#SVGID_00000034809544087968851870000010060308772339878576_);
                                            }
                        
                                            .st6 {
                                                fill: url(#SVGID_00000119114741667118596670000008830570964109727167_);
                                            }
                        
                                            .st7 {
                                                fill: url(#SVGID_00000152963830615206138700000011264877688113397689_);
                                            }
                        
                                            .st8 {
                                                fill: url(#SVGID_00000163073398514070238630000010677532294958404001_);
                                            }
                        
                                            .st9 {
                                                fill: #FFC802;
                                            }
                        
                                            .st10 {
                                                fill: url(#SVGID_00000176017010832893600890000016709534964343606171_);
                                            }
                        
                                            .st11 {
                                                fill: #D032F8;
                                            }
                                        </style>
                                        <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="99.0992"
                                            y1="99.9939" x2="99.0992" y2="37.8565">
                                            <stop offset="0" style="stop-color:#6128F1"></stop>
                                            <stop offset="1" style="stop-color:#D032F8"></stop>
                                        </linearGradient>
                                        <path class="st0"
                                            d="M117.7,37.9l-5,12.4H92.9v12.4h18.6l-5,12.4H92.9v12.4h24.8l-5,12.5H92.9H80.5V75.1V62.7V37.9H117.7z">
                                        </path>
                                        <path d="M323.2,37.9v49.7H348V100h-37.3V37.9H323.2z"></path>
                                        <path d="M369.4,37.9v49.7h24.8V100h-37.3V37.9H369.4z"></path>
                                        <g>
                                            <g>
                        
                                                <linearGradient
                                                    id="SVGID_00000183235582105081774520000012960746077843144857_"
                                                    gradientUnits="userSpaceOnUse" x1="62.4968" y1="71.4156"
                                                    x2="62.4968" y2="37.8504">
                                                    <stop offset="0" style="stop-color:#6128F1"></stop>
                                                    <stop offset="1" style="stop-color:#D032F8"></stop>
                                                </linearGradient>
                                                <polygon
                                                    style="fill:url(#SVGID_00000183235582105081774520000012960746077843144857_);"
                                                    points="59.1,71.4 52.4,54.4 59.1,37.9     72.6,37.9   ">
                                                </polygon>
                        
                                                <linearGradient
                                                    id="SVGID_00000110456286029403644340000013105186831457286312_"
                                                    gradientUnits="userSpaceOnUse" x1="41.9879" y1="100"
                                                    x2="41.9879" y2="37.8504">
                                                    <stop offset="0" style="stop-color:#6128F1"></stop>
                                                    <stop offset="1" style="stop-color:#D032F8"></stop>
                                                </linearGradient>
                                                <polygon
                                                    style="fill:url(#SVGID_00000110456286029403644340000013105186831457286312_);"
                                                    points="29.4,54.7 36.3,37.9 54.5,82.9     47.7,100   ">
                                                </polygon>
                        
                                                <linearGradient
                                                    id="SVGID_00000094614363692769864910000013456613408807472017_"
                                                    gradientUnits="userSpaceOnUse" x1="15.7781" y1="99.9954"
                                                    x2="15.7781" y2="37.8579">
                                                    <stop offset="0" style="stop-color:#6128F1"></stop>
                                                    <stop offset="1" style="stop-color:#D032F8"></stop>
                                                </linearGradient>
                                                <polygon
                                                    style="fill:url(#SVGID_00000094614363692769864910000013456613408807472017_);"
                                                    points="24.8,100 31.6,82.8 13.4,37.9     0,37.9   "></polygon>
                                            </g>
                                        </g>
                                        <path
                                            d="M222.8,44.3c-4.3-4.3-9.4-6.4-15.4-6.4h-9.3h-12.5v21.7v21.7V100h12.5V81.3h0V68.9h0v-9.3v-9.3h9.3c2.6,0,4.8,0.9,6.6,2.8  c1.8,1.8,2.7,4,2.7,6.6c0,2.6-0.9,4.8-2.7,6.6c-1.8,1.8-4,2.7-6.6,2.7h-0.7v12.4h0.7c6,0,11.1-2.1,15.4-6.4  c4.3-4.3,6.4-9.4,6.4-15.4C229.2,53.6,227.1,48.5,222.8,44.3z">
                                        </path>
                                        <linearGradient
                                            id="SVGID_00000182502405471752640740000005390288071205386419_"
                                            gradientUnits="userSpaceOnUse" x1="254.8837" y1="42.6983" x2="259.444"
                                            y2="33.3483">
                                            <stop offset="0" style="stop-color:#6128F1"></stop>
                                            <stop offset="1" style="stop-color:#D032F8"></stop>
                                        </linearGradient>
                                        <polygon
                                            style="fill:url(#SVGID_00000182502405471752640740000005390288071205386419_);"
                                            points="253.7,30.8 249.4,40 260.7,45.3   265,36 "></polygon>
                                        <linearGradient
                                            id="SVGID_00000036220914561205005820000004240122909477284799_"
                                            gradientUnits="userSpaceOnUse" x1="249.3741" y1="97.3262" x2="270.7371"
                                            y2="53.5256">
                                            <stop offset="0" style="stop-color:#FA8460"></stop>
                                            <stop offset="1" style="stop-color:#FCB121"></stop>
                                        </linearGradient>
                                        <path
                                            style="fill:url(#SVGID_00000036220914561205005820000004240122909477284799_);"
                                            d="M271.7,80.3c-1.4,3.1-3.8,5.2-7,6.4  c-3.2,1.2-6.4,1-9.5-0.4c-3.1-1.4-5.2-3.8-6.4-7c-1.2-3.2-1-6.4,0.4-9.5l9.9-21.2l-11.3-5.3l-9.9,21.2c-2.9,6.2-3.2,12.6-0.8,19  c2.3,6.5,6.6,11.1,12.9,14c6.2,2.9,12.5,3.2,19,0.8c6.5-2.3,11.2-6.6,14.1-12.8l9.9-21.2l-11.3-5.3L271.7,80.3z">
                                        </path>
                                        <linearGradient
                                            id="SVGID_00000005955962324536505440000002218348670894678662_"
                                            gradientUnits="userSpaceOnUse" x1="288.6676" y1="58.452" x2="293.2279"
                                            y2="49.102">
                                            <stop offset="0" style="stop-color:#6128F1"></stop>
                                            <stop offset="1" style="stop-color:#D032F8"></stop>
                                        </linearGradient>
                                        <polygon
                                            style="fill:url(#SVGID_00000005955962324536505440000002218348670894678662_);"
                                            points="287.5,46.5 283.2,55.8 294.4,61   298.7,51.8 "></polygon>
                                        <linearGradient
                                            id="SVGID_00000119803471964097349970000017660416210049322673_"
                                            gradientUnits="userSpaceOnUse" x1="150.3956" y1="99.9939" x2="150.3956"
                                            y2="37.8565">
                                            <stop offset="0" style="stop-color:#6128F1"></stop>
                                            <stop offset="1" style="stop-color:#D032F8"></stop>
                                        </linearGradient>
                                        <path
                                            style="fill:url(#SVGID_00000119803471964097349970000017660416210049322673_);"
                                            d="M173.3,76.3c-0.8-1.7-1.7-3.2-2.9-4.5  c-1.2-1.3-2.5-2.3-4-3.1c-1.5-0.8-3-1.2-4.5-1.3c1.5-0.1,2.8-0.5,4.1-1.3c1.2-0.8,2.3-1.8,3.2-3c0.9-1.2,1.6-2.5,2.1-3.9  c0.5-1.4,0.8-2.8,0.8-4c0-3.5-0.5-6.4-1.4-8.6c-1-2.2-2.3-4-3.9-5.3c-1.6-1.3-3.6-2.2-5.8-2.6c-2.2-0.5-4.6-0.7-7-0.7h-27.3v10.5  h11.9v0h15.4c0.8,0,1.7,0.2,2.4,0.6c0.8,0.4,1.4,0.9,2,1.5c0.6,0.6,1,1.4,1.4,2.2c0.3,0.9,0.5,1.7,0.5,2.6c0,0.9-0.2,1.8-0.5,2.8  c-0.3,0.9-0.8,1.8-1.4,2.5c-0.6,0.7-1.3,1.3-2.2,1.8c-0.9,0.5-1.9,0.7-3.1,0.7h-9.1v10.5h10c2.7,0,4.9,0.7,6.4,2.1  c1.6,1.4,2.3,3.2,2.3,5.3c0,1-0.2,2-0.6,3c-0.4,1-0.9,1.9-1.6,2.7c-0.7,0.8-1.5,1.5-2.4,2c-0.9,0.5-1.9,0.7-2.9,0.7h-16.8h0V73.7h0  V63.2h0v-9.3h-11.9V100h9.6h2.3h16.8c2.7,0,5.2-0.3,7.5-0.9c2.3-0.6,4.4-1.6,6.2-3.1c1.8-1.4,3.1-3.3,4.2-5.7c1-2.3,1.5-5.2,1.5-8.6  C174.4,79.8,174,78,173.3,76.3z">
                                        </path>
                                        <g>
                        
                                            <linearGradient
                                                id="SVGID_00000178203317655055216620000000048221405641408397_"
                                                gradientUnits="userSpaceOnUse" x1="270.7616" y1="25.6663"
                                                x2="296.63" y2="25.6663">
                                                <stop offset="0" style="stop-color:#FA8460"></stop>
                                                <stop offset="4.692155e-02" style="stop-color:#FA9150"></stop>
                                                <stop offset="0.1306" style="stop-color:#FBA43A"></stop>
                                                <stop offset="0.2295" style="stop-color:#FBB229"></stop>
                                                <stop offset="0.3519" style="stop-color:#FCBC1E"></stop>
                                                <stop offset="0.5235" style="stop-color:#FCC117"></stop>
                                                <stop offset="1" style="stop-color:#FCC315"></stop>
                                            </linearGradient>
                                            <path
                                                style="fill:url(#SVGID_00000178203317655055216620000000048221405641408397_);"
                                                d="M289.6,14.2c-2.1-1.1-4.3-1.5-6.5-1.4   c-0.1,0-0.1,0-0.2,0c-0.1,0-0.2,0-0.2,0c0,0-0.1,0-0.1,0c-0.1,0-0.2,0-0.2,0c-0.1,0-0.2,0-0.3,0c-0.1,0-0.2,0-0.2,0   c-0.3,0-0.5,0.1-0.8,0.1c-0.1,0-0.2,0-0.3,0.1c-0.1,0-0.2,0-0.3,0.1c-0.1,0-0.3,0.1-0.4,0.1c-0.1,0-0.2,0-0.2,0.1   c-0.1,0-0.2,0.1-0.3,0.1c-0.2,0.1-0.5,0.2-0.7,0.3c0,0,0,0,0,0c-0.3,0.1-0.6,0.3-0.9,0.4c-0.3,0.1-0.6,0.3-0.9,0.5   c-0.1,0.1-0.3,0.2-0.4,0.2c-0.1,0.1-0.2,0.2-0.4,0.2c-0.1,0.1-0.2,0.1-0.2,0.2c0,0-0.1,0-0.1,0.1c-0.1,0.1-0.2,0.1-0.2,0.2   c-0.1,0-0.1,0.1-0.2,0.2c-0.1,0.1-0.2,0.2-0.3,0.3c-0.1,0-0.1,0.1-0.2,0.2c-0.5,0.4-0.9,0.9-1.3,1.4c-0.1,0.1-0.1,0.1-0.2,0.2   c0,0-0.1,0.1-0.1,0.1c-0.1,0.1-0.1,0.2-0.2,0.3c0,0-0.1,0.1-0.1,0.1c-0.1,0.1-0.1,0.2-0.2,0.3c0,0,0,0.1-0.1,0.1   c-0.1,0.1-0.1,0.2-0.2,0.3c0,0,0,0.1-0.1,0.1c-0.1,0.1-0.1,0.2-0.2,0.3c-0.1,0.1-0.2,0.3-0.2,0.4c-3.3,6.3-0.8,14.1,5.6,17.4   c1.9,1,3.9,1.4,5.9,1.4c0.1,0,0.3,0,0.4,0c0.1,0,0.3,0,0.4,0c0.1,0,0.1,0,0.2,0c0.1,0,0.2,0,0.3,0c0,0,0,0,0.1,0c0.1,0,0.2,0,0.3,0   c0.1,0,0.2,0,0.2,0c0.1,0,0.2,0,0.3-0.1c0.1,0,0.3,0,0.4-0.1c0.1,0,0.3-0.1,0.4-0.1c0.1,0,0.2-0.1,0.3-0.1c0.2,0,0.4-0.1,0.5-0.2   c0.1,0,0.3-0.1,0.4-0.1c0.1,0,0.3-0.1,0.4-0.1c0,0,0,0,0,0c0.1,0,0.2-0.1,0.4-0.1c0,0,0,0,0,0c0,0,0.1,0,0.1,0c0,0,0.1,0,0.1-0.1   c0,0,0,0,0.1,0c0,0,0.1,0,0.1,0c0,0,0,0,0.1,0c0.1,0,0.2-0.1,0.3-0.2c0,0,0,0,0.1,0c0.1-0.1,0.2-0.1,0.3-0.2   c0.1-0.1,0.3-0.1,0.4-0.2c0.4-0.2,0.7-0.5,1.1-0.7c0.2-0.2,0.5-0.4,0.7-0.6c0.2-0.2,0.4-0.4,0.7-0.6c0.2-0.2,0.4-0.4,0.6-0.6   c0.2-0.2,0.4-0.5,0.6-0.7c0.1-0.1,0.2-0.2,0.3-0.4c0,0,0,0,0,0c0.1-0.1,0.2-0.2,0.3-0.4c0.1-0.1,0.2-0.3,0.2-0.4   c0.1-0.1,0.1-0.2,0.2-0.3c0-0.1,0.1-0.1,0.1-0.2c0.1-0.1,0.1-0.2,0.2-0.3C298.5,25.3,296,17.5,289.6,14.2z M279.6,26.7L279.6,26.7   l0.3,0.6c0.4,0.7,1,1.3,1.8,1.7c0.3,0.2,0.7,0.3,1,0.3c0.3,0,0.5-0.1,0.7-0.2c0.1,0,0.1-0.1,0.2-0.2c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0-0.1c0.2-0.3,0.2-0.7-0.1-1.3c0,0,0,0,0,0   c-0.1-0.2-0.2-0.4-0.4-0.6c-0.1-0.1-0.2-0.2-0.3-0.3c-0.4-0.4-0.7-0.8-0.9-1.2c-0.1-0.1-0.2-0.2-0.2-0.4c0,0,0,0,0-0.1   c0,0,0-0.1-0.1-0.1c0,0,0-0.1-0.1-0.1c-0.1-0.2-0.1-0.3-0.2-0.5c-0.1-0.2-0.1-0.4-0.2-0.6c0,0,0-0.1,0-0.1c0-0.1,0-0.2,0-0.3v-0.1   c0-0.1,0-0.1,0-0.2c0,0,0-0.1,0-0.1c0-0.1,0-0.2,0-0.3c0.1-0.3,0.2-0.6,0.3-0.8c0,0,0,0,0-0.1c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0-0.1c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0l0,0c0.4-0.5,0.9-0.9,1.5-1.1c0.7-0.2,1.4-0.3,2.1-0.1l0.9-1.7l1.9,1l-0.8,1.6c0.7,0.4,1.2,1,1.6,1.6   l0.2,0.4l-1.8,1.6l-0.3-0.5c-0.1-0.1-0.3-0.4-0.5-0.7c-0.1-0.1-0.3-0.3-0.5-0.4c0,0-0.1-0.1-0.1-0.1c-0.1-0.1-0.3-0.2-0.4-0.2   c0,0-0.1,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0   c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0h0c0,0,0,0-0.1,0h0   c0,0,0,0-0.1,0h0c0,0,0,0,0,0h0c0,0,0,0,0,0h0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c-0.2,0.1-0.4,0.3-0.5,0.5c-0.3,0.5-0.2,0.9,1,2.3c0.8,0.9,1.2,1.6,1.4,2.4   c0.2,0.8,0.1,1.6-0.3,2.5c-0.8,1.5-2.4,2.2-4.2,1.8l-0.8,1.6l-0.1,0.2l-2-1l0.9-1.7c-0.1-0.1-0.2-0.1-0.3-0.2   c-0.3-0.2-0.5-0.4-0.8-0.6c0,0-0.1-0.1-0.1-0.1c-0.3-0.3-0.5-0.6-0.7-1l-0.2-0.4L279.6,26.7z">
                                            </path>
                                            <path class="st9"
                                                d="M278,37c-6.3-3.3-8.8-11.1-5.6-17.4c1.3-2.4,3.2-4.3,5.4-5.5c-2.4,1.2-4.4,3.1-5.7,5.6   c-3.3,6.3-0.8,14.1,5.6,17.4c3.9,2,8.4,1.9,12-0.1C286.2,38.9,281.8,39,278,37z">
                                            </path>
                                        </g>
                                        <g>
                        
                                            <linearGradient
                                                id="SVGID_00000083068332637069705170000006733384460919624328_"
                                                gradientUnits="userSpaceOnUse" x1="79.734" y1="-130.471"
                                                x2="91.3973" y2="-130.471"
                                                gradientTransform="matrix(0.8624 -0.5063 0.5063 0.8624 260.6894 161.6657)">
                                                <stop offset="0" style="stop-color:#FA8460"></stop>
                                                <stop offset="4.692155e-02" style="stop-color:#FA9150"></stop>
                                                <stop offset="0.1306" style="stop-color:#FBA43A"></stop>
                                                <stop offset="0.2295" style="stop-color:#FBB229"></stop>
                                                <stop offset="0.3519" style="stop-color:#FCBC1E"></stop>
                                                <stop offset="0.5235" style="stop-color:#FCC117"></stop>
                                                <stop offset="1" style="stop-color:#FCC315"></stop>
                                            </linearGradient>
                                            <path
                                                style="fill:url(#SVGID_00000083068332637069705170000006733384460919624328_);"
                                                d="M268.1,0c-1,0.1-2,0.4-2.8,0.9   c0,0-0.1,0-0.1,0c0,0-0.1,0-0.1,0.1c0,0,0,0,0,0c0,0-0.1,0-0.1,0.1c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0-0.1,0.1   c-0.1,0.1-0.2,0.1-0.3,0.2c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0.1-0.1,0.1   c-0.1,0.1-0.1,0.2-0.2,0.3c0,0,0,0,0,0c-0.1,0.1-0.2,0.2-0.2,0.4c-0.1,0.1-0.2,0.3-0.2,0.4c0,0.1-0.1,0.1-0.1,0.2   c0,0.1-0.1,0.1-0.1,0.2c0,0,0,0.1-0.1,0.1c0,0,0,0,0,0.1c0,0,0,0.1-0.1,0.1c0,0,0,0.1,0,0.1c0,0.1,0,0.1-0.1,0.2c0,0,0,0.1,0,0.1   c-0.1,0.3-0.1,0.5-0.2,0.8c0,0,0,0.1,0,0.1c0,0,0,0.1,0,0.1c0,0.1,0,0.1,0,0.2c0,0,0,0,0,0.1c0,0.1,0,0.1,0,0.2c0,0,0,0,0,0.1   c0,0.1,0,0.1,0,0.2c0,0,0,0,0,0.1c0,0.1,0,0.1,0,0.2c0,0.1,0,0.1,0,0.2c0.2,3.2,2.9,5.7,6.1,5.5c1-0.1,1.9-0.3,2.6-0.8   c0.1,0,0.1-0.1,0.2-0.1c0.1,0,0.1-0.1,0.1-0.1c0,0,0,0,0.1,0c0,0,0.1,0,0.1-0.1c0,0,0,0,0,0c0,0,0.1-0.1,0.1-0.1c0,0,0.1,0,0.1-0.1   c0,0,0.1-0.1,0.1-0.1c0,0,0.1-0.1,0.1-0.1c0,0,0.1-0.1,0.1-0.1c0,0,0.1-0.1,0.1-0.1c0.1-0.1,0.1-0.1,0.2-0.2c0,0,0.1-0.1,0.1-0.1   c0,0,0.1-0.1,0.1-0.1c0,0,0,0,0,0c0,0,0.1-0.1,0.1-0.1c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0-0.1c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0.1-0.1,0.1-0.1c0,0,0,0,0,0c0,0,0.1-0.1,0.1-0.1c0-0.1,0.1-0.1,0.1-0.2c0.1-0.2,0.2-0.4,0.3-0.5c0.1-0.1,0.1-0.2,0.1-0.4   c0-0.1,0.1-0.3,0.1-0.4c0-0.1,0.1-0.3,0.1-0.4c0-0.1,0-0.3,0.1-0.4c0-0.1,0-0.1,0-0.2c0,0,0,0,0,0c0-0.1,0-0.1,0-0.2   c0-0.1,0-0.1,0-0.2c0-0.1,0-0.1,0-0.2c0,0,0-0.1,0-0.1c0-0.1,0-0.1,0-0.2C274.1,2.3,271.3-0.2,268.1,0z M267.1,7.2L267.1,7.2   l0.3,0.2c0.3,0.2,0.7,0.3,1.1,0.3c0.2,0,0.3-0.1,0.4-0.1c0.1-0.1,0.2-0.1,0.2-0.3c0,0,0-0.1,0-0.1c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0-0.2-0.1-0.3-0.3-0.5c0,0,0,0,0,0   c-0.1,0-0.2-0.1-0.3-0.1c-0.1,0-0.1,0-0.2-0.1c-0.2-0.1-0.4-0.2-0.6-0.2c-0.1,0-0.1-0.1-0.2-0.1c0,0,0,0,0,0c0,0,0,0-0.1,0   c0,0,0,0-0.1,0c-0.1,0-0.1-0.1-0.2-0.1c-0.1-0.1-0.1-0.1-0.2-0.2c0,0,0,0,0,0c0,0-0.1-0.1-0.1-0.1l0,0c0,0,0,0,0-0.1c0,0,0,0,0,0   c0,0,0-0.1-0.1-0.1c0-0.1-0.1-0.3-0.1-0.4c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0l0,0   c0-0.3,0.2-0.5,0.3-0.8c0.2-0.2,0.5-0.4,0.8-0.5l0-0.9l1-0.1l0,0.8c0.4,0,0.7,0.1,1,0.3l0.2,0.1l-0.3,1l-0.3-0.1   c-0.1,0-0.2-0.1-0.4-0.1c-0.1,0-0.2,0-0.3-0.1c0,0-0.1,0-0.1,0c-0.1,0-0.1,0-0.2,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0l0,0c0,0,0,0,0,0l0,0c0,0,0,0,0,0l0,0c0,0,0,0,0,0l0,0c0,0,0,0,0,0l0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c-0.1,0.1-0.1,0.2-0.1,0.3c0,0.2,0.1,0.4,0.9,0.7c0.5,0.2,0.8,0.4,1.1,0.6c0.3,0.3,0.4,0.6,0.4,1c0,0.8-0.4,1.4-1.2,1.7l0,0.8   l0,0.1l-1,0.1l0-0.8c-0.1,0-0.1,0-0.2,0c-0.2,0-0.3,0-0.4-0.1c0,0-0.1,0-0.1,0c-0.2-0.1-0.4-0.1-0.5-0.2l-0.2-0.1L267.1,7.2z">
                                            </path>
                                            <path class="st9"
                                                d="M268.8,11.5c-3.2,0.2-6-2.3-6.1-5.5c-0.1-1.2,0.3-2.4,0.9-3.4c-0.7,1-1,2.2-0.9,3.5c0.2,3.2,2.9,5.7,6.1,5.5   c2-0.1,3.7-1.2,4.6-2.8C272.4,10.4,270.7,11.4,268.8,11.5z">
                                            </path>
                                        </g>
                                        <polygon class="st11"
                                            points="297.8,43.3 296.4,42.7 294.6,46.7 295.4,41.2 296.8,41.8 298.7,37.9 ">
                                        </polygon>
                                        <polygon class="st11"
                                            points="264.2,27.2 262.9,26.4 260.7,30.2 262,24.8 263.3,25.6 265.5,21.8 ">
                                        </polygon>
                                    </svg>
                                </div>
                                <div class="f-22 f-md-32 w600 lh120 text-center text-uppercase mt15 ">Personal</div>
                            </div>
                            <div class=" ">
                                <ul class="table-list pl0 f-18 lh140 w400 black-clr ">
                                    <li>Build <span class="w500 ">Up-to 10 Sites</span></li>
                                    <li><span class="w500 ">Unlimited</span> Visitors</li>
                                    <li><span class="w500 ">Unlimited </span>Products, Services or Posts</li>
                                    <li>Create <span class="w500 ">Stunning Sites For Any Business Need.<span></li>
                                    <li class="w500 ">Sell Products, Services Or Promote Affiliates Offers </li>
                                    <li><span class="w500 ">Over 200 Eye-Catching Templates</span> Organized In 30+ Themes
                                    </li>
                                    <li><span class="w500 ">Built On World's No. 1, WP FRAMEWORK</span> For BUSINESSES
                                    </li>
                                    <li class="w500 ">Fully SEO-Optimized Sites</li>
                                    <li><span class="w500 ">Create 100% All Device ready</span> and ultra-fast loading websites.</li>
                                    <li><span class="w500 ">Get More Viral Traffic</span> with In-Built Social Media Tools
                                    </li>
                                    <li class="w500 ">Seamless WooCommerce Integration</li>
                                    <li>Change Website Width & Layout Without Touching Any Code</li>
                                    <li>Fully Customizable Typography</li>
                                    <li>Get 20+ Beautiful Sliders To Engage & Wow Visitors</li>
                                    <li>100% Secured With Top Notch Encryption</li>
                                    <li>Support Hooks, Custom CSS For Developers </li>
                                    <li>Comes With 10+ Ready To Use Short Codes & Widgets</li>
                                    <li>CTA management, Google Maps & Feature Rich slider</li>
                                    <li>Manage your websites from your mobile devices</li>
                                    <li>Inbuilt Lead Management Feature</li>
                                    <li>Analytics & Remarketing Ready Websites</li>
                                    <li>FREE Updates –When You Sign Up Today. </li>
                                    <li>100% Newbie Friendly</li>
                                    <li>Round-The-Clock Chat Support</li>
                                    <li class="cross-sign">Commercial License included</li>
                                    <li class="cross-sign">Use For Your Clients</li>
                                    <li class="cross-sign">Provide High In Demand Services</li>
                                    <li class="headline">BONUSES WORTH $2200 FREE!!!</li>
                                    <li class="cross-sign">Fast Action Bonus 1 - [LIVE TRAINING] Find Pre-Qualify Local Leads Worldwide & Close More Clients (Worth $1497)</li>
                                    <li class="cross-sign">Fast Action Bonus 2 - ProfitFox - Most Powerful Notification System (Worth $297)</li>
                                    <li class="cross-sign">Fast Action Bonus 3 - 100 Website Business Models (Worth $199)
                                    </li>
                                    <li class="cross-sign">Fast Action Bonus 4 - WP Site Launcher Review Pack (Worth $210) </li>
                                </ul>
                            </div>
                            <div class="table-btn ">
                                <div class="hideme-button ">
                        
                                    <a href="https://www.jvzoo.com/b/105757/379813/2"><img src="https://i.jvzoo.com/105757/379813/2" alt="WEBPULL Personal" class="img-fluid d-block mx-auto " border="0 " /></a>
                                </div>
                            </div>
                        </div>
                        </div> -->
                     <div class="col-12 col-md-8 mx-auto mt20 mt-md0">
                        <div class="table-wrap1 ">
                           <div class="table-head1 ">
                              <div class="text-center">
                                 <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 394.2 100" style="enable-background:new 0 0 394.2 100; max-height:60px" xml:space="preserve">
                                    <style type="text/css">
                                       .st00 {
                                       fill: #FFFFFF;
                                       }
                                       .st11 {
                                       fill: url(#SVGID_1_);
                                       }
                                       .st22 {
                                       fill: url(#SVGID_00000021093791325859232720000015953972570224053676_);
                                       }
                                       .st33 {
                                       fill: #FFC802;
                                       }
                                       .st44 {
                                       fill: url(#SVGID_00000003068462962921929030000010945183465240898967_);
                                       }
                                    </style>
                                    <path class="st00"
                                       d="M117.7,37.9l-5,12.4H92.9v12.4h18.6l-5,12.4H92.9v12.4h24.8l-5,12.5H92.9H80.5V75.1V62.7V37.9H117.7z">
                                    </path>
                                    <path class="st00" d="M323.2,37.9v49.7H348V100h-37.3V37.9H323.2z"></path>
                                    <path class="st00" d="M369.4,37.9v49.7h24.8V100h-37.3V37.9H369.4z"></path>
                                    <g>
                                       <g>
                                          <polygon class="st00"
                                             points="59.1,71.4 52.4,54.4 59.1,37.9 72.6,37.9   "></polygon>
                                          <polygon class="st00"
                                             points="29.4,54.7 36.3,37.9 54.5,82.9 47.7,100   "></polygon>
                                          <polygon class="st00"
                                             points="24.8,100 31.6,82.8 13.4,37.9 0,37.9   "></polygon>
                                       </g>
                                    </g>
                                    <path class="st00"
                                       d="M222.8,44.3c-4.3-4.3-9.4-6.4-15.4-6.4h-9.3h-12.5v21.7v21.7V100h12.5V81.3h0V68.9h0v-9.3v-9.3h9.3  c2.6,0,4.8,0.9,6.6,2.8c1.8,1.8,2.7,4,2.7,6.6c0,2.6-0.9,4.8-2.7,6.6c-1.8,1.8-4,2.7-6.6,2.7h-0.7v12.4h0.7c6,0,11.1-2.1,15.4-6.4  c4.3-4.3,6.4-9.4,6.4-15.4C229.2,53.6,227.1,48.5,222.8,44.3z">
                                    </path>
                                    <rect x="252.1" y="31.8"
                                       transform="matrix(0.4226 -0.9063 0.9063 0.4226 114.0209 255.0236)"
                                       class="st00" width="10.2" height="12.5"></rect>
                                    <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="249.3741"
                                       y1="97.3262" x2="270.7371" y2="53.5256">
                                       <stop offset="0" style="stop-color:#FA8460"></stop>
                                       <stop offset="1" style="stop-color:#FCB121"></stop>
                                    </linearGradient>
                                    <path class="st11"
                                       d="M271.7,80.3c-1.4,3.1-3.8,5.2-7,6.4c-3.2,1.2-6.4,1-9.5-0.4c-3.1-1.4-5.2-3.8-6.4-7c-1.2-3.2-1-6.4,0.4-9.5  l9.9-21.2l-11.3-5.3l-9.9,21.2c-2.9,6.2-3.2,12.6-0.8,19c2.3,6.5,6.6,11.1,12.9,14c6.2,2.9,12.5,3.2,19,0.8  c6.5-2.3,11.2-6.6,14.1-12.8l9.9-21.2l-11.3-5.3L271.7,80.3z">
                                    </path>
                                    <rect x="285.9" y="47.6"
                                       transform="matrix(0.4226 -0.9063 0.9063 0.4226 119.2494 294.738)"
                                       class="st00" width="10.2" height="12.5"></rect>
                                    <path class="st00"
                                       d="M173.3,76.3c-0.8-1.7-1.7-3.2-2.9-4.5c-1.2-1.3-2.5-2.3-4-3.1c-1.5-0.8-3-1.2-4.5-1.3c1.5-0.1,2.8-0.5,4.1-1.3  c1.2-0.8,2.3-1.8,3.2-3c0.9-1.2,1.6-2.5,2.1-3.9c0.5-1.4,0.8-2.8,0.8-4c0-3.5-0.5-6.4-1.4-8.6c-1-2.2-2.3-4-3.9-5.3  c-1.6-1.3-3.6-2.2-5.8-2.6c-2.2-0.5-4.6-0.7-7-0.7h-27.3v10.5h11.9v0h15.4c0.8,0,1.7,0.2,2.4,0.6c0.8,0.4,1.4,0.9,2,1.5  c0.6,0.6,1,1.4,1.4,2.2c0.3,0.9,0.5,1.7,0.5,2.6c0,0.9-0.2,1.8-0.5,2.8c-0.3,0.9-0.8,1.8-1.4,2.5c-0.6,0.7-1.3,1.3-2.2,1.8  c-0.9,0.5-1.9,0.7-3.1,0.7h-9.1v10.5h10c2.7,0,4.9,0.7,6.4,2.1c1.6,1.4,2.3,3.2,2.3,5.3c0,1-0.2,2-0.6,3c-0.4,1-0.9,1.9-1.6,2.7  c-0.7,0.8-1.5,1.5-2.4,2c-0.9,0.5-1.9,0.7-2.9,0.7h-16.8h0V73.7h0V63.2h0v-9.3h-11.9V100h9.6h2.3h16.8c2.7,0,5.2-0.3,7.5-0.9  c2.3-0.6,4.4-1.6,6.2-3.1c1.8-1.4,3.1-3.3,4.2-5.7c1-2.3,1.5-5.2,1.5-8.6C174.4,79.8,174,78,173.3,76.3z">
                                    </path>
                                    <g>
                                       <linearGradient
                                          id="SVGID_00000142155450205615942230000010409392281687221429_"
                                          gradientUnits="userSpaceOnUse" x1="270.7616" y1="25.6663"
                                          x2="296.63" y2="25.6663">
                                          <stop offset="0" style="stop-color:#FA8460"></stop>
                                          <stop offset="4.692155e-02" style="stop-color:#FA9150"></stop>
                                          <stop offset="0.1306" style="stop-color:#FBA43A"></stop>
                                          <stop offset="0.2295" style="stop-color:#FBB229"></stop>
                                          <stop offset="0.3519" style="stop-color:#FCBC1E"></stop>
                                          <stop offset="0.5235" style="stop-color:#FCC117"></stop>
                                          <stop offset="1" style="stop-color:#FCC315"></stop>
                                       </linearGradient>
                                       <path
                                          style="fill:url(#SVGID_00000142155450205615942230000010409392281687221429_);"
                                          d="M289.6,14.2c-2.1-1.1-4.3-1.5-6.5-1.4   c-0.1,0-0.1,0-0.2,0c-0.1,0-0.2,0-0.2,0c0,0-0.1,0-0.1,0c-0.1,0-0.2,0-0.2,0c-0.1,0-0.2,0-0.3,0c-0.1,0-0.2,0-0.2,0   c-0.3,0-0.5,0.1-0.8,0.1c-0.1,0-0.2,0-0.3,0.1c-0.1,0-0.2,0-0.3,0.1c-0.1,0-0.3,0.1-0.4,0.1c-0.1,0-0.2,0-0.2,0.1   c-0.1,0-0.2,0.1-0.3,0.1c-0.2,0.1-0.5,0.2-0.7,0.3c0,0,0,0,0,0c-0.3,0.1-0.6,0.3-0.9,0.4c-0.3,0.1-0.6,0.3-0.9,0.5   c-0.1,0.1-0.3,0.2-0.4,0.2c-0.1,0.1-0.2,0.2-0.4,0.2c-0.1,0.1-0.2,0.1-0.2,0.2c0,0-0.1,0-0.1,0.1c-0.1,0.1-0.2,0.1-0.2,0.2   c-0.1,0-0.1,0.1-0.2,0.2c-0.1,0.1-0.2,0.2-0.3,0.3c-0.1,0-0.1,0.1-0.2,0.2c-0.5,0.4-0.9,0.9-1.3,1.4c-0.1,0.1-0.1,0.1-0.2,0.2   c0,0-0.1,0.1-0.1,0.1c-0.1,0.1-0.1,0.2-0.2,0.3c0,0-0.1,0.1-0.1,0.1c-0.1,0.1-0.1,0.2-0.2,0.3c0,0,0,0.1-0.1,0.1   c-0.1,0.1-0.1,0.2-0.2,0.3c0,0,0,0.1-0.1,0.1c-0.1,0.1-0.1,0.2-0.2,0.3c-0.1,0.1-0.2,0.3-0.2,0.4c-3.3,6.3-0.8,14.1,5.6,17.4   c1.9,1,3.9,1.4,5.9,1.4c0.1,0,0.3,0,0.4,0c0.1,0,0.3,0,0.4,0c0.1,0,0.1,0,0.2,0c0.1,0,0.2,0,0.3,0c0,0,0,0,0.1,0c0.1,0,0.2,0,0.3,0   c0.1,0,0.2,0,0.2,0c0.1,0,0.2,0,0.3-0.1c0.1,0,0.3,0,0.4-0.1c0.1,0,0.3-0.1,0.4-0.1c0.1,0,0.2-0.1,0.3-0.1c0.2,0,0.4-0.1,0.5-0.2   c0.1,0,0.3-0.1,0.4-0.1c0.1,0,0.3-0.1,0.4-0.1c0,0,0,0,0,0c0.1,0,0.2-0.1,0.4-0.1c0,0,0,0,0,0c0,0,0.1,0,0.1,0c0,0,0.1,0,0.1-0.1   c0,0,0,0,0.1,0c0,0,0.1,0,0.1,0c0,0,0,0,0.1,0c0.1,0,0.2-0.1,0.3-0.2c0,0,0,0,0.1,0c0.1-0.1,0.2-0.1,0.3-0.2   c0.1-0.1,0.3-0.1,0.4-0.2c0.4-0.2,0.7-0.5,1.1-0.7c0.2-0.2,0.5-0.4,0.7-0.6c0.2-0.2,0.4-0.4,0.7-0.6c0.2-0.2,0.4-0.4,0.6-0.6   c0.2-0.2,0.4-0.5,0.6-0.7c0.1-0.1,0.2-0.2,0.3-0.4c0,0,0,0,0,0c0.1-0.1,0.2-0.2,0.3-0.4c0.1-0.1,0.2-0.3,0.2-0.4   c0.1-0.1,0.1-0.2,0.2-0.3c0-0.1,0.1-0.1,0.1-0.2c0.1-0.1,0.1-0.2,0.2-0.3C298.5,25.3,296,17.5,289.6,14.2z M279.6,26.7L279.6,26.7   l0.3,0.6c0.4,0.7,1,1.3,1.8,1.7c0.3,0.2,0.7,0.3,1,0.3c0.3,0,0.5-0.1,0.7-0.2c0.1,0,0.1-0.1,0.2-0.2c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0-0.1c0.2-0.3,0.2-0.7-0.1-1.3c0,0,0,0,0,0   c-0.1-0.2-0.2-0.4-0.4-0.6c-0.1-0.1-0.2-0.2-0.3-0.3c-0.4-0.4-0.7-0.8-0.9-1.2c-0.1-0.1-0.2-0.2-0.2-0.4c0,0,0,0,0-0.1   c0,0,0-0.1-0.1-0.1c0,0,0-0.1-0.1-0.1c-0.1-0.2-0.1-0.3-0.2-0.5c-0.1-0.2-0.1-0.4-0.2-0.6c0,0,0-0.1,0-0.1c0-0.1,0-0.2,0-0.3v-0.1   c0-0.1,0-0.1,0-0.2c0,0,0-0.1,0-0.1c0-0.1,0-0.2,0-0.3c0.1-0.3,0.2-0.6,0.3-0.8c0,0,0,0,0-0.1c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0-0.1c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0l0,0c0.4-0.5,0.9-0.9,1.5-1.1c0.7-0.2,1.4-0.3,2.1-0.1l0.9-1.7l1.9,1l-0.8,1.6c0.7,0.4,1.2,1,1.6,1.6   l0.2,0.4l-1.8,1.6l-0.3-0.5c-0.1-0.1-0.3-0.4-0.5-0.7c-0.1-0.1-0.3-0.3-0.5-0.4c0,0-0.1-0.1-0.1-0.1c-0.1-0.1-0.3-0.2-0.4-0.2   c0,0-0.1,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0   c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0h0c0,0,0,0-0.1,0h0   c0,0,0,0-0.1,0h0c0,0,0,0,0,0h0c0,0,0,0,0,0h0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c-0.2,0.1-0.4,0.3-0.5,0.5c-0.3,0.5-0.2,0.9,1,2.3c0.8,0.9,1.2,1.6,1.4,2.4   c0.2,0.8,0.1,1.6-0.3,2.5c-0.8,1.5-2.4,2.2-4.2,1.8l-0.8,1.6l-0.1,0.2l-2-1l0.9-1.7c-0.1-0.1-0.2-0.1-0.3-0.2   c-0.3-0.2-0.5-0.4-0.8-0.6c0,0-0.1-0.1-0.1-0.1c-0.3-0.3-0.5-0.6-0.7-1l-0.2-0.4L279.6,26.7z">
                                       </path>
                                       <path class="st33"
                                          d="M278,37c-6.3-3.3-8.8-11.1-5.6-17.4c1.3-2.4,3.2-4.3,5.4-5.5c-2.4,1.2-4.4,3.1-5.7,5.6   c-3.3,6.3-0.8,14.1,5.6,17.4c3.9,2,8.4,1.9,12-0.1C286.2,38.9,281.8,39,278,37z">
                                       </path>
                                    </g>
                                    <g>
                                       <linearGradient
                                          id="SVGID_00000016062571573767098340000005260099975199807150_"
                                          gradientUnits="userSpaceOnUse" x1="79.734" y1="-130.471"
                                          x2="91.3973" y2="-130.471"
                                          gradientTransform="matrix(0.8624 -0.5063 0.5063 0.8624 260.6894 161.6657)">
                                          <stop offset="0" style="stop-color:#FA8460"></stop>
                                          <stop offset="4.692155e-02" style="stop-color:#FA9150"></stop>
                                          <stop offset="0.1306" style="stop-color:#FBA43A"></stop>
                                          <stop offset="0.2295" style="stop-color:#FBB229"></stop>
                                          <stop offset="0.3519" style="stop-color:#FCBC1E"></stop>
                                          <stop offset="0.5235" style="stop-color:#FCC117"></stop>
                                          <stop offset="1" style="stop-color:#FCC315"></stop>
                                       </linearGradient>
                                       <path
                                          style="fill:url(#SVGID_00000016062571573767098340000005260099975199807150_);"
                                          d="M268.1,0c-1,0.1-2,0.4-2.8,0.9   c0,0-0.1,0-0.1,0c0,0-0.1,0-0.1,0.1c0,0,0,0,0,0c0,0-0.1,0-0.1,0.1c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0-0.1,0.1   c-0.1,0.1-0.2,0.1-0.3,0.2c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0.1-0.1,0.1   c-0.1,0.1-0.1,0.2-0.2,0.3c0,0,0,0,0,0c-0.1,0.1-0.2,0.2-0.2,0.4c-0.1,0.1-0.2,0.3-0.2,0.4c0,0.1-0.1,0.1-0.1,0.2   c0,0.1-0.1,0.1-0.1,0.2c0,0,0,0.1-0.1,0.1c0,0,0,0,0,0.1c0,0,0,0.1-0.1,0.1c0,0,0,0.1,0,0.1c0,0.1,0,0.1-0.1,0.2c0,0,0,0.1,0,0.1   c-0.1,0.3-0.1,0.5-0.2,0.8c0,0,0,0.1,0,0.1c0,0,0,0.1,0,0.1c0,0.1,0,0.1,0,0.2c0,0,0,0,0,0.1c0,0.1,0,0.1,0,0.2c0,0,0,0,0,0.1   c0,0.1,0,0.1,0,0.2c0,0,0,0,0,0.1c0,0.1,0,0.1,0,0.2c0,0.1,0,0.1,0,0.2c0.2,3.2,2.9,5.7,6.1,5.5c1-0.1,1.9-0.3,2.6-0.8   c0.1,0,0.1-0.1,0.2-0.1c0.1,0,0.1-0.1,0.1-0.1c0,0,0,0,0.1,0c0,0,0.1,0,0.1-0.1c0,0,0,0,0,0c0,0,0.1-0.1,0.1-0.1c0,0,0.1,0,0.1-0.1   c0,0,0.1-0.1,0.1-0.1c0,0,0.1-0.1,0.1-0.1c0,0,0.1-0.1,0.1-0.1c0,0,0.1-0.1,0.1-0.1c0.1-0.1,0.1-0.1,0.2-0.2c0,0,0.1-0.1,0.1-0.1   c0,0,0.1-0.1,0.1-0.1c0,0,0,0,0,0c0,0,0.1-0.1,0.1-0.1c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0-0.1c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0.1-0.1,0.1-0.1c0,0,0,0,0,0c0,0,0.1-0.1,0.1-0.1c0-0.1,0.1-0.1,0.1-0.2c0.1-0.2,0.2-0.4,0.3-0.5c0.1-0.1,0.1-0.2,0.1-0.4   c0-0.1,0.1-0.3,0.1-0.4c0-0.1,0.1-0.3,0.1-0.4c0-0.1,0-0.3,0.1-0.4c0-0.1,0-0.1,0-0.2c0,0,0,0,0,0c0-0.1,0-0.1,0-0.2   c0-0.1,0-0.1,0-0.2c0-0.1,0-0.1,0-0.2c0,0,0-0.1,0-0.1c0-0.1,0-0.1,0-0.2C274.1,2.3,271.3-0.2,268.1,0z M267.1,7.2L267.1,7.2   l0.3,0.2c0.3,0.2,0.7,0.3,1.1,0.3c0.2,0,0.3-0.1,0.4-0.1c0.1-0.1,0.2-0.1,0.2-0.3c0,0,0-0.1,0-0.1c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0-0.2-0.1-0.3-0.3-0.5c0,0,0,0,0,0   c-0.1,0-0.2-0.1-0.3-0.1c-0.1,0-0.1,0-0.2-0.1c-0.2-0.1-0.4-0.2-0.6-0.2c-0.1,0-0.1-0.1-0.2-0.1c0,0,0,0,0,0c0,0,0,0-0.1,0   c0,0,0,0-0.1,0c-0.1,0-0.1-0.1-0.2-0.1c-0.1-0.1-0.1-0.1-0.2-0.2c0,0,0,0,0,0c0,0-0.1-0.1-0.1-0.1l0,0c0,0,0,0,0-0.1c0,0,0,0,0,0   c0,0,0-0.1-0.1-0.1c0-0.1-0.1-0.3-0.1-0.4c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0l0,0   c0-0.3,0.2-0.5,0.3-0.8c0.2-0.2,0.5-0.4,0.8-0.5l0-0.9l1-0.1l0,0.8c0.4,0,0.7,0.1,1,0.3l0.2,0.1l-0.3,1l-0.3-0.1   c-0.1,0-0.2-0.1-0.4-0.1c-0.1,0-0.2,0-0.3-0.1c0,0-0.1,0-0.1,0c-0.1,0-0.1,0-0.2,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0l0,0c0,0,0,0,0,0l0,0c0,0,0,0,0,0l0,0c0,0,0,0,0,0l0,0c0,0,0,0,0,0l0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c-0.1,0.1-0.1,0.2-0.1,0.3c0,0.2,0.1,0.4,0.9,0.7c0.5,0.2,0.8,0.4,1.1,0.6c0.3,0.3,0.4,0.6,0.4,1c0,0.8-0.4,1.4-1.2,1.7l0,0.8   l0,0.1l-1,0.1l0-0.8c-0.1,0-0.1,0-0.2,0c-0.2,0-0.3,0-0.4-0.1c0,0-0.1,0-0.1,0c-0.2-0.1-0.4-0.1-0.5-0.2l-0.2-0.1L267.1,7.2z">
                                       </path>
                                       <path class="st33"
                                          d="M268.8,11.5c-3.2,0.2-6-2.3-6.1-5.5c-0.1-1.2,0.3-2.4,0.9-3.4c-0.7,1-1,2.2-0.9,3.5c0.2,3.2,2.9,5.7,6.1,5.5   c2-0.1,3.7-1.2,4.6-2.8C272.4,10.4,270.7,11.4,268.8,11.5z">
                                       </path>
                                    </g>
                                    <polygon class="st00"
                                       points="297.8,43.3 296.4,42.7 294.6,46.7 295.4,41.2 296.8,41.8 298.7,37.9 ">
                                    </polygon>
                                    <polygon class="st00"
                                       points="264.2,27.2 262.9,26.4 260.7,30.2 262,24.8 263.3,25.6 265.5,21.8 ">
                                    </polygon>
                                 </svg>
                              </div>
                              <div class="f-22 f-md-32 w600 lh120 text-center text-uppercase mt15 white-clr ">
                                 Commercial
                              </div>
                              <!-- <div class="center-line-white "></div> -->
                           </div>
                           <div class=" ">
                              <ul class="table-list1 pl0 f-18 f-md-18 w400 text-center lh140 white-clr ">
                                 <li>Build <span class="w500 ">Up-to 100 Sites</span></li>
                                 <li><span class="w500 ">Unlimited</span> Visitors</li>
                                 <li><span class="w500 ">Unlimited </span>Products, Services or Posts</li>
                                 <li>Create <span class="w500 ">Stunning Sites For Any Business Need.<span></li>
                                 <li class="w500 ">Sell Products, Services Or Promote Affiliates Offers </li>
                                 <li><span class="w500 ">Over 200 Eye-Catching Templates</span> Organized In 30+ Themes
                                 </li>
                                 <li><span class="w500 ">Built On World's No. 1, WP FRAMEWORK</span> For BUSINESSES
                                 </li>
                                 <li class="w500 ">Fully SEO-Optimized Sites</li>
                                 <li><span class="w500 ">Create 100% All Device ready</span> and ultra-fast loading websites.</li>
                                 <li><span class="w500 ">Get More Viral Traffic</span> with In-Built Social Media Tools
                                 </li>
                                 <li class="w500 ">Seamless WooCommerce Integration</li>
                                 <li>Change Website Width & Layout Without Touching Any Code</li>
                                 <li>Fully Customizable Typography</li>
                                 <li>Get 20+ Beautiful Sliders To Engage & Wow Visitors</li>
                                 <li>100% Secured With Top Notch Encryption</li>
                                 <li>Support Hooks, Custom CSS For Developers </li>
                                 <li>Comes With 10+ Ready To Use Short Codes & Widgets</li>
                                 <li>CTA management, Google Maps & Feature Rich slider</li>
                                 <li>Manage your websites from your mobile devices</li>
                                 <li>Inbuilt Lead Management Feature</li>
                                 <li>Analytics & Remarketing Ready Websites</li>
                                 <li>FREE Updates –When You Sign Up Today. </li>
                                 <li>100% Newbie Friendly</li>
                                 <li>Round-The-Clock Chat Support</li>
                                 <li>Commercial License included</li>
                                 <li>Help Local Clients That Desperately Need Your Service </li>
                                 <li>Provide High In Demand Services for BIG Profits</li>
                                 <li class="headline ">BONUSES WORTH $2200 FREE!!!</li>
                                 <li>Fast Action Bonus 1 - [LIVE TRAINING] Find Pre-Qualify Local Leads Worldwide & Close More Clients (Worth $1497)</li>
                                 <li>Fast Action Bonus 2 - ProfitFox - Most Powerful Notification System (Worth $297)
                                 </li>
                                 <li>Fast Action Bonus 3 - 100 Website Business Models (Worth $199)</li>
                                 <li>Fast Action Bonus 4 - WP Site Launcher Review Pack (Worth $210)</li>
                              </ul>
                           </div>
                           <div class="table-btn ">
                              <div class="hideme-button ">
                                 <a href="https://warriorplus.com/o2/buy/vr5dvn/f8s4cl/ftvdmb"><img src="https://warriorplus.com/o2/btn/fn200011000/vr5dvn/f8s4cl/314113" class="img-fluid mx-auto d-block"></a>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt30 ">
                  <div class="f-md-22 f-20 w400 lh140 text-center ">
                     *Commercial License Is ONLY Available With The Purchase Of Commercial Plan
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Discounted Price Section End -->
      <!-- To your awesome section -->
      <div class="awesome-section">
         <div class="container ">
            <div class="row ">
               <div class="col-12 ">
                  <div class="col-12 f-28 f-md-45 w700 lh140 white-clr text-center ">Thanks for Checking out WEBPULL
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
                  <div class="row">
                     <div class="col-md-6 col-12 text-center">
                        <img src="https://cdn.oppyo.com/launches/webpull/jv/ayush-sir.webp" class="img-fluid d-block mx-auto " alt="Ayush Jain">
                        <div class="f-24 f-md-28 w700 lh140 white-clr mt20">
                           Ayush Jain
                        </div>
                        <div class="f-14 lh140 w500 white-clr mt10">
                           (Entrepreneur &amp; Internet Marketer)
                        </div>
                     </div>
                     <div class="col-md-6 col-12 text-center mt20 mt-md0">
                        <img src="https://cdn.oppyo.com/launches/webpull/jv/pranshu-sir.webp" class="img-fluid d-block mx-auto " alt="Pranshu Gupta"> 
                        <div class="f-24 f-md-28 w700 lh140 text-center white-clr mt20">
                           Pranshu Gupta
                        </div>
                        <div class="f-14 lh140 w500 text-center white-clr mt10">
                           (Internet Marketer &amp; Product Creator)
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- To your awesome section End -->
      <!-- FAQ Section Start -->
      <div class="faq-section ">
         <div class="container ">
            <div class="row ">
               <div class="col-12 f-28 f-md-45 lh140 w600 text-center black-clr ">
                  Frequently Asked <span class="orange-clr ">Questions</span>
               </div>
               <div class="col-12 mt20 mt-md30 ">
                  <div class="row ">
                     <div class="col-md-6 col-12 ">
                        <div class="faq-list ">
                           <div class="f-md-22 f-20 w600 black-clr lh140 ">
                              Do I need to download or install WEBPULL somewhere?
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-start ">
                              NO! You just create an account online and you can get started immediately. To build a site, you need to download our Agencies Framework and upload it to your WordPress site only once. After that it run as 100% web-based solution hosted on the cloud.
                              <br><br> This means you never have to download or update anything again. 1 Click updating of your themes and templates. And It works across all browsers and all devices including Windows and Mac.
                           </div>
                        </div>
                        <div class="faq-list ">
                           <div class="f-md-22 f-20 w600 black-clr lh140 ">
                              Is my investment risk free?
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-start ">
                              We know the worth of your money. You can be rest assured that your investment is as safe as houses. However, we would like to clearly state that we don’t offer a no questions asked money back guarantee. You must provide a genuine reason and show us proof
                              that you tried it before asking for a refund.
                           </div>
                        </div>
                        <div class="faq-list ">
                           <div class="f-md-22 f-20 w600 black-clr lh140 ">
                              Do you charge any monthly fees?
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-start ">
                              There are NO monthly fees to use it during the launch period. During this period, you pay once and never again. We always believe in providing complete value for your money. However, there are upgrades as upsell which requires monthly payment but its
                              100% optional & not mandatory for working with WEBPULL. Those are recommended if you want to multiply your benefits.
                           </div>
                        </div>
                        <div class="faq-list ">
                           <div class="f-md-22 f-20 w600 black-clr lh140 ">
                              Will I get any training or support for my questions?
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-start ">
                              YES. We have created a detailed and step-by-step video training that shows you how to get setup everything quick & easy. You can access to the training in the member’s area. You will also get live chat - customer support so you never get stuck or have
                              any issues.
                           </div>
                        </div>
                     </div>
                     <div class="col-md-6 col-12 ">
                        <div class="faq-list ">
                           <div class="f-md-22 f-20 w600 black-clr lh140 ">
                              Do you charge any monthly fees?
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-start ">
                              NOT AT ALL. There are NO monthly fees to use WEBPULL during the launch period. During this period, you pay once and never again. We always believe in providing complete value for your money.
                           </div>
                        </div>
                        <div class="faq-list ">
                           <div class="f-md-22 f-20 w600 black-clr lh140 ">
                              Is WEBPULL compliant with all guidelines & compliances?
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-start ">
                              Yes, our platform is built with having all prescribed guidelines and compliances in consideration. We make constant efforts to ensure that we follow all the necessary guidelines and regulations. Still, we request all users to read very careful about third-party
                              services which is not a part of WEBPULL while choosing it for your business.
                           </div>
                        </div>
                        <div class="faq-list ">
                           <div class="f-md-22 f-20 w600 black-clr lh140 ">
                              What is the duration of service with this WEBPULL launch special deal?
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-start ">
                              As a nature of SAAS, we claim to provide services for the next 60 months. After this period gets over, be rest assured as our customer success team will renew your services for another 60 months for free and henceforth. We are giving it as complimentary
                              renewal to our founder members for buying from us early.
                           </div>
                        </div>
                        <div class="faq-list ">
                           <div class="f-md-22 f-20 w600 black-clr lh140 ">
                              How is WEBPULL is different from other available tools in the market?
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-start ">
                              Well, we have a nice comparison chart with other service providers. We won’t like to boast much about our software, but we can assure you that this is a cutting edge technology that will enable you to create and sell stunning wesbites at such a low introductory
                              price.
                           </div>
                        </div>
                        <div class="faq-list ">
                           <div class="f-md-22 f-20 w600 black-clr lh140 ">
                              Is WEBPULL Windows and Mac compatible?
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-start ">
                              YES. We’ve already stated that WEBPULL WordPress Framework is a web-based solution. So, it runs directly on the web and works across all browsers and all devices.
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50 ">
                  <div class="f-22 f-md-28 w600 black-clr px0 text-xs-center lh140 ">If you have any query, simply reach to us at <a href="mailto:support@bizomart.com " class="kapblue ">support@bizomart.com</a>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- FAQ Section End -->
      <!--final section-->
      <div class="final-section ">
         <div class="container ">
            <div class="row ">
               <div class="col-12 passport-content">
                  <div class="text-center">
                     <div class="f-45 f-md-45 lh140 w700 text-center white-clr red-shape2">
                        FINAL CALL
                     </div>
                  </div>
                  <div class="col-12 f-22 f-md-32 lh140 w600 text-center white-clr mt20 p0">
                     You Still Have The Opportunity To Take Your Game To The Next Level… <br> Remember, It’s Your Make Or Break Time!
                  </div>
                  <!-- CTA Btn Section Start -->
                  <div class="col-12 mt40 mt-md60">
                     <!-- <div class="f-24 f-md-32 w700 lh140 text-center white-clr animate-charcter2">
                        <img src=" ./assets/images/hurry.webp " alt="Hurry " class="mr10 hurry-img">
                        <span class="red-clr">Hurry!</span> Available For A Limited Time Only! No Monthly Fees!
                        </div>
                        <div class="f-20 f-md-24 w400 lh140 text-center mt20 white-clr ">
                        Use Coupon Code <span class="org-clr w600 ">“web10”</span> for an Additional <span class="org-clr w600 ">10% Discount</span>
                        </div> -->
                     <div class="f-22 f-md-28 w700 lh140 text-center white-clr">
                        (Free Commercial License + Low 1-Time Price For Launch Period Only)
                     </div>
                     <div class="row">
                        <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center ">
                           <a href="#buynow " class="cta-link-btn ">Get Instant Access To WEBPULL</a>
                        </div>
                     </div>
                     <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30 ">
                        <img src="assets/images/compaitable-with1.webp " class="img-fluid d-block mx-xs-center md-img-right ">
                        <div class="d-md-block d-none px-md30 "><img src="assets/images/v-line.webp " class="img-fluid "></div>
                        <img src="assets/images/days-gurantee1.webp " class="img-fluid d-block mx-xs-auto mt15 mt-md0 ">
                     </div>
                  </div>
                  <!-- CTA Btn Section End -->
               </div>
            </div>
         </div>
      </div>
      <!--final section-->
      <!--Footer Section Start -->
      <div class="footer-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 394.2 100" style="enable-background:new 0 0 394.2 100; height:60px;" xml:space="preserve">
                     <style type="text/css">
                        .st00{fill:#FFFFFF;}
                        .st11{fill:url(#SVGID_1_);}
                        .st22{fill:url(#SVGID_00000021093791325859232720000015953972570224053676_);}
                        .st33{fill:#FFC802;}
                        .st44{fill:url(#SVGID_00000003068462962921929030000010945183465240898967_);}
                     </style>
                     <path class="st00" d="M117.7,37.9l-5,12.4H92.9v12.4h18.6l-5,12.4H92.9v12.4h24.8l-5,12.5H92.9H80.5V75.1V62.7V37.9H117.7z"></path>
                     <path class="st00" d="M323.2,37.9v49.7H348V100h-37.3V37.9H323.2z"></path>
                     <path class="st00" d="M369.4,37.9v49.7h24.8V100h-37.3V37.9H369.4z"></path>
                     <g>
                        <g>
                           <polygon class="st00" points="59.1,71.4 52.4,54.4 59.1,37.9 72.6,37.9   "></polygon>
                           <polygon class="st00" points="29.4,54.7 36.3,37.9 54.5,82.9 47.7,100   "></polygon>
                           <polygon class="st00" points="24.8,100 31.6,82.8 13.4,37.9 0,37.9   "></polygon>
                        </g>
                     </g>
                     <path class="st00" d="M222.8,44.3c-4.3-4.3-9.4-6.4-15.4-6.4h-9.3h-12.5v21.7v21.7V100h12.5V81.3h0V68.9h0v-9.3v-9.3h9.3  c2.6,0,4.8,0.9,6.6,2.8c1.8,1.8,2.7,4,2.7,6.6c0,2.6-0.9,4.8-2.7,6.6c-1.8,1.8-4,2.7-6.6,2.7h-0.7v12.4h0.7c6,0,11.1-2.1,15.4-6.4  c4.3-4.3,6.4-9.4,6.4-15.4C229.2,53.6,227.1,48.5,222.8,44.3z"></path>
                     <rect x="252.1" y="31.8" transform="matrix(0.4226 -0.9063 0.9063 0.4226 114.0209 255.0236)" class="st00" width="10.2" height="12.5"></rect>
                     <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="249.3741" y1="97.3262" x2="270.7371" y2="53.5256">
                        <stop offset="0" style="stop-color:#FA8460"></stop>
                        <stop offset="1" style="stop-color:#FCB121"></stop>
                     </linearGradient>
                     <path class="st11" d="M271.7,80.3c-1.4,3.1-3.8,5.2-7,6.4c-3.2,1.2-6.4,1-9.5-0.4c-3.1-1.4-5.2-3.8-6.4-7c-1.2-3.2-1-6.4,0.4-9.5  l9.9-21.2l-11.3-5.3l-9.9,21.2c-2.9,6.2-3.2,12.6-0.8,19c2.3,6.5,6.6,11.1,12.9,14c6.2,2.9,12.5,3.2,19,0.8  c6.5-2.3,11.2-6.6,14.1-12.8l9.9-21.2l-11.3-5.3L271.7,80.3z"></path>
                     <rect x="285.9" y="47.6" transform="matrix(0.4226 -0.9063 0.9063 0.4226 119.2494 294.738)" class="st00" width="10.2" height="12.5"></rect>
                     <path class="st00" d="M173.3,76.3c-0.8-1.7-1.7-3.2-2.9-4.5c-1.2-1.3-2.5-2.3-4-3.1c-1.5-0.8-3-1.2-4.5-1.3c1.5-0.1,2.8-0.5,4.1-1.3  c1.2-0.8,2.3-1.8,3.2-3c0.9-1.2,1.6-2.5,2.1-3.9c0.5-1.4,0.8-2.8,0.8-4c0-3.5-0.5-6.4-1.4-8.6c-1-2.2-2.3-4-3.9-5.3  c-1.6-1.3-3.6-2.2-5.8-2.6c-2.2-0.5-4.6-0.7-7-0.7h-27.3v10.5h11.9v0h15.4c0.8,0,1.7,0.2,2.4,0.6c0.8,0.4,1.4,0.9,2,1.5  c0.6,0.6,1,1.4,1.4,2.2c0.3,0.9,0.5,1.7,0.5,2.6c0,0.9-0.2,1.8-0.5,2.8c-0.3,0.9-0.8,1.8-1.4,2.5c-0.6,0.7-1.3,1.3-2.2,1.8  c-0.9,0.5-1.9,0.7-3.1,0.7h-9.1v10.5h10c2.7,0,4.9,0.7,6.4,2.1c1.6,1.4,2.3,3.2,2.3,5.3c0,1-0.2,2-0.6,3c-0.4,1-0.9,1.9-1.6,2.7  c-0.7,0.8-1.5,1.5-2.4,2c-0.9,0.5-1.9,0.7-2.9,0.7h-16.8h0V73.7h0V63.2h0v-9.3h-11.9V100h9.6h2.3h16.8c2.7,0,5.2-0.3,7.5-0.9  c2.3-0.6,4.4-1.6,6.2-3.1c1.8-1.4,3.1-3.3,4.2-5.7c1-2.3,1.5-5.2,1.5-8.6C174.4,79.8,174,78,173.3,76.3z"></path>
                     <g>
                        <linearGradient id="SVGID_00000142155450205615942230000010409392281687221429_" gradientUnits="userSpaceOnUse" x1="270.7616" y1="25.6663" x2="296.63" y2="25.6663">
                           <stop offset="0" style="stop-color:#FA8460"></stop>
                           <stop offset="4.692155e-02" style="stop-color:#FA9150"></stop>
                           <stop offset="0.1306" style="stop-color:#FBA43A"></stop>
                           <stop offset="0.2295" style="stop-color:#FBB229"></stop>
                           <stop offset="0.3519" style="stop-color:#FCBC1E"></stop>
                           <stop offset="0.5235" style="stop-color:#FCC117"></stop>
                           <stop offset="1" style="stop-color:#FCC315"></stop>
                        </linearGradient>
                        <path style="fill:url(#SVGID_00000142155450205615942230000010409392281687221429_);" d="M289.6,14.2c-2.1-1.1-4.3-1.5-6.5-1.4   c-0.1,0-0.1,0-0.2,0c-0.1,0-0.2,0-0.2,0c0,0-0.1,0-0.1,0c-0.1,0-0.2,0-0.2,0c-0.1,0-0.2,0-0.3,0c-0.1,0-0.2,0-0.2,0   c-0.3,0-0.5,0.1-0.8,0.1c-0.1,0-0.2,0-0.3,0.1c-0.1,0-0.2,0-0.3,0.1c-0.1,0-0.3,0.1-0.4,0.1c-0.1,0-0.2,0-0.2,0.1   c-0.1,0-0.2,0.1-0.3,0.1c-0.2,0.1-0.5,0.2-0.7,0.3c0,0,0,0,0,0c-0.3,0.1-0.6,0.3-0.9,0.4c-0.3,0.1-0.6,0.3-0.9,0.5   c-0.1,0.1-0.3,0.2-0.4,0.2c-0.1,0.1-0.2,0.2-0.4,0.2c-0.1,0.1-0.2,0.1-0.2,0.2c0,0-0.1,0-0.1,0.1c-0.1,0.1-0.2,0.1-0.2,0.2   c-0.1,0-0.1,0.1-0.2,0.2c-0.1,0.1-0.2,0.2-0.3,0.3c-0.1,0-0.1,0.1-0.2,0.2c-0.5,0.4-0.9,0.9-1.3,1.4c-0.1,0.1-0.1,0.1-0.2,0.2   c0,0-0.1,0.1-0.1,0.1c-0.1,0.1-0.1,0.2-0.2,0.3c0,0-0.1,0.1-0.1,0.1c-0.1,0.1-0.1,0.2-0.2,0.3c0,0,0,0.1-0.1,0.1   c-0.1,0.1-0.1,0.2-0.2,0.3c0,0,0,0.1-0.1,0.1c-0.1,0.1-0.1,0.2-0.2,0.3c-0.1,0.1-0.2,0.3-0.2,0.4c-3.3,6.3-0.8,14.1,5.6,17.4   c1.9,1,3.9,1.4,5.9,1.4c0.1,0,0.3,0,0.4,0c0.1,0,0.3,0,0.4,0c0.1,0,0.1,0,0.2,0c0.1,0,0.2,0,0.3,0c0,0,0,0,0.1,0c0.1,0,0.2,0,0.3,0   c0.1,0,0.2,0,0.2,0c0.1,0,0.2,0,0.3-0.1c0.1,0,0.3,0,0.4-0.1c0.1,0,0.3-0.1,0.4-0.1c0.1,0,0.2-0.1,0.3-0.1c0.2,0,0.4-0.1,0.5-0.2   c0.1,0,0.3-0.1,0.4-0.1c0.1,0,0.3-0.1,0.4-0.1c0,0,0,0,0,0c0.1,0,0.2-0.1,0.4-0.1c0,0,0,0,0,0c0,0,0.1,0,0.1,0c0,0,0.1,0,0.1-0.1   c0,0,0,0,0.1,0c0,0,0.1,0,0.1,0c0,0,0,0,0.1,0c0.1,0,0.2-0.1,0.3-0.2c0,0,0,0,0.1,0c0.1-0.1,0.2-0.1,0.3-0.2   c0.1-0.1,0.3-0.1,0.4-0.2c0.4-0.2,0.7-0.5,1.1-0.7c0.2-0.2,0.5-0.4,0.7-0.6c0.2-0.2,0.4-0.4,0.7-0.6c0.2-0.2,0.4-0.4,0.6-0.6   c0.2-0.2,0.4-0.5,0.6-0.7c0.1-0.1,0.2-0.2,0.3-0.4c0,0,0,0,0,0c0.1-0.1,0.2-0.2,0.3-0.4c0.1-0.1,0.2-0.3,0.2-0.4   c0.1-0.1,0.1-0.2,0.2-0.3c0-0.1,0.1-0.1,0.1-0.2c0.1-0.1,0.1-0.2,0.2-0.3C298.5,25.3,296,17.5,289.6,14.2z M279.6,26.7L279.6,26.7   l0.3,0.6c0.4,0.7,1,1.3,1.8,1.7c0.3,0.2,0.7,0.3,1,0.3c0.3,0,0.5-0.1,0.7-0.2c0.1,0,0.1-0.1,0.2-0.2c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0-0.1c0.2-0.3,0.2-0.7-0.1-1.3c0,0,0,0,0,0   c-0.1-0.2-0.2-0.4-0.4-0.6c-0.1-0.1-0.2-0.2-0.3-0.3c-0.4-0.4-0.7-0.8-0.9-1.2c-0.1-0.1-0.2-0.2-0.2-0.4c0,0,0,0,0-0.1   c0,0,0-0.1-0.1-0.1c0,0,0-0.1-0.1-0.1c-0.1-0.2-0.1-0.3-0.2-0.5c-0.1-0.2-0.1-0.4-0.2-0.6c0,0,0-0.1,0-0.1c0-0.1,0-0.2,0-0.3v-0.1   c0-0.1,0-0.1,0-0.2c0,0,0-0.1,0-0.1c0-0.1,0-0.2,0-0.3c0.1-0.3,0.2-0.6,0.3-0.8c0,0,0,0,0-0.1c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0-0.1c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0l0,0c0.4-0.5,0.9-0.9,1.5-1.1c0.7-0.2,1.4-0.3,2.1-0.1l0.9-1.7l1.9,1l-0.8,1.6c0.7,0.4,1.2,1,1.6,1.6   l0.2,0.4l-1.8,1.6l-0.3-0.5c-0.1-0.1-0.3-0.4-0.5-0.7c-0.1-0.1-0.3-0.3-0.5-0.4c0,0-0.1-0.1-0.1-0.1c-0.1-0.1-0.3-0.2-0.4-0.2   c0,0-0.1,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0   c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0h0c0,0,0,0-0.1,0h0   c0,0,0,0-0.1,0h0c0,0,0,0,0,0h0c0,0,0,0,0,0h0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c-0.2,0.1-0.4,0.3-0.5,0.5c-0.3,0.5-0.2,0.9,1,2.3c0.8,0.9,1.2,1.6,1.4,2.4   c0.2,0.8,0.1,1.6-0.3,2.5c-0.8,1.5-2.4,2.2-4.2,1.8l-0.8,1.6l-0.1,0.2l-2-1l0.9-1.7c-0.1-0.1-0.2-0.1-0.3-0.2   c-0.3-0.2-0.5-0.4-0.8-0.6c0,0-0.1-0.1-0.1-0.1c-0.3-0.3-0.5-0.6-0.7-1l-0.2-0.4L279.6,26.7z"></path>
                        <path class="st33" d="M278,37c-6.3-3.3-8.8-11.1-5.6-17.4c1.3-2.4,3.2-4.3,5.4-5.5c-2.4,1.2-4.4,3.1-5.7,5.6   c-3.3,6.3-0.8,14.1,5.6,17.4c3.9,2,8.4,1.9,12-0.1C286.2,38.9,281.8,39,278,37z"></path>
                     </g>
                     <g>
                        <linearGradient id="SVGID_00000016062571573767098340000005260099975199807150_" gradientUnits="userSpaceOnUse" x1="79.734" y1="-130.471" x2="91.3973" y2="-130.471" gradientTransform="matrix(0.8624 -0.5063 0.5063 0.8624 260.6894 161.6657)">
                           <stop offset="0" style="stop-color:#FA8460"></stop>
                           <stop offset="4.692155e-02" style="stop-color:#FA9150"></stop>
                           <stop offset="0.1306" style="stop-color:#FBA43A"></stop>
                           <stop offset="0.2295" style="stop-color:#FBB229"></stop>
                           <stop offset="0.3519" style="stop-color:#FCBC1E"></stop>
                           <stop offset="0.5235" style="stop-color:#FCC117"></stop>
                           <stop offset="1" style="stop-color:#FCC315"></stop>
                        </linearGradient>
                        <path style="fill:url(#SVGID_00000016062571573767098340000005260099975199807150_);" d="M268.1,0c-1,0.1-2,0.4-2.8,0.9   c0,0-0.1,0-0.1,0c0,0-0.1,0-0.1,0.1c0,0,0,0,0,0c0,0-0.1,0-0.1,0.1c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0-0.1,0.1   c-0.1,0.1-0.2,0.1-0.3,0.2c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0.1-0.1,0.1   c-0.1,0.1-0.1,0.2-0.2,0.3c0,0,0,0,0,0c-0.1,0.1-0.2,0.2-0.2,0.4c-0.1,0.1-0.2,0.3-0.2,0.4c0,0.1-0.1,0.1-0.1,0.2   c0,0.1-0.1,0.1-0.1,0.2c0,0,0,0.1-0.1,0.1c0,0,0,0,0,0.1c0,0,0,0.1-0.1,0.1c0,0,0,0.1,0,0.1c0,0.1,0,0.1-0.1,0.2c0,0,0,0.1,0,0.1   c-0.1,0.3-0.1,0.5-0.2,0.8c0,0,0,0.1,0,0.1c0,0,0,0.1,0,0.1c0,0.1,0,0.1,0,0.2c0,0,0,0,0,0.1c0,0.1,0,0.1,0,0.2c0,0,0,0,0,0.1   c0,0.1,0,0.1,0,0.2c0,0,0,0,0,0.1c0,0.1,0,0.1,0,0.2c0,0.1,0,0.1,0,0.2c0.2,3.2,2.9,5.7,6.1,5.5c1-0.1,1.9-0.3,2.6-0.8   c0.1,0,0.1-0.1,0.2-0.1c0.1,0,0.1-0.1,0.1-0.1c0,0,0,0,0.1,0c0,0,0.1,0,0.1-0.1c0,0,0,0,0,0c0,0,0.1-0.1,0.1-0.1c0,0,0.1,0,0.1-0.1   c0,0,0.1-0.1,0.1-0.1c0,0,0.1-0.1,0.1-0.1c0,0,0.1-0.1,0.1-0.1c0,0,0.1-0.1,0.1-0.1c0.1-0.1,0.1-0.1,0.2-0.2c0,0,0.1-0.1,0.1-0.1   c0,0,0.1-0.1,0.1-0.1c0,0,0,0,0,0c0,0,0.1-0.1,0.1-0.1c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0-0.1c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0.1-0.1,0.1-0.1c0,0,0,0,0,0c0,0,0.1-0.1,0.1-0.1c0-0.1,0.1-0.1,0.1-0.2c0.1-0.2,0.2-0.4,0.3-0.5c0.1-0.1,0.1-0.2,0.1-0.4   c0-0.1,0.1-0.3,0.1-0.4c0-0.1,0.1-0.3,0.1-0.4c0-0.1,0-0.3,0.1-0.4c0-0.1,0-0.1,0-0.2c0,0,0,0,0,0c0-0.1,0-0.1,0-0.2   c0-0.1,0-0.1,0-0.2c0-0.1,0-0.1,0-0.2c0,0,0-0.1,0-0.1c0-0.1,0-0.1,0-0.2C274.1,2.3,271.3-0.2,268.1,0z M267.1,7.2L267.1,7.2   l0.3,0.2c0.3,0.2,0.7,0.3,1.1,0.3c0.2,0,0.3-0.1,0.4-0.1c0.1-0.1,0.2-0.1,0.2-0.3c0,0,0-0.1,0-0.1c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0-0.2-0.1-0.3-0.3-0.5c0,0,0,0,0,0   c-0.1,0-0.2-0.1-0.3-0.1c-0.1,0-0.1,0-0.2-0.1c-0.2-0.1-0.4-0.2-0.6-0.2c-0.1,0-0.1-0.1-0.2-0.1c0,0,0,0,0,0c0,0,0,0-0.1,0   c0,0,0,0-0.1,0c-0.1,0-0.1-0.1-0.2-0.1c-0.1-0.1-0.1-0.1-0.2-0.2c0,0,0,0,0,0c0,0-0.1-0.1-0.1-0.1l0,0c0,0,0,0,0-0.1c0,0,0,0,0,0   c0,0,0-0.1-0.1-0.1c0-0.1-0.1-0.3-0.1-0.4c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0l0,0   c0-0.3,0.2-0.5,0.3-0.8c0.2-0.2,0.5-0.4,0.8-0.5l0-0.9l1-0.1l0,0.8c0.4,0,0.7,0.1,1,0.3l0.2,0.1l-0.3,1l-0.3-0.1   c-0.1,0-0.2-0.1-0.4-0.1c-0.1,0-0.2,0-0.3-0.1c0,0-0.1,0-0.1,0c-0.1,0-0.1,0-0.2,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0l0,0c0,0,0,0,0,0l0,0c0,0,0,0,0,0l0,0c0,0,0,0,0,0l0,0c0,0,0,0,0,0l0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c-0.1,0.1-0.1,0.2-0.1,0.3c0,0.2,0.1,0.4,0.9,0.7c0.5,0.2,0.8,0.4,1.1,0.6c0.3,0.3,0.4,0.6,0.4,1c0,0.8-0.4,1.4-1.2,1.7l0,0.8   l0,0.1l-1,0.1l0-0.8c-0.1,0-0.1,0-0.2,0c-0.2,0-0.3,0-0.4-0.1c0,0-0.1,0-0.1,0c-0.2-0.1-0.4-0.1-0.5-0.2l-0.2-0.1L267.1,7.2z"></path>
                        <path class="st33" d="M268.8,11.5c-3.2,0.2-6-2.3-6.1-5.5c-0.1-1.2,0.3-2.4,0.9-3.4c-0.7,1-1,2.2-0.9,3.5c0.2,3.2,2.9,5.7,6.1,5.5   c2-0.1,3.7-1.2,4.6-2.8C272.4,10.4,270.7,11.4,268.8,11.5z"></path>
                     </g>
                     <polygon class="st00" points="297.8,43.3 296.4,42.7 294.6,46.7 295.4,41.2 296.8,41.8 298.7,37.9 "></polygon>
                     <polygon class="st00" points="264.2,27.2 262.9,26.4 260.7,30.2 262,24.8 263.3,25.6 265.5,21.8 "></polygon>
                  </svg>
                  <div class="f-14 f-md-16 w500 mt20 lh150 white-clr text-start" style="color:#7d8398;"><span class="w700">Note:</span> This site is not a part of the Facebook website or Facebook Inc. Additionally, this site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc.<br><br>
                  <span class="w700">Earning Disclaimer:</span> Please make your purchase decision wisely. There is no promise or representation that you will make a certain amount of money, or any money, or not lose money, as a result of using
                  our products and services. Any stats, conversion, earnings, or income statements are strictly estimates. There is no guarantee that you will make these levels for yourself. As with any business, your results will vary and will
                  be based on your personal abilities, experience, knowledge, capabilities, level of desire, and an infinite number of variables beyond our control, including variables we or you have not anticipated. There are no guarantees concerning
                  the level of success you may experience. Each person’s results will vary. There are unknown risks in any business, particularly with the Internet where advances and changes can happen quickly. The use of the plug-in and the given
                  information should be based on your own due diligence, and you agree that we are not liable for your success or failure.
               </div>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-md-16 f-16 w400 lh140 white-clr text-xs-center">Copyright © WebPull 2022</div>
                  <ul class="footer-ul w400 f-md-16 f-16 white-clr text-center text-md-right">
                     <li><a href="https://support.bizomart.com/hc/en-us " class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://webpull.co/legal/privacy-policy.html " class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://webpull.co/legal/terms-of-service.html " class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://webpull.co/legal/disclaimer.html " class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://webpull.co/legal/gdpr.html " class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://webpull.co/legal/dmca.html " class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://webpull.co/legal/anti-spam.html " class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!--Footer Section End -->
         <!-- Exit Popup and Timer -->
   <?php include('timer.php'); ?>
   <!-- Exit Popup and Timer End -->
   </body>
</html>
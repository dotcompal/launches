<html xmlns="http://www.w3.org/1999/xhtml">

<head>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
   <!-- Tell the browser to be responsive to screen width -->
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
   <meta name="title" content="WebPull Special Bonuses">
   <meta name="description" content="Get Premium, Limited Time Bonuses With Your WebPull Purchase & Make It Even Better">
   <meta name="keywords" content="">
   <meta property="og:image" content="https://www.webpull.co/special-bonus/thumbnail.png">
   <meta name="language" content="English">
   <meta name="revisit-after" content="1 days">
   <meta name="author" content="Ayush Jain">
   <!-- Open Graph / Facebook -->
   <meta property="og:type" content="website">
   <meta property="og:title" content="WebPull Special Bonuses">
   <meta property="og:description" content="Get Premium, Limited Time Bonuses With Your WebPull Purchase & Make It Even Better">
   <meta property="og:image" content="https://www.webpull.co/special-bonus/thumbnail.png">
   <!-- Twitter -->
   <meta property="twitter:card" content="summary_large_image">
   <meta property="twitter:title" content="WebPull Special Bonuses">
   <meta property="twitter:description" content="Get Premium, Limited Time Bonuses With Your WebPull Purchase & Make It Even Better">
   <meta property="twitter:image" content="https://www.webpull.co/special-bonus/thumbnail.png">
   <title>WebPull Special Bonuses</title>

   <!-- Shortcut Icon  -->
   <link rel="icon" href="https://cdn.oppyo.com/launches/webpull/common_assets/images/favicon.png" type="image/png">
   <link rel="preconnect" href="https://fonts.googleapis.com">
   <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
   <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
   <!-- Css CDN Load Link -->
   <link rel="stylesheet" href="https://cdn.oppyo.com/launches/webpull/common_assets/css/bootstrap.min.css" type="text/css">
   <link rel="stylesheet" type="text/css" href="assets/css/bonus-style.css">
   <link rel="stylesheet" href="assets/css/style.css" type="text/css">
   <script src="../common_assets/js/jquery.min.js"></script>
</head>

<body>
   <!-- New Timer  Start-->
   <?php
   $date = 'July 14 2022 11:00 AM EST';
   $exp_date = strtotime($date);
   $now = time();
   /*
         
         $date = date('F d Y g:i:s A eO');
         $rand_time_add = rand(700, 1200);
         $exp_date = strtotime($date) + $rand_time_add;
         $now = time();*/

   if ($now < $exp_date) {
   ?>
   <?php
   } else {
      echo "Times Up";
   }
   ?>
   <!-- New Timer End -->
   <?php
   if (!isset($_GET['afflink'])) {
      $_GET['afflink'] = 'https://warriorplus.com/o2/a/wyl4gg/0';
      $_GET['name'] = 'Ayush Jain';
   }
   ?>
   <div class="main-header">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-12 text-center">
               <div class="text-center">
                  <div class="f-md-32 f-20 lh140 w400 white-clr d-block d-md-flex align-items-center justify-content-center">
                     <span class="w700"><?php echo $_GET['name']; ?>'s</span> &nbsp;special bonus for &nbsp;
                     <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 394.2 100" style="enable-background:new 0 0 394.2 100; height:50px;" xml:space="preserve">
                        <style type="text/css">
                           .st00 {
                              fill: #FFFFFF;
                           }

                           .st11 {
                              fill: url(#SVGID_1_);
                           }

                           .st22 {
                              fill: url(#SVGID_00000021093791325859232720000015953972570224053676_);
                           }

                           .st33 {
                              fill: #FFC802;
                           }

                           .st44 {
                              fill: url(#SVGID_00000003068462962921929030000010945183465240898967_);
                           }
                        </style>
                        <path class="st00" d="M117.7,37.9l-5,12.4H92.9v12.4h18.6l-5,12.4H92.9v12.4h24.8l-5,12.5H92.9H80.5V75.1V62.7V37.9H117.7z"></path>
                        <path class="st00" d="M323.2,37.9v49.7H348V100h-37.3V37.9H323.2z"></path>
                        <path class="st00" d="M369.4,37.9v49.7h24.8V100h-37.3V37.9H369.4z"></path>
                        <g>
                           <g>
                              <polygon class="st00" points="59.1,71.4 52.4,54.4 59.1,37.9 72.6,37.9   "></polygon>
                              <polygon class="st00" points="29.4,54.7 36.3,37.9 54.5,82.9 47.7,100   "></polygon>
                              <polygon class="st00" points="24.8,100 31.6,82.8 13.4,37.9 0,37.9   "></polygon>
                           </g>
                        </g>
                        <path class="st00" d="M222.8,44.3c-4.3-4.3-9.4-6.4-15.4-6.4h-9.3h-12.5v21.7v21.7V100h12.5V81.3h0V68.9h0v-9.3v-9.3h9.3  c2.6,0,4.8,0.9,6.6,2.8c1.8,1.8,2.7,4,2.7,6.6c0,2.6-0.9,4.8-2.7,6.6c-1.8,1.8-4,2.7-6.6,2.7h-0.7v12.4h0.7c6,0,11.1-2.1,15.4-6.4  c4.3-4.3,6.4-9.4,6.4-15.4C229.2,53.6,227.1,48.5,222.8,44.3z"></path>
                        <rect x="252.1" y="31.8" transform="matrix(0.4226 -0.9063 0.9063 0.4226 114.0209 255.0236)" class="st00" width="10.2" height="12.5"></rect>
                        <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="249.3741" y1="97.3262" x2="270.7371" y2="53.5256">
                           <stop offset="0" style="stop-color:#FA8460"></stop>
                           <stop offset="1" style="stop-color:#FCB121"></stop>
                        </linearGradient>
                        <path class="st11" d="M271.7,80.3c-1.4,3.1-3.8,5.2-7,6.4c-3.2,1.2-6.4,1-9.5-0.4c-3.1-1.4-5.2-3.8-6.4-7c-1.2-3.2-1-6.4,0.4-9.5  l9.9-21.2l-11.3-5.3l-9.9,21.2c-2.9,6.2-3.2,12.6-0.8,19c2.3,6.5,6.6,11.1,12.9,14c6.2,2.9,12.5,3.2,19,0.8  c6.5-2.3,11.2-6.6,14.1-12.8l9.9-21.2l-11.3-5.3L271.7,80.3z"></path>
                        <rect x="285.9" y="47.6" transform="matrix(0.4226 -0.9063 0.9063 0.4226 119.2494 294.738)" class="st00" width="10.2" height="12.5"></rect>
                        <path class="st00" d="M173.3,76.3c-0.8-1.7-1.7-3.2-2.9-4.5c-1.2-1.3-2.5-2.3-4-3.1c-1.5-0.8-3-1.2-4.5-1.3c1.5-0.1,2.8-0.5,4.1-1.3  c1.2-0.8,2.3-1.8,3.2-3c0.9-1.2,1.6-2.5,2.1-3.9c0.5-1.4,0.8-2.8,0.8-4c0-3.5-0.5-6.4-1.4-8.6c-1-2.2-2.3-4-3.9-5.3  c-1.6-1.3-3.6-2.2-5.8-2.6c-2.2-0.5-4.6-0.7-7-0.7h-27.3v10.5h11.9v0h15.4c0.8,0,1.7,0.2,2.4,0.6c0.8,0.4,1.4,0.9,2,1.5  c0.6,0.6,1,1.4,1.4,2.2c0.3,0.9,0.5,1.7,0.5,2.6c0,0.9-0.2,1.8-0.5,2.8c-0.3,0.9-0.8,1.8-1.4,2.5c-0.6,0.7-1.3,1.3-2.2,1.8  c-0.9,0.5-1.9,0.7-3.1,0.7h-9.1v10.5h10c2.7,0,4.9,0.7,6.4,2.1c1.6,1.4,2.3,3.2,2.3,5.3c0,1-0.2,2-0.6,3c-0.4,1-0.9,1.9-1.6,2.7  c-0.7,0.8-1.5,1.5-2.4,2c-0.9,0.5-1.9,0.7-2.9,0.7h-16.8h0V73.7h0V63.2h0v-9.3h-11.9V100h9.6h2.3h16.8c2.7,0,5.2-0.3,7.5-0.9  c2.3-0.6,4.4-1.6,6.2-3.1c1.8-1.4,3.1-3.3,4.2-5.7c1-2.3,1.5-5.2,1.5-8.6C174.4,79.8,174,78,173.3,76.3z"></path>
                        <g>
                           <linearGradient id="SVGID_00000142155450205615942230000010409392281687221429_" gradientUnits="userSpaceOnUse" x1="270.7616" y1="25.6663" x2="296.63" y2="25.6663">
                              <stop offset="0" style="stop-color:#FA8460"></stop>
                              <stop offset="4.692155e-02" style="stop-color:#FA9150"></stop>
                              <stop offset="0.1306" style="stop-color:#FBA43A"></stop>
                              <stop offset="0.2295" style="stop-color:#FBB229"></stop>
                              <stop offset="0.3519" style="stop-color:#FCBC1E"></stop>
                              <stop offset="0.5235" style="stop-color:#FCC117"></stop>
                              <stop offset="1" style="stop-color:#FCC315"></stop>
                           </linearGradient>
                           <path style="fill:url(#SVGID_00000142155450205615942230000010409392281687221429_);" d="M289.6,14.2c-2.1-1.1-4.3-1.5-6.5-1.4   c-0.1,0-0.1,0-0.2,0c-0.1,0-0.2,0-0.2,0c0,0-0.1,0-0.1,0c-0.1,0-0.2,0-0.2,0c-0.1,0-0.2,0-0.3,0c-0.1,0-0.2,0-0.2,0   c-0.3,0-0.5,0.1-0.8,0.1c-0.1,0-0.2,0-0.3,0.1c-0.1,0-0.2,0-0.3,0.1c-0.1,0-0.3,0.1-0.4,0.1c-0.1,0-0.2,0-0.2,0.1   c-0.1,0-0.2,0.1-0.3,0.1c-0.2,0.1-0.5,0.2-0.7,0.3c0,0,0,0,0,0c-0.3,0.1-0.6,0.3-0.9,0.4c-0.3,0.1-0.6,0.3-0.9,0.5   c-0.1,0.1-0.3,0.2-0.4,0.2c-0.1,0.1-0.2,0.2-0.4,0.2c-0.1,0.1-0.2,0.1-0.2,0.2c0,0-0.1,0-0.1,0.1c-0.1,0.1-0.2,0.1-0.2,0.2   c-0.1,0-0.1,0.1-0.2,0.2c-0.1,0.1-0.2,0.2-0.3,0.3c-0.1,0-0.1,0.1-0.2,0.2c-0.5,0.4-0.9,0.9-1.3,1.4c-0.1,0.1-0.1,0.1-0.2,0.2   c0,0-0.1,0.1-0.1,0.1c-0.1,0.1-0.1,0.2-0.2,0.3c0,0-0.1,0.1-0.1,0.1c-0.1,0.1-0.1,0.2-0.2,0.3c0,0,0,0.1-0.1,0.1   c-0.1,0.1-0.1,0.2-0.2,0.3c0,0,0,0.1-0.1,0.1c-0.1,0.1-0.1,0.2-0.2,0.3c-0.1,0.1-0.2,0.3-0.2,0.4c-3.3,6.3-0.8,14.1,5.6,17.4   c1.9,1,3.9,1.4,5.9,1.4c0.1,0,0.3,0,0.4,0c0.1,0,0.3,0,0.4,0c0.1,0,0.1,0,0.2,0c0.1,0,0.2,0,0.3,0c0,0,0,0,0.1,0c0.1,0,0.2,0,0.3,0   c0.1,0,0.2,0,0.2,0c0.1,0,0.2,0,0.3-0.1c0.1,0,0.3,0,0.4-0.1c0.1,0,0.3-0.1,0.4-0.1c0.1,0,0.2-0.1,0.3-0.1c0.2,0,0.4-0.1,0.5-0.2   c0.1,0,0.3-0.1,0.4-0.1c0.1,0,0.3-0.1,0.4-0.1c0,0,0,0,0,0c0.1,0,0.2-0.1,0.4-0.1c0,0,0,0,0,0c0,0,0.1,0,0.1,0c0,0,0.1,0,0.1-0.1   c0,0,0,0,0.1,0c0,0,0.1,0,0.1,0c0,0,0,0,0.1,0c0.1,0,0.2-0.1,0.3-0.2c0,0,0,0,0.1,0c0.1-0.1,0.2-0.1,0.3-0.2   c0.1-0.1,0.3-0.1,0.4-0.2c0.4-0.2,0.7-0.5,1.1-0.7c0.2-0.2,0.5-0.4,0.7-0.6c0.2-0.2,0.4-0.4,0.7-0.6c0.2-0.2,0.4-0.4,0.6-0.6   c0.2-0.2,0.4-0.5,0.6-0.7c0.1-0.1,0.2-0.2,0.3-0.4c0,0,0,0,0,0c0.1-0.1,0.2-0.2,0.3-0.4c0.1-0.1,0.2-0.3,0.2-0.4   c0.1-0.1,0.1-0.2,0.2-0.3c0-0.1,0.1-0.1,0.1-0.2c0.1-0.1,0.1-0.2,0.2-0.3C298.5,25.3,296,17.5,289.6,14.2z M279.6,26.7L279.6,26.7   l0.3,0.6c0.4,0.7,1,1.3,1.8,1.7c0.3,0.2,0.7,0.3,1,0.3c0.3,0,0.5-0.1,0.7-0.2c0.1,0,0.1-0.1,0.2-0.2c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0-0.1c0.2-0.3,0.2-0.7-0.1-1.3c0,0,0,0,0,0   c-0.1-0.2-0.2-0.4-0.4-0.6c-0.1-0.1-0.2-0.2-0.3-0.3c-0.4-0.4-0.7-0.8-0.9-1.2c-0.1-0.1-0.2-0.2-0.2-0.4c0,0,0,0,0-0.1   c0,0,0-0.1-0.1-0.1c0,0,0-0.1-0.1-0.1c-0.1-0.2-0.1-0.3-0.2-0.5c-0.1-0.2-0.1-0.4-0.2-0.6c0,0,0-0.1,0-0.1c0-0.1,0-0.2,0-0.3v-0.1   c0-0.1,0-0.1,0-0.2c0,0,0-0.1,0-0.1c0-0.1,0-0.2,0-0.3c0.1-0.3,0.2-0.6,0.3-0.8c0,0,0,0,0-0.1c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0-0.1c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0l0,0c0.4-0.5,0.9-0.9,1.5-1.1c0.7-0.2,1.4-0.3,2.1-0.1l0.9-1.7l1.9,1l-0.8,1.6c0.7,0.4,1.2,1,1.6,1.6   l0.2,0.4l-1.8,1.6l-0.3-0.5c-0.1-0.1-0.3-0.4-0.5-0.7c-0.1-0.1-0.3-0.3-0.5-0.4c0,0-0.1-0.1-0.1-0.1c-0.1-0.1-0.3-0.2-0.4-0.2   c0,0-0.1,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0   c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0h0c0,0,0,0-0.1,0h0   c0,0,0,0-0.1,0h0c0,0,0,0,0,0h0c0,0,0,0,0,0h0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c-0.2,0.1-0.4,0.3-0.5,0.5c-0.3,0.5-0.2,0.9,1,2.3c0.8,0.9,1.2,1.6,1.4,2.4   c0.2,0.8,0.1,1.6-0.3,2.5c-0.8,1.5-2.4,2.2-4.2,1.8l-0.8,1.6l-0.1,0.2l-2-1l0.9-1.7c-0.1-0.1-0.2-0.1-0.3-0.2   c-0.3-0.2-0.5-0.4-0.8-0.6c0,0-0.1-0.1-0.1-0.1c-0.3-0.3-0.5-0.6-0.7-1l-0.2-0.4L279.6,26.7z"></path>
                           <path class="st33" d="M278,37c-6.3-3.3-8.8-11.1-5.6-17.4c1.3-2.4,3.2-4.3,5.4-5.5c-2.4,1.2-4.4,3.1-5.7,5.6   c-3.3,6.3-0.8,14.1,5.6,17.4c3.9,2,8.4,1.9,12-0.1C286.2,38.9,281.8,39,278,37z"></path>
                        </g>
                        <g>
                           <linearGradient id="SVGID_00000016062571573767098340000005260099975199807150_" gradientUnits="userSpaceOnUse" x1="79.734" y1="-130.471" x2="91.3973" y2="-130.471" gradientTransform="matrix(0.8624 -0.5063 0.5063 0.8624 260.6894 161.6657)">
                              <stop offset="0" style="stop-color:#FA8460"></stop>
                              <stop offset="4.692155e-02" style="stop-color:#FA9150"></stop>
                              <stop offset="0.1306" style="stop-color:#FBA43A"></stop>
                              <stop offset="0.2295" style="stop-color:#FBB229"></stop>
                              <stop offset="0.3519" style="stop-color:#FCBC1E"></stop>
                              <stop offset="0.5235" style="stop-color:#FCC117"></stop>
                              <stop offset="1" style="stop-color:#FCC315"></stop>
                           </linearGradient>
                           <path style="fill:url(#SVGID_00000016062571573767098340000005260099975199807150_);" d="M268.1,0c-1,0.1-2,0.4-2.8,0.9   c0,0-0.1,0-0.1,0c0,0-0.1,0-0.1,0.1c0,0,0,0,0,0c0,0-0.1,0-0.1,0.1c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0-0.1,0.1   c-0.1,0.1-0.2,0.1-0.3,0.2c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0.1-0.1,0.1   c-0.1,0.1-0.1,0.2-0.2,0.3c0,0,0,0,0,0c-0.1,0.1-0.2,0.2-0.2,0.4c-0.1,0.1-0.2,0.3-0.2,0.4c0,0.1-0.1,0.1-0.1,0.2   c0,0.1-0.1,0.1-0.1,0.2c0,0,0,0.1-0.1,0.1c0,0,0,0,0,0.1c0,0,0,0.1-0.1,0.1c0,0,0,0.1,0,0.1c0,0.1,0,0.1-0.1,0.2c0,0,0,0.1,0,0.1   c-0.1,0.3-0.1,0.5-0.2,0.8c0,0,0,0.1,0,0.1c0,0,0,0.1,0,0.1c0,0.1,0,0.1,0,0.2c0,0,0,0,0,0.1c0,0.1,0,0.1,0,0.2c0,0,0,0,0,0.1   c0,0.1,0,0.1,0,0.2c0,0,0,0,0,0.1c0,0.1,0,0.1,0,0.2c0,0.1,0,0.1,0,0.2c0.2,3.2,2.9,5.7,6.1,5.5c1-0.1,1.9-0.3,2.6-0.8   c0.1,0,0.1-0.1,0.2-0.1c0.1,0,0.1-0.1,0.1-0.1c0,0,0,0,0.1,0c0,0,0.1,0,0.1-0.1c0,0,0,0,0,0c0,0,0.1-0.1,0.1-0.1c0,0,0.1,0,0.1-0.1   c0,0,0.1-0.1,0.1-0.1c0,0,0.1-0.1,0.1-0.1c0,0,0.1-0.1,0.1-0.1c0,0,0.1-0.1,0.1-0.1c0.1-0.1,0.1-0.1,0.2-0.2c0,0,0.1-0.1,0.1-0.1   c0,0,0.1-0.1,0.1-0.1c0,0,0,0,0,0c0,0,0.1-0.1,0.1-0.1c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0-0.1c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0.1-0.1,0.1-0.1c0,0,0,0,0,0c0,0,0.1-0.1,0.1-0.1c0-0.1,0.1-0.1,0.1-0.2c0.1-0.2,0.2-0.4,0.3-0.5c0.1-0.1,0.1-0.2,0.1-0.4   c0-0.1,0.1-0.3,0.1-0.4c0-0.1,0.1-0.3,0.1-0.4c0-0.1,0-0.3,0.1-0.4c0-0.1,0-0.1,0-0.2c0,0,0,0,0,0c0-0.1,0-0.1,0-0.2   c0-0.1,0-0.1,0-0.2c0-0.1,0-0.1,0-0.2c0,0,0-0.1,0-0.1c0-0.1,0-0.1,0-0.2C274.1,2.3,271.3-0.2,268.1,0z M267.1,7.2L267.1,7.2   l0.3,0.2c0.3,0.2,0.7,0.3,1.1,0.3c0.2,0,0.3-0.1,0.4-0.1c0.1-0.1,0.2-0.1,0.2-0.3c0,0,0-0.1,0-0.1c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0-0.2-0.1-0.3-0.3-0.5c0,0,0,0,0,0   c-0.1,0-0.2-0.1-0.3-0.1c-0.1,0-0.1,0-0.2-0.1c-0.2-0.1-0.4-0.2-0.6-0.2c-0.1,0-0.1-0.1-0.2-0.1c0,0,0,0,0,0c0,0,0,0-0.1,0   c0,0,0,0-0.1,0c-0.1,0-0.1-0.1-0.2-0.1c-0.1-0.1-0.1-0.1-0.2-0.2c0,0,0,0,0,0c0,0-0.1-0.1-0.1-0.1l0,0c0,0,0,0,0-0.1c0,0,0,0,0,0   c0,0,0-0.1-0.1-0.1c0-0.1-0.1-0.3-0.1-0.4c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0l0,0   c0-0.3,0.2-0.5,0.3-0.8c0.2-0.2,0.5-0.4,0.8-0.5l0-0.9l1-0.1l0,0.8c0.4,0,0.7,0.1,1,0.3l0.2,0.1l-0.3,1l-0.3-0.1   c-0.1,0-0.2-0.1-0.4-0.1c-0.1,0-0.2,0-0.3-0.1c0,0-0.1,0-0.1,0c-0.1,0-0.1,0-0.2,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0l0,0c0,0,0,0,0,0l0,0c0,0,0,0,0,0l0,0c0,0,0,0,0,0l0,0c0,0,0,0,0,0l0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c-0.1,0.1-0.1,0.2-0.1,0.3c0,0.2,0.1,0.4,0.9,0.7c0.5,0.2,0.8,0.4,1.1,0.6c0.3,0.3,0.4,0.6,0.4,1c0,0.8-0.4,1.4-1.2,1.7l0,0.8   l0,0.1l-1,0.1l0-0.8c-0.1,0-0.1,0-0.2,0c-0.2,0-0.3,0-0.4-0.1c0,0-0.1,0-0.1,0c-0.2-0.1-0.4-0.1-0.5-0.2l-0.2-0.1L267.1,7.2z"></path>
                           <path class="st33" d="M268.8,11.5c-3.2,0.2-6-2.3-6.1-5.5c-0.1-1.2,0.3-2.4,0.9-3.4c-0.7,1-1,2.2-0.9,3.5c0.2,3.2,2.9,5.7,6.1,5.5   c2-0.1,3.7-1.2,4.6-2.8C272.4,10.4,270.7,11.4,268.8,11.5z"></path>
                        </g>
                        <polygon class="st00" points="297.8,43.3 296.4,42.7 294.6,46.7 295.4,41.2 296.8,41.8 298.7,37.9 "></polygon>
                        <polygon class="st00" points="264.2,27.2 262.9,26.4 260.7,30.2 262,24.8 263.3,25.6 265.5,21.8 "></polygon>
                     </svg>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md40 text-center">
                  <div class="preheadline f-20 f-md-21 w500 white-clr lh140">
                     Grab My 20 Exclusive Bonuses Before the Deal Ends…
                  </div>
               </div>
               <div class="col-12 mt-md25 mt20 f-md-45 f-28 w700 text-center white-clr lh140 text-shadow">
                  This Game-Changing Website Builder Lets You Create & Sell Beautiful Local WordPress Websites & Ecom Stores for Any Business in Just 7 Minutes for BIG Profits
               </div>
               <div class="col-12 mt-md35 mt20  text-center">
                  <div class="post-heading f-18 f-md-24 w500 text-center lh140 white-clr text-capitalize">
                     All From Single Dashboard Without Any Tech Skills and Zero Monthly Fees
                  </div>
               </div>
            </div>
         </div>
         <div class="row mt20 mt-md30 mb20 mb-md30">
            <div class="col-12 col-md-10 mx-auto">
               <!-- <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/product-box.webp" class="img-fluid mx-auto d-block" alt="ProductBox"> -->
               <div class="responsive-video">
                  <iframe src="https://webpull.dotcompal.com/video/embed/26sp46ptcb" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                  box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
               </div>
            </div>
         </div>
      </div>
   </div>

   <div class="list-section">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="row header-list-block">
                  <div class="col-12 col-md-6">
                     <div class="f-18 f-md-20 lh140 w400">
                        <ul class="bonus-list pl0">
                           <li class="w600">Tap Into Fast-Growing $284 Billion Industry &amp; Bank BIG </li>
                           <li>Create <span class="w600">Elegant, Ultra-Fast Loading &amp; Mobile Ready Websites</span> in 60 Seconds </li>
                           <li class="w600">400+ Eye-Catching Themes with Done-For-You Content </li>
                           <li><span class="w600">100% SEO Ready Websites</span> for Tons of FREE Search Traffic </li>
                           <li><span class="w600">Get More Likes, Shares and Viral Traffic with</span> In-Built Social Media Tools </li>
                           <li><span class="w600">Accept Payments</span> For Your Services &amp; Products </li>
                        </ul>
                     </div>
                  </div>
                  <div class="col-12 col-md-6">
                     <div class="f-18 f-md-20 lh140 w400">
                        <ul class="bonus-list pl0">
                           <li><span class="w600">Without Touching Any Code,</span> Customize Websites Easily</li>
                           <li>Analytics &amp; Remarketing Ready </li>
                           <li><span class="w600">Newbie-Friendly</span> - No Tech Skills Needed, No Monthly Fees Ever. </li>
                           <li><span class="w600">Commercial License Included</span> to Build an Incredible Income Helping Clients! </li>
                           <li>Bonus Live Training - <span class="w600">Find Tons of Local Clients Hassle-Free</span> </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>

   <!-- CTA Btn Section Start -->
   <div class="dark-cta-sec">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-md-12 col-12 text-center ">
               <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
               <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 orange-clr">TAKE ACTION NOW!</div>
               <div class="f-18 f-md-22 lh140 w400 text-center mt20 white-clr">
                  Use Coupon Code <span class="w700 orange-clr">“WEBPULL”</span> for an Additional <span class="w700 orange-clr">$3 Discount</span> on Commercial Licence
               </div>
            </div>
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="https://warriorplus.com/o2/a/wyl4gg/0" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab WebPull + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
               <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-24 f-22 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
               <div class="countdown counter-white white-clr">
                  <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">00</span><br><span class="f-14 f-md-18 ">Days</span> </div>
                  <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">09</span><br><span class="f-14 f-md-18 w500 ">Hours</span> </div>
                  <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald w700">11</span><br><span class="f-14 f-md-18 w500 ">Mins</span> </div>
                  <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald w700">58</span><br><span class="f-14 f-md-18 w500">Sec</span> </div>
               </div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>
   <!-- CTA Btn Section End -->

   <div class="step-section1">
      <div class="container ">
         <div class="row ">
            <div class="col-12 f-28 f-md-38 w700 lh140 black-clr text-center">
               Create Stunning Websites For <br class="d-none d-md-block">
               You or Clients <span class="orange-clr">in Just 3 Easy Steps</span>
            </div>
            <div class="col-12 mt30 mt-md50">
               <div class="row align-items-center ">
                  <div class="col-12 col-md-6">
                     <div class="step-shapes f-24 f-md-32 w600">
                        Step 1
                     </div>
                     <div class="f-24 f-md-38 w600 lh140 mt15 mt-md25">
                        Choose
                     </div>
                     <div class="f-18 f-md-20 w400 lh140 mt5 mt-md15">
                        Select a theme with done-for-you content from pre-loaded 400+ stunning business themes.
                     </div>
                     <img src="https://cdn.oppyo.com/launches/webpull/special/left-arrow.webp" class="img-fluid d-none d-md-block ms-auto step-arrow1">
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 ">
                     <img src="https://cdn.oppyo.com/launches/webpull/special/choose.webp " class="img-fluid d-block mx-auto ">
                  </div>
               </div>
            </div>
            <div class="col-12 mt30 mt-md50">
               <div class="row align-items-center ">
                  <div class="col-12 col-md-6 order-md-2">
                     <div class="step-shapes f-24 f-md-32 w600">
                        Step 2
                     </div>
                     <div class="f-24 f-md-38 w600 lh140 mt15 mt-md25">
                        Customize
                     </div>
                     <div class="f-18 f-md-20 w400 lh140 mt5 mt-md15">
                        Upload the logo, add your business name & customize it as per your business needs without touching any code. It’s super easy.
                     </div>
                     <img src="https://cdn.oppyo.com/launches/webpull/special/right-arrow.webp" class="img-fluid d-none d-md-block step-arrow1">
                  </div>
                  <div class="col-md-6 col-md-pull-6 mt20 mt-md0 order-md-1 ">
                     <img src="https://cdn.oppyo.com/launches/webpull/special/edit.webp " class="img-fluid d-block mx-auto ">
                  </div>
               </div>
            </div>
            <div class="col-12 mt30 mt-md50">
               <div class="row align-items-center ">
                  <div class="col-12 col-md-6">
                     <div class="step-shapes f-24 f-md-32 w600">
                        Step 3
                     </div>
                     <div class="f-24 f-md-38 w600 lh140 mt15 mt-md25">
                        Publish, Sell, and Profit
                     </div>
                     <div class="f-18 f-md-20 w400 lh140 mt5 mt-md15">
                        Now publish your website with a click to use it for your own business or sell websites to business owners in 1000+ niches.
                     </div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 ">
                     <img src="https://cdn.oppyo.com/launches/webpull/special/publish-report.webp " class="img-fluid d-block mx-auto ">
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>

   <!-- Cta Section -->
   <div class="dark-cta-sec">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-md-12 col-12 text-center ">
               <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
               <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 orange-clr">TAKE ACTION NOW!</div>
               <div class="f-18 f-md-22 lh140 w400 text-center mt20 white-clr">
                  Use Coupon Code <span class="w700 orange-clr">“WEBPULL”</span> for an Additional <span class="w700 orange-clr">$3 Discount</span> on Commercial Licence
               </div>
            </div>
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="https://warriorplus.com/o2/a/wyl4gg/0" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab WebPull + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
               <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-24 f-22 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
               <div class="countdown counter-white white-clr">
                  <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">00</span><br><span class="f-14 f-md-18 ">Days</span> </div>
                  <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">06</span><br><span class="f-14 f-md-18 w500 ">Hours</span> </div>
                  <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald w700">19</span><br><span class="f-14 f-md-18 w500 ">Mins</span> </div>
                  <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald w700">02</span><br><span class="f-14 f-md-18 w500">Sec</span> </div>
               </div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>
   <!-- Cta Section End -->
   <!-- Premium Websites Section -->
   <div class="premium-sec">
      <div class="container">
         <div class="row">
            <div class="col-12 ">
               <div class="f-28 f-md-45 w700 lh140 black-clr text-center">
                  See My REAL Earning from Selling Websites <br class="d-none d-md-block">
                  Built with WebPull (In 60 Seconds)
               </div>
            </div>
            <div class="col-12 mx-auto mt20 mt-md50">
               <img src="https://cdn.oppyo.com/launches/webpull/special/proof6.webp" class="img-fluid d-block mx-auto">
            </div>
         </div>
         <div class="row mt20 mt-md80">
            <div class="col-12 f-md-44 f-28 w500 lh140 black-clr text-center">
               Look At Some More Real-Life Proofs <br>
               <span class="w700">New Freelancers & Agencies are Making Tons of Money Building Simple Websites for Clients</span>
            </div>
         </div>
         <div class="row mt-md70 mt20 align-items-center">
            <div class="col-md-5">
               <img src="https://cdn.oppyo.com/launches/webpull/special/boomer-smith.webp" alt="" class="img-fluid d-block mx-auto">
            </div>
            <div class="col-md-7 f-md-20 f-18 w400 mt-md0 mt20">
               <span class="f-22 f-md-28 w700">Boomer Smith has made $20,400 on Fiverr in just 4 months of his start in Dec’21 & still counting. </span>
               <br><br>
               After quitting his Tele calling Job which he never liked, he started searching for any Freelancing opportunity on the internet where he encountered Fiverr and found 2 Big words Wed Design, and WordPress.
               <br><br>
               Now, he is charging $1,200 per project and has completed almost 17 projects in just 4 months which made him $20,400 in income.
            </div>
         </div>
         <div class="row mt-md70 mt20 align-items-center">
            <div class="col-md-5 order-md-2">
               <img src="https://cdn.oppyo.com/launches/webpull/special/brain.webp" alt="" class="img-fluid d-block mx-auto">
            </div>
            <div class="col-md-7 f-md-20 f-18 w400 mt-md0 mt20 order-md-1">
               <span class="f-22 f-md-28 w700"> Meet Brain C. ‘Rising Talent’ earned almost $17,495 in just 47 Days on Upwork. </span>

               <br><br>

               His journey started on Upwork with Graphic Designing in 2019. But as he came to know about many newbies earning huge just by servicing WordPress Website development, he gave it a shot and started earning more than his graphic design service.
               <br><br>
               He started his Website Development Services for the Lawyers & charges them $3,499 per website on Upwork, which Lawyers are happy to pay.
            </div>
         </div>
         <div class="row mt-md70 mt20 align-items-center">
            <div class="col-md-6">
               <img src="https://cdn.oppyo.com/launches/webpull/special/bright.webp" alt="" class="img-fluid d-block mx-auto">
            </div>
            <div class="col-md-6 f-md-20 f-18 w400 mt-md0 mt20">
               <span class="f-22 f-md-28 w700">Bright Dock has made almost $183k (till yet) on Freelancer </span>
               <br><br>
               He has completed Over 180 successfully completed Freelancer projects in under 2 years.
               <br><br>
               He started his WordPress Designing Freelancing journey 2 years ago on Freelancer and now has a freelancing team of 40 digital designers to work for him, so he just focuses on growth. He is charging $100 per Hr. and people are ready to pay for his awesome job.
            </div>
         </div>
      </div>
   </div>
   <!-- Premium Websites Section End -->

   <!-- Testimonial Section Start -->
   <div class="testimonial-section2">
      <div class="container ">
         <div class="row ">
            <div class="col-12 f-28 f-md-45 w700 lh140 black-clr text-center">
               Even Our Beta Users Are Getting Amazing <br class="d-none d-md-block">
               Results With Stunning Websites
            </div>
         </div>
         <div class="row row-cols-md-2 row-cols-1 gx4">
         <div class="col mt50">
               <div class="single-testimonial">
                  <p>
                     After losing all my assets and job in the pandemic, WEBPULL came out as a God Gift for me. I never knew that for a newbie like me, building a professional website could be so easy and life changing. <br><br>

                     WEBPULL is the easiest and most efficient way to start your own Freelancing Work on Fiverr, Upwork, etc, and tap into big Industry. <span class="w600">Today, I am making an average income of $4700/month & that too in just 3 months. </span>
                  </p>
                  <div class="client-info">
                     <img src="https://cdn.oppyo.com/launches/webpull/special/lewis-grey.webp" class="img-fluid d-block mx-auto">
                     <div class="client-details">
                        Lewis Grey
                     </div>
                  </div>
               </div>
            </div>
            <div class="col mt50">
               <div class="single-testimonial">
                  <p>
                     WEBPULL is one of the Best Website Builders I have come across. Being a freelancer, it was never easy for me to build websites for my clients from different businesses. And I was able to serve hardly 2-3 clients monthly, and cost more due to various plugins I must buy.  <br><br>
                     <span class="w600">But DANG! WEBPULL has changed it over, now I am serving 15 -20 clients per month and making upto $17,000 monthly,</span> with no additional cost of website development, and that is 4X more profits for me. 
                  </p>
                  <div class="client-info">
                     <img src="https://cdn.oppyo.com/launches/webpull/special/john-warner.webp" class="img-fluid d-block mx-auto">
                     <div class="client-details">
                        John Warner
                     </div>
                  </div>
               </div>
            </div>
            <div class="col mt50">
               <div class="single-testimonial">
                  <p>
                  I started my freelance website developer journey 4 months back, but being a newbie, I was struggling in creating a good website. But thanks for the early access to WEBPULL team, now <span class="w600">I can make a stunning, highly professional website quick & easy and able to serve more clients who happily pay me $4000 - $5000 each. </span>
                  </p>
                  <div class="client-info">
                     <img src="https://cdn.oppyo.com/launches/webpull/special/stuart-johnson.webp" class="img-fluid d-block mx-auto">
                     <div class="client-details">
                        Stuart Johnson
                     </div>
                  </div>
               </div>
            </div>
            <div class="col mt50">
               <div class="single-testimonial">
                  <p>
                     WEBPULL helped me to create my first real estate website in just 30 minutes and the best part is that I am not a tech geek.  
                     <br><br>
                     <span class="w600">Now, I’m able to get more FREE traffic & targeted leads online through my website. It helped me reach to my prospects in much lower cost & boosted profits by almost 3 times.</span> This is something that I really won’t forget in my life. 
                  </p>
                  <div class="client-info">
                     <img src="https://cdn.oppyo.com/launches/webpull/special/chuk-akspan.webp" class="img-fluid d-block mx-auto">
                     <div class="client-details">
                        Chuk Akspan
                     </div>
                  </div>
               </div>
            </div>
         </div>

      </div>
   </div>
   <!-- Testimonial Section End -->

   <!-- Cta Section -->
   <div class="dark-cta-sec">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-md-12 col-12 text-center ">
               <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
               <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 orange-clr">TAKE ACTION NOW!</div>
               <div class="f-18 f-md-22 lh140 w400 text-center mt20 white-clr">
                  Use Coupon Code <span class="w700 orange-clr">“WEBPULL”</span> for an Additional <span class="w700 orange-clr">$3 Discount</span> on Commercial Licence
               </div>
            </div>
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="https://warriorplus.com/o2/a/wyl4gg/0" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab WebPull + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
               <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-24 f-22 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
               <div class="countdown counter-white white-clr">
                  <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">00</span><br><span class="f-14 f-md-18 ">Days</span> </div>
                  <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">06</span><br><span class="f-14 f-md-18 w500 ">Hours</span> </div>
                  <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald w700">18</span><br><span class="f-14 f-md-18 w500 ">Mins</span> </div>
                  <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald w700">10</span><br><span class="f-14 f-md-18 w500">Sec</span> </div>
               </div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>
   <!-- Cta Section End -->
   <!-- Over Section Start -->
   <div class="over-section">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <img src="https://cdn.oppyo.com/launches/webpull/special/over-img.webp" alt="" class="img-fluid d-block mx-auto">
               <!-- <img src="https://cdn.oppyo.com/launches/webpull/special/siteefy.webp" alt="" class="img-fluid d-block mx-auto mt-md50 mt20"> -->
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md70">
            <div class="col-12">
               <div class="f-22 f-md-24 w400 lh140 white-clr text-center">
                  In this never-ending pandemic era, every business needs to switch to digital to survive,<br class="d-none d-md-block"> and having a website for any business is not an option anymore…
               </div>
               <div class="f-28 f-md-45 w700 orange-clr  text-center my10">
                  It’s a Compulsion Today!
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Over Section End -->
   <!-- Research Section Start -->
   <div class="research-section">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 f-24 f-md-36 w700 lh140 black-clr text-center">
               No Business Owner Would Like To Lose Those Visitors<br class="d-none d-md-block"> & Big Sales From All The Digital Sources
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md70">
            <div class="col-12 col-md-6 f-22 f-md-26 w600 lh140 black-clr order-md-2">
               As When You Have an SEO & Mobile Optimized Website Then You Seamlessly Get Customers from All 360-Degree Directions.
            </div>
            <div class="col-12 col-md-6 order-md-1 mt20 mt-md0">
               <img src="https://cdn.oppyo.com/launches/webpull/special/f-image.webp " class="img-fluid d-block mx-autos ms-md-auto" />
            </div>
         </div>
         <div class="row mt20 mt-md0 align-items-center">
            <div class="col-12 col-md-5 f-22 f-md-26 w600 lh140 black-clr">
               <span class="d-flex justify-content-md-end">And You Drive Non-Stop Traffic, Leads & Profits 24 by 7 - 100% Hands-Free </span>
            </div>
            <div class="col-12 col-md-7 mt20 mt-md50">
               <img src="https://cdn.oppyo.com/launches/webpull/special/h-image.webp " class="img-fluid d-block mx-autos" />
            </div>
         </div>
         <div class="row mt20 mt-md50">
            <div class="col-12 f-20 f-md-24 w500 lh140 black-clr text-center"><span class="w700">So, when it comes to doing a Business, Websites are the Present and The Future! </span> <br class="d-none d-md-block">In Easy Words, Every Business Need a Website Today!
            </div>
            <div class="col-12 mt-md50 mt20 f-md-28 f-22 w600 blue-clr lh140 text-center">
               And that opens a door to
            </div>
            <div class="col-12 mt-md30 mt20 f-md-50 f-28 w700 white-clr lh140 text-center">
               <div class="over-shape">
                  BIG $284 Billion Opportunity.
               </div>
            </div>
         </div>
         <div class="row mt30 mt-md65">
            <div class="f-md-28 f-22 w400 lh140 black-clr  text-center">
               <span class="w700 f-md-45 f-28 lh140"> Now You’re Probably Wondering? </span><br> “Can I Make a Full-Time Income?”
            </div>
         </div>
      </div>
   </div>
   <!-- Research Section End -->
   <div class="course-section">
      <div class="container">
         <div class="row">
            <div class="col-12 f-20 f-md-28 w500 lh140 text-center">
               <span class="w700 f-24 f-md-36"> Answer Is - Of Course, You Can! </span><br> See How Newbies Made Thousands of Dollars from Website Services
            </div>
            <div class="col-12 col-md-12 mx-auto mt20 mt-md50">
               <div class="row">
                  <div class="col-12 col-md-6 mt-md0 p-md-0">
                     <img src="https://cdn.oppyo.com/launches/webpull/special/proofimg1.webp" class="img-fluid d-block mx-auto">
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0  p-md-0">
                     <img src="https://cdn.oppyo.com/launches/webpull/special/proofimg2.webp" class="img-fluid d-block mx-auto">
                  </div>
                  <div class="col-12 col-md-6 p-md-0">
                     <img src="https://cdn.oppyo.com/launches/webpull/special/proofimg3.webp" class="img-fluid d-block mx-auto">
                  </div>
                  <div class="col-12 col-md-6 p-md-0">
                     <img src="https://cdn.oppyo.com/launches/webpull/special/proofimg4.webp" class="img-fluid d-block mx-auto">
                  </div>
               </div>
            </div>
            <div class="col-12 f-20 f-md-28 w400 lh140 text-center mt20 mt-md30">
               Now is the perfect time to get in and profit from this exponential growth!
            </div>
         </div>
         <div class="row mt30 mt-md50">
            <div class="col-12 text-center">
               <div class="presenting-shape f-md-45 f-28 w700 text-center white-clr lh140 text-uppercase"> IMPRESSIVE, RIGHT? </div>
            </div>
         </div>
         <div class="row mt20 mt-md50">
            <div class="col-12  f-24 f-md-36 lh140 w600 text-center black-clr lh140">
               So, how would you like to get your share starting today?
            </div>
         </div>
      </div>
   </div>
   <!-- Amit Pareek Section End -->

   <div class="next-gen-sec" id="product">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="presenting-shape2 f-md-32 f-24 w600 text-center white-clr lh140 text-uppercase">
                  Presenting... </div>
            </div>
            <div class="col-12 mt-md20 mt20 text-center">
               <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 394.2 100" style="enable-background:new 0 0 394.2 100; max-height:111px" xml:space="preserve">
                  <style type="text/css">
                     .st00 {
                        fill: #FFFFFF;
                     }

                     .st11 {
                        fill: url(#SVGID_1_);
                     }

                     .st22 {
                        fill: url(#SVGID_00000021093791325859232720000015953972570224053676_);
                     }

                     .st33 {
                        fill: #FFC802;
                     }

                     .st44 {
                        fill: url(#SVGID_00000003068462962921929030000010945183465240898967_);
                     }
                  </style>
                  <path class="st00" d="M117.7,37.9l-5,12.4H92.9v12.4h18.6l-5,12.4H92.9v12.4h24.8l-5,12.5H92.9H80.5V75.1V62.7V37.9H117.7z"></path>
                  <path class="st00" d="M323.2,37.9v49.7H348V100h-37.3V37.9H323.2z"></path>
                  <path class="st00" d="M369.4,37.9v49.7h24.8V100h-37.3V37.9H369.4z"></path>
                  <g>
                     <g>
                        <polygon class="st00" points="59.1,71.4 52.4,54.4 59.1,37.9 72.6,37.9   "></polygon>
                        <polygon class="st00" points="29.4,54.7 36.3,37.9 54.5,82.9 47.7,100   "></polygon>
                        <polygon class="st00" points="24.8,100 31.6,82.8 13.4,37.9 0,37.9   "></polygon>
                     </g>
                  </g>
                  <path class="st00" d="M222.8,44.3c-4.3-4.3-9.4-6.4-15.4-6.4h-9.3h-12.5v21.7v21.7V100h12.5V81.3h0V68.9h0v-9.3v-9.3h9.3  c2.6,0,4.8,0.9,6.6,2.8c1.8,1.8,2.7,4,2.7,6.6c0,2.6-0.9,4.8-2.7,6.6c-1.8,1.8-4,2.7-6.6,2.7h-0.7v12.4h0.7c6,0,11.1-2.1,15.4-6.4  c4.3-4.3,6.4-9.4,6.4-15.4C229.2,53.6,227.1,48.5,222.8,44.3z"></path>
                  <rect x="252.1" y="31.8" transform="matrix(0.4226 -0.9063 0.9063 0.4226 114.0209 255.0236)" class="st00" width="10.2" height="12.5"></rect>
                  <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="249.3741" y1="97.3262" x2="270.7371" y2="53.5256">
                     <stop offset="0" style="stop-color:#FA8460"></stop>
                     <stop offset="1" style="stop-color:#FCB121"></stop>
                  </linearGradient>
                  <path class="st11" d="M271.7,80.3c-1.4,3.1-3.8,5.2-7,6.4c-3.2,1.2-6.4,1-9.5-0.4c-3.1-1.4-5.2-3.8-6.4-7c-1.2-3.2-1-6.4,0.4-9.5  l9.9-21.2l-11.3-5.3l-9.9,21.2c-2.9,6.2-3.2,12.6-0.8,19c2.3,6.5,6.6,11.1,12.9,14c6.2,2.9,12.5,3.2,19,0.8  c6.5-2.3,11.2-6.6,14.1-12.8l9.9-21.2l-11.3-5.3L271.7,80.3z"></path>
                  <rect x="285.9" y="47.6" transform="matrix(0.4226 -0.9063 0.9063 0.4226 119.2494 294.738)" class="st00" width="10.2" height="12.5"></rect>
                  <path class="st00" d="M173.3,76.3c-0.8-1.7-1.7-3.2-2.9-4.5c-1.2-1.3-2.5-2.3-4-3.1c-1.5-0.8-3-1.2-4.5-1.3c1.5-0.1,2.8-0.5,4.1-1.3  c1.2-0.8,2.3-1.8,3.2-3c0.9-1.2,1.6-2.5,2.1-3.9c0.5-1.4,0.8-2.8,0.8-4c0-3.5-0.5-6.4-1.4-8.6c-1-2.2-2.3-4-3.9-5.3  c-1.6-1.3-3.6-2.2-5.8-2.6c-2.2-0.5-4.6-0.7-7-0.7h-27.3v10.5h11.9v0h15.4c0.8,0,1.7,0.2,2.4,0.6c0.8,0.4,1.4,0.9,2,1.5  c0.6,0.6,1,1.4,1.4,2.2c0.3,0.9,0.5,1.7,0.5,2.6c0,0.9-0.2,1.8-0.5,2.8c-0.3,0.9-0.8,1.8-1.4,2.5c-0.6,0.7-1.3,1.3-2.2,1.8  c-0.9,0.5-1.9,0.7-3.1,0.7h-9.1v10.5h10c2.7,0,4.9,0.7,6.4,2.1c1.6,1.4,2.3,3.2,2.3,5.3c0,1-0.2,2-0.6,3c-0.4,1-0.9,1.9-1.6,2.7  c-0.7,0.8-1.5,1.5-2.4,2c-0.9,0.5-1.9,0.7-2.9,0.7h-16.8h0V73.7h0V63.2h0v-9.3h-11.9V100h9.6h2.3h16.8c2.7,0,5.2-0.3,7.5-0.9  c2.3-0.6,4.4-1.6,6.2-3.1c1.8-1.4,3.1-3.3,4.2-5.7c1-2.3,1.5-5.2,1.5-8.6C174.4,79.8,174,78,173.3,76.3z"></path>
                  <g>
                     <linearGradient id="SVGID_00000142155450205615942230000010409392281687221429_" gradientUnits="userSpaceOnUse" x1="270.7616" y1="25.6663" x2="296.63" y2="25.6663">
                        <stop offset="0" style="stop-color:#FA8460"></stop>
                        <stop offset="4.692155e-02" style="stop-color:#FA9150"></stop>
                        <stop offset="0.1306" style="stop-color:#FBA43A"></stop>
                        <stop offset="0.2295" style="stop-color:#FBB229"></stop>
                        <stop offset="0.3519" style="stop-color:#FCBC1E"></stop>
                        <stop offset="0.5235" style="stop-color:#FCC117"></stop>
                        <stop offset="1" style="stop-color:#FCC315"></stop>
                     </linearGradient>
                     <path style="fill:url(#SVGID_00000142155450205615942230000010409392281687221429_);" d="M289.6,14.2c-2.1-1.1-4.3-1.5-6.5-1.4   c-0.1,0-0.1,0-0.2,0c-0.1,0-0.2,0-0.2,0c0,0-0.1,0-0.1,0c-0.1,0-0.2,0-0.2,0c-0.1,0-0.2,0-0.3,0c-0.1,0-0.2,0-0.2,0   c-0.3,0-0.5,0.1-0.8,0.1c-0.1,0-0.2,0-0.3,0.1c-0.1,0-0.2,0-0.3,0.1c-0.1,0-0.3,0.1-0.4,0.1c-0.1,0-0.2,0-0.2,0.1   c-0.1,0-0.2,0.1-0.3,0.1c-0.2,0.1-0.5,0.2-0.7,0.3c0,0,0,0,0,0c-0.3,0.1-0.6,0.3-0.9,0.4c-0.3,0.1-0.6,0.3-0.9,0.5   c-0.1,0.1-0.3,0.2-0.4,0.2c-0.1,0.1-0.2,0.2-0.4,0.2c-0.1,0.1-0.2,0.1-0.2,0.2c0,0-0.1,0-0.1,0.1c-0.1,0.1-0.2,0.1-0.2,0.2   c-0.1,0-0.1,0.1-0.2,0.2c-0.1,0.1-0.2,0.2-0.3,0.3c-0.1,0-0.1,0.1-0.2,0.2c-0.5,0.4-0.9,0.9-1.3,1.4c-0.1,0.1-0.1,0.1-0.2,0.2   c0,0-0.1,0.1-0.1,0.1c-0.1,0.1-0.1,0.2-0.2,0.3c0,0-0.1,0.1-0.1,0.1c-0.1,0.1-0.1,0.2-0.2,0.3c0,0,0,0.1-0.1,0.1   c-0.1,0.1-0.1,0.2-0.2,0.3c0,0,0,0.1-0.1,0.1c-0.1,0.1-0.1,0.2-0.2,0.3c-0.1,0.1-0.2,0.3-0.2,0.4c-3.3,6.3-0.8,14.1,5.6,17.4   c1.9,1,3.9,1.4,5.9,1.4c0.1,0,0.3,0,0.4,0c0.1,0,0.3,0,0.4,0c0.1,0,0.1,0,0.2,0c0.1,0,0.2,0,0.3,0c0,0,0,0,0.1,0c0.1,0,0.2,0,0.3,0   c0.1,0,0.2,0,0.2,0c0.1,0,0.2,0,0.3-0.1c0.1,0,0.3,0,0.4-0.1c0.1,0,0.3-0.1,0.4-0.1c0.1,0,0.2-0.1,0.3-0.1c0.2,0,0.4-0.1,0.5-0.2   c0.1,0,0.3-0.1,0.4-0.1c0.1,0,0.3-0.1,0.4-0.1c0,0,0,0,0,0c0.1,0,0.2-0.1,0.4-0.1c0,0,0,0,0,0c0,0,0.1,0,0.1,0c0,0,0.1,0,0.1-0.1   c0,0,0,0,0.1,0c0,0,0.1,0,0.1,0c0,0,0,0,0.1,0c0.1,0,0.2-0.1,0.3-0.2c0,0,0,0,0.1,0c0.1-0.1,0.2-0.1,0.3-0.2   c0.1-0.1,0.3-0.1,0.4-0.2c0.4-0.2,0.7-0.5,1.1-0.7c0.2-0.2,0.5-0.4,0.7-0.6c0.2-0.2,0.4-0.4,0.7-0.6c0.2-0.2,0.4-0.4,0.6-0.6   c0.2-0.2,0.4-0.5,0.6-0.7c0.1-0.1,0.2-0.2,0.3-0.4c0,0,0,0,0,0c0.1-0.1,0.2-0.2,0.3-0.4c0.1-0.1,0.2-0.3,0.2-0.4   c0.1-0.1,0.1-0.2,0.2-0.3c0-0.1,0.1-0.1,0.1-0.2c0.1-0.1,0.1-0.2,0.2-0.3C298.5,25.3,296,17.5,289.6,14.2z M279.6,26.7L279.6,26.7   l0.3,0.6c0.4,0.7,1,1.3,1.8,1.7c0.3,0.2,0.7,0.3,1,0.3c0.3,0,0.5-0.1,0.7-0.2c0.1,0,0.1-0.1,0.2-0.2c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0-0.1c0.2-0.3,0.2-0.7-0.1-1.3c0,0,0,0,0,0   c-0.1-0.2-0.2-0.4-0.4-0.6c-0.1-0.1-0.2-0.2-0.3-0.3c-0.4-0.4-0.7-0.8-0.9-1.2c-0.1-0.1-0.2-0.2-0.2-0.4c0,0,0,0,0-0.1   c0,0,0-0.1-0.1-0.1c0,0,0-0.1-0.1-0.1c-0.1-0.2-0.1-0.3-0.2-0.5c-0.1-0.2-0.1-0.4-0.2-0.6c0,0,0-0.1,0-0.1c0-0.1,0-0.2,0-0.3v-0.1   c0-0.1,0-0.1,0-0.2c0,0,0-0.1,0-0.1c0-0.1,0-0.2,0-0.3c0.1-0.3,0.2-0.6,0.3-0.8c0,0,0,0,0-0.1c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0-0.1c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0l0,0c0.4-0.5,0.9-0.9,1.5-1.1c0.7-0.2,1.4-0.3,2.1-0.1l0.9-1.7l1.9,1l-0.8,1.6c0.7,0.4,1.2,1,1.6,1.6   l0.2,0.4l-1.8,1.6l-0.3-0.5c-0.1-0.1-0.3-0.4-0.5-0.7c-0.1-0.1-0.3-0.3-0.5-0.4c0,0-0.1-0.1-0.1-0.1c-0.1-0.1-0.3-0.2-0.4-0.2   c0,0-0.1,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0   c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0h0c0,0,0,0-0.1,0h0   c0,0,0,0-0.1,0h0c0,0,0,0,0,0h0c0,0,0,0,0,0h0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c-0.2,0.1-0.4,0.3-0.5,0.5c-0.3,0.5-0.2,0.9,1,2.3c0.8,0.9,1.2,1.6,1.4,2.4   c0.2,0.8,0.1,1.6-0.3,2.5c-0.8,1.5-2.4,2.2-4.2,1.8l-0.8,1.6l-0.1,0.2l-2-1l0.9-1.7c-0.1-0.1-0.2-0.1-0.3-0.2   c-0.3-0.2-0.5-0.4-0.8-0.6c0,0-0.1-0.1-0.1-0.1c-0.3-0.3-0.5-0.6-0.7-1l-0.2-0.4L279.6,26.7z"></path>
                     <path class="st33" d="M278,37c-6.3-3.3-8.8-11.1-5.6-17.4c1.3-2.4,3.2-4.3,5.4-5.5c-2.4,1.2-4.4,3.1-5.7,5.6   c-3.3,6.3-0.8,14.1,5.6,17.4c3.9,2,8.4,1.9,12-0.1C286.2,38.9,281.8,39,278,37z"></path>
                  </g>
                  <g>
                     <linearGradient id="SVGID_00000016062571573767098340000005260099975199807150_" gradientUnits="userSpaceOnUse" x1="79.734" y1="-130.471" x2="91.3973" y2="-130.471" gradientTransform="matrix(0.8624 -0.5063 0.5063 0.8624 260.6894 161.6657)">
                        <stop offset="0" style="stop-color:#FA8460"></stop>
                        <stop offset="4.692155e-02" style="stop-color:#FA9150"></stop>
                        <stop offset="0.1306" style="stop-color:#FBA43A"></stop>
                        <stop offset="0.2295" style="stop-color:#FBB229"></stop>
                        <stop offset="0.3519" style="stop-color:#FCBC1E"></stop>
                        <stop offset="0.5235" style="stop-color:#FCC117"></stop>
                        <stop offset="1" style="stop-color:#FCC315"></stop>
                     </linearGradient>
                     <path style="fill:url(#SVGID_00000016062571573767098340000005260099975199807150_);" d="M268.1,0c-1,0.1-2,0.4-2.8,0.9   c0,0-0.1,0-0.1,0c0,0-0.1,0-0.1,0.1c0,0,0,0,0,0c0,0-0.1,0-0.1,0.1c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0-0.1,0.1   c-0.1,0.1-0.2,0.1-0.3,0.2c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0.1-0.1,0.1   c-0.1,0.1-0.1,0.2-0.2,0.3c0,0,0,0,0,0c-0.1,0.1-0.2,0.2-0.2,0.4c-0.1,0.1-0.2,0.3-0.2,0.4c0,0.1-0.1,0.1-0.1,0.2   c0,0.1-0.1,0.1-0.1,0.2c0,0,0,0.1-0.1,0.1c0,0,0,0,0,0.1c0,0,0,0.1-0.1,0.1c0,0,0,0.1,0,0.1c0,0.1,0,0.1-0.1,0.2c0,0,0,0.1,0,0.1   c-0.1,0.3-0.1,0.5-0.2,0.8c0,0,0,0.1,0,0.1c0,0,0,0.1,0,0.1c0,0.1,0,0.1,0,0.2c0,0,0,0,0,0.1c0,0.1,0,0.1,0,0.2c0,0,0,0,0,0.1   c0,0.1,0,0.1,0,0.2c0,0,0,0,0,0.1c0,0.1,0,0.1,0,0.2c0,0.1,0,0.1,0,0.2c0.2,3.2,2.9,5.7,6.1,5.5c1-0.1,1.9-0.3,2.6-0.8   c0.1,0,0.1-0.1,0.2-0.1c0.1,0,0.1-0.1,0.1-0.1c0,0,0,0,0.1,0c0,0,0.1,0,0.1-0.1c0,0,0,0,0,0c0,0,0.1-0.1,0.1-0.1c0,0,0.1,0,0.1-0.1   c0,0,0.1-0.1,0.1-0.1c0,0,0.1-0.1,0.1-0.1c0,0,0.1-0.1,0.1-0.1c0,0,0.1-0.1,0.1-0.1c0.1-0.1,0.1-0.1,0.2-0.2c0,0,0.1-0.1,0.1-0.1   c0,0,0.1-0.1,0.1-0.1c0,0,0,0,0,0c0,0,0.1-0.1,0.1-0.1c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0-0.1c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0.1-0.1,0.1-0.1c0,0,0,0,0,0c0,0,0.1-0.1,0.1-0.1c0-0.1,0.1-0.1,0.1-0.2c0.1-0.2,0.2-0.4,0.3-0.5c0.1-0.1,0.1-0.2,0.1-0.4   c0-0.1,0.1-0.3,0.1-0.4c0-0.1,0.1-0.3,0.1-0.4c0-0.1,0-0.3,0.1-0.4c0-0.1,0-0.1,0-0.2c0,0,0,0,0,0c0-0.1,0-0.1,0-0.2   c0-0.1,0-0.1,0-0.2c0-0.1,0-0.1,0-0.2c0,0,0-0.1,0-0.1c0-0.1,0-0.1,0-0.2C274.1,2.3,271.3-0.2,268.1,0z M267.1,7.2L267.1,7.2   l0.3,0.2c0.3,0.2,0.7,0.3,1.1,0.3c0.2,0,0.3-0.1,0.4-0.1c0.1-0.1,0.2-0.1,0.2-0.3c0,0,0-0.1,0-0.1c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0-0.2-0.1-0.3-0.3-0.5c0,0,0,0,0,0   c-0.1,0-0.2-0.1-0.3-0.1c-0.1,0-0.1,0-0.2-0.1c-0.2-0.1-0.4-0.2-0.6-0.2c-0.1,0-0.1-0.1-0.2-0.1c0,0,0,0,0,0c0,0,0,0-0.1,0   c0,0,0,0-0.1,0c-0.1,0-0.1-0.1-0.2-0.1c-0.1-0.1-0.1-0.1-0.2-0.2c0,0,0,0,0,0c0,0-0.1-0.1-0.1-0.1l0,0c0,0,0,0,0-0.1c0,0,0,0,0,0   c0,0,0-0.1-0.1-0.1c0-0.1-0.1-0.3-0.1-0.4c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0l0,0   c0-0.3,0.2-0.5,0.3-0.8c0.2-0.2,0.5-0.4,0.8-0.5l0-0.9l1-0.1l0,0.8c0.4,0,0.7,0.1,1,0.3l0.2,0.1l-0.3,1l-0.3-0.1   c-0.1,0-0.2-0.1-0.4-0.1c-0.1,0-0.2,0-0.3-0.1c0,0-0.1,0-0.1,0c-0.1,0-0.1,0-0.2,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0l0,0c0,0,0,0,0,0l0,0c0,0,0,0,0,0l0,0c0,0,0,0,0,0l0,0c0,0,0,0,0,0l0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c-0.1,0.1-0.1,0.2-0.1,0.3c0,0.2,0.1,0.4,0.9,0.7c0.5,0.2,0.8,0.4,1.1,0.6c0.3,0.3,0.4,0.6,0.4,1c0,0.8-0.4,1.4-1.2,1.7l0,0.8   l0,0.1l-1,0.1l0-0.8c-0.1,0-0.1,0-0.2,0c-0.2,0-0.3,0-0.4-0.1c0,0-0.1,0-0.1,0c-0.2-0.1-0.4-0.1-0.5-0.2l-0.2-0.1L267.1,7.2z"></path>
                     <path class="st33" d="M268.8,11.5c-3.2,0.2-6-2.3-6.1-5.5c-0.1-1.2,0.3-2.4,0.9-3.4c-0.7,1-1,2.2-0.9,3.5c0.2,3.2,2.9,5.7,6.1,5.5   c2-0.1,3.7-1.2,4.6-2.8C272.4,10.4,270.7,11.4,268.8,11.5z"></path>
                  </g>
                  <polygon class="st00" points="297.8,43.3 296.4,42.7 294.6,46.7 295.4,41.2 296.8,41.8 298.7,37.9 "></polygon>
                  <polygon class="st00" points="264.2,27.2 262.9,26.4 260.7,30.2 262,24.8 263.3,25.6 265.5,21.8 "></polygon>
               </svg>
            </div>
            <div class="f-md-36 f-22 w600 white-clr lh140 col-12 mt20 mt-md40 text-center">
               A Brand-New Technology Lets You Creates &amp; Sell Stunning <br class="d-none d-md-block"> Websites for BIG Profits In 1000+ Niches In 60 Seconds Flat…
            </div>
            <div class="f-18 f-md-22 lh140 w500 text-center white-clr mt20">
               Replace your old-school website builder with the amazing solution that<br class="d-none d-md-block"> helps you create lightning-fast, high converting and SEO-ready websites.
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md50 gx-4">
            <div class="col-12 col-md-6">
               <img src="https://cdn.oppyo.com/launches/webpull/jv/product-box.webp" class="img-fluid d-block mx-auto" alt="Product">
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <div class="proudly-list-bg">
                  <div class="col-12">
                     <ul class="proudly-tick pl0 m0 f-18 f-md-18 w400 black-clr lh160">
                        <li><span class="w600 f-20 f-md-22">Create Any Type Of Site </span> <br>
                           Create Affiliate Niche Sites, Local Sites, Marketplace, E-Com Stores, or Blogs Easily
                        </li>
                        <li><span class="w600 f-20 f-md-22">Super-Customizable &amp; Easy To Use
                           </span> <br>
                           Preloaded with 400+ Done-For-You Stunning Themes To Create Site In Any Niche </li>
                        <li><span class="w600 f-20 f-md-22">No Worries of Paying Monthly</span> <br>
                           During This Launch Special Deal, Get All Benefits At Limited Low One-Time-Fee.
                        </li>
                        <li><span class="w600 f-20 f-md-22">No Limits - Use for Yourself Or Clients
                           </span> <br>
                           Sell Your Own Products, Services or Affiliate Offers or Charge Your Clients for Elegant Websites.
                        </li>
                        <li style="padding-bottom:0px"><span class="w600 f-20 f-md-22">50+ More Features </span> <br>
                           We’ve Left No Stone Unturned to Give You an Unmatched Experience
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>

   <!-------Exclusive Bonus----------->
   <!-- Bonus Section Header Start -->
   <div class="bonus-header">
      <div class="container">
         <div class="row">
            <div class="col-12 col-md-10 mx-auto heading-bg text-center">
               <div class="f-24 f-md-36 lh140 w700"> When You Purchase WebPull, You Also Get <br class="hidden-xs"> Instant Access To These 20 Exclusive Bonuses</div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus Section Header End -->

   <!-- Bonus #1 Section Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 1</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/bonus1.webp" class="img-fluid mx-auto d-block" alt="Bonus1">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">Magic Video Templates Review Pack</div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>
                           There’s no secret in the fact that videos are the #1 source to boost customer attention& engagement.
                        </li>
                        <li>
                           Using Magic Video, you can easily create your own animated video in just minutes with 100+slides to choose from.
                        </li>
                        <li>
                           This Bonus with WebPull’s DFY marketing materials will instantly change your marketing presentation to be more customer-centric & drive better results from your campaigns.
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #1 Section End -->

   <!-- Bonus #2 Section Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 2</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30">
               <div class="row">
                  <div class="col-md-5 col-12 order-md-2">
                     <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/bonus2.webp" class="img-fluid mx-auto d-block" alt="Bonus2">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md0">
                        Facebook Webinar Pro Plugin
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>
                           With this powerful WordPress plugin you can create amazing Facebook webinar landing pages & drive maximum traction for your offers.
                        </li>
                        <li>
                           Using this Bonus, you can collect unlimited leads inside WordPress and shoot them targeted
                        </li>
                        <li>
                           emails on automation using WebPull’s Autoresponder Integration
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #2 Section End -->

   <!-- Bonus #3 Section Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 3</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/bonus3.webp" class="img-fluid mx-auto d-block " alt="Bonus3">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                        WordPress Website Security
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>This specific training course is designed to help you understand how to secure and protect your valuable WordPress site.</li>
                        <li>This Bonus is a great asset to use with WebPull as it helps to secure your business website made for marketing purposes, payment transactions & various business operations.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #3 Section End -->

   <!-- Bonus #4 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 4</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12 order-md-2">
                     <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/bonus4.webp" class="img-fluid mx-auto d-block " alt="Bonus4">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                        Audio Compression Magic
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>
                           Magically reduces the file size of your digital audio products up to an astonishing 82% while maintaining top sound quality.
                        </li>
                        <li>
                           Use this bonus to deliver light sized high quality professional marketing voice overs given in WebPull.
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #4 End -->

   <!-- Bonus #5 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 5</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/bonus5.webp" class="img-fluid mx-auto d-block " alt="Bonus5">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                        Lead Book Generator
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>
                           With this powerful plugin, you can easily integrate Facebook Lead Ads with your autoresponder and have your leads added to your mailing list automatically!
                        </li>
                        <li>
                           Use this Bonus with WebPull’s Autoresponder Integration for building your list and leveraging your sales quickly with Facebook.
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #5 End -->

   <!-- CTA Button Section Start -->
   <div class="dark-cta-sec">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-md-12 col-12 text-center ">
               <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
               <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 orange-clr">TAKE ACTION NOW!</div>
               <div class="f-18 f-md-22 lh140 w400 text-center mt20 white-clr">
                  Use Coupon Code <span class="w700 orange-clr">“WEBPULL”</span> for an Additional <span class="w700 orange-clr">$3 Discount</span> on Commercial Licence
               </div>
            </div>
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab WebPull + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
               <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-24 f-22 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
               <div class="countdown counter-white white-clr">
                  <div class="timer-label text-center">
                     <span class="f-31 f-md-60 timerbg oswald w700">01</span>
                     <span class="f-14 f-md-18 w500 ">Days</span>
                  </div>
                  <div class="timer-label text-center">
                     <span class="f-31 f-md-60 timerbg oswald w700">16</span>
                     <span class="f-14 f-md-18 w500 ">Hours</span>
                  </div>
                  <div class="timer-label text-center timer-mrgn">
                     <span class="f-31 f-md-60 timerbg oswald w700">59</span>
                     <span class="f-14 f-md-18 w500 ">Mins</span>
                  </div>
                  <div class="timer-label text-center ">
                     <span class="f-31 f-md-60 timerbg oswald w700">37</span>
                     <span class="f-14 f-md-18 w500 ">Sec</span>
                  </div>
               </div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>
   <!-- CTA Button Section End -->

   <!-- Bonus #6 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 6</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12 order-md-2">
                     <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/bonus6.webp" class="img-fluid mx-auto d-block " alt="Bonus6">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                        WP Profit Doubler
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>
                           This powerful WordPress Plugin allows you to make a second offer to those visitors who are leaving without any purchase.
                        </li>
                        <li>
                           Use this Bonus with WebPull’s Sales & Email Templates to double-down profit from your abandoned leads.
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #6 End -->

   <!-- Bonus #7 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 7</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/bonus7.webp" class="img-fluid mx-auto d-block " alt="Bonus7">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                        Turbo Tube Engage Pro
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>
                           This Software helps you to make your videos Interactive with just a few simple clicks.
                        </li>
                        <li>
                           Use this Bonus to transform your Video Commercials given in WebPull into a powerful list building & profit-making machine by improving your viewers’ interactivity.
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #7 End -->

   <!-- Bonus #8 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 8</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/bonus8.webp" class="img-fluid mx-auto d-block " alt="Bonus8">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                        BIZ Landing Page Plugin
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>
                           This is an easy-to-use WordPress Plugin that creates Social-Powered Business Landing Pages in seconds.
                        </li>
                        <li>
                           This Bonus when utilized with WebPull’s DFY Themes and Marketing Templates will ease your way to build professional websites within seconds.
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #8 End -->

   <!-- Bonus #9 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 9</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/bonus9.webp" class="img-fluid mx-auto d-block " alt="Bonus9">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                        Power Tools Video Site Builder
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>
                           With this software you can instantly create your own complete money-making video site featuring AdSense and Amazon ads with 120 profitable videos sourced from YouTube.
                        </li>
                        <li>
                           Use this Bonus with DFY Videos given in WebPull to start your own video sites using WebPull DFY Themes & monetize them by showing ads on them.
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #9 End -->

   <!-- Bonus #10 start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 10</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/bonus10.webp" class="img-fluid mx-auto d-block " alt="Bonus10">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                        Chatbot Marketing Mastery
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>
                           In this guide, you’re going to learn more about how chatbots can be used for marketing, and whether chatbots are a good fit for your business.
                        </li>
                        <li>
                           This Bonus will help you to use the free chatbot WordPress plugins efficiently with DFY Sales Chat/Call Script given with WebPull.
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #10 End -->

   <!-- CTA Button Section Start -->
   <div class="dark-cta-sec">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-md-12 col-12 text-center ">
               <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
               <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 orange-clr">TAKE ACTION NOW!</div>
               <div class="f-18 f-md-22 lh140 w400 text-center mt20 white-clr">
                  Use Coupon Code <span class="w700 orange-clr">“WEBPULL”</span> for an Additional <span class="w700 orange-clr">$3 Discount</span> on Commercial Licence
               </div>
            </div>
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab WebPull + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
               <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-24 f-22 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
               <div class="countdown counter-white white-clr">
                  <div class="timer-label text-center">
                     <span class="f-31 f-md-60 timerbg oswald w700">01</span>
                     <span class="f-14 f-md-18 w500 ">Days</span>
                  </div>
                  <div class="timer-label text-center">
                     <span class="f-31 f-md-60 timerbg oswald w700">16</span>
                     <span class="f-14 f-md-18 w500 ">Hours</span>
                  </div>
                  <div class="timer-label text-center timer-mrgn">
                     <span class="f-31 f-md-60 timerbg oswald w700">59</span>
                     <span class="f-14 f-md-18 w500 ">Mins</span>
                  </div>
                  <div class="timer-label text-center ">
                     <span class="f-31 f-md-60 timerbg oswald w700">37</span>
                     <span class="f-14 f-md-18 w500 ">Sec</span>
                  </div>
               </div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>
   <!-- CTA Button Section End -->

   <!-- Bonus #11 start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 11</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/bonus11.webp" class="img-fluid mx-auto d-block " alt="Bonus11">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                        Search Marketing 2.0
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Professionally designed lead capture package that comes with necessary tools, powerful media, and complete email marketing course.</li>
                        <li>This Bonus will help you create high quality Search Engine Optimized text and graphic content along with the Web Content given in WebPull.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #11 End -->

   <!-- Bonus #12 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 12</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/bonus12.webp" class="img-fluid mx-auto d-block " alt="Bonus12">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                        Customer Loyalty Magnet
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>5-Day Training on how to build customer loyalty through incentives.</li>
                        <li>Get Email Newsletter Series, Lead Capture Webpage and Graphics to use Customer Loyalty Magnet.</li>
                        <li>Use this Bonus with WebPull as a plus to increase your overall revenue by capturing high quality leads with huge conversions.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #12 End -->

   <!-- Bonus #13 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 13</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/bonus13.webp" class="img-fluid mx-auto d-block " alt="Bonus13">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                        250 HTML Templates WP Themes and Graphics
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Get complete package of 120 HTML Templates, 10 Sales Graphics Packs, 60+ WP Themes, 17 CSS Content Template, 40 Header Images, 4 Video Squeeze Pages & many more.</li>
                        <li>This Bonus when used with DFY themes of WebPull will create multiple websites with various features & graphics in all different niches.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #13 End -->

   <!-- Bonus #14 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 14</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/bonus14.webp" class="img-fluid mx-auto d-block " alt="Bonus14">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                        WP Email Timer Plus
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>WP Email Timer Plus is a plugin that allows you to create beautiful countdown timers even inside your emails</li>
                        <li>This Bonus will increase your sales conversions up to 4x when used on the Websites build using WebPull Themes.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #14 End -->

   <!-- Bonus #15 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 15</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/bonus15.webp" class="img-fluid mx-auto d-block " alt="Bonus15">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                        Social Pop-Ups Plugin
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>With this plugin you can create your own social media pop-ups for your WordPress blog or Website.</li>
                        <li>This Bonus is must to have in WebPull to capture your visitor’s attention by showing your social media popups along with social sharing icons at footer & below the posts.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #15 End -->

   <!-- CTA Button Section Start -->
   <div class="dark-cta-sec">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-md-12 col-12 text-center ">
               <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
               <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 orange-clr">TAKE ACTION NOW!</div>
               <div class="f-18 f-md-22 lh140 w400 text-center mt20 white-clr">
                  Use Coupon Code <span class="w700 orange-clr">“WEBPULL”</span> for an Additional <span class="w700 orange-clr">$3 Discount</span> on Commercial Licence
               </div>
            </div>
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab WebPull + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
               <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-24 f-22 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
               <div class="countdown counter-white white-clr">
                  <div class="timer-label text-center">
                     <span class="f-31 f-md-60 timerbg oswald w700">01</span>
                     <span class="f-14 f-md-18 w500 ">Days</span>
                  </div>
                  <div class="timer-label text-center">
                     <span class="f-31 f-md-60 timerbg oswald w700">16</span>
                     <span class="f-14 f-md-18 w500 ">Hours</span>
                  </div>
                  <div class="timer-label text-center timer-mrgn">
                     <span class="f-31 f-md-60 timerbg oswald w700">59</span>
                     <span class="f-14 f-md-18 w500 ">Mins</span>
                  </div>
                  <div class="timer-label text-center ">
                     <span class="f-31 f-md-60 timerbg oswald w700">37</span>
                     <span class="f-14 f-md-18 w500 ">Sec</span>
                  </div>
               </div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>
   <!-- CTA Button Section End -->

   <!-- Bonus #16 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 16</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/bonus16.webp" class="img-fluid mx-auto d-block " alt="Bonus16">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                        WP In-Content Popup Pro
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>WP In-Content Popup Pro is a new plugin that lets you create attention grabbing popups within your content.</li>
                        <li>Use this with WebPull to trigger in-content video popups, image popups, text popups, or content popups which you can use to showcase your products or services</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #16 End -->

   <!-- Bonus #17 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 17</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/bonus17.webp" class="img-fluid mx-auto d-block " alt="Bonus17">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                        WP Survey Creator
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>This is a WordPress Plugin that creates surveys with different types of questions with customizable presentation on your website.</li>
                        <li>This unique bonus when combined with WebPull’s analytics feature, will help you target your clients efficiently with better understanding of consumer behavior.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #17 End -->

   <!-- Bonus #18 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 18</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/bonus18.webp" class="img-fluid mx-auto d-block " alt="Bonus18">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                        Stakk Review Pack
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>This is a Mail Pop-Up Plugin</li>
                        <li>Use this Bonus along with high converting sales & email swipes given with WebPull to show Popups right inside your mails.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #18 End -->

   <!-- Bonus #19 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 19</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/bonus19.webp" class="img-fluid mx-auto d-block " alt="Bonus19">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                        Auto Support Bot
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Auto Support Bot is A 24/7 Live Chatbot on Your Websites and Video Pages.</li>
                        <li>Use this Bonus in WebPull to Automate your Customer Support Service by showing 24x7 Live Chatbot Popup.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #19 End -->

   <!-- Bonus #20 start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 20</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/bonus20.webp" class="img-fluid mx-auto d-block " alt="Bonus20">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                        Exit Pop Pro
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Exit Pop Pro is a WordPress Plugin that will show instant exit popup to the visitor on exiting the website.</li>
                        <li>Use this Bonus along with WebPull’s Call-To-Action feature to show highly effective Exit Popup.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #20 End -->

   <!-- Huge Woth Section Start -->
   <div class="huge-area mt30 mt-md10">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-12 text-center">
               <div class="f-md-50 f-28 lh120 w700 white-clr">That’s Huge Worth of</div>
               <br>
               <div class="f-md-50 f-28 lh120 w700 orange-clr">$3275!</div>
            </div>
         </div>
      </div>
   </div>
   <!-- Huge Worth Section End -->

   <!-- text Area Start -->
   <div class="white-section pb0">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-12 text-center">
               <div class="f-md-40 f-28 lh140 w700">So what are you waiting for? You have a great opportunity ahead + <span class="w700 blue-clr">My 20 Bonus Products</span> are making it a <span class="w700 blue-clr">completely NO Brainer!!</span></div>
            </div>
         </div>
      </div>
   </div>
   <!-- text Area End -->

   <!-- CTA Button Section Start -->
   <div class="cta-btn-section">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab WebPull + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-20 f-20 w500 text-center black">My Exclusive Bonuses Are Expiring in...</h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 col-md-10 mx-auto col-12 text-center">
               <div class="countdown counter-black">
                  <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">01</span><br><span class="f-14 f-md-18 w500  ">Days</span> </div>
                  <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">16</span><br><span class="f-14 f-md-18 w500 ">Hours</span> </div>
                  <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald w700">59</span><br><span class="f-14 f-md-18 w500 ">Mins</span> </div>
                  <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald w700">37</span><br><span class="f-14 f-md-18 w500 ">Sec</span> </div>
               </div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>
   <!-- CTA Button Section End -->

   <!--Footer Section Start -->
   <div class="footer-section">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 394.2 100" style="enable-background:new 0 0 394.2 100; height:60px;" xml:space="preserve">
                  <style type="text/css">
                     .st00 {
                        fill: #FFFFFF;
                     }

                     .st11 {
                        fill: url(#SVGID_1_);
                     }

                     .st22 {
                        fill: url(#SVGID_00000021093791325859232720000015953972570224053676_);
                     }

                     .st33 {
                        fill: #FFC802;
                     }

                     .st44 {
                        fill: url(#SVGID_00000003068462962921929030000010945183465240898967_);
                     }
                  </style>
                  <path class="st00" d="M117.7,37.9l-5,12.4H92.9v12.4h18.6l-5,12.4H92.9v12.4h24.8l-5,12.5H92.9H80.5V75.1V62.7V37.9H117.7z" />
                  <path class="st00" d="M323.2,37.9v49.7H348V100h-37.3V37.9H323.2z" />
                  <path class="st00" d="M369.4,37.9v49.7h24.8V100h-37.3V37.9H369.4z" />
                  <g>
                     <g>
                        <polygon class="st00" points="59.1,71.4 52.4,54.4 59.1,37.9 72.6,37.9   " />
                        <polygon class="st00" points="29.4,54.7 36.3,37.9 54.5,82.9 47.7,100   " />
                        <polygon class="st00" points="24.8,100 31.6,82.8 13.4,37.9 0,37.9   " />
                     </g>
                  </g>
                  <path class="st00" d="M222.8,44.3c-4.3-4.3-9.4-6.4-15.4-6.4h-9.3h-12.5v21.7v21.7V100h12.5V81.3h0V68.9h0v-9.3v-9.3h9.3  c2.6,0,4.8,0.9,6.6,2.8c1.8,1.8,2.7,4,2.7,6.6c0,2.6-0.9,4.8-2.7,6.6c-1.8,1.8-4,2.7-6.6,2.7h-0.7v12.4h0.7c6,0,11.1-2.1,15.4-6.4  c4.3-4.3,6.4-9.4,6.4-15.4C229.2,53.6,227.1,48.5,222.8,44.3z" />
                  <rect x="252.1" y="31.8" transform="matrix(0.4226 -0.9063 0.9063 0.4226 114.0209 255.0236)" class="st00" width="10.2" height="12.5" />
                  <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="249.3741" y1="97.3262" x2="270.7371" y2="53.5256">
                     <stop offset="0" style="stop-color:#FA8460" />
                     <stop offset="1" style="stop-color:#FCB121" />
                  </linearGradient>
                  <path class="st11" d="M271.7,80.3c-1.4,3.1-3.8,5.2-7,6.4c-3.2,1.2-6.4,1-9.5-0.4c-3.1-1.4-5.2-3.8-6.4-7c-1.2-3.2-1-6.4,0.4-9.5  l9.9-21.2l-11.3-5.3l-9.9,21.2c-2.9,6.2-3.2,12.6-0.8,19c2.3,6.5,6.6,11.1,12.9,14c6.2,2.9,12.5,3.2,19,0.8  c6.5-2.3,11.2-6.6,14.1-12.8l9.9-21.2l-11.3-5.3L271.7,80.3z" />
                  <rect x="285.9" y="47.6" transform="matrix(0.4226 -0.9063 0.9063 0.4226 119.2494 294.738)" class="st00" width="10.2" height="12.5" />
                  <path class="st00" d="M173.3,76.3c-0.8-1.7-1.7-3.2-2.9-4.5c-1.2-1.3-2.5-2.3-4-3.1c-1.5-0.8-3-1.2-4.5-1.3c1.5-0.1,2.8-0.5,4.1-1.3  c1.2-0.8,2.3-1.8,3.2-3c0.9-1.2,1.6-2.5,2.1-3.9c0.5-1.4,0.8-2.8,0.8-4c0-3.5-0.5-6.4-1.4-8.6c-1-2.2-2.3-4-3.9-5.3  c-1.6-1.3-3.6-2.2-5.8-2.6c-2.2-0.5-4.6-0.7-7-0.7h-27.3v10.5h11.9v0h15.4c0.8,0,1.7,0.2,2.4,0.6c0.8,0.4,1.4,0.9,2,1.5  c0.6,0.6,1,1.4,1.4,2.2c0.3,0.9,0.5,1.7,0.5,2.6c0,0.9-0.2,1.8-0.5,2.8c-0.3,0.9-0.8,1.8-1.4,2.5c-0.6,0.7-1.3,1.3-2.2,1.8  c-0.9,0.5-1.9,0.7-3.1,0.7h-9.1v10.5h10c2.7,0,4.9,0.7,6.4,2.1c1.6,1.4,2.3,3.2,2.3,5.3c0,1-0.2,2-0.6,3c-0.4,1-0.9,1.9-1.6,2.7  c-0.7,0.8-1.5,1.5-2.4,2c-0.9,0.5-1.9,0.7-2.9,0.7h-16.8h0V73.7h0V63.2h0v-9.3h-11.9V100h9.6h2.3h16.8c2.7,0,5.2-0.3,7.5-0.9  c2.3-0.6,4.4-1.6,6.2-3.1c1.8-1.4,3.1-3.3,4.2-5.7c1-2.3,1.5-5.2,1.5-8.6C174.4,79.8,174,78,173.3,76.3z" />
                  <g>
                     <linearGradient id="SVGID_00000142155450205615942230000010409392281687221429_" gradientUnits="userSpaceOnUse" x1="270.7616" y1="25.6663" x2="296.63" y2="25.6663">
                        <stop offset="0" style="stop-color:#FA8460" />
                        <stop offset="4.692155e-02" style="stop-color:#FA9150" />
                        <stop offset="0.1306" style="stop-color:#FBA43A" />
                        <stop offset="0.2295" style="stop-color:#FBB229" />
                        <stop offset="0.3519" style="stop-color:#FCBC1E" />
                        <stop offset="0.5235" style="stop-color:#FCC117" />
                        <stop offset="1" style="stop-color:#FCC315" />
                     </linearGradient>
                     <path style="fill:url(#SVGID_00000142155450205615942230000010409392281687221429_);" d="M289.6,14.2c-2.1-1.1-4.3-1.5-6.5-1.4   c-0.1,0-0.1,0-0.2,0c-0.1,0-0.2,0-0.2,0c0,0-0.1,0-0.1,0c-0.1,0-0.2,0-0.2,0c-0.1,0-0.2,0-0.3,0c-0.1,0-0.2,0-0.2,0   c-0.3,0-0.5,0.1-0.8,0.1c-0.1,0-0.2,0-0.3,0.1c-0.1,0-0.2,0-0.3,0.1c-0.1,0-0.3,0.1-0.4,0.1c-0.1,0-0.2,0-0.2,0.1   c-0.1,0-0.2,0.1-0.3,0.1c-0.2,0.1-0.5,0.2-0.7,0.3c0,0,0,0,0,0c-0.3,0.1-0.6,0.3-0.9,0.4c-0.3,0.1-0.6,0.3-0.9,0.5   c-0.1,0.1-0.3,0.2-0.4,0.2c-0.1,0.1-0.2,0.2-0.4,0.2c-0.1,0.1-0.2,0.1-0.2,0.2c0,0-0.1,0-0.1,0.1c-0.1,0.1-0.2,0.1-0.2,0.2   c-0.1,0-0.1,0.1-0.2,0.2c-0.1,0.1-0.2,0.2-0.3,0.3c-0.1,0-0.1,0.1-0.2,0.2c-0.5,0.4-0.9,0.9-1.3,1.4c-0.1,0.1-0.1,0.1-0.2,0.2   c0,0-0.1,0.1-0.1,0.1c-0.1,0.1-0.1,0.2-0.2,0.3c0,0-0.1,0.1-0.1,0.1c-0.1,0.1-0.1,0.2-0.2,0.3c0,0,0,0.1-0.1,0.1   c-0.1,0.1-0.1,0.2-0.2,0.3c0,0,0,0.1-0.1,0.1c-0.1,0.1-0.1,0.2-0.2,0.3c-0.1,0.1-0.2,0.3-0.2,0.4c-3.3,6.3-0.8,14.1,5.6,17.4   c1.9,1,3.9,1.4,5.9,1.4c0.1,0,0.3,0,0.4,0c0.1,0,0.3,0,0.4,0c0.1,0,0.1,0,0.2,0c0.1,0,0.2,0,0.3,0c0,0,0,0,0.1,0c0.1,0,0.2,0,0.3,0   c0.1,0,0.2,0,0.2,0c0.1,0,0.2,0,0.3-0.1c0.1,0,0.3,0,0.4-0.1c0.1,0,0.3-0.1,0.4-0.1c0.1,0,0.2-0.1,0.3-0.1c0.2,0,0.4-0.1,0.5-0.2   c0.1,0,0.3-0.1,0.4-0.1c0.1,0,0.3-0.1,0.4-0.1c0,0,0,0,0,0c0.1,0,0.2-0.1,0.4-0.1c0,0,0,0,0,0c0,0,0.1,0,0.1,0c0,0,0.1,0,0.1-0.1   c0,0,0,0,0.1,0c0,0,0.1,0,0.1,0c0,0,0,0,0.1,0c0.1,0,0.2-0.1,0.3-0.2c0,0,0,0,0.1,0c0.1-0.1,0.2-0.1,0.3-0.2   c0.1-0.1,0.3-0.1,0.4-0.2c0.4-0.2,0.7-0.5,1.1-0.7c0.2-0.2,0.5-0.4,0.7-0.6c0.2-0.2,0.4-0.4,0.7-0.6c0.2-0.2,0.4-0.4,0.6-0.6   c0.2-0.2,0.4-0.5,0.6-0.7c0.1-0.1,0.2-0.2,0.3-0.4c0,0,0,0,0,0c0.1-0.1,0.2-0.2,0.3-0.4c0.1-0.1,0.2-0.3,0.2-0.4   c0.1-0.1,0.1-0.2,0.2-0.3c0-0.1,0.1-0.1,0.1-0.2c0.1-0.1,0.1-0.2,0.2-0.3C298.5,25.3,296,17.5,289.6,14.2z M279.6,26.7L279.6,26.7   l0.3,0.6c0.4,0.7,1,1.3,1.8,1.7c0.3,0.2,0.7,0.3,1,0.3c0.3,0,0.5-0.1,0.7-0.2c0.1,0,0.1-0.1,0.2-0.2c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0-0.1c0.2-0.3,0.2-0.7-0.1-1.3c0,0,0,0,0,0   c-0.1-0.2-0.2-0.4-0.4-0.6c-0.1-0.1-0.2-0.2-0.3-0.3c-0.4-0.4-0.7-0.8-0.9-1.2c-0.1-0.1-0.2-0.2-0.2-0.4c0,0,0,0,0-0.1   c0,0,0-0.1-0.1-0.1c0,0,0-0.1-0.1-0.1c-0.1-0.2-0.1-0.3-0.2-0.5c-0.1-0.2-0.1-0.4-0.2-0.6c0,0,0-0.1,0-0.1c0-0.1,0-0.2,0-0.3v-0.1   c0-0.1,0-0.1,0-0.2c0,0,0-0.1,0-0.1c0-0.1,0-0.2,0-0.3c0.1-0.3,0.2-0.6,0.3-0.8c0,0,0,0,0-0.1c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0-0.1c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0l0,0c0.4-0.5,0.9-0.9,1.5-1.1c0.7-0.2,1.4-0.3,2.1-0.1l0.9-1.7l1.9,1l-0.8,1.6c0.7,0.4,1.2,1,1.6,1.6   l0.2,0.4l-1.8,1.6l-0.3-0.5c-0.1-0.1-0.3-0.4-0.5-0.7c-0.1-0.1-0.3-0.3-0.5-0.4c0,0-0.1-0.1-0.1-0.1c-0.1-0.1-0.3-0.2-0.4-0.2   c0,0-0.1,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0   c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0h0c0,0,0,0-0.1,0h0   c0,0,0,0-0.1,0h0c0,0,0,0,0,0h0c0,0,0,0,0,0h0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c-0.2,0.1-0.4,0.3-0.5,0.5c-0.3,0.5-0.2,0.9,1,2.3c0.8,0.9,1.2,1.6,1.4,2.4   c0.2,0.8,0.1,1.6-0.3,2.5c-0.8,1.5-2.4,2.2-4.2,1.8l-0.8,1.6l-0.1,0.2l-2-1l0.9-1.7c-0.1-0.1-0.2-0.1-0.3-0.2   c-0.3-0.2-0.5-0.4-0.8-0.6c0,0-0.1-0.1-0.1-0.1c-0.3-0.3-0.5-0.6-0.7-1l-0.2-0.4L279.6,26.7z" />
                     <path class="st33" d="M278,37c-6.3-3.3-8.8-11.1-5.6-17.4c1.3-2.4,3.2-4.3,5.4-5.5c-2.4,1.2-4.4,3.1-5.7,5.6   c-3.3,6.3-0.8,14.1,5.6,17.4c3.9,2,8.4,1.9,12-0.1C286.2,38.9,281.8,39,278,37z" />
                  </g>
                  <g>
                     <linearGradient id="SVGID_00000016062571573767098340000005260099975199807150_" gradientUnits="userSpaceOnUse" x1="79.734" y1="-130.471" x2="91.3973" y2="-130.471" gradientTransform="matrix(0.8624 -0.5063 0.5063 0.8624 260.6894 161.6657)">
                        <stop offset="0" style="stop-color:#FA8460" />
                        <stop offset="4.692155e-02" style="stop-color:#FA9150" />
                        <stop offset="0.1306" style="stop-color:#FBA43A" />
                        <stop offset="0.2295" style="stop-color:#FBB229" />
                        <stop offset="0.3519" style="stop-color:#FCBC1E" />
                        <stop offset="0.5235" style="stop-color:#FCC117" />
                        <stop offset="1" style="stop-color:#FCC315" />
                     </linearGradient>
                     <path style="fill:url(#SVGID_00000016062571573767098340000005260099975199807150_);" d="M268.1,0c-1,0.1-2,0.4-2.8,0.9   c0,0-0.1,0-0.1,0c0,0-0.1,0-0.1,0.1c0,0,0,0,0,0c0,0-0.1,0-0.1,0.1c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0-0.1,0.1   c-0.1,0.1-0.2,0.1-0.3,0.2c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0.1-0.1,0.1   c-0.1,0.1-0.1,0.2-0.2,0.3c0,0,0,0,0,0c-0.1,0.1-0.2,0.2-0.2,0.4c-0.1,0.1-0.2,0.3-0.2,0.4c0,0.1-0.1,0.1-0.1,0.2   c0,0.1-0.1,0.1-0.1,0.2c0,0,0,0.1-0.1,0.1c0,0,0,0,0,0.1c0,0,0,0.1-0.1,0.1c0,0,0,0.1,0,0.1c0,0.1,0,0.1-0.1,0.2c0,0,0,0.1,0,0.1   c-0.1,0.3-0.1,0.5-0.2,0.8c0,0,0,0.1,0,0.1c0,0,0,0.1,0,0.1c0,0.1,0,0.1,0,0.2c0,0,0,0,0,0.1c0,0.1,0,0.1,0,0.2c0,0,0,0,0,0.1   c0,0.1,0,0.1,0,0.2c0,0,0,0,0,0.1c0,0.1,0,0.1,0,0.2c0,0.1,0,0.1,0,0.2c0.2,3.2,2.9,5.7,6.1,5.5c1-0.1,1.9-0.3,2.6-0.8   c0.1,0,0.1-0.1,0.2-0.1c0.1,0,0.1-0.1,0.1-0.1c0,0,0,0,0.1,0c0,0,0.1,0,0.1-0.1c0,0,0,0,0,0c0,0,0.1-0.1,0.1-0.1c0,0,0.1,0,0.1-0.1   c0,0,0.1-0.1,0.1-0.1c0,0,0.1-0.1,0.1-0.1c0,0,0.1-0.1,0.1-0.1c0,0,0.1-0.1,0.1-0.1c0.1-0.1,0.1-0.1,0.2-0.2c0,0,0.1-0.1,0.1-0.1   c0,0,0.1-0.1,0.1-0.1c0,0,0,0,0,0c0,0,0.1-0.1,0.1-0.1c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0-0.1c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0.1-0.1,0.1-0.1c0,0,0,0,0,0c0,0,0.1-0.1,0.1-0.1c0-0.1,0.1-0.1,0.1-0.2c0.1-0.2,0.2-0.4,0.3-0.5c0.1-0.1,0.1-0.2,0.1-0.4   c0-0.1,0.1-0.3,0.1-0.4c0-0.1,0.1-0.3,0.1-0.4c0-0.1,0-0.3,0.1-0.4c0-0.1,0-0.1,0-0.2c0,0,0,0,0,0c0-0.1,0-0.1,0-0.2   c0-0.1,0-0.1,0-0.2c0-0.1,0-0.1,0-0.2c0,0,0-0.1,0-0.1c0-0.1,0-0.1,0-0.2C274.1,2.3,271.3-0.2,268.1,0z M267.1,7.2L267.1,7.2   l0.3,0.2c0.3,0.2,0.7,0.3,1.1,0.3c0.2,0,0.3-0.1,0.4-0.1c0.1-0.1,0.2-0.1,0.2-0.3c0,0,0-0.1,0-0.1c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0-0.2-0.1-0.3-0.3-0.5c0,0,0,0,0,0   c-0.1,0-0.2-0.1-0.3-0.1c-0.1,0-0.1,0-0.2-0.1c-0.2-0.1-0.4-0.2-0.6-0.2c-0.1,0-0.1-0.1-0.2-0.1c0,0,0,0,0,0c0,0,0,0-0.1,0   c0,0,0,0-0.1,0c-0.1,0-0.1-0.1-0.2-0.1c-0.1-0.1-0.1-0.1-0.2-0.2c0,0,0,0,0,0c0,0-0.1-0.1-0.1-0.1l0,0c0,0,0,0,0-0.1c0,0,0,0,0,0   c0,0,0-0.1-0.1-0.1c0-0.1-0.1-0.3-0.1-0.4c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0l0,0   c0-0.3,0.2-0.5,0.3-0.8c0.2-0.2,0.5-0.4,0.8-0.5l0-0.9l1-0.1l0,0.8c0.4,0,0.7,0.1,1,0.3l0.2,0.1l-0.3,1l-0.3-0.1   c-0.1,0-0.2-0.1-0.4-0.1c-0.1,0-0.2,0-0.3-0.1c0,0-0.1,0-0.1,0c-0.1,0-0.1,0-0.2,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0l0,0c0,0,0,0,0,0l0,0c0,0,0,0,0,0l0,0c0,0,0,0,0,0l0,0c0,0,0,0,0,0l0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c-0.1,0.1-0.1,0.2-0.1,0.3c0,0.2,0.1,0.4,0.9,0.7c0.5,0.2,0.8,0.4,1.1,0.6c0.3,0.3,0.4,0.6,0.4,1c0,0.8-0.4,1.4-1.2,1.7l0,0.8   l0,0.1l-1,0.1l0-0.8c-0.1,0-0.1,0-0.2,0c-0.2,0-0.3,0-0.4-0.1c0,0-0.1,0-0.1,0c-0.2-0.1-0.4-0.1-0.5-0.2l-0.2-0.1L267.1,7.2z" />
                     <path class="st33" d="M268.8,11.5c-3.2,0.2-6-2.3-6.1-5.5c-0.1-1.2,0.3-2.4,0.9-3.4c-0.7,1-1,2.2-0.9,3.5c0.2,3.2,2.9,5.7,6.1,5.5   c2-0.1,3.7-1.2,4.6-2.8C272.4,10.4,270.7,11.4,268.8,11.5z" />
                  </g>
                  <polygon class="st00" points="297.8,43.3 296.4,42.7 294.6,46.7 295.4,41.2 296.8,41.8 298.7,37.9 " />
                  <polygon class="st00" points="264.2,27.2 262.9,26.4 260.7,30.2 262,24.8 263.3,25.6 265.5,21.8 " />
               </svg>
               <div editabletype="text " class="f-16 f-md-18 w400 mt20 lh140 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK & INSTAGRAM are the trademarks of FACEBOOK, Inc.</div>
            </div>
            <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
               <div class="f-md-18 f-16 w400 lh140 white-clr text-xs-center">Copyright © WebPull 2022</div>
               <ul class="footer-ul w400 f-md-18 f-16 white-clr text-center text-md-right">
                  <li><a href="https://support.bizomart.com/hc/en-us " class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                  <li><a href="http://webpull.co/legal/privacy-policy.html " class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                  <li><a href="http://webpull.co/legal/terms-of-service.html " class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                  <li><a href="http://webpull.co/legal/disclaimer.html " class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                  <li><a href="http://webpull.co/legal/gdpr.html " class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                  <li><a href="http://webpull.co/legal/dmca.html " class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                  <li><a href="http://webpull.co/legal/anti-spam.html " class="white-clr t-decoration-none">Anti-Spam</a></li>
               </ul>
            </div>
         </div>
      </div>
   </div>
   <!-- Back to top button -->
   <a id="button">
      <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 300.003 300.003" style="enable-background:new 0 0 300.003 300.003;" xml:space="preserve">
         <g>
            <g>
               <path d="M150,0C67.159,0,0.001,67.159,0.001,150c0,82.838,67.157,150.003,149.997,150.003S300.002,232.838,300.002,150    C300.002,67.159,232.842,0,150,0z M217.685,189.794c-5.47,5.467-14.338,5.47-19.81,0l-48.26-48.27l-48.522,48.516    c-5.467,5.467-14.338,5.47-19.81,0c-2.731-2.739-4.098-6.321-4.098-9.905s1.367-7.166,4.103-9.897l56.292-56.297    c0.539-0.838,1.157-1.637,1.888-2.368c2.796-2.796,6.476-4.142,10.146-4.077c3.662-0.062,7.348,1.281,10.141,4.08    c0.734,0.729,1.349,1.528,1.886,2.365l56.043,56.043C223.152,175.454,223.156,184.322,217.685,189.794z" />
            </g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
      </svg>
   </a>
   <!--Footer Section End -->
   <!-- timer --->
   <?php
   if ($now < $exp_date) {
   ?>
      <script type="text/javascript">
         // Count down milliseconds = server_end - server_now = client_end - client_now
         var server_end = <?php echo $exp_date; ?> * 1000;
         var server_now = <?php echo time(); ?> * 1000;
         var client_now = new Date().getTime();
         var end = server_end - server_now + client_now; // this is the real end time

         var noob = $('.countdown').length;

         var _second = 1000;
         var _minute = _second * 60;
         var _hour = _minute * 60;
         var _day = _hour * 24
         var timer;

         function showRemaining() {
            var now = new Date();
            var distance = end - now;
            if (distance < 0) {
               clearInterval(timer);
               document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
               return;
            }

            var days = Math.floor(distance / _day);
            var hours = Math.floor((distance % _day) / _hour);
            var minutes = Math.floor((distance % _hour) / _minute);
            var seconds = Math.floor((distance % _minute) / _second);
            if (days < 10) {
               days = "0" + days;
            }
            if (hours < 10) {
               hours = "0" + hours;
            }
            if (minutes < 10) {
               minutes = "0" + minutes;
            }
            if (seconds < 10) {
               seconds = "0" + seconds;
            }
            var i;
            var countdown = document.getElementsByClassName('countdown');
            for (i = 0; i < noob; i++) {
               countdown[i].innerHTML = '';

               if (days) {
                  countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">' + days + '</span><br><span class="f-14 f-md-18 ">Days</span> </div>';
               }

               countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">' + hours + '</span><br><span class="f-14 f-md-18 w500 ">Hours</span> </div>';

               countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald w700">' + minutes + '</span><br><span class="f-14 f-md-18 w500 ">Mins</span> </div>';

               countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald w700">' + seconds + '</span><br><span class="f-14 f-md-18 w500">Sec</span> </div>';
            }

         }
         timer = setInterval(showRemaining, 1000);
      </script>
   <?php
   } else {
      echo "Times Up";
   }
   ?>
   <!--- timer end-->

   <!-- scroll Top to Bottom -->
   <script>
      var btn = $('#button');

      $(window).scroll(function() {
         if ($(window).scrollTop() > 300) {
            btn.addClass('show');
         } else {
            btn.removeClass('show');
         }
      });

      btn.on('click', function(e) {
         e.preventDefault();
         $('html, body').animate({
            scrollTop: 0
         }, '300');
      });
   </script>
</body>

</html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
   <!-- Tell the browser to be responsive to screen width -->
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
   <meta name="title" content="WebPull Special Bonuses">
   <meta name="description" content="Get Premium, Limited Time Bonuses With Your WebPull Purchase & Make It Even Better">
   <meta name="keywords" content="">
   <meta property="og:image" content="https://www.webpull.co/special-bonus/thumbnail.png">
   <meta name="language" content="English">
   <meta name="revisit-after" content="1 days">
   <meta name="author" content="Ayush & Pranshu">
   <!-- Open Graph / Facebook -->
   <meta property="og:type" content="website">
   <meta property="og:title" content="WebPull Special Bonuses">
   <meta property="og:description" content="Get Premium, Limited Time Bonuses With Your WebPull Purchase & Make It Even Better">
   <meta property="og:image" content="https://www.webpull.co/special-bonus/thumbnail.png">
   <!-- Twitter -->
   <meta property="twitter:card" content="summary_large_image">
   <meta property="twitter:title" content="WebPull Special Bonuses">
   <meta property="twitter:description" content="Get Premium, Limited Time Bonuses With Your WebPull Purchase & Make It Even Better">
   <meta property="twitter:image" content="https://www.webpull.co/special-bonus/thumbnail.png">
   <title>WebPull Special Bonuses</title>

   <!-- Shortcut Icon  -->
   <link rel="icon" href="https://cdn.oppyo.com/launches/webpull/common_assets/images/favicon.png" type="image/png">
   <link rel="preconnect" href="https://fonts.googleapis.com">
   <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
   <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
   <!-- Css CDN Load Link -->
   <link rel="stylesheet" href="https://cdn.oppyo.com/launches/webpull/common_assets/css/bootstrap.min.css" type="text/css">
   <link rel="stylesheet" type="text/css" href="assets/css/bonus-style.css">
   <link rel="stylesheet" href="assets/css/style.css" type="text/css">
   <script src="../common_assets/js/jquery.min.js"></script>
</head>

<body>
   <!-- New Timer  Start-->
   <?php
   $date = 'July 14 2022 11:00 AM EST';
   $exp_date = strtotime($date);
   $now = time();
   /*
         
         $date = date('F d Y g:i:s A eO');
         $rand_time_add = rand(700, 1200);
         $exp_date = strtotime($date) + $rand_time_add;
         $now = time();*/

   if ($now < $exp_date) {
   ?>
   <?php
   } else {
      echo "Times Up";
   }
   ?>
   <!-- New Timer End -->
   <?php
   if (!isset($_GET['afflink'])) {
      $_GET['afflink'] = 'https://warriorplus.com/o2/a/wyl4gg/0';
      $_GET['name'] = 'Ayush & Pranshu';
   }
   ?>

   <div class="main-header">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-12 text-center">
               <div class="text-center">
                  <div class="f-md-32 f-20 lh140 w400 white-clr d-block d-md-flex align-items-center justify-content-center">
                     <span class="w700"><?php echo $_GET['name']; ?>'s</span> &nbsp;special bonus for &nbsp;
                     <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 394.2 100" style="enable-background:new 0 0 394.2 100; height:50px;" xml:space="preserve">
                        <style type="text/css">
                           .st00 {
                              fill: #FFFFFF;
                           }

                           .st11 {
                              fill: url(#SVGID_1_);
                           }

                           .st22 {
                              fill: url(#SVGID_00000021093791325859232720000015953972570224053676_);
                           }

                           .st33 {
                              fill: #FFC802;
                           }

                           .st44 {
                              fill: url(#SVGID_00000003068462962921929030000010945183465240898967_);
                           }
                        </style>
                        <path class="st00" d="M117.7,37.9l-5,12.4H92.9v12.4h18.6l-5,12.4H92.9v12.4h24.8l-5,12.5H92.9H80.5V75.1V62.7V37.9H117.7z"></path>
                        <path class="st00" d="M323.2,37.9v49.7H348V100h-37.3V37.9H323.2z"></path>
                        <path class="st00" d="M369.4,37.9v49.7h24.8V100h-37.3V37.9H369.4z"></path>
                        <g>
                           <g>
                              <polygon class="st00" points="59.1,71.4 52.4,54.4 59.1,37.9 72.6,37.9   "></polygon>
                              <polygon class="st00" points="29.4,54.7 36.3,37.9 54.5,82.9 47.7,100   "></polygon>
                              <polygon class="st00" points="24.8,100 31.6,82.8 13.4,37.9 0,37.9   "></polygon>
                           </g>
                        </g>
                        <path class="st00" d="M222.8,44.3c-4.3-4.3-9.4-6.4-15.4-6.4h-9.3h-12.5v21.7v21.7V100h12.5V81.3h0V68.9h0v-9.3v-9.3h9.3  c2.6,0,4.8,0.9,6.6,2.8c1.8,1.8,2.7,4,2.7,6.6c0,2.6-0.9,4.8-2.7,6.6c-1.8,1.8-4,2.7-6.6,2.7h-0.7v12.4h0.7c6,0,11.1-2.1,15.4-6.4  c4.3-4.3,6.4-9.4,6.4-15.4C229.2,53.6,227.1,48.5,222.8,44.3z"></path>
                        <rect x="252.1" y="31.8" transform="matrix(0.4226 -0.9063 0.9063 0.4226 114.0209 255.0236)" class="st00" width="10.2" height="12.5"></rect>
                        <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="249.3741" y1="97.3262" x2="270.7371" y2="53.5256">
                           <stop offset="0" style="stop-color:#FA8460"></stop>
                           <stop offset="1" style="stop-color:#FCB121"></stop>
                        </linearGradient>
                        <path class="st11" d="M271.7,80.3c-1.4,3.1-3.8,5.2-7,6.4c-3.2,1.2-6.4,1-9.5-0.4c-3.1-1.4-5.2-3.8-6.4-7c-1.2-3.2-1-6.4,0.4-9.5  l9.9-21.2l-11.3-5.3l-9.9,21.2c-2.9,6.2-3.2,12.6-0.8,19c2.3,6.5,6.6,11.1,12.9,14c6.2,2.9,12.5,3.2,19,0.8  c6.5-2.3,11.2-6.6,14.1-12.8l9.9-21.2l-11.3-5.3L271.7,80.3z"></path>
                        <rect x="285.9" y="47.6" transform="matrix(0.4226 -0.9063 0.9063 0.4226 119.2494 294.738)" class="st00" width="10.2" height="12.5"></rect>
                        <path class="st00" d="M173.3,76.3c-0.8-1.7-1.7-3.2-2.9-4.5c-1.2-1.3-2.5-2.3-4-3.1c-1.5-0.8-3-1.2-4.5-1.3c1.5-0.1,2.8-0.5,4.1-1.3  c1.2-0.8,2.3-1.8,3.2-3c0.9-1.2,1.6-2.5,2.1-3.9c0.5-1.4,0.8-2.8,0.8-4c0-3.5-0.5-6.4-1.4-8.6c-1-2.2-2.3-4-3.9-5.3  c-1.6-1.3-3.6-2.2-5.8-2.6c-2.2-0.5-4.6-0.7-7-0.7h-27.3v10.5h11.9v0h15.4c0.8,0,1.7,0.2,2.4,0.6c0.8,0.4,1.4,0.9,2,1.5  c0.6,0.6,1,1.4,1.4,2.2c0.3,0.9,0.5,1.7,0.5,2.6c0,0.9-0.2,1.8-0.5,2.8c-0.3,0.9-0.8,1.8-1.4,2.5c-0.6,0.7-1.3,1.3-2.2,1.8  c-0.9,0.5-1.9,0.7-3.1,0.7h-9.1v10.5h10c2.7,0,4.9,0.7,6.4,2.1c1.6,1.4,2.3,3.2,2.3,5.3c0,1-0.2,2-0.6,3c-0.4,1-0.9,1.9-1.6,2.7  c-0.7,0.8-1.5,1.5-2.4,2c-0.9,0.5-1.9,0.7-2.9,0.7h-16.8h0V73.7h0V63.2h0v-9.3h-11.9V100h9.6h2.3h16.8c2.7,0,5.2-0.3,7.5-0.9  c2.3-0.6,4.4-1.6,6.2-3.1c1.8-1.4,3.1-3.3,4.2-5.7c1-2.3,1.5-5.2,1.5-8.6C174.4,79.8,174,78,173.3,76.3z"></path>
                        <g>
                           <linearGradient id="SVGID_00000142155450205615942230000010409392281687221429_" gradientUnits="userSpaceOnUse" x1="270.7616" y1="25.6663" x2="296.63" y2="25.6663">
                              <stop offset="0" style="stop-color:#FA8460"></stop>
                              <stop offset="4.692155e-02" style="stop-color:#FA9150"></stop>
                              <stop offset="0.1306" style="stop-color:#FBA43A"></stop>
                              <stop offset="0.2295" style="stop-color:#FBB229"></stop>
                              <stop offset="0.3519" style="stop-color:#FCBC1E"></stop>
                              <stop offset="0.5235" style="stop-color:#FCC117"></stop>
                              <stop offset="1" style="stop-color:#FCC315"></stop>
                           </linearGradient>
                           <path style="fill:url(#SVGID_00000142155450205615942230000010409392281687221429_);" d="M289.6,14.2c-2.1-1.1-4.3-1.5-6.5-1.4   c-0.1,0-0.1,0-0.2,0c-0.1,0-0.2,0-0.2,0c0,0-0.1,0-0.1,0c-0.1,0-0.2,0-0.2,0c-0.1,0-0.2,0-0.3,0c-0.1,0-0.2,0-0.2,0   c-0.3,0-0.5,0.1-0.8,0.1c-0.1,0-0.2,0-0.3,0.1c-0.1,0-0.2,0-0.3,0.1c-0.1,0-0.3,0.1-0.4,0.1c-0.1,0-0.2,0-0.2,0.1   c-0.1,0-0.2,0.1-0.3,0.1c-0.2,0.1-0.5,0.2-0.7,0.3c0,0,0,0,0,0c-0.3,0.1-0.6,0.3-0.9,0.4c-0.3,0.1-0.6,0.3-0.9,0.5   c-0.1,0.1-0.3,0.2-0.4,0.2c-0.1,0.1-0.2,0.2-0.4,0.2c-0.1,0.1-0.2,0.1-0.2,0.2c0,0-0.1,0-0.1,0.1c-0.1,0.1-0.2,0.1-0.2,0.2   c-0.1,0-0.1,0.1-0.2,0.2c-0.1,0.1-0.2,0.2-0.3,0.3c-0.1,0-0.1,0.1-0.2,0.2c-0.5,0.4-0.9,0.9-1.3,1.4c-0.1,0.1-0.1,0.1-0.2,0.2   c0,0-0.1,0.1-0.1,0.1c-0.1,0.1-0.1,0.2-0.2,0.3c0,0-0.1,0.1-0.1,0.1c-0.1,0.1-0.1,0.2-0.2,0.3c0,0,0,0.1-0.1,0.1   c-0.1,0.1-0.1,0.2-0.2,0.3c0,0,0,0.1-0.1,0.1c-0.1,0.1-0.1,0.2-0.2,0.3c-0.1,0.1-0.2,0.3-0.2,0.4c-3.3,6.3-0.8,14.1,5.6,17.4   c1.9,1,3.9,1.4,5.9,1.4c0.1,0,0.3,0,0.4,0c0.1,0,0.3,0,0.4,0c0.1,0,0.1,0,0.2,0c0.1,0,0.2,0,0.3,0c0,0,0,0,0.1,0c0.1,0,0.2,0,0.3,0   c0.1,0,0.2,0,0.2,0c0.1,0,0.2,0,0.3-0.1c0.1,0,0.3,0,0.4-0.1c0.1,0,0.3-0.1,0.4-0.1c0.1,0,0.2-0.1,0.3-0.1c0.2,0,0.4-0.1,0.5-0.2   c0.1,0,0.3-0.1,0.4-0.1c0.1,0,0.3-0.1,0.4-0.1c0,0,0,0,0,0c0.1,0,0.2-0.1,0.4-0.1c0,0,0,0,0,0c0,0,0.1,0,0.1,0c0,0,0.1,0,0.1-0.1   c0,0,0,0,0.1,0c0,0,0.1,0,0.1,0c0,0,0,0,0.1,0c0.1,0,0.2-0.1,0.3-0.2c0,0,0,0,0.1,0c0.1-0.1,0.2-0.1,0.3-0.2   c0.1-0.1,0.3-0.1,0.4-0.2c0.4-0.2,0.7-0.5,1.1-0.7c0.2-0.2,0.5-0.4,0.7-0.6c0.2-0.2,0.4-0.4,0.7-0.6c0.2-0.2,0.4-0.4,0.6-0.6   c0.2-0.2,0.4-0.5,0.6-0.7c0.1-0.1,0.2-0.2,0.3-0.4c0,0,0,0,0,0c0.1-0.1,0.2-0.2,0.3-0.4c0.1-0.1,0.2-0.3,0.2-0.4   c0.1-0.1,0.1-0.2,0.2-0.3c0-0.1,0.1-0.1,0.1-0.2c0.1-0.1,0.1-0.2,0.2-0.3C298.5,25.3,296,17.5,289.6,14.2z M279.6,26.7L279.6,26.7   l0.3,0.6c0.4,0.7,1,1.3,1.8,1.7c0.3,0.2,0.7,0.3,1,0.3c0.3,0,0.5-0.1,0.7-0.2c0.1,0,0.1-0.1,0.2-0.2c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0-0.1c0.2-0.3,0.2-0.7-0.1-1.3c0,0,0,0,0,0   c-0.1-0.2-0.2-0.4-0.4-0.6c-0.1-0.1-0.2-0.2-0.3-0.3c-0.4-0.4-0.7-0.8-0.9-1.2c-0.1-0.1-0.2-0.2-0.2-0.4c0,0,0,0,0-0.1   c0,0,0-0.1-0.1-0.1c0,0,0-0.1-0.1-0.1c-0.1-0.2-0.1-0.3-0.2-0.5c-0.1-0.2-0.1-0.4-0.2-0.6c0,0,0-0.1,0-0.1c0-0.1,0-0.2,0-0.3v-0.1   c0-0.1,0-0.1,0-0.2c0,0,0-0.1,0-0.1c0-0.1,0-0.2,0-0.3c0.1-0.3,0.2-0.6,0.3-0.8c0,0,0,0,0-0.1c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0-0.1c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0l0,0c0.4-0.5,0.9-0.9,1.5-1.1c0.7-0.2,1.4-0.3,2.1-0.1l0.9-1.7l1.9,1l-0.8,1.6c0.7,0.4,1.2,1,1.6,1.6   l0.2,0.4l-1.8,1.6l-0.3-0.5c-0.1-0.1-0.3-0.4-0.5-0.7c-0.1-0.1-0.3-0.3-0.5-0.4c0,0-0.1-0.1-0.1-0.1c-0.1-0.1-0.3-0.2-0.4-0.2   c0,0-0.1,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0   c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0h0c0,0,0,0-0.1,0h0   c0,0,0,0-0.1,0h0c0,0,0,0,0,0h0c0,0,0,0,0,0h0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c-0.2,0.1-0.4,0.3-0.5,0.5c-0.3,0.5-0.2,0.9,1,2.3c0.8,0.9,1.2,1.6,1.4,2.4   c0.2,0.8,0.1,1.6-0.3,2.5c-0.8,1.5-2.4,2.2-4.2,1.8l-0.8,1.6l-0.1,0.2l-2-1l0.9-1.7c-0.1-0.1-0.2-0.1-0.3-0.2   c-0.3-0.2-0.5-0.4-0.8-0.6c0,0-0.1-0.1-0.1-0.1c-0.3-0.3-0.5-0.6-0.7-1l-0.2-0.4L279.6,26.7z"></path>
                           <path class="st33" d="M278,37c-6.3-3.3-8.8-11.1-5.6-17.4c1.3-2.4,3.2-4.3,5.4-5.5c-2.4,1.2-4.4,3.1-5.7,5.6   c-3.3,6.3-0.8,14.1,5.6,17.4c3.9,2,8.4,1.9,12-0.1C286.2,38.9,281.8,39,278,37z"></path>
                        </g>
                        <g>
                           <linearGradient id="SVGID_00000016062571573767098340000005260099975199807150_" gradientUnits="userSpaceOnUse" x1="79.734" y1="-130.471" x2="91.3973" y2="-130.471" gradientTransform="matrix(0.8624 -0.5063 0.5063 0.8624 260.6894 161.6657)">
                              <stop offset="0" style="stop-color:#FA8460"></stop>
                              <stop offset="4.692155e-02" style="stop-color:#FA9150"></stop>
                              <stop offset="0.1306" style="stop-color:#FBA43A"></stop>
                              <stop offset="0.2295" style="stop-color:#FBB229"></stop>
                              <stop offset="0.3519" style="stop-color:#FCBC1E"></stop>
                              <stop offset="0.5235" style="stop-color:#FCC117"></stop>
                              <stop offset="1" style="stop-color:#FCC315"></stop>
                           </linearGradient>
                           <path style="fill:url(#SVGID_00000016062571573767098340000005260099975199807150_);" d="M268.1,0c-1,0.1-2,0.4-2.8,0.9   c0,0-0.1,0-0.1,0c0,0-0.1,0-0.1,0.1c0,0,0,0,0,0c0,0-0.1,0-0.1,0.1c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0-0.1,0.1   c-0.1,0.1-0.2,0.1-0.3,0.2c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0.1-0.1,0.1   c-0.1,0.1-0.1,0.2-0.2,0.3c0,0,0,0,0,0c-0.1,0.1-0.2,0.2-0.2,0.4c-0.1,0.1-0.2,0.3-0.2,0.4c0,0.1-0.1,0.1-0.1,0.2   c0,0.1-0.1,0.1-0.1,0.2c0,0,0,0.1-0.1,0.1c0,0,0,0,0,0.1c0,0,0,0.1-0.1,0.1c0,0,0,0.1,0,0.1c0,0.1,0,0.1-0.1,0.2c0,0,0,0.1,0,0.1   c-0.1,0.3-0.1,0.5-0.2,0.8c0,0,0,0.1,0,0.1c0,0,0,0.1,0,0.1c0,0.1,0,0.1,0,0.2c0,0,0,0,0,0.1c0,0.1,0,0.1,0,0.2c0,0,0,0,0,0.1   c0,0.1,0,0.1,0,0.2c0,0,0,0,0,0.1c0,0.1,0,0.1,0,0.2c0,0.1,0,0.1,0,0.2c0.2,3.2,2.9,5.7,6.1,5.5c1-0.1,1.9-0.3,2.6-0.8   c0.1,0,0.1-0.1,0.2-0.1c0.1,0,0.1-0.1,0.1-0.1c0,0,0,0,0.1,0c0,0,0.1,0,0.1-0.1c0,0,0,0,0,0c0,0,0.1-0.1,0.1-0.1c0,0,0.1,0,0.1-0.1   c0,0,0.1-0.1,0.1-0.1c0,0,0.1-0.1,0.1-0.1c0,0,0.1-0.1,0.1-0.1c0,0,0.1-0.1,0.1-0.1c0.1-0.1,0.1-0.1,0.2-0.2c0,0,0.1-0.1,0.1-0.1   c0,0,0.1-0.1,0.1-0.1c0,0,0,0,0,0c0,0,0.1-0.1,0.1-0.1c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0-0.1c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0.1-0.1,0.1-0.1c0,0,0,0,0,0c0,0,0.1-0.1,0.1-0.1c0-0.1,0.1-0.1,0.1-0.2c0.1-0.2,0.2-0.4,0.3-0.5c0.1-0.1,0.1-0.2,0.1-0.4   c0-0.1,0.1-0.3,0.1-0.4c0-0.1,0.1-0.3,0.1-0.4c0-0.1,0-0.3,0.1-0.4c0-0.1,0-0.1,0-0.2c0,0,0,0,0,0c0-0.1,0-0.1,0-0.2   c0-0.1,0-0.1,0-0.2c0-0.1,0-0.1,0-0.2c0,0,0-0.1,0-0.1c0-0.1,0-0.1,0-0.2C274.1,2.3,271.3-0.2,268.1,0z M267.1,7.2L267.1,7.2   l0.3,0.2c0.3,0.2,0.7,0.3,1.1,0.3c0.2,0,0.3-0.1,0.4-0.1c0.1-0.1,0.2-0.1,0.2-0.3c0,0,0-0.1,0-0.1c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0-0.2-0.1-0.3-0.3-0.5c0,0,0,0,0,0   c-0.1,0-0.2-0.1-0.3-0.1c-0.1,0-0.1,0-0.2-0.1c-0.2-0.1-0.4-0.2-0.6-0.2c-0.1,0-0.1-0.1-0.2-0.1c0,0,0,0,0,0c0,0,0,0-0.1,0   c0,0,0,0-0.1,0c-0.1,0-0.1-0.1-0.2-0.1c-0.1-0.1-0.1-0.1-0.2-0.2c0,0,0,0,0,0c0,0-0.1-0.1-0.1-0.1l0,0c0,0,0,0,0-0.1c0,0,0,0,0,0   c0,0,0-0.1-0.1-0.1c0-0.1-0.1-0.3-0.1-0.4c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0l0,0   c0-0.3,0.2-0.5,0.3-0.8c0.2-0.2,0.5-0.4,0.8-0.5l0-0.9l1-0.1l0,0.8c0.4,0,0.7,0.1,1,0.3l0.2,0.1l-0.3,1l-0.3-0.1   c-0.1,0-0.2-0.1-0.4-0.1c-0.1,0-0.2,0-0.3-0.1c0,0-0.1,0-0.1,0c-0.1,0-0.1,0-0.2,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0l0,0c0,0,0,0,0,0l0,0c0,0,0,0,0,0l0,0c0,0,0,0,0,0l0,0c0,0,0,0,0,0l0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c-0.1,0.1-0.1,0.2-0.1,0.3c0,0.2,0.1,0.4,0.9,0.7c0.5,0.2,0.8,0.4,1.1,0.6c0.3,0.3,0.4,0.6,0.4,1c0,0.8-0.4,1.4-1.2,1.7l0,0.8   l0,0.1l-1,0.1l0-0.8c-0.1,0-0.1,0-0.2,0c-0.2,0-0.3,0-0.4-0.1c0,0-0.1,0-0.1,0c-0.2-0.1-0.4-0.1-0.5-0.2l-0.2-0.1L267.1,7.2z"></path>
                           <path class="st33" d="M268.8,11.5c-3.2,0.2-6-2.3-6.1-5.5c-0.1-1.2,0.3-2.4,0.9-3.4c-0.7,1-1,2.2-0.9,3.5c0.2,3.2,2.9,5.7,6.1,5.5   c2-0.1,3.7-1.2,4.6-2.8C272.4,10.4,270.7,11.4,268.8,11.5z"></path>
                        </g>
                        <polygon class="st00" points="297.8,43.3 296.4,42.7 294.6,46.7 295.4,41.2 296.8,41.8 298.7,37.9 "></polygon>
                        <polygon class="st00" points="264.2,27.2 262.9,26.4 260.7,30.2 262,24.8 263.3,25.6 265.5,21.8 "></polygon>
                     </svg>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md40 text-center">
                  <div class="preheadline f-20 f-md-21 w500 white-clr lh140">
                     Grab My 20 Exclusive Bonuses Before the Deal Ends…
                  </div>
               </div>
               <div class="col-12 mt-md25 mt20 f-md-45 f-28 w700 text-center white-clr lh140 text-shadow">
                  This Game-Changing Website Builder Lets You Create & Sell Beautiful Local WordPress Websites & Ecom Stores for Any Business in Just 7 Minutes for BIG Profits
               </div>
               <div class="col-12 mt-md35 mt20  text-center">
                  <div class="post-heading f-18 f-md-24 w500 text-center lh140 white-clr text-capitalize">
                     All From Single Dashboard Without Any Tech Skills and Zero Monthly Fees
                  </div>
               </div>
            </div>
         </div>
         <div class="row mt20 mt-md30 mb20 mb-md30">
            <div class="col-12 col-md-10 mx-auto">
               <!-- <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/product-box.webp" class="img-fluid mx-auto d-block" alt="ProductBox"> -->
               <div class="responsive-video">
                  <iframe src="https://webpull.dotcompal.com/video/embed/26sp46ptcb" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                  box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
               </div>
            </div>
         </div>
      </div>
   </div>

   <div class="list-section">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="row header-list-block">
                  <div class="col-12 col-md-6">
                     <div class="f-18 f-md-20 lh140 w400">
                        <ul class="bonus-list pl0">
                           <li class="w600">Tap Into Fast-Growing $284 Billion Industry &amp; Bank BIG </li>
                           <li>Create <span class="w600">Elegant, Ultra-Fast Loading &amp; Mobile Ready Websites</span> in 60 Seconds </li>
                           <li class="w600">400+ Eye-Catching Themes with Done-For-You Content </li>
                           <li><span class="w600">100% SEO Ready Websites</span> for Tons of FREE Search Traffic </li>
                           <li><span class="w600">Get More Likes, Shares and Viral Traffic with</span> In-Built Social Media Tools </li>
                           <li><span class="w600">Accept Payments</span> For Your Services &amp; Products </li>
                        </ul>
                     </div>
                  </div>
                  <div class="col-12 col-md-6">
                     <div class="f-18 f-md-20 lh140 w400">
                        <ul class="bonus-list pl0">
                           <li><span class="w600">Without Touching Any Code,</span> Customize Websites Easily</li>
                           <li>Analytics &amp; Remarketing Ready </li>
                           <li><span class="w600">Newbie-Friendly</span> - No Tech Skills Needed, No Monthly Fees Ever. </li>
                           <li><span class="w600">Commercial License Included</span> to Build an Incredible Income Helping Clients! </li>
                           <li>Bonus Live Training - <span class="w600">Find Tons of Local Clients Hassle-Free</span> </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>

   <!-- CTA Btn Section Start -->
   <div class="dark-cta-sec">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-md-12 col-12 text-center ">
               <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
               <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 orange-clr">TAKE ACTION NOW!</div>
               <div class="f-18 f-md-22 lh140 w400 text-center mt20 white-clr">
                  Use Coupon Code <span class="w700 orange-clr">“WEBPULL”</span> for an Additional <span class="w700 orange-clr">$3 Discount</span> on Commercial Licence
               </div>
            </div>
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="https://warriorplus.com/o2/a/wyl4gg/0" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab WebPull + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
               <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-24 f-22 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
               <div class="countdown counter-white white-clr">
                  <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">00</span><br><span class="f-14 f-md-18 ">Days</span> </div>
                  <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">09</span><br><span class="f-14 f-md-18 w500 ">Hours</span> </div>
                  <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald w700">11</span><br><span class="f-14 f-md-18 w500 ">Mins</span> </div>
                  <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald w700">58</span><br><span class="f-14 f-md-18 w500">Sec</span> </div>
               </div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>
   <!-- CTA Btn Section End -->

   <div class="step-section1">
      <div class="container ">
         <div class="row ">
            <div class="col-12 f-28 f-md-38 w700 lh140 black-clr text-center">
               Create Stunning Websites For <br class="d-none d-md-block">
               You or Clients <span class="orange-clr">in Just 3 Easy Steps</span>
            </div>
            <div class="col-12 mt30 mt-md50">
               <div class="row align-items-center ">
                  <div class="col-12 col-md-6">
                     <div class="step-shapes f-24 f-md-32 w600">
                        Step 1
                     </div>
                     <div class="f-24 f-md-38 w600 lh140 mt15 mt-md25">
                        Choose
                     </div>
                     <div class="f-18 f-md-20 w400 lh140 mt5 mt-md15">
                        Select a theme with done-for-you content from pre-loaded 400+ stunning business themes.
                     </div>
                     <img src="https://cdn.oppyo.com/launches/webpull/special/left-arrow.webp" class="img-fluid d-none d-md-block ms-auto step-arrow1">
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 ">
                     <img src="https://cdn.oppyo.com/launches/webpull/special/choose.webp " class="img-fluid d-block mx-auto ">
                  </div>
               </div>
            </div>
            <div class="col-12 mt30 mt-md50">
               <div class="row align-items-center ">
                  <div class="col-12 col-md-6 order-md-2">
                     <div class="step-shapes f-24 f-md-32 w600">
                        Step 2
                     </div>
                     <div class="f-24 f-md-38 w600 lh140 mt15 mt-md25">
                        Customize
                     </div>
                     <div class="f-18 f-md-20 w400 lh140 mt5 mt-md15">
                        Upload the logo, add your business name & customize it as per your business needs without touching any code. It’s super easy.
                     </div>
                     <img src="https://cdn.oppyo.com/launches/webpull/special/right-arrow.webp" class="img-fluid d-none d-md-block step-arrow1">
                  </div>
                  <div class="col-md-6 col-md-pull-6 mt20 mt-md0 order-md-1 ">
                     <img src="https://cdn.oppyo.com/launches/webpull/special/edit.webp " class="img-fluid d-block mx-auto ">
                  </div>
               </div>
            </div>
            <div class="col-12 mt30 mt-md50">
               <div class="row align-items-center ">
                  <div class="col-12 col-md-6">
                     <div class="step-shapes f-24 f-md-32 w600">
                        Step 3
                     </div>
                     <div class="f-24 f-md-38 w600 lh140 mt15 mt-md25">
                        Publish, Sell, and Profit
                     </div>
                     <div class="f-18 f-md-20 w400 lh140 mt5 mt-md15">
                        Now publish your website with a click to use it for your own business or sell websites to business owners in 1000+ niches.
                     </div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 ">
                     <img src="https://cdn.oppyo.com/launches/webpull/special/publish-report.webp " class="img-fluid d-block mx-auto ">
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>

   <!-- Cta Section -->
   <div class="dark-cta-sec">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-md-12 col-12 text-center ">
               <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
               <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 orange-clr">TAKE ACTION NOW!</div>
               <div class="f-18 f-md-22 lh140 w400 text-center mt20 white-clr">
                  Use Coupon Code <span class="w700 orange-clr">“WEBPULL”</span> for an Additional <span class="w700 orange-clr">$3 Discount</span> on Commercial Licence
               </div>
            </div>
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="https://warriorplus.com/o2/a/wyl4gg/0" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab WebPull + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
               <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-24 f-22 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
               <div class="countdown counter-white white-clr">
                  <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">00</span><br><span class="f-14 f-md-18 ">Days</span> </div>
                  <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">06</span><br><span class="f-14 f-md-18 w500 ">Hours</span> </div>
                  <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald w700">19</span><br><span class="f-14 f-md-18 w500 ">Mins</span> </div>
                  <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald w700">02</span><br><span class="f-14 f-md-18 w500">Sec</span> </div>
               </div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>
   <!-- Cta Section End -->
   <!-- Premium Websites Section -->
   <div class="premium-sec">
      <div class="container">
         <div class="row">
            <div class="col-12 ">
               <div class="f-28 f-md-45 w700 lh140 black-clr text-center">
                  See My REAL Earning from Selling Websites <br class="d-none d-md-block">
                  Built with WebPull (In 60 Seconds)
               </div>
            </div>
            <div class="col-12 mx-auto mt20 mt-md50">
               <img src="https://cdn.oppyo.com/launches/webpull/special/proof6.webp" class="img-fluid d-block mx-auto">
            </div>
         </div>
         <div class="row mt20 mt-md80">
            <div class="col-12 f-md-44 f-28 w500 lh140 black-clr text-center">
               Look At Some More Real-Life Proofs <br>
               <span class="w700">New Freelancers & Agencies are Making Tons of Money Building Simple Websites for Clients</span>
            </div>
         </div>
         <div class="row mt-md70 mt20 align-items-center">
            <div class="col-md-5">
               <img src="https://cdn.oppyo.com/launches/webpull/special/boomer-smith.webp" alt="" class="img-fluid d-block mx-auto">
            </div>
            <div class="col-md-7 f-md-20 f-18 w400 mt-md0 mt20">
               <span class="f-22 f-md-28 w700">Boomer Smith has made $20,400 on Fiverr in just 4 months of his start in Dec’21 & still counting. </span>
               <br><br>
               After quitting his Tele calling Job which he never liked, he started searching for any Freelancing opportunity on the internet where he encountered Fiverr and found 2 Big words Wed Design, and WordPress.
               <br><br>
               Now, he is charging $1,200 per project and has completed almost 17 projects in just 4 months which made him $20,400 in income.
            </div>
         </div>
         <div class="row mt-md70 mt20 align-items-center">
            <div class="col-md-5 order-md-2">
               <img src="https://cdn.oppyo.com/launches/webpull/special/brain.webp" alt="" class="img-fluid d-block mx-auto">
            </div>
            <div class="col-md-7 f-md-20 f-18 w400 mt-md0 mt20 order-md-1">
               <span class="f-22 f-md-28 w700"> Meet Brain C. ‘Rising Talent’ earned almost $17,495 in just 47 Days on Upwork. </span>

               <br><br>

               His journey started on Upwork with Graphic Designing in 2019. But as he came to know about many newbies earning huge just by servicing WordPress Website development, he gave it a shot and started earning more than his graphic design service.
               <br><br>
               He started his Website Development Services for the Lawyers & charges them $3,499 per website on Upwork, which Lawyers are happy to pay.
            </div>
         </div>
         <div class="row mt-md70 mt20 align-items-center">
            <div class="col-md-6">
               <img src="https://cdn.oppyo.com/launches/webpull/special/bright.webp" alt="" class="img-fluid d-block mx-auto">
            </div>
            <div class="col-md-6 f-md-20 f-18 w400 mt-md0 mt20">
               <span class="f-22 f-md-28 w700">Bright Dock has made almost $183k (till yet) on Freelancer </span>
               <br><br>
               He has completed Over 180 successfully completed Freelancer projects in under 2 years.
               <br><br>
               He started his WordPress Designing Freelancing journey 2 years ago on Freelancer and now has a freelancing team of 40 digital designers to work for him, so he just focuses on growth. He is charging $100 per Hr. and people are ready to pay for his awesome job.
            </div>
         </div>
      </div>
   </div>
   <!-- Premium Websites Section End -->
   
   <!-- Testimonial Section Start -->
   <div class="testimonial-section2">
      <div class="container ">
         <div class="row ">
            <div class="col-12 f-28 f-md-45 w700 lh140 black-clr text-center">
               Even Our Beta Users Are Getting Amazing <br class="d-none d-md-block">
               Results With Stunning Websites
            </div>
         </div>
         <div class="row row-cols-md-2 row-cols-1 gx4">
         <div class="col mt50">
               <div class="single-testimonial">
                  <p>
                     After losing all my assets and job in the pandemic, WEBPULL came out as a God Gift for me. I never knew that for a newbie like me, building a professional website could be so easy and life changing. <br><br>

                     WEBPULL is the easiest and most efficient way to start your own Freelancing Work on Fiverr, Upwork, etc, and tap into big Industry. <span class="w600">Today, I am making an average income of $4700/month & that too in just 3 months. </span>
                  </p>
                  <div class="client-info">
                     <img src="https://cdn.oppyo.com/launches/webpull/special/lewis-grey.webp" class="img-fluid d-block mx-auto">
                     <div class="client-details">
                        Lewis Grey
                     </div>
                  </div>
               </div>
            </div>
            <div class="col mt50">
               <div class="single-testimonial">
                  <p>
                     WEBPULL is one of the Best Website Builders I have come across. Being a freelancer, it was never easy for me to build websites for my clients from different businesses. And I was able to serve hardly 2-3 clients monthly, and cost more due to various plugins I must buy.  <br><br>
                     <span class="w600">But DANG! WEBPULL has changed it over, now I am serving 15 -20 clients per month and making upto $17,000 monthly,</span> with no additional cost of website development, and that is 4X more profits for me. 
                  </p>
                  <div class="client-info">
                     <img src="https://cdn.oppyo.com/launches/webpull/special/john-warner.webp" class="img-fluid d-block mx-auto">
                     <div class="client-details">
                        John Warner
                     </div>
                  </div>
               </div>
            </div>
            <div class="col mt50">
               <div class="single-testimonial">
                  <p>
                  I started my freelance website developer journey 4 months back, but being a newbie, I was struggling in creating a good website. But thanks for the early access to WEBPULL team, now <span class="w600">I can make a stunning, highly professional website quick & easy and able to serve more clients who happily pay me $4000 - $5000 each. </span>
                  </p>
                  <div class="client-info">
                     <img src="https://cdn.oppyo.com/launches/webpull/special/stuart-johnson.webp" class="img-fluid d-block mx-auto">
                     <div class="client-details">
                        Stuart Johnson
                     </div>
                  </div>
               </div>
            </div>
            <div class="col mt50">
               <div class="single-testimonial">
                  <p>
                     WEBPULL helped me to create my first real estate website in just 30 minutes and the best part is that I am not a tech geek.  
                     <br><br>
                     <span class="w600">Now, I’m able to get more FREE traffic & targeted leads online through my website. It helped me reach to my prospects in much lower cost & boosted profits by almost 3 times.</span> This is something that I really won’t forget in my life. 
                  </p>
                  <div class="client-info">
                     <img src="https://cdn.oppyo.com/launches/webpull/special/chuk-akspan.webp" class="img-fluid d-block mx-auto">
                     <div class="client-details">
                        Chuk Akspan
                     </div>
                  </div>
               </div>
            </div>
         </div>

      </div>
   </div>
   <!-- Testimonial Section End -->

   <!-- Cta Section -->
   <div class="dark-cta-sec">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-md-12 col-12 text-center ">
               <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
               <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 orange-clr">TAKE ACTION NOW!</div>
               <div class="f-18 f-md-22 lh140 w400 text-center mt20 white-clr">
                  Use Coupon Code <span class="w700 orange-clr">“WEBPULL”</span> for an Additional <span class="w700 orange-clr">$3 Discount</span> on Commercial Licence
               </div>
            </div>
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="https://warriorplus.com/o2/a/wyl4gg/0" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab WebPull + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
               <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-24 f-22 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
               <div class="countdown counter-white white-clr">
                  <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">00</span><br><span class="f-14 f-md-18 ">Days</span> </div>
                  <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">06</span><br><span class="f-14 f-md-18 w500 ">Hours</span> </div>
                  <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald w700">18</span><br><span class="f-14 f-md-18 w500 ">Mins</span> </div>
                  <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald w700">10</span><br><span class="f-14 f-md-18 w500">Sec</span> </div>
               </div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>
   <!-- Cta Section End -->
   <!-- Over Section Start -->
   <div class="over-section">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <img src="https://cdn.oppyo.com/launches/webpull/special/over-img.webp" alt="" class="img-fluid d-block mx-auto">
               <!-- <img src="https://cdn.oppyo.com/launches/webpull/special/siteefy.webp" alt="" class="img-fluid d-block mx-auto mt-md50 mt20"> -->
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md70">
            <div class="col-12">
               <div class="f-22 f-md-24 w400 lh140 white-clr text-center">
                  In this never-ending pandemic era, every business needs to switch to digital to survive,<br class="d-none d-md-block"> and having a website for any business is not an option anymore…
               </div>
               <div class="f-28 f-md-45 w700 orange-clr  text-center my10">
                  It’s a Compulsion Today!
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Over Section End -->
   <!-- Research Section Start -->
   <div class="research-section">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 f-24 f-md-36 w700 lh140 black-clr text-center">
               No Business Owner Would Like To Lose Those Visitors<br class="d-none d-md-block"> & Big Sales From All The Digital Sources
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md70">
            <div class="col-12 col-md-6 f-22 f-md-26 w600 lh140 black-clr order-md-2">
               As When You Have an SEO & Mobile Optimized Website Then You Seamlessly Get Customers from All 360-Degree Directions.
            </div>
            <div class="col-12 col-md-6 order-md-1 mt20 mt-md0">
               <img src="https://cdn.oppyo.com/launches/webpull/special/f-image.webp " class="img-fluid d-block mx-autos ms-md-auto" />
            </div>
         </div>
         <div class="row mt20 mt-md0 align-items-center">
            <div class="col-12 col-md-5 f-22 f-md-26 w600 lh140 black-clr">
               <span class="d-flex justify-content-md-end">And You Drive Non-Stop Traffic, Leads & Profits 24 by 7 - 100% Hands-Free </span>
            </div>
            <div class="col-12 col-md-7 mt20 mt-md50">
               <img src="https://cdn.oppyo.com/launches/webpull/special/h-image.webp " class="img-fluid d-block mx-autos" />
            </div>
         </div>
         <div class="row mt20 mt-md50">
            <div class="col-12 f-20 f-md-24 w500 lh140 black-clr text-center"><span class="w700">So, when it comes to doing a Business, Websites are the Present and The Future! </span> <br class="d-none d-md-block">In Easy Words, Every Business Need a Website Today!
            </div>
            <div class="col-12 mt-md50 mt20 f-md-28 f-22 w600 blue-clr lh140 text-center">
               And that opens a door to
            </div>
            <div class="col-12 mt-md30 mt20 f-md-50 f-28 w700 white-clr lh140 text-center">
               <div class="over-shape">
                  BIG $284 Billion Opportunity.
               </div>
            </div>
         </div>
         <div class="row mt30 mt-md65">
            <div class="f-md-28 f-22 w400 lh140 black-clr  text-center">
               <span class="w700 f-md-45 f-28 lh140"> Now You’re Probably Wondering? </span><br> “Can I Make a Full-Time Income?”
            </div>
         </div>
      </div>
   </div>
   <!-- Research Section End -->
   <div class="course-section">
      <div class="container">
         <div class="row">
            <div class="col-12 f-20 f-md-28 w500 lh140 text-center">
               <span class="w700 f-24 f-md-36"> Answer Is - Of Course, You Can! </span><br> See How Newbies Made Thousands of Dollars from Website Services
            </div>
            <div class="col-12 col-md-12 mx-auto mt20 mt-md50">
               <div class="row">
                  <div class="col-12 col-md-6 mt-md0 p-md-0">
                     <img src="https://cdn.oppyo.com/launches/webpull/special/proofimg1.webp" class="img-fluid d-block mx-auto">
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0  p-md-0">
                     <img src="https://cdn.oppyo.com/launches/webpull/special/proofimg2.webp" class="img-fluid d-block mx-auto">
                  </div>
                  <div class="col-12 col-md-6 p-md-0">
                     <img src="https://cdn.oppyo.com/launches/webpull/special/proofimg3.webp" class="img-fluid d-block mx-auto">
                  </div>
                  <div class="col-12 col-md-6 p-md-0">
                     <img src="https://cdn.oppyo.com/launches/webpull/special/proofimg4.webp" class="img-fluid d-block mx-auto">
                  </div>
               </div>
            </div>
            <div class="col-12 f-20 f-md-28 w400 lh140 text-center mt20 mt-md30">
               Now is the perfect time to get in and profit from this exponential growth!
            </div>
         </div>
         <div class="row mt30 mt-md50">
            <div class="col-12 text-center">
               <div class="presenting-shape f-md-45 f-28 w700 text-center white-clr lh140 text-uppercase"> IMPRESSIVE, RIGHT? </div>
            </div>
         </div>
         <div class="row mt20 mt-md50">
            <div class="col-12  f-24 f-md-36 lh140 w600 text-center black-clr lh140">
               So, how would you like to get your share starting today?
            </div>
         </div>
      </div>
   </div>
   <!-- Amit Pareek Section End -->

   <div class="next-gen-sec" id="product">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="presenting-shape2 f-md-32 f-24 w600 text-center white-clr lh140 text-uppercase">
                  Presenting... </div>
            </div>
            <div class="col-12 mt-md20 mt20 text-center">
               <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 394.2 100" style="enable-background:new 0 0 394.2 100; max-height:111px" xml:space="preserve">
                  <style type="text/css">
                     .st00 {
                        fill: #FFFFFF;
                     }

                     .st11 {
                        fill: url(#SVGID_1_);
                     }

                     .st22 {
                        fill: url(#SVGID_00000021093791325859232720000015953972570224053676_);
                     }

                     .st33 {
                        fill: #FFC802;
                     }

                     .st44 {
                        fill: url(#SVGID_00000003068462962921929030000010945183465240898967_);
                     }
                  </style>
                  <path class="st00" d="M117.7,37.9l-5,12.4H92.9v12.4h18.6l-5,12.4H92.9v12.4h24.8l-5,12.5H92.9H80.5V75.1V62.7V37.9H117.7z"></path>
                  <path class="st00" d="M323.2,37.9v49.7H348V100h-37.3V37.9H323.2z"></path>
                  <path class="st00" d="M369.4,37.9v49.7h24.8V100h-37.3V37.9H369.4z"></path>
                  <g>
                     <g>
                        <polygon class="st00" points="59.1,71.4 52.4,54.4 59.1,37.9 72.6,37.9   "></polygon>
                        <polygon class="st00" points="29.4,54.7 36.3,37.9 54.5,82.9 47.7,100   "></polygon>
                        <polygon class="st00" points="24.8,100 31.6,82.8 13.4,37.9 0,37.9   "></polygon>
                     </g>
                  </g>
                  <path class="st00" d="M222.8,44.3c-4.3-4.3-9.4-6.4-15.4-6.4h-9.3h-12.5v21.7v21.7V100h12.5V81.3h0V68.9h0v-9.3v-9.3h9.3  c2.6,0,4.8,0.9,6.6,2.8c1.8,1.8,2.7,4,2.7,6.6c0,2.6-0.9,4.8-2.7,6.6c-1.8,1.8-4,2.7-6.6,2.7h-0.7v12.4h0.7c6,0,11.1-2.1,15.4-6.4  c4.3-4.3,6.4-9.4,6.4-15.4C229.2,53.6,227.1,48.5,222.8,44.3z"></path>
                  <rect x="252.1" y="31.8" transform="matrix(0.4226 -0.9063 0.9063 0.4226 114.0209 255.0236)" class="st00" width="10.2" height="12.5"></rect>
                  <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="249.3741" y1="97.3262" x2="270.7371" y2="53.5256">
                     <stop offset="0" style="stop-color:#FA8460"></stop>
                     <stop offset="1" style="stop-color:#FCB121"></stop>
                  </linearGradient>
                  <path class="st11" d="M271.7,80.3c-1.4,3.1-3.8,5.2-7,6.4c-3.2,1.2-6.4,1-9.5-0.4c-3.1-1.4-5.2-3.8-6.4-7c-1.2-3.2-1-6.4,0.4-9.5  l9.9-21.2l-11.3-5.3l-9.9,21.2c-2.9,6.2-3.2,12.6-0.8,19c2.3,6.5,6.6,11.1,12.9,14c6.2,2.9,12.5,3.2,19,0.8  c6.5-2.3,11.2-6.6,14.1-12.8l9.9-21.2l-11.3-5.3L271.7,80.3z"></path>
                  <rect x="285.9" y="47.6" transform="matrix(0.4226 -0.9063 0.9063 0.4226 119.2494 294.738)" class="st00" width="10.2" height="12.5"></rect>
                  <path class="st00" d="M173.3,76.3c-0.8-1.7-1.7-3.2-2.9-4.5c-1.2-1.3-2.5-2.3-4-3.1c-1.5-0.8-3-1.2-4.5-1.3c1.5-0.1,2.8-0.5,4.1-1.3  c1.2-0.8,2.3-1.8,3.2-3c0.9-1.2,1.6-2.5,2.1-3.9c0.5-1.4,0.8-2.8,0.8-4c0-3.5-0.5-6.4-1.4-8.6c-1-2.2-2.3-4-3.9-5.3  c-1.6-1.3-3.6-2.2-5.8-2.6c-2.2-0.5-4.6-0.7-7-0.7h-27.3v10.5h11.9v0h15.4c0.8,0,1.7,0.2,2.4,0.6c0.8,0.4,1.4,0.9,2,1.5  c0.6,0.6,1,1.4,1.4,2.2c0.3,0.9,0.5,1.7,0.5,2.6c0,0.9-0.2,1.8-0.5,2.8c-0.3,0.9-0.8,1.8-1.4,2.5c-0.6,0.7-1.3,1.3-2.2,1.8  c-0.9,0.5-1.9,0.7-3.1,0.7h-9.1v10.5h10c2.7,0,4.9,0.7,6.4,2.1c1.6,1.4,2.3,3.2,2.3,5.3c0,1-0.2,2-0.6,3c-0.4,1-0.9,1.9-1.6,2.7  c-0.7,0.8-1.5,1.5-2.4,2c-0.9,0.5-1.9,0.7-2.9,0.7h-16.8h0V73.7h0V63.2h0v-9.3h-11.9V100h9.6h2.3h16.8c2.7,0,5.2-0.3,7.5-0.9  c2.3-0.6,4.4-1.6,6.2-3.1c1.8-1.4,3.1-3.3,4.2-5.7c1-2.3,1.5-5.2,1.5-8.6C174.4,79.8,174,78,173.3,76.3z"></path>
                  <g>
                     <linearGradient id="SVGID_00000142155450205615942230000010409392281687221429_" gradientUnits="userSpaceOnUse" x1="270.7616" y1="25.6663" x2="296.63" y2="25.6663">
                        <stop offset="0" style="stop-color:#FA8460"></stop>
                        <stop offset="4.692155e-02" style="stop-color:#FA9150"></stop>
                        <stop offset="0.1306" style="stop-color:#FBA43A"></stop>
                        <stop offset="0.2295" style="stop-color:#FBB229"></stop>
                        <stop offset="0.3519" style="stop-color:#FCBC1E"></stop>
                        <stop offset="0.5235" style="stop-color:#FCC117"></stop>
                        <stop offset="1" style="stop-color:#FCC315"></stop>
                     </linearGradient>
                     <path style="fill:url(#SVGID_00000142155450205615942230000010409392281687221429_);" d="M289.6,14.2c-2.1-1.1-4.3-1.5-6.5-1.4   c-0.1,0-0.1,0-0.2,0c-0.1,0-0.2,0-0.2,0c0,0-0.1,0-0.1,0c-0.1,0-0.2,0-0.2,0c-0.1,0-0.2,0-0.3,0c-0.1,0-0.2,0-0.2,0   c-0.3,0-0.5,0.1-0.8,0.1c-0.1,0-0.2,0-0.3,0.1c-0.1,0-0.2,0-0.3,0.1c-0.1,0-0.3,0.1-0.4,0.1c-0.1,0-0.2,0-0.2,0.1   c-0.1,0-0.2,0.1-0.3,0.1c-0.2,0.1-0.5,0.2-0.7,0.3c0,0,0,0,0,0c-0.3,0.1-0.6,0.3-0.9,0.4c-0.3,0.1-0.6,0.3-0.9,0.5   c-0.1,0.1-0.3,0.2-0.4,0.2c-0.1,0.1-0.2,0.2-0.4,0.2c-0.1,0.1-0.2,0.1-0.2,0.2c0,0-0.1,0-0.1,0.1c-0.1,0.1-0.2,0.1-0.2,0.2   c-0.1,0-0.1,0.1-0.2,0.2c-0.1,0.1-0.2,0.2-0.3,0.3c-0.1,0-0.1,0.1-0.2,0.2c-0.5,0.4-0.9,0.9-1.3,1.4c-0.1,0.1-0.1,0.1-0.2,0.2   c0,0-0.1,0.1-0.1,0.1c-0.1,0.1-0.1,0.2-0.2,0.3c0,0-0.1,0.1-0.1,0.1c-0.1,0.1-0.1,0.2-0.2,0.3c0,0,0,0.1-0.1,0.1   c-0.1,0.1-0.1,0.2-0.2,0.3c0,0,0,0.1-0.1,0.1c-0.1,0.1-0.1,0.2-0.2,0.3c-0.1,0.1-0.2,0.3-0.2,0.4c-3.3,6.3-0.8,14.1,5.6,17.4   c1.9,1,3.9,1.4,5.9,1.4c0.1,0,0.3,0,0.4,0c0.1,0,0.3,0,0.4,0c0.1,0,0.1,0,0.2,0c0.1,0,0.2,0,0.3,0c0,0,0,0,0.1,0c0.1,0,0.2,0,0.3,0   c0.1,0,0.2,0,0.2,0c0.1,0,0.2,0,0.3-0.1c0.1,0,0.3,0,0.4-0.1c0.1,0,0.3-0.1,0.4-0.1c0.1,0,0.2-0.1,0.3-0.1c0.2,0,0.4-0.1,0.5-0.2   c0.1,0,0.3-0.1,0.4-0.1c0.1,0,0.3-0.1,0.4-0.1c0,0,0,0,0,0c0.1,0,0.2-0.1,0.4-0.1c0,0,0,0,0,0c0,0,0.1,0,0.1,0c0,0,0.1,0,0.1-0.1   c0,0,0,0,0.1,0c0,0,0.1,0,0.1,0c0,0,0,0,0.1,0c0.1,0,0.2-0.1,0.3-0.2c0,0,0,0,0.1,0c0.1-0.1,0.2-0.1,0.3-0.2   c0.1-0.1,0.3-0.1,0.4-0.2c0.4-0.2,0.7-0.5,1.1-0.7c0.2-0.2,0.5-0.4,0.7-0.6c0.2-0.2,0.4-0.4,0.7-0.6c0.2-0.2,0.4-0.4,0.6-0.6   c0.2-0.2,0.4-0.5,0.6-0.7c0.1-0.1,0.2-0.2,0.3-0.4c0,0,0,0,0,0c0.1-0.1,0.2-0.2,0.3-0.4c0.1-0.1,0.2-0.3,0.2-0.4   c0.1-0.1,0.1-0.2,0.2-0.3c0-0.1,0.1-0.1,0.1-0.2c0.1-0.1,0.1-0.2,0.2-0.3C298.5,25.3,296,17.5,289.6,14.2z M279.6,26.7L279.6,26.7   l0.3,0.6c0.4,0.7,1,1.3,1.8,1.7c0.3,0.2,0.7,0.3,1,0.3c0.3,0,0.5-0.1,0.7-0.2c0.1,0,0.1-0.1,0.2-0.2c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0-0.1c0.2-0.3,0.2-0.7-0.1-1.3c0,0,0,0,0,0   c-0.1-0.2-0.2-0.4-0.4-0.6c-0.1-0.1-0.2-0.2-0.3-0.3c-0.4-0.4-0.7-0.8-0.9-1.2c-0.1-0.1-0.2-0.2-0.2-0.4c0,0,0,0,0-0.1   c0,0,0-0.1-0.1-0.1c0,0,0-0.1-0.1-0.1c-0.1-0.2-0.1-0.3-0.2-0.5c-0.1-0.2-0.1-0.4-0.2-0.6c0,0,0-0.1,0-0.1c0-0.1,0-0.2,0-0.3v-0.1   c0-0.1,0-0.1,0-0.2c0,0,0-0.1,0-0.1c0-0.1,0-0.2,0-0.3c0.1-0.3,0.2-0.6,0.3-0.8c0,0,0,0,0-0.1c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0-0.1c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0l0,0c0.4-0.5,0.9-0.9,1.5-1.1c0.7-0.2,1.4-0.3,2.1-0.1l0.9-1.7l1.9,1l-0.8,1.6c0.7,0.4,1.2,1,1.6,1.6   l0.2,0.4l-1.8,1.6l-0.3-0.5c-0.1-0.1-0.3-0.4-0.5-0.7c-0.1-0.1-0.3-0.3-0.5-0.4c0,0-0.1-0.1-0.1-0.1c-0.1-0.1-0.3-0.2-0.4-0.2   c0,0-0.1,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0   c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0h0c0,0,0,0-0.1,0h0   c0,0,0,0-0.1,0h0c0,0,0,0,0,0h0c0,0,0,0,0,0h0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c-0.2,0.1-0.4,0.3-0.5,0.5c-0.3,0.5-0.2,0.9,1,2.3c0.8,0.9,1.2,1.6,1.4,2.4   c0.2,0.8,0.1,1.6-0.3,2.5c-0.8,1.5-2.4,2.2-4.2,1.8l-0.8,1.6l-0.1,0.2l-2-1l0.9-1.7c-0.1-0.1-0.2-0.1-0.3-0.2   c-0.3-0.2-0.5-0.4-0.8-0.6c0,0-0.1-0.1-0.1-0.1c-0.3-0.3-0.5-0.6-0.7-1l-0.2-0.4L279.6,26.7z"></path>
                     <path class="st33" d="M278,37c-6.3-3.3-8.8-11.1-5.6-17.4c1.3-2.4,3.2-4.3,5.4-5.5c-2.4,1.2-4.4,3.1-5.7,5.6   c-3.3,6.3-0.8,14.1,5.6,17.4c3.9,2,8.4,1.9,12-0.1C286.2,38.9,281.8,39,278,37z"></path>
                  </g>
                  <g>
                     <linearGradient id="SVGID_00000016062571573767098340000005260099975199807150_" gradientUnits="userSpaceOnUse" x1="79.734" y1="-130.471" x2="91.3973" y2="-130.471" gradientTransform="matrix(0.8624 -0.5063 0.5063 0.8624 260.6894 161.6657)">
                        <stop offset="0" style="stop-color:#FA8460"></stop>
                        <stop offset="4.692155e-02" style="stop-color:#FA9150"></stop>
                        <stop offset="0.1306" style="stop-color:#FBA43A"></stop>
                        <stop offset="0.2295" style="stop-color:#FBB229"></stop>
                        <stop offset="0.3519" style="stop-color:#FCBC1E"></stop>
                        <stop offset="0.5235" style="stop-color:#FCC117"></stop>
                        <stop offset="1" style="stop-color:#FCC315"></stop>
                     </linearGradient>
                     <path style="fill:url(#SVGID_00000016062571573767098340000005260099975199807150_);" d="M268.1,0c-1,0.1-2,0.4-2.8,0.9   c0,0-0.1,0-0.1,0c0,0-0.1,0-0.1,0.1c0,0,0,0,0,0c0,0-0.1,0-0.1,0.1c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0-0.1,0.1   c-0.1,0.1-0.2,0.1-0.3,0.2c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0.1-0.1,0.1   c-0.1,0.1-0.1,0.2-0.2,0.3c0,0,0,0,0,0c-0.1,0.1-0.2,0.2-0.2,0.4c-0.1,0.1-0.2,0.3-0.2,0.4c0,0.1-0.1,0.1-0.1,0.2   c0,0.1-0.1,0.1-0.1,0.2c0,0,0,0.1-0.1,0.1c0,0,0,0,0,0.1c0,0,0,0.1-0.1,0.1c0,0,0,0.1,0,0.1c0,0.1,0,0.1-0.1,0.2c0,0,0,0.1,0,0.1   c-0.1,0.3-0.1,0.5-0.2,0.8c0,0,0,0.1,0,0.1c0,0,0,0.1,0,0.1c0,0.1,0,0.1,0,0.2c0,0,0,0,0,0.1c0,0.1,0,0.1,0,0.2c0,0,0,0,0,0.1   c0,0.1,0,0.1,0,0.2c0,0,0,0,0,0.1c0,0.1,0,0.1,0,0.2c0,0.1,0,0.1,0,0.2c0.2,3.2,2.9,5.7,6.1,5.5c1-0.1,1.9-0.3,2.6-0.8   c0.1,0,0.1-0.1,0.2-0.1c0.1,0,0.1-0.1,0.1-0.1c0,0,0,0,0.1,0c0,0,0.1,0,0.1-0.1c0,0,0,0,0,0c0,0,0.1-0.1,0.1-0.1c0,0,0.1,0,0.1-0.1   c0,0,0.1-0.1,0.1-0.1c0,0,0.1-0.1,0.1-0.1c0,0,0.1-0.1,0.1-0.1c0,0,0.1-0.1,0.1-0.1c0.1-0.1,0.1-0.1,0.2-0.2c0,0,0.1-0.1,0.1-0.1   c0,0,0.1-0.1,0.1-0.1c0,0,0,0,0,0c0,0,0.1-0.1,0.1-0.1c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0-0.1c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0.1-0.1,0.1-0.1c0,0,0,0,0,0c0,0,0.1-0.1,0.1-0.1c0-0.1,0.1-0.1,0.1-0.2c0.1-0.2,0.2-0.4,0.3-0.5c0.1-0.1,0.1-0.2,0.1-0.4   c0-0.1,0.1-0.3,0.1-0.4c0-0.1,0.1-0.3,0.1-0.4c0-0.1,0-0.3,0.1-0.4c0-0.1,0-0.1,0-0.2c0,0,0,0,0,0c0-0.1,0-0.1,0-0.2   c0-0.1,0-0.1,0-0.2c0-0.1,0-0.1,0-0.2c0,0,0-0.1,0-0.1c0-0.1,0-0.1,0-0.2C274.1,2.3,271.3-0.2,268.1,0z M267.1,7.2L267.1,7.2   l0.3,0.2c0.3,0.2,0.7,0.3,1.1,0.3c0.2,0,0.3-0.1,0.4-0.1c0.1-0.1,0.2-0.1,0.2-0.3c0,0,0-0.1,0-0.1c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0-0.2-0.1-0.3-0.3-0.5c0,0,0,0,0,0   c-0.1,0-0.2-0.1-0.3-0.1c-0.1,0-0.1,0-0.2-0.1c-0.2-0.1-0.4-0.2-0.6-0.2c-0.1,0-0.1-0.1-0.2-0.1c0,0,0,0,0,0c0,0,0,0-0.1,0   c0,0,0,0-0.1,0c-0.1,0-0.1-0.1-0.2-0.1c-0.1-0.1-0.1-0.1-0.2-0.2c0,0,0,0,0,0c0,0-0.1-0.1-0.1-0.1l0,0c0,0,0,0,0-0.1c0,0,0,0,0,0   c0,0,0-0.1-0.1-0.1c0-0.1-0.1-0.3-0.1-0.4c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0l0,0   c0-0.3,0.2-0.5,0.3-0.8c0.2-0.2,0.5-0.4,0.8-0.5l0-0.9l1-0.1l0,0.8c0.4,0,0.7,0.1,1,0.3l0.2,0.1l-0.3,1l-0.3-0.1   c-0.1,0-0.2-0.1-0.4-0.1c-0.1,0-0.2,0-0.3-0.1c0,0-0.1,0-0.1,0c-0.1,0-0.1,0-0.2,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0l0,0c0,0,0,0,0,0l0,0c0,0,0,0,0,0l0,0c0,0,0,0,0,0l0,0c0,0,0,0,0,0l0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c-0.1,0.1-0.1,0.2-0.1,0.3c0,0.2,0.1,0.4,0.9,0.7c0.5,0.2,0.8,0.4,1.1,0.6c0.3,0.3,0.4,0.6,0.4,1c0,0.8-0.4,1.4-1.2,1.7l0,0.8   l0,0.1l-1,0.1l0-0.8c-0.1,0-0.1,0-0.2,0c-0.2,0-0.3,0-0.4-0.1c0,0-0.1,0-0.1,0c-0.2-0.1-0.4-0.1-0.5-0.2l-0.2-0.1L267.1,7.2z"></path>
                     <path class="st33" d="M268.8,11.5c-3.2,0.2-6-2.3-6.1-5.5c-0.1-1.2,0.3-2.4,0.9-3.4c-0.7,1-1,2.2-0.9,3.5c0.2,3.2,2.9,5.7,6.1,5.5   c2-0.1,3.7-1.2,4.6-2.8C272.4,10.4,270.7,11.4,268.8,11.5z"></path>
                  </g>
                  <polygon class="st00" points="297.8,43.3 296.4,42.7 294.6,46.7 295.4,41.2 296.8,41.8 298.7,37.9 "></polygon>
                  <polygon class="st00" points="264.2,27.2 262.9,26.4 260.7,30.2 262,24.8 263.3,25.6 265.5,21.8 "></polygon>
               </svg>
            </div>
            <div class="f-md-36 f-22 w600 white-clr lh140 col-12 mt20 mt-md40 text-center">
               A Brand-New Technology Lets You Creates &amp; Sell Stunning <br class="d-none d-md-block"> Websites for BIG Profits In 1000+ Niches In 60 Seconds Flat…
            </div>
            <div class="f-18 f-md-22 lh140 w500 text-center white-clr mt20">
               Replace your old-school website builder with the amazing solution that<br class="d-none d-md-block"> helps you create lightning-fast, high converting and SEO-ready websites.
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md50 gx-4">
            <div class="col-12 col-md-6">
               <img src="https://cdn.oppyo.com/launches/webpull/jv/product-box.webp" class="img-fluid d-block mx-auto" alt="Product">
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <div class="proudly-list-bg">
                  <div class="col-12">
                     <ul class="proudly-tick pl0 m0 f-18 f-md-18 w400 black-clr lh160">
                        <li><span class="w600 f-20 f-md-22">Create Any Type Of Site </span> <br>
                           Create Affiliate Niche Sites, Local Sites, Marketplace, E-Com Stores, or Blogs Easily
                        </li>
                        <li><span class="w600 f-20 f-md-22">Super-Customizable &amp; Easy To Use
                           </span> <br>
                           Preloaded with 400+ Done-For-You Stunning Themes To Create Site In Any Niche </li>
                        <li><span class="w600 f-20 f-md-22">No Worries of Paying Monthly</span> <br>
                           During This Launch Special Deal, Get All Benefits At Limited Low One-Time-Fee.
                        </li>
                        <li><span class="w600 f-20 f-md-22">No Limits - Use for Yourself Or Clients
                           </span> <br>
                           Sell Your Own Products, Services or Affiliate Offers or Charge Your Clients for Elegant Websites.
                        </li>
                        <li style="padding-bottom:0px"><span class="w600 f-20 f-md-22">50+ More Features </span> <br>
                           We’ve Left No Stone Unturned to Give You an Unmatched Experience
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>


   <!-------Exclusive Bonus----------->
   <div class="exclusive-bonus">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <div class="white-clr f-24 f-md-36 lh140 w600">
                  Exclusive Bonus #1 : JOBiin
               </div>
               <div class="f-18 f-md-20 w500 mt20 white-clr">
                  20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-------Exclusive Bonus----------->


   <div class="jobiin-header-section">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <svg id="Layer_1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 128.19 38" style="max-height:55px;">
                  <defs>
                     <style>
                        .cls-1,
                        .cls-2 {
                           fill: #fff;
                        }

                        .cls-2 {
                           fill-rule: evenodd;
                        }

                        .cls-3 {
                           fill: #0a52e2;
                        }
                     </style>
                  </defs>
                  <rect class="cls-1" x="79.21" width="48.98" height="38" rx="4" ry="4"></rect>
                  <path class="cls-1" d="M18.64,4.67V24.72c0,1.29-.21,2.47-.63,3.54-.42,1.07-1.03,1.97-1.84,2.71-.81,.74-1.79,1.32-2.93,1.74-1.15,.42-2.45,.63-3.9,.63s-2.64-.19-3.7-.57c-1.07-.38-1.98-.92-2.75-1.62-.77-.7-1.39-1.54-1.88-2.51S.19,26.6,0,25.41l5.74-1.13c.46,2.45,1.63,3.68,3.52,3.68,.89,0,1.65-.28,2.28-.85,.63-.57,.95-1.46,.95-2.67V9.6H3.92V4.67h14.72Z"></path>
                  <path class="cls-1" d="M54.91,33.37V4.63h9.71c3.38,0,6.02,.66,7.92,1.97,1.9,1.32,2.84,3.28,2.84,5.9,0,1.33-.35,2.52-1.06,3.56-.7,1.05-1.73,1.83-3.07,2.36,1.72,.37,3.02,1.16,3.88,2.37,.86,1.21,1.29,2.61,1.29,4.21,0,2.75-.91,4.83-2.72,6.25-1.82,1.42-4.39,2.12-7.72,2.12h-11.08Zm5.76-16.7h4.15c1.54,0,2.72-.32,3.55-.95,.83-.63,1.24-1.55,1.24-2.76,0-1.33-.42-2.31-1.25-2.94-.84-.63-2.08-.95-3.74-.95h-3.95v7.6Zm0,3.99v8.27h5.31c1.53,0,2.69-.33,3.49-.99,.8-.66,1.2-1.64,1.2-2.94,0-1.4-.34-2.48-1.03-3.22-.68-.74-1.76-1.11-3.24-1.11h-5.75Z"></path>
                  <g>
                     <path class="cls-1" d="M46.84,4.19C42.31-.12,36.87-1.08,31.16,1.2c-5.82,2.33-9.1,6.88-9.52,13.21-.41,6.17,2.57,10.9,6.84,15.15,.95-.84,1.22-1.22,2.01-2.05-.79-.65-1.41-1.09-2.43-2.11-5.86-6.15-5.78-15.66,1.04-20.46,5-3.52,11.71-3.13,16.37,.95,4.29,3.75,5.54,10.44,2.59,15.61-1.2,2.11-3.09,3.84-4.86,5.98,.28,.45,.72,1.18,1.44,2.35,1.77-2.01,3.11-3.72,4.38-5.63,4.39-6.6,3.56-14.59-2.17-20.02Z"></path>
                     <g>
                        <path class="cls-2" d="M39.67,27.34c-2.49,.37-4.9,.52-7.23-.46-.54-.23-1.13-1.06-1.18-1.65-.3-3.69,1.25-5.27,4.98-5.26,.31,0,.62,0,.92,.01,5.31-.01,5.96,6.85,2.51,7.36Z"></path>
                        <path class="cls-2" d="M39.49,16.69c-.04,1.69-1.37,2.98-3.06,2.98-1.6,0-3.01-1.43-3.01-3.06s1.39-3.04,3.02-3.03c1.75,0,3.1,1.38,3.06,3.12Z"></path>
                     </g>
                     <g>
                        <g>
                           <path class="cls-2" d="M42.93,26.23c-.67-3.46-.93-5.25-3.46-7.18,.63-1.21,1.3-1.83,2.1-2,2.43-.53,3.98,.21,4.84,1.97,.66,1.34,.78,2.29-.37,3.77-1.03,1.17-1.85,2.21-3.11,3.43Z"></path>
                           <path class="cls-2" d="M44.94,13.4c.01,1.6-1.27,2.91-2.88,2.92-1.6,.01-2.91-1.28-2.92-2.88-.01-1.6,1.28-2.9,2.88-2.92,1.6-.01,2.91,1.28,2.92,2.88Z"></path>
                        </g>
                        <g>
                           <path class="cls-2" d="M30.1,26.23c.67-3.46,.93-5.25,3.46-7.18-.63-1.21-1.3-1.83-2.1-2-2.43-.53-3.98,.21-4.84,1.97-.66,1.34-.88,2.01,.27,3.49,.96,1.3,1.88,2.44,3.21,3.71Z"></path>
                           <path class="cls-2" d="M28.08,13.4c0,1.6,1.28,2.91,2.88,2.92,1.6,.01,2.91-1.28,2.92-2.88,.01-1.6-1.27-2.9-2.88-2.92-1.6-.01-2.91,1.28-2.92,2.88Z"></path>
                        </g>
                     </g>
                     <path class="cls-1" d="M42.02,27.74c-.7,.41-6.95,2.1-10.73,.08l-2.22,2.57c.55,.61,5.12,5.27,7.55,7.6,2.22-2.17,7.2-7.37,7.2-7.37l-1.8-2.89Z"></path>
                  </g>
                  <g>
                     <rect class="cls-3" x="85.05" y="4.63" width="5.38" height="5.4" rx="2.69" ry="2.69"></rect>
                     <rect class="cls-3" x="85.05" y="13.43" width="5.38" height="19.94"></rect>
                     <rect class="cls-3" x="95.13" y="4.63" width="5.38" height="5.4" rx="2.69" ry="2.69"></rect>
                     <rect class="cls-3" x="95.13" y="13.43" width="5.38" height="19.94"></rect>
                     <path class="cls-3" d="M109.85,13.43l.24,2.86c.66-1.02,1.48-1.81,2.45-2.38,.97-.56,2.06-.85,3.26-.85,2.01,0,3.59,.63,4.72,1.9,1.13,1.27,1.7,3.25,1.7,5.95v12.46h-5.4v-12.47c0-1.34-.27-2.29-.81-2.85-.54-.56-1.36-.84-2.45-.84-.71,0-1.35,.14-1.92,.43-.57,.29-1.04,.7-1.42,1.23v14.5h-5.38V13.43h5.01Z"></path>
                  </g>
               </svg>
            </div>
            <div class="col-12 text-center mt20 mt-md50 lh130">
               <div class=" f-md-20 f-16 w600 lh130 headline-highlight">
                  Copy &amp; Paste Our SECRET FORMULA That Makes Us $328/Day Over &amp; Over Again!
               </div>
            </div>
            <div class="col-12 mt-md25 mt20 f-md-46 f-26 w600 text-center white-clr lh140">
               EXPOSED–A Breakthrough Software That<span class="w700 orange-clr"> Creates <span class="tm">
                     Indeed<sup class="f-20 w500 lh200">TM</sup>
                  </span> Like JOB Search Sites in Next 60 Seconds</span> with 100% Autopilot Job Listings, FREE Traffic &amp; Passive Commissions
            </div>
            <div class="col-12 mt-md25 mt20 f-18 f-md-22 w400 text-center lh140 grey-clr">
               Pre-Loaded with 10 million+ Job Listings I Earn Commissions on Every Click on Listings &amp; Banners | <br class="d-none d-md-block"> No Content Writing I No Tech Hassel Ever… Even A Beginner Can Start Right Away
            </div>
         </div>
         <div class="row d-flex align-items-center flex-wrap mt20 mt-md40">
            <div class="col-md-10 col-12 mx-auto">
               <div class="responsive-video">
                  <iframe src="https://jobiin.dotcompal.com/video/embed/x83jxcd4vl" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                        box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
               </div>
            </div>
            <div class="col-12 f-20 f-md-26 w700 lh150 text-center white-clr mt-md50 mt20">
               LIMITED TIME - FREE COMMERCIAL LICENSE INCLUDED
            </div>
         </div>
      </div>
      <img src="https://cdn.oppyo.com/launches/jobiin/common_assets/images/header-left-element.webp" alt="Header Element " class="img-fluid d-none header-left-element ">
      <img src="https://cdn.oppyo.com/launches/jobiin/common_assets/images/header-right-element.webp" alt="Header Element " class="img-fluid d-none header-right-element ">
   </div>

   <div class="jobiin-second-section">
      <div class="container">
         <div class="row">
            <div class="col-12 z-index-9">
               <div class="row jobiin-header-list-block">
                  <div class="col-12 col-md-6">
                     <ul class="features-list pl0 f-18 f-md-20 lh150 w500 black-clr text-capitalize">
                        <li><span class="w700">Tap into the $212 Billion Recruitment Industry</span></li>
                        <li><span class="w700">Create Multiple UNIQUE Job Sites</span> – Niche Focused (Automotive, Banking Jobs etc.) or City Focused (New York, London Jobs etc.)</li>
                        <li><span class="w700">100% Automated Job Posting &amp; Updates</span> on Your Site from 10 million+ Open Jobs</li>
                        <li>Get Job Listed from TOP Brands &amp; Global Giants <span class="w700">Like Amazon, Walmart, Costco, etc.</span></li>
                        <li>Create Website in <span class="w700">13 Different Languages &amp; Target 30+ Top Countries</span></li>
                        <li class="w700">Built-In FREE Search and Social Traffic from Google &amp; 80+ Social Media Platforms</li>
                        <li class="w700">Built-In SEO Optimised Fresh Content &amp; Blog Articles</li>
                     </ul>
                  </div>
                  <div class="col-12 col-md-6 mt10 mt-md0">
                     <ul class="features-list pl0 f-18 f-md-20 lh150 w500 black-clr text-capitalize">
                        <li class="w700">Make Commissions from Top Recruitment Companies on Every Click of Job Listings</li>
                        <li>Promote TOP Affiliate Offers Through <span class="w700">Banner and Text Ads for Extra Commission</span></li>
                        <li>Even Apply Yourself for Any <span class="w700">Good Part-Time Job to Make Extra Income</span></li>
                        <li>Easy And Intuitive to Use Software with <span class="w700">Step-by-Step Video Training</span></li>
                        <li><span class="w700">Get Started Immediately</span> – Launch Done-for-You Job Site and Start Making Money Right Away</li>
                        <li><span class="w700">Made For Absolute Newbies</span> &amp; Experienced Marketers</li>
                        <li class="w700">PLUS, FREE COMMERCIAL LICENSE IF YOU Start TODAY!</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>

   <div class="gr-1">
      <div class="container-fluid p0">
         <picture>
            <source media="(min-width:768px)" srcset="assets/images/jobiin-steps.webp">
            <source media="(min-width:320px)" srcset="assets/images/jobiin-steps-mview.webp">
            <img src="assets/images/jobiin-steps.webp" alt="Steps" style="width:100%;">
         </picture>
         <picture>
            <source media="(min-width:768px)" srcset="assets/images/jobiin-big-job.webp">
            <source media="(min-width:320px)" srcset="assets/images/jobiin-big-job-mview.webp">
            <img src="assets/images/jobiin-big-job.webp" alt="Real Result" style="width:100%;">
         </picture>
         <picture>
            <source media="(min-width:768px)" srcset="assets/images/jobiin-impressive.webp">
            <source media="(min-width:320px)" srcset="assets/images/jobiin-impressive-mview.webp">
            <img src="assets/images/jobiin-impressive.webp" alt="Real Result" style="width:100%;">
         </picture>
         <picture>
            <source media="(min-width:768px)" srcset="assets/images/jobiin-proudly-presenting.webp">
            <source media="(min-width:320px)" srcset="assets/images/jobiin-proudly-presenting-mview.webp">
            <img src="assets/images/jobiin-proudly-presenting.webp" alt="Real Result" style="width:100%;">
         </picture>
      </div>
   </div>

   <!----------Buziffy ---------->
   <div class="exclusive-bonus">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <div class="white-clr f-24 f-md-36 lh140 w600">
                  Exclusive Bonus #2 : Buzzify
               </div>
               <div class="f-18 f-md-20 w500 mt20 white-clr">
                  20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS
                  If you already have this bonus, you will get more licenses to sell (really great to have more)
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="buz-header-section">
      <div class="container">
         <div class="row">

            <div class="col-12">
               <div class="row">
                  <div class="col-md-3 text-center mx-auto">
                     <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 973.3 276.9" style="enable-background:new 0 0 973.3 276.9; max-height:40px;" xml:space="preserve">
                        <style type="text/css">
                           .st0 {
                              fill: #FFFFFF;
                           }
                        </style>
                        <g>
                           <path class="st0" d="M0,212.5V12.1h98.6c19.3,0,34.4,4.2,45.3,12.7c10.9,8.5,16.4,21,16.4,37.7c0,20.8-8.6,35.5-25.7,44.1
                                 c14,2.2,24.8,7.6,32.2,16.1c7.5,8.5,11.2,19.7,11.2,33.6c0,11.7-2.6,21.8-7.9,30.2c-5.3,8.4-12.8,14.8-22.5,19.3
                                 c-9.7,4.5-21.2,6.7-34.5,6.7H0z M39.2,93.3h50.3c10.7,0,18.8-2.2,24.5-6.6c5.6-4.4,8.4-10.7,8.4-19c0-8.3-2.8-14.6-8.3-18.8
                                 c-5.5-4.2-13.7-6.3-24.6-6.3H39.2V93.3z M39.2,181.9h60.6c12.3,0,21.6-2.2,27.7-6.7c6.2-4.5,9.3-11.2,9.3-20.2
                                 c0-8.9-3.2-15.8-9.5-20.6c-6.4-4.8-15.5-7.2-27.5-7.2H39.2V181.9z"></path>
                           <path class="st0" d="M268.1,216.4c-22.8,0-39.1-4.5-49.1-13.5c-10-9-15-23.6-15-44V88.2h37.3v61.9c0,13.1,2,22.4,6,27.7
                                 c4,5.4,10.9,8.1,20.8,8.1c9.7,0,16.6-2.7,20.6-8.1c4-5.4,6.1-14.6,6.1-27.7V88.2h37.3V159c0,20.3-5,35-14.9,44
                                 C307.2,211.9,290.9,216.4,268.1,216.4z"></path>
                           <path class="st0" d="M356.2,212.5l68.6-95.9h-58.7V88.2h121.6L419,184.1h64.5v28.4H356.2z"></path>
                           <path class="st0" d="M500.3,212.5l68.6-95.9h-58.7V88.2h121.6l-68.7,95.9h64.5v28.4H500.3z"></path>
                           <path class="st0" d="M679.8,62.3c-4.1,0-7.8-1-11.1-3c-3.4-2-6-4.7-8-8.1s-3-7.1-3-11.2c0-4,1-7.7,3-11c2-3.3,4.7-6,8-8
                                 c3.3-2,7-3,11.1-3c4,0,7.7,1,11.1,3c3.3,2,6,4.6,8,8c2,3.3,3,7,3,11c0,4.1-1,7.8-3,11.2c-2,3.4-4.7,6.1-8,8.1
                                 C687.5,61.3,683.8,62.3,679.8,62.3z M661.2,212.5V88.2h37.3v124.4H661.2z"></path>
                           <path class="st0" d="M747.9,212.5v-96.1h-16.8V88.2h16.8V57.3c0-11.7,1.8-21.9,5.5-30.5c3.6-8.6,8.9-15.2,15.7-19.9
                                 c6.8-4.7,15-7,24.6-7c5.9,0,11.7,0.9,17.5,2.7c5.7,1.8,10.7,4.4,14.8,7.6l-12.9,26.1c-3.8-2.3-8.1-3.5-12.9-3.5
                                 c-4.2,0-7.4,1.1-9.6,3.2c-2.2,2.1-3.7,5.1-4.4,8.9c-0.8,3.8-1.2,8.2-1.2,13.1v30.1h29.4v28.3h-29.4v96.1H747.9z"></path>
                           <path class="st0" d="M835.6,275.7l40.1-78.7l-59-108.8h42.7l38,72.9l33.2-72.9h42.7l-95,187.5H835.6z"></path>
                        </g>
                     </svg>
                  </div>
               </div>
            </div>
            <!-- <div class="col-12 mt20 mt-md50"> <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/demo-vid1.png" class="img-fluid d-block mx-auto"></div> -->
            <div class="col-12 text-center lh140 mt20 mt-md30">
               <div class="pre-heading-b f-md-20 f-18 w600 lh140">
                  <div class="skew-con d-flex gap20 align-items-center ">
                     It’s Time to Get Over Boring Content and Cash into The Latest Trending Topics in 2022
                  </div>
               </div>
            </div>
            <div class="col-12 mt-md30 mt20 f-md-40 f-28 w600 text-center white-clr lh140">
               Breakthrough 3-Click Software Uses a <span class="under yellow-clr w700"> Secret Method to Make Us $528/Day Over and Over</span> Again Using the Power of Trending Content &amp; Videos
            </div>
            <div class="col-12 mt-md25 mt20 f-18 f-md-21 w600 text-center lh140 yellow-clr">
               All of That Without Content Creation, Editing, Camera, or Tech Hassles Ever! Even Newbies Can Get Unlimited FREE Traffic and Steady PASSIVE Income…Every Day
            </div>
         </div>
         <div class="row d-flex align-items-center flex-wrap mt20 mt-md40">
            <div class="col-md-10 mx-auto col-12 mt20 mt-md0">
               <div class="col-12 responsive-video">
                  <iframe src="https://buzzify.dotcompal.com/video/embed/satfj9nvaq" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                        box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="bsecond-section">
      <div class="container">
         <div class="row">
            <div class="col-12 p-md0">
               <div class="col-12 key-features-bg-b d-flex align-items-center flex-wrap">
                  <div class="row">
                     <div class="col-12 col-md-6">
                        <ul class="list-head-b pl0 m0 f-16 f-md-18 lh160 w400 white-clr text-capitalize">
                           <li><span class="w600">Creates Beautiful &amp; Self-Updating Sites </span>with Hot Trending Content &amp; Videos </li>
                           <li class="w600">Built-In 1-Click Traffic Generating System </li>
                           <li class="w600">Puts Most Profitable Links on Your Websites using AI </li>
                           <li>Legally Use Other’s Trending Content To Generate Automated Profits- <span class="w600">Works in Any Niche or TOPIC </span></li>
                           <li>1-Click Social Media Automation – <span class="w600">People ONLY WANT TRENDY Topics These Days To Click.</span> </li>
                           <li><span class="w600">100% SEO Friendly </span> Website and Built-In Remarketing System </li>
                           <li>Complete Newbie Friendly And <span class="w600">Works Even If You Have No Prior Experience And Technical Skills</span> </li>
                        </ul>
                     </div>
                     <div class="col-12 col-md-6">
                        <ul class="list-head-b pl0 m0 f-16 f-md-18 lh160 w400 white-clr text-capitalize">
                           <li><span class="w600">Set and Forget System</span> With Single Keyword – Set Rules To Find &amp; Publish Trending Posts</li>
                           <li><span class="w600">Automatically Translate Your Sites In 15+ Language</span> According To Location For More Traffic</li>
                           <li><span class="w600">Make 5K-10K With Commercial License </span>That Allows You To Serve Your Clients &amp; Charge Them</li>
                           <li><span class="w600">Integration with Major Platforms</span> Including Autoresponders And Social Media Apps </li>
                           <li><span class="w600">In-Built Content Spinner</span> to Make your Content Fresh and More Engaging </li>
                           <li><span class="w600">A-Z Complete Video Training </span>Is Included To Make It Easier For You </li>
                           <li><span class="w600">Limited-Time Special</span> Bonuses Worth $2285 If You Buy Today </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>

      </div>
   </div>
   <div class="gr-1">
      <div class="container-fluid p0">
         <picture>
            <source media="(min-width:768px)" srcset="https://cdn.oppyo.com/launches/webpull/special-bonus/b3d.webp">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/webpull/special-bonus/b3m.webp">
            <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/b3d.webp" alt="Flowers" style="width:100%;">
         </picture>
         <picture>
            <source media="(min-width:768px)" srcset="https://cdn.oppyo.com/launches/webpull/special-bonus/bnd.webp">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/webpull/special-bonus/bnm.webp">
            <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/bnd.webp" alt="Flowers" style="width:100%;">
         </picture>
         <picture>
            <source media="(min-width:768px)" srcset="https://cdn.oppyo.com/launches/webpull/special-bonus/bpd.webp">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/webpull/special-bonus/bpm.webp">
            <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/bpd.webp" alt="Flowers" style="width:100%;">
         </picture>
      </div>
   </div>
   <!----------Buziffy ---------->
   <!------Ninjakash--------->
   <div class="exclusive-bonus">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <div class="white-clr f-24 f-md-36 lh140 w600">
                  Exclusive Bonus #3 : Ninja Kash
               </div>
               <div class="f-18 f-md-20 w500 mt20 white-clr">
                  My Recent Launch Ninja Kash - With Reseller License (worth $11999)
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="grand-logo-bg">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/logo-gr.webp" class="img-fluid mx-auto d-block">
            </div>
         </div>
      </div>
   </div>
   <div class="header-section-n">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="mt20 mt-md50">
                  <div class="f-md-24 f-20 w500 lh140 text-center black-clr post-heading-n">
                     Weird System Pays Us $500-1000/Day for Just Copying and Pasting...
                  </div>
               </div>
            </div>
            <div class="col-12">
               <div class="col-12 mt-md30 mt20">
                  <div class="f-28 f-md-50 w500 text-center white-clr lh140">
                     <span class="w700"> Break-Through App Creates a Ninja Affiliate Funnel In 5 Minutes Flat</span> That Drives FREE Traffic &amp; Sales on 100% Autopilot
                  </div>
               </div>
               <div class="col-12 f-22 f-md-28 w600 text-center  orange lh140 mt-md30 mt20">
                  NO Worries For Finding Products | NO Paid Traffic | No Tech Skills
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-md-7 col-12 mt-md40 mt20 px-md15 min-md-video-width-left">
               <!--<div>
                  <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/video-bg.png" class="img-responsive center-block"> 
                     </div>-->
               <div class="col-12 mt-md15">
                  <div class="col-12 responsive-video">
                     <iframe src="https://ninjakash.dotcompal.com/video/embed/t4spbhm1vi" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                        box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                  </div>
               </div>
            </div>
            <div class="col-md-5 col-12 f-20 f-md-20 lh140 w400 white-clr mt-md40 mt20 pl-md15 min-md-video-width-right">
               <ul class="list-head-b pl0 m0">
                  <li>Kickstart with 50 Hand-Picked Products</li>
                  <li>Promote Any Offer in Any niche HANDS FREE</li>
                  <li>SEO Optimized, Mobile Responsive Affiliate Funnels</li>
                  <li>Drive TONS OF Social &amp; Viral traffic</li>
                  <li>Build Your List and Convert AFFILIATE Sales in NO Time</li>
                  <li>No Monthly Fees…EVER</li>
               </ul>
            </div>
         </div>
      </div>
   </div>
   <div class="gr-1">
      <div class="container-fluid p0">
         <picture>
            <source media="(min-width:768px)" srcset="https://cdn.oppyo.com/launches/webpull/special-bonus/gr2.webp">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/webpull/special-bonus/gr2a.webp">
            <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/gr2.webp" alt="Flowers" style="width:100%;">
         </picture>
         <picture>
            <source media="(min-width:768px)" srcset="https://cdn.oppyo.com/launches/webpull/special-bonus/gr1.webp">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/webpull/special-bonus/gr1a.webp">
            <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/gr1.webp" alt="Flowers" style="width:100%;">
         </picture>
      </div>
   </div>
   <!------Ninjakash--------->

   <!-- VidRankXpress Start-->
   <div class="exclusive-bonus">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <div class="white-clr f-24 f-md-36 lh140 w600">
                  Exclusive Bonus #4 : VidRankXpress
               </div>
               <div class="f-18 f-md-20 w500 mt20 white-clr">
                  20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="grand-logo-bg">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/vidrankxpress.webp" alt="VidRankXpress Logo" class="img-fluid mx-auto d-block">
            </div>
         </div>
      </div>
   </div>
   <div class="vidrankxpress-header-section">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center white-clr f-md-20 f-18 lh140">
               Sick and tired of wasting countless efforts & money with NO Ranking & Views for your videos?
            </div>
            <div class="col-md-12 mt30 white-clr w700 f-md-40 f-28 lh140 text-center">
               Revolutionary Software <span class="red-color"> <span class="tstrip1"> Turns ANY Video into Endless</span> <span class="tstrip2">FREE Traffic Machine By Getting Page 1 Ranking on</span> <span class="tstrip3">YouTube <sup>TM</sup> & Google <sup>TM</sup></span> </span> and Tons of Traffic from TOP Social Giants...
            </div>
            <div class="col-12 col-md-10 mx-auto mt30">
               <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/phl.webp" alt="Line" class="img-fluid mx-auto d-block">
               <div class="mt20 f-md-20 f-18 w500 text-center white-clr">
                  Follow VERY EASY 3 Steps... to get Unlimited Traffic for Any Video in ANY Niche
               </div>
               <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/phl.webp" alt="Line" class="img-fluid mx-auto d-block mt20">
            </div>
         </div>
         <div class="row mt30">
            <div class="col-md-7">
               <div class="responsive-video">
                  <iframe width="560" height="315" src="https://www.youtube.com/embed/SuHFy4LAiFo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
               </div>
            </div>
            <div class="col-md-5 mt20 mt-md0">
               <ul class="f-md-18 f-18 lh130 w400 white-clr pl0 m0 vidrangexpress-features">
                  <li><span class="red-color w700">PAGE #1 RANKINGS –</span> Get TOP Ranks for Your Videos on Google &amp; YouTube Search.</li>
                  <li><span class="red-color w700">UNLIMITED Viral Traffic –</span> Drive FREE Buyer-Ready Traffic from TOP Social Networking Sites.</li>
                  <li><span class="red-color w700">In-Depth COMPETITIVE Analysis –</span> With Suggested and Trending Keywords of Top Performing Videos.</li>
                  <li><span class="red-color w700">SEO Optimized Videos -</span> By Creating Perfect Meta Title &amp; Description Using Top Performing Video Suggestions.</li>
                  <li><span class="red-color w700">Super-Easy to Use –</span> Completely Newbie Friendly, Simple 3 steps - No Learning Curve.</li>
                  <li><span class="red-color w700">Commercial License Included -</span> charge $97/month/client for your services.
                  </li>
               </ul>
            </div>
         </div>
      </div>
   </div>

   <div class="gr-1">
      <div class="container-fluid p0">
         <picture>
            <source media="(min-width:768px)" srcset="https://cdn.oppyo.com/launches/webpull/special-bonus/steps-vidrangexpress.webp">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/webpull/special-bonus/steps-mview-vidrangexpress.webp">
            <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/steps-vidrangexpress.webp" alt="Steps" style="width:100%;">
         </picture>
         <picture>
            <source media="(min-width:768px)" srcset="https://cdn.oppyo.com/launches/webpull/special-bonus/realresults-vidrankxpress.webp">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/webpull/special-bonus/realresults-mview-vidrankxpress.webp">
            <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/realresults-vidrankxpress.webp" alt="Real Result" style="width:100%;">
         </picture>
         <picture>
            <source media="(min-width:768px)" srcset="https://cdn.oppyo.com/launches/webpull/special-bonus/proudly-vidrankxpress.webp">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/webpull/special-bonus/Proudly-mview-virankxpress.webp">
            <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/proudly-vidrankxpress.webp" alt="Real Result" style="width:100%;">
         </picture>
      </div>
   </div>
   <!-- VidRankXpress End -->

   <!-------Exclusive Bonus----------->
   <div class="exclusive-bonus">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <div class="white-clr f-24 f-md-36 lh140 w600">
                  Exclusive Bonus #5 : Vidvee
               </div>
               <div class="f-18 f-md-20 w500 mt20 white-clr">
                  20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
               </div>
            </div>
         </div>
      </div>
   </div>
   <!------Vidvee Section------>
   <div class="vidvee-header-section">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/vidvee.logo.png" alt="" class="d-block mx-auto img-fluid">
            </div>
            <div class="col-12 text-center mt20 mt-md50 lh130">
               <div class="vidvee-pre-heading f-md-24 f-20 w600 lh130">
                  1 Click Exploits YouTube Loophole to Legally Use Their 800 Million+<br class="d-none d-md-block">
                  Awesome Videos for Making Us $528 Every Day Again &amp; Again...
               </div>
            </div>
            <div class="col-12 mt-md25 mt20 text-center">
               <div class="f-md-50 f-30 w500 white-clr lh130 line-center">
                  Breakthrough Software <span class="w700 vidvee-orange-clr">Creates Self-Growing Beautiful Video Channels Packed With RED-HOT Videos And Drives FREE Autopilot Traffic On Any Topic </span>
                  In Just 7 Minutes Flat…
               </div>
               <div class="mt-md25 mt20 f-20 f-md-24 w500 text-center lh130 white-clr">
                  No Content Creation. No Camera or Editing. No Tech Hassles Ever... 100% Beginner Friendly!
               </div>
               <div class="row d-flex align-items-center flex-wrap mt20 mt-md40">
                  <div class="col-md-10 mx-auto col-12 mt20 mt-md0">
                     <div class="col-12 responsive-video">
                        <iframe src="https://vidvee.dotcompal.com/video/embed/xcz62wags4" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                                                box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                     </div>
                  </div>
                  <div class="col-12 mt20 mt-md40 vidvee-key-features-bg">
                     <div class="row flex-wrap">
                        <div class="col-12 col-md-6">
                           <ul class="list-head pl0 m0 f-20 f-md-20 lh130 w400 white-clr">
                              <li>Complete Video &amp; YouTube Business Builder</li>
                              <li>Ground-Breaking Method Brings Unlimited Traffic To Any Offer, Page Or Link</li>
                              <li>Tap Into 800 Million YouTube Videos &amp; Make Any Video Yours Legally</li>
                              <li>Build Beautiful &amp; Branded Video Channels on Any Topic in Any Niche Within Minutes</li>
                           </ul>
                        </div>
                        <div class="col-12 col-md-6">
                           <ul class="list-head pl0 m0 f-20 f-md-20 lh130 w400 white-clr">
                              <li>Can Upload, Host &amp; Play Your Own Videos</li>
                              <li>100% FREE Viral, Social &amp; SEO Traffic (Powerful)</li>
                              <li>Make Tons of Sales, Amazon/Affiliate Commissions &amp; Leads.</li>
                              <li><u>FREE Commercial License</u> - Build An Incredible Income Offering Services &amp; Keep 100%!</li>
                           </ul>
                        </div>
                     </div>
                  </div>

               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="gr-1">
      <div class="container-fluid p0">
         <picture>
            <source media="(min-width:768px)" srcset="https://cdn.oppyo.com/launches/webpull/special-bonus/vidvee-steps.webp">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/webpull/special-bonus/vidvee-steps-mview.webp" style="width:100%" class="vidvee-mview">
            <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/vidvee-steps.webp" alt="Vidvee Steps" class="img-fluid">
         </picture>
         <picture>
            <source media="(min-width:768px)" srcset="https://cdn.oppyo.com/launches/webpull/special-bonus/no-doubt-vidvee.webp">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/webpull/special-bonus/no-doubt-mview-vidvee.webp">
            <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/no-doubt-vidvee.webp" alt="Flowers" style="width:100%;">
         </picture>
         <picture>
            <source media="(min-width:768px)" srcset="https://cdn.oppyo.com/launches/webpull/special-bonus/proudly-vidvee.webp">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/webpull/special-bonus/proudly-mview-vidvee.webp">
            <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/proudly-vidvee.webp" alt="Flowers" style="width:100%;">
         </picture>
      </div>
   </div>
   <!------Vidvee Section------>
   
   <!-- Bonus Section Header Start -->
   <div class="bonus-header">
      <div class="container">
         <div class="row">
            <div class="col-12 col-md-10 mx-auto heading-bg text-center">
               <div class="f-24 f-md-36 lh140 w700"> When You Purchase WebPull, You Also Get <br class="hidden-xs"> Instant Access To These 20 Exclusive Bonuses</div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus Section Header End -->

   <!-- Bonus #1 Section Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 1</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/bonus1.webp" class="img-fluid mx-auto d-block" alt="Bonus1">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">Magic Video Templates Review Pack</div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>
                           There’s no secret in the fact that videos are the #1 source to boost customer attention& engagement.
                        </li>
                        <li>
                           Using Magic Video, you can easily create your own animated video in just minutes with 100+slides to choose from.
                        </li>
                        <li>
                           This Bonus with WebPull’s DFY marketing materials will instantly change your marketing presentation to be more customer-centric & drive better results from your campaigns.
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #1 Section End -->

   <!-- Bonus #2 Section Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 2</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30">
               <div class="row">
                  <div class="col-md-5 col-12 order-md-2">
                     <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/bonus2.webp" class="img-fluid mx-auto d-block" alt="Bonus2">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md0">
                        Facebook Webinar Pro Plugin
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>
                           With this powerful WordPress plugin you can create amazing Facebook webinar landing pages & drive maximum traction for your offers.
                        </li>
                        <li>
                           Using this Bonus, you can collect unlimited leads inside WordPress and shoot them targeted
                        </li>
                        <li>
                           emails on automation using WebPull’s Autoresponder Integration
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #2 Section End -->

   <!-- Bonus #3 Section Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 3</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/bonus3.webp" class="img-fluid mx-auto d-block " alt="Bonus3">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                        WordPress Website Security
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>This specific training course is designed to help you understand how to secure and protect your valuable WordPress site.</li>
                        <li>This Bonus is a great asset to use with WebPull as it helps to secure your business website made for marketing purposes, payment transactions & various business operations.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #3 Section End -->

   <!-- Bonus #4 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 4</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12 order-md-2">
                     <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/bonus4.webp" class="img-fluid mx-auto d-block " alt="Bonus4">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                        Audio Compression Magic
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>
                           Magically reduces the file size of your digital audio products up to an astonishing 82% while maintaining top sound quality.
                        </li>
                        <li>
                           Use this bonus to deliver light sized high quality professional marketing voice overs given in WebPull.
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #4 End -->

   <!-- Bonus #5 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 5</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/bonus5.webp" class="img-fluid mx-auto d-block " alt="Bonus5">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                        Lead Book Generator
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>
                           With this powerful plugin, you can easily integrate Facebook Lead Ads with your autoresponder and have your leads added to your mailing list automatically!
                        </li>
                        <li>
                           Use this Bonus with WebPull’s Autoresponder Integration for building your list and leveraging your sales quickly with Facebook.
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #5 End -->

   <!-- CTA Button Section Start -->
   <div class="dark-cta-sec">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-md-12 col-12 text-center ">
               <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
               <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 orange-clr">TAKE ACTION NOW!</div>
               <div class="f-18 f-md-22 lh140 w400 text-center mt20 white-clr">
                  Use Coupon Code <span class="w700 orange-clr">“WEBPULL”</span> for an Additional <span class="w700 orange-clr">$3 Discount</span> on Commercial Licence
               </div>
            </div>
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab WebPull + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
               <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-24 f-22 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
               <div class="countdown counter-white white-clr">
                  <div class="timer-label text-center">
                     <span class="f-31 f-md-60 timerbg oswald w700">01</span>
                     <span class="f-14 f-md-18 w500 ">Days</span>
                  </div>
                  <div class="timer-label text-center">
                     <span class="f-31 f-md-60 timerbg oswald w700">16</span>
                     <span class="f-14 f-md-18 w500 ">Hours</span>
                  </div>
                  <div class="timer-label text-center timer-mrgn">
                     <span class="f-31 f-md-60 timerbg oswald w700">59</span>
                     <span class="f-14 f-md-18 w500 ">Mins</span>
                  </div>
                  <div class="timer-label text-center ">
                     <span class="f-31 f-md-60 timerbg oswald w700">37</span>
                     <span class="f-14 f-md-18 w500 ">Sec</span>
                  </div>
               </div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>
   <!-- CTA Button Section End -->

   <!-- Bonus #6 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 6</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12 order-md-2">
                     <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/bonus6.webp" class="img-fluid mx-auto d-block " alt="Bonus6">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                        WP Profit Doubler
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>
                           This powerful WordPress Plugin allows you to make a second offer to those visitors who are leaving without any purchase.
                        </li>
                        <li>
                           Use this Bonus with WebPull’s Sales & Email Templates to double-down profit from your abandoned leads.
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #6 End -->

   <!-- Bonus #7 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 7</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/bonus7.webp" class="img-fluid mx-auto d-block " alt="Bonus7">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                        Turbo Tube Engage Pro
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>
                           This Software helps you to make your videos Interactive with just a few simple clicks.
                        </li>
                        <li>
                           Use this Bonus to transform your Video Commercials given in WebPull into a powerful list building & profit-making machine by improving your viewers’ interactivity.
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #7 End -->

   <!-- Bonus #8 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 8</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/bonus8.webp" class="img-fluid mx-auto d-block " alt="Bonus8">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                        BIZ Landing Page Plugin
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>
                           This is an easy-to-use WordPress Plugin that creates Social-Powered Business Landing Pages in seconds.
                        </li>
                        <li>
                           This Bonus when utilized with WebPull’s DFY Themes and Marketing Templates will ease your way to build professional websites within seconds.
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #8 End -->

   <!-- Bonus #9 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 9</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/bonus9.webp" class="img-fluid mx-auto d-block " alt="Bonus9">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                        Power Tools Video Site Builder
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>
                           With this software you can instantly create your own complete money-making video site featuring AdSense and Amazon ads with 120 profitable videos sourced from YouTube.
                        </li>
                        <li>
                           Use this Bonus with DFY Videos given in WebPull to start your own video sites using WebPull DFY Themes & monetize them by showing ads on them.
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #9 End -->

   <!-- Bonus #10 start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 10</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/bonus10.webp" class="img-fluid mx-auto d-block " alt="Bonus10">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                        Chatbot Marketing Mastery
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>
                           In this guide, you’re going to learn more about how chatbots can be used for marketing, and whether chatbots are a good fit for your business.
                        </li>
                        <li>
                           This Bonus will help you to use the free chatbot WordPress plugins efficiently with DFY Sales Chat/Call Script given with WebPull.
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #10 End -->

   <!-- CTA Button Section Start -->
   <div class="dark-cta-sec">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-md-12 col-12 text-center ">
               <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
               <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 orange-clr">TAKE ACTION NOW!</div>
               <div class="f-18 f-md-22 lh140 w400 text-center mt20 white-clr">
                  Use Coupon Code <span class="w700 orange-clr">“WEBPULL”</span> for an Additional <span class="w700 orange-clr">$3 Discount</span> on Commercial Licence
               </div>
            </div>
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab WebPull + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
               <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-24 f-22 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
               <div class="countdown counter-white white-clr">
                  <div class="timer-label text-center">
                     <span class="f-31 f-md-60 timerbg oswald w700">01</span>
                     <span class="f-14 f-md-18 w500 ">Days</span>
                  </div>
                  <div class="timer-label text-center">
                     <span class="f-31 f-md-60 timerbg oswald w700">16</span>
                     <span class="f-14 f-md-18 w500 ">Hours</span>
                  </div>
                  <div class="timer-label text-center timer-mrgn">
                     <span class="f-31 f-md-60 timerbg oswald w700">59</span>
                     <span class="f-14 f-md-18 w500 ">Mins</span>
                  </div>
                  <div class="timer-label text-center ">
                     <span class="f-31 f-md-60 timerbg oswald w700">37</span>
                     <span class="f-14 f-md-18 w500 ">Sec</span>
                  </div>
               </div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>
   <!-- CTA Button Section End -->

   <!-- Bonus #11 start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 11</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/bonus11.webp" class="img-fluid mx-auto d-block " alt="Bonus11">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                        Search Marketing 2.0
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Professionally designed lead capture package that comes with necessary tools, powerful media, and complete email marketing course.</li>
                        <li>This Bonus will help you create high quality Search Engine Optimized text and graphic content along with the Web Content given in WebPull.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #11 End -->

   <!-- Bonus #12 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 12</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/bonus12.webp" class="img-fluid mx-auto d-block " alt="Bonus12">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                        Customer Loyalty Magnet
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>5-Day Training on how to build customer loyalty through incentives.</li>
                        <li>Get Email Newsletter Series, Lead Capture Webpage and Graphics to use Customer Loyalty Magnet.</li>
                        <li>Use this Bonus with WebPull as a plus to increase your overall revenue by capturing high quality leads with huge conversions.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #12 End -->

   <!-- Bonus #13 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 13</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/bonus13.webp" class="img-fluid mx-auto d-block " alt="Bonus13">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                        250 HTML Templates WP Themes and Graphics
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Get complete package of 120 HTML Templates, 10 Sales Graphics Packs, 60+ WP Themes, 17 CSS Content Template, 40 Header Images, 4 Video Squeeze Pages & many more.</li>
                        <li>This Bonus when used with DFY themes of WebPull will create multiple websites with various features & graphics in all different niches.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #13 End -->

   <!-- Bonus #14 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 14</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/bonus14.webp" class="img-fluid mx-auto d-block " alt="Bonus14">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                        WP Email Timer Plus
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>WP Email Timer Plus is a plugin that allows you to create beautiful countdown timers even inside your emails</li>
                        <li>This Bonus will increase your sales conversions up to 4x when used on the Websites build using WebPull Themes.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #14 End -->

   <!-- Bonus #15 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 15</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/bonus15.webp" class="img-fluid mx-auto d-block " alt="Bonus15">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                        Social Pop-Ups Plugin
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>With this plugin you can create your own social media pop-ups for your WordPress blog or Website.</li>
                        <li>This Bonus is must to have in WebPull to capture your visitor’s attention by showing your social media popups along with social sharing icons at footer & below the posts.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #15 End -->

   <!-- CTA Button Section Start -->
   <div class="dark-cta-sec">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-md-12 col-12 text-center ">
               <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
               <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 orange-clr">TAKE ACTION NOW!</div>
               <div class="f-18 f-md-22 lh140 w400 text-center mt20 white-clr">
                  Use Coupon Code <span class="w700 orange-clr">“WEBPULL”</span> for an Additional <span class="w700 orange-clr">$3 Discount</span> on Commercial Licence
               </div>
            </div>
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab WebPull + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
               <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-24 f-22 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
               <div class="countdown counter-white white-clr">
                  <div class="timer-label text-center">
                     <span class="f-31 f-md-60 timerbg oswald w700">01</span>
                     <span class="f-14 f-md-18 w500 ">Days</span>
                  </div>
                  <div class="timer-label text-center">
                     <span class="f-31 f-md-60 timerbg oswald w700">16</span>
                     <span class="f-14 f-md-18 w500 ">Hours</span>
                  </div>
                  <div class="timer-label text-center timer-mrgn">
                     <span class="f-31 f-md-60 timerbg oswald w700">59</span>
                     <span class="f-14 f-md-18 w500 ">Mins</span>
                  </div>
                  <div class="timer-label text-center ">
                     <span class="f-31 f-md-60 timerbg oswald w700">37</span>
                     <span class="f-14 f-md-18 w500 ">Sec</span>
                  </div>
               </div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>
   <!-- CTA Button Section End -->

   <!-- Bonus #16 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 16</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/bonus16.webp" class="img-fluid mx-auto d-block " alt="Bonus16">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                        WP In-Content Popup Pro
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>WP In-Content Popup Pro is a new plugin that lets you create attention grabbing popups within your content.</li>
                        <li>Use this with WebPull to trigger in-content video popups, image popups, text popups, or content popups which you can use to showcase your products or services</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #16 End -->

   <!-- Bonus #17 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 17</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/bonus17.webp" class="img-fluid mx-auto d-block " alt="Bonus17">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                        WP Survey Creator
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>This is a WordPress Plugin that creates surveys with different types of questions with customizable presentation on your website.</li>
                        <li>This unique bonus when combined with WebPull’s analytics feature, will help you target your clients efficiently with better understanding of consumer behavior.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #17 End -->

   <!-- Bonus #18 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 18</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/bonus18.webp" class="img-fluid mx-auto d-block " alt="Bonus18">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                        Stakk Review Pack
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>This is a Mail Pop-Up Plugin</li>
                        <li>Use this Bonus along with high converting sales & email swipes given with WebPull to show Popups right inside your mails.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #18 End -->

   <!-- Bonus #19 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 19</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/bonus19.webp" class="img-fluid mx-auto d-block " alt="Bonus19">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                        Auto Support Bot
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Auto Support Bot is A 24/7 Live Chatbot on Your Websites and Video Pages.</li>
                        <li>Use this Bonus in WebPull to Automate your Customer Support Service by showing 24x7 Live Chatbot Popup.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #19 End -->

   <!-- Bonus #20 start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w700">BONUS 20</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="https://cdn.oppyo.com/launches/webpull/special-bonus/bonus20.webp" class="img-fluid mx-auto d-block " alt="Bonus20">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                        Exit Pop Pro
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Exit Pop Pro is a WordPress Plugin that will show instant exit popup to the visitor on exiting the website.</li>
                        <li>Use this Bonus along with WebPull’s Call-To-Action feature to show highly effective Exit Popup.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #20 End -->

   <!-- Huge Woth Section Start -->
   <div class="huge-area mt30 mt-md10">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-12 text-center">
               <div class="f-md-50 f-28 lh120 w700 white-clr">That’s Huge Worth of</div>
               <br>
               <div class="f-md-50 f-28 lh120 w700 orange-clr">$3275!</div>
            </div>
         </div>
      </div>
   </div>
   <!-- Huge Worth Section End -->

   <!-- text Area Start -->
   <div class="white-section pb0">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-12 text-center">
               <div class="f-md-40 f-28 lh140 w700">So what are you waiting for? You have a great opportunity ahead + <span class="w700 blue-clr">My 20 Bonus Products</span> are making it a <span class="w700 blue-clr">completely NO Brainer!!</span></div>
            </div>
         </div>
      </div>
   </div>
   <!-- text Area End -->

   <!-- CTA Button Section Start -->
   <div class="cta-btn-section">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab WebPull + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-20 f-20 w500 text-center black">My Exclusive Bonuses Are Expiring in...</h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 col-md-10 mx-auto col-12 text-center">
               <div class="countdown counter-black">
                  <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">01</span><br><span class="f-14 f-md-18 w500  ">Days</span> </div>
                  <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">16</span><br><span class="f-14 f-md-18 w500 ">Hours</span> </div>
                  <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald w700">59</span><br><span class="f-14 f-md-18 w500 ">Mins</span> </div>
                  <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald w700">37</span><br><span class="f-14 f-md-18 w500 ">Sec</span> </div>
               </div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>
   <!-- CTA Button Section End -->

   <!--Footer Section Start -->
   <div class="footer-section">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 394.2 100" style="enable-background:new 0 0 394.2 100; height:60px;" xml:space="preserve">
                  <style type="text/css">
                     .st00 {
                        fill: #FFFFFF;
                     }

                     .st11 {
                        fill: url(#SVGID_1_);
                     }

                     .st22 {
                        fill: url(#SVGID_00000021093791325859232720000015953972570224053676_);
                     }

                     .st33 {
                        fill: #FFC802;
                     }

                     .st44 {
                        fill: url(#SVGID_00000003068462962921929030000010945183465240898967_);
                     }
                  </style>
                  <path class="st00" d="M117.7,37.9l-5,12.4H92.9v12.4h18.6l-5,12.4H92.9v12.4h24.8l-5,12.5H92.9H80.5V75.1V62.7V37.9H117.7z" />
                  <path class="st00" d="M323.2,37.9v49.7H348V100h-37.3V37.9H323.2z" />
                  <path class="st00" d="M369.4,37.9v49.7h24.8V100h-37.3V37.9H369.4z" />
                  <g>
                     <g>
                        <polygon class="st00" points="59.1,71.4 52.4,54.4 59.1,37.9 72.6,37.9   " />
                        <polygon class="st00" points="29.4,54.7 36.3,37.9 54.5,82.9 47.7,100   " />
                        <polygon class="st00" points="24.8,100 31.6,82.8 13.4,37.9 0,37.9   " />
                     </g>
                  </g>
                  <path class="st00" d="M222.8,44.3c-4.3-4.3-9.4-6.4-15.4-6.4h-9.3h-12.5v21.7v21.7V100h12.5V81.3h0V68.9h0v-9.3v-9.3h9.3  c2.6,0,4.8,0.9,6.6,2.8c1.8,1.8,2.7,4,2.7,6.6c0,2.6-0.9,4.8-2.7,6.6c-1.8,1.8-4,2.7-6.6,2.7h-0.7v12.4h0.7c6,0,11.1-2.1,15.4-6.4  c4.3-4.3,6.4-9.4,6.4-15.4C229.2,53.6,227.1,48.5,222.8,44.3z" />
                  <rect x="252.1" y="31.8" transform="matrix(0.4226 -0.9063 0.9063 0.4226 114.0209 255.0236)" class="st00" width="10.2" height="12.5" />
                  <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="249.3741" y1="97.3262" x2="270.7371" y2="53.5256">
                     <stop offset="0" style="stop-color:#FA8460" />
                     <stop offset="1" style="stop-color:#FCB121" />
                  </linearGradient>
                  <path class="st11" d="M271.7,80.3c-1.4,3.1-3.8,5.2-7,6.4c-3.2,1.2-6.4,1-9.5-0.4c-3.1-1.4-5.2-3.8-6.4-7c-1.2-3.2-1-6.4,0.4-9.5  l9.9-21.2l-11.3-5.3l-9.9,21.2c-2.9,6.2-3.2,12.6-0.8,19c2.3,6.5,6.6,11.1,12.9,14c6.2,2.9,12.5,3.2,19,0.8  c6.5-2.3,11.2-6.6,14.1-12.8l9.9-21.2l-11.3-5.3L271.7,80.3z" />
                  <rect x="285.9" y="47.6" transform="matrix(0.4226 -0.9063 0.9063 0.4226 119.2494 294.738)" class="st00" width="10.2" height="12.5" />
                  <path class="st00" d="M173.3,76.3c-0.8-1.7-1.7-3.2-2.9-4.5c-1.2-1.3-2.5-2.3-4-3.1c-1.5-0.8-3-1.2-4.5-1.3c1.5-0.1,2.8-0.5,4.1-1.3  c1.2-0.8,2.3-1.8,3.2-3c0.9-1.2,1.6-2.5,2.1-3.9c0.5-1.4,0.8-2.8,0.8-4c0-3.5-0.5-6.4-1.4-8.6c-1-2.2-2.3-4-3.9-5.3  c-1.6-1.3-3.6-2.2-5.8-2.6c-2.2-0.5-4.6-0.7-7-0.7h-27.3v10.5h11.9v0h15.4c0.8,0,1.7,0.2,2.4,0.6c0.8,0.4,1.4,0.9,2,1.5  c0.6,0.6,1,1.4,1.4,2.2c0.3,0.9,0.5,1.7,0.5,2.6c0,0.9-0.2,1.8-0.5,2.8c-0.3,0.9-0.8,1.8-1.4,2.5c-0.6,0.7-1.3,1.3-2.2,1.8  c-0.9,0.5-1.9,0.7-3.1,0.7h-9.1v10.5h10c2.7,0,4.9,0.7,6.4,2.1c1.6,1.4,2.3,3.2,2.3,5.3c0,1-0.2,2-0.6,3c-0.4,1-0.9,1.9-1.6,2.7  c-0.7,0.8-1.5,1.5-2.4,2c-0.9,0.5-1.9,0.7-2.9,0.7h-16.8h0V73.7h0V63.2h0v-9.3h-11.9V100h9.6h2.3h16.8c2.7,0,5.2-0.3,7.5-0.9  c2.3-0.6,4.4-1.6,6.2-3.1c1.8-1.4,3.1-3.3,4.2-5.7c1-2.3,1.5-5.2,1.5-8.6C174.4,79.8,174,78,173.3,76.3z" />
                  <g>
                     <linearGradient id="SVGID_00000142155450205615942230000010409392281687221429_" gradientUnits="userSpaceOnUse" x1="270.7616" y1="25.6663" x2="296.63" y2="25.6663">
                        <stop offset="0" style="stop-color:#FA8460" />
                        <stop offset="4.692155e-02" style="stop-color:#FA9150" />
                        <stop offset="0.1306" style="stop-color:#FBA43A" />
                        <stop offset="0.2295" style="stop-color:#FBB229" />
                        <stop offset="0.3519" style="stop-color:#FCBC1E" />
                        <stop offset="0.5235" style="stop-color:#FCC117" />
                        <stop offset="1" style="stop-color:#FCC315" />
                     </linearGradient>
                     <path style="fill:url(#SVGID_00000142155450205615942230000010409392281687221429_);" d="M289.6,14.2c-2.1-1.1-4.3-1.5-6.5-1.4   c-0.1,0-0.1,0-0.2,0c-0.1,0-0.2,0-0.2,0c0,0-0.1,0-0.1,0c-0.1,0-0.2,0-0.2,0c-0.1,0-0.2,0-0.3,0c-0.1,0-0.2,0-0.2,0   c-0.3,0-0.5,0.1-0.8,0.1c-0.1,0-0.2,0-0.3,0.1c-0.1,0-0.2,0-0.3,0.1c-0.1,0-0.3,0.1-0.4,0.1c-0.1,0-0.2,0-0.2,0.1   c-0.1,0-0.2,0.1-0.3,0.1c-0.2,0.1-0.5,0.2-0.7,0.3c0,0,0,0,0,0c-0.3,0.1-0.6,0.3-0.9,0.4c-0.3,0.1-0.6,0.3-0.9,0.5   c-0.1,0.1-0.3,0.2-0.4,0.2c-0.1,0.1-0.2,0.2-0.4,0.2c-0.1,0.1-0.2,0.1-0.2,0.2c0,0-0.1,0-0.1,0.1c-0.1,0.1-0.2,0.1-0.2,0.2   c-0.1,0-0.1,0.1-0.2,0.2c-0.1,0.1-0.2,0.2-0.3,0.3c-0.1,0-0.1,0.1-0.2,0.2c-0.5,0.4-0.9,0.9-1.3,1.4c-0.1,0.1-0.1,0.1-0.2,0.2   c0,0-0.1,0.1-0.1,0.1c-0.1,0.1-0.1,0.2-0.2,0.3c0,0-0.1,0.1-0.1,0.1c-0.1,0.1-0.1,0.2-0.2,0.3c0,0,0,0.1-0.1,0.1   c-0.1,0.1-0.1,0.2-0.2,0.3c0,0,0,0.1-0.1,0.1c-0.1,0.1-0.1,0.2-0.2,0.3c-0.1,0.1-0.2,0.3-0.2,0.4c-3.3,6.3-0.8,14.1,5.6,17.4   c1.9,1,3.9,1.4,5.9,1.4c0.1,0,0.3,0,0.4,0c0.1,0,0.3,0,0.4,0c0.1,0,0.1,0,0.2,0c0.1,0,0.2,0,0.3,0c0,0,0,0,0.1,0c0.1,0,0.2,0,0.3,0   c0.1,0,0.2,0,0.2,0c0.1,0,0.2,0,0.3-0.1c0.1,0,0.3,0,0.4-0.1c0.1,0,0.3-0.1,0.4-0.1c0.1,0,0.2-0.1,0.3-0.1c0.2,0,0.4-0.1,0.5-0.2   c0.1,0,0.3-0.1,0.4-0.1c0.1,0,0.3-0.1,0.4-0.1c0,0,0,0,0,0c0.1,0,0.2-0.1,0.4-0.1c0,0,0,0,0,0c0,0,0.1,0,0.1,0c0,0,0.1,0,0.1-0.1   c0,0,0,0,0.1,0c0,0,0.1,0,0.1,0c0,0,0,0,0.1,0c0.1,0,0.2-0.1,0.3-0.2c0,0,0,0,0.1,0c0.1-0.1,0.2-0.1,0.3-0.2   c0.1-0.1,0.3-0.1,0.4-0.2c0.4-0.2,0.7-0.5,1.1-0.7c0.2-0.2,0.5-0.4,0.7-0.6c0.2-0.2,0.4-0.4,0.7-0.6c0.2-0.2,0.4-0.4,0.6-0.6   c0.2-0.2,0.4-0.5,0.6-0.7c0.1-0.1,0.2-0.2,0.3-0.4c0,0,0,0,0,0c0.1-0.1,0.2-0.2,0.3-0.4c0.1-0.1,0.2-0.3,0.2-0.4   c0.1-0.1,0.1-0.2,0.2-0.3c0-0.1,0.1-0.1,0.1-0.2c0.1-0.1,0.1-0.2,0.2-0.3C298.5,25.3,296,17.5,289.6,14.2z M279.6,26.7L279.6,26.7   l0.3,0.6c0.4,0.7,1,1.3,1.8,1.7c0.3,0.2,0.7,0.3,1,0.3c0.3,0,0.5-0.1,0.7-0.2c0.1,0,0.1-0.1,0.2-0.2c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0-0.1c0.2-0.3,0.2-0.7-0.1-1.3c0,0,0,0,0,0   c-0.1-0.2-0.2-0.4-0.4-0.6c-0.1-0.1-0.2-0.2-0.3-0.3c-0.4-0.4-0.7-0.8-0.9-1.2c-0.1-0.1-0.2-0.2-0.2-0.4c0,0,0,0,0-0.1   c0,0,0-0.1-0.1-0.1c0,0,0-0.1-0.1-0.1c-0.1-0.2-0.1-0.3-0.2-0.5c-0.1-0.2-0.1-0.4-0.2-0.6c0,0,0-0.1,0-0.1c0-0.1,0-0.2,0-0.3v-0.1   c0-0.1,0-0.1,0-0.2c0,0,0-0.1,0-0.1c0-0.1,0-0.2,0-0.3c0.1-0.3,0.2-0.6,0.3-0.8c0,0,0,0,0-0.1c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0-0.1c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0l0,0c0.4-0.5,0.9-0.9,1.5-1.1c0.7-0.2,1.4-0.3,2.1-0.1l0.9-1.7l1.9,1l-0.8,1.6c0.7,0.4,1.2,1,1.6,1.6   l0.2,0.4l-1.8,1.6l-0.3-0.5c-0.1-0.1-0.3-0.4-0.5-0.7c-0.1-0.1-0.3-0.3-0.5-0.4c0,0-0.1-0.1-0.1-0.1c-0.1-0.1-0.3-0.2-0.4-0.2   c0,0-0.1,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0   c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0c0,0,0,0,0,0c0,0,0,0-0.1,0h0c0,0,0,0-0.1,0h0   c0,0,0,0-0.1,0h0c0,0,0,0,0,0h0c0,0,0,0,0,0h0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c-0.2,0.1-0.4,0.3-0.5,0.5c-0.3,0.5-0.2,0.9,1,2.3c0.8,0.9,1.2,1.6,1.4,2.4   c0.2,0.8,0.1,1.6-0.3,2.5c-0.8,1.5-2.4,2.2-4.2,1.8l-0.8,1.6l-0.1,0.2l-2-1l0.9-1.7c-0.1-0.1-0.2-0.1-0.3-0.2   c-0.3-0.2-0.5-0.4-0.8-0.6c0,0-0.1-0.1-0.1-0.1c-0.3-0.3-0.5-0.6-0.7-1l-0.2-0.4L279.6,26.7z" />
                     <path class="st33" d="M278,37c-6.3-3.3-8.8-11.1-5.6-17.4c1.3-2.4,3.2-4.3,5.4-5.5c-2.4,1.2-4.4,3.1-5.7,5.6   c-3.3,6.3-0.8,14.1,5.6,17.4c3.9,2,8.4,1.9,12-0.1C286.2,38.9,281.8,39,278,37z" />
                  </g>
                  <g>
                     <linearGradient id="SVGID_00000016062571573767098340000005260099975199807150_" gradientUnits="userSpaceOnUse" x1="79.734" y1="-130.471" x2="91.3973" y2="-130.471" gradientTransform="matrix(0.8624 -0.5063 0.5063 0.8624 260.6894 161.6657)">
                        <stop offset="0" style="stop-color:#FA8460" />
                        <stop offset="4.692155e-02" style="stop-color:#FA9150" />
                        <stop offset="0.1306" style="stop-color:#FBA43A" />
                        <stop offset="0.2295" style="stop-color:#FBB229" />
                        <stop offset="0.3519" style="stop-color:#FCBC1E" />
                        <stop offset="0.5235" style="stop-color:#FCC117" />
                        <stop offset="1" style="stop-color:#FCC315" />
                     </linearGradient>
                     <path style="fill:url(#SVGID_00000016062571573767098340000005260099975199807150_);" d="M268.1,0c-1,0.1-2,0.4-2.8,0.9   c0,0-0.1,0-0.1,0c0,0-0.1,0-0.1,0.1c0,0,0,0,0,0c0,0-0.1,0-0.1,0.1c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0-0.1,0.1   c-0.1,0.1-0.2,0.1-0.3,0.2c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0.1-0.1,0.1   c-0.1,0.1-0.1,0.2-0.2,0.3c0,0,0,0,0,0c-0.1,0.1-0.2,0.2-0.2,0.4c-0.1,0.1-0.2,0.3-0.2,0.4c0,0.1-0.1,0.1-0.1,0.2   c0,0.1-0.1,0.1-0.1,0.2c0,0,0,0.1-0.1,0.1c0,0,0,0,0,0.1c0,0,0,0.1-0.1,0.1c0,0,0,0.1,0,0.1c0,0.1,0,0.1-0.1,0.2c0,0,0,0.1,0,0.1   c-0.1,0.3-0.1,0.5-0.2,0.8c0,0,0,0.1,0,0.1c0,0,0,0.1,0,0.1c0,0.1,0,0.1,0,0.2c0,0,0,0,0,0.1c0,0.1,0,0.1,0,0.2c0,0,0,0,0,0.1   c0,0.1,0,0.1,0,0.2c0,0,0,0,0,0.1c0,0.1,0,0.1,0,0.2c0,0.1,0,0.1,0,0.2c0.2,3.2,2.9,5.7,6.1,5.5c1-0.1,1.9-0.3,2.6-0.8   c0.1,0,0.1-0.1,0.2-0.1c0.1,0,0.1-0.1,0.1-0.1c0,0,0,0,0.1,0c0,0,0.1,0,0.1-0.1c0,0,0,0,0,0c0,0,0.1-0.1,0.1-0.1c0,0,0.1,0,0.1-0.1   c0,0,0.1-0.1,0.1-0.1c0,0,0.1-0.1,0.1-0.1c0,0,0.1-0.1,0.1-0.1c0,0,0.1-0.1,0.1-0.1c0.1-0.1,0.1-0.1,0.2-0.2c0,0,0.1-0.1,0.1-0.1   c0,0,0.1-0.1,0.1-0.1c0,0,0,0,0,0c0,0,0.1-0.1,0.1-0.1c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0-0.1c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0.1-0.1,0.1-0.1c0,0,0,0,0,0c0,0,0.1-0.1,0.1-0.1c0-0.1,0.1-0.1,0.1-0.2c0.1-0.2,0.2-0.4,0.3-0.5c0.1-0.1,0.1-0.2,0.1-0.4   c0-0.1,0.1-0.3,0.1-0.4c0-0.1,0.1-0.3,0.1-0.4c0-0.1,0-0.3,0.1-0.4c0-0.1,0-0.1,0-0.2c0,0,0,0,0,0c0-0.1,0-0.1,0-0.2   c0-0.1,0-0.1,0-0.2c0-0.1,0-0.1,0-0.2c0,0,0-0.1,0-0.1c0-0.1,0-0.1,0-0.2C274.1,2.3,271.3-0.2,268.1,0z M267.1,7.2L267.1,7.2   l0.3,0.2c0.3,0.2,0.7,0.3,1.1,0.3c0.2,0,0.3-0.1,0.4-0.1c0.1-0.1,0.2-0.1,0.2-0.3c0,0,0-0.1,0-0.1c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0-0.2-0.1-0.3-0.3-0.5c0,0,0,0,0,0   c-0.1,0-0.2-0.1-0.3-0.1c-0.1,0-0.1,0-0.2-0.1c-0.2-0.1-0.4-0.2-0.6-0.2c-0.1,0-0.1-0.1-0.2-0.1c0,0,0,0,0,0c0,0,0,0-0.1,0   c0,0,0,0-0.1,0c-0.1,0-0.1-0.1-0.2-0.1c-0.1-0.1-0.1-0.1-0.2-0.2c0,0,0,0,0,0c0,0-0.1-0.1-0.1-0.1l0,0c0,0,0,0,0-0.1c0,0,0,0,0,0   c0,0,0-0.1-0.1-0.1c0-0.1-0.1-0.3-0.1-0.4c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0l0,0   c0-0.3,0.2-0.5,0.3-0.8c0.2-0.2,0.5-0.4,0.8-0.5l0-0.9l1-0.1l0,0.8c0.4,0,0.7,0.1,1,0.3l0.2,0.1l-0.3,1l-0.3-0.1   c-0.1,0-0.2-0.1-0.4-0.1c-0.1,0-0.2,0-0.3-0.1c0,0-0.1,0-0.1,0c-0.1,0-0.1,0-0.2,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0l0,0c0,0,0,0,0,0l0,0c0,0,0,0,0,0l0,0c0,0,0,0,0,0l0,0c0,0,0,0,0,0l0,0   c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c-0.1,0.1-0.1,0.2-0.1,0.3c0,0.2,0.1,0.4,0.9,0.7c0.5,0.2,0.8,0.4,1.1,0.6c0.3,0.3,0.4,0.6,0.4,1c0,0.8-0.4,1.4-1.2,1.7l0,0.8   l0,0.1l-1,0.1l0-0.8c-0.1,0-0.1,0-0.2,0c-0.2,0-0.3,0-0.4-0.1c0,0-0.1,0-0.1,0c-0.2-0.1-0.4-0.1-0.5-0.2l-0.2-0.1L267.1,7.2z" />
                     <path class="st33" d="M268.8,11.5c-3.2,0.2-6-2.3-6.1-5.5c-0.1-1.2,0.3-2.4,0.9-3.4c-0.7,1-1,2.2-0.9,3.5c0.2,3.2,2.9,5.7,6.1,5.5   c2-0.1,3.7-1.2,4.6-2.8C272.4,10.4,270.7,11.4,268.8,11.5z" />
                  </g>
                  <polygon class="st00" points="297.8,43.3 296.4,42.7 294.6,46.7 295.4,41.2 296.8,41.8 298.7,37.9 " />
                  <polygon class="st00" points="264.2,27.2 262.9,26.4 260.7,30.2 262,24.8 263.3,25.6 265.5,21.8 " />
               </svg>
               <div editabletype="text " class="f-16 f-md-18 w400 mt20 lh140 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK & INSTAGRAM are the trademarks of FACEBOOK, Inc.</div>
            </div>
            <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
               <div class="f-md-18 f-16 w400 lh140 white-clr text-xs-center">Copyright © WebPull 2022</div>
               <ul class="footer-ul w400 f-md-18 f-16 white-clr text-center text-md-right">
                  <li><a href="https://support.bizomart.com/hc/en-us " class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                  <li><a href="http://webpull.co/legal/privacy-policy.html " class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                  <li><a href="http://webpull.co/legal/terms-of-service.html " class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                  <li><a href="http://webpull.co/legal/disclaimer.html " class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                  <li><a href="http://webpull.co/legal/gdpr.html " class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                  <li><a href="http://webpull.co/legal/dmca.html " class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                  <li><a href="http://webpull.co/legal/anti-spam.html " class="white-clr t-decoration-none">Anti-Spam</a></li>
               </ul>
            </div>
         </div>
      </div>
   </div>
   <!-- Back to top button -->
   <a id="button">
      <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 300.003 300.003" style="enable-background:new 0 0 300.003 300.003;" xml:space="preserve">
         <g>
            <g>
               <path d="M150,0C67.159,0,0.001,67.159,0.001,150c0,82.838,67.157,150.003,149.997,150.003S300.002,232.838,300.002,150    C300.002,67.159,232.842,0,150,0z M217.685,189.794c-5.47,5.467-14.338,5.47-19.81,0l-48.26-48.27l-48.522,48.516    c-5.467,5.467-14.338,5.47-19.81,0c-2.731-2.739-4.098-6.321-4.098-9.905s1.367-7.166,4.103-9.897l56.292-56.297    c0.539-0.838,1.157-1.637,1.888-2.368c2.796-2.796,6.476-4.142,10.146-4.077c3.662-0.062,7.348,1.281,10.141,4.08    c0.734,0.729,1.349,1.528,1.886,2.365l56.043,56.043C223.152,175.454,223.156,184.322,217.685,189.794z" />
            </g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
      </svg>
   </a>
   <!--Footer Section End -->
   <!-- timer --->
   <?php
   if ($now < $exp_date) {
   ?>
      <script type="text/javascript">
         // Count down milliseconds = server_end - server_now = client_end - client_now
         var server_end = <?php echo $exp_date; ?> * 1000;
         var server_now = <?php echo time(); ?> * 1000;
         var client_now = new Date().getTime();
         var end = server_end - server_now + client_now; // this is the real end time

         var noob = $('.countdown').length;

         var _second = 1000;
         var _minute = _second * 60;
         var _hour = _minute * 60;
         var _day = _hour * 24
         var timer;

         function showRemaining() {
            var now = new Date();
            var distance = end - now;
            if (distance < 0) {
               clearInterval(timer);
               document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
               return;
            }

            var days = Math.floor(distance / _day);
            var hours = Math.floor((distance % _day) / _hour);
            var minutes = Math.floor((distance % _hour) / _minute);
            var seconds = Math.floor((distance % _minute) / _second);
            if (days < 10) {
               days = "0" + days;
            }
            if (hours < 10) {
               hours = "0" + hours;
            }
            if (minutes < 10) {
               minutes = "0" + minutes;
            }
            if (seconds < 10) {
               seconds = "0" + seconds;
            }
            var i;
            var countdown = document.getElementsByClassName('countdown');
            for (i = 0; i < noob; i++) {
               countdown[i].innerHTML = '';

               if (days) {
                  countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">' + days + '</span><br><span class="f-14 f-md-18 ">Days</span> </div>';
               }

               countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w700">' + hours + '</span><br><span class="f-14 f-md-18 w500 ">Hours</span> </div>';

               countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald w700">' + minutes + '</span><br><span class="f-14 f-md-18 w500 ">Mins</span> </div>';

               countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald w700">' + seconds + '</span><br><span class="f-14 f-md-18 w500">Sec</span> </div>';
            }

         }
         timer = setInterval(showRemaining, 1000);
      </script>
   <?php
   } else {
      echo "Times Up";
   }
   ?>
   <!--- timer end-->

   <!-- scroll Top to Bottom -->
   <script>
      var btn = $('#button');

      $(window).scroll(function() {
         if ($(window).scrollTop() > 300) {
            btn.addClass('show');
         } else {
            btn.removeClass('show');
         }
      });

      btn.on('click', function(e) {
         e.preventDefault();
         $('html, body').animate({
            scrollTop: 0
         }, '300');
      });
   </script>
</body>

</html>
<html>
   <head>
      <title>JV Page - QuickFunnel JV Invite</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=9">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
      <meta name="title" content="QuickFunnel | JV">
      <meta name="description" content="Trigger & Action Based High Converting Funnel, Landing Page & Website Builder to Cancel All Monthly Subscriptions">
      <meta name="keywords" content="QuickFunnel">
      <meta property="og:image" content="https://www.quickfunnel.live/jv/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Dr. Amit Pareek">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="QuickFunnel | JV">
      <meta property="og:description" content="Trigger & Action Based High Converting Funnel, Landing Page & Website Builder to Cancel All Monthly Subscriptions">
      <meta property="og:image" content="https://www.quickfunnel.live/jv/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="QuickFunnel | JV">
      <meta property="twitter:description" content="Trigger & Action Based High Converting Funnel, Landing Page & Website Builder to Cancel All Monthly Subscriptions">
      <meta property="twitter:image" content="https://www.quickfunnel.live/jv/thumbnail.png">
      <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
      <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&family=Crimson+Text:ital,wght@0,400;0,600;0,700;1,400;1,600;1,700&family=Great+Vibes&family=League+Spartan:wght@200;400;500;600;700;800;900&family=Merriweather:ital,wght@0,300;0,400;0,700;0,900;1,300;1,400;1,700;1,900&family=Niconne&family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
      <!-- required -->
      <link rel="stylesheet" href="../common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style.css" type="text/css">
      <link rel="stylesheet" href="assets/css/timer.css" type="text/css">
      <script src="../common_assets/js/jquery.min.js"></script>
   </head>
   <body>
      <!-- New Timer  Start-->
      <?php
         $date = 'Sept 26 2023 11:00 AM EST';
         $exp_date = strtotime($date);
         $now = time();  
         
         if ($now < $exp_date) {
         ?>
      <?php
         } else {
          echo "Times Up";
         }
         ?>
      <!-- New Timer End -->
      <!-- Header Section Start -->
      <div class="header-section">
         <div class="container">
            <div class="row">
               <div class="col-12 d-flex align-items-center justify-content-center justify-content-md-between flex-wrap flex-md-nowrap">
                  <div>
                     <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	                  viewBox="0 0 693.1 109.7" style="enable-background:new 0 0 693.1 109.7; height:45px;" xml:space="preserve">
                        <style type="text/css">
                           .st0{fill:#FFFFFF;}
                           .st1{fill:#EF3E39;}
                           .st2{fill:#853594;}
                           .st3{fill:#EF3E3A;}
                        </style>
                        <g>
	                        <g id="Layer_1-2">
		                        <path class="st0" d="M140.4,0H18.9c-2.8-0.1-5.2,2.1-5.4,4.9s2.1,5.2,4.9,5.4c0.2,0,0.3,0,0.5,0h42.3c2.8,0,5.1,2.3,5.1,5.1
                              c0,2.8-2.3,5.1-5.1,5.1h-56c-2.8,0.1-5,2.5-4.9,5.4c0.1,2.7,2.3,4.8,4.9,4.9h35.6l0,0h13.5c2.8,0.1,5,2.5,4.9,5.4
                              c-0.1,2.7-2.3,4.8-4.9,4.9H46c-2.8,0-5.1,2.4-5.1,5.2c0,2.7,2.1,4.9,4.8,5.1h17.8c2.8,0,5.2,2.2,5.2,5.1c0,2.8-2.2,5.2-5.1,5.2h-3
                              c-1.3,0-2.6,0.6-3.5,1.5c-2,2-2,5.2-0.1,7.3c1,1,2.3,1.5,3.7,1.5h8v37.7L103.1,96V68.6l42.8-57.6c2.3-3,1.6-7.3-1.4-9.6
                              C143.3,0.5,141.9,0,140.4,0z M66.2,36c0-2.8,2.3-5.1,5.1-5.1h10c2.8,0.1,5,2.5,4.9,5.4c-0.1,2.7-2.3,4.8-4.9,4.9h-10
                              C68.5,41.1,66.2,38.8,66.2,36z"/>
		                        <path class="st1" d="M34,46.3c0,2.8-2.3,5.1-5.1,5.1h-10c-2.8,0-5.1-2.3-5.1-5.1c0-2.8,2.3-5.1,5.1-5.1c0,0,0,0,0,0h10
			                     C31.7,41.2,34,43.5,34,46.3z"/>
		                        <path class="st1" d="M48.8,66.9c0,2.8-2.3,5.1-5.1,5.1h-2.9c-2.8,0-5.1-2.1-5.3-4.8c-0.1-1.5,0.4-3,1.5-4c0.9-0.9,2.2-1.5,3.5-1.5
			                     h3.4C46.6,61.8,48.9,64.1,48.8,66.9z"/>
                              <path class="st2" d="M45.8,61.7L45.8,61.7z"/>
                              <path class="st2" d="M63.7,61.7L63.7,61.7z"/>
                              <path class="st2" d="M63.7,61.7L63.7,61.7z"/>
                              <path class="st3" d="M230,48.3c0,4.5-0.7,8.9-2.3,13.1c-1.5,3.9-3.7,7.5-6.5,10.5c-2.8,3-6.1,5.4-9.9,7c-8,3.3-17,3.3-24.9,0
                              c-3.7-1.6-7.1-4-9.8-7c-2.8-3.1-5-6.7-6.4-10.5c-1.6-4.2-2.4-8.7-2.3-13.1c0-4.5,0.7-9,2.3-13.2c1.4-3.9,3.6-7.5,6.4-10.5
                              c2.8-3,6.1-5.4,9.8-7c8-3.3,17-3.3,24.9,0c3.7,1.6,7.1,4,9.9,7c2.8,3.1,5,6.6,6.5,10.5C229.3,39.3,230,43.8,230,48.3z M222.1,48.3
                              c0.1-4.6-0.9-9.2-3-13.4c-1.9-3.7-4.7-6.9-8.2-9.1c-7.4-4.4-16.6-4.4-24,0c-3.5,2.3-6.3,5.4-8.2,9.1c-2.1,4.2-3.1,8.7-3,13.4
                              c-0.1,4.6,1,9.2,3,13.3c1.8,3.7,4.7,6.9,8.2,9.2c7.4,4.4,16.6,4.4,24,0c3.5-2.3,6.3-5.4,8.2-9.2C221.1,57.5,222.1,52.9,222.1,48.3
                              z M233.1,84.8c1.9,0.1,3.4,1.6,3.4,3.5c-0.1,1.3-0.7,2.5-1.7,3.4c-1.3,1.2-2.8,2.2-4.4,2.9c-1.8,0.9-3.7,1.5-5.7,2
                              c-1.8,0.5-3.7,0.7-5.6,0.7c-3.1,0.1-6.2-0.3-9.2-1.2c-2.1-0.7-4.2-1.5-6.2-2.6c-1.8-1-3.5-1.9-5.2-2.6c-1.9-0.8-3.9-1.2-6-1.2
                              c-1,0-2,0.2-2.9,0.6c-0.8,0.4-1.6,0.7-2.3,1.1c-0.8,0.4-1.7,0.6-2.6,0.6c-1,0-2-0.4-2.7-1.1c-0.6-0.7-1-1.6-1-2.5
                              c-0.1-1.4,0.8-2.8,2.1-3.2L200,79l8.4,0.8l-15.7,5.5l2.6-1.9c2.2,0,4.4,0.3,6.4,1.1c1.9,0.7,3.7,1.5,5.5,2.5
                              c1.8,0.9,3.6,1.8,5.5,2.5c2.1,0.8,4.4,1.1,6.6,1.1c1.8,0,3.7-0.3,5.4-1c1.2-0.5,2.4-1.2,3.6-1.9c0.8-0.6,1.7-1.3,2.4-2
                              C231.3,85.1,232.1,84.8,233.1,84.8L233.1,84.8z"/>
		                        <path class="st3" d="M277.1,32.9c1,0,2,0.4,2.7,1.1c0.7,0.7,1,1.7,1,2.6v25.1c0,6.2-1.7,11-5.2,14.4s-8.3,5.1-14.5,5.1
                              c-6.2,0-11-1.7-14.4-5.1s-5.2-8.2-5.2-14.4V36.6c0-2,1.5-3.7,3.5-3.7c0,0,0.1,0,0.1,0c1,0,2,0.4,2.7,1.1c0.7,0.7,1,1.7,1,2.6v25.1
                              c0,4.2,1.1,7.3,3.2,9.5s5.2,3.2,9,3.2c4,0,7.1-1.1,9.2-3.2s3.2-5.3,3.2-9.5V36.6c0-2,1.5-3.7,3.5-3.7
                              C277,32.9,277,32.9,277.1,32.9L277.1,32.9z"/>
		                        <path class="st3" d="M297.9,26c-1.2,0.1-2.4-0.2-3.4-0.9c-0.8-0.7-1.2-1.8-1.1-2.9v-1.3c-0.1-1.1,0.4-2.2,1.2-2.9
                              c1-0.7,2.2-1,3.4-0.9c1.2-0.1,2.4,0.2,3.3,0.9c0.8,0.7,1.2,1.8,1.1,2.9v1.3c0.1,1.1-0.3,2.1-1.1,2.9
                              C300.2,25.8,299.1,26.1,297.9,26z M301.5,76.8c0,1-0.4,2-1.1,2.7c-1.4,1.5-3.8,1.5-5.3,0c0,0,0,0,0,0c-0.7-0.7-1-1.7-1-2.7V35.9
                              c-0.1-2,1.5-3.8,3.5-3.9s3.8,1.5,3.9,3.5c0,0.1,0,0.3,0,0.4L301.5,76.8z"/>
		                        <path class="st3" d="M336,31.7c2.9,0,5.7,0.3,8.5,1c2.2,0.5,4.2,1.5,6,2.8c1.3,0.9,2.2,2.4,2.2,4c0,0.8-0.3,1.6-0.8,2.3
                              c-0.5,0.7-1.3,1.1-2.2,1.1c-0.7,0-1.4-0.1-2.1-0.5c-0.5-0.3-1-0.7-1.5-1.2c-0.5-0.5-1.2-1-1.9-1.3c-1-0.5-2.1-0.8-3.2-1
                              c-1.4-0.2-2.8-0.4-4.2-0.4c-3.1-0.1-6.2,0.8-8.9,2.4c-2.5,1.6-4.6,3.8-6,6.5c-1.5,2.8-2.3,6-2.2,9.2c-0.1,3.2,0.7,6.4,2.1,9.2
                              c1.4,2.6,3.4,4.9,5.9,6.4c2.6,1.6,5.7,2.5,8.8,2.4c1.7,0,3.5-0.1,5.2-0.6c1.1-0.3,2.2-0.7,3.2-1.3c0.9-0.5,1.8-1.1,2.5-1.9
                              c1.4-1.3,3.5-1.2,4.8,0.1c0.6,0.7,0.9,1.6,0.9,2.5c0,1.1-0.8,2.3-2.3,3.5c-1.9,1.4-4,2.5-6.2,3.1c-2.9,0.9-5.9,1.3-8.9,1.3
                              c-4.3,0.1-8.6-1-12.3-3.3c-3.5-2.2-6.3-5.3-8.2-9c-2-3.9-3-8.3-2.9-12.7c-0.1-4.4,1-8.7,3-12.6C319.3,36.2,327.3,31.5,336,31.7z"
                              />
		                        <path class="st3" d="M367.1,80.6c-1,0-2-0.4-2.7-1.1c-0.7-0.7-1-1.7-1-2.7V16c0-1,0.3-1.9,1-2.6c0.7-0.7,1.7-1.1,2.7-1.1
                              c2,0,3.7,1.5,3.7,3.5c0,0,0,0.1,0,0.1v60.8c0,1-0.4,2-1.1,2.7C369,80.2,368.1,80.6,367.1,80.6z M397.7,32.6c1,0,1.9,0.4,2.5,1.2
                              c1.3,1.2,1.4,3.2,0.2,4.6c-0.2,0.2-0.4,0.4-0.6,0.5L370,65l-0.3-8.6l25.5-22.7C395.8,33,396.7,32.6,397.7,32.6z M398.7,80.5
                              c-1,0-2-0.4-2.7-1.2l-19.7-20.9l5.5-5.1l19.5,20.8c0.7,0.7,1.1,1.7,1.1,2.8c0,1-0.4,2-1.3,2.6C400.4,80.1,399.6,80.4,398.7,80.5
                              L398.7,80.5z"/>
		                        <path class="st0" d="M419,80.6c-1.6,0.1-3.1-0.5-4.2-1.6c-1.1-1.1-1.6-2.5-1.6-4V21.7c-0.1-3,2.3-5.6,5.3-5.7c0.1,0,0.2,0,0.3,0
                              h30c1.5,0,2.9,0.5,4,1.5c1.1,1,1.7,2.4,1.6,3.9c0,1.4-0.6,2.7-1.6,3.6c-1.1,1-2.5,1.6-4,1.6h-24.9l0.8-1.1v18.6l-0.6-1.2h20.6
                              c1.5,0,2.9,0.5,4,1.5c1.1,1,1.7,2.4,1.6,3.9c0,1.4-0.6,2.7-1.6,3.6c-1.1,1-2.5,1.6-4,1.6h-20.8l0.8-0.8V75c0,1.5-0.6,3-1.7,4
                              C421.9,80,420.5,80.6,419,80.6z"/>
		                        <path class="st0" d="M499.3,31.3c1.5-0.1,3,0.5,4,1.6c1,1.1,1.6,2.5,1.6,4v23.4c0,6.5-1.8,11.7-5.4,15.5
                              c-3.6,3.8-8.9,5.7-15.7,5.7c-6.8,0-12-1.9-15.6-5.7s-5.4-9-5.4-15.5V36.9c0-1.5,0.5-2.9,1.6-4c2.2-2.2,5.7-2.2,7.9,0
                              c1,1.1,1.6,2.5,1.6,4v23.4c0,3.8,0.8,6.5,2.5,8.3s4.1,2.7,7.5,2.7s5.9-0.9,7.6-2.7s2.5-4.6,2.5-8.3V36.9c0-1.5,0.5-2.9,1.6-4
                              C496.4,31.8,497.8,31.2,499.3,31.3z"/>
		                        <path class="st0" d="M543.2,30.3c4.6,0,8,1,10.4,2.9c2.4,1.9,4.1,4.6,4.9,7.6c0.9,3.4,1.4,7,1.3,10.6V75c0,1.5-0.5,2.9-1.6,4
                              c-2.2,2.2-5.7,2.2-7.9,0c-1-1.1-1.6-2.5-1.6-4V51.3c0-1.9-0.2-3.7-0.8-5.5c-0.5-1.6-1.5-2.9-2.8-3.9c-1.7-1.1-3.8-1.6-5.8-1.5
                              c-2.2-0.1-4.3,0.4-6.2,1.5c-1.6,0.9-3,2.3-3.9,3.9c-0.9,1.7-1.4,3.6-1.3,5.5V75c0,1.5-0.5,2.9-1.6,4c-2.2,2.2-5.7,2.2-7.9,0
                              c-1-1.1-1.6-2.5-1.6-4V36.9c0-1.5,0.5-2.9,1.6-4c2.2-2.2,5.7-2.2,7.9,0c1,1.1,1.6,2.5,1.6,4v4l-1.4-0.3c0.7-1.2,1.5-2.3,2.4-3.4
                              c1.1-1.3,2.3-2.4,3.7-3.4c1.5-1.1,3.1-1.9,4.8-2.5C539.2,30.7,541.2,30.3,543.2,30.3z"/>
		                        <path class="st0" d="M598.1,30.3c4.5,0,8,1,10.4,2.9c2.4,2,4.1,4.6,4.9,7.6c0.9,3.4,1.4,7,1.3,10.6V75c0,1.5-0.5,2.9-1.6,4
                              c-2.2,2.2-5.7,2.2-7.9,0c-1-1.1-1.6-2.5-1.6-4V51.3c0-1.9-0.2-3.7-0.8-5.5c-0.5-1.6-1.5-2.9-2.8-3.9c-1.7-1.1-3.8-1.6-5.8-1.5
                              c-2.2-0.1-4.3,0.4-6.3,1.5c-1.6,0.9-3,2.3-3.9,3.9c-0.9,1.7-1.4,3.6-1.3,5.5V75c0,1.5-0.5,2.9-1.6,4c-2.2,2.2-5.7,2.2-7.9,0
                              c-1-1.1-1.6-2.5-1.6-4V36.9c0-1.5,0.5-2.9,1.6-4c2.2-2.2,5.7-2.2,7.9,0c1,1.1,1.6,2.5,1.6,4v4l-1.4-0.3c0.7-1.2,1.5-2.3,2.4-3.4
                              c1.1-1.3,2.3-2.4,3.7-3.4c1.5-1.1,3.1-1.9,4.8-2.5C594.2,30.7,596.1,30.3,598.1,30.3z"/>
		                        <path class="st0" d="M649.9,81.5c-4.7,0.1-9.4-1-13.6-3.3c-3.7-2.1-6.8-5.2-8.8-8.9c-2.1-3.9-3.2-8.3-3.1-12.7
                              c-0.1-4.9,1-9.8,3.4-14.2c2.1-3.7,5.1-6.8,8.8-9c3.5-2,7.5-3.1,11.5-3.1c3.1,0,6.1,0.6,8.9,1.9c2.8,1.3,5.3,3.1,7.4,5.3
                              c2.2,2.3,3.9,4.9,5.1,7.8c1.3,3,1.9,6.2,1.9,9.4c0,1.4-0.7,2.7-1.8,3.6c-1.1,0.9-2.5,1.4-3.9,1.4h-35.2l-2.8-9.2h33.8l-2,1.8v-2.5
                              c-0.1-1.8-0.8-3.4-1.9-4.8c-1.1-1.4-2.6-2.6-4.2-3.4c-1.7-0.8-3.5-1.3-5.4-1.3c-1.8,0-3.5,0.2-5.2,0.7c-1.6,0.5-3,1.3-4.1,2.5
                              c-1.3,1.3-2.3,2.9-2.8,4.7c-0.7,2.4-1.1,4.9-1,7.5c-0.1,2.9,0.6,5.9,2.1,8.4c1.3,2.2,3.1,4,5.3,5.3c2.1,1.2,4.6,1.8,7,1.8
                              c1.8,0,3.7-0.1,5.4-0.6c1.1-0.3,2.3-0.7,3.3-1.3c0.8-0.5,1.6-1,2.3-1.3c1-0.5,2-0.8,3.1-0.8c2.6,0,4.7,2,4.7,4.6c0,0,0,0,0,0.1
                              c-0.1,1.8-1,3.5-2.6,4.5c-2,1.6-4.3,2.8-6.8,3.6C656,81,653,81.5,649.9,81.5z"/>
		                        <path class="st0" d="M693.1,75c0.1,3.1-2.4,5.6-5.4,5.6c-0.1,0-0.1,0-0.2,0c-1.5,0-2.9-0.6-3.9-1.6c-1.1-1.1-1.6-2.5-1.6-4V18
			                     c-0.1-3.1,2.4-5.6,5.4-5.6c0.1,0,0.1,0,0.2,0c1.5-0.1,2.9,0.5,3.9,1.6c1,1.1,1.6,2.5,1.5,4V75z"/>
	                        </g>
                        </g>
                     </svg>
                  </div>
                  <div class="d-flex align-items-center flex-wrap justify-content-center justify-content-md-end text-center text-md-end mt20 mt-md0">
                     <ul class="leader-ul f-16 f-md-18">
                        <li>                        
                           <a href="https://docs.google.com/document/d/1ziebrV1h3qFSZWJQNxtyK3HSml6bCNJm/edit?usp=sharing&ouid=117555941573669202344&rtpof=true&sd=true" target="_blank" class="active">JV Docs </a>
                        </li>
                        <li>
                           <a href="https://docs.google.com/document/d/1tT6W1KDEbxcIPLq9SS8Cm3OvezuZ1jSp/edit" target="_blank">Swipes & Bonuses </a>
                        </li>
                        <!-- <li>
                           <a href="#funnel">SP Preview</a>
                        </li> -->
                     </ul>
                     <a href="https://www.jvzoo.com/affiliate/affiliateinfonew/index/392375" class="affiliate-link-btn ml-md15 mt25 mt-md0" target="_blank"> Grab Your Affiliate Link</a>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md80">
                  <div class="row">
                     <div class="col-12 text-center">
                        <div class="f-16 f-md-20 w400 black-clr lh140 text-capitalize preheadline relative">
                        Say Goodbye to Monthly Bills and Hello to QuickFunnel
                           <img src="assets/images/pre-heading-icon.webp" class="img-fluid d-none d-md-block gameimg">
                        </div>
                     </div>
                     <div class="col-12 mt-md50 mt20 f-md-50 f-24 w600 text-center white-clr lh140"> 
                         The Only One-Stop Trigger & Action Based <span class="red-clr">Funnels and Website Solution</span> for <br class="d-none d-md-block">Low One-Time-Price
                     </div>
                     <div class="col-12 mt-md30 mt20">
                        <img src="assets/images/hr.webp" alt="hr" class="mx-auto img-fluid d-block hr">
                     </div>
                     <div class="col-12 mt20 mt-md30 text-center">
                        <div class="f-18 f-md-20 w400 text-center lh140 white-clr text-capitalize">               
                        Visual Funnel Planner & Builder  |  Drag & Drop Editor  |  400+ DFY Beautiful Templates  | <br class="d-none d-md-block"> Lead Generation  |  In-Built Email Marketing System
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="header-section-2">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-12 col-md-8">
                  <div class="video-box">
                     <div style="padding-bottom: 56.25%;position: relative;"><iframe src="https://quickfunnel.dotcompal.com/video/embed/m6gsa685be" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                     </div>
                  </div>  
               </div>
               <div class="col-md-4 col-12 mt20 mt-md0">
                  <div class="calendar-wrap">
                     <img src="assets/images/launch-date.webp" alt="Date" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="clearfix"></div>
                  <div class="countdown counter-black mt15 mt-md30">
                     <div class="timer-label text-center"><span class="f-30 f-md-40 lh100 timerbg col-12">00</span><br><span class="f-14 w500">Days</span> </div>
                     <div class="timer-label text-center"><span class="f-30 f-md-40 lh100 timerbg">00</span><br><span class="f-14 w500">Hours</span> </div>
                     <div class="timer-label text-center"><span class="f-30 f-md-40 lh100 timerbg">00</span><br><span class="f-14 w500">Mins</span> </div>
                     <div class="timer-label text-center"><span class="f-30 f-md-40 lh100 timerbg">00</span><br><span class="f-14 w500">Sec</span> </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Header Section End -->
      
      <!-- Form Section Start -->
      <div class="live-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="formbg">
                     <div class="col-12 col-md-12">
                        <div class="f-24 f-md-45 w600 text-center lh140 black-clr">
                           Grab Your <span class="red-clr1">JVZoo</span>  Affiliate Link to Jump on <br class="d-none d-md-block"> This Amazing Product Launch
                        </div>
                     </div>
                     <div class="col-md-12 mx-auto mt20 mt-md30">
                        <div class="row justify-content-center align-items-center">
                           <div class="col-12 col-md-3">
                              <img src="assets/images/jvzoo.webp" class="img-fluid d-block mx-auto" alt="Jvzoo"/>
                           </div>
                           <div class="col-12 col-md-4 affiliate-btn  mt-md19 mt15">
                              <a href="https://www.jvzoo.com/affiliate/affiliateinfonew/index/392375" class="f-18 f-md-20 w600 mx-auto" target="_blank">Request Affiliate Link</a>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <!-- List Section Start -->
      <div class="list-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center d-flex mx-auto justify-content-center">
               <span class="promote f-20 f-md-45 w600 lh140 white-clr">7 Reasons</span><div class="f-20 f-md-44 w600 lh140 black-clr winner-heading"> To Promote Quick Funnel</div>
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-12 col-md-5 mt20 mt-md0">
                  <img src="assets/images/thumbsup.webp" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 col-md-7 f-md-20 f-18 w500 lh150 mt30 mt-md0">
                  <ul class="f-18 f-md-20 w400 lh140 black-clr know-list pl0 mt0">
                     <li>Brand new Trigger & Action Based technology explodes results!</li>
                     <li>Replace All Monthly Subscriptions with low One time FEE!</li>
                     <li>The most powerful tool of its kind, guaranteed!</li>
                     <li>Works for Open Market, all list types, niches, and audiences!</li>
                     <li>High converting sales pages with Proven Funnel</li>
                     <li>Irresistible offer!</li>
                     <li>Sells itself… everyone wants these results!</li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!-- List Section End -->

      <!-- Problem Section Start -->
      <div class="problem-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-20 f-md-45 w600 white-clr heading-design">
                       The <span class="red-clr"><u>Problem</u></span> Is
                  </div>
               </div>
            </div>
            <!-- <div class="row mt20 mt-md50">
               <div class="f-18 f-md-20 w400 white-clr text-capitalize lh140 text-center lh180">
               In this fast-moving era, people prefer to spend quality time with their family & loved ones... & there is no limit on the budget to buying happiness. They love to take their day meal or dinner 
on weekends in a good restaurant... Today people search for a good restaurant online & check the review & ratings but the problem is -
               </div>
            </div> -->
            <div class="row align-items-center mt20 mt-md50">
              
              
               <div class="col-md-7 col-12 mt-md0 mt20 f-16 f-md-20 w300 white-clr lh140">
               <span class="f-26 f-md-45 w600 lh140">Not All Funnel Builders Are Created Equal... </span>
               <br><br>
               <span class="w500 red-clr">You’ve tried funnel builders and they were slow loading - </span>meaning that your visitors and traffic leave before they could ever see your offer and thus, you don’t make any money.
               <br><br>
               <span class="w500 red-clr">You tried to hire a developer but got frustrated</span> because they wanted to charge you upwards of anywhere $3,000 bucks PER FUNNEL and the design was still low quality. Overall, this turned out to be a disaster.
               <br><br>
               <span class="w500 red-clr">Or do you pay hundreds of dollars</span> to funnel building companies those are not optimized; whose servers are the absolute worst?  <span class="w500">It’s downright frustrating because you get pauper service for a premium price.</span> Yeah, a lot of you fall victim to that, too.
               <br><br>
               <span class="w500 red-clr" >Or Maybe... Just Maybe, You Let Building A Funnel Intimidate You.</span>
               And now you’re paralyzed because you haven’t made any profits or ROI, but you’re scared to cancel that funnel service. So, you only continue to flush money down the toilet month after month.
</div>
<div class="col-md-5 col-12 ">
                  <img src="assets/images/problem-img.webp" class="img-fluid mx-auto d-block">
               </div>
              
            </div>
            </div>
         </div>
      </div>
     
      <!-- Oppurtunity Section End -->
      <div class="butnotanymore-sec relative">   
            <div class="container">
               <div class="row">
                  <div class="col-12 text-center">
                     <div class="f-24 f-md-45 w600 red-clr lh140 cross relative">
                        But Not Anymore
                     </div>
                     <div class="f-24 f-md-45 w600 black-clr lh140  mt20 mt-md30">
                     After 36 Months of Brainstorming, Coding, <br class="d-none d-md-block">   Debugging & Added with Super Powerful
                     </div>
                     <div class="f-24 f-md-45 w600 red-clr lh140 trigger-block mt20 mt-md30">
                     Trigger & Action Based feature
                     </div>
                     <div class="f-24 f-md-45 w600 black-clr lh140 mt20 mt-md30">
                     we are back with QUICKFUNNEL That Make <br class="d-none d-md-block">  It a Cut above the Rest
                     </div>
                  </div>
               </div>
            </div>
            <img src="assets/images/butnotanymore-img.webp" alt="not any more" class="img-fluid d-none d-md-block ele1">
      </div>
      <!-- Presenting Section Start -->
      <div class="presenting-sec">   
            <div class="container">
               <div class="row">
                  <div class="col-12 text-center">
                     <div class="f-24 f-md-50 w600 red-clr lh140 presenting-head">
                        Presenting…
                     </div>
                     <div class="f-24 f-md-45 w600 white-clr lh140 mt30 mt-md50">
                        Trigger & Action Based High Converting <span class="red-clr"> Funnel, Landing Page & Website Builder </span> to Cancel All Monthly Subscriptions
                     </div>
                     <div class="mt20 mt-md90">
                        <img src="assets/images/pbox.webp" alt="Product Box" class="mx-auto d-block img-fluid">
                     </div>
                  </div>
               </div>
            </div>
      </div>
      <!-- Presenting Section End -->
      <div class="demo-sec">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-24 f-md-45 w600 lh140 text-center demo-text">
                  Just Select & Sell Ultrafast, High Converting Funnels And Websites In Any Niche from Our DFY 400+ DFY Website & Templates... 
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md60">
                  <div class="logos-effect">
                     <img src="assets/images/templates.webp" alt="Demo Image" class="d-block img-fluid mx-auto">
                  </div>
               </div>
            </div>
         </div>
      </div>
     

      <div class="features-headline" id="features">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-24 f-md-40 w600 lh150 black-clr">                    
                  Check Out These <span class="red-clr">GROUND -  BREAKING Features </span>That Makes Quick funnel A Cut above the Rest
                  </div>
                  <img src="assets/images/blue-arrow.webp" alt="Zig Zag Line" class="mx-auto img-fluid d-block mt20 mt-md30">
               </div>
            </div>
         </div>
         <div class="features-section mt30 mt-md50">
            <div class="container">
               <div class="row align-items-center">
                  <div class="col-12 ">
                     <div class="w600 f-24 f-md-36 white-clr lh150 text-center ">
                     Done-For-You Funnel Templates to <br class="d-none d-md-block">Create Beautiful Funnels For Any Marketing Goal
                     </div>
                     <div class="f-18 w400 lh150 mt10 text-center  white-clr">
                     Creating funnels just got faster and easier.  Create a high converting funnel in any niche with 400+ done-for-you & proven converting funnel templates. Simply use our state-of-the-art technology to create almost anything. You’ll get all you need to get up and running online and make money with QuickFunnel.   
                     </div>
                  </div>
                  <div class="col-md-6 col-12 "> 
                     <div class="mt-md80 mt30">
                        <img src="assets/images/fe1.webp" class="img-fluid d-block mx-auto" alt="feat1">
                        <div class="f-18 f-md-24 lh150 w600 text-center mt20 text-capitalize white-clr">Sales Funnels</div>
                     </div>
                     <div class="mt-md80 mt30">
                        <img src="assets/images/fe2.webp" class="img-fluid d-block mx-auto" alt="feat1">
                        <div class="f-18 f-md-24 lh150 w600 text-center mt20 text-capitalize white-clr">Product Launch Funnels</div>
                     </div>
                     <div class="mt-md80 mt30">
                        <img src="assets/images/fe3.webp" class="img-fluid d-block mx-auto" alt="feat1">
                        <div class="f-18 f-md-24 lh150 w600 text-center mt20 text-capitalize white-clr">Lead Nurturing Funnels</div>
                     </div>
                     <div class="mt-md80 mt30">
                        <img src="assets/images/fe4.webp" class="img-fluid d-block mx-auto" alt="feat1">
                        <div class="f-18 f-md-24 lh150 w600 text-center mt20 text-capitalize white-clr">Membership Funnels</div>
                     </div>
                  </div>
                  <div class="col-md-6 col-12">
                     <div class="mt-md90 mt30">
                        <img src="assets/images/fe5.webp" class="img-fluid d-block mx-auto" alt="feat1">
                        <div class="f-18 f-md-24 lh150 w600 text-center mt20 text-capitalize white-clr">Targeted Lead Funnels</div>
                     </div>
                     <div class="mt-md80 mt30">
                        <img src="assets/images/fe6.webp" class="img-fluid d-block mx-auto" alt="feat1">
                        <div class="f-18 f-md-24 lh150 w600 text-center mt20 text-capitalize white-clr">Webinar Funnels</div>
                     </div>
                     <div class="mt-md80 mt30">
                        <img src="assets/images/fe7.webp" class="img-fluid d-block mx-auto" alt="feat1">
                        <div class="f-18 f-md-24 lh150 w600 text-center mt20 text-capitalize white-clr">Client Closing Funnels</div>
                     </div>
                     <div class="mt-md80 mt30">
                        <img src="assets/images/fe8.webp" class="img-fluid d-block mx-auto" alt="feat1">
                        <div class="f-18 f-md-24 lh150 w600 text-center mt20 text-capitalize white-clr">Any Other Marketing Funnel</div>
                     </div>
                  </div>
               </div>
            </div>
         </div>   
      </div>
   
<div class="industry-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-24 f-md-40 w600 lh140 black-clr"> 
                     Industry’s First Free Flow Funnel Builder to Plan & Create Your Profitable Funnels from Scratch Quick & Easy
                  </div>
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-12 col-md-5 mt20 mt-md0">
                  <img src="assets/images/profit-man.webp" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 col-md-7 f-md-24 f-18 w400 lh150">
                  Plan & setup your entire winning funnel with one of a kind <span class="w600">Industry’s First funnels & journey builder. </span>
                  <br><br>
                  It enables you to create trigger & action-based funnels. Add upsells and down sells and create complete funnel sequence in few clicks. 
               </div>
            </div>
         </div>
      </div>


      <div class="trigger-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-24 f-md-40 w400 lh150 white-clr">    
                     <span class="red-clr w600">All New Trigger & Action Based Customer Journey Builder </span>to Automate Marketing, Leads & Sales                
                  </div>
                  <div class="f-16 f-md-20 w400 lh150 white-clr mt20 mt-md30">
                     Quick Funnel enables you to build an entire customer journey that provides personalised experience to your visitors, engage them and finally convert them.
                  </div>

                  <div class="f-18 f-md-24 w600 lh150 white-clr mt30 mt-md50 grey-bg">
                     Start your customer journey with a trigger – Eg. if someone leaves contact details, click on a link or buy something on a landing page. You can set multiple actions on that trigger – Eg. send this visitor to the upsell page, add a tag or lead score or add contacts into a list on complete automation.
                  </div>

                  <div class="f-16 f-md-20 w400 lh150 white-clr mt20 mt-md50">
                     You just set a journey once & it will work for you on complete automation 24*7
                  </div>
               </div>
            </div>
         </div>
         <div class="trigger-features-section mt30 mt-md50">
            <div class="container">
               <div class="row align-items-center">
                  <div class="col-12 ">
                     <div class="w400 f-24 f-md-45 black-clr lh150 text-center ">
                        <span class="red-clr w600">400+ Battle-Tested And Done-For-You Templates </span>To Build Beautiful, Mobile-Friendly & Fast-Loading Landing Pages
                     </div>
                     <div class="col-12 mt20">
                        <img src="assets/images/hr.webp" alt="hr" class="mx-auto img-fluid d-block hr-bottom">
                     </div>
                     <!-- <div class="f-18 w400 lh150 mt10 text-center  black-clr mt20">
                     Creating funnels just got faster and easier.  Create a high converting funnel in any niche with 400+ done-for-you & proven converting funnel templates. Simply use our state-of-the-art technology to create almost anything. You’ll get all you need to get up and running online and make money with QuickFunnel. 
                     </div> -->
                  </div>
                  <div class="col-md-6 col-12 "> 
                     <div class="mt-md80 mt30">
                        <img src="assets/images/ft1.webp" class="img-fluid d-block mx-auto" alt="feat1">
                        <div class="f-20 f-md-24 lh150 w600 text-center mt20 black-clr">Lead Generation Pages- </div>
                        <div class="f-18 f-md-20 lh150 w400 text-center  black-clr">Get Max Potential Buyer</div>
                     </div>
                     <div class="mt-md80 mt30">
                        <img src="assets/images/ft2.webp" class="img-fluid d-block mx-auto" alt="feat1">
                        <div class="f-20 f-md-24 lh150 w600 text-center mt20 black-clr">Secure Download Pages-</div>
                        <div class="f-18 f-md-20 lh150 w400 text-center black-clr">Get MAX Customer Satisfaction</div>
                     </div>
                     <div class="mt-md80 mt30">
                        <img src="assets/images/ft3.webp" class="img-fluid d-block mx-auto" alt="feat1">
                        <div class="f-20 f-md-24 lh150 w600 text-center mt20 black-clr">Webinar Registration Pages-</div>
                        <div class="f-18 f-md-20 lh150 w400 text-center black-clr">Get MAX Registrants for Your Webinars</div>
                     </div>
                     <div class="mt-md80 mt30">
                        <img src="assets/images/ft4.webp" class="img-fluid d-block mx-auto" alt="feat1">
                        <div class="f-20 f-md-24 lh150 w600 text-center mt20 black-clr">Event Pages- </div>
                        <div class="f-18 f-md-20 lh150 w400 text-center black-clr">Get MAX Bookings To Your Events</div>
                     </div>
                     <div class="mt-md80 mt30">
                        <img src="assets/images/ft5.webp" class="img-fluid d-block mx-auto" alt="feat1">
                        <div class="f-20 f-md-24 lh150 w600 text-center mt20 black-clr">Lead Nurturing Pages-</div>
                        <div class="f-18 f-md-20 lh150 w400 text-center black-clr">Convert Max Leads into customers by <br class="d-none d-md-block">educating  them & a proper CTA</div>
                     </div>
                  </div>
                  <div class="col-md-6 col-12">
                     <div class="mt-md60 mt30">
                        <img src="assets/images/ft6.webp" class="img-fluid d-block mx-auto" alt="feat1">
                        <div class="f-20 f-md-24 lh150 w600 text-center mt20 black-clr">Sales Pages-  </div>
                        <div class="f-18 f-md-20 lh150 w400 text-center black-clr">Get MAX Sales & Profits</div>
                     </div>
                     <div class="mt-md80 mt30">
                        <img src="assets/images/ft7.webp" class="img-fluid d-block mx-auto" alt="feat1">
                        <div class="f-20 f-md-24 lh150 w600 text-center mt20 black-clr">Coming Soon Pages-</div>
                        <div class="f-18 f-md-20 lh150 w400 text-center black-clr">Get MAX Curiosity For Your Offers</div>
                     </div>
                     <div class="mt-md80 mt30">
                        <img src="assets/images/ft8.webp" class="img-fluid d-block mx-auto" alt="feat1">
                        <div class="f-20 f-md-24 lh150 w600 text-center mt20 black-clr">Product Review & Bonus Pages-</div>
                        <div class="f-18 f-md-20 lh150 w400 text-center black-clr">Earn MAX Commissions</div>
                     </div>
                     <div class="mt-md80 mt30">
                        <img src="assets/images/ft9.webp" class="img-fluid d-block mx-auto" alt="feat1">
                        <div class="f-20 f-md-24 lh150 w600 text-center mt20 black-clr">Website Home Page-</div>
                        <div class="f-18 f-md-20 lh150 w400 text-center black-clr">Get MAX Exposure For Your Brand</div>
                     </div>
                     <div class="mt-md80 mt30">
                        <img src="assets/images/ft10.webp" class="img-fluid d-block mx-auto" alt="feat1">
                        <div class="f-20 f-md-24 lh150 w600 text-center mt20 black-clr">Create Any Type of Page- </div>
                        <div class="f-18 f-md-20 lh150 w400 text-center black-clr">Deal Page, thank you, portfolio, contact<br class="d-none d-md-block"> us, legal pages & much more</div>
                     </div>
                  </div>
               </div>
            </div>
         </div>   
      </div>


      <div class="customizable-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-24 f-md-45 w400 lh140 black-clr text-center emoji relative">
                      Fully Customizable <br class="d-none d-md-block"> Drag and Drop WYSIWYG Page Editor
                  </div>
                  <div class="f-24 f-md-45 w600 lh140 red-clr text-center">
                     That Requires Zero Designing or Tech Skills!
                  </div>
                  <img src="assets/images/curve-line.webp" alt="H Line" class="d-block mx-auto img-fluid mt20">
               </div>
            </div>
            <div class="row align-items-center mt30 mt-md50">
               <div class="col-md-6 col-12">
                  <div class="f-18 f-md-24 w400 lh140 black-clr">
                  <span class="w600">A Next generation pixel perfect drag & drop editor to</span> create whatever you want on any page wherever you want without even a single Pixel error. We have reinvented the page editor which is not like the old school bootstrap editor that set your elements without your control on your part.
                  </div>
               </div>
               <div class="col-md-6 col-12">
               <video width="100%" height="auto" loop="" autoplay="" muted="muted" class="mp4-border" controls="">
                   <source src="assets/images/next-generation-img.mp4" type="video/mp4">
               </video>
               </div>
               <div class="f-18 f-md-24 w400 lh140 black-clr mt20 mt-md50">
               We have designed the builder to be completely newbie friendly so you can accomplish tasks at pro level. Change font, colour, link, elements & much more effortlessly.
               </div>
            </div>
            <div class="row align-items-center mt30 mt-md100">
               <div class="col-md-8 col-12 mx-auto">
                  <div class="f-24 f-md-45 w600 lh140 red-clr text-center">
                     Check Out Some Cool Features...
                  </div>
                  <img src="assets/images/curve-line.webp" alt="H Line" class="d-block mx-auto img-fluid mt20">
               </div>
            </div>
            <div class="row row-cols-1 row-cols-md-3 gap30  justify-content-center mt0 mt-md70">
               <div class="col">
                  <div class="feature-list-box">
                     <img src="assets/images/fc1.webp" class="img-fluid mx-auto d-block">
                     <div class="f-18 f-md-24 w600 lh140 mt15">
                        Image/Logo
                     </div>
                     <div class="f-18 f-md-20 w400 lh140 mt15">
                        Upload Any Image and Edit it, Even you Can use this Image in Future for any other Campaign
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box">
                     <img src="assets/images/fc2.webp" class="img-fluid mx-auto d-block">
                     <div class="f-18 f-md-24 w600 lh140 mt15">
                        Video
                     </div>
                     <div class="f-18 f-md-20 w400 lh140 mt15">
                        Show Any Video on Page with Professional Inbuilt HLS Video Player
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box">
                     <img src="assets/images/fc3.webp" class="img-fluid mx-auto d-block">
                     <div class="f-18 f-md-24 w600 lh140 mt15">
                        Button
                     </div>
                     <div class="f-18 f-md-20 w400 lh140 mt15">
                        Show effective CTA that entices your visitors to click, sign up, read and buy.
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box">
                     <img src="assets/images/fc4.webp" class="img-fluid mx-auto d-block">
                     <div class="f-18 f-md-24 w600 lh140 mt15">
                        Section
                     </div>
                     <div class="f-18 f-md-20 w400 lh140 mt15">
                        Edit and add a new section in 1 click. You can customize it separately for Mobile and Desktop Visitors
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box">
                     <img src="assets/images/fc5.webp" class="img-fluid mx-auto d-block">
                     <div class="f-18 f-md-24 w600 lh140 mt15">
                     Headline Text
                     </div>
                     <div class="f-18 f-md-20 w400 lh140 mt15">
                     Create Engaging Headline with Advanced Typography Editing Technology
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box">
                     <img src="assets/images/fc6.webp" class="img-fluid mx-auto d-block">
                     <div class="f-18 f-md-24 w600 lh140 mt15">
                     Countdown Timer
                     </div>
                     <div class="f-18 f-md-20 w400 lh140 mt15">
                     Ticking Timers are legitimately one of the most effective ways to create scarcity and generate more sales for you.
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box">
                     <img src="assets/images/fc7.webp" class="img-fluid mx-auto d-block">
                     <div class="f-18 f-md-24 w600 lh140 mt15">
                     Form
                     </div>
                     <div class="f-18 f-md-20 w400 lh140 mt15">
                     Use our DFY optin-forms and send leads directly to Your favourite autoresponder
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box">
                     <img src="assets/images/fc8.webp" class="img-fluid mx-auto d-block">
                     <div class="f-18 f-md-24 w600 lh140 mt15">
                     Shapes
                     </div>
                     <div class="f-18 f-md-20 w400 lh140 mt15">
                     Draw any Box, Circle or lines to differentiate any part which helps you to get more focused by your visitors
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-list-box">
                     <img src="assets/images/fc9.webp" class="img-fluid mx-auto d-block">
                     <div class="f-18 f-md-24 w600 lh140 mt15">
                     Social
                     </div>
                     <div class="f-18 f-md-20 w400 lh140 mt15">
                     Add social media icons on your site and connect visitors with brand or ask them to share your pages for viral traffic.
                     </div>
                  </div>
               </div>
              
            </div>
            <div class="f-24 f-md-45 w600 lh140 black-clr mt30 mt-md100 text-center">
                  Create 100% Mobile Friendly Pages That Are Fast Loading & Simply Elegant So You Don’t Miss A Single Mobile Visitor.
               </div>
         </div>   
      </div>


      <!-- Exciting Launch Event Section Start -->
      <div class="exciting-section">
      <div class="container">
         <div class="row">
               <div class="col-12 text-center">
                  <div class="f-md-50 f-28 white-clr w700 lh140">
                  This Exciting Launch Event Is Divided Into 2 Phases
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md50 align-items-center">
               <div class="col-12 col-md-6">
                  <div class="f-24 f-md-34 lh140 white-clr w700">
                     To Make You Max Commissions
                  </div>
                  <ul class="launch-list f-md-20 f-18 w400 mb0 mt-md30 mt20 pl20">
                     <li>All Leads Are Hardcoded</li>
                     <li>We'll Re-Market Your Leads Heavily</li>
                     <li>Pitch Bundle Offer On Webinars</li>
                  </ul>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                  <img src="assets/images/phase1.webp" class="img-fluid d-block mx-auto " alt="Phase1">
               </div>
            </div>
            <div class="row mt20 mt-md50 align-items-center">
               <div class="col-12 col-md-6 order-md-2">
                  <div class="f-24 f-md-34 lh140 white-clr w700">
                     Big Opening Contest &amp; Bundle Offer
                  </div>
                  <ul class="launch-list f-md-20 f-18 w400 mb0 mt-md30 mt20 pl20">
                     <li>High in Demand Product with Top Conversion</li>
                     <li>Deep Funnel To Make You Double Digit EPCs</li>
                     <li>Earn Up To $415/Sale</li>
                     <li>Huge $2,000 JV Prizes</li>
                  </ul>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
                  <img src="assets/images/phase2.webp" class="img-fluid d-block mx-auto " alt="Phase2">
               </div>
            </div>
         </div>
      </div>
      <!-- Exciting Launch Event Section End -->

      <!-- Deep-Funnel Section Start -->
      <div class="deep-sec" id="funnel">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-24 f-md-45 w600 lh140 text-center">
                     Our Deep & High Converting Sales Funnel
                  </div>
               </div>
               <div class="col-12 col-md-12 mt20 mt-md70">
                  <img src="assets/images/funnel.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>
         </div>
      </div>
      <!-- Deep-Funnel Section End -->

      <!-- Prize Section Start -->
      <div class="prize-section">
         <div class="container">
            <div class="prize-inner-sec">
               <div class="row">
                  <div class="col-12 text-center">
                     <div class="f-24 f-md-65 w600 lh140 text-center black-clr text-uppercase launch-head">$2,000 Launch Contest</div>
                  </div>
               </div>
               <div class="row mt20 mt-md90">
                  <div class="col-md-6 mx-auto">
                     <img src="assets/images/prelaunch.webp" alt="PreLaunch" class="d-block img-fluid mx-auto">
                  </div>
                  <!-- <div class="col-md-6 mt20 mt-md0">
                     <img src="assets/images/exiciting-launch.webp" alt="Exiciting Launch" class="d-block img-fluid mx-auto">
                  </div> -->
               </div>
            </div>   
         </div>
         <img src="assets/images/businessman.webp" alt="Businessman" class="img-fluid d-none d-md-block ele4">
      </div>
      <!-- Prize Section End -->

      <!-- Reciprocate Section Start -->
      <div class="reciprocate-sec">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-24 f-md-45 w600 lh140 text-center">
                     Our Solid Track Record of <br class="d-none d-md-block">
                     <span class="red-clr"> Launching Top Converting Products</span>
                  </div>
                  <img src="assets/images/product-logo.webp" class="img-fluid d-block mx-auto mt20">
                  <div class="text-center f-md-45 f-24 lh140 w600 white-clr mt20 mt-md70 red-clr">
                     Do We Reciprocate?
                  </div>
                  <div class="f-18 f-md-18 lh140 mt20 w500 text-center black-clr">
                     We've been in top positions on various launch leaderboards &amp; sent huge sales for our valued JVs.<br><br>  
                     So, if you have a top-notch product with top conversions that fits our list, we would love to drive loads of sales for you. Here are
                      just some results from our recent promotions. 
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
                  <div class="logos-effect">
                     <img src="assets/images/logos.webp" class="img-fluid d-block mx-auto">
                  </div>
               </div>
               <div class="col-12 f-md-28 f-24 lh140 mt20 mt-md50 w600 text-center">
                  And The List Goes On And On...
               </div>
            </div>
         </div>
      </div>
      <!-- Reciprocate Section End -->

      <!-- Contact Section Start -->
      <div class="contact-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
               <div class="contact-block">
                     <div class="row gx-md-5">
                        <div class="col-12 mb20 mb-md50">
                           <div class="f-md-45 f-28 w700 lh140 text-center white-clr">
                             Have any Query? Contact us Anytime
                           </div>
                        </div>
                        <div class="col-md-4 col-12 text-center">
                           <div class="contact-shape">
                              <img src="assets/images/amit-pareek-sir.webp" class="img-fluid d-block mx-auto minus">
                              <div class="f-24 f-md-30 w600  lh140 text-center white-clr mt20">
                                 Dr Amit Pareek
                              </div>
                              <div class="f-14 w400 lh140 text-center white-clr">
                                 (Techpreneur &amp; Marketer)
                              </div>
                              <div class="col-12 mt30 d-flex justify-content-center">
                                 <a href="http://facebook.com/Dr.AmitPareek" class="link-text mr20">
                                 <img src="https://cdn.oppyo.com/launches/webpull/jv/am-fb.webp" class="center-block">
                                 </a>
                                 <a href="skype:amit.pareek77" class="link-text">
                                    <div class="col-12 ">
                                       <img src="https://cdn.oppyo.com/launches/webpull/jv/am-skype.webp" class="center-block">
                                    </div>
                                 </a>
                              </div>
                           </div>
                        </div>
                        <div class="col-md-4 col-12 text-center mt20 mt-md0">
                           <div class="contact-shape">
                              <img src="assets/images/tim-verdouw.webp" class="img-fluid d-block mx-auto minus">
                              <div class="f-24 f-md-30 w600  lh140 text-center white-clr mt20">
                                 Tim Verdouw
                              </div>
                              <div class="f-14 w400 lh140 text-center white-clr">
                                 (Marketer)
                              </div>
                              <div class="col-12 mt30 d-flex justify-content-center">
                                 <a href=" https://www.facebook.com/tim.verdouw" class="link-text mr20">
                                 <img src="https://cdn.oppyo.com/launches/webpull/jv/am-fb.webp" class="center-block">
                                 </a>
                                 <a href="#" class="link-text">
                                    <div class="col-12 ">
                                       <img src="https://cdn.oppyo.com/launches/webpull/jv/am-skype.webp" class="center-block">
                                    </div>
                                 </a>
                              </div>
                           </div>
                        </div>
                        <div class="col-md-4 col-12 text-center mt20 mt-md0">
                           <div class="contact-shape">
                              <img src="assets/images/atul-parrek-sir.webp" class="img-fluid d-block mx-auto minus">
                              <div class="f-24 f-md-30 w600  lh140 text-center white-clr mt20">
                                 Atul Pareek
                              </div>
                              <div class="f-14 w400 lh140 text-center white-clr">
                                 (Entrepreneur)
                              </div>
                              <div class="col-12 mt30 d-flex justify-content-center">
                                 <a href="https://www.facebook.com/profile.php?id=100076904623319" class="link-text mr20">
                                 <img src="https://cdn.oppyo.com/launches/webpull/jv/am-fb.webp" class="center-block">
                                 </a>
                                 <a href="skype:live:.cid.c3d2302c4a2815e0" class="link-text">
                                    <div class="col-12 ">
                                       <img src="https://cdn.oppyo.com/launches/webpull/jv/am-skype.webp" class="center-block">
                                    </div>
                                 </a>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Contact Section End -->

      <div class="terms-section">
         <div class="container px-md-15">
            <div class="row">
               <div class="col-12 f-md-45 f-24 w600 lh140 text-center black-clr">
                  Affiliate Promotions Terms &amp; Conditions
               </div>
               <div class="col-md-12 col-12 f-18 f-md-18 lh140 p-md0 mt-md20 mt20 w400 text-center">
                  We request you to read this section carefully before requesting for your affiliate link and being a part of this launch. Once you are
                   approved to be a part of this launch and promote this product, you must abide by the instructions listed below:
               </div>
               <div class="col-md-12 col-12 px-md-0 px30 mt15 mt-md50">
                  <ul class="b-tick1 pl0 m0 f-md-16 f-16 lh140 w400">
                     <li><span class="w700 red-clr">Please be sure to NOT send spam of any kind.</span> This includes cheap traffic methods in any way whatsoever. In case anyone does so,
                         they will be banned from all future promotion with us without any reason whatsoever. No exceptions will be entertained. </li>
                     <li>Please ensure you or your members DON'T use negative words such as 'scam' in any of your promotional campaigns in any way. 
                        If this comes to our knowledge that you have violated it, your affiliate account will be removed from our system with immediate
                         effect. </li>
                     <li>Please make it a point to not OFFER any cash incentives (such as rebates), cash backs etc or any such malpractices to people 
                        buying through your affiliate link. </li>
                     <li>Please note that you are not AUTHORIZED to create social media pages or run any such campaigns that may be detrimental using our
                         product or brand name in any way possible. Doing this may result in serious consequences. </li>
                     <li>We reserve the right to TERMINATE any affiliate with immediate effect if they're found to breach any/all of the conditions
                         mentioned above in any manner. </li>
                  </ul>
               </div>
            </div>
         </div>
      </div>

      <!--Footer Section Start -->
      <div class="footer-section">
         <div class="container ">
            <div class="row">
               <div class="col-12 text-center">
               <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 693.1 109.7" style="enable-background:new 0 0 693.1 109.7; height:45px;" xml:space="preserve">
                  <style type="text/css">
                     .st0{fill:#FFFFFF;}
                     .st1{fill:#EF3E39;}
                     .st2{fill:#853594;}
                     .st3{fill:#EF3E3A;}
                  </style>
                  <g>
                     <g id="Layer_1-2">
                        <path class="st0" d="M140.4,0H18.9c-2.8-0.1-5.2,2.1-5.4,4.9s2.1,5.2,4.9,5.4c0.2,0,0.3,0,0.5,0h42.3c2.8,0,5.1,2.3,5.1,5.1
                           c0,2.8-2.3,5.1-5.1,5.1h-56c-2.8,0.1-5,2.5-4.9,5.4c0.1,2.7,2.3,4.8,4.9,4.9h35.6l0,0h13.5c2.8,0.1,5,2.5,4.9,5.4
                           c-0.1,2.7-2.3,4.8-4.9,4.9H46c-2.8,0-5.1,2.4-5.1,5.2c0,2.7,2.1,4.9,4.8,5.1h17.8c2.8,0,5.2,2.2,5.2,5.1c0,2.8-2.2,5.2-5.1,5.2h-3
                           c-1.3,0-2.6,0.6-3.5,1.5c-2,2-2,5.2-0.1,7.3c1,1,2.3,1.5,3.7,1.5h8v37.7L103.1,96V68.6l42.8-57.6c2.3-3,1.6-7.3-1.4-9.6
                           C143.3,0.5,141.9,0,140.4,0z M66.2,36c0-2.8,2.3-5.1,5.1-5.1h10c2.8,0.1,5,2.5,4.9,5.4c-0.1,2.7-2.3,4.8-4.9,4.9h-10
                           C68.5,41.1,66.2,38.8,66.2,36z"/>
                        <path class="st1" d="M34,46.3c0,2.8-2.3,5.1-5.1,5.1h-10c-2.8,0-5.1-2.3-5.1-5.1c0-2.8,2.3-5.1,5.1-5.1c0,0,0,0,0,0h10
                           C31.7,41.2,34,43.5,34,46.3z"/>
                        <path class="st1" d="M48.8,66.9c0,2.8-2.3,5.1-5.1,5.1h-2.9c-2.8,0-5.1-2.1-5.3-4.8c-0.1-1.5,0.4-3,1.5-4c0.9-0.9,2.2-1.5,3.5-1.5
                           h3.4C46.6,61.8,48.9,64.1,48.8,66.9z"/>
                        <path class="st2" d="M45.8,61.7L45.8,61.7z"/>
                        <path class="st2" d="M63.7,61.7L63.7,61.7z"/>
                        <path class="st2" d="M63.7,61.7L63.7,61.7z"/>
                        <path class="st3" d="M230,48.3c0,4.5-0.7,8.9-2.3,13.1c-1.5,3.9-3.7,7.5-6.5,10.5c-2.8,3-6.1,5.4-9.9,7c-8,3.3-17,3.3-24.9,0
                           c-3.7-1.6-7.1-4-9.8-7c-2.8-3.1-5-6.7-6.4-10.5c-1.6-4.2-2.4-8.7-2.3-13.1c0-4.5,0.7-9,2.3-13.2c1.4-3.9,3.6-7.5,6.4-10.5
                           c2.8-3,6.1-5.4,9.8-7c8-3.3,17-3.3,24.9,0c3.7,1.6,7.1,4,9.9,7c2.8,3.1,5,6.6,6.5,10.5C229.3,39.3,230,43.8,230,48.3z M222.1,48.3
                           c0.1-4.6-0.9-9.2-3-13.4c-1.9-3.7-4.7-6.9-8.2-9.1c-7.4-4.4-16.6-4.4-24,0c-3.5,2.3-6.3,5.4-8.2,9.1c-2.1,4.2-3.1,8.7-3,13.4
                           c-0.1,4.6,1,9.2,3,13.3c1.8,3.7,4.7,6.9,8.2,9.2c7.4,4.4,16.6,4.4,24,0c3.5-2.3,6.3-5.4,8.2-9.2C221.1,57.5,222.1,52.9,222.1,48.3
                           z M233.1,84.8c1.9,0.1,3.4,1.6,3.4,3.5c-0.1,1.3-0.7,2.5-1.7,3.4c-1.3,1.2-2.8,2.2-4.4,2.9c-1.8,0.9-3.7,1.5-5.7,2
                           c-1.8,0.5-3.7,0.7-5.6,0.7c-3.1,0.1-6.2-0.3-9.2-1.2c-2.1-0.7-4.2-1.5-6.2-2.6c-1.8-1-3.5-1.9-5.2-2.6c-1.9-0.8-3.9-1.2-6-1.2
                           c-1,0-2,0.2-2.9,0.6c-0.8,0.4-1.6,0.7-2.3,1.1c-0.8,0.4-1.7,0.6-2.6,0.6c-1,0-2-0.4-2.7-1.1c-0.6-0.7-1-1.6-1-2.5
                           c-0.1-1.4,0.8-2.8,2.1-3.2L200,79l8.4,0.8l-15.7,5.5l2.6-1.9c2.2,0,4.4,0.3,6.4,1.1c1.9,0.7,3.7,1.5,5.5,2.5
                           c1.8,0.9,3.6,1.8,5.5,2.5c2.1,0.8,4.4,1.1,6.6,1.1c1.8,0,3.7-0.3,5.4-1c1.2-0.5,2.4-1.2,3.6-1.9c0.8-0.6,1.7-1.3,2.4-2
                           C231.3,85.1,232.1,84.8,233.1,84.8L233.1,84.8z"/>
                        <path class="st3" d="M277.1,32.9c1,0,2,0.4,2.7,1.1c0.7,0.7,1,1.7,1,2.6v25.1c0,6.2-1.7,11-5.2,14.4s-8.3,5.1-14.5,5.1
                           c-6.2,0-11-1.7-14.4-5.1s-5.2-8.2-5.2-14.4V36.6c0-2,1.5-3.7,3.5-3.7c0,0,0.1,0,0.1,0c1,0,2,0.4,2.7,1.1c0.7,0.7,1,1.7,1,2.6v25.1
                           c0,4.2,1.1,7.3,3.2,9.5s5.2,3.2,9,3.2c4,0,7.1-1.1,9.2-3.2s3.2-5.3,3.2-9.5V36.6c0-2,1.5-3.7,3.5-3.7
                           C277,32.9,277,32.9,277.1,32.9L277.1,32.9z"/>
                        <path class="st3" d="M297.9,26c-1.2,0.1-2.4-0.2-3.4-0.9c-0.8-0.7-1.2-1.8-1.1-2.9v-1.3c-0.1-1.1,0.4-2.2,1.2-2.9
                           c1-0.7,2.2-1,3.4-0.9c1.2-0.1,2.4,0.2,3.3,0.9c0.8,0.7,1.2,1.8,1.1,2.9v1.3c0.1,1.1-0.3,2.1-1.1,2.9
                           C300.2,25.8,299.1,26.1,297.9,26z M301.5,76.8c0,1-0.4,2-1.1,2.7c-1.4,1.5-3.8,1.5-5.3,0c0,0,0,0,0,0c-0.7-0.7-1-1.7-1-2.7V35.9
                           c-0.1-2,1.5-3.8,3.5-3.9s3.8,1.5,3.9,3.5c0,0.1,0,0.3,0,0.4L301.5,76.8z"/>
                        <path class="st3" d="M336,31.7c2.9,0,5.7,0.3,8.5,1c2.2,0.5,4.2,1.5,6,2.8c1.3,0.9,2.2,2.4,2.2,4c0,0.8-0.3,1.6-0.8,2.3
                           c-0.5,0.7-1.3,1.1-2.2,1.1c-0.7,0-1.4-0.1-2.1-0.5c-0.5-0.3-1-0.7-1.5-1.2c-0.5-0.5-1.2-1-1.9-1.3c-1-0.5-2.1-0.8-3.2-1
                           c-1.4-0.2-2.8-0.4-4.2-0.4c-3.1-0.1-6.2,0.8-8.9,2.4c-2.5,1.6-4.6,3.8-6,6.5c-1.5,2.8-2.3,6-2.2,9.2c-0.1,3.2,0.7,6.4,2.1,9.2
                           c1.4,2.6,3.4,4.9,5.9,6.4c2.6,1.6,5.7,2.5,8.8,2.4c1.7,0,3.5-0.1,5.2-0.6c1.1-0.3,2.2-0.7,3.2-1.3c0.9-0.5,1.8-1.1,2.5-1.9
                           c1.4-1.3,3.5-1.2,4.8,0.1c0.6,0.7,0.9,1.6,0.9,2.5c0,1.1-0.8,2.3-2.3,3.5c-1.9,1.4-4,2.5-6.2,3.1c-2.9,0.9-5.9,1.3-8.9,1.3
                           c-4.3,0.1-8.6-1-12.3-3.3c-3.5-2.2-6.3-5.3-8.2-9c-2-3.9-3-8.3-2.9-12.7c-0.1-4.4,1-8.7,3-12.6C319.3,36.2,327.3,31.5,336,31.7z"
                           />
                        <path class="st3" d="M367.1,80.6c-1,0-2-0.4-2.7-1.1c-0.7-0.7-1-1.7-1-2.7V16c0-1,0.3-1.9,1-2.6c0.7-0.7,1.7-1.1,2.7-1.1
                           c2,0,3.7,1.5,3.7,3.5c0,0,0,0.1,0,0.1v60.8c0,1-0.4,2-1.1,2.7C369,80.2,368.1,80.6,367.1,80.6z M397.7,32.6c1,0,1.9,0.4,2.5,1.2
                           c1.3,1.2,1.4,3.2,0.2,4.6c-0.2,0.2-0.4,0.4-0.6,0.5L370,65l-0.3-8.6l25.5-22.7C395.8,33,396.7,32.6,397.7,32.6z M398.7,80.5
                           c-1,0-2-0.4-2.7-1.2l-19.7-20.9l5.5-5.1l19.5,20.8c0.7,0.7,1.1,1.7,1.1,2.8c0,1-0.4,2-1.3,2.6C400.4,80.1,399.6,80.4,398.7,80.5
                           L398.7,80.5z"/>
                        <path class="st0" d="M419,80.6c-1.6,0.1-3.1-0.5-4.2-1.6c-1.1-1.1-1.6-2.5-1.6-4V21.7c-0.1-3,2.3-5.6,5.3-5.7c0.1,0,0.2,0,0.3,0
                           h30c1.5,0,2.9,0.5,4,1.5c1.1,1,1.7,2.4,1.6,3.9c0,1.4-0.6,2.7-1.6,3.6c-1.1,1-2.5,1.6-4,1.6h-24.9l0.8-1.1v18.6l-0.6-1.2h20.6
                           c1.5,0,2.9,0.5,4,1.5c1.1,1,1.7,2.4,1.6,3.9c0,1.4-0.6,2.7-1.6,3.6c-1.1,1-2.5,1.6-4,1.6h-20.8l0.8-0.8V75c0,1.5-0.6,3-1.7,4
                           C421.9,80,420.5,80.6,419,80.6z"/>
                        <path class="st0" d="M499.3,31.3c1.5-0.1,3,0.5,4,1.6c1,1.1,1.6,2.5,1.6,4v23.4c0,6.5-1.8,11.7-5.4,15.5
                           c-3.6,3.8-8.9,5.7-15.7,5.7c-6.8,0-12-1.9-15.6-5.7s-5.4-9-5.4-15.5V36.9c0-1.5,0.5-2.9,1.6-4c2.2-2.2,5.7-2.2,7.9,0
                           c1,1.1,1.6,2.5,1.6,4v23.4c0,3.8,0.8,6.5,2.5,8.3s4.1,2.7,7.5,2.7s5.9-0.9,7.6-2.7s2.5-4.6,2.5-8.3V36.9c0-1.5,0.5-2.9,1.6-4
                           C496.4,31.8,497.8,31.2,499.3,31.3z"/>
                        <path class="st0" d="M543.2,30.3c4.6,0,8,1,10.4,2.9c2.4,1.9,4.1,4.6,4.9,7.6c0.9,3.4,1.4,7,1.3,10.6V75c0,1.5-0.5,2.9-1.6,4
                           c-2.2,2.2-5.7,2.2-7.9,0c-1-1.1-1.6-2.5-1.6-4V51.3c0-1.9-0.2-3.7-0.8-5.5c-0.5-1.6-1.5-2.9-2.8-3.9c-1.7-1.1-3.8-1.6-5.8-1.5
                           c-2.2-0.1-4.3,0.4-6.2,1.5c-1.6,0.9-3,2.3-3.9,3.9c-0.9,1.7-1.4,3.6-1.3,5.5V75c0,1.5-0.5,2.9-1.6,4c-2.2,2.2-5.7,2.2-7.9,0
                           c-1-1.1-1.6-2.5-1.6-4V36.9c0-1.5,0.5-2.9,1.6-4c2.2-2.2,5.7-2.2,7.9,0c1,1.1,1.6,2.5,1.6,4v4l-1.4-0.3c0.7-1.2,1.5-2.3,2.4-3.4
                           c1.1-1.3,2.3-2.4,3.7-3.4c1.5-1.1,3.1-1.9,4.8-2.5C539.2,30.7,541.2,30.3,543.2,30.3z"/>
                        <path class="st0" d="M598.1,30.3c4.5,0,8,1,10.4,2.9c2.4,2,4.1,4.6,4.9,7.6c0.9,3.4,1.4,7,1.3,10.6V75c0,1.5-0.5,2.9-1.6,4
                           c-2.2,2.2-5.7,2.2-7.9,0c-1-1.1-1.6-2.5-1.6-4V51.3c0-1.9-0.2-3.7-0.8-5.5c-0.5-1.6-1.5-2.9-2.8-3.9c-1.7-1.1-3.8-1.6-5.8-1.5
                           c-2.2-0.1-4.3,0.4-6.3,1.5c-1.6,0.9-3,2.3-3.9,3.9c-0.9,1.7-1.4,3.6-1.3,5.5V75c0,1.5-0.5,2.9-1.6,4c-2.2,2.2-5.7,2.2-7.9,0
                           c-1-1.1-1.6-2.5-1.6-4V36.9c0-1.5,0.5-2.9,1.6-4c2.2-2.2,5.7-2.2,7.9,0c1,1.1,1.6,2.5,1.6,4v4l-1.4-0.3c0.7-1.2,1.5-2.3,2.4-3.4
                           c1.1-1.3,2.3-2.4,3.7-3.4c1.5-1.1,3.1-1.9,4.8-2.5C594.2,30.7,596.1,30.3,598.1,30.3z"/>
                        <path class="st0" d="M649.9,81.5c-4.7,0.1-9.4-1-13.6-3.3c-3.7-2.1-6.8-5.2-8.8-8.9c-2.1-3.9-3.2-8.3-3.1-12.7
                           c-0.1-4.9,1-9.8,3.4-14.2c2.1-3.7,5.1-6.8,8.8-9c3.5-2,7.5-3.1,11.5-3.1c3.1,0,6.1,0.6,8.9,1.9c2.8,1.3,5.3,3.1,7.4,5.3
                           c2.2,2.3,3.9,4.9,5.1,7.8c1.3,3,1.9,6.2,1.9,9.4c0,1.4-0.7,2.7-1.8,3.6c-1.1,0.9-2.5,1.4-3.9,1.4h-35.2l-2.8-9.2h33.8l-2,1.8v-2.5
                           c-0.1-1.8-0.8-3.4-1.9-4.8c-1.1-1.4-2.6-2.6-4.2-3.4c-1.7-0.8-3.5-1.3-5.4-1.3c-1.8,0-3.5,0.2-5.2,0.7c-1.6,0.5-3,1.3-4.1,2.5
                           c-1.3,1.3-2.3,2.9-2.8,4.7c-0.7,2.4-1.1,4.9-1,7.5c-0.1,2.9,0.6,5.9,2.1,8.4c1.3,2.2,3.1,4,5.3,5.3c2.1,1.2,4.6,1.8,7,1.8
                           c1.8,0,3.7-0.1,5.4-0.6c1.1-0.3,2.3-0.7,3.3-1.3c0.8-0.5,1.6-1,2.3-1.3c1-0.5,2-0.8,3.1-0.8c2.6,0,4.7,2,4.7,4.6c0,0,0,0,0,0.1
                           c-0.1,1.8-1,3.5-2.6,4.5c-2,1.6-4.3,2.8-6.8,3.6C656,81,653,81.5,649.9,81.5z"/>
                        <path class="st0" d="M693.1,75c0.1,3.1-2.4,5.6-5.4,5.6c-0.1,0-0.1,0-0.2,0c-1.5,0-2.9-0.6-3.9-1.6c-1.1-1.1-1.6-2.5-1.6-4V18
                           c-0.1-3.1,2.4-5.6,5.4-5.6c0.1,0,0.1,0,0.2,0c1.5-0.1,2.9,0.5,3.9,1.6c1,1.1,1.6,2.5,1.5,4V75z"/>
                     </g>
                  </g>
                  </svg>
                  <div class="f-16 f-md-18 w300 mt20 lh150 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-16 f-md-18 w300 lh150 white-clr text-xs-center">Copyright © QuickFunnel 2023</div>
                  <ul class="footer-ul f-16 f-md-18 w300 white-clr text-center text-md-right mt20 mt-md0">
                     <li><a href="https://support.oppyo.com/hc/en-us" class="white-clr t-decoration-none" target="_blank">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://quickfunnel.live/legal/privacy-policy.html" class="white-clr t-decoration-none" target="_blank">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://quickfunnel.live/legal/terms-of-service.html" class="white-clr t-decoration-none" target="_blank">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://quickfunnel.live/legal/disclaimer.html" class="white-clr t-decoration-none" target="_blank">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://quickfunnel.live/legal/gdpr.html" class="white-clr t-decoration-none" target="_blank">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://quickfunnel.live/legal/dmca.html" class="white-clr t-decoration-none" target="_blank">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://quickfunnel.live/legal/anti-spam.html" class="white-clr t-decoration-none" target="_blank">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!--Footer Section End -->
      <!-- timer --->
      <?php
         if ($now < $exp_date) {
         
         ?>
      <script type="text/javascript">
         // Count down milliseconds = server_end - server_now = client_end - client_now
         var server_end = <?php echo $exp_date; ?> * 1000;
         var server_now = <?php echo time(); ?> * 1000;
         var client_now = new Date().getTime();
         var end = server_end - server_now + client_now; // this is the real end time
         
         var noob = $('.countdown').length;
         
         var _second = 1000;
         var _minute = _second * 60;
         var _hour = _minute * 60;
         var _day = _hour * 24
         var timer;
         
         function showRemaining() {
             var now = new Date();
             var distance = end - now;
             if (distance < 0) {
                 clearInterval(timer);
                 document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
                 return;
             }
         
             var days = Math.floor(distance / _day);
             var hours = Math.floor((distance % _day) / _hour);
             var minutes = Math.floor((distance % _hour) / _minute);
             var seconds = Math.floor((distance % _minute) / _second);
             if (days < 10) {
                 days = "0" + days;
             }
             if (hours < 10) {
                 hours = "0" + hours;
             }
             if (minutes < 10) {
                 minutes = "0" + minutes;
             }
             if (seconds < 10) {
                 seconds = "0" + seconds;
             }
             var i;
             var countdown = document.getElementsByClassName('countdown');
             for (i = 0; i < noob; i++) {
                 countdown[i].innerHTML = '';
         
                 if (days) {
                     countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-30 f-md-40 timerbg">' + days + '</span><br><span class="f-16 w500">Days</span> </div>';
                 }
         
                 countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-30 f-md-40 timerbg">' + hours + '</span><br><span class="f-16 w500">Hours</span> </div>';
         
                 countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-30 f-md-40 timerbg">' + minutes + '</span><br><span class="f-16 w500">Mins</span> </div>';
         
                 countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-30 f-md-40 timerbg">' + seconds + '</span><br><span class="f-16 w500">Sec</span> </div>';
             }
         
         }
         timer = setInterval(showRemaining, 1000);
      </script>    
      <?php
         } else {
         echo "Times Up";
         }
         ?>
      <script type="text/javascript">
         // Special handling for in-app browsers that don't always support new windows
         (function() {
             function browserSupportsNewWindows(userAgent) {
                 var rules = [
                     'FBIOS',
                     'Twitter for iPhone',
                     'WebView',
                     '(iPhone|iPod|iPad)(?!.*Safari\/)',
                     'Android.*(wv|\.0\.0\.0)'
                 ];
                 var pattern = new RegExp('(' + rules.join('|') + ')', 'ig');
                 return !pattern.test(userAgent);
             }
         
             if (!browserSupportsNewWindows(navigator.userAgent || navigator.vendor || window.opera)) {
                 document.getElementById('af-form-575032983').parentElement.removeAttribute('target');
             }
         })();
      </script><script type="text/javascript">
         (function() {
             var IE = /*@cc_on!@*/false;
             if (!IE) { return; }
             if (document.compatMode && document.compatMode == 'BackCompat') {
                 if (document.getElementById("af-form-575032983")) {
                     document.getElementById("af-form-575032983").className = 'af-form af-quirksMode';
                 }
                 if (document.getElementById("af-body-575032983")) {
                     document.getElementById("af-body-575032983").className = "af-body inline af-quirksMode";
                 }
                 if (document.getElementById("af-header-575032983")) {
                     document.getElementById("af-header-575032983").className = "af-header af-quirksMode";
                 }
                 if (document.getElementById("af-footer-575032983")) {
                     document.getElementById("af-footer-575032983").className = "af-footer af-quirksMode";
                 }
             }
         })();
      </script>
      <!-- /AWeber Web Form Generator 3.0.1 -->
   </body>
</html>
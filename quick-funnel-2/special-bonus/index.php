<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <!-- Tell the browser to be responsive to screen width -->
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
      <meta name="title" content="QuickFunnel Bonuses">
      <meta name="description" content="QuickFunnel Bonuses">
      <meta name="keywords" content="Have A Sneak Peak of A Breakthrough Technology That Analyzes, Detects & Fix Deadly Legal Flaws on Your Website Just by Copy Pasting a Single Line of Code">
      <meta property="og:image" content="https://www.quickfunnel.live/special-bonus/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Dr. Amit Pareek">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="QuickFunnel Bonuses">
      <meta property="og:description" content="Have A Sneak Peak of A Breakthrough Technology That Analyzes, Detects & Fix Deadly Legal Flaws on Your Website Just by Copy Pasting a Single Line of Code">
      <meta property="og:image" content="https://www.quickfunnel.live/special-bonus/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="QuickFunnel Bonuses">
      <meta property="twitter:description" content="Have A Sneak Peak of A Breakthrough Technology That Analyzes, Detects & Fix Deadly Legal Flaws on Your Website Just by Copy Pasting a Single Line of Code">
      <meta property="twitter:image" content="https://www.quickfunnel.live/special-bonus/thumbnail.png">
      <title>QuickFunnel Bonuses</title>
      <!-- Shortcut Icon  -->
      <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
      <!-- Css CDN Load Link -->
      <link rel="stylesheet" type="text/css" href="assets/css/bonus-style.css">
      
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&family=Lexend:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
      <!-- required -->
      <!-- Start Editor required -->
      <link rel="stylesheet" href="https://cdn.oppyo.com/launches/yoseller/common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style.css" type="text/css">
      <script src="https://cdn.oppyo.com/launches/writerarc/common_assets/js/jquery.min.js"></script>
    
   </head>
   <body>
      <!-- New Timer  Start-->
      <?php
         $date = 'Sept 26 2023 11:00 PM EST';
         $exp_date = strtotime($date);
         $now = time();  
         /*
         
         $date = date('F d Y g:i:s A eO');
         $rand_time_add = rand(700, 1200);
         $exp_date = strtotime($date) + $rand_time_add;
         $now = time();*/
         
         if ($now < $exp_date) {
         ?>
      <?php
         } else {
         	echo "Times Up";
         }
         ?>
      <!-- New Timer End -->
      <?php
         if(!isset($_GET['afflink'])){
         $_GET['afflink'] = 'https://quickfunnel.live/special/';
         $_GET['name'] = 'Dr. Amit Pareek';      
         }
         else {
             
             $_GET['afflinkbundle'] = 'https://quickfunnel.live/bundle/'.$_GET['afflink'];
             $fe_id_array=explode("/",$_GET['afflink']); 
             $bundle_id="392382";
             $_GET['afflinkbundle']=str_replace($fe_id_array[5],$bundle_id,$_GET['afflink']);
         }
         ?>
          <?php
         if(!isset($_GET['afflinkbundle'])){
         $_GET['afflinkbundle'] = 'https://quickfunnel.live/bundle/';
         $_GET['name'] = 'Dr. Amit Pareek';      
         }
         ?>
     <!-- Header Section Start -->   
      <div class="header-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                  viewBox="0 0 693.1 109.7" style="enable-background:new 0 0 693.1 109.7; height:45px;" xml:space="preserve">
                     <style type="text/css">
                        .st0{fill:#FFFFFF;}
                        .st1{fill:#EF3E39;}
                        .st2{fill:#853594;}
                        .st3{fill:#EF3E3A;}
                     </style>
                     <g>
                        <g id="Layer_1-2">
                           <path class="st0" d="M140.4,0H18.9c-2.8-0.1-5.2,2.1-5.4,4.9s2.1,5.2,4.9,5.4c0.2,0,0.3,0,0.5,0h42.3c2.8,0,5.1,2.3,5.1,5.1
                           c0,2.8-2.3,5.1-5.1,5.1h-56c-2.8,0.1-5,2.5-4.9,5.4c0.1,2.7,2.3,4.8,4.9,4.9h35.6l0,0h13.5c2.8,0.1,5,2.5,4.9,5.4
                           c-0.1,2.7-2.3,4.8-4.9,4.9H46c-2.8,0-5.1,2.4-5.1,5.2c0,2.7,2.1,4.9,4.8,5.1h17.8c2.8,0,5.2,2.2,5.2,5.1c0,2.8-2.2,5.2-5.1,5.2h-3
                           c-1.3,0-2.6,0.6-3.5,1.5c-2,2-2,5.2-0.1,7.3c1,1,2.3,1.5,3.7,1.5h8v37.7L103.1,96V68.6l42.8-57.6c2.3-3,1.6-7.3-1.4-9.6
                           C143.3,0.5,141.9,0,140.4,0z M66.2,36c0-2.8,2.3-5.1,5.1-5.1h10c2.8,0.1,5,2.5,4.9,5.4c-0.1,2.7-2.3,4.8-4.9,4.9h-10
                           C68.5,41.1,66.2,38.8,66.2,36z"/>
                           <path class="st1" d="M34,46.3c0,2.8-2.3,5.1-5.1,5.1h-10c-2.8,0-5.1-2.3-5.1-5.1c0-2.8,2.3-5.1,5.1-5.1c0,0,0,0,0,0h10
                           C31.7,41.2,34,43.5,34,46.3z"/>
                           <path class="st1" d="M48.8,66.9c0,2.8-2.3,5.1-5.1,5.1h-2.9c-2.8,0-5.1-2.1-5.3-4.8c-0.1-1.5,0.4-3,1.5-4c0.9-0.9,2.2-1.5,3.5-1.5
                           h3.4C46.6,61.8,48.9,64.1,48.8,66.9z"/>
                           <path class="st2" d="M45.8,61.7L45.8,61.7z"/>
                           <path class="st2" d="M63.7,61.7L63.7,61.7z"/>
                           <path class="st2" d="M63.7,61.7L63.7,61.7z"/>
                           <path class="st3" d="M230,48.3c0,4.5-0.7,8.9-2.3,13.1c-1.5,3.9-3.7,7.5-6.5,10.5c-2.8,3-6.1,5.4-9.9,7c-8,3.3-17,3.3-24.9,0
                           c-3.7-1.6-7.1-4-9.8-7c-2.8-3.1-5-6.7-6.4-10.5c-1.6-4.2-2.4-8.7-2.3-13.1c0-4.5,0.7-9,2.3-13.2c1.4-3.9,3.6-7.5,6.4-10.5
                           c2.8-3,6.1-5.4,9.8-7c8-3.3,17-3.3,24.9,0c3.7,1.6,7.1,4,9.9,7c2.8,3.1,5,6.6,6.5,10.5C229.3,39.3,230,43.8,230,48.3z M222.1,48.3
                           c0.1-4.6-0.9-9.2-3-13.4c-1.9-3.7-4.7-6.9-8.2-9.1c-7.4-4.4-16.6-4.4-24,0c-3.5,2.3-6.3,5.4-8.2,9.1c-2.1,4.2-3.1,8.7-3,13.4
                           c-0.1,4.6,1,9.2,3,13.3c1.8,3.7,4.7,6.9,8.2,9.2c7.4,4.4,16.6,4.4,24,0c3.5-2.3,6.3-5.4,8.2-9.2C221.1,57.5,222.1,52.9,222.1,48.3
                           z M233.1,84.8c1.9,0.1,3.4,1.6,3.4,3.5c-0.1,1.3-0.7,2.5-1.7,3.4c-1.3,1.2-2.8,2.2-4.4,2.9c-1.8,0.9-3.7,1.5-5.7,2
                           c-1.8,0.5-3.7,0.7-5.6,0.7c-3.1,0.1-6.2-0.3-9.2-1.2c-2.1-0.7-4.2-1.5-6.2-2.6c-1.8-1-3.5-1.9-5.2-2.6c-1.9-0.8-3.9-1.2-6-1.2
                           c-1,0-2,0.2-2.9,0.6c-0.8,0.4-1.6,0.7-2.3,1.1c-0.8,0.4-1.7,0.6-2.6,0.6c-1,0-2-0.4-2.7-1.1c-0.6-0.7-1-1.6-1-2.5
                           c-0.1-1.4,0.8-2.8,2.1-3.2L200,79l8.4,0.8l-15.7,5.5l2.6-1.9c2.2,0,4.4,0.3,6.4,1.1c1.9,0.7,3.7,1.5,5.5,2.5
                           c1.8,0.9,3.6,1.8,5.5,2.5c2.1,0.8,4.4,1.1,6.6,1.1c1.8,0,3.7-0.3,5.4-1c1.2-0.5,2.4-1.2,3.6-1.9c0.8-0.6,1.7-1.3,2.4-2
                           C231.3,85.1,232.1,84.8,233.1,84.8L233.1,84.8z"/>
                           <path class="st3" d="M277.1,32.9c1,0,2,0.4,2.7,1.1c0.7,0.7,1,1.7,1,2.6v25.1c0,6.2-1.7,11-5.2,14.4s-8.3,5.1-14.5,5.1
                           c-6.2,0-11-1.7-14.4-5.1s-5.2-8.2-5.2-14.4V36.6c0-2,1.5-3.7,3.5-3.7c0,0,0.1,0,0.1,0c1,0,2,0.4,2.7,1.1c0.7,0.7,1,1.7,1,2.6v25.1
                           c0,4.2,1.1,7.3,3.2,9.5s5.2,3.2,9,3.2c4,0,7.1-1.1,9.2-3.2s3.2-5.3,3.2-9.5V36.6c0-2,1.5-3.7,3.5-3.7
                           C277,32.9,277,32.9,277.1,32.9L277.1,32.9z"/>
                           <path class="st3" d="M297.9,26c-1.2,0.1-2.4-0.2-3.4-0.9c-0.8-0.7-1.2-1.8-1.1-2.9v-1.3c-0.1-1.1,0.4-2.2,1.2-2.9
                           c1-0.7,2.2-1,3.4-0.9c1.2-0.1,2.4,0.2,3.3,0.9c0.8,0.7,1.2,1.8,1.1,2.9v1.3c0.1,1.1-0.3,2.1-1.1,2.9
                           C300.2,25.8,299.1,26.1,297.9,26z M301.5,76.8c0,1-0.4,2-1.1,2.7c-1.4,1.5-3.8,1.5-5.3,0c0,0,0,0,0,0c-0.7-0.7-1-1.7-1-2.7V35.9
                           c-0.1-2,1.5-3.8,3.5-3.9s3.8,1.5,3.9,3.5c0,0.1,0,0.3,0,0.4L301.5,76.8z"/>
                           <path class="st3" d="M336,31.7c2.9,0,5.7,0.3,8.5,1c2.2,0.5,4.2,1.5,6,2.8c1.3,0.9,2.2,2.4,2.2,4c0,0.8-0.3,1.6-0.8,2.3
                           c-0.5,0.7-1.3,1.1-2.2,1.1c-0.7,0-1.4-0.1-2.1-0.5c-0.5-0.3-1-0.7-1.5-1.2c-0.5-0.5-1.2-1-1.9-1.3c-1-0.5-2.1-0.8-3.2-1
                           c-1.4-0.2-2.8-0.4-4.2-0.4c-3.1-0.1-6.2,0.8-8.9,2.4c-2.5,1.6-4.6,3.8-6,6.5c-1.5,2.8-2.3,6-2.2,9.2c-0.1,3.2,0.7,6.4,2.1,9.2
                           c1.4,2.6,3.4,4.9,5.9,6.4c2.6,1.6,5.7,2.5,8.8,2.4c1.7,0,3.5-0.1,5.2-0.6c1.1-0.3,2.2-0.7,3.2-1.3c0.9-0.5,1.8-1.1,2.5-1.9
                           c1.4-1.3,3.5-1.2,4.8,0.1c0.6,0.7,0.9,1.6,0.9,2.5c0,1.1-0.8,2.3-2.3,3.5c-1.9,1.4-4,2.5-6.2,3.1c-2.9,0.9-5.9,1.3-8.9,1.3
                           c-4.3,0.1-8.6-1-12.3-3.3c-3.5-2.2-6.3-5.3-8.2-9c-2-3.9-3-8.3-2.9-12.7c-0.1-4.4,1-8.7,3-12.6C319.3,36.2,327.3,31.5,336,31.7z"
                           />
                           <path class="st3" d="M367.1,80.6c-1,0-2-0.4-2.7-1.1c-0.7-0.7-1-1.7-1-2.7V16c0-1,0.3-1.9,1-2.6c0.7-0.7,1.7-1.1,2.7-1.1
                           c2,0,3.7,1.5,3.7,3.5c0,0,0,0.1,0,0.1v60.8c0,1-0.4,2-1.1,2.7C369,80.2,368.1,80.6,367.1,80.6z M397.7,32.6c1,0,1.9,0.4,2.5,1.2
                           c1.3,1.2,1.4,3.2,0.2,4.6c-0.2,0.2-0.4,0.4-0.6,0.5L370,65l-0.3-8.6l25.5-22.7C395.8,33,396.7,32.6,397.7,32.6z M398.7,80.5
                           c-1,0-2-0.4-2.7-1.2l-19.7-20.9l5.5-5.1l19.5,20.8c0.7,0.7,1.1,1.7,1.1,2.8c0,1-0.4,2-1.3,2.6C400.4,80.1,399.6,80.4,398.7,80.5
                           L398.7,80.5z"/>
                           <path class="st0" d="M419,80.6c-1.6,0.1-3.1-0.5-4.2-1.6c-1.1-1.1-1.6-2.5-1.6-4V21.7c-0.1-3,2.3-5.6,5.3-5.7c0.1,0,0.2,0,0.3,0
                           h30c1.5,0,2.9,0.5,4,1.5c1.1,1,1.7,2.4,1.6,3.9c0,1.4-0.6,2.7-1.6,3.6c-1.1,1-2.5,1.6-4,1.6h-24.9l0.8-1.1v18.6l-0.6-1.2h20.6
                           c1.5,0,2.9,0.5,4,1.5c1.1,1,1.7,2.4,1.6,3.9c0,1.4-0.6,2.7-1.6,3.6c-1.1,1-2.5,1.6-4,1.6h-20.8l0.8-0.8V75c0,1.5-0.6,3-1.7,4
                           C421.9,80,420.5,80.6,419,80.6z"/>
                           <path class="st0" d="M499.3,31.3c1.5-0.1,3,0.5,4,1.6c1,1.1,1.6,2.5,1.6,4v23.4c0,6.5-1.8,11.7-5.4,15.5
                           c-3.6,3.8-8.9,5.7-15.7,5.7c-6.8,0-12-1.9-15.6-5.7s-5.4-9-5.4-15.5V36.9c0-1.5,0.5-2.9,1.6-4c2.2-2.2,5.7-2.2,7.9,0
                           c1,1.1,1.6,2.5,1.6,4v23.4c0,3.8,0.8,6.5,2.5,8.3s4.1,2.7,7.5,2.7s5.9-0.9,7.6-2.7s2.5-4.6,2.5-8.3V36.9c0-1.5,0.5-2.9,1.6-4
                           C496.4,31.8,497.8,31.2,499.3,31.3z"/>
                           <path class="st0" d="M543.2,30.3c4.6,0,8,1,10.4,2.9c2.4,1.9,4.1,4.6,4.9,7.6c0.9,3.4,1.4,7,1.3,10.6V75c0,1.5-0.5,2.9-1.6,4
                           c-2.2,2.2-5.7,2.2-7.9,0c-1-1.1-1.6-2.5-1.6-4V51.3c0-1.9-0.2-3.7-0.8-5.5c-0.5-1.6-1.5-2.9-2.8-3.9c-1.7-1.1-3.8-1.6-5.8-1.5
                           c-2.2-0.1-4.3,0.4-6.2,1.5c-1.6,0.9-3,2.3-3.9,3.9c-0.9,1.7-1.4,3.6-1.3,5.5V75c0,1.5-0.5,2.9-1.6,4c-2.2,2.2-5.7,2.2-7.9,0
                           c-1-1.1-1.6-2.5-1.6-4V36.9c0-1.5,0.5-2.9,1.6-4c2.2-2.2,5.7-2.2,7.9,0c1,1.1,1.6,2.5,1.6,4v4l-1.4-0.3c0.7-1.2,1.5-2.3,2.4-3.4
                           c1.1-1.3,2.3-2.4,3.7-3.4c1.5-1.1,3.1-1.9,4.8-2.5C539.2,30.7,541.2,30.3,543.2,30.3z"/>
                           <path class="st0" d="M598.1,30.3c4.5,0,8,1,10.4,2.9c2.4,2,4.1,4.6,4.9,7.6c0.9,3.4,1.4,7,1.3,10.6V75c0,1.5-0.5,2.9-1.6,4
                           c-2.2,2.2-5.7,2.2-7.9,0c-1-1.1-1.6-2.5-1.6-4V51.3c0-1.9-0.2-3.7-0.8-5.5c-0.5-1.6-1.5-2.9-2.8-3.9c-1.7-1.1-3.8-1.6-5.8-1.5
                           c-2.2-0.1-4.3,0.4-6.3,1.5c-1.6,0.9-3,2.3-3.9,3.9c-0.9,1.7-1.4,3.6-1.3,5.5V75c0,1.5-0.5,2.9-1.6,4c-2.2,2.2-5.7,2.2-7.9,0
                           c-1-1.1-1.6-2.5-1.6-4V36.9c0-1.5,0.5-2.9,1.6-4c2.2-2.2,5.7-2.2,7.9,0c1,1.1,1.6,2.5,1.6,4v4l-1.4-0.3c0.7-1.2,1.5-2.3,2.4-3.4
                           c1.1-1.3,2.3-2.4,3.7-3.4c1.5-1.1,3.1-1.9,4.8-2.5C594.2,30.7,596.1,30.3,598.1,30.3z"/>
                           <path class="st0" d="M649.9,81.5c-4.7,0.1-9.4-1-13.6-3.3c-3.7-2.1-6.8-5.2-8.8-8.9c-2.1-3.9-3.2-8.3-3.1-12.7
                           c-0.1-4.9,1-9.8,3.4-14.2c2.1-3.7,5.1-6.8,8.8-9c3.5-2,7.5-3.1,11.5-3.1c3.1,0,6.1,0.6,8.9,1.9c2.8,1.3,5.3,3.1,7.4,5.3
                           c2.2,2.3,3.9,4.9,5.1,7.8c1.3,3,1.9,6.2,1.9,9.4c0,1.4-0.7,2.7-1.8,3.6c-1.1,0.9-2.5,1.4-3.9,1.4h-35.2l-2.8-9.2h33.8l-2,1.8v-2.5
                           c-0.1-1.8-0.8-3.4-1.9-4.8c-1.1-1.4-2.6-2.6-4.2-3.4c-1.7-0.8-3.5-1.3-5.4-1.3c-1.8,0-3.5,0.2-5.2,0.7c-1.6,0.5-3,1.3-4.1,2.5
                           c-1.3,1.3-2.3,2.9-2.8,4.7c-0.7,2.4-1.1,4.9-1,7.5c-0.1,2.9,0.6,5.9,2.1,8.4c1.3,2.2,3.1,4,5.3,5.3c2.1,1.2,4.6,1.8,7,1.8
                           c1.8,0,3.7-0.1,5.4-0.6c1.1-0.3,2.3-0.7,3.3-1.3c0.8-0.5,1.6-1,2.3-1.3c1-0.5,2-0.8,3.1-0.8c2.6,0,4.7,2,4.7,4.6c0,0,0,0,0,0.1
                           c-0.1,1.8-1,3.5-2.6,4.5c-2,1.6-4.3,2.8-6.8,3.6C656,81,653,81.5,649.9,81.5z"/>
                           <path class="st0" d="M693.1,75c0.1,3.1-2.4,5.6-5.4,5.6c-0.1,0-0.1,0-0.2,0c-1.5,0-2.9-0.6-3.9-1.6c-1.1-1.1-1.6-2.5-1.6-4V18
                           c-0.1-3.1,2.4-5.6,5.4-5.6c0.1,0,0.1,0,0.2,0c1.5-0.1,2.9,0.5,3.9,1.6c1,1.1,1.6,2.5,1.5,4V75z"/>
                        </g>
                     </g>
                  </svg>
               </div>
               <div class="col-12 mt20">
                  <div class="row">
                  <div class="col-md-12 col-12 text-center mt30">
                  <div class="text-center">
                     <div class="f-md-32 f-20 lh140 w400 white-clr d-block d-md-flex align-items-center justify-content-center">
                     <span class="w600 red-clr"><?php echo $_GET['name'];?>'s</span> &nbsp;special bonus for QuickFunnel
                        
                     </div>
                  </div>
               </div>
                     <div class="col-12 col-md-12 text-center mt20">
                        <div class="f-16 f-md-20 w400 black-clr lh140 text-capitalize preheadline relative">
                        Grab QuickFunnels With Great Grand Bonuses Of The Year 2023, Before The Deal Ends...
                           <img src="assets/images/pre-heading-icon.webp" class="img-fluid d-none d-md-block gameimg">
                        </div>
                     </div>
                     
                     <div class="col-12 mt-md50 mt20 f-md-42 f-24 w600 text-center white-clr lh140"> 
                     Have A Sneak Peak -  <span class="red-clr"> How You Can Launch Blazing-Fast Loading Funnels And Websites In Just 7 Minutes </span>  With No Tech Hassles & No Monthly Fee Ever...
                     </div>
                     <div class="col-12 mt-md30 mt20">
                        <img src="assets/images/hr.webp" alt="hr" class="mx-auto img-fluid d-block hr">
                     </div>
                     <div class="col-12 mt20 mt-md30 text-center">
                        <div class="f-18 f-md-20 w400 text-center lh140 white-clr text-capitalize">               
                        Introducing The Industry’s First Visual Funnel Planner, 400+ Beautiful Templates, Free-Flow Editor, And <br class="d-none d-md-block">Inbuilt E-Mail Marketing - All Rolled Into One Easy To Use Application!
                        </div>
                     </div>
                     <div class="col-12 col-md-10 mx-auto mt20 mt-md40">
                     <div class="video-box">
                        <div style="padding-bottom: 56.25%;position: relative;">
                           <iframe src="https://quickfunnel.dotcompal.com/video/embed/occtysjaap" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                        </div>
                     </div>
               </div>
                  </div>
               </div>
            </div>
         </div>
      </div>


      <div class="step-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-md-45 f-28 w700 lh140 text-center black-clr">
                     Create <span class="red-clr"> Amazing Profit Pulling Websites &amp; Funnels</span> in 3 Easy Steps
                     <img src="assets/images/line.webp" alt="Line" class="mx-auto d-block img-fluid mt5">
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md90">
               <div class="col-12 col-md-4">
                  <div class="steps-block">
                     <img src="assets/images/step1.webp" class="img-fluid mx-auto d-block" alt="Upload">
                     <div class="f-24 f-md-32 w700 black-clr lh150 mt20 mt-md30 text-center">
                        Choose
                     </div>
                     <div class="f-18 w400 black-clr lh150 mt15 text-center">
                        To start, just choose from 400+ ready-to-use, beautiful templates according to your business niche &amp; marketing goals.
                     </div>
                 </div>
               </div>
               <div class="col-12 col-md-4 mt20 mt-md0">
                  <div class="steps-block">
                     <img src="assets/images/step2.webp" class="img-fluid mx-auto d-block" alt="Upload">
                     <div class="f-24 f-md-32 w700 black-clr lh150 mt20 mt-md30 text-center">
                        Edit
                     </div>
                     <div class="f-18 w400 black-clr lh150 mt15 text-center">
                        Revolutionize your funnel creation with our next-generation Drag &amp; Drop page editor and free-flow funnel planner! Zero coding or design skills needed.
                     </div>
                 </div>
               </div>
               <div class="col-12 col-md-4 mt20 mt-md0">
                  <div class="steps-block">
                     <img src="assets/images/step3.webp" class="img-fluid mx-auto d-block" alt="Upload">
                     <div class="f-24 f-md-32 w700 black-clr lh150 mt20 mt-md30 text-center">
                        Publish &amp; Get Paid
                     </div>
                     <div class="f-18 w400 black-clr lh150 mt15 text-center">
                        Now publish your funnel &amp; website that works 24*7 for you &amp; bring automated sales.
                     </div>
                 </div>
               </div>
            </div>
            <div class="row mt20 mt-md90">
               <div class="col-12 f-18 f-md-20 w400 lh140 black-clr text-center">
                  It Just Takes <span class="w600">Minutes</span> to go live… <span class="w600"> No Technical &amp; Designing Skills</span> of Any Kind is Needed!
               </div>
               <div class="col-12 f-md-24 f-20 w400 lh140 black-clr text-center mt15">
                  Plus, with included FREE commercial license, <span class="w600">this is the easiest &amp; fastest way</span> to start 6 figure business and help desperate local businesses in no time!
               </div>
            </div>
         </div>
      </div>


      <!-- CTA Btn Start-->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <!-- <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 red-clr">"AMITVIP"</span> for an Additional Discount on Commercial Licence
                  </div> -->
               </div>
               <div>
                  <div class="col-12 mt20">
                  <div class="f-18 f-md-22 lh140 w400 text-center mt10 white-clr">
                     Use Coupon Code <span class="w600 red-clr">“Quick3”</span> for An Additional <span class="w600 red-clr">$3 Discount</span>
                  </div>
                  </div>
                           </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt10">
                  
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab QuickFunnel + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <div class="f-18 f-md-22 lh140 w400 text-center mt10 white-clr">
                     Use Coupon Code <span class="w600 red-clr">“Quickbundle”</span> for An Additional <span class="w600 red-clr">$50 Discount</span>
                  </div>
                  <a href="<?php echo $_GET['afflinkbundle']; ?>" class="text-center bonusbuy-btn mt10">
                  <span class="text-center">Grab QuickFunnel Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 smmltd">Days</span>
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Sec</span>
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Btn End -->
<div class="second-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-28 f-md-45 lh140 w400 text-center black-clr">
                     We Guarantee, <span class="w700 red-clr">This Is The Only Funnel &amp; <br class="d-none d-md-block"> Website Builder You’ll Ever Need…</span> 
                  </div>
               </div>
            </div>
            <div class="row mt0 mt-md50">   
               <div class="col-12 col-md-6">
                  <div class="feature-list l-feature-odd">
                     <div class="f-18 f-md-20 lh140 w400">
                       <span class="w600">Create Proven-To-Convert Funnels &amp; Websites</span>  In Just A Few Minutes In Any Niche
                     </div>
                  </div>
                  <div class="feature-list l-feature-even">
                     <div class="f-18 f-md-20 lh140 w400">
                       <span class="w600">Build Any Type Of Funnel– </span> Sales Funnel, Lead Funnel, Webinar And Product Launches Funnel
                     </div>
                  </div>
                  <div class="feature-list l-feature-odd">
                     <div class="f-18 f-md-20 lh140 w400">
                        <span class="w600">400+ High Converting &amp; Ready-To-Go Templates</span>
                     </div>
                  </div>
                  <div class="feature-list l-feature-even">
                     <div class="f-18 f-md-20 lh140 w400">
                       <span class="w600">Mobile Friendly &amp; Fast Loading Pages-</span> Never Lose A Single Visitor, Lead Or Sale.
                     </div>
                  </div>
                  <div class="feature-list l-feature-odd">
                     <div class="f-18 f-md-20 lh140 w400">
                       <span class="w600">Inbuilt Email Marketing &amp; Lead Management–</span>  Collect Unlimited Leads and Send Unlimited Emails
                     </div>
                  </div>
                  <div class="feature-list l-feature-even">
                     <div class="f-18 f-md-20 lh140 w400">
                       <span class="w600">SEO Friendly &amp; Social Media Optimized Pages</span>  For More Traffic.
                     </div>
                  </div>
                  <div class="feature-list l-feature-odd">
                     <div class="f-18 f-md-20 lh140 w400">
                       <span class="w600">Precise Analytics </span> To Measure &amp; Make The Right Decisions For Future Success!
                     </div>
                  </div>
                  <div class="feature-list l-feature-even">
                     <div class="f-18 f-md-20 lh140 w400">
                       <span class="w600">Automatic SSL Encryption-</span> 100% Unbreakable Security
                     </div>
                  </div>
                  <div class="feature-list l-feature-odd">
                     <div class="f-18 f-md-20 lh140 w600">
                        PLUS, YOU’LL ALSO RECEIVE FREE COMMERCIAL LICENCE WHEN YOU GET STARTED TODAY!
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-6">
                  <div class="f-18 lh140 w400">                             
                     <div class="feature-list r-feature-odd">
                        <div class="f-18 f-md-20 lh140 w600">
                           Industry’s First, Fully Drag &amp; Drop &amp; Visual Funnel Designer
                        </div>
                     </div>
                     <div class="feature-list r-feature-even">
                        <div class="f-18 f-md-20 lh140 w400">
                          <span class="w600">Create Any Type Of Landing Page -</span> Sales Page, Lead Page, Membership, Bonus, or Product Review Page
                        </div>
                     </div>
                     <div class="feature-list r-feature-odd">
                        <div class="f-18 f-md-20 lh140 w400">
                           <span class="w600">Next-Gen Drag And Drop Editor</span> To Create Pixel Perfect Pages Or Templates From Scratch 
                        </div>
                     </div>
                     <div class="feature-list r-feature-even">
                        <div class="f-18 f-md-20 lh140 w400">
                           User Friendly Business Central Dashboard- <span class="w600">Everything You Need, Under One Roof At Your Fingertips!</span> 
                        </div>
                     </div>
                     <div class="feature-list r-feature-odd">
                        <div class="f-18 f-md-20 lh140 w400">
                          <span class="w600">Cutting-Edge Autoresponder Integration-</span> Use Any Email Autoresponder Without Any Technical Glitches
                        </div>
                     </div>
                     <div class="feature-list r-feature-even">
                        <div class="f-18 f-md-20 lh140 w400">
                          <span class="w600">Grab Maximum Leads -</span> Show Pop-Ups Inside Landing Pages And Funnels Pages
                        </div>
                     </div>
                     <div class="feature-list r-feature-odd">
                        <div class="f-18 f-md-20 lh140 w600">
                           Unmatched Webinar Set &amp; Forget Integration
                        </div>
                     </div>
                     <div class="feature-list r-feature-even">
                        <div class="f-18 f-md-20 lh140 w400">
                          <span class="w600"> No Expensive Domains &amp; Hosting Services-</span> Host Everything On Our Lightning Fast Server.
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>



      <section class="didyouknow-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-12 text-center">
                  <div class="f-28 f-md-45 w700 white-clr text-center lh140 head-design mt0 mt-md50">Do Funnels Still Work In 2023?</div>
                  <div class="f-28 f-md-100 w700 lh140 mt20 mt-md20">
                    <span class="black-clr"><img src="assets/images/thumbsup-smily.webp" alt="Thumbsup Smily"></span> <span class="red-clr">Yep,</span> <br>
                  </div>
                  <div class="f-24 f-md-28 w400 lh140 black-clr mt20 demo-text">
                     Here’s the 1 exact funnel that <span class="w600">got more than 32,000 visitors with 1,715 sales <br class="d-none d-md-block"> and total $156,837 in sales for one of my products.</span> 
                  </div>  
                  <div class="mt50 mt-md90">
                     <img src="assets/images/siteefy.webp" alt="Siteefy" class="img-fluid mx-auto d-block">
                  </div>
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-12 col-md-12 text-center">
                  <div class="f-24 f-md-28 w600 lh140 black-clr mt20 demo-text">
                     And by using the power of funnels, we have made 651K in sales <br class="d-none d-md-block"> &amp; 378K in affiliate commissions in the last 10 months.
                  </div>  
                  <div class="mt50 mt-md90">
                     <img src="assets/images/website-process.webp" alt="Siteefy" class="img-fluid mx-auto d-block">
                  </div>
               </div>
            </div>
            <div class="didyouknow-box mt20 mt-md70">
               <div class="row">
                  <div class="col-12">
                     <div class="f-18 f-md-20 lh150 w400 black-clr">
                        Listen, if you’re going to compete and get the life you desire in this ever-growing digital marketing era, then you’re going to <span class="w600"> <u>need an online presence that converts.</u></span> 
                        <br><br>
                        That means you’ll need websites with fast pages that guide, engage, and convert visitors into customers. <span class="w600">And that’s what funnels do. </span> 
                        <br><br>
                       <span class="w600">You only need to setup a funnel once and it delivers RESULTS again and again on complete autopilot!</span> 
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>


         <!-- CTA Btn Start-->
         <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <!-- <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 red-clr">"AMITVIP"</span> for an Additional Discount on Commercial Licence
                  </div> -->
               </div>
               <div class="col-12 mt20">
                  <div class="f-18 f-md-22 lh140 w400 text-center mt10 white-clr">
                     Use Coupon Code <span class="w600 red-clr">“Quick3”</span> for An Additional <span class="w600 red-clr">$3 Discount</span>
                  </div>
                  </div>
                           </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt10">
                  
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab QuickFunnel + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <div class="f-18 f-md-22 lh140 w400 text-center mt10 white-clr">
                     Use Coupon Code <span class="w600 red-clr">“Quickbundle”</span> for An Additional <span class="w600 red-clr">$50 Discount</span>
                  </div>
                  <a href="<?php echo $_GET['afflinkbundle']; ?>" class="text-center bonusbuy-btn mt10">
                  <span class="text-center">Grab QuickFunnel Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 smmltd">Days</span>
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Sec</span>
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Btn End -->


      <div class="testimonial-section">
         <div class="container ">
            <div class="row ">
               <!-- <div class="col-12 f-24 f-md-28 w400 lh140 black-clr text-center ">
                  And it’s not just us. We also have <span class="w600">400+ TOP marketers</span> and more than <span class="w600">1000 customers</span> are successfully using it and ABSOLUTELY loving it.
               </div> -->
               <div class="col-12 f-28 f-md-45 w700 lh140 black-clr text-center">
                  <span class="red-clr">Checkout What Marketers &amp; Early Users</span>  Have to Say About QuickFunnel
               </div>
            </div>
            <div class="row mt20 mt-md80 align-items-center">
               <div class="col-md-6">
                  <div class="responsive-video border-video">
                     <iframe src="https://quickfunnel.dotcompal.com/video/embed/ccb2pf2a7f" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                  </div>
                  <div class="f-20 f-md-24 w600 lh140 black-clr text-center mt20 mt-md50">James Milo</div> 
                  <div class="f-18 f-md-20 w400 lh140 black-clr text-center mt10">21st Century Expert.com &amp; Video Animation Network&nbsp;</div>   
               </div>
               <div class="col-md-6 mt20 mt-md0">
                  <div class="responsive-video border-video">
                     <iframe src="https://quickfunnel.dotcompal.com/video/embed/pw7r36cz1j" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                  </div>
                  <div class="f-20 f-md-24 w600 lh140 black-clr text-center mt20 mt-md50">Craig McIntosh&nbsp;</div> 
                  <div class="f-18 f-md-20 w400 lh140 black-clr text-center mt10">Digital Marketing Consultant&nbsp;</div>  
               </div>
            </div>
            <!-- <div class="row mt20 mt-md80 align-items-center">
               <div class="col-md-6 order-md-2">
                  <div class="responsive-video border-video">
                     <iframe src="https://quickfunnel.dotcompal.com/video/embed/pw7r36cz1j" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                  </div>
               </div>
               <div class="col-md-6 mt20 mt-md0 order-md-1"> 
                  <div class="f-20 f-md-24 w600 lh140 black-clr text-center text-md-start">Craig McIntosh </div> 
                  <div class="f-18 f-md-20 w400 lh140 black-clr text-center text-md-start mt10">Digital Marketing Consultant </div>  
               </div>
            </div> -->

            <div class="row row-cols-md-2 row-cols-1 gx4 mt80">
               <div class="col mt50">
                  <div class="single-testimonial">
                     <div class="st-img">
                        <img src="assets/images/imreviw.webp" class="img-fluid d-block mx-auto" alt="Imreviw">
                     </div>
                     <div class="mt20 md-md50 f-24 f-md-28 w600 lh140 black-clr"> IMReview Squad </div>
                     <div class="stars mt10">
                        <a href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                     </div>
                     <img src="assets/images/quote.webp" alt="Quotes" class="mx-auto d-block img-fluid mt20 mt-md50">
                     <p class="mt20 f-18 f-md-20 lh140 w400 text-center">
                        Wow!!! It’s an amazing creation that allows me to build high-converting pages and funnels EASY &amp; FAST.
                        Apart from its features, the best part I personally love is that it comes at a one-time price for the next few days…Complete value for your money. Guys go for it…
                     </p>
                  </div>
               </div>
               <div class="col mt120 mt-md50">
                  <div class="single-testimonial">
                     <div class="st-img">
                        <img src="assets/images/samuel.webp" class="img-fluid d-block mx-auto" alt="Samuel Marco">
                     </div>
                     <div class="mt20 md-md50 f-24 f-md-28 w600 lh140 black-clr"> Samuel Marco  </div>
                     <div class="stars mt10">
                        <a href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                     </div>
                     <img src="assets/images/quote.webp" alt="Quotes" class="mx-auto d-block img-fluid mt20 mt-md50">
                     <p class="mt20 f-18 f-md-20 lh140 w400 text-center">
                        QuickFunnel is next-level quality software. It's so easy to use, and the training included makes it even easier to build a page or funnel in any niche in a few minutes.
                        <br><br>
                        I love the fact that you can create high-converting sales funnels and pages without actually being a
                        technical nerd...all without any special skills. I say that this is a MUST-HAVE software for marketers! …
                     </p>
                  </div>
               </div>
            </div>

            <div id="features"></div>
         </div>
      </div>

      <div class="presenting-sec">
         
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-50 w600 red-clr lh140 presenting-head">
                     Proudly Presenting…
                  </div>
                  <div class="col-12 mt20 mt-md50">
                     <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 693.1 109.7" style="enable-background:new 0 0 693.1 109.7; height:45px;" xml:space="preserve">
                        <style type="text/css">
                           .st0{fill:#FFFFFF;}
                           .st1{fill:#EF3E39;}
                           .st2{fill:#853594;}
                           .st3{fill:#EF3E3A;}
                        </style>
                        <g>
                           <g id="Layer_1-2">
                              <path class="st0" d="M140.4,0H18.9c-2.8-0.1-5.2,2.1-5.4,4.9s2.1,5.2,4.9,5.4c0.2,0,0.3,0,0.5,0h42.3c2.8,0,5.1,2.3,5.1,5.1
                              c0,2.8-2.3,5.1-5.1,5.1h-56c-2.8,0.1-5,2.5-4.9,5.4c0.1,2.7,2.3,4.8,4.9,4.9h35.6l0,0h13.5c2.8,0.1,5,2.5,4.9,5.4
                              c-0.1,2.7-2.3,4.8-4.9,4.9H46c-2.8,0-5.1,2.4-5.1,5.2c0,2.7,2.1,4.9,4.8,5.1h17.8c2.8,0,5.2,2.2,5.2,5.1c0,2.8-2.2,5.2-5.1,5.2h-3
                              c-1.3,0-2.6,0.6-3.5,1.5c-2,2-2,5.2-0.1,7.3c1,1,2.3,1.5,3.7,1.5h8v37.7L103.1,96V68.6l42.8-57.6c2.3-3,1.6-7.3-1.4-9.6
                              C143.3,0.5,141.9,0,140.4,0z M66.2,36c0-2.8,2.3-5.1,5.1-5.1h10c2.8,0.1,5,2.5,4.9,5.4c-0.1,2.7-2.3,4.8-4.9,4.9h-10
                              C68.5,41.1,66.2,38.8,66.2,36z"></path>
                              <path class="st1" d="M34,46.3c0,2.8-2.3,5.1-5.1,5.1h-10c-2.8,0-5.1-2.3-5.1-5.1c0-2.8,2.3-5.1,5.1-5.1c0,0,0,0,0,0h10
                              C31.7,41.2,34,43.5,34,46.3z"></path>
                              <path class="st1" d="M48.8,66.9c0,2.8-2.3,5.1-5.1,5.1h-2.9c-2.8,0-5.1-2.1-5.3-4.8c-0.1-1.5,0.4-3,1.5-4c0.9-0.9,2.2-1.5,3.5-1.5
                              h3.4C46.6,61.8,48.9,64.1,48.8,66.9z"></path>
                              <path class="st2" d="M45.8,61.7L45.8,61.7z"></path>
                              <path class="st2" d="M63.7,61.7L63.7,61.7z"></path>
                              <path class="st2" d="M63.7,61.7L63.7,61.7z"></path>
                              <path class="st3" d="M230,48.3c0,4.5-0.7,8.9-2.3,13.1c-1.5,3.9-3.7,7.5-6.5,10.5c-2.8,3-6.1,5.4-9.9,7c-8,3.3-17,3.3-24.9,0
                              c-3.7-1.6-7.1-4-9.8-7c-2.8-3.1-5-6.7-6.4-10.5c-1.6-4.2-2.4-8.7-2.3-13.1c0-4.5,0.7-9,2.3-13.2c1.4-3.9,3.6-7.5,6.4-10.5
                              c2.8-3,6.1-5.4,9.8-7c8-3.3,17-3.3,24.9,0c3.7,1.6,7.1,4,9.9,7c2.8,3.1,5,6.6,6.5,10.5C229.3,39.3,230,43.8,230,48.3z M222.1,48.3
                              c0.1-4.6-0.9-9.2-3-13.4c-1.9-3.7-4.7-6.9-8.2-9.1c-7.4-4.4-16.6-4.4-24,0c-3.5,2.3-6.3,5.4-8.2,9.1c-2.1,4.2-3.1,8.7-3,13.4
                              c-0.1,4.6,1,9.2,3,13.3c1.8,3.7,4.7,6.9,8.2,9.2c7.4,4.4,16.6,4.4,24,0c3.5-2.3,6.3-5.4,8.2-9.2C221.1,57.5,222.1,52.9,222.1,48.3
                              z M233.1,84.8c1.9,0.1,3.4,1.6,3.4,3.5c-0.1,1.3-0.7,2.5-1.7,3.4c-1.3,1.2-2.8,2.2-4.4,2.9c-1.8,0.9-3.7,1.5-5.7,2
                              c-1.8,0.5-3.7,0.7-5.6,0.7c-3.1,0.1-6.2-0.3-9.2-1.2c-2.1-0.7-4.2-1.5-6.2-2.6c-1.8-1-3.5-1.9-5.2-2.6c-1.9-0.8-3.9-1.2-6-1.2
                              c-1,0-2,0.2-2.9,0.6c-0.8,0.4-1.6,0.7-2.3,1.1c-0.8,0.4-1.7,0.6-2.6,0.6c-1,0-2-0.4-2.7-1.1c-0.6-0.7-1-1.6-1-2.5
                              c-0.1-1.4,0.8-2.8,2.1-3.2L200,79l8.4,0.8l-15.7,5.5l2.6-1.9c2.2,0,4.4,0.3,6.4,1.1c1.9,0.7,3.7,1.5,5.5,2.5
                              c1.8,0.9,3.6,1.8,5.5,2.5c2.1,0.8,4.4,1.1,6.6,1.1c1.8,0,3.7-0.3,5.4-1c1.2-0.5,2.4-1.2,3.6-1.9c0.8-0.6,1.7-1.3,2.4-2
                              C231.3,85.1,232.1,84.8,233.1,84.8L233.1,84.8z"></path>
                              <path class="st3" d="M277.1,32.9c1,0,2,0.4,2.7,1.1c0.7,0.7,1,1.7,1,2.6v25.1c0,6.2-1.7,11-5.2,14.4s-8.3,5.1-14.5,5.1
                              c-6.2,0-11-1.7-14.4-5.1s-5.2-8.2-5.2-14.4V36.6c0-2,1.5-3.7,3.5-3.7c0,0,0.1,0,0.1,0c1,0,2,0.4,2.7,1.1c0.7,0.7,1,1.7,1,2.6v25.1
                              c0,4.2,1.1,7.3,3.2,9.5s5.2,3.2,9,3.2c4,0,7.1-1.1,9.2-3.2s3.2-5.3,3.2-9.5V36.6c0-2,1.5-3.7,3.5-3.7
                              C277,32.9,277,32.9,277.1,32.9L277.1,32.9z"></path>
                              <path class="st3" d="M297.9,26c-1.2,0.1-2.4-0.2-3.4-0.9c-0.8-0.7-1.2-1.8-1.1-2.9v-1.3c-0.1-1.1,0.4-2.2,1.2-2.9
                              c1-0.7,2.2-1,3.4-0.9c1.2-0.1,2.4,0.2,3.3,0.9c0.8,0.7,1.2,1.8,1.1,2.9v1.3c0.1,1.1-0.3,2.1-1.1,2.9
                              C300.2,25.8,299.1,26.1,297.9,26z M301.5,76.8c0,1-0.4,2-1.1,2.7c-1.4,1.5-3.8,1.5-5.3,0c0,0,0,0,0,0c-0.7-0.7-1-1.7-1-2.7V35.9
                              c-0.1-2,1.5-3.8,3.5-3.9s3.8,1.5,3.9,3.5c0,0.1,0,0.3,0,0.4L301.5,76.8z"></path>
                              <path class="st3" d="M336,31.7c2.9,0,5.7,0.3,8.5,1c2.2,0.5,4.2,1.5,6,2.8c1.3,0.9,2.2,2.4,2.2,4c0,0.8-0.3,1.6-0.8,2.3
                              c-0.5,0.7-1.3,1.1-2.2,1.1c-0.7,0-1.4-0.1-2.1-0.5c-0.5-0.3-1-0.7-1.5-1.2c-0.5-0.5-1.2-1-1.9-1.3c-1-0.5-2.1-0.8-3.2-1
                              c-1.4-0.2-2.8-0.4-4.2-0.4c-3.1-0.1-6.2,0.8-8.9,2.4c-2.5,1.6-4.6,3.8-6,6.5c-1.5,2.8-2.3,6-2.2,9.2c-0.1,3.2,0.7,6.4,2.1,9.2
                              c1.4,2.6,3.4,4.9,5.9,6.4c2.6,1.6,5.7,2.5,8.8,2.4c1.7,0,3.5-0.1,5.2-0.6c1.1-0.3,2.2-0.7,3.2-1.3c0.9-0.5,1.8-1.1,2.5-1.9
                              c1.4-1.3,3.5-1.2,4.8,0.1c0.6,0.7,0.9,1.6,0.9,2.5c0,1.1-0.8,2.3-2.3,3.5c-1.9,1.4-4,2.5-6.2,3.1c-2.9,0.9-5.9,1.3-8.9,1.3
                              c-4.3,0.1-8.6-1-12.3-3.3c-3.5-2.2-6.3-5.3-8.2-9c-2-3.9-3-8.3-2.9-12.7c-0.1-4.4,1-8.7,3-12.6C319.3,36.2,327.3,31.5,336,31.7z"></path>
                              <path class="st3" d="M367.1,80.6c-1,0-2-0.4-2.7-1.1c-0.7-0.7-1-1.7-1-2.7V16c0-1,0.3-1.9,1-2.6c0.7-0.7,1.7-1.1,2.7-1.1
                              c2,0,3.7,1.5,3.7,3.5c0,0,0,0.1,0,0.1v60.8c0,1-0.4,2-1.1,2.7C369,80.2,368.1,80.6,367.1,80.6z M397.7,32.6c1,0,1.9,0.4,2.5,1.2
                              c1.3,1.2,1.4,3.2,0.2,4.6c-0.2,0.2-0.4,0.4-0.6,0.5L370,65l-0.3-8.6l25.5-22.7C395.8,33,396.7,32.6,397.7,32.6z M398.7,80.5
                              c-1,0-2-0.4-2.7-1.2l-19.7-20.9l5.5-5.1l19.5,20.8c0.7,0.7,1.1,1.7,1.1,2.8c0,1-0.4,2-1.3,2.6C400.4,80.1,399.6,80.4,398.7,80.5
                              L398.7,80.5z"></path>
                              <path class="st0" d="M419,80.6c-1.6,0.1-3.1-0.5-4.2-1.6c-1.1-1.1-1.6-2.5-1.6-4V21.7c-0.1-3,2.3-5.6,5.3-5.7c0.1,0,0.2,0,0.3,0
                              h30c1.5,0,2.9,0.5,4,1.5c1.1,1,1.7,2.4,1.6,3.9c0,1.4-0.6,2.7-1.6,3.6c-1.1,1-2.5,1.6-4,1.6h-24.9l0.8-1.1v18.6l-0.6-1.2h20.6
                              c1.5,0,2.9,0.5,4,1.5c1.1,1,1.7,2.4,1.6,3.9c0,1.4-0.6,2.7-1.6,3.6c-1.1,1-2.5,1.6-4,1.6h-20.8l0.8-0.8V75c0,1.5-0.6,3-1.7,4
                              C421.9,80,420.5,80.6,419,80.6z"></path>
                              <path class="st0" d="M499.3,31.3c1.5-0.1,3,0.5,4,1.6c1,1.1,1.6,2.5,1.6,4v23.4c0,6.5-1.8,11.7-5.4,15.5
                              c-3.6,3.8-8.9,5.7-15.7,5.7c-6.8,0-12-1.9-15.6-5.7s-5.4-9-5.4-15.5V36.9c0-1.5,0.5-2.9,1.6-4c2.2-2.2,5.7-2.2,7.9,0
                              c1,1.1,1.6,2.5,1.6,4v23.4c0,3.8,0.8,6.5,2.5,8.3s4.1,2.7,7.5,2.7s5.9-0.9,7.6-2.7s2.5-4.6,2.5-8.3V36.9c0-1.5,0.5-2.9,1.6-4
                              C496.4,31.8,497.8,31.2,499.3,31.3z"></path>
                              <path class="st0" d="M543.2,30.3c4.6,0,8,1,10.4,2.9c2.4,1.9,4.1,4.6,4.9,7.6c0.9,3.4,1.4,7,1.3,10.6V75c0,1.5-0.5,2.9-1.6,4
                              c-2.2,2.2-5.7,2.2-7.9,0c-1-1.1-1.6-2.5-1.6-4V51.3c0-1.9-0.2-3.7-0.8-5.5c-0.5-1.6-1.5-2.9-2.8-3.9c-1.7-1.1-3.8-1.6-5.8-1.5
                              c-2.2-0.1-4.3,0.4-6.2,1.5c-1.6,0.9-3,2.3-3.9,3.9c-0.9,1.7-1.4,3.6-1.3,5.5V75c0,1.5-0.5,2.9-1.6,4c-2.2,2.2-5.7,2.2-7.9,0
                              c-1-1.1-1.6-2.5-1.6-4V36.9c0-1.5,0.5-2.9,1.6-4c2.2-2.2,5.7-2.2,7.9,0c1,1.1,1.6,2.5,1.6,4v4l-1.4-0.3c0.7-1.2,1.5-2.3,2.4-3.4
                              c1.1-1.3,2.3-2.4,3.7-3.4c1.5-1.1,3.1-1.9,4.8-2.5C539.2,30.7,541.2,30.3,543.2,30.3z"></path>
                              <path class="st0" d="M598.1,30.3c4.5,0,8,1,10.4,2.9c2.4,2,4.1,4.6,4.9,7.6c0.9,3.4,1.4,7,1.3,10.6V75c0,1.5-0.5,2.9-1.6,4
                              c-2.2,2.2-5.7,2.2-7.9,0c-1-1.1-1.6-2.5-1.6-4V51.3c0-1.9-0.2-3.7-0.8-5.5c-0.5-1.6-1.5-2.9-2.8-3.9c-1.7-1.1-3.8-1.6-5.8-1.5
                              c-2.2-0.1-4.3,0.4-6.3,1.5c-1.6,0.9-3,2.3-3.9,3.9c-0.9,1.7-1.4,3.6-1.3,5.5V75c0,1.5-0.5,2.9-1.6,4c-2.2,2.2-5.7,2.2-7.9,0
                              c-1-1.1-1.6-2.5-1.6-4V36.9c0-1.5,0.5-2.9,1.6-4c2.2-2.2,5.7-2.2,7.9,0c1,1.1,1.6,2.5,1.6,4v4l-1.4-0.3c0.7-1.2,1.5-2.3,2.4-3.4
                              c1.1-1.3,2.3-2.4,3.7-3.4c1.5-1.1,3.1-1.9,4.8-2.5C594.2,30.7,596.1,30.3,598.1,30.3z"></path>
                              <path class="st0" d="M649.9,81.5c-4.7,0.1-9.4-1-13.6-3.3c-3.7-2.1-6.8-5.2-8.8-8.9c-2.1-3.9-3.2-8.3-3.1-12.7
                              c-0.1-4.9,1-9.8,3.4-14.2c2.1-3.7,5.1-6.8,8.8-9c3.5-2,7.5-3.1,11.5-3.1c3.1,0,6.1,0.6,8.9,1.9c2.8,1.3,5.3,3.1,7.4,5.3
                              c2.2,2.3,3.9,4.9,5.1,7.8c1.3,3,1.9,6.2,1.9,9.4c0,1.4-0.7,2.7-1.8,3.6c-1.1,0.9-2.5,1.4-3.9,1.4h-35.2l-2.8-9.2h33.8l-2,1.8v-2.5
                              c-0.1-1.8-0.8-3.4-1.9-4.8c-1.1-1.4-2.6-2.6-4.2-3.4c-1.7-0.8-3.5-1.3-5.4-1.3c-1.8,0-3.5,0.2-5.2,0.7c-1.6,0.5-3,1.3-4.1,2.5
                              c-1.3,1.3-2.3,2.9-2.8,4.7c-0.7,2.4-1.1,4.9-1,7.5c-0.1,2.9,0.6,5.9,2.1,8.4c1.3,2.2,3.1,4,5.3,5.3c2.1,1.2,4.6,1.8,7,1.8
                              c1.8,0,3.7-0.1,5.4-0.6c1.1-0.3,2.3-0.7,3.3-1.3c0.8-0.5,1.6-1,2.3-1.3c1-0.5,2-0.8,3.1-0.8c2.6,0,4.7,2,4.7,4.6c0,0,0,0,0,0.1
                              c-0.1,1.8-1,3.5-2.6,4.5c-2,1.6-4.3,2.8-6.8,3.6C656,81,653,81.5,649.9,81.5z"></path>
                              <path class="st0" d="M693.1,75c0.1,3.1-2.4,5.6-5.4,5.6c-0.1,0-0.1,0-0.2,0c-1.5,0-2.9-0.6-3.9-1.6c-1.1-1.1-1.6-2.5-1.6-4V18
                              c-0.1-3.1,2.4-5.6,5.4-5.6c0.1,0,0.1,0,0.2,0c1.5-0.1,2.9,0.5,3.9,1.6c1,1.1,1.6,2.5,1.5,4V75z"></path>
                           </g>
                        </g>
                     </svg>
                  </div>
                  <div class="col-12 mt-md50 mt20 f-md-50 f-24 w600 text-center white-clr lh140">
                     Launch <span class="red-clr">Blazing-Fast Loading Funnels And Websites In Just 7 Minutes</span> With No Tech Hassles &amp; No Monthly Fee Ever…
                 </div>
                 <div class="col-12 mt20 mt-md30 lh140 f-18 f-md-20 w500 white-clr">
                  FREE Commercial License Included to Build an Incredible Income 
                 </div>
               </div>
            </div>
            <div class="row mt20 mt-md40 align-items-center">
               <div class="col-md-7 col-12 mx-auto">
                  <img src="assets/images/product-image.webp" class="img-fluid d-block mx-auto">
               
                     <!-- <div class="video-box">
                        <div style="padding-bottom: 56.25%;position: relative;">
                           <iframe src="https://restrosuite.dotcompal.com/video/embed/67fmqio11q" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                        </div>
                     </div> -->
               </div>
               <div class="col-md-5 col-12 mt30 mt-md0">
                  <div class="proudly-features">
                     <ul class="pl0 m0 f-18 f-md-20 lh150 w400 white-clr">
                       <li> <span class="red-clr caveat w700 f-24 f-md-30">Quick &amp; Easy</span> <br> Create Lightning-Fast Funnels Easily with Free-Flow Funnel Planner &amp; Drag-N-Drop Editor </li>
                       <li> <span class="red-clr caveat w700 f-24 f-md-30">Maximize Results</span> <br> Get Max Engagement, Max Leads &amp; Max Sales with High converting Funnels &amp; Pages  </li>
                       <li> <span class="red-clr caveat w700 f-24 f-md-30">Reliable &amp; Robust</span> <br> Battle-Tested Architecture to Handle Huge Load without Any Hassles</li>
                       <li> <span class="red-clr caveat w700 f-24 f-md-30">Proven &amp; Elegant</span> <br> Empowered with 400+ Proven Converting &amp; Elegant Templates </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
   </div>
      <!-- Bonus Section Header Start -->
      <div class="bonus-header">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-10 mx-auto heading-bg text-center">
                  <div class="f-24 f-md-36 lh140 w700"> When You Purchase QuickFunnel, You Also Get <br class="hidden-xs"> Instant Access To These 20 Exclusive Bonuses</div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus Section Header End -->
          <!-- Bonus Section Header End -->
          <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">Bonus 1</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus1.webp" class="img-fluid mx-auto d-block" alt="Bonus1">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Sales Funnel Blueprint
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>Who Else Wants To Skyrocket Sales Up To 300%... With The Same Amount Of Traffic & Buyers! Here's How You Can Get Started In 48 Hours Or Less...</b></li>
                           <li>The Internet marketing business had evolved throughout years. If you are in this business long enough, you are one of those to witness the changes in the past decade. From the beginning, the Internet marketer will only offer one product to the buyers. </li>
                          <li>Moving on, today, we have a sales funnel. Changes happenso rapidly. All things were shaped to fulfill the needs of the buyers. Thanks to the evolution of Internet that happened in the past decades, the reach has grown in an epic proportion. With more and more audience, things have to change. </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #1 Section End -->
      <!-- Bonus #2 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">Bonus 2</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30">
                  <div class="row">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="assets/images/bonus2.webp" class="img-fluid mx-auto d-block" alt="Bonus2">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md0 text-capitalize">
                        Wp Easy Optins
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b> If you're new to marketing and list building use this plug-in today to move yourself forward with your list building efforts fast and with little fuss.</b></li>
                           <li>If you’re not new to marketing then you will see the potential of owning full PLR right to this product.</li>
                           <li>When installed "Wp Easy Optins" automatically creates all the pages on your blog your funnel will need: Squeeze Page, Thank You Page, Oto Page, White List Page, and all the Legal Pages</li>
                           <li>"Wp Easy Optins" will give you the url's you'll need to go to your Autoresponder and set up a campaign.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #2 Section End -->
      <!-- Bonus #3 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">Bonus 3</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus3.webp" class="img-fluid mx-auto d-block " alt="Bonus3">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Multiple Product Funnel
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>PLR Audio On How to Create a Complete Product Funnel in Your Business - An Amazing Overview!</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #3 Section End -->
      <!-- Bonus #4 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">Bonus 4</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="assets/images/bonus4.webp" class="img-fluid mx-auto d-block " alt="Bonus4">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        How To Create A Product Funnel Fast
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li> <b> This is imperative, because if you don't have a product line, you are leaving money on the table. </b> </li>
                           <li>You see, when someone buys from me one time, they often buy again and again and again - and if you don't have enough products in your funnel, they will buy from someone else. </li>
                           <li>Solve this fast!</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #4 End -->
      <!-- Bonus #5 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">Bonus 5</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus5.webp" class="img-fluid mx-auto d-block " alt="Bonus5">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        WP Profit Page Creator
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li><b>Discover a Brand New and Highly Profitable WP Plugin that Once You Fire it Up, Will Generate You Endless Sales!</b> </li>
                        <li>If you are an online entrepreneur and you want to make so much profits out from your internet business, this amazing WordPress plugin is a huge help to you.</li>
                        <li>What this software does is that, it will create SEO friendly money making WordPress pages almost every with a little content.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #5 End -->
      <!-- CTA Button Section Start -->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <!-- <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 red-clr">"AMITVIP"</span> for an Additional Discount on Commercial Licence
                  </div> -->
               </div>
               <div class="col-12 mt20">
                  <div class="f-18 f-md-22 lh140 w400 text-center mt10 white-clr">
                     Use Coupon Code <span class="w600 red-clr">“Quick3”</span> for An Additional <span class="w600 red-clr">$3 Discount</span>
                  </div>
                  </div>
                           </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt10">
                  
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab QuickFunnel + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <div class="f-18 f-md-22 lh140 w400 text-center mt10 white-clr">
                     Use Coupon Code <span class="w600 red-clr">“Quickbundle”</span> for An Additional <span class="w600 red-clr">$50 Discount</span>
                  </div>
                  <a href="<?php echo $_GET['afflinkbundle']; ?>" class="text-center bonusbuy-btn mt10">
                  <span class="text-center">Grab QuickFunnel Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                        <span class="f-14 f-md-18 w500 ">Days</span>
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                        <span class="f-14 f-md-18 w500 ">Hours</span>
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                        <span class="f-14 f-md-18 w500 ">Mins</span>
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                        <span class="f-14 f-md-18 w500 ">Sec</span>
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->
    <!-- Bonus #6 Start -->
    <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 6</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="assets/images/bonus6.webp" class="img-fluid mx-auto d-block " alt="Bonus6">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Affiliate Marketing Manager Software
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li> <b>12,590+ PLR Products Which You Can Resell And Keep 100% Of The Profits!</b></li> 
                           <li>We feature over 12,500 products with Private Label and Resale rights licenses, which mean that you can SELL, EDIT or even CLAIM the product inside as your own!</li>
                           <li>We've been helping THOUSANDS Of Webmasters & Marketers Start & Grow their own Internet Business... Today Is YOUR Turn!</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #6 End -->
      <!-- Bonus #7 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 7</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus7.webp" class="img-fluid mx-auto d-block " alt="Bonus7">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Software Profit Mastery
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li><b>Why You Should Get Into The Lucrative Software Business?!</b></li>
                           <li>If you are a serious entrepreneur, venturing other types of business model is mostly open to you. And the fact is that, considering the boom of online business model, tapping into the software business platform is also a good choice to start with.</li>
                           <li>The thing is that, if you are not familiar with the environment, inside this video series are the necessary information that you need for you to study the ins and outs of this new type of online business model.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #7 End -->
      <!-- Bonus #8 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 8</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus8.webp" class="img-fluid mx-auto d-block " alt="Bonus8">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        YT Rank Analyzer
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li><b>Discover How to Dominate YouTube And Build MASSIVE Targeted Lists For FREE... By Using Software To Do ALL the Dirty Work!</b></li> 
                           <li>YouTube is now the second largest search engine and is the thirds most visited website in the world. If you are not into YouTube Marketing, then you waste a huge opportunity to attract more traffic and leads to subscribe your list.</li> 
                           <li>The good news is that inside this product is an amazing tool that will help you do the technical stuff and make your campaign effort more productive and scalable.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #8 End -->
      <!-- Bonus #9 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 9</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus9.webp" class="img-fluid mx-auto d-block " alt="Bonus9">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Domains and Affiliate Marketing
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li><b>Learn More About Domains and Affiliate Marketing!</b></li>
                           <li>Most affiliate marketers are no stranger to the domain name market. At the very least, they probably have a domain name for their website or blog. And some have dozens of domains in their possession.</li>
                           <li>There are a few ways that domains can be used in affiliate marketing. Many affiliates create niche sites or blogs for the sole purpose of promoting affiliate programs. Some also purchase domains to redirect to their affiliate links. This makes the URL shorter, easier to remember and more appealing.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #9 End -->
      <!-- Bonus #10 start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 10</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus10.webp" class="img-fluid mx-auto d-block " alt="Bonus10">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Boost Your Website Traffic
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>Businesses both large and small are always hoping that their target audience will be able to find their site among the thousands of websites they are competing against. </li>
                           <li>One of the best ways to do this is to utilize the free and paid methods for boosting website traffic. </li>
                           <li>However, like so many online marketing methods, it isn’t always clear on how to do this. Finding an effective way to boost the traffic to your website can not only be confusing, but it can also be a bit frustrating.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #10 End -->
      <!-- CTA Button Section Start -->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <!-- <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 red-clr">"AMITVIP"</span> for an Additional Discount on Commercial Licence
                  </div> -->
               </div>
               <div class="col-12 mt20">
                  <div class="f-18 f-md-22 lh140 w400 text-center mt10 white-clr">
                     Use Coupon Code <span class="w600 red-clr">“Quick3”</span> for An Additional <span class="w600 red-clr">$3 Discount</span>
                  </div>
                  </div>
                           </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt10">
                  
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab QuickFunnel + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <div class="f-18 f-md-22 lh140 w400 text-center mt10 white-clr">
                     Use Coupon Code <span class="w600 red-clr">“Quickbundle”</span> for An Additional <span class="w600 red-clr">$50 Discount</span>
                  </div>
                  <a href="<?php echo $_GET['afflinkbundle']; ?>" class="text-center bonusbuy-btn mt10">
                  <span class="text-center">Grab QuickFunnel Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 smmltd">Days</span> </div>
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                     <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                     <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->
   <!-- Bonus #11 start -->
   <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 11</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus11.webp" class="img-fluid mx-auto d-block " alt="Bonus13">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Facebook Pixel Insert WP Plugin
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b> Facebook is one of great way to get traffic to your website and to your promotions. The problem is that a lot of people are using Facebook the wrong way.</b></li>   
                           <li>What there doing is they're running a general ad that are targeting audience such people in the US, people in New Zealand and other countries. And this results to lower conversion rate and high cost per result. So what you need to do in your ad is by using retargetting. </li>
                           <li>Add a Facebook tracking pixel to all of your WordPress site pages and posts in seconds and start taking advantage of retargeting to increase your sales and conversions. </li>
                           </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #11 End -->
      <!-- Bonus #12 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 12</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus12.webp" class="img-fluid mx-auto d-block " alt="Bonus12">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        The Social Media Traffic
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>Get Traffic From Social Media Platforms Using These Top 10 Tips!</li>
                           <li>Social media has the potential to become the largest source of online traffic for any company. That distinction is now claimed by search engines.</li>
                           <li>While search engines do have a wider footprint and record more activity than social media, the ability of companies to reach out to their target audience and in person is what makes social media special.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #12 End -->
      <!-- Bonus #13 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 13</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus13.webp" class="img-fluid mx-auto d-block " alt="Bonus13">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Social Media Boom Software
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>12,590+ PLR Products Which You Can Resell And Keep 100% Of The Profits!</li>
                           <li>#1 PLR membership Since 2008, 100k members and growing, Updated almost DAILY</li>
                           <li>We feature over 12,500 products with Private Label and Resale rights licenses, which mean that you can SELL, EDIT or even CLAIM the product inside as your own!</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #13 End -->
      <!-- Bonus #14 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 14</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus14.webp" class="img-fluid mx-auto d-block " alt="Bonus14">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Xyber Email Assistant Software
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                          <li> <b>Do Your Customer Support with Ease Using Xyber Email Assistant! </b> </li>
                          <li>If you are a current online business, customer support is necessary. This is because you can't be so sure that your business will work perfectly!</li>
                          <li>  The good news though is that, you can now turbo-charge the growth of your business, while freeing Up Your Time With Reduced Customer Support Hassles! Stay On Top Of Your Business With The Ability To Instantly Respond To Email For More Satisfied Customers, Affiliates, and Partners!</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #14 End -->
      <!-- Bonus #15 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 15</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus15.webp" class="img-fluid mx-auto d-block " alt="Bonus15">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        WP SEO Track Plugin
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                          <li><b> With this simple plugin you can get the true insight on your web traffic efforts in only seconds! </b>Watch as your social network shares increase, your google PageRank and more.</li> 
                          <li>Get a clear vision on what you need to start focusing on with your SEO efforts. Start more effective backlinks from Facebook, Twitter, Google and more.</li> 
                          <li>You will get all of the most important stats that you need to know for your SEO web traffic. Focus on the amount of shares you have on popular social sites like Facebook, Twitter, Stumble Upon and more.</li> 
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #15 End -->
      
      <!-- CTA Button Section Start -->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <!-- <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 red-clr">"AMITVIP"</span> for an Additional Discount on Commercial Licence
                  </div> -->
               </div>
               <div class="col-12 mt20">
                  <div class="f-18 f-md-22 lh140 w400 text-center mt10 white-clr">
                     Use Coupon Code <span class="w600 red-clr">“Quick3”</span> for An Additional <span class="w600 red-clr">$3 Discount</span>
                  </div>
                  </div>
                           </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt10">
                  
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab QuickFunnel + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <div class="f-18 f-md-22 lh140 w400 text-center mt10 white-clr">
                     Use Coupon Code <span class="w600 red-clr">“Quickbundle”</span> for An Additional <span class="w600 red-clr">$50 Discount</span>
                  </div>
                  <a href="<?php echo $_GET['afflinkbundle']; ?>" class="text-center bonusbuy-btn mt10">
                  <span class="text-center">Grab QuickFunnel Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 smmltd">Days</span> </div>
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                     <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                     <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->

      <!-- Bonus #16 start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 16</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus16.webp" class="img-fluid mx-auto d-block " alt="Bonus16">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Automated Traffic Bot
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>The Biggest Yahoo! Secret That No One Is Telling You & How To Snag Thousands Of Visitors Using This Simple Trick!</b></li>   
                           <li>Would you like to increase your search Engine Rankings without complicated SEO? If you are tired of all the complicated search engine information out there, you're far from alone!</li>
                           <li>In fact, most people just plain give up on SEO because it's so complicated. Worse yet, the majority of people who do follow through with SEO usually see little to no results after all that work! </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #16 End -->
      <!-- Bonus #17 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 17</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus17.webp" class="img-fluid mx-auto d-block " alt="Bonus17">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        WP BotBlocker Plugin
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>Stop your wordpress site from being attacked by hackers using bots to try and bring down your site! </b></li>
                           <li>Website hacking has been an issue for many WordPress users for several years now. That's why WordPress Developers are making security plugins to defend WordPress sites.</li>
                           <li>If you are having the same problem, chances are you might want to also install this amazing WordPress called WP BotBlocker with will block any bots that will attempt to do brute entry of your website's backend. </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #17 End -->
      <!-- Bonus #18 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 18</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus18.webp" class="img-fluid mx-auto d-block " alt="Bonus18">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Website Monitor
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li> <b>Automatically Know If Your Websites Go Down With Just 1 Click!</b></li>
                           <li>If you are having an online business, your website would be your best asset to market your services or products on the internet right?</li>
                           <li>The thing is that even if you are an I.T. or you have a person working for you as an I.T. for your online business, you still can't monitor your website 24/7 when it will go down.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #18 End -->
      <!-- Bonus #19 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 19</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus19.webp" class="img-fluid mx-auto d-block " alt="Bonus19">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        BIZ Landing Page Plugin
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                          <li> <b>You probably have noticed that most businesses online are listed in directories such as YellowPages only... Now with one wordpress plugin you can create an all-in-one website that will pull in multiple sources and display in one place.</b></li>
                          <li>This is a stand alone plugin that will create a business website in one landing page. Add tabbed content to keep your visitors staying on one page!</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #19 End -->
      <!-- Bonus #20 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 20</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus20.webp" class="img-fluid mx-auto d-block " alt="Bonus20">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Seo Revolution
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                          <li><b> Today, only the smartest search engine optimizers with latest SEO techniques and strategies and the best content writers reach the pedestal where they are the number one hit on the SERPs. </b></li> 
                          <li>If you are thinking about revamping your SEO strategy, here is an excellent opportunity to attract tons of website traffic. </li> 
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #20 End -->

      <!-- Huge Woth Section Start -->
      <div class="huge-area mt30 mt-md10">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="f-md-65 f-40 lh120 w700 white-clr">That's Huge Worth of</div>
                  <br>
                  <div class="f-md-60 f-40 lh120 w800 yellow-clr">$3300!</div>
               </div>
            </div>
         </div>
      </div>
      <!-- Huge Worth Section End -->
      <!-- text Area Start -->
      <div class="white-section pb0">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="f-md-36 f-25 lh140 w500">So what are you waiting for? You have a great opportunity <br class="hidden-xs"> ahead + <span class="w700 ">My 20 Bonus Products</span> are making it a <br class="hidden-xs"> <span class="w700 ">completely NO Brainer!!</span></div>
               </div>
            </div>
         </div>
      </div>
      <!-- text Area End -->
      <!-- CTA Button Section Start -->
      <div class="cta-btn-section">
         <div class="container">
            <div class="row">
            <div class="col-12 mt20">
                  <div class="f-18 f-md-22 lh140 w500 text-center mt10 black-clr">
                     Use Coupon Code <span class="w600 red-clr">“Quick3”</span> for An Additional <span class="w600 red-clr">$3 Discount</span>
                  </div>
                  </div>
                           </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt10">
                  
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab QuickFunnel + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <div class="f-18 f-md-22 lh140 w500 text-center mt10 black-clr">
                     Use Coupon Code <span class="w600 red-clr">“Quickbundle”</span> for An Additional <span class="w600 red-clr">$50 Discount</span>
                  </div>
                  <a href="<?php echo $_GET['afflinkbundle']; ?>" class="text-center bonusbuy-btn mt10">
                  <span class="text-center">Grab QuickFunnel Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-12 mt15 mt-md20" align="center">
                  <h3 class="f-md-28 f-20 w500 text-center black">My Exclusive Bonuses Are Expiring in...</h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 col-md-10 mx-auto col-12 text-center">
                  <div class="countdown counter-black">
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                     <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                     <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->
      <div class="footer-section" style="font-size: 14px;">
         <div class="container " style="font-size: 14px;">
            <div class="row" style="font-size: 14px;">
               <div class="col-12 text-center" style="font-size: 14px;">
               <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 693.1 109.7" style="enable-background:new 0 0 693.1 109.7; height:45px;" xml:space="preserve">
                        <style type="text/css">
                           .st0{fill:#FFFFFF;}
                           .st1{fill:#EF3E39;}
                           .st2{fill:#853594;}
                           .st3{fill:#EF3E3A;}
                        </style>
                        <g>
	                        <g id="Layer_1-2">
		                        <path class="st0" d="M140.4,0H18.9c-2.8-0.1-5.2,2.1-5.4,4.9s2.1,5.2,4.9,5.4c0.2,0,0.3,0,0.5,0h42.3c2.8,0,5.1,2.3,5.1,5.1
                              c0,2.8-2.3,5.1-5.1,5.1h-56c-2.8,0.1-5,2.5-4.9,5.4c0.1,2.7,2.3,4.8,4.9,4.9h35.6l0,0h13.5c2.8,0.1,5,2.5,4.9,5.4
                              c-0.1,2.7-2.3,4.8-4.9,4.9H46c-2.8,0-5.1,2.4-5.1,5.2c0,2.7,2.1,4.9,4.8,5.1h17.8c2.8,0,5.2,2.2,5.2,5.1c0,2.8-2.2,5.2-5.1,5.2h-3
                              c-1.3,0-2.6,0.6-3.5,1.5c-2,2-2,5.2-0.1,7.3c1,1,2.3,1.5,3.7,1.5h8v37.7L103.1,96V68.6l42.8-57.6c2.3-3,1.6-7.3-1.4-9.6
                              C143.3,0.5,141.9,0,140.4,0z M66.2,36c0-2.8,2.3-5.1,5.1-5.1h10c2.8,0.1,5,2.5,4.9,5.4c-0.1,2.7-2.3,4.8-4.9,4.9h-10
                              C68.5,41.1,66.2,38.8,66.2,36z"></path>
		                        <path class="st1" d="M34,46.3c0,2.8-2.3,5.1-5.1,5.1h-10c-2.8,0-5.1-2.3-5.1-5.1c0-2.8,2.3-5.1,5.1-5.1c0,0,0,0,0,0h10
			                     C31.7,41.2,34,43.5,34,46.3z"></path>
		                        <path class="st1" d="M48.8,66.9c0,2.8-2.3,5.1-5.1,5.1h-2.9c-2.8,0-5.1-2.1-5.3-4.8c-0.1-1.5,0.4-3,1.5-4c0.9-0.9,2.2-1.5,3.5-1.5
			                     h3.4C46.6,61.8,48.9,64.1,48.8,66.9z"></path>
                              <path class="st2" d="M45.8,61.7L45.8,61.7z"></path>
                              <path class="st2" d="M63.7,61.7L63.7,61.7z"></path>
                              <path class="st2" d="M63.7,61.7L63.7,61.7z"></path>
                              <path class="st3" d="M230,48.3c0,4.5-0.7,8.9-2.3,13.1c-1.5,3.9-3.7,7.5-6.5,10.5c-2.8,3-6.1,5.4-9.9,7c-8,3.3-17,3.3-24.9,0
                              c-3.7-1.6-7.1-4-9.8-7c-2.8-3.1-5-6.7-6.4-10.5c-1.6-4.2-2.4-8.7-2.3-13.1c0-4.5,0.7-9,2.3-13.2c1.4-3.9,3.6-7.5,6.4-10.5
                              c2.8-3,6.1-5.4,9.8-7c8-3.3,17-3.3,24.9,0c3.7,1.6,7.1,4,9.9,7c2.8,3.1,5,6.6,6.5,10.5C229.3,39.3,230,43.8,230,48.3z M222.1,48.3
                              c0.1-4.6-0.9-9.2-3-13.4c-1.9-3.7-4.7-6.9-8.2-9.1c-7.4-4.4-16.6-4.4-24,0c-3.5,2.3-6.3,5.4-8.2,9.1c-2.1,4.2-3.1,8.7-3,13.4
                              c-0.1,4.6,1,9.2,3,13.3c1.8,3.7,4.7,6.9,8.2,9.2c7.4,4.4,16.6,4.4,24,0c3.5-2.3,6.3-5.4,8.2-9.2C221.1,57.5,222.1,52.9,222.1,48.3
                              z M233.1,84.8c1.9,0.1,3.4,1.6,3.4,3.5c-0.1,1.3-0.7,2.5-1.7,3.4c-1.3,1.2-2.8,2.2-4.4,2.9c-1.8,0.9-3.7,1.5-5.7,2
                              c-1.8,0.5-3.7,0.7-5.6,0.7c-3.1,0.1-6.2-0.3-9.2-1.2c-2.1-0.7-4.2-1.5-6.2-2.6c-1.8-1-3.5-1.9-5.2-2.6c-1.9-0.8-3.9-1.2-6-1.2
                              c-1,0-2,0.2-2.9,0.6c-0.8,0.4-1.6,0.7-2.3,1.1c-0.8,0.4-1.7,0.6-2.6,0.6c-1,0-2-0.4-2.7-1.1c-0.6-0.7-1-1.6-1-2.5
                              c-0.1-1.4,0.8-2.8,2.1-3.2L200,79l8.4,0.8l-15.7,5.5l2.6-1.9c2.2,0,4.4,0.3,6.4,1.1c1.9,0.7,3.7,1.5,5.5,2.5
                              c1.8,0.9,3.6,1.8,5.5,2.5c2.1,0.8,4.4,1.1,6.6,1.1c1.8,0,3.7-0.3,5.4-1c1.2-0.5,2.4-1.2,3.6-1.9c0.8-0.6,1.7-1.3,2.4-2
                              C231.3,85.1,232.1,84.8,233.1,84.8L233.1,84.8z"></path>
		                        <path class="st3" d="M277.1,32.9c1,0,2,0.4,2.7,1.1c0.7,0.7,1,1.7,1,2.6v25.1c0,6.2-1.7,11-5.2,14.4s-8.3,5.1-14.5,5.1
                              c-6.2,0-11-1.7-14.4-5.1s-5.2-8.2-5.2-14.4V36.6c0-2,1.5-3.7,3.5-3.7c0,0,0.1,0,0.1,0c1,0,2,0.4,2.7,1.1c0.7,0.7,1,1.7,1,2.6v25.1
                              c0,4.2,1.1,7.3,3.2,9.5s5.2,3.2,9,3.2c4,0,7.1-1.1,9.2-3.2s3.2-5.3,3.2-9.5V36.6c0-2,1.5-3.7,3.5-3.7
                              C277,32.9,277,32.9,277.1,32.9L277.1,32.9z"></path>
		                        <path class="st3" d="M297.9,26c-1.2,0.1-2.4-0.2-3.4-0.9c-0.8-0.7-1.2-1.8-1.1-2.9v-1.3c-0.1-1.1,0.4-2.2,1.2-2.9
                              c1-0.7,2.2-1,3.4-0.9c1.2-0.1,2.4,0.2,3.3,0.9c0.8,0.7,1.2,1.8,1.1,2.9v1.3c0.1,1.1-0.3,2.1-1.1,2.9
                              C300.2,25.8,299.1,26.1,297.9,26z M301.5,76.8c0,1-0.4,2-1.1,2.7c-1.4,1.5-3.8,1.5-5.3,0c0,0,0,0,0,0c-0.7-0.7-1-1.7-1-2.7V35.9
                              c-0.1-2,1.5-3.8,3.5-3.9s3.8,1.5,3.9,3.5c0,0.1,0,0.3,0,0.4L301.5,76.8z"></path>
		                        <path class="st3" d="M336,31.7c2.9,0,5.7,0.3,8.5,1c2.2,0.5,4.2,1.5,6,2.8c1.3,0.9,2.2,2.4,2.2,4c0,0.8-0.3,1.6-0.8,2.3
                              c-0.5,0.7-1.3,1.1-2.2,1.1c-0.7,0-1.4-0.1-2.1-0.5c-0.5-0.3-1-0.7-1.5-1.2c-0.5-0.5-1.2-1-1.9-1.3c-1-0.5-2.1-0.8-3.2-1
                              c-1.4-0.2-2.8-0.4-4.2-0.4c-3.1-0.1-6.2,0.8-8.9,2.4c-2.5,1.6-4.6,3.8-6,6.5c-1.5,2.8-2.3,6-2.2,9.2c-0.1,3.2,0.7,6.4,2.1,9.2
                              c1.4,2.6,3.4,4.9,5.9,6.4c2.6,1.6,5.7,2.5,8.8,2.4c1.7,0,3.5-0.1,5.2-0.6c1.1-0.3,2.2-0.7,3.2-1.3c0.9-0.5,1.8-1.1,2.5-1.9
                              c1.4-1.3,3.5-1.2,4.8,0.1c0.6,0.7,0.9,1.6,0.9,2.5c0,1.1-0.8,2.3-2.3,3.5c-1.9,1.4-4,2.5-6.2,3.1c-2.9,0.9-5.9,1.3-8.9,1.3
                              c-4.3,0.1-8.6-1-12.3-3.3c-3.5-2.2-6.3-5.3-8.2-9c-2-3.9-3-8.3-2.9-12.7c-0.1-4.4,1-8.7,3-12.6C319.3,36.2,327.3,31.5,336,31.7z"></path>
		                        <path class="st3" d="M367.1,80.6c-1,0-2-0.4-2.7-1.1c-0.7-0.7-1-1.7-1-2.7V16c0-1,0.3-1.9,1-2.6c0.7-0.7,1.7-1.1,2.7-1.1
                              c2,0,3.7,1.5,3.7,3.5c0,0,0,0.1,0,0.1v60.8c0,1-0.4,2-1.1,2.7C369,80.2,368.1,80.6,367.1,80.6z M397.7,32.6c1,0,1.9,0.4,2.5,1.2
                              c1.3,1.2,1.4,3.2,0.2,4.6c-0.2,0.2-0.4,0.4-0.6,0.5L370,65l-0.3-8.6l25.5-22.7C395.8,33,396.7,32.6,397.7,32.6z M398.7,80.5
                              c-1,0-2-0.4-2.7-1.2l-19.7-20.9l5.5-5.1l19.5,20.8c0.7,0.7,1.1,1.7,1.1,2.8c0,1-0.4,2-1.3,2.6C400.4,80.1,399.6,80.4,398.7,80.5
                              L398.7,80.5z"></path>
		                        <path class="st0" d="M419,80.6c-1.6,0.1-3.1-0.5-4.2-1.6c-1.1-1.1-1.6-2.5-1.6-4V21.7c-0.1-3,2.3-5.6,5.3-5.7c0.1,0,0.2,0,0.3,0
                              h30c1.5,0,2.9,0.5,4,1.5c1.1,1,1.7,2.4,1.6,3.9c0,1.4-0.6,2.7-1.6,3.6c-1.1,1-2.5,1.6-4,1.6h-24.9l0.8-1.1v18.6l-0.6-1.2h20.6
                              c1.5,0,2.9,0.5,4,1.5c1.1,1,1.7,2.4,1.6,3.9c0,1.4-0.6,2.7-1.6,3.6c-1.1,1-2.5,1.6-4,1.6h-20.8l0.8-0.8V75c0,1.5-0.6,3-1.7,4
                              C421.9,80,420.5,80.6,419,80.6z"></path>
		                        <path class="st0" d="M499.3,31.3c1.5-0.1,3,0.5,4,1.6c1,1.1,1.6,2.5,1.6,4v23.4c0,6.5-1.8,11.7-5.4,15.5
                              c-3.6,3.8-8.9,5.7-15.7,5.7c-6.8,0-12-1.9-15.6-5.7s-5.4-9-5.4-15.5V36.9c0-1.5,0.5-2.9,1.6-4c2.2-2.2,5.7-2.2,7.9,0
                              c1,1.1,1.6,2.5,1.6,4v23.4c0,3.8,0.8,6.5,2.5,8.3s4.1,2.7,7.5,2.7s5.9-0.9,7.6-2.7s2.5-4.6,2.5-8.3V36.9c0-1.5,0.5-2.9,1.6-4
                              C496.4,31.8,497.8,31.2,499.3,31.3z"></path>
		                        <path class="st0" d="M543.2,30.3c4.6,0,8,1,10.4,2.9c2.4,1.9,4.1,4.6,4.9,7.6c0.9,3.4,1.4,7,1.3,10.6V75c0,1.5-0.5,2.9-1.6,4
                              c-2.2,2.2-5.7,2.2-7.9,0c-1-1.1-1.6-2.5-1.6-4V51.3c0-1.9-0.2-3.7-0.8-5.5c-0.5-1.6-1.5-2.9-2.8-3.9c-1.7-1.1-3.8-1.6-5.8-1.5
                              c-2.2-0.1-4.3,0.4-6.2,1.5c-1.6,0.9-3,2.3-3.9,3.9c-0.9,1.7-1.4,3.6-1.3,5.5V75c0,1.5-0.5,2.9-1.6,4c-2.2,2.2-5.7,2.2-7.9,0
                              c-1-1.1-1.6-2.5-1.6-4V36.9c0-1.5,0.5-2.9,1.6-4c2.2-2.2,5.7-2.2,7.9,0c1,1.1,1.6,2.5,1.6,4v4l-1.4-0.3c0.7-1.2,1.5-2.3,2.4-3.4
                              c1.1-1.3,2.3-2.4,3.7-3.4c1.5-1.1,3.1-1.9,4.8-2.5C539.2,30.7,541.2,30.3,543.2,30.3z"></path>
		                        <path class="st0" d="M598.1,30.3c4.5,0,8,1,10.4,2.9c2.4,2,4.1,4.6,4.9,7.6c0.9,3.4,1.4,7,1.3,10.6V75c0,1.5-0.5,2.9-1.6,4
                              c-2.2,2.2-5.7,2.2-7.9,0c-1-1.1-1.6-2.5-1.6-4V51.3c0-1.9-0.2-3.7-0.8-5.5c-0.5-1.6-1.5-2.9-2.8-3.9c-1.7-1.1-3.8-1.6-5.8-1.5
                              c-2.2-0.1-4.3,0.4-6.3,1.5c-1.6,0.9-3,2.3-3.9,3.9c-0.9,1.7-1.4,3.6-1.3,5.5V75c0,1.5-0.5,2.9-1.6,4c-2.2,2.2-5.7,2.2-7.9,0
                              c-1-1.1-1.6-2.5-1.6-4V36.9c0-1.5,0.5-2.9,1.6-4c2.2-2.2,5.7-2.2,7.9,0c1,1.1,1.6,2.5,1.6,4v4l-1.4-0.3c0.7-1.2,1.5-2.3,2.4-3.4
                              c1.1-1.3,2.3-2.4,3.7-3.4c1.5-1.1,3.1-1.9,4.8-2.5C594.2,30.7,596.1,30.3,598.1,30.3z"></path>
		                        <path class="st0" d="M649.9,81.5c-4.7,0.1-9.4-1-13.6-3.3c-3.7-2.1-6.8-5.2-8.8-8.9c-2.1-3.9-3.2-8.3-3.1-12.7
                              c-0.1-4.9,1-9.8,3.4-14.2c2.1-3.7,5.1-6.8,8.8-9c3.5-2,7.5-3.1,11.5-3.1c3.1,0,6.1,0.6,8.9,1.9c2.8,1.3,5.3,3.1,7.4,5.3
                              c2.2,2.3,3.9,4.9,5.1,7.8c1.3,3,1.9,6.2,1.9,9.4c0,1.4-0.7,2.7-1.8,3.6c-1.1,0.9-2.5,1.4-3.9,1.4h-35.2l-2.8-9.2h33.8l-2,1.8v-2.5
                              c-0.1-1.8-0.8-3.4-1.9-4.8c-1.1-1.4-2.6-2.6-4.2-3.4c-1.7-0.8-3.5-1.3-5.4-1.3c-1.8,0-3.5,0.2-5.2,0.7c-1.6,0.5-3,1.3-4.1,2.5
                              c-1.3,1.3-2.3,2.9-2.8,4.7c-0.7,2.4-1.1,4.9-1,7.5c-0.1,2.9,0.6,5.9,2.1,8.4c1.3,2.2,3.1,4,5.3,5.3c2.1,1.2,4.6,1.8,7,1.8
                              c1.8,0,3.7-0.1,5.4-0.6c1.1-0.3,2.3-0.7,3.3-1.3c0.8-0.5,1.6-1,2.3-1.3c1-0.5,2-0.8,3.1-0.8c2.6,0,4.7,2,4.7,4.6c0,0,0,0,0,0.1
                              c-0.1,1.8-1,3.5-2.6,4.5c-2,1.6-4.3,2.8-6.8,3.6C656,81,653,81.5,649.9,81.5z"></path>
		                        <path class="st0" d="M693.1,75c0.1,3.1-2.4,5.6-5.4,5.6c-0.1,0-0.1,0-0.2,0c-1.5,0-2.9-0.6-3.9-1.6c-1.1-1.1-1.6-2.5-1.6-4V18
			                     c-0.1-3.1,2.4-5.6,5.4-5.6c0.1,0,0.1,0,0.2,0c1.5-0.1,2.9,0.5,3.9,1.6c1,1.1,1.6,2.5,1.5,4V75z"></path>
	                        </g>
                        </g>
                     </svg>
                  <div class="f-16 f-md-18 w300 mt20 lh150 white-clr text-center" style="font-size: 16px;">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40" style="font-size: 14px;">
                  <div class="f-16 f-md-18 w300 lh150 white-clr text-xs-center" style="font-size: 16px;">Copyright © QuickFunnel 2023</div>
                  <ul class="footer-ul f-16 f-md-18 w300 white-clr text-center text-md-right">
                     <li style="font-size: 16px;"><a href="https://support.oppyo.com/hc/en-us" class="white-clr t-decoration-none" style="font-size: 16px;">Contact</a> <span class="px5" style="font-size: 16px;">|</span> </li>
                     <li style="font-size: 16px;"><a href="http://quickfunnel.live/legal/privacy-policy.html" class="white-clr t-decoration-none" style="font-size: 16px;">Privacy</a> <span class="px5" style="font-size: 16px;">|</span> </li>
                     <li style="font-size: 16px;"><a href="http://quickfunnel.live/legal/terms-of-service.html" class="white-clr t-decoration-none" style="font-size: 16px;">T&amp;C</a> <span class="px5" style="font-size: 16px;">|</span> </li>
                     <li style="font-size: 16px;"><a href="http://quickfunnel.live/legal/disclaimer.html" class="white-clr t-decoration-none" style="font-size: 16px;">Disclaimer</a> <span class="px5" style="font-size: 16px;">|</span> </li>
                     <li style="font-size: 16px;"><a href="http://quickfunnel.live/legal/gdpr.html" class="white-clr t-decoration-none" style="font-size: 16px;">GDPR</a> <span class="px5" style="font-size: 16px;">|</span> </li>
                     <li style="font-size: 16px;"><a href="http://quickfunnel.live/legal/dmca.html" class="white-clr t-decoration-none" style="font-size: 16px;">DMCA</a> <span class="px5" style="font-size: 16px;">|</span> </li>
                     <li style="font-size: 16px;"><a href="http://quickfunnel.live/legal/anti-spam.html" class="white-clr t-decoration-none" style="font-size: 16px;">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!-- timer --->
      <?php
         if ($now < $exp_date) {
         ?>
      <script type="text/javascript">
         // Count down milliseconds = server_end - server_now = client_end - client_now
         var server_end = <?php echo $exp_date; ?> * 1000;
         var server_now = <?php echo time(); ?> * 1000;
         var client_now = new Date().getTime();
         var end = server_end - server_now + client_now; // this is the real end time
         
         var noob = $('.countdown').length;
         
         var _second = 1000;
         var _minute = _second * 60;
         var _hour = _minute * 60;
         var _day = _hour * 24
         var timer;
         
         function showRemaining() {
         var now = new Date();
         var distance = end - now;
         if (distance < 0) {
         	clearInterval(timer);
         	document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
         	return;
         }
         
         var days = Math.floor(distance / _day);
         var hours = Math.floor((distance % _day) / _hour);
         var minutes = Math.floor((distance % _hour) / _minute);
         var seconds = Math.floor((distance % _minute) / _second);
         if (days < 10) {
         	days = "0" + days;
         }
         if (hours < 10) {
         	hours = "0" + hours;
         }
         if (minutes < 10) {
         	minutes = "0" + minutes;
         }
         if (seconds < 10) {
         	seconds = "0" + seconds;
         }
         var i;
         var countdown = document.getElementsByClassName('countdown');
         for (i = 0; i < noob; i++) {
         	countdown[i].innerHTML = '';
         
         	if (days) {
         		countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + days + '</span><span class="f-14 f-md-18 smmltd">Days</span> </div>';
         	}
         
         	countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + hours + '</span><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>';
         
         	countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">' + minutes + '</span><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>';
         
         	countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">' + seconds + '</span><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>';
         }
         
         }
         timer = setInterval(showRemaining, 1000);
         	
      </script>
      <?php
         } else {
         echo "Times Up";
         }
         ?>
      <!--- timer end-->
   </body>
</html>
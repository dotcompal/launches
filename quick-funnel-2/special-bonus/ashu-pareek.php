<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <!-- Tell the browser to be responsive to screen width -->
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
      <meta name="title" content="QuickFunnel Bonuses">
      <meta name="description" content="QuickFunnel Bonuses">
      <meta name="keywords" content="Have A Sneak Peak of A Breakthrough Technology That Analyzes, Detects & Fix Deadly Legal Flaws on Your Website Just by Copy Pasting a Single Line of Code">
      <meta property="og:image" content="https://www.quickfunnel.live/special-bonus/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Ashu Kumar">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="QuickFunnel Bonuses">
      <meta property="og:description" content="Have A Sneak Peak of A Breakthrough Technology That Analyzes, Detects & Fix Deadly Legal Flaws on Your Website Just by Copy Pasting a Single Line of Code">
      <meta property="og:image" content="https://www.quickfunnel.live/special-bonus/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="QuickFunnel Bonuses">
      <meta property="twitter:description" content="Have A Sneak Peak of A Breakthrough Technology That Analyzes, Detects & Fix Deadly Legal Flaws on Your Website Just by Copy Pasting a Single Line of Code">
      <meta property="twitter:image" content="https://www.quickfunnel.live/special-bonus/thumbnail.png">
      <title>QuickFunnel Bonuses</title>
      <!-- Shortcut Icon  -->
      <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
      <!-- Css CDN Load Link -->
      <link rel="stylesheet" type="text/css" href="assets/css/bonus-style.css">
      <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&family=Crimson+Text:ital,wght@0,400;0,600;0,700;1,400;1,600;1,700&family=Great+Vibes&family=League+Spartan:wght@200;400;500;600;700;800;900&family=Merriweather:ital,wght@0,300;0,400;0,700;0,900;1,300;1,400;1,700;1,900&family=Niconne&family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
      <!-- required -->
      <!-- Start Editor required -->
      <link rel="stylesheet" href="https://cdn.oppyo.com/launches/yoseller/common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style.css" type="text/css">
      <script src="https://cdn.oppyo.com/launches/writerarc/common_assets/js/jquery.min.js"></script>
   
   </head>
   <body>
      <!-- New Timer  Start-->
      <?php
         $date = 'Sept 26 2023 11:00 PM EST';
         $exp_date = strtotime($date);
         $now = time();  
         /*
         
         $date = date('F d Y g:i:s A eO');
         $rand_time_add = rand(700, 1200);
         $exp_date = strtotime($date) + $rand_time_add;
         $now = time();*/
         
         if ($now < $exp_date) {
         ?>
         <?php
         } else {
         	echo "Times Up";
         }
         ?>
      <!-- New Timer End -->
      <?php
         if(!isset($_GET['afflink'])){
         $_GET['afflink'] = 'https://jvz6.com/c/47069/392375/';
         $_GET['name'] = 'Ashu Kumar';      
         }
         ?>
         
     <!-- Header Section Start -->   
     <div class="header-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                  viewBox="0 0 693.1 109.7" style="enable-background:new 0 0 693.1 109.7; height:45px;" xml:space="preserve">
                     <style type="text/css">
                        .st0{fill:#FFFFFF;}
                        .st1{fill:#EF3E39;}
                        .st2{fill:#853594;}
                        .st3{fill:#EF3E3A;}
                     </style>
                     <g>
                        <g id="Layer_1-2">
                           <path class="st0" d="M140.4,0H18.9c-2.8-0.1-5.2,2.1-5.4,4.9s2.1,5.2,4.9,5.4c0.2,0,0.3,0,0.5,0h42.3c2.8,0,5.1,2.3,5.1,5.1
                           c0,2.8-2.3,5.1-5.1,5.1h-56c-2.8,0.1-5,2.5-4.9,5.4c0.1,2.7,2.3,4.8,4.9,4.9h35.6l0,0h13.5c2.8,0.1,5,2.5,4.9,5.4
                           c-0.1,2.7-2.3,4.8-4.9,4.9H46c-2.8,0-5.1,2.4-5.1,5.2c0,2.7,2.1,4.9,4.8,5.1h17.8c2.8,0,5.2,2.2,5.2,5.1c0,2.8-2.2,5.2-5.1,5.2h-3
                           c-1.3,0-2.6,0.6-3.5,1.5c-2,2-2,5.2-0.1,7.3c1,1,2.3,1.5,3.7,1.5h8v37.7L103.1,96V68.6l42.8-57.6c2.3-3,1.6-7.3-1.4-9.6
                           C143.3,0.5,141.9,0,140.4,0z M66.2,36c0-2.8,2.3-5.1,5.1-5.1h10c2.8,0.1,5,2.5,4.9,5.4c-0.1,2.7-2.3,4.8-4.9,4.9h-10
                           C68.5,41.1,66.2,38.8,66.2,36z"/>
                           <path class="st1" d="M34,46.3c0,2.8-2.3,5.1-5.1,5.1h-10c-2.8,0-5.1-2.3-5.1-5.1c0-2.8,2.3-5.1,5.1-5.1c0,0,0,0,0,0h10
                           C31.7,41.2,34,43.5,34,46.3z"/>
                           <path class="st1" d="M48.8,66.9c0,2.8-2.3,5.1-5.1,5.1h-2.9c-2.8,0-5.1-2.1-5.3-4.8c-0.1-1.5,0.4-3,1.5-4c0.9-0.9,2.2-1.5,3.5-1.5
                           h3.4C46.6,61.8,48.9,64.1,48.8,66.9z"/>
                           <path class="st2" d="M45.8,61.7L45.8,61.7z"/>
                           <path class="st2" d="M63.7,61.7L63.7,61.7z"/>
                           <path class="st2" d="M63.7,61.7L63.7,61.7z"/>
                           <path class="st3" d="M230,48.3c0,4.5-0.7,8.9-2.3,13.1c-1.5,3.9-3.7,7.5-6.5,10.5c-2.8,3-6.1,5.4-9.9,7c-8,3.3-17,3.3-24.9,0
                           c-3.7-1.6-7.1-4-9.8-7c-2.8-3.1-5-6.7-6.4-10.5c-1.6-4.2-2.4-8.7-2.3-13.1c0-4.5,0.7-9,2.3-13.2c1.4-3.9,3.6-7.5,6.4-10.5
                           c2.8-3,6.1-5.4,9.8-7c8-3.3,17-3.3,24.9,0c3.7,1.6,7.1,4,9.9,7c2.8,3.1,5,6.6,6.5,10.5C229.3,39.3,230,43.8,230,48.3z M222.1,48.3
                           c0.1-4.6-0.9-9.2-3-13.4c-1.9-3.7-4.7-6.9-8.2-9.1c-7.4-4.4-16.6-4.4-24,0c-3.5,2.3-6.3,5.4-8.2,9.1c-2.1,4.2-3.1,8.7-3,13.4
                           c-0.1,4.6,1,9.2,3,13.3c1.8,3.7,4.7,6.9,8.2,9.2c7.4,4.4,16.6,4.4,24,0c3.5-2.3,6.3-5.4,8.2-9.2C221.1,57.5,222.1,52.9,222.1,48.3
                           z M233.1,84.8c1.9,0.1,3.4,1.6,3.4,3.5c-0.1,1.3-0.7,2.5-1.7,3.4c-1.3,1.2-2.8,2.2-4.4,2.9c-1.8,0.9-3.7,1.5-5.7,2
                           c-1.8,0.5-3.7,0.7-5.6,0.7c-3.1,0.1-6.2-0.3-9.2-1.2c-2.1-0.7-4.2-1.5-6.2-2.6c-1.8-1-3.5-1.9-5.2-2.6c-1.9-0.8-3.9-1.2-6-1.2
                           c-1,0-2,0.2-2.9,0.6c-0.8,0.4-1.6,0.7-2.3,1.1c-0.8,0.4-1.7,0.6-2.6,0.6c-1,0-2-0.4-2.7-1.1c-0.6-0.7-1-1.6-1-2.5
                           c-0.1-1.4,0.8-2.8,2.1-3.2L200,79l8.4,0.8l-15.7,5.5l2.6-1.9c2.2,0,4.4,0.3,6.4,1.1c1.9,0.7,3.7,1.5,5.5,2.5
                           c1.8,0.9,3.6,1.8,5.5,2.5c2.1,0.8,4.4,1.1,6.6,1.1c1.8,0,3.7-0.3,5.4-1c1.2-0.5,2.4-1.2,3.6-1.9c0.8-0.6,1.7-1.3,2.4-2
                           C231.3,85.1,232.1,84.8,233.1,84.8L233.1,84.8z"/>
                           <path class="st3" d="M277.1,32.9c1,0,2,0.4,2.7,1.1c0.7,0.7,1,1.7,1,2.6v25.1c0,6.2-1.7,11-5.2,14.4s-8.3,5.1-14.5,5.1
                           c-6.2,0-11-1.7-14.4-5.1s-5.2-8.2-5.2-14.4V36.6c0-2,1.5-3.7,3.5-3.7c0,0,0.1,0,0.1,0c1,0,2,0.4,2.7,1.1c0.7,0.7,1,1.7,1,2.6v25.1
                           c0,4.2,1.1,7.3,3.2,9.5s5.2,3.2,9,3.2c4,0,7.1-1.1,9.2-3.2s3.2-5.3,3.2-9.5V36.6c0-2,1.5-3.7,3.5-3.7
                           C277,32.9,277,32.9,277.1,32.9L277.1,32.9z"/>
                           <path class="st3" d="M297.9,26c-1.2,0.1-2.4-0.2-3.4-0.9c-0.8-0.7-1.2-1.8-1.1-2.9v-1.3c-0.1-1.1,0.4-2.2,1.2-2.9
                           c1-0.7,2.2-1,3.4-0.9c1.2-0.1,2.4,0.2,3.3,0.9c0.8,0.7,1.2,1.8,1.1,2.9v1.3c0.1,1.1-0.3,2.1-1.1,2.9
                           C300.2,25.8,299.1,26.1,297.9,26z M301.5,76.8c0,1-0.4,2-1.1,2.7c-1.4,1.5-3.8,1.5-5.3,0c0,0,0,0,0,0c-0.7-0.7-1-1.7-1-2.7V35.9
                           c-0.1-2,1.5-3.8,3.5-3.9s3.8,1.5,3.9,3.5c0,0.1,0,0.3,0,0.4L301.5,76.8z"/>
                           <path class="st3" d="M336,31.7c2.9,0,5.7,0.3,8.5,1c2.2,0.5,4.2,1.5,6,2.8c1.3,0.9,2.2,2.4,2.2,4c0,0.8-0.3,1.6-0.8,2.3
                           c-0.5,0.7-1.3,1.1-2.2,1.1c-0.7,0-1.4-0.1-2.1-0.5c-0.5-0.3-1-0.7-1.5-1.2c-0.5-0.5-1.2-1-1.9-1.3c-1-0.5-2.1-0.8-3.2-1
                           c-1.4-0.2-2.8-0.4-4.2-0.4c-3.1-0.1-6.2,0.8-8.9,2.4c-2.5,1.6-4.6,3.8-6,6.5c-1.5,2.8-2.3,6-2.2,9.2c-0.1,3.2,0.7,6.4,2.1,9.2
                           c1.4,2.6,3.4,4.9,5.9,6.4c2.6,1.6,5.7,2.5,8.8,2.4c1.7,0,3.5-0.1,5.2-0.6c1.1-0.3,2.2-0.7,3.2-1.3c0.9-0.5,1.8-1.1,2.5-1.9
                           c1.4-1.3,3.5-1.2,4.8,0.1c0.6,0.7,0.9,1.6,0.9,2.5c0,1.1-0.8,2.3-2.3,3.5c-1.9,1.4-4,2.5-6.2,3.1c-2.9,0.9-5.9,1.3-8.9,1.3
                           c-4.3,0.1-8.6-1-12.3-3.3c-3.5-2.2-6.3-5.3-8.2-9c-2-3.9-3-8.3-2.9-12.7c-0.1-4.4,1-8.7,3-12.6C319.3,36.2,327.3,31.5,336,31.7z"
                           />
                           <path class="st3" d="M367.1,80.6c-1,0-2-0.4-2.7-1.1c-0.7-0.7-1-1.7-1-2.7V16c0-1,0.3-1.9,1-2.6c0.7-0.7,1.7-1.1,2.7-1.1
                           c2,0,3.7,1.5,3.7,3.5c0,0,0,0.1,0,0.1v60.8c0,1-0.4,2-1.1,2.7C369,80.2,368.1,80.6,367.1,80.6z M397.7,32.6c1,0,1.9,0.4,2.5,1.2
                           c1.3,1.2,1.4,3.2,0.2,4.6c-0.2,0.2-0.4,0.4-0.6,0.5L370,65l-0.3-8.6l25.5-22.7C395.8,33,396.7,32.6,397.7,32.6z M398.7,80.5
                           c-1,0-2-0.4-2.7-1.2l-19.7-20.9l5.5-5.1l19.5,20.8c0.7,0.7,1.1,1.7,1.1,2.8c0,1-0.4,2-1.3,2.6C400.4,80.1,399.6,80.4,398.7,80.5
                           L398.7,80.5z"/>
                           <path class="st0" d="M419,80.6c-1.6,0.1-3.1-0.5-4.2-1.6c-1.1-1.1-1.6-2.5-1.6-4V21.7c-0.1-3,2.3-5.6,5.3-5.7c0.1,0,0.2,0,0.3,0
                           h30c1.5,0,2.9,0.5,4,1.5c1.1,1,1.7,2.4,1.6,3.9c0,1.4-0.6,2.7-1.6,3.6c-1.1,1-2.5,1.6-4,1.6h-24.9l0.8-1.1v18.6l-0.6-1.2h20.6
                           c1.5,0,2.9,0.5,4,1.5c1.1,1,1.7,2.4,1.6,3.9c0,1.4-0.6,2.7-1.6,3.6c-1.1,1-2.5,1.6-4,1.6h-20.8l0.8-0.8V75c0,1.5-0.6,3-1.7,4
                           C421.9,80,420.5,80.6,419,80.6z"/>
                           <path class="st0" d="M499.3,31.3c1.5-0.1,3,0.5,4,1.6c1,1.1,1.6,2.5,1.6,4v23.4c0,6.5-1.8,11.7-5.4,15.5
                           c-3.6,3.8-8.9,5.7-15.7,5.7c-6.8,0-12-1.9-15.6-5.7s-5.4-9-5.4-15.5V36.9c0-1.5,0.5-2.9,1.6-4c2.2-2.2,5.7-2.2,7.9,0
                           c1,1.1,1.6,2.5,1.6,4v23.4c0,3.8,0.8,6.5,2.5,8.3s4.1,2.7,7.5,2.7s5.9-0.9,7.6-2.7s2.5-4.6,2.5-8.3V36.9c0-1.5,0.5-2.9,1.6-4
                           C496.4,31.8,497.8,31.2,499.3,31.3z"/>
                           <path class="st0" d="M543.2,30.3c4.6,0,8,1,10.4,2.9c2.4,1.9,4.1,4.6,4.9,7.6c0.9,3.4,1.4,7,1.3,10.6V75c0,1.5-0.5,2.9-1.6,4
                           c-2.2,2.2-5.7,2.2-7.9,0c-1-1.1-1.6-2.5-1.6-4V51.3c0-1.9-0.2-3.7-0.8-5.5c-0.5-1.6-1.5-2.9-2.8-3.9c-1.7-1.1-3.8-1.6-5.8-1.5
                           c-2.2-0.1-4.3,0.4-6.2,1.5c-1.6,0.9-3,2.3-3.9,3.9c-0.9,1.7-1.4,3.6-1.3,5.5V75c0,1.5-0.5,2.9-1.6,4c-2.2,2.2-5.7,2.2-7.9,0
                           c-1-1.1-1.6-2.5-1.6-4V36.9c0-1.5,0.5-2.9,1.6-4c2.2-2.2,5.7-2.2,7.9,0c1,1.1,1.6,2.5,1.6,4v4l-1.4-0.3c0.7-1.2,1.5-2.3,2.4-3.4
                           c1.1-1.3,2.3-2.4,3.7-3.4c1.5-1.1,3.1-1.9,4.8-2.5C539.2,30.7,541.2,30.3,543.2,30.3z"/>
                           <path class="st0" d="M598.1,30.3c4.5,0,8,1,10.4,2.9c2.4,2,4.1,4.6,4.9,7.6c0.9,3.4,1.4,7,1.3,10.6V75c0,1.5-0.5,2.9-1.6,4
                           c-2.2,2.2-5.7,2.2-7.9,0c-1-1.1-1.6-2.5-1.6-4V51.3c0-1.9-0.2-3.7-0.8-5.5c-0.5-1.6-1.5-2.9-2.8-3.9c-1.7-1.1-3.8-1.6-5.8-1.5
                           c-2.2-0.1-4.3,0.4-6.3,1.5c-1.6,0.9-3,2.3-3.9,3.9c-0.9,1.7-1.4,3.6-1.3,5.5V75c0,1.5-0.5,2.9-1.6,4c-2.2,2.2-5.7,2.2-7.9,0
                           c-1-1.1-1.6-2.5-1.6-4V36.9c0-1.5,0.5-2.9,1.6-4c2.2-2.2,5.7-2.2,7.9,0c1,1.1,1.6,2.5,1.6,4v4l-1.4-0.3c0.7-1.2,1.5-2.3,2.4-3.4
                           c1.1-1.3,2.3-2.4,3.7-3.4c1.5-1.1,3.1-1.9,4.8-2.5C594.2,30.7,596.1,30.3,598.1,30.3z"/>
                           <path class="st0" d="M649.9,81.5c-4.7,0.1-9.4-1-13.6-3.3c-3.7-2.1-6.8-5.2-8.8-8.9c-2.1-3.9-3.2-8.3-3.1-12.7
                           c-0.1-4.9,1-9.8,3.4-14.2c2.1-3.7,5.1-6.8,8.8-9c3.5-2,7.5-3.1,11.5-3.1c3.1,0,6.1,0.6,8.9,1.9c2.8,1.3,5.3,3.1,7.4,5.3
                           c2.2,2.3,3.9,4.9,5.1,7.8c1.3,3,1.9,6.2,1.9,9.4c0,1.4-0.7,2.7-1.8,3.6c-1.1,0.9-2.5,1.4-3.9,1.4h-35.2l-2.8-9.2h33.8l-2,1.8v-2.5
                           c-0.1-1.8-0.8-3.4-1.9-4.8c-1.1-1.4-2.6-2.6-4.2-3.4c-1.7-0.8-3.5-1.3-5.4-1.3c-1.8,0-3.5,0.2-5.2,0.7c-1.6,0.5-3,1.3-4.1,2.5
                           c-1.3,1.3-2.3,2.9-2.8,4.7c-0.7,2.4-1.1,4.9-1,7.5c-0.1,2.9,0.6,5.9,2.1,8.4c1.3,2.2,3.1,4,5.3,5.3c2.1,1.2,4.6,1.8,7,1.8
                           c1.8,0,3.7-0.1,5.4-0.6c1.1-0.3,2.3-0.7,3.3-1.3c0.8-0.5,1.6-1,2.3-1.3c1-0.5,2-0.8,3.1-0.8c2.6,0,4.7,2,4.7,4.6c0,0,0,0,0,0.1
                           c-0.1,1.8-1,3.5-2.6,4.5c-2,1.6-4.3,2.8-6.8,3.6C656,81,653,81.5,649.9,81.5z"/>
                           <path class="st0" d="M693.1,75c0.1,3.1-2.4,5.6-5.4,5.6c-0.1,0-0.1,0-0.2,0c-1.5,0-2.9-0.6-3.9-1.6c-1.1-1.1-1.6-2.5-1.6-4V18
                           c-0.1-3.1,2.4-5.6,5.4-5.6c0.1,0,0.1,0,0.2,0c1.5-0.1,2.9,0.5,3.9,1.6c1,1.1,1.6,2.5,1.5,4V75z"/>
                        </g>
                     </g>
                  </svg>
               </div>
               <div class="col-12 mt20">
                  <div class="row">
                  <div class="col-md-12 col-12 text-center mt30">
                  <div class="text-center">
                     <div class="f-md-32 f-20 lh140 w400 white-clr d-block d-md-flex align-items-center justify-content-center">
                        <span class="w500 red-clr"><?php echo $_GET['name'];?>'s</span> &nbsp;special bonus for QuickFunnel
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-12 text-center mt20">
                        <div class="f-16 f-md-20 w400 black-clr lh140 text-capitalize preheadline relative">
                        Grab QuickFunnels With Great Grand Bonuses Of The Year 2023, Before The Deal Ends...
                           <img src="assets/images/pre-heading-icon.webp" class="img-fluid d-none d-md-block gameimg">
                        </div>
                     </div>
                     
                     <div class="col-12 mt-md50 mt20 f-md-42 f-24 w600 text-center white-clr lh140"> 
                     Have A Sneak Peak -  <span class="red-clr"> How You Can Launch Blazing-Fast Loading Funnels And Websites In Just 7 Minutes </span>  With No Tech Hassles & No Monthly Fee Ever...
                     </div>
                     <div class="col-12 mt-md30 mt20">
                        <img src="assets/images/hr.webp" alt="hr" class="mx-auto img-fluid d-block hr">
                     </div>
                     <div class="col-12 mt20 mt-md30 text-center">
                        <div class="f-18 f-md-20 w400 text-center lh140 white-clr text-capitalize">               
                        Introducing The Industry’s First Visual Funnel Planner, 400+ Beautiful Templates, Free-Flow Editor, And <br class="d-none d-md-block">Inbuilt E-Mail Marketing - All Rolled Into One Easy To Use Application!
                        </div>
                     </div>
                     <div class="col-12 col-md-10 mx-auto mt20 mt-md40">
                     <div class="video-box">
                        <div style="padding-bottom: 56.25%;position: relative;">
                           <iframe src="https://quickfunnel.dotcompal.com/video/embed/occtysjaap" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
                           </div>
      
  <!-- Step Section Start -->
  <div class="step-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-md-45 f-28 w700 lh140 text-center black-clr">
                     Create <span class="red-clr"> Amazing Profit Pulling Websites &amp; Funnels</span> in 3 Easy Steps
                     <img src="assets/images/line.webp" alt="Line" class="mx-auto d-block img-fluid mt5">
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md90">
               <div class="col-12 col-md-4">
                  <div class="steps-block">
                     <img src="assets/images/step1.webp" class="img-fluid mx-auto d-block" alt="Upload">
                     <div class="f-24 f-md-32 w700 black-clr lh150 mt20 mt-md30 text-center">
                        Choose
                     </div>
                     <div class="f-18 w400 black-clr lh150 mt15 text-center">
                        To start, just choose from 400+ ready-to-use, beautiful templates according to your business niche &amp; marketing goals.
                     </div>
                 </div>
               </div>
               <div class="col-12 col-md-4 mt20 mt-md0">
                  <div class="steps-block">
                     <img src="assets/images/step2.webp" class="img-fluid mx-auto d-block" alt="Upload">
                     <div class="f-24 f-md-32 w700 black-clr lh150 mt20 mt-md30 text-center">
                        Edit
                     </div>
                     <div class="f-18 w400 black-clr lh150 mt15 text-center">
                        Revolutionize your funnel creation with our next-generation Drag &amp; Drop page editor and free-flow funnel planner! Zero coding or design skills needed.
                     </div>
                 </div>
               </div>
               <div class="col-12 col-md-4 mt20 mt-md0">
                  <div class="steps-block">
                     <img src="assets/images/step3.webp" class="img-fluid mx-auto d-block" alt="Upload">
                     <div class="f-24 f-md-32 w700 black-clr lh150 mt20 mt-md30 text-center">
                        Publish &amp; Get Paid
                     </div>
                     <div class="f-18 w400 black-clr lh150 mt15 text-center">
                        Now publish your funnel &amp; website that works 24*7 for you &amp; bring automated sales.
                     </div>
                 </div>
               </div>
            </div>
            <div class="row mt20 mt-md90">
               <div class="col-12 f-18 f-md-20 w400 lh140 black-clr text-center">
                  It Just Takes <span class="w600">Minutes</span> to go live… <span class="w600"> No Technical &amp; Designing Skills</span> of Any Kind is Needed!
               </div>
               <div class="col-12 f-md-24 f-20 w400 lh140 black-clr text-center mt15">
                  Plus, with included FREE commercial license, <span class="w600">this is the easiest &amp; fastest way</span> to start 6 figure business and help desperate local businesses in no time!
               </div>
            </div>
         </div>
      </div>
      <!-- Step Section End -->
      <!-- CTA Btn Start-->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w600">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w600 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <!-- <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w600 red-clr">"AtulVIP"</span> for an Additional Discount on Commercial Licence
                  </div> -->
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                  Use Coupon Code <span class="w600 red-clr">"ASHUSPECIAL"</span> for an Additional <span class="w600 red-clr">$3 Discount</span> on FE Deal
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab QuickFunnel + My 20 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                  Use Coupon Code <span class="w600 red-clr">"ASHUBUNDLE"</span> for an Additional <span class="w600 red-clr">$50 Discount</span> on Bundle Deal
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="https://jvz2.com/c/47069/392382/" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab QuickFunnel Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 smmltd">Days</span>
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Sec</span>
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Btn End -->
      <div class="second-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-28 f-md-45 lh140 w400 text-center black-clr">
                     We Guarantee, <span class="w700 red-clr">This Is The Only Funnel &amp; <br class="d-none d-md-block"> Website Builder You’ll Ever Need…</span> 
                  </div>
               </div>
            </div>
            <div class="row mt0 mt-md50">   
               <div class="col-12 col-md-6">
                  <div class="feature-list l-feature-odd">
                     <div class="f-18 f-md-20 lh140 w400">
                       <span class="w600">Create Proven-To-Convert Funnels &amp; Websites</span>  In Just A Few Minutes In Any Niche
                     </div>
                  </div>
                  <div class="feature-list l-feature-even">
                     <div class="f-18 f-md-20 lh140 w400">
                       <span class="w600">Build Any Type Of Funnel– </span> Sales Funnel, Lead Funnel, Webinar And Product Launches Funnel
                     </div>
                  </div>
                  <div class="feature-list l-feature-odd">
                     <div class="f-18 f-md-20 lh140 w400">
                        <span class="w600">400+ High Converting &amp; Ready-To-Go Templates</span>
                     </div>
                  </div>
                  <div class="feature-list l-feature-even">
                     <div class="f-18 f-md-20 lh140 w400">
                       <span class="w600">Mobile Friendly &amp; Fast Loading Pages-</span> Never Lose A Single Visitor, Lead Or Sale.
                     </div>
                  </div>
                  <div class="feature-list l-feature-odd">
                     <div class="f-18 f-md-20 lh140 w400">
                       <span class="w600">Inbuilt Email Marketing &amp; Lead Management–</span>  Collect Unlimited Leads and Send Unlimited Emails
                     </div>
                  </div>
                  <div class="feature-list l-feature-even">
                     <div class="f-18 f-md-20 lh140 w400">
                       <span class="w600">SEO Friendly &amp; Social Media Optimized Pages</span>  For More Traffic.
                     </div>
                  </div>
                  <div class="feature-list l-feature-odd">
                     <div class="f-18 f-md-20 lh140 w400">
                       <span class="w600">Precise Analytics </span> To Measure &amp; Make The Right Decisions For Future Success!
                     </div>
                  </div>
                  <div class="feature-list l-feature-even">
                     <div class="f-18 f-md-20 lh140 w400">
                       <span class="w600">Automatic SSL Encryption-</span> 100% Unbreakable Security
                     </div>
                  </div>
                  <div class="feature-list l-feature-odd">
                     <div class="f-18 f-md-20 lh140 w600">
                        PLUS, YOU’LL ALSO RECEIVE FREE COMMERCIAL LICENCE WHEN YOU GET STARTED TODAY!
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-6">
                  <div class="f-18 lh140 w400">                             
                     <div class="feature-list r-feature-odd">
                        <div class="f-18 f-md-20 lh140 w600">
                           Industry’s First, Fully Drag &amp; Drop &amp; Visual Funnel Designer
                        </div>
                     </div>
                     <div class="feature-list r-feature-even">
                        <div class="f-18 f-md-20 lh140 w400">
                          <span class="w600">Create Any Type Of Landing Page -</span> Sales Page, Lead Page, Membership, Bonus, or Product Review Page
                        </div>
                     </div>
                     <div class="feature-list r-feature-odd">
                        <div class="f-18 f-md-20 lh140 w400">
                           <span class="w600">Next-Gen Drag And Drop Editor</span> To Create Pixel Perfect Pages Or Templates From Scratch 
                        </div>
                     </div>
                     <div class="feature-list r-feature-even">
                        <div class="f-18 f-md-20 lh140 w400">
                           User Friendly Business Central Dashboard- <span class="w600">Everything You Need, Under One Roof At Your Fingertips!</span> 
                        </div>
                     </div>
                     <div class="feature-list r-feature-odd">
                        <div class="f-18 f-md-20 lh140 w400">
                          <span class="w600">Cutting-Edge Autoresponder Integration-</span> Use Any Email Autoresponder Without Any Technical Glitches
                        </div>
                     </div>
                     <div class="feature-list r-feature-even">
                        <div class="f-18 f-md-20 lh140 w400">
                          <span class="w600">Grab Maximum Leads -</span> Show Pop-Ups Inside Landing Pages And Funnels Pages
                        </div>
                     </div>
                     <div class="feature-list r-feature-odd">
                        <div class="f-18 f-md-20 lh140 w600">
                           Unmatched Webinar Set &amp; Forget Integration
                        </div>
                     </div>
                     <div class="feature-list r-feature-even">
                        <div class="f-18 f-md-20 lh140 w400">
                          <span class="w600"> No Expensive Domains &amp; Hosting Services-</span> Host Everything On Our Lightning Fast Server.
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>



     <section class="didyouknow-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-12 text-center">
                  <div class="f-28 f-md-45 w700 white-clr text-center lh140 head-design mt0 mt-md50">Do Funnels Still Work In 2023?</div>
                  <div class="f-28 f-md-100 w700 lh140 mt20 mt-md20">
                    <span class="black-clr"><img src="assets/images/thumbsup-smily.webp" alt="Thumbsup Smily"></span> <span class="red-clr">Yep,</span> <br>
                  </div>
                  <div class="f-24 f-md-28 w400 lh140 black-clr mt20 demo-text">
                     Here’s the 1 exact funnel that <span class="w600">got more than 32,000 visitors with 1,715 sales <br class="d-none d-md-block"> and total $156,837 in sales for one of my products.</span> 
                  </div>  
                  <div class="mt50 mt-md90">
                     <img src="assets/images/siteefy.webp" alt="Siteefy" class="img-fluid mx-auto d-block">
                  </div>
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-12 col-md-12 text-center">
                  <div class="f-24 f-md-28 w600 lh140 black-clr mt20 demo-text">
                     And by using the power of funnels, we have made 651K in sales <br class="d-none d-md-block"> &amp; 378K in affiliate commissions in the last 10 months.
                  </div>  
                  <div class="mt50 mt-md90">
                     <img src="assets/images/website-process.webp" alt="Siteefy" class="img-fluid mx-auto d-block">
                  </div>
               </div>
            </div>
            <div class="didyouknow-box mt20 mt-md70">
               <div class="row">
                  <div class="col-12">
                     <div class="f-18 f-md-20 lh150 w400 black-clr">
                        Listen, if you’re going to compete and get the life you desire in this ever-growing digital marketing era, then you’re going to <span class="w600"> <u>need an online presence that converts.</u></span> 
                        <br><br>
                        That means you’ll need websites with fast pages that guide, engage, and convert visitors into customers. <span class="w600">And that’s what funnels do. </span> 
                        <br><br>
                       <span class="w600">You only need to setup a funnel once and it delivers RESULTS again and again on complete autopilot!</span> 
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
  <!-- CTA Btn Start-->
  <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w600">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w600 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <!-- <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                  Use Coupon Code <span class="w600 red-clr">"AtulVIP"</span> for an Additional Discount on Commercial Licence
                  </div> -->
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                  Use Coupon Code <span class="w600 red-clr">"ASHUSPECIAL"</span> for an Additional <span class="w600 red-clr">$3 Discount</span> on FE Deal
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab QuickFunnel + My 20 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                  Use Coupon Code <span class="w600 red-clr">"ASHUBUNDLE"</span> for an Additional <span class="w600 red-clr">$50 Discount</span> on Bundle Deal
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="https://jvz2.com/c/47069/392382/" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab QuickFunnel Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 smmltd">Days</span>
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Sec</span>
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Btn End -->

      <div class="testimonial-section">
         <div class="container ">
            <div class="row ">
               <!-- <div class="col-12 f-24 f-md-28 w400 lh140 black-clr text-center ">
                  And it’s not just us. We also have <span class="w600">400+ TOP marketers</span> and more than <span class="w600">1000 customers</span> are successfully using it and ABSOLUTELY loving it.
               </div> -->
               <div class="col-12 f-28 f-md-45 w700 lh140 black-clr text-center">
                  <span class="red-clr">Checkout What Marketers &amp; Early Users</span>  Have to Say About QuickFunnel
               </div>
            </div>
            <div class="row mt20 mt-md80 align-items-center">
               <div class="col-md-6">
                  <div class="responsive-video border-video">
                     <iframe src="https://quickfunnel.dotcompal.com/video/embed/ccb2pf2a7f" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                  </div>
                  <div class="f-20 f-md-24 w600 lh140 black-clr text-center mt20 mt-md50">James Milo</div> 
                  <div class="f-18 f-md-20 w400 lh140 black-clr text-center mt10">21st Century Expert.com &amp; Video Animation Network&nbsp;</div>   
               </div>
               <div class="col-md-6 mt20 mt-md0">
                  <div class="responsive-video border-video">
                     <iframe src="https://quickfunnel.dotcompal.com/video/embed/pw7r36cz1j" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                  </div>
                  <div class="f-20 f-md-24 w600 lh140 black-clr text-center mt20 mt-md50">Craig McIntosh&nbsp;</div> 
                  <div class="f-18 f-md-20 w400 lh140 black-clr text-center mt10">Digital Marketing Consultant&nbsp;</div>  
               </div>
            </div>
           

            <div class="row row-cols-md-2 row-cols-1 gx4 mt80">
               <div class="col mt50">
                  <div class="single-testimonial">
                     <div class="st-img">
                        <img src="assets/images/imreviw.webp" class="img-fluid d-block mx-auto" alt="Imreviw">
                     </div>
                     <div class="mt20 md-md50 f-24 f-md-28 w600 lh140 black-clr"> IMReview Squad </div>
                     <div class="stars mt10">
                        <a href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                     </div>
                     <img src="assets/images/quote.webp" alt="Quotes" class="mx-auto d-block img-fluid mt20 mt-md50">
                     <p class="mt20 f-18 f-md-20 lh140 w400 text-center">
                        Wow!!! It’s an amazing creation that allows me to build high-converting pages and funnels EASY &amp; FAST.
                        Apart from its features, the best part I personally love is that it comes at a one-time price for the next few days…Complete value for your money. Guys go for it…
                     </p>
                  </div>
               </div>
               <div class="col mt120 mt-md50">
                  <div class="single-testimonial">
                     <div class="st-img">
                        <img src="assets/images/samuel.webp" class="img-fluid d-block mx-auto" alt="Samuel Marco">
                     </div>
                     <div class="mt20 md-md50 f-24 f-md-28 w600 lh140 black-clr"> Samuel Marco  </div>
                     <div class="stars mt10">
                        <a href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                     </div>
                     <img src="assets/images/quote.webp" alt="Quotes" class="mx-auto d-block img-fluid mt20 mt-md50">
                     <p class="mt20 f-18 f-md-20 lh140 w400 text-center">
                        QuickFunnel is next-level quality software. It's so easy to use, and the training included makes it even easier to build a page or funnel in any niche in a few minutes.
                        <br><br>
                        I love the fact that you can create high-converting sales funnels and pages without actually being a
                        technical nerd...all without any special skills. I say that this is a MUST-HAVE software for marketers! …
                     </p>
                  </div>
               </div>
            </div>

            <div id="features"></div>
         </div>
      </div>


      <div class="presenting-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-50 w600 red-clr lh140 presenting-head">
                     Proudly Presenting…
                  </div>
                  <div class="col-12 mt20 mt-md50">
                     <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 693.1 109.7" style="enable-background:new 0 0 693.1 109.7; height:45px;" xml:space="preserve">
                        <style type="text/css">
                           .st0{fill:#FFFFFF;}
                           .st1{fill:#EF3E39;}
                           .st2{fill:#853594;}
                           .st3{fill:#EF3E3A;}
                        </style>
                        <g>
                           <g id="Layer_1-2">
                              <path class="st0" d="M140.4,0H18.9c-2.8-0.1-5.2,2.1-5.4,4.9s2.1,5.2,4.9,5.4c0.2,0,0.3,0,0.5,0h42.3c2.8,0,5.1,2.3,5.1,5.1
                              c0,2.8-2.3,5.1-5.1,5.1h-56c-2.8,0.1-5,2.5-4.9,5.4c0.1,2.7,2.3,4.8,4.9,4.9h35.6l0,0h13.5c2.8,0.1,5,2.5,4.9,5.4
                              c-0.1,2.7-2.3,4.8-4.9,4.9H46c-2.8,0-5.1,2.4-5.1,5.2c0,2.7,2.1,4.9,4.8,5.1h17.8c2.8,0,5.2,2.2,5.2,5.1c0,2.8-2.2,5.2-5.1,5.2h-3
                              c-1.3,0-2.6,0.6-3.5,1.5c-2,2-2,5.2-0.1,7.3c1,1,2.3,1.5,3.7,1.5h8v37.7L103.1,96V68.6l42.8-57.6c2.3-3,1.6-7.3-1.4-9.6
                              C143.3,0.5,141.9,0,140.4,0z M66.2,36c0-2.8,2.3-5.1,5.1-5.1h10c2.8,0.1,5,2.5,4.9,5.4c-0.1,2.7-2.3,4.8-4.9,4.9h-10
                              C68.5,41.1,66.2,38.8,66.2,36z"></path>
                              <path class="st1" d="M34,46.3c0,2.8-2.3,5.1-5.1,5.1h-10c-2.8,0-5.1-2.3-5.1-5.1c0-2.8,2.3-5.1,5.1-5.1c0,0,0,0,0,0h10
                              C31.7,41.2,34,43.5,34,46.3z"></path>
                              <path class="st1" d="M48.8,66.9c0,2.8-2.3,5.1-5.1,5.1h-2.9c-2.8,0-5.1-2.1-5.3-4.8c-0.1-1.5,0.4-3,1.5-4c0.9-0.9,2.2-1.5,3.5-1.5
                              h3.4C46.6,61.8,48.9,64.1,48.8,66.9z"></path>
                              <path class="st2" d="M45.8,61.7L45.8,61.7z"></path>
                              <path class="st2" d="M63.7,61.7L63.7,61.7z"></path>
                              <path class="st2" d="M63.7,61.7L63.7,61.7z"></path>
                              <path class="st3" d="M230,48.3c0,4.5-0.7,8.9-2.3,13.1c-1.5,3.9-3.7,7.5-6.5,10.5c-2.8,3-6.1,5.4-9.9,7c-8,3.3-17,3.3-24.9,0
                              c-3.7-1.6-7.1-4-9.8-7c-2.8-3.1-5-6.7-6.4-10.5c-1.6-4.2-2.4-8.7-2.3-13.1c0-4.5,0.7-9,2.3-13.2c1.4-3.9,3.6-7.5,6.4-10.5
                              c2.8-3,6.1-5.4,9.8-7c8-3.3,17-3.3,24.9,0c3.7,1.6,7.1,4,9.9,7c2.8,3.1,5,6.6,6.5,10.5C229.3,39.3,230,43.8,230,48.3z M222.1,48.3
                              c0.1-4.6-0.9-9.2-3-13.4c-1.9-3.7-4.7-6.9-8.2-9.1c-7.4-4.4-16.6-4.4-24,0c-3.5,2.3-6.3,5.4-8.2,9.1c-2.1,4.2-3.1,8.7-3,13.4
                              c-0.1,4.6,1,9.2,3,13.3c1.8,3.7,4.7,6.9,8.2,9.2c7.4,4.4,16.6,4.4,24,0c3.5-2.3,6.3-5.4,8.2-9.2C221.1,57.5,222.1,52.9,222.1,48.3
                              z M233.1,84.8c1.9,0.1,3.4,1.6,3.4,3.5c-0.1,1.3-0.7,2.5-1.7,3.4c-1.3,1.2-2.8,2.2-4.4,2.9c-1.8,0.9-3.7,1.5-5.7,2
                              c-1.8,0.5-3.7,0.7-5.6,0.7c-3.1,0.1-6.2-0.3-9.2-1.2c-2.1-0.7-4.2-1.5-6.2-2.6c-1.8-1-3.5-1.9-5.2-2.6c-1.9-0.8-3.9-1.2-6-1.2
                              c-1,0-2,0.2-2.9,0.6c-0.8,0.4-1.6,0.7-2.3,1.1c-0.8,0.4-1.7,0.6-2.6,0.6c-1,0-2-0.4-2.7-1.1c-0.6-0.7-1-1.6-1-2.5
                              c-0.1-1.4,0.8-2.8,2.1-3.2L200,79l8.4,0.8l-15.7,5.5l2.6-1.9c2.2,0,4.4,0.3,6.4,1.1c1.9,0.7,3.7,1.5,5.5,2.5
                              c1.8,0.9,3.6,1.8,5.5,2.5c2.1,0.8,4.4,1.1,6.6,1.1c1.8,0,3.7-0.3,5.4-1c1.2-0.5,2.4-1.2,3.6-1.9c0.8-0.6,1.7-1.3,2.4-2
                              C231.3,85.1,232.1,84.8,233.1,84.8L233.1,84.8z"></path>
                              <path class="st3" d="M277.1,32.9c1,0,2,0.4,2.7,1.1c0.7,0.7,1,1.7,1,2.6v25.1c0,6.2-1.7,11-5.2,14.4s-8.3,5.1-14.5,5.1
                              c-6.2,0-11-1.7-14.4-5.1s-5.2-8.2-5.2-14.4V36.6c0-2,1.5-3.7,3.5-3.7c0,0,0.1,0,0.1,0c1,0,2,0.4,2.7,1.1c0.7,0.7,1,1.7,1,2.6v25.1
                              c0,4.2,1.1,7.3,3.2,9.5s5.2,3.2,9,3.2c4,0,7.1-1.1,9.2-3.2s3.2-5.3,3.2-9.5V36.6c0-2,1.5-3.7,3.5-3.7
                              C277,32.9,277,32.9,277.1,32.9L277.1,32.9z"></path>
                              <path class="st3" d="M297.9,26c-1.2,0.1-2.4-0.2-3.4-0.9c-0.8-0.7-1.2-1.8-1.1-2.9v-1.3c-0.1-1.1,0.4-2.2,1.2-2.9
                              c1-0.7,2.2-1,3.4-0.9c1.2-0.1,2.4,0.2,3.3,0.9c0.8,0.7,1.2,1.8,1.1,2.9v1.3c0.1,1.1-0.3,2.1-1.1,2.9
                              C300.2,25.8,299.1,26.1,297.9,26z M301.5,76.8c0,1-0.4,2-1.1,2.7c-1.4,1.5-3.8,1.5-5.3,0c0,0,0,0,0,0c-0.7-0.7-1-1.7-1-2.7V35.9
                              c-0.1-2,1.5-3.8,3.5-3.9s3.8,1.5,3.9,3.5c0,0.1,0,0.3,0,0.4L301.5,76.8z"></path>
                              <path class="st3" d="M336,31.7c2.9,0,5.7,0.3,8.5,1c2.2,0.5,4.2,1.5,6,2.8c1.3,0.9,2.2,2.4,2.2,4c0,0.8-0.3,1.6-0.8,2.3
                              c-0.5,0.7-1.3,1.1-2.2,1.1c-0.7,0-1.4-0.1-2.1-0.5c-0.5-0.3-1-0.7-1.5-1.2c-0.5-0.5-1.2-1-1.9-1.3c-1-0.5-2.1-0.8-3.2-1
                              c-1.4-0.2-2.8-0.4-4.2-0.4c-3.1-0.1-6.2,0.8-8.9,2.4c-2.5,1.6-4.6,3.8-6,6.5c-1.5,2.8-2.3,6-2.2,9.2c-0.1,3.2,0.7,6.4,2.1,9.2
                              c1.4,2.6,3.4,4.9,5.9,6.4c2.6,1.6,5.7,2.5,8.8,2.4c1.7,0,3.5-0.1,5.2-0.6c1.1-0.3,2.2-0.7,3.2-1.3c0.9-0.5,1.8-1.1,2.5-1.9
                              c1.4-1.3,3.5-1.2,4.8,0.1c0.6,0.7,0.9,1.6,0.9,2.5c0,1.1-0.8,2.3-2.3,3.5c-1.9,1.4-4,2.5-6.2,3.1c-2.9,0.9-5.9,1.3-8.9,1.3
                              c-4.3,0.1-8.6-1-12.3-3.3c-3.5-2.2-6.3-5.3-8.2-9c-2-3.9-3-8.3-2.9-12.7c-0.1-4.4,1-8.7,3-12.6C319.3,36.2,327.3,31.5,336,31.7z"></path>
                              <path class="st3" d="M367.1,80.6c-1,0-2-0.4-2.7-1.1c-0.7-0.7-1-1.7-1-2.7V16c0-1,0.3-1.9,1-2.6c0.7-0.7,1.7-1.1,2.7-1.1
                              c2,0,3.7,1.5,3.7,3.5c0,0,0,0.1,0,0.1v60.8c0,1-0.4,2-1.1,2.7C369,80.2,368.1,80.6,367.1,80.6z M397.7,32.6c1,0,1.9,0.4,2.5,1.2
                              c1.3,1.2,1.4,3.2,0.2,4.6c-0.2,0.2-0.4,0.4-0.6,0.5L370,65l-0.3-8.6l25.5-22.7C395.8,33,396.7,32.6,397.7,32.6z M398.7,80.5
                              c-1,0-2-0.4-2.7-1.2l-19.7-20.9l5.5-5.1l19.5,20.8c0.7,0.7,1.1,1.7,1.1,2.8c0,1-0.4,2-1.3,2.6C400.4,80.1,399.6,80.4,398.7,80.5
                              L398.7,80.5z"></path>
                              <path class="st0" d="M419,80.6c-1.6,0.1-3.1-0.5-4.2-1.6c-1.1-1.1-1.6-2.5-1.6-4V21.7c-0.1-3,2.3-5.6,5.3-5.7c0.1,0,0.2,0,0.3,0
                              h30c1.5,0,2.9,0.5,4,1.5c1.1,1,1.7,2.4,1.6,3.9c0,1.4-0.6,2.7-1.6,3.6c-1.1,1-2.5,1.6-4,1.6h-24.9l0.8-1.1v18.6l-0.6-1.2h20.6
                              c1.5,0,2.9,0.5,4,1.5c1.1,1,1.7,2.4,1.6,3.9c0,1.4-0.6,2.7-1.6,3.6c-1.1,1-2.5,1.6-4,1.6h-20.8l0.8-0.8V75c0,1.5-0.6,3-1.7,4
                              C421.9,80,420.5,80.6,419,80.6z"></path>
                              <path class="st0" d="M499.3,31.3c1.5-0.1,3,0.5,4,1.6c1,1.1,1.6,2.5,1.6,4v23.4c0,6.5-1.8,11.7-5.4,15.5
                              c-3.6,3.8-8.9,5.7-15.7,5.7c-6.8,0-12-1.9-15.6-5.7s-5.4-9-5.4-15.5V36.9c0-1.5,0.5-2.9,1.6-4c2.2-2.2,5.7-2.2,7.9,0
                              c1,1.1,1.6,2.5,1.6,4v23.4c0,3.8,0.8,6.5,2.5,8.3s4.1,2.7,7.5,2.7s5.9-0.9,7.6-2.7s2.5-4.6,2.5-8.3V36.9c0-1.5,0.5-2.9,1.6-4
                              C496.4,31.8,497.8,31.2,499.3,31.3z"></path>
                              <path class="st0" d="M543.2,30.3c4.6,0,8,1,10.4,2.9c2.4,1.9,4.1,4.6,4.9,7.6c0.9,3.4,1.4,7,1.3,10.6V75c0,1.5-0.5,2.9-1.6,4
                              c-2.2,2.2-5.7,2.2-7.9,0c-1-1.1-1.6-2.5-1.6-4V51.3c0-1.9-0.2-3.7-0.8-5.5c-0.5-1.6-1.5-2.9-2.8-3.9c-1.7-1.1-3.8-1.6-5.8-1.5
                              c-2.2-0.1-4.3,0.4-6.2,1.5c-1.6,0.9-3,2.3-3.9,3.9c-0.9,1.7-1.4,3.6-1.3,5.5V75c0,1.5-0.5,2.9-1.6,4c-2.2,2.2-5.7,2.2-7.9,0
                              c-1-1.1-1.6-2.5-1.6-4V36.9c0-1.5,0.5-2.9,1.6-4c2.2-2.2,5.7-2.2,7.9,0c1,1.1,1.6,2.5,1.6,4v4l-1.4-0.3c0.7-1.2,1.5-2.3,2.4-3.4
                              c1.1-1.3,2.3-2.4,3.7-3.4c1.5-1.1,3.1-1.9,4.8-2.5C539.2,30.7,541.2,30.3,543.2,30.3z"></path>
                              <path class="st0" d="M598.1,30.3c4.5,0,8,1,10.4,2.9c2.4,2,4.1,4.6,4.9,7.6c0.9,3.4,1.4,7,1.3,10.6V75c0,1.5-0.5,2.9-1.6,4
                              c-2.2,2.2-5.7,2.2-7.9,0c-1-1.1-1.6-2.5-1.6-4V51.3c0-1.9-0.2-3.7-0.8-5.5c-0.5-1.6-1.5-2.9-2.8-3.9c-1.7-1.1-3.8-1.6-5.8-1.5
                              c-2.2-0.1-4.3,0.4-6.3,1.5c-1.6,0.9-3,2.3-3.9,3.9c-0.9,1.7-1.4,3.6-1.3,5.5V75c0,1.5-0.5,2.9-1.6,4c-2.2,2.2-5.7,2.2-7.9,0
                              c-1-1.1-1.6-2.5-1.6-4V36.9c0-1.5,0.5-2.9,1.6-4c2.2-2.2,5.7-2.2,7.9,0c1,1.1,1.6,2.5,1.6,4v4l-1.4-0.3c0.7-1.2,1.5-2.3,2.4-3.4
                              c1.1-1.3,2.3-2.4,3.7-3.4c1.5-1.1,3.1-1.9,4.8-2.5C594.2,30.7,596.1,30.3,598.1,30.3z"></path>
                              <path class="st0" d="M649.9,81.5c-4.7,0.1-9.4-1-13.6-3.3c-3.7-2.1-6.8-5.2-8.8-8.9c-2.1-3.9-3.2-8.3-3.1-12.7
                              c-0.1-4.9,1-9.8,3.4-14.2c2.1-3.7,5.1-6.8,8.8-9c3.5-2,7.5-3.1,11.5-3.1c3.1,0,6.1,0.6,8.9,1.9c2.8,1.3,5.3,3.1,7.4,5.3
                              c2.2,2.3,3.9,4.9,5.1,7.8c1.3,3,1.9,6.2,1.9,9.4c0,1.4-0.7,2.7-1.8,3.6c-1.1,0.9-2.5,1.4-3.9,1.4h-35.2l-2.8-9.2h33.8l-2,1.8v-2.5
                              c-0.1-1.8-0.8-3.4-1.9-4.8c-1.1-1.4-2.6-2.6-4.2-3.4c-1.7-0.8-3.5-1.3-5.4-1.3c-1.8,0-3.5,0.2-5.2,0.7c-1.6,0.5-3,1.3-4.1,2.5
                              c-1.3,1.3-2.3,2.9-2.8,4.7c-0.7,2.4-1.1,4.9-1,7.5c-0.1,2.9,0.6,5.9,2.1,8.4c1.3,2.2,3.1,4,5.3,5.3c2.1,1.2,4.6,1.8,7,1.8
                              c1.8,0,3.7-0.1,5.4-0.6c1.1-0.3,2.3-0.7,3.3-1.3c0.8-0.5,1.6-1,2.3-1.3c1-0.5,2-0.8,3.1-0.8c2.6,0,4.7,2,4.7,4.6c0,0,0,0,0,0.1
                              c-0.1,1.8-1,3.5-2.6,4.5c-2,1.6-4.3,2.8-6.8,3.6C656,81,653,81.5,649.9,81.5z"></path>
                              <path class="st0" d="M693.1,75c0.1,3.1-2.4,5.6-5.4,5.6c-0.1,0-0.1,0-0.2,0c-1.5,0-2.9-0.6-3.9-1.6c-1.1-1.1-1.6-2.5-1.6-4V18
                              c-0.1-3.1,2.4-5.6,5.4-5.6c0.1,0,0.1,0,0.2,0c1.5-0.1,2.9,0.5,3.9,1.6c1,1.1,1.6,2.5,1.5,4V75z"></path>
                           </g>
                        </g>
                     </svg>
                  </div>
                  <div class="col-12 mt-md50 mt20 f-md-50 f-24 w600 text-center white-clr lh140">
                     Launch <span class="red-clr">Blazing-Fast Loading Funnels And Websites In Just 7 Minutes</span> With No Tech Hassles &amp; No Monthly Fee Ever…
                 </div>
                 <div class="col-12 mt20 mt-md30 lh140 f-18 f-md-20 w500 white-clr">
                  FREE Commercial License Included to Build an Incredible Income 
                 </div>
               </div>
            </div>
            <div class="row mt20 mt-md40 align-items-center">
               <div class="col-md-7 col-12 mx-auto">
                  <img src="assets/images/product-image.webp" class="img-fluid d-block mx-auto">
               
                     <!-- <div class="video-box">
                        <div style="padding-bottom: 56.25%;position: relative;">
                           <iframe src="https://restrosuite.dotcompal.com/video/embed/67fmqio11q" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                        </div>
                     </div> -->
               </div>
               <div class="col-md-5 col-12 mt30 mt-md0">
                  <div class="proudly-features">
                     <ul class="pl0 m0 f-18 f-md-20 lh150 w400 white-clr">
                       <li> <span class="red-clr caveat w700 f-24 f-md-30">Quick &amp; Easy</span> <br> Create Lightning-Fast Funnels Easily with Free-Flow Funnel Planner &amp; Drag-N-Drop Editor </li>
                       <li> <span class="red-clr caveat w700 f-24 f-md-30">Maximize Results</span> <br> Get Max Engagement, Max Leads &amp; Max Sales with High converting Funnels &amp; Pages  </li>
                       <li> <span class="red-clr caveat w700 f-24 f-md-30">Reliable &amp; Robust</span> <br> Battle-Tested Architecture to Handle Huge Load without Any Hassles</li>
                       <li> <span class="red-clr caveat w700 f-24 f-md-30">Proven &amp; Elegant</span> <br> Empowered with 400+ Proven Converting &amp; Elegant Templates </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
   </div>
      
      <!-------Exclusive Bonus----------->
      <div class="exclusive-bonus">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                  <div class="white-clr f-24 f-md-36 lh140 w500">
                     Exclusive Bonus #1 : Nexusgpt
                  </div>
                  <div class="f-18 f-md-20 w500 mt20 white-clr">
                     20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="nexusgpt-header-section">
      <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row align-items-center">
                     <div class="col-md-12 text-center">
                        <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1073.21 250" style="max-height:65px;">
                           <defs>
                              <style>
                                 .cls-1ng {
                                 fill: #fff;
                                 }

                                 .cls-2ng {
                                 fill-rule: evenodd;
                                 }

                                 .cls-2ng, .cls-3ng {
                                 fill: #00a4ff;
                                 }

                                 .cls-4ng {
                                 fill: #f19b10;
                                 }
                              </style>
                           </defs>
                           <g id="Layer_1-2" data-name="Layer 1">
                              <g>
                                 <g>
                                    <g>
                                       <path class="cls-1ng" d="m872.38,109.17c-3.33-7.67-8.42-13.69-15.25-18.08-6.83-4.39-14.86-6.58-24.08-6.58-8.67,0-16.45,2-23.33,6-6.89,4-12.33,9.7-16.33,17.08-4,7.39-6,15.97-6,25.75s2,18.39,6,25.83c4,7.45,9.44,13.17,16.33,17.17,6.89,4,14.67,6,23.33,6,8.11,0,15.42-1.75,21.92-5.25s11.72-8.5,15.67-15c3.94-6.5,6.19-14.08,6.75-22.75h-49v-9.5h61.33v8.5c-.56,10.22-3.33,19.47-8.33,27.75-5,8.28-11.7,14.81-20.08,19.58-8.39,4.78-17.81,7.17-28.25,7.17s-20.56-2.53-29.33-7.58c-8.78-5.05-15.69-12.11-20.75-21.17-5.06-9.05-7.58-19.3-7.58-30.75s2.53-21.69,7.58-30.75c5.05-9.05,11.97-16.11,20.75-21.17,8.78-5.05,18.55-7.58,29.33-7.58,12.44,0,23.33,3.11,32.67,9.33,9.33,6.22,16.11,14.89,20.33,26h-13.67Z"></path>
                                       <path class="cls-1ng" d="m975.71,132.83c-6.56,6.11-16.39,9.17-29.5,9.17h-23.5v49.67h-11.67v-116.5h35.17c13,0,22.8,3.06,29.42,9.17,6.61,6.11,9.92,14.22,9.92,24.33s-3.28,18.06-9.83,24.17Zm-2.17-24.17c0-7.78-2.17-13.67-6.5-17.67-4.33-4-11.28-6-20.83-6h-23.5v47h23.5c18.22,0,27.33-7.78,27.33-23.33Z"></path>
                                       <path class="cls-1ng" d="m1073.21,75.17v9.67h-32.33v106.83h-11.67v-106.83h-32.5v-9.67h76.5Z"></path>
                                    </g>
                                    <g>
                                       <path class="cls-1ng" d="m322.77,192.08h-28.5l-47.67-72.17v72.17h-28.5v-117h28.5l47.67,72.5v-72.5h28.5v117Z"></path>
                                       <path class="cls-1ng" d="m505.27,192.08l-23.83-35.83-21,35.83h-32.33l37.5-59.5-38.33-57.5h33.17l23.5,35.33,20.67-35.33h32.33l-37.17,59,38.67,58h-33.17Z"></path>
                                       <path class="cls-1ng" d="m580.77,75.08v70c0,7,1.72,12.39,5.17,16.17,3.44,3.78,8.5,5.67,15.17,5.67s11.78-1.89,15.33-5.67c3.55-3.78,5.33-9.17,5.33-16.17v-70h28.5v69.83c0,10.45-2.22,19.28-6.67,26.5-4.45,7.22-10.42,12.67-17.92,16.33-7.5,3.67-15.86,5.5-25.08,5.5s-17.47-1.8-24.75-5.42c-7.28-3.61-13.03-9.05-17.25-16.33-4.22-7.28-6.33-16.14-6.33-26.58v-69.83h28.5Z"></path>
                                       <path class="cls-1ng" d="m689.6,189.08c-6.78-2.78-12.2-6.89-16.25-12.33-4.06-5.44-6.2-12-6.42-19.67h30.33c.44,4.33,1.94,7.64,4.5,9.92,2.55,2.28,5.89,3.42,10,3.42s7.55-.97,10-2.92c2.44-1.94,3.67-4.64,3.67-8.08,0-2.89-.97-5.28-2.92-7.17-1.95-1.89-4.33-3.44-7.17-4.67-2.83-1.22-6.86-2.61-12.08-4.17-7.56-2.33-13.72-4.67-18.5-7-4.78-2.33-8.89-5.78-12.33-10.33-3.45-4.55-5.17-10.5-5.17-17.83,0-10.89,3.94-19.42,11.83-25.58,7.89-6.17,18.17-9.25,30.83-9.25s23.28,3.08,31.17,9.25c7.89,6.17,12.11,14.75,12.67,25.75h-30.83c-.22-3.78-1.61-6.75-4.17-8.92-2.56-2.17-5.83-3.25-9.83-3.25-3.45,0-6.22.92-8.33,2.75-2.11,1.83-3.17,4.47-3.17,7.92,0,3.78,1.78,6.72,5.33,8.83,3.55,2.11,9.11,4.39,16.67,6.83,7.55,2.56,13.69,5,18.42,7.33,4.72,2.33,8.8,5.72,12.25,10.17,3.44,4.45,5.17,10.17,5.17,17.17s-1.7,12.72-5.08,18.17c-3.39,5.45-8.31,9.78-14.75,13-6.45,3.22-14.06,4.83-22.83,4.83s-16.22-1.39-23-4.17Z"></path>
                                       <rect class="cls-1ng" x="343.41" y="75.29" width="71.69" height="22.79"></rect>
                                       <rect class="cls-1ng" x="343.41" y="121.79" width="71.69" height="22.79"></rect>
                                       <rect class="cls-1ng" x="343.41" y="169.5" width="71.69" height="22.79"></rect>
                                    </g>
                                 </g>
                                 <g>
                                    <path class="cls-3ng" d="m76.03.47L13.55,16.04C5.96,17.93,0,28.08,0,38.78v147.74c0,.3,0,.61,0,.91v23.78c0,10.7,5.96,20.86,13.55,22.75l62.48,15.56c10.52,2.63,19.39-5.93,19.39-19.22v-112.1c0-.25,0-.5,0-.75V19.7c0-13.29-8.88-21.85-19.4-19.22Zm-34.9,227.64c-2.77-1.31-4.37-4.32-4.37-7.13s1.61-5.83,4.37-7.13c4.4-2.08,10.41.47,10.41,7.13s-6.01,9.22-10.41,7.13ZM54.19,23.79l-19.12,4.39c-1.93.44-3.86-.76-4.3-2.69-.06-.27-.09-.54-.09-.81,0-1.63,1.12-3.11,2.78-3.49l19.13-4.39c1.92-.45,3.85.76,4.29,2.69.07.27.09.54.09.81,0,1.63-1.12,3.11-2.78,3.49Z"></path>
                                    <path class="cls-4ng" d="m166.8,83.52l-44.46-11.07c-7.48-1.87-13.8,4.23-13.8,13.68v149.86c0,9.46,6.32,15.55,13.8,13.68l44.46-11.07c5.41-1.35,9.64-8.57,9.64-16.18v-122.7c0-7.61-4.23-14.84-9.64-16.18Zm-19.62,150.91c-3.13,1.47-7.41-.34-7.41-5.08s4.27-6.56,7.41-5.08c1.97.93,3.11,3.08,3.11,5.08s-1.14,4.14-3.11,5.08Zm6.79-144.13c-.33,1.31-1.65,2.11-2.96,1.79l-12.66-2.64c-1.5-.42-2.2-1.68-2.2-2.85,0-.23.03-.46.08-.68.32-1.32,1.65-2.12,2.96-1.79l12.98,3.21c1.12.28,1.87,1.27,1.87,2.37,0,.2-.03.39-.08.59Z"></path>
                                    <g>
                                       <path class="cls-2ng" d="m163.94,67.4h0c-.95,1.77-2.97,2.67-4.92,2.18-1.95-.48-3.32-2.22-3.34-4.23.96-3.86,1.37-7.85,1.22-11.85-.25-7.01-2.24-14.05-6.12-20.47-.17-.27-.33-.54-.51-.81-.25-.41-.52-.81-.79-1.2-.38-.57-.78-1.12-1.19-1.66-.09-.12-.18-.23-.27-.35-.22-.28-.43-.55-.66-.82-2.05-2.55-4.37-4.81-6.87-6.76-.3-.24-.6-.48-.91-.69-.43-.32-.85-.62-1.28-.92-1.09-.75-2.21-1.44-3.35-2.07-3.35-1.87-6.9-3.26-10.53-4.15-3.93-.97-7.96-1.37-11.97-1.21-1.96-.42-3.39-2.12-3.47-4.12v-.18c0-1.93,1.26-3.64,3.12-4.21,9.06-.36,18.22,1.67,26.45,6.12.54.29,1.08.59,1.62.91.36.21.71.43,1.06.64.35.22.7.44,1.05.67,2.31,1.53,4.53,3.25,6.63,5.19,1.68,1.56,3.25,3.2,4.68,4.92.22.27.43.54.65.81.2.25.4.5.59.75.33.43.65.86.96,1.29.25.34.49.69.73,1.04.24.35.47.71.7,1.06.23.36.46.72.67,1.07.16.25.31.51.46.77.36.61.71,1.22,1.03,1.84.32.59.62,1.19.92,1.8.18.38.36.75.53,1.13.17.38.34.76.5,1.14.17.38.32.77.48,1.15.27.68.52,1.35.76,2.03,3.37,9.57,3.71,19.71,1.38,29.18Z"></path>
                                       <g>
                                          <path class="cls-2ng" d="m116.46,56.27c-.03-.25-.08-.5-.16-.74,0-.02,0-.02,0-.04-.02-.07-.04-.13-.07-.18,0-.05-.03-.1-.05-.14,0,0,0-.03-.02-.03,0,0,0,0,0-.02-.04-.11-.09-.2-.15-.3,0-.03-.02-.04-.03-.07-.05-.09-.12-.19-.18-.28,0-.02-.02-.05-.04-.07-.03-.04-.06-.09-.09-.13-.04-.04-.08-.08-.12-.12,0,0,0-.02-.03-.03,0,0-.02-.03-.03-.04-.08-.08-.15-.16-.23-.23-.02-.03-.05-.05-.07-.08-.06-.05-.12-.1-.18-.15,0,0-.02-.02-.03-.03l-.3-.2s-.04-.03-.06-.04c-.04-.03-.09-.05-.13-.08-.04-.02-.08-.03-.13-.05h0s0-.02,0-.02c-.02,0-.05-.03-.07-.03-.08-.04-.18-.08-.27-.12-.02,0-.03,0-.04,0-.04-.02-.09-.03-.13-.04-.06-.02-.11-.02-.17-.04s-.12-.03-.19-.05c-.03-.02-.08-.03-.12-.03h-.02l-.18-.03s-.1-.02-.15-.02c-.07,0-.13,0-.19,0h-.54c-.13.02-.25.03-.38.07-.07,0-.16.03-.23.05-.03,0-.05.02-.08.03-.02,0-.06.02-.09.02h-.02c-.11.04-.21.08-.32.13-.02,0-.04.02-.06.03,0,0-.02,0-.02,0-.09.04-.18.09-.28.14h0c-.11.07-.21.13-.31.2-.02,0-.02,0-.03.03-.04.02-.08.05-.13.08,0,0-.02.02-.03.03-.04.03-.08.06-.13.11,0,0-.02,0-.03.03-.05.03-.09.07-.13.12-.03.03-.07.06-.09.09-.02.02-.04.04-.07.07-.03.02-.05.05-.08.08-.07.08-.13.17-.2.25-.05.07-.1.15-.15.23-.02.02-.03.03-.03.05-.02.02-.03.06-.05.08-.03.05-.06.11-.08.17-.03.03-.03.04-.03.07-.04.08-.08.16-.11.23,0,0,0,.02,0,.03-.03.07-.07.16-.08.23-.02.04-.03.07-.04.12-.04.12-.07.25-.09.38,0,.07-.02.13-.02.21,0,0,0,.03,0,.04,0,.02,0,.03,0,.04,0,.05-.02.11,0,.16-.03.2-.02.4,0,.6,0,.05,0,.09,0,.13,0,.03,0,.08.02.11,0,.03.02.07.03.1,0,.1.02.19.05.28,0,.03.02.04.02.07.03.12.08.25.12.37.04.09.08.17.13.27,0,.02,0,.03.02.04.02.04.04.09.07.13,0,.02.02.03.03.06,0,.02.03.05.05.07,0,0,.02.03.03.04s.02.04.04.06c.03.07.07.12.12.18.02.03.03.05.06.07,0,0,0,.02,0,.02.04.06.08.11.13.16.03.05.08.09.12.13.02.03.04.04.06.07,0,0,.02.02.03.03.03.03.05.05.08.07.06.06.11.11.17.16.03.03.06.05.08.07.02.03.05.04.07.06.08.06.15.11.23.15.06.04.13.08.19.12,0,0,0,0,0,0,.06.03.11.07.17.09l.18.08s.06.03.08.04c.09.03.18.07.28.09.05.03.1.04.15.05.05.02.1.03.16.03.02.02.04.02.07.02.03,0,.08.02.11.02.08.02.17.03.25.04.03,0,.07,0,.1,0,.12.02.24.02.37.02.09,0,.19,0,.29,0,.08,0,.17-.02.26-.03.1-.02.2-.04.3-.07.09-.03.19-.06.29-.09,0,0,.02,0,.04,0h0c.12-.04.22-.09.33-.14.08-.03.15-.07.22-.1.11-.05.21-.11.3-.17,0,0,.02,0,.03-.02.02,0,.02-.02.04-.02.03-.03.08-.05.11-.08,0,0,0,0,.02,0,.02-.02.04-.03.06-.05,0,0,.02,0,.03-.03.1-.07.19-.15.28-.23.02-.02.03-.03.04-.04.09-.09.18-.18.26-.28.1-.12.19-.24.28-.37.02-.03.05-.08.07-.11.04-.05.07-.11.1-.17.02-.03.04-.07.06-.11.13-.26.23-.53.3-.82,0,0,0-.03,0-.04.08-.34.11-.7.08-1.06,0-.07,0-.13,0-.2Z"></path>
                                          <path class="cls-2ng" d="m147.39,63.31c-.95,1.77-2.97,2.67-4.93,2.18-1.95-.48-3.32-2.22-3.34-4.23.57-2.32.83-4.71.73-7.12,0-.2-.02-.39-.03-.59-.23-4.02-1.43-8.03-3.64-11.72-.96-1.58-2.05-3.03-3.27-4.31-.5-.55-1.03-1.07-1.58-1.54-.1-.1-.21-.19-.31-.28-.3-.27-.61-.53-.92-.77-.14-.11-.28-.22-.42-.31,0-.02,0-.02-.02-.02,0-.02-.02-.02-.03-.02-.16-.13-.32-.25-.48-.36,0,0,0,0,0,0-.16-.12-.32-.23-.48-.33-.12-.11-.26-.19-.39-.28-2.51-1.67-5.23-2.83-8.04-3.52-2.37-.58-4.82-.82-7.24-.71-.19,0-.39,0-.58.02-1.97-.37-3.45-2.02-3.58-4.02-.13-2.01,1.11-3.84,3.01-4.48,5.91-.4,11.91.74,17.38,3.45.04.02.08.03.11.05,0,0,.02,0,.02.02.25.13.49.25.74.38.34.17.68.37,1.02.56.34.2.67.4,1.02.61.23.14.45.28.68.43.44.29.89.59,1.32.9,0,0,.02,0,.02.02.21.15.41.3.61.45l.02.02c.62.48,1.24.98,1.85,1.5.24.21.48.43.72.65.22.2.43.4.64.61.93.89,1.79,1.83,2.59,2.81.27.33.53.66.78.99.17.23.34.46.5.68.17.23.33.46.49.69.21.3.41.6.61.91,0,.03.02.04.03.06.38.59.74,1.19,1.08,1.8.18.32.34.63.51.95.14.28.28.56.42.84.14.28.27.57.4.85.13.28.25.57.37.86,2.9,6.91,3.33,14.38,1.63,21.32Z"></path>
                                       </g>
                                       <path class="cls-2ng" d="m130.85,59.22c-.95,1.77-2.98,2.67-4.93,2.18-1.94-.48-3.32-2.22-3.34-4.23.19-.75.28-1.53.25-2.32v-.14c0-.15-.02-.3-.03-.45-.12-1.24-.52-2.48-1.2-3.62-.54-.9-1.22-1.65-1.98-2.27-.03-.02-.07-.05-.1-.07-.05-.04-.09-.08-.13-.1-.07-.06-.14-.11-.21-.16,0,0-.02,0-.03-.02-.08-.07-.17-.12-.26-.17-.23-.17-.48-.33-.72-.44-.71-.42-1.46-.7-2.22-.88-.87-.21-1.75-.28-2.62-.22-.06,0-.12.02-.17.03-.14,0-.28.03-.42.04-2-.22-3.59-1.76-3.88-3.74-.28-1.99.81-3.92,2.66-4.69,4.02-.58,8.25.27,11.86,2.65.24.15.48.32.72.49-.08-.06-.17-.12-.25-.17.17.12.35.24.53.38.2.14.38.29.57.45.07.05.15.11.22.18.18.15.36.31.53.47-.1-.1-.19-.19-.29-.28.15.13.3.28.44.41,0,0,.01,0,.01.01,0,0,.01,0,.02.01,1.14,1.05,2.16,2.28,3,3.68,2.45,4.06,3.02,8.72,1.97,12.98Z"></path>
                                    </g>
                                 </g>
                              </g>
                           </g>
                        </svg>
                     </div>
                    
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50 text-center">
                  <div class="nexusgpt-pre-heading f-md-22 f-18 w500 lh140">
                  Exploit The NFC &amp; Ai Tech to <u>Make Multiple Recurring Income Streams </u>  
                  </div>
               </div>
               <div class="col-12 mt80 nexusgpt-head-design relative">
                  <div class="nexusgpt-gametext">
                     First-to-JVZoo Technology 
                  </div>
                  <div class=" f-md-40 f-28 w400 text-center black-clr lh140">
                     <span class="w600">Super-Easy NFC App Creates Contactless Digital Business Cards with Ai-Assistant, </span> Generates Leads, Followers, Reviews &amp; Sales  <span class="w600 underline-text2">with Just One Touch.</span>
                  </div>
               </div>
               <div class="col-12 mt-md40 mt20 f-20 f-md-24 w400 text-center lh140 white-clr text-capitalize">
               Easily Create &amp; Sell Contactless NFC Digital Cards &amp; GPT-Ai Assistants for Big Profits to Dentists, Attorney, Gyms, Spas, Restaurants, Cafes, Coaches, Affiliates &amp; 100+ Other Niches... 
               </div>
               <div class="col-12 mt20 f-20 f-md-24 w500 text-center lh140 orange-clr2 text-capitalize">
               No Tech Skills of Any Kind Needed. No Monthly Fee Ever. 
               </div>
            </div>
            <div class="row mt20 mt-md50 align-items-center">
               <div class="col-md-8 col-12">
                  <div class="col-12">
                     <div class="video-box">
                        <div style="padding-bottom: 56.25%;position: relative;"><iframe src="https://nexusgpt.oppyo.com/video/embed/e786pt8zf4" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe></div>
                     </div>
                  </div>
               </div>
               <div class="col-md-4 col-12 mt20 mt-md0">
                  <div class="">
                     <ul class="nexusgpt-list-head pl0 m0 f-18 lh140 w400 white-clr">
                        <li><span class="w600">Nexus Of NFC Tech &amp; GPT AI</span> Revolutionise Your Marketing</li>
                        <li><span class="w600">Help Desperate Local Businesses In Any Niche</span> within Minutes</li>
                        <li><span class="w600">GPT AI Assistant Closes Leads &amp; Sales 24x7</span> on Automation</li>
                        <li><span class="w600">Impress Your Colleagues &amp; Client’s</span> with Contactless NFC card</li>
                        <li><span class="w600">Tons of Ready-To-Go Templates</span> with Free-Flow Editor</li>
                        <li>Sell High In-Demand Services with <span class="w600">Included Commercial License.</span> </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Header Trendio End -->
      <div class="tr-all">
      <picture>
               <source media="(min-width:768px)" srcset="assets/images/nexusgpt.webp">
               <source media="(min-width:320px)" srcset="assets/images/nexusgpt-mview.webp" style="width:100%" class="vidvee-mview">
               <img src="assets/images/nexusgpt.webp" alt="" class="img-fluid" style="width: 100%;">
            </picture>
      </div>
      <!--Trendio ends-->

           <!-------WriterArc Starts----------->
     <!-------Exclusive Bonus----------->
     <div class="exclusive-bonus">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                  <div class="white-clr f-24 f-md-36 lh140 w500">
                     Exclusive Bonus #2 :  LinkPro
                  </div>
                  <div class="f-18 f-md-20 w500 mt20 white-clr">
                     20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
                  </div>
               </div>
            </div>
         </div>
      </div>

      <div class="linkpro-header-section">
      <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" style="max-height: 55px; width: 214px;" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1500 380" enable-background="new 0 0 1500 380" xml:space="preserve">
              <g>
                 <g id="Layer_1-2">
                    
                       <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="-689.5011" y1="10559.4189" x2="-790.6911" y2="10663.2998" gradientTransform="matrix(-0.7071 0.7071 0.7071 0.7071 -7815.8701 -6918.3799)">
                       <stop offset="0" style="stop-color:#3BB6FF"></stop>
                       <stop offset="1" style="stop-color:#006DE3"></stop>
                    </linearGradient>
                    <path fill="url(#SVGID_1_)" d="M137.2,100.2l53.2-8.5c13.1-28.5,42.8-45.7,74-42.9c-20.2-24.4-53.7-33.3-83.3-21.9
                       c-3.5,2.8-6.8,5.8-9.9,8.9C153.6,53.4,141.8,75.9,137.2,100.2z"></path>
                    
                       <linearGradient id="SVGID_2_" gradientUnits="userSpaceOnUse" x1="-526.0667" y1="10729.4053" x2="-433.6907" y2="10729.4053" gradientTransform="matrix(-0.7071 0.7071 0.7071 0.7071 -7815.8701 -6918.3799)">
                       <stop offset="0" style="stop-color:#EFA222"></stop>
                       <stop offset="1" style="stop-color:#F0592D"></stop>
                    </linearGradient>
                    <path fill="url(#SVGID_2_)" d="M167.9,371.5c11.4-4.5,22.1-10.7,31.7-18.4c-29.6,11.3-63.1,2.5-83.3-21.9
                       c-31.6-2.6-58-25.2-65.5-56c-2,27.5,5.4,55,21,77.7c5.8,8.6,12.8,16.4,20.6,23.2C117.4,382.5,143.8,380.8,167.9,371.5L167.9,371.5
                       z"></path>
                    
                       <linearGradient id="SVGID_3_" gradientUnits="userSpaceOnUse" x1="-789.4672" y1="10592.5967" x2="-794.1872" y2="10698.4463" gradientTransform="matrix(-0.7071 0.7071 0.7071 0.7071 -7815.8701 -6918.3799)">
                       <stop offset="0" style="stop-color:#3BB6FF"></stop>
                       <stop offset="1" style="stop-color:#006DE3"></stop>
                    </linearGradient>
                    <path fill="url(#SVGID_3_)" d="M181.1,27c29.6-11.4,63.2-2.6,83.4,21.9c31.6,2.6,57.9,25.1,65.5,55.9c0.9-12.2-0.1-24.6-3-36.5
                       c-6-25.1-19.6-47.7-39.1-64.6c-10.1-2.5-20.4-3.7-30.8-3.7C229.4,0.1,202.6,9.7,181.1,27z"></path>
                    
                       <linearGradient id="SVGID_4_" gradientUnits="userSpaceOnUse" x1="-498.1561" y1="10793.4258" x2="-595.8061" y2="10605.5264" gradientTransform="matrix(-0.7071 0.7071 0.7071 0.7071 -7815.8701 -6918.3799)">
                       <stop offset="0" style="stop-color:#EFA222"></stop>
                       <stop offset="1" style="stop-color:#F0592D"></stop>
                    </linearGradient>
                    <path fill="url(#SVGID_4_)" d="M75.1,370.4c5.6,2.4,11.5,4.3,17.4,5.8c-7.8-6.8-14.7-14.6-20.6-23.2c-15.6-22.8-23-50.2-21-77.7
                       c-6.2-25.2,1.2-51.8,19.5-70.1c9.8-9.9,22.3-16.8,35.9-19.8l76.6-11l58-8.3l7.1-1l-2.9-2.9l-23.6-23.6l-2.6-2.6l-14.1-14.1
                       l-31.9,4.7l-3.3,0.5l-9,1.4l-54.8,8.3l-11.5,1.7l-2,0.5C71,144.3,51.6,155.3,36,170.8c-47.9,47.8-48,125.3-0.2,173.2
                       C47.1,355.3,60.4,364.3,75.1,370.4L75.1,370.4z"></path>
                    
                       <linearGradient id="SVGID_5_" gradientUnits="userSpaceOnUse" x1="-608.9395" y1="10772.7686" x2="-482.3742" y2="10772.7686" gradientTransform="matrix(-0.7071 0.7071 0.7071 0.7071 -7815.8701 -6918.3799)">
                       <stop offset="0" style="stop-color:#EFA222"></stop>
                       <stop offset="1" style="stop-color:#F0592D"></stop>
                    </linearGradient>
                    <path fill="url(#SVGID_5_)" d="M174.9,310.3c-14.1,13.9-33.1,21.5-52.9,21.2c-1.9,0-3.8-0.1-5.6-0.3
                       c20.2,24.4,53.7,33.2,83.3,21.9c3.2-2.6,6.4-5.4,9.3-8.3l0.6-0.6c17.5-17.5,29.4-40,33.9-64.3l-53.2,8.5
                       C186.6,296.6,181.3,304,174.9,310.3z"></path>
                    
                       <linearGradient id="SVGID_6_" gradientUnits="userSpaceOnUse" x1="-810.9832" y1="10699.4775" x2="-631.0933" y2="10745.2881" gradientTransform="matrix(-0.7071 0.7071 0.7071 0.7071 -7815.8701 -6918.3799)">
                       <stop offset="0" style="stop-color:#3BB6FF"></stop>
                       <stop offset="1" style="stop-color:#006DE3"></stop>
                    </linearGradient>
                    <path fill="url(#SVGID_6_)" d="M208,253.7l3.2-0.5l9-1.4l54.8-8.3l11.4-1.7l2-0.5c21.6-5.5,41.3-16.9,57-32.7
                       c11.6-11.7,20.7-25.7,26.7-41c24.9-63-6-134.2-69-159.1c-5-2-10-3.6-15.2-4.9C307.3,20.5,321,43.2,327,68.3
                       c2.9,11.9,3.9,24.2,3,36.5c1.4,5.6,2.1,11.3,2.1,17c0.3,19.7-7.3,38.8-21.2,52.8c-0.1,0.1-0.3,0.3-0.4,0.4
                       c-9.8,9.9-22.3,16.8-35.8,19.8l-76.7,11l-58,8.3l-7.1,1l2.9,2.9l23.6,23.6l2.6,2.6l13.9,14.1L208,253.7z"></path>
                    <rect x="613.5" y="166.6" fill="#FFFFFF" width="44.4" height="159.8"></rect>
                    <path fill="#FFFFFF" d="M652.8,107.8c8.7,7.9,9.4,21.4,1.5,30.1c-0.5,0.5-1,1.1-1.5,1.5c-9.6,8.8-24.3,8.8-33.9,0
                       c-8.7-7.9-9.4-21.4-1.5-30.1c0.5-0.6,1-1.1,1.6-1.6C628.5,99,643.2,99,652.8,107.8z"></path>
                    <path fill="#FFFFFF" d="M834,192.6c4.6,9,6.9,19.7,6.9,32.1v101.7h-44.4v-93.8c0.1-9.8-2.4-17.4-7.5-22.9s-12.1-8.3-21-8.3
                       c-5.5-0.1-11,1.2-15.8,3.9c-4.6,2.6-8.3,6.5-10.6,11.2c-2.5,4.9-3.8,10.8-3.9,17.6v92.4h-44.4V166.6h42.3v28.2h1.9
                       c3.4-9.1,9.6-16.9,17.8-22.1c8.3-5.4,18.4-8.2,30.3-8.2c11.1,0,20.8,2.4,29.1,7.3C823,176.7,829.7,183.9,834,192.6z"></path>
                    <polygon fill="#FFFFFF" points="493,289.3 584.5,289.3 584.5,326.4 447.9,326.4 447.9,113.4 493,113.4 		"></polygon>
                    <polygon fill="#FFFFFF" points="1027.8,326.4 975.8,326.4 932.2,262 920.2,275.7 920.2,326.4 875.8,326.4 875.8,113.4 
                       920.2,113.4 920.2,227.3 922.6,227.3 973.8,166.6 1024.8,166.6 965.4,235.8 		"></polygon>
                    <path fill="#FFFFFF" d="M1198.9,148.1c-6-10.9-15.1-19.7-26.1-25.5c-11.4-6.1-25.2-9.2-41.4-9.2h-84.2v213h45.1v-69h37.9
                       c16.4,0,30.4-3,41.9-9c11.1-5.6,20.4-14.5,26.5-25.3c6.1-10.8,9.2-23.3,9.2-37.4S1204.9,158.9,1198.9,148.1z M1157.3,204.1
                       c-2.8,5.4-7.3,9.8-12.7,12.6c-5.7,3-12.8,4.5-21.5,4.5h-30.6v-71h30.4c8.8,0,16,1.5,21.7,4.4c5.4,2.7,9.9,7,12.8,12.3
                       c2.9,5.7,4.4,12.1,4.2,18.6C1161.6,192,1160.2,198.4,1157.3,204.1z"></path>
                    <path fill="#FFFFFF" d="M1333.6,165.9v39.3c-2.8-0.8-5.7-1.3-8.6-1.7c-3.4-0.5-6.7-0.7-10.1-0.7c-6.1-0.1-12.1,1.3-17.4,4.2
                       c-5,2.7-9.2,6.8-12.1,11.7c-3,5.2-4.5,11.2-4.4,17.3v90.4h-44.4V166.6h43v27.9h1.7c2.9-9.9,7.8-17.4,14.7-22.5
                       c6.9-5.1,15.2-7.8,23.8-7.6c2.4,0,4.8,0.2,7.2,0.4C1329.1,165,1331.4,165.4,1333.6,165.9z"></path>
                    <path fill="#FFFFFF" d="M1421.2,329.5c-16.2,0-30.2-3.5-41.9-10.4c-11.7-6.8-21.2-16.8-27.3-28.9c-6.4-12.4-9.6-26.8-9.6-43.1
                       c0-16.5,3.2-31,9.6-43.3c6.1-12.1,15.6-22.1,27.3-28.9c11.8-6.9,25.7-10.4,41.9-10.4s30.2,3.4,41.9,10.4
                       c11.7,6.8,21.1,16.8,27.2,28.9c6.4,12.4,9.6,26.8,9.6,43.3c0,16.4-3.2,30.7-9.6,43.1c-6.1,12.1-15.6,22.1-27.2,28.9
                       C1451.4,326.1,1437.4,329.5,1421.2,329.5z M1421.4,295.2c7.3,0,13.5-2.1,18.4-6.3c5-4.2,8.7-9.9,11.2-17.2
                       c5.1-16.2,5.1-33.5,0-49.7c-2.5-7.3-6.3-13-11.2-17.3c-4.9-4.2-11.1-6.3-18.4-6.4c-7.4,0-13.7,2.1-18.7,6.4s-8.8,10-11.3,17.3
                       c-5.1,16.2-5.1,33.5,0,49.7c2.5,7.3,6.3,13,11.3,17.2S1414,295.2,1421.4,295.2z"></path>
                 </g>
              </g>
              </svg>
                  
               </div>
            </div>
            <div class="row mt20 mt-md50">
               <div class="col-12 text-center">
                  <div class="f-20 f-md-24 w700 black-clr lh140 preheadline-lp">
                    <span class="orange-gradient"> Are you sick &amp; tired of losing clicks </span> with long, broken, or <br class="d-none d-md-block"> suspicious links in your emails, website, etc.?
                     <!-- First to JVZoo & <span class="under">MUST HAVE Solution</span> for All Marketers…   -->
                  </div>
               </div>
               <div class="col-12 mt20 mt-md80 relative">
                  <div class="linkpro-gametext d-none d-md-block">
                     Next-Gen Technology
                  </div>
                  <div class="linkpro-main-heading">
                      <div class=" f-md-45 f-28 w800 text-center white-clr lh140 ">
                           <span class="orange-gradient"> Convert ANY Long &amp; Ugly Marketing URL into Profit-Pulling SMART Link </span> (Clean, Trackable, &amp; QR Code Ready) <u>in Just 30 Seconds</u>... 
                      </div>
                    
                  </div>
               </div>

               <div class="col-12 mt20 mt-md50 text-center">
                  <div class="f-20 f-md-28 w500 text-center lh140 white-clr text-capitalize">               
                     Introducing The Industry-First, 5-In-One Builder – <span class="orange-gradient w700">QR Code, Link Cloaker, Shortener, Checkout Links &amp; Bio-Pages Creator - </span> All Rolled into One Easy to Use App!
                  </div>
               </div>
            </div>

            <div class="row mt20 mt-md70 align-items-center">
               <div class="col-md-8 col-12 mx-auto">
                  <div class="video-box">
                       <!--<video width="100%" height="auto" controls autoplay muted="muted">-->
                       <!--       <source src="assets/images/coming-soon.mp4" type="video/mp4">-->
                       <!--    </video> -->
                      <div style="padding-bottom: 56.25%;position: relative;">
                         <iframe src="https://linkpro.dotcompal.com/video/embed/x8ipwmt2vf" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
   box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe> No Matter What You Do Online for Living
                     </div>
                  </div>
               </div>
               <div class="col-md-4 col-12 mx-auto">
                  <div class="">
                     <ul class="linkpro-list-head pl0 m0 f-16 f-md-18 lh150 w400 white-clr">
                        <li><span class="orange-gradient w700">	Boost Your Click Through Rates</span> By Cloaking Long &amp; Suspicious Links</li>
                        <li><span class="orange-gradient w700">	Create QR Codes</span> For Touch-Less Payments &amp; Branding For Local Businesses</li>
                        <li><span class="orange-gradient w700">	Double Your Email Clicks &amp; Profits</span></li>
                        <li><span class="orange-gradient w700">	Boost Followership</span> Using Ready-To-Use Bio Pages.</li>
                        <li><span class="orange-gradient w700">	Fix All Broken Links on Any Website</span> In Just A Few Clicks </li>
                        <li><span class="orange-gradient w700">	Track Even A Single Click</span> So Never Lose Affiliate Commissions Again</li>
                        <li>	Sell High In-Demand Services With <span class="orange-gradient w700">Included Commercial License.</span></li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
    

      <div class="gr-1">
         <div class="container-fluid p0">
         <picture>
         <source media="(min-width:768px)" srcset="assets/images/linkpro.webp">
               <source media="(min-width:320px)" srcset="assets/images/linkpro-mview.webp" style="width:100%" class="vidvee-mview">
               <img src="assets/images/linkpro.webp" alt="" class="img-fluid" style="width: 100%;">
         </picture>
         </div>
      </div>
      <!-- WriterArc End -->

      <!-- Bonus Section Header Start -->
      <div class="bonus-header">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-10 mx-auto heading-bg text-center">
                  <div class="f-24 f-md-36 lh140 w600"> When You Purchase QuickFunnel, You Also Get <br class="hidden-xs"> Instant Access To These 20 Exclusive Bonuses</div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus Section Header End -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w600">Bonus 1</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus1.webp" class="img-fluid mx-auto d-block" alt="Bonus1">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w600 lh140 bonus-title-color text-capitalize">
                        Sales Funnel Blueprint
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>Who Else Wants To Skyrocket Sales Up To 300%... With The Same Amount Of Traffic & Buyers! Here's How You Can Get Started In 48 Hours Or Less...</b></li>
                           <li>The Internet marketing business had evolved throughout years. If you are in this business long enough, you are one of those to witness the changes in the past decade. From the beginning, the Internet marketer will only offer one product to the buyers. </li>
                          <li>Moving on, today, we have a sales funnel. Changes happenso rapidly. All things were shaped to fulfill the needs of the buyers. Thanks to the evolution of Internet that happened in the past decades, the reach has grown in an epic proportion. With more and more audience, things have to change. </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #1 Section End -->
      <!-- Bonus #2 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w600">Bonus 2</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30">
                  <div class="row">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="assets/images/bonus2.webp" class="img-fluid mx-auto d-block" alt="Bonus2">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w600 lh140 bonus-title-color mt20 mt-md0 text-capitalize">
                        Wp Easy Optins
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b> If you're new to marketing and list building use this plug-in today to move yourself forward with your list building efforts fast and with little fuss.</b></li>
                           <li>If you’re not new to marketing then you will see the potential of owning full PLR right to this product.</li>
                           <li>When installed "Wp Easy Optins" automatically creates all the pages on your blog your funnel will need: Squeeze Page, Thank You Page, Oto Page, White List Page, and all the Legal Pages</li>
                           <li>"Wp Easy Optins" will give you the url's you'll need to go to your Autoresponder and set up a campaign.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #2 Section End -->
      <!-- Bonus #3 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w600">Bonus 3</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus3.webp" class="img-fluid mx-auto d-block " alt="Bonus3">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w600 lh140 bonus-title-color text-capitalize">
                        Multiple Product Funnel
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>PLR Audio On How to Create a Complete Product Funnel in Your Business - An Amazing Overview!</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #3 Section End -->
      <!-- Bonus #4 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w600">Bonus 4</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="assets/images/bonus4.webp" class="img-fluid mx-auto d-block " alt="Bonus4">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w600 lh140 bonus-title-color text-capitalize">
                        How To Create A Product Funnel Fast
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li> <b> This is imperative, because if you don't have a product line, you are leaving money on the table. </b> </li>
                           <li>You see, when someone buys from me one time, they often buy again and again and again - and if you don't have enough products in your funnel, they will buy from someone else. </li>
                           <li>Solve this fast!</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #4 End -->
      <!-- Bonus #5 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w600">Bonus 5</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus5.webp" class="img-fluid mx-auto d-block " alt="Bonus5">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w600 lh140 bonus-title-color text-capitalize">
                        WP Profit Page Creator
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li><b>Discover a Brand New and Highly Profitable WP Plugin that Once You Fire it Up, Will Generate You Endless Sales!</b> </li>
                        <li>If you are an online entrepreneur and you want to make so much profits out from your internet business, this amazing WordPress plugin is a huge help to you.</li>
                        <li>What this software does is that, it will create SEO friendly money making WordPress pages almost every with a little content.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #5 End -->
      <!-- CTA Button Section Start -->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w600">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w600 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <!-- <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                  Use Coupon Code <span class="w600 red-clr">"AtulVIP"</span> for an Additional Discount on Commercial Licence
                  </div> -->
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                  Use Coupon Code <span class="w600 red-clr">"ASHUSPECIAL"</span> for an Additional <span class="w600 red-clr">$3 Discount</span> on FE Deal
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab QuickFunnel + My 20 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                  Use Coupon Code <span class="w600 red-clr">"ASHUBUNDLE"</span> for an Additional <span class="w600 red-clr">$50 Discount</span> on Bundle Deal
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="https://jvz2.com/c/47069/392382/" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab QuickFunnel Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald w600">00</span>
                        <span class="f-14 f-md-18 w500 ">Days</span>
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald w600">00</span>
                        <span class="f-14 f-md-18 w500 ">Hours</span>
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald w600">00</span>
                        <span class="f-14 f-md-18 w500 ">Mins</span>
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald w600">00</span>
                        <span class="f-14 f-md-18 w500 ">Sec</span>
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->
     <!-- Bonus #6 Start -->
     <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w600">BONUS 6</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="assets/images/bonus6.webp" class="img-fluid mx-auto d-block " alt="Bonus6">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w600 lh140 bonus-title-color text-capitalize">
                        Affiliate Marketing Manager Software
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li> <b>12,590+ PLR Products Which You Can Resell And Keep 100% Of The Profits!</b></li> 
                           <li>We feature over 12,500 products with Private Label and Resale rights licenses, which mean that you can SELL, EDIT or even CLAIM the product inside as your own!</li>
                           <li>We've been helping THOUSANDS Of Webmasters & Marketers Start & Grow their own Internet Business... Today Is YOUR Turn!</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #6 End -->
      <!-- Bonus #7 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w600">BONUS 7</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus7.webp" class="img-fluid mx-auto d-block " alt="Bonus7">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w600 lh140 bonus-title-color text-capitalize">
                        Software Profit Mastery
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li><b>Why You Should Get Into The Lucrative Software Business?!</b></li>
                           <li>If you are a serious entrepreneur, venturing other types of business model is mostly open to you. And the fact is that, considering the boom of online business model, tapping into the software business platform is also a good choice to start with.</li>
                           <li>The thing is that, if you are not familiar with the environment, inside this video series are the necessary information that you need for you to study the ins and outs of this new type of online business model.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #7 End -->
      <!-- Bonus #8 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w600">BONUS 8</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus8.webp" class="img-fluid mx-auto d-block " alt="Bonus8">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w600 lh140 bonus-title-color text-capitalize">
                        YT Rank Analyzer
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li><b>Discover How to Dominate YouTube And Build MASSIVE Targeted Lists For FREE... By Using Software To Do ALL the Dirty Work!</b></li> 
                           <li>YouTube is now the second largest search engine and is the thirds most visited website in the world. If you are not into YouTube Marketing, then you waste a huge opportunity to attract more traffic and leads to subscribe your list.</li> 
                           <li>The good news is that inside this product is an amazing tool that will help you do the technical stuff and make your campaign effort more productive and scalable.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #8 End -->
      <!-- Bonus #9 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w600">BONUS 9</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus9.webp" class="img-fluid mx-auto d-block " alt="Bonus9">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w600 lh140 bonus-title-color text-capitalize">
                        Domains and Affiliate Marketing
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li><b>Learn More About Domains and Affiliate Marketing!</b></li>
                           <li>Most affiliate marketers are no stranger to the domain name market. At the very least, they probably have a domain name for their website or blog. And some have dozens of domains in their possession.</li>
                           <li>There are a few ways that domains can be used in affiliate marketing. Many affiliates create niche sites or blogs for the sole purpose of promoting affiliate programs. Some also purchase domains to redirect to their affiliate links. This makes the URL shorter, easier to remember and more appealing.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #9 End -->
      <!-- Bonus #10 start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w600">BONUS 10</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus10.webp" class="img-fluid mx-auto d-block " alt="Bonus10">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w600 lh140 bonus-title-color text-capitalize">
                        Boost Your Website Traffic
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>Businesses both large and small are always hoping that their target audience will be able to find their site among the thousands of websites they are competing against. </li>
                           <li>One of the best ways to do this is to utilize the free and paid methods for boosting website traffic. </li>
                           <li>However, like so many online marketing methods, it isn’t always clear on how to do this. Finding an effective way to boost the traffic to your website can not only be confusing, but it can also be a bit frustrating.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #10 End -->
      <!-- CTA Button Section Start -->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w600">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w600 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <!-- <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                  Use Coupon Code <span class="w600 red-clr">"AtulVIP"</span> for an Additional Discount on Commercial Licence
                  </div> -->
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                  Use Coupon Code <span class="w600 red-clr">"ASHUSPECIAL"</span> for an Additional <span class="w600 red-clr">$3 Discount</span> on FE Deal
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab QuickFunnel + My 20 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                  Use Coupon Code <span class="w600 red-clr">"ASHUBUNDLE"</span> for an Additional <span class="w600 red-clr">$50 Discount</span> on Bundle Deal
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="https://jvz2.com/c/47069/392382/" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab QuickFunnel Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 smmltd">Days</span> </div>
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                     <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                     <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->
    <!-- Bonus #11 start -->
    <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w600">BONUS 11</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus11.webp" class="img-fluid mx-auto d-block " alt="Bonus13">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w600 lh140 bonus-title-color text-capitalize">
                        Facebook Pixel Insert WP Plugin
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b> Facebook is one of great way to get traffic to your website and to your promotions. The problem is that a lot of people are using Facebook the wrong way.</b></li>   
                           <li>What there doing is they're running a general ad that are targeting audience such people in the US, people in New Zealand and other countries. And this results to lower conversion rate and high cost per result. So what you need to do in your ad is by using retargetting. </li>
                           <li>Add a Facebook tracking pixel to all of your WordPress site pages and posts in seconds and start taking advantage of retargeting to increase your sales and conversions. </li>
                           </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #11 End -->
      <!-- Bonus #12 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w600">BONUS 12</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus12.webp" class="img-fluid mx-auto d-block " alt="Bonus12">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w600 lh140 bonus-title-color text-capitalize">
                        The Social Media Traffic
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>Get Traffic From Social Media Platforms Using These Top 10 Tips!</li>
                           <li>Social media has the potential to become the largest source of online traffic for any company. That distinction is now claimed by search engines.</li>
                           <li>While search engines do have a wider footprint and record more activity than social media, the ability of companies to reach out to their target audience and in person is what makes social media special.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #12 End -->
      <!-- Bonus #13 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w600">BONUS 13</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus13.webp" class="img-fluid mx-auto d-block " alt="Bonus13">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w600 lh140 bonus-title-color text-capitalize">
                        Social Media Boom Software
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>12,590+ PLR Products Which You Can Resell And Keep 100% Of The Profits!</li>
                           <li>#1 PLR membership Since 2008, 100k members and growing, Updated almost DAILY</li>
                           <li>We feature over 12,500 products with Private Label and Resale rights licenses, which mean that you can SELL, EDIT or even CLAIM the product inside as your own!</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #13 End -->
      <!-- Bonus #14 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w600">BONUS 14</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus14.webp" class="img-fluid mx-auto d-block " alt="Bonus14">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w600 lh140 bonus-title-color text-capitalize">
                        Xyber Email Assistant Software
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                          <li> <b>Do Your Customer Support with Ease Using Xyber Email Assistant! </b> </li>
                          <li>If you are a current online business, customer support is necessary. This is because you can't be so sure that your business will work perfectly!</li>
                          <li>  The good news though is that, you can now turbo-charge the growth of your business, while freeing Up Your Time With Reduced Customer Support Hassles! Stay On Top Of Your Business With The Ability To Instantly Respond To Email For More Satisfied Customers, Affiliates, and Partners!</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #14 End -->
      <!-- Bonus #15 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w600">BONUS 15</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus15.webp" class="img-fluid mx-auto d-block " alt="Bonus15">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w600 lh140 bonus-title-color text-capitalize">
                        WP SEO Track Plugin
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                          <li><b> With this simple plugin you can get the true insight on your web traffic efforts in only seconds! </b>Watch as your social network shares increase, your google PageRank and more.</li> 
                          <li>Get a clear vision on what you need to start focusing on with your SEO efforts. Start more effective backlinks from Facebook, Twitter, Google and more.</li> 
                          <li>You will get all of the most important stats that you need to know for your SEO web traffic. Focus on the amount of shares you have on popular social sites like Facebook, Twitter, Stumble Upon and more.</li> 
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #15 End -->
      
      <!-- CTA Button Section Start -->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w600">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w600 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <!-- <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                  Use Coupon Code <span class="w600 red-clr">"AtulVIP"</span> for an Additional Discount on Commercial Licence
                  </div> -->
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                  Use Coupon Code <span class="w600 red-clr">"ASHUSPECIAL"</span> for an Additional <span class="w600 red-clr">$3 Discount</span> on FE Deal
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab QuickFunnel + My 20 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                  Use Coupon Code <span class="w600 red-clr">"ASHUBUNDLE"</span> for an Additional <span class="w600 red-clr">$50 Discount</span> on Bundle Deal
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="https://jvz2.com/c/47069/392382/" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab QuickFunnel Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 smmltd">Days</span> </div>
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                     <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                     <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->
<!-- Bonus #16 start -->
<div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w600">BONUS 16</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus16.webp" class="img-fluid mx-auto d-block " alt="Bonus16">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w600 lh140 bonus-title-color text-capitalize">
                        Automated Traffic Bot
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>The Biggest Yahoo! Secret That No One Is Telling You & How To Snag Thousands Of Visitors Using This Simple Trick!</b></li>   
                           <li>Would you like to increase your search Engine Rankings without complicated SEO? If you are tired of all the complicated search engine information out there, you're far from alone!</li>
                           <li>In fact, most people just plain give up on SEO because it's so complicated. Worse yet, the majority of people who do follow through with SEO usually see little to no results after all that work! </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #16 End -->
      <!-- Bonus #17 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w600">BONUS 17</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus17.webp" class="img-fluid mx-auto d-block " alt="Bonus17">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w600 lh140 bonus-title-color text-capitalize">
                        WP BotBlocker Plugin
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>Stop your wordpress site from being attacked by hackers using bots to try and bring down your site! </b></li>
                           <li>Website hacking has been an issue for many WordPress users for several years now. That's why WordPress Developers are making security plugins to defend WordPress sites.</li>
                           <li>If you are having the same problem, chances are you might want to also install this amazing WordPress called WP BotBlocker with will block any bots that will attempt to do brute entry of your website's backend. </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #17 End -->
      <!-- Bonus #18 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w600">BONUS 18</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus18.webp" class="img-fluid mx-auto d-block " alt="Bonus18">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w600 lh140 bonus-title-color text-capitalize">
                        Website Monitor
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li> <b>Automatically Know If Your Websites Go Down With Just 1 Click!</b></li>
                           <li>If you are having an online business, your website would be your best asset to market your services or products on the internet right?</li>
                           <li>The thing is that even if you are an I.T. or you have a person working for you as an I.T. for your online business, you still can't monitor your website 24/7 when it will go down.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #18 End -->
      <!-- Bonus #19 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w600">BONUS 19</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus19.webp" class="img-fluid mx-auto d-block " alt="Bonus19">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w600 lh140 bonus-title-color text-capitalize">
                        BIZ Landing Page Plugin
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                          <li> <b>You probably have noticed that most businesses online are listed in directories such as YellowPages only... Now with one wordpress plugin you can create an all-in-one website that will pull in multiple sources and display in one place.</b></li>
                          <li>This is a stand alone plugin that will create a business website in one landing page. Add tabbed content to keep your visitors staying on one page!</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #19 End -->
      <!-- Bonus #20 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w600">BONUS 20</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus20.webp" class="img-fluid mx-auto d-block " alt="Bonus20">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w600 lh140 bonus-title-color text-capitalize">
                        Seo Revolution
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                          <li><b> Today, only the smartest search engine optimizers with latest SEO techniques and strategies and the best content writers reach the pedestal where they are the number one hit on the SERPs. </b></li> 
                          <li>If you are thinking about revamping your SEO strategy, here is an excellent opportunity to attract tons of website traffic. </li> 
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #20 End -->
      <!-- Huge Woth Section Start -->
      <div class="huge-area mt30 mt-md10">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="f-md-65 f-40 lh120 w600 white-clr">That's Huge Worth of</div>
                  <br>
                  <div class="f-md-60 f-40 lh120 w800 yellow-clr">$3300!</div>
               </div>
            </div>
         </div>
      </div>
      <!-- Huge Worth Section End -->
      <!-- text Area Start -->
      <div class="white-section pb0">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="f-md-36 f-25 lh140 w500">So what are you waiting for? You have a great opportunity <br class="hidden-xs"> ahead + <span class="w600 ">My 20 Bonus Products</span> are making it a <br class="hidden-xs"> <span class="w600 ">completely NO Brainer!!</span></div>
               </div>
            </div>
         </div>
      </div>
      <!-- text Area End -->
      <!-- CTA Button Section Start -->
      <div class="cta-btn-section">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center">
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 black-clr mt-md30">
                     Use Coupon Code <span class="w600 red-clr">"ASHUSPECIAL"</span> for an Additional <span class="w600 red-clr">$3 Discount</span> on FE Deal
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab QuickFunnel + My 20 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 black-clr mt-md30">
                  Use Coupon Code <span class="w600 red-clr">"ASHUBUNDLE"</span> for an Additional <span class="w600 red-clr">$50 Discount</span> on Bundle Deal
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="https://jvz2.com/c/47069/392382/" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab QuickFunnel Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-12 mt15 mt-md20" align="center">
                  <h3 class="f-md-28 f-20 w500 text-center black">My Exclusive Bonuses Are Expiring in...</h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 col-md-10 mx-auto col-12 text-center">
                  <div class="countdown counter-black">
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                     <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                     <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->
      <div class="footer-section" style="font-size: 14px;">
         <div class="container " style="font-size: 14px;">
            <div class="row" style="font-size: 14px;">
               <div class="col-12 text-center" style="font-size: 14px;">
               <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 693.1 109.7" style="enable-background:new 0 0 693.1 109.7; height:45px;" xml:space="preserve">
                        <style type="text/css">
                           .st0{fill:#FFFFFF;}
                           .st1{fill:#EF3E39;}
                           .st2{fill:#853594;}
                           .st3{fill:#EF3E3A;}
                        </style>
                        <g>
	                        <g id="Layer_1-2">
		                        <path class="st0" d="M140.4,0H18.9c-2.8-0.1-5.2,2.1-5.4,4.9s2.1,5.2,4.9,5.4c0.2,0,0.3,0,0.5,0h42.3c2.8,0,5.1,2.3,5.1,5.1
                              c0,2.8-2.3,5.1-5.1,5.1h-56c-2.8,0.1-5,2.5-4.9,5.4c0.1,2.7,2.3,4.8,4.9,4.9h35.6l0,0h13.5c2.8,0.1,5,2.5,4.9,5.4
                              c-0.1,2.7-2.3,4.8-4.9,4.9H46c-2.8,0-5.1,2.4-5.1,5.2c0,2.7,2.1,4.9,4.8,5.1h17.8c2.8,0,5.2,2.2,5.2,5.1c0,2.8-2.2,5.2-5.1,5.2h-3
                              c-1.3,0-2.6,0.6-3.5,1.5c-2,2-2,5.2-0.1,7.3c1,1,2.3,1.5,3.7,1.5h8v37.7L103.1,96V68.6l42.8-57.6c2.3-3,1.6-7.3-1.4-9.6
                              C143.3,0.5,141.9,0,140.4,0z M66.2,36c0-2.8,2.3-5.1,5.1-5.1h10c2.8,0.1,5,2.5,4.9,5.4c-0.1,2.7-2.3,4.8-4.9,4.9h-10
                              C68.5,41.1,66.2,38.8,66.2,36z"></path>
		                        <path class="st1" d="M34,46.3c0,2.8-2.3,5.1-5.1,5.1h-10c-2.8,0-5.1-2.3-5.1-5.1c0-2.8,2.3-5.1,5.1-5.1c0,0,0,0,0,0h10
			                     C31.7,41.2,34,43.5,34,46.3z"></path>
		                        <path class="st1" d="M48.8,66.9c0,2.8-2.3,5.1-5.1,5.1h-2.9c-2.8,0-5.1-2.1-5.3-4.8c-0.1-1.5,0.4-3,1.5-4c0.9-0.9,2.2-1.5,3.5-1.5
			                     h3.4C46.6,61.8,48.9,64.1,48.8,66.9z"></path>
                              <path class="st2" d="M45.8,61.7L45.8,61.7z"></path>
                              <path class="st2" d="M63.7,61.7L63.7,61.7z"></path>
                              <path class="st2" d="M63.7,61.7L63.7,61.7z"></path>
                              <path class="st3" d="M230,48.3c0,4.5-0.7,8.9-2.3,13.1c-1.5,3.9-3.7,7.5-6.5,10.5c-2.8,3-6.1,5.4-9.9,7c-8,3.3-17,3.3-24.9,0
                              c-3.7-1.6-7.1-4-9.8-7c-2.8-3.1-5-6.7-6.4-10.5c-1.6-4.2-2.4-8.7-2.3-13.1c0-4.5,0.7-9,2.3-13.2c1.4-3.9,3.6-7.5,6.4-10.5
                              c2.8-3,6.1-5.4,9.8-7c8-3.3,17-3.3,24.9,0c3.7,1.6,7.1,4,9.9,7c2.8,3.1,5,6.6,6.5,10.5C229.3,39.3,230,43.8,230,48.3z M222.1,48.3
                              c0.1-4.6-0.9-9.2-3-13.4c-1.9-3.7-4.7-6.9-8.2-9.1c-7.4-4.4-16.6-4.4-24,0c-3.5,2.3-6.3,5.4-8.2,9.1c-2.1,4.2-3.1,8.7-3,13.4
                              c-0.1,4.6,1,9.2,3,13.3c1.8,3.7,4.7,6.9,8.2,9.2c7.4,4.4,16.6,4.4,24,0c3.5-2.3,6.3-5.4,8.2-9.2C221.1,57.5,222.1,52.9,222.1,48.3
                              z M233.1,84.8c1.9,0.1,3.4,1.6,3.4,3.5c-0.1,1.3-0.7,2.5-1.7,3.4c-1.3,1.2-2.8,2.2-4.4,2.9c-1.8,0.9-3.7,1.5-5.7,2
                              c-1.8,0.5-3.7,0.7-5.6,0.7c-3.1,0.1-6.2-0.3-9.2-1.2c-2.1-0.7-4.2-1.5-6.2-2.6c-1.8-1-3.5-1.9-5.2-2.6c-1.9-0.8-3.9-1.2-6-1.2
                              c-1,0-2,0.2-2.9,0.6c-0.8,0.4-1.6,0.7-2.3,1.1c-0.8,0.4-1.7,0.6-2.6,0.6c-1,0-2-0.4-2.7-1.1c-0.6-0.7-1-1.6-1-2.5
                              c-0.1-1.4,0.8-2.8,2.1-3.2L200,79l8.4,0.8l-15.7,5.5l2.6-1.9c2.2,0,4.4,0.3,6.4,1.1c1.9,0.7,3.7,1.5,5.5,2.5
                              c1.8,0.9,3.6,1.8,5.5,2.5c2.1,0.8,4.4,1.1,6.6,1.1c1.8,0,3.7-0.3,5.4-1c1.2-0.5,2.4-1.2,3.6-1.9c0.8-0.6,1.7-1.3,2.4-2
                              C231.3,85.1,232.1,84.8,233.1,84.8L233.1,84.8z"></path>
		                        <path class="st3" d="M277.1,32.9c1,0,2,0.4,2.7,1.1c0.7,0.7,1,1.7,1,2.6v25.1c0,6.2-1.7,11-5.2,14.4s-8.3,5.1-14.5,5.1
                              c-6.2,0-11-1.7-14.4-5.1s-5.2-8.2-5.2-14.4V36.6c0-2,1.5-3.7,3.5-3.7c0,0,0.1,0,0.1,0c1,0,2,0.4,2.7,1.1c0.7,0.7,1,1.7,1,2.6v25.1
                              c0,4.2,1.1,7.3,3.2,9.5s5.2,3.2,9,3.2c4,0,7.1-1.1,9.2-3.2s3.2-5.3,3.2-9.5V36.6c0-2,1.5-3.7,3.5-3.7
                              C277,32.9,277,32.9,277.1,32.9L277.1,32.9z"></path>
		                        <path class="st3" d="M297.9,26c-1.2,0.1-2.4-0.2-3.4-0.9c-0.8-0.7-1.2-1.8-1.1-2.9v-1.3c-0.1-1.1,0.4-2.2,1.2-2.9
                              c1-0.7,2.2-1,3.4-0.9c1.2-0.1,2.4,0.2,3.3,0.9c0.8,0.7,1.2,1.8,1.1,2.9v1.3c0.1,1.1-0.3,2.1-1.1,2.9
                              C300.2,25.8,299.1,26.1,297.9,26z M301.5,76.8c0,1-0.4,2-1.1,2.7c-1.4,1.5-3.8,1.5-5.3,0c0,0,0,0,0,0c-0.7-0.7-1-1.7-1-2.7V35.9
                              c-0.1-2,1.5-3.8,3.5-3.9s3.8,1.5,3.9,3.5c0,0.1,0,0.3,0,0.4L301.5,76.8z"></path>
		                        <path class="st3" d="M336,31.7c2.9,0,5.7,0.3,8.5,1c2.2,0.5,4.2,1.5,6,2.8c1.3,0.9,2.2,2.4,2.2,4c0,0.8-0.3,1.6-0.8,2.3
                              c-0.5,0.7-1.3,1.1-2.2,1.1c-0.7,0-1.4-0.1-2.1-0.5c-0.5-0.3-1-0.7-1.5-1.2c-0.5-0.5-1.2-1-1.9-1.3c-1-0.5-2.1-0.8-3.2-1
                              c-1.4-0.2-2.8-0.4-4.2-0.4c-3.1-0.1-6.2,0.8-8.9,2.4c-2.5,1.6-4.6,3.8-6,6.5c-1.5,2.8-2.3,6-2.2,9.2c-0.1,3.2,0.7,6.4,2.1,9.2
                              c1.4,2.6,3.4,4.9,5.9,6.4c2.6,1.6,5.7,2.5,8.8,2.4c1.7,0,3.5-0.1,5.2-0.6c1.1-0.3,2.2-0.7,3.2-1.3c0.9-0.5,1.8-1.1,2.5-1.9
                              c1.4-1.3,3.5-1.2,4.8,0.1c0.6,0.7,0.9,1.6,0.9,2.5c0,1.1-0.8,2.3-2.3,3.5c-1.9,1.4-4,2.5-6.2,3.1c-2.9,0.9-5.9,1.3-8.9,1.3
                              c-4.3,0.1-8.6-1-12.3-3.3c-3.5-2.2-6.3-5.3-8.2-9c-2-3.9-3-8.3-2.9-12.7c-0.1-4.4,1-8.7,3-12.6C319.3,36.2,327.3,31.5,336,31.7z"></path>
		                        <path class="st3" d="M367.1,80.6c-1,0-2-0.4-2.7-1.1c-0.7-0.7-1-1.7-1-2.7V16c0-1,0.3-1.9,1-2.6c0.7-0.7,1.7-1.1,2.7-1.1
                              c2,0,3.7,1.5,3.7,3.5c0,0,0,0.1,0,0.1v60.8c0,1-0.4,2-1.1,2.7C369,80.2,368.1,80.6,367.1,80.6z M397.7,32.6c1,0,1.9,0.4,2.5,1.2
                              c1.3,1.2,1.4,3.2,0.2,4.6c-0.2,0.2-0.4,0.4-0.6,0.5L370,65l-0.3-8.6l25.5-22.7C395.8,33,396.7,32.6,397.7,32.6z M398.7,80.5
                              c-1,0-2-0.4-2.7-1.2l-19.7-20.9l5.5-5.1l19.5,20.8c0.7,0.7,1.1,1.7,1.1,2.8c0,1-0.4,2-1.3,2.6C400.4,80.1,399.6,80.4,398.7,80.5
                              L398.7,80.5z"></path>
		                        <path class="st0" d="M419,80.6c-1.6,0.1-3.1-0.5-4.2-1.6c-1.1-1.1-1.6-2.5-1.6-4V21.7c-0.1-3,2.3-5.6,5.3-5.7c0.1,0,0.2,0,0.3,0
                              h30c1.5,0,2.9,0.5,4,1.5c1.1,1,1.7,2.4,1.6,3.9c0,1.4-0.6,2.7-1.6,3.6c-1.1,1-2.5,1.6-4,1.6h-24.9l0.8-1.1v18.6l-0.6-1.2h20.6
                              c1.5,0,2.9,0.5,4,1.5c1.1,1,1.7,2.4,1.6,3.9c0,1.4-0.6,2.7-1.6,3.6c-1.1,1-2.5,1.6-4,1.6h-20.8l0.8-0.8V75c0,1.5-0.6,3-1.7,4
                              C421.9,80,420.5,80.6,419,80.6z"></path>
		                        <path class="st0" d="M499.3,31.3c1.5-0.1,3,0.5,4,1.6c1,1.1,1.6,2.5,1.6,4v23.4c0,6.5-1.8,11.7-5.4,15.5
                              c-3.6,3.8-8.9,5.7-15.7,5.7c-6.8,0-12-1.9-15.6-5.7s-5.4-9-5.4-15.5V36.9c0-1.5,0.5-2.9,1.6-4c2.2-2.2,5.7-2.2,7.9,0
                              c1,1.1,1.6,2.5,1.6,4v23.4c0,3.8,0.8,6.5,2.5,8.3s4.1,2.7,7.5,2.7s5.9-0.9,7.6-2.7s2.5-4.6,2.5-8.3V36.9c0-1.5,0.5-2.9,1.6-4
                              C496.4,31.8,497.8,31.2,499.3,31.3z"></path>
		                        <path class="st0" d="M543.2,30.3c4.6,0,8,1,10.4,2.9c2.4,1.9,4.1,4.6,4.9,7.6c0.9,3.4,1.4,7,1.3,10.6V75c0,1.5-0.5,2.9-1.6,4
                              c-2.2,2.2-5.7,2.2-7.9,0c-1-1.1-1.6-2.5-1.6-4V51.3c0-1.9-0.2-3.7-0.8-5.5c-0.5-1.6-1.5-2.9-2.8-3.9c-1.7-1.1-3.8-1.6-5.8-1.5
                              c-2.2-0.1-4.3,0.4-6.2,1.5c-1.6,0.9-3,2.3-3.9,3.9c-0.9,1.7-1.4,3.6-1.3,5.5V75c0,1.5-0.5,2.9-1.6,4c-2.2,2.2-5.7,2.2-7.9,0
                              c-1-1.1-1.6-2.5-1.6-4V36.9c0-1.5,0.5-2.9,1.6-4c2.2-2.2,5.7-2.2,7.9,0c1,1.1,1.6,2.5,1.6,4v4l-1.4-0.3c0.7-1.2,1.5-2.3,2.4-3.4
                              c1.1-1.3,2.3-2.4,3.7-3.4c1.5-1.1,3.1-1.9,4.8-2.5C539.2,30.7,541.2,30.3,543.2,30.3z"></path>
		                        <path class="st0" d="M598.1,30.3c4.5,0,8,1,10.4,2.9c2.4,2,4.1,4.6,4.9,7.6c0.9,3.4,1.4,7,1.3,10.6V75c0,1.5-0.5,2.9-1.6,4
                              c-2.2,2.2-5.7,2.2-7.9,0c-1-1.1-1.6-2.5-1.6-4V51.3c0-1.9-0.2-3.7-0.8-5.5c-0.5-1.6-1.5-2.9-2.8-3.9c-1.7-1.1-3.8-1.6-5.8-1.5
                              c-2.2-0.1-4.3,0.4-6.3,1.5c-1.6,0.9-3,2.3-3.9,3.9c-0.9,1.7-1.4,3.6-1.3,5.5V75c0,1.5-0.5,2.9-1.6,4c-2.2,2.2-5.7,2.2-7.9,0
                              c-1-1.1-1.6-2.5-1.6-4V36.9c0-1.5,0.5-2.9,1.6-4c2.2-2.2,5.7-2.2,7.9,0c1,1.1,1.6,2.5,1.6,4v4l-1.4-0.3c0.7-1.2,1.5-2.3,2.4-3.4
                              c1.1-1.3,2.3-2.4,3.7-3.4c1.5-1.1,3.1-1.9,4.8-2.5C594.2,30.7,596.1,30.3,598.1,30.3z"></path>
		                        <path class="st0" d="M649.9,81.5c-4.7,0.1-9.4-1-13.6-3.3c-3.7-2.1-6.8-5.2-8.8-8.9c-2.1-3.9-3.2-8.3-3.1-12.7
                              c-0.1-4.9,1-9.8,3.4-14.2c2.1-3.7,5.1-6.8,8.8-9c3.5-2,7.5-3.1,11.5-3.1c3.1,0,6.1,0.6,8.9,1.9c2.8,1.3,5.3,3.1,7.4,5.3
                              c2.2,2.3,3.9,4.9,5.1,7.8c1.3,3,1.9,6.2,1.9,9.4c0,1.4-0.7,2.7-1.8,3.6c-1.1,0.9-2.5,1.4-3.9,1.4h-35.2l-2.8-9.2h33.8l-2,1.8v-2.5
                              c-0.1-1.8-0.8-3.4-1.9-4.8c-1.1-1.4-2.6-2.6-4.2-3.4c-1.7-0.8-3.5-1.3-5.4-1.3c-1.8,0-3.5,0.2-5.2,0.7c-1.6,0.5-3,1.3-4.1,2.5
                              c-1.3,1.3-2.3,2.9-2.8,4.7c-0.7,2.4-1.1,4.9-1,7.5c-0.1,2.9,0.6,5.9,2.1,8.4c1.3,2.2,3.1,4,5.3,5.3c2.1,1.2,4.6,1.8,7,1.8
                              c1.8,0,3.7-0.1,5.4-0.6c1.1-0.3,2.3-0.7,3.3-1.3c0.8-0.5,1.6-1,2.3-1.3c1-0.5,2-0.8,3.1-0.8c2.6,0,4.7,2,4.7,4.6c0,0,0,0,0,0.1
                              c-0.1,1.8-1,3.5-2.6,4.5c-2,1.6-4.3,2.8-6.8,3.6C656,81,653,81.5,649.9,81.5z"></path>
		                        <path class="st0" d="M693.1,75c0.1,3.1-2.4,5.6-5.4,5.6c-0.1,0-0.1,0-0.2,0c-1.5,0-2.9-0.6-3.9-1.6c-1.1-1.1-1.6-2.5-1.6-4V18
			                     c-0.1-3.1,2.4-5.6,5.4-5.6c0.1,0,0.1,0,0.2,0c1.5-0.1,2.9,0.5,3.9,1.6c1,1.1,1.6,2.5,1.5,4V75z"></path>
	                        </g>
                        </g>
                     </svg>
                  <div class="f-16 f-md-18 w300 mt20 lh150 white-clr text-center" style="font-size: 16px;">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40" style="font-size: 14px;">
                  <div class="f-16 f-md-18 w300 lh150 white-clr text-xs-center" style="font-size: 16px;">Copyright © QuickFunnel 2023</div>
                  <ul class="footer-ul f-16 f-md-18 w300 white-clr text-center text-md-right">
                     <li style="font-size: 16px;"><a href="https://support.oppyo.com/hc/en-us" class="white-clr t-decoration-none" style="font-size: 16px;">Contact</a> <span class="px5" style="font-size: 16px;">|</span> </li>
                     <li style="font-size: 16px;"><a href="http://quickfunnel.live/legal/privacy-policy.html" class="white-clr t-decoration-none" style="font-size: 16px;">Privacy</a> <span class="px5" style="font-size: 16px;">|</span> </li>
                     <li style="font-size: 16px;"><a href="http://quickfunnel.live/legal/terms-of-service.html" class="white-clr t-decoration-none" style="font-size: 16px;">T&amp;C</a> <span class="px5" style="font-size: 16px;">|</span> </li>
                     <li style="font-size: 16px;"><a href="http://quickfunnel.live/legal/disclaimer.html" class="white-clr t-decoration-none" style="font-size: 16px;">Disclaimer</a> <span class="px5" style="font-size: 16px;">|</span> </li>
                     <li style="font-size: 16px;"><a href="http://quickfunnel.live/legal/gdpr.html" class="white-clr t-decoration-none" style="font-size: 16px;">GDPR</a> <span class="px5" style="font-size: 16px;">|</span> </li>
                     <li style="font-size: 16px;"><a href="http://quickfunnel.live/legal/dmca.html" class="white-clr t-decoration-none" style="font-size: 16px;">DMCA</a> <span class="px5" style="font-size: 16px;">|</span> </li>
                     <li style="font-size: 16px;"><a href="http://quickfunnel.live/legal/anti-spam.html" class="white-clr t-decoration-none" style="font-size: 16px;">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!-- timer --->
      <?php
         if ($now < $exp_date) {
         ?>
      <script type="text/javascript">
         // Count down milliseconds = server_end - server_now = client_end - client_now
         var server_end = <?php echo $exp_date; ?> * 1000;
         var server_now = <?php echo time(); ?> * 1000;
         var client_now = new Date().getTime();
         var end = server_end - server_now + client_now; // this is the real end time
         
         var noob = $('.countdown').length;
         
         var _second = 1000;
         var _minute = _second * 60;
         var _hour = _minute * 60;
         var _day = _hour * 24
         var timer;
         
         function showRemaining() {
         var now = new Date();
         var distance = end - now;
         if (distance < 0) {
         	clearInterval(timer);
         	document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
         	return;
         }
         
         var days = Math.floor(distance / _day);
         var hours = Math.floor((distance % _day) / _hour);
         var minutes = Math.floor((distance % _hour) / _minute);
         var seconds = Math.floor((distance % _minute) / _second);
         if (days < 10) {
         	days = "0" + days;
         }
         if (hours < 10) {
         	hours = "0" + hours;
         }
         if (minutes < 10) {
         	minutes = "0" + minutes;
         }
         if (seconds < 10) {
         	seconds = "0" + seconds;
         }
         var i;
         var countdown = document.getElementsByClassName('countdown');
         for (i = 0; i < noob; i++) {
         	countdown[i].innerHTML = '';
         
         	if (days) {
         		countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + days + '</span><span class="f-14 f-md-18 smmltd">Days</span> </div>';
         	}
         
         	countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + hours + '</span><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>';
         
         	countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">' + minutes + '</span><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>';
         
         	countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">' + seconds + '</span><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>';
         }
         
         }
         timer = setInterval(showRemaining, 1000);
         	
      </script>
      <?php
         } else {
         echo "Times Up";
         }
         ?>
      <!--- timer end-->
   </body>
</html>
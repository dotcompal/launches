<!DOCTYPE html>
<html lang="en-US">
<!--<![endif]-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <title>SendPro</title>
    <link rel="shortcut icon" href="images/favicon.png" />
    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    <!-- CSS Links -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" type="text/css" />
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="css/general.css" />
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/custom-styles.css">
    <link rel="stylesheet" href="css/aos.css">
    <link rel="stylesheet" href="css/timer.css">

    <!-- New Timer  Start-->
    <?php
	$date = 'May 15 2022 7:58 AM EST';
	$exp_date = strtotime($date);
	$now = time();  
	/*
	
	$date = date('F d Y g:i:s A eO');
	$rand_time_add = rand(700, 1200);
	$exp_date = strtotime($date) + $rand_time_add;
	$now = time();*/
	
	if ($now < $exp_date) {
	?>
        <?php
	} else {
	 echo "Times Up";
	}
	?>
            <!-- New Timer End -->
</head>

<body>
    <!-- Header -->
    <div class="header">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-12 text-center">
                    <div class="header-tag f-20 f-md-45 w700">Ayush Jain's Special: Great Grand Bonus For Ayush Family</div>
                    <div class="f-20 f-md-32 w500 mt20 mt-md30 red-clr"> Get Free Saas Software with access of SendPro</div>
                    <div class="header-tag f-18 f-md-24 w600 mt20 mt-md30"> Deal For Limited Time Period</div>
                </div>
                <div class="col-12 col-md-6 mt20 mt-md30">

                    <img src="images/logo.png" class="img-fluid d-block mx-auto">

                </div>
                <div class="col-md-6 col-12 mt20 mt-md30">
                    <img src="images/ayush-jain.png" class="img-fluid d-block mx-auto">
                </div>
            </div>

            <div class="row mt20 mt-md50">
                <div class="col-12 text-center ">
                    <div class="pre-headline f-18 f-md-20 w600"> Get Instant Access To Brand New Email System Is Extremely Profitable  AT A LOW, ONE TIME PRICE!</div>
                </div>

                <div class="col-12">
                    <div class="headline text-center w700 f-28 f-md-36 mt20 mt-md30"> Earn $94.68 Consistently Sending Emails With Done-For-You Copy & Paste Email Swipes, Ready-To-Buy Active Leads
                    </div>
                </div>



            </div>
            <div class="row mt30 mt-sm40">
                <div class="col-11 col-md-5 mx-auto w700 f-16 f-md-18 text-center pre-headline">
                    Click To Watch The Power Of SendPro
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md30">
                    <div class="responsive-video">
                        <iframe src="https://www.youtube.com/embed/daO8o0AmbKs?rel=0&showinfo=0&enablejsapi=1&loop=0&controls=1&" frameborder="0" allow="autoplay; accelerometer;  encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- Header end -->








    <!-- Grand Bonuses Start -->
    <div class="strip" style="background:#0c235b;" id="grand">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-12">
                    <div class="text-center f-19 f-sm-45 text-white lh160 margin0 w500">
                        <strong>A Complete Biz Solution As a Great Grand Bonus For The First Time<span class=" f-19 f-md-50 orange-clr"> "DotcomPal"</span> </strong>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Grand Bonuses End -->

    <!-- Semi Grand Bonus start -->
    <div class="bonuses">
        <div class="container">

            <div class="row align-items-center">
                <div class="col-12 col-md-10 mx-auto mb30 mb-md40 text-center">
                    <span class="semi-bonus f-lg-30 f-md-26 f-20 w700 my20 my-md-40">Bonus #1</span>
                    <img src="images/dotcompal.png" class="img-fluid mx-auto d-block my30 my-md50">
                    
                    <div class="f-18 f-sm-20 lh140 w400 text-left mt30">
                        You can start your own Online Business & Grow Your Business in One Place Dotcom Pal is redefining your way of entrepreneur journey.<br><br> You can make
                        <ul class="proudly-tick p0 m0 mt20">
                            <li>Pop-ups</li>
                            <li>landing pages</li>
                            <li>sales pages</li>
                            <li>email autoresponder</li>
                            <li>email scheduling</li>
                            <li>cloud storage for your buyer’s lead and much more.</li>
                        </ul>     
                         
                        <br>
                        <span class="w600">It’s Software as a service, Software</span>  (or SaaS) that can deliver applications over the Internet as a service. Instead of installing and maintaining software, you simply access it via the Internet by freeing
                        yourself from complex software and hardware management.
                        <br><br> It’s a complete one-time solution for customers to minimize their effort and time. You can manage more than 10 + app at the same time.
                        <br><br> Stop spending time, do it all and all in one with DotcomPal with no technical hassle.
                    </div>
                </div>

                <div class="col-md-8 col-11 mx-auto text-center text-white instant_button text-center animated infinite pulse delay-2s mt0 mt-md30 mb30 mb-md50">
                    <a href="https://cutt.ly/7HhxyJ7">
                        <div class="text-center w600 f-sm-24 f-18 lh140">
                        <!-- Special Bonus * "30 Reseller Licenses" If You By Today -->
                        First 150 Action Takers Unlock $250 Daily
                        </div>
                    </a>
                </div>

                <!-- <div class="col-12 text-center">
                    <div class="f-20 w700 f-md-20">
                        <a href="https://cutt.ly/6Hppx9t" class="black remove-a">
							Discount Coupon "LOVEAYUSHVR"
						</a>
                    </div>
                </div> -->
            </div>

            <div class="row mt20 mt-md20">
                <div class="col-12 col-md-8 mx-auto text-center">
                    <div class="f-md-22 f-20 w600 lh140 px0 px-sm15">
                        Hurry Up! Free Offer Going Away in…
                    </div>
                </div>
                <div class="col-12 col-md-6 mx-auto mt-md20 mt20">
                    <div class="countdown counter-black">
                        <div class="timer-label text-center"><span class="f-40 f-md-40 timerbg timerbg">01</span><br><span class="f-16 w500">Days</span> </div>
                        <div class="timer-label text-center"><span class="f-40 f-md-40 timerbg timerbg">16</span><br><span class="f-16 w500">Hours</span> </div>
                        <div class="timer-label text-center"><span class="f-40 f-md-40 timerbg timerbg">59</span><br><span class="f-16 w500">Mins</span> </div>
                        <div class="timer-label text-center "><span class="f-40 f-md-40 timerbg timerbg">37</span><br><span class="f-14 f-md-18 w500 f-16 w500">Sec</span> </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- Semi Grand Bonus End -->


    <!-- Bonuses Start -->
    <div class="bonuses">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-12 mx-auto">
                    <div class="mt-md60 mt30 w600">
                        <div class="text-center">
                            <span class="semi-bonus f-lg-30 f-md-26 f-20 w700">Bonus #2</span>
                            <!-- <span class="sem-gb">Lead Generation On Demand</span> -->
                        </div>
                    </div>
                </div>
            </div>

            <div class="row mt30 mt-sm40 align-items-center">
                <div class="col-12 col-sm-6 mb20 mb-sm0">
                    <img src="images/lead-gen.jpg" class="img-fluid mx-auto d-block" style="max-width:70%;">
                </div>


                <div class="col-12 col-sm-6">
                    <div class="w600 f-lg-30 f-md-26 f-20">
                        Lead Generation On Demand
                    </div>
                    <div class="f-20 f-md-22 lh140 w400 mt30">
                        It doesn’t matter what kind of business you’re in, if you aren’t able to generate new leads and turn them into paying customers, your company will never succeed. 
                        <br><br> 
                        You need to be constantly bringing in new customers if you want your business to thrive.
                        <br><br>
                        Generating more leads is anything but easy and if you don’t have a solid marketing strategy that will drive more traffic to your website, you’ll never be able to generate the leads you need for your business to succeed. 
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-12 col-lg-12 mx-auto">
                    <div class="mt-lg30 mt-md30 mt30 w600">
                        <div class="text-center">
                            <span class="semi-bonus f-lg-30 f-md-26 f-20 w700">Bonus #3</span>
                            <!-- <span class="sem-gb"> VidProfits 2.0</span> -->
                        </div>
                    </div>
                </div>
            </div>


            <div class="row mt30 mt-sm40 align-items-center">
                <div class="col-12 col-sm-6 order-sm-12 mb20 mb-sm0">
                    <img src="images/eturbo.jpg" class="img-fluid mx-auto d-block" style="max-width:70%;">
                </div>


                <div class="col-12 col-sm-6 order-sm-1">
                    <div class="w600 f-lg-30 f-md-26 f-20">
                        Turbo ECom + Addon PRO
                    </div>
                    <div class="f-20 f-md-22 lh140 w400 mt30">
                        Turbo eCom Amazon™ Module For PrestaShop is the latest in speed, ingenuity and grace and gives you the ability to effectively incorporate eCommerce functionality to your PrestaShop site.
                        <br><br> 
                        It is the most flexible, easiest-by-far architecture (especially for newbies!) Module that literally enhances your existing PrestaShop site by fetching Products from Amazon to it with complete ease, essentially turning your shop into money.
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-12 col-lg-12 mx-auto">
                    <div class="mt-lg30 mt-md30 mt30 w600">
                        <div class="text-center">
                            <span class="semi-bonus f-lg-30 f-md-26 f-20 w700">Bonus #4</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row mt30 mt-sm40 align-items-center">
                <div class="col-12 col-sm-6 mb20 mb-sm0">
                    <img src="images/insta.jpg" class="img-fluid mx-auto d-block" style="max-width:70%;">
                </div>


                <div class="col-12 col-sm-6">
                    <div class="w600 f-lg-30 f-md-26 f-20">
                        Instagram Ads Success
                    </div>
                    <div class="f-20 f-md-22 lh140 w400 mt30">
                        There are so many reasons to get excited about the Instagram platform.
                        <br><br>
                        It now has a billion active users and businesses of all kinds are seeing excellent results from setting up a profile and advertising with Instagram.
                        <br><br>
                        Instagram Ads are not the most expensive way to promote your business and brand, but it is not a cheap option either.
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-12 col-lg-12 mx-auto">
                    <div class="mt-lg30 mt-md30 mt30 w600">
                        <div class="text-center">
                            <span class="semi-bonus f-lg-30 f-md-26 f-20 w700">Bonus #5</span>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row mt30 mt-sm40 align-items-center">
                <div class="col-12 col-sm-6 order-sm-12 mb20 mb-sm0">
                    <img src="images/script.jpg" class="img-fluid mx-auto d-block" style="max-width:70%;">
                </div>


                <div class="col-12 col-sm-6 order-sm-1">
                    <div class="w600 f-lg-30 f-md-26 f-20">
                        Scrip Engage Product Review Package
                    </div>
                    <div class="f-20 f-md-22 lh140 w400 mt30">
                        Make Money Online Doing Affiliate Review for Script Engage Product!
                        <br><br>
                        Ever sat down and tried to write high-converting copy for your product or service?
                        <br><br>
                        If you did, you would know just how difficult it can be.  The simple fact is that probably you and me are not copywriters and thus, writing sales copy feels like trying to get water from stone!!
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-8 col-11 mx-auto text-center text-white instant_button text-center animated infinite pulse delay-2s mt30 mt-md30 mb30 mb-md50">
                    <a href="https://cutt.ly/7HhxyJ7">
                        <div class="text-center w600 f-sm-24 f-18 lh140">
                            Grab SendPro With Great Grand Bonus
                        </div>
                    </a>
                </div>
            </div>

            <div class="row">
                <div class="col-12 col-lg-12 mx-auto">
                    <div class="mt-lg30 mt-md30 mt30 w600">
                        <div class="text-center">
                            <span class="semi-bonus f-lg-30 f-md-26 f-20 w700">Bonus #6</span>
                            <!-- <span class="sem-gb">CopyMate OTO1 + Reseller </span> -->
                        </div>
                    </div>
                </div>
            </div>

            <div class="row mt30 mt-sm40 align-items-center">
                <div class="col-12 col-sm-6 mb20 mb-sm0">
                    <img src="images/smart.jpg" class="img-fluid mx-auto d-block">
                </div>


                <div class="col-12 col-sm-6">
                    <div class="w600 f-lg-30 f-md-26 f-20">
                        Smart Agent Pro
                    </div>
                    <div class="f-20 f-md-22 lh140 w400 mt30">
                        Skyrocket Your Conversions, Sales And Profits With Your Own Automated Sales Assistants, Providing 24/7 Support On All Your Websites!
                        <br><br>
                        You might already heard the saying that to become successful, you need to study what successful people do and model their actions!
                        <br><br>
                        Well if you've been following the Internet Marketing scene lately you'll have noticed the growing popularity of those little 'exit chat agent' scripts. All the big name 'guru's' seem to be using them on all their sites.
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 col-lg-12 mx-auto">
                    <div class="mt-lg30 mt-md30 mt30 w600">
                        <div class="text-center">
                            <span class="semi-bonus f-lg-30 f-md-26 f-20 w700">Bonus #7</span>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row mt30 mt-sm40 align-items-center">
                <div class="col-12 col-sm-6 order-sm-12 mb20 mb-sm0">
                    <img src="images/youtuber.jpg" class="img-fluid mx-auto d-block">
                </div>


                <div class="col-12 col-sm-6 order-sm-1">
                    <div class="w600 f-lg-30 f-md-26 f-20">
                        YouTubeR Playlist Creator
                    </div>
                    <div class="f-20 f-md-22 lh140 w400 mt30">
                        YouTubeR Playlist Creator - Using this new software, you can now create killer looking YouTube Playlist sites in record time!
                        <br><br>
                        The YouTubeR Playlist Site Creator Software is Windows based and is very simple to use. Review questions presented, fill in your answers, click finish and page is saved to your hard drive.
                        <br><br>
                        Each playlist site takes no more than a few minutes to create once you get the hang of it. Complete documentation is provided and we even walk you step-by-step in obtaining required free API keys.
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 col-lg-12 mx-auto">
                    <div class="mt-lg30 mt-md30 mt30 w600">
                        <div class="text-center">
                            <span class="semi-bonus f-lg-30 f-md-26 f-20 w700">Bonus #8</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row mt30 mt-sm40 align-items-center">
                <div class="col-12 col-sm-6 mb20 mb-sm0">
                    <img src="images/customer.jpg" class="img-fluid mx-auto d-block">
                </div>


                <div class="col-12 col-sm-6">
                    <div class="w600 f-lg-30 f-md-26 f-20">
                        Customer Service Template Guide
                    </div>
                    <div class="f-20 f-md-22 lh140 w400 mt30">
                        Fill-in-The-Blank Time-Tested Marketing Templates!
                        <br><br>
                        If you are a company offering customer support services or a freelance guy who offers the same service, having the template or script that works is a huge advantage in your asset.
                        <br><br>
                        The thing is that what if you are not good at constructing the script or template in the first place. Well, the good news is that inside this product is bundle of templates that you can use today. 
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-8 col-11 mx-auto text-center text-white instant_button text-center animated infinite pulse delay-2s mt30 mt-md70 mb30 mb-md50">
                    <a href="https://cutt.ly/7HhxyJ7">
                        <div class="text-center w600 f-sm-24 f-18 lh140">
                            Grab SendPro With Great Grand Bonus
                        </div>
                    </a>
                </div>
            </div>

        </div>
    </div>


    <!-- Steps-Section-Start -->
    <div class="step-sec mt30 mt-md0">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 p0">
                    <picture>
                        <source media="(min-width:767px)" srcset="images/steps.png" class="img-fluid d-block mx-auto">
                        <source media="(min-width:320px)" srcset="images/steps-mview.png" class="img-fluid d-block w-100" >
                        <img src="images/steps.png" alt="Flowers" class="img-fluid d-block mx-auto">
                    </picture>
                </div>
            </div>
        </div>
        
    </div>
    <!-- Steps-Section-End -->

    <div class="feature-sec mt30 mt-md70">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 p0">                    
                    <picture>
                        <source media="(min-width:767px)" srcset="images/feature.png" class="img-fluid d-block mx-auto">
                        <source media="(min-width:320px)" srcset="images/features-mview.png" class="img-fluid d-block mx-auto w-100">
                        <img src="images/feature.png" alt="Flowers" class="img-fluid d-block mx-auto w-100">
                    </picture>

                </div>
            </div>
        </div>
    </div>

    <div class="btn-sec">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-md-11 col-11 mx-auto text-center text-white instant_button text-center animated infinite pulse delay-2s mt30 mt-md0 mb30 mb-md50">
                    <a href="https://cutt.ly/7HhxyJ7">
                        <div class="text-center w600 f-sm-24 f-18 lh140">
                            Grab SendPro With Great Grand Bonus
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>



    <!-- Footer Section Start -->
    <footer class="footer-sect">
        <div class="container">



            <div class="row">
                <div class="col-md-4 col-12 text-center">
                    <img src="images/bizomart-logo.png" class="img-fluid" style="width: 40%;">
                </div>
                <div class="col-md-4 col-12 mt-sm0 mt15 d-left-m-center">
                    <div class="f-md-20 f-sm-18 f-16 lh150 w500 text-white">Get In Touch</div>
                    <div class="f-md-16 f-sm-15 f-14 mt10 lh150 w400">
                        <a href="https://support.bizomart.com/" class="white">Support Desk</a>
                    </div>
                </div>
                <div class="col-md-4 col-12 mt-sm0 mt15 d-left-m-center">
                    <div class="f-md-20 f-sm-18 f-16 lh150 w500 text-white">Legal &amp; Information</div>
                    <div class="f-md-16 f-sm-15 f-14 mt10 lh150 w400">
                        <a href="http://bizomart.com/terms" class="white">Terms &amp; Condition</a>
                    </div>
                    <div class="f-md-16 f-sm-15 f-14 mt10 lh150 w400 text-xs-center">
                        <a href="http://bizomart.com/privacy" class="white">Privacy Policy</a>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- footer end -->


    <div class="totop"><i class="fa fa-level-up"></i></div>

    <!---Scriptload file-->
    <script src="js/jquery.min.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/custom.js" type="text/javascript"></script>
    <script src="js/totop.min.js"></script>
    <script>
        $(function() {
            $('a[href*=#]').on('click', function(e) {
                e.preventDefault();
                $('html, body').animate({
                    scrollTop: $($(this).attr('href')).offset().top
                }, 500, 'linear');
            });
        });
    </script>
    <script>
        $('.totop').tottTop({
            scrollTop: 100
        });
    </script>


    <script src="./js/aos.js"></script>
    <script>
        AOS.init();
    </script>

    <!-- timer -->
    <?php
         if ($now < $exp_date) {
         
         ?>
        <script type="text/javascript">
            // Count down milliseconds = server_end - server_now = client_end - client_now
            var server_end = <?php echo $exp_date; ?> * 1000;
            var server_now = <?php echo time(); ?> * 1000;
            var client_now = new Date().getTime();
            var end = server_end - server_now + client_now; // this is the real end time

            var noob = $('.countdown').length;

            var _second = 1000;
            var _minute = _second * 60;
            var _hour = _minute * 60;
            var _day = _hour * 24
            var timer;

            function showRemaining() {
                var now = new Date();
                var distance = end - now;
                if (distance < 0) {
                    clearInterval(timer);
                    document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
                    return;
                }

                var days = Math.floor(distance / _day);
                var hours = Math.floor((distance % _day) / _hour);
                var minutes = Math.floor((distance % _hour) / _minute);
                var seconds = Math.floor((distance % _minute) / _second);
                if (days < 10) {
                    days = "0" + days;
                }
                if (hours < 10) {
                    hours = "0" + hours;
                }
                if (minutes < 10) {
                    minutes = "0" + minutes;
                }
                if (seconds < 10) {
                    seconds = "0" + seconds;
                }
                var i;
                var countdown = document.getElementsByClassName('countdown');
                for (i = 0; i < noob; i++) {
                    countdown[i].innerHTML = '';

                    if (days) {
                        countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-40 f-sm-40 timerbg">' + days + '</span><br><span class="f-16 w500">Days</span> </div>';
                    }

                    countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-40 f-sm-40 timerbg">' + hours + '</span><br><span class="f-16 w500">Hours</span> </div>';

                    countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-40 f-sm-40 timerbg">' + minutes + '</span><br><span class="f-16 w500">Mins</span> </div>';

                    countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-40 f-sm-40 timerbg">' + seconds + '</span><br><span class="f-16 w500">Sec</span> </div>';
                }

            }
            timer = setInterval(showRemaining, 1000);
        </script>
        <?php
         } else {
         echo "Times Up";
         }
         ?>
            <!-- timer end-->



</body>

</html>
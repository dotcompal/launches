let el = document.querySelector('#custom-btn');

// el.classList.add('btn-show');
// el.classList.remove('btn-hide');

// $(document).scroll(function () {
//     var y = $(this).scrollTop();
//     if (y > 2000) {
//         $('#custom-btn').fadeIn();
//     } else {
//         $('#custom-btn').fadeOut();
//     }
// });

document.addEventListener('scroll', (el) => {
    if(window.scrollY > 3000) {
        el.classList.add('btn-show');
        el.classList.remove('btn-hide');
    }
})
/*
    Temporary Button to Test Scroll To To Functionality 
*/


// let bottomBtn = document.querySelector('#temp-btn');

// bottomBtn.addEventListener('click', () => {
//     window.scroll({
//             top: 0,
//             left: 0,
//             behavior: 'smooth'
//         });
// })
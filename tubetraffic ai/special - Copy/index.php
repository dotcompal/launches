<!Doctype html>
<html>
   <head>
      <title>TubeTrafficAi Special</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=9">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
      <meta name="title" content="TubeTrafficAi Speical">
      <meta name="description" content="World's First Google-Bard Powered AI App That *Legally* Hacks Into YouTube's 800 Million Videos…">
      <meta name="keywords" content="TubeTrafficAi">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta property="og:image" content="https://www.tubetrafficai.com/special/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Pranshu Gupta">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="TubeTrafficAi Speical">
      <meta property="og:description" content="World's First Google-Bard Powered AI App That *Legally* Hacks Into YouTube's 800 Million Videos…">
      <meta property="og:image" content="https://www.tubetrafficai.com/special/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="TubeTrafficAi Speical">
      <meta property="twitter:description" content="World's First Google-Bard Powered AI App That *Legally* Hacks Into YouTube's 800 Million Videos…">
      <meta property="twitter:image" content="https://www.tubetrafficai.com/special/thumbnail.png">
      <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&family=Poppins:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
      <!-- Start Editor required -->
      <link rel="stylesheet" href="https://cdn.oppyotest.com/launches/sellero/common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style-feature.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style-pawan.css" type="text/css">
      <link rel="stylesheet" href="../common_assets/css/general.css">
      <link rel="stylesheet" href="assets/css/style-bottom.css" type="text/css">
      <link rel="stylesheet" href="assets/css/timer.css" type="text/css">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <script src="https://cdn.oppyotest.com/launches/sellero/common_assets/js/bootstrap.min.js"></script>
      <script src="https://cdn.oppyotest.com/launches/sellero/common_assets/js/jquery.min.js"></script>
	  
	  <script>
(function(w, i, d, g, e, t, s) {
if(window.businessDomain != undefined){z
console.log("Your page have duplicate embed code. Please check it.");
return false;
}
businessDomain = 'tubetrafficai';
allowedDomain = 'tubetrafficai.com';
if(!window.location.hostname.includes(allowedDomain)){
console.log("Your page have not authorized. Please check it.");
return false;
}
console.log("Your script is ready...");
w[d] = w[d] || [];
t = i.createElement(g);
t.async = 1;
t.src = e;
s = i.getElementsByTagName(g)[0];
s.parentNode.insertBefore(t, s);
})(window, document, '_gscq', 'script', 'https://cdn.staticdcp.com/apps/engage/smart_engage/js/config-loader.js');
</script>

   </head>
   <body>

      <div class="timer-header-top fixed-top">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-12 col-md-3 ">
                  <div class="tht-left f-14 f-md-13 text-md-start text-center white-clr">
                     Use Coupon <span class="orange-clr">"TubeAi"</span>  for <br class="d-block d-md-none">Extra <span class="orange-clr"> $3 Discount </span>
                  </div>
               </div>
               <div class="col-8 col-md-6">
                  <div class="countdown counter-white text-center">
                     <div class="timer-label text-center"><span class="f-24 f-md-28 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-16 w500 smmltd">Days</span> </div>
                     <div class="timer-label text-center"><span class="f-24 f-md-28 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-16 w500 smmltd">Hours</span> </div>
                     <div class="timer-label text-center timer-mrgn"><span class="f-24 f-md-28 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-16 w500 smmltd">Mins</span> </div>
                     <div class="timer-label text-center "><span class="f-24 f-md-28 timerbg oswald">00</span><br><span class="f-14 f-md-16 w500 smmltd">Sec</span> </div>
                  </div>
               </div>
               <div class="col-4 col-md-3 text-center text-md-right">
                  <div class="affiliate-link-btn"><a href="#buynow" class="t-decoration-none">Buy Now</a></div>
               </div>
            </div>
         </div>
      </div>
      <!--1. Header Section Start -->
      <div class="header-section">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-3 text-center mx-auto">
               <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 800 131.43" style="max-height:75px;">
                        <defs>
                            <style>
                            .cls-1 {
                                fill: #fff;
                            }
        
                            .cls-2 {
                                fill: #fd2f15;
                            }
        
                            .cls-3 {
                                fill: #fbbf33;
                            }
                            </style>
                        </defs>
                        <g id="Layer_1-2" data-name="Layer 1">
                            <g>
                            <g>
                                <path class="cls-3" d="m105.61,78.06l40.05,23.12c6.01,3.47,13.51-.87,13.51-7.8v-46.25c0-6.93-7.51-11.27-13.51-7.8l-40.05,23.12c-6.01,3.47-6.01,12.14,0,15.6Z"></path>
                                <g>
                                <rect class="cls-3" x="26.92" y="58.86" width="16.03" height="46.81"></rect>
                                <path class="cls-2" d="m125.22,26.34h-7.16v46.06c0,32.6-26.43,59.03-59.03,59.03H0v-65.94C0,32.89,26.43,6.45,59.03,6.45h38.52l-12.38,19.88h-26.14c-21.59,0-39.16,17.57-39.16,39.15v46.07h39.16c21.59,0,39.16-17.57,39.16-39.16V26.34h-5.79L108.81,0l16.41,26.34Z"></path>
                                <path class="cls-3" d="m67.04,46.21h-16.02v59.46h8.01c2.76,0,5.45-.35,8.01-.99v-58.47Z"></path>
                                <path class="cls-3" d="m90.77,33.56h-16.03v68.14c7.57-4.07,13.39-10.99,16.03-19.31v-48.83Z"></path>
                                </g>
                            </g>
                            <g>
                                <g>
                                <path class="cls-1" d="m195.69,38.68v-5.58h49.35v5.58h-21.57v62.48h-6.21v-62.48h-21.57Z"></path>
                                <path class="cls-1" d="m283.29,81.65v-31.54h5.92v51.05h-5.92v-8.47h-.47c-1.2,2.61-3.09,4.79-5.68,6.53-2.59,1.74-5.76,2.61-9.5,2.61-3.26,0-6.15-.71-8.67-2.14-2.53-1.43-4.51-3.53-5.95-6.3-1.44-2.77-2.16-6.17-2.16-10.2v-33.07h5.92v32.7c0,3.99,1.17,7.2,3.52,9.62,2.35,2.43,5.36,3.64,9.04,3.64,2.3,0,4.52-.55,6.65-1.66,2.13-1.11,3.88-2.73,5.25-4.87,1.37-2.14,2.06-4.77,2.06-7.89Z"></path>
                                <path class="cls-1" d="m306.05,101.16V33.09h5.95v26.75h.53c.66-1.53,1.62-3.1,2.87-4.72,1.25-1.62,2.9-2.97,4.95-4.07,2.05-1.1,4.64-1.65,7.76-1.65,4.19,0,7.86,1.1,11.03,3.31,3.17,2.21,5.64,5.28,7.41,9.24,1.77,3.95,2.66,8.56,2.66,13.81s-.88,9.89-2.64,13.86c-1.76,3.97-4.22,7.06-7.38,9.27-3.16,2.22-6.82,3.32-10.98,3.32-3.1,0-5.69-.55-7.76-1.66-2.07-1.11-3.74-2.48-5-4.1-1.26-1.63-2.24-3.22-2.92-4.77h-.73v9.47h-5.75Zm5.85-25.49c0,4.12.61,7.77,1.84,10.95,1.23,3.18,3,5.67,5.32,7.48,2.31,1.81,5.12,2.71,8.42,2.71s6.26-.94,8.61-2.81c2.35-1.87,4.13-4.4,5.35-7.59,1.22-3.19,1.83-6.77,1.83-10.73s-.6-7.46-1.81-10.6c-1.21-3.15-2.99-5.64-5.33-7.49-2.35-1.85-5.23-2.78-8.64-2.78s-6.14.89-8.46,2.68c-2.32,1.78-4.08,4.24-5.3,7.38-1.22,3.14-1.83,6.74-1.83,10.82Z"></path>
                                <path class="cls-1" d="m382.86,102.22c-4.76,0-8.88-1.11-12.36-3.34-3.48-2.23-6.16-5.32-8.06-9.27s-2.84-8.51-2.84-13.68.95-9.71,2.84-13.71c1.89-4,4.52-7.13,7.88-9.41,3.36-2.27,7.24-3.41,11.65-3.41,2.77,0,5.44.5,8.01,1.51,2.57,1.01,4.88,2.55,6.93,4.62,2.05,2.07,3.67,4.68,4.87,7.81,1.2,3.14,1.79,6.84,1.79,11.12v2.92h-39.88v-5.22h33.83c0-3.28-.66-6.23-1.98-8.86-1.32-2.63-3.15-4.7-5.48-6.23-2.34-1.53-5.04-2.29-8.09-2.29-3.23,0-6.08.86-8.54,2.59-2.46,1.73-4.38,4.01-5.77,6.85-1.38,2.84-2.09,5.94-2.11,9.31v3.12c0,4.05.7,7.59,2.11,10.62,1.41,3.02,3.41,5.37,6,7.03,2.59,1.66,5.66,2.49,9.21,2.49,2.41,0,4.54-.38,6.36-1.13,1.83-.75,3.37-1.77,4.62-3.04,1.25-1.27,2.2-2.68,2.84-4.2l5.62,1.83c-.78,2.15-2.04,4.13-3.81,5.95-1.76,1.82-3.95,3.27-6.58,4.37-2.63,1.1-5.65,1.65-9.06,1.65Z"></path>
                                <path class="cls-1" d="m412.07,44.96v-11.86h55.9v11.86h-20.84v56.2h-14.22v-56.2h-20.84Z"></path>
                                <path class="cls-1" d="m471.62,101.16v-51.05h13.73v8.91h.53c.93-3.17,2.49-5.57,4.69-7.2,2.19-1.63,4.72-2.44,7.58-2.44.71,0,1.47.04,2.29.13.82.09,1.54.21,2.16.37v12.56c-.66-.2-1.58-.38-2.76-.53-1.17-.15-2.25-.23-3.22-.23-2.08,0-3.94.45-5.57,1.35-1.63.9-2.91,2.14-3.86,3.74-.94,1.6-1.41,3.43-1.41,5.52v28.88h-14.16Z"></path>
                                <path class="cls-1" d="m523,102.12c-3.26,0-6.16-.57-8.71-1.71-2.55-1.14-4.56-2.84-6.03-5.08-1.47-2.25-2.21-5.06-2.21-8.42,0-2.84.52-5.22,1.56-7.15,1.04-1.93,2.46-3.48,4.25-4.65,1.79-1.17,3.84-2.06,6.13-2.66,2.29-.6,4.7-1.02,7.23-1.26,2.97-.31,5.36-.6,7.18-.88,1.82-.28,3.13-.69,3.95-1.25.82-.55,1.23-1.37,1.23-2.46v-.2c0-2.1-.66-3.73-1.98-4.89-1.32-1.15-3.19-1.73-5.6-1.73-2.55,0-4.58.56-6.08,1.68-1.51,1.12-2.5,2.52-2.99,4.2l-13.09-1.06c.66-3.1,1.97-5.79,3.92-8.06,1.95-2.27,4.47-4.02,7.56-5.25,3.09-1.23,6.67-1.84,10.75-1.84,2.84,0,5.55.33,8.16,1,2.6.66,4.92,1.7,6.95,3.09,2.03,1.4,3.63,3.19,4.8,5.37,1.17,2.18,1.76,4.79,1.76,7.83v34.43h-13.43v-7.08h-.4c-.82,1.6-1.92,3-3.29,4.2-1.37,1.21-3.02,2.15-4.95,2.82-1.93.68-4.15,1.01-6.68,1.01Zm4.05-9.77c2.08,0,3.92-.42,5.52-1.25s2.85-1.96,3.76-3.37c.91-1.42,1.36-3.02,1.36-4.82v-5.42c-.44.29-1.05.55-1.81.78s-1.62.44-2.58.63c-.95.19-1.91.35-2.86.5-.95.14-1.82.27-2.59.38-1.66.24-3.11.63-4.35,1.16-1.24.53-2.21,1.25-2.89,2.14-.69.9-1.03,2.01-1.03,3.34,0,1.93.7,3.4,2.11,4.4,1.41,1.01,3.2,1.51,5.37,1.51Z"></path>
                                <path class="cls-1" d="m580.03,50.11h10.17v10.63h-10.17v40.41h-14.12v-40.41h-7.21v-10.63h7.21v-3.69c0-3.7.72-6.77,2.18-9.21,1.45-2.44,3.44-4.26,5.97-5.48,2.53-1.22,5.4-1.83,8.61-1.83,2.17,0,4.15.17,5.97.5,1.8.33,3.15.63,4.04.9l-2.53,10.63c-.55-.18-1.23-.34-2.04-.5s-1.64-.23-2.48-.23c-2.08,0-3.53.48-4.35,1.45-.83.96-1.23,2.31-1.23,4.04v3.42Z"></path>
                                <path class="cls-1" d="m616.12,50.11h10.17v10.63h-10.17v40.41h-14.12v-40.41h-7.21v-10.63h7.21v-3.69c0-3.7.72-6.77,2.18-9.21,1.45-2.44,3.44-4.26,5.97-5.48,2.53-1.22,5.4-1.83,8.61-1.83,2.17,0,4.15.17,5.97.5,1.8.33,3.15.63,4.04.9l-2.53,10.63c-.55-.18-1.23-.34-2.04-.5-.81-.15-1.64-.23-2.48-.23-2.08,0-3.53.48-4.35,1.45-.83.96-1.23,2.31-1.23,4.04v3.42Z"></path>
                                <path class="cls-1" d="m642.05,43.53c-2.11,0-3.9-.7-5.4-2.09-1.5-1.4-2.24-3.08-2.24-5.05s.75-3.66,2.26-5.07c1.51-1.41,3.3-2.11,5.38-2.11s3.91.7,5.4,2.09c1.5,1.4,2.24,3.08,2.24,5.05s-.75,3.66-2.24,5.07c-1.5,1.41-3.3,2.11-5.4,2.11Zm-7.11,57.63v-51.05h14.16v51.05h-14.16Z"></path>
                                <path class="cls-1" d="m683.46,102.15c-5.23,0-9.72-1.11-13.48-3.34-3.76-2.23-6.64-5.32-8.64-9.29-2.01-3.97-3.01-8.53-3.01-13.69s1.01-9.82,3.04-13.78c2.03-3.95,4.91-7.05,8.66-9.27,3.74-2.23,8.2-3.34,13.36-3.34,4.45,0,8.35.81,11.7,2.43,3.35,1.62,5.99,3.89,7.94,6.81,1.95,2.92,3.02,6.36,3.22,10.3h-13.36c-.38-2.55-1.37-4.6-2.97-6.16-1.61-1.56-3.71-2.34-6.3-2.34-2.19,0-4.1.59-5.73,1.78-1.63,1.19-2.9,2.91-3.81,5.17s-1.36,5-1.36,8.21.45,6.03,1.35,8.31c.9,2.28,2.17,4.02,3.81,5.22,1.64,1.2,3.56,1.79,5.75,1.79,1.62,0,3.07-.33,4.37-1,1.3-.66,2.37-1.63,3.22-2.91.85-1.27,1.41-2.81,1.68-4.6h13.36c-.22,3.9-1.28,7.33-3.17,10.29-1.89,2.96-4.5,5.27-7.83,6.93-3.32,1.66-7.26,2.49-11.8,2.49Z"></path>
                                </g>
                                <g>
                                <path class="cls-3" d="m800,37.72c0,4.31-3.5,7.81-7.82,7.81s-7.81-3.5-7.81-7.81,3.5-7.82,7.81-7.82,7.82,3.5,7.82,7.82Z"></path>
                                <path class="cls-2" d="m736.11,85.52c-.09,0-.18,0-.27-.01-.09,0-.18-.02-.26-.03.18.01.35.05.53.05Z"></path>
                                <path class="cls-2" d="m784.37,50.18v32.89c0,1.31-1.05,2.37-2.35,2.43-.03,0-.07.02-.11.02h-.37l-18.68-41.36-3.1-6.86c-2.03-4.5-6.52-7.4-11.47-7.4s-9.42,2.9-11.46,7.4l-11.96,26.47-10.4,23.03c-3.04,6.73,1.88,14.36,9.28,14.36h66.01c5.65,0,10.25-4.58,10.25-10.24v-40.74h-15.63Zm-48.25,35.34c-.09,0-.18,0-.27-.01-.09,0-.18-.02-.26-.03-2.25-.33-3.65-2.75-2.67-4.91l12.93-28.64c.95-2.1,3.93-2.1,4.89,0l15.17,33.59h-29.79Z"></path>
                                </g>
                            </g>
                            </g>
                        </g>
                        </svg>
               </div>
               <!-- <div class="col-md-9 mt-md0 mt15">
                  <ul class="leader-ul f-20 w400 white-clr text-md-end text-center">
                     <li>
                        <a href="#features" class="white-clr t-decoration-none">Features</a><span class="pl15">|</span>
                     </li>
                     <li>
                        <a href="#productdemo" class="white-clr t-decoration-none">Demo</a>
                     </li>
                     <li class="affiliate-link-btn">
                        <a href="#buynow">Buy Now</a>
                     </li>
                  </ul>
               </div> -->
               <div class="col-12 text-center lh150 mt20 mt-md50">
                    <div class="pre-heading f-18 f-md-24 w600 lh140 white-clr">
                        YouTube has been dominated by Ai…
                    </div>
                </div>
                <div class="col-12 f-md-42 f-28 w700 text-center white-clr lh140 mt-md40 mt20 p0">
                    World's First Google-Bard Powered AI App That 
                    <span class="red-brush-top"> *Legally* Hacks Into YouTube's 800 Million Videos… </span> 
                </div>
                <div class="col-12 mt-md20 mt20 text-center">
                    <div class="f-18 f-md-24 w600 lh140 white-clr ">
                        <i>Then Redirect Their Views To ANY Link or Offer of Our Choice & Sent Us…</i>
                    </div>
                </div>
                <div class="col-12 mt-md20 mt20 text-center">
                    <div class="f-md-24 f-18 w500 yellow-clr lh140 post-heading">
                    Thousands Of Views In 3 Hours or Less… Making Us $346.34 Daily on Autopilot
                    </div>
                </div>
                <div class="col-12 mt-md30 mt20 text-center">
                <div class="col-12 f-md-24 f-18 w400 white-clr lh140">
                Without Creating Videos | Without Uploading | Without Paid Ads | Without Any Upfront Cost
                </div>
            </div>
            </div>

            <div class="row mt20 mt-md80 gx-md-5">
               <div class="col-md-7 col-12">
                  <div class="col-12">
                     <div class="f-18 f-md-19 w500 lh140 white-clr shape-head">
                     Click Play And Watch TubeTraffic Ai In Action Making Us Hundreds Of Dollars…
                     </div>
                     <div class=" mt20 ">
                        <!-- <img src="assets/images/product-image.webp" alt="Video" class="mx-auto d-block img-fluid"> -->
                        <div style="padding-bottom: 56.25%;position: relative;">
                        <iframe src="https://tubetrafficai.oppyo.com/video/embed/mvuk8vau96" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div>
                     </div>
                  </div>
                  <div class="col-12 mt20">
                     <div class="border-dashed">
                        <div class="f-16 f-md-20 w500 lh140 text-center grey-clr">
                        First 99 Action Taker Get Instant Access To TubeTraffic Ai DFY Videos
                        <span class="yellow-clr"> (Average User Saw 193% Increase In Profit worth $297)</span>
                        </div>
                     </div>
                     <div class="f-22 f-md-24 w600 lh150 text-center mt20 white-clr">
                     Get TubeTraffic Ai Today And Save $384 <br class="d-none d-md-block"> Just Pay Once  <del class="red-clr w500"> Instead Of Monthly</del>
                     </div>
                     <div class="row">
                        <div class="col-md-12 mx-auto col-12 mt20 mt-md20 text-center affiliate-link-btn1">
                           <a href="#buynow">>>> Click Here Now To Get Started <<<</a>
                        </div>
                        <!-- <div class="col-12 mt20 text-center">
                           <div class="f-18 f-md-20 w300 lh140 white-clr">
                              Special Bonus* <span class="w600">"30 Reseller Licenses"</span> If You Buy Today         
                           </div>
                        </div> -->
                     </div>
                     <div class="col-12 mt20 mt-md30">
                        <img src="assets/images/compaitable-gurantee1.webp" alt="Compaitable Gurantee" class="mx-auto d-block img-fluid">
                     </div>

                     <div class="mt20 mt-md30">
                        <div class="countdown counter-white text-center">
                           <div class="timer-label text-center"><span class="f-24 f-md-42 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
                           <div class="timer-label text-center"><span class="f-24 f-md-42 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                           <div class="timer-label text-center timer-mrgn"><span class="f-24 f-md-42 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                           <div class="timer-label text-center "><span class="f-24 f-md-42 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-5 col-12 mt20 mt-md0">
                  <div class="key-features-bg">
                     <ul class="list-head pl0 m0 f-16 f-md-18 lh160 w400 white-clr">
                        <li>No video creation whatsoever, AI does all of that…</li>
                        <li><span class="w700">	Works in any niche no matter what is it.</span></li>
                        <li>Built-in AI traffic blaster that will send thousands of extra clicks for free</li>
                        <li><span class="w700">	In Just Few Click Embed ANYWHERE On The Internet </span></li>
                        <li>Built In AI Traffic Generator That Sends Us Thousands Of Views 100% Free</li>
                        <li><span class="w700">Redirect thousands of clicks every day to any link you want..</span></li>
                        <li>Integrates with all the top APIs to give you a seamless experience</li>
                        <li><span classs="w700">No setup and no configuration needed</span></li>
                        <li>No upfront spending is needed at all. You get started for 100% free.</li>
                        <li><span class="w700">99.99% Up Time Guaranteed</span></li>
                        <li>30 Days Money-Back Guarantee</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--1. Header Section End -->

   <!-- Limited Section Start    -->
      <div class="limited-time-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="row align-items-center flex-column flex-md-row justify-content-center">
                     
                     <div class="col-md-10 col-12 f-20 f-md-22 w600 text-center white-clr px-md0">
                     <img src="assets/images/caution-icon.webp">Warning! Your $3,583.11 Bonuses Package Expires When Timer Hits ZERO <img src="assets/images/caution-icon.webp">
                     </div>
                     
                  </div>
               </div>
            </div>
         </div>
      </div> 
   <!-- Limited Section End -->

   <div class="step-section">
      <div class="container">
         <div class="row">
         <div class="col-12 f-28 f-md-38 w500 lh140 black-clr text-center">
            <span class="f-28 f-md-50 w700 red-clr">You're Just 3 Clicks Away</span> 
            <br class="d-none d-md-block"> From Flooding Any Link With Buyers Traffic On-Demand…
            </div>
            <div class="col-12 col-md-8 mx-auto f-md-22 f-18 w600 text-center white-clr lh140 mt20 need-head">
            (Simple Ai Hack Changed Our Lives and Thousands Of Other…)
                </div>
            <div class="col-12 mt30 mt-md60">
               <div class="row align-items-center">
                  <div class="col-12 col-md-6">
                     <div class="step-shapes f-20 f-md-22 w600 black-clr">
                        Step 1
                     </div>
                     <div class="f-34 f-md-55 w700 lh140 mt15 caveat">
                        Login
                     </div>
                     <div class="col-12 col-md-10 f-20 f-md-22 w400 lh140 mt5 mt-md10 text-capitalize">
                     Click On Any Of The Buttons Below To Get Instant Access To TubeTrafficAi & Login to Powerful Dashboard (Dead-Easy To Use)
                     </div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 ">
                     <img src="assets/images/step1-img.png" alt="Video" class="mx-auto d-block img-fluid">
                  </div>
               </div>
            </div>
            <div class="col-12">
               <img src="assets/images/left-arrow.webp" class="img-fluid d-none d-md-block mx-auto " alt="Left Arrow">
            </div>
            <div class="col-12">
               <div class="row align-items-center ">
                  <div class="col-12 col-md-6 order-md-2">
                     <div class="step-shapes f-20 f-md-22 w600 black-clr">
                        Step 2
                     </div>
                     <div class="f-34 f-md-55 w700 lh140 mt15 caveat">
                     Add A New Video
                     </div>
                     <div class=" f-20 f-md-22 w400 lh140 mt5 mt-md10 text-capitalize">
                     With Just One Click, Our AI Model Will Research from 800 million+ YouTube Videos & Publish an Awesome Video For You. And Send You Endless Clicks In Any Niche.
                     </div>
                  </div>
                  <div class="col-md-6 col-md-pull-6 mt20 mt-md0 order-md-1 ">
                     <img src="assets/images/step2-img.png" alt="Video" class="mx-auto d-block img-fluid">
                  </div>
               </div>
            </div>
            <div class="col-12">
               <img src="assets/images/right-arrow.webp" class="img-fluid d-none d-md-block mx-auto" alt="Right Arrow">
            </div>
            <div class="col-12">
               <div class="row align-items-center ">
                  <div class="col-12 col-md-6">
                     <div class="step-shapes f-20 f-md-22 w600 black-clr">
                        Step 3
                     </div>
                     <div class="f-34 f-md-55 w700 lh140 mt15 caveat">
                        Profit
                     </div>
                     <div class="col-12 col-md-11 f-20 f-md-22 w400 lh140 mt5 mt-md10 text-capitalize">
                     That's All We Do. Every time We Promote An Affiliate Offer, or Our Product with Those Videos, We Wake Up To Money Like This
                     </div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 ">
                  <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 1px solid #fd2f15;"><source src="assets/images/profit.mp4" type="video/mp4">
                     </video>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>

   <div class="everything-section">
      <div class="container">
         <div class="row">
            <div class="col-12 f-32 f-md-50 w600 lh140 white-clr text-center">
            TubeTraffic Ai Made It Fail-Proof To Add 100s Of Profitable Videos In No Time With A.I.

            </div>
         </div>
         <div class="row row-cols-1 row-cols-sm-2 row-cols-md-2 gap30 mt20 mt-md10 mx5">
            <div class="col">
               <div class="e-box">
                  <div class="figure">
                     <img src="assets/images/e1.webp" alt="DFY Websites" class="mx-auto d-block img-fluid">
                     <div class="figure-caption text-center text-md-start">
                        <img src="assets/images/red-cross.webp" alt="DFY Websites" class="mx-auto d-block img-fluid mtm40">
                        <div class="f-22 f-md-38 w600 lh140 mt20 white-clr">No Video Creation</div>
                        <div class="f-18 f-md-20 w300 lh140 mt10 white-clr">Creating a video is not an easy task. Putting your face on camera can be scary and uncomfortable for many people…</div>
                        <div class="f-18 f-md-20 w300 lh140 mt10 white-clr">But that won't be an issue with TubeTraffic Ai.</div>
                        <div class="f-18 f-md-20 w300 lh140 mt10 white-clr">Because It Will Automatically Pull Best YouTube Video For You…</div>
                        <div class="f-18 f-md-20 w300 lh140 mt10 white-clr">Just Enter A Keyword.</div>
                     </div>
                  </div>
               </div>
            </div>

            <div class="col">
               <div class="e-box">
                  <div class="figure">
                     <img src="assets/images/e2.webp" alt="DFY Websites" class="mx-auto d-block img-fluid">
                     <div class="figure-caption text-center text-md-start">
                        <img src="assets/images/red-cross.webp" alt="DFY Websites" class="mx-auto d-block img-fluid mtm40">
                        <div class="f-22 f-md-38 w600 lh140 mt20 white-clr">No VoiceOver</div>
                        <div class="f-18 f-md-20 w300 lh140 mt10 white-clr">Any Good Video Should Have A Voice-Over.</div>
                        <div class="f-18 f-md-20 w300 lh140 mt10 white-clr">But Hiring A Voiceover Actor Is Just Too Expensive…</div>
                        <div class="f-18 f-md-20 w300 lh140 mt10 white-clr">Especially If You Wanna Make Tens Of Videos Each Day…</div>
                        <div class="f-18 f-md-20 w300 lh140 mt10 white-clr">Luckily with TubeTrafficAi You Do Not Need to Create VoiceOvers At All.</div>
                     </div>
                  </div>
               </div>
            </div>

            <div class="col">
               <div class="e-box">
                  <div class="figure">
                     <img src="assets/images/e3.webp" alt="DFY Websites" class="mx-auto d-block img-fluid">
                     <div class="figure-caption text-center text-md-start">
                        <img src="assets/images/red-cross.webp" alt="DFY Websites" class="mx-auto d-block img-fluid mtm40">
                        <div class="f-22 f-md-38 w600 lh140 mt20 white-clr">No Script Writing</div>
                        <div class="f-18 f-md-20 w300 lh140 mt10 white-clr">In Order For A Video To Generate Profit, It Must Be Entertaining…</div>
                        <div class="f-18 f-md-20 w300 lh140 mt10 white-clr">It Must Have A Good Script That Entertains The Viewers</div>
                        <div class="f-18 f-md-20 w300 lh140 mt10 white-clr">And Sells Them At The Same Time…</div>
                        <div class="f-18 f-md-20 w300 lh140 mt10 white-clr">But You Don't Have To Worry About That.</div>
                        <div class="f-18 f-md-20 w300 lh140 mt10 white-clr">Because Our AI Will Research Only PROVEN Video For YOU</div>
                        <div class="f-18 f-md-20 w300 lh140 mt10 white-clr">Just Enter Your Niche, And It Will Pull As Many Videos As You Want.</div>
                     </div>
                  </div>
               </div>
            </div>

            <div class="col">
               <div class="e-box">
                  <div class="figure">
                     <img src="assets/images/e4.webp" alt="DFY Websites" class="mx-auto d-block img-fluid">
                     <div class="figure-caption text-center text-md-start">
                        <img src="assets/images/red-cross.webp" alt="DFY Websites" class="mx-auto d-block img-fluid mtm40">
                        <div class="f-22 f-md-38 w600 lh140 mt20 white-clr">No Video Editing</div>
                        <div class="f-18 f-md-20 w300 lh140 mt10 white-clr">No Need To Edit Anything Once TubeTrafficAi Add A Video.</div>
                        
                        <div class="f-18 f-md-20 w300 lh140 mt10 white-clr">TubeTrafficAi Adds ONLY STUNNING Videos That Are Proven To Generate Profit Every Single Day.</div>
                     </div>
                  </div>
               </div>
            </div>

            <div class="col">
               <div class="e-box">
                  <div class="figure">
                     <img src="assets/images/e5.webp" alt="DFY Websites" class="mx-auto d-block img-fluid">
                     <div class="figure-caption text-center text-md-start">
                        <img src="assets/images/red-cross.webp" alt="DFY Websites" class="mx-auto d-block img-fluid mtm40">
                        <div class="f-22 f-md-38 w600 lh140 mt20 white-clr">No Banned Account</div>
                        <div class="f-18 f-md-20 w300 lh140 mt10 white-clr">Unlike Anything Else, TubeTrafficAi Doesn't Break Any TOS.</div>
                        <div class="f-18 f-md-20 w300 lh140 mt10 white-clr">And It Doesn't Spam.</div>
                        <div class="f-18 f-md-20 w300 lh140 mt10 white-clr">It Legally Exploits YouTube Platform…</div>
                        <div class="f-18 f-md-20 w300 lh140 mt10 white-clr">So, You Can Rest Assured That You Will Never Get Banned Or Shadow banned.</div>
                     </div>
                  </div>
               </div>
            </div>

            <div class="col">
               <div class="e-box">
                  <div class="figure">
                     <img src="assets/images/e6.webp" alt="DFY Websites" class="mx-auto d-block img-fluid">
                     <div class="figure-caption text-center text-md-start">
                        <img src="assets/images/red-cross.webp" alt="DFY Websites" class="mx-auto d-block img-fluid mtm40">
                        <div class="f-22 f-md-38 w600 lh140 mt20 white-clr">No Website Needed</div>
                        <div class="f-18 f-md-20 w300 lh140 mt10 white-clr">You Don't Need A Website/Store To Start Profiting With TubeTrafficAi</div>
                        <div class="f-18 f-md-20 w300 lh140 mt10 white-clr">All You Need Is To Follow The 3 Fail-Proof Steps</div>
                        <div class="f-18 f-md-20 w300 lh140 mt10 white-clr">And You're Good To Go…</div>
                        <div class="f-18 f-md-20 w300 lh140 mt10 white-clr">TubeTrafficAi Does Everything For You In The Background.</div>
                     </div>
                  </div>
               </div>
            </div>

            <div class="col">
               <div class="e-box">
                  <div class="figure">
                     <img src="assets/images/e7.webp" alt="DFY Websites" class="mx-auto d-block img-fluid">
                     <div class="figure-caption text-center text-md-start">
                        <img src="assets/images/red-cross.webp" alt="DFY Websites" class="mx-auto d-block img-fluid mtm40">
                        <div class="f-22 f-md-38 w600 lh140 mt20 white-clr">No Hidden Fees</div>
                        <div class="f-18 f-md-20 w300 lh140 mt10 white-clr">There Is Nothing Else Needed To Start Getting Results With TubeTrafficAi</div>
                        <ul class="dont-list mt10 ">
                           <li class="f-18 f-md-20 w400 mt10 white-clr">No other tools</li>
                           <li class="f-18 f-md-20 w400 mt10 white-clr">No hosting</li>
                           <li class="f-18 f-md-20 w400 mt10 white-clr">No domains</li>
                        </ul>
                        <div class="f-18 f-md-20 w300 lh140 mt10 white-clr">All You Need Is TubeTrafficAi Account</div>
                     </div>
                  </div>
               </div>
            </div>

            <div class="col">
               <div class="e-box">
                  <div class="figure">
                     <img src="assets/images/e8.png" alt="DFY Websites" class="mx-auto d-block img-fluid">
                     <div class="figure-caption text-center text-md-start">
                        <img src="assets/images/red-cross.webp" alt="DFY Websites" class="mx-auto d-block img-fluid mtm40">
                        <div class="f-22 f-md-38 w600 lh140 mt20 white-clr">No Waiting</div>
                        <div class="f-18 f-md-20 w300 lh140 mt10 white-clr">We Live In A Fast World…</div>
                        <div class="f-18 f-md-20 w300 lh140 mt10 white-clr">We All Want Results NOW, Not Tomorrow…</div>
                        <div class="f-18 f-md-20 w300 lh140 mt10 white-clr">And That's Exactly What TubeTrafficAi Will Deliver…</div>
                        <div class="f-18 f-md-20 w300 lh140 mt10 white-clr">Fast, Tangible Results.</div>
                        <div class="f-18 f-md-20 w300 lh140 mt10 white-clr">Not Next Week, Not Tomorrow…</div>
                        <div class="f-18 f-md-20 w600 lh140 mt10 white-clr"><u>NOW.</u></div>
                     </div>
                  </div>
               </div>
            </div>


            <div class="col">
               <div class="e-box-blue">
                  <div class="figure">
                     <img src="assets/images/e9.webp" alt="DFY Websites" class="mx-auto d-block img-fluid">
                     <div class="figure-caption text-center text-md-start">
                        <img src="assets/images/blue-tick.webp" alt="DFY Websites" class="mx-auto d-block img-fluid mtm40">
                        <div class="f-22 f-md-38 w600 lh140 mt20 white-clr">Instant Traffic</div>
                        <div class="f-18 f-md-20 w300 lh140 mt10 white-clr">We Start Getting Thousands Of Buyer Clicks… Moments After Publishing a HOT Video…</div>
                        <div class="f-18 f-md-20 w300 lh140 mt10 white-clr">This Is The FASTEST Traffic Generation Method In 2023</div>
                     </div>
                  </div>
               </div>
            </div>

            <div class="col">
               <div class="e-box-blue">   
                  <div class="figure">
                     <img src="assets/images/e10.webp" alt="DFY Websites" class="mx-auto d-block img-fluid">
                     <div class="figure-caption text-center text-md-start">
                        <img src="assets/images/blue-tick.webp" alt="DFY Websites" class="mx-auto d-block img-fluid mtm40">
                        <div class="f-22 f-md-38 w600 lh140 mt20 white-clr">Instant Monetization</div>
                        <div class="f-18 f-md-20 w300 lh140 mt10 white-clr">With TubeTrafficAi, You Can Promote Anything…</div>
                        <div class="f-18 f-md-20 w300 lh140 mt10 white-clr">Your Affiliate Offer, Your Own Product, Or Your ECom Store…</div>
                        <div class="f-18 f-md-20 w300 lh140 mt10 white-clr">But If You Have None Of Those.</div>
                        <div class="f-18 f-md-20 w300 lh140 mt10 white-clr">Don't Worry, Use Our DFY High Ticket Offers That Pay Up To $997 Per Sale</div>
                        
                     </div>
                  </div>
               </div>
            </div>
            
         </div>
         <div class="row">
            <div class="col-12 f-22 f-md-32 w600 lh140 white-clr text-center mt20 mt-md50">
               This Is Just A Fraction Of What TubeTrafficAi Will Do For You…
            </div>
         </div>
      </div>
   </div>

 
  <!-- CTA Section Start -->
  <div class="cta-section-white">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-34 w700 lh140 text-center white-clr">
               Get TubeTraffic Ai Today And Save $384
               </div>
               <div class="f-22 f-md-24 w400 lh140 text-center white-clr mt10">
                  <span class="w700">Just Pay Once</span> Instead Of <strike class="red-clr"> Monthly</strike>
               </div>
            </div>
         </div>
         <div class="row mt20 mt-md50">
            <div class="col-md-8 text-center">
               <a href="#buynow" class="cta-link-btn"> > > > Get Instant Access To TubeTrafficAi < < <   </a>
               <img src="assets/images/compaitable-gurantee1.webp" alt="Compaitable Gurantee" class="mx-auto d-block img-fluid mt20 mt-md30">
            </div>
            <div class="col-md-4 mt20 mt-md0 px-md0">
               <div class="countdown-container">
                  <div class="f-20 f-md-24 w400 lh140 text-center red-clr mb10">
                     <span class="w700 white-clr">Hurry up!</span>  Price Increases In
                  </div>
                  <div class="countdown counter-white text-center">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-37 timerbg oswald">00&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 smmltd">Days</span> 
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-37 timerbg oswald">00&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-37 timerbg oswald">00&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-37 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> 
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- CTA Section End -->

   <!-- Limited Section Start    -->
   <div class="limited-time-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="row align-items-center flex-column flex-md-row justify-content-center">
                  <div class="col-md-10 col-12 f-20 f-md-22 w600 text-center white-clr px-md0">
                     <img src="assets/images/caution-icon.webp">Warning! Your $3,583.11 Bonuses Package Expires When Timer Hits ZERO <img src="assets/images/caution-icon.webp">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   <!-- Limited Section End -->
   
   <!-- Testimonial Section Start -->
   <div class="testimonial-sec">
      <div class="container ">
         <div class="row ">
            <div class="col-12 f-28 f-md-50 w700 lh140 black-clr text-center">
            TubeTrafficAi Is Used By <span class="red-clr">1,467 Marketers </span> To Generate On-Demand Buyers Traffic…

            </div>
            <div class="col-12 f-20 f-md-28 w500 lh140 black-clr text-center mt10">
            (And You Can Join Them In 10 Seconds)
            </div>
         </div>
         <div class="row row-cols-md-2 row-cols-1 gx4 mt-md100 mt0">
            <div class="col mt20 mt-md50">
               <div class="single-testimonial">
                  <div class="st-img">
                     <img src="assets/images/craig-mcintosh.webp" class="img-fluid d-block mx-auto " alt="Craig McIntosh">
                  </div>
                  <div class="mt20 md-md50 f-24 f-md-28 w600 lh140 black-clr">Craig McIntosh </div>
                  <!-- <div class="mt10 f-20 f-md-22 w400 lh140 black-clr">
                     Digital Marketer and Internet Marketer.
                  </div>
                  <div class="stars mt10">
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                  </div> -->
                  <img src="assets/images/quote.webp" alt="Quotes" class="mx-auto d-block img-fluid mt20 mt-md50 ">
                  <p class="mt20 f-18 f-md-20 lh140 w400 text-center">
                  "TubeTrafficAI has transformed my video marketing. Tapping into YouTube's vast library without video creation, its SEO features boost visibility, while the ad-free player delights viewers. The integration options and top-notch support make it an indispensable tool for maximizing affiliate gains. Truly a game-changer!"
                  </p>
               </div>
            </div>
            <div class="col mt20 mt-md50">
               <div class="single-testimonial">
                  <div class="st-img">
                     <img src="assets/images/loveneet.jpg" class="img-fluid d-block mx-auto " style="border-radius:50%;">
                  </div>
                  <div class="mt20 md-md50 f-24 f-md-28 w600 lh140 black-clr"> Loveneet Rajora  </div>
                  <!-- <div class="mt10 f-20 f-md-22 w400 lh140 black-clr">
                  Founder,  AI Marketing and Sales Software
                  </div>
                  <div class="stars mt10">
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                  </div> -->
                  <img src="assets/images/quote.webp" alt="Quotes" class="mx-auto d-block img-fluid mt20 mt-md50 ">
                  <p class="mt20 f-18 f-md-20 lh140 w400 text-center">
                     I can say that TubeTrafficAi has been optimized and developed nicely since their launch, I tried it a days ago and now it become more mature with high quality voices and more features, Great work guys keep it up
                  </p>
               </div>
            </div>
            <div class="col mt-md120 mx-auto">
               <div class="single-testimonial">
                  <div class="st-img">
                     <img src="assets/images/kundan.jpg" class="img-fluid d-block mx-auto" style="border-radius:50%;">
                  </div>
                  <div class="mt20 md-md50 f-24 f-md-28 w600 lh140 black-clr">Kundan Choudhary  </div>
                  <!-- <div class="stars mt10">
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                  </div> -->
                  <img src="assets/images/quote.webp" alt="Quotes" class="mx-auto d-block img-fluid mt20 mt-md50 ">
                  <p class="mt20 f-18 f-md-20 lh140 w400 text-center">
                     Best Text To Speech ever, literally love this thing. I have dumped my old robotic voice overs subscriptions with this.. High quality audio, never experienced tech like this before. Well done to the TubeTrafficAi team!
                  </p>
               </div>
            </div>
            <div class="col mt-md120 mx-auto">
               <div class="single-testimonial">
                  <div class="st-img">
                     <img src="assets/images/atul-sir.png" class="img-fluid d-block mx-auto " alt="">
                  </div>
                  <div class="mt20 md-md50 f-24 f-md-28 w600 lh140 black-clr"> Atul Pareek  </div>
                  <!-- <div class="stars mt10">
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                  </div> -->
                  <img src="assets/images/quote.webp" alt="Quotes" class="mx-auto d-block img-fluid mt20 mt-md50 ">
                  <p class="mt20 f-18 f-md-20 lh140 w400 text-center">
                     Best real time voice changer out there right now. Extremely realistic voices with an amazing community and incredibly receptive development team. Kudos To Team TubeTrafficAi.
                  </p>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Testimonial Section End -->

 
   <!-- CTA Section Start -->
   <div class="cta-section-white">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-34 w700 lh140 text-center white-clr">
               Get TubeTraffic Ai Today And Save $384
               </div>
               <div class="f-22 f-md-24 w400 lh140 text-center white-clr mt10">
                  <span class="w700">Just Pay Once</span> Instead Of <strike class="red-clr"> Monthly</strike>
               </div>
            </div>
         </div>
         <div class="row mt20 mt-md50">
            <div class="col-md-8 text-center">
               <a href="#buynow" class="cta-link-btn"> > > >  Get Instant Access To TubeTrafficAi < < <  </a>
               <img src="assets/images/compaitable-gurantee1.webp" alt="Compaitable Gurantee" class="mx-auto d-block img-fluid mt20 mt-md30">
            </div>
            <div class="col-md-4 mt20 mt-md0 px-md0">
               <div class="countdown-container">
                  <div class="f-20 f-md-24 w400 lh140 text-center red-clr mb10">
                     <span class="w700 white-clr">Hurry up!</span>  Price Increases In
                  </div>
                  <div class="countdown counter-white text-center">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-37 timerbg oswald">00&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 smmltd">Days</span> 
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-37 timerbg oswald">00&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-37 timerbg oswald">00&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-37 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> 
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- CTA Section End -->

   <!-- Limited Section Start    -->
   <div class="limited-time-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="row align-items-center flex-column flex-md-row justify-content-center">
                  <div class="col-md-10 col-12 f-20 f-md-22 w600 text-center white-clr px-md0">
                     <img src="assets/images/caution-icon.webp">Warning! Your $3,583.11 Bonuses Package Expires When Timer Hits ZERO <img src="assets/images/caution-icon.webp">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   <!-- Limited Section End -->

   <div class="white-section">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 f-28 f-md-50 w700 lh140 black-clr text-center">
               YouTube Isn’t About “Videos” 
            </div>
            <div class="col-md-6 col-12">
               <img src="assets/images/v1.png" class="img-fluid mx-auto d-block mt20 mt-md50">
            </div>
            <div class="col-md-6 col-12">
               <div class="f-18 f-md-22 w400 lh140 black-clr mt20 mt-md50">
               I know you may think that this is dumb…
               </div>

               <div class="f-18 f-md-22 w400 lh140 black-clr mt20 mt-md20">
               And that YouTube was created to be a video hub…
               </div>

               <div class="f-18 f-md-22 w400 lh140 black-clr mt20 mt-md20">
               But we are in 2023, and we have AI now…
               </div>

               <div class="f-18 f-md-22 w400 lh140 black-clr mt20 mt-md20">
               And there is hundreds of ways that we leverage Youtube…
               </div>

               <div class="f-18 f-md-22 w400 lh140 black-clr mt20 mt-md20">
               WITHOUT ever creating a video…
               </div>

               <div class="f-18 f-md-22 w400 lh140 black-clr mt20 mt-md20">
               And WITHOUT even uploading a video…
               </div>

               <div class="f-18 f-md-22 w700 lh140 red-clr mt20 mt-md20">
               Can you believe it?
               
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr mt20 mt-md20">
               And we make hundreds of dollars every single day…
               </div>
            </div>
         </div>
         <div class="row">
               <div class="col-12 mt20 mt-md50">
                  <img src="assets/images/income-proof.png" class="img-fluid mx-auto d-block">
               </div>
            </div>
      </div>
   </div>

   <div class="grey-section">
      <div class="container">
      <div class="row align-items-center">
            <div class="col-12 col-md-10 mx-auto f-28 f-md-50 w700 lh140 black-clr text-center">
            We Generate <span class="red-clr">10,000’s Of Clicks</span> WITHOUT Videos…
            </div>
            <div class="col-md-6 col-12 order-md-2">
               <img src="assets/images/v2.png" class="img-fluid mx-auto d-block mt20 mt-md50">
            </div>
            <div class="col-md-6 col-12 order-md-1">
               <div class="f-18 f-md-22 w400 lh140 black-clr mt20 mt-md50">
               Yea, your eyes aren’t fooling you…
               </div>

               <div class="f-18 f-md-22 w400 lh140 black-clr mt20 mt-md20">
               We generate 100,000+ clicks
               </div>

               <div class="f-18 f-md-22 w400 lh140 black-clr mt20 mt-md20">
               Without creating videos, or even uploading a video…  
               </div>

               <div class="f-18 f-md-22 w400 lh140 black-clr mt20 mt-md20">
               What we do is actually brilliant…
               </div>

               <div class="f-18 f-md-22 w400 lh140 black-clr mt20 mt-md20">
               We hijack other people's videos…
               </div>

               <div class="f-18 f-md-22 w400 lh140 black-clr mt20 mt-md20">
               Redirect their views to any link we want…  
               </div>

               
               <div class="f-18 f-md-22 w400 lh140 black-clr mt20 mt-md20">
               And… that’s it
               </div>
            </div>
         </div>
         <div class="row">
               <div class="col-12 mt20 mt-md50">
                  <img src="assets/images/traffic-proof.webp" class="img-fluid mx-auto d-block">
               </div>
               <div class="f-22 f-md-28 w600 lh140 text-center black-clr mt20 mt-md50">
               We just hijack what competitors already did… 
               </div>
               <div class="f-22 f-md-28 w600 lh140 text-center black-clr mt10 mt-md10">
               And leverage their videos, views, and audience… To make money…
               </div>
            </div>
      </div>
   </div>

   <div class="white-section">
      <div class="container">
         <div class="row align-items-center">
            
            <div class="col-md-6 col-12">
               <img src="assets/images/v3.png" class="img-fluid mx-auto d-block mt20 mt-md50">
            </div>
            <div class="col-md-6 col-12 pr-md0">
               <div class="f-28 f-md-50 w700 lh140 black-clr mt20 mt-md50">
               And Without Paying A Penny In Ads Too…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr mt20 mt-md20">
               It wouldn’t be brilliant if it costs money 
               </div>

               <div class="f-18 f-md-22 w400 lh140 black-clr mt20 mt-md20">
               What we do requires exactly zero dollars…
               </div>

               <div class="f-18 f-md-22 w400 lh140 black-clr mt20 mt-md20">
               No money for ads, no paying for professionals…
               </div>

               <div class="f-18 f-md-22 w400 lh140 black-clr mt20 mt-md20">
               None of that is needed…  
               </div>

               <div class="f-18 f-md-22 w400 lh140 black-clr mt20 mt-md20">
               You simply select your niche, and embrace yourself for the traffic plethora…
               </div>
            </div>
         </div>
      </div>
   </div>

   <div class="notalong-section">
      <div class="container">
      <div class="row align-items-center">
            
            <!-- <div class="col-md-6 col-12">
               <img src="assets/images/v3.png" class="img-fluid mx-auto d-block mt20 mt-md50">
            </div> -->
            <div class="col-md-6 col-12">
               <div class="f-28 f-md-50 w700 lh140 white-clr">
               Ai Does It All For Us… 
               </div>
               <div class="f-18 f-md-22 w400 lh140 white-clr mt20 mt-md20">
               It’s crazy…
               </div>

               <div class="f-18 f-md-22 w400 lh140 white-clr mt20 mt-md20">
               But we really don’t do any of the work…
               </div>

               <div class="f-18 f-md-22 w400 lh140 white-clr mt20 mt-md20">
               Our AI model do everything for us… 
               </div>

               <div class="f-18 f-md-22 w400 lh140 white-clr mt20 mt-md20">
               We just click a few buttons, which takes a bit over 50 seconds…  
               </div>

               <div class="f-18 f-md-22 w400 lh140 white-clr mt20 mt-md20">
               And we are done…  
               </div>
               <div class="f-18 f-md-22 w400 lh140 white-clr mt20 mt-md20">
               AI takes care of all the heavy work…
               </div>
            </div>
         </div>
      </div>
   </div>

   <div class="white-section">
      <div class="container">
         <div class="row align-items-center">
            
            <div class="col-md-6 col-12">
               <img src="assets/images/productbox1.png" class="img-fluid mx-auto d-block mt20 mt-md50">
            </div>
            <div class="col-md-6 col-12 pr-md0">
               <div class="f-28 f-md-45 w700 lh140 black-clr mt20 mt-md50">
              <span class="w400">Best Part Is…</span>
              <br>
              It Works In Any Niche…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr mt20 mt-md20">
               It doesn’t matter what niche you are in…  
               </div>

               <div class="f-18 f-md-22 w400 lh140 black-clr mt20 mt-md20">
               TubeTraffic Ai got your back…
               </div>

               <div class="f-18 f-md-22 w400 lh140 black-clr mt20 mt-md20">
               It works in any niche, in any industry…
               </div>

               <div class="f-18 f-md-22 w400 lh140 black-clr mt20 mt-md20">
               And it works in any country too
               </div>

              
            </div>
         </div>
      </div>
   </div>

   <div class="grey-section">
      <div class="container">
      <div class="row align-items-center">
           
            <div class="col-md-6 col-12 order-md-2">
               <img src="assets/images/v4.png" class="img-fluid mx-auto d-block mt20 mt-md50">
            </div>
            
            <div class="col-md-6 col-12 order-md-1 pl-md0">
               <div class="f-28 f-md-50 w700 lh140 black-clr mt20 mt-md50">
               And This Isn’t Useless Traffic… 
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr mt20 mt-md20">
               If you think that we just drive random traffic 
               </div>

               <div class="f-18 f-md-22 w400 lh140 black-clr mt20 mt-md20">
               You are wrong, my friend…  
               </div>

               <div class="f-18 f-md-22 w400 lh140 black-clr mt20 mt-md20">
               We drive laser-targeted clicks to any link we want…
               </div>

               <div class="f-18 f-md-22 w400 lh140 black-clr mt20 mt-md20">
               This is way more targeted than any ad network you may ever use 
               </div>

               <div class="f-18 f-md-22 w700 lh140 black-clr mt20 mt-md20">
               Yes, more targeted than facebook, google, youtube, and twitter
               </div>

               
               <div class="f-18 f-md-22 w700 lh140 black-clr mt20 mt-md20">
               COMBINED  
               </div>
            </div>
         </div>
      </div>
   </div>


   <div class="itshuge-section">
      <div class="container">
         <div class="row">
            <div class="col-12 f-28 f-md-50 text-center black-clr w700 lh140">
            It’s Traffic That Turns Into Cash…
            </div>
            <div class="col-12 f-20 f-md-28 text-center black-clr w600 lh140 mt10 mt-md10">
            Our traffic converts to cold, hard cash, Like this
            </div>
            <div class="col-md-6 col-12 mt20 mt-md40">
               <img src="assets/images/cash-img1.png" class="img-fluid mx-auto d-block">
            </div>
            <div class="col-md-6 col-12 mt20 mt-md40">
               <img src="assets/images/cash-img2.png" class="img-fluid mx-auto d-block">
            </div>
            
            <div class="col-12 f-20 f-md-28 text-center black-clr w600 lh140 mt20 mt-md30">
            I mean, look…Just yesterday, we made a bit over $1,000
            </div>
            <div class="col-md-6 offset-md-3 col-12 mt20 mt-md40">
               <img src="assets/images/cash-img3.png" class="img-fluid mx-auto d-block">
               <div class="f-20 f-md-28 text-center black-clr w600 lh140 mt20 mt-md30">
                  All thanks to TubeTraffic Ai
               </div>
            </div>
         </div>
      </div>
   </div>

   <div class="seconds-section">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-md-6 col-12">
               <img src="assets/images/seconds-img.png" class="img-fluid mx-auto d-block">
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0">
               <div class="f-24 f-md-42 black-clr w700 lh140">
               All It Takes Is 56 Seconds…
               </div>
               <div class="f-18 f-md-22 lh140 w400 black-clr mt10 mt-md10">
               That’s the fun part…
               </div>
               <div class="f-18 f-md-22 lh140 w400 black-clr mt10 mt-md10">
               We put in less than a minute everyday to get these results
               </div>
               <ul class="dont-list mt10 mt-md10 f-18 f-md-22 lh140 black-clr w400">
                  <li>No hard work…</li>
                  <li>No setup…</li>
                  <li>No configuration…</li>
               </ul>
               <div class="f-18 f-md-22 lh140 w400 black-clr mt10 mt-md10">
                  Literally, anyone can do it…
               </div>
               <div class="f-18 f-md-22 lh140 w400 black-clr mt10 mt-md10">
                  Even if you never made money before
               </div>
            </div>
         </div>
      </div>
   </div>

   <div class="solution-section">
      <div class="container">
         <div class="row">
            <div class="col-12 f-28 f-md-50 lh140 text-center white-clr w700">
            TubeTraffic Ai Is The Solution You Always Wanted…
            </div>
         </div>
      </div>
   </div>

   <section class="there-section">
      <div class="container hi-their-box">
         <div class="row">
            <div class="col-12 text-center">
               <div class="hi-their-shadow">
                  <div class="f-28 f-md-45 lh140 black-clr caveat w700 hi-their-head">
                     Hey There,
                  </div>
               </div>
            </div>
         </div>
         <div class="row mt20 mt-md80">
            <div class="col-md-3 text-center">
               <div class="figure mr15 mr-md0">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/pranshu-gupta.webp" alt="Pranshu Gupta" class="mx-auto d-block img-fluid  img-shadow">
                  <div class="figure-caption mt10">
                     <div class="f-24 f-md-28 w700 lh140 black-clr caveat text-center">
                        Pranshu Gupta
                     </div>
                  </div>
               </div>
               <div class="figure mt20 mt-md30">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/bizomart-team.webp" alt="Bizomart" class="mx-auto d-block img-fluid  img-shadow">
                  <div class="figure-caption mt10">
                     <div class="f-24 f-md-28 w700 lh140 black-clr caveat text-center">
                        Bizomart
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-md-9 mt20 mt-md0">
               <div class="f-28 f-md-38 lh140 black-clr w400">
                  It's <span class="w600">Pranshu</span> along with <span class="w600">Bizomart.</span> 
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr mt20 mt-md30">
               I'm a full-time internet marketer…
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr mt20">
               Over the years, I tried dozens (it could be hundreds even…) of methods to make money online…
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr mt20">
               But the only thing that gave me the life I always wanted was…
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr mt20">
               Creating YouTube Videos…
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr mt20">
               <span class="w600 f-22 f-md-32 lh140">Selling HIGH QUALITY content & voice overs as an agency to a network of 3.2 million hungry buyers</span> 
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr mt20">
               Who are spending 1000s of dollars to get their work done on fiverr, freelancers, and other platforms…
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr mt20">
                  It's the easiest, and fastest way to build wealth…
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr mt20">
                  Not just make some pocket change…&nbsp;
               </div>
               <div class="f-20 f-md-22 lh140 w600 black-clr mt20">
                  Thanks to that…
               </div>
            </div>
         </div>

         <div class="row mt20 mt-md50">
            <div class="col-12">
               <div class="blue-border"></div>
            </div>
         </div>

         <div class="row mt20 mt-md50">
            <div class="col-12 text-center">
               <div class="f-22 f-md-28 lh140 black-clr w600">
                  I can spend my time as I please
               </div>
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/red-underline.webp" alt="Quotes" class="mx-auto d-block img-fluid  mt10">
            </div>
         </div>

         <div class="row mt20 mt-md50">
            <div class="col-md-3">
               <figure>
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/family.webp" alt="Family" class="mx-auto d-block img-fluid ">
                  <figure-caption>
                     <div class="f-20 f-md-22 w600 lh140 black-clr mt10 text-center">
                        Spend it with my family
                     </div>
                  </figure-caption>
               </figure>
            </div>
            <div class="col-md-3">
               <figure>
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/travel.webp" alt="Travel" class="mx-auto d-block img-fluid ">
                  <figure-caption>
                     <div class="f-20 f-md-22 w600 lh140 black-clr mt10 text-center">
                        Travel
                     </div>
                  </figure-caption>
               </figure>
            </div>
            <div class="col-md-3">
               <figure>
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/sunset-beach.webp" alt="Enjoy a sunset on the beach" class="mx-auto d-block img-fluid ">
                  <figure-caption>
                     <div class="f-20 f-md-22 w600 lh140 black-clr mt10 text-center">
                        Enjoy a sunset on the beach
                     </div>
                  </figure-caption>
               </figure>
            </div>
            <div class="col-md-3">
               <figure>
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/video-games.webp" alt="Or even play video games on my PS5" class="mx-auto d-block img-fluid ">
                  <figure-caption>
                     <div class="f-20 f-md-22 w600 lh140 black-clr mt10 text-center">
                        Or even play video games on my PS5
                     </div>
                  </figure-caption>
               </figure>
            </div>
         </div>
         <div class="row mt20 mt-md80">
            <div class="col-12 text-center">
               <div class="f-20 f-md-24 w600 lh140 black-clr">
                  And the best part is… No matter what I do with my day… I still make a lot of money…
               </div>
               <div class="f-20 f-md-24 w400 lh140 black-clr mt20">
                  And I'm not telling you this to brag… Not at all… I'm telling you this to demonstrate why you should even listen to me…
               </div>
            </div>
         </div>
      </div>
   </section>

   <div class="white-section">
      <div class="container">
         <div class="row align-items-center">
            <div class="f-28 f-md-50 text-center black-clr lh140 w700">
               <img src="assets/images/buld-img.png">  have One Goal This Year…  <img src="assets/images/buld-img.png">
            </div>
            
            <div class="col-md-6 col-12 mt20 mt-md40">
               
               <div class="f-18 f-md-22 w400 lh140 black-clr mt20 mt-md20">
               You see…
               </div>

               <div class="f-18 f-md-22 w400 lh140 black-clr mt20 mt-md20">
               Making money online is good any everything…  
               </div>

               <div class="f-18 f-md-22 w400 lh140 black-clr mt20 mt-md20">
               But what’s 10x better… Is helping someone else make it…
               </div>

               <div class="f-18 f-md-22 w400 lh140 black-clr mt20 mt-md20">
               This is why I decided that in 2023 I will help 100 new people…  
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr mt20 mt-md20">
               Quit their job, and have the financial freedom they always wanted… 
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr mt20 mt-md20">
               So far, it’s been going GREAT…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr mt20 mt-md20">
               I’ve managed to help countless people…
               </div>

              
            </div>
            <div class="col-md-6 col-12 mt20 mt-md40">
               <img src="assets/images/v5.png" class="img-fluid mx-auto d-block">
            </div>
         </div>
      </div>
   </div>

   <div class="success-section">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-10 offset-md-1 f-28 f-md-50 lh140 w700 text-center black-clr">
            And I’m Here To Help You My Next Success Story…
            </div>
            <div class="col-md-6 col-12 mt20 mt-md40 order-md-2">
               
               <div class="f-18 f-md-22 w400 lh140 black-clr mt20 mt-md20">
               Listen, if you’re reading this page right now…  
               </div>

               <div class="f-18 f-md-22 w400 lh140 black-clr mt20 mt-md20">
               It means one thing, you’re dedicated to make money online…
               </div>

               <div class="f-18 f-md-22 w400 lh140 black-clr mt20 mt-md20">
               And I have some good news for you…  
               </div>

               <div class="f-18 f-md-22 w400 lh140 black-clr mt20 mt-md20">
               On this page, I will give you everything you need…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr mt20 mt-md20">
               To get started and join me and thousands of other profitable members…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr mt20 mt-md20">
               Who changed their lives forever…  
               </div>

              
            </div>
            <div class="col-md-6 col-12 mt20 mt-md40 order-md-1">
               <img src="assets/images/v6.png" class="img-fluid mx-auto d-block">
            </div>
         </div>
      </div>
   </div>

   <div class="cool-section">
      <div class="container">
         <div class="row">
            <div class="col-12 f-28 f-md-50 lh140 w700 text-center black-clr">
               <img src="assets/images/cool-emoji.png"> Let Me Show You Something Cool…
               <img src="assets/images/red-underline-brush2.png" class="img-fluid mx-auto d-block">
         </div>
         <div class="mt20 mt-md50 demo-video">
                        
                        <div style="padding-bottom: 56.25%;position: relative;">
                        <iframe src="https://tubetrafficai.oppyo.com/video/embed/lrz2tuvwkt" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important; border-radius:5px;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div>
                     </div>
            <div class="col-12">
               <div class="f-22 f-md-28 lh140 w600 text-center black-clr mt20 mt-md30">
               You see this endless stream of payments?
               </div>
               <div class="f-22 f-md-28 lh140 w400 text-center black-clr mt10 mt-md10">
               We got all of those doing only one thing…
               </div>
               <img src="assets/images/cool-img.png" class="img-fluid mx-auto d-block mt20 mt-md20">
               <div class="f-22 f-md-28 lh140 w400 text-center black-clr mt20 mt-md30">
               It allowed me to hijack hundreds of thousands of clicks for free..
               </div>
               <div class="f-22 f-md-28 lh140 w400 text-center black-clr mt10 mt-md20">
               And redirect them to any link i want… And enjoy my life…
               </div>
               <div class="f-22 f-md-28 lh140 w600 text-center black-clr mt10 mt-md20">
             
               This month alone…We made <span class="red-clr underline">$32,556.34 on one of our affiliate accounts…</span>
               </div>
               <img src="assets/images/cool-img2.png" class="img-fluid mx-auto d-block mt20 mt-md20">
               <div class="f-22 f-md-28 lh140 w600 text-center black-clr mt10 mt-md20">
             
               And I’m gonna show you exactly how I do it…
               </div>
            </div>
         </div>
      </div>
   </div>

      <div class="proudly-section" id="product">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="prdly-pres f-28 f-md-38 w700 lh140 text-center white-clr caveat">
                  Proudly Presenting…
                  </div>
               </div>
               <div class="col-12 mt-md50 mt20 text-center">
               <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 800 131.43" style="max-height:100px;">
                        <defs>
                            <style>
                            .cls-1 {
                                fill: #fff;
                            }
        
                            .cls-2 {
                                fill: #fd2f15;
                            }
        
                            .cls-3 {
                                fill: #fbbf33;
                            }
                            </style>
                        </defs>
                        <g id="Layer_1-2" data-name="Layer 1">
                            <g>
                            <g>
                                <path class="cls-3" d="m105.61,78.06l40.05,23.12c6.01,3.47,13.51-.87,13.51-7.8v-46.25c0-6.93-7.51-11.27-13.51-7.8l-40.05,23.12c-6.01,3.47-6.01,12.14,0,15.6Z"></path>
                                <g>
                                <rect class="cls-3" x="26.92" y="58.86" width="16.03" height="46.81"></rect>
                                <path class="cls-2" d="m125.22,26.34h-7.16v46.06c0,32.6-26.43,59.03-59.03,59.03H0v-65.94C0,32.89,26.43,6.45,59.03,6.45h38.52l-12.38,19.88h-26.14c-21.59,0-39.16,17.57-39.16,39.15v46.07h39.16c21.59,0,39.16-17.57,39.16-39.16V26.34h-5.79L108.81,0l16.41,26.34Z"></path>
                                <path class="cls-3" d="m67.04,46.21h-16.02v59.46h8.01c2.76,0,5.45-.35,8.01-.99v-58.47Z"></path>
                                <path class="cls-3" d="m90.77,33.56h-16.03v68.14c7.57-4.07,13.39-10.99,16.03-19.31v-48.83Z"></path>
                                </g>
                            </g>
                            <g>
                                <g>
                                <path class="cls-1" d="m195.69,38.68v-5.58h49.35v5.58h-21.57v62.48h-6.21v-62.48h-21.57Z"></path>
                                <path class="cls-1" d="m283.29,81.65v-31.54h5.92v51.05h-5.92v-8.47h-.47c-1.2,2.61-3.09,4.79-5.68,6.53-2.59,1.74-5.76,2.61-9.5,2.61-3.26,0-6.15-.71-8.67-2.14-2.53-1.43-4.51-3.53-5.95-6.3-1.44-2.77-2.16-6.17-2.16-10.2v-33.07h5.92v32.7c0,3.99,1.17,7.2,3.52,9.62,2.35,2.43,5.36,3.64,9.04,3.64,2.3,0,4.52-.55,6.65-1.66,2.13-1.11,3.88-2.73,5.25-4.87,1.37-2.14,2.06-4.77,2.06-7.89Z"></path>
                                <path class="cls-1" d="m306.05,101.16V33.09h5.95v26.75h.53c.66-1.53,1.62-3.1,2.87-4.72,1.25-1.62,2.9-2.97,4.95-4.07,2.05-1.1,4.64-1.65,7.76-1.65,4.19,0,7.86,1.1,11.03,3.31,3.17,2.21,5.64,5.28,7.41,9.24,1.77,3.95,2.66,8.56,2.66,13.81s-.88,9.89-2.64,13.86c-1.76,3.97-4.22,7.06-7.38,9.27-3.16,2.22-6.82,3.32-10.98,3.32-3.1,0-5.69-.55-7.76-1.66-2.07-1.11-3.74-2.48-5-4.1-1.26-1.63-2.24-3.22-2.92-4.77h-.73v9.47h-5.75Zm5.85-25.49c0,4.12.61,7.77,1.84,10.95,1.23,3.18,3,5.67,5.32,7.48,2.31,1.81,5.12,2.71,8.42,2.71s6.26-.94,8.61-2.81c2.35-1.87,4.13-4.4,5.35-7.59,1.22-3.19,1.83-6.77,1.83-10.73s-.6-7.46-1.81-10.6c-1.21-3.15-2.99-5.64-5.33-7.49-2.35-1.85-5.23-2.78-8.64-2.78s-6.14.89-8.46,2.68c-2.32,1.78-4.08,4.24-5.3,7.38-1.22,3.14-1.83,6.74-1.83,10.82Z"></path>
                                <path class="cls-1" d="m382.86,102.22c-4.76,0-8.88-1.11-12.36-3.34-3.48-2.23-6.16-5.32-8.06-9.27s-2.84-8.51-2.84-13.68.95-9.71,2.84-13.71c1.89-4,4.52-7.13,7.88-9.41,3.36-2.27,7.24-3.41,11.65-3.41,2.77,0,5.44.5,8.01,1.51,2.57,1.01,4.88,2.55,6.93,4.62,2.05,2.07,3.67,4.68,4.87,7.81,1.2,3.14,1.79,6.84,1.79,11.12v2.92h-39.88v-5.22h33.83c0-3.28-.66-6.23-1.98-8.86-1.32-2.63-3.15-4.7-5.48-6.23-2.34-1.53-5.04-2.29-8.09-2.29-3.23,0-6.08.86-8.54,2.59-2.46,1.73-4.38,4.01-5.77,6.85-1.38,2.84-2.09,5.94-2.11,9.31v3.12c0,4.05.7,7.59,2.11,10.62,1.41,3.02,3.41,5.37,6,7.03,2.59,1.66,5.66,2.49,9.21,2.49,2.41,0,4.54-.38,6.36-1.13,1.83-.75,3.37-1.77,4.62-3.04,1.25-1.27,2.2-2.68,2.84-4.2l5.62,1.83c-.78,2.15-2.04,4.13-3.81,5.95-1.76,1.82-3.95,3.27-6.58,4.37-2.63,1.1-5.65,1.65-9.06,1.65Z"></path>
                                <path class="cls-1" d="m412.07,44.96v-11.86h55.9v11.86h-20.84v56.2h-14.22v-56.2h-20.84Z"></path>
                                <path class="cls-1" d="m471.62,101.16v-51.05h13.73v8.91h.53c.93-3.17,2.49-5.57,4.69-7.2,2.19-1.63,4.72-2.44,7.58-2.44.71,0,1.47.04,2.29.13.82.09,1.54.21,2.16.37v12.56c-.66-.2-1.58-.38-2.76-.53-1.17-.15-2.25-.23-3.22-.23-2.08,0-3.94.45-5.57,1.35-1.63.9-2.91,2.14-3.86,3.74-.94,1.6-1.41,3.43-1.41,5.52v28.88h-14.16Z"></path>
                                <path class="cls-1" d="m523,102.12c-3.26,0-6.16-.57-8.71-1.71-2.55-1.14-4.56-2.84-6.03-5.08-1.47-2.25-2.21-5.06-2.21-8.42,0-2.84.52-5.22,1.56-7.15,1.04-1.93,2.46-3.48,4.25-4.65,1.79-1.17,3.84-2.06,6.13-2.66,2.29-.6,4.7-1.02,7.23-1.26,2.97-.31,5.36-.6,7.18-.88,1.82-.28,3.13-.69,3.95-1.25.82-.55,1.23-1.37,1.23-2.46v-.2c0-2.1-.66-3.73-1.98-4.89-1.32-1.15-3.19-1.73-5.6-1.73-2.55,0-4.58.56-6.08,1.68-1.51,1.12-2.5,2.52-2.99,4.2l-13.09-1.06c.66-3.1,1.97-5.79,3.92-8.06,1.95-2.27,4.47-4.02,7.56-5.25,3.09-1.23,6.67-1.84,10.75-1.84,2.84,0,5.55.33,8.16,1,2.6.66,4.92,1.7,6.95,3.09,2.03,1.4,3.63,3.19,4.8,5.37,1.17,2.18,1.76,4.79,1.76,7.83v34.43h-13.43v-7.08h-.4c-.82,1.6-1.92,3-3.29,4.2-1.37,1.21-3.02,2.15-4.95,2.82-1.93.68-4.15,1.01-6.68,1.01Zm4.05-9.77c2.08,0,3.92-.42,5.52-1.25s2.85-1.96,3.76-3.37c.91-1.42,1.36-3.02,1.36-4.82v-5.42c-.44.29-1.05.55-1.81.78s-1.62.44-2.58.63c-.95.19-1.91.35-2.86.5-.95.14-1.82.27-2.59.38-1.66.24-3.11.63-4.35,1.16-1.24.53-2.21,1.25-2.89,2.14-.69.9-1.03,2.01-1.03,3.34,0,1.93.7,3.4,2.11,4.4,1.41,1.01,3.2,1.51,5.37,1.51Z"></path>
                                <path class="cls-1" d="m580.03,50.11h10.17v10.63h-10.17v40.41h-14.12v-40.41h-7.21v-10.63h7.21v-3.69c0-3.7.72-6.77,2.18-9.21,1.45-2.44,3.44-4.26,5.97-5.48,2.53-1.22,5.4-1.83,8.61-1.83,2.17,0,4.15.17,5.97.5,1.8.33,3.15.63,4.04.9l-2.53,10.63c-.55-.18-1.23-.34-2.04-.5s-1.64-.23-2.48-.23c-2.08,0-3.53.48-4.35,1.45-.83.96-1.23,2.31-1.23,4.04v3.42Z"></path>
                                <path class="cls-1" d="m616.12,50.11h10.17v10.63h-10.17v40.41h-14.12v-40.41h-7.21v-10.63h7.21v-3.69c0-3.7.72-6.77,2.18-9.21,1.45-2.44,3.44-4.26,5.97-5.48,2.53-1.22,5.4-1.83,8.61-1.83,2.17,0,4.15.17,5.97.5,1.8.33,3.15.63,4.04.9l-2.53,10.63c-.55-.18-1.23-.34-2.04-.5-.81-.15-1.64-.23-2.48-.23-2.08,0-3.53.48-4.35,1.45-.83.96-1.23,2.31-1.23,4.04v3.42Z"></path>
                                <path class="cls-1" d="m642.05,43.53c-2.11,0-3.9-.7-5.4-2.09-1.5-1.4-2.24-3.08-2.24-5.05s.75-3.66,2.26-5.07c1.51-1.41,3.3-2.11,5.38-2.11s3.91.7,5.4,2.09c1.5,1.4,2.24,3.08,2.24,5.05s-.75,3.66-2.24,5.07c-1.5,1.41-3.3,2.11-5.4,2.11Zm-7.11,57.63v-51.05h14.16v51.05h-14.16Z"></path>
                                <path class="cls-1" d="m683.46,102.15c-5.23,0-9.72-1.11-13.48-3.34-3.76-2.23-6.64-5.32-8.64-9.29-2.01-3.97-3.01-8.53-3.01-13.69s1.01-9.82,3.04-13.78c2.03-3.95,4.91-7.05,8.66-9.27,3.74-2.23,8.2-3.34,13.36-3.34,4.45,0,8.35.81,11.7,2.43,3.35,1.62,5.99,3.89,7.94,6.81,1.95,2.92,3.02,6.36,3.22,10.3h-13.36c-.38-2.55-1.37-4.6-2.97-6.16-1.61-1.56-3.71-2.34-6.3-2.34-2.19,0-4.1.59-5.73,1.78-1.63,1.19-2.9,2.91-3.81,5.17s-1.36,5-1.36,8.21.45,6.03,1.35,8.31c.9,2.28,2.17,4.02,3.81,5.22,1.64,1.2,3.56,1.79,5.75,1.79,1.62,0,3.07-.33,4.37-1,1.3-.66,2.37-1.63,3.22-2.91.85-1.27,1.41-2.81,1.68-4.6h13.36c-.22,3.9-1.28,7.33-3.17,10.29-1.89,2.96-4.5,5.27-7.83,6.93-3.32,1.66-7.26,2.49-11.8,2.49Z"></path>
                                </g>
                                <g>
                                <path class="cls-3" d="m800,37.72c0,4.31-3.5,7.81-7.82,7.81s-7.81-3.5-7.81-7.81,3.5-7.82,7.81-7.82,7.82,3.5,7.82,7.82Z"></path>
                                <path class="cls-2" d="m736.11,85.52c-.09,0-.18,0-.27-.01-.09,0-.18-.02-.26-.03.18.01.35.05.53.05Z"></path>
                                <path class="cls-2" d="m784.37,50.18v32.89c0,1.31-1.05,2.37-2.35,2.43-.03,0-.07.02-.11.02h-.37l-18.68-41.36-3.1-6.86c-2.03-4.5-6.52-7.4-11.47-7.4s-9.42,2.9-11.46,7.4l-11.96,26.47-10.4,23.03c-3.04,6.73,1.88,14.36,9.28,14.36h66.01c5.65,0,10.25-4.58,10.25-10.24v-40.74h-15.63Zm-48.25,35.34c-.09,0-.18,0-.27-.01-.09,0-.18-.02-.26-.03-2.25-.33-3.65-2.75-2.67-4.91l12.93-28.64c.95-2.1,3.93-2.1,4.89,0l15.17,33.59h-29.79Z"></path>
                                </g>
                            </g>
                            </g>
                        </g>
                        </svg>
                  </div>
                  <div class="col-12 f-md-42 f-28 w700 text-center white-clr lh140 mt-md40 mt20 p0">
                    World's First Google-Bard Powered AI App That 
                    <span class="red-brush-top"> *Legally* Hacks Into YouTube's 800 Million Videos… </span> 
                </div>
                <div class="col-12 mt-md20 mt20 text-center">
                    <div class="f-18 f-md-24 w600 lh140 white-clr ">
                        <i>Then Redirect Their Views To ANY Link or Offer of Our Choice & Sent Us…</i>
                    </div>
                </div>
                <div class="col-12 mt-md20 mt20 text-center">
                    <div class="f-md-24 f-18 w500 yellow-clr lh140 post-heading">
                        92,346 Views In 4 Hours Or Less… Making Us $346.34 Daily
                    </div>
                </div>
                <div class="col-12 mt-md30 mt20 text-center">
                <div class="col-12 f-md-24 f-18 w400 white-clr lh140">
                Without Creating Videos | Without Uploading | Without Ads I No Upfront Cost at All.
                </div>
               </div>
               <div class="row d-flex flex-wrap align-items-center mt20 mt-md50">
                  <div class="col-12 col-md-10 mx-auto">
                     <img src="assets/images/product-image.webp" class="img-fluid d-block mx-auto mt20 mt-md30">
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-12">
                  <div class="f-28 f-md-30 w700 lh140 text-center white-clr mt20 mt-md50">
                  Click Here Now To Get Started
                  </div>
                  <div class="f-22 f-md-24 w400 lh140 text-center white-clr mt10">
                     <span class="w700">Just Pay Once</span> Instead Of <strike class="red-clr"> Monthly</strike>
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md30">
               <div class="col-md-8 col-12 mx-auto text-center">
                  <a href="#buynow" class="cta-link-btn"> > > > Get Instant Access To TubeTrafficAi < < <  </a>
                  <img src="assets/images/compaitable-gurantee1.webp" alt="Compaitable Gurantee" class="mx-auto d-block img-fluid mt20 mt-md30">
               </div>
            
             </div>
         </div>
      </div>

      <div class="dominates-section">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-12 f-28 f-md-50 lh140 w700 text-center black-clr">
                  TubeTraffic Ai Dominates YouTube…
               </div>
               <div class="col-md-6 col-12 mt20 mt-md50">
                  <div class="f-18 f-md-22 lh140 w400 black-clr mt10 mt-md20">
                  This has never been done before…
                  </div>
                  <div class="f-18 f-md-22 lh140 w400  black-clr mt10 mt-md20">
                  TubeTraffic Ai is the only app on the market…
                  </div>
                  <div class="f-18 f-md-22 lh140 w400 black-clr mt10 mt-md20">
                  That really exploits YouTube for MASSIVE profits…
                  </div>
                  <div class="f-18 f-md-22 lh140 w400 black-clr mt10 mt-md20">
                  And it all happens in seconds… not even minutes…
                  </div>
                  <div class="f-18 f-md-22 lh140 w400 black-clr mt10 mt-md20">
                  Best part is…
                  </div>
                  <div class="f-18 f-md-22 lh140 w700 black-clr mt10 mt-md20">
                  It’s all legal and ethical…
                  </div>
                  <div class="f-18 f-md-22 lh140 w400 black-clr mt10 mt-md20">
                  And we make money like this…
                  </div>
               </div>
               <div class="col-md-6 col-12 mt20 mt-md50">
                  <img src="assets/images/v7.png" class="img-fluid mx-auto d-block">
               </div>
            </div>
            <div class="row">
               <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
                  <img src="assets/images/income-proof-img2.webp." class="img-fluid mx-auto d-block">
               </div>
            </div>
         </div>
      </div>

   <div class="niche-section">
      <div class="container">
         <div class="row align-items-center mt20 mt-md30">
            <div class="col-12 f-28 f-md-50 lh140 w700 black-clr text-center">
            It Works In ANY Niche…
            </div>
            <div class="col-12 col-md-6 order-md-2 mt20 mt-md50">
               
               <div class="f-18 f-md-22 w400 lh140 black-clr mt20">
               Doesn’t matter what niche you are in…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr  mt20">
               TubeTraffic Ai got your back…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr mt20">
               And it doesn’t matter where you live…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr mt20">
               TubeTraffic Ai also got your back…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr mt20">
               You are no longer limited to where you live, or what niche you are in…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr  mt20">
               TubeTraffic Ai will expand your horizons…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr mt20">
               TubeTraffic Aiworks for
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md50 order-md-1">
               <img src="assets/images/niche-girl.png" alt="Matter" class="mx-auto d-block img-fluid">
            </div>
         </div>
        
         <div class="">
            <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 mt20 mt-md50">
               <div class="col">
                  <div class="figure">
                     <img src="assets/images/niche-img1.png" alt="Product Creators" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption">
                        <div class="f-22 f-md-28 w400 lh140 text-center black-clr mt10">
                        Affiliate Marketers
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="figure">
                     <img src="assets/images/niche-img2.png" alt="Affiliate Marketers" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption">
                        <div class="f-22 f-md-28 w400 lh140 text-center black-clr mt10">
                           CPA Marketers
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="figure">
                     <img src="assets/images/niche-img3.png" alt="eCom Store Owners" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption">
                        <div class="f-22 f-md-28 w400 lh140 text-center black-clr mt10">
                           Blog Owners
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col mt20 mt-md50">
                  <div class="figure">
                     <img src="assets/images/niche-img4.png" alt="Blog Owners" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption">
                        <div class="f-22 f-md-28 w400 lh140 text-center black-clr mt10">
                           Product Creators
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col mt20 mt-md50">
                  <div class="figure">
                     <img src="assets/images/niche-img5.png" alt="CPA Marketers" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption">
                        <div class="f-22 f-md-28 w400 lh140 text-center black-clr mt10">
                           Ecom Store Owners
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col mt20 mt-md50">
                  <div class="figure">
                     <img src="assets/images/niche-img6.png" alt="Video Marketers" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption">
                        <div class="f-22 f-md-28 w400 lh140 text-center black-clr mt10">
                           Local Business Owners
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col mt20 mt-md50 mx-auto">
                  <div class="figure">
                     <img src="assets/images/niche-img7.png" alt="Artists/Content Creators" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption">
                        <div class="f-22 f-md-28 w400 lh140 text-center black-clr mt10">
                           Agency Owners
                        </div>
                     </div>
                  </div>
               </div>
               
            </div>
         </div>

      </div>
   </div>

   <div class="white-section">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-md-6 col-12">
               <img src="assets/images/v8.png" class="img-fluid mx-auto d-block">
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0">
               <div class="f-28 f-md-50 lh140 w700 black-clr">
               Say Goodbye To Monthly Payments.
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr mt20 mt-md20">
               You don’t have to pay anything monthly to use TubeTraffic Ai
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr mt20 mt-md20">
               You get everything you need just by paying the low one-time fee
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr mt20 mt-md20">
               And that’s it…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr mt20 mt-md20">
               No hidden fees, and no extra money to be paid
               </div>
            </div>
         </div>
      </div>
   </div>

   <div class="overnight-section">
      <div class="container">
         <div class="row">
            <div class="col-12 f-28 f-md-50 lh140 w700 text-center black-clr">
            TubeTraffic Ai Changed My Life Overnight…
            </div>
            <div class="col-12 f-24 f-md-32 lh140 w400 text-center black-clr mt20 mt-md20">
            I know it's a hyped thing to say… But it’s true… <span class="w700">It took my bank account from looking like this…</span>
            </div>
            <div class="col-12 mt20 mt-md50">
               <img src="assets/images/overnight-img1.png" class="img-fluid mx-auto d-block">
               <div class="f-18 f-md-22 lh140 w600 text-center black-clr mt20 mt-md20">
               In just a few days…
               </div>
               <img src="assets/images/overnight-img2.png" class="img-fluid mx-auto d-block mt20 mt-md20">
            </div>
         </div>
      </div>
   </div>
                            
   <div class="work-section">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 f-28 f-md-50 lh140 w700 text-center black-clr">
               <span class="red-clr">How</span> Does It Work?
            </div>
            <div class="col-md-6 col-12 mt20 mt-md50">
               <div class="f-22 f-md-28 lh140 w700 black-clr">
               Let me explain…
               </div>
               <div class="f-18 f-md-22 lh140 w400 black-clr mt20 mt-md20">
               TubeTraffic Ai uses an AI model that no one else is using…
               </div>
               <div class="f-18 f-md-22 lh140 w400 black-clr mt20 mt-md20">
               And use it to exploit legal loopholes deep inside YouTube servers…
               </div>
               <div class="f-18 f-md-22 lh140 w400 black-clr mt20 mt-md20">
               By doing that we have control over millions and millions of videos
               </div>
               <div class="f-18 f-md-22 lh140 w400 black-clr mt20 mt-md20">
               And we can simply redirect those views…
               </div>
               <div class="f-18 f-md-22 lh140 w400 black-clr mt20 mt-md20">
               To any link we want…
               </div>
               <div class="f-18 f-md-22 lh140 w400 black-clr mt20 mt-md20">
               As simple as that…
               </div>
               <div class="f-18 f-md-22 lh140 w400 black-clr mt20 mt-md20">
               Told you it's very simple
               </div>
               <div class="f-18 f-md-22 lh140 w400 black-clr mt20 mt-md20">
               But incredibly powerful
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md50">
               <img src="assets/images/v9.png" class="img-fluid mx-auto d-block">
            </div>
         </div>
      </div>
   </div>

   <div class="different-section">
      <div class="container">
         <div class="row align-items-center">
           
            <div class="col-md-6 col-12 order-md-2">
               <div class="f-28 f-md-50 lh140 w700 black-clr">
                  <span class="red-clr">How</span> Is It Different?
               </div>
               <div class="f-18 f-md-22 lh140 w400 black-clr mt20 mt-md20">
               There is nothing on the market that can do what TubeTraffic Ai does…
               </div>
               <div class="f-18 f-md-22 lh140 w400 black-clr mt20 mt-md20">
               NONE…
               </div>
               <div class="f-18 f-md-22 lh140 w400 black-clr mt20 mt-md20">
               We have exclusive access to an AI model…
               </div>
               <div class="f-18 f-md-22 lh140 w400 black-clr mt20 mt-md20">
               That makes us daily money on autopilot…
               </div>
               <div class="f-18 f-md-22 lh140 w400 black-clr mt20 mt-md20">
               And we can guarantee that no one else has access to it…
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0 order-md-1">
               <img src="assets/images/v10.png" class="img-fluid mx-auto d-block">
            </div>
         </div>
      </div>
   </div>


   <div class="step-section">
      <div class="container">
         <div class="row">
         <div class="col-12 f-28 f-md-38 w500 lh140 black-clr text-center">
            <span class="f-28 f-md-50 w700 red-clr">You're Just 3 Clicks Away</span> 
            <br class="d-none d-md-block"> From Flooding Any Link With Buyers Traffic On-Demand…
            </div>
            <div class="col-12 col-md-8 mx-auto f-md-22 f-18 w600 text-center white-clr lh140 mt20 need-head">
            (Simple Ai Hack Changed Our Lives and Thousands Of Other…)
                </div>
            <div class="col-12 mt30 mt-md60">
               <div class="row align-items-center">
                  <div class="col-12 col-md-6">
                     <div class="step-shapes f-20 f-md-22 w600 black-clr">
                        Step 1
                     </div>
                     <div class="f-34 f-md-55 w700 lh140 mt15 caveat">
                        Login
                     </div>
                     <div class="col-12 col-md-10 f-20 f-md-22 w400 lh140 mt5 mt-md10 text-capitalize">
                     Click On Any Of The Buttons Below To Get Instant Access To TubeTrafficAi & Login to Powerful Dashboard (Dead-Easy To Use)
                     </div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 ">
                     <img src="assets/images/step1-img.png" alt="Video" class="mx-auto d-block img-fluid">
                  </div>
               </div>
            </div>
            <div class="col-12">
               <img src="assets/images/left-arrow.webp" class="img-fluid d-none d-md-block mx-auto " alt="Left Arrow">
            </div>
            <div class="col-12">
               <div class="row align-items-center ">
                  <div class="col-12 col-md-6 order-md-2">
                     <div class="step-shapes f-20 f-md-22 w600 black-clr">
                        Step 2
                     </div>
                     <div class="f-34 f-md-55 w700 lh140 mt15 caveat">
                     Add A New Video
                     </div>
                     <div class=" f-20 f-md-22 w400 lh140 mt5 mt-md10 text-capitalize">
                     With Just One Click, Our AI Model Will Research from 800 million+ YouTube Videos & Publish an Awesome Video For You. And Send You Endless Clicks In Any Niche.
                     </div>
                  </div>
                  <div class="col-md-6 col-md-pull-6 mt20 mt-md0 order-md-1 ">
                     <img src="assets/images/step2-img.png" alt="Video" class="mx-auto d-block img-fluid">
                  </div>
               </div>
            </div>
            <div class="col-12">
               <img src="assets/images/right-arrow.webp" class="img-fluid d-none d-md-block mx-auto" alt="Right Arrow">
            </div>
            <div class="col-12">
               <div class="row align-items-center ">
                  <div class="col-12 col-md-6">
                     <div class="step-shapes f-20 f-md-22 w600 black-clr">
                        Step 3
                     </div>
                     <div class="f-34 f-md-55 w700 lh140 mt15 caveat">
                        Profit
                     </div>
                     <div class="col-12 col-md-11 f-20 f-md-22 w400 lh140 mt5 mt-md10 text-capitalize">
                     That's All We Do. Every time We Promote An Affiliate Offer, or Our Product with Those Videos, We Wake Up To Money Like This
                     </div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 ">
                  <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 1px solid #fd2f15;"><source src="assets/images/profit.mp4" type="video/mp4">
                     </video>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>

   

   <!-- CTA Section Start -->
   <div class="cta-section-white">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-34 w700 lh140 text-center white-clr">
               Get TubeTraffic Ai Today And Save $384
               </div>
               <div class="f-22 f-md-24 w400 lh140 text-center white-clr mt10">
                  <span class="w700">Just Pay Once</span> Instead Of <strike class="red-clr"> Monthly</strike>
               </div>
            </div>
         </div>
         <div class="row mt20 mt-md50">
            <div class="col-md-8 text-center">
               <a href="#buynow" class="cta-link-btn"> > > > Get Instant Access To TubeTrafficAi < < <  </a>
               <img src="assets/images/compaitable-gurantee1.webp" alt="Compaitable Gurantee" class="mx-auto d-block img-fluid mt20 mt-md30">
            </div>
            <div class="col-md-4 mt20 mt-md0 px-md0">
               <div class="countdown-container">
                  <div class="f-20 f-md-24 w400 lh140 text-center red-clr mb10">
                     <span class="w700 white-clr">Hurry up!</span>  Price Increases In
                  </div>
                  <div class="countdown counter-white text-center">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-37 timerbg oswald">00&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 smmltd">Days</span> 
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-37 timerbg oswald">00&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-37 timerbg oswald">00&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-37 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> 
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- CTA Section End -->
   <div class="limited-time-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="row align-items-center flex-column flex-md-row justify-content-center">
                  <div class="col-md-10 col-12 f-20 f-md-22 w600 text-center white-clr px-md0">
                     <img src="assets/images/caution-icon.webp">Warning! Your $3,583.11 Bonuses Package Expires When Timer Hits ZERO <img src="assets/images/caution-icon.webp">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   
   <section class="white-section">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center px-md0">
               <div class="f-28 f-md-50 w700 lh140 black-clr text-center">
               Complete YouTube Business Builder with Ai
                  <img src="assets/images/red-underline-brush-big.png" class="mx-auto d-block img-fluid">
               </div>
               <div class="f-24 f-md-32 lh140 w799 text-center black-clr mt20 mt-md30">
               And Allowed Us To (Legally) Hack into YouTube’s 800 million+ Videos.
               </div>
            </div>
         </div>
        
         <div class="row mt20 mt-md50 align-items-center">
            <div class="col-12 col-md-6 order-md-2">
               <div class="col-12 col-md-10 f-28 f-md-30 lh140 w700  black-clr p0">
               Done-For-You Business Builder with Ai
               </div>
               <div class="f-20 f-md-22 w400 lh140 mt20 black-clr">
               We build super engaging video channels gem packed with 100s of HOT & Trendy videos at the push of a button, 
               <br><br>
               Then get FREE traffic automatically and convert it into Commissions & SALES, 
               <br><br>
               All from start to finish with TubeTrafficAi.
               </div>
              
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/w2.webp" alt="AI Content Generator" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </section>
   <div class="grey-section">
      <div class="container">
         <div class="row mt20 align-items-center">
            <div class="col-12 col-md-6">
               <div class="f-28 f-md-30 lh140 w700 black-clr">
               Ai YouTube Hijacker 
               </div>
               <div class="f-20 f-md-22 w400 lh140 mt20 black-clr">
               With one click we can leverage AI to hijack any video from YouTube
               <br><br>
               There are 800 million+ awesome videos are available for you to choose from.
               <br><br>
               And we can enjoy their views and redirect them to any link we want. 
               </div>
              
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/w3.webp" class="mx-auto d-block img-fluid ">
            </div>
         </div>
         
      </div>
   </div>
   <div class="white-section">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-6 order-md-2">
            <div class="f-28 f-md-30 lh140 w700  black-clr">
               Ai Self-Growing Channels
               </div>
               <div class="f-20 f-md-22 w400 lh140 mt20 black-clr">
               Without creating any videos, you will be able to use TubeTrafficAI to create self-growing channels.<br><br>
               In any niche, and without doing any extra work.
               </div>
              
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/w4.webp" alt=" Mobile Optimizer" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>
   <div class="grey-section">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-6">
               <div class="f-28 f-md-30 lh140 w700 black-clr">
               Ai Monetizer
               </div>
               <div class="f-20 f-md-22 w400 lh140 mt20 black-clr">
               Automatically monetize any channel you want with MULTIPLE income streams.<br><br>
               You will be able to integrate Amazon, eBay, AdSense, ClickBank, And more.<br><br>
               All with a single click
               </div>
               
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/w5.webp" alt=" Mobile EDITION" class="mx-auto d-block img-fluid ">
            </div>
         </div>
         </div>
   </div>
   <div class="white-section">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-6 order-md-2">
            <div class="f-28 f-md-30 lh140 w700  black-clr">
            Ai Built-In Traffic
               </div>
               <div class="f-20 f-md-22 w400 lh140 mt20 black-clr">
               You don't have to worry about traffic anymore, let AI handle that for you. 
               <br><br>
               Drive Thousands of Visitors on Any Link. <br><br>
               100% FREE
               </div>
               
               
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/w66.webp" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>
   <div class="grey-section">
      <div class="container">      
         <div class="row align-items-center">
            <div class="col-12 col-md-6">
            <div class="f-28 f-md-30 lh140 w700 black-clr">
            Ai Templates
               </div>
               <div class="f-20 f-md-22 w400 lh140 mt20 black-clr">
               Capture Leads or Make Every Visitor Click on Any Offer with Our Stunning Templates.<br><br>
               Select from dozens of premade templates with just a click.
               
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/w7.webp" alt="World-class support" class="mx-auto d-block img-fluid ">
            </div>
         </div>
        
      </div>
   </div>

   <div class="white-section">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-6 order-md-2">
            <div class="f-28 f-md-30 lh140 w700  black-clr">
            Play Videos Fast & 100% Ads-Free
               </div>
               <div class="f-20 f-md-22 w400 lh140 mt20 black-clr">
               Play Videos Smoothly on any device without Any YouTube Ads at all. <br><br>
               It goes without saying that annoyed visitors don’t buy. <br><br>
               Being Ad Free Means You Don’t Annoy Your Viewers… <br><br>
               So, ultimately boost your profits.
               </div>
               
               
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/w66.webp" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <div class="grey-section">
      <div class="container">      
         <div class="row align-items-center">
            <div class="col-12 col-md-6">
            <div class="f-28 f-md-30 lh140 w700 black-clr">
            Have 100% Control on Every Video
               </div>
               <div class="f-20 f-md-22 w400 lh140 mt20 black-clr">
               When We Say Hijack A Video… We Mean Take 100% Control…<br><br>
               We can do anything we want with these videos…<br><br>
               Play Ad-Free Videos, Add A Lead Form, Add Buy Button to Sell Products Right Inside Video or Ask Visitors to Share on social media.<br><br>
               This is beautiful.
               
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/w7.webp" alt="World-class support" class="mx-auto d-block img-fluid ">
            </div>
         </div>
        
      </div>
   </div>

   <div class="white-section">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-6 order-md-2">
            <div class="f-28 f-md-30 lh140 w700  black-clr">
            Show Subtitles in 28+ Languages.
               </div>
               <div class="f-20 f-md-22 w400 lh140 mt20 black-clr">
               Localize all your content automatically in 28 different languages…<br><br>
               TubeTrafficAi will show subtitles in different languages…<br><br>
               So, it doesn't matter what country you are trying to target, TubeTrafficAi got you covered.
               </div>
               
               
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/w66.webp" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <div class="grey-section">
      <div class="container">      
         <div class="row align-items-center">
            <div class="col-12 col-md-6">
            <div class="f-28 f-md-30 lh140 w700 black-clr">
            Ai Leads
               </div>
               <div class="f-20 f-md-22 w400 lh140 mt20 black-clr">
               Convert every viewer you hijack into a hot lead…<br><br>
               Directly from inside the video, without any extra work.<br><br>
               Capture audience attention when they’re most engaged & <br><br>
               directly send list to your favourite Autoresponder.
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/w7.webp" alt="World-class support" class="mx-auto d-block img-fluid ">
            </div>
         </div>
        
      </div>
   </div>

   <div class="white-section">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-6 order-md-2">
            <div class="f-28 f-md-30 lh140 w700  black-clr">
            Extra FREE Traffic to Your Videos:
               </div>
               <div class="f-20 f-md-22 w400 lh140 mt20 black-clr">
               TubeTrafficAi build SEO Optimized Video Channel & Pages for Search Traffic. 
               <br><br>
               Additionally, it allows visitors to share videos on major social platforms and send extra social traffic on your videos.
               <br><br>
               It’s also completely FREE.
               </div>
               
               
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/w66.webp" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <div class="grey-section">
      <div class="container">      
         <div class="row align-items-center">
            <div class="col-12 col-md-6">
            <div class="f-28 f-md-30 lh140 w700 black-clr">
            100% Mobile Responsive
               </div>
               <div class="f-20 f-md-22 w400 lh140 mt20 black-clr">
               Every page, and every video you will generate with TubeTrafficAI is mobile optimized.<br><br>
               This alone will 5x your earnings easily
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/w7.webp" alt="World-class support" class="mx-auto d-block img-fluid ">
            </div>
         </div>
        
      </div>
   </div>

   <div class="white-section">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-6 order-md-2">
            <div class="f-28 f-md-30 lh140 w700  black-clr">
            Ai Deep Analytics      
               </div>
               <div class="f-20 f-md-22 w400 lh140 mt20 black-clr">
               Always Know How Your Videos Are Doing with Deep Analytics. <br><br>
               Know your precise numbers to improve your marketing campaigns & get better results.
               </div>
               
               
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/w66.webp" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <div class="grey-section">
      <div class="container">      
         <div class="row align-items-center">
            <div class="col-12 col-md-6">
               <div class="f-28 f-md-30 lh140 w700 black-clr">
               100% Newbie Friendly Software 
               </div>
               <div class="f-20 f-md-22 w400 lh140 mt20 black-clr">
               Fully Cloud-Based Software with Zero Tech Hassles. <br><br>
               Also comes with Step-by-Step Video Training &<br><br>
               24*7 Customer Support
               </div>
               
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/w7.webp" alt="World-class support" class="mx-auto d-block img-fluid ">
            </div>
         </div>
        
      </div>
   </div>
   <div class="white-section">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-28 f-md-30 lh140 w700  black-clr">
               No Monthly Fees or Additional Charges
               </div>   
               <div class="f-20 f-md-22 w400 lh140 mt20 black-clr">
               Everything is coming at ONE TIME FEE. <br><br>
               Only until this offer is live. <br><br>
               So, grab it while it’s still available.
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/w66.webp" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <section class="grey-section">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-28 f-md-50 w700 lh140 black-clr text-center">
                  Here Is What You’re About To Access
                  <img src="assets/images/red-underline-brush-big.png" class="mx-auto d-block img-fluid">
               </div>
            </div>
         </div>
         <div class="row mt20 mt-md80 align-items-center">
            <div class="col-12 col-md-6">
               <div class="f-28 f-md-30 lh140 w700 text-center white-clr need-head">
               TubeTraffic Ai AI App
               </div>
               <div class="f-20 f-md-22 w400 lh140 mt20 black-clr">
               Exploit YouTube’s server and start redirecting thousands of views to any link you want in seconds
               </div>
               <div class="f-20 f-md-22 w700 lh140 mt10 green-clr">
               (Worth $997/mo)
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/w1.webp" class="mx-auto d-block img-fluid ">
            </div>
         </div>
         <div class="row mt20 mt-md80">
            <div class="col-12">
               <div class="border-line"></div>
            </div>
         </div>
         <div class="row mt20 mt-md80 align-items-center">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-28 f-md-30 lh140 w700 text-center white-clr need-head">
               Ai Traffic  
               </div>
               <div class="f-20 f-md-22 w400 lh140 mt20 black-clr">
               Within minutes start driving thousands of buyers clicks to any link you want
               <br><br>
               Without paying a penny in ads
               </div>
               <div class="f-20 f-md-22 w700 lh140 mt10 green-clr">
                  (Worth $997)
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/w2.webp"  class="mx-auto d-block img-fluid ">
            </div>
         </div>
         <div class="row mt20 mt-md80">
            <div class="col-12">
               <div class="border-line"></div>
            </div>
         </div>
         <div class="row mt20 mt-md80 align-items-center">
            <div class="col-12 col-md-6">
               <div class="f-28 f-md-30 lh140 w700 text-center white-clr need-head">
               Ai Videos 
               </div>
               <div class="f-20 f-md-22 w400 lh140 mt20 black-clr">
               This feature alone worth its weight in gold. It eliminates the need to create/edit/upload any videos
               </div>
               <div class="f-20 f-md-22 w700 lh140 mt10 green-clr">
                  (Worth $997)
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/w3.webp"  class="mx-auto d-block img-fluid ">
            </div>
         </div>
         <div class="row mt20 mt-md80">
            <div class="col-12">
               <div class="border-line"></div>
            </div>
         </div>
         <div class="row mt20 mt-md80 align-items-center">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-28 f-md-30 lh140 w700 text-center white-clr need-head">
               TubeTraffic Ai Mobile EDITION
               </div>
               <div class="f-20 f-md-22 w400 lh140 mt20 black-clr">
               This will allow you to also operate TubeTraffic Ai, even from your mobile phone…
               <br><br>
               Whether it’s an Android, iPhone, or tablet, it will work…
               </div>
               <div class="f-20 f-md-22 w700 lh140 mt10 green-clr">
               (Worth $497)
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/w4.webp" alt=" Mobile Optimizer" class="mx-auto d-block img-fluid ">
            </div>
         </div>
         <div class="row mt20 mt-md80">
            <div class="col-12">
               <div class="border-line"></div>
            </div>
         </div>
         <div class="row mt20 mt-md80 align-items-center">
            <div class="col-12 col-md-6">
               <div class="f-28 f-md-30 lh140 w700 text-center white-clr need-head">
               Training videos 
               </div>
               <div class="f-20 f-md-22 w400 lh140 mt20 black-clr">
               There is NOTHING missing in this training…
<br><br>
Everything you need to know is explained in IMMENSE details
               </div>
               <div class="f-20 f-md-22 w700 lh140 mt10 green-clr">
                  (Worth $997)
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/w5.webp" alt=" Mobile EDITION" class="mx-auto d-block img-fluid ">
            </div>
         </div>
         <div class="row mt20 mt-md80">
            <div class="col-12">
               <div class="border-line"></div>
            </div>
         </div>
         <div class="row mt20 mt-md80 align-items-center">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-28 f-md-30 lh140 w700 text-center white-clr need-head">
               World-class support
               </div>
               <div class="f-20 f-md-22 w400 lh140 mt20 black-clr">
               Have a question? Just reach out to us and our team will do their best to fix your problem in no time
               </div>
               <div class="f-20 f-md-22 w700 lh140 mt10 green-clr">
               (Worth A LOT)
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/w66.webp" alt="Training videos" class="mx-auto d-block img-fluid ">
            </div>
         </div>
         <div class="row mt20 mt-md80">
            <div class="col-12">
               <div class="border-line"></div>
            </div>
         </div>
         <div class="row mt20 mt-md80 align-items-center">
            <div class="col-12 col-md-6">
               <div class="f-28 f-md-30 lh140 w700 text-center white-clr need-head">
               World-class support
               </div>
               <div class="f-20 f-md-22 w400 lh140 mt20 black-clr">
               Have a question? Just reach out to us and our team will do their best to fix your problem in no time&nbsp;
               </div>
               <div class="f-20 f-md-22 w700 lh140 mt10 green-clr">
               (Worth A LOT)
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/w7.webp" alt="World-class support" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </section>
   <!-- CTA Section Start -->
   <div class="cta-section-white">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-34 w700 lh140 text-center white-clr">
               Get TubeTraffic Ai Today And Save $384
               </div>
               <div class="f-22 f-md-24 w400 lh140 text-center white-clr mt10">
                  <span class="w700">Just Pay Once</span> Instead Of <strike class="red-clr"> Monthly</strike>
               </div>
            </div>
         </div>
         <div class="row mt20 mt-md50">
            <div class="col-md-8 text-center">
               <a href="#buynow" class="cta-link-btn"> > > > Get Instant Access To TubeTrafficAi < < <  </a>
               <img src="assets/images/compaitable-gurantee1.webp" alt="Compaitable Gurantee" class="mx-auto d-block img-fluid mt20 mt-md30">
            </div>
            <div class="col-md-4 mt20 mt-md0 px-md0">
               <div class="countdown-container">
                  <div class="f-20 f-md-24 w400 lh140 text-center red-clr mb10">
                     <span class="w700 white-clr">Hurry up!</span>  Price Increases In
                  </div>
                  <div class="countdown counter-white text-center">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-37 timerbg oswald">00&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 smmltd">Days</span> 
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-37 timerbg oswald">00&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-37 timerbg oswald">00&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-37 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> 
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
    <!-- CTA Section End -->
    <div class="limited-time-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="row align-items-center flex-column flex-md-row justify-content-center">
                  <div class="col-md-10 col-12 f-20 f-md-22 w600 text-center white-clr px-md0">
                     <img src="assets/images/caution-icon.webp">Warning! Your $3,583.11 Bonuses Package Expires When Timer Hits ZERO <img src="assets/images/caution-icon.webp">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

   <div class="butif-section">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-26 f-md-50 w700 lh140 black-clr text-center text-md-start">
               But It Gets Better…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Look, it’s simple…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               We don’t want anything to stand in the way of you profiting from TubeTraffic Ai
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
              
We went above and beyond to make this app the only thing you will ever need…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               But if that wasn’t enough…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               We also CUSTOM MADE bonuses for you that will elevate your experience with TubeTraffic Ai
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               These are not just any bonuses…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               It was designed to help you fast-track your results with TubeTraffic Ai
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Let me show you
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/butit-img.png" alt="Profitable" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>

   
      

	<!-- Don't Section End -->
   <div class="grey-section">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-head f-28 f-md-48 w600 lh140 text-center white-clr">
                  But That's Not All
               </div>
               <div class="f-28 f-md-50 lh140 w500 text-center black-clr mt20 mt-md30">
               Claim Your <span class="w700">$3,583.11</span> Now
               </div>
               <div class="f-20 f-md-22 w500 lh140 black-clr mt20">
                  In addition, we have several bonuses for those who want to take action <br class="d-none d-md-block">
                  today and start profiting from this opportunity.
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md80">
            <div class="col-12 col-md-6">
               <div class="">
                  <img src="assets/images/bonus1.webp" alt="Bonus 1" class="img-fluid d-block ">
                  <div class="mt20 f-20 f-md-24 w700 lh140 black-clr">
                  Exclusive F’ree Training 
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 black-clr mt15">
                  Get ahead in the creative game with TubeTraffic AI's F’REE Training! Elevate your skills and maximize your results with this limited-time opportunity. Gain expert insights, tips, and strategies to supercharge your Tubetraffic AI experience. Don't miss out on this valuable resource to boost your success. 
                  </div>
                  <!-- <div class="mt20 f-20 f-md-24 w700 lh140 black-clr mt15">
                  Value - $227
                  </div> -->
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/bonus.webp" alt="Bonus Box" class="mx-auto d-block img-fluid ">
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md80">
            <div class="col-12 col-md-6 order-md-2">
               <div class="">
                  <img src="assets/images/bonus2.webp" alt="Bonus 2" class="img-fluid d-block ">
                  <div class="mt20 f-20 f-md-24 w700 lh140 black-clr">
                  How To Rank Your YouTube Videos ( Value $667 )
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 black-clr mt15">
                  Discover the ultimate guide to elevate your YouTube video rankings with TubeTraffic AI. Uncover cutting-edge strategies, tips, and tricks to optimize your content for maximum visibility. Harness the power of AI-driven insights to skyrocket your video's reach and engagement, boosting your online presence and success. Don't miss out on this bonus for YouTube creators!
                  </div>
                  <!-- <div class="mt20 f-20 f-md-24 w700 lh140 black-clr mt15">
                  Value - $667
                  </div> -->
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/bonus.webp" alt="Bonus Box" class="mx-auto d-block img-fluid ">
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md80">
            <div class="col-12 col-md-6">
               <div class="">
                  <img src="assets/images/bonus3.webp" alt="Bonus 3" class="img-fluid d-block ">
                  <div class="mt20 f-20 f-md-24 w700 lh140 black-clr">
                  25 Youtube Outro Clips ( Value $467 )
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 black-clr mt15">
                  Unlock the power of Tubetraffic AI with - 25 YouTube Outro Clips! Elevate your video outros with professionally designed and customizable templates. Enhance viewer engagement, promote your brand, and drive more subscribers and traffic to your channel. Elevate your content and leave a lasting impression with these dynamic outros.
                  </div>
                  <!-- <div class="mt20 f-20 f-md-24 w700 lh140 black-clr mt15">
                  Value - $567
                  </div> -->
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/bonus.webp" alt="Bonus Box" class="mx-auto d-block img-fluid ">
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md80">
            <div class="col-12 col-md-6 order-md-2">
               <div class="">
                  <img src="assets/images/bonus4.webp" alt="Bonus 4" class="img-fluid d-block ">
                  <div class="mt20 f-20 f-md-24 w700 lh140 black-clr">
                  Youtube Channel Income Accelerator ( Value $567 ) 
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 black-clr mt15">
                  YouTube Channel Income Accelerator by TubeTraffic AI is your ultimate HD video Training guide to turbocharge your YouTube earnings. Unlock insider tips, strategies, and AI-powered insights to boost views, engagement, and revenue. Elevate your channel to new heights and maximize your income potential with this invaluable resource."
                  </div>
                  <!-- <div class="mt20 f-20 f-md-24 w700 lh140 black-clr mt15">
                  Value - $297
                  </div> -->
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/bonus.webp" alt="Bonus Box" class="mx-auto d-block img-fluid ">
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md80">
            <div class="col-12 col-md-6">
               <div class="">
                  <img src="assets/images/bonus5.webp" alt="Bonus 5" class="img-fluid d-block ">
                  <div class="mt20 f-20 f-md-24 w700 lh140 black-clr">
                  20 Ways To Get More People Watching ( Value $329) 
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 black-clr mt15">
                  Unlock the secrets of Tubetraffic AI with 20 Ways To Get More People Watching.' Discover expert strategies to boost your online video visibility, engagement, and audience growth. Elevate your content game and maximize your reach in just 20 actionable steps
                  </div>
                  <!-- <div class="mt20 f-20 f-md-24 w700 lh140 black-clr mt15">
                  Value - $997
                  </div> -->
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/bonus.webp" alt="Bonus Box" class="mx-auto d-block img-fluid ">
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md80">
            <div class="col-12 col-md-6 order-md-2">
               <div class="">
                  <img src="assets/images/bonus6.webp" alt="Bonus 6" class="img-fluid d-block ">
                  <div class="mt20 f-20 f-md-24 w700 lh140 black-clr">
                  YouTube Ads Excellence ( Value $567 )
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 black-clr mt15">
                  Unlock the full potential of YouTube advertising - YouTube Ads Excellence for TubeTraffic AI. This comprehensive training program provides expert insights and strategies to optimize your ad campaigns, maximize ROI, and boost your online presence. Elevate your YouTube advertising game with this invaluable resource."
                  </div>
                  <!-- <div class="mt20 f-20 f-md-24 w700 lh140 black-clr mt15">
                  Value - $297
                  </div> -->
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/bonus.webp" alt="Bonus Box" class="mx-auto d-block img-fluid ">
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md80">
            <div class="col-12 col-md-6">
               <div class="">
                  <img src="assets/images/bonus7.webp" alt="Bonus 7" class="img-fluid d-block ">
                  <div class="mt20 f-20 f-md-24 w700 lh140 black-clr">
                  Master Youtube Influence ( Value $467) 
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 black-clr mt15">
                  Unlock the ultimate YouTube mastery with Tubetraffic AI's Bonus-Master YouTube Influence. This exclusive feature empowers creators to harness the full potential of their content, enhance engagement, and skyrocket their channel's visibility. Elevate your YouTube presence and achieve unprecedented success with this game-changing tool.
                  </div>
                  <!-- <div class="mt20 f-20 f-md-24 w700 lh140 black-clr mt15">
                  Value - $997
                  </div> -->
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/bonus.webp" alt="Bonus Box" class="mx-auto d-block img-fluid ">
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md80">
            <div class="col-12 col-md-6 order-md-2">
               <div class="">
                  <img src="assets/images/bonus8.webp" alt="Bonus 6" class="img-fluid d-block ">
                  <div class="mt20 f-20 f-md-23 w700 lh140 black-clr">
                  UNLIMITED Commercial License ( Value - $997 )
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 black-clr mt15">
                  You have full rights to use this software. You can use it for anyone whether for individuals or for companies. Generate massive free traffic, Sales & Leads to yourself and for others as well.
                  </div>
                  <!-- <div class="mt20 f-20 f-md-24 w700 lh140 black-clr mt15">
                  Value - $297
                  </div> -->
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/bonus.webp" alt="Bonus Box" class="mx-auto d-block img-fluid ">
            </div>
         </div>
         <div class="row">
            <div class="col-12 f-24 f-md-32 lh140 w600 text-center black-clr mt20 mt-md50">
            BTW - there is more unannounced bonuses waiting for you inside…


            </div>
         </div>
      </div>
   </div>

   <div class="white-section">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-50 w700 lh140 text-center black-clr">
               Wanna Know <span class="orange-clr"> How Much It Costs? </span>
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md80">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr">
               Look, I'm gonna be honest with you…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               We can charge anything we want for TubeTrafficAi
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               Even $997 a month…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               And it will be a no-brainer…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               After all, our beta testers make so much more than that…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               And on top of that, there is no work for you to do.
               </div>
               <div class="f-20 f-md-22 w600 lh140 text-center text-md-start black-clr mt20">
               TubeTrafficAi does it all…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               But I make enough money from using TubeTrafficAi
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               So I don't need to charge you that
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               I use it on a daily basis…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               So I don't see why not I shouldn't give you access to it…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               So, for a limited time only…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               I'm willing to give you full access to TubeTrafficAi
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               For a fraction of the price
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               Less than the price of a cheap dinner
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               Which is enough for me to cover the cost of the servers running TubeTrafficAi
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/cost.png" alt="Costs" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <div class="quick-section">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-6 ">
               <div class="f-28 f-md-50 w700 lh140 text-center text-md-start white-clr">
               You Have To Be Quick Tho…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start white-clr mt20 mt-md30">
               The last thing on earth I want is to get TubeTrafficAi saturated…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start white-clr mt20">
               So, sadly I'll have to limit the number of licenses I can give…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start white-clr mt20">
               After that, I'm raising the price to <span class="w600 orange-clr">$97 monthly.</span> 
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start white-clr mt20">
               And trust me, that day will come very…VERY SOON.
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start white-clr mt20">
               So you better act quickly and grab your copy while you can.
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/quick-img.png" alt="Money Scale" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div> 

   <!-- CTA Section Start -->
   <div class="cta-section-white">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-34 w700 lh140 text-center white-clr">
               Get TubeTraffic Ai Today And Save $384
               </div>
               <div class="f-22 f-md-24 w400 lh140 text-center white-clr mt10">
                  <span class="w700">Just Pay Once</span> Instead Of <strike class="red-clr"> Monthly</strike>
               </div>
            </div>
         </div>
         <div class="row mt20 mt-md50">
            <div class="col-md-8 text-center">
               <a href="#buynow" class="cta-link-btn"> > > > Get Instant Access To TubeTrafficAi < < <  </a>
               <img src="assets/images/compaitable-gurantee1.webp" alt="Compaitable Gurantee" class="mx-auto d-block img-fluid mt20 mt-md30">
            </div>
            <div class="col-md-4 mt20 mt-md0 px-md0">
               <div class="countdown-container">
                  <div class="f-20 f-md-24 w400 lh140 text-center red-clr mb10">
                     <span class="w700 white-clr">Hurry up!</span>  Price Increases In
                  </div>
                  <div class="countdown counter-white text-center">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-37 timerbg oswald">00&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 smmltd">Days</span> 
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-37 timerbg oswald">00&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-37 timerbg oswald">00&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-37 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> 
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
    <!-- CTA Section End -->
    <div class="limited-time-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="row align-items-center flex-column flex-md-row justify-content-center">
                  <div class="col-md-10 col-12 f-20 f-md-22 w600 text-center white-clr px-md0">
                     <img src="assets/images/caution-icon.webp">Warning! Your $3,583.11 Bonuses Package Expires When Timer Hits ZERO <img src="assets/images/caution-icon.webp">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <div class="butif-section">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-26 f-md-50 w700 lh140 black-clr text-center text-md-start">
               Can't Decide?
               </div>
               <div class="f-18 f-md-22 w600 lh140 black-clr text-center text-md-start mt20">
               Listen…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               It's really simple…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               If you're sick of all the BS that is going on the market right now…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               If you're worried about inflation and the prices that are going up like there is no tomorrow
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               TubeTrafficAi IS for you <span class="w600"> PERIOD </span>
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               It was designed to be the life-changing system that you've been looking for…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Right now, you have the option to change your life
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               With just a few clicks
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               We created this for people exactly like you. To help them finally break through.
               </div>
               <div class="f-18 f-md-22 w600 lh140 red-clr text-center text-md-start mt20">
               Worried what will happen if it didn't work for you?
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/cantdecide-img.png" alt="Profitable" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <div class="grey-section">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-20 f-md-38 w400 lh140 black-clr text-center">
               We Will Pay You To Fail With TubeTrafficAi
               </div>
               <div class="f-28 f-md-45 w700 lh140 black-clr text-center">
               Our 30 Days Iron Clad  Money Back Guarantee
               </div>
            </div>
         </div>
         <div class="row align-items-center mt30 mt-md50">
            <div class="col-12 col-md-6">
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start">
               We trust our app blindly…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               We know it works, after all we been using it for a year…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               And not just us…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               But hey, I know you probably don't know me… and you may be hesitant…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               And i understand that. A bit of skepticism is always healthy…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               But I can help…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Here is the deal, get access to TubeTrafficAi now…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Use it, and enjoy its features to the fullest…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               And if for any reason you don't think TubeTrafficAi is worth its weight in gold…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Just send us a message to our 24/7 customer support…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               And we will refund every single penny back to you…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               No questions asked… Not just that…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               We will send you a bundle of premium software as a gift for wasting your time.
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
              Worst case scenario, you get TubeTrafficAi and don't make any money
               </div>
               <div class="f-18 f-md-22 w600 lh140 red-clr text-center text-md-start mt20">
               You will still get extra bundle of premium software for trying it out.
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/money-back-guarantee.webp" alt="imagine-img" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div> 

   <!-- CTA Section Start -->
   <div class="cta-section-white">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-34 w700 lh140 text-center white-clr">
               Get TubeTraffic Ai Today And Save $384
               </div>
               <div class="f-22 f-md-24 w400 lh140 text-center white-clr mt10">
                  <span class="w700">Just Pay Once</span> Instead Of <strike class="red-clr"> Monthly</strike>
               </div>
            </div>
         </div>
         <div class="row mt20 mt-md50">
            <div class="col-md-8 text-center">
               <a href="#buynow" class="cta-link-btn"> > > > Get Instant Access To TubeTrafficAi < < <  </a>
               <img src="assets/images/compaitable-gurantee1.webp" alt="Compaitable Gurantee" class="mx-auto d-block img-fluid mt20 mt-md30">
            </div>
            <div class="col-md-4 mt20 mt-md0 px-md0">
               <div class="countdown-container">
                  <div class="f-20 f-md-24 w400 lh140 text-center red-clr mb10">
                     <span class="w700 white-clr">Hurry up!</span>  Price Increases In
                  </div>
                  <div class="countdown counter-white text-center">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-37 timerbg oswald">00&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 smmltd">Days</span> 
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-37 timerbg oswald">00&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-37 timerbg oswald">00&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-37 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> 
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
    <!-- CTA Section End -->
    <div class="limited-time-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="row align-items-center flex-column flex-md-row justify-content-center">
                  <div class="col-md-10 col-12 f-20 f-md-22 w600 text-center white-clr px-md0">
                     <img src="assets/images/caution-icon.webp">Warning! Your $3,583.11 Bonuses Package Expires When Timer Hits ZERO <img src="assets/images/caution-icon.webp">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <div class="white-section" id="buynow">
      <div class="container">
         <div class="row">
            <div class="col-12 col-md-9 mx-auto text-center">
               <div class="table-wrap">
                  <div class="table-head">
                     <div class="f-28 f-md-45 lh140 white-clr w700">
                        Today with TubeTrafficAi, You're Getting
                     </div>
                  </div>
                  <div class=" ">
                     <ul class="table-list pl0 f-18 f-md-20 w400 text-center lh140 black-clr mt20 mt-md40">
                        <li>A True Ai App That Leverages Super Trending IBM's Watson & ChatGPT4 Technology - <span class="w600">That's PRICELESS</span> </li>
                        <li>IBM's Watson & ChatGPT 4 Powered Real Human Emotion Based Voice Creator - <span class="w600">Worth $997</span> </li>
                        <li>Let Ai Create 100s of AudioBooks & Podcasts for Yourself & Clients - <span class="w600">Worth $997</span> </li>
                        <li>Even Ai Generates High Quality Unique Content for You/Clients - <span class="w600">That's Worth $997</span> </li>
                        <li>TubeTrafficAi Built-In Traffic: Instant High Converting Traffic For 100% Free - <span class="w600"> Worth $997 </span></li>
                        <li>300+ Real Voices with Human Emotions to Choose - <span class="w600">Worth $497</span> </li>
                        <li>100+ Languages to Choose From - <span class="w600">Worth $297</span> </li>
                        <li>Inbuilt DFY 1 million Articles with Full PLR License - <span class="w600">Worth $267</span> </li>
                        <li>Step-By-Step Training Videos - <span class="w600">Worth $297</span> </li>
                        <li>Round-The-Clock World-Class Support <span class="w600">(Priceless – It's Worth A LOT)</span> </li>
                        <li>Create Unlimited VSL's, Sales Copies, Emails, Ads Copy Etc.</li>
                        <li>Pay Once & Get Profit Forever Without Any Restrictions - <span class="w600">Beyond A Value</span></li>
                        <li>100% Newbie Friendly - <span class="w600">Beyond A Value</span></li>
                        <li>Step-By-Step Training Videos - <span class="w600">Worth $297</span></li>
                        <li>Round-The-Clock World-Class Support <span class="w600">(Priceless – It's Worth A LOT)</span></li>
                        <li>Commercial License to Sell Unlimited Content, Voice Overs, AudioBooks & Earn Like the Big Boys - <span class="w600">Priceless</span></li>
                        <li class="headline my15">BONUSES WORTH $3,583.11 FREE!!! </li>
                        <li><span class="w700 red-clr">Bonus 1 - </span>Exclusive F’ree Training</li>
                        <li><span class="w700 red-clr">Bonus 2 - </span>How To Rank Your YouTube Videos</li>
                        <li><span class="w700 red-clr">Bonus 3 - </span>25 Youtube Outro Clips</li>
                        <li><span class="w700 red-clr">Bonus 4 - </span>Youtube Channel Income Accelerator</li>
                        <li><span class="w700 red-clr">Bonus 5 - </span>20 Ways To Get More People Watching</li>
                        <li><span class="w700 red-clr">Bonus 6 - </span>YouTube Ads Excellence</li>
                        <li><span class="w700 red-clr">Bonus 7 - </span>Master Youtube Influence</li>
                        <li><span class="w700 red-clr">Bonus 8 - </span>UNLIMITED Commercial License</li>
                     </ul>
                  </div>
                  <div class="table-btn ">
                     <div class="f-22 f-md-28 w400 lh140 black-clr">
                        Total Value of Everything
                     </div>
                     <div class="f-26 f-md-36 w700 lh140 black-clr mt12">
                        YOU GET TODAY:
                     </div>
                     <div class="f-28 f-md-70 w700 red-clr mt12 l140">
                        $5,346.00
                     </div>
                     <div class="f-22 f-md-28 w400 lh140 black-clr mt20">
                        For Limited Time Only, Grab It For:
                     </div>
                     <div class="f-21 f-md-40 w700 lh140 red-clr mt20">
                       <del>$97 Monthly</del> 
                     </div>
                     <div class="f-24 f-md-34 w700 lh140 black-clr mt20">
                        Today, <span class="blue-clr">Just $17 One-Time</span> 
                     </div>
                     
                     <div class="col-12 mx-auto text-center">
                     <a href="https://warriorplus.com/o2/buy/hz2587/wl4q8v/g71kw3" id="buyButton" class="cta-link-btn mt20"> Get Instant Access To TubeTrafficAi</a>
                           </div>
                     <!-- <a href="https://warriorplus.com/o2/buy/qyptv4/xw7477/tbq1nm"><img src="https://warriorplus.com/o2/btn/fn200011000/qyptv4/xw7477/360705" class="d-block img-fluid mx-auto mt20 mt-md30"></a>  -->
                  </div>
                  <div class="table-border-content">
                     <div class="tb-check d-flex align-items-center f-18">
                        <label class="checkbox inline">
                           <img src="https://assets.clickfunnels.com/templates/listhacking-sales/images/arrow-flash-small.gif" alt="">
                           <input type="checkbox" id="check" onclick="myFunction()">
                        </label>
                        <label class="checkbox inline">
                           <h5>Yes, Add TubeTrafficAi + Unlock Our Secret $2k/Day AI Traffic System</h5>
                        </label>
                     </div>
                     <div class="p20">
                        <p class="f-18 text-center text-md-start"><b class="red-clr underline">LIMITED TIME OFFER :</b> Activate Done-For-You Ai Traffic Source To Your Order To Make Additional $2k/Day with FREE Traffic. Get $1K Paying Clients on Autopilot + Put Your Account On A Faster Server for More Faster Content & VoiceOver Creations, Client's Delivery, & Profits. <br><span class="w700">(97% Of Customers Pick This Up And Get Immediate Results With It)</span></p>
                        <p class="f-18 text-center text-md-start"> <span class="green-clr w600">  Just $9.97 One Time Only</span> </p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>

   <div class="grey-section">
      <div class="container">
         <div class="row align-items-center mt30 mt-md50">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-28 f-md-50 w700 lh140 black-clr text-center text-md-start">
               Remember…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               You don't have a lot of time. We will remove this huge discount once we hit our limit.
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               After that, you will have to <span class="w600"> pay $97/mo for it.</span>
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               While it will still be worth it.
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Why pay more, when you can just pay a small one-time fee today and get access to everything?
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/remember-img.png" alt="imagine-img" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <div class="white-section">
      <div class="container">
         <div class="row align-items-center mt30 mt-md50">
            <div class="col-12 col-md-6">
               <div class="f-28 f-md-45 w700 lh140 black-clr text-center text-md-start">
               So, Are you ready yet?
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               You reading this far into the page means one thing.
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               You're interested.
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               The good news is, you're just minutes away from your breakthrough
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Just imagine waking up every day to thousands of dollars in your account Instead of ZERO.
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               The bad news is, it will sell out FAST
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               So you need to act now.
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Ready to join us?
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/are-you-ready-img.png" alt="imagine-img" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <!-- CTA Section Start -->
   <div class="cta-section-white">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-34 w700 lh140 text-center white-clr">
               Get TubeTraffic Ai Today And Save $384
               </div>
               <div class="f-22 f-md-24 w400 lh140 text-center white-clr mt10">
                  <span class="w700">Just Pay Once</span> Instead Of <strike class="red-clr"> Monthly</strike>
               </div>
            </div>
         </div>
         <div class="row mt20 mt-md50">
            <div class="col-md-4 offset-md-2 text-center">
            <a href="https://warriorplus.com/o2/buy/hz2587/wl4q8v/trx0mv"><img src="https://warriorplus.com/o2/btn/fn200011000/hz2587/wl4q8v/364898" class="img-fluid mx-auto d-block"></a> 
            </div>
            <div class="col-md-4 mt20 mt-md0 px-md0">
               <div class="countdown-container">
                  <div class="f-20 f-md-24 w400 lh140 text-center red-clr mb10">
                     <span class="w700 white-clr">Hurry up!</span>  Price Increases In
                  </div>
                  <div class="countdown counter-white text-center">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-37 timerbg oswald">00&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 smmltd">Days</span> 
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-37 timerbg oswald">00&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-37 timerbg oswald">00&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-37 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> 
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
    <!-- CTA Section End -->
    <div class="limited-time-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="row align-items-center flex-column flex-md-row justify-content-center">
                  <div class="col-md-10 col-12 f-20 f-md-22 w600 text-center white-clr px-md0">
                     <img src="assets/images/caution-icon.webp">Warning! Your $3,583.11 Bonuses Package Expires When Timer Hits ZERO <img src="assets/images/caution-icon.webp">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <div class="inside-section">
      <div class="container">
         <div class="row">
            <div class="col-12 col-md-12">
               <div class="f-28 f-md-45 lh140 w700 text-center white-clr">
                  We'll see you inside,
               </div>
              
            </div>
         </div>
         <div class="row mt20 mt-md70">
            <div class="col-12 col-md-8 mx-auto">
               <div class="row justify-content-between">
                  <div class="col-md-5">
                     <div class="inside-box">
                        <img src="assets/images/pranshu-gupta.webp" alt="Pranshu Gupta" class="mx-auto d-block mx-auto ">
                        <div class="f-22 f-md-28 lh140 w600 text-center black-clr mt20">
                           Pranshu Gupta
                        </div>
                     </div>
                  </div>
                  <div class="col-md-5 mt20 mt-md0">
                     <div class="inside-box">
                        <img src="assets/images/bizomart-team.webp" alt="Bizomart" class="mx-auto d-block mx-auto ">
                        <div class="f-22 f-md-28 lh140 w600 text-center black-clr mt20">
                           Bizomart
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="row mt20 mt-md70">
            <div class="col-12">
               <div class="f-20 f-md-22 lh140 w400 white-clr text-center text-md-start">
                  <span class="w600">PS:</span> If you act now, you will instantly receive [bonuses] worth over  <span class="w600">$7,458.34</span> This bonus is designed specifically to help you get 10x the results, in half the time required
               </div>
               <div class="f-20 f-md-22 lh140 w400 white-clr text-center text-md-start mt20">
                 <span class="w600">PPS:</span>There is nothing else required for you to start earning with TubeTrafficAi No hidden fees, no monthly costs, nothing.
               </div>
               <div class="f-20 f-md-22 lh140 w400 white-clr text-center text-md-start mt20">
                 <span class="w600">PPPS:</span> Remember, you're protected by our money-back guarantee If you fail, Not only will we refund your money.
               </div>
               <div class="f-20 f-md-22 lh140 w400 white-clr text-center text-md-start mt20">
               We will send you <span class="red-clr">$300 out of our own pockets just for wasting your time</span> 
               </div>
               <div class="f-20 f-md-22 lh140 w400 white-clr text-center text-md-start mt20">
                 <span class="w600">PPPPS:</span>   If you put off the decision till later, you will end up paying $997/mo Instead of just a one-time flat fee.
               </div>
               <div class="f-20 f-md-22 lh140 w400 white-clr text-center text-md-start mt20">
               And why pay more, when you can pay less?
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="faq-section ">
      <div class="container ">
         <div class="row ">
            <div class="col-12 f-28 f-md-45 lh140 w600 text-center black-clr ">
               Frequently Asked <span class="orange-clr ">Questions</span>
            </div>
            <div class="col-12 mt20 mt-md30 ">
               <div class="row ">
                  <div class="col-md-9 mx-auto col-12 ">
                     <div class="faq-list ">
                        <div class="f-md-22 f-20 w600 black-clr lh140 ">
                           Do I need any experience to get started? 
                        </div>
                        <div class="f-18 w400 lh140 mt10 text-start ">
                           None, all you need is just an internet connection. And you're good to go
                        </div>
                     </div>
                     <div class="faq-list mt20">
                        <div class="f-md-22 f-20 w600 black-clr lh140 ">
                           Is there any monthly cost?
                        </div>
                        <div class="f-18 w400 lh140 mt10 text-start ">
                           Depends, If you act now, NONE. 
                           But if you wait, you might end up paying $97/month
                           <br><br>
                           It's up to you. 
                        </div>
                     </div>
                     <div class="faq-list mt20">
                        <div class="f-md-22 f-20 w600 black-clr lh140 ">
                           How long does it take to make money?
                        </div>
                        <div class="f-18 w400 lh140 mt10 text-start ">
                           Our average member made their first sale the same day they got access to TubeTrafficAi.
                        </div>
                     </div>
                     <div class="faq-list mt20">
                        <div class="f-md-22 f-20 w600 black-clr lh140 ">
                           Do I need to purchase anything else for it to work?
                        </div>
                        <div class="f-18 w400 lh140 mt10 text-start ">
                           Nop, TubeTrafficAi is the complete thing. 
                           <br><br>
                           You get everything you need to make it work. Nothing is left behind. 
                        </div>
                     </div>
                     <div class="faq-list mt20">
                        <div class="f-md-22 f-20 w600 black-clr lh140 ">
                           What if I failed?
                        </div>
                        <div class="f-18 w400 lh140 mt10 text-start ">
                           While that is unlikely, we removed all the risk for you.
                           <br><br>
                           If you tried TubeTrafficAi and failed, we will refund you every cent you paid
                           <br><br>
                           And send you $300 on top of that just to apologize for wasting your time.
                        </div>
                     </div>
                     <div class="faq-list mt20">
                        <div class="f-md-22 f-20 w600 black-clr lh140 ">
                           How can I get started?
                        </div>
                        <div class="f-18 w400 lh140 mt10 text-start ">
                           Awesome, I like your excitement, All you have to do is click any of the buy buttons on the page, and secure your copy of TubeTrafficAi at a one-time fee
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md50 ">
               <div class="f-22 f-md-28 w600 black-clr px0 text-center lh140 ">If you have any query, simply reach to us at <a href="mailto:support@bizomart.com " class="kapblue ">support@bizomart.com</a>
               </div>
            </div>
         </div>
      </div>
   </div>

   <!-- CTA Section Start -->
   <div class="cta-section-white1">
      <div class="container">
         <div class="row">
         <div class="col-12 text-center">
                  <div class="but-design-border">
                     <div class="f-md-45 f-28 lh140 w700 balck-clr2 button-shape">
                        FINAL CALL
                     </div>
                  </div>
               </div>
            <div class="col-12">
               <div class="f-28 f-md-34 w700 lh140 text-center black-clr">
               Get TubeTraffic Ai Today And Save $384
               </div>
               <div class="f-22 f-md-24 w400 lh140 text-center black-clr mt10">
                  <span class="w700">Just Pay Once</span> Instead Of <strike class="red-clr"> Monthly</strike>
               </div>
            </div>
         </div>
         <div class="row mt20 mt-md50">
            <div class="col-md-4 offset-md-2 text-center">
            <a href="https://warriorplus.com/o2/buy/hz2587/wl4q8v/g71kw3"><img src="https://warriorplus.com/o2/btn/fn200011000/hz2587/wl4q8v/364104" class="img-fluid mx-auto d-block"></a>
            </div>
            <div class="col-md-4 mt20 mt-md0 px-md0">
               <div class="countdown-container1">
                  <div class="f-20 f-md-24 w400 lh140 text-center red-clr mb10">
                     <span class="w700 black-clr">Hurry up!</span>  Price Increases In
                  </div>
                  <div class="countdown counter-white text-center" style="display:flex;">
                  <div class="timer-label1 text-center black-clr">
                        <span class="f-31 f-md-37  oswald black-clr">&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 smmltd"></span> 
                     </div>
                     <div class="timer-label1 text-center black-clr">
                        <span class="f-31 f-md-37  oswald black-clr">00&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 smmltd">Days</span> 
                     </div>
                     <div class="timer-label1 text-center">
                        <span class="f-31 f-md-37 oswald black-clr">00&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                     </div>
                     <div class="timer-label1 text-center timer-mrgn">
                        <span class="f-31 f-md-37 oswald black-clr">00&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label1 text-center ">
                        <span class="f-31 f-md-37 oswald black-clr">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> 
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
    <!-- CTA Section End -->
    <div class="limited-time-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="row align-items-center flex-column flex-md-row justify-content-center">
                  <div class="col-md-10 col-12 f-20 f-md-22 w600 text-center white-clr px-md0">
                     <img src="assets/images/caution-icon.webp">Warning! Your $3,583.11 Bonuses Package Expires When Timer Hits ZERO <img src="assets/images/caution-icon.webp">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>


   

      
      <div class="footer-section">
         <div class="container ">
            <div class="row">
               <div class="col-12 text-center">
               <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 800 131.43" style="max-height:75px;">
                        <defs>
                            <style>
                            .cls-1 {
                                fill: #fff;
                            }
        
                            .cls-2 {
                                fill: #fd2f15;
                            }
        
                            .cls-3 {
                                fill: #fbbf33;
                            }
                            </style>
                        </defs>
                        <g id="Layer_1-2" data-name="Layer 1">
                            <g>
                            <g>
                                <path class="cls-3" d="m105.61,78.06l40.05,23.12c6.01,3.47,13.51-.87,13.51-7.8v-46.25c0-6.93-7.51-11.27-13.51-7.8l-40.05,23.12c-6.01,3.47-6.01,12.14,0,15.6Z"></path>
                                <g>
                                <rect class="cls-3" x="26.92" y="58.86" width="16.03" height="46.81"></rect>
                                <path class="cls-2" d="m125.22,26.34h-7.16v46.06c0,32.6-26.43,59.03-59.03,59.03H0v-65.94C0,32.89,26.43,6.45,59.03,6.45h38.52l-12.38,19.88h-26.14c-21.59,0-39.16,17.57-39.16,39.15v46.07h39.16c21.59,0,39.16-17.57,39.16-39.16V26.34h-5.79L108.81,0l16.41,26.34Z"></path>
                                <path class="cls-3" d="m67.04,46.21h-16.02v59.46h8.01c2.76,0,5.45-.35,8.01-.99v-58.47Z"></path>
                                <path class="cls-3" d="m90.77,33.56h-16.03v68.14c7.57-4.07,13.39-10.99,16.03-19.31v-48.83Z"></path>
                                </g>
                            </g>
                            <g>
                                <g>
                                <path class="cls-1" d="m195.69,38.68v-5.58h49.35v5.58h-21.57v62.48h-6.21v-62.48h-21.57Z"></path>
                                <path class="cls-1" d="m283.29,81.65v-31.54h5.92v51.05h-5.92v-8.47h-.47c-1.2,2.61-3.09,4.79-5.68,6.53-2.59,1.74-5.76,2.61-9.5,2.61-3.26,0-6.15-.71-8.67-2.14-2.53-1.43-4.51-3.53-5.95-6.3-1.44-2.77-2.16-6.17-2.16-10.2v-33.07h5.92v32.7c0,3.99,1.17,7.2,3.52,9.62,2.35,2.43,5.36,3.64,9.04,3.64,2.3,0,4.52-.55,6.65-1.66,2.13-1.11,3.88-2.73,5.25-4.87,1.37-2.14,2.06-4.77,2.06-7.89Z"></path>
                                <path class="cls-1" d="m306.05,101.16V33.09h5.95v26.75h.53c.66-1.53,1.62-3.1,2.87-4.72,1.25-1.62,2.9-2.97,4.95-4.07,2.05-1.1,4.64-1.65,7.76-1.65,4.19,0,7.86,1.1,11.03,3.31,3.17,2.21,5.64,5.28,7.41,9.24,1.77,3.95,2.66,8.56,2.66,13.81s-.88,9.89-2.64,13.86c-1.76,3.97-4.22,7.06-7.38,9.27-3.16,2.22-6.82,3.32-10.98,3.32-3.1,0-5.69-.55-7.76-1.66-2.07-1.11-3.74-2.48-5-4.1-1.26-1.63-2.24-3.22-2.92-4.77h-.73v9.47h-5.75Zm5.85-25.49c0,4.12.61,7.77,1.84,10.95,1.23,3.18,3,5.67,5.32,7.48,2.31,1.81,5.12,2.71,8.42,2.71s6.26-.94,8.61-2.81c2.35-1.87,4.13-4.4,5.35-7.59,1.22-3.19,1.83-6.77,1.83-10.73s-.6-7.46-1.81-10.6c-1.21-3.15-2.99-5.64-5.33-7.49-2.35-1.85-5.23-2.78-8.64-2.78s-6.14.89-8.46,2.68c-2.32,1.78-4.08,4.24-5.3,7.38-1.22,3.14-1.83,6.74-1.83,10.82Z"></path>
                                <path class="cls-1" d="m382.86,102.22c-4.76,0-8.88-1.11-12.36-3.34-3.48-2.23-6.16-5.32-8.06-9.27s-2.84-8.51-2.84-13.68.95-9.71,2.84-13.71c1.89-4,4.52-7.13,7.88-9.41,3.36-2.27,7.24-3.41,11.65-3.41,2.77,0,5.44.5,8.01,1.51,2.57,1.01,4.88,2.55,6.93,4.62,2.05,2.07,3.67,4.68,4.87,7.81,1.2,3.14,1.79,6.84,1.79,11.12v2.92h-39.88v-5.22h33.83c0-3.28-.66-6.23-1.98-8.86-1.32-2.63-3.15-4.7-5.48-6.23-2.34-1.53-5.04-2.29-8.09-2.29-3.23,0-6.08.86-8.54,2.59-2.46,1.73-4.38,4.01-5.77,6.85-1.38,2.84-2.09,5.94-2.11,9.31v3.12c0,4.05.7,7.59,2.11,10.62,1.41,3.02,3.41,5.37,6,7.03,2.59,1.66,5.66,2.49,9.21,2.49,2.41,0,4.54-.38,6.36-1.13,1.83-.75,3.37-1.77,4.62-3.04,1.25-1.27,2.2-2.68,2.84-4.2l5.62,1.83c-.78,2.15-2.04,4.13-3.81,5.95-1.76,1.82-3.95,3.27-6.58,4.37-2.63,1.1-5.65,1.65-9.06,1.65Z"></path>
                                <path class="cls-1" d="m412.07,44.96v-11.86h55.9v11.86h-20.84v56.2h-14.22v-56.2h-20.84Z"></path>
                                <path class="cls-1" d="m471.62,101.16v-51.05h13.73v8.91h.53c.93-3.17,2.49-5.57,4.69-7.2,2.19-1.63,4.72-2.44,7.58-2.44.71,0,1.47.04,2.29.13.82.09,1.54.21,2.16.37v12.56c-.66-.2-1.58-.38-2.76-.53-1.17-.15-2.25-.23-3.22-.23-2.08,0-3.94.45-5.57,1.35-1.63.9-2.91,2.14-3.86,3.74-.94,1.6-1.41,3.43-1.41,5.52v28.88h-14.16Z"></path>
                                <path class="cls-1" d="m523,102.12c-3.26,0-6.16-.57-8.71-1.71-2.55-1.14-4.56-2.84-6.03-5.08-1.47-2.25-2.21-5.06-2.21-8.42,0-2.84.52-5.22,1.56-7.15,1.04-1.93,2.46-3.48,4.25-4.65,1.79-1.17,3.84-2.06,6.13-2.66,2.29-.6,4.7-1.02,7.23-1.26,2.97-.31,5.36-.6,7.18-.88,1.82-.28,3.13-.69,3.95-1.25.82-.55,1.23-1.37,1.23-2.46v-.2c0-2.1-.66-3.73-1.98-4.89-1.32-1.15-3.19-1.73-5.6-1.73-2.55,0-4.58.56-6.08,1.68-1.51,1.12-2.5,2.52-2.99,4.2l-13.09-1.06c.66-3.1,1.97-5.79,3.92-8.06,1.95-2.27,4.47-4.02,7.56-5.25,3.09-1.23,6.67-1.84,10.75-1.84,2.84,0,5.55.33,8.16,1,2.6.66,4.92,1.7,6.95,3.09,2.03,1.4,3.63,3.19,4.8,5.37,1.17,2.18,1.76,4.79,1.76,7.83v34.43h-13.43v-7.08h-.4c-.82,1.6-1.92,3-3.29,4.2-1.37,1.21-3.02,2.15-4.95,2.82-1.93.68-4.15,1.01-6.68,1.01Zm4.05-9.77c2.08,0,3.92-.42,5.52-1.25s2.85-1.96,3.76-3.37c.91-1.42,1.36-3.02,1.36-4.82v-5.42c-.44.29-1.05.55-1.81.78s-1.62.44-2.58.63c-.95.19-1.91.35-2.86.5-.95.14-1.82.27-2.59.38-1.66.24-3.11.63-4.35,1.16-1.24.53-2.21,1.25-2.89,2.14-.69.9-1.03,2.01-1.03,3.34,0,1.93.7,3.4,2.11,4.4,1.41,1.01,3.2,1.51,5.37,1.51Z"></path>
                                <path class="cls-1" d="m580.03,50.11h10.17v10.63h-10.17v40.41h-14.12v-40.41h-7.21v-10.63h7.21v-3.69c0-3.7.72-6.77,2.18-9.21,1.45-2.44,3.44-4.26,5.97-5.48,2.53-1.22,5.4-1.83,8.61-1.83,2.17,0,4.15.17,5.97.5,1.8.33,3.15.63,4.04.9l-2.53,10.63c-.55-.18-1.23-.34-2.04-.5s-1.64-.23-2.48-.23c-2.08,0-3.53.48-4.35,1.45-.83.96-1.23,2.31-1.23,4.04v3.42Z"></path>
                                <path class="cls-1" d="m616.12,50.11h10.17v10.63h-10.17v40.41h-14.12v-40.41h-7.21v-10.63h7.21v-3.69c0-3.7.72-6.77,2.18-9.21,1.45-2.44,3.44-4.26,5.97-5.48,2.53-1.22,5.4-1.83,8.61-1.83,2.17,0,4.15.17,5.97.5,1.8.33,3.15.63,4.04.9l-2.53,10.63c-.55-.18-1.23-.34-2.04-.5-.81-.15-1.64-.23-2.48-.23-2.08,0-3.53.48-4.35,1.45-.83.96-1.23,2.31-1.23,4.04v3.42Z"></path>
                                <path class="cls-1" d="m642.05,43.53c-2.11,0-3.9-.7-5.4-2.09-1.5-1.4-2.24-3.08-2.24-5.05s.75-3.66,2.26-5.07c1.51-1.41,3.3-2.11,5.38-2.11s3.91.7,5.4,2.09c1.5,1.4,2.24,3.08,2.24,5.05s-.75,3.66-2.24,5.07c-1.5,1.41-3.3,2.11-5.4,2.11Zm-7.11,57.63v-51.05h14.16v51.05h-14.16Z"></path>
                                <path class="cls-1" d="m683.46,102.15c-5.23,0-9.72-1.11-13.48-3.34-3.76-2.23-6.64-5.32-8.64-9.29-2.01-3.97-3.01-8.53-3.01-13.69s1.01-9.82,3.04-13.78c2.03-3.95,4.91-7.05,8.66-9.27,3.74-2.23,8.2-3.34,13.36-3.34,4.45,0,8.35.81,11.7,2.43,3.35,1.62,5.99,3.89,7.94,6.81,1.95,2.92,3.02,6.36,3.22,10.3h-13.36c-.38-2.55-1.37-4.6-2.97-6.16-1.61-1.56-3.71-2.34-6.3-2.34-2.19,0-4.1.59-5.73,1.78-1.63,1.19-2.9,2.91-3.81,5.17s-1.36,5-1.36,8.21.45,6.03,1.35,8.31c.9,2.28,2.17,4.02,3.81,5.22,1.64,1.2,3.56,1.79,5.75,1.79,1.62,0,3.07-.33,4.37-1,1.3-.66,2.37-1.63,3.22-2.91.85-1.27,1.41-2.81,1.68-4.6h13.36c-.22,3.9-1.28,7.33-3.17,10.29-1.89,2.96-4.5,5.27-7.83,6.93-3.32,1.66-7.26,2.49-11.8,2.49Z"></path>
                                </g>
                                <g>
                                <path class="cls-3" d="m800,37.72c0,4.31-3.5,7.81-7.82,7.81s-7.81-3.5-7.81-7.81,3.5-7.82,7.81-7.82,7.82,3.5,7.82,7.82Z"></path>
                                <path class="cls-2" d="m736.11,85.52c-.09,0-.18,0-.27-.01-.09,0-.18-.02-.26-.03.18.01.35.05.53.05Z"></path>
                                <path class="cls-2" d="m784.37,50.18v32.89c0,1.31-1.05,2.37-2.35,2.43-.03,0-.07.02-.11.02h-.37l-18.68-41.36-3.1-6.86c-2.03-4.5-6.52-7.4-11.47-7.4s-9.42,2.9-11.46,7.4l-11.96,26.47-10.4,23.03c-3.04,6.73,1.88,14.36,9.28,14.36h66.01c5.65,0,10.25-4.58,10.25-10.24v-40.74h-15.63Zm-48.25,35.34c-.09,0-.18,0-.27-.01-.09,0-.18-.02-.26-.03-2.25-.33-3.65-2.75-2.67-4.91l12.93-28.64c.95-2.1,3.93-2.1,4.89,0l15.17,33.59h-29.79Z"></path>
                                </g>
                            </g>
                            </g>
                        </g>
                        </svg>
                  <div class="f-14 f-md-16 w400 mt20 lh150 white-clr text-center">
                     <span class="w600">Note:</span> This site is not a part of the Facebook website or Facebook Inc. Additionally, this site is NOT endorsed by Facebook in any way. FACEBOOK & INSTAGRAM are the trademarks of FACEBOOK, Inc.<br>
                  </div>
                  
                  <div class="f-15 f-md-18 w600 lh150 white-clr text-xs-center mt20"><script type="text/javascript" src="https://warriorplus.com/o2/disclaimer/hz2587" defer></script><div class="wplus_spdisclaimer"></div></div>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-14 f-md-16 w400 lh150 white-clr text-xs-center">Copyright © TubeTrafficAi 2023</div>
                  <ul class="footer-ul w400 f-14 f-md-16 white-clr text-center text-md-right">
                     <li><a href="https://support.bizomart.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://tubetrafficai.com/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://tubetrafficai.com/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://tubetrafficai.com/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://tubetrafficai.com/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://tubetrafficai.com/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://tubetrafficai.com/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                    
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!-- Exit Popup and Timer -->
      <?php //include('timer.php'); ?>	  
      <!-- Exit Popup and Timer End -->
   </body>
</html>

<script> 
function myFunction() {
  var checkBox = document.getElementById("check");
  var buybutton = document.getElementById("buyButton");
  if (checkBox.checked == true){
 
	    buybutton.getAttribute("href");
	    buybutton.setAttribute("href","https://warriorplus.com/o2/buy/hz2587/wl4q8v/trx0mv");
	} else {
		 buybutton.getAttribute("href");
		 buybutton.setAttribute("href","https://warriorplus.com/o2/buy/hz2587/wl4q8v/g71kw3");
	  }
}
</script>

<script>
	// Define the audio data as a JSON object
	var audioData = {
	"Angry" : "https://gptvoicer.s3.amazonaws.com/voices/en-US-JaneNeural-angry.mp3",
	"Cheerful" : "https://gptvoicer.s3.amazonaws.com/voices/en-US-JaneNeural-cheerful.mp3",
	"Excited" : "https://gptvoicer.s3.amazonaws.com/voices/en-US-JaneNeural-excited.mp3",
	"Friendly" : "https://gptvoicer.s3.amazonaws.com/voices/en-US-JaneNeural-friendly.mp3",
	"Hopeful" : "https://gptvoicer.s3.amazonaws.com/voices/en-US-JaneNeural-hopeful.mp3",
	"Sad" : "https://gptvoicer.s3.amazonaws.com/voices/en-US-JaneNeural-sad.mp3",
	"Shouting" : "https://gptvoicer.s3.amazonaws.com/voices/en-US-JaneNeural-shouting.mp3",
	"Terrified" : "https://gptvoicer.s3.amazonaws.com/voices/en-US-JaneNeural-terrified.mp3",
	"Unfriendly" : "https://gptvoicer.s3.amazonaws.com/voices/en-US-JaneNeural-unfriendly.mp3",
	"Whispering" : "https://gptvoicer.s3.amazonaws.com/voices/en-US-JaneNeural-whispering.mp3",
	"Normal" : "https://gptvoicer.s3.amazonaws.com/voices/en-US-JaneNeural.mp3"
};
  
	// Get the container for the audio boxes
	var container = document.getElementById('audioContainer');
  
	// Loop through the audio data and generate the HTML for each audio box
	for (var voice in audioData) {
	  var box = '<div class="audiobox" style="background-color: #f6d89e; border-radius: 6px; padding: 5px 5px; margin-top:10px">' +
				  '<div class="row align-items-center">' +
					'<div class="col-5">' +
					  '<div class="f-md-22 f-12 text-center w400">' +
						voice +
					  '</div>' +
					'</div>' +
					'<div class="col-7">' +
					  '<button class="playButton plybtn" data-audio="' + audioData[voice] + '">' +
						'<span class="playIcon" style="font-size: 20px;">▶</span>' +
					  '</button>' +
					  '<audio class="myAudio" data-state="paused">' +
						'<source src="" type="audio/mpeg">' +
					  '</audio>' +
					'</div>' +
				  '</div>' +
				'</div>';
  
	  container.innerHTML += box;
	}
  
	// Get all the play buttons, play icons, and audio elements
	var playButtons = document.querySelectorAll('.playButton');
	var playIcons = document.querySelectorAll('.playIcon');
	var audioElements = document.querySelectorAll('.myAudio');
  
	// Add event listeners to the play buttons
	playButtons.forEach(function(button, index) {
	  button.addEventListener('click', function() {
		var audio = audioElements[index];
		var playIcon = playIcons[index];
  
		if (audio.dataset.state === 'paused') {
		  // Pause any currently playing audio elements
		  audioElements.forEach(function(element) {
			if (element.dataset.state === 'playing') {
			  element.pause();
			  var index = Array.from(audioElements).indexOf(element);
			  playIcons[index].innerHTML = '▶';
			  element.dataset.state = 'paused';
			}
		  });
  
		  // Start playing the new audio element
		  audio.src = button.dataset.audio;
		  audio.play();
		  playIcon.innerHTML = '❚❚';
		  audio.dataset.state = 'playing';
		} else {
		  audio.pause();
		  playIcon.innerHTML = '▶';
		  audio.dataset.state = 'paused';
		}
	  });
	});
  </script>
  <script>
	// Define the audio data as a JSON object
	var audioDatamale = {
	"Angry" : "https://gptvoicer.s3.amazonaws.com/voices/en-US-JasonNeural-angry.mp3",
	"Cheerful" : "https://gptvoicer.s3.amazonaws.com/voices/en-US-JasonNeural-cheerful.mp3",
	"Excited" : "https://gptvoicer.s3.amazonaws.com/voices/en-US-JasonNeural-excited.mp3",
	"Friendly" : "https://gptvoicer.s3.amazonaws.com/voices/en-US-JasonNeural-friendly.mp3",
	"Hopeful" : "https://gptvoicer.s3.amazonaws.com/voices/en-US-JasonNeural-hopeful.mp3",
	"Sad" : "https://gptvoicer.s3.amazonaws.com/voices/en-US-JasonNeural-sad.mp3",
	"Shouting" : "https://gptvoicer.s3.amazonaws.com/voices/en-US-JasonNeural-shouting.mp3",
	"Terrified" : "https://gptvoicer.s3.amazonaws.com/voices/en-US-JasonNeural-terrified.mp3",
	"Unfriendly" : "https://gptvoicer.s3.amazonaws.com/voices/en-US-JasonNeural-unfriendly.mp3",
	"Whispering" : "https://gptvoicer.s3.amazonaws.com/voices/en-US-JasonNeural-whispering.mp3",
	"Normal" : "https://gptvoicer.s3.amazonaws.com/voices/en-US-JasonNeural.mp3"
}
  
	// Get the container for the audio boxes
	var containermale = document.getElementById('audioContainermale');
  
	// Loop through the audio data and generate the HTML for each audio box
	for (var voicemale in audioDatamale) {
	  var boxmale = '<div class="audioboxmale" style="background-color: #bddbec; border-radius: 6px; padding: 5px 5px; margin-top:10px">' +
				  '<div class="row align-items-center">' +
					'<div class="col-5">' +
					  '<div class="f-md-22 f-12 text-center w400">' +
						voicemale +
					  '</div>' +
					'</div>' +
					'<div class="col-7">' +
					  '<button class="playButtonmale plybtnmale" data-audio="' + audioDatamale[voicemale] + '">' +
						'<span class="playIconmale" style="font-size: 20px;">▶</span>' +
					  '</button>' +
					  '<audio class="myAudiomale" data-state="paused">' +
						'<source src="" type="audio/mpeg">' +
					  '</audio>' +
					'</div>' +
				  '</div>' +
				'</div>';
  
		containermale.innerHTML += boxmale;
	}
  
	// Get all the play buttons, play icons, and audio elements
	var playButtonsmale = document.querySelectorAll('.playButtonmale');
	var playIconsmale = document.querySelectorAll('.playIconmale');
	var audioElementsmale = document.querySelectorAll('.myAudiomale');
  
	// Add event listeners to the play buttons
	playButtonsmale.forEach(function(button, index) {
	  button.addEventListener('click', function() {
		var audio = audioElementsmale[index];
		var playIcon = playIconsmale[index];
  
		if (audio.dataset.state === 'paused') {
		  // Pause any currently playing audio elements
		  audioElementsmale.forEach(function(element) {
			if (element.dataset.state === 'playing') {
			  element.pause();
			  var index = Array.from(audioElementsmale).indexOf(element);
			  playIconsmale[index].innerHTML = '▶';
			  element.dataset.state = 'paused';
			}
		  });
  
		  // Start playing the new audio element
		  audio.src = button.dataset.audio;
		  audio.play();
		  playIcon.innerHTML = '❚❚';
		  audio.dataset.state = 'playing';
		} else {
		  audio.pause();
		  playIcon.innerHTML = '▶';
		  audio.dataset.state = 'paused';
		}
	  });
	});
  </script>
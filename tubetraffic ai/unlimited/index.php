<!Doctype html>
<html>
<head>
    <title>TubeTrafficAi Unlimited</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=9">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta name="title" content="TubeTrafficAi | Pro">
    <meta name="description" content="Remove All Limitations To Go Unlimited & Get 3X More Traffic, Commissions & Profits Faster & Easier With This Pro Upgrade?">
    <meta name="keywords" content="TubeTrafficAi">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta property="og:image" content="https://tubetrafficai.com/pro/thumbnail.png">
    <meta name="language" content="English">
    <meta name="revisit-after" content="1 days">
    <meta name="author" content="Dr. Amit Pareek">
    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:title" content="TubeTrafficAi | Pro">
    <meta property="og:description" content="Remove All Limitations To Go Unlimited & Get 3X More Traffic, Commissions & Profits Faster & Easier With This Pro Upgrade?">
    <meta property="og:image" content="https://tubetrafficai.com/pro/thumbnail.png">
    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:title" content="TubeTrafficAi | Pro">
    <meta property="twitter:description" content="Remove All Limitations To Go Unlimited & Get 3X More Traffic, Commissions & Profits Faster & Easier With This Pro Upgrade?">
    <meta property="twitter:image" content="https://tubetrafficai.com/pro/thumbnail.png">
    <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&family=Poppins:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    <!-- Start Editor required -->
    <link rel="stylesheet" href="../common_assets/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="../common_assets/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="../common_assets/css/general.css">
    <link rel="stylesheet" href="assets/css/style.css" type="text/css">
    <link rel="stylesheet" href="assets/css/timer.css" type="text/css">
    <script src="../common_assets/js/jquery.min.js"></script>
    <script src="../common_assets/js/popper.min.js"></script>
    <script src="../common_assets/js/bootstrap.min.js"></script>
</head>
<body>
    <header class="d-flex justify-content-center align-items-center">
        <div>
            <img src="assets/images/error.webp" class="img-fluid">
        </div>
        <h1 class="text-white text-center WesFYW03Bold"> Warning: This offer will expire automatically once you leave the page</h1>
	</header> 
    <!--1. Header Section Start -->
    <div class="header-section">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 800 131.43" style="max-height:75px;">
                        <defs>
                            <style>
                            .cls-1 {
                                fill: #fff;
                            }
        
                            .cls-2 {
                                fill: #fd2f15;
                            }
        
                            .cls-3 {
                                fill: #fbbf33;
                            }
                            </style>
                        </defs>
                        <g id="Layer_1-2" data-name="Layer 1">
                            <g>
                            <g>
                                <path class="cls-3" d="m105.61,78.06l40.05,23.12c6.01,3.47,13.51-.87,13.51-7.8v-46.25c0-6.93-7.51-11.27-13.51-7.8l-40.05,23.12c-6.01,3.47-6.01,12.14,0,15.6Z"></path>
                                <g>
                                <rect class="cls-3" x="26.92" y="58.86" width="16.03" height="46.81"></rect>
                                <path class="cls-2" d="m125.22,26.34h-7.16v46.06c0,32.6-26.43,59.03-59.03,59.03H0v-65.94C0,32.89,26.43,6.45,59.03,6.45h38.52l-12.38,19.88h-26.14c-21.59,0-39.16,17.57-39.16,39.15v46.07h39.16c21.59,0,39.16-17.57,39.16-39.16V26.34h-5.79L108.81,0l16.41,26.34Z"></path>
                                <path class="cls-3" d="m67.04,46.21h-16.02v59.46h8.01c2.76,0,5.45-.35,8.01-.99v-58.47Z"></path>
                                <path class="cls-3" d="m90.77,33.56h-16.03v68.14c7.57-4.07,13.39-10.99,16.03-19.31v-48.83Z"></path>
                                </g>
                            </g>
                            <g>
                                <g>
                                <path class="cls-1" d="m195.69,38.68v-5.58h49.35v5.58h-21.57v62.48h-6.21v-62.48h-21.57Z"></path>
                                <path class="cls-1" d="m283.29,81.65v-31.54h5.92v51.05h-5.92v-8.47h-.47c-1.2,2.61-3.09,4.79-5.68,6.53-2.59,1.74-5.76,2.61-9.5,2.61-3.26,0-6.15-.71-8.67-2.14-2.53-1.43-4.51-3.53-5.95-6.3-1.44-2.77-2.16-6.17-2.16-10.2v-33.07h5.92v32.7c0,3.99,1.17,7.2,3.52,9.62,2.35,2.43,5.36,3.64,9.04,3.64,2.3,0,4.52-.55,6.65-1.66,2.13-1.11,3.88-2.73,5.25-4.87,1.37-2.14,2.06-4.77,2.06-7.89Z"></path>
                                <path class="cls-1" d="m306.05,101.16V33.09h5.95v26.75h.53c.66-1.53,1.62-3.1,2.87-4.72,1.25-1.62,2.9-2.97,4.95-4.07,2.05-1.1,4.64-1.65,7.76-1.65,4.19,0,7.86,1.1,11.03,3.31,3.17,2.21,5.64,5.28,7.41,9.24,1.77,3.95,2.66,8.56,2.66,13.81s-.88,9.89-2.64,13.86c-1.76,3.97-4.22,7.06-7.38,9.27-3.16,2.22-6.82,3.32-10.98,3.32-3.1,0-5.69-.55-7.76-1.66-2.07-1.11-3.74-2.48-5-4.1-1.26-1.63-2.24-3.22-2.92-4.77h-.73v9.47h-5.75Zm5.85-25.49c0,4.12.61,7.77,1.84,10.95,1.23,3.18,3,5.67,5.32,7.48,2.31,1.81,5.12,2.71,8.42,2.71s6.26-.94,8.61-2.81c2.35-1.87,4.13-4.4,5.35-7.59,1.22-3.19,1.83-6.77,1.83-10.73s-.6-7.46-1.81-10.6c-1.21-3.15-2.99-5.64-5.33-7.49-2.35-1.85-5.23-2.78-8.64-2.78s-6.14.89-8.46,2.68c-2.32,1.78-4.08,4.24-5.3,7.38-1.22,3.14-1.83,6.74-1.83,10.82Z"></path>
                                <path class="cls-1" d="m382.86,102.22c-4.76,0-8.88-1.11-12.36-3.34-3.48-2.23-6.16-5.32-8.06-9.27s-2.84-8.51-2.84-13.68.95-9.71,2.84-13.71c1.89-4,4.52-7.13,7.88-9.41,3.36-2.27,7.24-3.41,11.65-3.41,2.77,0,5.44.5,8.01,1.51,2.57,1.01,4.88,2.55,6.93,4.62,2.05,2.07,3.67,4.68,4.87,7.81,1.2,3.14,1.79,6.84,1.79,11.12v2.92h-39.88v-5.22h33.83c0-3.28-.66-6.23-1.98-8.86-1.32-2.63-3.15-4.7-5.48-6.23-2.34-1.53-5.04-2.29-8.09-2.29-3.23,0-6.08.86-8.54,2.59-2.46,1.73-4.38,4.01-5.77,6.85-1.38,2.84-2.09,5.94-2.11,9.31v3.12c0,4.05.7,7.59,2.11,10.62,1.41,3.02,3.41,5.37,6,7.03,2.59,1.66,5.66,2.49,9.21,2.49,2.41,0,4.54-.38,6.36-1.13,1.83-.75,3.37-1.77,4.62-3.04,1.25-1.27,2.2-2.68,2.84-4.2l5.62,1.83c-.78,2.15-2.04,4.13-3.81,5.95-1.76,1.82-3.95,3.27-6.58,4.37-2.63,1.1-5.65,1.65-9.06,1.65Z"></path>
                                <path class="cls-1" d="m412.07,44.96v-11.86h55.9v11.86h-20.84v56.2h-14.22v-56.2h-20.84Z"></path>
                                <path class="cls-1" d="m471.62,101.16v-51.05h13.73v8.91h.53c.93-3.17,2.49-5.57,4.69-7.2,2.19-1.63,4.72-2.44,7.58-2.44.71,0,1.47.04,2.29.13.82.09,1.54.21,2.16.37v12.56c-.66-.2-1.58-.38-2.76-.53-1.17-.15-2.25-.23-3.22-.23-2.08,0-3.94.45-5.57,1.35-1.63.9-2.91,2.14-3.86,3.74-.94,1.6-1.41,3.43-1.41,5.52v28.88h-14.16Z"></path>
                                <path class="cls-1" d="m523,102.12c-3.26,0-6.16-.57-8.71-1.71-2.55-1.14-4.56-2.84-6.03-5.08-1.47-2.25-2.21-5.06-2.21-8.42,0-2.84.52-5.22,1.56-7.15,1.04-1.93,2.46-3.48,4.25-4.65,1.79-1.17,3.84-2.06,6.13-2.66,2.29-.6,4.7-1.02,7.23-1.26,2.97-.31,5.36-.6,7.18-.88,1.82-.28,3.13-.69,3.95-1.25.82-.55,1.23-1.37,1.23-2.46v-.2c0-2.1-.66-3.73-1.98-4.89-1.32-1.15-3.19-1.73-5.6-1.73-2.55,0-4.58.56-6.08,1.68-1.51,1.12-2.5,2.52-2.99,4.2l-13.09-1.06c.66-3.1,1.97-5.79,3.92-8.06,1.95-2.27,4.47-4.02,7.56-5.25,3.09-1.23,6.67-1.84,10.75-1.84,2.84,0,5.55.33,8.16,1,2.6.66,4.92,1.7,6.95,3.09,2.03,1.4,3.63,3.19,4.8,5.37,1.17,2.18,1.76,4.79,1.76,7.83v34.43h-13.43v-7.08h-.4c-.82,1.6-1.92,3-3.29,4.2-1.37,1.21-3.02,2.15-4.95,2.82-1.93.68-4.15,1.01-6.68,1.01Zm4.05-9.77c2.08,0,3.92-.42,5.52-1.25s2.85-1.96,3.76-3.37c.91-1.42,1.36-3.02,1.36-4.82v-5.42c-.44.29-1.05.55-1.81.78s-1.62.44-2.58.63c-.95.19-1.91.35-2.86.5-.95.14-1.82.27-2.59.38-1.66.24-3.11.63-4.35,1.16-1.24.53-2.21,1.25-2.89,2.14-.69.9-1.03,2.01-1.03,3.34,0,1.93.7,3.4,2.11,4.4,1.41,1.01,3.2,1.51,5.37,1.51Z"></path>
                                <path class="cls-1" d="m580.03,50.11h10.17v10.63h-10.17v40.41h-14.12v-40.41h-7.21v-10.63h7.21v-3.69c0-3.7.72-6.77,2.18-9.21,1.45-2.44,3.44-4.26,5.97-5.48,2.53-1.22,5.4-1.83,8.61-1.83,2.17,0,4.15.17,5.97.5,1.8.33,3.15.63,4.04.9l-2.53,10.63c-.55-.18-1.23-.34-2.04-.5s-1.64-.23-2.48-.23c-2.08,0-3.53.48-4.35,1.45-.83.96-1.23,2.31-1.23,4.04v3.42Z"></path>
                                <path class="cls-1" d="m616.12,50.11h10.17v10.63h-10.17v40.41h-14.12v-40.41h-7.21v-10.63h7.21v-3.69c0-3.7.72-6.77,2.18-9.21,1.45-2.44,3.44-4.26,5.97-5.48,2.53-1.22,5.4-1.83,8.61-1.83,2.17,0,4.15.17,5.97.5,1.8.33,3.15.63,4.04.9l-2.53,10.63c-.55-.18-1.23-.34-2.04-.5-.81-.15-1.64-.23-2.48-.23-2.08,0-3.53.48-4.35,1.45-.83.96-1.23,2.31-1.23,4.04v3.42Z"></path>
                                <path class="cls-1" d="m642.05,43.53c-2.11,0-3.9-.7-5.4-2.09-1.5-1.4-2.24-3.08-2.24-5.05s.75-3.66,2.26-5.07c1.51-1.41,3.3-2.11,5.38-2.11s3.91.7,5.4,2.09c1.5,1.4,2.24,3.08,2.24,5.05s-.75,3.66-2.24,5.07c-1.5,1.41-3.3,2.11-5.4,2.11Zm-7.11,57.63v-51.05h14.16v51.05h-14.16Z"></path>
                                <path class="cls-1" d="m683.46,102.15c-5.23,0-9.72-1.11-13.48-3.34-3.76-2.23-6.64-5.32-8.64-9.29-2.01-3.97-3.01-8.53-3.01-13.69s1.01-9.82,3.04-13.78c2.03-3.95,4.91-7.05,8.66-9.27,3.74-2.23,8.2-3.34,13.36-3.34,4.45,0,8.35.81,11.7,2.43,3.35,1.62,5.99,3.89,7.94,6.81,1.95,2.92,3.02,6.36,3.22,10.3h-13.36c-.38-2.55-1.37-4.6-2.97-6.16-1.61-1.56-3.71-2.34-6.3-2.34-2.19,0-4.1.59-5.73,1.78-1.63,1.19-2.9,2.91-3.81,5.17s-1.36,5-1.36,8.21.45,6.03,1.35,8.31c.9,2.28,2.17,4.02,3.81,5.22,1.64,1.2,3.56,1.79,5.75,1.79,1.62,0,3.07-.33,4.37-1,1.3-.66,2.37-1.63,3.22-2.91.85-1.27,1.41-2.81,1.68-4.6h13.36c-.22,3.9-1.28,7.33-3.17,10.29-1.89,2.96-4.5,5.27-7.83,6.93-3.32,1.66-7.26,2.49-11.8,2.49Z"></path>
                                </g>
                                <g>
                                <path class="cls-3" d="m800,37.72c0,4.31-3.5,7.81-7.82,7.81s-7.81-3.5-7.81-7.81,3.5-7.82,7.81-7.82,7.82,3.5,7.82,7.82Z"></path>
                                <path class="cls-2" d="m736.11,85.52c-.09,0-.18,0-.27-.01-.09,0-.18-.02-.26-.03.18.01.35.05.53.05Z"></path>
                                <path class="cls-2" d="m784.37,50.18v32.89c0,1.31-1.05,2.37-2.35,2.43-.03,0-.07.02-.11.02h-.37l-18.68-41.36-3.1-6.86c-2.03-4.5-6.52-7.4-11.47-7.4s-9.42,2.9-11.46,7.4l-11.96,26.47-10.4,23.03c-3.04,6.73,1.88,14.36,9.28,14.36h66.01c5.65,0,10.25-4.58,10.25-10.24v-40.74h-15.63Zm-48.25,35.34c-.09,0-.18,0-.27-.01-.09,0-.18-.02-.26-.03-2.25-.33-3.65-2.75-2.67-4.91l12.93-28.64c.95-2.1,3.93-2.1,4.89,0l15.17,33.59h-29.79Z"></path>
                                </g>
                            </g>
                            </g>
                        </g>
                    </svg>
                </div>
                <div class="col-12 mt20 mt-md50 text-center ">
                    <div class="f-md-22 f-18 w600 lh130 post-heading white-clr">
                        <span>Congratulations! Your order is almost complete. But before you get started...</span>
                    </div>
                </div>
                <div class="col-12 mt20 mt-md40 px-md30">
                    <div class="f-md-50 f-30 w500 text-center white-clr lh130">
                        Unlock Unlimited Potential To Get <span class="yellow-clr w700">5X More Traffic, Commissions & Profits Faster & Easier </span> With This Pro Upgrade
                    </div>
                </div>
                <div class="col-12 mt20 mt-md40 f-22 f-md-26 w400 text-center lh130 white-clr">
                    Unlock Never Seen Before Features- Create Unlimited Channels & Projects, Add Unlimited Videos, Add Unlimited Custom Domains, Get Unlimited Storage, Get Unlimited Bandwidth, Drive Unlimited Traffic & Leads, Promote Unlimited Affiliate Offers and 10+ Other Pro Features…
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md40">
                    <img src="assets/images/product-img.webp" class="img-fluid mx-auto d-block">
                   <!-- <div class="responsive-video" editabletype="video" style="z-index: 10;">
                     <iframe src="https://TubeTrafficAi.dotcompal.com/video/embed/jpjzgsnfod" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                        box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                     </div> -->
                </div>
                <div class=" col-12 col-md-10 mx-auto mt30 mt-md50">
                    <div class="f-20 f-md-35 text-center probtn1">
                        <a href="#buynow">Get Instant Access to TubeTrafficAi Pro</a>
                    </div>
                </div>
                <div class="col-12 mt20 mt-md20 f-md-28 f-22 lh130 w400 text-center white-clr">
                    This is An Exclusive Deal for <span class="w500">"TubeTrafficAi"</span> Users Only...
                </div>
            </div>
        </div>
    </div>
    <!--1. Header Section End -->
    <!--2. Second Section Start -->
    <div class="second-section">
        <div class="container ">
            <div class="row">
                <div class="col-12 text-center text-capitalize">
                    <div class="f-22 f-md-26 text-center w400 lh130">
                        You Are About To Witness The RAW Power Of TubeTrafficAi In A Way You Have Never Seen It.
                    </div>
                </div>
                <div class="col-12 mt20">
                    <div class="f-md-50 f-30 w500 lh130 text-capitalize text-center black-clr">
                        A Whopping <span class="w700"> 92% Of The TubeTrafficAi Members
                     Upgraded  To TubeTrafficAi Pro...</span> Here's Why
                    </div>
                </div>
                <div class="col-12 mt20 mt-md70">
                    <div class="row">
                        <div class="col-md-6 col-12">
                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-start text-center text-md-start">
                                <img src="assets/images/fe1.png" class="img-fluid d-block mx-auto" width="70">
                                <div class="f-20 f-md-22 lh130 w400 ml-md30 mt20 mt-md0 text-capitalize">
									Break Free & Go Limitless- Create Unlimited Channels & Projects, Add Unlimited Videos, Add Unlimited Custom Domains, Get Unlimited Storage, Get Unlimited Bandwidth, Drive Unlimited Traffic & Leads, Promote Unlimited Affiliate Offers.
                                </div>
                            </div>
                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-center text-center text-md-start mt20 mt-md50">
                                <img src="assets/images/fe2.png" class="img-fluid d-block mx-auto" width="70">
                                <div class="f-20 f-md-22 lh130 w400 ml-md30 mt20 mt-md0 text-capitalize w-100">
                                    Create Unlimited Channels in ANY NICHE For Yourself and Clients
                                </div>
                            </div>
                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-center text-center text-md-start mt20 mt-md50">
                                <img src="assets/images/fe3.png" class="img-fluid d-block mx-auto" width="70">
                                <div class="f-20 f-md-22 lh130 w400 ml-md30 mt20 mt-md0 text-capitalize w-100">
                                    Create Unlimited Projects Inside Your Business & Get Maximum Audience Hooked
                                </div>
                            </div>
                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-center text-center text-md-start mt20 mt-md50">
                                <img src="assets/images/fe4.png" class="img-fluid d-block mx-auto" width="70">
                                <div class="f-20 f-md-22 lh130 w400 ml-md30 mt20 mt-md0 text-capitalize w-100">
                                    Add Unlimited Videos & Show MORE Content On 100s Of Topic Or Keywords
                                </div>
                            </div>
                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-start text-center text-md-start mt20 mt-md50">
                                <img src="assets/images/fe5.png" class="img-fluid d-block mx-auto" width="70">
                                <div class="f-20 f-md-22 lh130 w400 ml-md30 mt20 mt-md0 text-capitalize">
                                    Add Unlimited Custom Domains To Build Your Authority & Credibility
                                </div>
                            </div>
                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-start text-center text-md-start mt20 mt-md50">
                                <img src="assets/images/fe6.png" class="img-fluid d-block mx-auto" width="70">
                                <div class="f-20 f-md-22 lh130 w400 ml-md30 mt20 mt-md0 text-capitalize">
									Getting Unlimited Storage To Get Your Media Content Delivered Faster
                                </div>
                            </div>
                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-start text-center text-md-start mt20 mt-md50">
                                <img src="assets/images/fe7.png" class="img-fluid d-block mx-auto" width="70">
                                <div class="f-20 f-md-22 lh130 w400 ml-md30 mt20 mt-md0 text-capitalize">
                                    Getting Unlimited Bandwidth To Give Best User Experience
                                </div>
                            </div>
                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-center text-center text-md-start mt20 mt-md50">
                                <img src="assets/images/fe8.png" class="img-fluid d-block mx-auto" width="70">
                                <div class="f-20 f-md-22 lh130 w400 ml-md30 mt20 mt-md0 text-capitalize">
                                    Drive Unlimited Visitors And Leads For Your Offers
                                </div>
                            </div>

                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-start text-center text-md-start mt20 mt-md50">
                                <img src="assets/images/fe9.png" class="img-fluid d-block mx-auto" width="70">
                                <div class="f-20 f-md-22 lh130 w400 ml-md30 mt20 mt-md0 text-capitalize">
                                    Create Unlimited Ads & Get Maximum Affiliate Commissions
                                </div>
                            </div>
							 <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-start text-center text-md-start mt20 mt-md50">
                                <img src="assets/images/fe10.png" class="img-fluid d-block mx-auto" width="70">
                                <div class="f-20 f-md-22 lh130 w400 ml-md30 mt20 mt-md0 text-capitalize">
									Get 5 Stunning And Ready-To-Use Fully Customizable Video Players
                                </div>
                            </div>

                            <!--14-->
                        </div>
                        <div class="col-md-6 col-12">

                           
                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-start text-center text-md-start mt20 mt-md0">
                                <img src="assets/images/fe11.png" class="img-fluid d-block mx-auto" width="70">
                                <div class="f-20 f-md-22 lh130 w400 ml-md30 mt20 mt-md0 text-capitalize">
                                    Customize Your Player With 8 Attractive And Eye-Catchy Frames
                                </div>
                            </div>

                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-start text-center text-md-start mt20 mt-md50">
                                <img src="assets/images/fe12.png" class="img-fluid d-block mx-auto" width="70">
                                <div class="f-20 f-md-22 lh130 w400 ml-md30 mt20 mt-md0 text-capitalize">
                                    Get hordes of traffic by getting your videos forcefully shared on top social media platforms.
                                </div>
                            </div>

                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-center text-center text-md-start mt20 mt-md50">
                                <img src="assets/images/fe13.png" class="img-fluid d-block mx-auto" width="70">
                                <div class="f-20 f-md-22 lh130 w400 ml-md30 mt20 mt-md0 text-capitalize w-100">
                                    Get 50 EXTRA PROVEN Converting Lead And Promo Templates
                                </div>
                            </div>

                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-center text-center text-md-start mt20 mt-md50">
                                <img src="assets/images/fe14.png" class="img-fluid d-block mx-auto" width="70">
                                <div class="f-20 f-md-22 lh130 w400 ml-md30 mt20 mt-md0 text-capitalize w-100">
                                    Never Offered Before Video Personalization To Boost Conversions
                                </div>
                            </div>

                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-center text-center text-md-start mt20 mt-md50">
                                <img src="assets/images/fe15.png" class="img-fluid d-block" width="70">
                                <div class="f-20 f-md-22 lh130 w400 ml-md30 mt20 mt-md0 text-capitalize">
                                    Create A Personalized E-Mail Link of The Video
                                </div>
                            </div>
                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-center text-center text-md-start mt20 mt-md50">
                                <img src="assets/images/fe16.png" class="img-fluid d-block mx-auto" width="70">
                                <div class="f-20 f-md-22 lh130 w400 ml-md30 mt20 mt-md0 text-capitalize">
									Viral Page Customization Feature To Enhance Its Visual Appeal & Resonate With Your Brand
                                </div>
                            </div>

                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-center text-center text-md-start mt20 mt-md50">
                                <img src="assets/images/fe17.png" class="img-fluid d-block mx-auto" width="70">
                                <div class="f-20 f-md-22 lh130 w400 ml-md30 mt20 mt-md0 text-capitalize">
                                   Capture Leads Of Your Visitors When They Interact On Your Videos
                                </div>
                            </div>

                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-center text-center text-md-start mt20 mt-md50">
                                <img src="assets/images/fe18.png" class="img-fluid d-block mx-auto" width="70">
                                <div class="f-20 f-md-22 lh130 w400 ml-md30 mt20 mt-md0 text-capitalize">
                                    Maximize ROI From Your Leads With Webinar Integration
                                </div>
                            </div>
							<div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-center text-center text-md-start mt20 mt-md50">
                                <img src="assets/images/fe19.png" class="img-fluid d-block mx-auto" width="70">
                                <div class="f-20 f-md-22 lh130 w400 ml-md30 mt20 mt-md0 text-capitalize">
                                    Unlimited Team Management To Let Your Team Handle All The Manual Work
                                </div>
                            </div>
							<div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-center text-center text-md-start mt20 mt-md50">
                                <img src="assets/images/fe20.png" class="img-fluid d-block" width="70">
                                <div class="f-20 f-md-22 lh130 w400 ml-md30 mt20 mt-md0 text-capitalize">
                                    Get All These Benefits At An Unparalleled Price
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-10 mx-auto col-12 mt20 mt-md60">
                    <div class="f-22 f-md-36 text-center probtn"><a href="#buynow">Get Instant Access to TubeTrafficAi Pro</a></div>
                </div>
            </div>
        </div>
    </div>
    <!--2. Second Section End -->
    <div class="chackout">
        <div class= "container">
            <div class="row"> 
                <div class="col-12 f-35 f-md-46 w600 text-center white-clr ">
                Checkout Real Results That Our Customers Got By Upgrading To Pro Today…
                </div> 
            </div>
            <div class="row mt20 mt-md40">
                <div class="col-12">
                    <img src="assets/images/income.webp" class="img-fluid" alt="">
                </div> 
            </div>         
        </div>               
    </div>                   
    <!--3. Third Section Start -->
    <div class="third-section">
        <div class="container ">
            <div class="row">
                <div class="col-12 col-md-12">
                    <div class="f-30 f-md-50 lh130 w500 text-center black-clr">
                        This Is Your Chance To <span class="w700">Get Unfair Advantage </span> <br class="d-none d-md-block"> Over Your Competition
                    </div>
                    <div class="f-20 f-md-22 w400 mt20 mt-md35 lh140 text-center">
                        <!-- If you are on this page, you have already realized the power of this revolutionary technology and secured a leap from the competitors. If a basic feature that everyone is getting can bring such massive results… <span class="w600">what more this pro-upgrade can bring on the table with the power to  Create Unlimited Business, Create Unlimited Video Channels, Add Unlimited Videos In Channels, Get Unlimited Bandwidth and Other Pro Features</span> -->
						If you are on this page, you have already realized the power of this revolutionary technology and secured a leap from the competitors. If a basic feature that everyone is getting can bring such massive results… what more this pro-upgrade can bring on the table with the power to Create Unlimited Channels & Projects, Add Unlimited Videos, Add Unlimited Custom Domains, Get Unlimited Storage, Get Unlimited Bandwidth, Drive Unlimited Traffic & Leads, Promote Unlimited Affiliate Offers and Other Pro Features.
                        <br>
                        <br> I know you're probably very eager to get to your members area and <span class="w600">start using TubeTrafficAi and start getting more sales and commissions from your marketing campaigns.</span> However, if you can give me a few minutes
                        I'll show you how to take this system beyond basic features using it to skyrocket your profit.
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--3. Third Section End -->
    <!--4. Fourth Section Starts -->
    <div class="fifth-section">
        <div class="container ">
            <div class="row">
                <div class="col-12 text-center">
                    <div class="f-28 f-md-50 w700 lh140 white-clr caveat proudly-sec">
                        Presenting…
                     </div>
                    
                </div>
                <div class="col-12 mt-md30 mt20 text-center">
                    <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 800 131.43" style="max-height:100px;">
                        <defs>
                            <style>
                            .cls-1 {
                                fill: #fff;
                            }
        
                            .cls-2 {
                                fill: #fd2f15;
                            }
        
                            .cls-3 {
                                fill: #fbbf33;
                            }
                            </style>
                        </defs>
                        <g id="Layer_1-2" data-name="Layer 1">
                            <g>
                            <g>
                                <path class="cls-3" d="m105.61,78.06l40.05,23.12c6.01,3.47,13.51-.87,13.51-7.8v-46.25c0-6.93-7.51-11.27-13.51-7.8l-40.05,23.12c-6.01,3.47-6.01,12.14,0,15.6Z"></path>
                                <g>
                                <rect class="cls-3" x="26.92" y="58.86" width="16.03" height="46.81"></rect>
                                <path class="cls-2" d="m125.22,26.34h-7.16v46.06c0,32.6-26.43,59.03-59.03,59.03H0v-65.94C0,32.89,26.43,6.45,59.03,6.45h38.52l-12.38,19.88h-26.14c-21.59,0-39.16,17.57-39.16,39.15v46.07h39.16c21.59,0,39.16-17.57,39.16-39.16V26.34h-5.79L108.81,0l16.41,26.34Z"></path>
                                <path class="cls-3" d="m67.04,46.21h-16.02v59.46h8.01c2.76,0,5.45-.35,8.01-.99v-58.47Z"></path>
                                <path class="cls-3" d="m90.77,33.56h-16.03v68.14c7.57-4.07,13.39-10.99,16.03-19.31v-48.83Z"></path>
                                </g>
                            </g>
                            <g>
                                <g>
                                <path class="cls-1" d="m195.69,38.68v-5.58h49.35v5.58h-21.57v62.48h-6.21v-62.48h-21.57Z"></path>
                                <path class="cls-1" d="m283.29,81.65v-31.54h5.92v51.05h-5.92v-8.47h-.47c-1.2,2.61-3.09,4.79-5.68,6.53-2.59,1.74-5.76,2.61-9.5,2.61-3.26,0-6.15-.71-8.67-2.14-2.53-1.43-4.51-3.53-5.95-6.3-1.44-2.77-2.16-6.17-2.16-10.2v-33.07h5.92v32.7c0,3.99,1.17,7.2,3.52,9.62,2.35,2.43,5.36,3.64,9.04,3.64,2.3,0,4.52-.55,6.65-1.66,2.13-1.11,3.88-2.73,5.25-4.87,1.37-2.14,2.06-4.77,2.06-7.89Z"></path>
                                <path class="cls-1" d="m306.05,101.16V33.09h5.95v26.75h.53c.66-1.53,1.62-3.1,2.87-4.72,1.25-1.62,2.9-2.97,4.95-4.07,2.05-1.1,4.64-1.65,7.76-1.65,4.19,0,7.86,1.1,11.03,3.31,3.17,2.21,5.64,5.28,7.41,9.24,1.77,3.95,2.66,8.56,2.66,13.81s-.88,9.89-2.64,13.86c-1.76,3.97-4.22,7.06-7.38,9.27-3.16,2.22-6.82,3.32-10.98,3.32-3.1,0-5.69-.55-7.76-1.66-2.07-1.11-3.74-2.48-5-4.1-1.26-1.63-2.24-3.22-2.92-4.77h-.73v9.47h-5.75Zm5.85-25.49c0,4.12.61,7.77,1.84,10.95,1.23,3.18,3,5.67,5.32,7.48,2.31,1.81,5.12,2.71,8.42,2.71s6.26-.94,8.61-2.81c2.35-1.87,4.13-4.4,5.35-7.59,1.22-3.19,1.83-6.77,1.83-10.73s-.6-7.46-1.81-10.6c-1.21-3.15-2.99-5.64-5.33-7.49-2.35-1.85-5.23-2.78-8.64-2.78s-6.14.89-8.46,2.68c-2.32,1.78-4.08,4.24-5.3,7.38-1.22,3.14-1.83,6.74-1.83,10.82Z"></path>
                                <path class="cls-1" d="m382.86,102.22c-4.76,0-8.88-1.11-12.36-3.34-3.48-2.23-6.16-5.32-8.06-9.27s-2.84-8.51-2.84-13.68.95-9.71,2.84-13.71c1.89-4,4.52-7.13,7.88-9.41,3.36-2.27,7.24-3.41,11.65-3.41,2.77,0,5.44.5,8.01,1.51,2.57,1.01,4.88,2.55,6.93,4.62,2.05,2.07,3.67,4.68,4.87,7.81,1.2,3.14,1.79,6.84,1.79,11.12v2.92h-39.88v-5.22h33.83c0-3.28-.66-6.23-1.98-8.86-1.32-2.63-3.15-4.7-5.48-6.23-2.34-1.53-5.04-2.29-8.09-2.29-3.23,0-6.08.86-8.54,2.59-2.46,1.73-4.38,4.01-5.77,6.85-1.38,2.84-2.09,5.94-2.11,9.31v3.12c0,4.05.7,7.59,2.11,10.62,1.41,3.02,3.41,5.37,6,7.03,2.59,1.66,5.66,2.49,9.21,2.49,2.41,0,4.54-.38,6.36-1.13,1.83-.75,3.37-1.77,4.62-3.04,1.25-1.27,2.2-2.68,2.84-4.2l5.62,1.83c-.78,2.15-2.04,4.13-3.81,5.95-1.76,1.82-3.95,3.27-6.58,4.37-2.63,1.1-5.65,1.65-9.06,1.65Z"></path>
                                <path class="cls-1" d="m412.07,44.96v-11.86h55.9v11.86h-20.84v56.2h-14.22v-56.2h-20.84Z"></path>
                                <path class="cls-1" d="m471.62,101.16v-51.05h13.73v8.91h.53c.93-3.17,2.49-5.57,4.69-7.2,2.19-1.63,4.72-2.44,7.58-2.44.71,0,1.47.04,2.29.13.82.09,1.54.21,2.16.37v12.56c-.66-.2-1.58-.38-2.76-.53-1.17-.15-2.25-.23-3.22-.23-2.08,0-3.94.45-5.57,1.35-1.63.9-2.91,2.14-3.86,3.74-.94,1.6-1.41,3.43-1.41,5.52v28.88h-14.16Z"></path>
                                <path class="cls-1" d="m523,102.12c-3.26,0-6.16-.57-8.71-1.71-2.55-1.14-4.56-2.84-6.03-5.08-1.47-2.25-2.21-5.06-2.21-8.42,0-2.84.52-5.22,1.56-7.15,1.04-1.93,2.46-3.48,4.25-4.65,1.79-1.17,3.84-2.06,6.13-2.66,2.29-.6,4.7-1.02,7.23-1.26,2.97-.31,5.36-.6,7.18-.88,1.82-.28,3.13-.69,3.95-1.25.82-.55,1.23-1.37,1.23-2.46v-.2c0-2.1-.66-3.73-1.98-4.89-1.32-1.15-3.19-1.73-5.6-1.73-2.55,0-4.58.56-6.08,1.68-1.51,1.12-2.5,2.52-2.99,4.2l-13.09-1.06c.66-3.1,1.97-5.79,3.92-8.06,1.95-2.27,4.47-4.02,7.56-5.25,3.09-1.23,6.67-1.84,10.75-1.84,2.84,0,5.55.33,8.16,1,2.6.66,4.92,1.7,6.95,3.09,2.03,1.4,3.63,3.19,4.8,5.37,1.17,2.18,1.76,4.79,1.76,7.83v34.43h-13.43v-7.08h-.4c-.82,1.6-1.92,3-3.29,4.2-1.37,1.21-3.02,2.15-4.95,2.82-1.93.68-4.15,1.01-6.68,1.01Zm4.05-9.77c2.08,0,3.92-.42,5.52-1.25s2.85-1.96,3.76-3.37c.91-1.42,1.36-3.02,1.36-4.82v-5.42c-.44.29-1.05.55-1.81.78s-1.62.44-2.58.63c-.95.19-1.91.35-2.86.5-.95.14-1.82.27-2.59.38-1.66.24-3.11.63-4.35,1.16-1.24.53-2.21,1.25-2.89,2.14-.69.9-1.03,2.01-1.03,3.34,0,1.93.7,3.4,2.11,4.4,1.41,1.01,3.2,1.51,5.37,1.51Z"></path>
                                <path class="cls-1" d="m580.03,50.11h10.17v10.63h-10.17v40.41h-14.12v-40.41h-7.21v-10.63h7.21v-3.69c0-3.7.72-6.77,2.18-9.21,1.45-2.44,3.44-4.26,5.97-5.48,2.53-1.22,5.4-1.83,8.61-1.83,2.17,0,4.15.17,5.97.5,1.8.33,3.15.63,4.04.9l-2.53,10.63c-.55-.18-1.23-.34-2.04-.5s-1.64-.23-2.48-.23c-2.08,0-3.53.48-4.35,1.45-.83.96-1.23,2.31-1.23,4.04v3.42Z"></path>
                                <path class="cls-1" d="m616.12,50.11h10.17v10.63h-10.17v40.41h-14.12v-40.41h-7.21v-10.63h7.21v-3.69c0-3.7.72-6.77,2.18-9.21,1.45-2.44,3.44-4.26,5.97-5.48,2.53-1.22,5.4-1.83,8.61-1.83,2.17,0,4.15.17,5.97.5,1.8.33,3.15.63,4.04.9l-2.53,10.63c-.55-.18-1.23-.34-2.04-.5-.81-.15-1.64-.23-2.48-.23-2.08,0-3.53.48-4.35,1.45-.83.96-1.23,2.31-1.23,4.04v3.42Z"></path>
                                <path class="cls-1" d="m642.05,43.53c-2.11,0-3.9-.7-5.4-2.09-1.5-1.4-2.24-3.08-2.24-5.05s.75-3.66,2.26-5.07c1.51-1.41,3.3-2.11,5.38-2.11s3.91.7,5.4,2.09c1.5,1.4,2.24,3.08,2.24,5.05s-.75,3.66-2.24,5.07c-1.5,1.41-3.3,2.11-5.4,2.11Zm-7.11,57.63v-51.05h14.16v51.05h-14.16Z"></path>
                                <path class="cls-1" d="m683.46,102.15c-5.23,0-9.72-1.11-13.48-3.34-3.76-2.23-6.64-5.32-8.64-9.29-2.01-3.97-3.01-8.53-3.01-13.69s1.01-9.82,3.04-13.78c2.03-3.95,4.91-7.05,8.66-9.27,3.74-2.23,8.2-3.34,13.36-3.34,4.45,0,8.35.81,11.7,2.43,3.35,1.62,5.99,3.89,7.94,6.81,1.95,2.92,3.02,6.36,3.22,10.3h-13.36c-.38-2.55-1.37-4.6-2.97-6.16-1.61-1.56-3.71-2.34-6.3-2.34-2.19,0-4.1.59-5.73,1.78-1.63,1.19-2.9,2.91-3.81,5.17s-1.36,5-1.36,8.21.45,6.03,1.35,8.31c.9,2.28,2.17,4.02,3.81,5.22,1.64,1.2,3.56,1.79,5.75,1.79,1.62,0,3.07-.33,4.37-1,1.3-.66,2.37-1.63,3.22-2.91.85-1.27,1.41-2.81,1.68-4.6h13.36c-.22,3.9-1.28,7.33-3.17,10.29-1.89,2.96-4.5,5.27-7.83,6.93-3.32,1.66-7.26,2.49-11.8,2.49Z"></path>
                                </g>
                                <g>
                                <path class="cls-3" d="m800,37.72c0,4.31-3.5,7.81-7.82,7.81s-7.81-3.5-7.81-7.81,3.5-7.82,7.81-7.82,7.82,3.5,7.82,7.82Z"></path>
                                <path class="cls-2" d="m736.11,85.52c-.09,0-.18,0-.27-.01-.09,0-.18-.02-.26-.03.18.01.35.05.53.05Z"></path>
                                <path class="cls-2" d="m784.37,50.18v32.89c0,1.31-1.05,2.37-2.35,2.43-.03,0-.07.02-.11.02h-.37l-18.68-41.36-3.1-6.86c-2.03-4.5-6.52-7.4-11.47-7.4s-9.42,2.9-11.46,7.4l-11.96,26.47-10.4,23.03c-3.04,6.73,1.88,14.36,9.28,14.36h66.01c5.65,0,10.25-4.58,10.25-10.24v-40.74h-15.63Zm-48.25,35.34c-.09,0-.18,0-.27-.01-.09,0-.18-.02-.26-.03-2.25-.33-3.65-2.75-2.67-4.91l12.93-28.64c.95-2.1,3.93-2.1,4.89,0l15.17,33.59h-29.79Z"></path>
                                </g>
                            </g>
                            </g>
                        </g>
                        </svg>
                </div>
                <div class="col-12 col-md-12 mt20 mt-md30">
                    <div class="f-22 f-md-42 text-center white-clr lh130 w600">
                        The Pro Features Top Marketers Demand & Need Are Now Available At A Fraction Of The Cost
                    </div>
                </div>
                <div class="col-12 col-md-8 mx-auto mt20 mt-md50">
                    <img src="assets/images/product-img.webp" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-md-10 mx-auto col-12 mt20 mt-md50">
                    <div class="f-18 f-md-36 text-center probtn1">
                        <a href="#buynow" class="text-center">Get Instant Access to TubeTrafficAi Pro</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--4. Fourth Section End -->
    <div class="white-section">
        <div class="container ">
            <div class="row">
                <div class="col-12 col-md-12">
                    <div class="f-30 f-md-50 lh130 w500 text-center black-clr">
                        Here’s What You’re Getting with <br class="d-none d-md-block"> <span class="w700">This Unfair Advantage Upgrade</span>
                    </div>
                </div>
                <div class="col-12 col-md-12 mt20 mt-md50 ">
                    <div class="f-24 f-md-30 w600 lh130 text-center">
                        Go Limitless As You’re Getting Unlimited Everything
                    </div>
                    <div class="col-12 col-md-12 mt-md50 mt20">
                        <div class="row">
                            <div class="col-12 col-md-3">
                                <img src="assets/images/channel.png" class="img-fluid d-block mx-auto">
                            </div>
                            <div class="col-12 col-md-3 mt20 mt-md0 ">
                                <img src="assets/images/video.png" class="img-fluid d-block mx-auto">
                            </div>
                            <div class="col-12 col-md-3 mt20 mt-md0">
                                <img src="assets/images/custom-domains.png" class="img-fluid d-block mx-auto">
                            </div>
                            <div class="col-12 col-md-3 mt20 mt-md0">
                                <img src="assets/images/storage.png" class="img-fluid d-block mx-auto">
                            </div>
                        </div>
                    </div>
					<div class="col-12 col-md-12 mt-md50 mt20">
                        <div class="row">
                            <div class="col-12 col-md-3">
                                <img src="assets/images/bandwidth.png" class="img-fluid d-block mx-auto">
                            </div>
                            <div class="col-12 col-md-3 mt20 mt-md0 ">
                                <img src="assets/images/traffic.png" class="img-fluid d-block mx-auto">
                            </div>
                            <div class="col-12 col-md-3 mt20 mt-md0">
                                <img src="assets/images/affiliate-offers.png" class="img-fluid d-block mx-auto">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 f-20 f-md-22 w400 lh130 mt20 mt-md50">
                    No more worrying about anything and focus on growing your business leaps and bounds. <br><br> You’ll have NO LIMITS to what you can do and take your business to the next level. TubeTrafficAi Pro upgrade enables you stand out of the crowd,
                    and the best part is that you can <span class="w600">go unlimited for one-time price but ONLY for limited time.</span>
                </div>
            </div>
        </div>
    </div>
	
	<div class="grey-section">
        <div class="container ">
            <div class="row">
                <div class="col-12">
                    <img src="assets/images/1.png" class="img-responsive">
                    <div class="f-24 f-md-36 w600 lh130 mt20 mt-md0 black-clr text-capitalize">
						Create Unlimited Channels in ANY NICHE For Yourself and Clients
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md40">
                    <img src="assets/images/f1.webp" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 f-20 f-md-22 w400 lh130 mt20 mt-md40 text-left">
                    Now, establish yourself as an authority in multiple niches or topics by creating unlimited video channels to get maximum traffic, lead & sales hands down.<br>
					Create channels on all your favorite niches...
					<br><br>
					With this feature, you can create AS MANY CHANNELS AS YOU WANT without any limitations. It’s certainly awesome to be able to create unlimited video channels at the push of a button, build your list fast and easily curate more content - all with minimal work.

                </div>
            </div>
        </div>
    </div>
	
	<div class="white-section">
        <div class="container ">
            <div class="row">
                <div class="col-12">
                    <img src="assets/images/2.png" class="img-responsive">
                    <div class="f-24 f-md-36 w600 lh130 mt20 mt-md0 black-clr text-capitalize">
                        Create Unlimited Projects Inside Your Business & Get Maximum Audience Hooked
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md40">
                    <img src="assets/images/f2.webp" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 f-20 f-md-22 w400 lh130 mt20 mt-md40 text-left">
                    When you upgrade to Pro, you can create UNLIMITED projects inside your business and get viral targeted traffic to boost your profits manifold.
                </div>
            </div>
        </div>
    </div>
	
	<div class="grey-section">
        <div class="container ">
            <div class="row">
                <div class="col-12">
                    <img src="assets/images/3.png" class="img-responsive">
                    <div class="f-24 f-md-36 w600 lh130 mt20 mt-md0 black-clr text-capitalize">
                        Add Unlimited Videos & Show MORE Content On 100s Of Topic Or Keywords
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md40">
                    <img src="assets/images/f3.webp" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 f-20 f-md-22 w400 lh130 mt20 mt-md40 text-left">
                    Adding Unlimited Videos to get your audience hooked got faster & easier. Now you too can showcase MORE content on 100s of topic or keywords to reach out to wider audience hands-free.
                </div>
            </div>
        </div>
    </div>
	
	<div class="white-section">
        <div class="container ">
            <div class="row">
                <div class="col-12">
                    <img src="assets/images/4.png" class="img-responsive">
                    <div class="f-24 f-md-36 w600 lh130 mt20 mt-md0 black-clr text-capitalize">
                        Add Unlimited Custom Domains To Build Your Authority & Credibility
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md40">
                    <img src="assets/images/f4.webp" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 f-20 f-md-22 w400 lh130 mt20 mt-md40 text-left">
                    Build your authority and credibility by showing videos on your own domain with your own LOGO using our unlimited custom domain feature. It also gives you better exposure in search engines.
                </div>
            </div>
        </div>
    </div>
	
	<div class="grey-section">
        <div class="container ">
            <div class="row">
                <div class="col-12">
                    <img src="assets/images/5.png" class="img-responsive">
                    <div class="f-24 f-md-36 w600 lh130 mt20 mt-md0 black-clr text-capitalize">
                        Get Unlimited Storage To Get Your Media Content Delivered Faster
                    </div>
                </div>
                <div class="col-12 col-md-6 mx-auto mt20 mt-md40">
                    <img src="assets/images/f5.png" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 f-20 f-md-22 w400 lh130 mt20 mt-md40 text-left">
                    Gone are the days when you had to pay extra for getting unlimited storage.  TubeTrafficAi Pro helps to go beyond the extra mile by getting unlimited storage to get your media content delivered faster & easier.
                </div>
            </div>
        </div>
    </div>
	
	<div class="white-section">
        <div class="container ">
            <div class="row">
                <div class="col-12">
                    <img src="assets/images/6.png" class="img-responsive">
                    <div class="f-24 f-md-36 w600 lh130 mt20 mt-md0 black-clr text-capitalize">
                        Get Unlimited Bandwidth To Give Best User Experience
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md40">
                    <img src="assets/images/bandwidth-img.webp" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 f-20 f-md-22 w400 lh130 mt20 mt-md40 text-left">
                    We mean every word when we say LIMITLESS. So with the TubeTrafficAi Pro edition, you get <span class="w600">UNLIMITED bandwidth to give best user experience without getting worried of any limitations of video size, rich media content and content uploading and downloading bandwidth.</span>
                </div>
            </div>
        </div>
    </div>
	
	<div class="grey-section">
        <div class="container ">
            <div class="row">
                <div class="col-12">
                    <img src="assets/images/7.png" class="img-responsive">
                    <div class="f-24 f-md-36 w600 lh130 mt20 mt-md0 black-clr text-capitalize">
                        Drive Unlimited Visitors And Leads For Your Offers
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md40">
                    <img src="assets/images/f7.webp" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 f-20 f-md-22 w400 lh130 mt20 mt-md40 text-left">
                    Driving UNLIMITED visitors on your video pages is a reality now. TubeTrafficAi Pro gives you the power to drive unlimited visitors & make the most from them in a hands free manner.
                </div>
            </div>
        </div>
    </div>
	
	<div class="white-section">
        <div class="container ">
            <div class="row">
                <div class="col-12">
                    <img src="assets/images/8.png" class="img-responsive">
                    <div class="f-24 f-md-36 w600 lh130 mt20 mt-md0 black-clr text-capitalize">
                        Create Unlimited Ads & Get Maximum Affiliate Commissions
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md40">
                    <img src="assets/images/f8.webp" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 f-20 f-md-22 w400 lh130 mt20 mt-md40 text-left">
                    TubeTrafficAi Pro also enables business owners to maximize their profits & affiliate commissions by creating unlimited ads & driving maximum affiliate commissions’ hassle free.
                </div>
            </div>
        </div>
    </div>
	
    <div class="grey-section">
        <div class="container ">
            <div class="row">
                <div class="col-12">
                    <img src="assets/images/9.png" class="img-responsive">
                    <div class="f-24 f-md-36 w600 lh130 mt20 mt-md0 black-clr text-capitalize">
                        Get 5 stunning and ready-to-use fully customizable video players
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto  mt20 mt-md40">
                    <img src="assets/images/f9.webp" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 f-20 f-md-22 w400 lh130 mt20 mt-md40">
                    With this edition, we are giving 5 fully customizable video players, it can show your videos in the most stunning way possible to get best results.<br><br> Click on the customization option, now insert the name of your player, and
                    select player which you want to customize.
                    <br><br> Appearance of video player can be changed by changing the player’s color and player’s text color. There are many more controls are available like center play button, bottom play button, share option, volume controls etc. to
                    make your player beautiful.
                    <br><br> You can change the video actions, by choosing any one from start action and end action. Now click on the save and apply button to go ahead or hit the reset tab to make more changes.
                </div>
            </div>
        </div>
    </div>
	
	 <div class="white-section">
        <div class="container ">
            <div class="row">
                <div class="col-12 col-md-12">
                    <img src="assets/images/10.png" class="img-responsive">
                    <div class="f-24 f-md-36 w600 lh130 mt20 mt-md0 black-clr text-capitalize">
                        Customize your player with 8 attractive and eye-catchy frames
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto  mt20 mt-md40">
                    <img src="assets/images/f10.webp" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 mt20 mt-md40">
                    <div class="f-20 f-md-22 w400 lh130">
                        Make your videos look enticing and attention grabbing with 8 eye-pleasing video frames that are available only for the customers of this Pro edition. You can pick any frame from the list and your video will be played in the same video frame that you chose.
                        <br>
                        <br> If you choose mobile frame or any other desired screen, then your video will be displayed in the same screen as I am showing you in the video. This is something that will intensify your user engagement level altogether.
                    </div>
                </div>
            </div>
        </div>
    </div>
	<div class="grey-section">
        <div class="container ">
            <div class="row">
                <div class="col-12 ">
                    <img src="assets/images/11.png" class="img-responsive">
                    <div class="f-24 f-md-36 w600 lh130 mt20 mt-md0 black-clr text-capitalize">
						Get hordes of traffic by getting your videos forcefully shared on top social media platforms.
					</div>
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md40">
                    <img src="assets/images/f11.webp" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 col-md-12 mt20 mt-md40">
                    <div class="f-20 f-md-22 w400 lh130">
                        With this feature, you can lock a video anywhere & force the visitors to share it on their social media to watch the FULL video. That way, it can make your video VIRAL and MULTIPLY the traffic with NO EXTRA effort.
<br><br>
It hardly takes 2 minutes in choosing a template and setting up a Social app. You can set Video URL or even your affiliate link to be shared directly on the wall of visitor’s social media. Now you can see the power.
<br><br>
You can do a lot more with it, decision is yours. If anyone find your video useful and embeds the video on their pages, now it works even better as a viral traffic machine. Our social app work right inside the Video and still share your main URL that you set to be shared when someone takes action even on this embedded video.
                    </div>
                </div>
            </div>
        </div>
    </div>
	
	<div class="white-section">
        <div class="container ">
            <div class="row">
                <div class="col-12 col-md-12">
                    <img src="assets/images/12.png" class="img-responsive">
                    <div class="f-24 f-md-36 w600 lh130 mt20 mt-md0 black-clr text-capitalize">
                        Get 50 EXTRA PROVEN Converting Lead And Promo Templates
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md40">
                    <img src="assets/images/f12.webp" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 mt20 mt-md40 ">
                    <div class="f-20 f-md-22 w400 lh130">
                        Oh yes, I am not kidding here. To multiply the benefits of your purchase, we’re offering 50 extra attractive and eye-catchy templates to grab more leads for your offers. No coding, no imagining what looks good and what works, we already did the hard work
                        for you.
                    </div>
                </div>
                <div class="col-12 mt20 mt-md40 ">
                    <div class="f-20 f-md-22 w400 lh130 black-clr">
                        You just choose a template, edit the call to action text and image to create a beautiful, crispy and effective Lead app and promo app in seconds. You can set the time at which you want to show them as well.
                        <br>
                        <br> Simply, lock the video and allow it to be watched only when someone fills in the form or allow it to be skipped. We've never offered features like this before, and now this is your only chance to get them.
                    </div>
                </div>
            </div>
        </div>
    </div>
	
	<div class="grey-section">
        <div class="container ">
            <div class="row">
                <div class="col-12 col-md-12">
                    <img src="assets/images/13.png" class="img-responsive">
                    <div class="f-24 f-md-36 w600 lh130 mt20 mt-md0 black-clr text-capitalize">
                        Never Offered Before Video Personalization To Boost Conversions
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto  mt20 mt-md40">
                    <img src="assets/images/f13.webp" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 col-md-12 mt20 mt-md40">
                    <div class="f-20 f-md-22 w400 lh130">
                      This is something that your customers will love. Now you too can show viewer's name, e-mail address & location inside video on any particular duration.<br><br>

Just imagine how beautiful it would look when you get an email with your own name, location etc. on it.
                    </div>
                </div>
            </div>
        </div>
    </div>
   <div class="white-section">
        <div class="container ">
            <div class="row">
                <div class="col-12 col-md-12">
                    <img src="assets/images/14.png" class="img-responsive">
                    <div class="f-24 f-md-36 w600 lh130 mt20 mt-md0 black-clr text-capitalize">
                        Create a personalized e-mail link of the video
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md40">
                    <img src="assets/images/f14.webp" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 col-md-12 mt20 mt-md40">
                    <div class="f-20 f-md-22 w400 lh130">
                        Wanna create a personalized link of the video & share it inside your email to your audience, TubeTrafficAi Pro edition has got your back.
                        <br><br> Just use this state of the art technology, & be ready to share your videos to audience scattered globally with zero hassles.
                    </div>
                </div>
            </div>
        </div>
    </div>
   
	<div class="grey-section">
        <div class="container ">
            <div class="row">
                <div class="col-12 col-md-12">
                    <img src="assets/images/15.png" class="img-responsive">
                    <div class="f-24 f-md-36 w600 lh130 mt20 mt-md0 black-clr text-capitalize">
                        Viral Page Customization Feature To Enhance Its Visual Appeal & Resonate With Your Brand
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md40">
                    <img src="assets/images/f15.webp" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 col-md-12 mt20 mt-md40">
                    <div class="f-20 f-md-22 w400 lh130">
					With this feature, you can give your video page a unique look & feel by managing various options like page layout, sharing option, related videos etc.
					<br><br>
					You can also insert Google Ads or your own banner ads to boost monetization.
                    </div>
                </div>
            </div>
        </div>
    </div>
	
	<div class="white-section">
        <div class="container ">
            <div class="row">
                <div class="col-12 col-md-12">
                    <img src="assets/images/16.png" class="img-responsive">
                    <div class="f-24 f-md-36 w600 lh130 mt20 mt-md0 black-clr text-capitalize">
                        Capture Leads Of Your Visitors When They Interact On Your Videos
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md40">
                    <img src="assets/images/f16.webp" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 col-md-12 mt20 mt-md40">
                    <div class="f-20 f-md-22 w400 lh130">
                        This is something that’s gonna take the industry by the storm. No hard work, no contact management system needs to be resorted. With this feature, you will get all your subscribers details in your TubeTrafficAi as well as in autoresponder. Whenever your viewer
                        interacts on your videos, they will have to enter their credentials on your video page first and that will get you their name, email address and also show what action they have taken.
                    </div>
                </div>
            </div>
        </div>
    </div>
	
	<div class="grey-section">
        <div class="container ">
            <div class="row">
                <div class="col-12 col-md-12">
                    <img src="assets/images/17.png" class="img-responsive">
                    <div class="f-24 f-md-36 w600 lh130 mt20 mt-md0 black-clr text-capitalize">
                        Maximize ROI From Your Leads With Webinar Integration
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md40">
                    <img src="assets/images/f17.webp" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 col-md-12 mt20 mt-md40">
                    <div class="f-20 f-md-22 w400 lh130">
                        With Pro edition, we are making it easier for you to integrate a webinar like a pro. So, when someone fill the form, they automatically will get registered for webinar & you can get maximum ROI.
                        <br><br> For webinar integration, just put API key inside the box and you are ready to get more registrants without losing a single lead.
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- <div class="white-section">
        <div class="container ">
            <div class="row">
                <div class="col-12">
                    <img src="assets/images/8.png" class="img-responsive">
                    <div class="f-24 f-md-36 w600 lh130 mt20 mt-md0 black-clr text-capitalize">
                        Create Unlimited Business
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md40">
                    <img src="assets/images/f1.png" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 f-20 f-md-22 w400 lh130 mt20 mt-md40 text-left">
                    When you upgrade to Pro, you can create UNLIMITED channels and get viral targeted traffic to boost your profits manifold.<br><br> Create channels on all your favorite niches...<br><br> With this feature, you can create AS MANY CHANNELS
                    AS YOU WANT without any limitations. It’s certainly awesome to be able to create unlimited video channels at the push of a button, build your list fast and easily curate more content - all with minimal work.
                </div>
            </div>
        </div>
    </div>
  
    <div class="grey-section">
        <div class="container ">
            <div class="row">
                <div class="col-12">
                    <img src="assets/images/9.png" class="img-responsive">
                    <div class="f-24 f-md-36 w600 lh130 mt20 mt-md0 black-clr text-capitalize">
                        Create Unlimited Video Channels
                    </div>
                </div>
                <div class="col-12 mx-auto mt20 mt-md40">
                    <img src="assets/images/f3.png" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12  f-20 f-md-22 w400 lh130 mt20 mt-md40">
                    When you upgrade to Pro, you can create UNLIMITED channels and get viral targeted traffic to boost your profits manifold.<br><br> Create channels on all your favorite niches...<br><br> With this feature, you can create AS MANY CHANNELS
                    AS YOU WANT without any limitations. It’s certainly awesome to be able to create unlimited video channels at the push of a button, build your list fast and easily curate more content - all with minimal work.
                </div>
            </div>
        </div>
    </div>
   
    <div class="white-section">
        <div class="container ">
            <div class="row">
                <div class="col-12">
                    <img src="assets/images/10.png" class="img-responsive">
                    <div class="f-24 f-md-36 w600 lh130 mt20 mt-md0 black-clr text-capitalize">
                        Add Unlimited Videos In Channels
                    </div>
                </div>
                <div class="col-12 mx-auto mt20 mt-md40">
                    <img src="assets/images/f3.png" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12  f-20 f-md-22 w400 lh130 mt20 mt-md40">
                    When you upgrade to Pro, you can create UNLIMITED channels and get viral targeted traffic to boost your profits manifold.<br><br> Create channels on all your favorite niches...<br><br> With this feature, you can create AS MANY CHANNELS
                    AS YOU WANT without any limitations. It’s certainly awesome to be able to create unlimited video channels at the push of a button, build your list fast and easily curate more content - all with minimal work.
                </div>
            </div>
        </div>
    </div>
     -->
    <!-- <div class="grey-section">
        <div class="container ">
            <div class="row">
                <div class="col-12">
                    <img src="assets/images/11.png" class="img-responsive">
                    <div class="f-24 f-md-36 w600 lh130 mt20 mt-md0 black-clr text-capitalize">
                        Get UNLIMITED Bandwidth To Give Best User Experience
                    </div>
                </div>
                <div class="col-12 mx-auto  mt20 mt-md40">
                    <img src="assets/images/f4.png" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 f-20 f-md-22 w400 lh130 mt20 mt-md40">
                    We mean every word when we say LIMITLESS. So with the TubeTrafficAi Pro edition, you get <span class="w600">UNLIMITED bandwidth to give best user experience without getting worried of any limitations of video size, rich media content and content uploading and downloading bandwidth. </span>
                </div>
            </div>
        </div>
    </div> -->
  
    
    
    
    <!-- <div class="white-section">
        <div class="container ">
            <div class="row">
                <div class="col-12 ">
                    <img src="assets/images/16.png" class="img-responsive">
                    <div class="f-24 f-md-36 w600 lh130 mt20 mt-md0 black-clr text-capitalize">
                        Unlock our Advanced Advertisement technology for better monetization with Video Ads, Image Ads, Text ads or Even Show an Html Page Right inside videos
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md40">
                    <img src="assets/images/f7.png" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 col-md-12 mt20 mt-md40">
                    <div class="f-20 f-md-22 w400 lh130">
                        This is something that’s gonna take the industry by the storm. No hard work, no contact management system needs to be resorted. With this feature, you will get all your subscribers details in your TubeTrafficAi as well as in autoresponder. Whenever your viewer
                        interacts on your videos, they will have to enter their credentials on your video page first and that will get you their name, email address and also show what action they have taken.
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <!-- <div class="grey-section">
        <div class="container ">
            <div class="row">
                <div class="col-12 col-md-12">
                    <img src="assets/images/17.png" class="img-responsive">
                    <div class="f-24 f-md-36 w600 lh130 mt20 mt-md0 black-clr text-capitalize">
                        Make more money using Google AdSense or Affiliate Banner on your channel and video pages
                    </div>
                </div>
                <div class="col-12 col-md-12 mt20 mt-md40">
                    <img src="assets/images/f18.png" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 col-md-12 mt20 mt-md40">
                    <div class="f-20 f-md-22 w400 lh130 black-clr">
                        With this amazing add on, you can attract more and more customers by inserting Google AdSense and banner ads on your channel/video pages. Everything is already fixed you don’t need to sweat out for anything.
                        <br><br> For inserting google ads you just need to insert the required link inside the box. For putting banner inside the video, you have to insert the URL of the place where you want your customer to get redirected.
                        <br><br> Page layout setting is another great feature available to make changes in the look of the video page by adding tabs like share tab, Email Tab, Embed tab, comment tab and many more. Additionally, you can make these pages
                        public or password protected if you want.
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    
    <!-- <div class="white-section">
        <div class="container ">
            <div class="row">
                <div class="col-12 col-md-12">
                    <img src="assets/images/18.png" class="img-responsive">
                    <div class="f-24 f-md-36 w600 lh130 mt20 mt-md0 black-clr text-capitalize">
                        Maximize ROI From Your Leads With Webinar Integration
                    </div>
                </div>
                <div class="col-12 mt20 mt-md40">
                    <img src="assets/images/f17.png" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 col-md-12 mt20 mt-md40">
                    <div class="f-20 f-md-22 w400 lh130">
                        With Pro edition, we are making it easier for you to integrate a webinar like a pro. So, when someone fill the form, they automatically will get registered for webinar & you can get maximum ROI.
                        <br><br> For webinar integration, just put API key inside the box and you are ready to get more registrants without losing a single lead.
                    </div>
                </div>
            </div>
        </div>
    </div> -->
	
	<div class="white-section">
        <div class="container ">
            <div class="row">
                <div class="col-12 col-md-12">
                    <img src="assets/images/18.png" class="img-responsive">
                    <div class="f-24 f-md-36 w600 lh130 mt20 mt-md0 black-clr text-capitalize">
                        Unlimited Team Management To Let Your Team Handle All The Manual Work
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md40">
                    <img src="assets/images/team-member.png" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 col-md-12 mt20 mt-md40">
                    <div class="f-20 f-md-22 w400 lh130">
                        Create custom roles, assign privileges and manage your projects and even your client’s projects simply fast and easy for Unlimited Team members.
                    </div>
                </div>
            </div>
        </div>
    </div>
	
    <div class="grey-section">
        <div class="container ">
            <div class="row">
                <div class="col-12 col-md-12 ">
                    <img src="assets/images/19.png" class="img-responsive">
                    <div class="f-24 f-md-36 w600 lh130 mt20 mt-md0 black-clr text-capitalize">
                        Get All These Benefits At An Unparalleled Price
                    </div>
                </div>
                <div class="col-12 mt-md50">
                    <div class="row d-flex align-items-center">
                        <div class="col-12 col-md-6 order-md-2 mt20 mt-md0">
                            <img src="assets/images/f19.png" class="img-fluid d-block mx-auto">
                        </div>
                        <div class="col-12 col-md-6 order-md-1 mt20 mt-md0">
                            <div class="f-20 f-md-22 w400 lh130">
                                Ultimately you are saving thousands of dollars monthly with us, & every dollar that you save, adds to your profits. By multiplying engagement & opt-ins, you’re not only saving money,<span class="w600">you’re also taking your business to the next level.</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--7. Seventh Section Starts -->
    <div class="goodreason-section">
        <div class="container ">
            <div class="row">
                <div class="col-12 col-md-12">
                    <div class="f-30 f-md-50 w500 lh130 text-center">
                        Act Now For <span class="w700">2 Very Good Reasons:</span>
                    </div>
                </div>
                <div class="col-12 mt20 mt-md40">
                    <div class="row">
                        <div class="col-xs-2 col-md-1 pl0 pr10">
                            <div>
                                <img src="assets/images/one.png" class="d-block img-fluid mb-md0 mb15 mx-auto">
                            </div>
                        </div>
                        <div class="col-xs-10 col-md-11">
                            <div class="f-20 f-md-22 w400 lh130">
                                We’re offering TubeTrafficAi Pro at a CUSTOMER’S ONLY discount, so you can grab this <span class="w600">game-changing upgrade for a low ONE-TIME fee.</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 mt20 mt-md40">
                    <div class="row">
                        <div class="col-xs-2 col-md-1 pl0 pr10">
                            <div>
                                <img src="assets/images/two.png" class="d-block img-fluid mb-md0 mb15 mx-auto">
                            </div>
                        </div>
                        <div class="col-xs-10 col-md-11">
                            <div class="f-20 f-md-22 w400 lh130 mt-md10">
                                <span class="w600">Your only chance to get access is right now,</span> on this page. When this page closes, so does this exclusive offer.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--7. Seventh Section End -->
    <!--money back Start -->
    <div class="moneyback-section">
        <div class="container ">
            <div class="row">
                <div class="col-12">
                    <div class="f-30 f-md-50 lh130 w500 text-center white-clr">
                        Let’s Remove All The Risks For You And Secure It With <span class="w700"> <br class="d-none d-md-block"> 30 Days Money Back Guarantee.</span>
                    </div>
                </div>
                <div class="col-12 mt20 mt-md40">
                    <div class="row d-flex align-items-center">
                        <div class="col-12 col-md-4">
                            <img src="assets/images/moneyback.png" class="img-fluid d-block mx-auto">
                        </div>
                        <div class="col-12 col-md-8 mt20 mt-md0">
                            <div class="f-md-24 f-20 lh130 w400 white-clr">
                                You can buy with confidence because if you face any technical issue which we don’t resolve for you, <span class="w500">just raise a ticket within 30 days and we'll refund you everything,</span> down to the last penny.
                                <br><br> <span class="w500">Our team has a 99.21% proven record of solving customer problems and helping them through any issues. </span>
                                <br><br> Guarantees like that don't come along every day, and neither do golden chances like this.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--money back End-->
    <!---Bonus Section Starts-->
    <div class="bonus-section">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12 text-center">
                    <div class="but-design f-28 f-md-50 w700 lh130 text-center white-clr">
                        But That’s Not All
                    </div>
                </div>
                <div class="col-12 mt20 mt-md20">
                    <div class="f-22 f-md-30 w400  text-center lh130">
                        In addition, we have several bonuses for those who want to take action <br class="d-none d-md-block"> today and start profiting from this opportunity.
                    </div>
                </div>
                <!-- bonus1 -->
                <div class="col-12 col-md-12 mt20 mt-md60 d-flex align-items-center flex-wrap">
                    <div class="col-12 col-md-6 ">
                        <div class="f-md-36 f-24 w700 black-clr text-nowrap">
                            Bonus 1
                        </div>
                        <div class="f-24 f-md-30 w500 lh130 mt20 mt-md20 text-left black-clr">
                            Utilizing Facebook For Your Online Business <span class="w400 blue-head"> (Valued at $47)</span>
                        </div>
                        <div class="f-20 f-md-22 w400 lh130 mt10 mt-md10 text-left">
                            Facebook helps people connect easily with your business. It offers some compelling methods for companies that wish to maximize their business reach. When used properly, it gives best results for your business.
                            <br>
                            <br> Keeping this in mind, here’s an exclusive guide will let you how exactly Facebook works and helps small business become bigger than imagined through the power of social networking.
                        </div>
                    </div>
                    <div class="col-12 col-md-6 ">
                        <div class="mt20 mt-md0">
                            <img src="assets/images/bonusbox.webp" class="img-fluid d-block mx-auto">
                        </div>
                    </div>
                </div>
                <!-- bonus2 -->
                <div class="col-12 col-md-12 mt20 mt-md60 d-flex align-items-center flex-wrap">
                    <div class="col-12 col-md-6 order-md-2">
                        <div class="f-md-36 f-24 w700 black-clr text-nowrap">
                            Bonus 2
                        </div>
                        <div class="f-24 f-md-30 w500 lh130 mt20 mt-md20 text-left black-clr">
                            List Building Mojo <span class="w400 blue-head">(Valued at $47)</span>
                        </div>
                        <div class="f-20 f-md-22 w400 lh130 mt10 mt-md10 text-left">
                            Interested in growing your list, discover 100 techniques to inflame your list subscriber like Specific Date technique, JV discount technique, Give it away technique, subscribe-only technique, plenty of ways technique and Opt-in-auction technique etc.
                            This is an enlightening report which disclose a closer look of list building that you need to know.
                        </div>
                    </div>
                    <div class="col-12 col-md-6 order-md-1">
                        <div class="mt20 mt-md0">
                            <img src="assets/images/bonusbox.webp" class="img-fluid d-block mx-auto">
                        </div>
                    </div>
                </div>
                <!-- bonus3 -->
                <div class="col-12 col-md-12 mt20 mt-md60 d-flex align-items-center flex-wrap">
                    <div class="col-12 col-md-6 ">
                        <div class="f-md-36 f-24 w700 black-clr text-nowrap">
                            Bonus 3
                        </div>
                        <div class="f-24 f-md-30 w500 lh130 mt20 mt-md20 text-left black-clr">
                            How To Create A Lead Magnet <span class="w400 blue-head">(Valued at $47)</span>
                        </div>
                        <div class="f-20 f-md-22 w400 lh130 mt10 mt-md10 text-left">
                            Lead Magnet maximizes the number of targeted leads you are getting for an offer. You can also increase the number of prospects and customers you have joining your list.
                            <br>
                            <br> So, to help you make the most from this opportunity, this ultimate guide will help you find step by step tips for creating your lead magnet and make their best use to maximize your profits.
                        </div>
                    </div>
                    <div class="col-12 col-md-6 ">
                        <div class="mt20 mt-md0">
                            <img src="assets/images/bonusbox.webp" class="img-fluid d-block mx-auto">
                        </div>
                    </div>
                </div>
                <!-- bonus4 -->
                <div class="col-12 col-md-12 mt40 mt-md60 d-flex align-items-center flex-wrap">
                    <div class="col-12 col-md-6 order-md-2">
                        <div class="f-md-36 f-24 w700 black-clr text-nowrap">
                            Bonus 4
                        </div>
                        <div class="f-24 f-md-30 w500 lh130 mt20 mt-md20 text-left black-clr">
                            Viral Marketing Stampede <span class="w400 blue-head">(Valued at $47)</span>
                        </div>
                        <div class="f-20 f-md-22 w400 lh130 mt10 mt-md10 text-left">
                            Inside this bonus, you'll get the exact strategies that highly successful marketers have used for years to target masses without spending a fortune. So, you can easily drive hordes of traffic and get maximum exposure for your offerings. Stop thinking
                            and take action now to get best results.
                        </div>
                    </div>
                    <div class="col-12 col-md-6 order-md-1  mt-md30">
                        <div class="mt20 mt-md0">
                            <img src="assets/images/bonusbox.webp" class="img-fluid d-block mx-auto">
                        </div>
                    </div>
                </div>
                <!-- bonus5 -->
                <div class="col-12 col-md-12 mt40 mt-md60 d-flex align-items-center flex-wrap">
                    <div class="col-12 col-md-6 ">
                        <div class="f-md-36 f-24 w700 black-clr text-nowrap">
                            Bonus 5
                        </div>
                        <div class="f-24 f-md-30 w500 lh130 mt20 mt-md20 text-left black-clr">
                            The Newbies Guide to Traffic Generation<br class="d-none d-md-block"> <span class="w400 blue-head">(Valued at $47)</span>
                        </div>
                        <div class="f-20 f-md-22 w400 lh130 mt10 mt-md10 text-left">
                            Online business is rocking & is the best way to reach out to maximum customers without any geographical barrier. And, it’s no surprise that 85% of the people who spend time online also purchase online.
                            <br>
                            <br> Those who are ready to build their own website and make huge profit by drive lots of traffic can get help from this ultimate bonus package. You will surely learn effective methods to drive real traffic to your business
                        </div>
                    </div>
                    <div class="col-12 col-md-6  mt-md30">
                        <div class="mt20 mt-md0">
                            <img src="assets/images/bonusbox.webp" class="img-fluid d-block mx-auto">
                        </div>
                    </div>
                </div>
                <!-- bonus6 -->
                <div class="col-12 col-md-12 mt40 mt-md60 d-flex align-items-center flex-wrap">
                    <div class="col-12 col-md-6 order-md-2">
                        <div class="f-md-36 f-24 w700 black-clr text-nowrap">
                            Bonus 6
                        </div>
                        <div class="f-24 f-md-30 w500 lh130 mt20 mt-md20 text-left black-clr">
                            Targeted Traffic Mastery <span class="w400 blue-head">(Valued at $47)</span>
                        </div>
                        <div class="f-20 f-md-22 w400 lh130 mt10 mt-md10 text-left">
                            The key to success in affiliate business is targeted traffic. Making real money from affiliate program is not that easy. But if you have established your business and have built a good following and repeated traffic, you can make greater profit per month.
                            Quick and targeted traffic can only be acquired via advertisement.
                            <br>
                            <br> Inside this video series, you are about to learn the essential information relevant to setting up a paid advertising campaign
                        </div>
                    </div>
                    <div class="col-12 col-md-6 order-md-1 mt-md30">
                        <div class="mt20 mt-md0">
                            <img src="assets/images/bonusbox.webp" class="img-fluid d-block mx-auto">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Bonus Section Starts -->
    <!--10. Table Section Starts -->
    <div class="table-section" id="buynow">
        <div class="container ">
            <div class="row">
                <div class="col-12 col-md-12">
                    <div class="f-20 f-md-22 w400 lh130 text-center">
                        Yup..! Take action on this while you can as you won’t be seeing this offer again.
                    </div>
                    <div class="f-30 f-md-50 w500 lh130 mt20 text-center black-clr">
                        Today You Can <span class="w700">Get Unrestricted Access to TubeTrafficAi</span> For LESS Than the Price of Just One Month’s Membership.
                    </div>
                </div>
                <div class="col-12 col-md-8 mx-auto mt30 mt-md70">
                    <div class="tablebox2">
                        <div class="tbbg2 text-center">
                            <div class="tbbg2-text mt0 mt-md0 w600 f-md-50 f-30 text-center" editabletype="text" style="z-index:10;">
                                TubeTrafficAi Pro Plan
                            </div>
                        </div>
                        <div class="text-center">
                            <ul class="f-20 f-md-22 w400 lh130 text-center vgreytick mb0 text-capitalize">
                                <li class="odd">
                                    <span class="w600">Break Free & Go Limitless- </span>Create Unlimited Channels & Projects, Add Unlimited Videos, Add Unlimited Custom Domains, Get Unlimited Storage, Get Unlimited Bandwidth, Drive Unlimited Traffic & Leads, Promote Unlimited Affiliate Offers.
                                </li>
                                <li class="even">
                                    Create Unlimited Channels in ANY NICHE For Yourself and Clients
                                </li>
                                <li class="even">
                                    Create Unlimited Projects Inside Your Business & Get Maximum Audience Hooked
                                </li>
                                <li class="even">
                                    Add Unlimited Videos & Show MORE Content On 100s Of Topic Or Keywords
                                </li>
                                <li class="odd">
                                    Add Unlimited Custom Domains To Build Your Authority & Credibility
                                </li>
                                <li class="even">
                                   Getting Unlimited Storage To Get Your Media Content Delivered Faster
                                </li>
                                <li class="even">
                                   Getting Unlimited Bandwidth To Give Best User Experience
                                </li>
                                <li class="odd">
                                    Drive Unlimited Visitors And Leads For Your Offers
                                </li>
                                <li class="odd">
                                    Create Unlimited Ads & Get Maximum Affiliate Commissions
                                </li>
                                <li class="odd">
                                    Get 5 Stunning And Ready-To-Use Fully Customizable Video Players
                                </li>
                                <li class="even">
                                    Customize Your Player With 8 Attractive And Eye-Catchy Frames
                                </li>
                                <li class="odd">
                                   Get hordes of traffic by getting your videos forcefully shared on top social media platforms.
                                </li>
                                <li class="odd">
                                    Get 50 EXTRA PROVEN Converting Lead And Promo Templates
                                </li>
                                <li class="even">
                                   Never Offered Before Video Personalization To Boost Conversions
                                </li>
                                <li class="odd">
                                    Create A Personalized E-Mail Link of The Video
                                </li>
                                <li class="odd">
                                    Viral Page Customization Feature To Enhance Its Visual Appeal & Resonate With Your Brand
                                </li>
                                <li class="even">
                                   Capture Leads Of Your Visitors When They Interact On Your Videos
                                </li>
                                <li class="odd">
                                   Maximize ROI From Your Leads With Webinar Integration
                                </li>
								<li class="even">
                                   Unlimited Team Management To Let Your Team Handle All The Manual Work
                                </li>
                                <li class="odd">
                                   Get All These Benefits At An Unparalleled Price
                                </li>
                            </ul>
                        </div>
                        <div class="myfeatureslast f-md-25 f-16 w400 text-center lh130 hideme relative mb-md15" editabletype="shape" style="opacity: 1; z-index: 8;">
                            <div class="col-12 p0">
                                <div class="row">
                                    <div class="col-md-6 offset-md-3 col-12  mb-md0 mb30 mb-md0">
                                        <div class="f-md-30 f-20 w600 lh130 mb-md20 mb15 text-center">
                                            One Time Plan
                                        </div>
                                        <div>
                                            <a href="https://warriorplus.com/o2/buy/hz2587/l07g46/wc8nsf"><img src="https://warriorplus.com/o2/btn/fn200011000/hz2587/l07g46/364108" alt="TubeTrafficAi Pro One Time Deal" class="img-fluid d-block mx-auto" border="0" /></a>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 mt20 mt-md40 thanks-button text-center">
                    <a class="kapblue f-20 f-md-22 lh130 w400 text-capitalize" href="https://warriorplus.com/o/nothanks/l07g46" target="_blank">
               No Thanks, I Don't Want To Go Unlimited & Get 5x More Traffic, Commissions & Profits Faster & Easier With This Pro Upgrade. Please take me to the next step to get access to my purchase.
                  </a>
                </div>
            </div>
        </div>
    </div>
    <!--10. Table Section End -->
    <!--Footer Section Start -->
    <div class="footer-section">
        <div class="container ">
            <div class="row">
                <div class="col-12 text-center">
                    <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 800 131.43" style="max-height:75px;">
                        <defs>
                            <style>
                            .cls-1 {
                                fill: #fff;
                            }
        
                            .cls-2 {
                                fill: #fd2f15;
                            }
        
                            .cls-3 {
                                fill: #fbbf33;
                            }
                            </style>
                        </defs>
                        <g id="Layer_1-2" data-name="Layer 1">
                            <g>
                            <g>
                                <path class="cls-3" d="m105.61,78.06l40.05,23.12c6.01,3.47,13.51-.87,13.51-7.8v-46.25c0-6.93-7.51-11.27-13.51-7.8l-40.05,23.12c-6.01,3.47-6.01,12.14,0,15.6Z"></path>
                                <g>
                                <rect class="cls-3" x="26.92" y="58.86" width="16.03" height="46.81"></rect>
                                <path class="cls-2" d="m125.22,26.34h-7.16v46.06c0,32.6-26.43,59.03-59.03,59.03H0v-65.94C0,32.89,26.43,6.45,59.03,6.45h38.52l-12.38,19.88h-26.14c-21.59,0-39.16,17.57-39.16,39.15v46.07h39.16c21.59,0,39.16-17.57,39.16-39.16V26.34h-5.79L108.81,0l16.41,26.34Z"></path>
                                <path class="cls-3" d="m67.04,46.21h-16.02v59.46h8.01c2.76,0,5.45-.35,8.01-.99v-58.47Z"></path>
                                <path class="cls-3" d="m90.77,33.56h-16.03v68.14c7.57-4.07,13.39-10.99,16.03-19.31v-48.83Z"></path>
                                </g>
                            </g>
                            <g>
                                <g>
                                <path class="cls-1" d="m195.69,38.68v-5.58h49.35v5.58h-21.57v62.48h-6.21v-62.48h-21.57Z"></path>
                                <path class="cls-1" d="m283.29,81.65v-31.54h5.92v51.05h-5.92v-8.47h-.47c-1.2,2.61-3.09,4.79-5.68,6.53-2.59,1.74-5.76,2.61-9.5,2.61-3.26,0-6.15-.71-8.67-2.14-2.53-1.43-4.51-3.53-5.95-6.3-1.44-2.77-2.16-6.17-2.16-10.2v-33.07h5.92v32.7c0,3.99,1.17,7.2,3.52,9.62,2.35,2.43,5.36,3.64,9.04,3.64,2.3,0,4.52-.55,6.65-1.66,2.13-1.11,3.88-2.73,5.25-4.87,1.37-2.14,2.06-4.77,2.06-7.89Z"></path>
                                <path class="cls-1" d="m306.05,101.16V33.09h5.95v26.75h.53c.66-1.53,1.62-3.1,2.87-4.72,1.25-1.62,2.9-2.97,4.95-4.07,2.05-1.1,4.64-1.65,7.76-1.65,4.19,0,7.86,1.1,11.03,3.31,3.17,2.21,5.64,5.28,7.41,9.24,1.77,3.95,2.66,8.56,2.66,13.81s-.88,9.89-2.64,13.86c-1.76,3.97-4.22,7.06-7.38,9.27-3.16,2.22-6.82,3.32-10.98,3.32-3.1,0-5.69-.55-7.76-1.66-2.07-1.11-3.74-2.48-5-4.1-1.26-1.63-2.24-3.22-2.92-4.77h-.73v9.47h-5.75Zm5.85-25.49c0,4.12.61,7.77,1.84,10.95,1.23,3.18,3,5.67,5.32,7.48,2.31,1.81,5.12,2.71,8.42,2.71s6.26-.94,8.61-2.81c2.35-1.87,4.13-4.4,5.35-7.59,1.22-3.19,1.83-6.77,1.83-10.73s-.6-7.46-1.81-10.6c-1.21-3.15-2.99-5.64-5.33-7.49-2.35-1.85-5.23-2.78-8.64-2.78s-6.14.89-8.46,2.68c-2.32,1.78-4.08,4.24-5.3,7.38-1.22,3.14-1.83,6.74-1.83,10.82Z"></path>
                                <path class="cls-1" d="m382.86,102.22c-4.76,0-8.88-1.11-12.36-3.34-3.48-2.23-6.16-5.32-8.06-9.27s-2.84-8.51-2.84-13.68.95-9.71,2.84-13.71c1.89-4,4.52-7.13,7.88-9.41,3.36-2.27,7.24-3.41,11.65-3.41,2.77,0,5.44.5,8.01,1.51,2.57,1.01,4.88,2.55,6.93,4.62,2.05,2.07,3.67,4.68,4.87,7.81,1.2,3.14,1.79,6.84,1.79,11.12v2.92h-39.88v-5.22h33.83c0-3.28-.66-6.23-1.98-8.86-1.32-2.63-3.15-4.7-5.48-6.23-2.34-1.53-5.04-2.29-8.09-2.29-3.23,0-6.08.86-8.54,2.59-2.46,1.73-4.38,4.01-5.77,6.85-1.38,2.84-2.09,5.94-2.11,9.31v3.12c0,4.05.7,7.59,2.11,10.62,1.41,3.02,3.41,5.37,6,7.03,2.59,1.66,5.66,2.49,9.21,2.49,2.41,0,4.54-.38,6.36-1.13,1.83-.75,3.37-1.77,4.62-3.04,1.25-1.27,2.2-2.68,2.84-4.2l5.62,1.83c-.78,2.15-2.04,4.13-3.81,5.95-1.76,1.82-3.95,3.27-6.58,4.37-2.63,1.1-5.65,1.65-9.06,1.65Z"></path>
                                <path class="cls-1" d="m412.07,44.96v-11.86h55.9v11.86h-20.84v56.2h-14.22v-56.2h-20.84Z"></path>
                                <path class="cls-1" d="m471.62,101.16v-51.05h13.73v8.91h.53c.93-3.17,2.49-5.57,4.69-7.2,2.19-1.63,4.72-2.44,7.58-2.44.71,0,1.47.04,2.29.13.82.09,1.54.21,2.16.37v12.56c-.66-.2-1.58-.38-2.76-.53-1.17-.15-2.25-.23-3.22-.23-2.08,0-3.94.45-5.57,1.35-1.63.9-2.91,2.14-3.86,3.74-.94,1.6-1.41,3.43-1.41,5.52v28.88h-14.16Z"></path>
                                <path class="cls-1" d="m523,102.12c-3.26,0-6.16-.57-8.71-1.71-2.55-1.14-4.56-2.84-6.03-5.08-1.47-2.25-2.21-5.06-2.21-8.42,0-2.84.52-5.22,1.56-7.15,1.04-1.93,2.46-3.48,4.25-4.65,1.79-1.17,3.84-2.06,6.13-2.66,2.29-.6,4.7-1.02,7.23-1.26,2.97-.31,5.36-.6,7.18-.88,1.82-.28,3.13-.69,3.95-1.25.82-.55,1.23-1.37,1.23-2.46v-.2c0-2.1-.66-3.73-1.98-4.89-1.32-1.15-3.19-1.73-5.6-1.73-2.55,0-4.58.56-6.08,1.68-1.51,1.12-2.5,2.52-2.99,4.2l-13.09-1.06c.66-3.1,1.97-5.79,3.92-8.06,1.95-2.27,4.47-4.02,7.56-5.25,3.09-1.23,6.67-1.84,10.75-1.84,2.84,0,5.55.33,8.16,1,2.6.66,4.92,1.7,6.95,3.09,2.03,1.4,3.63,3.19,4.8,5.37,1.17,2.18,1.76,4.79,1.76,7.83v34.43h-13.43v-7.08h-.4c-.82,1.6-1.92,3-3.29,4.2-1.37,1.21-3.02,2.15-4.95,2.82-1.93.68-4.15,1.01-6.68,1.01Zm4.05-9.77c2.08,0,3.92-.42,5.52-1.25s2.85-1.96,3.76-3.37c.91-1.42,1.36-3.02,1.36-4.82v-5.42c-.44.29-1.05.55-1.81.78s-1.62.44-2.58.63c-.95.19-1.91.35-2.86.5-.95.14-1.82.27-2.59.38-1.66.24-3.11.63-4.35,1.16-1.24.53-2.21,1.25-2.89,2.14-.69.9-1.03,2.01-1.03,3.34,0,1.93.7,3.4,2.11,4.4,1.41,1.01,3.2,1.51,5.37,1.51Z"></path>
                                <path class="cls-1" d="m580.03,50.11h10.17v10.63h-10.17v40.41h-14.12v-40.41h-7.21v-10.63h7.21v-3.69c0-3.7.72-6.77,2.18-9.21,1.45-2.44,3.44-4.26,5.97-5.48,2.53-1.22,5.4-1.83,8.61-1.83,2.17,0,4.15.17,5.97.5,1.8.33,3.15.63,4.04.9l-2.53,10.63c-.55-.18-1.23-.34-2.04-.5s-1.64-.23-2.48-.23c-2.08,0-3.53.48-4.35,1.45-.83.96-1.23,2.31-1.23,4.04v3.42Z"></path>
                                <path class="cls-1" d="m616.12,50.11h10.17v10.63h-10.17v40.41h-14.12v-40.41h-7.21v-10.63h7.21v-3.69c0-3.7.72-6.77,2.18-9.21,1.45-2.44,3.44-4.26,5.97-5.48,2.53-1.22,5.4-1.83,8.61-1.83,2.17,0,4.15.17,5.97.5,1.8.33,3.15.63,4.04.9l-2.53,10.63c-.55-.18-1.23-.34-2.04-.5-.81-.15-1.64-.23-2.48-.23-2.08,0-3.53.48-4.35,1.45-.83.96-1.23,2.31-1.23,4.04v3.42Z"></path>
                                <path class="cls-1" d="m642.05,43.53c-2.11,0-3.9-.7-5.4-2.09-1.5-1.4-2.24-3.08-2.24-5.05s.75-3.66,2.26-5.07c1.51-1.41,3.3-2.11,5.38-2.11s3.91.7,5.4,2.09c1.5,1.4,2.24,3.08,2.24,5.05s-.75,3.66-2.24,5.07c-1.5,1.41-3.3,2.11-5.4,2.11Zm-7.11,57.63v-51.05h14.16v51.05h-14.16Z"></path>
                                <path class="cls-1" d="m683.46,102.15c-5.23,0-9.72-1.11-13.48-3.34-3.76-2.23-6.64-5.32-8.64-9.29-2.01-3.97-3.01-8.53-3.01-13.69s1.01-9.82,3.04-13.78c2.03-3.95,4.91-7.05,8.66-9.27,3.74-2.23,8.2-3.34,13.36-3.34,4.45,0,8.35.81,11.7,2.43,3.35,1.62,5.99,3.89,7.94,6.81,1.95,2.92,3.02,6.36,3.22,10.3h-13.36c-.38-2.55-1.37-4.6-2.97-6.16-1.61-1.56-3.71-2.34-6.3-2.34-2.19,0-4.1.59-5.73,1.78-1.63,1.19-2.9,2.91-3.81,5.17s-1.36,5-1.36,8.21.45,6.03,1.35,8.31c.9,2.28,2.17,4.02,3.81,5.22,1.64,1.2,3.56,1.79,5.75,1.79,1.62,0,3.07-.33,4.37-1,1.3-.66,2.37-1.63,3.22-2.91.85-1.27,1.41-2.81,1.68-4.6h13.36c-.22,3.9-1.28,7.33-3.17,10.29-1.89,2.96-4.5,5.27-7.83,6.93-3.32,1.66-7.26,2.49-11.8,2.49Z"></path>
                                </g>
                                <g>
                                <path class="cls-3" d="m800,37.72c0,4.31-3.5,7.81-7.82,7.81s-7.81-3.5-7.81-7.81,3.5-7.82,7.81-7.82,7.82,3.5,7.82,7.82Z"></path>
                                <path class="cls-2" d="m736.11,85.52c-.09,0-.18,0-.27-.01-.09,0-.18-.02-.26-.03.18.01.35.05.53.05Z"></path>
                                <path class="cls-2" d="m784.37,50.18v32.89c0,1.31-1.05,2.37-2.35,2.43-.03,0-.07.02-.11.02h-.37l-18.68-41.36-3.1-6.86c-2.03-4.5-6.52-7.4-11.47-7.4s-9.42,2.9-11.46,7.4l-11.96,26.47-10.4,23.03c-3.04,6.73,1.88,14.36,9.28,14.36h66.01c5.65,0,10.25-4.58,10.25-10.24v-40.74h-15.63Zm-48.25,35.34c-.09,0-.18,0-.27-.01-.09,0-.18-.02-.26-.03-2.25-.33-3.65-2.75-2.67-4.91l12.93-28.64c.95-2.1,3.93-2.1,4.89,0l15.17,33.59h-29.79Z"></path>
                                </g>
                            </g>
                            </g>
                        </g>
                        </svg>
                    <div editabletype="text" class="f-18 f-md-18 w400 mt20 lh130 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
                    <div class="f-15 f-md-18 w600 lh150 white-clr text-xs-center mt20"><script type="text/javascript" src="https://warriorplus.com/o2/disclaimer/hz2587" defer></script><div class="wplus_spdisclaimer"></div></div>
                </div>
                <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                    <div class="f-18 f-md-20 w400 lh130 white-clr text-xs-center">Copyright © TubeTrafficAi 2023</div>
                    <ul class="footer-ul w400 f-18 f-md-20 white-clr text-center text-md-right">
                        <li><a href="https://support.bizomart.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                        <li><a href="http://tubetrafficai.com/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                        <li><a href="http://tubetrafficai.com/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                        <li><a href="http://tubetrafficai.com/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                        <li><a href="http://tubetrafficai.com/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                        <li><a href="http://tubetrafficai.com/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                        <li><a href="http://tubetrafficai.com/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="assets/js/ouibounce.min.js"></script>
<div id="ouibounce-modal" style="z-index:9999999; display:none;">
<div class="underlay"></div>
<div class="modal-wrapper" style="display:block;">
    <div class="modal-bg">
        <button type="button" class="close" onclick="document.getElementById('ouibounce-modal').style.display = 'none';">
            X 
        </button>
        <div class="model-header">
    <div class="d-flex justify-content-center align-items-center gap-3">
            <img src="assets/images/hold.png" alt="" class="img-fluid d-block m131">
            <div>
            <div class="f-md-56 f-24 w700 lh130 white-clr">
                WAIT! HOLD ON!!
            </div>
            <div class="f-md-28 f-18 w700 lh130 black-clr mt10 mt-md0" style="background-color:yellow; padding:5px 10px; display:inline-block">
                Don't Leave Empty Handed
            </div>
            </div>
        </div>
        </div>
        <div class="col-12 for-padding">
            <div class="copun-line f-md-45 f-16 w500 lh130 text-center black-clr mt10">
        You've Qualified For An <span class="w700 red-clr"><br> INSTANT $100 DISCOUNT</span>  
            </div>
        <div class="copun-line f-md-30 f-16 w700 lh130 text-center black-clr mt10" style="background-color:yellow; padding:5px 10px; display:inline-block">
            Regular Price <strike>$147</strike>,
            Now Only $47	
        </div>
        <div class="mt-md20 mt10 text-center">
            <a href="https://warriorplus.com/o2/buy/hz2587/l07g46/wc8nsf" class="cta-link-btn1">Grab SPECIAL Discount Now!
            <br>
            <span class="f-12 black-clr w600 text-uppercase">Act Now! Limited to First 100 Buyers Only.</span> </a>
        </div>
            <!-- <div class="mt-md20 mt10 text-center f-md-26 f-16 w900 lh150 text-center red-clr text-uppercase">
                Coupon Code EXPIRES IN:
            </div>
            <div class="timer-new">
            <div class="days" class="timer-label">
            <span id="days"></span> <span class="text">Days</span>
            </div> 
            <div class="hours" class="timer-label">
            <span id="hours"></span> <span  class="text">Hours</span>
            </div>
            <div class="days" class="timer-label">
            <span id="mins" ></span> <span class="text">Min</span>
            </div>
            <div class="secs" class="timer-label">
            <span id="secs" ></span> <span class="text">Sec</span>
            </div>
        </div> -->
    </div>
</div>
</div>
<script type="text/javascript">
    var timeInSecs;
    var ticker;
    function startTimer(secs) {
    timeInSecs = parseInt(secs);
    tick();
    //ticker = setInterval("tick()", 1000);
    }
    function tick() {
    var secs = timeInSecs;
    if (secs > 0) {
    timeInSecs--;
    } else {
    clearInterval(ticker);
    //startTimer(20 * 60); // 4 minutes in seconds
    }
    var days = Math.floor(secs / 86400);
    secs %= 86400;
    var hours = Math.floor(secs / 3600);
    secs %= 3600;
    var mins = Math.floor(secs / 60);
    secs %= 60;
    var pretty = ((days < 10) ? "0" : "") + days + ":" + ((hours < 10) ? "0" : "") + hours + ":" + ((mins < 10) ? "0" : "") + mins + ":" + ((secs < 10) ? "0" : "") + secs;
    document.getElementById("days").innerHTML = days
    document.getElementById("hours").innerHTML = hours
    document.getElementById("mins").innerHTML = mins
    document.getElementById("secs").innerHTML = secs;
    }
    
    //startTimer(60 * 60); 
    // 4 minutes in seconds

    function getCookie(cname) {
          var name = cname + "=";
          var decodedCookie = decodeURIComponent(document.cookie);
          var ca = decodedCookie.split(';');
          for(var i = 0; i <ca.length; i++) {
              var c = ca[i];
              while (c.charAt(0) == ' ') {
                  c = c.substring(1);
              }
              if (c.indexOf(name) == 0) {
                  return c.substring(name.length, c.length);
              }
          }
          return "";
    }
    var cnt = 60*60;

    function counter(){
     
       if(getCookie("cnt") > 0){
          cnt = getCookie("cnt");
       }

       cnt -= 1;
       document.cookie = "cnt="+ cnt;

       startTimer(getCookie("cnt"));
       //jQuery("#counter").val(getCookie("cnt"));

       if(cnt>0){
          setTimeout(counter,1000);
       }
    
    }
    
    counter();
    

    </script>-
    <script type="text/javascript">
       var _ouibounce = ouibounce(document.getElementById('ouibounce-modal'),{
       aggressive: true, //Making this true makes ouibounce not to obey "once per visitor" rule
       });
    </script>
    <!-- Exit Popup Ends-->
          <!--- timer end-->
 <!-- Exit Popup and Timer End -->
</body>
</html>
<script> 
function myFunction() {
var checkBox = document.getElementById("check");
var buybutton = document.getElementById("buyButton");
if (checkBox.checked == true){
      buybutton.getAttribute("href");
      buybutton.setAttribute("href","https://warriorplus.com/o2/buy/zh5gzv/stszb4/qr34pz");
  } else {
       buybutton.getAttribute("href");
       buybutton.setAttribute("href","https://warriorplus.com/o2/buy/zh5gzv/stszb4/qr34pz");
    }
}
</script>
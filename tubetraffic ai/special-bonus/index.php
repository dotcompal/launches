<html xmlns="http://www.w3.org/1999/xhtml">
<head>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
   <!-- Tell the browser to be responsive to screen width -->
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
   <meta name="title" content="TubeTrafficAi Special Bonuses">
   <meta name="description" content="World's First Google-Bard Powered AI App That *Legally* Hacks Into YouTube's 800 Million Videos…">
   <meta name="keywords" content="">
   <meta property="og:image" content="https://www.tubetrafficai.com/special-bonus/thumbnail.png">
   <meta name="language" content="English">
   <meta name="revisit-after" content="1 days">
   <meta name="author" content="Dr. Amit Pareek">
   <!-- Open Graph / Facebook -->
   <meta property="og:type" content="website">
   <meta property="og:title" content="TubeTrafficAi Special Bonuses">
   <meta property="og:description" content="World's First Google-Bard Powered AI App That *Legally* Hacks Into YouTube's 800 Million Videos…">
   <meta property="og:image" content="https://www.tubetrafficai.com/special-bonus/thumbnail.png">
   <!-- Twitter -->
   <meta property="twitter:card" content="summary_large_image">
   <meta property="twitter:title" content="TubeTrafficAi Special Bonuses">
   <meta property="twitter:description" content="World's First Google-Bard Powered AI App That *Legally* Hacks Into YouTube's 800 Million Videos…">
   <meta property="twitter:image" content="https://www.tubetrafficai.com/special-bonus/thumbnail.png">
   <title>TubeTrafficAi Special Bonuses</title>

   <link rel="preconnect" href="https://fonts.googleapis.com">
   <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
   <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&amp;family=Poppins:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&amp;display=swap" rel="stylesheet">
   <!-- Shortcut Icon  -->
   <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
   <!-- Css CDN Load Link -->
   <link rel="stylesheet" href="../common_assets/css/bootstrap.min.css" type="text/css">
   <link rel="stylesheet" href="../common_assets/css/general.css">
   <link rel="stylesheet" type="text/css" href="assets/css/bonus-style.css">
   <link rel="stylesheet" href="assets/css/style.css" type="text/css">
   <script src="https://cdn.oppyo.com/launches/vidboxs/common_assets/js/jquery.min.js"></script>
</head>

<body>
   <!-- New Timer  Start-->
   <?php
   $date = 'September 16 2023 11:00 AM EST';
   $exp_date = strtotime($date);
   $now = time();

   if ($now < $exp_date) {
   ?>
   <?php
   } else {
      echo "Times Up";
   }
   ?>
   <!-- New Timer End -->
   <?php
   if (!isset($_GET['afflink'])) {
      $_GET['afflink'] = 'https://warriorplus.com/o2/a/djw1w2/0';
      $_GET['name'] = 'Dr. Amit Pareek';
   }
   ?>
 
   <div class="main-header">
      <div class="header-nav">
         <!-- container -->
         <div class="conatiner">
               <div class="row">
                  <div class="col-12 text-center">
                  <div class="text-center">
                     <div class="f-md-32 f-20 lh140 w500 white-clr d-block d-md-flex align-items-center justify-content-center">
                        <span class="w600 yellow-clr"><?php echo $_GET['name']; ?>'s</span> &nbsp;special bonus for &nbsp;
                        <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 800 131.43" style="max-height:75px;">
                           <defs>
                              <style>
                              .cls-1 {
                                 fill: #fff;
                              }
         
                              .cls-2 {
                                 fill: #fd2f15;
                              }
         
                              .cls-3 {
                                 fill: #fbbf33;
                              }
                              </style>
                           </defs>
                           <g id="Layer_1-2" data-name="Layer 1">
                              <g>
                              <g>
                                 <path class="cls-3" d="m105.61,78.06l40.05,23.12c6.01,3.47,13.51-.87,13.51-7.8v-46.25c0-6.93-7.51-11.27-13.51-7.8l-40.05,23.12c-6.01,3.47-6.01,12.14,0,15.6Z"/>
                                 <g>
                                 <rect class="cls-3" x="26.92" y="58.86" width="16.03" height="46.81"/>
                                 <path class="cls-2" d="m125.22,26.34h-7.16v46.06c0,32.6-26.43,59.03-59.03,59.03H0v-65.94C0,32.89,26.43,6.45,59.03,6.45h38.52l-12.38,19.88h-26.14c-21.59,0-39.16,17.57-39.16,39.15v46.07h39.16c21.59,0,39.16-17.57,39.16-39.16V26.34h-5.79L108.81,0l16.41,26.34Z"/>
                                 <path class="cls-3" d="m67.04,46.21h-16.02v59.46h8.01c2.76,0,5.45-.35,8.01-.99v-58.47Z"/>
                                 <path class="cls-3" d="m90.77,33.56h-16.03v68.14c7.57-4.07,13.39-10.99,16.03-19.31v-48.83Z"/>
                                 </g>
                              </g>
                              <g>
                                 <g>
                                 <path class="cls-1" d="m195.69,38.68v-5.58h49.35v5.58h-21.57v62.48h-6.21v-62.48h-21.57Z"/>
                                 <path class="cls-1" d="m283.29,81.65v-31.54h5.92v51.05h-5.92v-8.47h-.47c-1.2,2.61-3.09,4.79-5.68,6.53-2.59,1.74-5.76,2.61-9.5,2.61-3.26,0-6.15-.71-8.67-2.14-2.53-1.43-4.51-3.53-5.95-6.3-1.44-2.77-2.16-6.17-2.16-10.2v-33.07h5.92v32.7c0,3.99,1.17,7.2,3.52,9.62,2.35,2.43,5.36,3.64,9.04,3.64,2.3,0,4.52-.55,6.65-1.66,2.13-1.11,3.88-2.73,5.25-4.87,1.37-2.14,2.06-4.77,2.06-7.89Z"/>
                                 <path class="cls-1" d="m306.05,101.16V33.09h5.95v26.75h.53c.66-1.53,1.62-3.1,2.87-4.72,1.25-1.62,2.9-2.97,4.95-4.07,2.05-1.1,4.64-1.65,7.76-1.65,4.19,0,7.86,1.1,11.03,3.31,3.17,2.21,5.64,5.28,7.41,9.24,1.77,3.95,2.66,8.56,2.66,13.81s-.88,9.89-2.64,13.86c-1.76,3.97-4.22,7.06-7.38,9.27-3.16,2.22-6.82,3.32-10.98,3.32-3.1,0-5.69-.55-7.76-1.66-2.07-1.11-3.74-2.48-5-4.1-1.26-1.63-2.24-3.22-2.92-4.77h-.73v9.47h-5.75Zm5.85-25.49c0,4.12.61,7.77,1.84,10.95,1.23,3.18,3,5.67,5.32,7.48,2.31,1.81,5.12,2.71,8.42,2.71s6.26-.94,8.61-2.81c2.35-1.87,4.13-4.4,5.35-7.59,1.22-3.19,1.83-6.77,1.83-10.73s-.6-7.46-1.81-10.6c-1.21-3.15-2.99-5.64-5.33-7.49-2.35-1.85-5.23-2.78-8.64-2.78s-6.14.89-8.46,2.68c-2.32,1.78-4.08,4.24-5.3,7.38-1.22,3.14-1.83,6.74-1.83,10.82Z"/>
                                 <path class="cls-1" d="m382.86,102.22c-4.76,0-8.88-1.11-12.36-3.34-3.48-2.23-6.16-5.32-8.06-9.27s-2.84-8.51-2.84-13.68.95-9.71,2.84-13.71c1.89-4,4.52-7.13,7.88-9.41,3.36-2.27,7.24-3.41,11.65-3.41,2.77,0,5.44.5,8.01,1.51,2.57,1.01,4.88,2.55,6.93,4.62,2.05,2.07,3.67,4.68,4.87,7.81,1.2,3.14,1.79,6.84,1.79,11.12v2.92h-39.88v-5.22h33.83c0-3.28-.66-6.23-1.98-8.86-1.32-2.63-3.15-4.7-5.48-6.23-2.34-1.53-5.04-2.29-8.09-2.29-3.23,0-6.08.86-8.54,2.59-2.46,1.73-4.38,4.01-5.77,6.85-1.38,2.84-2.09,5.94-2.11,9.31v3.12c0,4.05.7,7.59,2.11,10.62,1.41,3.02,3.41,5.37,6,7.03,2.59,1.66,5.66,2.49,9.21,2.49,2.41,0,4.54-.38,6.36-1.13,1.83-.75,3.37-1.77,4.62-3.04,1.25-1.27,2.2-2.68,2.84-4.2l5.62,1.83c-.78,2.15-2.04,4.13-3.81,5.95-1.76,1.82-3.95,3.27-6.58,4.37-2.63,1.1-5.65,1.65-9.06,1.65Z"/>
                                 <path class="cls-1" d="m412.07,44.96v-11.86h55.9v11.86h-20.84v56.2h-14.22v-56.2h-20.84Z"/>
                                 <path class="cls-1" d="m471.62,101.16v-51.05h13.73v8.91h.53c.93-3.17,2.49-5.57,4.69-7.2,2.19-1.63,4.72-2.44,7.58-2.44.71,0,1.47.04,2.29.13.82.09,1.54.21,2.16.37v12.56c-.66-.2-1.58-.38-2.76-.53-1.17-.15-2.25-.23-3.22-.23-2.08,0-3.94.45-5.57,1.35-1.63.9-2.91,2.14-3.86,3.74-.94,1.6-1.41,3.43-1.41,5.52v28.88h-14.16Z"/>
                                 <path class="cls-1" d="m523,102.12c-3.26,0-6.16-.57-8.71-1.71-2.55-1.14-4.56-2.84-6.03-5.08-1.47-2.25-2.21-5.06-2.21-8.42,0-2.84.52-5.22,1.56-7.15,1.04-1.93,2.46-3.48,4.25-4.65,1.79-1.17,3.84-2.06,6.13-2.66,2.29-.6,4.7-1.02,7.23-1.26,2.97-.31,5.36-.6,7.18-.88,1.82-.28,3.13-.69,3.95-1.25.82-.55,1.23-1.37,1.23-2.46v-.2c0-2.1-.66-3.73-1.98-4.89-1.32-1.15-3.19-1.73-5.6-1.73-2.55,0-4.58.56-6.08,1.68-1.51,1.12-2.5,2.52-2.99,4.2l-13.09-1.06c.66-3.1,1.97-5.79,3.92-8.06,1.95-2.27,4.47-4.02,7.56-5.25,3.09-1.23,6.67-1.84,10.75-1.84,2.84,0,5.55.33,8.16,1,2.6.66,4.92,1.7,6.95,3.09,2.03,1.4,3.63,3.19,4.8,5.37,1.17,2.18,1.76,4.79,1.76,7.83v34.43h-13.43v-7.08h-.4c-.82,1.6-1.92,3-3.29,4.2-1.37,1.21-3.02,2.15-4.95,2.82-1.93.68-4.15,1.01-6.68,1.01Zm4.05-9.77c2.08,0,3.92-.42,5.52-1.25s2.85-1.96,3.76-3.37c.91-1.42,1.36-3.02,1.36-4.82v-5.42c-.44.29-1.05.55-1.81.78s-1.62.44-2.58.63c-.95.19-1.91.35-2.86.5-.95.14-1.82.27-2.59.38-1.66.24-3.11.63-4.35,1.16-1.24.53-2.21,1.25-2.89,2.14-.69.9-1.03,2.01-1.03,3.34,0,1.93.7,3.4,2.11,4.4,1.41,1.01,3.2,1.51,5.37,1.51Z"/>
                                 <path class="cls-1" d="m580.03,50.11h10.17v10.63h-10.17v40.41h-14.12v-40.41h-7.21v-10.63h7.21v-3.69c0-3.7.72-6.77,2.18-9.21,1.45-2.44,3.44-4.26,5.97-5.48,2.53-1.22,5.4-1.83,8.61-1.83,2.17,0,4.15.17,5.97.5,1.8.33,3.15.63,4.04.9l-2.53,10.63c-.55-.18-1.23-.34-2.04-.5s-1.64-.23-2.48-.23c-2.08,0-3.53.48-4.35,1.45-.83.96-1.23,2.31-1.23,4.04v3.42Z"/>
                                 <path class="cls-1" d="m616.12,50.11h10.17v10.63h-10.17v40.41h-14.12v-40.41h-7.21v-10.63h7.21v-3.69c0-3.7.72-6.77,2.18-9.21,1.45-2.44,3.44-4.26,5.97-5.48,2.53-1.22,5.4-1.83,8.61-1.83,2.17,0,4.15.17,5.97.5,1.8.33,3.15.63,4.04.9l-2.53,10.63c-.55-.18-1.23-.34-2.04-.5-.81-.15-1.64-.23-2.48-.23-2.08,0-3.53.48-4.35,1.45-.83.96-1.23,2.31-1.23,4.04v3.42Z"/>
                                 <path class="cls-1" d="m642.05,43.53c-2.11,0-3.9-.7-5.4-2.09-1.5-1.4-2.24-3.08-2.24-5.05s.75-3.66,2.26-5.07c1.51-1.41,3.3-2.11,5.38-2.11s3.91.7,5.4,2.09c1.5,1.4,2.24,3.08,2.24,5.05s-.75,3.66-2.24,5.07c-1.5,1.41-3.3,2.11-5.4,2.11Zm-7.11,57.63v-51.05h14.16v51.05h-14.16Z"/>
                                 <path class="cls-1" d="m683.46,102.15c-5.23,0-9.72-1.11-13.48-3.34-3.76-2.23-6.64-5.32-8.64-9.29-2.01-3.97-3.01-8.53-3.01-13.69s1.01-9.82,3.04-13.78c2.03-3.95,4.91-7.05,8.66-9.27,3.74-2.23,8.2-3.34,13.36-3.34,4.45,0,8.35.81,11.7,2.43,3.35,1.62,5.99,3.89,7.94,6.81,1.95,2.92,3.02,6.36,3.22,10.3h-13.36c-.38-2.55-1.37-4.6-2.97-6.16-1.61-1.56-3.71-2.34-6.3-2.34-2.19,0-4.1.59-5.73,1.78-1.63,1.19-2.9,2.91-3.81,5.17s-1.36,5-1.36,8.21.45,6.03,1.35,8.31c.9,2.28,2.17,4.02,3.81,5.22,1.64,1.2,3.56,1.79,5.75,1.79,1.62,0,3.07-.33,4.37-1,1.3-.66,2.37-1.63,3.22-2.91.85-1.27,1.41-2.81,1.68-4.6h13.36c-.22,3.9-1.28,7.33-3.17,10.29-1.89,2.96-4.5,5.27-7.83,6.93-3.32,1.66-7.26,2.49-11.8,2.49Z"/>
                                 </g>
                                 <g>
                                 <path class="cls-3" d="m800,37.72c0,4.31-3.5,7.81-7.82,7.81s-7.81-3.5-7.81-7.81,3.5-7.82,7.81-7.82,7.82,3.5,7.82,7.82Z"/>
                                 <path class="cls-2" d="m736.11,85.52c-.09,0-.18,0-.27-.01-.09,0-.18-.02-.26-.03.18.01.35.05.53.05Z"/>
                                 <path class="cls-2" d="m784.37,50.18v32.89c0,1.31-1.05,2.37-2.35,2.43-.03,0-.07.02-.11.02h-.37l-18.68-41.36-3.1-6.86c-2.03-4.5-6.52-7.4-11.47-7.4s-9.42,2.9-11.46,7.4l-11.96,26.47-10.4,23.03c-3.04,6.73,1.88,14.36,9.28,14.36h66.01c5.65,0,10.25-4.58,10.25-10.24v-40.74h-15.63Zm-48.25,35.34c-.09,0-.18,0-.27-.01-.09,0-.18-.02-.26-.03-2.25-.33-3.65-2.75-2.67-4.91l12.93-28.64c.95-2.1,3.93-2.1,4.89,0l15.17,33.59h-29.79Z"/>
                                 </g>
                              </g>
                              </g>
                           </g>
                        </svg>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      
      </div>
      <div class="container">
         <div class="row">  
            <div class="col-12 f-md-42 f-28 w700 text-center white-clr lh140 mt-md30 mt20 px-md-0">
                    World's First Google-Bard Powered AI App That <span class="red-brush"> *Legally* Hacks Into YouTube's 800 Million Videos… </span> 
                </div>
                <div class="col-12 mt-md20 mt20 text-center">
                    <div class="f-18 f-md-24 w600 lh140 white-clr ">
                        <i>Then Redirect Their Views To ANY Link or Offer of Our Choice</i>
                    </div>
                </div>
                <div class="col-12 mt-md20 mt20 text-center">
                    <div class="f-md-24 f-18 w500 yellow-clr lh140 post-heading">
                        92,346 Views In 4 Hours Or Less… Making Us $346.34 Daily
                    </div>
                </div>
                <div class="col-12 mt-md30 mt20 text-center">
                    <div class="col-12 f-md-24 f-18 w400 white-clr lh140">
                        Without Creating Videos | Without Uploading | Without Ads | No Upfront Cost at All
                    </div>
                </div>
            </div>
         <div class="row mt30 mt-md50">
            <div class="col-12 col-md-10 mx-auto">
               <img src="assets/images/productbox.webp" class="img-fluid mx-auto d-block" alt="ProductBox">
               <!-- <div class="responsive-video">
                  <iframe src=" https://vidboxs.dotcompal.com/video/embed/w4rbh264oa" style=" position: absolute;top: 0;left: 0;width: 100%;
                  height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; 
                  fullscreen" allowfullscreen=""></iframe>
               </div>  -->
            </div>
         </div>
      </div>
   </div>
   <!-- Header Section Start -->
   <div class="list-section">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="row header-list-block">
                  <div class="col-12 col-md-6">
                     <div class="f-18 f-md-20 lh140 w500">
                        <ul class="bonus-list pl0">
                        <li>Tap Into Fast-Growing $200 Billion+ Video Industry & Bank BIG. </li>
                        <li>Create Unlimited Reels & Short Videos for any Niche. </li>
                        <li>Create Reels, Stories & Shorts Using Just One keyword </li>
                        <li>Create Boomerang Video to Engage Audience  </li>
                        <li>Drive Unlimited Traffic & Sales to Any Offer or Page </li>
                        
                        <li>Add Background Music and/or Voiceover to Any Video </li>
                        </ul>
                     </div>
                  </div>
                  <div class="col-12 col-md-6">
                     <div class="f-18 f-md-20 lh140 w500">
                        <ul class="bonus-list pl0">
                        <li>Create Reels & Short Videos from Any Video or Text in 3 Simple Clicks </li>
                        <li>Make Tons of Affiliate Commissions or Ad Profits </li>
                        <li>Pre-designed 50+ Shorts Video templates & 25+ vector images</li>
                        <li>Inbuilt Voiceover Creator with 150+ Human Voice in 30+ Languages </li>
                        <li>100% Newbie Friendly, Required No Prior Experience or Tech Skills </li>  
                        <li>No Camera Recording, No Voice, or Complex Editing Required</li>
                        </ul>
                     </div>
                  </div>
                  <!-- <div class="col-12 col-md-12">
                     <div class="f-18 f-md-20 lh140 w500">
                        <ul class="bonus-list pl0">
                        </ul>
                     </div>
                  </div> -->
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Header Section End -->
   <!-- CTA Section Start -->
   <div class="dark-cta-sec">
      <div class="container-fluid">
         <div class="row">
            <div class="col-md-12 col-md-12 col-12 text-center ">
               <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
               <div class="f-md-36 f-22 text-center  lh120 w600 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
               <div class="f-18 f-md-22 lh140 w400 text-center mt20 white-clr">
                   Use Special Coupon <span class="w600 yellow-clr">"Trafficadmin"</span> For <span class="w600 yellow-clr"> 32% Off</span> On Full Funnel
               </div>
            </div>
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab TubeTrafficAi + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
               <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-20 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
               <div class="countdown counter-white white-clr"><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w600">00</span><br><span class="f-14 f-md-18 ">Days</span> </div><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w600">10</span><br><span class="f-14 f-md-18 w500 ">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald w600">28</span><br><span class="f-14 f-md-18 w500 ">Mins</span> </div><div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald w600">20</span><br><span class="f-14 f-md-18 w500">Sec</span> </div></div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>
   <!-- CTA Section End -->
   <!-- Step Section -->
   <div class="step-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-39 f-28 w600 text-center black-clr2 lh140">
                  Create Stunning Stories, Reels &amp; Shorts for Massive Traffic To Any Offer, Page, or Link <span class="red-clr w700">in Just 3 Easy Steps</span>
               </div>
            </div>
            <div class="row mt20 mt-md50">
               <div class="col-12 col-md-4">
                  <div class="step-block">
                     <div class="step-title f-20 f-md-22 w700">Step 1</div>
                     <img src="assets/images/step-2.webp" class="img-fluid d-block mx-auto mt15 mt-md30">
                     <div class="step-content">
                        <div class="f-22 f-md-28 w600 black-clr2 mt10">Choose Platform</div>
                        <div class="w400 f-18 f-md-20 lh140 mt10 black-clr2">
                           Choose the social platform- YouTube, Instagram, TikTok, Facebook, or Snapchat you want to create Reel or Shorts for
                        </div>
                     </div>  
                  </div>
               </div>
               <div class="col-12 col-md-4 mt20 mt-md0">
                  <div class="step-block">
                     <div class="step-title f-20 f-md-22 w700">Step 2</div>
                     <img src="assets/images/step-1.webp" class="img-fluid d-block mx-auto mt15 mt-md30">
                     <div class="step-content">
                        <div class="f-22 f-md-28 w600 black-clr2 mt10">Enter a Keyword</div>
                        <div class="w400 f-18 f-md-20 lh140 mt10 black-clr2">
                           Enter the desired Keyword &amp; you will get several short videos. Select any video &amp; customize it by adding background, vectors, audio, &amp; fonts to make it engaging
                        </div>
                     </div>  
                  </div>
               </div>
               <div class="col-12 col-md-4 mt20 mt-md0">
                  <div class="step-block">
                     <div class="step-title f-20 f-md-22 w700">Step 3</div>
                     <img src="assets/images/step-3.webp" class="img-fluid d-block mx-auto mt15 mt-md30">
                     <div class="step-content">
                        <div class="f-22 f-md-28 w600 black-clr2 mt10">Publish and Profit</div>
                        <div class="w400 f-18 f-md-20 lh140 mt10 black-clr2">
                           Now publish your Reels &amp; Shortson different social platforms with a click to drive unlimited viral traffic &amp; sales on your offer, page, or website.
                        </div>
                     </div>  
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md70">
               <div class="col-12 text-center">
                  <div class="f-20 f-md-24 w600 lh140">
                     No Download/Installation  |  No Prior Knowledge  |  100% Beginners Friendly
                  </div>
                  <div class="f-18 f-md-20 w400 mt15 mt-md30 lh140">
                     In just 60 seconds, you can create your first profitable Reels &amp; Shorts to enjoy MASSIVE FREE Traffic, Sales &amp; Profits coming into your bank account on autopilot.
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Step Section End -->
      <div class="proof-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-40 f-28 w700 lh140 text-center black-clr">
                  Got 34,309 Targeted Visitors in Last 30 Days… .                  
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
                  <img src="https://cdn.oppyo.com/launches/vidboxs/fe/proof.webp" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 mt20 mt-md50 f-md-40 f-28 w700 lh140 text-center black-clr">
                  Consistently Making Average $535 In Profits <br class="d-none d-md-block"> Each & Every Day              
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
                  <img src="https://cdn.oppyo.com/launches/vidboxs/fe/proof1.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>
         </div>
      </div>
     <!-- Testimonials Section -->
     <div class="testimonial-section">
         <div class="container ">
            <div class="row ">
               <div class="f-md-40 f-28 w700 lh140 text-center black-clr2">
                  Checkout What TubeTrafficAi Early Users Have to SAY  
               </div>
            </div>
            <div class="row mt25 mt-md50 align-items-center">
               <div class="col-12 col-md-10">
                  <div class="testi-block">
                     <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-12 col-md-3 order-md-2">
                           <img src="assets/images/c1.webp" class="img-fluid d-block mx-auto">
                           <div class="f-22 f-md-24 w700 lh120 yellow-clr mt20 text-center">Benjamin Davis</div>
                        </div>
                        <div class="col-12 col-md-9 order-md-1 mt20 mt-md0 text-center text-md-start">
                           <div class="f-22 f-md-28 w700 lh140">I got 2500+ Views &amp; 509 Subscribers in 4 days</div>
                           <div class="f-20 w400 lh140 mt20 quote1">
                              Hi, I am from Australia and promote weight loss products on ClickBank &amp; Amazon as an affiliate. I was struggling to get traffic on my offers and my YouTube channels have only 257 subscribers the reason is that I could not create and post Videos and Shorts regularly as it takes lots of effort and time. But recently, I got access to TubeTrafficAi, I created around 15 Short Videos with almost no effort, that was amazingly EASY. And <span class="w700">I got 2500+ Views &amp; 509 Subscribers in 4 days</span> on my weight loss channel. Kudos to the team for creating a solution that can make the complete process fast &amp; easy. 
                           </div> 
                         </div>
                     </div>
                 </div>
               </div>
               <div class="col-12 col-md-10 offset-md-2 mt20 mt-md40">
                  <div class="testi-block">
                     <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-12 col-md-3">
                           <img src="assets/images/c2.webp" class="img-fluid d-block mx-auto">
                           <div class="f-22 f-md-24 w700 yellow-clr mt20 text-center">Liam Smith</div>
                        </div>
                        <div class="col-12 col-md-9 mt20 mt-md0 text-center text-md-start">
                           <div class="f-22 f-md-28 w700 lh140">Best part about TubeTrafficAi is that it takes away all hassles</div>
                           <div class="f-20 w400 lh140 mt20 quote1">
                              Thanks for the early access, TubeTrafficAi Team. <span class="w700">The best part about TubeTrafficAi is that it takes away</span> all hassles like video recording or editing, voiceover recording, background music and driving traffic to your offers, etc. I am sure this will give you complete value for your money and help you get the desired success in the long run.
                           </div>  
                        </div>
                     </div>
                 </div>
               </div>
               <div class="col-12 col-md-10 mt20 mt-md40">
                  <div class="testi-block">
                     <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-12 col-md-3 order-md-2">
                           <img src="assets/images/c3.webp" class="img-fluid d-block mx-auto">
                           <div class="f-22 f-md-24 w700 lh120 yellow-clr mt20 text-center">Noah Brown</div>
                        </div>
                        <div class="col-12 col-md-9 order-md-1 mt20 mt-md0 text-center text-md-start">
                           <div class="f-22 f-md-28 w700 lh140">Create super engaging video shorts for YouTube</div>
                           <div class="f-20 w400 lh140 mt20 quote1">
                              TubeTrafficAi is the perfect platform to <span class="w700">create super engaging video shorts for YouTube and social media and drive tons of viral traffic in a stress-free manner.</span> I am enjoying all the functionalities of this amazing software. If someone asks me to give marks, I would love to give ten-on-ten to this fabulous product.
                           </div> 
                        </div>
                     </div>
                 </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Testimonials Section End -->
   <!-- CTA Section Start -->
   <div class="dark-cta-sec">
      <div class="container-fluid">
         <div class="row">
            <div class="col-md-12 col-md-12 col-12 text-center ">
               <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
               <div class="f-md-36 f-22 text-center  lh120 w600 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
               <div class="f-18 f-md-22 lh140 w400 text-center mt20 white-clr">
               Use Special Coupon <span class="w600 yellow-clr">"Trafficadmin"</span> For <span class="w600 yellow-clr"> 32% Off</span> On Full Funnel
               </div>
            </div>
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab TubeTrafficAi + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
               <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-20 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
               <div class="countdown counter-white white-clr"><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w600">00</span><br><span class="f-14 f-md-18 ">Days</span> </div><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w600">10</span><br><span class="f-14 f-md-18 w500 ">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald w600">28</span><br><span class="f-14 f-md-18 w500 ">Mins</span> </div><div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald w600">20</span><br><span class="f-14 f-md-18 w500">Sec</span> </div></div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>
   <!-- CTA Section End -->

   <div class="billion-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-45 w600 white-clr lh140 heading-design">Did You Know ? YouTube Shorts Hit</div>
                  <div class="f-45 f-md-100 w600 black-clr2 mt20 lh140"> <span class="red-clr">30 Billion</span> View</div>
                  <div class="f-28 f-md-45 w600 lh140 black-clr2">Every Single Day!</div>
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-12 col-md-7">
                  <div class="f-18 f-md-24 w600 black-clr2 lh140">Yes, YouTube Shorts are Generating 30 Billion+ Views Per Day, A 4X Increase From This Time Last Year
                  </div>
                  <div class="f-18 f-md-24 w400 lh140 black-clr21 mt20">This clearly shows the trend is changing. Internet users prefer short videos to consume content instead of watching long videos like earlier…</div>
                  <div class="f-28 f-md-45 w600 lh140 red-clr mt20 mt-md50">No Doubt!</div>
                  <div class="f-20 f-md-24 w600 lh140 black-clr2 mt10">Reels &amp; Shorts Are Present and Future of Video Marketing…</div>
               </div>
               <div class="col-12 col-md-5 mt20 mt-md0">
                  <img src="assets/images/girl.webp" class="img-fluid mx-auto d-block">
               </div>
            </div>
         </div>
         <div class="billion-wrap">
            <div class="container">
               <div class="row">
                  <div class="col-12 text-center">
                     <div class="f-28 f-md-45 w600 lh140 black-clr2">Here Are Some More Facts - <br class="d-none d-md-block">Why One Should Get Started with Reels &amp; Shorts! </div>
                  </div>
               </div>
               <div class="row align-items-center mt20 mt-md80">
                  <div class="col-12 col-md-6">
                     <div class="f-20 f-md-24 w600 lh140 text-start black-clr2">YouTube has 2+ Bn Users &amp; 70% of Videos are Viewed on Mobile</div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 ">
                     <img src="assets/images/facts1.webp" class="img-fluid d-block mx-auto">
                  </div>
               </div>
               <!-- <div class="row align-items-center mt20 mt-md80">
                  <div class="col-12 col-md-6 order-md-2">
                     <div class="f-20 f-md-24 w600 lh140 text-start black-clr2">YouTube Confirms Ads Are Coming to Shorts</div>
                     <div class="f-18 f-md-20 w400 lh140 mt10 text-start black-clr21"> Ads in Shorts would give creators in the YouTube Partner Program the ability to earn more revenue on the platform.
                     </div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
                     <img src="assets/images/facts2.webp" alt="YouTube Confirms Ads Are Coming to Shorts" class="mx-auto img-fluid d-block">
                  </div>
               </div> -->
               <div class="row align-items-center mt20 mt-md80">
                  <div class="col-12 col-md-6 order-md-2">
                     <div class="f-20 f-md-24 w600 lh140 text-start black-clr2">Instagram Reels have 2.5 Billion Monthly Active Users</div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
                     <img src="assets/images/facts3.webp" class="img-fluid d-block mx-auto" alt="Instagram Reels">
                  </div>
               </div>
               <div class="row align-items-center mt20 mt-md80">
                  <div class="col-12 col-md-6">
                     <div class="f-20 f-md-24 w600 lh140 text-start black-clr2">Tiktok has 50 Million Daily Active User in USA &amp; 1 Bn Users Worldwide</div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0">
                     <img src="assets/images/facts4.webp" alt="Tiktok" class="mx-auto img-fluid d-block">
                  </div>
               </div>
               <div class="row align-items-center mt20 mt-md80">
                  <div class="col-12 col-md-6 order-md-2">
                     <div class="f-20 f-md-24 w600 lh140 text-start black-clr2">100 Million hours Daily watch Time by Facebook users.</div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
                     <img src="assets/images/facts5.webp" class="img-fluid d-block mx-auto" alt="Facebook Users">
                  </div>
               </div>
               <div class="row align-items-center mt20 mt-md80">
                  <div class="col-12 col-md-6">
                     <div class="f-20 f-md-24 w600 lh140 text-start black-clr2">Snapchat has over 360+ million daily active users</div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0">
                     <img src="assets/images/facts6.webp" alt="Website Analyser" class="mx-auto img-fluid d-block">
                  </div>
               </div>
               <div class="row mt20 mt-md80">
                  <div class="col-12 text-center">
                     <div class="f-28 f-md-45 w600 lh140 ">So, Capitalising on Reels &amp; Shorts in the Video Marketing Strategy of Any Business is the Need of The Hour.</div>
                     <div class="f-md-45 f-28 w600 lh140 mt20 mt-md30 black-clr2  border-cover">
                        <div class="f-22 f-md-26 w600 lh140 black-clr2">It Opens the Door to</div>
                        <div class=""> Tsunami <span class="yellow-clr"> of FREE </span>Traffic</div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
<!-- Proudly Section -->
   <div class="next-gen-sec" id="product">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
            <div class="f-28 f-md-50 w700 lh140 white-clr caveat proudly-sec">
               Introducing…
            </div>
            </div>
            <div class="col-12 mt-md50 mt30 text-center">
            <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 800 131.43" style="max-height:75px;">
               <defs>
                     <style>
                     .cls-1 {
                        fill: #fff;
                     }

                     .cls-2 {
                        fill: #fd2f15;
                     }

                     .cls-3 {
                        fill: #fbbf33;
                     }
                     </style>
               </defs>
               <g id="Layer_1-2" data-name="Layer 1">
                     <g>
                     <g>
                        <path class="cls-3" d="m105.61,78.06l40.05,23.12c6.01,3.47,13.51-.87,13.51-7.8v-46.25c0-6.93-7.51-11.27-13.51-7.8l-40.05,23.12c-6.01,3.47-6.01,12.14,0,15.6Z"/>
                        <g>
                        <rect class="cls-3" x="26.92" y="58.86" width="16.03" height="46.81"/>
                        <path class="cls-2" d="m125.22,26.34h-7.16v46.06c0,32.6-26.43,59.03-59.03,59.03H0v-65.94C0,32.89,26.43,6.45,59.03,6.45h38.52l-12.38,19.88h-26.14c-21.59,0-39.16,17.57-39.16,39.15v46.07h39.16c21.59,0,39.16-17.57,39.16-39.16V26.34h-5.79L108.81,0l16.41,26.34Z"/>
                        <path class="cls-3" d="m67.04,46.21h-16.02v59.46h8.01c2.76,0,5.45-.35,8.01-.99v-58.47Z"/>
                        <path class="cls-3" d="m90.77,33.56h-16.03v68.14c7.57-4.07,13.39-10.99,16.03-19.31v-48.83Z"/>
                        </g>
                     </g>
                     <g>
                        <g>
                        <path class="cls-1" d="m195.69,38.68v-5.58h49.35v5.58h-21.57v62.48h-6.21v-62.48h-21.57Z"/>
                        <path class="cls-1" d="m283.29,81.65v-31.54h5.92v51.05h-5.92v-8.47h-.47c-1.2,2.61-3.09,4.79-5.68,6.53-2.59,1.74-5.76,2.61-9.5,2.61-3.26,0-6.15-.71-8.67-2.14-2.53-1.43-4.51-3.53-5.95-6.3-1.44-2.77-2.16-6.17-2.16-10.2v-33.07h5.92v32.7c0,3.99,1.17,7.2,3.52,9.62,2.35,2.43,5.36,3.64,9.04,3.64,2.3,0,4.52-.55,6.65-1.66,2.13-1.11,3.88-2.73,5.25-4.87,1.37-2.14,2.06-4.77,2.06-7.89Z"/>
                        <path class="cls-1" d="m306.05,101.16V33.09h5.95v26.75h.53c.66-1.53,1.62-3.1,2.87-4.72,1.25-1.62,2.9-2.97,4.95-4.07,2.05-1.1,4.64-1.65,7.76-1.65,4.19,0,7.86,1.1,11.03,3.31,3.17,2.21,5.64,5.28,7.41,9.24,1.77,3.95,2.66,8.56,2.66,13.81s-.88,9.89-2.64,13.86c-1.76,3.97-4.22,7.06-7.38,9.27-3.16,2.22-6.82,3.32-10.98,3.32-3.1,0-5.69-.55-7.76-1.66-2.07-1.11-3.74-2.48-5-4.1-1.26-1.63-2.24-3.22-2.92-4.77h-.73v9.47h-5.75Zm5.85-25.49c0,4.12.61,7.77,1.84,10.95,1.23,3.18,3,5.67,5.32,7.48,2.31,1.81,5.12,2.71,8.42,2.71s6.26-.94,8.61-2.81c2.35-1.87,4.13-4.4,5.35-7.59,1.22-3.19,1.83-6.77,1.83-10.73s-.6-7.46-1.81-10.6c-1.21-3.15-2.99-5.64-5.33-7.49-2.35-1.85-5.23-2.78-8.64-2.78s-6.14.89-8.46,2.68c-2.32,1.78-4.08,4.24-5.3,7.38-1.22,3.14-1.83,6.74-1.83,10.82Z"/>
                        <path class="cls-1" d="m382.86,102.22c-4.76,0-8.88-1.11-12.36-3.34-3.48-2.23-6.16-5.32-8.06-9.27s-2.84-8.51-2.84-13.68.95-9.71,2.84-13.71c1.89-4,4.52-7.13,7.88-9.41,3.36-2.27,7.24-3.41,11.65-3.41,2.77,0,5.44.5,8.01,1.51,2.57,1.01,4.88,2.55,6.93,4.62,2.05,2.07,3.67,4.68,4.87,7.81,1.2,3.14,1.79,6.84,1.79,11.12v2.92h-39.88v-5.22h33.83c0-3.28-.66-6.23-1.98-8.86-1.32-2.63-3.15-4.7-5.48-6.23-2.34-1.53-5.04-2.29-8.09-2.29-3.23,0-6.08.86-8.54,2.59-2.46,1.73-4.38,4.01-5.77,6.85-1.38,2.84-2.09,5.94-2.11,9.31v3.12c0,4.05.7,7.59,2.11,10.62,1.41,3.02,3.41,5.37,6,7.03,2.59,1.66,5.66,2.49,9.21,2.49,2.41,0,4.54-.38,6.36-1.13,1.83-.75,3.37-1.77,4.62-3.04,1.25-1.27,2.2-2.68,2.84-4.2l5.62,1.83c-.78,2.15-2.04,4.13-3.81,5.95-1.76,1.82-3.95,3.27-6.58,4.37-2.63,1.1-5.65,1.65-9.06,1.65Z"/>
                        <path class="cls-1" d="m412.07,44.96v-11.86h55.9v11.86h-20.84v56.2h-14.22v-56.2h-20.84Z"/>
                        <path class="cls-1" d="m471.62,101.16v-51.05h13.73v8.91h.53c.93-3.17,2.49-5.57,4.69-7.2,2.19-1.63,4.72-2.44,7.58-2.44.71,0,1.47.04,2.29.13.82.09,1.54.21,2.16.37v12.56c-.66-.2-1.58-.38-2.76-.53-1.17-.15-2.25-.23-3.22-.23-2.08,0-3.94.45-5.57,1.35-1.63.9-2.91,2.14-3.86,3.74-.94,1.6-1.41,3.43-1.41,5.52v28.88h-14.16Z"/>
                        <path class="cls-1" d="m523,102.12c-3.26,0-6.16-.57-8.71-1.71-2.55-1.14-4.56-2.84-6.03-5.08-1.47-2.25-2.21-5.06-2.21-8.42,0-2.84.52-5.22,1.56-7.15,1.04-1.93,2.46-3.48,4.25-4.65,1.79-1.17,3.84-2.06,6.13-2.66,2.29-.6,4.7-1.02,7.23-1.26,2.97-.31,5.36-.6,7.18-.88,1.82-.28,3.13-.69,3.95-1.25.82-.55,1.23-1.37,1.23-2.46v-.2c0-2.1-.66-3.73-1.98-4.89-1.32-1.15-3.19-1.73-5.6-1.73-2.55,0-4.58.56-6.08,1.68-1.51,1.12-2.5,2.52-2.99,4.2l-13.09-1.06c.66-3.1,1.97-5.79,3.92-8.06,1.95-2.27,4.47-4.02,7.56-5.25,3.09-1.23,6.67-1.84,10.75-1.84,2.84,0,5.55.33,8.16,1,2.6.66,4.92,1.7,6.95,3.09,2.03,1.4,3.63,3.19,4.8,5.37,1.17,2.18,1.76,4.79,1.76,7.83v34.43h-13.43v-7.08h-.4c-.82,1.6-1.92,3-3.29,4.2-1.37,1.21-3.02,2.15-4.95,2.82-1.93.68-4.15,1.01-6.68,1.01Zm4.05-9.77c2.08,0,3.92-.42,5.52-1.25s2.85-1.96,3.76-3.37c.91-1.42,1.36-3.02,1.36-4.82v-5.42c-.44.29-1.05.55-1.81.78s-1.62.44-2.58.63c-.95.19-1.91.35-2.86.5-.95.14-1.82.27-2.59.38-1.66.24-3.11.63-4.35,1.16-1.24.53-2.21,1.25-2.89,2.14-.69.9-1.03,2.01-1.03,3.34,0,1.93.7,3.4,2.11,4.4,1.41,1.01,3.2,1.51,5.37,1.51Z"/>
                        <path class="cls-1" d="m580.03,50.11h10.17v10.63h-10.17v40.41h-14.12v-40.41h-7.21v-10.63h7.21v-3.69c0-3.7.72-6.77,2.18-9.21,1.45-2.44,3.44-4.26,5.97-5.48,2.53-1.22,5.4-1.83,8.61-1.83,2.17,0,4.15.17,5.97.5,1.8.33,3.15.63,4.04.9l-2.53,10.63c-.55-.18-1.23-.34-2.04-.5s-1.64-.23-2.48-.23c-2.08,0-3.53.48-4.35,1.45-.83.96-1.23,2.31-1.23,4.04v3.42Z"/>
                        <path class="cls-1" d="m616.12,50.11h10.17v10.63h-10.17v40.41h-14.12v-40.41h-7.21v-10.63h7.21v-3.69c0-3.7.72-6.77,2.18-9.21,1.45-2.44,3.44-4.26,5.97-5.48,2.53-1.22,5.4-1.83,8.61-1.83,2.17,0,4.15.17,5.97.5,1.8.33,3.15.63,4.04.9l-2.53,10.63c-.55-.18-1.23-.34-2.04-.5-.81-.15-1.64-.23-2.48-.23-2.08,0-3.53.48-4.35,1.45-.83.96-1.23,2.31-1.23,4.04v3.42Z"/>
                        <path class="cls-1" d="m642.05,43.53c-2.11,0-3.9-.7-5.4-2.09-1.5-1.4-2.24-3.08-2.24-5.05s.75-3.66,2.26-5.07c1.51-1.41,3.3-2.11,5.38-2.11s3.91.7,5.4,2.09c1.5,1.4,2.24,3.08,2.24,5.05s-.75,3.66-2.24,5.07c-1.5,1.41-3.3,2.11-5.4,2.11Zm-7.11,57.63v-51.05h14.16v51.05h-14.16Z"/>
                        <path class="cls-1" d="m683.46,102.15c-5.23,0-9.72-1.11-13.48-3.34-3.76-2.23-6.64-5.32-8.64-9.29-2.01-3.97-3.01-8.53-3.01-13.69s1.01-9.82,3.04-13.78c2.03-3.95,4.91-7.05,8.66-9.27,3.74-2.23,8.2-3.34,13.36-3.34,4.45,0,8.35.81,11.7,2.43,3.35,1.62,5.99,3.89,7.94,6.81,1.95,2.92,3.02,6.36,3.22,10.3h-13.36c-.38-2.55-1.37-4.6-2.97-6.16-1.61-1.56-3.71-2.34-6.3-2.34-2.19,0-4.1.59-5.73,1.78-1.63,1.19-2.9,2.91-3.81,5.17s-1.36,5-1.36,8.21.45,6.03,1.35,8.31c.9,2.28,2.17,4.02,3.81,5.22,1.64,1.2,3.56,1.79,5.75,1.79,1.62,0,3.07-.33,4.37-1,1.3-.66,2.37-1.63,3.22-2.91.85-1.27,1.41-2.81,1.68-4.6h13.36c-.22,3.9-1.28,7.33-3.17,10.29-1.89,2.96-4.5,5.27-7.83,6.93-3.32,1.66-7.26,2.49-11.8,2.49Z"/>
                        </g>
                        <g>
                        <path class="cls-3" d="m800,37.72c0,4.31-3.5,7.81-7.82,7.81s-7.81-3.5-7.81-7.81,3.5-7.82,7.81-7.82,7.82,3.5,7.82,7.82Z"/>
                        <path class="cls-2" d="m736.11,85.52c-.09,0-.18,0-.27-.01-.09,0-.18-.02-.26-.03.18.01.35.05.53.05Z"/>
                        <path class="cls-2" d="m784.37,50.18v32.89c0,1.31-1.05,2.37-2.35,2.43-.03,0-.07.02-.11.02h-.37l-18.68-41.36-3.1-6.86c-2.03-4.5-6.52-7.4-11.47-7.4s-9.42,2.9-11.46,7.4l-11.96,26.47-10.4,23.03c-3.04,6.73,1.88,14.36,9.28,14.36h66.01c5.65,0,10.25-4.58,10.25-10.24v-40.74h-15.63Zm-48.25,35.34c-.09,0-.18,0-.27-.01-.09,0-.18-.02-.26-.03-2.25-.33-3.65-2.75-2.67-4.91l12.93-28.64c.95-2.1,3.93-2.1,4.89,0l15.17,33.59h-29.79Z"/>
                        </g>
                     </g>
                     </g>
               </g>
            </svg>
            </div>
         </div>
         <div class="row  align-items-center">
         <div class="col-12 f-md-42 f-28 w700 text-center white-clr lh140 mt-md40 mt20 px-md-0">
                    World's First Google-Bard Powered AI App That <span class="red-brush"> *Legally* Hacks Into YouTube's 800 Million Videos… </span> 
                </div>
                <div class="col-12 mt-md20 mt20 text-center">
                    <div class="f-18 f-md-24 w600 lh140 white-clr ">
                        <i>Then Redirect Their Views To ANY Link or Offer of Our Choice</i>
                    </div>
                </div>
                <div class="col-12 mt-md20 mt20 text-center">
                    <div class="f-md-24 f-18 w500 yellow-clr lh140 post-heading">
                        92,346 Views In 4 Hours Or Less… Making Us $346.34 Daily
                    </div>
                </div>
                <div class="col-12 mt-md30 mt20 text-center">
                    <div class="col-12 f-md-24 f-18 w400 white-clr lh140">
                        Without Creating Videos | Without Uploading | Without Ads | No Upfront Cost at All
                    </div>
                </div>
            <div class="col-12 mt20 mt-md50 col-md-10 mx-auto">
               <img src="assets/images/productbox.webp" class="img-fluid d-block mx-auto img-animation" alt="Product">
            </div>
            <!-- <div class="col-12 mt20 mt-md50 f-18 f-md-20 lh140 w400 white-clr text-center lh140">
               This is An Exclusive Deal for <span class="w600 yellow-clr">"TubeTrafficAi"</span> Users Only...
            </div>
            <div class="col-md-10 mx-auto col-12 mt30 mt-md30">
               <div class="f-18 f-md-36 text-center probtn1" editabletype="button" style="z-index: 10;">
                     <a href="#buynow" class="text-center">Upgrade to TubeTrafficAi Agency Now</a>
               </div>
            </div> -->
         </div>
      </div>
   </div>
<!-- Proudly Section End -->
   <!-- Bonus Section Header Start -->
   <div class="bonus-header">
      <div class="container">
         <div class="row">
            <div class="col-12 col-md-10 mx-auto heading-bg text-center">
               <div class="f-24 f-md-36 lh140 w600"> When You Purchase TubeTrafficAi, You Also Get <br class="hidden-xs"> Instant Access To These 20 Exclusive Bonuses</div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus Section Header End -->

   <!-- Bonus #1 Section Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w600">BONUS 1</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus1.webp" class="img-fluid mx-auto d-block" alt="Bonus1">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">Resilience Video Upgrade  </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li class="w600">With this video course you will learn how to build mental strength to boost resilience and overcome 
                           any difficult situation in life.</li>
                        <li>Learning how to bounce back is teachable and learnable. </li>
                        <li>It is imperative to fully understand what resilience is, learn about its components, and learn about resilience 
                           boosting techniques.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #1 Section End -->

   <!-- Bonus #2 Section Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w600">BONUS 2</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30">
               <div class="row">
                  <div class="col-md-5 col-12 order-md-2">
                     <img src="assets/images/bonus2.webp" class="img-fluid mx-auto d-block" alt="Bonus2">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md0">
                     Facebook Ad Secrets Advance 
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li class="w600">While the FB Ad Secrets was made for beginners just starting in Facebook advertising, these videos answer 
                           the coming challenges you will face as you scale to spending hundreds or thousands of dollars per day while 
                           remaining profitable. </li>
                        <li>For beginners, you will usually start with a low budget to run your Facebook ads. But eventually when the time comes 
                           for you to scale your offer with a bigger budget, you will come across problems such as your ad account being disabled,
                            profit margin becomes thinner and things like when and how to split test your ads. </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #2 Section End -->

   <!-- Bonus #3 Section Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w600">BONUS 3</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus3.webp" class="img-fluid mx-auto d-block " alt="Bonus3">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     Modern Niche Marketing Video Upgrade 
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li class="w600">With this video guide you will learn everything that you need to know to properly assess niche ideas for 
                           their viability and how to drive targeted traffic to your niche website.</li>
                        <li>There is more competition these days which means that you really have to be smart to succeed. If you follow the advice 
                           in this guide then you will provide yourself with the maximum chance of success. Don’t believe the hype – niche marketing 
                           does work if you do it correctly.</li>
                        <li>There are niche websites being sold every day for large amounts of money. These websites are generating regular income 
                           and have a good following. </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #3 Section End -->

   <!-- Bonus #4 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w600">BONUS 4</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12 order-md-2">
                     <img src="assets/images/bonus4.webp" class="img-fluid mx-auto d-block " alt="Bonus4">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     Web Traffic Excellence
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li class="w600">
                        You can have the best product or the best service in the world but if you have no traffic – it’s all completely 
                        worthless.</li>
                        <li>This 5 - part video course will help new and experienced marketers generate huge amount of traffic from five 
                           different sources.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #4 End -->

   <!-- Bonus #5 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w600">BONUS 5</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus5.webp" class="img-fluid mx-auto d-block " alt="Bonus5">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     Video Marketing Blueprint v2 
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Who Else Wants To Use Videos To Market Their Business And Generate Massive Leads ... Starting Today?</li>
                        <li>Video marketing has become one of the highest rising and most popular traffic generations. You?ve made the right decision 
                           in investing in this course where you?ll learn how to get torrents of traffic in just a few easy steps.</li>
                        <li>Video Marketing Blueprint is the key to your marketing success. With it, you will find out how to grow your mailing 
                           list, generate massive traffic to your website, increase the conversion and purchase rate, of course, so much more.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #5 End -->

   <!-- CTA Button Section Start -->
   <div class="dark-cta-sec">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-md-12 col-12 text-center ">
               <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
               <div class="f-md-36 f-22 text-center  lh120 w600 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
               <div class="f-18 f-md-22 lh140 w400 text-center mt20 white-clr">
                  Use Special Coupon <span class="w600 yellow-clr">"Trafficadmin"</span> For <span class="w600 yellow-clr"> 32% Off</span> On Full Funnel
               </div>
            </div>
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab TubeTrafficAi + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
               <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-20 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
               <div class="countdown counter-white white-clr">
                  <div class="timer-label text-center">
                     <span class="f-31 f-md-60 timerbg oswald w600">01</span>
                     <span class="f-14 f-md-18 w500 ">Days</span>
                  </div>
                  <div class="timer-label text-center">
                     <span class="f-31 f-md-60 timerbg oswald w600">16</span>
                     <span class="f-14 f-md-18 w500 ">Hours</span>
                  </div>
                  <div class="timer-label text-center timer-mrgn">
                     <span class="f-31 f-md-60 timerbg oswald w600">59</span>
                     <span class="f-14 f-md-18 w500 ">Mins</span>
                  </div>
                  <div class="timer-label text-center ">
                     <span class="f-31 f-md-60 timerbg oswald w600">37</span>
                     <span class="f-14 f-md-18 w500 ">Sec</span>
                  </div>
               </div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>
   <!-- CTA Button Section End -->

   <!-- Bonus #6 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w600">BONUS 6</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12 order-md-2">
                     <img src="assets/images/bonus6.webp" class="img-fluid mx-auto d-block " alt="Bonus6">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     Site Speed Secrets Video Upgrade 
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Discover how you can finally speed up your website and increase your conversion rates. All the information you need is 
                           here, and nothing has been left out in this course! </li>
                        <li>Not all web hosts are created equal – make sure you sign up for a fast web host that will help you reach your business 
                           goals.  </li>
                        <li>Know how to use and analyze the results of the best website speed testing tools out there.  </li>
                        <li>Never underestimate the power of caching ever again – it will help your site load much faster than ever before. </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #6 End -->

   <!-- Bonus #7 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w600">BONUS 7</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus7.webp" class="img-fluid mx-auto d-block " alt="Bonus7">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     Shopify Traffic
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Compared to 10 years ago, starting an online e-store is not as hard anymore today. Driven by the dream of having an internet
                            lifestyle, and the rewarding monthly income, everyone is triggered to own a business for a better life.</li>
                        <li>With all the platforms and opportunities available, it's easy to kick start an online business anytime you want. Even if you
                            have little budget, you can start an online shop on a small scale. </li>
                        <li>Shopify Traffic is a series of training course that will teach you how to generate traffic to your Shopify e-store with
                            effective methods and platform from personal experience and culmination of researches together with years of studies. </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #7 End -->

   <!-- Bonus #8 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w600">BONUS 8</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="assets/images/bonus8.webp" class="img-fluid mx-auto d-block " alt="Bonus8">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     Content Syndication 
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Content Syndication Videos is a series of 40 On-Screen, Easy-To-Follow Video Tutorials on how to market and publish your 
                           content expertly.</li>
                        <li>The Internet is full of 'me too marketers' and what makes this frustrating is that even if you're a genuine expert, it's not
                            a guarantee you'll be successful with marketing yourself. </li>
                        <li>This course will help you to get an unfair advantage and stay ahead of the competition. Lessons are short, simple and
                            direct.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #8 End -->

   <!-- Bonus #9 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w600">BONUS 9</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus9.webp" class="img-fluid mx-auto d-block " alt="Bonus9">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     Kick Ass Affiliate 
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Kick Ass Affiliate is a video course that will enlighten you how to become a super affiliate even if you don’t have a 
                           huge email list or you don’t have a huge following on your blog or in your social media circle.</li>
                        <li>If you are thinking that you can’t beat those deadbeat affiliates that keep on topping the leader board, inside the 
                           course are you will learn how to change that mentality. </li>
                        <li>You are also about to learn the principle of offering crazy bonuses and how to set this up effectively in your affiliate
                            marketing campaign. If you already have a mailing list, learn how to create a persuasive approach that will make your
                             subscribers buy the product you offer </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #9 End -->

   <!-- Bonus #10 start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w600">BONUS 10</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="assets/images/bonus10.webp" class="img-fluid mx-auto d-block " alt="Bonus10">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     100 Split Tests 
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>100 Split Tests is the result of performing over thousands of different split tests across all selling and advertising mediums online - and we've distilled it into one hundred tweaks and 'hacks' you can do to immediately boost your conversion rates!</li>
                        <li>Split testing has been known to drive some pretty compelling results, but that all assumes one thing: that it’s done correctly. There are numerous aspects that you can look into for split testing, and they can get complicated.</li>
                        <li>Is your goal to drive traffic? Is it clicks? Or even opt-ins? A clear result is determined by setting a clear goal at the start. An email subject line that wins at open rate may be different than the winner for click-through rates. </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #10 End -->

   <!-- CTA Button Section Start -->
   <div class="dark-cta-sec">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-md-12 col-12 text-center ">
               <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
               <div class="f-md-36 f-22 text-center  lh120 w600 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
               <div class="f-18 f-md-22 lh140 w400 text-center mt20 white-clr">
                  Use Special Coupon <span class="w600 yellow-clr">"Trafficadmin"</span> For <span class="w600 yellow-clr"> 32% Off</span> On Full Funnel
               </div>
            </div>
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab TubeTrafficAi + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
               <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-20 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
               <div class="countdown counter-white white-clr">
                  <div class="timer-label text-center">
                     <span class="f-31 f-md-60 timerbg oswald w600">01</span>
                     <span class="f-14 f-md-18 w500 ">Days</span>
                  </div>
                  <div class="timer-label text-center">
                     <span class="f-31 f-md-60 timerbg oswald w600">16</span>
                     <span class="f-14 f-md-18 w500 ">Hours</span>
                  </div>
                  <div class="timer-label text-center timer-mrgn">
                     <span class="f-31 f-md-60 timerbg oswald w600">59</span>
                     <span class="f-14 f-md-18 w500 ">Mins</span>
                  </div>
                  <div class="timer-label text-center ">
                     <span class="f-31 f-md-60 timerbg oswald w600">37</span>
                     <span class="f-14 f-md-18 w500 ">Sec</span>
                  </div>
               </div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>
   <!-- CTA Button Section End -->

   <!-- Bonus #11 start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w600">BONUS 11</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus11.webp" class="img-fluid mx-auto d-block " alt="Bonus11">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     Copy Cash Secrets 
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li><b>Now You Can Write Compelling Sales Letters That Effectively Generate A Call-To-Action And Potentially Increase Sales Leads On A Regular Basis!</b></li>
                        <li>Words is powerful tool if utilized properly by anyone in marketing or sales. Keyword for ‘guarantee’ is an essential component for most good eCommerce marketing copy. </li>
                        <li>The word “Guaranteed” appeals to consumer emotional triggers like safety and trust. The word works like a safety net which is a promise made by a retailer to a consumer that they will be pleased with their purchase. </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #11 End -->

   <!-- Bonus #12 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w600">BONUS 12</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="assets/images/bonus12.webp" class="img-fluid mx-auto d-block " alt="Bonus12">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     Traffic Babylon
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li><b>If You Are Looking To Send Your Website Traffic Even Further Through The Roof, then here's a short, 12 part video series that reveals to you 6 paid and 6 free traffic sources to SKYROCKET your traffic!</b></li>
                        <li>YES! Traffic is indeed the life-blood of your website. But the question is that, how many traffic generating ideas you have for you to perform on your campaign?</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #12 End -->

   <!-- Bonus #13 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w600">BONUS 13</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus13.webp" class="img-fluid mx-auto d-block " alt="Bonus13">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     Viral Profit Machine 
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Discover How YOU Can Make MAXIMUM Profits With Minimum Effort By Getting Other People To Willingly Market For You!</li>
                        <li>Unlock The Secrets To Getting Other People To Talk About You And Your Business Willingly Whether They're Paid To Do It Or Not!</li>
                        <li>Announcing: Viral Profit Machine = Here's a sneak peak of what you'll find in this 50 minutes 54 seconds exclusive audio session:</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #13 End -->

   <!-- Bonus #14 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w600">BONUS 14</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="assets/images/bonus14.webp" class="img-fluid mx-auto d-block " alt="Bonus14">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     Public Domain Profits 
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>If you're sick and tired of not making any money online then you need to discover the... The Lazy Internet Marketer's Way To Riches</li>
                        <li>Now you too can become a money making internet marketer without busting your butt with super hard work or extra long hours...</li>
                        <li>If you've always dreamed of making more money while being able to do less and less work, THIS is what you've been searching for...</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #14 End -->

   <!-- Bonus #15 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w600">BONUS 15</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus15.webp" class="img-fluid mx-auto d-block " alt="Bonus15">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     Keyword Goldrush 
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Keywords are absolutely vital for you to succeed in making money online.</li>
                        <li>Your keywords are how your websites are found by your potential customers using the search engines.</li>
                        <li>If you are not optimised for the right keywords, and you do not rank high for these keywords, then you are going to struggle to get the traffic that you want for making money.</li>
                        <li>This program is all about how to find the keywords you want to target, how to use them, and most importantly, how to profit from them.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #15 End -->

   <!-- CTA Button Section Start -->
   <div class="dark-cta-sec">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-md-12 col-12 text-center ">
               <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
               <div class="f-md-36 f-22 text-center  lh120 w600 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
               <div class="f-18 f-md-22 lh140 w400 text-center mt20 white-clr">
               Use Special Coupon <span class="w600 yellow-clr">"Trafficadmin"</span> For <span class="w600 yellow-clr"> 32% Off</span> On Full Funnel
               </div>
            </div>
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab TubeTrafficAi + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
               <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-20 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
               <div class="countdown counter-white white-clr">
                  <div class="timer-label text-center">
                     <span class="f-31 f-md-60 timerbg oswald w600">01</span>
                     <span class="f-14 f-md-18 w500 ">Days</span>
                  </div>
                  <div class="timer-label text-center">
                     <span class="f-31 f-md-60 timerbg oswald w600">16</span>
                     <span class="f-14 f-md-18 w500 ">Hours</span>
                  </div>
                  <div class="timer-label text-center timer-mrgn">
                     <span class="f-31 f-md-60 timerbg oswald w600">59</span>
                     <span class="f-14 f-md-18 w500 ">Mins</span>
                  </div>
                  <div class="timer-label text-center ">
                     <span class="f-31 f-md-60 timerbg oswald w600">37</span>
                     <span class="f-14 f-md-18 w500 ">Sec</span>
                  </div>
               </div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>
   <!-- CTA Button Section End -->

   <!-- Bonus #16 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w600">BONUS 16</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="assets/images/bonus16.webp" class="img-fluid mx-auto d-block " alt="Bonus16">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     SEO Reborn
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Finally A Fool Proof Way Of Ranking On The First Page Of Google Has Been Cracked And Revealed By A Former Internet Newbie Turned SEO King!</li>
                        <li>Introducing A Complete Underground Secret SEO Solution In Ranking Your Website On The First Page Of Google Without Paying A Dime For Traffic!</li>
                        <li>Finally Rank Higher On The Search Engines Without Having To Worry About Paid Traffic. Take Advantage Of First Page Google Rankings And Easily Beat Your Competition! It Is Time To DOMINATE The Search Engines And Start Being The BIG Player In Your Market!</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #16 End -->

   <!-- Bonus #17 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w600">BONUS 17</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus17.webp" class="img-fluid mx-auto d-block " alt="Bonus17">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     Membership Mastery
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>The Lazy Man's Guide To Creating A Membership Site From Scratch : Now You Can Kick The Technical Roadblocks Out of Your Way And Focus On Building Your Own Membership Site - the Right Way! Look over the shoulder of a real membership owner to see how you can setup a membership site quickly from scratch.</li>
                        <li>On the Internet and in the world around you, members only clubs are sprouting everywhere, whether paid or free. This is because Membership sites generally provide access to something that is unique from other sites. They also build communities of people alike. In fact most people want to improve themselves, improve their business... you get the point. So they're willing to join membership sites that help them do that.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #17 End -->

   <!-- Bonus #18 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w600">BONUS 18</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="assets/images/bonus18.webp" class="img-fluid mx-auto d-block " alt="Bonus18">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     Marketing Stomp 
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Discover the Jealously Guarded Secrets of Seemingly Ordinary Folks Generating Extraordinary Incomes With Just a Computer and Internet Connection!</li>
                        <li>There are many ways to make money online and one of these profitable concepts is by selling your own digital products that many people needs.</li>
                        <li>If you are new to internet marketing or online entrepreneurship, having a guide in doing this for the first time is a huge help.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #18 End -->

   <!-- Bonus #19 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w600">BONUS 19</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus19.webp" class="img-fluid mx-auto d-block " alt="Bonus19">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     Customer Product Portals 
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>See how easy it is to put together a central repository where your customers can access all of the products they have purchased from you. At the same time market all of your other products to your buyers passively!</li>
                        <li>Indeed, there are so many ways to make money online. And of those effective ways is selling digital goods because of it's several advantages in terms of delivering the product.</li>
                        <li>But the thing though is that, selling these digital products also has a flaw and that is it can easily be shared or steal by some hackers. </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #19 End -->

   <!-- Bonus #20 start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w600">BONUS 20</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="assets/images/bonus20.webp" class="img-fluid mx-auto d-block " alt="Bonus20">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w700 lh140 bonus-title-color">
                     Support Desk System 
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li><b>How To Set Up A Professional Email Support Ticket System!</b></li>
                        <li>Reduce Your Refund Rates, Keep Your Buyers Happy And Never Worry About Not Answering Your Customer's Emails Again!</li>
                        <li>Now You Can Side Aside The Mundane Task Of Answering Emails, Set Up A Support Desk For Free And Give Yourself Time To Progress Your Business!</li>
                        <li>8-part video tutorials reveal how you can install a customer support system in just less than an hour and start using it to keep in contact with your prospects and customers!</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #20 End -->

   <!-- Huge Woth Section Start -->
   <div class="huge-area mt30 mt-md10">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-12 text-center">
               <div class="f-md-65 f-40 lh120 w600 white-clr">That’s Huge Worth of</div>
               <br>
               <div class="f-md-60 f-40 lh120 w700 yellow-clr">$2285!</div>
            </div>
         </div>
      </div>
   </div>
   <!-- Huge Worth Section End -->

   <!-- text Area Start -->
   <div class="white-section pb0">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-12 text-center">
               <div class="f-md-40 f-28 lh140 w600">So what are you waiting for? You have a great opportunity ahead + My 20 Bonus Products are making it a completely NO Brainer!!</div>
            </div>
         </div>
      </div>
   </div>
   <!-- text Area End -->

   <!-- CTA Button Section Start -->
   <div class="cta-btn-section">
      <div class="container">
         <div class="row">
         <div class="f-18 f-md-22 lh140 w500 text-center mt20 black-clr">
                     Use Special Coupon <span class="w600 red-clr">"Trafficadmin"</span> For <span class="w600 red-clr"> 32% Off</span> On Full Funnel
               </div>
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md30">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab TubeTrafficAi + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-20 f-20 w500 text-center black">My Exclusive Bonuses Are Expiring in...</h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 col-md-10 mx-auto col-12 text-center">
               <div class="countdown counter-black">
                  <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w600">01</span><br><span class="f-14 f-md-18 w500  ">Days</span> </div>
                  <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w600">16</span><br><span class="f-14 f-md-18 w500 ">Hours</span> </div>
                  <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald w600">59</span><br><span class="f-14 f-md-18 w500 ">Mins</span> </div>
                  <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald w600">37</span><br><span class="f-14 f-md-18 w500 ">Sec</span> </div>
               </div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div></div></div>
   <!-- CTA Button Section End -->


   <!--Footer Section Start -->
   <div class="footer-section">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 800 131.43" style="max-height:75px;">
                        <defs>
                            <style>
                            .cls-1 {
                                fill: #fff;
                            }
        
                            .cls-2 {
                                fill: #fd2f15;
                            }
        
                            .cls-3 {
                                fill: #fbbf33;
                            }
                            </style>
                        </defs>
                        <g id="Layer_1-2" data-name="Layer 1">
                            <g>
                            <g>
                                <path class="cls-3" d="m105.61,78.06l40.05,23.12c6.01,3.47,13.51-.87,13.51-7.8v-46.25c0-6.93-7.51-11.27-13.51-7.8l-40.05,23.12c-6.01,3.47-6.01,12.14,0,15.6Z"/>
                                <g>
                                <rect class="cls-3" x="26.92" y="58.86" width="16.03" height="46.81"/>
                                <path class="cls-2" d="m125.22,26.34h-7.16v46.06c0,32.6-26.43,59.03-59.03,59.03H0v-65.94C0,32.89,26.43,6.45,59.03,6.45h38.52l-12.38,19.88h-26.14c-21.59,0-39.16,17.57-39.16,39.15v46.07h39.16c21.59,0,39.16-17.57,39.16-39.16V26.34h-5.79L108.81,0l16.41,26.34Z"/>
                                <path class="cls-3" d="m67.04,46.21h-16.02v59.46h8.01c2.76,0,5.45-.35,8.01-.99v-58.47Z"/>
                                <path class="cls-3" d="m90.77,33.56h-16.03v68.14c7.57-4.07,13.39-10.99,16.03-19.31v-48.83Z"/>
                                </g>
                            </g>
                            <g>
                                <g>
                                <path class="cls-1" d="m195.69,38.68v-5.58h49.35v5.58h-21.57v62.48h-6.21v-62.48h-21.57Z"/>
                                <path class="cls-1" d="m283.29,81.65v-31.54h5.92v51.05h-5.92v-8.47h-.47c-1.2,2.61-3.09,4.79-5.68,6.53-2.59,1.74-5.76,2.61-9.5,2.61-3.26,0-6.15-.71-8.67-2.14-2.53-1.43-4.51-3.53-5.95-6.3-1.44-2.77-2.16-6.17-2.16-10.2v-33.07h5.92v32.7c0,3.99,1.17,7.2,3.52,9.62,2.35,2.43,5.36,3.64,9.04,3.64,2.3,0,4.52-.55,6.65-1.66,2.13-1.11,3.88-2.73,5.25-4.87,1.37-2.14,2.06-4.77,2.06-7.89Z"/>
                                <path class="cls-1" d="m306.05,101.16V33.09h5.95v26.75h.53c.66-1.53,1.62-3.1,2.87-4.72,1.25-1.62,2.9-2.97,4.95-4.07,2.05-1.1,4.64-1.65,7.76-1.65,4.19,0,7.86,1.1,11.03,3.31,3.17,2.21,5.64,5.28,7.41,9.24,1.77,3.95,2.66,8.56,2.66,13.81s-.88,9.89-2.64,13.86c-1.76,3.97-4.22,7.06-7.38,9.27-3.16,2.22-6.82,3.32-10.98,3.32-3.1,0-5.69-.55-7.76-1.66-2.07-1.11-3.74-2.48-5-4.1-1.26-1.63-2.24-3.22-2.92-4.77h-.73v9.47h-5.75Zm5.85-25.49c0,4.12.61,7.77,1.84,10.95,1.23,3.18,3,5.67,5.32,7.48,2.31,1.81,5.12,2.71,8.42,2.71s6.26-.94,8.61-2.81c2.35-1.87,4.13-4.4,5.35-7.59,1.22-3.19,1.83-6.77,1.83-10.73s-.6-7.46-1.81-10.6c-1.21-3.15-2.99-5.64-5.33-7.49-2.35-1.85-5.23-2.78-8.64-2.78s-6.14.89-8.46,2.68c-2.32,1.78-4.08,4.24-5.3,7.38-1.22,3.14-1.83,6.74-1.83,10.82Z"/>
                                <path class="cls-1" d="m382.86,102.22c-4.76,0-8.88-1.11-12.36-3.34-3.48-2.23-6.16-5.32-8.06-9.27s-2.84-8.51-2.84-13.68.95-9.71,2.84-13.71c1.89-4,4.52-7.13,7.88-9.41,3.36-2.27,7.24-3.41,11.65-3.41,2.77,0,5.44.5,8.01,1.51,2.57,1.01,4.88,2.55,6.93,4.62,2.05,2.07,3.67,4.68,4.87,7.81,1.2,3.14,1.79,6.84,1.79,11.12v2.92h-39.88v-5.22h33.83c0-3.28-.66-6.23-1.98-8.86-1.32-2.63-3.15-4.7-5.48-6.23-2.34-1.53-5.04-2.29-8.09-2.29-3.23,0-6.08.86-8.54,2.59-2.46,1.73-4.38,4.01-5.77,6.85-1.38,2.84-2.09,5.94-2.11,9.31v3.12c0,4.05.7,7.59,2.11,10.62,1.41,3.02,3.41,5.37,6,7.03,2.59,1.66,5.66,2.49,9.21,2.49,2.41,0,4.54-.38,6.36-1.13,1.83-.75,3.37-1.77,4.62-3.04,1.25-1.27,2.2-2.68,2.84-4.2l5.62,1.83c-.78,2.15-2.04,4.13-3.81,5.95-1.76,1.82-3.95,3.27-6.58,4.37-2.63,1.1-5.65,1.65-9.06,1.65Z"/>
                                <path class="cls-1" d="m412.07,44.96v-11.86h55.9v11.86h-20.84v56.2h-14.22v-56.2h-20.84Z"/>
                                <path class="cls-1" d="m471.62,101.16v-51.05h13.73v8.91h.53c.93-3.17,2.49-5.57,4.69-7.2,2.19-1.63,4.72-2.44,7.58-2.44.71,0,1.47.04,2.29.13.82.09,1.54.21,2.16.37v12.56c-.66-.2-1.58-.38-2.76-.53-1.17-.15-2.25-.23-3.22-.23-2.08,0-3.94.45-5.57,1.35-1.63.9-2.91,2.14-3.86,3.74-.94,1.6-1.41,3.43-1.41,5.52v28.88h-14.16Z"/>
                                <path class="cls-1" d="m523,102.12c-3.26,0-6.16-.57-8.71-1.71-2.55-1.14-4.56-2.84-6.03-5.08-1.47-2.25-2.21-5.06-2.21-8.42,0-2.84.52-5.22,1.56-7.15,1.04-1.93,2.46-3.48,4.25-4.65,1.79-1.17,3.84-2.06,6.13-2.66,2.29-.6,4.7-1.02,7.23-1.26,2.97-.31,5.36-.6,7.18-.88,1.82-.28,3.13-.69,3.95-1.25.82-.55,1.23-1.37,1.23-2.46v-.2c0-2.1-.66-3.73-1.98-4.89-1.32-1.15-3.19-1.73-5.6-1.73-2.55,0-4.58.56-6.08,1.68-1.51,1.12-2.5,2.52-2.99,4.2l-13.09-1.06c.66-3.1,1.97-5.79,3.92-8.06,1.95-2.27,4.47-4.02,7.56-5.25,3.09-1.23,6.67-1.84,10.75-1.84,2.84,0,5.55.33,8.16,1,2.6.66,4.92,1.7,6.95,3.09,2.03,1.4,3.63,3.19,4.8,5.37,1.17,2.18,1.76,4.79,1.76,7.83v34.43h-13.43v-7.08h-.4c-.82,1.6-1.92,3-3.29,4.2-1.37,1.21-3.02,2.15-4.95,2.82-1.93.68-4.15,1.01-6.68,1.01Zm4.05-9.77c2.08,0,3.92-.42,5.52-1.25s2.85-1.96,3.76-3.37c.91-1.42,1.36-3.02,1.36-4.82v-5.42c-.44.29-1.05.55-1.81.78s-1.62.44-2.58.63c-.95.19-1.91.35-2.86.5-.95.14-1.82.27-2.59.38-1.66.24-3.11.63-4.35,1.16-1.24.53-2.21,1.25-2.89,2.14-.69.9-1.03,2.01-1.03,3.34,0,1.93.7,3.4,2.11,4.4,1.41,1.01,3.2,1.51,5.37,1.51Z"/>
                                <path class="cls-1" d="m580.03,50.11h10.17v10.63h-10.17v40.41h-14.12v-40.41h-7.21v-10.63h7.21v-3.69c0-3.7.72-6.77,2.18-9.21,1.45-2.44,3.44-4.26,5.97-5.48,2.53-1.22,5.4-1.83,8.61-1.83,2.17,0,4.15.17,5.97.5,1.8.33,3.15.63,4.04.9l-2.53,10.63c-.55-.18-1.23-.34-2.04-.5s-1.64-.23-2.48-.23c-2.08,0-3.53.48-4.35,1.45-.83.96-1.23,2.31-1.23,4.04v3.42Z"/>
                                <path class="cls-1" d="m616.12,50.11h10.17v10.63h-10.17v40.41h-14.12v-40.41h-7.21v-10.63h7.21v-3.69c0-3.7.72-6.77,2.18-9.21,1.45-2.44,3.44-4.26,5.97-5.48,2.53-1.22,5.4-1.83,8.61-1.83,2.17,0,4.15.17,5.97.5,1.8.33,3.15.63,4.04.9l-2.53,10.63c-.55-.18-1.23-.34-2.04-.5-.81-.15-1.64-.23-2.48-.23-2.08,0-3.53.48-4.35,1.45-.83.96-1.23,2.31-1.23,4.04v3.42Z"/>
                                <path class="cls-1" d="m642.05,43.53c-2.11,0-3.9-.7-5.4-2.09-1.5-1.4-2.24-3.08-2.24-5.05s.75-3.66,2.26-5.07c1.51-1.41,3.3-2.11,5.38-2.11s3.91.7,5.4,2.09c1.5,1.4,2.24,3.08,2.24,5.05s-.75,3.66-2.24,5.07c-1.5,1.41-3.3,2.11-5.4,2.11Zm-7.11,57.63v-51.05h14.16v51.05h-14.16Z"/>
                                <path class="cls-1" d="m683.46,102.15c-5.23,0-9.72-1.11-13.48-3.34-3.76-2.23-6.64-5.32-8.64-9.29-2.01-3.97-3.01-8.53-3.01-13.69s1.01-9.82,3.04-13.78c2.03-3.95,4.91-7.05,8.66-9.27,3.74-2.23,8.2-3.34,13.36-3.34,4.45,0,8.35.81,11.7,2.43,3.35,1.62,5.99,3.89,7.94,6.81,1.95,2.92,3.02,6.36,3.22,10.3h-13.36c-.38-2.55-1.37-4.6-2.97-6.16-1.61-1.56-3.71-2.34-6.3-2.34-2.19,0-4.1.59-5.73,1.78-1.63,1.19-2.9,2.91-3.81,5.17s-1.36,5-1.36,8.21.45,6.03,1.35,8.31c.9,2.28,2.17,4.02,3.81,5.22,1.64,1.2,3.56,1.79,5.75,1.79,1.62,0,3.07-.33,4.37-1,1.3-.66,2.37-1.63,3.22-2.91.85-1.27,1.41-2.81,1.68-4.6h13.36c-.22,3.9-1.28,7.33-3.17,10.29-1.89,2.96-4.5,5.27-7.83,6.93-3.32,1.66-7.26,2.49-11.8,2.49Z"/>
                                </g>
                                <g>
                                <path class="cls-3" d="m800,37.72c0,4.31-3.5,7.81-7.82,7.81s-7.81-3.5-7.81-7.81,3.5-7.82,7.81-7.82,7.82,3.5,7.82,7.82Z"/>
                                <path class="cls-2" d="m736.11,85.52c-.09,0-.18,0-.27-.01-.09,0-.18-.02-.26-.03.18.01.35.05.53.05Z"/>
                                <path class="cls-2" d="m784.37,50.18v32.89c0,1.31-1.05,2.37-2.35,2.43-.03,0-.07.02-.11.02h-.37l-18.68-41.36-3.1-6.86c-2.03-4.5-6.52-7.4-11.47-7.4s-9.42,2.9-11.46,7.4l-11.96,26.47-10.4,23.03c-3.04,6.73,1.88,14.36,9.28,14.36h66.01c5.65,0,10.25-4.58,10.25-10.24v-40.74h-15.63Zm-48.25,35.34c-.09,0-.18,0-.27-.01-.09,0-.18-.02-.26-.03-2.25-.33-3.65-2.75-2.67-4.91l12.93-28.64c.95-2.1,3.93-2.1,4.89,0l15.17,33.59h-29.79Z"/>
                                </g>
                            </g>
                            </g>
                        </g>
                        </svg>
                    <div editabletype="text" class="f-16 f-md-18 w400 lh140 mt20 white-clr text-center">Note: This site is not a part of the
                      Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; 
                      INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
                      <div class=" mt20 mt-md40 white-clr text-center"> <script type="text/javascript" src="https://warriorplus.com/o2/disclaimer/jrrjv5" defer=""></script><div class="wplus_spdisclaimer f-16 f-md-16 w400 lh140 white-clr"><span class="w600">Disclaimer : </span> WarriorPlus is used to help manage the sale of products on this site. While WarriorPlus helps facilitate the sale, all payments are made directly to the product vendor and NOT WarriorPlus. Thus, all product questions, support inquiries and/or refund requests must be sent to the vendor. WarriorPlus's role should not be construed as an endorsement, approval or review of these products or any claim, statement or opinion used in the marketing of these products.</div></div>
                </div>
                <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                    <div class="f-14 f-md-16 w400 lh160 white-clr text-xs-center">Copyright © TubeTrafficAi 2023</div>
                    <ul class="footer-ul w400 f-14 f-md-16 white-clr text-center text-md-right">
                        <li><a href="https://support.bizomart.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                        <li><a href="http://tubetrafficai.com/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                        <li><a href="http://tubetrafficai.com/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                        <li><a href="http://tubetrafficai.com/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                        <li><a href="http://tubetrafficai.com/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                        <li><a href="http://tubetrafficai.com/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                        <li><a href="http://tubetrafficai.com/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
   <!--Footer Section End -->
 
   <!--Footer Section End -->
   <!-- timer --->
   <?php
   if ($now < $exp_date) {
   ?>
      <script type="text/javascript">
         // Count down milliseconds = server_end - server_now = client_end - client_now
         var server_end = <?php echo $exp_date; ?> * 1000;
         var server_now = <?php echo time(); ?> * 1000;
         var client_now = new Date().getTime();
         var end = server_end - server_now + client_now; // this is the real end time

         var noob = $('.countdown').length;

         var _second = 1000;
         var _minute = _second * 60;
         var _hour = _minute * 60;
         var _day = _hour * 24
         var timer;

         function showRemaining() {
            var now = new Date();
            var distance = end - now;
            if (distance < 0) {
               clearInterval(timer);
               document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
               return;
            }

            var days = Math.floor(distance / _day);
            var hours = Math.floor((distance % _day) / _hour);
            var minutes = Math.floor((distance % _hour) / _minute);
            var seconds = Math.floor((distance % _minute) / _second);
            if (days < 10) {
               days = "0" + days;
            }
            if (hours < 10) {
               hours = "0" + hours;
            }
            if (minutes < 10) {
               minutes = "0" + minutes;
            }
            if (seconds < 10) {
               seconds = "0" + seconds;
            }
            var i;
            var countdown = document.getElementsByClassName('countdown');
            for (i = 0; i < noob; i++) {
               countdown[i].innerHTML = '';

               if (days) {
                  countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w600">' + days + '</span><br><span class="f-14 f-md-18 ">Days</span> </div>';
               }

               countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w600">' + hours + '</span><br><span class="f-14 f-md-18 w500 ">Hours</span> </div>';

               countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald w600">' + minutes + '</span><br><span class="f-14 f-md-18 w500 ">Mins</span> </div>';

               countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald w600">' + seconds + '</span><br><span class="f-14 f-md-18 w500">Sec</span> </div>';
            }

         }
         timer = setInterval(showRemaining, 1000);
      </script>
   <?php
   } else {
      echo "Times Up";
   }
   ?>
   <!--- timer end-->

   
   </script>
</body>

</html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
   <!-- Tell the browser to be responsive to screen width -->
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
   <meta name="title" content="TubeTrafficAi Special Bonuses">
   <meta name="description" content="World's First Google-Bard Powered AI App That *Legally* Hacks Into YouTube's 800 Million Videos…">
   <meta name="keywords" content="">
   <meta property="og:image" content="https://www.tubetrafficai.com/special-bonus/thumbnail.png">
   <meta name="language" content="English">
   <meta name="revisit-after" content="1 days">
   <meta name="author" content="Dr. Amit Pareek">
   <!-- Open Graph / Facebook -->
   <meta property="og:type" content="website">
   <meta property="og:title" content="TubeTrafficAi Special Bonuses">
   <meta property="og:description" content="World's First Google-Bard Powered AI App That *Legally* Hacks Into YouTube's 800 Million Videos…">
   <meta property="og:image" content="https://www.tubetrafficai.com/special-bonus/thumbnail.png">
   <!-- Twitter -->
   <meta property="twitter:card" content="summary_large_image">
   <meta property="twitter:title" content="TubeTrafficAi Special Bonuses">
   <meta property="twitter:description" content="World's First Google-Bard Powered AI App That *Legally* Hacks Into YouTube's 800 Million Videos…">
   <meta property="twitter:image" content="https://www.tubetrafficai.com/special-bonus/thumbnail.png">
   <title>TubeTrafficAi Special Bonuses</title>

   <link rel="preconnect" href="https://fonts.googleapis.com">
   <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
   <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&amp;family=Poppins:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&amp;display=swap" rel="stylesheet">
   
   <!-- Shortcut Icon  -->
   <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
   <!-- Css CDN Load Link -->
   <link rel="stylesheet" href="../common_assets/css/bootstrap.min.css" type="text/css">
   <link rel="stylesheet" href="../common_assets/css/general.css">
   <link rel="stylesheet" type="text/css" href="assets/css/bonus-style.css">
   <link rel="stylesheet" href="assets/css/style.css" type="text/css">
   <script src="../common_assets/js/jquery.min.js"></script>
</head>

<body>
   <!-- New Timer  Start-->
   <?php
   $date = 'September 16 2023 11:00 AM EST';
   $exp_date = strtotime($date);
   $now = time();

   if ($now < $exp_date) {
   ?>
   <?php
   } else {
      echo "Times Up";
   }
   ?>
   <!-- New Timer End -->
   <?php
   if (!isset($_GET['afflink'])) {
      $_GET['afflink'] = 'https://warriorplus.com/o2/a/djw1w2/0';
      $_GET['name'] = 'Dr. Amit Pareek';
   }
   ?>
   <div class="main-header">
      <div class="header-nav">
         <!-- container -->
         <div class="conatiner">
               <div class="row">
                  <div class="col-12 text-center">
                  <div class="text-center">
                     <div class="f-md-32 f-20 lh140 w500 white-clr d-block d-md-flex align-items-center justify-content-center">
                        <span class="w600 yellow-clr"><?php echo $_GET['name']; ?>'s</span> &nbsp;special bonus for &nbsp;
                        <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 800 131.43" style="max-height:75px;">
                           <defs>
                              <style>
                              .cls-1 {
                                 fill: #fff;
                              }
         
                              .cls-2 {
                                 fill: #fd2f15;
                              }
         
                              .cls-3 {
                                 fill: #fbbf33;
                              }
                              </style>
                           </defs>
                           <g id="Layer_1-2" data-name="Layer 1">
                              <g>
                              <g>
                                 <path class="cls-3" d="m105.61,78.06l40.05,23.12c6.01,3.47,13.51-.87,13.51-7.8v-46.25c0-6.93-7.51-11.27-13.51-7.8l-40.05,23.12c-6.01,3.47-6.01,12.14,0,15.6Z"/>
                                 <g>
                                 <rect class="cls-3" x="26.92" y="58.86" width="16.03" height="46.81"/>
                                 <path class="cls-2" d="m125.22,26.34h-7.16v46.06c0,32.6-26.43,59.03-59.03,59.03H0v-65.94C0,32.89,26.43,6.45,59.03,6.45h38.52l-12.38,19.88h-26.14c-21.59,0-39.16,17.57-39.16,39.15v46.07h39.16c21.59,0,39.16-17.57,39.16-39.16V26.34h-5.79L108.81,0l16.41,26.34Z"/>
                                 <path class="cls-3" d="m67.04,46.21h-16.02v59.46h8.01c2.76,0,5.45-.35,8.01-.99v-58.47Z"/>
                                 <path class="cls-3" d="m90.77,33.56h-16.03v68.14c7.57-4.07,13.39-10.99,16.03-19.31v-48.83Z"/>
                                 </g>
                              </g>
                              <g>
                                 <g>
                                 <path class="cls-1" d="m195.69,38.68v-5.58h49.35v5.58h-21.57v62.48h-6.21v-62.48h-21.57Z"/>
                                 <path class="cls-1" d="m283.29,81.65v-31.54h5.92v51.05h-5.92v-8.47h-.47c-1.2,2.61-3.09,4.79-5.68,6.53-2.59,1.74-5.76,2.61-9.5,2.61-3.26,0-6.15-.71-8.67-2.14-2.53-1.43-4.51-3.53-5.95-6.3-1.44-2.77-2.16-6.17-2.16-10.2v-33.07h5.92v32.7c0,3.99,1.17,7.2,3.52,9.62,2.35,2.43,5.36,3.64,9.04,3.64,2.3,0,4.52-.55,6.65-1.66,2.13-1.11,3.88-2.73,5.25-4.87,1.37-2.14,2.06-4.77,2.06-7.89Z"/>
                                 <path class="cls-1" d="m306.05,101.16V33.09h5.95v26.75h.53c.66-1.53,1.62-3.1,2.87-4.72,1.25-1.62,2.9-2.97,4.95-4.07,2.05-1.1,4.64-1.65,7.76-1.65,4.19,0,7.86,1.1,11.03,3.31,3.17,2.21,5.64,5.28,7.41,9.24,1.77,3.95,2.66,8.56,2.66,13.81s-.88,9.89-2.64,13.86c-1.76,3.97-4.22,7.06-7.38,9.27-3.16,2.22-6.82,3.32-10.98,3.32-3.1,0-5.69-.55-7.76-1.66-2.07-1.11-3.74-2.48-5-4.1-1.26-1.63-2.24-3.22-2.92-4.77h-.73v9.47h-5.75Zm5.85-25.49c0,4.12.61,7.77,1.84,10.95,1.23,3.18,3,5.67,5.32,7.48,2.31,1.81,5.12,2.71,8.42,2.71s6.26-.94,8.61-2.81c2.35-1.87,4.13-4.4,5.35-7.59,1.22-3.19,1.83-6.77,1.83-10.73s-.6-7.46-1.81-10.6c-1.21-3.15-2.99-5.64-5.33-7.49-2.35-1.85-5.23-2.78-8.64-2.78s-6.14.89-8.46,2.68c-2.32,1.78-4.08,4.24-5.3,7.38-1.22,3.14-1.83,6.74-1.83,10.82Z"/>
                                 <path class="cls-1" d="m382.86,102.22c-4.76,0-8.88-1.11-12.36-3.34-3.48-2.23-6.16-5.32-8.06-9.27s-2.84-8.51-2.84-13.68.95-9.71,2.84-13.71c1.89-4,4.52-7.13,7.88-9.41,3.36-2.27,7.24-3.41,11.65-3.41,2.77,0,5.44.5,8.01,1.51,2.57,1.01,4.88,2.55,6.93,4.62,2.05,2.07,3.67,4.68,4.87,7.81,1.2,3.14,1.79,6.84,1.79,11.12v2.92h-39.88v-5.22h33.83c0-3.28-.66-6.23-1.98-8.86-1.32-2.63-3.15-4.7-5.48-6.23-2.34-1.53-5.04-2.29-8.09-2.29-3.23,0-6.08.86-8.54,2.59-2.46,1.73-4.38,4.01-5.77,6.85-1.38,2.84-2.09,5.94-2.11,9.31v3.12c0,4.05.7,7.59,2.11,10.62,1.41,3.02,3.41,5.37,6,7.03,2.59,1.66,5.66,2.49,9.21,2.49,2.41,0,4.54-.38,6.36-1.13,1.83-.75,3.37-1.77,4.62-3.04,1.25-1.27,2.2-2.68,2.84-4.2l5.62,1.83c-.78,2.15-2.04,4.13-3.81,5.95-1.76,1.82-3.95,3.27-6.58,4.37-2.63,1.1-5.65,1.65-9.06,1.65Z"/>
                                 <path class="cls-1" d="m412.07,44.96v-11.86h55.9v11.86h-20.84v56.2h-14.22v-56.2h-20.84Z"/>
                                 <path class="cls-1" d="m471.62,101.16v-51.05h13.73v8.91h.53c.93-3.17,2.49-5.57,4.69-7.2,2.19-1.63,4.72-2.44,7.58-2.44.71,0,1.47.04,2.29.13.82.09,1.54.21,2.16.37v12.56c-.66-.2-1.58-.38-2.76-.53-1.17-.15-2.25-.23-3.22-.23-2.08,0-3.94.45-5.57,1.35-1.63.9-2.91,2.14-3.86,3.74-.94,1.6-1.41,3.43-1.41,5.52v28.88h-14.16Z"/>
                                 <path class="cls-1" d="m523,102.12c-3.26,0-6.16-.57-8.71-1.71-2.55-1.14-4.56-2.84-6.03-5.08-1.47-2.25-2.21-5.06-2.21-8.42,0-2.84.52-5.22,1.56-7.15,1.04-1.93,2.46-3.48,4.25-4.65,1.79-1.17,3.84-2.06,6.13-2.66,2.29-.6,4.7-1.02,7.23-1.26,2.97-.31,5.36-.6,7.18-.88,1.82-.28,3.13-.69,3.95-1.25.82-.55,1.23-1.37,1.23-2.46v-.2c0-2.1-.66-3.73-1.98-4.89-1.32-1.15-3.19-1.73-5.6-1.73-2.55,0-4.58.56-6.08,1.68-1.51,1.12-2.5,2.52-2.99,4.2l-13.09-1.06c.66-3.1,1.97-5.79,3.92-8.06,1.95-2.27,4.47-4.02,7.56-5.25,3.09-1.23,6.67-1.84,10.75-1.84,2.84,0,5.55.33,8.16,1,2.6.66,4.92,1.7,6.95,3.09,2.03,1.4,3.63,3.19,4.8,5.37,1.17,2.18,1.76,4.79,1.76,7.83v34.43h-13.43v-7.08h-.4c-.82,1.6-1.92,3-3.29,4.2-1.37,1.21-3.02,2.15-4.95,2.82-1.93.68-4.15,1.01-6.68,1.01Zm4.05-9.77c2.08,0,3.92-.42,5.52-1.25s2.85-1.96,3.76-3.37c.91-1.42,1.36-3.02,1.36-4.82v-5.42c-.44.29-1.05.55-1.81.78s-1.62.44-2.58.63c-.95.19-1.91.35-2.86.5-.95.14-1.82.27-2.59.38-1.66.24-3.11.63-4.35,1.16-1.24.53-2.21,1.25-2.89,2.14-.69.9-1.03,2.01-1.03,3.34,0,1.93.7,3.4,2.11,4.4,1.41,1.01,3.2,1.51,5.37,1.51Z"/>
                                 <path class="cls-1" d="m580.03,50.11h10.17v10.63h-10.17v40.41h-14.12v-40.41h-7.21v-10.63h7.21v-3.69c0-3.7.72-6.77,2.18-9.21,1.45-2.44,3.44-4.26,5.97-5.48,2.53-1.22,5.4-1.83,8.61-1.83,2.17,0,4.15.17,5.97.5,1.8.33,3.15.63,4.04.9l-2.53,10.63c-.55-.18-1.23-.34-2.04-.5s-1.64-.23-2.48-.23c-2.08,0-3.53.48-4.35,1.45-.83.96-1.23,2.31-1.23,4.04v3.42Z"/>
                                 <path class="cls-1" d="m616.12,50.11h10.17v10.63h-10.17v40.41h-14.12v-40.41h-7.21v-10.63h7.21v-3.69c0-3.7.72-6.77,2.18-9.21,1.45-2.44,3.44-4.26,5.97-5.48,2.53-1.22,5.4-1.83,8.61-1.83,2.17,0,4.15.17,5.97.5,1.8.33,3.15.63,4.04.9l-2.53,10.63c-.55-.18-1.23-.34-2.04-.5-.81-.15-1.64-.23-2.48-.23-2.08,0-3.53.48-4.35,1.45-.83.96-1.23,2.31-1.23,4.04v3.42Z"/>
                                 <path class="cls-1" d="m642.05,43.53c-2.11,0-3.9-.7-5.4-2.09-1.5-1.4-2.24-3.08-2.24-5.05s.75-3.66,2.26-5.07c1.51-1.41,3.3-2.11,5.38-2.11s3.91.7,5.4,2.09c1.5,1.4,2.24,3.08,2.24,5.05s-.75,3.66-2.24,5.07c-1.5,1.41-3.3,2.11-5.4,2.11Zm-7.11,57.63v-51.05h14.16v51.05h-14.16Z"/>
                                 <path class="cls-1" d="m683.46,102.15c-5.23,0-9.72-1.11-13.48-3.34-3.76-2.23-6.64-5.32-8.64-9.29-2.01-3.97-3.01-8.53-3.01-13.69s1.01-9.82,3.04-13.78c2.03-3.95,4.91-7.05,8.66-9.27,3.74-2.23,8.2-3.34,13.36-3.34,4.45,0,8.35.81,11.7,2.43,3.35,1.62,5.99,3.89,7.94,6.81,1.95,2.92,3.02,6.36,3.22,10.3h-13.36c-.38-2.55-1.37-4.6-2.97-6.16-1.61-1.56-3.71-2.34-6.3-2.34-2.19,0-4.1.59-5.73,1.78-1.63,1.19-2.9,2.91-3.81,5.17s-1.36,5-1.36,8.21.45,6.03,1.35,8.31c.9,2.28,2.17,4.02,3.81,5.22,1.64,1.2,3.56,1.79,5.75,1.79,1.62,0,3.07-.33,4.37-1,1.3-.66,2.37-1.63,3.22-2.91.85-1.27,1.41-2.81,1.68-4.6h13.36c-.22,3.9-1.28,7.33-3.17,10.29-1.89,2.96-4.5,5.27-7.83,6.93-3.32,1.66-7.26,2.49-11.8,2.49Z"/>
                                 </g>
                                 <g>
                                 <path class="cls-3" d="m800,37.72c0,4.31-3.5,7.81-7.82,7.81s-7.81-3.5-7.81-7.81,3.5-7.82,7.81-7.82,7.82,3.5,7.82,7.82Z"/>
                                 <path class="cls-2" d="m736.11,85.52c-.09,0-.18,0-.27-.01-.09,0-.18-.02-.26-.03.18.01.35.05.53.05Z"/>
                                 <path class="cls-2" d="m784.37,50.18v32.89c0,1.31-1.05,2.37-2.35,2.43-.03,0-.07.02-.11.02h-.37l-18.68-41.36-3.1-6.86c-2.03-4.5-6.52-7.4-11.47-7.4s-9.42,2.9-11.46,7.4l-11.96,26.47-10.4,23.03c-3.04,6.73,1.88,14.36,9.28,14.36h66.01c5.65,0,10.25-4.58,10.25-10.24v-40.74h-15.63Zm-48.25,35.34c-.09,0-.18,0-.27-.01-.09,0-.18-.02-.26-.03-2.25-.33-3.65-2.75-2.67-4.91l12.93-28.64c.95-2.1,3.93-2.1,4.89,0l15.17,33.59h-29.79Z"/>
                                 </g>
                              </g>
                              </g>
                           </g>
                        </svg>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      
      </div>
      <div class="container">
         <div class="row">
            <div class="col-12 text-center mt20 px-md-0">    
                  <div class="pre-heading f-16 f-md-20 w400 lh140 white-clr">
                     Grab My 20 Exclusive Bonuses Before the Deal Ends…
                  </div>
                  <div class="col-12 f-md-42 f-28 w700 text-center white-clr lh140 mt-md30 mt20 ">
                    World's First Google-Bard Powered AI App That <span class="red-brush"> *Legally* Hacks Into YouTube's 800 Million Videos… </span> 
                </div>
                <div class="col-12 mt-md20 mt20 text-center">
                    <div class="f-18 f-md-24 w600 lh140 white-clr ">
                        <i>Then Redirect Their Views To ANY Link or Offer of Our Choice</i>
                    </div>
                </div>
                <div class="col-12 mt-md20 mt20 text-center">
                    <div class="f-md-24 f-18 w500 yellow-clr lh140 post-heading">
                        92,346 Views In 4 Hours Or Less… Making Us $346.34 Daily
                    </div>
                </div>
                <div class="col-12 mt-md30 mt20 text-center">
                    <div class="col-12 f-md-24 f-18 w400 white-clr lh140">
                        Without Creating Videos | Without Uploading | Without Ads | No Upfront Cost at All
                    </div>
                </div>
            </div>
            </div>
         </div>
         <div class="row mt30 mt-md50">
            <div class="col-12 col-md-10 mx-auto">
               <img src="assets/images/productbox.webp" class="img-fluid mx-auto d-block" alt="ProductBox">
               <!-- <div class="responsive-video">
                  <iframe src=" https://vidboxs.dotcompal.com/video/embed/w4rbh264oa" style=" position: absolute;top: 0;left: 0;width: 100%;
                  height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; 
                  fullscreen" allowfullscreen=""></iframe>
               </div>  -->
            </div>
         </div>
      </div>
   </div>
   <!-- Header Section Start -->
   <div class="list-section">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="row header-list-block">
                  <div class="col-12 col-md-6">
                     <div class="f-18 f-md-20 lh140 w600">
                     <ul class="bonus-list pl0">
                        <li>Tap Into Fast-Growing $200 Billion+ Video Industry & Bank BIG. </li>
                        <li>Create Unlimited Reels & Short Videos for any Niche. </li>
                        <li>Create Reels, Stories & Shorts Using Just One keyword </li>
                        <li>Create Boomerang Video to Engage Audience  </li>
                        <li>Drive Unlimited Traffic & Sales to Any Offer or Page </li>
                        <li>Add Background Music and/or Voiceover to Any Video </li>
                        </ul>
                     </div>
                  </div>
                  <div class="col-12 col-md-6">
                     <div class="f-18 f-md-20 lh140 w500">
                        <ul class="bonus-list pl0">
                        <li>Create Reels & Short Videos from Any Video or Text in 3 Simple Clicks </li>
                        <li>Make Tons of Affiliate Commissions or Ad Profits </li>
                        <li>Pre-designed 50+ Shorts Video templates & 25+ vector images</li>
                        <li>Inbuilt Voiceover Creator with 150+ Human Voice in 30+ Languages </li>
                        <li>100% Newbie Friendly, Required No Prior Experience or Tech Skills </li>  
                        <li>No Camera Recording, No Voice, or Complex Editing Required</li>
                        </ul>
                     </div>
                  </div>
                  <!-- <div class="col-12 col-md-12">
                     <div class="f-18 f-md-20 lh140 w600">
                        <ul class="bonus-list pl0">
                        
                  </ul>
               </div> -->
            </div>
            </div>
            </div>
         </div>
      </div>
   </div>

   <!-- CTA Section Start -->
   <div class="dark-cta-sec">
      <div class="container-fluid">
         <div class="row">
            <div class="col-md-12 col-md-12 col-12 text-center ">
               <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
               <div class="f-md-36 f-22 text-center  lh120 w600 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
               <div class="f-18 f-md-22 lh150 w400 text-center mt20 white-clr">
                  Use Special Coupon <span class="w600 yellow-clr">"Trafficadmin"</span> For <span class="w600 yellow-clr"> 32% Off</span> On Full Funnel
               </div>
            </div>
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab TubeTrafficAi + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
               <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-20 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
               <div class="countdown counter-white white-clr"><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w600">00</span><br><span class="f-14 f-md-18 ">Days</span> </div><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w600">10</span><br><span class="f-14 f-md-18 w500 ">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald w600">28</span><br><span class="f-14 f-md-18 w500 ">Mins</span> </div><div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald w600">20</span><br><span class="f-14 f-md-18 w500">Sec</span> </div></div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>
   <!-- CTA Section End -->
   <!-- Step Section -->
   <div class="step-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-39 f-28 w600 text-center black-clr2 lh140">
                  Create Stunning Stories, Reels &amp; Shorts for Massive Traffic To Any Offer, Page, or Link <span class="red-clr w700">in Just 3 Easy Steps</span>
               </div>
            </div>
            <div class="row mt20 mt-md50">
               <div class="col-12 col-md-4">
                  <div class="step-block">
                     <div class="step-title f-20 f-md-22 w700">Step 1</div>
                     <img src="assets/images/step-2.webp" class="img-fluid d-block mx-auto mt15 mt-md30">
                     <div class="step-content">
                        <div class="f-22 f-md-28 w600 black-clr2 mt10">Choose Platform</div>
                        <div class="w400 f-18 f-md-20 lh140 mt10 black-clr2">
                           Choose the social platform- YouTube, Instagram, TikTok, Facebook, or Snapchat you want to create Reel or Shorts for
                        </div>
                     </div>  
                  </div>
               </div>
               <div class="col-12 col-md-4 mt20 mt-md0">
                  <div class="step-block">
                     <div class="step-title f-20 f-md-22 w700">Step 2</div>
                     <img src="assets/images/step-1.webp" class="img-fluid d-block mx-auto mt15 mt-md30">
                     <div class="step-content">
                        <div class="f-22 f-md-28 w600 black-clr2 mt10">Enter a Keyword</div>
                        <div class="w400 f-18 f-md-20 lh140 mt10 black-clr2">
                           Enter the desired Keyword &amp; you will get several short videos. Select any video &amp; customize it by adding background, vectors, audio, &amp; fonts to make it engaging
                        </div>
                     </div>  
                  </div>
               </div>
               <div class="col-12 col-md-4 mt20 mt-md0">
                  <div class="step-block">
                     <div class="step-title f-20 f-md-22 w700">Step 3</div>
                     <img src="assets/images/step-3.webp" class="img-fluid d-block mx-auto mt15 mt-md30">
                     <div class="step-content">
                        <div class="f-22 f-md-28 w600 black-clr2 mt10">Publish and Profit</div>
                        <div class="w400 f-18 f-md-20 lh140 mt10 black-clr2">
                           Now publish your Reels &amp; Shortson different social platforms with a click to drive unlimited viral traffic &amp; sales on your offer, page, or website.
                        </div>
                     </div>  
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md70">
               <div class="col-12 text-center">
                  <div class="f-20 f-md-24 w600 lh140">
                     No Download/Installation  |  No Prior Knowledge  |  100% Beginners Friendly
                  </div>
                  <div class="f-18 f-md-20 w400 mt15 mt-md30 lh140">
                     In just 60 seconds, you can create your first profitable Reels &amp; Shorts to enjoy MASSIVE FREE Traffic, Sales &amp; Profits coming into your bank account on autopilot.
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Step Section End -->
      <div class="proof-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-40 f-28 w700 lh140 text-center black-clr">
                  Got 34,309 Targeted Visitors in Last 30 Days… .                  
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
                  <img src="https://cdn.oppyo.com/launches/vidboxs/fe/proof.webp" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 mt20 mt-md50 f-md-40 f-28 w700 lh140 text-center black-clr">
                  Consistently Making Average $535 In Profits <br class="d-none d-md-block"> Each & Every Day              
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
                  <img src="https://cdn.oppyo.com/launches/vidboxs/fe/proof1.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>
         </div>
      </div>
     <!-- Testimonials Section -->
     <div class="testimonial-section">
         <div class="container ">
            <div class="row ">
               <div class="f-md-44 f-28 w700 lh140 text-center black-clr2">
                  Checkout What TubeTrafficAi Early Users Have to SAY  
               </div>
            </div>
            <div class="row mt25 mt-md50 align-items-center">
               <div class="col-12 col-md-10">
                  <div class="testi-block">
                     <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-12 col-md-3 order-md-2">
                           <img src="assets/images/c1.webp" class="img-fluid d-block mx-auto">
                           <div class="f-22 f-md-24 w700 lh120 red-clr mt20 text-center">Benjamin Davis</div>
                        </div>
                        <div class="col-12 col-md-9 order-md-1 mt20 mt-md0 text-center text-md-start">
                           <div class="f-22 f-md-28 w700 lh140">I got 2500+ Views &amp; 509 Subscribers in 4 days</div>
                           <div class="f-20 w400 lh140 mt20 quote1">
                              Hi, I am from Australia and promote weight loss products on ClickBank &amp; Amazon as an affiliate. I was struggling to get traffic on my offers and my YouTube channels have only 257 subscribers the reason is that I could not create and post Videos and Shorts regularly as it takes lots of effort and time. But recently, I got access to TubeTrafficAi, I created around 15 Short Videos with almost no effort, that was amazingly EASY. And <span class="w700">I got 2500+ Views &amp; 509 Subscribers in 4 days</span> on my weight loss channel. Kudos to the team for creating a solution that can make the complete process fast &amp; easy. 
                           </div> 
                         </div>
                     </div>
                 </div>
               </div>
               <div class="col-12 col-md-10 offset-md-2 mt20 mt-md40">
                  <div class="testi-block">
                     <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-12 col-md-3">
                           <img src="assets/images/c2.webp" class="img-fluid d-block mx-auto">
                           <div class="f-22 f-md-24 w700 red-clr mt20 text-center">Liam Smith</div>
                        </div>
                        <div class="col-12 col-md-9 mt20 mt-md0 text-center text-md-start">
                           <div class="f-22 f-md-28 w700 lh140">Best part about TubeTrafficAi is that it takes away all hassles</div>
                           <div class="f-20 w400 lh140 mt20 quote1">
                              Thanks for the early access, TubeTrafficAi Team. <span class="w700">The best part about TubeTrafficAi is that it takes away</span> all hassles like video recording or editing, voiceover recording, background music and driving traffic to your offers, etc. I am sure this will give you complete value for your money and help you get the desired success in the long run.
                           </div>  
                        </div>
                     </div>
                 </div>
               </div>
               <div class="col-12 col-md-10 mt20 mt-md40">
                  <div class="testi-block">
                     <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-12 col-md-3 order-md-2">
                           <img src="assets/images/c3.webp" class="img-fluid d-block mx-auto">
                           <div class="f-22 f-md-24 w700 lh120 red-clr mt20 text-center">Noah Brown</div>
                        </div>
                        <div class="col-12 col-md-9 order-md-1 mt20 mt-md0 text-center text-md-start">
                           <div class="f-22 f-md-28 w700 lh140">Create super engaging video shorts for YouTube</div>
                           <div class="f-20 w400 lh140 mt20 quote1">
                              TubeTrafficAi is the perfect platform to <span class="w700">create super engaging video shorts for YouTube and social media and drive tons of viral traffic in a stress-free manner.</span> I am enjoying all the functionalities of this amazing software. If someone asks me to give marks, I would love to give ten-on-ten to this fabulous product.
                           </div> 
                        </div>
                     </div>
                 </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Testimonials Section End -->

   <!-- CTA Section Start -->
   <div class="dark-cta-sec">
      <div class="container-fluid">
         <div class="row">
            <div class="col-md-12 col-md-12 col-12 text-center ">
               <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
               <div class="f-md-36 f-22 text-center  lh120 w600 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
               <div class="f-18 f-md-22 lh140 w400 text-center mt20 white-clr">
                  Use Special Coupon <span class="w600 yellow-clr">"Trafficadmin"</span> For <span class="w600 yellow-clr"> 32% Off</span> On Full Funnel
               </div>
            </div>
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab TubeTrafficAi + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
               <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-20 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
               <div class="countdown counter-white white-clr"><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w600">00</span><br><span class="f-14 f-md-18 ">Days</span> </div><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w600">10</span><br><span class="f-14 f-md-18 w500 ">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald w600">28</span><br><span class="f-14 f-md-18 w500 ">Mins</span> </div><div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald w600">20</span><br><span class="f-14 f-md-18 w500">Sec</span> </div></div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>
   <!-- CTA Section End -->

   <div class="billion-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-45 w600 white-clr lh140 heading-design">Did You Know ? YouTube Shorts Hit</div>
                  <div class="f-45 f-md-100 w600 black-clr2 mt20 lh140"> <span class="red-clr">30 Billion</span> View</div>
                  <div class="f-28 f-md-45 w600 lh140 black-clr2">Every Single Day!</div>
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-12 col-md-7">
                  <div class="f-18 f-md-24 w600 black-clr2 lh140">Yes, YouTube Shorts are Generating 30 Billion+ Views Per Day, A 4X Increase From This Time Last Year
                  </div>
                  <div class="f-18 f-md-24 w400 lh140 black-clr21 mt20">This clearly shows the trend is changing. Internet users prefer short videos to consume content instead of watching long videos like earlier…</div>
                  <div class="f-28 f-md-45 w600 lh140 red-clr mt20 mt-md50">No Doubt!</div>
                  <div class="f-20 f-md-24 w600 lh140 black-clr2 mt10">Reels &amp; Shorts Are Present and Future of Video Marketing…</div>
               </div>
               <div class="col-12 col-md-5 mt20 mt-md0">
                  <img src="assets/images/girl.webp" class="img-fluid mx-auto d-block">
               </div>
            </div>
         </div>
         <div class="billion-wrap">
            <div class="container">
               <div class="row">
                  <div class="col-12 text-center">
                     <div class="f-28 f-md-45 w600 lh140 black-clr2">Here Are Some More Facts - <br class="d-none d-md-block">Why One Should Get Started with Reels &amp; Shorts! </div>
                  </div>
               </div>
               <div class="row align-items-center mt20 mt-md80">
                  <div class="col-12 col-md-6">
                     <div class="f-20 f-md-24 w600 lh140 text-start black-clr2">YouTube has 2+ Bn Users &amp; 70% of Videos are Viewed on Mobile</div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 ">
                     <img src="assets/images/facts1.webp" class="img-fluid d-block mx-auto">
                  </div>
               </div>
               <!-- <div class="row align-items-center mt20 mt-md80">
                  <div class="col-12 col-md-6 order-md-2">
                     <div class="f-20 f-md-24 w600 lh140 text-start black-clr2">YouTube Confirms Ads Are Coming to Shorts</div>
                     <div class="f-18 f-md-20 w400 lh140 mt10 text-start black-clr21"> Ads in Shorts would give creators in the YouTube Partner Program the ability to earn more revenue on the platform.
                     </div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
                     <img src="assets/images/facts2.webp" alt="YouTube Confirms Ads Are Coming to Shorts" class="mx-auto img-fluid d-block">
                  </div>
               </div> -->
               <div class="row align-items-center mt20 mt-md80">
                  <div class="col-12 col-md-6 order-md-2">
                     <div class="f-20 f-md-24 w600 lh140 text-start black-clr2">Instagram Reels have 2.5 Billion Monthly Active Users</div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
                     <img src="assets/images/facts3.webp" class="img-fluid d-block mx-auto" alt="Instagram Reels">
                  </div>
               </div>
               <div class="row align-items-center mt20 mt-md80">
                  <div class="col-12 col-md-6">
                     <div class="f-20 f-md-24 w600 lh140 text-start black-clr2">Tiktok has 50 Million Daily Active User in USA &amp; 1 Bn Users Worldwide</div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0">
                     <img src="assets/images/facts4.webp" alt="Tiktok" class="mx-auto img-fluid d-block">
                  </div>
               </div>
               <div class="row align-items-center mt20 mt-md80">
                  <div class="col-12 col-md-6 order-md-2">
                     <div class="f-20 f-md-24 w600 lh140 text-start black-clr2">100 Million hours Daily watch Time by Facebook users.</div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
                     <img src="assets/images/facts5.webp" class="img-fluid d-block mx-auto" alt="Facebook Users">
                  </div>
               </div>
               <div class="row align-items-center mt20 mt-md80">
                  <div class="col-12 col-md-6">
                     <div class="f-20 f-md-24 w600 lh140 text-start black-clr2">Snapchat has over 360+ million daily active users</div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0">
                     <img src="assets/images/facts6.webp" alt="Website Analyser" class="mx-auto img-fluid d-block">
                  </div>
               </div>
               <div class="row mt20 mt-md80">
                  <div class="col-12 text-center">
                     <div class="f-28 f-md-45 w600 lh140 ">So, Capitalising on Reels &amp; Shorts in the Video Marketing Strategy of Any Business is the Need of The Hour.</div>
                     <div class="f-md-45 f-28 w600 lh140 mt20 mt-md30 black-clr2  border-cover">
                        <div class="f-22 f-md-26 w600 lh140 black-clr2">It Opens the Door to</div>
                        <div class=""> Tsunami <span class="red-clr"> of FREE </span>Traffic</div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
  <!-- Proudly Section -->
  <div class="next-gen-sec" id="product">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
            <div class="f-28 f-md-50 w700 lh140 white-clr caveat proudly-sec">
               Introducing…
            </div>
            </div>
            <div class="col-12 mt-md50 mt30 text-center">
            <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 800 131.43" style="max-height:75px;">
               <defs>
                     <style>
                     .cls-1 {
                        fill: #fff;
                     }

                     .cls-2 {
                        fill: #fd2f15;
                     }

                     .cls-3 {
                        fill: #fbbf33;
                     }
                     </style>
               </defs>
               <g id="Layer_1-2" data-name="Layer 1">
                     <g>
                     <g>
                        <path class="cls-3" d="m105.61,78.06l40.05,23.12c6.01,3.47,13.51-.87,13.51-7.8v-46.25c0-6.93-7.51-11.27-13.51-7.8l-40.05,23.12c-6.01,3.47-6.01,12.14,0,15.6Z"/>
                        <g>
                        <rect class="cls-3" x="26.92" y="58.86" width="16.03" height="46.81"/>
                        <path class="cls-2" d="m125.22,26.34h-7.16v46.06c0,32.6-26.43,59.03-59.03,59.03H0v-65.94C0,32.89,26.43,6.45,59.03,6.45h38.52l-12.38,19.88h-26.14c-21.59,0-39.16,17.57-39.16,39.15v46.07h39.16c21.59,0,39.16-17.57,39.16-39.16V26.34h-5.79L108.81,0l16.41,26.34Z"/>
                        <path class="cls-3" d="m67.04,46.21h-16.02v59.46h8.01c2.76,0,5.45-.35,8.01-.99v-58.47Z"/>
                        <path class="cls-3" d="m90.77,33.56h-16.03v68.14c7.57-4.07,13.39-10.99,16.03-19.31v-48.83Z"/>
                        </g>
                     </g>
                     <g>
                        <g>
                        <path class="cls-1" d="m195.69,38.68v-5.58h49.35v5.58h-21.57v62.48h-6.21v-62.48h-21.57Z"/>
                        <path class="cls-1" d="m283.29,81.65v-31.54h5.92v51.05h-5.92v-8.47h-.47c-1.2,2.61-3.09,4.79-5.68,6.53-2.59,1.74-5.76,2.61-9.5,2.61-3.26,0-6.15-.71-8.67-2.14-2.53-1.43-4.51-3.53-5.95-6.3-1.44-2.77-2.16-6.17-2.16-10.2v-33.07h5.92v32.7c0,3.99,1.17,7.2,3.52,9.62,2.35,2.43,5.36,3.64,9.04,3.64,2.3,0,4.52-.55,6.65-1.66,2.13-1.11,3.88-2.73,5.25-4.87,1.37-2.14,2.06-4.77,2.06-7.89Z"/>
                        <path class="cls-1" d="m306.05,101.16V33.09h5.95v26.75h.53c.66-1.53,1.62-3.1,2.87-4.72,1.25-1.62,2.9-2.97,4.95-4.07,2.05-1.1,4.64-1.65,7.76-1.65,4.19,0,7.86,1.1,11.03,3.31,3.17,2.21,5.64,5.28,7.41,9.24,1.77,3.95,2.66,8.56,2.66,13.81s-.88,9.89-2.64,13.86c-1.76,3.97-4.22,7.06-7.38,9.27-3.16,2.22-6.82,3.32-10.98,3.32-3.1,0-5.69-.55-7.76-1.66-2.07-1.11-3.74-2.48-5-4.1-1.26-1.63-2.24-3.22-2.92-4.77h-.73v9.47h-5.75Zm5.85-25.49c0,4.12.61,7.77,1.84,10.95,1.23,3.18,3,5.67,5.32,7.48,2.31,1.81,5.12,2.71,8.42,2.71s6.26-.94,8.61-2.81c2.35-1.87,4.13-4.4,5.35-7.59,1.22-3.19,1.83-6.77,1.83-10.73s-.6-7.46-1.81-10.6c-1.21-3.15-2.99-5.64-5.33-7.49-2.35-1.85-5.23-2.78-8.64-2.78s-6.14.89-8.46,2.68c-2.32,1.78-4.08,4.24-5.3,7.38-1.22,3.14-1.83,6.74-1.83,10.82Z"/>
                        <path class="cls-1" d="m382.86,102.22c-4.76,0-8.88-1.11-12.36-3.34-3.48-2.23-6.16-5.32-8.06-9.27s-2.84-8.51-2.84-13.68.95-9.71,2.84-13.71c1.89-4,4.52-7.13,7.88-9.41,3.36-2.27,7.24-3.41,11.65-3.41,2.77,0,5.44.5,8.01,1.51,2.57,1.01,4.88,2.55,6.93,4.62,2.05,2.07,3.67,4.68,4.87,7.81,1.2,3.14,1.79,6.84,1.79,11.12v2.92h-39.88v-5.22h33.83c0-3.28-.66-6.23-1.98-8.86-1.32-2.63-3.15-4.7-5.48-6.23-2.34-1.53-5.04-2.29-8.09-2.29-3.23,0-6.08.86-8.54,2.59-2.46,1.73-4.38,4.01-5.77,6.85-1.38,2.84-2.09,5.94-2.11,9.31v3.12c0,4.05.7,7.59,2.11,10.62,1.41,3.02,3.41,5.37,6,7.03,2.59,1.66,5.66,2.49,9.21,2.49,2.41,0,4.54-.38,6.36-1.13,1.83-.75,3.37-1.77,4.62-3.04,1.25-1.27,2.2-2.68,2.84-4.2l5.62,1.83c-.78,2.15-2.04,4.13-3.81,5.95-1.76,1.82-3.95,3.27-6.58,4.37-2.63,1.1-5.65,1.65-9.06,1.65Z"/>
                        <path class="cls-1" d="m412.07,44.96v-11.86h55.9v11.86h-20.84v56.2h-14.22v-56.2h-20.84Z"/>
                        <path class="cls-1" d="m471.62,101.16v-51.05h13.73v8.91h.53c.93-3.17,2.49-5.57,4.69-7.2,2.19-1.63,4.72-2.44,7.58-2.44.71,0,1.47.04,2.29.13.82.09,1.54.21,2.16.37v12.56c-.66-.2-1.58-.38-2.76-.53-1.17-.15-2.25-.23-3.22-.23-2.08,0-3.94.45-5.57,1.35-1.63.9-2.91,2.14-3.86,3.74-.94,1.6-1.41,3.43-1.41,5.52v28.88h-14.16Z"/>
                        <path class="cls-1" d="m523,102.12c-3.26,0-6.16-.57-8.71-1.71-2.55-1.14-4.56-2.84-6.03-5.08-1.47-2.25-2.21-5.06-2.21-8.42,0-2.84.52-5.22,1.56-7.15,1.04-1.93,2.46-3.48,4.25-4.65,1.79-1.17,3.84-2.06,6.13-2.66,2.29-.6,4.7-1.02,7.23-1.26,2.97-.31,5.36-.6,7.18-.88,1.82-.28,3.13-.69,3.95-1.25.82-.55,1.23-1.37,1.23-2.46v-.2c0-2.1-.66-3.73-1.98-4.89-1.32-1.15-3.19-1.73-5.6-1.73-2.55,0-4.58.56-6.08,1.68-1.51,1.12-2.5,2.52-2.99,4.2l-13.09-1.06c.66-3.1,1.97-5.79,3.92-8.06,1.95-2.27,4.47-4.02,7.56-5.25,3.09-1.23,6.67-1.84,10.75-1.84,2.84,0,5.55.33,8.16,1,2.6.66,4.92,1.7,6.95,3.09,2.03,1.4,3.63,3.19,4.8,5.37,1.17,2.18,1.76,4.79,1.76,7.83v34.43h-13.43v-7.08h-.4c-.82,1.6-1.92,3-3.29,4.2-1.37,1.21-3.02,2.15-4.95,2.82-1.93.68-4.15,1.01-6.68,1.01Zm4.05-9.77c2.08,0,3.92-.42,5.52-1.25s2.85-1.96,3.76-3.37c.91-1.42,1.36-3.02,1.36-4.82v-5.42c-.44.29-1.05.55-1.81.78s-1.62.44-2.58.63c-.95.19-1.91.35-2.86.5-.95.14-1.82.27-2.59.38-1.66.24-3.11.63-4.35,1.16-1.24.53-2.21,1.25-2.89,2.14-.69.9-1.03,2.01-1.03,3.34,0,1.93.7,3.4,2.11,4.4,1.41,1.01,3.2,1.51,5.37,1.51Z"/>
                        <path class="cls-1" d="m580.03,50.11h10.17v10.63h-10.17v40.41h-14.12v-40.41h-7.21v-10.63h7.21v-3.69c0-3.7.72-6.77,2.18-9.21,1.45-2.44,3.44-4.26,5.97-5.48,2.53-1.22,5.4-1.83,8.61-1.83,2.17,0,4.15.17,5.97.5,1.8.33,3.15.63,4.04.9l-2.53,10.63c-.55-.18-1.23-.34-2.04-.5s-1.64-.23-2.48-.23c-2.08,0-3.53.48-4.35,1.45-.83.96-1.23,2.31-1.23,4.04v3.42Z"/>
                        <path class="cls-1" d="m616.12,50.11h10.17v10.63h-10.17v40.41h-14.12v-40.41h-7.21v-10.63h7.21v-3.69c0-3.7.72-6.77,2.18-9.21,1.45-2.44,3.44-4.26,5.97-5.48,2.53-1.22,5.4-1.83,8.61-1.83,2.17,0,4.15.17,5.97.5,1.8.33,3.15.63,4.04.9l-2.53,10.63c-.55-.18-1.23-.34-2.04-.5-.81-.15-1.64-.23-2.48-.23-2.08,0-3.53.48-4.35,1.45-.83.96-1.23,2.31-1.23,4.04v3.42Z"/>
                        <path class="cls-1" d="m642.05,43.53c-2.11,0-3.9-.7-5.4-2.09-1.5-1.4-2.24-3.08-2.24-5.05s.75-3.66,2.26-5.07c1.51-1.41,3.3-2.11,5.38-2.11s3.91.7,5.4,2.09c1.5,1.4,2.24,3.08,2.24,5.05s-.75,3.66-2.24,5.07c-1.5,1.41-3.3,2.11-5.4,2.11Zm-7.11,57.63v-51.05h14.16v51.05h-14.16Z"/>
                        <path class="cls-1" d="m683.46,102.15c-5.23,0-9.72-1.11-13.48-3.34-3.76-2.23-6.64-5.32-8.64-9.29-2.01-3.97-3.01-8.53-3.01-13.69s1.01-9.82,3.04-13.78c2.03-3.95,4.91-7.05,8.66-9.27,3.74-2.23,8.2-3.34,13.36-3.34,4.45,0,8.35.81,11.7,2.43,3.35,1.62,5.99,3.89,7.94,6.81,1.95,2.92,3.02,6.36,3.22,10.3h-13.36c-.38-2.55-1.37-4.6-2.97-6.16-1.61-1.56-3.71-2.34-6.3-2.34-2.19,0-4.1.59-5.73,1.78-1.63,1.19-2.9,2.91-3.81,5.17s-1.36,5-1.36,8.21.45,6.03,1.35,8.31c.9,2.28,2.17,4.02,3.81,5.22,1.64,1.2,3.56,1.79,5.75,1.79,1.62,0,3.07-.33,4.37-1,1.3-.66,2.37-1.63,3.22-2.91.85-1.27,1.41-2.81,1.68-4.6h13.36c-.22,3.9-1.28,7.33-3.17,10.29-1.89,2.96-4.5,5.27-7.83,6.93-3.32,1.66-7.26,2.49-11.8,2.49Z"/>
                        </g>
                        <g>
                        <path class="cls-3" d="m800,37.72c0,4.31-3.5,7.81-7.82,7.81s-7.81-3.5-7.81-7.81,3.5-7.82,7.81-7.82,7.82,3.5,7.82,7.82Z"/>
                        <path class="cls-2" d="m736.11,85.52c-.09,0-.18,0-.27-.01-.09,0-.18-.02-.26-.03.18.01.35.05.53.05Z"/>
                        <path class="cls-2" d="m784.37,50.18v32.89c0,1.31-1.05,2.37-2.35,2.43-.03,0-.07.02-.11.02h-.37l-18.68-41.36-3.1-6.86c-2.03-4.5-6.52-7.4-11.47-7.4s-9.42,2.9-11.46,7.4l-11.96,26.47-10.4,23.03c-3.04,6.73,1.88,14.36,9.28,14.36h66.01c5.65,0,10.25-4.58,10.25-10.24v-40.74h-15.63Zm-48.25,35.34c-.09,0-.18,0-.27-.01-.09,0-.18-.02-.26-.03-2.25-.33-3.65-2.75-2.67-4.91l12.93-28.64c.95-2.1,3.93-2.1,4.89,0l15.17,33.59h-29.79Z"/>
                        </g>
                     </g>
                     </g>
               </g>
            </svg>
            </div>
         </div>
         <div class="row  align-items-center">
            <div class="col-12 f-md-42 f-28 w700 text-center white-clr lh140 mt-md40 mt20 px-md-0">
                    World's First Google-Bard Powered AI App That <span class="red-brush"> *Legally* Hacks Into YouTube's 800 Million Videos… </span> 
                </div>
                <div class="col-12 mt-md20 mt20 text-center">
                    <div class="f-18 f-md-24 w600 lh140 white-clr ">
                        <i>Then Redirect Their Views To ANY Link or Offer of Our Choice</i>
                    </div>
                </div>
                <div class="col-12 mt-md20 mt20 text-center">
                    <div class="f-md-24 f-18 w500 yellow-clr lh140 post-heading">
                        92,346 Views In 4 Hours Or Less… Making Us $346.34 Daily
                    </div>
                </div>
                <div class="col-12 mt-md30 mt20 text-center">
                    <div class="col-12 f-md-24 f-18 w400 white-clr lh140">
                        Without Creating Videos | Without Uploading | Without Ads | No Upfront Cost at All
                    </div>
                </div>
            <div class="col-12 mt20 mt-md50 col-md-10 mx-auto">
               <img src="assets/images/productbox.webp" class="img-fluid d-block mx-auto img-animation" alt="Product">
            </div>
            <!-- <div class="col-12 mt20 mt-md50 f-18 f-md-20 lh140 w400 white-clr text-center lh140">
               This is An Exclusive Deal for <span class="w600 yellow-clr">"TubeTrafficAi"</span> Users Only...
            </div>
            <div class="col-md-10 mx-auto col-12 mt30 mt-md30">
               <div class="f-18 f-md-36 text-center probtn1" editabletype="button" style="z-index: 10;">
                     <a href="#buynow" class="text-center">Upgrade to TubeTrafficAi Agency Now</a>
               </div>
            </div> -->
         </div>
      </div>
   </div>
   <!-- Proudly Section End -->
 <!-------Exclusive Bonus-One-Start--------->
 <div class="exclusive-bonus">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <div class="white-clr f-24 f-md-36 lh140 w600">
                  Exclusive Bonus #1 : VoiceFusion AI
               </div>
               <div class="f-18 f-md-20 w500 mt20 white-clr">
               On Purchasing fe you will get 20 accounts as a RESELLER for the VoiceFusion AI to resell it. And on purchasing any OTO you will get 200 accounts as a RESELLER for the VoiceFusion AI to resell it. 
               </div>
            </div>
         </div>
      </div>
   </div>
    <!-------Exclusive Bonus-One-End--------->
    <!-- header-section- voice-fusion Start-->
   <div class="header-section-voice">
      <div class="container">
         <div class="row align-items-center justify-content-center">
            <div class="col-md-5 text-md-start text-center">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 795 123.19" style="max-height: 80px;">
                  <defs>
                  <style>
                  .cls-1v{fill:#fff;}
                  .cls-2v,
                  .cls-4v{fill:#ff7c24;}
                  .cls-3v{fill:#15c5f3;}
                  .cls-4v{fill-rule:evenodd;}
                  </style>
                  </defs>
                  <g id="Layer_2" data-name="Layer 2">
                  <g id="Layer_1-2" data-name="Layer 1">
                  <path class="cls-1v" d="M151.7,38.52,168.39,91H169l16.73-52.49H202l-23.82,69.09H159.32L135.47,38.52Z"></path>
                  <path class="cls-1v" d="M227.5,108.62a26.33,26.33,0,0,1-13.58-3.36,22.68,22.68,0,0,1-8.82-9.38,30.07,30.07,0,0,1-3.1-14,30.25,30.25,0,0,1,3.1-14.05,22.68,22.68,0,0,1,8.82-9.38,29.12,29.12,0,0,1,27.16,0,22.68,22.68,0,0,1,8.82,9.38A30.25,30.25,0,0,1,253,81.9a30.07,30.07,0,0,1-3.1,14,22.68,22.68,0,0,1-8.82,9.38A26.33,26.33,0,0,1,227.5,108.62Zm.07-11.13a8.85,8.85,0,0,0,6-2,12.6,12.6,0,0,0,3.63-5.58,27,27,0,0,0,0-16.12,12.67,12.67,0,0,0-3.63-5.6,8.81,8.81,0,0,0-6-2.06,9,9,0,0,0-6.06,2.06,12.4,12.4,0,0,0-3.67,5.6,26.81,26.81,0,0,0,0,16.12,12.33,12.33,0,0,0,3.67,5.58A9.09,9.09,0,0,0,227.57,97.49Z"></path>
                  <path class="cls-1v" d="M269.6,49.11A7.75,7.75,0,0,1,264.12,47a6.9,6.9,0,0,1,0-10.27,8.09,8.09,0,0,1,10.94,0,6.93,6.93,0,0,1,0,10.28A7.72,7.72,0,0,1,269.6,49.11Zm-7.22,58.5V55.79h14.37v51.82Z"></path>
                  <path class="cls-1v" d="M311.64,108.62A26.34,26.34,0,0,1,298,105.23a22.59,22.59,0,0,1-8.78-9.43,30.48,30.48,0,0,1-3.05-13.9,30.19,30.19,0,0,1,3.09-14A22.84,22.84,0,0,1,298,58.5a26,26,0,0,1,13.56-3.39,26.84,26.84,0,0,1,11.87,2.47,19.81,19.81,0,0,1,8.07,6.91A20.31,20.31,0,0,1,334.78,75H321.22a10.66,10.66,0,0,0-3-6.26,8.81,8.81,0,0,0-6.4-2.38A9.63,9.63,0,0,0,306,68.12a11.59,11.59,0,0,0-3.87,5.24,22.47,22.47,0,0,0-1.38,8.34,22.91,22.91,0,0,0,1.37,8.43,11.56,11.56,0,0,0,3.86,5.3,10.25,10.25,0,0,0,10.27.81,8.64,8.64,0,0,0,3.27-2.95,11.27,11.27,0,0,0,1.71-4.68h13.56a21.3,21.3,0,0,1-3.22,10.44,19.64,19.64,0,0,1-7.95,7A26.54,26.54,0,0,1,311.64,108.62Z"></path>
                  <path class="cls-1v" d="M367.64,108.62a27.48,27.48,0,0,1-13.75-3.26A22,22,0,0,1,345,96.12a30.44,30.44,0,0,1-3.1-14.19,30.28,30.28,0,0,1,3.1-14,22.88,22.88,0,0,1,8.76-9.41,25.4,25.4,0,0,1,13.27-3.38,27.48,27.48,0,0,1,9.57,1.64,21.68,21.68,0,0,1,7.76,4.91,22.48,22.48,0,0,1,5.18,8.21,33,33,0,0,1,1.85,11.56v3.94H347.67v-8.9h30.22a11,11,0,0,0-1.35-5.5,9.81,9.81,0,0,0-3.72-3.76,10.92,10.92,0,0,0-5.52-1.37,11.1,11.1,0,0,0-5.79,1.5,10.82,10.82,0,0,0-3.93,4,11.42,11.42,0,0,0-1.45,5.58v8.47a14.33,14.33,0,0,0,1.44,6.64,10.22,10.22,0,0,0,4.06,4.32,12.33,12.33,0,0,0,6.24,1.52,13.61,13.61,0,0,0,4.39-.67,9.14,9.14,0,0,0,3.41-2,8.94,8.94,0,0,0,2.16-3.3l13.29.87a18.14,18.14,0,0,1-4.14,8.35,20.71,20.71,0,0,1-8,5.53A30.14,30.14,0,0,1,367.64,108.62Z"></path>
                  <path class="cls-2v" d="M401.07,107.61V38.52h45.74v12H415.68V67h28.1v12h-28.1v28.54Z"></path>
                  <path class="cls-2v" d="M486.35,85.54V55.79h14.37v51.82H486.93V98.19h-.54a15.3,15.3,0,0,1-5.82,7.32,17.14,17.14,0,0,1-9.9,2.77,17.51,17.51,0,0,1-9.15-2.36,16.07,16.07,0,0,1-6.15-6.71,23,23,0,0,1-2.25-10.43v-33h14.37V86.22A10.51,10.51,0,0,0,470,93.47a8.37,8.37,0,0,0,6.51,2.67,10.23,10.23,0,0,0,4.86-1.2A9.39,9.39,0,0,0,485,91.38,11.1,11.1,0,0,0,486.35,85.54Z"></path>
                  <path class="cls-2v" d="M555.31,70.56l-13.16.81a6.9,6.9,0,0,0-1.45-3.05,7.89,7.89,0,0,0-2.92-2.19,10.24,10.24,0,0,0-4.3-.83,10.64,10.64,0,0,0-5.63,1.4,4.23,4.23,0,0,0-2.3,3.73A4,4,0,0,0,527,73.57a11.72,11.72,0,0,0,5.09,2.05l9.38,1.89q7.56,1.56,11.27,5a11.75,11.75,0,0,1,3.71,9,14.2,14.2,0,0,1-3,8.94,19.55,19.55,0,0,1-8.16,6,31.16,31.16,0,0,1-11.93,2.14q-10.29,0-16.38-4.3a16.67,16.67,0,0,1-7.13-11.73L524,91.85a7.26,7.26,0,0,0,3.11,4.78,13.18,13.18,0,0,0,12.4.16A4.43,4.43,0,0,0,541.88,93a4,4,0,0,0-1.65-3.22,12,12,0,0,0-5-1.94l-9-1.79q-7.59-1.51-11.28-5.26a13,13,0,0,1-3.7-9.55,13.88,13.88,0,0,1,2.72-8.6,17.16,17.16,0,0,1,7.66-5.57,31.41,31.41,0,0,1,11.58-2q9.82,0,15.47,4.15A16.06,16.06,0,0,1,555.31,70.56Z"></path>
                  <path class="cls-2v" d="M572.78,49.11A7.75,7.75,0,0,1,567.3,47a6.9,6.9,0,0,1,0-10.27,8.09,8.09,0,0,1,10.94,0,6.93,6.93,0,0,1,0,10.28A7.72,7.72,0,0,1,572.78,49.11Zm-7.22,58.5V55.79h14.37v51.82Z"></path>
                  <path class="cls-2v" d="M614.82,108.62a26.3,26.3,0,0,1-13.58-3.36,22.64,22.64,0,0,1-8.83-9.38,30.19,30.19,0,0,1-3.1-14,30.37,30.37,0,0,1,3.1-14.05,22.64,22.64,0,0,1,8.83-9.38,29.1,29.1,0,0,1,27.15,0,22.64,22.64,0,0,1,8.83,9.38,30.37,30.37,0,0,1,3.1,14.05,30.19,30.19,0,0,1-3.1,14,22.64,22.64,0,0,1-8.83,9.38A26.29,26.29,0,0,1,614.82,108.62Zm.06-11.13a8.82,8.82,0,0,0,6-2,12.52,12.52,0,0,0,3.63-5.58,27,27,0,0,0,0-16.12,12.59,12.59,0,0,0-3.63-5.6,8.78,8.78,0,0,0-6-2.06,9,9,0,0,0-6,2.06,12.41,12.41,0,0,0-3.68,5.6,27,27,0,0,0,0,16.12,12.35,12.35,0,0,0,3.68,5.58A9.09,9.09,0,0,0,614.88,97.49Z"></path>
                  <path class="cls-2v" d="M664.07,77.65v30H649.7V55.79h13.7v9.14h.6a14.6,14.6,0,0,1,5.77-7.17,17.59,17.59,0,0,1,9.82-2.65A18.26,18.26,0,0,1,689,57.47a16,16,0,0,1,6.24,6.74,22.57,22.57,0,0,1,2.23,10.4v33H683.1V77.18c0-3.18-.79-5.65-2.43-7.44a8.73,8.73,0,0,0-6.78-2.68,10.2,10.2,0,0,0-5.11,1.24,8.56,8.56,0,0,0-3.44,3.63A12.45,12.45,0,0,0,664.07,77.65Z"></path>
                  <path class="cls-1v" d="M721,107.61H705.29l23.86-69.09H748l23.82,69.09H756.13L738.83,54.3h-.54Zm-1-27.16h37v11.4H720Z"></path>
                  <path class="cls-1v" d="M787.24,49.11A7.76,7.76,0,0,1,781.75,47a6.92,6.92,0,0,1,0-10.27,8.1,8.1,0,0,1,11,0,6.93,6.93,0,0,1,0,10.28A7.76,7.76,0,0,1,787.24,49.11ZM780,107.61V55.79h14.37v51.82Z"></path>
                  <path class="cls-3v" d="M.46,79.85a45.62,45.62,0,0,0,91.13,0,2.16,2.16,0,0,0-2.15-2.28H58.74a12.72,12.72,0,0,1-25.43,0H2.61A2.17,2.17,0,0,0,.46,79.85ZM46,96.62A18.94,18.94,0,0,0,59.49,91l15,14.95A40,40,0,0,1,46,117.77Z"></path>
                  <path class="cls-3v" d="M46,83.77a6.21,6.21,0,0,0,6.2-6.2H39.82A6.21,6.21,0,0,0,46,83.77Z"></path>
                  <path class="cls-3v" d="M52.9,3v7.55a3,3,0,0,1-3,3H48.32v6.79H43.74V13.57H42.2a3,3,0,0,1-3-3V3a3,3,0,0,1,3-3h7.65A3,3,0,0,1,52.9,3Z"></path>
                  <path class="cls-2v" d="M96.76,36.77V29.46a2.05,2.05,0,0,1,2-2.06h0a2.06,2.06,0,0,1,2.06,2.06v7.31a2.06,2.06,0,0,1-2.06,2.06h0A2.05,2.05,0,0,1,96.76,36.77Z"></path>
                  <path class="cls-2v" d="M103.2,36.77V29.46a2.06,2.06,0,0,1,2.06-2.06h0a2.06,2.06,0,0,1,2.06,2.06v7.31a2.06,2.06,0,0,1-2.06,2.06h0A2.06,2.06,0,0,1,103.2,36.77Z"></path>
                  <rect class="cls-2v" x="109.65" y="27.4" width="4.11" height="11.43" rx="0.91"></rect>
                  <path class="cls-2v" d="M116.1,36.77V29.46a2.05,2.05,0,0,1,2.05-2.06h0a2.06,2.06,0,0,1,2.06,2.06v7.31a2.06,2.06,0,0,1-2.06,2.06h0A2.05,2.05,0,0,1,116.1,36.77Z"></path>
                  <path class="cls-2v" d="M96.76,50.86V43.55a2.05,2.05,0,0,1,2-2.06h0a2.06,2.06,0,0,1,2.06,2.06v7.31a2.06,2.06,0,0,1-2.06,2.06h0A2.05,2.05,0,0,1,96.76,50.86Z"></path>
                  <path class="cls-2v" d="M103.2,50.86V43.55a2.06,2.06,0,0,1,2.06-2.06h0a2.06,2.06,0,0,1,2.06,2.06v7.31a2.06,2.06,0,0,1-2.06,2.06h0A2.06,2.06,0,0,1,103.2,50.86Z"></path>
                  <path class="cls-2v" d="M96.76,65V57.64a2.05,2.05,0,0,1,2-2h0a2.05,2.05,0,0,1,2.06,2V65A2.05,2.05,0,0,1,98.81,67h0A2.05,2.05,0,0,1,96.76,65Z"></path>
                  <path class="cls-2v" d="M103.2,65V57.64a2.05,2.05,0,0,1,2.06-2h0a2.05,2.05,0,0,1,2.06,2V65A2.05,2.05,0,0,1,105.26,67h0A2.05,2.05,0,0,1,103.2,65Z"></path>
                  <rect class="cls-2v" x="109.65" y="55.59" width="4.11" height="11.43" rx="0.91"></rect>
                  <path class="cls-4v" d="M83.53,20.36h-75A8.52,8.52,0,0,0,0,28.87v40a2.3,2.3,0,0,0,2.29,2.29H89.76a2.3,2.3,0,0,0,2.29-2.29v-40A8.52,8.52,0,0,0,83.53,20.36ZM28.85,56.08A10.31,10.31,0,1,1,39.16,45.77,10.31,10.31,0,0,1,28.85,56.08Zm34.35,0A10.31,10.31,0,1,1,73.5,45.77,10.31,10.31,0,0,1,63.2,56.08Z"></path>
                  </g>
                  </g>
               </svg>
            </div>
            <div class="col-12 text-center lh150 mt20 mt-md50">
               <div class="pre-heading-voice f-18 f-md-22 w500 lh140 white-clr">
               Finally! The Days of Dull &amp; Robotic Voices Are Over… 
               </div>
            </div>
            <div class="col-12 mt-md30 mt20 f-md-40 f-24 w400 text-center white-clr lh140">
            World’s First Super-Trending  <span class="orange-clr-voice">"IBM's Watson &amp; ChatGPT4" Powered Ai App</span>  Generates Us 
               <span class="w700">Real Human Emotion Based Voices, Unique Content, Audiobooks, and Podcasts</span>  <u> with Just 1 Keyword. </u>
            </div>
            
            <div class="col-12 mt-md15 mt15 text-center">
               <div class="f-22 f-md-32 w700 lh140 black-clr yellow-brush">
               Then Sell Them to Our Secret Buyers Network of 3.2 million Users...
               </div>
            </div>
            <div class="col-12 mt20 text-center">
               <div class="f-22 f-md-24 w400 lh140 white-clr">
               &amp; Making Us <u>$382.38 Daily with Literally Zero Work. </u> Even A 100% Beginner Can Do It.          
               </div>
            </div>

            <div class="col-12 mt20 text-center">
               <div class="f-18 f-md-22 w400 lh140 red-bg-voice white-clr">
                  Without Hiring Anyone - No Writing Content - No Experience - No Hidden Fee - No BS
               </div>
            </div>
         </div>

         <div class="row mt20 mt-md40 gx-md-5">
            <div class="col-md-7 col-12">
               <div class="col-12">
                  <!-- <div class="f-18 f-md-19 w500 lh140 white-clr shape-head">
                  Watch How We Swiped A Profitable Funnel With Just One Click And Made $30,345.34 Per Month On Complete Autopilot…
                  </div> -->
                  <div class="video-box mt20 mt-md30">
                     <!-- <img src="assets/images/video-thumb.webp" alt="Video" class="mx-auto d-block img-fluid"> -->
                     <div style="padding-bottom: 56.25%;position: relative;">
                     <iframe src="https://voicefusionai.oppyo.com/video/embed/i6871gjjt7" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe></div>
                  </div>
               </div>
            </div>
            <div class="col-md-5 col-12 mt20 mt-md0">
               <div class="key-features-bg">
                  <ul class="list-head pl0 m0 f-16 f-md-18 lh160 w400 white-clr">
                     <li><span class="w700">Eliminate All the Guesswork</span> and Jump Straight to Results</li>
                     <li><span class="w700">	Let Ai Create Real Human Emotion Based Voices</span></li>
                     <li>Create Complete <span class="w700">AudioBooks &amp; Podcasts</span> Easily</li>
                     <li><span class="w700">	Even Ai Write High Quality Unique Content for You</span></li>
                     <li><span class="w700">A True Ai App </span> That Leverages IBM's Watson &amp; ChatGPT4 Technology</li>
                     <li><span class="w700">Instant High Converting Traffic For 100% Free…</span> No Need to Run Ads</li>
                     <li><span class="w700">300+ Real Voices</span> to Choose from with Human Emotions</li>
                     <li><span classs="w700">100+ Languages</span> to Choose From</li>
                     <li><span class="w700">DFY 1 million Articles</span> with Full PLR License</li>
                     <li><span class="w700">Create Unlimited</span> VSL’s, Sales Copies, Emails, Ads Copy Etc.</li>
                     <li><span class="w700">Turn Your Old Boring Robotic Voices</span> into a Real Human Voices</li>
                     <li>98% Of Beta Testers Made <span class="w700">At Least One Sale Within 24 Hours</span> of Using VoiceFusionAi</li>
                     <li><span class="w700">Pay Only Once</span> &amp; Get Profit Forever</li>
                     <li><span class="w700">No Complicated Setup</span> - Get Up and Running In 2 Minutes</li>
                     <li> <span class="w700">30 Days Money-Back Guarantee</span></li>
                     <li><span class="w700">Free COMMERCIAL LICENSE Included</span> - Serve Unlimited Clients</li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="gr-1">
      <div class="container-fluid p0">
         <picture>
            <source media="(min-width:768px)" srcset="assets/images/voicefusionai.webp">
            <source media="(min-width:320px)" srcset="assets/images/voicefusionai-mview.webp">
            <img src="assets/images/voicefusionai.webp" alt="Steps" style="width:100%;">
         </picture>
      </div>
   </div>
   <!-- header-section- voice-fusion  End-->
   <!-------Exclusive Bonus-Two-Start--------->
   <div class="exclusive-bonus">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <div class="white-clr f-24 f-md-36 lh140 w600">
                  Exclusive Bonus #2 : Ninja AI
               </div>
               <div class="f-18 f-md-20 w500 mt20 white-clr">
               On Purchasing fe you will get 20 accounts as a RESELLER for the  Ninja AI to resell it. And on purchasing any OTO you will get 200 accounts as a RESELLER for the  Ninja AI to resell it. 
               </div>
            </div>
         </div>
      </div>
   </div>
    <!-------Exclusive Bonus-Two-End--------->
   <div class="header-section-ninja">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
         <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 645.02 151.26" style="max-height:70px">
               <defs>
                  <style>
                  .cls-1ai{
                     fill: url(#linear-gradient-2);
                  }
               
                  .cls-2ai{
                     fill: #fff;
                  }
               
                  .cls-3ai{
                     fill-rule: evenodd;
                  }
               
                  .cls-3ai, .cls-4ai{
                     fill: #ff813b;
                  }
               
                  .cls-5ai{
                     fill: url(#linear-gradient-3);
                  }
               
                  .cls-6ai{
                     fill: url(#linear-gradient);
                  }
               
                  .cls-7ai{
                     fill: url(#linear-gradient-4);
                  }
                  </style>
                  <linearGradient id="linear-gradient" x1="47.81" y1="75" x2="185.48" y2="75" gradientUnits="userSpaceOnUse">
                  <stop offset="0" stop-color="#b956ff"></stop>
                  <stop offset="1" stop-color="#6e33ff"></stop>
                  </linearGradient>
                  <linearGradient id="linear-gradient-2" x1="0" y1="75" x2="85.18" y2="75" gradientUnits="userSpaceOnUse">
                  <stop offset="0" stop-color="#b956ff"></stop>
                  <stop offset="1" stop-color="#a356fa"></stop>
                  </linearGradient>
                  <linearGradient id="linear-gradient-3" x1="55.61" y1="62.54" x2="62.86" y2="62.54" gradientUnits="userSpaceOnUse">
                  <stop offset="0" stop-color="#b956ff"></stop>
                  <stop offset="1" stop-color="#ac59fa"></stop>
                  </linearGradient>
                  <linearGradient id="linear-gradient-4" x1="54.62" y1="79" x2="61.87" y2="79" gradientTransform="translate(19.64 -11.24) rotate(13.24)" xlink:href="#linear-gradient-3"></linearGradient>
               </defs>
               <g id="Layer_1-2" data-name="Layer 1">
                  <g>
                  <g>
                     <g>
                     <path class="cls-6ai" d="m181.49,58.53h-10.72c-1.4-5.13-3.43-9.99-6.01-14.51l7.58-7.58c1.56-1.56,1.56-4.09,0-5.65l-17.65-17.65c-1.56-1.56-4.09-1.56-5.65,0l-7.58,7.58c-4.51-2.58-9.38-4.61-14.51-6.01V4c0-2.21-1.79-4-4-4h-24.96c-2.21,0-4,1.79-4,4v10.72c-.16.04-.31.09-.47.14-.27.08-.53.15-.8.23-.27.08-.55.16-.82.24-.55.17-1.09.35-1.63.53-.11.04-.22.08-.33.12-.48.17-.96.34-1.44.52-.12.04-.24.09-.36.14-.52.2-1.04.4-1.56.62-.04.02-.08.03-.12.05-2.41,1-4.74,2.15-6.99,3.43l-7.57-7.57c-1.56-1.56-4.09-1.56-5.65,0l-17.65,17.65c-.36.36-.63.76-.82,1.2h61.99v.02c.23,0,.45-.02.68-.02,23.76,0,43.01,19.26,43.01,43.01s-19.26,43.01-43.01,43.01c-.5,0-1-.02-1.5-.04v.04h-61.17c.19.43.46.84.82,1.2l17.65,17.65c1.56,1.56,4.09,1.56,5.65,0l7.57-7.57c2.25,1.28,4.58,2.43,6.99,3.43.04.02.08.03.12.05.51.21,1.03.42,1.55.62.12.05.24.09.36.14.47.18.95.35,1.43.52.11.04.23.08.34.12.54.18,1.08.36,1.63.53.27.08.55.16.83.25.26.08.53.16.79.23.16.04.31.09.47.14v10.72c0,2.21,1.79,4,4,4h24.96c2.21,0,4-1.79,4-4v-10.72c5.13-1.4,9.99-3.43,14.51-6.01l7.58,7.58c1.56,1.56,4.09,1.56,5.65,0l17.65-17.65c1.56-1.56,1.56-4.09,0-5.65l-7.58-7.58c2.58-4.51,4.61-9.38,6.01-14.51h10.72c2.21,0,4-1.79,4-4v-24.96c0-2.21-1.79-4-4-4Z"></path>
                     <path class="cls-1ai" d="m76.64,101.54c-5.74-7.31-9.18-16.52-9.18-26.54s3.44-19.23,9.18-26.54c2.45-3.13,5.33-5.9,8.53-8.24h-48.39c-1.31-1.89-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.13h17.07c-1.51,3.21-2.76,6.58-3.71,10.07h3.1c1.31-1.89,3.49-3.13,5.97-3.13,4.01,0,7.25,3.25,7.25,7.25s-3.22,7.23-7.21,7.25h-.04c-2.48,0-4.66-1.24-5.97-3.13H13.22c-.44-.64-.98-1.19-1.59-1.65-1.21-.92-2.73-1.48-4.38-1.48-4,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.14h38.82c1.31-1.89,3.49-3.13,5.97-3.13.43,0,.84.05,1.25.12,3.41.59,6,3.56,6,7.14,0,2.78-1.56,5.19-3.86,6.41-1.01.53-2.16.84-3.39.84-2.48,0-4.66-1.24-5.97-3.13h-17.85c-1.31-1.9-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.13h15.96c.95,3.48,2.2,6.85,3.71,10.07h-5.63c-1.31-1.89-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25,0,1.53.48,2.95,1.28,4.12,1.31,1.9,3.49,3.14,5.97,3.14s4.66-1.24,5.97-3.14h36.95c-3.21-2.34-6.08-5.11-8.53-8.24ZM30.64,47.97c-2,0-3.62-1.62-3.62-3.62s1.62-3.62,3.62-3.62,3.63,1.62,3.63,3.62-1.63,3.62-3.63,3.62Zm-23.39,26.47c-2,0-3.62-1.63-3.62-3.62s1.63-3.63,3.62-3.63,3.63,1.63,3.63,3.63-1.63,3.62-3.63,3.62Zm20.97,16.45c-2,0-3.63-1.63-3.63-3.63s1.63-3.62,3.63-3.62,3.62,1.63,3.62,3.62-1.63,3.63-3.62,3.63Zm14.08,18.39c-2,0-3.62-1.62-3.62-3.62s1.62-3.62,3.62-3.62,3.62,1.62,3.62,3.62-1.62,3.62-3.62,3.62Z"></path>
                     <circle class="cls-5ai" cx="59.23" cy="62.54" r="3.63"></circle>
                     <circle class="cls-7ai" cx="58.24" cy="79" r="3.63" transform="translate(-16.54 15.44) rotate(-13.24)"></circle>
                     </g>
                     <g>
                     <g>
                        <path class="cls-3ai" d="m104.63,78.16c-9.44,6-14.52,2.86-15.25-9.4,4.93,3.38,10.02,6.52,15.25,9.4Z"></path>
                        <path class="cls-3ai" d="m131.01,68.76c-.72,12.26-5.81,15.4-15.25,9.4,5.24-2.88,10.32-6.02,15.25-9.4Z"></path>
                     </g>
                     <path class="cls-4ai" d="m110.16,40.13c-.89,0-1.78.03-2.66.1-.08,0-.16,0-.24.02-9.59.79-18.07,5.45-23.89,12.42-.25.3-.49.59-.73.89-.14.19-.29.38-.43.57-4.02,5.37-6.52,11.93-6.88,19.07,0,.01,0,.03,0,.04-.01.22-.02.45-.03.68-.01.3-.02.6-.02.9v.18c0,.28,0,.55,0,.83.44,18.64,15.5,33.66,34.15,34.04.24,0,.48,0,.72,0,.3,0,.6,0,.9,0,18.85-.48,33.98-15.9,33.98-34.87s-15.61-34.88-34.88-34.88Zm.19,39.35c-14.41,14.37-29.15,1.23-29.15-9.45s13.05,0,29.15,0,29.15-10.67,29.15,0-14.74,23.81-29.15,9.45Z"></path>
                     </g>
                  </g>
                  <g>
                     <path class="cls-2ai" d="m286.65,118.51h-17.37l-39.32-59.42v59.42h-17.37V31.8h17.37l39.32,59.54V31.8h17.37v86.71Z"></path>
                     <path class="cls-2ai" d="m304.95,38.69c-2.03-1.94-3.04-4.36-3.04-7.26s1.01-5.31,3.04-7.26c2.03-1.94,4.57-2.92,7.63-2.92s5.6.97,7.63,2.92c2.02,1.94,3.04,4.36,3.04,7.26s-1.02,5.31-3.04,7.26c-2.03,1.94-4.57,2.91-7.63,2.91s-5.6-.97-7.63-2.91Zm16.19,11.1v68.72h-17.37V49.79h17.37Z"></path>
                     <path class="cls-2ai" d="m396.18,56.55c5.04,5.17,7.57,12.38,7.57,21.65v40.31h-17.36v-37.96c0-5.46-1.37-9.65-4.1-12.59-2.73-2.94-6.45-4.4-11.16-4.4s-8.58,1.47-11.35,4.4c-2.77,2.94-4.15,7.13-4.15,12.59v37.96h-17.37V49.79h17.37v8.56c2.31-2.98,5.27-5.31,8.87-7.01,3.6-1.69,7.55-2.54,11.85-2.54,8.19,0,14.8,2.59,19.85,7.75Z"></path>
                     <path class="cls-2ai" d="m437.61,129.8c0,7.61-1.88,13.09-5.64,16.44-3.77,3.35-9.16,5.02-16.19,5.02h-7.69v-14.76h4.96c2.64,0,4.51-.52,5.58-1.55,1.07-1.03,1.61-2.71,1.61-5.02V49.79h17.37v80.01Zm-16.31-91.11c-2.03-1.94-3.04-4.36-3.04-7.26s1.01-5.31,3.04-7.26c2.02-1.94,4.61-2.92,7.75-2.92s5.58.97,7.57,2.92c1.98,1.94,2.98,4.36,2.98,7.26s-.99,5.31-2.98,7.26c-1.98,1.94-4.51,2.91-7.57,2.91s-5.73-.97-7.75-2.91Z"></path>
                     <path class="cls-2ai" d="m454.42,65.42c2.77-5.37,6.53-9.51,11.29-12.4,4.76-2.89,10.07-4.34,15.94-4.34,5.13,0,9.61,1.04,13.46,3.1,3.85,2.07,6.92,4.67,9.24,7.82v-9.8h17.49v68.72h-17.49v-10.05c-2.23,3.23-5.31,5.89-9.24,8-3.93,2.11-8.46,3.16-13.58,3.16-5.79,0-11.06-1.49-15.82-4.47-4.76-2.98-8.52-7.17-11.29-12.59-2.77-5.41-4.16-11.64-4.16-18.67s1.38-13.11,4.16-18.48Zm47.45,7.88c-1.65-3.02-3.89-5.33-6.7-6.95-2.81-1.61-5.83-2.42-9.06-2.42s-6.2.79-8.93,2.36c-2.73,1.57-4.94,3.87-6.64,6.88-1.69,3.02-2.54,6.6-2.54,10.73s.85,7.75,2.54,10.85c1.69,3.1,3.93,5.48,6.7,7.13,2.77,1.66,5.72,2.48,8.87,2.48s6.24-.81,9.06-2.42c2.81-1.61,5.04-3.93,6.7-6.95,1.65-3.02,2.48-6.64,2.48-10.85s-.83-7.83-2.48-10.85Z"></path>
                     <path class="cls-4ai" d="m591.93,102.01h-34.48l-5.71,16.5h-18.24l31.14-86.71h20.22l31.13,86.71h-18.36l-5.71-16.5Zm-4.71-13.89l-12.53-36.22-12.53,36.22h25.06Z"></path>
                     <path class="cls-4ai" d="m645.02,31.93v86.58h-17.37V31.93h17.37Z"></path>
                  </g>
                  </g>
               </g>
            </svg>
            </div>
            <div class="col-12 text-center lh150 mt20 mt-md50">
               <div class="pre-heading-ninja f-18 f-md-22 w600 lh140 white-clr">
                  Legally... Swipe Guru's Profitable Funnels with Our “ChatGPT4 Secret Agent”
               </div>
            </div>
            <div class="col-12 mt-md30 mt20 f-md-42 f-28 w400 text-center white-clr lh140">
               World's First Google-Killer Ai App Builds Us <br class="d-none d-md-block">
               <span class="white-clr w700">DFY Profitable Affiliate Funnel Sites, Prefilled with Content, Reviews &amp; Lead Magnet in Just 60 Seconds.</span>
            </div>
            <div class="col-12 mt-md15 mt15 text-center">
               <div class="f-22 f-md-28 w700 lh140 black-clr yellow-brush">
                  Then Promote Them to Our Secret Buyers Network of 496 million Users...
               </div>
            </div>
            <div class="col-12 mt20 text-center">
               <div class="f-22 f-md-28 w400 lh140 white-clr">
                  &amp; Making Us <span class="w600 underline">$955.45 Commissions Daily</span>  with Literally Zero Work                 
               </div>
            </div>
            <div class="col-12 mt20 text-center">
               <div class="f-20 f-md-22 w400 lh140 red-bg-ninja white-clr">
                  No Designing, No Copywriting, &amp; No Hosting…. Even A 100% Beginner Can Do It.
               </div>
            </div>
         </div>
         <div class="row mt20 mt-md40 gx-md-5">
            <div class="col-md-7 col-12">
               <div class="col-12">
                  <div class="f-18 f-md-19 w500 lh140 white-clr shape-head">
                  Watch How We Swiped A Profitable Funnel With Just One Click And Made $30,345.34 Per Month On Complete Autopilot…
                  </div>
                  <div class="video-box mt20 mt-md30">
                     <!-- <img src="assets/images/video-thumb.webp" alt="Video" class="mx-auto d-block img-fluid"> -->
                     <div style="padding-bottom: 56.25%;position: relative;">
                     <iframe src="https://ninjaai.dotcompal.com/video/embed/q5429rqlji" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe></div>
                  </div>
               </div>
            </div>
            <div class="col-md-5 col-12 mt20 mt-md0">
               <div class="key-features-bg">
                  <ul class="list-head-ninja pl0 m0 f-16 f-md-18 lh160 w400 white-clr">
                     <li><span class="w600">Eliminate All The Guesswork</span> And Jump Straight to Results</li>
                     <li><span class="w600">Let Ai Find</span> Hundreds Of The Guru's Profitable Funnels</li>
                     <li><span class="w600">Let Ai Create</span> Proven Money Making Funnels On Autopilot</li>
                     <li class="w600">All The Copywriting And Designing Is Done For You</li>
                     <li>Generate 100% <span class="w600">SEO &amp; Mobile Optimized</span> Funnels</li>
                     <li><span class="w600">Instant High Converting Traffic For 100% Free…</span> No Need To Run Ads</li>
                     <li>98% Of Beta Testers Made At Least One Sale <span class="w600 underline">Within 24 Hours</span> Of Using Ninja AI.</li>
                     <li><span class="w600">A True Ai App</span> That Leverages ChatGPT4</li>
                     <li>Connect Your Favourite Autoresponders And <span class="w600">Build Your List FAST.</span></li>
                     <li>Seamless Integration With Any Payment Processor You Want</li>
                     <li class="w600">ZERO Upfront Cost</li>
                     <li>No Complicated Setup - Get Up And Running In 2 Minutes</li>
                     <li class="w600">So Easy, Anyone Can Do it.</li>
                     <li>30 Days Money-Back Guarantee</li>
                     <li><span class="w600">Free Commercial License Included -</span> That's Priceless</li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="gr-1">
      <div class="container-fluid p0">
         <picture>
            <source media="(min-width:768px)" srcset="assets/images/ninja-ai.webp">
            <source media="(min-width:320px)" srcset="assets/images/ninja-ai-mview.webp">
            <img src="assets/images/ninja-ai.webp" alt="Steps" style="width:100%;">
         </picture>
      </div>
   </div>
   <!-------Exclusive Bonus-Three-Start--------->
   <div class="exclusive-bonus">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <div class="white-clr f-24 f-md-36 lh140 w600">
                  Exclusive Bonus #3 : WebAI Studio
               </div>
               <div class="f-18 f-md-20 w500 mt20 white-clr">
               On Purchasing fe you will get 20 accounts as a RESELLER for the WebAI Studio to resell it. And on purchasing any OTO you will get 200 accounts as a RESELLER for the  WebAI Studio to resell it. 
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-------Exclusive Bonus-Three-End--------->
   <!-- header-section- WebAI Studio Start-->
   <div class="header-section-webstudio">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <img src="assets/images/logo2.png" class="img-fluid d-block mx-auto my10" alt="Date">
            </div>
            <div class="col-12 text-center mt20 mt-md50">
               <div class="pre-heading f-md-22 f-20 w600 lh140 white-clr">
                  We Exploited ChatGPT4 To Create A Better And Smarter AI Model...
               </div>
            </div>
            <div class="col-12 mt-md25 mt20 f-md-50 f-28 w700 text-center white-clr lh140">
               World's First A.I Bot <span class="orange-gradient-webstudio"> Builds Us DFY Websites Prefilled With Smoking Hot Content…</span> 
            </div>
            <div class="col-12 mt-md15 mt20 f-22 f-md-28 w700 text-center lh140 white-clr text-capitalize">
               Then Promote It To 495 MILLION Buyers For 100% Free…
            </div>
            <div class="col-12 mt-md20 mt20 f-22 f-md-28 w700 text-center lh140 black-clr text-capitalize">
               <div class="head-yellow">
                  Banking Us $578.34 Daily With Zero Work
               </div> 
            </div>
            <div class="col-12 mt-md15 mt20 f-20 f-md-22 w600 text-center lh140 orange-gradient-webstudio text-capitalize">
               Without Creating Anything | Without Writing Content | Without Promoting | Without Paid Ads
            </div>
            <div class="col-12 mt-md15 mt20 f-20 f-md-22 w500 text-center lh140 white-clr text-capitalize">
               No Tech Skills Needed - No Experience Required - No Hidden Fees - No BS
            </div>
            <div class="col-12 mt20 mt-md40">
               <div class="row">
                  <div class="col-md-7 col-12 min-md-video-width-left">
                     <div class="f-18 f-md-20 w600 lh140 text-center white-clr blue-design">
                     Watch How We Deploy Profitable AI-Managed Websites That Makes Us Over $17,349 Monthly In Less Than 30 Seconds…
                     </div>

                      <div class="col-12 mt20 mt-md30">
                        <div class="video-box">
                           <div style="padding-bottom: 56.25%;position: relative;">
                              <iframe src="https://player.vimeo.com/video/853717917?h=723d3f94a6" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" class="dsfsdfds" width="640" height="480" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen=""></iframe>
                           </div>
                        </div>      
                     </div>
                  </div>
                  <div class="col-md-5 col-12 mt20 mt-md0 min-md-video-width-right">
                     <div class="key-features-bg">
                        <ul class="list-head-webstudio pl0 m0 f-18 f-md-20 lh150 w400 white-clr">
                           <li>Deploy Stunning Websites By Leveraging ChatGPT…</li>
                           <li><span class="w600">All Websites Are Prefilled With Smoking Hot, AI, Human-Like Content</span></li>
                           <li>Instant High Convertring Traffic For 100% Free</li>
                           <li><span class="w600">100% Of Beta Testers Made At Least $100 Within 24 Hours Of Using WebAIStudio</span></li>
                           <li>HighTicket Offers For DFY Monetization</li>
                           <li><span class="w600">No Complicated Setup - Get Up And Running In 2 Minutes</span></li>
                           <li><span class="w600">We Let AI Do All The Work For Us.</span></li>
                           <li>Get Up And Running In 30 Seconds Or Less </li>
                           <li><span class="w600">Instantly Tap Into $1.3 Trillion Platform</span> </li>
                           <li>Leverage A Better And Smarter AI Model Than ChatGPT4</li>
                           <li><span class="w600">99.99% Up Time Guaranteed</span> </li>
                           <li>ZERO Upfront Cost</li>
                           <li><span class="w600">30 Days Money-Back Guarantee</span></li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="gr-1">
      <div class="container-fluid p0">
         <picture>
            <source media="(min-width:768px)" srcset="assets/images/webaistudio.webp">
            <source media="(min-width:320px)" srcset="assets/images/webaistudio-mview.webp">
            <img src="assets/images/webaistudio.webp" alt="Steps" style="width:100%;">
         </picture>
      </div>
   </div>
   <!-- header-section- WebAI Studio End-->
   <!-- Bonus Section Header Start -->
   <div class="bonus-header">
      <div class="container">
         <div class="row">
            <div class="col-12 col-md-10 mx-auto heading-bg text-center">
               <div class="f-24 f-md-36 lh140 w600"> When You Purchase TubeTrafficAi, You Also Get <br class="hidden-xs"> Instant Access To These 20 Exclusive Bonuses</div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus Section Header End -->
   <!-- Bonus #1 Section Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w600">BONUS 1</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus1.webp" class="img-fluid mx-auto d-block" alt="Bonus1">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w600 lh140 bonus-title-color">Resilience Video Upgrade  </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li class="w600">With this video course you will learn how to build mental strength to boost resilience and overcome 
                           any difficult situation in life.</li>
                        <li>Learning how to bounce back is teachable and learnable. </li>
                        <li>It is imperative to fully understand what resilience is, learn about its components, and learn about resilience 
                           boosting techniques.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #1 Section End -->
   <!-- Bonus #2 Section Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w600">BONUS 2</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30">
               <div class="row">
                  <div class="col-md-5 col-12 order-md-2">
                     <img src="assets/images/bonus2.webp" class="img-fluid mx-auto d-block" alt="Bonus2">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                     <div class="f-22 f-md-32 w600 lh140 bonus-title-color mt20 mt-md0">
                     Facebook Ad Secrets Advance 
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li class="w600">While the FB Ad Secrets was made for beginners just starting in Facebook advertising, these videos answer 
                           the coming challenges you will face as you scale to spending hundreds or thousands of dollars per day while 
                           remaining profitable. </li>
                        <li>For beginners, you will usually start with a low budget to run your Facebook ads. But eventually when the time comes 
                           for you to scale your offer with a bigger budget, you will come across problems such as your ad account being disabled,
                            profit margin becomes thinner and things like when and how to split test your ads. </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #2 Section End -->
   <!-- Bonus #3 Section Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w600">BONUS 3</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus3.webp" class="img-fluid mx-auto d-block " alt="Bonus3">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w600 lh140 bonus-title-color">
                     Modern Niche Marketing Video Upgrade 
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li class="w600">With this video guide you will learn everything that you need to know to properly assess niche ideas for 
                           their viability and how to drive targeted traffic to your niche website.</li>
                        <li>There is more competition these days which means that you really have to be smart to succeed. If you follow the advice 
                           in this guide then you will provide yourself with the maximum chance of success. Don’t believe the hype – niche marketing 
                           does work if you do it correctly.</li>
                        <li>There are niche websites being sold every day for large amounts of money. These websites are generating regular income 
                           and have a good following. </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #3 Section End -->
   <!-- Bonus #4 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w600">BONUS 4</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12 order-md-2">
                     <img src="assets/images/bonus4.webp" class="img-fluid mx-auto d-block " alt="Bonus4">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                     <div class="f-22 f-md-32 w600 lh140 bonus-title-color">
                     Web Traffic Excellence
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li class="w600">
                        You can have the best product or the best service in the world but if you have no traffic – it’s all completely 
                        worthless.</li>
                        <li>This 5 - part video course will help new and experienced marketers generate huge amount of traffic from five 
                           different sources.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #4 End -->
   <!-- Bonus #5 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w600">BONUS 5</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus5.webp" class="img-fluid mx-auto d-block " alt="Bonus5">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w600 lh140 bonus-title-color">
                     Video Marketing Blueprint v2 
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Who Else Wants To Use Videos To Market Their Business And Generate Massive Leads ... Starting Today?</li>
                        <li>Video marketing has become one of the highest rising and most popular traffic generations. You?ve made the right decision 
                           in investing in this course where you?ll learn how to get torrents of traffic in just a few easy steps.</li>
                        <li>Video Marketing Blueprint is the key to your marketing success. With it, you will find out how to grow your mailing 
                           list, generate massive traffic to your website, increase the conversion and purchase rate, of course, so much more.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #5 End -->
   <!-- CTA Button Section Start -->
   <div class="dark-cta-sec">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-md-12 col-12 text-center ">
               <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
               <div class="f-md-36 f-22 text-center  lh120 w600 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
               <div class="f-18 f-md-22 lh140 w400 text-center mt20 white-clr">
                  Use Special Coupon <span class="w600 yellow-clr">"Trafficadmin"</span> For <span class="w600 yellow-clr"> 32% Off</span> On Full Funnel
                  on Commercial Licence
               </div>
            </div>
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab TubeTrafficAi + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
               <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-20 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
               <div class="countdown counter-white white-clr">
                  <div class="timer-label text-center">
                     <span class="f-31 f-md-60 timerbg oswald w600">01</span>
                     <span class="f-14 f-md-18 w500 ">Days</span>
                  </div>
                  <div class="timer-label text-center">
                     <span class="f-31 f-md-60 timerbg oswald w600">16</span>
                     <span class="f-14 f-md-18 w500 ">Hours</span>
                  </div>
                  <div class="timer-label text-center timer-mrgn">
                     <span class="f-31 f-md-60 timerbg oswald w600">59</span>
                     <span class="f-14 f-md-18 w500 ">Mins</span>
                  </div>
                  <div class="timer-label text-center ">
                     <span class="f-31 f-md-60 timerbg oswald w600">37</span>
                     <span class="f-14 f-md-18 w500 ">Sec</span>
                  </div>
               </div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>
   <!-- CTA Button Section End -->
   <!-- Bonus #6 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w600">BONUS 6</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12 order-md-2">
                     <img src="assets/images/bonus6.webp" class="img-fluid mx-auto d-block " alt="Bonus6">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                     <div class="f-22 f-md-32 w600 lh140 bonus-title-color">
                     Site Speed Secrets Video Upgrade 
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Discover how you can finally speed up your website and increase your conversion rates. All the information you need is 
                           here, and nothing has been left out in this course! </li>
                        <li>Not all web hosts are created equal – make sure you sign up for a fast web host that will help you reach your business 
                           goals.  </li>
                        <li>Know how to use and analyze the results of the best website speed testing tools out there.  </li>
                        <li>Never underestimate the power of caching ever again – it will help your site load much faster than ever before. </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #6 End -->
   <!-- Bonus #7 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w600">BONUS 7</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus7.webp" class="img-fluid mx-auto d-block " alt="Bonus7">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w600 lh140 bonus-title-color">
                     Shopify Traffic
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Compared to 10 years ago, starting an online e-store is not as hard anymore today. Driven by the dream of having an internet
                            lifestyle, and the rewarding monthly income, everyone is triggered to own a business for a better life.</li>
                        <li>With all the platforms and opportunities available, it's easy to kick start an online business anytime you want. Even if you
                            have little budget, you can start an online shop on a small scale. </li>
                        <li>Shopify Traffic is a series of training course that will teach you how to generate traffic to your Shopify e-store with
                            effective methods and platform from personal experience and culmination of researches together with years of studies. </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #7 End -->
   <!-- Bonus #8 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w600">BONUS 8</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="assets/images/bonus8.webp" class="img-fluid mx-auto d-block " alt="Bonus8">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w600 lh140 bonus-title-color">
                     Content Syndication 
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Content Syndication Videos is a series of 40 On-Screen, Easy-To-Follow Video Tutorials on how to market and publish your 
                           content expertly.</li>
                        <li>The Internet is full of 'me too marketers' and what makes this frustrating is that even if you're a genuine expert, it's not
                            a guarantee you'll be successful with marketing yourself. </li>
                        <li>This course will help you to get an unfair advantage and stay ahead of the competition. Lessons are short, simple and
                            direct.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #8 End -->
   <!-- Bonus #9 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w600">BONUS 9</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus9.webp" class="img-fluid mx-auto d-block " alt="Bonus9">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w600 lh140 bonus-title-color">
                     Kick Ass Affiliate 
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Kick Ass Affiliate is a video course that will enlighten you how to become a super affiliate even if you don’t have a 
                           huge email list or you don’t have a huge following on your blog or in your social media circle.</li>
                        <li>If you are thinking that you can’t beat those deadbeat affiliates that keep on topping the leader board, inside the 
                           course are you will learn how to change that mentality. </li>
                        <li>You are also about to learn the principle of offering crazy bonuses and how to set this up effectively in your affiliate
                            marketing campaign. If you already have a mailing list, learn how to create a persuasive approach that will make your
                             subscribers buy the product you offer </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #9 End -->
   <!-- Bonus #10 start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w600">BONUS 10</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="assets/images/bonus10.webp" class="img-fluid mx-auto d-block " alt="Bonus10">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w600 lh140 bonus-title-color">
                     100 Split Tests 
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>100 Split Tests is the result of performing over thousands of different split tests across all selling and advertising mediums online - and we've distilled it into one hundred tweaks and 'hacks' you can do to immediately boost your conversion rates!</li>
                        <li>Split testing has been known to drive some pretty compelling results, but that all assumes one thing: that it’s done correctly. There are numerous aspects that you can look into for split testing, and they can get complicated.</li>
                        <li>Is your goal to drive traffic? Is it clicks? Or even opt-ins? A clear result is determined by setting a clear goal at the start. An email subject line that wins at open rate may be different than the winner for click-through rates. </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #10 End -->
   <!-- CTA Button Section Start -->
   <div class="dark-cta-sec">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-md-12 col-12 text-center ">
               <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
               <div class="f-md-36 f-22 text-center  lh120 w600 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
               <div class="f-18 f-md-22 lh140 w400 text-center mt20 white-clr">
                  Use Special Coupon <span class="w600 yellow-clr">"Trafficadmin"</span> For <span class="w600 yellow-clr"> 32% Off</span> On Full Funnel
               </div>
            </div>
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab TubeTrafficAi + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
               <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-20 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
               <div class="countdown counter-white white-clr">
                  <div class="timer-label text-center">
                     <span class="f-31 f-md-60 timerbg oswald w600">01</span>
                     <span class="f-14 f-md-18 w500 ">Days</span>
                  </div>
                  <div class="timer-label text-center">
                     <span class="f-31 f-md-60 timerbg oswald w600">16</span>
                     <span class="f-14 f-md-18 w500 ">Hours</span>
                  </div>
                  <div class="timer-label text-center timer-mrgn">
                     <span class="f-31 f-md-60 timerbg oswald w600">59</span>
                     <span class="f-14 f-md-18 w500 ">Mins</span>
                  </div>
                  <div class="timer-label text-center ">
                     <span class="f-31 f-md-60 timerbg oswald w600">37</span>
                     <span class="f-14 f-md-18 w500 ">Sec</span>
                  </div>
               </div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>
   <!-- CTA Button Section End -->
   <!-- Bonus #11 start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w600">BONUS 11</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus11.webp" class="img-fluid mx-auto d-block " alt="Bonus11">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w600 lh140 bonus-title-color">
                     Copy Cash Secrets 
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li><b>Now You Can Write Compelling Sales Letters That Effectively Generate A Call-To-Action And Potentially Increase Sales Leads On A Regular Basis!</b></li>
                        <li>Words is powerful tool if utilized properly by anyone in marketing or sales. Keyword for ‘guarantee’ is an essential component for most good eCommerce marketing copy. </li>
                        <li>The word “Guaranteed” appeals to consumer emotional triggers like safety and trust. The word works like a safety net which is a promise made by a retailer to a consumer that they will be pleased with their purchase. </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #11 End -->
   <!-- Bonus #12 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w600">BONUS 12</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="assets/images/bonus12.webp" class="img-fluid mx-auto d-block " alt="Bonus12">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w600 lh140 bonus-title-color">
                     Traffic Babylon
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li><b>If You Are Looking To Send Your Website Traffic Even Further Through The Roof, then here's a short, 12 part video series that reveals to you 6 paid and 6 free traffic sources to SKYROCKET your traffic!</b></li>
                        <li>YES! Traffic is indeed the life-blood of your website. But the question is that, how many traffic generating ideas you have for you to perform on your campaign?</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #12 End -->
   <!-- Bonus #13 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w600">BONUS 13</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus13.webp" class="img-fluid mx-auto d-block " alt="Bonus13">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w600 lh140 bonus-title-color">
                     Viral Profit Machine 
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Discover How YOU Can Make MAXIMUM Profits With Minimum Effort By Getting Other People To Willingly Market For You!</li>
                        <li>Unlock The Secrets To Getting Other People To Talk About You And Your Business Willingly Whether They're Paid To Do It Or Not!</li>
                        <li>Announcing: Viral Profit Machine = Here's a sneak peak of what you'll find in this 50 minutes 54 seconds exclusive audio session:</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #13 End -->
   <!-- Bonus #14 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w600">BONUS 14</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="assets/images/bonus14.webp" class="img-fluid mx-auto d-block " alt="Bonus14">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w600 lh140 bonus-title-color">
                     Public Domain Profits 
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>If you're sick and tired of not making any money online then you need to discover the... The Lazy Internet Marketer's Way To Riches</li>
                        <li>Now you too can become a money making internet marketer without busting your butt with super hard work or extra long hours...</li>
                        <li>If you've always dreamed of making more money while being able to do less and less work, THIS is what you've been searching for...</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #14 End -->
   <!-- Bonus #15 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w600">BONUS 15</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus15.webp" class="img-fluid mx-auto d-block " alt="Bonus15">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w600 lh140 bonus-title-color">
                     Keyword Goldrush 
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Keywords are absolutely vital for you to succeed in making money online.</li>
                        <li>Your keywords are how your websites are found by your potential customers using the search engines.</li>
                        <li>If you are not optimised for the right keywords, and you do not rank high for these keywords, then you are going to struggle to get the traffic that you want for making money.</li>
                        <li>This program is all about how to find the keywords you want to target, how to use them, and most importantly, how to profit from them.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #15 End -->
   <!-- CTA Button Section Start -->
   <div class="dark-cta-sec">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-md-12 col-12 text-center ">
               <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
               <div class="f-md-36 f-22 text-center  lh120 w600 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
               <div class="f-18 f-md-22 lh140 w400 text-center mt20 white-clr">
                   Use Special Coupon <span class="w600 yellow-clr">"Trafficadmin"</span> For <span class="w600 yellow-clr"> 32% Off</span> On Full Funnel
               </div>
            </div>
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab TubeTrafficAi + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md20">
               <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-20 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
               <div class="countdown counter-white white-clr">
                  <div class="timer-label text-center">
                     <span class="f-31 f-md-60 timerbg oswald w600">01</span>
                     <span class="f-14 f-md-18 w500 ">Days</span>
                  </div>
                  <div class="timer-label text-center">
                     <span class="f-31 f-md-60 timerbg oswald w600">16</span>
                     <span class="f-14 f-md-18 w500 ">Hours</span>
                  </div>
                  <div class="timer-label text-center timer-mrgn">
                     <span class="f-31 f-md-60 timerbg oswald w600">59</span>
                     <span class="f-14 f-md-18 w500 ">Mins</span>
                  </div>
                  <div class="timer-label text-center ">
                     <span class="f-31 f-md-60 timerbg oswald w600">37</span>
                     <span class="f-14 f-md-18 w500 ">Sec</span>
                  </div>
               </div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>
   <!-- CTA Button Section End -->
   <!-- Bonus #16 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w600">BONUS 16</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="assets/images/bonus16.webp" class="img-fluid mx-auto d-block " alt="Bonus16">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w600 lh140 bonus-title-color">
                     SEO Reborn
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Finally A Fool Proof Way Of Ranking On The First Page Of Google Has Been Cracked And Revealed By A Former Internet Newbie Turned SEO King!</li>
                        <li>Introducing A Complete Underground Secret SEO Solution In Ranking Your Website On The First Page Of Google Without Paying A Dime For Traffic!</li>
                        <li>Finally Rank Higher On The Search Engines Without Having To Worry About Paid Traffic. Take Advantage Of First Page Google Rankings And Easily Beat Your Competition! It Is Time To DOMINATE The Search Engines And Start Being The BIG Player In Your Market!</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #16 End -->
   <!-- Bonus #17 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w600">BONUS 17</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus17.webp" class="img-fluid mx-auto d-block " alt="Bonus17">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w600 lh140 bonus-title-color">
                     Membership Mastery
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>The Lazy Man's Guide To Creating A Membership Site From Scratch : Now You Can Kick The Technical Roadblocks Out of Your Way And Focus On Building Your Own Membership Site - the Right Way! Look over the shoulder of a real membership owner to see how you can setup a membership site quickly from scratch.</li>
                        <li>On the Internet and in the world around you, members only clubs are sprouting everywhere, whether paid or free. This is because Membership sites generally provide access to something that is unique from other sites. They also build communities of people alike. In fact most people want to improve themselves, improve their business... you get the point. So they're willing to join membership sites that help them do that.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #17 End -->
   <!-- Bonus #18 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w600">BONUS 18</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="assets/images/bonus18.webp" class="img-fluid mx-auto d-block " alt="Bonus18">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w600 lh140 bonus-title-color">
                     Marketing Stomp 
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>Discover the Jealously Guarded Secrets of Seemingly Ordinary Folks Generating Extraordinary Incomes With Just a Computer and Internet Connection!</li>
                        <li>There are many ways to make money online and one of these profitable concepts is by selling your own digital products that many people needs.</li>
                        <li>If you are new to internet marketing or online entrepreneurship, having a guide in doing this for the first time is a huge help.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #18 End -->
   <!-- Bonus #19 Start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w600">BONUS 19</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 col-12">
                     <img src="assets/images/bonus19.webp" class="img-fluid mx-auto d-block " alt="Bonus19">
                  </div>
                  <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w600 lh140 bonus-title-color">
                     Customer Product Portals 
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>See how easy it is to put together a central repository where your customers can access all of the products they have purchased from you. At the same time market all of your other products to your buyers passively!</li>
                        <li>Indeed, there are so many ways to make money online. And of those effective ways is selling digital goods because of it's several advantages in terms of delivering the product.</li>
                        <li>But the thing though is that, selling these digital products also has a flaw and that is it can easily be shared or steal by some hackers. </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #19 End -->
   <!-- Bonus #20 start -->
   <div class="section-bonus">
      <div class="container">
         <div class="row">
            <div class="col-12 xstext1">
               <div class="bonus-title-bg">
                  <div class="f-22 f-md-28 lh120 w600">BONUS 20</div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
               <div class="row">
                  <div class="col-md-5 order-md-2 col-12">
                     <img src="assets/images/bonus20.webp" class="img-fluid mx-auto d-block " alt="Bonus20">
                  </div>
                  <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="f-22 f-md-32 w600 lh140 bonus-title-color">
                     Support Desk System 
                     </div>
                     <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li><b>How To Set Up A Professional Email Support Ticket System!</b></li>
                        <li>Reduce Your Refund Rates, Keep Your Buyers Happy And Never Worry About Not Answering Your Customer's Emails Again!</li>
                        <li>Now You Can Side Aside The Mundane Task Of Answering Emails, Set Up A Support Desk For Free And Give Yourself Time To Progress Your Business!</li>
                        <li>8-part video tutorials reveal how you can install a customer support system in just less than an hour and start using it to keep in contact with your prospects and customers!</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Bonus #20 End -->
   <!-- Huge Woth Section Start -->
   <div class="huge-area mt30 mt-md10">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-12 text-center">
               <div class="f-md-65 f-40 lh120 w600 white-clr">That’s Huge Worth of</div>
               <br>
               <div class="f-md-60 f-40 lh120 w600 yellow-clr">$2285!</div>
            </div>
         </div>
      </div>
   </div>
   <!-- Huge Worth Section End -->
   <!-- text Area Start -->
   <div class="white-section pb0">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-12 text-center">
            <div class="f-md-35 f-28 lh140 w600">So what are you waiting for? You have a great opportunity ahead + My 20 Bonus Products are making it's a completely NO Brainer!!</div>
            </div>
         </div>
      </div>
   </div>
   <!-- text Area End -->
   <!-- CTA Button Section Start -->
   <div class="cta-btn-section">
      <div class="container">
         <div class="row">
         <div class="f-18 f-md-22 lh140 w500 text-center mt20 black-clr">
                  Use Special Coupon <span class="w600 red-clr">"Trafficadmin"</span> For <span class="w600 red-clr"> 32% Off</span> On Full Funnel
               </div>
            <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md30">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab TubeTrafficAi + My 20 Exclusive Bonuses</span> <br>
               </a>
            </div>
            <div class="col-12 mt15 mt-md20" align="center">
               <h3 class="f-md-20 f-20 w500 text-center black">My Exclusive Bonuses Are Expiring in...</h3>
            </div>
            <!-- Timer -->
            <div class="col-md-8 col-md-10 mx-auto col-12 text-center">
               <div class="countdown counter-black">
                  <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w600">01</span><br><span class="f-14 f-md-18 w500  ">Days</span> </div>
                  <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w600">16</span><br><span class="f-14 f-md-18 w500 ">Hours</span> </div>
                  <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald w600">59</span><br><span class="f-14 f-md-18 w500 ">Mins</span> </div>
                  <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald w600">37</span><br><span class="f-14 f-md-18 w500 ">Sec</span> </div>
               </div>
            </div>
            <!-- Timer End -->
         </div>
      </div>
   </div>
   <!-- CTA Button Section End -->
   <!--Footer Section Start -->
   <div class="footer-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
               <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 800 131.43" style="max-height:75px;">
               <defs>
                     <style>
                     .cls-1 {
                        fill: #fff;
                     }

                     .cls-2 {
                        fill: #fd2f15;
                     }

                     .cls-3 {
                        fill: #fbbf33;
                     }
                     </style>
               </defs>
               <g id="Layer_1-2" data-name="Layer 1">
                     <g>
                     <g>
                        <path class="cls-3" d="m105.61,78.06l40.05,23.12c6.01,3.47,13.51-.87,13.51-7.8v-46.25c0-6.93-7.51-11.27-13.51-7.8l-40.05,23.12c-6.01,3.47-6.01,12.14,0,15.6Z"/>
                        <g>
                        <rect class="cls-3" x="26.92" y="58.86" width="16.03" height="46.81"/>
                        <path class="cls-2" d="m125.22,26.34h-7.16v46.06c0,32.6-26.43,59.03-59.03,59.03H0v-65.94C0,32.89,26.43,6.45,59.03,6.45h38.52l-12.38,19.88h-26.14c-21.59,0-39.16,17.57-39.16,39.15v46.07h39.16c21.59,0,39.16-17.57,39.16-39.16V26.34h-5.79L108.81,0l16.41,26.34Z"/>
                        <path class="cls-3" d="m67.04,46.21h-16.02v59.46h8.01c2.76,0,5.45-.35,8.01-.99v-58.47Z"/>
                        <path class="cls-3" d="m90.77,33.56h-16.03v68.14c7.57-4.07,13.39-10.99,16.03-19.31v-48.83Z"/>
                        </g>
                     </g>
                     <g>
                        <g>
                        <path class="cls-1" d="m195.69,38.68v-5.58h49.35v5.58h-21.57v62.48h-6.21v-62.48h-21.57Z"/>
                        <path class="cls-1" d="m283.29,81.65v-31.54h5.92v51.05h-5.92v-8.47h-.47c-1.2,2.61-3.09,4.79-5.68,6.53-2.59,1.74-5.76,2.61-9.5,2.61-3.26,0-6.15-.71-8.67-2.14-2.53-1.43-4.51-3.53-5.95-6.3-1.44-2.77-2.16-6.17-2.16-10.2v-33.07h5.92v32.7c0,3.99,1.17,7.2,3.52,9.62,2.35,2.43,5.36,3.64,9.04,3.64,2.3,0,4.52-.55,6.65-1.66,2.13-1.11,3.88-2.73,5.25-4.87,1.37-2.14,2.06-4.77,2.06-7.89Z"/>
                        <path class="cls-1" d="m306.05,101.16V33.09h5.95v26.75h.53c.66-1.53,1.62-3.1,2.87-4.72,1.25-1.62,2.9-2.97,4.95-4.07,2.05-1.1,4.64-1.65,7.76-1.65,4.19,0,7.86,1.1,11.03,3.31,3.17,2.21,5.64,5.28,7.41,9.24,1.77,3.95,2.66,8.56,2.66,13.81s-.88,9.89-2.64,13.86c-1.76,3.97-4.22,7.06-7.38,9.27-3.16,2.22-6.82,3.32-10.98,3.32-3.1,0-5.69-.55-7.76-1.66-2.07-1.11-3.74-2.48-5-4.1-1.26-1.63-2.24-3.22-2.92-4.77h-.73v9.47h-5.75Zm5.85-25.49c0,4.12.61,7.77,1.84,10.95,1.23,3.18,3,5.67,5.32,7.48,2.31,1.81,5.12,2.71,8.42,2.71s6.26-.94,8.61-2.81c2.35-1.87,4.13-4.4,5.35-7.59,1.22-3.19,1.83-6.77,1.83-10.73s-.6-7.46-1.81-10.6c-1.21-3.15-2.99-5.64-5.33-7.49-2.35-1.85-5.23-2.78-8.64-2.78s-6.14.89-8.46,2.68c-2.32,1.78-4.08,4.24-5.3,7.38-1.22,3.14-1.83,6.74-1.83,10.82Z"/>
                        <path class="cls-1" d="m382.86,102.22c-4.76,0-8.88-1.11-12.36-3.34-3.48-2.23-6.16-5.32-8.06-9.27s-2.84-8.51-2.84-13.68.95-9.71,2.84-13.71c1.89-4,4.52-7.13,7.88-9.41,3.36-2.27,7.24-3.41,11.65-3.41,2.77,0,5.44.5,8.01,1.51,2.57,1.01,4.88,2.55,6.93,4.62,2.05,2.07,3.67,4.68,4.87,7.81,1.2,3.14,1.79,6.84,1.79,11.12v2.92h-39.88v-5.22h33.83c0-3.28-.66-6.23-1.98-8.86-1.32-2.63-3.15-4.7-5.48-6.23-2.34-1.53-5.04-2.29-8.09-2.29-3.23,0-6.08.86-8.54,2.59-2.46,1.73-4.38,4.01-5.77,6.85-1.38,2.84-2.09,5.94-2.11,9.31v3.12c0,4.05.7,7.59,2.11,10.62,1.41,3.02,3.41,5.37,6,7.03,2.59,1.66,5.66,2.49,9.21,2.49,2.41,0,4.54-.38,6.36-1.13,1.83-.75,3.37-1.77,4.62-3.04,1.25-1.27,2.2-2.68,2.84-4.2l5.62,1.83c-.78,2.15-2.04,4.13-3.81,5.95-1.76,1.82-3.95,3.27-6.58,4.37-2.63,1.1-5.65,1.65-9.06,1.65Z"/>
                        <path class="cls-1" d="m412.07,44.96v-11.86h55.9v11.86h-20.84v56.2h-14.22v-56.2h-20.84Z"/>
                        <path class="cls-1" d="m471.62,101.16v-51.05h13.73v8.91h.53c.93-3.17,2.49-5.57,4.69-7.2,2.19-1.63,4.72-2.44,7.58-2.44.71,0,1.47.04,2.29.13.82.09,1.54.21,2.16.37v12.56c-.66-.2-1.58-.38-2.76-.53-1.17-.15-2.25-.23-3.22-.23-2.08,0-3.94.45-5.57,1.35-1.63.9-2.91,2.14-3.86,3.74-.94,1.6-1.41,3.43-1.41,5.52v28.88h-14.16Z"/>
                        <path class="cls-1" d="m523,102.12c-3.26,0-6.16-.57-8.71-1.71-2.55-1.14-4.56-2.84-6.03-5.08-1.47-2.25-2.21-5.06-2.21-8.42,0-2.84.52-5.22,1.56-7.15,1.04-1.93,2.46-3.48,4.25-4.65,1.79-1.17,3.84-2.06,6.13-2.66,2.29-.6,4.7-1.02,7.23-1.26,2.97-.31,5.36-.6,7.18-.88,1.82-.28,3.13-.69,3.95-1.25.82-.55,1.23-1.37,1.23-2.46v-.2c0-2.1-.66-3.73-1.98-4.89-1.32-1.15-3.19-1.73-5.6-1.73-2.55,0-4.58.56-6.08,1.68-1.51,1.12-2.5,2.52-2.99,4.2l-13.09-1.06c.66-3.1,1.97-5.79,3.92-8.06,1.95-2.27,4.47-4.02,7.56-5.25,3.09-1.23,6.67-1.84,10.75-1.84,2.84,0,5.55.33,8.16,1,2.6.66,4.92,1.7,6.95,3.09,2.03,1.4,3.63,3.19,4.8,5.37,1.17,2.18,1.76,4.79,1.76,7.83v34.43h-13.43v-7.08h-.4c-.82,1.6-1.92,3-3.29,4.2-1.37,1.21-3.02,2.15-4.95,2.82-1.93.68-4.15,1.01-6.68,1.01Zm4.05-9.77c2.08,0,3.92-.42,5.52-1.25s2.85-1.96,3.76-3.37c.91-1.42,1.36-3.02,1.36-4.82v-5.42c-.44.29-1.05.55-1.81.78s-1.62.44-2.58.63c-.95.19-1.91.35-2.86.5-.95.14-1.82.27-2.59.38-1.66.24-3.11.63-4.35,1.16-1.24.53-2.21,1.25-2.89,2.14-.69.9-1.03,2.01-1.03,3.34,0,1.93.7,3.4,2.11,4.4,1.41,1.01,3.2,1.51,5.37,1.51Z"/>
                        <path class="cls-1" d="m580.03,50.11h10.17v10.63h-10.17v40.41h-14.12v-40.41h-7.21v-10.63h7.21v-3.69c0-3.7.72-6.77,2.18-9.21,1.45-2.44,3.44-4.26,5.97-5.48,2.53-1.22,5.4-1.83,8.61-1.83,2.17,0,4.15.17,5.97.5,1.8.33,3.15.63,4.04.9l-2.53,10.63c-.55-.18-1.23-.34-2.04-.5s-1.64-.23-2.48-.23c-2.08,0-3.53.48-4.35,1.45-.83.96-1.23,2.31-1.23,4.04v3.42Z"/>
                        <path class="cls-1" d="m616.12,50.11h10.17v10.63h-10.17v40.41h-14.12v-40.41h-7.21v-10.63h7.21v-3.69c0-3.7.72-6.77,2.18-9.21,1.45-2.44,3.44-4.26,5.97-5.48,2.53-1.22,5.4-1.83,8.61-1.83,2.17,0,4.15.17,5.97.5,1.8.33,3.15.63,4.04.9l-2.53,10.63c-.55-.18-1.23-.34-2.04-.5-.81-.15-1.64-.23-2.48-.23-2.08,0-3.53.48-4.35,1.45-.83.96-1.23,2.31-1.23,4.04v3.42Z"/>
                        <path class="cls-1" d="m642.05,43.53c-2.11,0-3.9-.7-5.4-2.09-1.5-1.4-2.24-3.08-2.24-5.05s.75-3.66,2.26-5.07c1.51-1.41,3.3-2.11,5.38-2.11s3.91.7,5.4,2.09c1.5,1.4,2.24,3.08,2.24,5.05s-.75,3.66-2.24,5.07c-1.5,1.41-3.3,2.11-5.4,2.11Zm-7.11,57.63v-51.05h14.16v51.05h-14.16Z"/>
                        <path class="cls-1" d="m683.46,102.15c-5.23,0-9.72-1.11-13.48-3.34-3.76-2.23-6.64-5.32-8.64-9.29-2.01-3.97-3.01-8.53-3.01-13.69s1.01-9.82,3.04-13.78c2.03-3.95,4.91-7.05,8.66-9.27,3.74-2.23,8.2-3.34,13.36-3.34,4.45,0,8.35.81,11.7,2.43,3.35,1.62,5.99,3.89,7.94,6.81,1.95,2.92,3.02,6.36,3.22,10.3h-13.36c-.38-2.55-1.37-4.6-2.97-6.16-1.61-1.56-3.71-2.34-6.3-2.34-2.19,0-4.1.59-5.73,1.78-1.63,1.19-2.9,2.91-3.81,5.17s-1.36,5-1.36,8.21.45,6.03,1.35,8.31c.9,2.28,2.17,4.02,3.81,5.22,1.64,1.2,3.56,1.79,5.75,1.79,1.62,0,3.07-.33,4.37-1,1.3-.66,2.37-1.63,3.22-2.91.85-1.27,1.41-2.81,1.68-4.6h13.36c-.22,3.9-1.28,7.33-3.17,10.29-1.89,2.96-4.5,5.27-7.83,6.93-3.32,1.66-7.26,2.49-11.8,2.49Z"/>
                        </g>
                        <g>
                        <path class="cls-3" d="m800,37.72c0,4.31-3.5,7.81-7.82,7.81s-7.81-3.5-7.81-7.81,3.5-7.82,7.81-7.82,7.82,3.5,7.82,7.82Z"/>
                        <path class="cls-2" d="m736.11,85.52c-.09,0-.18,0-.27-.01-.09,0-.18-.02-.26-.03.18.01.35.05.53.05Z"/>
                        <path class="cls-2" d="m784.37,50.18v32.89c0,1.31-1.05,2.37-2.35,2.43-.03,0-.07.02-.11.02h-.37l-18.68-41.36-3.1-6.86c-2.03-4.5-6.52-7.4-11.47-7.4s-9.42,2.9-11.46,7.4l-11.96,26.47-10.4,23.03c-3.04,6.73,1.88,14.36,9.28,14.36h66.01c5.65,0,10.25-4.58,10.25-10.24v-40.74h-15.63Zm-48.25,35.34c-.09,0-.18,0-.27-.01-.09,0-.18-.02-.26-.03-2.25-.33-3.65-2.75-2.67-4.91l12.93-28.64c.95-2.1,3.93-2.1,4.89,0l15.17,33.59h-29.79Z"/>
                        </g>
                     </g>
                     </g>
               </g>
            </svg>
                  <div editabletype="text" class="f-16 f-md-18 w400 lh140 mt20 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
                  <div class=" mt20 mt-md40 white-clr text-center"> <script type="text/javascript" src="https://warriorplus.com/o2/disclaimer/jrrjv5" defer=""></script><div class="wplus_spdisclaimer f-16 f-md-16 w400 lh140 white-clr"><span class="w600">Disclaimer : </span> WarriorPlus is used to help manage the sale of products on this site. While WarriorPlus helps facilitate the sale, all payments are made directly to the product vendor and NOT WarriorPlus. Thus, all product questions, support inquiries and/or refund requests must be sent to the vendor. WarriorPlus's role should not be construed as an endorsement, approval or review of these products or any claim, statement or opinion used in the marketing of these products.</div></div>
              </div>
              <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                    <div class="f-14 f-md-16 w400 lh160 white-clr text-xs-center">Copyright © TubeTrafficAi 2023</div>
                    <ul class="footer-ul w400 f-14 f-md-16 white-clr text-center text-md-right">
                        <li><a href="https://support.bizomart.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                        <li><a href="http://tubetrafficai.com/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                        <li><a href="http://tubetrafficai.com/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                        <li><a href="http://tubetrafficai.com/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                        <li><a href="http://tubetrafficai.com/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                        <li><a href="http://tubetrafficai.com/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                        <li><a href="http://tubetrafficai.com/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                    </ul>
                </div>
          </div>
      </div>
  </div>
   <!-- Back to top button -->
   <a id="button">
      <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 300.003 300.003" style="enable-background:new 0 0 300.003 300.003;" xml:space="preserve">
         <g>
            <g>
               <path d="M150,0C67.159,0,0.001,67.159,0.001,150c0,82.838,67.157,150.003,149.997,150.003S300.002,232.838,300.002,150    C300.002,67.159,232.842,0,150,0z M217.685,189.794c-5.47,5.467-14.338,5.47-19.81,0l-48.26-48.27l-48.522,48.516    c-5.467,5.467-14.338,5.47-19.81,0c-2.731-2.739-4.098-6.321-4.098-9.905s1.367-7.166,4.103-9.897l56.292-56.297    c0.539-0.838,1.157-1.637,1.888-2.368c2.796-2.796,6.476-4.142,10.146-4.077c3.662-0.062,7.348,1.281,10.141,4.08    c0.734,0.729,1.349,1.528,1.886,2.365l56.043,56.043C223.152,175.454,223.156,184.322,217.685,189.794z" />
            </g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
         <g>
         </g>
      </svg>
   </a>
   <!--Footer Section End -->
   <!-- timer --->
   <?php
   if ($now < $exp_date) {
   ?>
      <script type="text/javascript">
         // Count down milliseconds = server_end - server_now = client_end - client_now
         var server_end = <?php echo $exp_date; ?> * 1000;
         var server_now = <?php echo time(); ?> * 1000;
         var client_now = new Date().getTime();
         var end = server_end - server_now + client_now; // this is the real end time

         var noob = $('.countdown').length;

         var _second = 1000;
         var _minute = _second * 60;
         var _hour = _minute * 60;
         var _day = _hour * 24
         var timer;

         function showRemaining() {
            var now = new Date();
            var distance = end - now;
            if (distance < 0) {
               clearInterval(timer);
               document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
               return;
            }

            var days = Math.floor(distance / _day);
            var hours = Math.floor((distance % _day) / _hour);
            var minutes = Math.floor((distance % _hour) / _minute);
            var seconds = Math.floor((distance % _minute) / _second);
            if (days < 10) {
               days = "0" + days;
            }
            if (hours < 10) {
               hours = "0" + hours;
            }
            if (minutes < 10) {
               minutes = "0" + minutes;
            }
            if (seconds < 10) {
               seconds = "0" + seconds;
            }
            var i;
            var countdown = document.getElementsByClassName('countdown');
            for (i = 0; i < noob; i++) {
               countdown[i].innerHTML = '';

               if (days) {
                  countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w600">' + days + '</span><br><span class="f-14 f-md-18 ">Days</span> </div>';
               }

               countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald w600">' + hours + '</span><br><span class="f-14 f-md-18 w500 ">Hours</span> </div>';

               countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald w600">' + minutes + '</span><br><span class="f-14 f-md-18 w500 ">Mins</span> </div>';

               countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald w600">' + seconds + '</span><br><span class="f-14 f-md-18 w500">Sec</span> </div>';
            }

         }
         timer = setInterval(showRemaining, 1000);
      </script>
   <?php
   } else {
      echo "Times Up";
   }
   ?>
   <!--- timer end-->

   <!-- scroll Top to Bottom -->
   <script>
      var btn = $('#button');

      $(window).scroll(function() {
         if ($(window).scrollTop() > 300) {
            btn.addClass('show');
         } else {
            btn.removeClass('show');
         }
      });

      btn.on('click', function(e) {
         e.preventDefault();
         $('html, body').animate({
            scrollTop: 0
         }, '300');
      });
   </script>
</body>

</html>
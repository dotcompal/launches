<!Doctype html>
<html lang="en">
   <head>
   <head>
    <title>TubeTrafficAi | Free Gift</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=9">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta name="title" content="TubeTrafficAi | Free Gift">
    <meta name="description" content="World's First Google-Bard Powered AI App That *Legally* Hacks Into YouTube's 800 Million Videos…">
    <meta name="keywords" content="TubeTrafficAi | Free Gift">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta property="og:image" content="https://tubetrafficai.com/free-gift/thumbnail.png">
    <meta name="language" content="English">
    <meta name="revisit-after" content="1 days">
    <meta name="author" content="Dr. Amit Pareek">
    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:title" content="TubeTrafficAi | Free Gift">
    <meta property="og:description" content="World's First Google-Bard Powered AI App That *Legally* Hacks Into YouTube's 800 Million Videos…">
    <meta property="og:image" content="https://tubetrafficai.com/free-gift/thumbnail.png">
    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:title" content="TubeTrafficAi | Free Gift">
    <meta property="twitter:description" content="World's First Google-Bard Powered AI App That *Legally* Hacks Into YouTube's 800 Million Videos…">
    <meta property="twitter:image" content="https://tubetrafficai.com/free-gift/thumbnail.png">
   <!-- Font-Family -->
    <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&amp;family=Poppins:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&amp;display=swap" rel="stylesheet">
    <!-- Start Editor required -->
    <link rel="stylesheet" href="https://cdn.oppyotest.com/launches/sellero/common_assets/css/bootstrap.min.css" type="text/css">
	<!-- <link rel="stylesheet" type="text/css" href="assets/css/bonus-style.css"> -->
	<link rel="stylesheet" href="../common_assets/css/general.css">
	<link rel="stylesheet" href="assets/css/style.css" type="text/css">
    <link rel="stylesheet" href="assets/css/aweberform.css" type="text/css">
    <script src="https://cdn.oppyotest.com/launches/sellero/common_assets/js/jquery.min.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- End -->
</head>
<body>
	<div class="header-sec" id="product">
        <div class="container px-md-0">
            <div class="row">
				<div class="col-12 text-center">
                    <div class="white-clr f-20 f-md-32">
                        Free Gifts by <span class="w600 yellow-clr">Dr. Amit Pareek's</span> for SuperVIP Customers
                    </div>
                    <div class="uy mt20 mt-md40">
                        <div class="f-20 f-md-24 lh160 w500 black-clr">
                            <b class="f-20 f-md-24 red-clr">Congratulations!</b> you have also <b><span class="red-clr">unlocked Special Exclusive coupon <br class="d-none d-md-block">  "Trafficadmin" </span></b> for my Lightning Fast Software <b><span class="red-clr">TubeTrafficAi</span></b> launch on <br class="d-none d-md-block"> <span class="red-clr w700">15th September 2023  @ 11:00 AM EST</span>
                        </div>
                        <div class="f-20 f-md-24 lh130 w600 mt20 gn-box">Good News :</div>
                        <div class="f-20 f-md-24 lh160 w400 mt20 mt-md30 black-clr">
                            If you want any other <b>BONUSES</b> that are relevant for your business <br class="d-none d-md-block"> or <b>any niche</b> to increase more of your profits then, <b>please feel free <br class="d-none d-md-block"> to email</b> us at <a href="https://support.bizomart.com" target="_blank" class="red-clr">https://support.bizomart.com</a>
                        </div>
                    </div>
                </div>
               	<div class="col-12 mt-md50 mt20 text-center">
				   <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 800 131.43" style="max-height:75px;">
                        <defs>
                            <style>
                            .cls-1 {
                                fill: #fff;
                            }
        
                            .cls-2 {
                                fill: #fd2f15;
                            }
        
                            .cls-3 {
                                fill: #fbbf33;
                            }
                            </style>
                        </defs>
                        <g id="Layer_1-2" data-name="Layer 1">
                            <g>
                            <g>
                                <path class="cls-3" d="m105.61,78.06l40.05,23.12c6.01,3.47,13.51-.87,13.51-7.8v-46.25c0-6.93-7.51-11.27-13.51-7.8l-40.05,23.12c-6.01,3.47-6.01,12.14,0,15.6Z"></path>
                                <g>
                                <rect class="cls-3" x="26.92" y="58.86" width="16.03" height="46.81"></rect>
                                <path class="cls-2" d="m125.22,26.34h-7.16v46.06c0,32.6-26.43,59.03-59.03,59.03H0v-65.94C0,32.89,26.43,6.45,59.03,6.45h38.52l-12.38,19.88h-26.14c-21.59,0-39.16,17.57-39.16,39.15v46.07h39.16c21.59,0,39.16-17.57,39.16-39.16V26.34h-5.79L108.81,0l16.41,26.34Z"></path>
                                <path class="cls-3" d="m67.04,46.21h-16.02v59.46h8.01c2.76,0,5.45-.35,8.01-.99v-58.47Z"></path>
                                <path class="cls-3" d="m90.77,33.56h-16.03v68.14c7.57-4.07,13.39-10.99,16.03-19.31v-48.83Z"></path>
                                </g>
                            </g>
                            <g>
                                <g>
                                <path class="cls-1" d="m195.69,38.68v-5.58h49.35v5.58h-21.57v62.48h-6.21v-62.48h-21.57Z"></path>
                                <path class="cls-1" d="m283.29,81.65v-31.54h5.92v51.05h-5.92v-8.47h-.47c-1.2,2.61-3.09,4.79-5.68,6.53-2.59,1.74-5.76,2.61-9.5,2.61-3.26,0-6.15-.71-8.67-2.14-2.53-1.43-4.51-3.53-5.95-6.3-1.44-2.77-2.16-6.17-2.16-10.2v-33.07h5.92v32.7c0,3.99,1.17,7.2,3.52,9.62,2.35,2.43,5.36,3.64,9.04,3.64,2.3,0,4.52-.55,6.65-1.66,2.13-1.11,3.88-2.73,5.25-4.87,1.37-2.14,2.06-4.77,2.06-7.89Z"></path>
                                <path class="cls-1" d="m306.05,101.16V33.09h5.95v26.75h.53c.66-1.53,1.62-3.1,2.87-4.72,1.25-1.62,2.9-2.97,4.95-4.07,2.05-1.1,4.64-1.65,7.76-1.65,4.19,0,7.86,1.1,11.03,3.31,3.17,2.21,5.64,5.28,7.41,9.24,1.77,3.95,2.66,8.56,2.66,13.81s-.88,9.89-2.64,13.86c-1.76,3.97-4.22,7.06-7.38,9.27-3.16,2.22-6.82,3.32-10.98,3.32-3.1,0-5.69-.55-7.76-1.66-2.07-1.11-3.74-2.48-5-4.1-1.26-1.63-2.24-3.22-2.92-4.77h-.73v9.47h-5.75Zm5.85-25.49c0,4.12.61,7.77,1.84,10.95,1.23,3.18,3,5.67,5.32,7.48,2.31,1.81,5.12,2.71,8.42,2.71s6.26-.94,8.61-2.81c2.35-1.87,4.13-4.4,5.35-7.59,1.22-3.19,1.83-6.77,1.83-10.73s-.6-7.46-1.81-10.6c-1.21-3.15-2.99-5.64-5.33-7.49-2.35-1.85-5.23-2.78-8.64-2.78s-6.14.89-8.46,2.68c-2.32,1.78-4.08,4.24-5.3,7.38-1.22,3.14-1.83,6.74-1.83,10.82Z"></path>
                                <path class="cls-1" d="m382.86,102.22c-4.76,0-8.88-1.11-12.36-3.34-3.48-2.23-6.16-5.32-8.06-9.27s-2.84-8.51-2.84-13.68.95-9.71,2.84-13.71c1.89-4,4.52-7.13,7.88-9.41,3.36-2.27,7.24-3.41,11.65-3.41,2.77,0,5.44.5,8.01,1.51,2.57,1.01,4.88,2.55,6.93,4.62,2.05,2.07,3.67,4.68,4.87,7.81,1.2,3.14,1.79,6.84,1.79,11.12v2.92h-39.88v-5.22h33.83c0-3.28-.66-6.23-1.98-8.86-1.32-2.63-3.15-4.7-5.48-6.23-2.34-1.53-5.04-2.29-8.09-2.29-3.23,0-6.08.86-8.54,2.59-2.46,1.73-4.38,4.01-5.77,6.85-1.38,2.84-2.09,5.94-2.11,9.31v3.12c0,4.05.7,7.59,2.11,10.62,1.41,3.02,3.41,5.37,6,7.03,2.59,1.66,5.66,2.49,9.21,2.49,2.41,0,4.54-.38,6.36-1.13,1.83-.75,3.37-1.77,4.62-3.04,1.25-1.27,2.2-2.68,2.84-4.2l5.62,1.83c-.78,2.15-2.04,4.13-3.81,5.95-1.76,1.82-3.95,3.27-6.58,4.37-2.63,1.1-5.65,1.65-9.06,1.65Z"></path>
                                <path class="cls-1" d="m412.07,44.96v-11.86h55.9v11.86h-20.84v56.2h-14.22v-56.2h-20.84Z"></path>
                                <path class="cls-1" d="m471.62,101.16v-51.05h13.73v8.91h.53c.93-3.17,2.49-5.57,4.69-7.2,2.19-1.63,4.72-2.44,7.58-2.44.71,0,1.47.04,2.29.13.82.09,1.54.21,2.16.37v12.56c-.66-.2-1.58-.38-2.76-.53-1.17-.15-2.25-.23-3.22-.23-2.08,0-3.94.45-5.57,1.35-1.63.9-2.91,2.14-3.86,3.74-.94,1.6-1.41,3.43-1.41,5.52v28.88h-14.16Z"></path>
                                <path class="cls-1" d="m523,102.12c-3.26,0-6.16-.57-8.71-1.71-2.55-1.14-4.56-2.84-6.03-5.08-1.47-2.25-2.21-5.06-2.21-8.42,0-2.84.52-5.22,1.56-7.15,1.04-1.93,2.46-3.48,4.25-4.65,1.79-1.17,3.84-2.06,6.13-2.66,2.29-.6,4.7-1.02,7.23-1.26,2.97-.31,5.36-.6,7.18-.88,1.82-.28,3.13-.69,3.95-1.25.82-.55,1.23-1.37,1.23-2.46v-.2c0-2.1-.66-3.73-1.98-4.89-1.32-1.15-3.19-1.73-5.6-1.73-2.55,0-4.58.56-6.08,1.68-1.51,1.12-2.5,2.52-2.99,4.2l-13.09-1.06c.66-3.1,1.97-5.79,3.92-8.06,1.95-2.27,4.47-4.02,7.56-5.25,3.09-1.23,6.67-1.84,10.75-1.84,2.84,0,5.55.33,8.16,1,2.6.66,4.92,1.7,6.95,3.09,2.03,1.4,3.63,3.19,4.8,5.37,1.17,2.18,1.76,4.79,1.76,7.83v34.43h-13.43v-7.08h-.4c-.82,1.6-1.92,3-3.29,4.2-1.37,1.21-3.02,2.15-4.95,2.82-1.93.68-4.15,1.01-6.68,1.01Zm4.05-9.77c2.08,0,3.92-.42,5.52-1.25s2.85-1.96,3.76-3.37c.91-1.42,1.36-3.02,1.36-4.82v-5.42c-.44.29-1.05.55-1.81.78s-1.62.44-2.58.63c-.95.19-1.91.35-2.86.5-.95.14-1.82.27-2.59.38-1.66.24-3.11.63-4.35,1.16-1.24.53-2.21,1.25-2.89,2.14-.69.9-1.03,2.01-1.03,3.34,0,1.93.7,3.4,2.11,4.4,1.41,1.01,3.2,1.51,5.37,1.51Z"></path>
                                <path class="cls-1" d="m580.03,50.11h10.17v10.63h-10.17v40.41h-14.12v-40.41h-7.21v-10.63h7.21v-3.69c0-3.7.72-6.77,2.18-9.21,1.45-2.44,3.44-4.26,5.97-5.48,2.53-1.22,5.4-1.83,8.61-1.83,2.17,0,4.15.17,5.97.5,1.8.33,3.15.63,4.04.9l-2.53,10.63c-.55-.18-1.23-.34-2.04-.5s-1.64-.23-2.48-.23c-2.08,0-3.53.48-4.35,1.45-.83.96-1.23,2.31-1.23,4.04v3.42Z"></path>
                                <path class="cls-1" d="m616.12,50.11h10.17v10.63h-10.17v40.41h-14.12v-40.41h-7.21v-10.63h7.21v-3.69c0-3.7.72-6.77,2.18-9.21,1.45-2.44,3.44-4.26,5.97-5.48,2.53-1.22,5.4-1.83,8.61-1.83,2.17,0,4.15.17,5.97.5,1.8.33,3.15.63,4.04.9l-2.53,10.63c-.55-.18-1.23-.34-2.04-.5-.81-.15-1.64-.23-2.48-.23-2.08,0-3.53.48-4.35,1.45-.83.96-1.23,2.31-1.23,4.04v3.42Z"></path>
                                <path class="cls-1" d="m642.05,43.53c-2.11,0-3.9-.7-5.4-2.09-1.5-1.4-2.24-3.08-2.24-5.05s.75-3.66,2.26-5.07c1.51-1.41,3.3-2.11,5.38-2.11s3.91.7,5.4,2.09c1.5,1.4,2.24,3.08,2.24,5.05s-.75,3.66-2.24,5.07c-1.5,1.41-3.3,2.11-5.4,2.11Zm-7.11,57.63v-51.05h14.16v51.05h-14.16Z"></path>
                                <path class="cls-1" d="m683.46,102.15c-5.23,0-9.72-1.11-13.48-3.34-3.76-2.23-6.64-5.32-8.64-9.29-2.01-3.97-3.01-8.53-3.01-13.69s1.01-9.82,3.04-13.78c2.03-3.95,4.91-7.05,8.66-9.27,3.74-2.23,8.2-3.34,13.36-3.34,4.45,0,8.35.81,11.7,2.43,3.35,1.62,5.99,3.89,7.94,6.81,1.95,2.92,3.02,6.36,3.22,10.3h-13.36c-.38-2.55-1.37-4.6-2.97-6.16-1.61-1.56-3.71-2.34-6.3-2.34-2.19,0-4.1.59-5.73,1.78-1.63,1.19-2.9,2.91-3.81,5.17s-1.36,5-1.36,8.21.45,6.03,1.35,8.31c.9,2.28,2.17,4.02,3.81,5.22,1.64,1.2,3.56,1.79,5.75,1.79,1.62,0,3.07-.33,4.37-1,1.3-.66,2.37-1.63,3.22-2.91.85-1.27,1.41-2.81,1.68-4.6h13.36c-.22,3.9-1.28,7.33-3.17,10.29-1.89,2.96-4.5,5.27-7.83,6.93-3.32,1.66-7.26,2.49-11.8,2.49Z"></path>
                                </g>
                                <g>
                                <path class="cls-3" d="m800,37.72c0,4.31-3.5,7.81-7.82,7.81s-7.81-3.5-7.81-7.81,3.5-7.82,7.81-7.82,7.82,3.5,7.82,7.82Z"></path>
                                <path class="cls-2" d="m736.11,85.52c-.09,0-.18,0-.27-.01-.09,0-.18-.02-.26-.03.18.01.35.05.53.05Z"></path>
                                <path class="cls-2" d="m784.37,50.18v32.89c0,1.31-1.05,2.37-2.35,2.43-.03,0-.07.02-.11.02h-.37l-18.68-41.36-3.1-6.86c-2.03-4.5-6.52-7.4-11.47-7.4s-9.42,2.9-11.46,7.4l-11.96,26.47-10.4,23.03c-3.04,6.73,1.88,14.36,9.28,14.36h66.01c5.65,0,10.25-4.58,10.25-10.24v-40.74h-15.63Zm-48.25,35.34c-.09,0-.18,0-.27-.01-.09,0-.18-.02-.26-.03-2.25-.33-3.65-2.75-2.67-4.91l12.93-28.64c.95-2.1,3.93-2.1,4.89,0l15.17,33.59h-29.79Z"></path>
                                </g>
                            </g>
                            </g>
                        </g>
                        </svg>
               	</div>
				<div class="col-12 text-center lh150 mt20 mt-md50">
                    <div class="pre-heading f-18 f-md-24 w600 lh140 white-clr">
                        YouTube Has Been Dominated By Ai…
                    </div>
                </div>
				<div class="gx-md-0">
					<div class="col-12 f-md-42 f-28 w700 text-center white-clr lh140 mt-md40 mt20 ">
                    	World's First Google-Bard Powered AI App That <span class="red-brush"> *Legally* Hacks Into YouTube's 800 Million Videos… </span> 
                	</div>
				</div>
				<div class="col-12 mt-md20 mt20 text-center">
                    <div class="f-18 f-md-24 w600 lh140 white-clr ">
                        <i>Then Redirect Their Views To ANY Link or Offer of Our Choice &amp; Sent Us…</i>
                    </div>
                </div>
				<div class="col-12 mt-md20 mt20 text-center">
                    <div class="f-md-24 f-18 w500 yellow-clr lh140 post-heading">
                        92,346 Views In 4 Hours Or Less… Making Us $346.34 Daily
                    </div>
                </div>
				<div class="col-12 mt-md30 mt20 text-center">
					<div class="col-12 f-md-24 f-18 w400 white-clr lh140">
						PROVEN TO CONVERT || HIGH EPC'S || NEVER SEEN BEFORE
					</div>
            	</div>
				<div class="col-12 white-clr text-center">
					<div class="f-md-36 f-28 lh160 w700 mt30 white-clr">
						Download Your Free Gifts Below
					</div>
					<div class="mt10 f-20 f-md-32 lh160 ">
						<span class="tde">Enjoy Your Extra Bonus Below</span>
					</div>
				</div>
				<div class="col-12 mt20 mt-md50">
					<img src="assets/images/productbox.webp" class="img-fluid d-block mx-auto img-animation" alt="Proudly Presenting...">
				</div>
			</div>

			<!-- Bonus-1 -->
			<div class="row mt20 mt-md100 align-items-center">
               	<div class="col-md-6">
                  	<div class="f-22 f-md-28 lh140 w700 white-clr">
                     	<div class="option-design mr10 mr-md20">Bonus 1</div><br><br>
                     	Google Ads Mastery
                  	</div>
                  	<div class="f-20 f-md-22 w500 lh140 white-clr mt20 mt-md30">
					  	Google AdWords is one of the most powerful advertising tools ever created. It deals with millions of searches by internet users every day.
                  	</div>
                  	<div class="f-20 f-md-22 w500 lh140 white-clr mt20">
					  	It gives business owners a unique opportunity to convert many of these people into business leads and customers.
                  	</div>
                 	<div class="f-20 f-md-22 w500 lh140 white-clr mt20">
					 	This guide will show you EXACTLY how to set up your Google AdWords campaigns properly, so you get more clicks, cheaper clicks, slash your ad costs, and maximize your return on investment (ROI).
                  	</div>
					<div class="f-md-22 w700 lh160 d-btn mt20 mt-md30">
                       <a href="https://super-vip-bonuses.dotcompal.co/api/mydrive/docs/download_object?key=V2xoc1MyTkdjRVJUVkZwUFVrVXdNVlF3VWs1bFJYaEVVMjVrV2xkRk5UWmFSRWsxWlZad1JGTlVXa3BoVlc4MQ==" target="_blank">Download Now 🎁</a>
                    </div>
               	</div>
               	<div class="col-md-6 mt20 mt-md0">
                  	<img src="assets/images/bonus1.webp" alt="Bonus 1" class="mx-auto d-block img-fluid">
               	</div>
            </div>

			<!-- Bonus-2 -->
			<div class="row mt20 mt-md100 align-items-center">
               	<div class="col-md-6 order-md-2">
                  	<div class="f-22 f-md-28 lh140 w700 white-clr">
                     	<div class="option-design mr10 mr-md20">Bonus 2</div><br><br>
                     	Camping Niche Software
                  	</div>
                  	<div class="f-20 f-md-22 w500 lh140 white-clr mt20 mt-md30">
					  	Dominate Your Competition in Your Niche!
                  	</div>
                  	<div class="f-20 f-md-22 w500 lh140 white-clr mt20">
					  	If you are a niche marketer, doing keyword research is a must to learn more about your niche and at the same time, know the topics around the niche.
                  	</div>
                 	<div class="f-20 f-md-22 w500 lh140 white-clr mt20">
					 	But sometimes, it can be a pain if you manage several niche sites. That's why most successful niche marketes used softwares to help them automate the process.
                  	</div>
					<div class="f-md-22 w700 lh160 d-btn mt20 mt-md30">
                       <a href="https://super-vip-bonuses.dotcompal.co/api/mydrive/docs/download_object?key=V2xoc1MyTkdjRVJUVkZwUFVrVXdNVlF3VWs1bFJYaEVVMjVrV2xkRk5UWmFSRWsxWlZad1JGTlVXa3BoVlc4MQ==" target="_blank">Download Now 🎁</a>
                    </div>
               	</div>
               	<div class="col-md-6 mt20 mt-md0 order-md-1">
                  	<img src="assets/images/bonus2.webp" alt="Bonus 2" class="mx-auto d-block img-fluid">
               	</div>
            </div>

			<!-- Bonus-3 -->
			<div class="row mt20 mt-md100 align-items-center">
               	<div class="col-md-6">
                  	<div class="f-22 f-md-28 lh140 w700 white-clr">
                     	<div class="option-design mr10 mr-md20">Bonus 3</div><br><br>
                     	Keyword Tool
                  	</div>
                  	<div class="f-20 f-md-22 w500 lh140 white-clr mt20 mt-md30">
					  Get Ready to Give Your Business a Huge Upgrade, Because You're About to Discover the Time Saving, Profit Boosting Magic of...
                  	</div>
                  	<div class="f-20 f-md-22 w500 lh140 white-clr mt20">
					  The purpose of this is to generate multiple combinations of keywords which you can further filter and then create a final "Master" list. The list can either be saved or exported as a file. This is one program you definitely need to use in order to fully appreciate!
                  	</div>
					<div class="f-md-22 w700 lh160 d-btn mt20 mt-md30">
                       <a href="https://super-vip-bonuses.dotcompal.co/api/mydrive/docs/download_object?key=V2xoc1MyTkdjRVJUVkZwUFVrVXdNVlF3VWs1a01IaEVVMjVrV2xkRk5UWmFSRWsxWlZad1JGTlVXa3BoVlc4MQ==" target="_blank">Download Now 🎁</a>
                    </div>
               	</div>
               	<div class="col-md-6 mt20 mt-md0">
                  	<img src="assets/images/bonus3.webp" alt="Bonus 3" class="mx-auto d-block img-fluid">
               	</div>
            </div>

			<!-- Bonus-4 -->
			<div class="row mt20 mt-md100 align-items-center">
               	<div class="col-md-6 order-md-2">
                  	<div class="f-22 f-md-28 lh140 w700 white-clr">
                     	<div class="option-design mr10 mr-md20">Bonus 4</div><br><br>
                     	Auto Content Pro
                  	</div>
                  	<div class="f-20 f-md-22 w500 lh140 white-clr mt20 mt-md30">
					  	A Quick And Easy Way To Get Loads Of Search Engine Friendly, Automatically Updated Content On Your Websites - All Delivered On Autopilot!
                  	</div>
                  	<div class="f-20 f-md-22 w500 lh140 white-clr mt20">
					  	Get Free Automatically Updated Article Content - simply visit the appropriate article directory site, select a few options and then paste the supplied snippet of code into your web page. Instantly you get automatically updated content shown on your web page!
                  	</div>
					<div class="f-md-22 w700 lh160 d-btn mt20 mt-md30">
                       <a href="https://super-vip-bonuses.dotcompal.co/api/mydrive/docs/download_object?key=V2xoc1MyTkdjRVJUVkZwUFVrVXdNVlF3VWtwT1ZYaEVVMjVrV2xkRk5UWmFSRWsxWlZad1JGTlVXa3BoVlc4MQ==" target="_blank">Download Now 🎁</a>
                    </div>
               	</div>
               	<div class="col-md-6 mt20 mt-md0 order-md-1">
                  	<img src="assets/images/bonus4.webp" alt="Bonus 4" class="mx-auto d-block img-fluid">
               	</div>
            </div>

			<!-- Bonus-5 -->
			<div class="row mt20 mt-md100 align-items-center">
               	<div class="col-md-6">
                  	<div class="f-22 f-md-28 lh140 w700 white-clr">
                     	<div class="option-design mr10 mr-md20">Bonus 5</div><br><br>
                     	Secret 4 Figure Affiliate Promotion
                  	</div>
                  	<div class="f-20 f-md-22 w500 lh140 white-clr mt20 mt-md30">
					  	Learn the Secrets of the 4-Figure Affiliate Promotion!
                  	</div>
                  	<div class="f-20 f-md-22 w500 lh140 white-clr mt20">
					  	If you want to make money online, affiliate marketing could be one of the best way to do it. Well, if you have been doing it for awhile and you still don't have good results in your campaign, trying other marketing technique could lead you to the answer you keep asking.
                  	</div>
					<div class="f-20 f-md-22 w500 lh140 white-clr mt20">
						The good thing is that, the answer that you are looking for is inside this product which you are about to learn the secret strategies that successful affiliate marketers are doing on their promotion.
                  	</div>
					<div class="f-md-22 w700 lh160 d-btn mt20 mt-md30">
                       <a href="https://super-vip-bonuses.dotcompal.co/api/mydrive/docs/download_object?key=V2xoc1MyTkdjRVJUVkZwUFVrVXdNVlF3VWs1bFZYaEVVMjVrV2xkRk5UWmFSRWsxWlZad1JGTlVXa3BoVlc4MQ==" target="_blank">Download Now 🎁</a>
                    </div>
               	</div>
               	<div class="col-md-6 mt20 mt-md0">
                  	<img src="assets/images/bonus5.webp" alt="Bonus 5" class="mx-auto d-block img-fluid">
               	</div>
            </div>

			<!-- Bonus-6 -->
			<div class="row mt20 mt-md100 align-items-center">
               	<div class="col-md-6 order-md-2">
                  	<div class="f-22 f-md-28 lh140 w700 white-clr">
                     	<div class="option-design mr10 mr-md20">Bonus 6</div><br><br>
                     	Abstract Image Collection
                  	</div>
                  	<div class="f-20 f-md-22 w500 lh140 white-clr mt20 mt-md30">
					  	Ready to use abstract images for your needs!
                  	</div>
                  	<div class="f-20 f-md-22 w500 lh140 white-clr mt20">
					  	Images like photos and digital graphics are few of the media that is very useful in to many people. Image is also a good media to attract people to your business both online and offline.
                  	</div>
					<div class="f-20 f-md-22 w500 lh140 white-clr mt20">
					  	Inside this product is a bundle of abstract graphic images that you can use as website background or a quote background that you can post online.
                  	</div>
					<div class="f-md-22 w700 lh160 d-btn mt20 mt-md30">
                       <a href="https://super-vip-bonuses.dotcompal.co/api/mydrive/docs/download_object?key=V2xoc1MyTkdjRVJUVkZwUFVrVXdNVlF3VWs1TlZYaEVVMjVrV2xkRk5UWmFSRWsxWlZad1JGTlVXa3BoVlc4MQ==" target="_blank">Download Now 🎁</a>
                    </div>
               	</div>
               	<div class="col-md-6 mt20 mt-md0 order-md-1">
                  	<img src="assets/images/bonus6.webp" alt="Bonus 6" class="mx-auto d-block img-fluid">
               	</div>
            </div>

			<!-- Bonus-7 -->
			<div class="row mt20 mt-md100 align-items-center">
               	<div class="col-md-6">
                  	<div class="f-22 f-md-28 lh140 w700 white-clr">
                     	<div class="option-design mr10 mr-md20">Bonus 7</div><br><br>
                     	Easy Video Sales Pages
                  	</div>
                  	<div class="f-20 f-md-22 w500 lh140 white-clr mt20 mt-md30">
					  	Create video sales pages using a proven, winning formula! Simple sucessful formula for quick sales pages all the time!
                  	</div>
                  	<div class="f-20 f-md-22 w500 lh140 white-clr mt20">
					  	Sales page plays a very big role in converting your website visitors into buyers. If you create a sales page for granted, you will just waste your time, effort and money in selling your services or products you offer.
                  	</div>
					<div class="f-20 f-md-22 w500 lh140 white-clr mt20">
						In this amazing software, you are about to have those qualifications and expect a huge sale in your offers.
                  	</div>
					<div class="f-md-22 w700 lh160 d-btn mt20 mt-md30">
                       <a href="https://super-vip-bonuses.dotcompal.co/api/mydrive/docs/download_object?key=V2xoc1MyTkdjRVJUVkZwUFVrVXdNVlF3VWs1TmEzaEVVMjVrV2xkRk5UWmFSRWsxWlZad1JGTlVXa3BoVlc4MQ==" target="_blank">Download Now 🎁</a>
                    </div>
               	</div>
               	<div class="col-md-6 mt20 mt-md0">
                  	<img src="assets/images/bonus7.webp" alt="Bonus 7" class="mx-auto d-block img-fluid">
               	</div>
            </div>

			<!-- Bonus-8 -->
			<div class="row mt20 mt-md100 align-items-center">
               	<div class="col-md-6 order-md-2">
                  	<div class="f-22 f-md-28 lh140 w700 white-clr">
                     	<div class="option-design mr10 mr-md20">Bonus 8</div><br><br>
                     	Content Spin Bot 
                  	</div>
                  	<div class="f-20 f-md-22 w500 lh140 white-clr mt20 mt-md30">
					  	Revolutionary New Software Creates Unique Articles For Better Search Engine Rankings, More Vistors And Ultimately Make Money Online!
                  	</div>
                  	<div class="f-20 f-md-22 w500 lh140 white-clr mt20">
					  	We've been helping THOUSANDS Of Webmasters & Marketers Start & Grow their own Internet Business... Today Is YOUR Turn!
                  	</div>
					<div class="f-md-22 w700 lh160 d-btn mt20 mt-md30">
                       <a href="https://super-vip-bonuses.dotcompal.co/api/mydrive/docs/download_object?key=V2xoc1MyTkdjRVJUVkZwUFVrVXdNVlF3VWs1Tk1IaEVVMjVrV2xkRk5UWmFSRWsxWlZad1JGTlVXa3BoVlc4MQ==" target="_blank">Download Now 🎁</a>
                    </div>
               	</div>
               	<div class="col-md-6 mt20 mt-md0 order-md-1">
                  	<img src="assets/images/bonus8.webp" alt="Bonus 8" class="mx-auto d-block img-fluid">
               	</div>
            </div>

			<!-- Bonus-9 -->
			<div class="row mt20 mt-md100 align-items-center">
               	<div class="col-md-6">
                  	<div class="f-22 f-md-28 lh140 w700 white-clr">
                     	<div class="option-design mr10 mr-md20">Bonus 9</div><br><br>
                     	Sales Bot Generator
                  	</div>
                  	<div class="f-20 f-md-22 w500 lh140 white-clr mt20 mt-md30">
					  	Unleash These 24 Hour Online Sales Agents & Immediately Skyrocket Conversions, Sales & Profits!
                  	</div>
                  	<div class="f-20 f-md-22 w500 lh140 white-clr mt20">
					  	Sales Bot Generator is a simple little script you can use to set up your personal exit traffic salesmen. As soon as they get a sense someone is about to abandon your site they jump into action, giving your visitors a second chance to interact with your site and take action on your offers. You can generate unlimited such salesmen and put them on any webpage you want.
                  	</div>
					<div class="f-md-22 w700 lh160 d-btn mt20 mt-md30">
                       <a href="https://super-vip-bonuses.dotcompal.co/api/mydrive/docs/download_object?key=V2xoc1MyTkdjRVJUVkZwUFVrVXdNVlF3VWs1TlJYaEVVMjVrV2xkRk5UWmFSRWsxWlZad1JGTlVXa3BoVlc4MQ==" target="_blank">Download Now 🎁</a>
                    </div>
               	</div>
               	<div class="col-md-6 mt20 mt-md0">
                  	<img src="assets/images/bonus9.webp" alt="Bonus 9" class="mx-auto d-block img-fluid">
               	</div>
            </div>

			<!-- Bonus-10 -->
			<div class="row mt20 mt-md100 align-items-center">
               	<div class="col-md-6 order-md-2">
                  	<div class="f-22 f-md-28 lh140 w700 white-clr">
                     	<div class="option-design mr10 mr-md20">Bonus 10</div><br><br>
                     	Start Your Online Presence
                  	</div>
                  	<div class="f-20 f-md-22 w500 lh140 white-clr mt20 mt-md30">
					  	Get Your First Website Online Today!
                  	</div>
                  	<div class="f-20 f-md-22 w500 lh140 white-clr mt20">
					  	Finally the problem that so many people that are beginning on the internet have struggled with has been solved! Learn how you can easily get your first website online. This video series is targeted to the beginner. no prior knowledge is needed. get your first website online today!
                  	</div>
					<div class="f-20 f-md-22 w500 lh140 white-clr mt20">
					  	The information has been laid out for you in five easy to follow videos. Just follow the instructions in these videos and you will have your first site up within hours!
                  	</div>
					<div class="f-md-22 w700 lh160 d-btn mt20 mt-md30">
                       <a href="https://super-vip-bonuses.dotcompal.co/api/mydrive/docs/download_object?key=V2xoc1MyTkdjRVJUVkZwUFVrVXdNVlF3VWs1T1JYaEVVMjVrV2xkRk5UWmFSRWsxWlZad1JGTlVXa3BoVlc4MQ==" target="_blank">Download Now 🎁</a>
                    </div>
               	</div>
               	<div class="col-md-6 mt20 mt-md0 order-md-1">
                  	<img src="assets/images/bonus10.webp" alt="Bonus 10" class="mx-auto d-block img-fluid">
               	</div>
            </div>
		</div>
		<div class="dark-cta-sec">
			<div class="container">
				<div class="row">
				<div class="col-md-12 col-md-12 col-12 text-center mt20 mt-md20">
					<div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
					<div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
					<div class="f-18 f-md-24 lh150 w400 text-center mt20 white-clr">
					Use Special Coupon <span class="w600 yellow-clr">"Trafficadmin"</span> For <span class="w600 yellow-clr"> 32% Off</span> On Full Funnel
					</div>
				</div>
				<div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
					<a href="https://warriorplus.com/o2/a/djw1w2/0" class="text-center bonusbuy-btn px-md80">
						<span class="text-center">Grab TubeTrafficAi + 10 Exclusive Bonuses</span>
					</a>
				</div>
				</div>
			</div>
      	</div>
	</div>

	<!--Footer Section Start -->
	<div class="footer-section">
        <div class="container">
            <div class="row">
               	<div class="col-12 text-center">
				   <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 800 131.43" style="max-height:75px;">
                        <defs>
                            <style>
                            .cls-1 {
                                fill: #fff;
                            }
        
                            .cls-2 {
                                fill: #fd2f15;
                            }
        
                            .cls-3 {
                                fill: #fbbf33;
                            }
                            </style>
                        </defs>
                        <g id="Layer_1-2" data-name="Layer 1">
                            <g>
                            <g>
                                <path class="cls-3" d="m105.61,78.06l40.05,23.12c6.01,3.47,13.51-.87,13.51-7.8v-46.25c0-6.93-7.51-11.27-13.51-7.8l-40.05,23.12c-6.01,3.47-6.01,12.14,0,15.6Z"/>
                                <g>
                                <rect class="cls-3" x="26.92" y="58.86" width="16.03" height="46.81"/>
                                <path class="cls-2" d="m125.22,26.34h-7.16v46.06c0,32.6-26.43,59.03-59.03,59.03H0v-65.94C0,32.89,26.43,6.45,59.03,6.45h38.52l-12.38,19.88h-26.14c-21.59,0-39.16,17.57-39.16,39.15v46.07h39.16c21.59,0,39.16-17.57,39.16-39.16V26.34h-5.79L108.81,0l16.41,26.34Z"/>
                                <path class="cls-3" d="m67.04,46.21h-16.02v59.46h8.01c2.76,0,5.45-.35,8.01-.99v-58.47Z"/>
                                <path class="cls-3" d="m90.77,33.56h-16.03v68.14c7.57-4.07,13.39-10.99,16.03-19.31v-48.83Z"/>
                                </g>
                            </g>
                            <g>
                                <g>
                                <path class="cls-1" d="m195.69,38.68v-5.58h49.35v5.58h-21.57v62.48h-6.21v-62.48h-21.57Z"/>
                                <path class="cls-1" d="m283.29,81.65v-31.54h5.92v51.05h-5.92v-8.47h-.47c-1.2,2.61-3.09,4.79-5.68,6.53-2.59,1.74-5.76,2.61-9.5,2.61-3.26,0-6.15-.71-8.67-2.14-2.53-1.43-4.51-3.53-5.95-6.3-1.44-2.77-2.16-6.17-2.16-10.2v-33.07h5.92v32.7c0,3.99,1.17,7.2,3.52,9.62,2.35,2.43,5.36,3.64,9.04,3.64,2.3,0,4.52-.55,6.65-1.66,2.13-1.11,3.88-2.73,5.25-4.87,1.37-2.14,2.06-4.77,2.06-7.89Z"/>
                                <path class="cls-1" d="m306.05,101.16V33.09h5.95v26.75h.53c.66-1.53,1.62-3.1,2.87-4.72,1.25-1.62,2.9-2.97,4.95-4.07,2.05-1.1,4.64-1.65,7.76-1.65,4.19,0,7.86,1.1,11.03,3.31,3.17,2.21,5.64,5.28,7.41,9.24,1.77,3.95,2.66,8.56,2.66,13.81s-.88,9.89-2.64,13.86c-1.76,3.97-4.22,7.06-7.38,9.27-3.16,2.22-6.82,3.32-10.98,3.32-3.1,0-5.69-.55-7.76-1.66-2.07-1.11-3.74-2.48-5-4.1-1.26-1.63-2.24-3.22-2.92-4.77h-.73v9.47h-5.75Zm5.85-25.49c0,4.12.61,7.77,1.84,10.95,1.23,3.18,3,5.67,5.32,7.48,2.31,1.81,5.12,2.71,8.42,2.71s6.26-.94,8.61-2.81c2.35-1.87,4.13-4.4,5.35-7.59,1.22-3.19,1.83-6.77,1.83-10.73s-.6-7.46-1.81-10.6c-1.21-3.15-2.99-5.64-5.33-7.49-2.35-1.85-5.23-2.78-8.64-2.78s-6.14.89-8.46,2.68c-2.32,1.78-4.08,4.24-5.3,7.38-1.22,3.14-1.83,6.74-1.83,10.82Z"/>
                                <path class="cls-1" d="m382.86,102.22c-4.76,0-8.88-1.11-12.36-3.34-3.48-2.23-6.16-5.32-8.06-9.27s-2.84-8.51-2.84-13.68.95-9.71,2.84-13.71c1.89-4,4.52-7.13,7.88-9.41,3.36-2.27,7.24-3.41,11.65-3.41,2.77,0,5.44.5,8.01,1.51,2.57,1.01,4.88,2.55,6.93,4.62,2.05,2.07,3.67,4.68,4.87,7.81,1.2,3.14,1.79,6.84,1.79,11.12v2.92h-39.88v-5.22h33.83c0-3.28-.66-6.23-1.98-8.86-1.32-2.63-3.15-4.7-5.48-6.23-2.34-1.53-5.04-2.29-8.09-2.29-3.23,0-6.08.86-8.54,2.59-2.46,1.73-4.38,4.01-5.77,6.85-1.38,2.84-2.09,5.94-2.11,9.31v3.12c0,4.05.7,7.59,2.11,10.62,1.41,3.02,3.41,5.37,6,7.03,2.59,1.66,5.66,2.49,9.21,2.49,2.41,0,4.54-.38,6.36-1.13,1.83-.75,3.37-1.77,4.62-3.04,1.25-1.27,2.2-2.68,2.84-4.2l5.62,1.83c-.78,2.15-2.04,4.13-3.81,5.95-1.76,1.82-3.95,3.27-6.58,4.37-2.63,1.1-5.65,1.65-9.06,1.65Z"/>
                                <path class="cls-1" d="m412.07,44.96v-11.86h55.9v11.86h-20.84v56.2h-14.22v-56.2h-20.84Z"/>
                                <path class="cls-1" d="m471.62,101.16v-51.05h13.73v8.91h.53c.93-3.17,2.49-5.57,4.69-7.2,2.19-1.63,4.72-2.44,7.58-2.44.71,0,1.47.04,2.29.13.82.09,1.54.21,2.16.37v12.56c-.66-.2-1.58-.38-2.76-.53-1.17-.15-2.25-.23-3.22-.23-2.08,0-3.94.45-5.57,1.35-1.63.9-2.91,2.14-3.86,3.74-.94,1.6-1.41,3.43-1.41,5.52v28.88h-14.16Z"/>
                                <path class="cls-1" d="m523,102.12c-3.26,0-6.16-.57-8.71-1.71-2.55-1.14-4.56-2.84-6.03-5.08-1.47-2.25-2.21-5.06-2.21-8.42,0-2.84.52-5.22,1.56-7.15,1.04-1.93,2.46-3.48,4.25-4.65,1.79-1.17,3.84-2.06,6.13-2.66,2.29-.6,4.7-1.02,7.23-1.26,2.97-.31,5.36-.6,7.18-.88,1.82-.28,3.13-.69,3.95-1.25.82-.55,1.23-1.37,1.23-2.46v-.2c0-2.1-.66-3.73-1.98-4.89-1.32-1.15-3.19-1.73-5.6-1.73-2.55,0-4.58.56-6.08,1.68-1.51,1.12-2.5,2.52-2.99,4.2l-13.09-1.06c.66-3.1,1.97-5.79,3.92-8.06,1.95-2.27,4.47-4.02,7.56-5.25,3.09-1.23,6.67-1.84,10.75-1.84,2.84,0,5.55.33,8.16,1,2.6.66,4.92,1.7,6.95,3.09,2.03,1.4,3.63,3.19,4.8,5.37,1.17,2.18,1.76,4.79,1.76,7.83v34.43h-13.43v-7.08h-.4c-.82,1.6-1.92,3-3.29,4.2-1.37,1.21-3.02,2.15-4.95,2.82-1.93.68-4.15,1.01-6.68,1.01Zm4.05-9.77c2.08,0,3.92-.42,5.52-1.25s2.85-1.96,3.76-3.37c.91-1.42,1.36-3.02,1.36-4.82v-5.42c-.44.29-1.05.55-1.81.78s-1.62.44-2.58.63c-.95.19-1.91.35-2.86.5-.95.14-1.82.27-2.59.38-1.66.24-3.11.63-4.35,1.16-1.24.53-2.21,1.25-2.89,2.14-.69.9-1.03,2.01-1.03,3.34,0,1.93.7,3.4,2.11,4.4,1.41,1.01,3.2,1.51,5.37,1.51Z"/>
                                <path class="cls-1" d="m580.03,50.11h10.17v10.63h-10.17v40.41h-14.12v-40.41h-7.21v-10.63h7.21v-3.69c0-3.7.72-6.77,2.18-9.21,1.45-2.44,3.44-4.26,5.97-5.48,2.53-1.22,5.4-1.83,8.61-1.83,2.17,0,4.15.17,5.97.5,1.8.33,3.15.63,4.04.9l-2.53,10.63c-.55-.18-1.23-.34-2.04-.5s-1.64-.23-2.48-.23c-2.08,0-3.53.48-4.35,1.45-.83.96-1.23,2.31-1.23,4.04v3.42Z"/>
                                <path class="cls-1" d="m616.12,50.11h10.17v10.63h-10.17v40.41h-14.12v-40.41h-7.21v-10.63h7.21v-3.69c0-3.7.72-6.77,2.18-9.21,1.45-2.44,3.44-4.26,5.97-5.48,2.53-1.22,5.4-1.83,8.61-1.83,2.17,0,4.15.17,5.97.5,1.8.33,3.15.63,4.04.9l-2.53,10.63c-.55-.18-1.23-.34-2.04-.5-.81-.15-1.64-.23-2.48-.23-2.08,0-3.53.48-4.35,1.45-.83.96-1.23,2.31-1.23,4.04v3.42Z"/>
                                <path class="cls-1" d="m642.05,43.53c-2.11,0-3.9-.7-5.4-2.09-1.5-1.4-2.24-3.08-2.24-5.05s.75-3.66,2.26-5.07c1.51-1.41,3.3-2.11,5.38-2.11s3.91.7,5.4,2.09c1.5,1.4,2.24,3.08,2.24,5.05s-.75,3.66-2.24,5.07c-1.5,1.41-3.3,2.11-5.4,2.11Zm-7.11,57.63v-51.05h14.16v51.05h-14.16Z"/>
                                <path class="cls-1" d="m683.46,102.15c-5.23,0-9.72-1.11-13.48-3.34-3.76-2.23-6.64-5.32-8.64-9.29-2.01-3.97-3.01-8.53-3.01-13.69s1.01-9.82,3.04-13.78c2.03-3.95,4.91-7.05,8.66-9.27,3.74-2.23,8.2-3.34,13.36-3.34,4.45,0,8.35.81,11.7,2.43,3.35,1.62,5.99,3.89,7.94,6.81,1.95,2.92,3.02,6.36,3.22,10.3h-13.36c-.38-2.55-1.37-4.6-2.97-6.16-1.61-1.56-3.71-2.34-6.3-2.34-2.19,0-4.1.59-5.73,1.78-1.63,1.19-2.9,2.91-3.81,5.17s-1.36,5-1.36,8.21.45,6.03,1.35,8.31c.9,2.28,2.17,4.02,3.81,5.22,1.64,1.2,3.56,1.79,5.75,1.79,1.62,0,3.07-.33,4.37-1,1.3-.66,2.37-1.63,3.22-2.91.85-1.27,1.41-2.81,1.68-4.6h13.36c-.22,3.9-1.28,7.33-3.17,10.29-1.89,2.96-4.5,5.27-7.83,6.93-3.32,1.66-7.26,2.49-11.8,2.49Z"/>
                                </g>
                                <g>
                                <path class="cls-3" d="m800,37.72c0,4.31-3.5,7.81-7.82,7.81s-7.81-3.5-7.81-7.81,3.5-7.82,7.81-7.82,7.82,3.5,7.82,7.82Z"/>
                                <path class="cls-2" d="m736.11,85.52c-.09,0-.18,0-.27-.01-.09,0-.18-.02-.26-.03.18.01.35.05.53.05Z"/>
                                <path class="cls-2" d="m784.37,50.18v32.89c0,1.31-1.05,2.37-2.35,2.43-.03,0-.07.02-.11.02h-.37l-18.68-41.36-3.1-6.86c-2.03-4.5-6.52-7.4-11.47-7.4s-9.42,2.9-11.46,7.4l-11.96,26.47-10.4,23.03c-3.04,6.73,1.88,14.36,9.28,14.36h66.01c5.65,0,10.25-4.58,10.25-10.24v-40.74h-15.63Zm-48.25,35.34c-.09,0-.18,0-.27-.01-.09,0-.18-.02-.26-.03-2.25-.33-3.65-2.75-2.67-4.91l12.93-28.64c.95-2.1,3.93-2.1,4.89,0l15.17,33.59h-29.79Z"/>
                                </g>
                            </g>
                            </g>
                        </g>
                </svg>
               	</div>
				<div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                    <div class="f-14 f-md-16 w400 lh160 white-clr text-xs-center">Copyright © TubeTrafficAi 2023</div>
                    <ul class="footer-ul w400 f-14 f-md-16 white-clr text-center text-md-right">
                        <li><a href="https://support.bizomart.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                        <li><a href="http://tubetrafficai.com/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                        <li><a href="http://tubetrafficai.com/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                        <li><a href="http://tubetrafficai.com/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                        <li><a href="http://tubetrafficai.com/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                        <li><a href="http://tubetrafficai.com/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                        <li><a href="http://tubetrafficai.com/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!--Footer Section End -->


</body>
</html>

<!Doctype html>
<html>
   <head>
      <title>LearnX Special</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=9">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
      <meta name="title" content="LearnX Special">
      <meta name="description" content="Next-Gen AI App: Creates Hundreds of Impressive Courses on Any Topic and Sells Them on Your Own
Set-and-Forget Udemy-Like E-Learning Site.">
      <meta name="keywords" content="LearnX">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta property="og:image" content="https://getlearnx.com/special/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Ayush Jain">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="LearnX Special">
      <meta property="og:description" content="Next-Gen AI App: Creates Hundreds of Impressive Courses on Any Topic and Sells Them on Your Own
Set-and-Forget Udemy-Like E-Learning Site.">
      <meta property="og:image" content="https://getlearnx.com/special/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="LearnX Special">
      <meta property="twitter:description" content="Next-Gen AI App: Creates Hundreds of Impressive Courses on Any Topic and Sells Them on Your Own
Set-and-Forget Udemy-Like E-Learning Site.">
      <meta property="twitter:image" content="https://getlearnx.com/special/thumbnail.png">
      <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&family=Poppins:wght@400;500;600;700;800;900&family=Red+Hat+Display:wght@300;400;500;600;700;800;900&display=swap" rel="stylesheet">
      <!-- Start Editor required -->
      <link rel="stylesheet" href="https://cdn.oppyotest.com/launches/sellero/common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style-feature.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style-pawan.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style-bottom.css" type="text/css">
      <link rel="stylesheet" href="assets/css/timer.css" type="text/css">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <script src="https://cdn.oppyotest.com/launches/sellero/common_assets/js/bootstrap.min.js"></script>
      <script src="https://cdn.oppyotest.com/launches/sellero/common_assets/js/jquery.min.js"></script>
      <script src="assets/js/ouibounce.min.js"></script>
	  
	  <script>
(function(w, i, d, g, e, t, s) {
if(window.businessDomain != undefined){
console.log("Your page have duplicate embed code. Please check it.");
return false;
}
businessDomain = 'aicademy';
allowedDomain = 'getlearnx.com';
if(!window.location.hostname.includes(allowedDomain)){
console.log("Your page have not authorized. Please check it.");
return false;
}
console.log("Your script is ready...");
w[d] = w[d] || [];
t = i.createElement(g);
t.async = 1;
t.src = e;
s = i.getElementsByTagName(g)[0];
s.parentNode.insertBefore(t, s);
})(window, document, '_gscq', 'script', 'https://cdn.staticdcp.com/apps/engage/smart_engage/js/config-loader.js');
</script>

   </head>
   <body>

      <div class="timer-header-top fixed-top">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-12 col-md-4">
                  <div class="tht-left f-14 f-md-16 text-md-start text-center white-clr">
                     Use Coupon <span class="yellow-clr">"LEARNX"</span>  for <br class="d-block d-md-none">Extra <span class="yellow-clr"> $3 Discount </span>
                  </div>
               </div>
               <div class="col-8 col-md-5">
               <div class="countdown counter-black">
						<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Days</span> </div>
						<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
						<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
						<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
					</div>
               </div>
               <div class="col-4 col-md-3 text-center text-md-right">
                  <div class="affiliate-link-btn"><a href="#buynow" class="t-decoration-none">Buy Now</a></div>
               </div>
            </div>
         </div>
      </div> 
      <!--1. Header Section Start--->
      <div class="header-section mt30 mt-md100">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
               <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 484.58 200.84" style="max-height:75px;">
                        <defs>
                            <style>
                                .cls-1,.cls-4{fill:#fff;}
                                .cls-2,.cls-3,.cls-4{fill-rule:evenodd;}
                                .cls-2{fill:url(#linear-gradient);}
                                .cls-3{fill:url(#linear-gradient-2);}
                                .cls-5{fill:url(#linear-gradient-3);}
                            </style>
                            <linearGradient id="linear-gradient" x1="-154.67" y1="191.86" x2="249.18" y2="191.86" gradientTransform="matrix(1.19, -0.01, 0.05, 1, 178.46, -4.31)" gradientUnits="userSpaceOnUse">
                                <stop offset="0" stop-color="#ead40c"/>
                                <stop offset="1" stop-color="#ff00e9"/>
                            </linearGradient>
                            <linearGradient id="linear-gradient-2" x1="23.11" y1="160.56" x2="57.61" y2="160.56" gradientTransform="matrix(1, 0, 0, 1, 0, 0)" xlink:href="#linear-gradient"/>
                            <linearGradient id="linear-gradient-3" x1="384.49" y1="98.33" x2="483.38" y2="98.33" gradientTransform="matrix(1, 0, 0, 1, 0, 0)" xlink:href="#linear-gradient"/>
                        </defs>
                        <g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1">
                            <path class="cls-1" d="M112.31,154.3q-11.65,0-20.1-4.87a32.56,32.56,0,0,1-13-13.82q-4.53-9-4.54-21.11a46.06,46.06,0,0,1,4.57-21A34.29,34.29,0,0,1,92,79.37a36.11,36.11,0,0,1,19.31-5.06,39.65,39.65,0,0,1,13.54,2.29,31,31,0,0,1,11.3,7.09,33.32,33.32,0,0,1,7.75,12.18,49.23,49.23,0,0,1,2.82,17.57V119H83.26v-12.3h46A19.87,19.87,0,0,0,127,97.38a16.6,16.6,0,0,0-6.18-6.48,17.59,17.59,0,0,0-9.21-2.37,17.88,17.88,0,0,0-9.83,2.7,18.81,18.81,0,0,0-6.58,7.06,20.17,20.17,0,0,0-2.4,9.56v10.74a25.15,25.15,0,0,0,2.47,11.57,17.42,17.42,0,0,0,6.91,7.37,20.52,20.52,0,0,0,10.39,2.55,21.62,21.62,0,0,0,7.22-1.14,15.71,15.71,0,0,0,5.59-3.35,14.11,14.11,0,0,0,3.59-5.5L146,132a26.44,26.44,0,0,1-6.13,11.77,29.72,29.72,0,0,1-11.52,7.77A43.74,43.74,0,0,1,112.31,154.3Z"/>
                            <path class="cls-1" d="M184.49,154.35a31.78,31.78,0,0,1-13.24-2.65,21.26,21.26,0,0,1-9.28-7.84,22.86,22.86,0,0,1-3.41-12.81A21.92,21.92,0,0,1,161,120.2a18.9,18.9,0,0,1,6.61-6.86,33.85,33.85,0,0,1,9.46-3.9,77.76,77.76,0,0,1,10.92-2q6.81-.7,11-1.28a15.91,15.91,0,0,0,6.18-1.82,4.25,4.25,0,0,0,1.94-3.86v-.3q0-5.7-3.38-8.83T194,88.28q-6.71,0-10.62,2.92a14.55,14.55,0,0,0-5.27,6.91l-17-2.42a27.51,27.51,0,0,1,6.66-11.83,29.46,29.46,0,0,1,11.35-7.16,43.73,43.73,0,0,1,14.83-2.39,47.89,47.89,0,0,1,11.14,1.31,31.6,31.6,0,0,1,10.14,4.31,22.12,22.12,0,0,1,7.39,8.14q2.81,5.15,2.8,12.87v51.85H207.84V142.14h-.61a22.1,22.1,0,0,1-4.66,6,22.35,22.35,0,0,1-7.52,4.49A30,30,0,0,1,184.49,154.35Zm4.74-13.42a19.7,19.7,0,0,0,9.53-2.19,16,16,0,0,0,6.23-5.83,15,15,0,0,0,2.19-7.91v-9.13a8.72,8.72,0,0,1-2.9,1.31,45.56,45.56,0,0,1-4.56,1.06c-1.68.3-3.35.57-5,.8l-4.29.61a32,32,0,0,0-7.32,1.81A12.29,12.29,0,0,0,178,125a9.76,9.76,0,0,0,1.82,13.39A16,16,0,0,0,189.23,140.93Z"/>
                            <path class="cls-1" d="M243.8,152.79V75.31h17.7V88.23h.81a19.36,19.36,0,0,1,7.29-10.37,20,20,0,0,1,11.83-3.66c1,0,2.14,0,3.4.13a23.83,23.83,0,0,1,3.15.38V91.5a21,21,0,0,0-3.65-.73,38.61,38.61,0,0,0-4.82-.32,18.47,18.47,0,0,0-8.95,2.14,15.94,15.94,0,0,0-6.23,5.93,16.55,16.55,0,0,0-2.27,8.72v45.55Z"/>
                            <path class="cls-1" d="M318.4,107.39v45.4H300.14V75.31h17.45V88.48h.91a22,22,0,0,1,8.55-10.34q5.86-3.84,14.55-3.83a27.58,27.58,0,0,1,14,3.43,23.19,23.19,0,0,1,9.28,9.93,34.22,34.22,0,0,1,3.26,15.79v49.33H349.87V106.28c0-5.17-1.34-9.23-4-12.15s-6.37-4.39-11.07-4.39a17,17,0,0,0-8.5,2.09,14.67,14.67,0,0,0-5.8,6A20,20,0,0,0,318.4,107.39Z"/>
                            <path class="cls-2" d="M314.05,172.88c12.69-.14,27.32,2.53,40.82,2.61l-.09,1c1.07.15,1-.88,2-.78,1.42-.18,0,1.57,1.82,1.14.54-.06.08-.56-.32-.68.77,0,2.31.17,3,1.26,1.3,0-.9-1.34.85-.89,6,1.63,13.39,0,20,3.2,11.64.72,24.79,2.38,38,4.27,2.85.41,5.37,1.11,7.57,1.05,2-.05,5.68.78,8,1.08,2.53.34,5.16,1.56,7.13,1.65-1.18-.05,5.49-.78,4.54.76,5-.72,10.9,1.67,15.94,1.84.83-.07.69.53.67,1,1.23-1.17,3.32.56,4.28-.56.25,2,2.63-.48,3.3,1.61,2.23-1.39,4.83.74,7.55,1.37,1.91.44,5.29-.21,5.55,2.14-2.1-.13-5.12.81-11.41-1.09,1,.87-3.35.74-3.39-.64-1.81,1.94-6.28-1-7.79,1.19-1.26-.41,0-.72-.64-1.35-5,.55-8.09-1.33-12.91-1.56,2.57,2.44,6.08,3.16,10.06,3.22,1.28.15,0,.74,1,1.39,1.37-.19.75-.48,1.26-1.17,5.37,2.28,15.36,2.35,21,4.91-4.69-.63-11.38,0-15.15-2.09A148,148,0,0,1,441.58,196c-.53-.1-1.81.12-.7-.71-1.65.73-3.41.1-5.74-.22-37.15-5.15-80.47-7.78-115.75-9.76-39.54-2.22-76.79-3.22-116.44-2.72-61.62.78-119.59,7.93-175.21,13.95-5,.55-10,1.49-15.11,1.47-1.33-.32-2.46-1.91-1.25-3-2-.38-2.93.29-5-.15a9.83,9.83,0,0,1-1.25-3,13.73,13.73,0,0,1,6.42-2.94c.77-.54.12-.8.15-1.6a171,171,0,0,1,45.35-8.58c26.43-1.1,53.68-4.73,79.64-6,6.25-.3,11.72.06,18.41.14,8.31.1,19.09-1,29.19-.12,6.25.54,13.67-.55,21.16-.56,39.35-.09,71.16-.23,112.28,2C317.24,171.93,315.11,173.81,314.05,172.88Zm64.22,16.05-1.6-.2C376.2,190,378.42,190.26,378.27,188.93Z"/>
                            <polygon class="cls-3" points="52.57 166.06 57.61 151.99 23.11 157.23 32.36 169.12 52.57 166.06"/>
                            <polygon class="cls-4" points="44.69 183.24 51.75 168.35 33.86 171.06 44.69 183.24"/>
                            <path class="cls-4" d="M.12,10.15l22,145.06,35.47-5.39-22-145a2.6,2.6,0,0,0,0-.4C35,.76,26.62-.95,16.81.54S-.52,6.16,0,9.77A2.62,2.62,0,0,0,.12,10.15Z"/>
                            <path class="cls-5" d="M433.68,79.45l21-36.33h27.56L448.48,97.51l34.9,56H456l-22-37.76-21.94,37.76H384.49l34.9-56L385.72,43.12h27.35Z"/>
                        </g>
                            </g>
                    </svg>
               </div>
               <!-- <div class="col-md-9  mt-md0 mt15">
                  <ul class="leader-ul f-20 w400 white-clr text-md-end text-center">
                     <li>
                        <a href="#features" class="white-clr t-decoration-none">Features</a><span class="pl15">|</span>
                     </li>
                     <li>
                        <a href="#productdemo" class="white-clr t-decoration-none">Demo</a>
                     </li>
                     <li class="affiliate-link-btn">
                        <a href="#buynow">Buy Now</a>
                     </li>
                  </ul>
               </div> -->
               <div class="col-12 text-center lh150 mt20 mt-md70">
                  <div class="pre-heading f-18 f-md-22 w600 lh140 white-clr">
                  Seize the Opportunity to <u>Transform Learning into Earnings</u> Today!
                  </div>
               </div>
               <div class="col-12 col-md-12 px-md-0">
                    
                    <div class="main-heading mt-md50 mt20 f-md-40 f-25 w900 text-center black2-clr lh150 red-hat-font">
                        <span class="main-heading-inner w700 f-md-40 white-clr">Next-Gen AI App:</span> Creates Hundreds of Impressive Courses on Any Topic and Sells Them <span class="w400">on Your Own</span> <br class="d-none d-md-block"> <span class="w400"><u>Set-and-Forget</u> Udemy-Like E-Learning Site.</span>
                        <div class="f-md-26 f-22 w700 text-center red-hat-font white-clr main-heading-bottom mt20">
                        All Without Writing Anything, Being on Camera, or Hiring Anyone.

                        </div>
                    </div>
                    
                </div>
              
               <div class="col-12 col-md-12 mt20 text-center mt-md40">
                  <div class="f-22 f-md-24 w700 lh140 yellow-clr red-hat-font">
                  Launch Today, Get Access to AI Course Creator, 500+ Ready-Made Courses, Udemy-Style Website, Auto Chatbots, Built-In Traffic, and Certificates Creator – All Within a User-Friendly App.
                  </div>
               </div>
               <!-- <div class="col-12 mt20 text-center">
                  <div class="f-20 f-md-22 w500 lh140 white-clr">
                  Without Us Creating Anything…. Even A 100% Beginner Or Camera SHY Can Do It.
                  </div>
               </div> -->
               <!-- <div class="col-12 mt15 text-center">
                  <div class="f-20 f-md-26 w600 lh140 yellow-clr red-hat-font">
                  Huge Opportunity to Transform Learnings into Earnings Today!
                  </div>
               </div> -->
            </div>

            <div class="row mt20 mt-md40 gx-md-5 align-items-center">
               <div class="col-md-7 col-12">
                  <div class="col-12">
                     <div class="f-18 f-md-19 w600 lh140 white-clr shape-head">
                     Watch As We Build Udemy-like Website, And Sell Hundreds of Dollar In Courses In Just Few Days…
                     </div>
                     <!-- <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/video-thumb.webp" alt="Video" class="mx-auto d-block img-fluid mt20 mt-md30"> -->
                     <div class="video-box mt20 mt-md30">
                        <div style="padding-bottom: 56.25%;position: relative;">
                        <iframe src="https://learnx.dotcompal.com/video/embed/l2h5hvlipk" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                        </div>
                     </div>
                  </div>
                  <div class="col-12 mt20 mt-md50">
                     <div class="border-dashed">
                        <div class="f-18 f-md-20 w500 lh140 text-center grey-clr">
                           Pick Your Copy Now And Secure Your Copy Of LearnX DFY For Free.
                        </div>
                        <div class="f-18 f-md-20 w500 lh140 text-center yellow-clr">
                           On Average, Our Users Made $463.21 More With This Free Bonus (Limited To First 99 Only)
                        </div>
                     </div>
                     <div class="f-22 f-md-28 w500 lh150 text-center mt20 mt-md30 white-clr">
                        Get LearnX And Save $293 Now <br class="d-none d-md-block"> Just Pay Once Instead Of a <del> Monthly Fee</del>
                     </div>
                     <div class="row">
                     <div class="cta-link-btn mt20">
                        <a href="#buynow" >&gt;&gt;&gt; Get Instant Access To LearnX &lt;&lt;&lt;</a>
                     </div>
                     </div>
                     <div class="col-12  mt20 mt-md30">
                        <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/compaitable-gurantee1.webp" alt="Compaitable Gurantee" class="mx-auto d-block img-fluid">
                     </div>

                     <div class="mt20 mt-md30">
                        <div class="countdown counter-white text-center">
                           <div class="timer-label text-center"><span class="f-24 f-md-42 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
                           <div class="timer-label text-center"><span class="f-24 f-md-42 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                           <div class="timer-label text-center timer-mrgn"><span class="f-24 f-md-42 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                           <div class="timer-label text-center "><span class="f-24 f-md-42 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-5 col-12 mt20 mt-md0">
                  <div class="key-features-bg">
                     <ul class="list-head pl0 m0 f-16 f-md-18 lh160 w400 white-clr">
                    <li> Tap Into A $300 Billion Industry with Just 3 Steps.</li>
                     <li>Let AI Create Hundreds Of Courses For You.</li>
                    <li> AI Finds The Best Profitable Niche Ideas For You.</li>
                    <li> AI Creates Your Website Too, No Tech Setup Whatsoever.</li>
                    <li> Access 500+ Done-For-You Smoking Hot Courses.</li>
                    <li> Even Traffic Is Done For You, Without Spending A Dime.</li>
                   <li>  Accept Payment In Your Favorite Payment Processor.</li>
                    <li> No Complicated Setup—Get Up And Running In 2 Minutes.</li>
                    <li> Build Your List And Earn Affiliate Commissions.</li>
                    <li> AI Chatbots That Will Handle All Customer Support.</li>
                    <li> Impress Your Students With Certificates Upon Course Completion.</li>
                   <li>  So Easy, Anyone Can Do It—ZERO Upfront Cost.</li>
                     <li>Cancel All Your Costly Subscriptions.</li>
                    <li> Benefit From A 30-Day Money-Back Guarantee.</li>
                    <li> Free Commercial License Included—That's Priceless.</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--1. Header Section End -->

   <!-- Limited Section Start    -->
   <div class="limited-time-sec">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-20 f-md-24 w700 text-center white-clr align-items-center flex-column flex-md-row justify-content-center">
                  <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/caution-icon.webp" class="img-fluid mr-md5 mb5 mb-md0" alt="Caution Icon">
                  Your $2,475.34 Bonuses Package Expires When Timer Hits ZERO 
                  <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/caution-icon.webp" class="img-fluid ml-md5 mb5 mb-md0" alt="Caution Icon">
               </div>
            </div>
         </div>
      </div>
   </div> 
   <!-- Limited Section End -->

   <div class="step-section">
      <div class="container ">
         <div class="row ">
            <div class="col-12 f-28 f-md-50 w700 lh140 black-clr text-center">
               All It Takes Is 3 Fail-Proof Steps<br class="d-none d-md-block">
            </div>
            <div class="col-12 f-24 f-md-28 w600 lh140 white-clr text-center mt20">
               <div class="niche-design">
                  Start Your E-Learning Empire Today.
               </div>
            </div>
            <div class="col-12 mt30 mt-md90">
               <div class="row align-items-center ">
                  <div class="col-12 col-md-6">
                     <div class="step-shapes f-20 f-md-22 w600 white-clr">
                           Step 1
                        </div>
                        <div class="f-34 f-md-65 w700 lh140 mt15 caveat">
                           Login
                        </div>
                        <div class="f-20 f-md-24 w400 lh140 mt5 mt-md10">
                           Click On Any Of The Button Below To Get Instant Access To LearnX
                        </div>
                     </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 ">
                     <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 2px solid #F27D66;">
                        <source src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/step1.mp4" type="video/mp4">
                     </video>
                  </div>
               </div>
            </div>
            <div class="col-12">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/left-arrow.webp" class="img-fluid d-none d-md-block mx-auto " alt="Left Arrow">
            </div>
            <div class="col-12 mt30 mt-md30">
               <div class="row align-items-center ">
                  <div class="col-12 col-md-6 order-md-2">
                     <div class="step-shapes f-20 f-md-22 w600 white-clr">
                        Step 2
                     </div>
                     <div class="f-34 f-md-65 w700 lh140 mt15 caveat">
                        Deploy
                     </div>
                     <div class="f-20 f-md-24 w400 lh140 mt5 mt-md10">
                        Just select any niche you want, and let AI create your Udemy-Like website in 30 seconds or less (prefilled With AI Courses, And With DFY Traffic)
                     </div>
                  </div>
                  <div class="col-md-6 col-md-pull-6 mt20 mt-md0 order-md-1 ">
                     <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 2px solid #F27D66;">
                        <source src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/step2.mp4" type="video/mp4">
                     </video>
                  </div>
               </div>
            </div>
            <div class="col-12">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/right-arrow.webp" class="img-fluid d-none d-md-block mx-auto" alt="Right Arrow">
            </div>
            <div class="col-12 mt30 mt-md30">
               <div class="row align-items-center ">
                  <div class="col-12 col-md-6">
                     <div class="step-shapes f-20 f-md-22 w600 white-clr">
                        Step 3
                     </div>
                     <div class="f-34 f-md-65 w700 lh140 mt15 caveat">
                        Profit
                     </div>
                     <div class="f-20 f-md-24 w400 lh140 mt5 mt-md10">
                        Sit back and relax. And let AI do the rest of the work. While you enjoy the results
                     </div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 ">
                     <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 2px solid #F27D66;">
                        <source src="https://cdn.oppyotest.com/launches/webgenie/preview/images/step3.mp4" type="video/mp4">
                     </video>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>

   <!-- CTA Section Start -->
   <div class="cta-section-white">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-38 w700 lh140 text-center white-clr">
                  Get LearnX And Save $293 Now <br class="d-none d-md-block"> 
               </div>
               <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                  <span class="w700">Just Pay Once</span> Instead Of a <s >Monthly Fee</s>
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md50">
            <div class="col-md-7">
            <div class="cta-link-btn mt20">
                        <a href="#buynow" >&gt;&gt;&gt; Get Instant Access To LearnX &lt;&lt;&lt;</a>
                     </div>
               <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/compaitable-gurantee1.webp" alt="Compaitable Gurantee" class="mx-auto d-block img-fluid mt20 mt-md30">
            </div>
            <div class="col-md-5 mt20 mt-md0">
               <div class="countdown-container">
                  <div class="f-20 f-md-24 w400 lh140 text-center white-clr mb10">
                     <span class="w700 red-clr">Hurry up!</span>  Price Increases In
                  </div>
                  <div class="countdown counter-white text-center">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 smmltd">Days</span> 
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-45 timerbg oswald">46&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-45 timerbg oswald">28</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> 
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- CTA Section End -->

   <!--Limited Section Start -->
   <div class="limited-time-sec">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-20 f-md-24 w700 text-center white-clr align-items-center flex-column flex-md-row justify-content-center">
                  <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/caution-icon.webp" class="img-fluid mr-md5 mb5 mb-md0" alt="Caution Icon">
                  Your $2,475.34 Bonuses Package Expires When Timer Hits ZERO 
                  <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/caution-icon.webp" class="img-fluid ml-md5 mb5 mb-md0" alt="Caution Icon">
               </div>
            </div>
         </div>
      </div>
   </div>
   <!--Limited Section End -->

   <div class="profitwall-sec">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-28 f-md-45 w700 lh140 black-clr text-center dollars-head">
               LearnX Made It <u>Fail-Proof</u>  To Create A <br class="d-none d-md-block"> Profitable E-Learning Business Today. 
               </div>
               <div class="f-26 f-md-32 lh140 w600 text-center white-clr mt20">
               (Let AI Do All The Work For You)
               </div>
            </div>
         </div>
         <div class="row row-cols-1 row-cols-sm-2 row-cols-md-2 gap30 mt20 mt-md10">
            <div class="col">
               <div class="text-center">
                  <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/proof1.webp" alt="No Coding" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-26 f-md-32 lh140 w600 text-center white-clr mt20 yellow-head">
                     Ai Creates Website For Us
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/proof2.webp" alt="No Coding" class="img-fluid mr-md5 mb5 mb-md0 ">
               <div class="f-26 f-md-32 lh140 w600 text-center white-clr mt20 yellow-head">
                  Ai Gives Us Best Niches
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/proof3.webp" alt="No Coding" class="img-fluid mr-md5 mb5 mb-md0 ">
               <div class="f-26 f-md-32 lh140 w600 text-center white-clr mt20 yellow-head">
                  Ai Create Courses For Us
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/proof4.webp" alt="No Coding" class="img-fluid mr-md5 mb5 mb-md0 ">
               <div class="f-26 f-md-32 lh140 w600 text-center white-clr mt20 yellow-head">
                  Ai Generate Traffic For Us
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/proof5.webp" alt="No Coding" class="img-fluid mr-md5 mb5 mb-md0 ">
               <div class="f-26 f-md-32 lh140 w600 text-center white-clr mt20 yellow-head">
                  Ai Generate Sales Pages For Us
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/proof6.webp" alt="No Coding" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-26 f-md-32 lh140 w600 text-center white-clr mt20 yellow-head">
                  Ai Generate Chat Bots For Customer Support For Us
                  </div>
               </div>
            </div>

            <div class="col">
               <div class="text-center">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/proof7.webp" alt="No Coding" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-26 f-md-32 lh140 w600 text-center white-clr mt20 red-head">
                  No Coding
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/proof8.webp" alt="No Coding" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-26 f-md-32 lh140 w600 text-center white-clr mt20 red-head">
                  No Designing
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/proof9.webp" alt="No Coding" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-26 f-md-32 lh140 w600 text-center white-clr mt20 red-head">
                  No Content Writing
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="text-center">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/proof10.webp" alt="No Coding" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-26 f-md-32 lh140 w600 text-center white-clr mt20 red-head">
                  No Optimizing
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>

   <!-- Testimonial Section Start -->
   <!-- <div class="testimonial-sec">
      <div class="container ">
         <div class="row ">
            <div class="col-12 f-25 f-md-42 w700 lh140 black-clr text-center">
               Join Over 2,463 Members Who Uses LearnX Every Day
            </div>
            <div class="col-12 f-22 f-md-28 w400 lh140 black-clr text-center">
               (And You Can Join Them In 10 Seconds)
            </div>
         </div>
         <div class="row row-cols-md-2 row-cols-1 gx4 mt-md20 mt0">
            <div class="col mt20 mt-md50 ">
               <div class="single-testimonial">
                  <div >
                     <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/craig-mcintosh.webp" class="img-fluid d-block mx-auto testimonials-profile" alt="Craig McIntosh">
                  </div>
                  <div class="f-24 f-md-28 w600 lh140 white-clr pl-md20">Craig McIntosh </div>
                  <div class="stars mt10 pl-md20">
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                  </div>
                 
                  <p class="mt20 f-18 f-md-20 lh140 w400 white-clr">
                  LearnX is a game-changer in online education! This breakthrough e-learning platform allowed me to effortlessly create top-notch, ready-to-go academy websites in minutes. With the added advantage of being preloaded with 500+ engaging courses and e-books, it made the process fast and efficient. Don't miss out on this opportunity; you'll regret it.
                  </p>
               </div>
            </div>
            <div class="col mt20 mt-md50">
               <div class="single-testimonial">
                  <div>
                     <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/edward-reid.webp" class="img-fluid d-block mx-auto testimonials-profile" alt="Edward Reid">
                  </div>
                  <div class="f-24 f-md-28 w600 lh140 white-clr pl-md20"> Edward Reid  </div>
                  <div class="stars mt10 pl-md20">
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                  </div>
                 
                  <p class="mt20 f-18 f-md-20 lh140 w400 white-clr">
                  LearnX has been a game-changer for me! Creating and selling courses has never been so effortless. The AI-powered platform took care of everything, allowing me to focus on my content. The results? Increased profits and a stress-free e-learning journey. Highly recommended!
                  </p>
               </div>
            </div>
            <div class="col mt-md50 mt20 mx-auto">
               <div class="single-testimonial">
                  <div >
                     <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/jaeden-downs.webp" class="img-fluid d-block mx-auto testimonials-profile" alt="Jaeden Downs">
                  </div>
                  <div class="f-24 f-md-28 w600 lh140 white-clr pl-md20"> Jaeden Downs  </div>
                  <div class="stars mt10 pl-md20">
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                  </div>
                  
                  <p class="mt20 f-18 f-md-20 lh140 w400 white-clr">
                  LearnX is a revolutionary tool that turned my passion into profits. With its easy-to-use interface and AI-powered course creation, I effortlessly built my own e-learning site. The best part? It's a one-stop solution, handling everything from setup to sales.
                  </p>
               </div>
            </div>
            <div class="col mt-md50 mt20 mx-auto">
               <div class="single-testimonial">
                  <div>
                     <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/stuart-johnson.webp" class="img-fluid d-block mx-auto testimonials-profile" alt="Drew Warren">
                  </div>
                  <div class="f-24 f-md-28 w600 lh140 white-clr pl-md20"> Drew Warren  </div>
                  <div class="stars mt10 pl-md20">
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                  </div>
                  
                  <p class="mt20 f-18 f-md-20 lh140 w400 white-clr">
                  LearnX transformed the way I approach online courses. The AI-generated content and seamless platform gave my courses a professional edge. The pre-loaded courses were a fantastic bonus, making it a breeze to kickstart my e-learning business. A must-have for anyone in the digital education space!
                  </p>
               </div>
            </div>
         </div>
      </div>
   </div> -->
   <!-- Testimonial Section End -->

   <div class="areyouready-section">
      <div class="container">
         <div class="row">
            <div class="col-12 f-28 f-md-50 w700 lh140 black-clr text-center">
               Are You Ready For An A.I. Revolution?
            </div>
            <div class="col-12 col-md-7 mx-auto f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
            LearnX  And AI Pay Us Daily…
            </div>
            <div class="col-12 f-18 f-md-22 w400 lh140 black-clr text-center mt20 mt-md50">
               There is nothing that AI can't do in the digital world… It's the most powerful technology that exists now… And LearnX is the only app on the market…<br><br>
               <span class="w600 lh120">
               That harnessed that power, and created something that makes us money day after day
            </span>
            </div>
            <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/proof11.webp" alt="Quotes" class="mx-auto d-block img-fluid mt20 mt-md30 ">
            <!-- <div class="col-12 f-18 f-md-22 w400 lh140 black-clr text-center mt20 mt-md50">
               Every day we make money without doing any work… And not pocket change…<br><br>
               <span class="w600 lh120">
               Last month alone we made a bit over $17,000 with LearnX 
               </span>
            </div>
            <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/proof2.webp" alt="Quotes" class="mx-auto d-block img-fluid mt20 mt-md30 "> -->
            <!-- <div class="col-12 f-28 f-md-50 w700 lh140 black-clr text-center">
               Wanna know how?
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/red-underline.webp" alt="Quotes" class="mx-auto d-block img-fluid ">
            </div> -->
         </div>
      </div>
   </div>
                        
   <!-- Eye-Opening Facts Section Start -->
   <!-- <div class="stats-section">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-12">
               
            </div>
            <div class="col-md-12 col-12">
                  <div class="f-md-50 f-28 lh140 w700 black-clr text-center  text-center text-capitalize mt20 mt-md50">
                     Checkout These Stats When It Comes <br class="visible-lg"> To E-Learning

                  </div>
            </div>
            <div class="col-12 col-md-10 offset-md-1 mt20 mt-md30">
                  <div class="row">
                     <div class="col-12 col-md-3 order-md-2 p0">
                        <div>
                              <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/stats1-icon.png" class="img-fluid mx-auto d-block">
                        </div>
                     </div>
                     <div class="col-12 col-md-9 order-md-1 mt20 mt-md30 p-md0">
                        <div class="content-style1" editabletype="shape" style="z-index: 10;">
                              <div class="f-20 f-md-24 w400 lh140 text-xs-center black-clr" editabletype="text" style="z-index: 12;">For every dollar spent on eLearning, <span class="w700">companies make $30 in productivity</span></div>
                        </div>
                     </div>
                  </div>
            </div>

            <div class="col-12 col-md-10 offset-md-1 mt20 mt-md15">
                  <div class="row">
                     <div class="col-12 col-md-3 p0">
                        <div>
                              <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/stats2-icon.png" class="img-fluid mx-auto d-block">
                        </div>
                     </div>
                     <div class="col-12 col-md-9 mt20 mt-md30 p-md0">
                        <div class="content-style2" editabletype="shape" style="z-index: 10;">
                              <div class="f-20 f-md-24 w400 lh140 text-xs-center black-clr" editabletype="text" style="z-index: 12;">The mobile e-learning market is expected to <span class="w700"> cross $38 billion USD by 2020</span></div>
                        </div>
                     </div>
                  </div>
            </div>

            <div class="col-12 col-md-10 offset-md-1 mt20 mt-md15">
                  <div class="row">
                     <div class="col-12 col-md-3 order-md-2 p0">
                        <div>
                              <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/stats3-icon.png" class="img-fluid mx-auto d-block">
                        </div>
                     </div>
                     <div class="col-12 col-md-9 order-md-1 mt20 mt-sm30 p-sm0">
                        <div class="content-style1" editabletype="shape" style="z-index: 10;">
                              <div class="f-20 f-md-24 w400 lh140 text-xs-center black-clr" editabletype="text" style="z-index: 12;"><span class="w700">More than 40% of Fortune 500 companies </span> use e-learning regularly and extensively</div>
                        </div>
                     </div>
                  </div>
            </div>

            <div class="col-12 col-md-10 offset-md-1 mt20 mt-md15">
                  <div class="row">
                     <div class="col-12 col-md-3 p0">
                        <div>
                              <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/stats4-icon.png" class="img-fluid mx-auto d-block">
                        </div>
                     </div>
                     <div class="col-12 col-md-9 mt20 mt-md30 p-md0">
                        <div class="content-style2" editabletype="shape" style="z-index: 10;">
                              <div class="f-20 f-md-24 w400 lh140 text-xs-center black-clr" editabletype="text" style="z-index: 12;"><span class="w700">72% of organizations believe  </span>that eLearning puts them at a competitive advantage</div>
                        </div>
                     </div>
                  </div>
            </div>
            <div class="col-12 mt20 mt-md50">
                  <div class="f-20 f-md-28 w700 lh140 text-center black-clr" editabletype="text" style="z-index:10;">Yes, it’s very high in-demand!</div>
            </div>
         </div>
      </div>
   </div> -->
<!-- Eye-Opening Facts Section End -->

   <!-- Stil No Compettion Section Start -->
   <section class="still-think-section">
         <div class="container">
            <div class="row">
               <div class="col-12"><img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/e-learning-box.png" class="img-fluid mx-auto d-block"></div>
               <div class="col-md-12 col-12 px-md30 mt20 mt-md50">
                  <div class="f-28 f-md-40 lh140 w700 text-center white-clr">
                  Look At These Marketers Generating 6 Figures Just By Selling Simple Courses Online
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-12 col-md-12 mt20 mt-md150">
                  <div class="still-think-shape">
                     <div class="row">
                        <div class="col-md-4 col-12">
                           <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/len-smith.webp" class="img-fluid mx-auto d-block still-section-image">
                        </div>
                        <div class="col-md-8 col-12">
                           <div class="f-24 f-md-32 lh140 w400 white-clr mt20 mt-md20">
                           Len Smith is making <span class="yellow-clr w700">$90K every month</span> selling his Copywriting Secrets Course
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-12 col-md-12 mt20 mt-md200">
                  <div class="still-think-shape1">
                     <div class="row">
                        <div class="col-md-4 col-12 order-md-2">
                           <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/john-omar.webp" class="img-fluid mx-auto d-block still-section-image1">
                        </div>
                        <div class="col-md-8 col-12 order-md-1 ">
                           <div class="f-24 f-md-32 lh140 w400 white-clr mt20 mt-md0">
                           John Omar and Eliot Arntz made <span class="yellow-clr w700">$700K in a month</span> by Selling their iOS App Development Course
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-12 col-md-12 mt20 mt-md200">
                  <div class="still-think-shape">
                     <div class="row">
                        <div class="col-md-4 col-12">
                           <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/rob.webp" class="img-fluid mx-auto d-block still-section-image">
                        </div>
                        <div class="col-md-8 col-12">
                           <div class="f-24 f-md-32 lh140 w400 white-clr mt20 mt-md0">
                           Rob Percival makes <span class="yellow-clr w700">$150K every month</span> - selling the same programming course to new students
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
           
            <div class="col-md-12 col-12 mt20 mt-md150">
               <div class="f-md-40 f-26 lh160 w700 text-center white-clr">
               But You’re Probably Wondering!
               </div>
            </div>
         </div>
      </section>
   <!-- Stil No Compettion Section End -->
   
   <div class="you-can-section">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-md-12 col-12">
               <div class="f-md-35 f-26 lh140 w700 white-clr you-can-shape">
               Can I Make A Full Time Income Selling My Knowledge?
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md50">
               <div class="f-md-50 f-28 lh140 w700 black-clr">
               Of Course You Can!
               </div>
               <div class="f-md-22 f-198 lh160 w400 black-clr mt20">
               Everyday, millions upon millions scour the internet looking for solutions to their problems. These same people voraciously buy up all sorts of courses to help them achievea goal or avoid pain.
               </div>
               <div class="f-md-22 f-198 lh160 w400 black-clr mt20">
               And trust me, you already know something that people are willing to pay you for.
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md50">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/you-cam-image.webp" class="img-fluid mx-auto d-block">
            </div>
         </div>
      </div>
   </div>

   <!-- You can teach anything  Start -->
   <div class="anything-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                     <div class="f-22 f-md-32 w400  lh140 black-clr text-center" editabletype="text" style="z-index:10;"><span class="w700">You can teach anything.</span> You <span class="w700">don’t need to be an expert</span> or have everything be perfect to get started.
                     </div>
               </div>
               <div class="col-12 mt20 mt-md50">
                     <div editabletype="image" style="z-index:10;">
                        <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/you-can-teach-anything.png" class="img-fluid mx-auto d-block">
                     </div>
               </div>
            </div>
         </div>
   </div>
   <!-- You can teach anything  End -->

   

   <!-- Imagine Section Start -->
   <div class="imagine-section">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-28 f-md-50 w400 lh140 black-clr text-center">
               Imagine If You Can Start Today.. <br class="d-none d-md-block"><span class="w700"> With ONE App…</span>
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md50">
            <div class="col-12 col-md-6  order-md-2">
               <div class="f-22 f-md-22 w400 lh140 black-clr text-center text-md-start">
               I know it sounds weird…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               But it’s actually true…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               We created something that the world has not seen before…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               We managed to infiltrate the Online-Learning industry…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Which is estimate to be worth a bit over $300 BILLION…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               With one app…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               And the best part is….
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               This one app does everything for us…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Literally all what we have to do…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Is sit back…
               </div>
               <!-- <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               And watch payments like this roll in…
               </div> -->
               <!-- <div class="mt20">
                 <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/proof1.webp" alt="imagine-img" class="mx-auto d-block img-fluid ">
               </div> -->
               <!-- <div class="f-18 f-md-22 w600 lh140 black-clr text-center text-md-start mt20">
               <u>But wanna know how it works?</u>
               </div> -->
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/imagine-img.webp" alt="imagine-img" class="mx-auto d-block img-fluid ">
            </div>
         </div>

         <div class="row mt20 mt-md50">
         <div class="col-12 f-28 f-md-50 w700 lh140 black-clr text-center">
               Wanna know how?
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/red-underline.webp" alt="Quotes" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>
   <!-- Imagine Section End -->


   

   <div class="profitale-section">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="easy-text f-28 f-md-65 w700 lh140 white-clr mx-auto">Easy</div>
               <div class="f-28 f-md-50 w300 lh140 black-clr text-center mt20">
                  We Sell <u><span class="w600">PROFITABLE AI Courses!</span></u> 
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md50">
            <div class="col-12 col-md-6">
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start">
                  I know what you thinking….
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
                  “I don’t know the first thing about creating, let alone selling a course”
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
                  You would be surprised that, I don’t know too…
               </div>
               <div class="f-18 f-md-22 w600 lh140 black-clr text-center text-md-start mt20">
                  Yes…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
                  It’s not easy…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
                  And that’s why we created a beast of an app…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
                  That do all of that for us on autopilot…
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/profitable-img.webp" alt="Profitable" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <div class="grey-section">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-28 f-md-50 w700 lh140 black-clr text-center">
                  I NEVER Create A Course…
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md50">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-22 f-md-22 w400 lh140 black-clr text-center text-md-start">
                  It’s true… Never in my life…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
                  I can’t even if i wanted to…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
                  But you know who can?
               </div>
               <div class="f-24 f-md-28 w600 lh140 white-clr text-center text-md-start mt20">
                  <div class="niche-design">
                  AI
                  </div>
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
                  We Created the world’s first app…
               </div>
               <div class="f-18 f-md-22 w600 lh140 black-clr text-center text-md-start mt20">
                  That leverage AI to create us hundreds of courses…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
                  In any niche we want…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
                  Without us doing ANY thing…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
                  But that’s not all….
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/create-course.webp" alt="Create Course" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <div class="profitale-section">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-28 f-md-50 w300 lh140 black-clr text-center">
               We Create Udemy-Like Websites <br class="d-none d-md-block"><span class="w700">That Pay Us Daily…</span>
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md50">
            <div class="col-12 col-md-6">
               <div class="pay-wall">
                  <div class="f-18 f-md-22 w400 lh140 white-clr">
                     Udemy is one of the most visited websites on earth…
                  </div>
                  <div class="f-18 f-md-22 w400 lh140 white-clr mt20">
                     And it’s for a good reason…
                  </div>
                  <div class="f-18 f-md-22 w400 lh140 white-clr mt20">
                     In this digital age, everyone is trying to learn and expand their skills online…
                  </div>
                  <div class="f-18 f-md-22 w600 lh140 white-clr mt20">
                     People will happily hundreds of dollars a month…
                  </div>
                  <div class="f-18 f-md-22 w400 lh140 white-clr mt20">
                     To access courses…
                  </div>
                  <div class="f-18 f-md-22 w400 lh140 white-clr mt20">
                     And it’s not just Udemy…
                  </div>
                  <div class="f-18 f-md-22 w400 lh140 white-clr mt20">
                     Websites like…
                  </div>
                  <ul class="list-head pl0 m0 f-18 f-md-22 lh160 w600 white-clr">
                     <li>SkillShare</li>
                     <li>Lynda</li>
                     <li>Masterclass</li>
                  </ul>
                  <div class="f-18 f-md-22 w400 lh140 white-clr mt20">
                     And we managed to create an app, that will replicate their success for us…
                  </div>
                  <div class="f-18 f-md-22 w400 lh140 white-clr mt20">
                     With zero work involved…
                  </div>
                  <div class="f-18 f-md-22 w400 lh140 white-clr mt20">
                     It does everything for us…
                  </div>
                  <div class="f-18 f-md-22 w400 lh140 white-clr mt20">
                     And I do mean… EVERYTHING.
                  </div>
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/udemy-website.webp" alt="Profitable" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <div class="take-look-sec">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-28 f-md-50 w600 lh140 white-clr text-center">
               Take A Look At What LearnX <br class="d-none d-md-block"> Created For Us… 
               </div>
            </div>
         </div>
         <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 gap30 mt20 mt-md10">
            <div class="col">
               <div class="video-box1">
                  <div style="padding-bottom: 56.25%;position: relative;"><iframe src="https://aicademy.oppyo.com/video/embed/xwgw1x48vd" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div>
               </div>
            </div>
            <div class="col">
               <div class="video-box1">
                  <div style="padding-bottom: 56.25%;position: relative;"><iframe src="https://aicademy.oppyo.com/video/embed/a7aofg8uzm" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div>
               </div>
            </div>
            <div class="col">
               <div class="video-box1">
                  <div style="padding-bottom: 56.25%;position: relative;"><iframe src="https://aicademy.oppyo.com/video/embed/6lsgozh9xc" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div>
               </div>
            </div>
            <div class="col">
               <div style="padding-bottom: 56.25%;position: relative;"><iframe src="https://aicademy.oppyo.com/video/embed/zhhocsqwcg" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div>
            </div>
            <div class="col">
               <div class="video-box1">
                  <div style="padding-bottom: 56.25%;position: relative;"><iframe src="https://aicademy.oppyo.com/video/embed/cpkcbm24pf" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div>
               </div>
            </div>
            <div class="col">
               <div class="video-box1">
                  <div style="padding-bottom: 56.25%;position: relative;"><iframe src="https://aicademy.oppyo.com/video/embed/ta2b2rhknp" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div>
               </div>
            </div>
         </div>
      </div>
   </div>

   <div class="not-just">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-50 w700 lh140  black-clr text-center">
                  But Not Just Courses…
               </div>
            </div>
            <div class="col-12 mt20 mt-md30 text-center">
               <div class="f-24 f-md-36 w400 black-clr lh140">
                  LearnX Creates Stunning Websites for us that will work 24/7…. To sell and our courses…
               </div>
            </div>
            <div class="col-12 mt-md50 mt20">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/f1.webp" class="img-fluid d-block mx-auto">
            </div>
         </div>
      </div>
   </div>

   <div class="traffic-section">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-6">
               <div class="f-28 f-md-45 w400 lh140 mt20 black-clr text-center text-md-start">
               Even Traffic… <br class="d-none d-md-block"><span class="w600">Is Done-For-Us</span>
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               LearnX does that for us automatically…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               It lines up hundreds of customers for us every single day…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Without us spending a penny on ads… or waiting…
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/traffic-img.webp" alt="Traffic" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <div class="online-section">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-28 f-md-45 w400 lh140 mt20 black-clr text-center text-md-start">
               This Is Your Chance…  <br class="d-none d-md-block"> <span class="w600">To Tap Into The Online-Learning Industry</span>
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               LearnX made it incredibly easy for us…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               To tap into the exploding business that is e-learning with just one click…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               The same business that even big tech giants like IBM, Google, And Microsoft got into…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               They spent billions of dollars creating their own e-learning portals…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Both for profit, and to train their staff…
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/online-img.webp" alt="Traffic" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <div class="capability-sec" id="features">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-45 w500 lh140 text-center black-clr">
                  Take A Look At What <span class="w700 orange-line"> LearnX Can Do For You </span>
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md90">
            <div class="col-md-6 col-12">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
                  Build A Udemy-Like Website  
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  With just one click, you will have a DFY Udemy-like website that well sell any course you want on your behalf…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Without you doing any of the work…
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0">
               <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 2px solid #F27D66;">
                  <source src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/udemy-website.mp4" type="video/mp4">
               </video>
            </div>
         </div>

         <div class="row align-items-center mt-md120">
            <div class="col-md-6 col-12 order-md-2">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
                  Generate AI Courses Easily
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Sell high-quality courses that you did NOT create…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  But rather, AI created…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  It’s as easy as it sounds, just enter a keyword…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  And let AI do its magic…
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0 order-md-1">
               <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 2px solid #F27D66;">
                  <source src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/generate-course.mp4" type="video/mp4">
               </video>
            </div>
         </div>

         <div class="row align-items-center mt-md120">
            <div class="col-md-6 col-12">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
                  1-Click Course Details.
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Each course you create…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  AI will generate all the details, descriptions, tags for each one…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  This will make your life 10x easier…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  As it will do all the selling on your behalf.
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0">
               <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 2px solid #F27D66;">
                  <source src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/course-details.mp4" type="video/mp4">
               </video>
            </div>
         </div>

         <div class="row align-items-center mt-md120">
            <div class="col-md-6 col-12 order-md-2">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
                  AI-ChatBots
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Forget all about manual customer support.
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Because now, you will have automated AI bots that will engage with your customers and offer them any kind of help they want…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Not only that, it will even upsell the customers for you.
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0 order-md-1">
               <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 2px solid #F27D66;">
                  <source src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/ai-chatbot.mp4" type="video/mp4">
               </video>
            </div>
         </div>
         
         <div class="row align-items-center mt-md120">
            <div class="col-md-6 col-12 ">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
                  Generate Attention-Sucking Images
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Turn any keyword into a stunning graphic generated by AI… 
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  This alone will turn your website into a futuristic website with ease…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Never hire a graphic designer again. 
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/attention-sucking.webp" alt="Attention Sucking" class="mx-auto d-block img-fluid">
               <!-- <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted">
                  <source src="https://cdn.oppyotest.com/launches/webgenie/preview/images/attention-sucking.mp4" type="video/mp4">
               </video> -->
            </div>
         </div>

      </div>
   </div>

   <div class="dfy-section">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-50 w700 text-center white-clr">
               DFY Themes
               </div>
               <div class="f-22 f-md-28 w400 text-center white-clr mt10">
               Choose from out stunning designs. Without writing any code… Or design anything… All of that is DFY on autopilot
               </div>
            </div>
            <div class="col-md-10 mx-auto mt20 mt-md50">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/funnel-planner.webp" alt="DFY Themes" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <div class="capability-sec">
   <div class="container">
         <div class="row align-items-center">
            <div class="col-md-6 col-12">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
                  Accept Payments
               </div>
               
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
               Start taking money with the payment method you like...
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
               LearnX is made to easily work with any way you like to get paid.
               </div>
               
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/ayment-accept.webp" alt="100% Mobile Optimized Websites" class="mx-auto d-block img-fluid ">
            </div>
         </div> 
         <div class="row align-items-center mt-md120">
            <div class="col-md-6 col-12 order-md-2">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
               Course Completion Certificate
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Easily build your targeted email list with LearnX AR integration…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Simply connect your autoresponder, and let LearnX do the rest…
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0 order-md-1">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/certificate.webp" alt="Seamless AR Integration" class="mx-auto d-block img-fluid ">
            </div>
         </div>    
         <div class="row align-items-center mt-md120">
            <div class="col-md-6 col-12">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
               Create VSLs
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
               Produce high-quality VSLs for your courses...
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
               Now you can showcase each course to your audience without hiring a video editor or recording anything yourself.
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/vsl.webp" alt="Seamless AR Integration" class="mx-auto d-block img-fluid ">
            </div>
         </div>       
         <div class="row align-items-center mt-md120">
            <div class="col-md-6 col-12 order-md-2">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
                  100% Mobile Optimized Websites
               </div>
               
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
               Award certificates to your students upon completing their courses, celebrating their achievements!
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0 order-md-1">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/optimized-websites.webp" alt="100% Mobile Optimized Websites" class="mx-auto d-block img-fluid ">
            </div>
         </div>
        
         <div class="row align-items-center mt-md120">
            <div class="col-md-6 col-12">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
                  Seamless AR Integration
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Easily build your targeted email list with LearnX AR integration…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Simply connect your autoresponder, and let LearnX do the rest…
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/ar-integration.webp" alt="Seamless AR Integration" class="mx-auto d-block img-fluid ">
            </div>
         </div>

         <!--<div class="row align-items-center mt-md120">-->
         <!--   <div class="col-md-6 col-12">-->
         <!--      <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">-->
         <!--         Built-In Appointment Booking-->
         <!--      </div>-->
         <!--      <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">-->
         <!--         Does your business requires setting appointments?-->
         <!--      </div>-->
         <!--      <div class="f-22 f-md-28 w700 text-center text-md-start orange-gradient mt20">-->
         <!--         Easy.-->
         <!--      </div>-->
         <!--      <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">-->
         <!--         Let LearnX does that for you on autopilot with its powerful appointment-setting features-->
         <!--      </div>-->
         <!--   </div>-->
         <!--   <div class="col-md-6 col-12 mt20 mt-md0">-->
         <!--      <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/appointment-booking.webp" alt="Appointment Booking" class="mx-auto d-block img-fluid ">-->
         <!--   </div>-->
         <!--</div>-->

         <div class="row align-items-center mt-md120">
            <div class="col-md-6 col-12 order-md-2">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
                  Built-In Social Media Integration
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  With one click, auto syndicate all your content into over 50+ social media platform…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  This will give you a surge of highly targeted traffic within seconds.
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0 order-md-1">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/social-media-integration.webp" alt="Social Media Integration" class="mx-auto d-block img-fluid ">
            </div>
         </div>

         <div class="row align-items-center mt-md120">
            <div class="col-md-6 col-12">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
                  Commercial License Included
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Your copy of LearnX will come with a commercial license, so you can use it on your client website too
               </div>
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr mt20">
                  Valid only until the timer hits zero…
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/license.webp" alt=" Commercial License" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>
   
   <div class="in-action-section" id="demo">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="f-28 f-md-50 w700 lh140 white-clr text-center">
                        Watch LearnX In Action, And See What It Can Do For You…
                    </div>
                    <div class="mt20 mt-md50">
                        <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/blue-arrow.webp" alt="Product Box" class="mx-auto d-block img-fluid ">
                    </div>
                </div>
                <div class="col-md-10 col-12 mx-auto mt20 mt-md50">
                    <div class="col-12">
                        <div class="video-box">
                           <div style="padding-bottom: 56.25%;position: relative;"><iframe src="https://learnx.dotcompal.com/video/embed/7hm6bwdezf" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div>
                        </div>      
                    </div>
                </div>
            </div>
        </div>
        <div class="container mt20 mt-md30">
            <div class="row">
                <div class="col-12">
                    <div class="f-28 f-md-38 w700 lh140 text-center white-clr">
                        Get LearnX And Save $293 Now <br class="d-none d-md-block"> 
                    </div>
                    <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                        <span class="w700">Just Pay Once</span> Instead Of a <s>Monthly Fee</s>
                    </div>
                </div>
            </div>
            <div class="row align-items-center mt20 mt-md50">
                <div class="col-md-7">
                  <div class="cta-link-btn mt20">
                     <a href="#buynow" >&gt;&gt;&gt; Get Instant Access To LearnX &lt;&lt;&lt;</a>
                  </div>
                    <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/compaitable-gurantee1.webp" alt="Compaitable Gurantee" class="mx-auto d-block img-fluid mt20 mt-md30">
                </div>
                <div class="col-md-5 mt20 mt-md0">
                    <div class="countdown-container">
                        <div class="f-20 f-md-24 w400 lh140 text-center white-clr mb10">
                            <span class="w700 yellow-clr">Hurry up!</span>  Price Increases In
                        </div>
                        <div class="countdown counter-white text-center">
                            <div class="timer-label text-center">
                                <span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 smmltd">Days</span> 
                            </div>
                            <div class="timer-label text-center">
                                <span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                            </div>
                            <div class="timer-label text-center timer-mrgn">
                                <span class="f-31 f-md-45 timerbg oswald">46&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                            </div>
                            <div class="timer-label text-center ">
                                <span class="f-31 f-md-45 timerbg oswald">28</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

   

 

   <div class="newbie-section">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-6">
               <div class="f-22 f-md-50 w400 lh140 mt20 black-clr text-center text-md-start">
                  Even If You Are A <br class="d-none d-md-block"> Complete Newbie…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Can you believe it?
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Even if you don’t have ANY business right now…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               We still can make TONS of money using LearnX
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Simply by selling online courses
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Let LearnX do all the work…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               And we just collect money…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               As simple as that…
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/newbie-img.webp" alt="Newbie" class="mx-auto d-block img-fluid ">
            </div>
         </div>
         <div class="row mt20 mt-md95">
            <div class="col-12 text-center">
               <div class="f-28 f-md-50 w700 lh140 black-clr">
                  Ready To Transform Your Life?
               </div>
               <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/red-line.webp" alt="Red Line" class="mx-auto d-block img-fluid">
            </div>
         </div>
      </div>
   </div>

   <div class="hi-their-sec">
      <div class="container hi-their-box">
         <div class="row">
            <div class="col-12 text-center">
               <div class="hi-their-shadow">
                  <div class="f-28 f-md-45 lh140 black-clr caveat w700 hi-their-head">
                     Hey There,
                  </div>
               </div>
            </div>
         </div>
         <div class="row mt20 mt-md80">
            <div class="col-md-3 text-center">
               <div class="figure mr15 mr-md0">
                  <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/pranshu-gupta.webp" alt="Pranshu Gupta" class="mx-auto d-block img-fluid  img-shadow">
                  <div class="figure-caption mt10">
                     <div class="f-24 f-md-28 w700 lh140 black-clr caveat text-center">
                        Pranshu Gupta
                     </div>
                  </div>
               </div>
               <div class="figure mt20 mt-md30">
                  <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/bizomart-team.webp" alt="Bizomart Team" class="mx-auto d-block img-fluid  img-shadow">
                  <div class="figure-caption mt10">
                     <div class="f-24 f-md-28 w700 lh140 black-clr caveat text-center">
                        Bizomart Team
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-md-9 mt20 mt-md0">
               <div class="f-28 f-md-38 lh140 black-clr w400">
                  It’s <span class="w600">Pranshu</span> along with <span class="w600">Bizomart Team.</span> 
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr mt20 mt-md30">
                  I’m a full time internet marketer…
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr mt20">
                  Over the years, I tried dozens (it could be hundreds even…) of methods to make money online…
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr mt20">
                  But the only thing that gave me the life I always wanted was…
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr mt20">
               Creating Learning websites…
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr mt20">
                  It’s the easiest, and fastest way to build wealth…
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr mt20">
                  Not just make some pocket change…&nbsp;
               </div>
               <div class="f-20 f-md-22 lh140 w600 black-clr mt20">
                  Thanks to that…
               </div>
            </div>
         </div>

         <div class="row mt20 mt-md50">
            <div class="col-12">
               <div class="blue-border"></div>
            </div>
         </div>

         <div class="row mt20 mt-md50">
            <div class="col-12 text-center">
               <div class="f-22 f-md-28 lh140 black-clr w600">
                  I can spend my time as I please
               </div>
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/red-underline.webp" alt="Quotes" class="mx-auto d-block img-fluid  mt10">
            </div>
         </div>

         <div class="row mt20 mt-md50">
            <div class="col-md-3">
               <figure>
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/family.webp" alt="Family" class="mx-auto d-block img-fluid ">
                  <figure-caption>
                     <div class="f-20 f-md-22 w600 lh140 black-clr mt10 text-center">
                        Spend it with my family
                     </div>
                  </figure-caption>
               </figure>
            </div>
            <div class="col-md-3">
               <figure>
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/travel.webp" alt="Travel" class="mx-auto d-block img-fluid ">
                  <figure-caption>
                     <div class="f-20 f-md-22 w600 lh140 black-clr mt10 text-center">
                        Travel
                     </div>
                  </figure-caption>
               </figure>
            </div>
            <div class="col-md-3">
               <figure>
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/sunset-beach.webp" alt="Enjoy a sunset on the beach" class="mx-auto d-block img-fluid ">
                  <figure-caption>
                     <div class="f-20 f-md-22 w600 lh140 black-clr mt10 text-center">
                        Enjoy a sunset on the beach
                     </div>
                  </figure-caption>
               </figure>
            </div>
            <div class="col-md-3">
               <figure>
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/video-games.webp" alt="Or even play video games on my PS5" class="mx-auto d-block img-fluid ">
                  <figure-caption>
                     <div class="f-20 f-md-22 w600 lh140 black-clr mt10 text-center">
                        Or even play video games on my PS5
                     </div>
                  </figure-caption>
               </figure>
            </div>
         </div>

         <div class="row mt20 mt-md80">
            <div class="col-12 text-center">
               <div class="f-20 f-md-24 w600 lh140 black-clr">
                  And the best part is… No matter what I do with my day… I still make a lot of money…
               </div>
               <div class="f-20 f-md-24 w400 lh140 black-clr mt20">
                  And I’m not telling you this to brag… Not at all… I’m telling you this to demonstrate why you should even listen to me…
               </div>
            </div>
         </div>
      </div>
   </div>

   <div class="goal-section">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-28 f-md-50 w700 lh140 black-clr">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/blub.webp" class="img-fluid mr-md5 mb5 mb-md0" alt="blub">
                  I have One Goal This Year…
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/blub.webp" class="img-fluid ml-md5 mb5 mb-md0" alt="blub">
               </div>
            </div>
         </div>
         <div class="row mt20 mt-md70 align-items-center">
            <div class="col-md-6">
               <div class="f-20 f-md22 w400 lh140 black-clr">
                  You see…
               </div>
               <div class="f-20 f-md22 w400 lh140 black-clr mt20">
                  Making money online is good any everything…&nbsp;
               </div>
               <div class="f-20 f-md22 w400 lh140 black-clr mt20">
                  But what’s 10x better… Is helping someone else make it…
               </div>
               <div class="f-20 f-md22 w400 lh140 black-clr mt20">
                  This is why I decided that in 2023 <span class="w600">I will help 100 new people…</span> 
               </div>
               <div class="f-20 f-md22 w400 lh140 black-clr mt20">
                  Quit their job, and have the financial freedom they always wanted…
               </div>
               <div class="f-20 f-md22 w400 lh140 black-clr mt20">
                  So far, it’s been going GREAT…
               </div>
               <div class="f-20 f-md22 w400 lh140 black-clr mt20">
                  I’ve managed to help countless people…
               </div>
            </div>
            <div class="col-md-6 mt20 mt-md0">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/goal.webp" alt="Women" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <div class="success-story-section">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-28 f-md-50 w700 lh140 black-clr">
                  And I’m Here To Help You My <br class="d-none d-md-block">
                  Next Success Story…
               </div>
            </div>
         </div>
         <div class="row mt20 mt-md70 align-items-center">
            <div class="col-md-6 order-md-2">
               <div class="f-20 f-md22 w400 lh140 black-clr">
                  Listen, if you’re reading this page right now…
               </div>
               <div class="f-20 f-md22 w400 lh140 black-clr mt20">
                  It means one thing, you’re dedicated to making money online…
               </div>
               <div class="f-20 f-md22 w600 lh140 black-clr mt20">
                  And I have some good news for you…
               </div>
               <div class="f-20 f-md22 w400 lh140 black-clr mt20">
                  On this page, I will give you everything you need…
               </div>
               <div class="f-20 f-md22 w400 lh140 black-clr mt20">
                  To get started and join me and thousands of other profitable members…&nbsp;
               </div>
               <div class="f-20 f-md22 w400 lh140 black-clr mt20">
                  Who changed their lives forever…
               </div>
            </div>
            <div class="col-md-6 mt20 mt-md0 order-md-1">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/successful-man.webp" alt="SuccessFull Man" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <div class="demo-sec">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-28 f-md-50 w700 black-clr lh140">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/cool.webp" class="img-fluid mr-md5 mb5 mb-md0" alt="Cool">
                  Let Me Show You Something Cool…
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/takealook-brush.webp" alt="Quotes" class="mx-auto d-block img-fluid ">
               </div>
            </div>
            <div class="col-12 mt20 mt-md80">
               <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="">
                  <source src="https://cdn.oppyotest.com/launches/webgenie/preview/images/cool.mp4" type="video/mp4">
               </video>
            </div>
            <div class="col-12 mt20 mt-md50 text-center">
               <div class="f-22 f-md-28 w600 lh140 black-clr">
                  You see this endless stream of payments?
               </div>
               <div class="f-22 f-md-28 w400 lh140 black-clr mt10">
                  We got all of those doing only one thing…
               </div>
               <div class="smile-text f-22 f-md-28 w600 lh140 white-clr mt20 mt-md50">Using LearnX</div>
               <div class="f-22 f-md-28 w400 lh140 black-clr mt20 mt-md49">
                  We turn it on, and that’s pretty much it… As simple as that…
               </div>
               <div class="f-22 f-md-28 w400 lh140 black-clr mt20">
                  This month alone…
               </div>
               <!-- <div class="f-28 f-md-38 w400 lh140 black-clr mt20 mt-md30">
                  We made <span class="red-clr w600 affliliate-underline">  $24,124.65 on one of our affiliate accounts…</span>  
               </div>
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/arrow-down-3.webp" alt="Demo Proof" class="d-block img-fluid  mt20 ">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/demo-proof.webp" alt="Demo Proof" class="mx-auto d-block img-fluid  "> -->
               <div class="f-28 f-md-38 w400 lh140 black-clr mt20 mt-md30">
               And I’m gonna show you exactly how I do it…
               </div>
            </div>
         </div>
      </div>
   </div>
      
      <!-- Well, not anymore Section End-->
      <div class="proudly-section" id="product">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="prdly-pres f-28 f-md-38 w700 lh140 text-center white-clr">
                  Introducing
                  </div>
               </div>
               <div class="col-12 mt-md50 mt20 text-center">
               <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 484.58 200.84" style="max-height:167px">
                        <defs>
                            <style>
                                .cls-1,.cls-4{fill:#fff;}
                                .cls-2,.cls-3,.cls-4{fill-rule:evenodd;}
                                .cls-2{fill:url(#linear-gradient);}
                                .cls-3{fill:url(#linear-gradient-2);}
                                .cls-5{fill:url(#linear-gradient-3);}
                            </style>
                            <linearGradient id="linear-gradient" x1="-154.67" y1="191.86" x2="249.18" y2="191.86" gradientTransform="matrix(1.19, -0.01, 0.05, 1, 178.46, -4.31)" gradientUnits="userSpaceOnUse">
                                <stop offset="0" stop-color="#ead40c"/>
                                <stop offset="1" stop-color="#ff00e9"/>
                            </linearGradient>
                            <linearGradient id="linear-gradient-2" x1="23.11" y1="160.56" x2="57.61" y2="160.56" gradientTransform="matrix(1, 0, 0, 1, 0, 0)" xlink:href="#linear-gradient"/>
                            <linearGradient id="linear-gradient-3" x1="384.49" y1="98.33" x2="483.38" y2="98.33" gradientTransform="matrix(1, 0, 0, 1, 0, 0)" xlink:href="#linear-gradient"/>
                        </defs>
                        <g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1">
                            <path class="cls-1" d="M112.31,154.3q-11.65,0-20.1-4.87a32.56,32.56,0,0,1-13-13.82q-4.53-9-4.54-21.11a46.06,46.06,0,0,1,4.57-21A34.29,34.29,0,0,1,92,79.37a36.11,36.11,0,0,1,19.31-5.06,39.65,39.65,0,0,1,13.54,2.29,31,31,0,0,1,11.3,7.09,33.32,33.32,0,0,1,7.75,12.18,49.23,49.23,0,0,1,2.82,17.57V119H83.26v-12.3h46A19.87,19.87,0,0,0,127,97.38a16.6,16.6,0,0,0-6.18-6.48,17.59,17.59,0,0,0-9.21-2.37,17.88,17.88,0,0,0-9.83,2.7,18.81,18.81,0,0,0-6.58,7.06,20.17,20.17,0,0,0-2.4,9.56v10.74a25.15,25.15,0,0,0,2.47,11.57,17.42,17.42,0,0,0,6.91,7.37,20.52,20.52,0,0,0,10.39,2.55,21.62,21.62,0,0,0,7.22-1.14,15.71,15.71,0,0,0,5.59-3.35,14.11,14.11,0,0,0,3.59-5.5L146,132a26.44,26.44,0,0,1-6.13,11.77,29.72,29.72,0,0,1-11.52,7.77A43.74,43.74,0,0,1,112.31,154.3Z"/>
                            <path class="cls-1" d="M184.49,154.35a31.78,31.78,0,0,1-13.24-2.65,21.26,21.26,0,0,1-9.28-7.84,22.86,22.86,0,0,1-3.41-12.81A21.92,21.92,0,0,1,161,120.2a18.9,18.9,0,0,1,6.61-6.86,33.85,33.85,0,0,1,9.46-3.9,77.76,77.76,0,0,1,10.92-2q6.81-.7,11-1.28a15.91,15.91,0,0,0,6.18-1.82,4.25,4.25,0,0,0,1.94-3.86v-.3q0-5.7-3.38-8.83T194,88.28q-6.71,0-10.62,2.92a14.55,14.55,0,0,0-5.27,6.91l-17-2.42a27.51,27.51,0,0,1,6.66-11.83,29.46,29.46,0,0,1,11.35-7.16,43.73,43.73,0,0,1,14.83-2.39,47.89,47.89,0,0,1,11.14,1.31,31.6,31.6,0,0,1,10.14,4.31,22.12,22.12,0,0,1,7.39,8.14q2.81,5.15,2.8,12.87v51.85H207.84V142.14h-.61a22.1,22.1,0,0,1-4.66,6,22.35,22.35,0,0,1-7.52,4.49A30,30,0,0,1,184.49,154.35Zm4.74-13.42a19.7,19.7,0,0,0,9.53-2.19,16,16,0,0,0,6.23-5.83,15,15,0,0,0,2.19-7.91v-9.13a8.72,8.72,0,0,1-2.9,1.31,45.56,45.56,0,0,1-4.56,1.06c-1.68.3-3.35.57-5,.8l-4.29.61a32,32,0,0,0-7.32,1.81A12.29,12.29,0,0,0,178,125a9.76,9.76,0,0,0,1.82,13.39A16,16,0,0,0,189.23,140.93Z"/>
                            <path class="cls-1" d="M243.8,152.79V75.31h17.7V88.23h.81a19.36,19.36,0,0,1,7.29-10.37,20,20,0,0,1,11.83-3.66c1,0,2.14,0,3.4.13a23.83,23.83,0,0,1,3.15.38V91.5a21,21,0,0,0-3.65-.73,38.61,38.61,0,0,0-4.82-.32,18.47,18.47,0,0,0-8.95,2.14,15.94,15.94,0,0,0-6.23,5.93,16.55,16.55,0,0,0-2.27,8.72v45.55Z"/>
                            <path class="cls-1" d="M318.4,107.39v45.4H300.14V75.31h17.45V88.48h.91a22,22,0,0,1,8.55-10.34q5.86-3.84,14.55-3.83a27.58,27.58,0,0,1,14,3.43,23.19,23.19,0,0,1,9.28,9.93,34.22,34.22,0,0,1,3.26,15.79v49.33H349.87V106.28c0-5.17-1.34-9.23-4-12.15s-6.37-4.39-11.07-4.39a17,17,0,0,0-8.5,2.09,14.67,14.67,0,0,0-5.8,6A20,20,0,0,0,318.4,107.39Z"/>
                            <path class="cls-2" d="M314.05,172.88c12.69-.14,27.32,2.53,40.82,2.61l-.09,1c1.07.15,1-.88,2-.78,1.42-.18,0,1.57,1.82,1.14.54-.06.08-.56-.32-.68.77,0,2.31.17,3,1.26,1.3,0-.9-1.34.85-.89,6,1.63,13.39,0,20,3.2,11.64.72,24.79,2.38,38,4.27,2.85.41,5.37,1.11,7.57,1.05,2-.05,5.68.78,8,1.08,2.53.34,5.16,1.56,7.13,1.65-1.18-.05,5.49-.78,4.54.76,5-.72,10.9,1.67,15.94,1.84.83-.07.69.53.67,1,1.23-1.17,3.32.56,4.28-.56.25,2,2.63-.48,3.3,1.61,2.23-1.39,4.83.74,7.55,1.37,1.91.44,5.29-.21,5.55,2.14-2.1-.13-5.12.81-11.41-1.09,1,.87-3.35.74-3.39-.64-1.81,1.94-6.28-1-7.79,1.19-1.26-.41,0-.72-.64-1.35-5,.55-8.09-1.33-12.91-1.56,2.57,2.44,6.08,3.16,10.06,3.22,1.28.15,0,.74,1,1.39,1.37-.19.75-.48,1.26-1.17,5.37,2.28,15.36,2.35,21,4.91-4.69-.63-11.38,0-15.15-2.09A148,148,0,0,1,441.58,196c-.53-.1-1.81.12-.7-.71-1.65.73-3.41.1-5.74-.22-37.15-5.15-80.47-7.78-115.75-9.76-39.54-2.22-76.79-3.22-116.44-2.72-61.62.78-119.59,7.93-175.21,13.95-5,.55-10,1.49-15.11,1.47-1.33-.32-2.46-1.91-1.25-3-2-.38-2.93.29-5-.15a9.83,9.83,0,0,1-1.25-3,13.73,13.73,0,0,1,6.42-2.94c.77-.54.12-.8.15-1.6a171,171,0,0,1,45.35-8.58c26.43-1.1,53.68-4.73,79.64-6,6.25-.3,11.72.06,18.41.14,8.31.1,19.09-1,29.19-.12,6.25.54,13.67-.55,21.16-.56,39.35-.09,71.16-.23,112.28,2C317.24,171.93,315.11,173.81,314.05,172.88Zm64.22,16.05-1.6-.2C376.2,190,378.42,190.26,378.27,188.93Z"/>
                            <polygon class="cls-3" points="52.57 166.06 57.61 151.99 23.11 157.23 32.36 169.12 52.57 166.06"/>
                            <polygon class="cls-4" points="44.69 183.24 51.75 168.35 33.86 171.06 44.69 183.24"/>
                            <path class="cls-4" d="M.12,10.15l22,145.06,35.47-5.39-22-145a2.6,2.6,0,0,0,0-.4C35,.76,26.62-.95,16.81.54S-.52,6.16,0,9.77A2.62,2.62,0,0,0,.12,10.15Z"/>
                            <path class="cls-5" d="M433.68,79.45l21-36.33h27.56L448.48,97.51l34.9,56H456l-22-37.76-21.94,37.76H384.49l34.9-56L385.72,43.12h27.35Z"/>
                        </g>
                            </g>
                    </svg>
               </div>
               <div class="text-center mt20 mt-md50">
                  <div class="pre-heading1 f-md-22 f-20 w600 lh140 white-clr">
                  Exploit The Emerging E-Learning Business With A Click…  
                  </div>
               </div>
               <div class="f-md-50 f-28 w700 white-clr lh140 mt-md30 mt20 text-center">
                  First To Market AI App <span class="yellow-clr"> Creates Self-Managed Academy Sites, And Prefill It With Tons of Unique AI-Generated Courses</span>
               </div>
               <!-- <div class="mt-md30 mt20 f-28 f-md-50 w700 text-center lh140 text-capitalize white-clr">
                  <div class="post-heading">And Makes Us $872.36 Per Day…</div>
                  
               </div> -->
               <div class="f-22 f-md-28 lh140 w400 text-center white-clr mt20 mt-md30">
                  Finally, Let AI Build You A Passive Income Stream Without Doing Any Work
               </div>
            </div>
            <div class="row d-flex flex-wrap align-items-center mt20 mt-md50">
               <div class="col-12 col-md-10 mx-auto">
                  <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/product-image.webp" class="img-fluid d-block mx-auto margin-bottom-15">
               </div>
            </div>
         </div>
      </div>
   <!-- Section-12 End Story Dosnt Section Start -->
   <div class="endstory">
         <div class="container">
            <div class="row">

               <div class="col-12 px0 px-md15">
                     <div class="f-md-50 f-28 lh140 w700  black-clr text-center">
                        Let Me Share Why Creating Courses Is An Incredible Opportunity!
                     </div>
               </div>
               <div class="col-12 col-md-12 px0 mt20 mt-sm40">
                  <div class="row align-items-center">
                     <div class="col-12 col-md-9 order-md-2 px0 px-md15 mt-md30">
                        <div class="f-22 f-md-28 w400  lh140 black-clr text-xs-center" editabletype="text" style="z-index:10;">
                           You can create a course once and create recurring income <span class="w700">by selling the same course </span>Over and over again. </div>
                     </div>

                     <div class="col-12 col-md-3 order-md-1 px0 px-md15 mt15 mt-sm0">
                        <div editabletype="image" style="z-index:10;">
                           <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/advantages-image.png" class="img-responsive center-block">
                        </div>
                     </div>
                  </div>
               </div>

               <div class="col-12 col-md-12 px0 mt20 mt-md30">
                     <div class="row align-items-center">
                     <div class="col-12 col-md-9 px0 px-md15 mt-md30">
                        <div class="f-22 f-md-28 w400  lh140 black-clr text-xs-center" editabletype="text" style="z-index:10;">
                           The sky's the limit – you can scale your e-learning to epic proportions by offering different levels such  <span class="w700">as beginner, Intermediate, and advanced.</span>

                        </div>
                     </div>

                     <div class="col-12 col-md-3 px0 px-md15 mt15 mt-md0">
                        <div editabletype="image" style="z-index:10;">
                           <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/sky-is-the.png" class="img-responsive center-block">
                        </div>
                     </div>
                     </div>
               </div>

               <div class="col-12 col-md-12 px0 mt20 mt-md30">
                     <div class="row align-items-center">
                     <div class="col-12 col-md-9 order-md-2 px0 px-md15 mt-md30">
                        <div class="f-22 f-md-28 w700  lh140 black-clr text-xs-center" editabletype="text" style="z-index:10;">
                           Create & sell multiple courses on multiple topics
                        </div>
                     </div>

                     <div class="col-12 col-md-3 order-md-1 px0 px-md15 mt15 mt-md0">
                        <div editabletype="image" style="z-index:10;">
                           <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/create-sell-multiple.png" class="img-responsive center-block">
                        </div>
                     </div>
                     </div>
               </div>

               <div class="col-12 col-md-12 px0 mt20 mt-md30">
                    <div class="row align-items-center">
                    <div class="col-12 col-md-9 px0 px-md15 mt-md30">
                        <div class="f-22 f-md-28 w700  lh140 black-clr text-xs-center" editabletype="text" style="z-index:10;">
                           Charge others to sell their courses on your platform

                        </div>
                     </div>

                     <div class="col-12 col-md-3 px0 px-md15 mt15 mt-md0">
                        <div editabletype="image" style="z-index:10;">
                           <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/and-if-you-dont.png" class="img-responsive center-block">
                        </div>
                     </div>
                    </Div>
               </div>

               <div class="col-12 col-md-12 px0 mt20 mt-md30">
                     <div class="row align-items-center">
                     <div class="col-12 col-md-9 order-md-2 px0 px-md15 mt-md30">
                        <div class="f-22 f-md-28 w400  lh140 black-clr text-xs-center" editabletype="text" style="z-index:10;">
                           <span class="w700">This offers you true freedom.You can do it, when & where you want </span>– in lockdown, after lockdown, there’s no limitation.

                        </div>
                     </div>

                     <div class="col-12 col-md-3 order-md-1 px0 px-md15 mt15 mt-md0">
                        <div editabletype="image" style="z-index:10;">
                           <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/and-you-dont-need-to-take.png" class="img-responsive center-block">
                        </div>
                     </div>
                     </div>
               </div>

               <div class="col-12 px0 mt30 mt-md80">
                     <div class="f-22 f-md-36 w400  lh140 black-clr text-center" editabletype="text" style="z-index:10;"><span class="w700">And the best thing is </span>–having e-courses and e-learning marketplaces allows people to access your courses at anytime from anywhere via Mobile, Tabs, Pads, Laptops, Desktops etc.
                     </div>
               </div>


               <div class="col-12 mt-md50 mt20">
                     <div editabletype="image" style="z-index:10;">
                        <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/social-image1.png" class="img-responsive center-block">
                     </div>
               </div>

               <div class="col-12 px0 mt-md50 mt20">
                     <div class="f-20 f-md-36 w400  lh140 black-clr text-center" editabletype="text" style="z-index:10;">That means you will be able to drive non-stop traffic, leads, sales & profits 24/7, 365 days a year - 100% hands free!</div>
               </div>
            </div>
         </div>
   </div>
   <!-- Section-12 End Story Dosnt Section End -->

   <!-- CTA Section Start -->
   <div class="cta-section-white">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-38 w700 lh140 text-center white-clr">
                  Get LearnX And Save $293 Now <br class="d-none d-md-block"> 
               </div>
               <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                  <span class="w700">Just Pay Once</span> Instead Of a <s>Monthly Fee</s>
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md50">
            <div class="col-md-7">
            <div class="cta-link-btn mt20">
                     <a href="#buynow" >&gt;&gt;&gt; Get Instant Access To LearnX &lt;&lt;&lt;</a>
                  </div>
               <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/compaitable-gurantee1.webp" alt="Compaitable Gurantee" class="mx-auto d-block img-fluid mt20 mt-md30">
            </div>
            <div class="col-md-5 mt20 mt-md0">
               <div class="countdown-container">
                  <div class="f-20 f-md-24 w400 lh140 text-center white-clr mb10">
                     <span class="w700 yellow-clr">Hurry up!</span>  Price Increases In
                  </div>
                  <div class="countdown counter-white text-center">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 smmltd">Days</span> 
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-45 timerbg oswald">46&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-45 timerbg oswald">28</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> 
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- CTA Section End -->

   <!--Limited Section Start -->
   <div class="limited-time-sec">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-20 f-md-24 w700 text-center white-clr align-items-center flex-column flex-md-row justify-content-center">
                  <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/caution-icon.webp" class="img-fluid mr-md5 mb5 mb-md0" alt="Caution Icon">
                  Your $2,475.34 Bonuses Package Expires When Timer Hits ZERO 
                  <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/caution-icon.webp" class="img-fluid ml-md5 mb5 mb-md0" alt="Caution Icon">
               </div>
            </div>
         </div>
      </div>
   </div>
   <!--Limited Section End -->

      <div class="capability-sec" id="features">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-45 w500 lh140 text-center black-clr">
                  Take A Look At What <span class="w700 orange-line"> LearnX Can Do For You </span>
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md90">
            <div class="col-md-6 col-12">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
                  Build A Udemy-Like Website  
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  With just one click, you will have a DFY Udemy-like website that well sell any course you want on your behalf…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Without you doing any of the work…
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0">
               <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 2px solid #F27D66;">
                  <source src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/udemy-website.mp4" type="video/mp4">
               </video>
            </div>
         </div>

         <div class="row align-items-center mt-md120">
            <div class="col-md-6 col-12 order-md-2">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
                  Generate AI Courses Easily
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Sell high-quality courses that you did NOT create…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  But rather, AI created…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  It’s as easy as it sounds, just enter a keyword…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  And let AI do its magic…
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0 order-md-1">
               <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 2px solid #F27D66;">
                  <source src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/generate-course.mp4" type="video/mp4">
               </video>
            </div>
         </div>

         <div class="row align-items-center mt-md120">
            <div class="col-md-6 col-12">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
                  1-Click Course Details.
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Each course you create…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  AI will generate all the details, descriptions, tags for each one…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  This will make your life 10x easier…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  As it will do all the selling on your behalf.
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0">
               <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 2px solid #F27D66;">
                  <source src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/course-details.mp4" type="video/mp4">
               </video>
            </div>
         </div>

         <div class="row align-items-center mt-md120">
            <div class="col-md-6 col-12 order-md-2">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
                  AI-ChatBots
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Forget all about manual customer support.
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Because now, you will have automated AI bots that will engage with your customers and offer them any kind of help they want…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Not only that, it will even upsell the customers for you.
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0 order-md-1">
               <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 2px solid #F27D66;">
                  <source src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/ai-chatbot.mp4" type="video/mp4">
               </video>
            </div>
         </div>
         
         <div class="row align-items-center mt-md120">
            <div class="col-md-6 col-12 ">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
                  Generate Attention-Sucking Images
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Turn any keyword into a stunning graphic generated by AI… 
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  This alone will turn your website into a futuristic website with ease…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Never hire a graphic designer again. 
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0 ">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/attention-sucking.webp" alt="Attention Sucking" class="mx-auto d-block img-fluid">
               <!-- <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted">
                  <source src="https://cdn.oppyotest.com/launches/webgenie/preview/images/attention-sucking.mp4" type="video/mp4">
               </video> -->
            </div>
         </div>

      </div>
   </div>

   <div class="dfy-section">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-50 w700 text-center white-clr">
               DFY Themes
               </div>
               <div class="f-22 f-md-28 w400 text-center white-clr mt10">
               Choose from out stunning designs. Without writing any code… Or design anything… All of that is DFY on autopilot
               </div>
            </div>
            <div class="col-md-10 mx-auto mt20 mt-md50">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/funnel-planner.webp" alt="DFY Themes" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <div class="capability-sec">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-md-6 col-12">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
                  Accept Payments
               </div>
               
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
               Start taking money with the payment method you like...
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
               LearnX is made to easily work with any way you like to get paid.
               </div>
               
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/ayment-accept.webp" alt="100% Mobile Optimized Websites" class="mx-auto d-block img-fluid ">
            </div>
         </div> 
         <div class="row align-items-center mt-md120">
            <div class="col-md-6 col-12 order-md-2">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
               Course Completion Certificate
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Easily build your targeted email list with LearnX AR integration…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Simply connect your autoresponder, and let LearnX do the rest…
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0 order-md-1">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/certificate.webp" alt="Seamless AR Integration" class="mx-auto d-block img-fluid ">
            </div>
         </div>    
         <div class="row align-items-center mt-md120">
            <div class="col-md-6 col-12">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
               Create VSLs
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
               Produce high-quality VSLs for your courses...
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
               Now you can showcase each course to your audience without hiring a video editor or recording anything yourself.
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/vsl.webp" alt="Seamless AR Integration" class="mx-auto d-block img-fluid ">
            </div>
         </div>       
         <div class="row align-items-center mt-md120">
            <div class="col-md-6 col-12 order-md-2">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
                  100% Mobile Optimized Websites
               </div>
               
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
               Award certificates to your students upon completing their courses, celebrating their achievements!
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0 order-md-1">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/optimized-websites.webp" alt="100% Mobile Optimized Websites" class="mx-auto d-block img-fluid ">
            </div>
         </div>
        
         <div class="row align-items-center mt-md120">
            <div class="col-md-6 col-12">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
                  Seamless AR Integration
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Easily build your targeted email list with LearnX AR integration…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Simply connect your autoresponder, and let LearnX do the rest…
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/ar-integration.webp" alt="Seamless AR Integration" class="mx-auto d-block img-fluid ">
            </div>
         </div>

         <!--<div class="row align-items-center mt-md120">-->
         <!--   <div class="col-md-6 col-12">-->
         <!--      <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">-->
         <!--         Built-In Appointment Booking-->
         <!--      </div>-->
         <!--      <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">-->
         <!--         Does your business requires setting appointments?-->
         <!--      </div>-->
         <!--      <div class="f-22 f-md-28 w700 text-center text-md-start orange-gradient mt20">-->
         <!--         Easy.-->
         <!--      </div>-->
         <!--      <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">-->
         <!--         Let LearnX does that for you on autopilot with its powerful appointment-setting features-->
         <!--      </div>-->
         <!--   </div>-->
         <!--   <div class="col-md-6 col-12 mt20 mt-md0">-->
         <!--      <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/appointment-booking.webp" alt="Appointment Booking" class="mx-auto d-block img-fluid ">-->
         <!--   </div>-->
         <!--</div>-->

         <div class="row align-items-center mt-md120">
            <div class="col-md-6 col-12 order-md-2">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
                  Built-In Social Media Integration
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  With one click, auto syndicate all your content into over 50+ social media platform…
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  This will give you a surge of highly targeted traffic within seconds.
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0 order-md-1">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/social-media-integration.webp" alt="Social Media Integration" class="mx-auto d-block img-fluid ">
            </div>
         </div>

         <div class="row align-items-center mt-md120">
            <div class="col-md-6 col-12">
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr">
                  Commercial License Included
               </div>
               <div class="f-20 f-md-22 w400 text-center text-md-start black-clr mt20">
                  Your copy of LearnX will come with a commercial license, so you can use it on your client website too
               </div>
               <div class="f-26 f-md-36 w700 text-center text-md-start black-clr mt20">
                  Valid only until the timer hits zero…
               </div>
            </div>
            <div class="col-md-6 col-12 mt20 mt-md0">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/license.webp" alt=" Commercial License" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>
   
    

   


   <div class="white-section">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-6">
               <div class="f-22 f-md-45 w400 lh140 mt20 black-clr text-center text-md-start">
                  Finally… <br class="d-none d-md-block"> Dominate The E-Learning Niche
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
                  With LearnX…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
                  It never been easier to dominate this niche…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
                  We literally made it fail-proof…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
                  There is no way for you to fail this. Even if you tried to…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
                  We give you the AI model…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
                  That made it all possible for us.  
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/e-learning.webp" alt="E-Leaning" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <div class="experience-section">
      <div class="container">
         <div class="row">
            <div class="col-12">
            <div class="f-28 f-md-45 w700 lh140 mt20 black-clr text-center">
               It Doesn’t Matter What Experience You Have…
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md30">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Look, it’s very easy and simple…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               This is the best side income you can have…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               You can continue doing your job or business as usual if you want…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               And just use LearnX to make a few thousands a month extra…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Or you can go all in…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               And use it to make fulltime income like us…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               And enjoy life…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               As long as you are trying to make money online…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               LearnX is for you…
               </div>
               <div class="f-18 f-md-22 w600 lh140 black-clr text-center text-md-start mt20">
               It work for…
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/matter.webp" alt="Matter" class="mx-auto d-block img-fluid ">
            </div>
         </div>
        <!-- <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/blue-arrow.webp" alt="arrow" class="mx-auto d-block img-fluid mt20 mt-md50"> -->
         <div class="regardless-sec">
            <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3">
               <div class="col">
                  <div class="figure">
                     <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/product-creator.webp" alt="Product Creators" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption">
                        <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                        Product Creators
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="figure">
                     <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/affiliate-marketers.webp" alt="Affiliate Marketers" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption">
                        <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                           Affiliate Marketers
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="figure">
                     <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/ecom-store-owners.webp" alt="eCom Store Owners" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption">
                        <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                           eCom Store Owners
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="figure">
                     <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/blog-owners.webp" alt="Blog Owners" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption">
                        <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                           Blog Owners
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="figure">
                     <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/marketers.webp" alt="CPA Marketers" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption">
                        <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                           CPA Marketers
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="figure">
                     <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/video-marketers.webp" alt="Video Marketers" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption">
                        <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                           Video Marketers
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="figure">
                     <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/content-creators.webp" alt="Artists/Content Creators" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption">
                        <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                           Artists/Content Creators
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="figure">
                     <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/personal-brands.webp" alt="Personal Brands" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption">
                        <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                           Personal Brands
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="figure">
                     <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/freelancers.webp" alt="Freelancers" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption">
                        <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                           Freelancers
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>

      </div>
   </div>

   <div class="white-section">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-6">
               <div class="f-28 f-md-50 w700 lh140 text-center text-md-start black-clr">
               There Is Nothing For You To Do…<span class="red-clr"><u> EXCEPT</u></span>
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               We did all the work for you…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               There is nothing for you to do literally…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               AI will take over, and create your website…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               AND sell it for you…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               The only thing you would need to do is…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Keep refreshing your account and see how much you’ve made…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Other than that…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               You’re all set.
               </div>
               <ul class="cross-list pl0 m0 f-18 f-md-22 lh160 w600 black-clr">
                  <li>No Website Creation</li>
                  <li>No Programming Required</li>
                  <li>No Complicated Setup</li>
                  <li>No Hiring Anyone</li>
               </ul>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/no-setup.webp" alt="Nothing Except" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <div class="grey-section">
      <div class="container">
         <div class="row ">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-28 f-md-50 w700 lh140 text-center text-md-start black-clr">
                  Allow ChatGPT To Run Your Business…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               We created the best AI model that integrate with ChatGPT…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               That will run everything for us…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Its the same model that we used on a brand new account and& streamlining tasks effortlessly.
                           </div>
               <div class="text-center text-md-start mt20 mt-md70 ml-md30">
                  <div class="smile-text f-20 f-md-22 w400 lh140 white-clr mt20 mt-md24">Sounds good, huh?</div>
               </div>
            </div>
            <div class="col-12 col-md-6  mt20 mt-md0 order-md-1">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/chatgpt.webp" alt="ChatGPT" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>


   <div class="white-section"> 
      <div class="container">
         <div class="row">
            <div class="col-12 col-md-6">
               <div class="f-28 f-md-50 w700 lh140 text-center text-md-start black-clr">
                 <span class="red-clr">How</span>  Does It Work?
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               It’s very easy actually…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               LearnX will create you a udemy-like website…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               And then generate AI courses for you in any niche you want…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               Giving you a stunning and fully functional courses website…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               Then it will start promoting it on your behalf  too…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               You don’t have to run ads or do anything…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               It will do  that for you.
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               And with one click it will integrate with your favoritet payment processor and autoresponder
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               Now you have a fully functional courses website…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               That will start bringing in customers and sales <span class="w600">every single day…</span> 
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               Best part is, all it takes is just 30 seconds. If not less.
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/work-men.webp" alt="Work Men" class="d-block mx-auto img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <div class="grey-section">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-28 f-md-50 w700 lh140 text-center text-md-start black-clr">
                 <span class="red-clr">How</span> Is It Different?
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                 LearnX is the only app that does everything…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               It doesn’t matter if  you  have experience or not…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               There is nothing technical that you need to do whatsoever….
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               Not even a line of code…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               And you don’t need to design anything either…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               All you need is just give your website a name…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               And that pretty much it
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               Removing all the guesswork, and hard work from your end… 
               </div>
            </div>
            <div class="col-12 col-md-6 order-md-1 mt20 mt-md0">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/different-girl.webp" alt="Different" class="d-block mx-auto img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <div class="white-section">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-24 f-md-38 w400 text-center black-clr dotted-border">
               With LearnX
               </div>
               <div class="f-28 f-md-50 w700 lh140 mt11 black-clr">
               Everything Is Done For You By The Most <br class="d-none d-md-block">Powerful AI Model Ever Created…
               </div>
            </div>
         </div>
         <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 gap30 mt20 mt-md10">
            <div class="col">
               <div class="figure">
                  <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/dfy-websites.webp" alt="Udemy-Like Websites" class="mx-auto d-block img-fluid ">
                  <div class="figure-caption text-center">
                     <div class="f-22 f-md-28 w400 lh140 mt20 black-clr">Udemy-Like Websites</div>
                  </div>
               </div>
            </div>

            <div class="col">
               <div class="figure">
                  <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/dfy-designs.webp" alt="DFY Design" class="mx-auto d-block img-fluid ">
                  <div class="figure-caption text-center">
                     <div class="f-22 f-md-28 w400 lh140 mt20 black-clr">DFY Design</div>
                  </div>
               </div>
            </div>

            <div class="col">
               <div class="figure">
                  <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/dfy-content.webp" alt="DFY Courses" class="mx-auto d-block img-fluid ">
                  <div class="figure-caption text-center">
                     <div class="f-22 f-md-28 w400 lh140 mt20 black-clr">DFY Courses</div>
                  </div>
               </div>
            </div>

            <div class="col">
               <div class="figure">
                  <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/dfy-training.webp" alt="DFY Training" class="mx-auto d-block img-fluid ">
                  <div class="figure-caption text-center">
                     <div class="f-22 f-md-28 w400 lh140 mt20 black-clr">DFY Training </div>
                  </div>
               </div>
            </div>

            <div class="col">
               <div class="figure">
                  <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/dfy-traffic.webp" alt="DFY Traffic" class="mx-auto d-block img-fluid ">
                  <div class="figure-caption text-center">
                     <div class="f-22 f-md-28 w400 lh140 mt20 black-clr">DFY Traffic</div>
                  </div>
               </div>
            </div>

            <div class="col">
               <div class="figure">
                  <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/dfy-mobile-optimization.webp" alt="DFY Mobile Optimization" class="mx-auto d-block img-fluid ">
                  <div class="figure-caption text-center">
                     <div class="f-22 f-md-28 w400 lh140 mt20 black-clr">DFY Mobile Optimization</div>
                  </div>
               </div>
            </div>

            <div class="col">
               <div class="figure">
                  <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/dfy-chatbot.webp" alt="DFY AI Chatbots" class="mx-auto d-block img-fluid ">
                  <div class="figure-caption text-center">
                     <div class="f-22 f-md-28 w400 lh140 mt20 black-clr">DFY AI Chatbots</div>
                  </div>
               </div>
            </div>

            <div class="col">
               <div class="figure">
                  <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/dfy-woocomerce-integration.webp" alt="DFY Integration" class="mx-auto d-block img-fluid ">
                  <div class="figure-caption text-center">
                     <div class="f-22 f-md-28 w400 lh140 mt20 black-clr">DFY Integration</div>
                  </div>
               </div>
            </div>
            
            <div class="col">
               <div class="figure">
                  <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/dfy-templates.webp" alt="DFY Templates" class="mx-auto d-block img-fluid ">
                  <div class="figure-caption text-center">
                     <div class="f-22 f-md-28 w400 lh140 mt20 black-clr">DFY Templates</div>
                  </div>
               </div>
            </div>

         </div>
      </div>
   </div>

   <!-- CTA Section Start -->
   <div class="cta-section-white">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-38 w700 lh140 text-center white-clr">
                  Get LearnX And Save $293 Now <br class="d-none d-md-block"> 
               </div>
               <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                  <span class="w700">Just Pay Once</span> Instead Of a <s>Monthly Fee</s>
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md50">
            <div class="col-md-7">
            <div class="cta-link-btn mt20">
                     <a href="#buynow" >&gt;&gt;&gt; Get Instant Access To LearnX &lt;&lt;&lt;</a>
                  </div>
               <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/compaitable-gurantee1.webp" alt="Compaitable Gurantee" class="mx-auto d-block img-fluid mt20 mt-md30">
            </div>
            <div class="col-md-5 mt20 mt-md0">
               <div class="countdown-container">
                  <div class="f-20 f-md-24 w400 lh140 text-center white-clr mb10">
                     <span class="w700 yellow-clr">Hurry up!</span>  Price Increases In
                  </div>
                  <div class="countdown counter-white text-center">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 smmltd">Days</span> 
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-45 timerbg oswald">46&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-45 timerbg oswald">28</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> 
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- CTA Section End -->

   <!--Limited Section Start -->
   <div class="limited-time-sec">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-20 f-md-24 w700 text-center white-clr align-items-center flex-column flex-md-row justify-content-center">
                  <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/caution-icon.webp" class="img-fluid mr-md5 mb5 mb-md0" alt="Caution Icon">
                  Your $2,475.34 Bonuses Package Expires When Timer Hits ZERO 
                  <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/caution-icon.webp" class="img-fluid ml-md5 mb5 mb-md0" alt="Caution Icon">
               </div>
            </div>
         </div>
      </div>
   </div>
   <!--Limited Section End -->

   <div class="step-section">
      <div class="container ">
         <div class="row ">
            <div class="col-12 f-28 f-md-50 w700 lh140 black-clr text-center">
               All It Takes Is 3 Fail-Proof Steps<br class="d-none d-md-block">
            </div>
            <div class="col-12 f-24 f-md-28 w600 lh140 white-clr text-center mt20">
               <div class="niche-design">
                  Start Your E-Learning Empire Today.
               </div>
            </div>
            <div class="col-12 mt30 mt-md90">
               <div class="row align-items-center ">
                  <div class="col-12 col-md-6">
                     <div class="step-shapes f-20 f-md-22 w600 white-clr">
                        Step 1
                     </div>
                     <div class="f-34 f-md-65 w700 lh140 mt15 caveat">
                        Login
                     </div>
                     <div class="f-20 f-md-24 w400 lh140 mt5 mt-md10">
                        Click On Any Of The Button Below To Get Instant Access To LearnX
                     </div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 ">
                     <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 1px solid #F27D66;">
                        <source src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/step1.mp4" type="video/mp4">
                     </video>
                  </div>
               </div>
            </div>
            <div class="col-12">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/left-arrow.webp" class="img-fluid d-none d-md-block mx-auto " alt="Left Arrow">
            </div>
            <div class="col-12 mt30 mt-md30">
               <div class="row align-items-center ">
                  <div class="col-12 col-md-6 order-md-2">
                     <div class="step-shapes f-20 f-md-22 w600 white-clr">
                        Step 2
                     </div>
                     <div class="f-34 f-md-65 w700 lh140 mt15 caveat">
                        Deploy
                     </div>
                     <div class="f-20 f-md-24 w400 lh140 mt5 mt-md10">
                        Just select any niche you want, and let AI create your Udemy-Like website in 30 seconds or less (prefilled With AI Courses, And With DFY Traffic)
                     </div>
                  </div>
                  <div class="col-md-6 col-md-pull-6 mt20 mt-md0 order-md-1 ">
                     <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 1px solid #F27D66;">
                        <source src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/step2.mp4" type="video/mp4">
                     </video>
                  </div>
               </div>
            </div>
            <div class="col-12">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/right-arrow.webp" class="img-fluid d-none d-md-block mx-auto" alt="Right Arrow">
            </div>
            <div class="col-12 mt30 mt-md30">
               <div class="row align-items-center ">
                  <div class="col-12 col-md-6">
                     <div class="step-shapes f-20 f-md-22 w600 white-clr">
                        Step 3
                     </div>
                     <div class="f-34 f-md-65 w700 lh140 mt15 caveat">
                        Profit
                     </div>
                     <div class="f-20 f-md-24 w400 lh140 mt5 mt-md10">
                        Sit back and relax. And let AI do the rest of the work. While you enjoy the results
                     </div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 ">
                     <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 1px solid #F27D66;">
                        <source src="https://cdn.oppyotest.com/launches/webgenie/preview/images/step3.mp4" type="video/mp4">
                     </video>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>

   <!-- CTA Section Start -->
   <div class="cta-section-white">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-38 w700 lh140 text-center white-clr">
                  Get LearnX And Save $293 Now <br class="d-none d-md-block"> 
               </div>
               <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                  <span class="w700">Just Pay Once</span> Instead Of a <s>Monthly Fee</s>
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md50">
            <div class="col-md-7">
            <div class="cta-link-btn mt20">
                     <a href="#buynow" >&gt;&gt;&gt; Get Instant Access To LearnX &lt;&lt;&lt;</a>
                  </div>
               <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/compaitable-gurantee1.webp" alt="Compaitable Gurantee" class="mx-auto d-block img-fluid mt20 mt-md30">
            </div>
            <div class="col-md-5 mt20 mt-md0">
               <div class="countdown-container">
                  <div class="f-20 f-md-24 w400 lh140 text-center white-clr mb10">
                     <span class="w700 yellow-clr">Hurry up!</span>  Price Increases In
                  </div>
                  <div class="countdown counter-white text-center">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 smmltd">Days</span> 
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-45 timerbg oswald">46&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-45 timerbg oswald">28</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> 
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- CTA Section End -->

   <!--Limited Section Start -->
   <div class="limited-time-sec">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-20 f-md-24 w700 text-center white-clr align-items-center flex-column flex-md-row justify-content-center">
                  <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/caution-icon.webp" class="img-fluid mr-md5 mb5 mb-md0" alt="Caution Icon">
                  Your $2,475.34 Bonuses Package Expires When Timer Hits ZERO 
                  <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/caution-icon.webp" class="img-fluid ml-md5 mb5 mb-md0" alt="Caution Icon">
               </div>
            </div>
         </div>
      </div>
   </div>
   <!--Limited Section End -->

   <section class="white-section">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-28 f-md-50 w700 lh140 black-clr text-center">
                 <span class="left-line">Everything You Need</span> Is Included…
               </div>
            </div>
         </div>
         <div class="row mt20 mt-md80 align-items-center">
            <div class="col-12 col-md-6">
               <div class="f-28 f-md-38 lh140 w700 text-center white-clr need-head">
                  LearnX App
               </div>
               <div class="f-20 f-md-22 w400 lh140 mt20 black-clr">
                  The app that is responsible for everything…
                  Start Your E-Learning Empire Today…
               </div>
               <div class="f-20 f-md-22 w700 lh140 mt10 red-clr">
                  (Worth $997/mo)
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/webgenie-app.webp" alt="LearnX App" class="mx-auto d-block img-fluid ">
            </div>
         </div>
         <div class="row mt20 mt-md80">
            <div class="col-12">
               <div class="border-line"></div>
            </div>
         </div>
         <div class="row mt20 mt-md80 align-items-center">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-28 f-md-38 lh140 w700 text-center white-clr need-head">
                  AI Course Generator
               </div>
               <div class="f-20 f-md-22 w400 lh140 mt20 black-clr">
                  With one click, Fill your new website with hundreds of AI courses in any niche you want. Without you doing any of the work
               </div>
               <div class="f-20 f-md-22 w700 lh140 mt10 red-clr">
                  (Worth $997)
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/ai-content-generator.webp" alt="AI Content Generator" class="mx-auto d-block img-fluid ">
            </div>
         </div>
         <div class="row mt20 mt-md80">
            <div class="col-12">
               <div class="border-line"></div>
            </div>
         </div>
         <div class="row mt20 mt-md80 align-items-center">
            <div class="col-12 col-md-6">
               <div class="f-28 f-md-38 lh140 w700 text-center white-clr need-head">
                  LearnX Built-In Traffic
               </div>
               <div class="f-20 f-md-22 w400 lh140 mt20 black-clr">
                  You don’t have to worry about traffic any more, let AI handle that for you
               </div>
               <div class="f-20 f-md-22 w700 lh140 mt10 red-clr">
                  (Worth $997)
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/webgenie-built-in-traffic.webp" alt="LearnX App" class="mx-auto d-block img-fluid ">
            </div>
         </div>
         <div class="row mt20 mt-md80">
            <div class="col-12">
               <div class="border-line"></div>
            </div>
         </div>
         <div class="row mt20 mt-md80 align-items-center">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-28 f-md-38 lh140 w700 text-center white-clr need-head">
               LearnX Mobile Optimizer
               </div>
               <div class="f-20 f-md-22 w400 lh140 mt20 black-clr">
                  No need to optimize anything yourself.
               </div>
               <div class="f-20 f-md-22 w400 lh140 mt20 black-clr">
               Let AI take care of that too.
               </div>
               <div class="f-20 f-md-22 w700 lh140 mt10 red-clr">
                  (Worth $997)
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/webgenie-mobile-optimizer.webp" alt="LearnX Mobile Optimizer" class="mx-auto d-block img-fluid ">
            </div>
         </div>
         <div class="row mt20 mt-md80">
            <div class="col-12">
               <div class="border-line"></div>
            </div>
         </div>
         <div class="row mt20 mt-md80 align-items-center">
            <div class="col-12 col-md-6">
               <div class="f-28 f-md-38 lh140 w700 text-center white-clr need-head">
               LearnX Mobile EDITION
               </div>
               <div class="f-20 f-md-22 w400 lh140 mt20 black-clr">
               This will allow you to also operate LearnX, even from your mobile phone…<br>
               Whether it’s an Android, iPhone, or tablet, it will work…
               </div>
               <div class="f-20 f-md-22 w700 lh140 mt10 red-clr">
                  (Worth $497)
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/webgenie-mobile-edition.webp" alt="LearnX Mobile EDITION" class="mx-auto d-block img-fluid ">
            </div>
         </div>
         <div class="row mt20 mt-md80">
            <div class="col-12">
               <div class="border-line"></div>
            </div>
         </div>
         <div class="row mt20 mt-md80 align-items-center">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-28 f-md-38 lh140 w700 text-center white-clr need-head">
               Training videos
               </div>
               <div class="f-20 f-md-22 w400 lh140 mt20 black-clr">
               There is NOTHING missing in this training… <br>
               Everything you need to know is explained in IMMENSE details
               </div>
               <div class="f-20 f-md-22 w700 lh140 mt10 red-clr">
                  (Worth $997)
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/training-videos.webp" alt="Training videos" class="mx-auto d-block img-fluid ">
            </div>
         </div>
         <div class="row mt20 mt-md80">
            <div class="col-12">
               <div class="border-line"></div>
            </div>
         </div>
         <div class="row mt20 mt-md80 align-items-center">
            <div class="col-12 col-md-6">
               <div class="f-28 f-md-38 lh140 w700 text-center white-clr need-head">
               World-class support
               </div>
               <div class="f-20 f-md-22 w400 lh140 mt20 black-clr">
               Have a question? Just reach out to us and our team will do their best to fix your problem in no time
               </div>
               <div class="f-20 f-md-22 w700 lh140 mt10 red-clr">
               (Worth A LOT)
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/world-class-support.webp" alt="World-class support" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </section>

   <!-- CTA Section Start -->
   <div class="cta-section-white">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-38 w700 lh140 text-center white-clr">
                  Get LearnX And Save $293 Now <br class="d-none d-md-block"> 
               </div>
               <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                  <span class="w700">Just Pay Once</span> Instead Of a <s>Monthly Fee</s>
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md50">
            <div class="col-md-7">
            <div class="cta-link-btn mt20">
                     <a href="#buynow" >&gt;&gt;&gt; Get Instant Access To LearnX &lt;&lt;&lt;</a>
                  </div>
               <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/compaitable-gurantee1.webp" alt="Compaitable Gurantee" class="mx-auto d-block img-fluid mt20 mt-md30">
            </div>
            <div class="col-md-5 mt20 mt-md0">
               <div class="countdown-container">
                  <div class="f-20 f-md-24 w400 lh140 text-center white-clr mb10">
                     <span class="w700 yellow-clr">Hurry up!</span>  Price Increases In
                  </div>
                  <div class="countdown counter-white text-center">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 smmltd">Days</span> 
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-45 timerbg oswald">46&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-45 timerbg oswald">28</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> 
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- CTA Section End -->

   <!--Limited Section Start -->
   <div class="limited-time-sec">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-20 f-md-24 w700 text-center white-clr align-items-center flex-column flex-md-row justify-content-center">
                  <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/caution-icon.webp" class="img-fluid mr-md5 mb5 mb-md0" alt="Caution Icon">
                  Your $2,475.34 Bonuses Package Expires When Timer Hits ZERO 
                  <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/caution-icon.webp" class="img-fluid ml-md5 mb5 mb-md0" alt="Caution Icon">
               </div>
            </div>
         </div>
      </div>
   </div>
   <!--Limited Section End -->
     
   <div class="white-section">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-28 f-md-50 w700 lh140 text-center text-md-start black-clr red-line">
               But There Is More…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20 mt-md30">
               By getting access to LearnX
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  You will immediately unlock access to our custom-made bonuses…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               We created this pack exclusively for LearnX
               </div>
               <div class="f-20 f-md-22 w600 lh140 text-center text-md-start black-clr mt20">
                  Our goal was simple…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  Give fast action takers the advantage…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               By giving them whatever they need to achieve 10x the results…
               In half the time…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               Here is exactly what you will get:
               </div>
            </div>
            <div class="col-12 col-md-6 order-md-1 mt20 mt-md0">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/better-man.webp" alt="Better Man" class="mx-auto d-block img-fluid">
            </div>
         </div>
      </div>
   </div>

   <div class="grey-section">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-head f-28 f-md-48 w600 lh140 text-center white-clr">
                  But That's Not All
               </div>
               <div class="f-20 f-md-22 w500 lh140 black-clr mt20">
                  In addition, we have several bonuses for those who want to take action <br class="d-none d-md-block">
                  today and start profiting from this opportunity.
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md80">
            <div class="col-12 col-md-6">
               <div class="">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/bonus1.webp" alt="Bonus 1" class="img-fluid d-block ">
                  <div class="mt20 f-20 f-md-24 w700 lh140 black-clr">
                  Exclusive Special Training
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 black-clr mt15">
                  Elevate your LearnX journey with our complimentary training session. Uncover insider tips, strategies, and hacks to maximize the potential of AI-Boosted Courses. Don't miss this exclusive opportunity to supercharge your e-learning success. Enroll now and transform your knowledge into profit!
                  </div>
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/bonus.webp" alt="Bonus Box" class="mx-auto d-block img-fluid ">
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md80">
            <div class="col-12 col-md-6 order-md-2">
               <div class="">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/bonus2.webp" alt="Bonus 2" class="img-fluid d-block ">
                  <div class="mt20 f-20 f-md-24 w700 lh140 black-clr">
                  Instant Course Streamer ( Value $247 )
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 black-clr mt15">
                  Transform the way you deliver knowledge with Instant Course Streamer! Seamlessly integrate this powerful tool with LearnX to provide your audience with real-time access to your AI-boosted courses. Say goodbye to waiting times, as your learners can now stream their lessons instantly, enhancing engagement and satisfaction.
                  </div>
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/bonus.webp" alt="Bonus Box" class="mx-auto d-block img-fluid ">
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md80">
            <div class="col-12 col-md-6">
               <div class="">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/bonus3.webp" alt="Bonus 3" class="img-fluid d-block ">
                  <div class="mt20 f-20 f-md-24 w700 lh140 black-clr">
                  Course Conversion Mastery ( Value $229 )
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 black-clr mt15">
                  Elevate your LearnX experience with Course Conversion Mastery bonuses! Unlock a rapid content toolkit, a proven sales funnel blueprint, a traffic boost accelerator, and an AI integration mastery guide. Plus, gain exclusive access to the LearnX Insider Community. Supercharge your course creation and marketing efforts for unparalleled success! 
                  </div>
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/bonus.webp" alt="Bonus Box" class="mx-auto d-block img-fluid ">
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md80">
            <div class="col-12 col-md-6 order-md-2">
               <div class="">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/bonus4.webp" alt="Bonus 4" class="img-fluid d-block ">
                  <div class="mt20 f-20 f-md-24 w700 lh140 black-clr">
                  Sales Funnel Mastery ( Value $247 ) 
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 black-clr mt15">
                  Maximize LearnX's potential with our Sales Funnel Mastery bonus! Craft irresistible offers, optimize landing pages, and master persuasive copywriting to turn your AI-boosted courses into a revenue-generating powerhouse. Elevate your e-learning business with this essential guide to sales funnel success!
                  </div>
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/bonus.webp" alt="Bonus Box" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <div class="white-section">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-6">
               <div class="f-28 f-md-50 w700 lh140 text-center text-md-start black-clr">
               Your Future Starts Today…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20 mt-md30">
               Forget all about half-done apps that’s on the market right now….
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               Now you have the option to get on the ground floor of something massive
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               No more wasting your time and money. On stuff that just doesn’t work…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               this is your chance to finally jump on something that has been proven to work…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               Not just for us…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               But for HUNDREDS of beta-testers…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               With %100 success rate…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               We’re confident to say that LearnX is the BEST app on the market right now…
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/future-start.webp" alt="Future Start" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <div class="grey-section">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-50 w700 lh140 text-center black-clr">
               Ofcourse You Are Wondering…  <br class="d-none d-md-block"><span class="red-clr">How Much Does It Cost…</span>
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md80">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr">
                  Look, I'm gonna be honest with you…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  We can charge anything we want for LearnX
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  Even $997 a month…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  And it will be a no-brainer…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  After all, our beta testers make so much more than that…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  And on top of that, there is no work for you to do.
               </div>
               <div class="f-20 f-md-22 w600 lh140 text-center text-md-start black-clr mt20">
                  LearnX does it all…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  But I make enough money from using LearnX
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  So I don’t need to charge you that
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  I use it on a daily basis…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  So I don’t see why not I shouldn’t give you access to it…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  So, for a limited time only…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  I’m willing to give you full access to LearnX
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  For a fraction of the price
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  Less than the price of a cheap dinner
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               Which is enough for me to cover the cost of the servers running LearnX
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/costs.webp" alt="Costs" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <div class="white-section">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-6 ">
               <div class="f-28 f-md-50 w700 lh140 text-center text-md-start black-clr">
               But You Have To Be Fast…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20 mt-md30">
               The last thing on earth I want is to get LearnX saturated…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               So, sadly I’ll have to limit the number of licenses I can give…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  After that, I'm raising the price to <span class="w600 red-clr">$997 monthly.</span> 
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  And trust me, that day will come very…VERY SOON.
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  So you better act quickly and grab your copy while you can.
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/money-scale.webp" alt="Money Scale" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <!-- CTA Section Start -->
   <div class="cta-section-white">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-38 w700 lh140 text-center white-clr">
                  Get LearnX And Save $293 Now <br class="d-none d-md-block"> 
               </div>
               <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                  <span class="w700">Just Pay Once</span> Instead Of a <s>Monthly Fee</s>
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md50">
            <div class="col-md-7">
            <div class="cta-link-btn mt20">
                     <a href="#buynow" >&gt;&gt;&gt; Get Instant Access To LearnX &lt;&lt;&lt;</a>
                  </div>
               <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/compaitable-gurantee1.webp" alt="Compaitable Gurantee" class="mx-auto d-block img-fluid mt20 mt-md30">
            </div>
            <div class="col-md-5 mt20 mt-md0">
               <div class="countdown-container">
                  <div class="f-20 f-md-24 w400 lh140 text-center white-clr mb10">
                     <span class="w700 yellow-clr">Hurry up!</span>  Price Increases In
                  </div>
                  <div class="countdown counter-white text-center">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 smmltd">Days</span> 
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-45 timerbg oswald">46&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-45 timerbg oswald">28</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> 
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- CTA Section End -->

   <!--Limited Section Start -->
   <div class="limited-time-sec">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-20 f-md-24 w700 text-center white-clr align-items-center flex-column flex-md-row justify-content-center">
                  <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/caution-icon.webp" class="img-fluid mr-md5 mb5 mb-md0" alt="Caution Icon">
                  Your $2,475.34 Bonuses Package Expires When Timer Hits ZERO 
                  <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/caution-icon.webp" class="img-fluid ml-md5 mb5 mb-md0" alt="Caution Icon">
               </div>
            </div>
         </div>
      </div>
   </div>
   <!--Limited Section End -->

   <div class="grey-section">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-6">
               <div class="f-28 f-md-50 w700 lh140 text-center text-md-start black-clr decide-line">
               Hesitant?
               </div>
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/decide-line.webp" alt="Red Line" class="d-block img-fluid ">
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20 mt-md30">
                  Listen…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  It's really simple…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  If you're sick of all the BS that is going on the market right now…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  If you're worried about inflation and the prices that are going up like there is no tomorrow
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               LearnX IS for you  <span class="w600">PERIOD</span> 
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  It was designed to be the life-changing system that you've been looking for…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  Right now, you have the option to change your life With just a few clicks
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  We created this for people exactly like you. To help them finally break through.
               </div>
               <div class="f-20 f-md-22 w600 lh140 text-center text-md-start red-clr mt20">
                  Worried what will happen if it didn't work for you?
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/confused-girl.webp" alt="Confused Girl" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <div class="white-section">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-24 f-md-38 w400 lh140 text-center black-clr">
               We Will Pay You To Fail With LearnX
               </div>
               <div class="f-28 f-md-45 w700 lh140 text-center black-clr mt10">
                  Our 30 Days Iron Clad <span class="need-head white-clr">Money Back Guarantee</span> 
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md80">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr">
                  We trust our app blindly…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  We know it works, after all we been using it for a year… 
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  And not just us… 
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  But hey, I know you probably don’t know me… and you may be hesitant… 
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  And I understand that. A bit of skepticism is always healthy… 
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  But I can help… 
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  Here is the deal, get access to LearnX now…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  Use it, and enjoy its features to the fullest… 
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  And if for any reason you don’t think LearnX is worth its weight in gold…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  Just send us a message to our 24/7 customer support… 
               </div>
               <div class="f-20 f-md-22 w600 lh140 text-center text-md-start black-clr mt20">
                  And we will refund every single penny back to you… 
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  No questions asked… Not just that… 
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  We will send you a bundle of premium software as a gift for wasting your time.
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  Worst case scenario, you get LearnX and don’t make any money
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                 <u> You will still get extra bundle of premium software for trying it out. </u>
               </div>
            </div>
            <div class="col-12 col-md-6 order-md-1 mt20 mt-md0">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/money-back-guarantee.webp" alt="Money Back Guarantee" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <!-- CTA Section Start -->
   <div class="cta-section-white">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-38 w700 lh140 text-center white-clr">
                  Get LearnX And Save $293 Now <br class="d-none d-md-block"> 
               </div>
               <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                  <span class="w700">Just Pay Once</span> Instead Of a <s>Monthly Fee</s>
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md50">
            <div class="col-md-7">
            <div class="cta-link-btn mt20">
                     <a href="#buynow" >&gt;&gt;&gt; Get Instant Access To LearnX &lt;&lt;&lt;</a>
                  </div>
               <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/compaitable-gurantee1.webp" alt="Compaitable Gurantee" class="mx-auto d-block img-fluid mt20 mt-md30">
            </div>
            <div class="col-md-5 mt20 mt-md0">
               <div class="countdown-container">
                  <div class="f-20 f-md-24 w400 lh140 text-center white-clr mb10">
                     <span class="w700 yellow-clr">Hurry up!</span>  Price Increases In
                  </div>
                  <div class="countdown counter-white text-center">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 smmltd">Days</span> 
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-45 timerbg oswald">46&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-45 timerbg oswald">28</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> 
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- CTA Section End -->

   <!--Limited Section Start -->
   <div class="limited-time-sec">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-20 f-md-24 w700 text-center white-clr align-items-center flex-column flex-md-row justify-content-center">
                  <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/caution-icon.webp" class="img-fluid mr-md5 mb5 mb-md0" alt="Caution Icon">
                  Your $2,475.34 Bonuses Package Expires When Timer Hits ZERO 
                  <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/caution-icon.webp" class="img-fluid ml-md5 mb5 mb-md0" alt="Caution Icon">
               </div>
            </div>
         </div>
      </div>
   </div>
   <!--Limited Section End -->

   <div class="white-section" id="buynow">
      <div class="container">
         <div class="row">
            <!-- <div class="col-12">
               <div class="f-28 f-md-45 lh140 w700 text-center black-clr">
               Here Is What You’re About To Access
               </div>
            </div> -->
            <div class="col-12 col-md-9 mx-auto text-center">
               <div class="table-wrap">
                  <div class="table-head">
                     <div class="f-28 f-md-45 lh140 white-clr w700">
                        Today with LearnX, You’re Getting :
                     </div>
                  </div>
                  <div class=" ">
                     <ul class="table-list pl0 f-18 f-md-20 w400 text-center lh140 black-clr mt20 mt-md40">
                        <li>AI Technology Creates High Quality Course For You - Just Enter A Keyword - <span class="w600">That’s PRICELESS</span> </li>
                        <li>Create Auto Updating Websites with 100% Original, Search Engine Friendly Courses For Any Niche - <span class="w600">That’s PRICELESS</span> </li>
                        <li>LearnX App To Start Your E-Learning Empire Having Marketplace, Blog & Members Area within Minutes - <span class="w600">Worth $997/Month</span> </li>
                        <li>AI Course Generator To Fill your New Website With Hundreds of AI Courses in Any Niche in Just 1 Click - <span class="w600">Worth $697</span> </li>
                        <li>Comes With 50,000+ SEO Optimized Done For You Smoking Hot AI Courses - <span class="w600"> Worth $997</span></li>
                        <li>LearnX Built-In Traffic - <span class="w600">Worth $997</span> </li>
                        <li>100%  Mobile Optimizer - <span class="w600">Worth $697</span> </li>
                        <li>Live AI ChatBots That Will Handle All Customer Support For You - <span class="w600">Worth $297</span> </li>
                        <li>Accept Payments With All Top Platforms – PayPal, Stripe, JVZoo, ClickBank, Warrior Plus, PayDotCom Etc - <span class="w600">Worth $297</span> </li>
                        <li>LearnX Mobile EDITION To Operate LearnX from Mobile Whether Android, iPhone, or Tablet - <span class="w600">Worth $497</span> </li>
                        <li>Manage Leads With Inbuilt Lead Management System - <span class="w600">Worth $257</span> </li>
                        <li>100% Newbie Friendly <span class="w600">(Beyond A Value)</span> </li>
                        <li>Step-By-Step Training Videos - <span class="w600">Worth $997</span> </li>
                        <li>Round-The-Clock World-Class Support <span class="w600">(Priceless – It’s Worth A LOT)</span> </li>
                        <li>Commercial License - <span class="w600"> That's Priceless</span></li>
                        <li class="headline my15">BONUSES WORTH $2,475.34 FREE!!! </li>
                        <li>Bonus Course Package with Resell Rights</li>
                        <li>Niche Graphics Set</li>
                        <li>Video Creation Suite to Create Promo Videos and Courses</li>
                        <li>Social Media Traffic Streams</li>
                     </ul>
                  </div>
                  <div class="table-btn ">
                     <div class="f-22 f-md-28 w400 lh140 black-clr">
                        Total Value of Everything
                     </div>
                     <div class="f-26 f-md-36 w700 lh140 black-clr mt12">
                        YOU GET TODAY:
                     </div>
                     <div class="f-28 f-md-70 w700 red-clr mt12 l140">
                        $6,745.00
                     </div>
                     <div class="f-22 f-md-28 w400 lh140 black-clr mt20">
                        For Limited Time Only, Grab It For:
                     </div>
                     <div class="f-21 f-md-40 w700 lh140 red-clr mt20">
                       <del>$97 Monthly</del> 
                     </div>
                     <div class="f-24 f-md-34 w700 lh140 black-clr mt20">
                        Today, <span class="blue-clr">Just $17 One-Time</span> 
                     </div>
                     <div class="cta-link-btn mt20">
                     <a href="https://warriorplus.com/o2/buy/fbmn7d/vvw6tk/fx48dn"  id="buyButton" class="cta-link-btn mt20"> Get Instant Access To LearnX</a>
                  </div>
                  </div>
                  <div class="table-border-content mt20">
                     <div class="tb-check d-flex align-items-center f-18">
                        <label class="checkbox inline">
                           <img src="https://assets.clickfunnels.com/templates/listhacking-sales/images/arrow-flash-small.gif" alt="" style="margin-top:-5px;">
                           <input type="checkbox" id="check" onclick="myFunction()" style="width:16px !important; height:16px !important; margin-top:5px;">
                        </label>
                        <label class="checkbox inline">
                           <h5>Yes, Add LearnX + $1K/Day DFY AI Campaigns + AI Blogs!</h5>
                        </label>
                     </div>
                     <div class="p20">
                        <p class="f-18 text-center text-md-start"><b class="red-clr underline">Limited to the First 30 Buyers:</b> Activate Our $1,067 Per Day Campaigns And Get 5 DFY Profitable Campaigns + Auto-Updating AI Blogs + Put Your LearnX Site On A Faster Server + Exclusive higher priority support!
                        <br> <br><span class="w700">(97% of Customers Grab This and Witness Immediate Results)</span></p>
                        <p class="f-18 text-center text-md-start"> Get it now for just<span class="green-clr w600">  $9.97 One Time Only</span> <span class="black-clr"><strike>(Normally $247)</strike></span>
                     
                     </p>
                        
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>

   <!-- CTA Section Start -->
   <!--<div class="cta-section-white">-->
   <!--   <div class="container">-->
   <!--      <div class="row">-->
   <!--         <div class="col-12">-->
   <!--            <div class="f-28 f-md-38 w700 lh140 text-center white-clr">-->
   <!--               Get LearnX And Save $293 Now <br class="d-none d-md-block"> -->
   <!--            </div>-->
   <!--            <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">-->
   <!--               <span class="w700">Just Pay Once</span> Instead Of a <s>Monthly Fee</s>-->
   <!--            </div>-->
   <!--         </div>-->
   <!--      </div>-->
   <!--      <div class="row align-items-center mt20 mt-md50">-->
   <!--         <div class="col-md-7">-->
   <!--            <a href="#buynow" class="cta-link-btn">>>> Get Instant Access To LearnX <<<</a>-->
   <!--            <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/compaitable-gurantee1.webp" alt="Compaitable Gurantee" class="mx-auto d-block img-fluid mt20 mt-md30">-->
   <!--         </div>-->
   <!--         <div class="col-md-5 mt20 mt-md0">-->
   <!--            <div class="countdown-container">-->
   <!--               <div class="f-20 f-md-24 w400 lh140 text-center white-clr mb10">-->
   <!--                  <span class="w700 yellow-clr">Hurry up!</span>  Price Increases In-->
   <!--               </div>-->
   <!--               <div class="countdown counter-white text-center">-->
   <!--                  <div class="timer-label text-center">-->
   <!--                     <span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 smmltd">Days</span> -->
   <!--                  </div>-->
   <!--                  <div class="timer-label text-center">-->
   <!--                     <span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> -->
   <!--                  </div>-->
   <!--                  <div class="timer-label text-center timer-mrgn">-->
   <!--                     <span class="f-31 f-md-45 timerbg oswald">46&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> -->
   <!--                  </div>-->
   <!--                  <div class="timer-label text-center ">-->
   <!--                     <span class="f-31 f-md-45 timerbg oswald">28</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> -->
   <!--                  </div>-->
   <!--               </div>-->
   <!--            </div>-->
   <!--         </div>-->
   <!--      </div>-->
   <!--   </div>-->
   <!--</div>-->
   <!-- CTA Section End -->

   <!--Limited Section Start -->
   <!--<div class="limited-time-sec">-->
   <!--   <div class="container">-->
   <!--      <div class="row">-->
   <!--         <div class="col-12 text-center">-->
   <!--            <div class="f-20 f-md-24 w700 text-center white-clr align-items-center flex-column flex-md-row justify-content-center">-->
   <!--               <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/caution-icon.webp" class="img-fluid mr-md5 mb5 mb-md0" alt="Caution Icon">-->
   <!--               Your $2,475.34 Bonuses Package Expires When Timer Hits ZERO -->
   <!--               <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/caution-icon.webp" class="img-fluid ml-md5 mb5 mb-md0" alt="Caution Icon">-->
   <!--            </div>-->
   <!--         </div>-->
   <!--      </div>-->
   <!--   </div>-->
   <!--</div>-->
   <!--Limited Section End -->


   <div class="remeber-section">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-28 f-md-45 w700 lh140 text-center text-md-start black-clr">
                  Remember…
               </div>
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/remember-line.webp" alt="Red Line" class="d-block img-fluid">
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  You don't have a lot of time. We will remove this huge discount once we hit our limit.
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  After that, you will have to pay $997/mo for it.
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  While it will still be worth it.
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  Why pay more, when you can just pay a small one-time fee today and get access to everything?
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1 mt20 mt-md0">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/remember.webp" alt="Remember" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <div class="white-section">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-6">
               <div class="f-28 f-md-45 w700 lh140 text-center text-md-start black-clr">
                  So, Are you ready yet?
               </div>
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/ready-line.webp" alt="Red Line" class="d-block mx-auto img-fluid mt5 ">
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  You reading this far into the page means one thing..
               </div>
               <div class="f-20 f-md-22 w600 lh140 text-center text-md-start black-clr mt20">
                  You're interested.
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  The good news is, you're just minutes away from your breakthrough
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  Just imagine waking up every day to thousands of dollars in your account Instead of ZERO.
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  The bad news is, it will sell out FAST So you need to act now.
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
                  Ready to join us?
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/ready.webp" alt="Ready" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <!-- CTA Section Start -->
   <div class="cta-section-white">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-38 w700 lh140 text-center white-clr">
                  Get LearnX And Save $293 Now <br class="d-none d-md-block"> 
               </div>
               <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                  <span class="w700">Just Pay Once</span> Instead Of a <s>Monthly Fee</s>
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md50">
            <div class="col-md-7">
            <div class="cta-link-btn mt20">
            <a href="https://warriorplus.com/o2/buy/fbmn7d/vvw6tk/fx48dn"><img src="https://warriorplus.com/o2/btn/fn200011000/fbmn7d/vvw6tk/373772" class="img-fluid d-block mx-auto"></a> 
                  </div>
               <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/compaitable-gurantee1.webp" alt="Compaitable Gurantee" class="mx-auto d-block img-fluid mt20 mt-md30">
            </div>
            <div class="col-md-5 mt20 mt-md0">
               <div class="countdown-container">
                  <div class="f-20 f-md-24 w400 lh140 text-center white-clr mb10">
                     <span class="w700 yellow-clr">Hurry up!</span>  Price Increases In
                  </div>
                  <div class="countdown counter-white text-center">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 smmltd">Days</span> 
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-45 timerbg oswald">46&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-45 timerbg oswald">28</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> 
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- CTA Section End -->

   <!--Limited Section Start -->
   <div class="limited-time-sec">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-20 f-md-24 w700 text-center white-clr align-items-center flex-column flex-md-row justify-content-center">
                  <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/caution-icon.webp" class="img-fluid mr-md5 mb5 mb-md0" alt="Caution Icon">
                  Your $2,475.34 Bonuses Package Expires When Timer Hits ZERO 
                  <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/caution-icon.webp" class="img-fluid ml-md5 mb5 mb-md0" alt="Caution Icon">
               </div>
            </div>
         </div>
      </div>
   </div>
   <!--Limited Section End -->

   <div class="inside-section">
      <div class="container">
         <div class="row">
            <div class="col-12 col-md-12">
               <div class="f-28 f-md-45 lh140 w700 text-center black-clr">
                  We'll see you inside,
               </div>
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/orange-line.webp" alt="Orange Line" class="mx-auto d-block img-fluid mt5 ">
            </div>
         </div>
         <div class="row mt20 mt-md70">
            <div class="col-12 col-md-8 mx-auto">
               <div class="row justify-content-between">
                  <div class="col-md-5">
                     <div class="inside-box">
                        <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/pranshu-gupta.webp" alt="Pranshu Gupta" class="mx-auto d-block mx-auto ">
                        <div class="f-22 f-md-28 lh140 w600 text-center black-clr mt20">
                           Pranshu Gupta
                        </div>
                     </div>
                  </div>
                  <div class="col-md-5 mt20 mt-md0">
                     <div class="inside-box">
                        <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/bizomart-team.webp" alt="Bizomart Team" class="mx-auto d-block mx-auto ">
                        <div class="f-22 f-md-28 lh140 w600 text-center black-clr mt20">
                           Bizomart Team
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="row mt20 mt-md70">
            <div class="col-12">
               <div class="f-20 f-md-22 lh140 w400 black-clr text-center text-md-start">
                  <span class="w600">PS:</span>  If you act now, you will instantly receive [bonuses] worth over <span class="w600">$7,458.34</span> This bonus is designed specifically to help you get 10x the results, in half the time required
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr text-center text-md-start mt20">
                 <span class="w600">PPS:</span>There is nothing else required for you to start earning with LearnX No hidden fees, no monthly costs, nothing.
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr text-center text-md-start mt20">
                 <span class="w600">PPPS:</span>  Remember, you're protected by our money-back guarantee If you fail, Not only will we refund your money.
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr text-center text-md-start mt20">
               We will send you <span class="red-clr">a bundle of premium software as a gift for wasting your time</span> 
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr text-center text-md-start mt20">
                 <span class="w600">PPPPS:</span>  If you put off the decision till later, you will end up paying $997/mo Instead of just a one-time flat fee.
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr text-center text-md-start mt20">
                  And why pay more, when you can pay less?
               </div>
            </div>
         </div>
      </div>
   </div>

   <div class="faq-section ">
      <div class="container ">
         <div class="row ">
            <div class="col-12 f-28 f-md-45 lh140 w600 text-center black-clr ">
               Frequently Asked <span class="orange-clr ">Questions</span>
            </div>
            <div class="col-12 mt20 mt-md30 ">
               <div class="row ">
                  <div class="col-md-9 mx-auto col-12 ">
                     <div class="faq-list ">
                        <div class="f-md-22 f-20 w600 black-clr lh140 ">
                           Do I need any experience to get started? 
                        </div>
                        <div class="f-18 w400 lh140 mt10 text-start ">
                           None, all you need is just an internet connection. And you're good to go
                        </div>
                     </div>
                     <div class="faq-list mt20">
                        <div class="f-md-22 f-20 w600 black-clr lh140 ">
                           Is there any monthly cost?
                        </div>
                        <div class="f-18 w400 lh140 mt10 text-start ">
                           Depends, If you act now, NONE. 
                           But if you wait, you might end up paying $997/month
                           <br><br>
                           It's up to you. 
                        </div>
                     </div>
                     <div class="faq-list mt20">
                        <div class="f-md-22 f-20 w600 black-clr lh140 ">
                           How long does it take to make money?
                        </div>
                        <div class="f-18 w400 lh140 mt10 text-start ">
                           Our average member made their first sale the same day they got access to LearnX.
                        </div>
                     </div>
                     <div class="faq-list mt20">
                        <div class="f-md-22 f-20 w600 black-clr lh140 ">
                           Do I need to purchase anything else for it to work?
                        </div>
                        <div class="f-18 w400 lh140 mt10 text-start ">
                           Nop, LearnX is the complete thing. 
                           <br><br>
                           You get everything you need to make it work. Nothing is left behind. 
                        </div>
                     </div>
                     <div class="faq-list mt20">
                        <div class="f-md-22 f-20 w600 black-clr lh140 ">
                           What if I failed?
                        </div>
                        <div class="f-18 w400 lh140 mt10 text-start ">
                           While that is unlikely, we removed all the risk for you.
                           <br><br>
                           If you tried LearnX and failed, we will refund you every cent you paid
                           <br><br>
                           And send you $300 on top of that just to apologize for wasting your time.
                        </div>
                     </div>
                     <div class="faq-list mt20">
                        <div class="f-md-22 f-20 w600 black-clr lh140 ">
                           How can I get started?
                        </div>
                        <div class="f-18 w400 lh140 mt10 text-start ">
                           Awesome, I like your excitement, All you have to do is click any of the buy buttons on the page, and secure your copy of LearnX at a one-time fee
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md50 ">
               <div class="f-22 f-md-28 w600 black-clr px0 text-center lh140 ">If you have any query, simply reach to us at <a href="mailto:support@bizomart.com " class="kapblue ">support@bizomart.com</a>
               </div>
            </div>
         </div>
      </div>
   </div>


   <!-- CTA Section Start -->
   <div class="cta-section-white">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-45 f-md-45 lh140 w700 white-clr final-shape">
                  FINAL CALL
               </div>
               <div class="f-22 f-md-36 lh140 w600 white-clr mt20 mt-md30">
                  You Still Have the Opportunity To Raise Your Game… <br> Remember, It's Your Make-or-Break Time!
               </div>
               <div class="f-28 f-md-38 w700 lh140 text-center white-clr mt20 mt-md30">
                  Get LearnX And Save $293 Now <br class="d-none d-md-block"> 
               </div>
               <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                  <span class="w700">Just Pay Once</span> Instead Of a <s>Monthly Fee</s>
               </div>
               <div class="row">
                  <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center">
                  <div class="cta-link-btn mt20">
                     <a href="#buynow" >&gt;&gt;&gt; Get Instant Access To LearnX &lt;&lt;&lt;</a>
                     <!-- <a href="https://warriorplus.com/o2/buy/fbmn7d/vvw6tk/ns89jq"><img src="https://warriorplus.com/o2/btn/fn200011000/fbmn7d/vvw6tk/373792" class="d-block img-fluid mx-auto mt20 mt-md30"></a> -->
                  </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30">
                  <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/compaitable-gurantee1.webp" alt="Compaitable Gurantee" class="mx-auto d-block img-fluid">
               </div>
               <!--<div class="col-12 mt20 mt-md30">-->
               <!--    <a href="https://warriorplus.com/o2/buy/hb06pf/wmp7ql/pzsb6y"><img src="https://warriorplus.com/o2/btn/fn200011000/hb06pf/wmp7ql/352565" class="d-block img-fluid mx-auto mt20 mt-md30"></a>-->
               <!--</div>-->
            </div>
         </div>
      </div>
   </div>
   <!-- CTA Section End -->

   <!--Limited Section Start -->
   <div class="limited-time-sec">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-20 f-md-24 w700 text-center white-clr align-items-center flex-column flex-md-row justify-content-center">
                  <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/caution-icon.webp" class="img-fluid mr-md5 mb5 mb-md0" alt="Caution Icon">
                  Your $2,475.34 Bonuses Package Expires When Timer Hits ZERO 
                  <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/caution-icon.webp" class="img-fluid ml-md5 mb5 mb-md0" alt="Caution Icon">
               </div>
            </div>
         </div>
      </div>
   </div>
   <!--Limited Section End -->

      
      <div class="footer-section">
         <div class="container ">
            <div class="row">
               <div class="col-12 text-center">
               <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 484.58 200.84" style="max-height:75px;">
                        <defs>
                            <style>
                                .cls-1,.cls-4{fill:#fff;}
                                .cls-2,.cls-3,.cls-4{fill-rule:evenodd;}
                                .cls-2{fill:url(#linear-gradient);}
                                .cls-3{fill:url(#linear-gradient-2);}
                                .cls-5{fill:url(#linear-gradient-3);}
                            </style>
                            <linearGradient id="linear-gradient" x1="-154.67" y1="191.86" x2="249.18" y2="191.86" gradientTransform="matrix(1.19, -0.01, 0.05, 1, 178.46, -4.31)" gradientUnits="userSpaceOnUse">
                                <stop offset="0" stop-color="#ead40c"/>
                                <stop offset="1" stop-color="#ff00e9"/>
                            </linearGradient>
                            <linearGradient id="linear-gradient-2" x1="23.11" y1="160.56" x2="57.61" y2="160.56" gradientTransform="matrix(1, 0, 0, 1, 0, 0)" xlink:href="#linear-gradient"/>
                            <linearGradient id="linear-gradient-3" x1="384.49" y1="98.33" x2="483.38" y2="98.33" gradientTransform="matrix(1, 0, 0, 1, 0, 0)" xlink:href="#linear-gradient"/>
                        </defs>
                        <g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1">
                            <path class="cls-1" d="M112.31,154.3q-11.65,0-20.1-4.87a32.56,32.56,0,0,1-13-13.82q-4.53-9-4.54-21.11a46.06,46.06,0,0,1,4.57-21A34.29,34.29,0,0,1,92,79.37a36.11,36.11,0,0,1,19.31-5.06,39.65,39.65,0,0,1,13.54,2.29,31,31,0,0,1,11.3,7.09,33.32,33.32,0,0,1,7.75,12.18,49.23,49.23,0,0,1,2.82,17.57V119H83.26v-12.3h46A19.87,19.87,0,0,0,127,97.38a16.6,16.6,0,0,0-6.18-6.48,17.59,17.59,0,0,0-9.21-2.37,17.88,17.88,0,0,0-9.83,2.7,18.81,18.81,0,0,0-6.58,7.06,20.17,20.17,0,0,0-2.4,9.56v10.74a25.15,25.15,0,0,0,2.47,11.57,17.42,17.42,0,0,0,6.91,7.37,20.52,20.52,0,0,0,10.39,2.55,21.62,21.62,0,0,0,7.22-1.14,15.71,15.71,0,0,0,5.59-3.35,14.11,14.11,0,0,0,3.59-5.5L146,132a26.44,26.44,0,0,1-6.13,11.77,29.72,29.72,0,0,1-11.52,7.77A43.74,43.74,0,0,1,112.31,154.3Z"/>
                            <path class="cls-1" d="M184.49,154.35a31.78,31.78,0,0,1-13.24-2.65,21.26,21.26,0,0,1-9.28-7.84,22.86,22.86,0,0,1-3.41-12.81A21.92,21.92,0,0,1,161,120.2a18.9,18.9,0,0,1,6.61-6.86,33.85,33.85,0,0,1,9.46-3.9,77.76,77.76,0,0,1,10.92-2q6.81-.7,11-1.28a15.91,15.91,0,0,0,6.18-1.82,4.25,4.25,0,0,0,1.94-3.86v-.3q0-5.7-3.38-8.83T194,88.28q-6.71,0-10.62,2.92a14.55,14.55,0,0,0-5.27,6.91l-17-2.42a27.51,27.51,0,0,1,6.66-11.83,29.46,29.46,0,0,1,11.35-7.16,43.73,43.73,0,0,1,14.83-2.39,47.89,47.89,0,0,1,11.14,1.31,31.6,31.6,0,0,1,10.14,4.31,22.12,22.12,0,0,1,7.39,8.14q2.81,5.15,2.8,12.87v51.85H207.84V142.14h-.61a22.1,22.1,0,0,1-4.66,6,22.35,22.35,0,0,1-7.52,4.49A30,30,0,0,1,184.49,154.35Zm4.74-13.42a19.7,19.7,0,0,0,9.53-2.19,16,16,0,0,0,6.23-5.83,15,15,0,0,0,2.19-7.91v-9.13a8.72,8.72,0,0,1-2.9,1.31,45.56,45.56,0,0,1-4.56,1.06c-1.68.3-3.35.57-5,.8l-4.29.61a32,32,0,0,0-7.32,1.81A12.29,12.29,0,0,0,178,125a9.76,9.76,0,0,0,1.82,13.39A16,16,0,0,0,189.23,140.93Z"/>
                            <path class="cls-1" d="M243.8,152.79V75.31h17.7V88.23h.81a19.36,19.36,0,0,1,7.29-10.37,20,20,0,0,1,11.83-3.66c1,0,2.14,0,3.4.13a23.83,23.83,0,0,1,3.15.38V91.5a21,21,0,0,0-3.65-.73,38.61,38.61,0,0,0-4.82-.32,18.47,18.47,0,0,0-8.95,2.14,15.94,15.94,0,0,0-6.23,5.93,16.55,16.55,0,0,0-2.27,8.72v45.55Z"/>
                            <path class="cls-1" d="M318.4,107.39v45.4H300.14V75.31h17.45V88.48h.91a22,22,0,0,1,8.55-10.34q5.86-3.84,14.55-3.83a27.58,27.58,0,0,1,14,3.43,23.19,23.19,0,0,1,9.28,9.93,34.22,34.22,0,0,1,3.26,15.79v49.33H349.87V106.28c0-5.17-1.34-9.23-4-12.15s-6.37-4.39-11.07-4.39a17,17,0,0,0-8.5,2.09,14.67,14.67,0,0,0-5.8,6A20,20,0,0,0,318.4,107.39Z"/>
                            <path class="cls-2" d="M314.05,172.88c12.69-.14,27.32,2.53,40.82,2.61l-.09,1c1.07.15,1-.88,2-.78,1.42-.18,0,1.57,1.82,1.14.54-.06.08-.56-.32-.68.77,0,2.31.17,3,1.26,1.3,0-.9-1.34.85-.89,6,1.63,13.39,0,20,3.2,11.64.72,24.79,2.38,38,4.27,2.85.41,5.37,1.11,7.57,1.05,2-.05,5.68.78,8,1.08,2.53.34,5.16,1.56,7.13,1.65-1.18-.05,5.49-.78,4.54.76,5-.72,10.9,1.67,15.94,1.84.83-.07.69.53.67,1,1.23-1.17,3.32.56,4.28-.56.25,2,2.63-.48,3.3,1.61,2.23-1.39,4.83.74,7.55,1.37,1.91.44,5.29-.21,5.55,2.14-2.1-.13-5.12.81-11.41-1.09,1,.87-3.35.74-3.39-.64-1.81,1.94-6.28-1-7.79,1.19-1.26-.41,0-.72-.64-1.35-5,.55-8.09-1.33-12.91-1.56,2.57,2.44,6.08,3.16,10.06,3.22,1.28.15,0,.74,1,1.39,1.37-.19.75-.48,1.26-1.17,5.37,2.28,15.36,2.35,21,4.91-4.69-.63-11.38,0-15.15-2.09A148,148,0,0,1,441.58,196c-.53-.1-1.81.12-.7-.71-1.65.73-3.41.1-5.74-.22-37.15-5.15-80.47-7.78-115.75-9.76-39.54-2.22-76.79-3.22-116.44-2.72-61.62.78-119.59,7.93-175.21,13.95-5,.55-10,1.49-15.11,1.47-1.33-.32-2.46-1.91-1.25-3-2-.38-2.93.29-5-.15a9.83,9.83,0,0,1-1.25-3,13.73,13.73,0,0,1,6.42-2.94c.77-.54.12-.8.15-1.6a171,171,0,0,1,45.35-8.58c26.43-1.1,53.68-4.73,79.64-6,6.25-.3,11.72.06,18.41.14,8.31.1,19.09-1,29.19-.12,6.25.54,13.67-.55,21.16-.56,39.35-.09,71.16-.23,112.28,2C317.24,171.93,315.11,173.81,314.05,172.88Zm64.22,16.05-1.6-.2C376.2,190,378.42,190.26,378.27,188.93Z"/>
                            <polygon class="cls-3" points="52.57 166.06 57.61 151.99 23.11 157.23 32.36 169.12 52.57 166.06"/>
                            <polygon class="cls-4" points="44.69 183.24 51.75 168.35 33.86 171.06 44.69 183.24"/>
                            <path class="cls-4" d="M.12,10.15l22,145.06,35.47-5.39-22-145a2.6,2.6,0,0,0,0-.4C35,.76,26.62-.95,16.81.54S-.52,6.16,0,9.77A2.62,2.62,0,0,0,.12,10.15Z"/>
                            <path class="cls-5" d="M433.68,79.45l21-36.33h27.56L448.48,97.51l34.9,56H456l-22-37.76-21.94,37.76H384.49l34.9-56L385.72,43.12h27.35Z"/>
                        </g>
                            </g>
                    </svg>
                  <div class="f-14 f-md-16 w400 mt20 lh150 white-clr text-center" style="color:#7d8398;"><span class="w600">Note:</span> This site is not a part of the Facebook website or Facebook Inc. Additionally, this site is NOT endorsed by Facebook in any way. FACEBOOK & INSTAGRAM are the trademarks of FACEBOOK, Inc.<br><br>
                     
                  </div>
                  
                  <div class=" mt20 mt-md40 white-clr"> <script type="text/javascript" src="https://warriorplus.com/o2/disclaimer/fbmn7d"
                  defer></script><div class="wplus_spdisclaimer f-17 w300 mt20 lh140 white-clr text-center"></div></div>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-14 f-md-16 w400 lh150 white-clr text-xs-center">Copyright © LearnX 2023</div>
                  <ul class="footer-ul w400 f-14 f-md-16 white-clr text-center text-md-right">
                     <li><a href="https://support.bizomart.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://getlearnx.com/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://getlearnx.com/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://getlearnx.com/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://getlearnx.com/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://getlearnx.com/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://getlearnx.com/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                    
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!-- Exit Popup and Timer -->
      <?php include('timer.php'); ?>	  
      <!-- Exit Popup and Timer End -->
      <script> 
function myFunction() {
  var checkBox = document.getElementById("check");
  var buybutton = document.getElementById("buyButton");
  if (checkBox.checked == true){
 
	    buybutton.getAttribute("href");
	    buybutton.setAttribute("href","https://warriorplus.com/o2/buy/fbmn7d/vvw6tk/ns89jq");
	} else {
		 buybutton.getAttribute("href");
		 buybutton.setAttribute("href","https://warriorplus.com/o2/buy/fbmn7d/vvw6tk/fx48dn");
	  }
}
</script>

<script type="text/javascript" src="assets/js/ouibounce.min.js"></script>
<div id="ouibounce-modal" style="z-index:9999999; display:none;">
   <div class="underlay"></div>
   <div class="modal-wrapper" style="display:block;">
      <div class="modal-bg">
		   <button type="button" class="close" onclick="document.getElementById('ouibounce-modal').style.display = 'none';">
			   X 
		   </button>
		   <div class="model-header">
            <div class="d-flex justify-content-center align-items-center gap-3">
               <img src="https://cdn.dotcompaltest.com/uploads/launches/learnX/special/hold.png" alt="" class="img-fluid d-block ">
			   <div>
            <div class="f-md-50 f-20 w700 lh130 white-clr">
               WAIT! HOLD ON!!
			   </div>
            <div class="f-md-18 f-16 w700 lh130 white-clr mt10 mt-md0">
              Don't Leave Empty Handed
			   </div>
         </div>
      </div>
   </div>
	
   <div class="col-12 for-padding">
		<div class="copun-line f-md-22 f-16 w500 lh130 text-center black-clr mt10">
			Use Coupon Code <span class="w700 timer-clr"> "LearnX"</span>  to Get <span class="w700 timer-clr">$3 Off</span> Instantly.
		</div>
		<div class="mt-md20 mt10 text-center ">
		   <a href="https://warriorplus.com/o2/buy/fbmn7d/vvw6tk/fx48dn" id="buyButton" class="cta-link-btn1">Grab SPECIAL Discount Now!
			<br>
			<span class="f-12 white-clr w500 text-uppercase">Act Now! Limited to First 100 Buyers Only.</span> </a>
		</div>
		
      <div class="table-border-content mt20">
         <div class="tb-check d-flex align-items-center f-14">
            <label class="checkbox inline">
               <img src="https://assets.clickfunnels.com/templates/listhacking-sales/images/arrow-flash-small.gif" alt="">
               <input type="checkbox" id="checked"  onclick="myFunctionPopup()">
            </label>
            <label class="checkbox inline">
               <h5>Yes, Add LearnX + $1K/Day DFY AI Campaigns + AI Blogs!</h5>
            </label>
         </div>
         <div class="p20">
            <p class="f-14 text-center text-md-start"><b class="red-clr underline">LIMITED TIME OFFER:</b> Activate Our $1,067 Per Day Campaigns And Get 5 DFY Profitable Campaigns + Auto-Updating AI Blogs + Put Your LearnX Site On A Faster Server + Exclusive higher priority support!<br><b>(97% of Customers Grab This and Witness Immediate Results)</b></p>
            <p class="f-18 text-center text-md-start">Get it now for just <span class="green-clr w600">  $9.97 One Time Only</span> (Normally $247) </p>
         </div>
      </div>
      <div style="height:20px;"></div>
   </div>
</div>
    
<script type="text/javascript">
   var noob = $('.countdown').length;
   var stimeInSecs;
   var tickers;

   function timerBegin(seconds) {   
      if(seconds>=0)  {
         stimeInSecs = parseInt(seconds);           
         showRemaining();
      }
      //tickers = setInterval("showRemaining()", 1000);
   }

   function showRemaining() {

      var seconds = stimeInSecs;
         
      if (seconds > 0) {         
         stimeInSecs--;       
      } else {        
         clearInterval(tickers);       
         //timerBegin(59 * 60); 
      }

      var days = Math.floor(seconds / 86400);

      seconds %= 86400;
      
      var hours = Math.floor(seconds / 3600);
      
      seconds %= 3600;
      
      var minutes = Math.floor(seconds / 60);
      
      seconds %= 60;


      if (days < 10) {
         days = "0" + days;
      }
      if (hours < 10) {
         hours = "0" + hours;
      }
      if (minutes < 10) {
         minutes = "0" + minutes;
      }
      if (seconds < 10) {
         seconds = "0" + seconds;
      }
      var i;
      var countdown = document.getElementsByClassName('countdown');

      for (i = 0; i < noob; i++) {
         countdown[i].innerHTML = '';
      
         if (days) {
            countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-24 f-md-42 timerbg oswald">' + days + '</span><br> &nbsp;&nbsp;&nbsp;&nbsp;<span class="f-14 f-md-14 w400 smmltd">Days</span> </div>';
         }
      
         countdown[i].innerHTML += '<div class="timer-label text-center">&nbsp;&nbsp;&nbsp;&nbsp;<span class="f-24 f-md-42 timerbg oswald">' + hours + '</span><br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="f-14 f-md-14 w400 smmltd">Hours</span> </div>';
      
         countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"> &nbsp;&nbsp;&nbsp;&nbsp;<span class="f-24 f-md-42 timerbg oswald">' + minutes + '</span><br> &nbsp;&nbsp;&nbsp;&nbsp;<span class="f-14 f-md-14 w400 smmltd">Mins</span> </div>';
      
         countdown[i].innerHTML += '<div class="timer-label text-center "> &nbsp;&nbsp;&nbsp;&nbsp; <span class="f-24 f-md-42 timerbg oswald">' + seconds + '</span><br><span class="f-14 f-md-14 w400 smmltd">Sec</span> </div>';
      }
   
   }

const givenTimestamp = 'Mon Dec 4 2023 20:30:00 GMT+0530'
const durationTime = 60;

const date = new Date(givenTimestamp);
const givenTime = date.getTime() / 1000;
const currentTime = new Date().getTime()



let cnt;
const matchInterval = setInterval(() => {
   if(Math.round(givenTime) === Math.round(new Date().getTime() /1000)){
   cnt = durationTime * 60;
   counter();
   clearInterval(matchInterval);
   }else if(Math.round(givenTime) < Math.round(new Date().getTime() /1000)){
      const timeGap = Math.round(new Date().getTime() /1000) - Math.round(givenTime)
      let difference = (durationTime * 60) - timeGap;
      if(difference >= 0){
         cnt = difference
         localStorage.mavasTimer = cnt;

      }else{
         
         difference = difference % (durationTime * 60);
         cnt = (durationTime * 60) + difference
         localStorage.mavasTimer = cnt;

      }

      counter();
      clearInterval(matchInterval);
   }
},1000)





function counter(){
   if(parseInt(localStorage.mavasTimer) > 0){
      cnt = parseInt(localStorage.mavasTimer);
   }
   cnt -= 1;
   localStorage.mavasTimer = cnt;
   if(localStorage.mavasTimer !== NaN){
      timerBegin(parseInt(localStorage.mavasTimer));
   }
   if(cnt>0){
      setTimeout(counter,1000);
  }else if(cnt === 0){
   cnt = durationTime * 60 
   counter()
  }
}

</script>
  <!--- timer end-->
<script type="text/javascript">
   var _ouibounce = ouibounce(document.getElementById('ouibounce-modal'),{
   aggressive: true, //Making this true makes ouibounce not to obey "once per visitor" rule
   });
</script>
   </body>
</html>


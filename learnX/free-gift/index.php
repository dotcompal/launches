<!Doctype html>
<html lang="en">
   <head>
   <head>
    <title>Aicademy | Free Gift</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=9">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">

    <meta name="title" content="Aicademy | Free Gift">
    <meta name="description" content="Sell Courses, Products & Agency Services With Done-for-You Website In Next 7 Minutes Flat.">
    <meta name="keywords" content="Aicademy | Free Gift">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta property="og:image" content="https://www.aicademy.live/free-gift/thumbnail.png">
    <meta name="language" content="English">
    <meta name="revisit-after" content="1 days">
    <meta name="author" content="Pranshu Gupta">

    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:title" content="Aicademy | Free Gift">
    <meta property="og:description" content="Sell Courses, Products & Agency Services With Done-for-You Website In Next 7 Minutes Flat.">
    <meta property="og:image" content="https://www.aicademy.live/free-gift/thumbnail.png">

    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:title" content="Aicademy | Free Gift">
    <meta property="twitter:description" content="Sell Courses, Products & Agency Services With Done-for-You Website In Next 7 Minutes Flat.">
    <meta property="twitter:image" content="https://www.aicademy.live/free-gift/thumbnail.png">

   <!-- Font-Family -->
    <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
    <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    <!-- Start Editor required -->
    <link rel="stylesheet" href="https://cdn.oppyotest.com/launches/sellero/common_assets/css/bootstrap.min.css" type="text/css">
	<!-- <link rel="stylesheet" type="text/css" href="assets/css/bonus-style.css"> -->
    <link rel="stylesheet" href="assets/css/style.css" type="text/css">
    <link rel="stylesheet" href="assets/css/aweberform.css" type="text/css">
    <script src="https://cdn.oppyotest.com/launches/sellero/common_assets/js/jquery.min.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- End -->
</head>
<body>
	<a href="#" id="scroll" style="display: block;"><span></span></a>
	<?php
		if(!isset($_GET['afflink'])){
		$_GET['afflink'] = 'https://aicademy.live/special/';
		$_GET['name'] = 'Pranshu Gupta';      
		}
	?>

	<div class="header-sec" id="product">
        <div class="container">
            <div class="row">
				<div class="col-12 text-center">
                    <div class="white-clr f-20 f-md-32">
                        Free Gifts by <span class="w600 yellow-clr">Pranshu Gupta's</span> for SuperVIP Customers
                    </div>
                    <div class="uy mt20 mt-md40">
                        <div class="f-20 f-md-24 lh160 w500 black-clr">
                            <span class="f-20 f-md-24 purple-clr w600">Congratulations!</span> you have also <span class="purple-clr w600">unlocked Special Exclusive coupon <br class="d-none d-md-block">  "ADMIN0FF" </span> for my Lightning Fast Software <span class="purple-clr w600">"Aicademy"</span> launch on <br class="d-none d-md-block"> <span class="purple-clr w600">22th June 2023  @ 11:00 AM EST</span>
                        </div>
                        <div class="f-20 f-md-24 lh130 w600 mt20 gn-box">Good News :</div>
                        <div class="f-20 f-md-24 lh160 w400 mt20 mt-md30 black-clr">
                            If you want any other <b>BONUSES</b> that are relevant for your business <br class="d-none d-md-block"> or <b>any niche</b> to increase more of your profits then, <b>please feel free <br class="d-none d-md-block"> to email</b> us at <a href="https://support.bizomart.com/hc/en-us" target="_blank" class="purple-clr">https://support.bizomart.com/hc/en-us</a>
                        </div>
                    </div>
                </div>
               	<div class="col-12 mt-md50 mt20 text-center">
				   <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 644.49 120" style="max-height:55px">
                        <defs>
                          <style>
                            .cls-1 {
                              fill: #fff;
                            }
                            .cls-2 {
                              fill: #0af;
                            }
                          </style>
                        </defs>
                        <g id="Layer_1-2" data-name="Layer 1">
                          <g>
                            <g>
                              <circle class="cls-2" cx="79.02" cy="43.5" r="7.25"></circle>
                              <circle class="cls-2" cx="70.47" cy="78.46" r="5.46"></circle>
                              <path class="cls-2" d="m158.03,45.28l-33.74,13.31v36.4l-45.27,16.86-27.9-10.4c-4.17,3.37-7.04,6.74-8.15,10.11-1.01,3.22-.07,6.21,2.46,8.42-5.44-1.46-8-5.25-6.96-9.76,1.24-3.62,4.79-7.68,10-12.08h.01c3.95-3.31,8.84-6.84,14.46-10.5,2.04,1.67,4.68,2.68,7.53,2.68,6.56,0,11.88-5.32,11.88-11.88s-5.32-11.88-11.88-11.88-11.88,5.32-11.88,11.88c0,1.13.15,2.21.46,3.25-8.01,5.01-14.83,10.36-19.57,15.44l-5.75-2.14v-7.66c-7.84,4.92-11.84,11.06-5.99,16.99-5.17-1.64-7.69-4.49-7.69-8.05,0-4.83,4.67-10.99,13.69-17.26l35.41-23.23c2.71,2.18,6.13,3.48,9.87,3.48,8.7,0,15.76-7.06,15.76-15.76s-7.06-15.76-15.76-15.76-15.76,7.06-15.76,15.76c0,1.59.23,3.12.68,4.57l-30.2,19.53v-9L0,45.28,79.02,0l79.01,45.28Z"></path>
                              <path class="cls-2" d="m149.38,68.32v-19.62l-1.51.6v19.03c-1.66.37-2.9,1.96-2.9,3.85v11.69h7.31v-11.69c0-1.9-1.24-3.48-2.9-3.85Z"></path>
                            </g>
                            <g>
                              <g>
                                <path class="cls-2" d="m178.03,98.08l22.25-71.1h19.3l22.25,71.1h-17.01l-4.36-16.9h-21.7l-4.36,16.9h-16.36Zm25.74-35.99l-1.74,6.54h15.16l-1.64-6.54c-.95-3.56-1.91-7.34-2.89-11.34-.98-4-1.95-7.85-2.89-11.56h-.44c-.87,3.78-1.76,7.65-2.67,11.61-.91,3.96-1.87,7.73-2.89,11.29Z"></path>
                                <path class="cls-2" d="m256.22,36.03c-2.69,0-4.87-.76-6.54-2.29-1.67-1.53-2.51-3.56-2.51-6.11s.83-4.58,2.51-6.11c1.67-1.53,3.85-2.29,6.54-2.29s4.87.76,6.54,2.29c1.67,1.53,2.51,3.56,2.51,6.11s-.84,4.58-2.51,6.11c-1.67,1.53-3.85,2.29-6.54,2.29Zm-7.96,62.05v-54.09h16.03v54.09h-16.03Z"></path>
                              </g>
                              <g>
                                <path class="cls-1" d="m299.89,99.38c-5.02,0-9.54-1.11-13.58-3.33-4.03-2.22-7.23-5.45-9.6-9.71-2.36-4.25-3.54-9.36-3.54-15.32s1.31-11.16,3.93-15.38c2.62-4.22,6.07-7.43,10.36-9.65,4.29-2.22,8.9-3.33,13.85-3.33,3.34,0,6.31.55,8.89,1.64,2.58,1.09,4.89,2.47,6.92,4.14l-7.52,10.36c-2.55-2.11-4.98-3.16-7.31-3.16-3.85,0-6.92,1.38-9.21,4.14-2.29,2.76-3.44,6.51-3.44,11.23s1.15,8.38,3.44,11.18c2.29,2.8,5.18,4.2,8.67,4.2,1.74,0,3.45-.38,5.13-1.15,1.67-.76,3.2-1.69,4.58-2.78l6.33,10.47c-2.69,2.33-5.6,3.98-8.72,4.96-3.13.98-6.18,1.47-9.16,1.47Z"></path>
                                <path class="cls-1" d="m339.81,99.38c-4.94,0-8.87-1.58-11.78-4.74-2.91-3.16-4.36-7.03-4.36-11.61,0-5.67,2.4-10.1,7.2-13.3,4.8-3.2,12.54-5.34,23.23-6.43-.15-2.4-.86-4.31-2.13-5.73-1.27-1.42-3.4-2.13-6.38-2.13-2.25,0-4.54.44-6.87,1.31-2.33.87-4.8,2.07-7.42,3.6l-5.78-10.58c3.42-2.11,7.07-3.82,10.96-5.13,3.89-1.31,7.94-1.96,12.16-1.96,6.91,0,12.21,2,15.92,6,3.71,4,5.56,10.14,5.56,18.43v30.97h-13.09l-1.09-5.56h-.44c-2.25,2.04-4.67,3.69-7.25,4.96-2.58,1.27-5.4,1.91-8.45,1.91Zm5.45-12.43c1.82,0,3.4-.42,4.74-1.25,1.34-.83,2.71-1.94,4.09-3.33v-9.49c-5.67.73-9.6,1.87-11.78,3.44-2.18,1.56-3.27,3.4-3.27,5.51,0,1.74.56,3.04,1.69,3.87,1.13.84,2.63,1.25,4.53,1.25Z"></path>
                                <path class="cls-1" d="m402.95,99.38c-6.69,0-12.05-2.53-16.09-7.58-4.03-5.05-6.05-11.98-6.05-20.77,0-5.89,1.07-10.96,3.22-15.21,2.14-4.25,4.94-7.51,8.4-9.76,3.45-2.25,7.07-3.38,10.85-3.38,2.98,0,5.49.51,7.52,1.53,2.04,1.02,3.96,2.4,5.78,4.14l-.66-8.29v-18.43h16.03v76.45h-13.09l-1.09-5.34h-.44c-1.89,1.89-4.11,3.47-6.65,4.74-2.55,1.27-5.13,1.91-7.74,1.91Zm4.14-13.09c1.74,0,3.33-.36,4.74-1.09,1.42-.73,2.78-2,4.09-3.82v-22.14c-1.38-1.31-2.85-2.22-4.42-2.73-1.56-.51-3.07-.76-4.53-.76-2.55,0-4.8,1.22-6.76,3.65-1.96,2.44-2.94,6.23-2.94,11.4s.85,9.21,2.56,11.72c1.71,2.51,4.13,3.76,7.25,3.76Z"></path>
                                <path class="cls-1" d="m470.34,99.38c-5.16,0-9.81-1.13-13.96-3.38-4.14-2.25-7.42-5.49-9.81-9.71-2.4-4.22-3.6-9.31-3.6-15.27s1.22-10.94,3.65-15.16c2.43-4.22,5.62-7.47,9.54-9.76,3.93-2.29,8.03-3.44,12.32-3.44,5.16,0,9.43,1.15,12.81,3.44,3.38,2.29,5.92,5.38,7.63,9.27,1.71,3.89,2.56,8.31,2.56,13.25,0,1.38-.07,2.74-.22,4.09-.15,1.35-.29,2.34-.44,3h-32.39c.73,3.93,2.36,6.82,4.91,8.67,2.54,1.85,5.6,2.78,9.16,2.78,3.85,0,7.74-1.2,11.67-3.6l5.34,9.71c-2.76,1.89-5.85,3.38-9.27,4.47-3.42,1.09-6.73,1.64-9.92,1.64Zm-12-34.24h19.52c0-2.98-.71-5.43-2.13-7.36-1.42-1.93-3.73-2.89-6.92-2.89-2.47,0-4.69.86-6.65,2.56-1.96,1.71-3.24,4.27-3.82,7.69Z"></path>
                                <path class="cls-1" d="m502.62,98.08v-54.09h13.09l1.09,6.98h.44c2.25-2.25,4.65-4.2,7.2-5.83,2.54-1.64,5.6-2.45,9.16-2.45,3.85,0,6.96.78,9.32,2.34,2.36,1.56,4.23,3.8,5.62,6.71,2.4-2.47,4.94-4.6,7.63-6.38,2.69-1.78,5.82-2.67,9.38-2.67,5.82,0,10.09,1.95,12.81,5.83,2.73,3.89,4.09,9.21,4.09,15.98v33.59h-16.03v-31.52c0-3.93-.53-6.61-1.58-8.07-1.05-1.45-2.74-2.18-5.07-2.18-2.69,0-5.78,1.74-9.27,5.23v36.53h-16.03v-31.52c0-3.93-.53-6.61-1.58-8.07-1.05-1.45-2.74-2.18-5.07-2.18-2.69,0-5.74,1.74-9.16,5.23v36.53h-16.03Z"></path>
                                <path class="cls-1" d="m602.07,119.23c-1.6,0-3-.11-4.2-.33-1.2-.22-2.34-.47-3.44-.76l2.84-12.21c.51.07,1.09.2,1.74.38.65.18,1.27.27,1.85.27,2.69,0,4.76-.65,6.22-1.96,1.45-1.31,2.54-3.02,3.27-5.13l.76-2.84-20.83-52.67h16.14l7.74,23.23c.8,2.47,1.53,4.98,2.18,7.52.65,2.55,1.34,5.16,2.07,7.85h.44c.58-2.54,1.18-5.11,1.8-7.69.62-2.58,1.25-5.14,1.91-7.69l6.54-23.23h15.38l-18.76,54.63c-1.67,4.51-3.53,8.31-5.56,11.4-2.04,3.09-4.49,5.4-7.36,6.92-2.87,1.53-6.45,2.29-10.74,2.29Z"></path>
                              </g>
                            </g>
                          </g>
                        </g>
                      </svg>
               	</div>
				<div class="col-12 mt-md30 mt20 f-md-50 f-28 w400 text-center white-clr lh140">
				3 Steps AI-App Create <span class="w700 yellow-clr">Online-Learning Platform For Us, Prefill Them With AI Courses…</span> 
				</div>
				<div class="col-12 mt-md15 mt15 text-center ">
                  <div class="f-22 f-md-28 w700 lh140 white-clr">
				  Then Market Them To over 3.5 Millions Customers…
                  </div>
               </div>
			   <div class="col-12 mt20 text-center">
                  <div class="f-22 f-md-28 w700 lh140 white-clr post-heading">
				  Resulting In $872.36 Daily For Us…
                  </div>
               </div>
			   <div class="col-12 mt20 text-center">
                  <div class="f-20 f-md-22 w500 lh140 white-clr">
				  Without Us Creating Anything….
                  </div>
               </div>
			   <div class="col-12 mt15 text-center">
                  <div class="f-20 f-md-22 w600 lh140 yellow-clr">
				  No Ads | No Upfront Cost | No Experience | No BS
                  </div>
               </div>
				<div class="col-12 white-clr text-center">
					<div class="f-md-36 f-28 lh160 w700 mt30 white-clr">
						Download Your Free Gifts Below
					</div>
					<div class="mt10 f-20 f-md-32 lh160 ">
						<span class="tde">Enjoy Your Extra Bonus Below</span>
					</div>
				</div>
				<div class="col-12 mt20 mt-md50">
					<img src="assets/images/product-image.webp" class="img-fluid d-block mx-auto img-animation" alt="Proudly Presenting...">
				</div>
			</div>

			<!-- Bonus-1 -->
			<div class="row mt20 mt-md100 align-items-center">
               	<div class="col-md-6">
                  	<div class="f-22 f-md-28 lh140 w700 white-clr">
                     	<div class="option-design mr10 mr-md20">Bonus 1</div>
                     	License File Ninja
                  	</div>
                  	<div class="f-20 f-md-22 w500 lh140 white-clr mt20 mt-md30">
					  How To Create Professional License File PDFs In A Snap Using This Newbie Friendly Software!
                  	</div>
                  	<div class="f-20 f-md-22 w500 lh140 white-clr mt20">
					  This is a PC desktop software that is easy to use. It allows you to crank out professional PDF license files with only a few clicks of your mouse.
                  	</div>
                 	<div class="f-20 f-md-22 w500 lh140 white-clr mt20">
					 This saves you hours of time doing this manually so you can get back to doing the things that make you the most money.
                  	</div>
					<div class="f-md-22 w700 lh160 d-btn mt20 mt-md30">
                       <a href="https://shorturl.at/cDRVZ" target="_blank">Download Now 🎁</a>
                    </div>
               	</div>
               	<div class="col-md-6 mt20 mt-md0">
                  	<img src="assets/images/bonus1.webp" alt="Bonus 1" class="mx-auto d-block img-fluid">
               	</div>
            </div>

			<!-- Bonus-2 -->
			<div class="row mt20 mt-md100 align-items-center">
               	<div class="col-md-6 order-md-2">
                  	<div class="f-22 f-md-28 lh140 w700 white-clr">
                     	<div class="option-design mr10 mr-md20">Bonus 2</div>
                     	Elite Presentation Kit 
                  	</div>
                  	<div class="f-20 f-md-22 w500 lh140 white-clr mt20 mt-md30">
					  Stunning & Professional Presentation That Can Easily Impress Your Clients/Prospects, Increase Your Credibility, And Close Any Deals. Even IF Your Never Create A Presentation Before!
                  	</div>
                  	<div class="f-20 f-md-22 w500 lh140 white-clr mt20">
					  It's not a secret that first impression is THE BEST impression. And winning a business deal is that critical part of your business, but does it ALWAYS depend on what you sell?
                  	</div>
                 	<div class="f-20 f-md-22 w500 lh140 white-clr mt20">
					 Not necessarity. But... Presentation matters! With growing competition in every industry, you must stand first and ahead of others to survive and run any business. 
                  	</div>
					<div class="f-md-22 w700 lh160 d-btn mt20 mt-md30">
                       <a href="https://shorturl.at/OPT08" target="_blank">Download Now 🎁</a>
                    </div>
               	</div>
               	<div class="col-md-6 mt20 mt-md0 order-md-1">
                  	<img src="assets/images/bonus2.webp" alt="Bonus 2" class="mx-auto d-block img-fluid">
               	</div>
            </div>

			<!-- Bonus-3 -->
			<div class="row mt20 mt-md100 align-items-center">
               	<div class="col-md-6">
                  	<div class="f-22 f-md-28 lh140 w700 white-clr">
                     	<div class="option-design mr10 mr-md20">Bonus 3</div>
                     	Keyword Harvester 
                  	</div>
                  	<div class="f-20 f-md-22 w500 lh140 white-clr mt20 mt-md30">
					  Attention: Online Marketers, Here's A Simple Solution To Create Your Pay-Per-Click Campaigns and Perform Market Research Faster...
                  	</div>
                  	<div class="f-20 f-md-22 w500 lh140 white-clr mt20">
					  Discover How You Can Want to download Keyword Harvester with private label rights? If you need PLR Keyword Harvester Software with different terms, then please submit a PLR product request.
                  	</div>
					<div class="f-md-22 w700 lh160 d-btn mt20 mt-md30">
                       <a href="https://tinyurl.com/yc2kptr6" target="_blank">Download Now 🎁</a>
                    </div>
               	</div>
               	<div class="col-md-6 mt20 mt-md0">
                  	<img src="assets/images/bonus3.webp" alt="Bonus 3" class="mx-auto d-block img-fluid">
               	</div>
            </div>

			<!-- Bonus-4 -->
			<div class="row mt20 mt-md100 align-items-center">
               	<div class="col-md-6 order-md-2">
                  	<div class="f-22 f-md-28 lh140 w700 white-clr">
                     	<div class="option-design mr10 mr-md20">Bonus 4</div>
                     	Funtastic Fonts 
                  	</div>
                  	<div class="f-20 f-md-22 w500 lh140 white-clr mt20 mt-md30">
					  If you are having a hard time finding just the right font for your projects then this collection is a must.
                  	</div>
                  	<div class="f-20 f-md-22 w500 lh140 white-clr mt20">
					  "Funtastic Fonts" is a huge collection of over 6,500 TrueType fonts!
					  Sure you could do those projects using fonts like "Arial" or "Tahoma", but why not jazz up those same presentations with some super fonts.
                  	</div>
					<div class="f-md-22 w700 lh160 d-btn mt20 mt-md30">
                       <a href="https://tinyurl.com/5285sbyn" target="_blank">Download Now 🎁</a>
                    </div>
               	</div>
               	<div class="col-md-6 mt20 mt-md0 order-md-1">
                  	<img src="assets/images/bonus4.webp" alt="Bonus 4" class="mx-auto d-block img-fluid">
               	</div>
            </div>

			<!-- Bonus-5 -->
			<div class="row mt20 mt-md100 align-items-center">
               	<div class="col-md-6">
                  	<div class="f-22 f-md-28 lh140 w700 white-clr">
                     	<div class="option-design mr10 mr-md20">Bonus 5</div>
                     	YT Rank Analyzer 
                  	</div>
                  	<div class="f-20 f-md-22 w500 lh140 white-clr mt20 mt-md30">
					  Discover How to Dominate YouTube And Build MASSIVE Targeted Lists For FREE... By Using Software To Do ALL the Dirty Work!
                  	</div>
                  	<div class="f-20 f-md-22 w500 lh140 white-clr mt20">
					  YouTube is now the second largest search engine and is the thirds most visited website in the world. If you are not into YouTube Marketing, then you waste a huge opportunity to attract more traffic and leads to subscribe your list.
                  	</div>
					<div class="f-20 f-md-22 w500 lh140 white-clr mt20">
					Well, doing YouTube Marketing can also be technical and time-consuming but if you do the consistent hard-work, it will surely paid off. Plus this will get easier and easier as you go along.
                  	</div>
					<div class="f-md-22 w700 lh160 d-btn mt20 mt-md30">
                       <a href="https://tinyurl.com/2tep666a" target="_blank">Download Now 🎁</a>
                    </div>
               	</div>
               	<div class="col-md-6 mt20 mt-md0">
                  	<img src="assets/images/bonus5.webp" alt="Bonus 5" class="mx-auto d-block img-fluid">
               	</div>
            </div>

			<!-- Bonus-6 -->
			<div class="row mt20 mt-md100 align-items-center">
               	<div class="col-md-6 order-md-2">
                  	<div class="f-22 f-md-28 lh140 w700 white-clr">
                     	<div class="option-design mr10 mr-md20">Bonus 6</div>
                     	Elegant Press 
                  	</div>
                  	<div class="f-20 f-md-22 w500 lh140 white-clr mt20 mt-md30">
					  	Finally... Now You Really Can Create Your Own PROFESSIONAL & ATTRACTIVE Presentation in Just 30 Minutes or Less!
                  	</div>
                  	<div class="f-20 f-md-22 w500 lh140 white-clr mt20">
					  	Want to Create a Professional Presentation with Little Time and Skill? Now You Can Make YOUR Presentation Amazing in Seconds Without the Headache or Hassle of Learning PowerPoint! Create a Professional Presentation To Impress and Amaze Your Audience!
                  	</div>
					<div class="f-20 f-md-22 w500 lh140 white-clr mt20">
						A wise man once said that 'Presentation is Everything'. It's such a timeless quote because it’s true in so many areas of life.
                  	</div>
					<div class="f-md-22 w700 lh160 d-btn mt20 mt-md30">
                       <a href="https://tinyurl.com/yc4dmtjz" target="_blank">Download Now 🎁</a>
                    </div>
               	</div>
               	<div class="col-md-6 mt20 mt-md0 order-md-1">
                  	<img src="assets/images/bonus6.webp" alt="Bonus 6" class="mx-auto d-block img-fluid">
               	</div>
            </div>

			<!-- Bonus-7 -->
			<div class="row mt20 mt-md100 align-items-center">
               	<div class="col-md-6">
                  	<div class="f-22 f-md-28 lh140 w700 white-clr">
                     	<div class="option-design mr10 mr-md20">Bonus 7</div>
                     	125 Pro Header Templates 
                  	</div>
                  	<div class="f-20 f-md-22 w500 lh140 white-clr mt20 mt-md30">
					  125 high quality header image templates complete with PSD and blank jpg files - There Should Be Something For Everyone In This 125 Header Pack!
                  	</div>
                  	<div class="f-20 f-md-22 w500 lh140 white-clr mt20">
					  Want to download 125 Pro Header Templates with private label rights? If you need PLR 125 Pro Header Templates Graphics with different terms, then please submit a PLR product request.
                  	</div>
					<div class="f-md-22 w700 lh160 d-btn mt20 mt-md30">
                       <a href="https://tinyurl.com/fdduer55" target="_blank">Download Now 🎁</a>
                    </div>
               	</div>
               	<div class="col-md-6 mt20 mt-md0">
                  	<img src="assets/images/bonus7.webp" alt="Bonus 7" class="mx-auto d-block img-fluid">
               	</div>
            </div>

			<!-- Bonus-8 -->
			<div class="row mt20 mt-md100 align-items-center">
               	<div class="col-md-6 order-md-2">
                  	<div class="f-22 f-md-28 lh140 w700 white-clr">
                     	<div class="option-design mr10 mr-md20">Bonus 8</div>
                     	3D Box Templates 
                  	</div>
                  	<div class="f-20 f-md-22 w500 lh140 white-clr mt20 mt-md30">
					  3D Box Templates allow you to create great looking eBox covers in just a few simple steps; Upload template, Enter text, Set color and you're done!
                  	</div>
                  	<div class="f-20 f-md-22 w500 lh140 white-clr mt20">
					  This package comes with 5 eBox templates. Each template comes in 3 versions; with text, without text and with .psd source you you can make changes!
                  	</div>
					<div class="f-md-22 w700 lh160 d-btn mt20 mt-md30">
                       <a href="https://tinyurl.com/2rm2zyas" target="_blank">Download Now 🎁</a>
                    </div>
               	</div>
               	<div class="col-md-6 mt20 mt-md0 order-md-1">
                  	<img src="assets/images/bonus8.webp" alt="Bonus 8" class="mx-auto d-block img-fluid">
               	</div>
            </div>

			<!-- Bonus-9 -->
			<div class="row mt20 mt-md100 align-items-center">
               	<div class="col-md-6">
                  	<div class="f-22 f-md-28 lh140 w700 white-clr">     
                     	<div class="option-design mr10 mr-md20">Bonus 9</div>
                     	Evergreen Infographics Pack
                  	</div>
                  	<div class="f-20 f-md-22 w500 lh140 white-clr mt20 mt-md30">
					  One of the most important types of visuals for a business owners in the online arena are infographics.
                  	</div>
                  	<div class="f-20 f-md-22 w500 lh140 white-clr mt20">
					  Infographics are used to showcase statistics, to sell products, to advertise, promote, attract and connect better with your audience.
                  	</div>
					  <div class="f-20 f-md-22 w500 lh140 white-clr mt20">
					  It is the visual representation that you showcase to your potential customers and that sends a clear message to them! This is a collection of high quality, evergreen, powerful infographics that you can use for your ebooks, marketing materials, blog posts, product line, social media sites, and more. Non-transferable resell rights are included!
                  	</div>
					<div class="f-md-22 w700 lh160 d-btn mt20 mt-md30">
                       <a href="https://tinyurl.com/y5fuyafr" target="_blank">Download Now 🎁</a>
                    </div>
               	</div>
               	<div class="col-md-6 mt20 mt-md0">
                  	<img src="assets/images/bonus9.webp" alt="Bonus 9" class="mx-auto d-block img-fluid">
               	</div>
            </div>

			<!-- Bonus-10 -->
			<div class="row mt20 mt-md100 align-items-center">
               	<div class="col-md-6 order-md-2">
                  	<div class="f-22 f-md-28 lh140 w700 white-clr">
                     	<div class="option-design mr10 mr-md20">Bonus 10</div>
                     		Go Graphics 
                  	</div>
                  	<div class="f-20 f-md-22 w500 lh140 white-clr mt20 mt-md30">
					  You're About To Discover An Awfully Simple-But-Powerful System That Can Do All The E-Cover, CD & Membership Pass Works For You ... as easy as 123!
                  	</div>
                  	<div class="f-20 f-md-22 w500 lh140 white-clr mt20">
					  NO Waiting And NO Fancy Graphic Designing Gizmo, Knowledge & Experience Required - As Long As You Can Scroll And Click, You TOO Can Design Superb Covers For Your Digital Products!
                  	</div>
					<div class="f-20 f-md-22 w500 lh140 white-clr mt20">
					Introducing The Awfully-Simple-But-Superb Cover Designing System - 123 Go Graphics - Finally! You Can Now Churn Out Unlimited Instant HIGH Quality 3D Covers!
                  	</div>
					<div class="f-md-22 w700 lh160 d-btn mt20 mt-md30">
                       <a href="https://tinyurl.com/ycxvum36" target="_blank">Download Now 🎁</a>
                    </div>
               	</div>
               	<div class="col-md-6 mt20 mt-md0 order-md-1">
                  	<img src="assets/images/bonus10.webp" alt="Bonus 10" class="mx-auto d-block img-fluid">
               	</div>
            </div>
		</div>
	</div>

	<!--Footer Section Start -->
	<div class="footer-section">
         <div class="container ">
            <div class="row">
               <div class="col-12 text-center">
                  <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 644.49 120" style="max-height:55px">
                     <defs>
                       <style>
                         .cls-1 {
                           fill: #fff;
                         }
                   
                         .cls-2 {
                           fill: #0af;
                         }
                       </style>
                     </defs>
                     <g id="Layer_1-2" data-name="Layer 1">
                       <g>
                         <g>
                           <circle class="cls-2" cx="79.02" cy="43.5" r="7.25"></circle>
                           <circle class="cls-2" cx="70.47" cy="78.46" r="5.46"></circle>
                           <path class="cls-2" d="m158.03,45.28l-33.74,13.31v36.4l-45.27,16.86-27.9-10.4c-4.17,3.37-7.04,6.74-8.15,10.11-1.01,3.22-.07,6.21,2.46,8.42-5.44-1.46-8-5.25-6.96-9.76,1.24-3.62,4.79-7.68,10-12.08h.01c3.95-3.31,8.84-6.84,14.46-10.5,2.04,1.67,4.68,2.68,7.53,2.68,6.56,0,11.88-5.32,11.88-11.88s-5.32-11.88-11.88-11.88-11.88,5.32-11.88,11.88c0,1.13.15,2.21.46,3.25-8.01,5.01-14.83,10.36-19.57,15.44l-5.75-2.14v-7.66c-7.84,4.92-11.84,11.06-5.99,16.99-5.17-1.64-7.69-4.49-7.69-8.05,0-4.83,4.67-10.99,13.69-17.26l35.41-23.23c2.71,2.18,6.13,3.48,9.87,3.48,8.7,0,15.76-7.06,15.76-15.76s-7.06-15.76-15.76-15.76-15.76,7.06-15.76,15.76c0,1.59.23,3.12.68,4.57l-30.2,19.53v-9L0,45.28,79.02,0l79.01,45.28Z"></path>
                           <path class="cls-2" d="m149.38,68.32v-19.62l-1.51.6v19.03c-1.66.37-2.9,1.96-2.9,3.85v11.69h7.31v-11.69c0-1.9-1.24-3.48-2.9-3.85Z"></path>
                         </g>
                         <g>
                           <g>
                             <path class="cls-2" d="m178.03,98.08l22.25-71.1h19.3l22.25,71.1h-17.01l-4.36-16.9h-21.7l-4.36,16.9h-16.36Zm25.74-35.99l-1.74,6.54h15.16l-1.64-6.54c-.95-3.56-1.91-7.34-2.89-11.34-.98-4-1.95-7.85-2.89-11.56h-.44c-.87,3.78-1.76,7.65-2.67,11.61-.91,3.96-1.87,7.73-2.89,11.29Z"></path>
                             <path class="cls-2" d="m256.22,36.03c-2.69,0-4.87-.76-6.54-2.29-1.67-1.53-2.51-3.56-2.51-6.11s.83-4.58,2.51-6.11c1.67-1.53,3.85-2.29,6.54-2.29s4.87.76,6.54,2.29c1.67,1.53,2.51,3.56,2.51,6.11s-.84,4.58-2.51,6.11c-1.67,1.53-3.85,2.29-6.54,2.29Zm-7.96,62.05v-54.09h16.03v54.09h-16.03Z"></path>
                           </g>
                           <g>
                             <path class="cls-1" d="m299.89,99.38c-5.02,0-9.54-1.11-13.58-3.33-4.03-2.22-7.23-5.45-9.6-9.71-2.36-4.25-3.54-9.36-3.54-15.32s1.31-11.16,3.93-15.38c2.62-4.22,6.07-7.43,10.36-9.65,4.29-2.22,8.9-3.33,13.85-3.33,3.34,0,6.31.55,8.89,1.64,2.58,1.09,4.89,2.47,6.92,4.14l-7.52,10.36c-2.55-2.11-4.98-3.16-7.31-3.16-3.85,0-6.92,1.38-9.21,4.14-2.29,2.76-3.44,6.51-3.44,11.23s1.15,8.38,3.44,11.18c2.29,2.8,5.18,4.2,8.67,4.2,1.74,0,3.45-.38,5.13-1.15,1.67-.76,3.2-1.69,4.58-2.78l6.33,10.47c-2.69,2.33-5.6,3.98-8.72,4.96-3.13.98-6.18,1.47-9.16,1.47Z"></path>
                             <path class="cls-1" d="m339.81,99.38c-4.94,0-8.87-1.58-11.78-4.74-2.91-3.16-4.36-7.03-4.36-11.61,0-5.67,2.4-10.1,7.2-13.3,4.8-3.2,12.54-5.34,23.23-6.43-.15-2.4-.86-4.31-2.13-5.73-1.27-1.42-3.4-2.13-6.38-2.13-2.25,0-4.54.44-6.87,1.31-2.33.87-4.8,2.07-7.42,3.6l-5.78-10.58c3.42-2.11,7.07-3.82,10.96-5.13,3.89-1.31,7.94-1.96,12.16-1.96,6.91,0,12.21,2,15.92,6,3.71,4,5.56,10.14,5.56,18.43v30.97h-13.09l-1.09-5.56h-.44c-2.25,2.04-4.67,3.69-7.25,4.96-2.58,1.27-5.4,1.91-8.45,1.91Zm5.45-12.43c1.82,0,3.4-.42,4.74-1.25,1.34-.83,2.71-1.94,4.09-3.33v-9.49c-5.67.73-9.6,1.87-11.78,3.44-2.18,1.56-3.27,3.4-3.27,5.51,0,1.74.56,3.04,1.69,3.87,1.13.84,2.63,1.25,4.53,1.25Z"></path>
                             <path class="cls-1" d="m402.95,99.38c-6.69,0-12.05-2.53-16.09-7.58-4.03-5.05-6.05-11.98-6.05-20.77,0-5.89,1.07-10.96,3.22-15.21,2.14-4.25,4.94-7.51,8.4-9.76,3.45-2.25,7.07-3.38,10.85-3.38,2.98,0,5.49.51,7.52,1.53,2.04,1.02,3.96,2.4,5.78,4.14l-.66-8.29v-18.43h16.03v76.45h-13.09l-1.09-5.34h-.44c-1.89,1.89-4.11,3.47-6.65,4.74-2.55,1.27-5.13,1.91-7.74,1.91Zm4.14-13.09c1.74,0,3.33-.36,4.74-1.09,1.42-.73,2.78-2,4.09-3.82v-22.14c-1.38-1.31-2.85-2.22-4.42-2.73-1.56-.51-3.07-.76-4.53-.76-2.55,0-4.8,1.22-6.76,3.65-1.96,2.44-2.94,6.23-2.94,11.4s.85,9.21,2.56,11.72c1.71,2.51,4.13,3.76,7.25,3.76Z"></path>
                             <path class="cls-1" d="m470.34,99.38c-5.16,0-9.81-1.13-13.96-3.38-4.14-2.25-7.42-5.49-9.81-9.71-2.4-4.22-3.6-9.31-3.6-15.27s1.22-10.94,3.65-15.16c2.43-4.22,5.62-7.47,9.54-9.76,3.93-2.29,8.03-3.44,12.32-3.44,5.16,0,9.43,1.15,12.81,3.44,3.38,2.29,5.92,5.38,7.63,9.27,1.71,3.89,2.56,8.31,2.56,13.25,0,1.38-.07,2.74-.22,4.09-.15,1.35-.29,2.34-.44,3h-32.39c.73,3.93,2.36,6.82,4.91,8.67,2.54,1.85,5.6,2.78,9.16,2.78,3.85,0,7.74-1.2,11.67-3.6l5.34,9.71c-2.76,1.89-5.85,3.38-9.27,4.47-3.42,1.09-6.73,1.64-9.92,1.64Zm-12-34.24h19.52c0-2.98-.71-5.43-2.13-7.36-1.42-1.93-3.73-2.89-6.92-2.89-2.47,0-4.69.86-6.65,2.56-1.96,1.71-3.24,4.27-3.82,7.69Z"></path>
                             <path class="cls-1" d="m502.62,98.08v-54.09h13.09l1.09,6.98h.44c2.25-2.25,4.65-4.2,7.2-5.83,2.54-1.64,5.6-2.45,9.16-2.45,3.85,0,6.96.78,9.32,2.34,2.36,1.56,4.23,3.8,5.62,6.71,2.4-2.47,4.94-4.6,7.63-6.38,2.69-1.78,5.82-2.67,9.38-2.67,5.82,0,10.09,1.95,12.81,5.83,2.73,3.89,4.09,9.21,4.09,15.98v33.59h-16.03v-31.52c0-3.93-.53-6.61-1.58-8.07-1.05-1.45-2.74-2.18-5.07-2.18-2.69,0-5.78,1.74-9.27,5.23v36.53h-16.03v-31.52c0-3.93-.53-6.61-1.58-8.07-1.05-1.45-2.74-2.18-5.07-2.18-2.69,0-5.74,1.74-9.16,5.23v36.53h-16.03Z"></path>
                             <path class="cls-1" d="m602.07,119.23c-1.6,0-3-.11-4.2-.33-1.2-.22-2.34-.47-3.44-.76l2.84-12.21c.51.07,1.09.2,1.74.38.65.18,1.27.27,1.85.27,2.69,0,4.76-.65,6.22-1.96,1.45-1.31,2.54-3.02,3.27-5.13l.76-2.84-20.83-52.67h16.14l7.74,23.23c.8,2.47,1.53,4.98,2.18,7.52.65,2.55,1.34,5.16,2.07,7.85h.44c.58-2.54,1.18-5.11,1.8-7.69.62-2.58,1.25-5.14,1.91-7.69l6.54-23.23h15.38l-18.76,54.63c-1.67,4.51-3.53,8.31-5.56,11.4-2.04,3.09-4.49,5.4-7.36,6.92-2.87,1.53-6.45,2.29-10.74,2.29Z"></path>
                           </g>
                         </g>
                       </g>
                     </g>
                   </svg>
                  <div editabletype="text" class="f-14 f-md-16 w400 mt20 lh160 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
                  <div class=" mt20 mt-md40 white-clr">
                     <script type="text/javascript" src="https://warriorplus.com/o2/disclaimer/rbhnkl" defer=""></script>
                     <div class="wplus_spdisclaimer"><strong>Disclaimer:</strong> <em>WarriorPlus is used to help manage the sale of products on this site. While WarriorPlus helps facilitate the sale, all payments are made directly to the product vendor and NOT WarriorPlus. Thus, all product questions, support inquiries and/or refund requests must be sent to the vendor. WarriorPlus's role should not be construed as an endorsement, approval or review of these products or any claim, statement or opinion used in the marketing of these products.</em></div>
                  </div>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-14 f-md-16 w400 lh160 white-clr text-xs-center">Copyright © Aicademy 2022</div>
                  <ul class="footer-ul w400 f-14 f-md-16 white-clr text-center text-md-right">
                     <li><a href="https://support.bizomart.com/hc/en-us" class="white-clr  t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://aicademy.live/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://aicademy.live/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://aicademy.live/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://aicademy.live/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://aicademy.live/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://aicademy.live/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
    <!--Footer Section End -->


</body>
</html>

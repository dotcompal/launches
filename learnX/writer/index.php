<!Doctype html>
<html>
   <head>
      <title> Aicademy | Writer</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=9">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
      <meta name="title" content="Aicademy Writer">
      <meta name="description" content="Futuristic A.I. Technology Creates Stunning Content for Any Local or Online Niche 10X Faster…Just by Using a Keyword">
      <meta name="keywords" content="Aicademy | Writer">
      <meta property="og:image" content="https://www.aicademy.live/writer/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Pranshu Gupta">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="Website">
      <meta property="og:title" content="Aicademy Writer">
      <meta property="og:description" content=" Futuristic A.I. Technology Creates Stunning Content for Any Local or Online Niche 10X Faster…Just by Using a Keyword">
      <meta property="og:image" content="https://www.aicademy.live/writer/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="Aicademy Writer">
      <meta property="twitter:description" content=" Futuristic A.I. Technology Creates Stunning Content for Any Local or Online Niche 10X Faster…Just by Using a Keyword">
      <meta property="twitter:image" content="https://www.aicademy.live/writer/thumbnail.png">
      <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800;900&display=swap" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css2?family=Pacifico&display=swap" rel="stylesheet">
      <!-- required -->
      <link rel="stylesheet" href="https://cdn.oppyo.com/launches/yodrive/common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style.css" type="text/css">
      <link rel="stylesheet" href="assets/css/timer.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style-feature.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style-bottom.css" type="text/css">
      <!-- <script>
         (function(w, i, d, g, e, t, s) {
             if(window.businessDomain != undefined){
                 console.log("Your page have duplicate embed code. Please check it.");
                 return false;
             }
             businessDomain = 'writerarc';
             allowedDomain = 'aicademy.live';
             if(!window.location.hostname.includes(allowedDomain)){
                 console.log("Your page have not authorized. Please check it.");
                 return false;
             }
             console.log("Your script is ready...");
             w[d] = w[d] || [];
             t = i.createElement(g);
             t.async = 1;
             t.src = e;
             s = i.getElementsByTagName(g)[0];
             s.parentNode.insertBefore(t, s);
         })(window, document, '_gscq', 'script', 'https://cdn.staticdcp.com/apps/engage/smart_engage/js/config-loader.js');
         </script> -->
   </head>
   <body>
   <div class="warning-section">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <div class="offer">
                        <div>
                            <img src="https://cdn.oppyo.com/launches/appzilo/special/error.webp">
                        </div>
                        <div class="f-22 f-md-30 w600 lh140 white-clr text-center">
                            This EXCLUSIVE Offer Has Never Been Offered Before...
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
      <!-- Header Section Start -->
      <div class="header-section">
         <div class="container">
            <div class="row">
               <div class="col-12 d-flex align-items-center justify-content-center  justify-content-md-between flex-wrap flex-md-nowrap">
                  <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 644.49 120" style="max-height:55px">
                     <defs>
                       <style>
                         .cls-1 {
                           fill: #fff;
                         }
                         .cls-2 {
                           fill: #0af;
                         }
                       </style>
                     </defs>
                     <g id="Layer_1-2" data-name="Layer 1">
                       <g>
                         <g>
                           <circle class="cls-2" cx="79.02" cy="43.5" r="7.25"></circle>
                           <circle class="cls-2" cx="70.47" cy="78.46" r="5.46"></circle>
                           <path class="cls-2" d="m158.03,45.28l-33.74,13.31v36.4l-45.27,16.86-27.9-10.4c-4.17,3.37-7.04,6.74-8.15,10.11-1.01,3.22-.07,6.21,2.46,8.42-5.44-1.46-8-5.25-6.96-9.76,1.24-3.62,4.79-7.68,10-12.08h.01c3.95-3.31,8.84-6.84,14.46-10.5,2.04,1.67,4.68,2.68,7.53,2.68,6.56,0,11.88-5.32,11.88-11.88s-5.32-11.88-11.88-11.88-11.88,5.32-11.88,11.88c0,1.13.15,2.21.46,3.25-8.01,5.01-14.83,10.36-19.57,15.44l-5.75-2.14v-7.66c-7.84,4.92-11.84,11.06-5.99,16.99-5.17-1.64-7.69-4.49-7.69-8.05,0-4.83,4.67-10.99,13.69-17.26l35.41-23.23c2.71,2.18,6.13,3.48,9.87,3.48,8.7,0,15.76-7.06,15.76-15.76s-7.06-15.76-15.76-15.76-15.76,7.06-15.76,15.76c0,1.59.23,3.12.68,4.57l-30.2,19.53v-9L0,45.28,79.02,0l79.01,45.28Z"></path>
                           <path class="cls-2" d="m149.38,68.32v-19.62l-1.51.6v19.03c-1.66.37-2.9,1.96-2.9,3.85v11.69h7.31v-11.69c0-1.9-1.24-3.48-2.9-3.85Z"></path>
                         </g>
                         <g>
                           <g>
                             <path class="cls-2" d="m178.03,98.08l22.25-71.1h19.3l22.25,71.1h-17.01l-4.36-16.9h-21.7l-4.36,16.9h-16.36Zm25.74-35.99l-1.74,6.54h15.16l-1.64-6.54c-.95-3.56-1.91-7.34-2.89-11.34-.98-4-1.95-7.85-2.89-11.56h-.44c-.87,3.78-1.76,7.65-2.67,11.61-.91,3.96-1.87,7.73-2.89,11.29Z"></path>
                             <path class="cls-2" d="m256.22,36.03c-2.69,0-4.87-.76-6.54-2.29-1.67-1.53-2.51-3.56-2.51-6.11s.83-4.58,2.51-6.11c1.67-1.53,3.85-2.29,6.54-2.29s4.87.76,6.54,2.29c1.67,1.53,2.51,3.56,2.51,6.11s-.84,4.58-2.51,6.11c-1.67,1.53-3.85,2.29-6.54,2.29Zm-7.96,62.05v-54.09h16.03v54.09h-16.03Z"></path>
                           </g>
                           <g>
                             <path class="cls-1" d="m299.89,99.38c-5.02,0-9.54-1.11-13.58-3.33-4.03-2.22-7.23-5.45-9.6-9.71-2.36-4.25-3.54-9.36-3.54-15.32s1.31-11.16,3.93-15.38c2.62-4.22,6.07-7.43,10.36-9.65,4.29-2.22,8.9-3.33,13.85-3.33,3.34,0,6.31.55,8.89,1.64,2.58,1.09,4.89,2.47,6.92,4.14l-7.52,10.36c-2.55-2.11-4.98-3.16-7.31-3.16-3.85,0-6.92,1.38-9.21,4.14-2.29,2.76-3.44,6.51-3.44,11.23s1.15,8.38,3.44,11.18c2.29,2.8,5.18,4.2,8.67,4.2,1.74,0,3.45-.38,5.13-1.15,1.67-.76,3.2-1.69,4.58-2.78l6.33,10.47c-2.69,2.33-5.6,3.98-8.72,4.96-3.13.98-6.18,1.47-9.16,1.47Z"></path>
                             <path class="cls-1" d="m339.81,99.38c-4.94,0-8.87-1.58-11.78-4.74-2.91-3.16-4.36-7.03-4.36-11.61,0-5.67,2.4-10.1,7.2-13.3,4.8-3.2,12.54-5.34,23.23-6.43-.15-2.4-.86-4.31-2.13-5.73-1.27-1.42-3.4-2.13-6.38-2.13-2.25,0-4.54.44-6.87,1.31-2.33.87-4.8,2.07-7.42,3.6l-5.78-10.58c3.42-2.11,7.07-3.82,10.96-5.13,3.89-1.31,7.94-1.96,12.16-1.96,6.91,0,12.21,2,15.92,6,3.71,4,5.56,10.14,5.56,18.43v30.97h-13.09l-1.09-5.56h-.44c-2.25,2.04-4.67,3.69-7.25,4.96-2.58,1.27-5.4,1.91-8.45,1.91Zm5.45-12.43c1.82,0,3.4-.42,4.74-1.25,1.34-.83,2.71-1.94,4.09-3.33v-9.49c-5.67.73-9.6,1.87-11.78,3.44-2.18,1.56-3.27,3.4-3.27,5.51,0,1.74.56,3.04,1.69,3.87,1.13.84,2.63,1.25,4.53,1.25Z"></path>
                             <path class="cls-1" d="m402.95,99.38c-6.69,0-12.05-2.53-16.09-7.58-4.03-5.05-6.05-11.98-6.05-20.77,0-5.89,1.07-10.96,3.22-15.21,2.14-4.25,4.94-7.51,8.4-9.76,3.45-2.25,7.07-3.38,10.85-3.38,2.98,0,5.49.51,7.52,1.53,2.04,1.02,3.96,2.4,5.78,4.14l-.66-8.29v-18.43h16.03v76.45h-13.09l-1.09-5.34h-.44c-1.89,1.89-4.11,3.47-6.65,4.74-2.55,1.27-5.13,1.91-7.74,1.91Zm4.14-13.09c1.74,0,3.33-.36,4.74-1.09,1.42-.73,2.78-2,4.09-3.82v-22.14c-1.38-1.31-2.85-2.22-4.42-2.73-1.56-.51-3.07-.76-4.53-.76-2.55,0-4.8,1.22-6.76,3.65-1.96,2.44-2.94,6.23-2.94,11.4s.85,9.21,2.56,11.72c1.71,2.51,4.13,3.76,7.25,3.76Z"></path>
                             <path class="cls-1" d="m470.34,99.38c-5.16,0-9.81-1.13-13.96-3.38-4.14-2.25-7.42-5.49-9.81-9.71-2.4-4.22-3.6-9.31-3.6-15.27s1.22-10.94,3.65-15.16c2.43-4.22,5.62-7.47,9.54-9.76,3.93-2.29,8.03-3.44,12.32-3.44,5.16,0,9.43,1.15,12.81,3.44,3.38,2.29,5.92,5.38,7.63,9.27,1.71,3.89,2.56,8.31,2.56,13.25,0,1.38-.07,2.74-.22,4.09-.15,1.35-.29,2.34-.44,3h-32.39c.73,3.93,2.36,6.82,4.91,8.67,2.54,1.85,5.6,2.78,9.16,2.78,3.85,0,7.74-1.2,11.67-3.6l5.34,9.71c-2.76,1.89-5.85,3.38-9.27,4.47-3.42,1.09-6.73,1.64-9.92,1.64Zm-12-34.24h19.52c0-2.98-.71-5.43-2.13-7.36-1.42-1.93-3.73-2.89-6.92-2.89-2.47,0-4.69.86-6.65,2.56-1.96,1.71-3.24,4.27-3.82,7.69Z"></path>
                             <path class="cls-1" d="m502.62,98.08v-54.09h13.09l1.09,6.98h.44c2.25-2.25,4.65-4.2,7.2-5.83,2.54-1.64,5.6-2.45,9.16-2.45,3.85,0,6.96.78,9.32,2.34,2.36,1.56,4.23,3.8,5.62,6.71,2.4-2.47,4.94-4.6,7.63-6.38,2.69-1.78,5.82-2.67,9.38-2.67,5.82,0,10.09,1.95,12.81,5.83,2.73,3.89,4.09,9.21,4.09,15.98v33.59h-16.03v-31.52c0-3.93-.53-6.61-1.58-8.07-1.05-1.45-2.74-2.18-5.07-2.18-2.69,0-5.78,1.74-9.27,5.23v36.53h-16.03v-31.52c0-3.93-.53-6.61-1.58-8.07-1.05-1.45-2.74-2.18-5.07-2.18-2.69,0-5.74,1.74-9.16,5.23v36.53h-16.03Z"></path>
                             <path class="cls-1" d="m602.07,119.23c-1.6,0-3-.11-4.2-.33-1.2-.22-2.34-.47-3.44-.76l2.84-12.21c.51.07,1.09.2,1.74.38.65.18,1.27.27,1.85.27,2.69,0,4.76-.65,6.22-1.96,1.45-1.31,2.54-3.02,3.27-5.13l.76-2.84-20.83-52.67h16.14l7.74,23.23c.8,2.47,1.53,4.98,2.18,7.52.65,2.55,1.34,5.16,2.07,7.85h.44c.58-2.54,1.18-5.11,1.8-7.69.62-2.58,1.25-5.14,1.91-7.69l6.54-23.23h15.38l-18.76,54.63c-1.67,4.51-3.53,8.31-5.56,11.4-2.04,3.09-4.49,5.4-7.36,6.92-2.87,1.53-6.45,2.29-10.74,2.29Z"></path>
                           </g>
                         </g>
                       </g>
                     </g>
                   </svg>
                  <div class="d-flex align-items-center flex-wrap justify-content-center  justify-content-md-end mt15 mt-md0">
                     <!-- <ul class="leader-ul f-16 f-md-18">
                        <li>
                           <a href="#features" class="t-decoration-none">Features</a><span class="pl9 white-clr">|</span>
                        </li>
                        <li>
                           <a href="#demo" class="t-decoration-none">Demo</a>
                        </li> -->
                        <li class="affiliate-link-btn"><a href="#buynow" class="t-decoration-none">Buy Now</a></li>
                     </ul>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md40 text-center">
                  <div class="f-20 f-md-22 w500 blue-clr1 lh140">
                     Never Waste Your Time & Money on Manually Writing Boring Content Again…
                  </div>
               </div>
               <div class="col-12 mt-md30 mt20 f-md-50 f-28 w700 text-center white-clr lh140">
                 <span style="border-bottom:4px solid #fff">Futuristic A.I. Technology</span>  Creates Stunning Content for <span class="gradient-orange"> Any Local or Online Niche 10X Faster…Just by Using a Keyword </span>
               </div>
               <div class="col-12 mt20 text-center">
                  <div class="f-22 f-md-40 w700 lh140 white-clr post-heading">
                  And Makes $15444.28 Extra
                  </div>
               </div>
               <div class="col-12 mt-md30 mt20 text-center">
                  <div class="post-headline f-18 f-md-22 w700 text-center lh160 white-clr text-capitalize">
                     You Sit Back & Relax, Aicademy Will Create Top Converting Content for You <br class="d-none d-md-block">  No Prior Skill Needed | No Monthly Fee Ever…
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md50">
               <div class="col-md-7 col-12 min-md-video-width-left">
                  <img src="assets/images/product-box.webp" class="img-fluid d-block mx-auto">
                  <!-- <div class="col-12 responsive-video border-video">
                     <iframe src="https://writerarc.dotcompal.com/video/embed/spru84q6w8" style="width:100%; height:100%" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                  </div> -->
               </div>
               <div class="col-md-5 col-12 min-md-video-width-right mt20 mt-md0">
                  <ul class="list-head pl0 m0 f-18 f-md-20 lh150 w500 white-clr">
                     <li><span class="orange-clr1">Create Content</span> for Any Local or Online Business</li>
                     <li><span class="orange-clr1"> Let Our Super Powerful</span> A.I. Engine Do the Heavy Lifting</li> 
                     <li><span class="orange-clr1">Preloaded with 50+ Copywriting Templates, </span> like Ads, Video Scripts, Website Content, and much more.</li> 
                     <li><span class="orange-clr1">Works in 35+ Languages</span>  and 22+ Tons of Writing</li>
                     <li> <span class="orange-clr1">Download Your Content</span> in Word/PDF Format</li>
                     <li><span class="orange-clr1">Free Commercial License – </span>Sell Service to Local Clients for BIG Profits</li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!-- Header Section End -->
      <!-- Step Section Start -->
      <div class="step-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center f-28 f-md-50 w500 lh130 black-clr1">
                  The Writer’s A.I. Engine Will <span class="w700">Accomplish All 
                  The Marketing Content Needs In 3 Easy Steps</span> 
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-12 col-md-6 relative">
                  <div class="f-28 f-md-50 step-text pacifico gradient-orange">
                     Step #1   
                  </div>
                  <div class="f-28 f-md-65 w700 lh140 black-clr1 mt10">
                     Choose
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 black-clr1 mt10">
                     Choose from 35+ Use Cases, 22+ Tones, and Languages for which you want to create content. The Aicademy Artificial intelligence is trained by highly professional copywriters to create top converting & plagiarism-free content.                     
                  </div>
                  <img src="assets/images/arrow-left.webp" class="img-fluid d-none ms-md-auto d-md-block" style="position: absolute; right: -140px; top: 340px;">
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                  <img src="assets/images/choose.webp" class="img-fluid d-block mx-auto" alt="Sales &amp; Profits">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md90">
               <div class="col-12 col-md-6 order-md-2 relative">
                  <div class="f-28 f-md-50 step-text pacifico gradient-orange">
                     Step #2 
                  </div>
                  <div class="f-28 f-md-65 w700 lh140 black-clr1 mt10">
                     Enter Keyword
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 black-clr1 mt10">
                     Enter your business name, & then choose purpose (sales copy, email, or ad etc) to create relative and effective content
                  </div>
                  <img src="assets/images/arrow-right.webp" class="img-fluid d-none me-md-auto d-md-block" style="position: absolute; top: 320px; left: -240px;">
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
                  <img src="assets/images/enter.webp" class="img-fluid d-block mx-auto" alt="Udemy, Udacity">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md90">
               <div class="col-12 col-md-6 relative">
                  <div class="f-28 f-md-50 step-text pacifico gradient-orange">
                     Step #3
                  </div>
                  <div class="f-28 f-md-65 w700 lh140 black-clr1 mt10">
                     Publish & Profit 
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 black-clr1 mt10">
                     Aicademy will automatically create professional, fresh and 100% plagiarism-free content of your need in just a few seconds.                  
                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                  <img src="assets/images/create.webp" class="img-fluid d-block mx-auto" alt="Sales &amp; Profits">
               </div>
            </div>
            <div class="row mt20 mt-md90">
               <div class="col-12">
                  <div class="f-20 f-md-24 w700 lh140 text-center">
                     Aicademy is Built for Everyone Across All Levels
                  </div>
                  <div class="mt20 d-flex gap-2 gap-md-4 justify-content-center flex-wrap">
                     <div class="d-flex align-items-center gap-3 justify-content-center">
                        <img src="https://cdn.oppyo.com/launches/writerarc/bizdrive/cross.webp" alt="Cross" class="img-fluid d-block">
                        <div class="f-md-26 w700 lh140 f-20 red-clr">No installation</div>
                     </div>
                     <div class="d-flex align-items-center gap-3 justify-content-center">
                        <img src="https://cdn.oppyo.com/launches/writerarc/bizdrive/cross.webp" alt="Cross" class="img-fluid d-block">
                        <div class="f-md-26 w700 lh140 f-20 red-clr">No Other Techie Stuff</div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Step Section End -->
      <div class="grey-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-40 f-28 w700 lh140 text-center black-clr2">   
                  And Making  Profits Like This      
               </div>
               <div class="col-12 mx-auto mt20 mt-md50">
                  <img src="assets/images/proof1.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>
         </div>
      </div>
      <!-- CTA Btn -->
      <div class="cta-section-new">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-20 f-md-26 w600 lh140 text-center white-clr">
                     Hurry! Limited Time Only. No Monthly Fees
                  </div>
                  <div class="f-16 f-md-20 lh140 w400 text-center mt10 white-clr">
                     Use Coupon Code <span class="w700 orange-clr">Aicademy</span> for an Additional <span class="w700 orange-clr"> $3 Discount</span>
                  </div>
                  <div class="row">
                     <div class="col-12 col-md-7 mx-auto mt20">
                        <div class="f-20 f-md-35 text-center probtn1">
                           <a href="#buynow" class="cta-link-btn d-block">Get Instant Access To Aicademy</a>
                        </div>
                        <img src="https://cdn.oppyo.com/launches/writerarc/bizdrive/payment-options.webp" class="img-fluid mx-auto d-block mt20">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- CTA Btn End -->
      <!-- Features List Section Start -->
      <!-- <div class="list-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="list-block">
                     <div class="row">
                        <div class="col-12">
                           <div class="f-md-50 f-28 w500 lh140 text-capitalize text-center white-clr list-title">
                              We Guarantee That This Will Be The <span class="w700"> <br class="d-none d-md-block">Last Content Writing Tool You’ll Ever Need</span>
                           </div>
                        </div>
                        <div class="col-12 col-md-6">
                           <ul class="features-list pl0 f-20 f-md-22 w400 lh160 black-clr text-capitalize">
                              <li><span class="w600"> A 10X Faster And Easier Way </span> To Generate Killer Content Effortlessly.</li>
                              <li>An A.I. Writing Assistant That Helps You <span class="w600">Create High-Quality Content In Just A Few Seconds.</span> </li> 
                              <li> <span class="w600">Powered By The State Of Art Language A.I.</span> To Generate Original, And Search Engine Optimized Content For Almost Any Vertical.</li> 
                              <li> <span class="w600">50+ Use Case Based Templates To</span>  Cover All Your Content Needs.</li> 
                              <li> <span class="w600">Choose From 30+ Languages</span> - Write In Your Own Or Other Languages For Your Clients.</li> 
                              <li>Write Every Content With The <span class="w600">Right Emotions Through 22+ Tones Of Voice.</span> </li> 
                           </ul>
                        </div>
                        <div class="col-12 col-md-6 mt10 mt-md0">
                           <ul class="features-list pl0 f-20 f-md-22 w400 lh160 black-clr text-capitalize">
                             <li>Loaded With 36+ Predefined Local & Online Business Niches So You <span class="w600">Don’t Have To Hassle With Finding Keywords And The Right Descriptions.</span> </li> 
                             <li>Uses Standard Copywriting Features Like <span class="w600">AIDA And PAS To Provide Top Converting Output.</span> </li> 
                             <li> <span class="w600">100% Cloud-Based Software</span> So You Can Access It From Any In The World.</li> 
                             <li>Fast, Responsive, And 100% Mobile Friendly To <span class="w600">Work Seamlessly With Any Device.</span> </li> 
                             <li> <span class="w600">Download All Your Projects In PDF And Word Formats.</span> </li> 
                             <li> <span class="w600">100% Newbie Friendly,</span>  Need No Prior Skills, No Installation, And No Tech Hassles.</li> 
                           </ul>
                        </div>
                        <div class="col-12 col-md-12 ">
                           <ul class="features-list pl0 f-20 f-md-22 w400 lh160 black-clr text-capitalize">
                              <li><span class="w600">Best In Industry Customer Support With A 96% Customer Satisfaction Rate.</span> </li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div> -->
      <!-- Features List Section End -->
      <!-- Proven Section -->
      <div class="proof-section">
         <div class="container">
            <div class="row">
               <div class="f-md-28 f-22 w500 lh140 text-capitalize text-center black-clr col-12">
                  Let’s agree on one thing… 
               </div>
               <div class="w700 f-md-38 f-28 lh140 text-capitalize text-center black-clr col-12">
                  Today’s World Has Become Too Packed With Old, Repetitive <br class="d-none d-md-block"> & Boring Content!
               </div>
               <div class="f-20 f-md-22 w500 lh140 black-clr text-center mt10">
                 <i>So, having fresh, engaging & better content than your opposition <br class="d-none d-md-block">
                  has become important for every business. Because…</i>  
               </div>
               <div class="col-12 text-center mt20 mt-md40">
                  <div class="reason-box">
                     <div class="f-24 f-md-38 w700 lh140 blue-clr2">
                        Content is a Backbone for Every Form of Marketing…
                     </div>
                     <div class="f-18 f-md-20 w500 lh140 black-clr">
                        Whatever you do to promote your business, whether Social Media Posts, Facebook & Google Ads, YouTube Marketing, Blogging, Email Marketing, or having Landing Page or Website. You need Unique, Engaging, <br class="d-none d-md-block"> and SEO-friendly content. 

                     </div>
                  </div>
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md50">
               <div class="col-12 col-md-6">
                  <!-- <img src="assets/images/proof1.webp" class="img-fluid d-block mx-auto"> -->
                  <div class="f-md-28 f-24 w600 black-clr lh140">
                     And according to Forbes, Here Are Top 10 Reasons Why Content Matters 
                  </div>
                  <ul class="agree-list f-md-18 f-16 w400 mb0 mt20 lh140 black-clr">
                     <li>Help in building customer’s trust</li>
                     <li>Makes you look like an expert</li>
                     <li>You can build a library of content that helps in branding</li>
                     <li>Content creation is a key element of SEO</li>
                     <li>Customers like helpful data and are more likely to follow your brand</li>
                     <li>Content is an excellent and core part of your marketing campaign</li>
                     <li>Establishes your Brand </li>
                     <li>Content is a great way to spread your brand and increase market share</li>
                     <li>Helps in quality lead generation</li>
                     <li>Drives more and more web traffic</li>
                  </ul>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                  <img src="assets/images/proof2.webp" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 f-md-22 f-20 w500 lh140 text-center mt15 mt-md30">
                  And that’s the reason why businesses capitalize on content that is <span class="w700">above 70% of the Total Web Traffic</span>
               </div>
            </div>
         </div>
      </div>
      <!-- Proven Section End -->

       <!-- Old & Boring Content Section Start -->
       <section class="old-content-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="old-content-box ">
                     <div class="f-24 f-md-38 w500 lh140">And You Would Agree That </div>
                     <div class="f-md-65 f-38 w800 lh120 mt15">OLD & BORING CONTENT 
                        <br class="d-none d-md-block">IS NOT WORKING IN 2022! </div>
                  </div>
               </div>
               <div class="col-12">
                  <div class="f-18 f-md-20 w500 white-clr lh140 mt50 mt-md60 text-center">
                     Those are not working on social media, not on YouTube, and not even worth publishing on your <br class="d-none d-md-block">
                     own website or blogs. And that’s the reason why you don’t get FREE traffic, lose<br class="d-none d-md-block">
                     sales &revenue, and must think of PAID traffic methods, my friend.<br class="d-none d-md-block">
                  </div>
               </div>
               <div class="col-12 text-center">
                  <div class="old-headline f-md-28 f-24 w600 lh140 white-clr mt-md30 mt20 ">
                     But before revealing our secret. Let me ask you a question!
                  </div>
               </div>
               <div class="col-12">
                  <div class="w700 f-md-38 f-24 lh140 yellow-clr mt-md30 mt20 text-center">
                     How many times have you clicked & read these <br class="d-none d-md-block">
                     amazing headlines?
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md50">
               <div class="col-12 col-md-8">
                  <img src="assets/images/seo.webp" alt="SEO" class="img-fluid mx-auto d-block">
               </div>
               <div class="col-12 col-md-4">
                  <img src="assets/images/elon.webp" alt="Elon" class="img-fluid mx-auto d-block">
               </div>
               <div class="col-12 mt20 mt-md30">
                  <div class="f-20 f-md-24 w500 lh140 white-clr text-center">
                     We all have clicked & read such trending posts while surfing online regularly. <br class="d-none d-md-block">
                     <u>People only want fresh & unique content, news & articles. This is working like a miracle.</u>
                  </div>
                  <div class="f-24 f-md-38 w700 lh140 yellow-clr text-center mt20 mt-md30">Fresh & Unique Content is The WINNER In 2022 & Beyond...</div>
               </div>
            </div>
         </div>
      </section>
      <!-- Old & Boring Content Section End -->
  <!-- Business Section Start  -->
      <section class="business-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-24 f-md-32 w700 lh140 black-clr text-center">
                     Now, it is safe to say that if you want to grow your business you
                     must create content better than your opposition.
                  </div>
               </div>
               <div class="col-12 text-center mt20 mt-md40">
                  <div class="reason-box">
                     <div class="f-28 f-md-45 w800 lh140 black-clr">
                        Out of 550,000 New Business Every Month
                     </div>
                     <div class="f-24 f-md-32 w700 lh140 black-clr text-center mt10">
                        <span class="blue-clr2">90% of Businesses</span>  Embrace <span class="blue-clr2">Content Marketing</span>  <br class="d-none d-md-block">
                        for their Growth.
                     </div>
                  </div>
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md50">
               <div class="col-12 col-md-6">
                  <div class="f-20 f-md-22 w400 lh140 black-clr text-center text-md-start">
                     However, even after so many efforts, and investing
                     time and money in copywriting, business owners do
                     not get the desired results
                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                  <img src="assets/images/invest.webp" alt="Invest" class="img-fluid mx-auto d-block">
               </div>
               <div class="col-12 text-center mt20 mt-md50">
                  <div class="billion-content">
                     <div class="f-24 f-md-36 w800 lh140 black-clr">
                        This Opens the Door to the $413 Billion Content <br class="d-none d-md-block">
                        Marketing Industry
                     </div>
                  </div>
                  <div class="f-22 f-md-28 w400 lh140 black-clr mt20 mt-md30">
                     and it’s a huge opportunity for freelance copywriters and newbies <br class="d-none d-md-block">
                     who are trying to build a regular income source.
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- Business Section End -->
      
      <div class="wearenot-alone-section ">
         <div class="container ">
            <div class="row mx-0 ">
               <div class="col-12 col-md-10 mx-auto alone-bg ">
                  <div class="row align-items-center">
                     <div class="col-md-1 ">
                        <img src="assets/images/dollars-image.webp " class="img-fluid d-block dollars-sm-image ">
                     </div>
                     <div class="col-md-11 f-28 f-md-45 w700 lh140 white-clr text-center mt15 mt-md0 ">You can build a profitable 6-figure Agency by just serving 1 client a day.
                     </div>
                  </div>
               </div>
               <div class="col-12 px10 f-28 f-md-45 w600 lh140 black-clr text-center mt20 mt-md80 ">Here’s a simple math</div>
               <div class="col-12 mt20 mt-md50 ">
                  <img src="assets/images/make-icon.webp " class="img-fluid d-none d-md-block mx-auto ">
                  <img src="assets/images/make-icon-xs.webp" alt="Make Icon" class="img-fluid d-block d-md-none mx-auto">
               </div>
               <div class="col-12 mt20 mt-md70">
                  <div class="f-md-45 f-28 w600 lh140 text-center white-clr">
                     <span class="title-shape">And It’s Not A 1-Time Income</span>
                  </div>
               </div>
               <div class="col-12 f-20 f-md-24 w400 lh140 mt40 mt-md40 text-center ">
                  You can do this for life and charge your new as well as existing clients for your <br class="d-none d-md-block">
                  services again and again forever.                  
               </div>
            </div>
         </div>
      </div>
      <div class="potential-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-34 f-24 w700 lh150 black-clr text-center">
                  Here’re some examples of freelance copywriters on Fiverr, Odesk, Freelancers, etc who are making 5-6 figures income by helping businesses with their content.
               </div>
            </div>
            <div class="row mt-md50 mt20 align-items-center">
               <div class="col-md-7">
                  <img src="assets/images/meet-smith.webp" alt="" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-md-5 f-md-20 f-18 w400 mt-md0 mt20">
                  <b>Meet Smith</b> has made $10,000 on Fiverr in just 2 months by providing 
                  copywriting services to local businesses
               </div>
            </div>
            <div class="row mt-md50 mt20 align-items-center">
               <div class="col-md-7 order-md-2">
                  <img src="assets/images/boomer-dock.webp" alt="" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-md-5 f-md-20 f-18 w400 mt-md0 mt20 order-md-1">
                  <b>Boomer Dock</b> has made almost $28k on Freelancer and has completed 
                  Over 18 successfully completed Freelancer projects in under 2 months.  
               </div>
            </div>
            <div class="row mt-md0 mt20 align-items-center">
               <div class="col-md-6">
                  <img src="assets/images/bright-brian.webp" alt="" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-md-6 f-md-20 f-18 w400 mt-md0 mt20">
                  <b> Bright Brian</b> has earned almost $17,500 in just 47 Days of his start 
                  as a content writer on Upwork.  
               </div>
            </div>
            
         </div>
      </div>


     
          <!--Awesome Section -->
          <!-- <div class="awesome-sections">
            <div class="container">
               <div class="row">
                  <div class="col-12 mt20 mt-md40">
                     <div class="awesome-shape">
                        <div class="row">
                           <div class="col-12 text-center">
                              <div class="f-md-45 f-28 lh140 w600 white-clr heading-design">
                                 Hi There,
                              </div>
                              <div class="f-md-28 f-22 lh140 w600 white-clr mt20 mt-md40 text-center text-md-start">
                                 From The Desk of Dr Amit Pareek and Atul Pareek
                              </div>
                           </div>
                           <div class="col-12 text-center mt20 mt-md30">
                              <div class="row mt20 mt-md50 ">
                                 <div class="col-md-4 col-12 text-center">
                                    <div class="contact-shape">
                                       <img src="assets/images/amit-pareek-sir.webp" class="img-fluid d-block mx-auto" alt="Amit Pareek">
                                       <div class="f-22 f-md-32 w700 mt15 mt-md20 lh140 text-center white-clr">
                                          Dr Amit Pareek
                                       </div>
                                       <div class="f-15 w500 lh140 text-center white-clr">
                                          (Techpreneur &amp; Marketer)
                                       </div>
                                    </div>
                                 </div>
                                  <div class="col-md-4 col-12 text-center mt20 mt-md0">
                                    <div class="contact-shape">
                                       <img src="assets/images/achal-goswami-sir.webp" class="img-fluid d-block mx-auto" alt="Achal Goswami">
                                       <div class="f-22 f-md-32 w700 mt15 mt-md20  lh140 text-center white-clr">
                                          Achal Goswami
                                       </div>
                                       <div class="f-15 w500 lh140 text-center white-clr">
                                          (Entrepreneur &amp; Internet Marketer)
                                       </div>
                                    </div>
                                 </div> 
                                 <div class="col-md-4 col-12 text-center mt20 mt-md0">
                                    <div class="contact-shape">
                                       <img src="assets/images/atul-parrek-sir.webp" class="img-fluid d-block mx-auto" alt="Atul Pareek">
                                       <div class="f-22 f-md-32 w700 mt15 mt-md20  lh140 text-center white-clr">
                                          Atul Pareek
                                       </div>
                                       <div class="f-15 w500  lh140 text-center white-clr">
                                          (Entrepreneur &amp; Product Creator)
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="col-12 f-18 f-md-20 lh140 w400 white-clr mt20 mt-md30 text-center text-md-start">
                              We have 20+ years of experience together and run 7-figure businesses online and till now not only we have made millions of dollars using content marketing for ourselves but also have helped thousands of people get the desired sales & profits.
                              And while we don’t say this to brag, <b>we do want to make one thing clear: </b>
                                <br><br>
                             <b> Whatever business you do, whether online or local you need content for your website, social media, Facebook & Google Ads, and for every other marketing needs </b>
                              <br><br>
                              Yes! And, by the End of 2022, traffic over fresh & trending content will be above 70% of all internet traffic across the world. Content will definitely dominate the internet. Blogs & Social Media Content will be the primary medium for how internet users will consume information. 
                              
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div> -->
         <!--Awesome Section End-->

      <!-- Big Question Section Start -->
      <!-- <section class="big-ques-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-7">
                  <div class="blue-design">
                     <div class="f-27 f-md-50 w800 white-clr text-center lh140">
                        Here's A BIG Question?
                     </div>
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 black-clr mt40 mt-md50">
                     Even after the fact that content is the need of the hour and the current marketing the scenario has forced businesses to emphasize content marketing, which has created huge demand in the market.
                     <br><br>
                     It is surprising to see that not many people are jumping in, to Tap the $413 Billion Industry and Bridge this Huge Demand and Supply Gap.
                  </div>
               </div>
               <div class="col-12 col-md-5 relative mt20 mt-md0">
                  <img src="assets/images/robot-ques.webp" alt="Robot Question" class="img-fluid mx-auto d-block robot-ques">
               </div>
            </div>
         </div>
      </section> -->
      <!-- Big Question Section End -->
      <!-- Problem Section Start -->
      <!-- <section class="problem-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-28 f-md-50 w700 red-clr1 lh140 text-center">
                     Because The Problem is… 
                  </div>
                  <div class="problem-content-box mt20 mt-md20">
                     <div class="f-18 f-md-20 w400 lh140 text-center white-clr">
                        Writing high converting, engaging, fresh, and SEO friendly copy is not an easy task at all. Especially when you have no copywriting skills and experience. It requires a deep understanding of marketing, a need to understand your audience, and a time-consuming process.  
                        <br><br>
                        And until now there is no reliable solution to help you succeed and the Artificial Intelligence is too costly and accessible only to the Big Fishes.  
                        <br><br>
                        So, if you want to encash content marketing or write content for your business or your client, you have the following options…  
                     </div>
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md40">
               <div class="col-12 col-md-12 mx-auto">
                  <div class="row gx-md-5">
                     <div class="col-12 col-md-6">
                        <div class="rating-container">
                           <div class="rating-box">
                              <div class="f-20 f-md-22 w600 lh140 white-clr">
                                 Option 1: Do It Yourself 
                              </div>
                           </div>
                           <img src="assets/images/hour-rate.webp" alt="Hourly Rate" class="img-fluid mx-auto d-block mt20">
                           <div class="f-18 f-md-20 w400 lh140 white-clr mt20 text-justify">
                              You can do the content writing by yourself for your or your client’s business. But you have to spend countless months learning copywriting skills, but still, you will not get that much experience in a short time. And the most frustrating thing is, during all this time you will not be able to make any money.  
                              <br><br>
                              Even after all this learning, you have to spend countless weeks researching the topic and find good ideas to write high-converting and SEO-friendly content.
                           </div>
                           
                        </div>
                     </div>
                     <div class="col-12 col-md-6 mt20 mt-md0">
                        <div class="rating-container">
                           <div class="rating-box">
                              <div class="f-20 f-md-22 w600 lh140 white-clr">
                                 Option 2: Hire Copywriting Expert 
                              </div>
                           </div>
                           <img src="assets/images/flat-rate.webp" alt="Flat Rate" class="img-fluid mx-auto d-block mt20">
                           <div class="f-18 f-md-20 w400 lh140 white-clr mt20 text-justify">
                              You can hire professionals to help you with content creation for your business & clients. But be prepared to spend a huge amount of money even before you earn anything. Hiring someone on an hourly basis from platforms like Upwork, Freelances, etc will cost you somewhere between $50 to $200 per hour. And hiring someone on a flat rate per project will cost you somewhere between $1000 to $3000. 
                              <br><br>
                              Or you can even hire a full-time employee, but the medium annual salary for a Content Writer is up to $80,000 
                           </div>
                           
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-12 text-center mt20 mt-md50">
                  <div class="red-shape">
                     <div class="f-24 f-md-38 w700 white-clr lh140">Are you Ready to Pay for this…?</div>
                  </div>
               </div>
            </div>
            <div class="scary-container mt20 mt-md50">
               <div class="row">
                  <div class="col-12">
                     <div class="f-md-38 f-24 w700 lh140 text-center white-clr">
                        Isn’t It Scary…?
                     </div>
                  </div>
                  <img src="assets/images/scary-line.webp" alt="Scary Line" class="img-fluid mx-auto d-block w-auto">
               </div>
               <div class="row mt20 mt-md40">
                  <div class="col-12 col-md-7">
                     <div class="f-18 f-md-20 w400 lh140 white-clr">
                        <span class="w600">YES, IT IS. But Not Anymore!</span>
                        <br><br>
                        I and my partners started working to find the solution and after years of learning, planning, designing, coding, debugging & real-user-testing… 
                        <br><br>
                        We’ve got it down to Disruptive A.I. Technology that will change the game forever and you can be at the forefront of this disruption. 
                        <br><br>
                        Stick around and I will show you how this A.I. Technology can give you an unfair advantage in the world of content marketing and how you can jump into a $413 Billion content marketing industry and start your own agency business.
                     </div>
                  </div>
                  <div class="col-12 col-md-5 mt20 mt-md0">
                     <img src="assets/images/scary-person.webp" alt="Scary Person" class="img-fluid mx-auto d-block">
                  </div>
               </div>
            </div>
         </div>
      </section> -->
      <!-- Problem Section End -->
       <!-- Proudly Section -->
       <div class="proudly-sec" id="product">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center lh140">
                  <div class="per-shape f-28 f-md-34 w700">
                     Introducing… 
                  </div>
              </div>
               <div class="col-12 mt-md30 mt20 mt-md40 text-center">
                  <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 644.49 120" style="max-height:55px">
                     <defs>
                       <style>
                         .cls-1 {
                           fill: #fff;
                         }
                         .cls-2 {
                           fill: #0af;
                         }
                       </style>
                     </defs>
                     <g id="Layer_1-2" data-name="Layer 1">
                       <g>
                         <g>
                           <circle class="cls-2" cx="79.02" cy="43.5" r="7.25"></circle>
                           <circle class="cls-2" cx="70.47" cy="78.46" r="5.46"></circle>
                           <path class="cls-2" d="m158.03,45.28l-33.74,13.31v36.4l-45.27,16.86-27.9-10.4c-4.17,3.37-7.04,6.74-8.15,10.11-1.01,3.22-.07,6.21,2.46,8.42-5.44-1.46-8-5.25-6.96-9.76,1.24-3.62,4.79-7.68,10-12.08h.01c3.95-3.31,8.84-6.84,14.46-10.5,2.04,1.67,4.68,2.68,7.53,2.68,6.56,0,11.88-5.32,11.88-11.88s-5.32-11.88-11.88-11.88-11.88,5.32-11.88,11.88c0,1.13.15,2.21.46,3.25-8.01,5.01-14.83,10.36-19.57,15.44l-5.75-2.14v-7.66c-7.84,4.92-11.84,11.06-5.99,16.99-5.17-1.64-7.69-4.49-7.69-8.05,0-4.83,4.67-10.99,13.69-17.26l35.41-23.23c2.71,2.18,6.13,3.48,9.87,3.48,8.7,0,15.76-7.06,15.76-15.76s-7.06-15.76-15.76-15.76-15.76,7.06-15.76,15.76c0,1.59.23,3.12.68,4.57l-30.2,19.53v-9L0,45.28,79.02,0l79.01,45.28Z"></path>
                           <path class="cls-2" d="m149.38,68.32v-19.62l-1.51.6v19.03c-1.66.37-2.9,1.96-2.9,3.85v11.69h7.31v-11.69c0-1.9-1.24-3.48-2.9-3.85Z"></path>
                         </g>
                         <g>
                           <g>
                             <path class="cls-2" d="m178.03,98.08l22.25-71.1h19.3l22.25,71.1h-17.01l-4.36-16.9h-21.7l-4.36,16.9h-16.36Zm25.74-35.99l-1.74,6.54h15.16l-1.64-6.54c-.95-3.56-1.91-7.34-2.89-11.34-.98-4-1.95-7.85-2.89-11.56h-.44c-.87,3.78-1.76,7.65-2.67,11.61-.91,3.96-1.87,7.73-2.89,11.29Z"></path>
                             <path class="cls-2" d="m256.22,36.03c-2.69,0-4.87-.76-6.54-2.29-1.67-1.53-2.51-3.56-2.51-6.11s.83-4.58,2.51-6.11c1.67-1.53,3.85-2.29,6.54-2.29s4.87.76,6.54,2.29c1.67,1.53,2.51,3.56,2.51,6.11s-.84,4.58-2.51,6.11c-1.67,1.53-3.85,2.29-6.54,2.29Zm-7.96,62.05v-54.09h16.03v54.09h-16.03Z"></path>
                           </g>
                           <g>
                             <path class="cls-1" d="m299.89,99.38c-5.02,0-9.54-1.11-13.58-3.33-4.03-2.22-7.23-5.45-9.6-9.71-2.36-4.25-3.54-9.36-3.54-15.32s1.31-11.16,3.93-15.38c2.62-4.22,6.07-7.43,10.36-9.65,4.29-2.22,8.9-3.33,13.85-3.33,3.34,0,6.31.55,8.89,1.64,2.58,1.09,4.89,2.47,6.92,4.14l-7.52,10.36c-2.55-2.11-4.98-3.16-7.31-3.16-3.85,0-6.92,1.38-9.21,4.14-2.29,2.76-3.44,6.51-3.44,11.23s1.15,8.38,3.44,11.18c2.29,2.8,5.18,4.2,8.67,4.2,1.74,0,3.45-.38,5.13-1.15,1.67-.76,3.2-1.69,4.58-2.78l6.33,10.47c-2.69,2.33-5.6,3.98-8.72,4.96-3.13.98-6.18,1.47-9.16,1.47Z"></path>
                             <path class="cls-1" d="m339.81,99.38c-4.94,0-8.87-1.58-11.78-4.74-2.91-3.16-4.36-7.03-4.36-11.61,0-5.67,2.4-10.1,7.2-13.3,4.8-3.2,12.54-5.34,23.23-6.43-.15-2.4-.86-4.31-2.13-5.73-1.27-1.42-3.4-2.13-6.38-2.13-2.25,0-4.54.44-6.87,1.31-2.33.87-4.8,2.07-7.42,3.6l-5.78-10.58c3.42-2.11,7.07-3.82,10.96-5.13,3.89-1.31,7.94-1.96,12.16-1.96,6.91,0,12.21,2,15.92,6,3.71,4,5.56,10.14,5.56,18.43v30.97h-13.09l-1.09-5.56h-.44c-2.25,2.04-4.67,3.69-7.25,4.96-2.58,1.27-5.4,1.91-8.45,1.91Zm5.45-12.43c1.82,0,3.4-.42,4.74-1.25,1.34-.83,2.71-1.94,4.09-3.33v-9.49c-5.67.73-9.6,1.87-11.78,3.44-2.18,1.56-3.27,3.4-3.27,5.51,0,1.74.56,3.04,1.69,3.87,1.13.84,2.63,1.25,4.53,1.25Z"></path>
                             <path class="cls-1" d="m402.95,99.38c-6.69,0-12.05-2.53-16.09-7.58-4.03-5.05-6.05-11.98-6.05-20.77,0-5.89,1.07-10.96,3.22-15.21,2.14-4.25,4.94-7.51,8.4-9.76,3.45-2.25,7.07-3.38,10.85-3.38,2.98,0,5.49.51,7.52,1.53,2.04,1.02,3.96,2.4,5.78,4.14l-.66-8.29v-18.43h16.03v76.45h-13.09l-1.09-5.34h-.44c-1.89,1.89-4.11,3.47-6.65,4.74-2.55,1.27-5.13,1.91-7.74,1.91Zm4.14-13.09c1.74,0,3.33-.36,4.74-1.09,1.42-.73,2.78-2,4.09-3.82v-22.14c-1.38-1.31-2.85-2.22-4.42-2.73-1.56-.51-3.07-.76-4.53-.76-2.55,0-4.8,1.22-6.76,3.65-1.96,2.44-2.94,6.23-2.94,11.4s.85,9.21,2.56,11.72c1.71,2.51,4.13,3.76,7.25,3.76Z"></path>
                             <path class="cls-1" d="m470.34,99.38c-5.16,0-9.81-1.13-13.96-3.38-4.14-2.25-7.42-5.49-9.81-9.71-2.4-4.22-3.6-9.31-3.6-15.27s1.22-10.94,3.65-15.16c2.43-4.22,5.62-7.47,9.54-9.76,3.93-2.29,8.03-3.44,12.32-3.44,5.16,0,9.43,1.15,12.81,3.44,3.38,2.29,5.92,5.38,7.63,9.27,1.71,3.89,2.56,8.31,2.56,13.25,0,1.38-.07,2.74-.22,4.09-.15,1.35-.29,2.34-.44,3h-32.39c.73,3.93,2.36,6.82,4.91,8.67,2.54,1.85,5.6,2.78,9.16,2.78,3.85,0,7.74-1.2,11.67-3.6l5.34,9.71c-2.76,1.89-5.85,3.38-9.27,4.47-3.42,1.09-6.73,1.64-9.92,1.64Zm-12-34.24h19.52c0-2.98-.71-5.43-2.13-7.36-1.42-1.93-3.73-2.89-6.92-2.89-2.47,0-4.69.86-6.65,2.56-1.96,1.71-3.24,4.27-3.82,7.69Z"></path>
                             <path class="cls-1" d="m502.62,98.08v-54.09h13.09l1.09,6.98h.44c2.25-2.25,4.65-4.2,7.2-5.83,2.54-1.64,5.6-2.45,9.16-2.45,3.85,0,6.96.78,9.32,2.34,2.36,1.56,4.23,3.8,5.62,6.71,2.4-2.47,4.94-4.6,7.63-6.38,2.69-1.78,5.82-2.67,9.38-2.67,5.82,0,10.09,1.95,12.81,5.83,2.73,3.89,4.09,9.21,4.09,15.98v33.59h-16.03v-31.52c0-3.93-.53-6.61-1.58-8.07-1.05-1.45-2.74-2.18-5.07-2.18-2.69,0-5.78,1.74-9.27,5.23v36.53h-16.03v-31.52c0-3.93-.53-6.61-1.58-8.07-1.05-1.45-2.74-2.18-5.07-2.18-2.69,0-5.74,1.74-9.16,5.23v36.53h-16.03Z"></path>
                             <path class="cls-1" d="m602.07,119.23c-1.6,0-3-.11-4.2-.33-1.2-.22-2.34-.47-3.44-.76l2.84-12.21c.51.07,1.09.2,1.74.38.65.18,1.27.27,1.85.27,2.69,0,4.76-.65,6.22-1.96,1.45-1.31,2.54-3.02,3.27-5.13l.76-2.84-20.83-52.67h16.14l7.74,23.23c.8,2.47,1.53,4.98,2.18,7.52.65,2.55,1.34,5.16,2.07,7.85h.44c.58-2.54,1.18-5.11,1.8-7.69.62-2.58,1.25-5.14,1.91-7.69l6.54-23.23h15.38l-18.76,54.63c-1.67,4.51-3.53,8.31-5.56,11.4-2.04,3.09-4.49,5.4-7.36,6.92-2.87,1.53-6.45,2.29-10.74,2.29Z"></path>
                           </g>
                         </g>
                       </g>
                     </g>
                   </svg>
               </div>
               <div class="col-12 mt20 mt-md50 f-24 f-md-30 text-center white-clr lh140 w500">
                  Breath-Taking A.I. Based Content Creator That Smartly Creates Any Type of  <br class="d-none d-md-block">
                  Marketing Content for Any Local or Online Niche with Just a Click of a Button 
               </div>
            </div>
            <div class="row mt30 mt-md50 align-items-center">
               <div class="col-12 col-md-12">
                  <img src="assets/images/product-box.webp" class="img-fluid d-block mx-auto img-animation">
               </div>
               <div class="f-18 f-md-20 w400 lh140 white-clr mt20 text-center">
                  A Better and 10X Faster Way to Write Copies. <br>
                  So, stop wasting time and money on content and copywriting anymore. As, now you can ensure the growth of your <br class="d-none d-md-block"> business and embrace content marketing using the power of Artificial Intelligence. 
               </div>
            </div>
            <div class="prouldly-li-box">
               <div class="row mt20 mt-md30 g-0">
                  <div class="col-12 col-md-6">
                     <div class="boxbg boxborder-rb boxborder top-first ">
                        <div class="f-md-20 f-18 w400 lh140 white-clr">
                        Without Investing Time & Money in learning <br class="d-none d-md-block">
                        copywriting.
                        </div>
                     </div>
                     <div class="boxbg boxborder-r boxborder">
                        <div class="f-md-20 f-18 w400 lh140 white-clr">
                        Without Indulging yourself in Hard Work and getting <br class="d-none d-md-block"> Frustrated.  
                        </div>
                     </div>
                  </div>
                  <div class="col-12 col-md-6">
                     <div class="boxbg boxborder-b boxborder ">
                        <div class="f-md-20 f-18 w400 lh140 white-clr">
                        Without Wasting countless weeks in Researching & <br class="d-none d-md-block"> Writing yourself. 
                        </div>
                     </div>
                     <div class="boxbg boxborder ">
                        <div class="f-md-20 f-18 w400 lh140 white-clr">
                        Without Spending thousands of dollars on Hiring <br class="d-none d-md-block"> Copywriting Experts.
                        </div>
                     </div>
                  </div>
               </div>   
            </div>
         </div>
      </div>
      <!-- Proudly Section End -->
       <!-- Step Section Start -->
       <div class="step-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center f-28 f-md-50 w500 lh130 black-clr1">
                  The Writer’s A.I. Engine Will <span class="w700">Accomplish All 
                  The Marketing Content Needs In 3 Easy Steps</span> 
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-12 col-md-6 relative">
                  <div class="f-28 f-md-50 step-text pacifico gradient-orange">
                     Step #1   
                  </div>
                  <div class="f-28 f-md-65 w700 lh140 black-clr1 mt10">
                     Choose
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 black-clr1 mt10">
                     Choose from 35+ Use Cases, 22+ Tones, and Languages for which you want to create content. The Aicademy Artificial intelligence is trained by highly professional copywriters to create top converting &amp; plagiarism-free content.                     
                  </div>
                  <img src="assets/images/arrow-left.webp" class="img-fluid d-none ms-md-auto d-md-block" style="position: absolute; right: -140px; top: 340px;">
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                  <img src="assets/images/choose.webp" class="img-fluid d-block mx-auto" alt="Sales &amp; Profits">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md90">
               <div class="col-12 col-md-6 order-md-2 relative">
                  <div class="f-28 f-md-50 step-text pacifico gradient-orange">
                     Step #2 
                  </div>
                  <div class="f-28 f-md-65 w700 lh140 black-clr1 mt10">
                     Enter Keyword
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 black-clr1 mt10">
                     Enter your business name, &amp; then choose purpose (sales copy, email, or ad etc) to create relative and effective content
                  </div>
                  <img src="assets/images/arrow-right.webp" class="img-fluid d-none me-md-auto d-md-block" style="position: absolute; top: 320px; left: -240px;">
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
                  <img src="assets/images/enter.webp" class="img-fluid d-block mx-auto" alt="Udemy, Udacity">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md90">
               <div class="col-12 col-md-6 relative">
                  <div class="f-28 f-md-50 step-text pacifico gradient-orange">
                     Step #3
                  </div>
                  <div class="f-28 f-md-65 w700 lh140 black-clr1 mt10">
                     Publish &amp; Profit 
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 black-clr1 mt10">
                     Aicademy will automatically create professional, fresh and 100% plagiarism-free content of your need in just a few seconds.                  
                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                  <img src="assets/images/create.webp" class="img-fluid d-block mx-auto" alt="Sales &amp; Profits">
               </div>
            </div>
            <div class="row mt20 mt-md90">
               <div class="col-12">
                  <div class="f-20 f-md-24 w700 lh140 text-center">
                     Aicademy is Built for Everyone Across All Levels
                  </div>
                  <div class="mt20 d-flex gap-2 gap-md-4 justify-content-center flex-wrap">
                     <div class="d-flex align-items-center gap-3 justify-content-center">
                        <img src="https://cdn.oppyo.com/launches/writerarc/bizdrive/cross.webp" alt="Cross" class="img-fluid d-block">
                        <div class="f-md-26 w700 lh140 f-20 red-clr">No installation</div>
                     </div>
                     <div class="d-flex align-items-center gap-3 justify-content-center">
                        <img src="https://cdn.oppyo.com/launches/writerarc/bizdrive/cross.webp" alt="Cross" class="img-fluid d-block">
                        <div class="f-md-26 w700 lh140 f-20 red-clr">No Other Techie Stuff</div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Step Section End -->
      <!-- Demo Section Start -->
       <!-- <div class="demo-section" id="demo">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-50 f-28 w700 lh140 text-center">
                  Watch Aicademy in Action… 
               </div>
               <div class="col-12 col-md-9 mx-auto mt20 mt-md50 relative">
                  <div class="col-12 responsive-video border-video">
                     <iframe src="https://writerarc.dotcompal.com/video/embed/2vptj5wd6o" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                        box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md50">
               <div class="col-12">
                  <div class="f-20 f-md-26 w600 lh140 text-center black-clr">
                     Hurry! Limited Time Only. No Monthly Fees
                  </div>
                  <div class="f-16 f-md-20 lh140 w400 text-center mt10 black-clr">
                     Use Coupon Code <span class="w700 orange-clr">Aicademy</span> for an Additional <span class="w700 orange-clr"> $3 Discount</span>
                  </div>
                  <div class="row">
                     <div class="col-12 col-md-7 mx-auto mt20">
                        <div class="f-20 f-md-35 text-center probtn1">
                           <a href="#buynow" class="cta-link-btn d-block">Get Instant Access To Aicademy</a>
                        </div>
                        <img src="https://cdn.oppyo.com/launches/writerarc/bizdrive/payment-options1.webp" class="img-fluid mx-auto d-block mt20">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div> -->
      <!--Demo Section End -->
      
      <!-- Testimonial Section -->
      <div class="testimonial-section">
         <div class="container ">
            <div class="row ">
               <div class="col-12 f-28 f-md-45 w700 lh140 black-clr text-center">
                  And Here's What Some More Beta Users <br class="d-none d-md-block"> Says About Aicademy
               </div>
            </div>
            <div class="row row-cols-md-2 row-cols-1 gx4 mb-md80">
               <div class="col mt50">
                  <div class="single-testimonial">
                     <p>
                        Thanks for the early access, Aicademy team. Now I am able to create unique content for my client’s social media & google ads campaigns with the help of Aicademy. And I am amazed to see that it has increased ROI by more than 30% in just two days. I have created content for my real estate and restaurant client.
                        <br></br>
                        Aicademy is truly capable of creating any type of content for any business. <span class="w600">I highly recommend Aicademy to every newbie and growth marketer.</span> 
                     </p>
                     <div class="client-info">
                        <img src="assets/images/drake.webp" class="img-fluid d-block mx-auto" alt="Drake Lewis">
                        <div class="client-details">
                           Drake Lewis
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col mt50">
                  <div class="single-testimonial">
                     <p>
                        I am a Website Agency owner but one of the biggest challenges I was facing in my business is the need for a Content Writer. The majority of my clients were demanding website content along with website development and I was unable to fulfill this demand as I was not able to hire a full-time copywriter as it is too costly and not affordable for me.
                        <br><br>
                       <span class="w600">But, thanks to Aicademy, now I am able to create website content by myself by just entering the keyword.</span> I am experiencing 2X growth in my business by providing a package of website development plus content to my clients. 
                     </p>
                     <div class="client-info">
                        <img src="assets/images/josh.webp" class="img-fluid d-block mx-auto" alt="Josh Paul">
                        <div class="client-details">
                           Josh Paul
                        </div>
                     </div>
                  </div>
               </div>
               <!-- <div class="col mt50">
                  <div class="single-testimonial">
                     <p>
                        Aicademy has turned out to be a miracle for my business growth. I am Antonio Martin, a local gift store owner and I usually do promotion of my business through social media ads. But, since the pandemic has happened, more and more businesses have come online which eventually has increased competition for me as well and I was observing a downfall in ROI and customers in my store because my social media ads were not working that well due to old repetitive content, and I am very short of time to hassle for writing new content. But thanks to the Aicademy team for providing me beta access. <span class="w600">Now I am able to generate fresh, and engaging content for my ads campaign, and my ROI is better than ever before.</span>   
                     </p>
                     <div class="client-info">
                        <img src="assets/images/antonio.webp" class="img-fluid d-block mx-auto" alt="Antonio Martin">
                        <div class="client-details">
                           Antonio Martin 
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col mt50">
                  <div class="single-testimonial">
                     <p>
                        I am a full-time content writer. But nowadays I was facing challenges to get my client’s websites and google business listingsto rank on the most popular keywords, the reason is that the internet is overcrowded with old and repetitive content, and creating fresh & unique content wasn’t easy anymore. <br><br> 
                        <span class="w600">But thanks to this life-changing A.I. technology called Aicademy,</span> it has eased my problem and it writes perfect and SEO-optimized content with just the help of a keyword. Now I have more satisfied clients and I can create more and more content in just a few seconds.  
                     </p>
                     <div class="client-info">
                        <img src="assets/images/mike.webp" class="img-fluid d-block mx-auto" alt="Mike East">
                        <div class="client-details">
                           Mike East
                        </div>
                     </div>
                  </div>
               </div> -->
            </div>
            <div id="features"></div>
         </div>
      </div>
      <!-- Testimonial Section End -->
      <!-- Features Headline Start -->
      <div class="features-headline" id="features">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-50 w700 lh140 white-clr">
                     Checkout The Power Packed Features <br class="d-none d-md-block"> of This Disruptive A.I. Technology
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Features Headline End -->
      <img src="assets/images/down-arrow.webp" class="img-fluid mx-auto d-none d-md-block vert-move" alt="Arrow" style="margin-top:-35px;">
      <!-- Features 1 Section Start -->
      <div class="features-one">
         <div class="container">
            <div class="row align-items-center flex-wrap">
               <div class="col-12 col-md-6">
                  <div class="f-24 f-md-36 w700 lh140 black-clr text-capitalize">
                     Create Fresh & SEO-Friendly Copies 
                  </div>
                  <div class="f-18 f-md-20  w400 lh140 mt10 text-left">                
                     Aicademy A.I. Engine Generate 100% Fresh and Unique Copy 
                     which is search engine optimized and easy to rank.  
                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                  <img src="assets/images/f1.webp" class="img-fluid mx-auto d-block" alt="Feature 1">
               </div>
            </div>
         </div>
      </div>
      <!-- Features 1 Section End -->
      <div class="potential-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-24 f-md-36 w700 lh140 black-clr text-capitalize">
                     Create Content for Any Local or Online Niche 
                  </div>
                  <div class="f-18 f-md-20  w400 lh140 mt10 text-left">                
                     Aicademy has tons of predefined Local and Online business categories with <br class="d-none d-md-block">
                     predefine keywords for 36+ Niches. So, you can create content with just a click of a button. 
                  </div>
                  <div class="f-18 f-md-20 w700 lh140 black-clr text-capitalize mt20">
                     Here is the List of Potential Use Cases Aicademy can be Used For.
                  </div>
               </div>
            </div>
            <div class="row mt-md50 mt20">
               <div class="col-md-2 col-6">
                  <div class="list-shape">
                     <img src="assets/images/pn1.webp" class="img-fluid mx-auto d-block" alt="Business Coaches">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Business Coaches
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt0">
                  <div class="list-shape">
                     <img src="assets/images/pn2.webp" class="img-fluid mx-auto d-block" alt="Courses Sellers">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Courses Sellers
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn3.webp" class="img-fluid mx-auto d-block" alt="Architect">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Architect
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn4.webp" class="img-fluid mx-auto d-block" alt="Hotels">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Hotels
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn5.webp" class="img-fluid mx-auto d-block" alt="E-Book Sellers">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     E-Book Sellers
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn6.webp" class="img-fluid mx-auto d-block" alt="Yoga Classes">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Yoga Classes
                  </div>
               </div>
            </div>
            <div class="row mt-md50 mt0">
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn7.webp" class="img-fluid mx-auto d-block" alt="Painters &amp; Decorators">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Painters &amp; Decorators
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn8.webp" class="img-fluid mx-auto d-block" alt="Schools">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Schools
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn9.webp" class="img-fluid mx-auto d-block" alt="Freelancers">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Freelancers
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn10.webp" class="img-fluid mx-auto d-block" alt="Home Tutors">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Home Tutors
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn11.webp" class="img-fluid mx-auto d-block" alt="Computer Repair">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Computer Repair
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn12.webp" class="img-fluid mx-auto d-block" alt="Gym">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Gym
                  </div>
               </div>
            </div>
            <div class="row mt-md50 mt0">
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn13.webp" class="img-fluid mx-auto d-block" alt="Real Estate">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Real Estate
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn14.webp" class="img-fluid mx-auto d-block" alt="Dance &amp; Music Trainers">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Dance &amp; Music Trainers
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn15.webp" class="img-fluid mx-auto d-block" alt="Home Security">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Home Security
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn16.webp" class="img-fluid mx-auto d-block" alt="Dentists">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Dentists
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn17.webp" class="img-fluid mx-auto d-block" alt="E-Com Sellers">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     E-Com Sellers
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn18.webp" class="img-fluid mx-auto d-block" alt="Cooking Classes">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Cooking Classes
                  </div>
               </div>
            </div>
            <div class="row mt-md50 mt0">
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn19.webp" class="img-fluid mx-auto d-block" alt="Lock Smith">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Lock Smith
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn20.webp" class="img-fluid mx-auto d-block" alt="Lawyers">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Lawyers
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn21.webp" class="img-fluid mx-auto d-block" alt="Digital Product Sellers">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Digital Product Sellers
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn22.webp" class="img-fluid mx-auto d-block" alt="Affiliate Marketers">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Affiliate Marketers
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn23.webp" class="img-fluid mx-auto d-block" alt="Veterinarians">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Veterinarians
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn24.webp" class="img-fluid mx-auto d-block" alt="Financial Adviser">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Financial Adviser
                  </div>
               </div>
            </div>
            <div class="row mt-md50 mt0">
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn25.webp" class="img-fluid mx-auto d-block" alt="YouTubers">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     YouTubers
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn26.webp" class="img-fluid mx-auto d-block" alt="Digital Marketers">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Digital Marketers
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn27.webp" class="img-fluid mx-auto d-block" alt="Florist">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Florist
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn28.webp" class="img-fluid mx-auto d-block" alt="Dermatologists">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Dermatologists
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn29.webp" class="img-fluid mx-auto d-block" alt="Car &amp;Taxi Services">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Car &amp; Taxi Services
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn30.webp" class="img-fluid mx-auto d-block" alt="Plumbers">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Plumbers
                  </div>
               </div>
            </div>
            <div class="row mt-md50 mt0">
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn31.webp" class="img-fluid mx-auto d-block" alt="Sports Clubs">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Sports Clubs
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn32.webp" class="img-fluid mx-auto d-block" alt="Counsellors">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Counsellors
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn33.webp" class="img-fluid mx-auto d-block" alt="Garage Owners">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Garage Owners
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn34.webp" class="img-fluid mx-auto d-block" alt="Carpenter">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Carpenter
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn35.webp" class="img-fluid mx-auto d-block" alt="Bars &amp; Restaurants">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Bars &amp; Restaurants
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn36.webp" class="img-fluid mx-auto d-block" alt="Chiropractors">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Chiropractors
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Features 3 Section Start -->
      <div class="features-three">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-24 f-md-36 w700 lh140 white-clr text-capitalize">
                     Advance Copywriting Templates 
                  </div>
                  <div class="f-18 f-md-20  w400 lh140 mt10 text-left white-clr">                
                     Aicademy is trained by highly experienced copywriters, powered with 20 Billion plus data samples and has 50+ advanced pre-defined copywriting templates based on different use cases like Google Ads, Facebook Ads, Blogs, Sales Copy, YouTube descriptions, Video Scripts, Website Content, and much more.
                  </div>
               </div>
            </div>
            <div class="row row-cols-1 row-cols-md-3 mt-md50">
               <div class="col">
                  <div class="feature-blog">
                     <img src="assets/images/ic1.webp" class="img-fluid" alt="">
                     <div class="f-18 f-md-20 w400 lh150 white-clr">
                        Blog Ideas
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-blog">
                     <img src="assets/images/ic2.webp" class="img-fluid" alt="">
                     <div class="f-18 f-md-20 w400 lh150 white-clr">
                        Brand Description
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-blog">
                     <img src="assets/images/ic3.webp" class="img-fluid" alt="">
                     <div class="f-18 f-md-20 w400 lh150 white-clr">
                        Copywriting Framework
                        AIDA & PAS
                     </div>
                  </div>
               </div>
            </div>
            <div class="row row-cols-1 row-cols-md-3">
               <div class="col">
                  <div class="feature-blog">
                     <img src="assets/images/ic4.webp" class="img-fluid" alt="">
                     <div class="f-18 f-md-20 w400 lh150 white-clr">
                        Promotional & Newsletter Emails
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-blog">
                     <img src="assets/images/ic5.webp" class="img-fluid" alt="">
                     <div class="f-18 f-md-20 w400 lh150 white-clr">
                        Social Media Captions
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-blog">
                     <img src="assets/images/ic6.webp" class="img-fluid" alt="">
                     <div class="f-18 f-md-20 w400 lh150 white-clr">
                        Facebook Ads
                     </div>
                  </div>
               </div>
            </div>
            <div class="row row-cols-1 row-cols-md-3">
               <div class="col">
                  <div class="feature-blog">
                     <img src="assets/images/ic7.webp" class="img-fluid" alt="">
                     <div class="f-18 f-md-20 w400 lh150 white-clr">
                        Google Ads
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-blog">
                     <img src="assets/images/ic8.webp" class="img-fluid" alt="">
                     <div class="f-18 f-md-20 w400 lh150 white-clr">
                        Social Media Posts
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-blog">
                     <img src="assets/images/ic9.webp" class="img-fluid" alt="">
                     <div class="f-18 f-md-20 w400 lh150 white-clr">
                        Product Description
                     </div>
                  </div>
               </div>
            </div>
            <div class="row row-cols-1 row-cols-md-3">
               <div class="col">
                  <div class="feature-blog">
                     <img src="assets/images/ic10.webp" class="img-fluid" alt="">
                     <div class="f-18 f-md-20 w400 lh150 white-clr">
                        Product Reviews
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-blog">
                     <img src="assets/images/ic11.webp" class="img-fluid" alt="">
                     <div class="f-18 f-md-20 w400 lh150 white-clr">
                        Testimonials
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-blog">
                     <img src="assets/images/ic12.webp" class="img-fluid" alt="">
                     <div class="f-18 f-md-20 w400 lh150 white-clr">
                        And Much More
                     </div>
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md50">
               <div class="col-12">
                  <div class="but-design">
                     <div class="f-24 f-md-38 w700 black-clr text-center lh140">
                        Here are some examples of what Aicademy can do for you
                     </div>
                  </div>
               </div>
            </div>
            <div class="row row-cols-1 mt50 mt-md50">
               <div class="col">
                  <img src="assets/images/te1.webp" class="img-fluid mx-auto d-block" alt="">
               </div>
            </div>
         </div>
      </div>
      <!-- Features 3 Section End -->
      <!-- Features 4 Section Start -->
      <div class="features-four">
         <div class="container">
            <div class="row align-items-center flex-wrap">
               <div class="col-12 col-md-6">
                  <div class="f-24 f-md-36 w700 lh140 black-clr text-capitalize">
                     35+ Languages & 22+ Tones 
                  </div>
                  <div class="f-18 f-md-20  w400 lh140 mt10 text-left">                
                     Aicademy can write copy in 35+ languages and over 
                     22+ tones of voice available to fit your business need.  
                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                  <img src="assets/images/f4.webp" class="img-fluid mx-auto d-block" alt="Feature 1">
               </div>
            </div>
            <div class="row align-items-center flex-wrap mt20 mt-md70">
               <div class="col-12 col-md-6 order-md-2">
                  <div class="f-24 f-md-36 w700 lh140 black-clr text-capitalize">
                     Generate 1000 Write Up Monthly 
                  </div>
                  <div class="f-18 f-md-20  w400 lh140 mt10 text-left">                
                     You can generate as many as up to 1000 Copies each 
                     month through Aicademy. 
                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
                  <img src="assets/images/f5.webp" class="img-fluid mx-auto d-block" alt="Feature 1">
               </div>
            </div>
            <div class="row align-items-center flex-wrap mt20 mt-md70">
               <div class="col-12 col-md-6">
                  <div class="f-24 f-md-36 w700 lh140 black-clr text-capitalize">
                     Social Media Integration 
                  </div>
                  <div class="f-18 f-md-20  w400 lh140 mt10 text-left">                
                     Aicademy provides Social Media Integration to 
                     Directly Share Your posts on Social Media
                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                  <img src="assets/images/f6.webp" class="img-fluid mx-auto d-block" alt="Feature 1">
               </div>
            </div>
            <div class="row align-items-center flex-wrap mt20 mt-md70">
               <div class="col-12 col-md-6 order-md-2">
                  <div class="f-24 f-md-36 w700 lh140 black-clr text-capitalize">
                     Download Your Content
                  </div>
                  <div class="f-18 f-md-20  w400 lh140 mt10 text-left">                
                     You can easily download your content in Word or PDF format
                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
                  <img src="assets/images/f7.webp" class="img-fluid mx-auto d-block" alt="Feature 1">
               </div>
            </div>
            <div class="row align-items-center flex-wrap mt20 mt-md70">
               <div class="col-12 col-md-6">
                  <div class="f-24 f-md-36 w700 lh140 black-clr text-capitalize">
                     Commercial License Included 
                  </div>
                  <div class="f-18 f-md-20  w400 lh140 mt10 text-left">                
                     Aicademy comes with Commercial use license so that you can use 
                     Aicademy for commercial purposes and sell copies to your clients.
                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                  <img src="assets/images/commercial-license-img.webp" class="img-fluid mx-auto d-block" alt="Feature 1">
               </div>
            </div>
         </div>
      </div>
      <!-- Features 4 Section End -->
      <!-- CTA  Section Start -->
      <div class="cta-section-new">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-20 f-md-26 w600 lh140 text-center white-clr">
                     Hurry! Limited Time Only. No Monthly Fees
                  </div>
                  <div class="f-16 f-md-20 lh140 w400 text-center mt10 white-clr">
                     Use Coupon Code <span class="w700 orange-clr">Aicademy</span> for an Additional <span class="w700 orange-clr"> $3 Discount</span>
                  </div>
                  <div class="row">
                     <div class="col-12 col-md-7 mx-auto mt20">
                        <div class="f-20 f-md-35 text-center probtn1">
                           <a href="#buynow" class="cta-link-btn d-block">Get Instant Access To Aicademy</a>
                        </div>
                        <img src="https://cdn.oppyo.com/launches/writerarc/bizdrive/payment-options.webp" class="img-fluid mx-auto d-block mt20">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- CTA  Section End -->
      <!-- Suport Section Start -->
      <div class="support-section">
         <div class="container ">
            <div class="row ">
               <div class="col-12 ">
                  <div class="f-md-50 f-28 w700 lh140 black-clr text-center ">
                     Here’re Some More Features
                  </div>
               </div>
            </div>
            <div class="row row-cols-md-3 row-cols-xl-3 row-cols-2">
               <div class="col mt20 mt-md30">
                  <div class="feature-wrapper">
                     <img src="assets/images/k1.webp" alt="Round-the-Clock Support" class="img-fluid">
                     <div class="white-clr lh140 f-18 f-md-20 w500">Round-the-Clock <br class="d-none d-md-block"> Support</div>
                  </div>
               </div>
               <div class="col mt20 mt-md30">
                  <div class="feature-wrapper">
                     <img src="assets/images/k2.webp" alt="No Technical Skills Required" class="img-fluid">
                     <div class="white-clr lh140 f-18 f-md-20 w500">No Coding, Design or <br class="d-none d-md-block">
                        Technical Skills Required
                     </div>
                  </div>
               </div>
               <div class="col mt20 mt-md30">
                  <div class="feature-wrapper">
                     <img src="assets/images/k3.webp" alt="Regular Updates" class="img-fluid">
                     <div class="white-clr lh140 f-18 f-md-20 w500">Regular Updates</div>
                  </div>
               </div>
               <div class="col mt20 mt-md30">
                  <div class="feature-wrapper">
                     <img src="assets/images/k4.webp" alt="Training & Tutorial Video" class="img-fluid">
                     <div class="white-clr lh140 f-18 f-md-20 w500">Complete Step-by-Step Video <br class="d-none d-md-block"> Training And Tutorials Included</div>
                  </div>
               </div>
               <div class="col mt20 mt-md30">
                  <div class="feature-wrapper">
                     <img src="assets/images/k5.webp" alt="Cloud-Based Software" class="img-fluid">
                     <div class="white-clr lh140 f-18 f-md-20 w500">Cloud-Based Software</div>
                  </div>
               </div>
               <div class="col mt20 mt-md30">
                  <div class="feature-wrapper">
                     <img src="assets/images/k6.webp" alt="Limited Commercial License" class="img-fluid">
                     <div class="white-clr lh140 f-18 f-md-20 w500">Limited Commercial License</div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Suport Section End -->
      <!-- Compare Table Section -->
      <section class="comp-tablse">
         <div class="comparetable-section">
            <div class="container ">
               <div class="row ">
                  <div class="col-12">
                     <div class="f-md-38 f-28 w700 lh140 text-capitalize text-center black-clr">
                        A.I. Based Content Creation Technology in your hands… 
                     </div>
                     <div class="f-md-22 f-20 w500 lh140 text-capitalize text-center black-clr mt20">
                        Create and Export All Type of Marketing Content with Just a Click of a Button Effortlessly. 
                     </div>
                  </div>
               </div>
               <div class="row mt20 mt-md50 row-cols-1 row-cols-md-2">
                  <div class="col relative">
                     <div class="compare-red-wrap">
                        <div class="f-md-28 f-22 w700 lh140 text-center"> <u>Without Aicademy</u> </div>
                        <ul class="compare-red pl0 f-20 f-md-22 w400 lh160 black-clr text-capitalize mt20">
                           <li>Keep thinking where to start from</li>
                           <li>Old and copied content that fails to engage and rank on Google. </li>
                           <li>Less ROI due to non-performing ads, posts or emails</li>
                           <li>Wasting thousands of dollars on copywriting experts and still not getting desired results. </li>
                           <li>Lower brand reputation, lesser sales and leads. </li>
                        </ul>                        
                     </div>
                     <img src="assets/images/ai-robot1.webp" alt="A.I Robot" class="img-fluid mx-auto d-none d-md-block" style="position:absolute; top: 100px; left: -160px;">
                  </div>
                  <div class="col mt20 mt-md0 relative">
                     <div class="compare-red-wrap1">
                        <div class="f-md-28 f-22 w700 lh140 text-center"> <u>With Aicademy</u> </div>
                        <ul class="compare-red1 pl0 f-20 f-md-22 w400 lh160 black-clr text-capitalize mt20">
                           <li>Never have to think where to start from. </li>
                           <li>Higher social media engagement, higher SEO ranking, lower bounce rate and more</li>
                           <li>High returns on Facebook & Google Ads</li>
                           <li>Manage all your marketing content under one dashboard.</li>
                           <li>Trained AI writes fresh and unique copy every time. </li>
                           <li>Write marketing content for any product or service in any niche</li>
                           <li>No fear of plagiarism. </li>
                           <li>No tech skills required, follow just 3 simple steps to get your content done.</li>
                        </ul>
                     </div>
                     <img src="assets/images/ai-robot2.webp" alt="A.I Robot" class="img-fluid mx-auto d-none d-md-block" style="position:absolute; top: 200px; right: -230px;">
                  </div>
               </div>
               <div class="row mt20 mt-md100">
                  <div class="col-12 col-md-12 no-comparison-shape">
                     <div class="text-capitalize f-md-45 f-28 w700 lh140 black-clr text-center text-md-start">
                        There is ABSOLUTELY NO Comparison of Aicademy at Any Price.
                     </div>
                     <div class="f-18 f-md-20 w400 lh140 mt20 text-center text-md-start">
                        That’s correct! There’s absolutely no match to the power packed Aicademy. It’s built with years of experience, designing, coding, debugging & real-user-testing to help get you maximum results.<br><br>
                        When it comes to creating super engaging content for any business need, no other software even comes close! Aicademy gives you the power to literally crush your competition so…<br><br>
                        Grab it today for this low one-time price because it will never be available again after launch at this price.
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- Compare Table Section End -->
      <div class="cta-section-new white-bg">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-20 f-md-26 w600 lh140 text-center white-clr">
                     Hurry! Limited Time Only. No Monthly Fees
                  </div>
                  <div class="f-16 f-md-20 lh140 w400 text-center mt10 white-clr">
                     Use Coupon Code <span class="w700 orange-clr">Aicademy</span> for an Additional <span class="w700 orange-clr"> $3 Discount</span>
                  </div>
                  <div class="row">
                     <div class="col-12 col-md-7 mx-auto mt20">
                        <div class="f-20 f-md-35 text-center probtn1">
                           <a href="#buynow" class="cta-link-btn d-block">Get Instant Access To Aicademy</a>
                        </div>
                        <img src="https://cdn.oppyo.com/launches/writerarc/bizdrive/payment-options.webp" class="img-fluid mx-auto d-block mt20">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
     

      <!-- Bonus Section Start -->
      <div class="bonus-section">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-12 text-center">
                  <div class="but-design">
                     <div class="f-24 f-md-45 w700 black-clr text-center lh140">
                        Plus You’ll Also Receive These Amazing Bonuses With Your Purchase Today…
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-12 mt50 mt-md60">
                  <div class="f-18 f-md-22 w400 lh140 text-center">
                     Aicademy is already worth 10X the price we’re charging today. But just to sweeten the pot a bit more and over deliver, we’re going to include these special bonuses that will enhance your investment in Aicademy
                  </div>
               </div>
               <div class="col-12 col-md-12 mt60 mt-md100">
                  <div class="bonus-section-shape text-center">
                     <div class="col-12 margin-t-60">
                        <div class="bonus-headline">
                           <div class="f-md-24 f-20 w600 white-clr text-nowrap text-center">
                              Bonus #1
                           </div>
                        </div>
                     </div>
                     <div class="row d-flex align-items-center flex-wrap mt15 mt-md30">
                        <div class="col-12 col-md-7 order-md-2">
                           <div class="w600 f-22 f-md-24 black-clr lh140 text-start">
                              How To Start a Freelance Business
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-start">
                              Being a freelancer is one of the most liberating career paths. It is also one of the toughest. Starting a freelance business is not just as simple as waking up in the morning, rolling out of bed, and opening your laptop. Freelancing is not just an easy way to monetize your hobbies and nor is it easier than a “proper” job. This video course will give you all the tools that you need to be a successful freelancer. It will tackle common problems and answer the most common questions that new freelancers have.
                           </div>
                        </div>
                        <div class="col-12 col-md-5 order-md-1 mt20 mt-md0">
                           <img src="assets/images/bonus1.webp" class="img-fluid d-block mx-auto max-h450" alt="Bonus">
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-12 mt60 mt-md100">
                  <div class="bonus-section-shape text-center">
                     <div class="col-12 margin-t-60">
                        <div class="bonus-headline">
                           <div class="f-md-24 f-20 w600 white-clr text-nowrap text-center">
                              Bonus #2 
                           </div>
                        </div>
                     </div>
                     <div class="row d-flex align-items-center flex-wrap mt15 mt-md30">
                        <div class="col-12 col-md-7">
                           <div class="w600 f-22 f-md-24 black-clr lh140 text-start">
                              Content Marketing Formula 
                           </div>
                           <div class="f-18 f-md-20 w400 lh140 mt10 text-start">
                              Content marketing is currently one of the biggest trends in digital marketing as a whole and is an area that many website owners and brands are investing in heavily right now thanks to the impressive returns that they are seeing. In this guide you will  learn how content marketing works, why it is crucial for your business and how to harness it in a way that completely transforms your success. You’ll receive a completely content marketing formula that you can adapt to your own brand and that you can use to build immense authority and a huge list of readers 
                           </div>
                        </div>
                        <div class="col-12 col-md-5 mt20 mt-md0">
                           <img src="assets/images/bonus1.webp" class="img-fluid d-block mx-auto max-h450" alt="Bonus">
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-12 mt60 mt-md100">
                  <div class="bonus-section-shape text-center">
                     <div class="col-12 margin-t-60">
                        <div class="bonus-headline">
                           <div class="f-md-24 f-20 w600 white-clr text-nowrap text-center">
                              Bonus #3
                           </div>
                        </div>
                     </div>
                     <div class="row d-flex align-items-center flex-wrap mt15 mt-md30">
                        <div class="col-12 col-md-7 order-md-2">
                           <div class="w600 f-22 f-md-24 black-clr lh140 text-start">
                              Blogging Traffic Mantra
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-start">
                              Making a living as a blogger has to be one of the sweetest gigs out there. As a blogger, This is all about working hard and smart now so that you can reap the benefits later. Too many people approach blogging in the wrong way, thinking that they can just write a few posts on a semi-regular basis and that that will be enough to ensure their success. In reality, you need to approach blogging as a full-time job if you ever want it to be your main source of income.
                           </div>
                        </div>
                        <div class="col-12 col-md-5 order-md-1 mt20 mt-md0">
                           <img src="assets/images/bonus1.webp" class="img-fluid d-block mx-auto max-h450" alt="Bonus">
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-12 mt60 mt-md100">
                  <div class="bonus-section-shape text-center">
                     <div class="col-12 margin-t-60">
                        <div class="bonus-headline">
                           <div class="f-md-24 f-20 w600 white-clr text-nowrap text-center">
                              Bonus #4
                           </div>
                        </div>
                     </div>
                     <div class="row d-flex align-items-center flex-wrap mt15 mt-md30">
                        <div class="col-12 col-md-7">
                           <div class="w600 f-22 f-md-24 black-clr lh140 text-start">
                              How To Launch Your Online Course 
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-start">
                              Knowing the best online tools and marketing techniques are the keys to creating a financially successful online course. Launching an online course does not have to be difficult. With the help of this guide, you will learn key aspects of a successful online course launch. You will learn beginner-friendly tools and tips, gain key insights on course design, creation, editing, and launching. 
                           </div>
                        </div>
                        <div class="col-12 col-md-5 mt20 mt-md0">
                           <img src="assets/images/bonus1.webp" class="img-fluid d-block mx-auto max-h450" alt="Bonus">
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 text-center">
                  <div class="bonus-gradient">
                     <div class="f-22 f-md-36 w600 white-clr lh240 text-center ">
                        That’s A Total Value of <br class="d-none d-md-block"><span class="f-60 f-md-100 w600">$3300</span>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!---Bonus Section End-->
      <!-- amazing-sound -->
      <div class="amazing-sound">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-12">
                  <div class="col-12 f-28 f-md-45 w600 white-clr text-center lh140">
                     <div class="title-shape">
                        <div class="f-22 f-md-28 w700 white-clr lh140 text-center">
                           “Wow! That Sounds Like an Amazing Deal!”
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-12 mt20 mt-md30">
                  <div class="f-24 f-md-40 w600 lh140 text-center">
                     But “I’m Just Getting Started Online. Can Aicademy 
                     Help Me Build an Online Business Too?”                     
                  </div>
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md50">
               <div class="col-12 col-md-7">
                  <div class="f-md-20 f-18 w400 lh140 black-clr ">
                     <span class="w600 f-20 f-md-36 "><u>Absolutely</u>,</span>
                     <br>
                     You easily can tap into the huge content writing service selling industry right 
                     from the comfort of your home. Help local businesses to get great ROI with 
                     their digital marketing and make a sustainable income stream.
                     <br><br>
                     <span class="w600">You can literally create a highly engaging marketing content by your 
                     own and start an online business today</span> or for local clients without any 
                     tech hassles or specialiezed skills. Even better, you won’t go bankrupt in 
                     the process by hiring money sucking copywriting experts. 
                     <br><br>
                     <span class="w600">This is something great we are offering you today that will <u> open a HUGE Business Opportunity For YOU!</u> </span>
                     </span>
                  </div>
               </div>
               <div class="col-12 col-md-5 mt20 mt-md0 ">
                  <img src="assets/images/sounds-great-img.webp" alt="Sounds Great" class="img-fluid mx-auto d-block">
               </div>
            </div>
         </div>
      </div>
      <!-- amazing-sound-end -->

      <div class="riskfree-section">
         <div class="container ">
            <div class="row align-items-center ">
               <div class="f-28 f-md-50 w700 black-clr text-center col-12 mb-md40">Test Drive It Risk FREE For 30 Days
               </div>
               <div class="col-md-6 col-12 mt15 mt-md0">
                  <div class="f-18 f-md-20 w400 lh140 black-clr"><span class="w600">Your purchase is secured with our 30-day money back guarantee.</span>
                     <br><br>
                     So, you can buy with full confidence and try it for yourself and for your clients risk free. If you face any Issue or don’t get results you desired after using it, just raise a ticket on support desk within 30 days and we'll refund you everything, down to the last penny.
                     <br><br>
                     We would like to clearly state that we do NOT offer a “no questions asked” money back guarantee. You must provide a genuine reason when you contact our support and we will refund your hard-earned money.
                  </div>
               </div>
               <div class="col-md-6 col-12 mt20 mt-md0">
                  <img src="assets/images/riskfree-img.webp " class="img-fluid d-block mx-auto" alt="Money Back Guarantee">
               </div>
            </div>
         </div>
      </div>
      <!-- Guarantee Section End -->
      <!-- Discounted Price Section Start -->
      <div class="table-section" id="buynow">
         <div class="container ">
            <div class="row ">
               <div class="col-12 ">
                  <div class="f-20 f-md-24 w400 text-center lh140">
                     Take Advantage of Our Limited Time Deal
                  </div>
                  <div class="f-md-50 f-28 w700 text-center mt10 ">
                     Get Aicademy For A Low <br class="d-none d-md-block"> One-Time- Price, No Monthly Fee.
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 text-center mt20 black-clr">
                     Use Coupon Code <span class="orange-clr w700">"Aicademy"</span> for an Additional <span class="orange-clr w700 "> $3 Discount</span> on Commercial Licence
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md50 ">
                  <div class="row mx-auto">
                     <div class="col-12 col-md-8 mt20 mt-md0 mx-auto">
                        <div class="table-wrap1 ">
                           <div class="table-head1 ">
                              <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 644.49 120" style="max-height:55px">
                                 <defs>
                                   <style>
                                     .cls-1 {
                                       fill: #fff;
                                     }
                                     .cls-2 {
                                       fill: #0af;
                                     }
                                   </style>
                                 </defs>
                                 <g id="Layer_1-2" data-name="Layer 1">
                                   <g>
                                     <g>
                                       <circle class="cls-2" cx="79.02" cy="43.5" r="7.25"></circle>
                                       <circle class="cls-2" cx="70.47" cy="78.46" r="5.46"></circle>
                                       <path class="cls-2" d="m158.03,45.28l-33.74,13.31v36.4l-45.27,16.86-27.9-10.4c-4.17,3.37-7.04,6.74-8.15,10.11-1.01,3.22-.07,6.21,2.46,8.42-5.44-1.46-8-5.25-6.96-9.76,1.24-3.62,4.79-7.68,10-12.08h.01c3.95-3.31,8.84-6.84,14.46-10.5,2.04,1.67,4.68,2.68,7.53,2.68,6.56,0,11.88-5.32,11.88-11.88s-5.32-11.88-11.88-11.88-11.88,5.32-11.88,11.88c0,1.13.15,2.21.46,3.25-8.01,5.01-14.83,10.36-19.57,15.44l-5.75-2.14v-7.66c-7.84,4.92-11.84,11.06-5.99,16.99-5.17-1.64-7.69-4.49-7.69-8.05,0-4.83,4.67-10.99,13.69-17.26l35.41-23.23c2.71,2.18,6.13,3.48,9.87,3.48,8.7,0,15.76-7.06,15.76-15.76s-7.06-15.76-15.76-15.76-15.76,7.06-15.76,15.76c0,1.59.23,3.12.68,4.57l-30.2,19.53v-9L0,45.28,79.02,0l79.01,45.28Z"></path>
                                       <path class="cls-2" d="m149.38,68.32v-19.62l-1.51.6v19.03c-1.66.37-2.9,1.96-2.9,3.85v11.69h7.31v-11.69c0-1.9-1.24-3.48-2.9-3.85Z"></path>
                                     </g>
                                     <g>
                                       <g>
                                         <path class="cls-2" d="m178.03,98.08l22.25-71.1h19.3l22.25,71.1h-17.01l-4.36-16.9h-21.7l-4.36,16.9h-16.36Zm25.74-35.99l-1.74,6.54h15.16l-1.64-6.54c-.95-3.56-1.91-7.34-2.89-11.34-.98-4-1.95-7.85-2.89-11.56h-.44c-.87,3.78-1.76,7.65-2.67,11.61-.91,3.96-1.87,7.73-2.89,11.29Z"></path>
                                         <path class="cls-2" d="m256.22,36.03c-2.69,0-4.87-.76-6.54-2.29-1.67-1.53-2.51-3.56-2.51-6.11s.83-4.58,2.51-6.11c1.67-1.53,3.85-2.29,6.54-2.29s4.87.76,6.54,2.29c1.67,1.53,2.51,3.56,2.51,6.11s-.84,4.58-2.51,6.11c-1.67,1.53-3.85,2.29-6.54,2.29Zm-7.96,62.05v-54.09h16.03v54.09h-16.03Z"></path>
                                       </g>
                                       <g>
                                         <path class="cls-1" d="m299.89,99.38c-5.02,0-9.54-1.11-13.58-3.33-4.03-2.22-7.23-5.45-9.6-9.71-2.36-4.25-3.54-9.36-3.54-15.32s1.31-11.16,3.93-15.38c2.62-4.22,6.07-7.43,10.36-9.65,4.29-2.22,8.9-3.33,13.85-3.33,3.34,0,6.31.55,8.89,1.64,2.58,1.09,4.89,2.47,6.92,4.14l-7.52,10.36c-2.55-2.11-4.98-3.16-7.31-3.16-3.85,0-6.92,1.38-9.21,4.14-2.29,2.76-3.44,6.51-3.44,11.23s1.15,8.38,3.44,11.18c2.29,2.8,5.18,4.2,8.67,4.2,1.74,0,3.45-.38,5.13-1.15,1.67-.76,3.2-1.69,4.58-2.78l6.33,10.47c-2.69,2.33-5.6,3.98-8.72,4.96-3.13.98-6.18,1.47-9.16,1.47Z"></path>
                                         <path class="cls-1" d="m339.81,99.38c-4.94,0-8.87-1.58-11.78-4.74-2.91-3.16-4.36-7.03-4.36-11.61,0-5.67,2.4-10.1,7.2-13.3,4.8-3.2,12.54-5.34,23.23-6.43-.15-2.4-.86-4.31-2.13-5.73-1.27-1.42-3.4-2.13-6.38-2.13-2.25,0-4.54.44-6.87,1.31-2.33.87-4.8,2.07-7.42,3.6l-5.78-10.58c3.42-2.11,7.07-3.82,10.96-5.13,3.89-1.31,7.94-1.96,12.16-1.96,6.91,0,12.21,2,15.92,6,3.71,4,5.56,10.14,5.56,18.43v30.97h-13.09l-1.09-5.56h-.44c-2.25,2.04-4.67,3.69-7.25,4.96-2.58,1.27-5.4,1.91-8.45,1.91Zm5.45-12.43c1.82,0,3.4-.42,4.74-1.25,1.34-.83,2.71-1.94,4.09-3.33v-9.49c-5.67.73-9.6,1.87-11.78,3.44-2.18,1.56-3.27,3.4-3.27,5.51,0,1.74.56,3.04,1.69,3.87,1.13.84,2.63,1.25,4.53,1.25Z"></path>
                                         <path class="cls-1" d="m402.95,99.38c-6.69,0-12.05-2.53-16.09-7.58-4.03-5.05-6.05-11.98-6.05-20.77,0-5.89,1.07-10.96,3.22-15.21,2.14-4.25,4.94-7.51,8.4-9.76,3.45-2.25,7.07-3.38,10.85-3.38,2.98,0,5.49.51,7.52,1.53,2.04,1.02,3.96,2.4,5.78,4.14l-.66-8.29v-18.43h16.03v76.45h-13.09l-1.09-5.34h-.44c-1.89,1.89-4.11,3.47-6.65,4.74-2.55,1.27-5.13,1.91-7.74,1.91Zm4.14-13.09c1.74,0,3.33-.36,4.74-1.09,1.42-.73,2.78-2,4.09-3.82v-22.14c-1.38-1.31-2.85-2.22-4.42-2.73-1.56-.51-3.07-.76-4.53-.76-2.55,0-4.8,1.22-6.76,3.65-1.96,2.44-2.94,6.23-2.94,11.4s.85,9.21,2.56,11.72c1.71,2.51,4.13,3.76,7.25,3.76Z"></path>
                                         <path class="cls-1" d="m470.34,99.38c-5.16,0-9.81-1.13-13.96-3.38-4.14-2.25-7.42-5.49-9.81-9.71-2.4-4.22-3.6-9.31-3.6-15.27s1.22-10.94,3.65-15.16c2.43-4.22,5.62-7.47,9.54-9.76,3.93-2.29,8.03-3.44,12.32-3.44,5.16,0,9.43,1.15,12.81,3.44,3.38,2.29,5.92,5.38,7.63,9.27,1.71,3.89,2.56,8.31,2.56,13.25,0,1.38-.07,2.74-.22,4.09-.15,1.35-.29,2.34-.44,3h-32.39c.73,3.93,2.36,6.82,4.91,8.67,2.54,1.85,5.6,2.78,9.16,2.78,3.85,0,7.74-1.2,11.67-3.6l5.34,9.71c-2.76,1.89-5.85,3.38-9.27,4.47-3.42,1.09-6.73,1.64-9.92,1.64Zm-12-34.24h19.52c0-2.98-.71-5.43-2.13-7.36-1.42-1.93-3.73-2.89-6.92-2.89-2.47,0-4.69.86-6.65,2.56-1.96,1.71-3.24,4.27-3.82,7.69Z"></path>
                                         <path class="cls-1" d="m502.62,98.08v-54.09h13.09l1.09,6.98h.44c2.25-2.25,4.65-4.2,7.2-5.83,2.54-1.64,5.6-2.45,9.16-2.45,3.85,0,6.96.78,9.32,2.34,2.36,1.56,4.23,3.8,5.62,6.71,2.4-2.47,4.94-4.6,7.63-6.38,2.69-1.78,5.82-2.67,9.38-2.67,5.82,0,10.09,1.95,12.81,5.83,2.73,3.89,4.09,9.21,4.09,15.98v33.59h-16.03v-31.52c0-3.93-.53-6.61-1.58-8.07-1.05-1.45-2.74-2.18-5.07-2.18-2.69,0-5.78,1.74-9.27,5.23v36.53h-16.03v-31.52c0-3.93-.53-6.61-1.58-8.07-1.05-1.45-2.74-2.18-5.07-2.18-2.69,0-5.74,1.74-9.16,5.23v36.53h-16.03Z"></path>
                                         <path class="cls-1" d="m602.07,119.23c-1.6,0-3-.11-4.2-.33-1.2-.22-2.34-.47-3.44-.76l2.84-12.21c.51.07,1.09.2,1.74.38.65.18,1.27.27,1.85.27,2.69,0,4.76-.65,6.22-1.96,1.45-1.31,2.54-3.02,3.27-5.13l.76-2.84-20.83-52.67h16.14l7.74,23.23c.8,2.47,1.53,4.98,2.18,7.52.65,2.55,1.34,5.16,2.07,7.85h.44c.58-2.54,1.18-5.11,1.8-7.69.62-2.58,1.25-5.14,1.91-7.69l6.54-23.23h15.38l-18.76,54.63c-1.67,4.51-3.53,8.31-5.56,11.4-2.04,3.09-4.49,5.4-7.36,6.92-2.87,1.53-6.45,2.29-10.74,2.29Z"></path>
                                       </g>
                                     </g>
                                   </g>
                                 </g>
                               </svg>
                              <div class="f-22 f-md-32 w600 lh120 text-center text-uppercase mt30 commercial-shape white-clr">Writer Commercial</div>
                           </div>
                           
                              <ul class="table-list1 pl0 f-18 lh140 w400 black-clr mb0">
                                 <li>Create Fresh & SEO-Friendly Copies</li>
                                 <li>Create Content for Any Local or Online Niche (36+ preloaded niches) </li>
                                 <li>50+ Use Case Based Advance Copywriting Templates</li>
                                 <li>Write Copy in 35+ Languages & 22+ Tones of Voice</li>
                                 <li>Generate 1000 Write Up Monthly </li>
                                 <li>Social Media Integration to Directly Share Your posts on Social Media</li>
                                 <li>Download Your Content in Word or PDF format.</li>
                                 <li>Round The Clock Support </li>
                                 <li>Regular Updates</li>
                                 <li>Step-By-Step Video Training</li>
                                 <li>Commercial License to Serve Your Clients </li>
                                 <li class="headline f-md-24">Fast Action Bonuses</li>
                                 <li>Bonus #1 - Live Training</li>
                                 <li>Bonus #2 - Video Training To Start E-Learning/Course Selling Business</li>
                                 <li>Bonus #3 - Lead Generation Workshop</li>
                                 <li>Bonus #4 - Training on How to Make Money throug Affiliate Markeitng</li>
                              </ul>
                           
                           <div class="table-btn">
                              <div class="hideme-button ">
                                 <a href="https://warriorplus.com/o2/buy/hb06pf/z9740z/pvxt4z"><img src="https://warriorplus.com/o2/btn/fn200011000/hb06pf/z9740z/353915" alt="Aicademy WriterArc Commercial" border="0" class="img-fluid d-block mx-auto" /></a>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt30 ">
                  <div class="col-12 mt20 mt-md50 ">
                     <div class="f-22 f-md-28 w600 black-clr px0 text-center lh140 ">If you have any query, simply reach to us at <a href="mailto:support@bizomart.com " class="orange-clr ">support@bizomart.com</a></div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Discounted Price Section End -->
      
      <!--final section-->
      <div class="final-section ">
         <div class="container ">
            <div class="row ">
               <div class="col-12 passport-content">
                  <div class="text-center">
                     <div class="f-45 f-md-45 lh140 w600 text-center white-clr final-shape">
                        FINAL CALL
                     </div>
                  </div>
                  <div class="col-12 f-22 f-md-32 lh140 w600 text-center white-clr mt20 p0 mb20 mb-md50">
                     You Still Have The Opportunity To Take Your Game To The Next Level… <br class="d-none d-md-block"> Remember, It’s Your Make Or Break Time!
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-12">
                  <div class="f-20 f-md-26 w600 lh140 text-center white-clr">
                     Hurry! Limited Time Only. No Monthly Fees
                  </div>
                  <div class="f-16 f-md-20 lh140 w400 text-center mt10 white-clr">
                     Use Coupon Code <span class="w700 orange-clr">Aicademy</span> for an Additional <span class="w700 orange-clr"> $3 Discount</span>
                  </div>
                  <div class="row">
                     <div class="col-12 col-md-7 mx-auto mt20">
                        <div class="f-20 f-md-35 text-center probtn1">
                           <a href="#buynow" class="cta-link-btn d-block">Get Instant Access To Aicademy</a>
                        </div>
                        <img src="https://cdn.oppyo.com/launches/writerarc/bizdrive/payment-options.webp" class="img-fluid mx-auto d-block mt20">
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-12 mt25 mt-md40 text-center">
               <a class="white-clr f-md-24 f-18 lh140 w600" href="https://warriorplus.com/o/nothanks/z9740z" target="_blank" class="link">
               No thanks - I Don't Want To Creates Stunning Content for Any Local or Online Niche 10X Faster... Please Take Me To The Next Step To Get Access To My Purchase.
               </a>
          </div>
         </div>
      </div>
      <!--final section-->
      <!--Footer Section Start -->
      <div class="footer-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 644.49 120" style="max-height:55px">
                     <defs>
                       <style>
                         .cls-1 {
                           fill: #fff;
                         }
                         .cls-2 {
                           fill: #0af;
                         }
                       </style>
                     </defs>
                     <g id="Layer_1-2" data-name="Layer 1">
                       <g>
                         <g>
                           <circle class="cls-2" cx="79.02" cy="43.5" r="7.25"></circle>
                           <circle class="cls-2" cx="70.47" cy="78.46" r="5.46"></circle>
                           <path class="cls-2" d="m158.03,45.28l-33.74,13.31v36.4l-45.27,16.86-27.9-10.4c-4.17,3.37-7.04,6.74-8.15,10.11-1.01,3.22-.07,6.21,2.46,8.42-5.44-1.46-8-5.25-6.96-9.76,1.24-3.62,4.79-7.68,10-12.08h.01c3.95-3.31,8.84-6.84,14.46-10.5,2.04,1.67,4.68,2.68,7.53,2.68,6.56,0,11.88-5.32,11.88-11.88s-5.32-11.88-11.88-11.88-11.88,5.32-11.88,11.88c0,1.13.15,2.21.46,3.25-8.01,5.01-14.83,10.36-19.57,15.44l-5.75-2.14v-7.66c-7.84,4.92-11.84,11.06-5.99,16.99-5.17-1.64-7.69-4.49-7.69-8.05,0-4.83,4.67-10.99,13.69-17.26l35.41-23.23c2.71,2.18,6.13,3.48,9.87,3.48,8.7,0,15.76-7.06,15.76-15.76s-7.06-15.76-15.76-15.76-15.76,7.06-15.76,15.76c0,1.59.23,3.12.68,4.57l-30.2,19.53v-9L0,45.28,79.02,0l79.01,45.28Z"></path>
                           <path class="cls-2" d="m149.38,68.32v-19.62l-1.51.6v19.03c-1.66.37-2.9,1.96-2.9,3.85v11.69h7.31v-11.69c0-1.9-1.24-3.48-2.9-3.85Z"></path>
                         </g>
                         <g>
                           <g>
                             <path class="cls-2" d="m178.03,98.08l22.25-71.1h19.3l22.25,71.1h-17.01l-4.36-16.9h-21.7l-4.36,16.9h-16.36Zm25.74-35.99l-1.74,6.54h15.16l-1.64-6.54c-.95-3.56-1.91-7.34-2.89-11.34-.98-4-1.95-7.85-2.89-11.56h-.44c-.87,3.78-1.76,7.65-2.67,11.61-.91,3.96-1.87,7.73-2.89,11.29Z"></path>
                             <path class="cls-2" d="m256.22,36.03c-2.69,0-4.87-.76-6.54-2.29-1.67-1.53-2.51-3.56-2.51-6.11s.83-4.58,2.51-6.11c1.67-1.53,3.85-2.29,6.54-2.29s4.87.76,6.54,2.29c1.67,1.53,2.51,3.56,2.51,6.11s-.84,4.58-2.51,6.11c-1.67,1.53-3.85,2.29-6.54,2.29Zm-7.96,62.05v-54.09h16.03v54.09h-16.03Z"></path>
                           </g>
                           <g>
                             <path class="cls-1" d="m299.89,99.38c-5.02,0-9.54-1.11-13.58-3.33-4.03-2.22-7.23-5.45-9.6-9.71-2.36-4.25-3.54-9.36-3.54-15.32s1.31-11.16,3.93-15.38c2.62-4.22,6.07-7.43,10.36-9.65,4.29-2.22,8.9-3.33,13.85-3.33,3.34,0,6.31.55,8.89,1.64,2.58,1.09,4.89,2.47,6.92,4.14l-7.52,10.36c-2.55-2.11-4.98-3.16-7.31-3.16-3.85,0-6.92,1.38-9.21,4.14-2.29,2.76-3.44,6.51-3.44,11.23s1.15,8.38,3.44,11.18c2.29,2.8,5.18,4.2,8.67,4.2,1.74,0,3.45-.38,5.13-1.15,1.67-.76,3.2-1.69,4.58-2.78l6.33,10.47c-2.69,2.33-5.6,3.98-8.72,4.96-3.13.98-6.18,1.47-9.16,1.47Z"></path>
                             <path class="cls-1" d="m339.81,99.38c-4.94,0-8.87-1.58-11.78-4.74-2.91-3.16-4.36-7.03-4.36-11.61,0-5.67,2.4-10.1,7.2-13.3,4.8-3.2,12.54-5.34,23.23-6.43-.15-2.4-.86-4.31-2.13-5.73-1.27-1.42-3.4-2.13-6.38-2.13-2.25,0-4.54.44-6.87,1.31-2.33.87-4.8,2.07-7.42,3.6l-5.78-10.58c3.42-2.11,7.07-3.82,10.96-5.13,3.89-1.31,7.94-1.96,12.16-1.96,6.91,0,12.21,2,15.92,6,3.71,4,5.56,10.14,5.56,18.43v30.97h-13.09l-1.09-5.56h-.44c-2.25,2.04-4.67,3.69-7.25,4.96-2.58,1.27-5.4,1.91-8.45,1.91Zm5.45-12.43c1.82,0,3.4-.42,4.74-1.25,1.34-.83,2.71-1.94,4.09-3.33v-9.49c-5.67.73-9.6,1.87-11.78,3.44-2.18,1.56-3.27,3.4-3.27,5.51,0,1.74.56,3.04,1.69,3.87,1.13.84,2.63,1.25,4.53,1.25Z"></path>
                             <path class="cls-1" d="m402.95,99.38c-6.69,0-12.05-2.53-16.09-7.58-4.03-5.05-6.05-11.98-6.05-20.77,0-5.89,1.07-10.96,3.22-15.21,2.14-4.25,4.94-7.51,8.4-9.76,3.45-2.25,7.07-3.38,10.85-3.38,2.98,0,5.49.51,7.52,1.53,2.04,1.02,3.96,2.4,5.78,4.14l-.66-8.29v-18.43h16.03v76.45h-13.09l-1.09-5.34h-.44c-1.89,1.89-4.11,3.47-6.65,4.74-2.55,1.27-5.13,1.91-7.74,1.91Zm4.14-13.09c1.74,0,3.33-.36,4.74-1.09,1.42-.73,2.78-2,4.09-3.82v-22.14c-1.38-1.31-2.85-2.22-4.42-2.73-1.56-.51-3.07-.76-4.53-.76-2.55,0-4.8,1.22-6.76,3.65-1.96,2.44-2.94,6.23-2.94,11.4s.85,9.21,2.56,11.72c1.71,2.51,4.13,3.76,7.25,3.76Z"></path>
                             <path class="cls-1" d="m470.34,99.38c-5.16,0-9.81-1.13-13.96-3.38-4.14-2.25-7.42-5.49-9.81-9.71-2.4-4.22-3.6-9.31-3.6-15.27s1.22-10.94,3.65-15.16c2.43-4.22,5.62-7.47,9.54-9.76,3.93-2.29,8.03-3.44,12.32-3.44,5.16,0,9.43,1.15,12.81,3.44,3.38,2.29,5.92,5.38,7.63,9.27,1.71,3.89,2.56,8.31,2.56,13.25,0,1.38-.07,2.74-.22,4.09-.15,1.35-.29,2.34-.44,3h-32.39c.73,3.93,2.36,6.82,4.91,8.67,2.54,1.85,5.6,2.78,9.16,2.78,3.85,0,7.74-1.2,11.67-3.6l5.34,9.71c-2.76,1.89-5.85,3.38-9.27,4.47-3.42,1.09-6.73,1.64-9.92,1.64Zm-12-34.24h19.52c0-2.98-.71-5.43-2.13-7.36-1.42-1.93-3.73-2.89-6.92-2.89-2.47,0-4.69.86-6.65,2.56-1.96,1.71-3.24,4.27-3.82,7.69Z"></path>
                             <path class="cls-1" d="m502.62,98.08v-54.09h13.09l1.09,6.98h.44c2.25-2.25,4.65-4.2,7.2-5.83,2.54-1.64,5.6-2.45,9.16-2.45,3.85,0,6.96.78,9.32,2.34,2.36,1.56,4.23,3.8,5.62,6.71,2.4-2.47,4.94-4.6,7.63-6.38,2.69-1.78,5.82-2.67,9.38-2.67,5.82,0,10.09,1.95,12.81,5.83,2.73,3.89,4.09,9.21,4.09,15.98v33.59h-16.03v-31.52c0-3.93-.53-6.61-1.58-8.07-1.05-1.45-2.74-2.18-5.07-2.18-2.69,0-5.78,1.74-9.27,5.23v36.53h-16.03v-31.52c0-3.93-.53-6.61-1.58-8.07-1.05-1.45-2.74-2.18-5.07-2.18-2.69,0-5.74,1.74-9.16,5.23v36.53h-16.03Z"></path>
                             <path class="cls-1" d="m602.07,119.23c-1.6,0-3-.11-4.2-.33-1.2-.22-2.34-.47-3.44-.76l2.84-12.21c.51.07,1.09.2,1.74.38.65.18,1.27.27,1.85.27,2.69,0,4.76-.65,6.22-1.96,1.45-1.31,2.54-3.02,3.27-5.13l.76-2.84-20.83-52.67h16.14l7.74,23.23c.8,2.47,1.53,4.98,2.18,7.52.65,2.55,1.34,5.16,2.07,7.85h.44c.58-2.54,1.18-5.11,1.8-7.69.62-2.58,1.25-5.14,1.91-7.69l6.54-23.23h15.38l-18.76,54.63c-1.67,4.51-3.53,8.31-5.56,11.4-2.04,3.09-4.49,5.4-7.36,6.92-2.87,1.53-6.45,2.29-10.74,2.29Z"></path>
                           </g>
                         </g>
                       </g>
                     </g>
                   </svg>
                  <div editabletype="text" class="f-16 f-md-16 w300 mt20 lh140 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
                  <div class=" mt20 mt-md40 white-clr"> <script type="text/javascript" src="https://warriorplus.com/o2/disclaimer/hb06pf" defer=""></script><div class="wplus_spdisclaimer f-17 w300 mt20 lh140 white-clr text-center"><strong>Disclaimer:</strong> <em>WarriorPlus is used to help manage the sale of products on this site. While WarriorPlus helps facilitate the sale, all payments are made directly to the product vendor and NOT WarriorPlus. Thus, all product questions, support inquiries and/or refund requests must be sent to the vendor. WarriorPlus's role should not be construed as an endorsement, approval or review of these products or any claim, statement or opinion used in the marketing of these products.</em></div></div>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-16 f-md-16 w300 lh140 white-clr text-xs-center">Copyright © Aicademy</div>
                  <ul class="footer-ul w300 f-16 f-md-16 white-clr text-center text-md-right">
                     <li><a href="https://support.bizomart.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://aicademy.live/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://aicademy.live/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://aicademy.live/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://aicademy.live/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://aicademy.live/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://aicademy.live/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!--Footer Section End -->
      <!-- Exit Popup and Timer -->
         <?php include('timer.php'); ?>
      <!-- Exit Popup and Timer End -->
   </body>
</html>
<!Doctype html>
<html>
   <head>
      <title>Aicademy | Traffic </title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=9">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
      <meta name="title" content="Aicademy | Traffic">
      <meta name="description" content="Brand New AI APP Creates 100s of Reels, Boomerang, & Short Videos With Just One Keyword & Drive Floods Of FREE Traffic And Sales">
      <meta name="keywords" content="Aicademy | Traffic">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta property="og:image" content="https://www.aicademy.live/traffic/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Pranshu Gupta">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="Aicademy | Traffic">
      <meta property="og:description" content="Brand New AI APP Creates 100s of Reels, Boomerang, & Short Videos With Just One Keyword & Drive Floods Of FREE Traffic And Sales">
      <meta property="og:image" content="https://www.aicademy.live/traffic/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="Aicademy | Traffic">
      <meta property="twitter:description" content="Brand New AI APP Creates 100s of Reels, Boomerang, & Short Videos With Just One Keyword & Drive Floods Of FREE Traffic And Sales">
      <meta property="twitter:image" content="https://www.aicademy.live/traffic/thumbnail.png">
      <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
      <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
      <link rel="stylesheet" href="https://cdn.oppyotest.com/launches/sellero/common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style.css" type="text/css">
      <link rel="stylesheet" href="assets/css/timer.css" type="text/css">
      <script src="../common_assets/js/jquery.min.js"></script>
      <script src="../common_assets/js/bootstrap.min.js"></script>
      <script src="assets/js/timer.js"></script>
   </head>
   <body>
   <div class="warning-section">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <div class="offer">
                        <div>
                            <img src="https://cdn.oppyo.com/launches/appzilo/special/error.webp">
                        </div>
                        <div class="f-22 f-md-30 w500 lh140 white-clr text-center">
                            <span class="w600">ATTENTION: </span> Avoid This Offer & Be Ready To Pay A Fortune...
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
      <!-- Header Section Start -->
      <div class="header-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
               <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 644.49 120" style="max-height:65px">
                                 <defs>
                                    <style>
                                       .cls-1 {
                                       fill: #fff;
                                       }
                                 
                                       .cls-2 {
                                       fill: #0af;
                                       }
                                    </style>
                                 </defs>
                                 <g id="Layer_1-2" data-name="Layer 1">
                                    <g>
                                       <g>
                                          <circle class="cls-2" cx="79.02" cy="43.5" r="7.25"></circle>
                                          <circle class="cls-2" cx="70.47" cy="78.46" r="5.46"></circle>
                                          <path class="cls-2" d="m158.03,45.28l-33.74,13.31v36.4l-45.27,16.86-27.9-10.4c-4.17,3.37-7.04,6.74-8.15,10.11-1.01,3.22-.07,6.21,2.46,8.42-5.44-1.46-8-5.25-6.96-9.76,1.24-3.62,4.79-7.68,10-12.08h.01c3.95-3.31,8.84-6.84,14.46-10.5,2.04,1.67,4.68,2.68,7.53,2.68,6.56,0,11.88-5.32,11.88-11.88s-5.32-11.88-11.88-11.88-11.88,5.32-11.88,11.88c0,1.13.15,2.21.46,3.25-8.01,5.01-14.83,10.36-19.57,15.44l-5.75-2.14v-7.66c-7.84,4.92-11.84,11.06-5.99,16.99-5.17-1.64-7.69-4.49-7.69-8.05,0-4.83,4.67-10.99,13.69-17.26l35.41-23.23c2.71,2.18,6.13,3.48,9.87,3.48,8.7,0,15.76-7.06,15.76-15.76s-7.06-15.76-15.76-15.76-15.76,7.06-15.76,15.76c0,1.59.23,3.12.68,4.57l-30.2,19.53v-9L0,45.28,79.02,0l79.01,45.28Z"></path>
                                          <path class="cls-2" d="m149.38,68.32v-19.62l-1.51.6v19.03c-1.66.37-2.9,1.96-2.9,3.85v11.69h7.31v-11.69c0-1.9-1.24-3.48-2.9-3.85Z"></path>
                                       </g>
                                       <g>
                                          <g>
                                          <path class="cls-2" d="m178.03,98.08l22.25-71.1h19.3l22.25,71.1h-17.01l-4.36-16.9h-21.7l-4.36,16.9h-16.36Zm25.74-35.99l-1.74,6.54h15.16l-1.64-6.54c-.95-3.56-1.91-7.34-2.89-11.34-.98-4-1.95-7.85-2.89-11.56h-.44c-.87,3.78-1.76,7.65-2.67,11.61-.91,3.96-1.87,7.73-2.89,11.29Z"></path>
                                          <path class="cls-2" d="m256.22,36.03c-2.69,0-4.87-.76-6.54-2.29-1.67-1.53-2.51-3.56-2.51-6.11s.83-4.58,2.51-6.11c1.67-1.53,3.85-2.29,6.54-2.29s4.87.76,6.54,2.29c1.67,1.53,2.51,3.56,2.51,6.11s-.84,4.58-2.51,6.11c-1.67,1.53-3.85,2.29-6.54,2.29Zm-7.96,62.05v-54.09h16.03v54.09h-16.03Z"></path>
                                          </g>
                                          <g>
                                          <path class="cls-1" d="m299.89,99.38c-5.02,0-9.54-1.11-13.58-3.33-4.03-2.22-7.23-5.45-9.6-9.71-2.36-4.25-3.54-9.36-3.54-15.32s1.31-11.16,3.93-15.38c2.62-4.22,6.07-7.43,10.36-9.65,4.29-2.22,8.9-3.33,13.85-3.33,3.34,0,6.31.55,8.89,1.64,2.58,1.09,4.89,2.47,6.92,4.14l-7.52,10.36c-2.55-2.11-4.98-3.16-7.31-3.16-3.85,0-6.92,1.38-9.21,4.14-2.29,2.76-3.44,6.51-3.44,11.23s1.15,8.38,3.44,11.18c2.29,2.8,5.18,4.2,8.67,4.2,1.74,0,3.45-.38,5.13-1.15,1.67-.76,3.2-1.69,4.58-2.78l6.33,10.47c-2.69,2.33-5.6,3.98-8.72,4.96-3.13.98-6.18,1.47-9.16,1.47Z"></path>
                                          <path class="cls-1" d="m339.81,99.38c-4.94,0-8.87-1.58-11.78-4.74-2.91-3.16-4.36-7.03-4.36-11.61,0-5.67,2.4-10.1,7.2-13.3,4.8-3.2,12.54-5.34,23.23-6.43-.15-2.4-.86-4.31-2.13-5.73-1.27-1.42-3.4-2.13-6.38-2.13-2.25,0-4.54.44-6.87,1.31-2.33.87-4.8,2.07-7.42,3.6l-5.78-10.58c3.42-2.11,7.07-3.82,10.96-5.13,3.89-1.31,7.94-1.96,12.16-1.96,6.91,0,12.21,2,15.92,6,3.71,4,5.56,10.14,5.56,18.43v30.97h-13.09l-1.09-5.56h-.44c-2.25,2.04-4.67,3.69-7.25,4.96-2.58,1.27-5.4,1.91-8.45,1.91Zm5.45-12.43c1.82,0,3.4-.42,4.74-1.25,1.34-.83,2.71-1.94,4.09-3.33v-9.49c-5.67.73-9.6,1.87-11.78,3.44-2.18,1.56-3.27,3.4-3.27,5.51,0,1.74.56,3.04,1.69,3.87,1.13.84,2.63,1.25,4.53,1.25Z"></path>
                                          <path class="cls-1" d="m402.95,99.38c-6.69,0-12.05-2.53-16.09-7.58-4.03-5.05-6.05-11.98-6.05-20.77,0-5.89,1.07-10.96,3.22-15.21,2.14-4.25,4.94-7.51,8.4-9.76,3.45-2.25,7.07-3.38,10.85-3.38,2.98,0,5.49.51,7.52,1.53,2.04,1.02,3.96,2.4,5.78,4.14l-.66-8.29v-18.43h16.03v76.45h-13.09l-1.09-5.34h-.44c-1.89,1.89-4.11,3.47-6.65,4.74-2.55,1.27-5.13,1.91-7.74,1.91Zm4.14-13.09c1.74,0,3.33-.36,4.74-1.09,1.42-.73,2.78-2,4.09-3.82v-22.14c-1.38-1.31-2.85-2.22-4.42-2.73-1.56-.51-3.07-.76-4.53-.76-2.55,0-4.8,1.22-6.76,3.65-1.96,2.44-2.94,6.23-2.94,11.4s.85,9.21,2.56,11.72c1.71,2.51,4.13,3.76,7.25,3.76Z"></path>
                                          <path class="cls-1" d="m470.34,99.38c-5.16,0-9.81-1.13-13.96-3.38-4.14-2.25-7.42-5.49-9.81-9.71-2.4-4.22-3.6-9.31-3.6-15.27s1.22-10.94,3.65-15.16c2.43-4.22,5.62-7.47,9.54-9.76,3.93-2.29,8.03-3.44,12.32-3.44,5.16,0,9.43,1.15,12.81,3.44,3.38,2.29,5.92,5.38,7.63,9.27,1.71,3.89,2.56,8.31,2.56,13.25,0,1.38-.07,2.74-.22,4.09-.15,1.35-.29,2.34-.44,3h-32.39c.73,3.93,2.36,6.82,4.91,8.67,2.54,1.85,5.6,2.78,9.16,2.78,3.85,0,7.74-1.2,11.67-3.6l5.34,9.71c-2.76,1.89-5.85,3.38-9.27,4.47-3.42,1.09-6.73,1.64-9.92,1.64Zm-12-34.24h19.52c0-2.98-.71-5.43-2.13-7.36-1.42-1.93-3.73-2.89-6.92-2.89-2.47,0-4.69.86-6.65,2.56-1.96,1.71-3.24,4.27-3.82,7.69Z"></path>
                                          <path class="cls-1" d="m502.62,98.08v-54.09h13.09l1.09,6.98h.44c2.25-2.25,4.65-4.2,7.2-5.83,2.54-1.64,5.6-2.45,9.16-2.45,3.85,0,6.96.78,9.32,2.34,2.36,1.56,4.23,3.8,5.62,6.71,2.4-2.47,4.94-4.6,7.63-6.38,2.69-1.78,5.82-2.67,9.38-2.67,5.82,0,10.09,1.95,12.81,5.83,2.73,3.89,4.09,9.21,4.09,15.98v33.59h-16.03v-31.52c0-3.93-.53-6.61-1.58-8.07-1.05-1.45-2.74-2.18-5.07-2.18-2.69,0-5.78,1.74-9.27,5.23v36.53h-16.03v-31.52c0-3.93-.53-6.61-1.58-8.07-1.05-1.45-2.74-2.18-5.07-2.18-2.69,0-5.74,1.74-9.16,5.23v36.53h-16.03Z"></path>
                                          <path class="cls-1" d="m602.07,119.23c-1.6,0-3-.11-4.2-.33-1.2-.22-2.34-.47-3.44-.76l2.84-12.21c.51.07,1.09.2,1.74.38.65.18,1.27.27,1.85.27,2.69,0,4.76-.65,6.22-1.96,1.45-1.31,2.54-3.02,3.27-5.13l.76-2.84-20.83-52.67h16.14l7.74,23.23c.8,2.47,1.53,4.98,2.18,7.52.65,2.55,1.34,5.16,2.07,7.85h.44c.58-2.54,1.18-5.11,1.8-7.69.62-2.58,1.25-5.14,1.91-7.69l6.54-23.23h15.38l-18.76,54.63c-1.67,4.51-3.53,8.31-5.56,11.4-2.04,3.09-4.49,5.4-7.36,6.92-2.87,1.53-6.45,2.29-10.74,2.29Z"></path>
                                          </g>
                                       </g>
                                    </g>
                                 </g>
                              </svg>
                           </div>
               <div class="col-12 text-center mt20 mt-md50">
                  <div class="pre-heading f-18 f-md-18 w400 lh140 white-clr">
                     Make BIG Profits in 2023 by Copying a Proven System That Make Us $545/Day Over and Over Again…
                  </div>
               </div>
               <div class="col-12 mt-md50 mt30 f-md-45 f-28 w700 text-center white-clr lh140 text-capitalize">
                  Brand New AI APP <span class="yellow-clr">Creates 100s  Reels, Boomerang, & Short Videos with Just One Keyword…</span> for Floods of FREE Traffic and Sales
               </div>
               <div class="col-12 mt-md25 mt20 f-16 f-md-20 w400 text-center lh140 white-clr text-capitalize">
               Even A Complete Beginner Can Drive Thousands of Visitors to Any Offer or Link  Instantly <br> |  No Camera | No Writing & Editing | No Paid Traffic Ever…
               </div>
            </div>
            <div class="row d-flex align-items-center flex-wrap mt20 mt-md40">
               <div class="col-md-7 col-12 min-md-video-width-left">
                  <img src="assets/images/product-image.webp" class="img-fluid mx-auto d-block">
                  <!-- <div class="col-12 responsive-video border-video">
                     <iframe src="https://vidmaster.dotcompal.com/video/embed/8x6aokyyl7"
                        style="width:100%; height:100%" frameborder="0" allow="fullscreen"
                        allowfullscreen=""></iframe>
                     </div> -->
                  <div class="col-12 mt40 d-none d-md-block">
                     <div class="f-20 f-md-28 w700 lh140 text-center white-clr">
                        Bonus Agency License Included for Time!
                     </div>
                     <!-- <div class="f-18 f-md-20 w400 white-clr lh140 mt10">
                        Use Coupon Code <span class="red-clr w600">"VidMaster"</span>  for an Additional <span class="red-clr w600"> $3 Discount</span>
                     </div> -->
                     <div class="row">
                        <div class="col-md-11 mx-auto col-12 mt20 mt-md20 text-center">
                           <a href="#buynow" class="cta-link-btn d-block px0">Get Instant Access To Aicademy</a>
                        </div>
                     </div>
                     <div class="col-12 col-md-10 mx-auto d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                        <img src="assets/images/compaitable-with1.webp"
                           class="img-fluid mx-xs-center md-img-right" alt="visa">
                        <div class="d-md-block d-none visible-md px-md30"><img
                           src="assets/images/v-line.webp" class="img-fluid"
                           alt="line">
                        </div>
                        <img src="assets/images/days-gurantee1.webp"
                           class="img-fluid mx-xs-auto mt15 mt-md0" alt="30 days">
                     </div>
                     <!-- <div class="col-12 col-md-8 mx-auto mt30 d-none d-md-block">
                        <img src="assets/images/limited-time.webp" class="img-fluid d-block mx-auto">
                     </div> -->
                  </div>
               </div>
               <div class="col-md-5 col-12 mt20 mt-md0 min-md-video-width-right">
                  <div class="calendar-wrap mb20 mb-md0">
                     <ul class="list-head pl0 m0 f-18 lh140 w400 white-clr">
                     <li>Drive Tsunami of FREE Traffic in Any Niche</li>
                     <li>Create Reels & Shorts Using Just One Keyword </li>
                     <li>Create Boomerang Video to Engage Audience</li>
                     <li>Get Thousands of Visitors & Sales to Any Offer or Page</li>
                     <li>Make Tons of Affiliate Commissions or Ad Profits</li> 
                     <li>Create Short Videos from Any Video, Text, or Just a Keyword</li>
                     <li>Add Background Music and/or Voiceover to Any Video</li>
                     <li>Pre-designed 50+ Shorts Video templates</li>
                     <li>25+ Vector Images to Choose From</li>
                     <li>100+ Social Sharing Platforms ForFREE Viral Traffic</li>
                     <li>100% Newbie Friendly, No Prior Experience Or Tech Skills Needed</li>
                     <li>No Camera Recording, No Voice, Or Complex Editing Required</li>
                     <li>Free Commercial License To Sell Video Services For High Profits</li>
                     </ul>
                  </div>
                  <div class="d-md-none">
                     <div class="f-18 f-md-26 w700 lh140 text-center white-clr">
                        Free Commercial License + Low 1-Time Price For Launch Period Only 
                     </div>
                     <div class="row">
                        <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center">
                           <a href="#buynow" class="cta-link-btn d-block px0">Get Instant Access To Aicademy</a>
                        </div>
                     </div>
                     <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                        <img src="assets/images/compaitable-with1.webp"
                           class="img-responsive mx-xs-center md-img-right" alt="visa">
                        <div class="d-md-block d-none visible-md px-md30"><img
                           src="assets/images/v-line.webp" class="img-fluid"
                           alt="line">
                        </div>
                        <img src="assets/images/days-gurantee1.webp"
                           class="img-responsive mx-xs-auto mt15 mt-md0" alt="30 days">
                     </div>
                     <!-- <div class="col-12 col-md-8 mx-auto mt30 d-md-none">
                        <img src="assets/images/limited-time.webp" class="img-fluid d-block mx-auto">
                     </div> -->
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Header Section End -->

      <!-- Step Section -->
      <div class="step-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-39 f-28 w600 text-center black-clr2 lh140">
                  Create Stunning Stories, Reels & Shorts for Massive Traffic To Any Offer, Page, or Link <span class="red-clr w700">in Just 3 Easy Steps</span>
               </div>
            </div>
            <div class="row mt20 mt-md50">
               <div class="col-12 col-md-4">
                  <div class="step-block">
                     <div class="step-title f-20 f-md-22 w700">Step 1</div>
                     <img src="assets/images/step-1.webp" class="img-fluid d-block mx-auto mt15 mt-md30">
                     <div class="step-content">
                        <div class="f-22 f-md-28 w600 black-clr2 mt10">Choose Platform</div>
                        <div class="w400 f-18 f-md-20 lh140 mt10 black-clr2">
                           Choose the social platform- YouTube, Instagram, TikTok, Facebook, or Snapchat you want to create Reel or Shorts for
                        </div>
                     </div>  
                  </div>
               </div>
               <div class="col-12 col-md-4 mt20 mt-md0">
                  <div class="step-block">
                     <div class="step-title f-20 f-md-22 w700">Step 2</div>
                     <img src="assets/images/step-2.webp" class="img-fluid d-block mx-auto mt15 mt-md30">
                     <div class="step-content">
                        <div class="f-22 f-md-28 w600 black-clr2 mt10">Enter a Keyword </div>
                        <div class="w400 f-18 f-md-20 lh140 mt10 black-clr2">
                           Enter the desired Keyword & you will get several short videos. Select any video & customize it by adding background, vectors, audio, & fonts to make it engaging
                        </div>
                     </div>  
                  </div>
               </div>
               <div class="col-12 col-md-4 mt20 mt-md0">
                  <div class="step-block">
                     <div class="step-title f-20 f-md-22 w700">Step 3</div>
                     <img src="assets/images/step-3.webp" class="img-fluid d-block mx-auto mt15 mt-md30">
                     <div class="step-content">
                        <div class="f-22 f-md-28 w600 black-clr2 mt10">Publish and Profit</div>
                        <div class="w400 f-18 f-md-20 lh140 mt10 black-clr2">
                           Now publish your Reels & Shortson different social platforms with a click to drive unlimited viral traffic & sales on your offer, page, or website.
                        </div>
                     </div>  
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md70">
               <div class="col-12 text-center">
                  <div class="f-20 f-md-24 w600 lh140">
                     No Download/Installation  |  No Prior Knowledge  |  100% Beginners Friendly
                  </div>
                  <div class="f-18 f-md-20 w400 mt15 mt-md30 lh140">
                     In just 60 seconds, you can create your first profitable Reels & Shorts to enjoy MASSIVE FREE Traffic, Sales & Profits coming into your bank account on autopilot.
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Step Section End -->

      <!-- Proof Section Start -->
      <div class="proof-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-40 f-28 w700 lh140 text-center black-clr2">
                  Got 34,309 Targeted Visitors in Last 30 Days…                
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
                  <img src="assets/images/proof.webp" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 mt20 mt-md50 f-md-40 f-28 w700 lh140 text-center black-clr2">   
                  And Making Consistent $535 In Profits <br class="d-none d-md-block"> Each & Every Day        
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
                  <img src="assets/images/proof1.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>
         </div>
      </div>
      <!-- Proof Section End -->

      <!-- Testimonials Section -->
      <div class="testimonial-section">
         <div class="container ">
            <div class="row ">
               <div class="f-md-44 f-28 w700 lh140 text-center black-clr2">
                  Checkout What Aicademy Early Users Have to SAY
               </div>
            </div>
            <div class="row mt25 mt-md50 align-items-center">
               <div class="col-12 col-md-10">
                  <div class="testi-block">
                     <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-12 col-md-3 order-md-2">
                           <img src="assets/images/c1.webp" class="img-fluid d-block mx-auto">
                           <div class="f-22 f-md-24 w700 lh120 red-clr mt20 text-center">Benjamin Davis</div>
                        </div>
                        <div class="col-12 col-md-9 order-md-1 mt20 mt-md0 text-center text-md-start">
                           <div class="f-22 f-md-28 w700 lh140">I got 2500+ Views & 509 Subscribers in 4 days</div>
                           <div class="f-20 w400 lh140 mt20 quote1">
                              Hi, I am from Australia and promote weight loss products on ClickBank & Amazon as an affiliate. I was struggling to get traffic on my offers and my YouTube channels have only 257 subscribers the reason is that I could not create and post Videos and Shorts regularly as it takes lots of effort and time. But recently, I got access to Aicademy, I created around 15 Short Videos with almost no effort, that was amazingly EASY. And <span class="w700">I got 2500+ Views & 509 Subscribers in 4 days</span> on my weight loss channel. Kudos to the team for creating a solution that can make the complete process fast & easy. 
                           </div> 
                         </div>
                     </div>
                 </div>
               </div>
               <div class="col-12 col-md-10 offset-md-2 mt20 mt-md40">
                  <div class="testi-block">
                     <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-12 col-md-3">
                           <img src="assets/images/c2.webp" class="img-fluid d-block mx-auto">
                           <div class="f-22 f-md-24 w700 red-clr mt20 text-center">Liam Smith</div>
                        </div>
                        <div class="col-12 col-md-9 mt20 mt-md0 text-center text-md-start">
                           <div class="f-22 f-md-28 w700 lh140">Best part about Aicademy is that it takes away all hassles</div>
                           <div class="f-20 w400 lh140 mt20 quote1">
                              Thanks for the early access, Aicademy Team. <span class="w700">The best part about Aicademy is that it takes away</span> all hassles like video recording or editing, voiceover recording, background music and driving traffic to your offers, etc. I am sure this will give you complete value for your money and help you get the desired success in the long run.
                           </div>  
                        </div>
                     </div>
                 </div>
               </div>
               <div class="col-12 col-md-10 mt20 mt-md40">
                  <div class="testi-block">
                     <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-12 col-md-3 order-md-2">
                           <img src="assets/images/c3.webp" class="img-fluid d-block mx-auto">
                           <div class="f-22 f-md-24 w700 lh120 red-clr mt20 text-center">Noah Brown</div>
                        </div>
                        <div class="col-12 col-md-9 order-md-1 mt20 mt-md0 text-center text-md-start">
                           <div class="f-22 f-md-28 w700 lh140">Create super engaging video shorts for YouTube</div>
                           <div class="f-20 w400 lh140 mt20 quote1">
                              Aicademy is the perfect platform to <span class="w700">create super engaging video shorts for YouTube and social media and drive tons of viral traffic in a stress-free manner.</span> I am enjoying all the functionalities of this amazing software. If someone asks me to give marks, I would love to give ten-on-ten to this fabulous product.
                           </div> 
                        </div>
                     </div>
                 </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Testimonials Section End -->

      <!-- CTA Section-->
      <div class="cta-section-new">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-18 f-md-28 w700 lh140 text-center white-clr">
                     Free Commercial License + Low 1-Time Price For Launch Period Only
                  </div>
                  <!-- <div class="f-18 f-md-20 w400 lh140 text-center mt5 white-clr">
                     Use Discount Coupon <span class="red-clr w600"> "Aicademy" </span> for Instant <span class="red-clr w600"> $3 OFF </span>
                 </div> -->
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 mt-md30 text-center">
                        <a href="#buynow" class="cta-link-btn">Get Instant Access To Aicademy</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                     <img src="assets/images/compaitable-with1.webp" class="img-fluid mx-xs-center md-img-right" alt="visa">
                     <div class="d-md-block d-none visible-md px-md30">
                        <img src="assets/images/v-line.webp" class="img-fluid" alt="line">
                     </div>
                     <img src="assets/images/days-gurantee1.webp" class="img-fluid mx-xs-auto mt15 mt-md0" alt="30 days">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- CTA Section End-->

      <!-- Reels Section Start -->
      <section class="reel-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-45 w700 lh140 black-clr2">
                     Checkout Some of Reels & Shorts <br class="d-none d-md-block">
                     Videos Created Using Aicademy
                  </div>
               </div>
            </div>
            <div class="row row-cols-md-3 row-cols-xl-3 row-cols-1  mt-md50">
               <div class="col mt20 mt-md100">
               <video width="100%" height="auto" loop autoplay muted="muted" class="mp4-border" controls>
                   <source src="assets/images/reel1.mp4" type="video/mp4">
               </video>
               </div>
               <div class="col mt20 mt-md0">
               <video width="100%" height="auto" loop autoplay muted="muted" class="mp4-border" controls>
                   <source src="assets/images/reel2.mp4" type="video/mp4">
               </video>
               </div>
               <div class="col mt20 mt-md60">
               <video width="100%" height="auto" loop autoplay muted="muted" class="mp4-border" controls>
                   <source src="assets/images/reel3.mp4" type="video/mp4">
               </video>
               </div>
               <div class="col mt20 mt-md30">
               <video width="100%" height="auto" loop autoplay muted="muted" class="mp4-border" controls>
                   <source src="assets/images/reel4.mp4" type="video/mp4">
               </video>
               </div>
               <div class="col mt20">
               <video width="100%" height="auto" loop autoplay muted="muted" class="mt-md-90 mp4-border" controls>
                   <source src="assets/images/reel5.mp4" type="video/mp4">
               </video>
               </div>
               <div class="col mt20 mt-md40">
               <video width="100%" height="auto" loop autoplay muted="muted" class="mt-md-50 mp4-border" controls>
                   <source src="assets/images/reel6.mp4" type="video/mp4">
               </video>
               </div>
            </div>
         </div>
      </section>
      <!-- Reels Section End -->

      <!-- Ground-Breaking Section Start -->
      <div class="ground-breaking">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-40 f-28 w700 text-center black-clr2 lh140">
                  Aicademy Is Packed with Tons of Cool Features
               </div>
            </div>
            <div class="row row-cols-md-3 row-cols-xl-3 row-cols-1">
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat1.webp" alt="Create Niche based short video" class="img-fluid">
                     <div class="sep-line">
                     </div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Create Niche based short video</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat2.webp" alt="Create Video from Text" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">
                        Create Video from Text
                     </div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat3.webp" alt="Create Video Using Video Clips" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Create Video Using Video Clips</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat4.webp" alt="Create Video from Images" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Create Video from Images</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat5.webp" alt="Create Boomerang Video" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Create Boomerang Video</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat6.webp" alt="Create Podcasts" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Create Podcasts</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat7.webp" alt="50+ Short Video Templates" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">50+ Short Video Templates</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat8.webp" alt="Add 25+ Vector Images" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Add 25+ Vector Images</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat9.webp" alt="Add Sound Waves in Video" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Add Sound Waves in Video</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat10.webp" alt="Add Voiceover to Any Video" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Add Voiceover to Any Video</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat11.webp" alt="Add Background Music to Any video" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Add Background Music to Any video</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat12.webp" alt="Create Voiceover in 150+ Voices in 30+ Languages" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Create Voiceover in 150+ Voices in 30+ Languages</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat13.webp" alt="Simple Video Editor" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Simple Video Editor</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat14.webp" alt="Add Logo &amp; Watermark" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Add Logo &amp; Watermark</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat15.webp" alt="Free Stock Images &amp; Videos" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Free Stock Images &amp; Videos</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat16.webp" alt="Multiple Video Quality (HD, 720p, 1080p, etc)" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Multiple Video Quality (HD, 720p, 1080p, etc)</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat17.webp" alt="Inbuilt Music Library" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Inbuilt Music Library</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat18.webp" alt="Built-in Biz Drive to Store &amp; Share Media" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Built-in Biz Drive to Store &amp; Share Media</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat19.webp" alt="Post Videos on YouTube" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Post Videos on YouTube</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat20.webp" alt=" 100+ Social Sharing Platforms" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500"> 100+ Social Sharing Platforms</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat21.webp" alt="Drive Unlimited Viral Traffic" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Drive Unlimited Viral Traffic</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat22.webp" alt="Capture Unlimited Leads" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Capture Unlimited Leads</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat23.webp" alt="100% Cloud-Based Software" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">100% Cloud-Based Software</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat24.webp" alt="100% Newbie Friendly" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">100% Newbie Friendly</div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Ground-Breaking Section Start -->

      <!-- CTA Section-->
      <div class="cta-section-new">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-18 f-md-28 w700 lh140 text-center white-clr">
                     Free Commercial License + Low 1-Time Price For Launch Period Only
                  </div>
                  <!-- <div class="f-18 f-md-20 w400 lh140 text-center mt5 white-clr">
                     Use Discount Coupon <span class="red-clr w600"> "Aicademy" </span> for Instant <span class="red-clr w600"> $3 OFF </span>
                 </div> -->
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 mt-md30 text-center">
                        <a href="#buynow" class="cta-link-btn">Get Instant Access To Aicademy</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                     <img src="assets/images/compaitable-with1.webp" class="img-fluid mx-xs-center md-img-right" alt="visa">
                     <div class="d-md-block d-none visible-md px-md30">
                        <img src="assets/images/v-line.webp" class="img-fluid" alt="line">
                     </div>
                     <img src="assets/images/days-gurantee1.webp" class="img-fluid mx-xs-auto mt15 mt-md0" alt="30 days">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- CTA Section End-->


      <!-- Billion Section Start-->
      <div class="billion-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-45 w600 white-clr lh140 heading-design">Did You Know ? YouTube Shorts Hit</div>
                  <div class="f-45 f-md-100 w600 black-clr2 mt20 lh140"> <span class="red-clr">30 Billion</span> View</div>
                  <div class="f-28 f-md-45 w600 lh140 black-clr2">Every Single Day!</div>
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-12 col-md-7">
                  <div class="f-18 f-md-24 w600 black-clr2 lh140">Yes, YouTube Shorts are Generating 30 Billion+ Views Per Day, A 4X Increase From This Time Last Year
                  </div>
                  <div class="f-18 f-md-24 w400 lh140 black-clr21 mt20">This clearly shows the trend is changing. Internet users prefer short videos to consume content instead of watching long videos like earlier…</div>
                  <div class="f-28 f-md-45 w600 lh140 red-clr mt20 mt-md50">No Doubt!</div>
                  <div class="f-20 f-md-24 w600 lh140 black-clr2 mt10">Reels & Shorts Are Present and Future of Video Marketing…</div>
               </div>
               <div class="col-12 col-md-5 mt20 mt-md0">
                  <img src="assets/images/girl.webp" class="img-fluid mx-auto d-block">
               </div>
            </div>
         </div>
         <div class="billion-wrap">
            <div class="container">
               <div class="row">
                  <div class="col-12 text-center">
                     <div class="f-28 f-md-45 w600 lh140 black-clr2">Here Are Some More Facts - <br class="d-none d-md-block">Why One Should Get Started with Reels & Shorts! </div>
                  </div>
               </div>
               <div class="row align-items-center mt20 mt-md80">
                  <div class="col-12 col-md-6">
                     <div class="f-20 f-md-24 w600 lh140 text-start black-clr2">YouTube has 2+ Bn Users & 70% of Videos are Viewed on Mobile</div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 ">
                     <img src="assets/images/facts1.webp" class="img-fluid d-block mx-auto">
                  </div>
               </div>
               <!-- <div class="row align-items-center mt20 mt-md80">
                  <div class="col-12 col-md-6 order-md-2">
                     <div class="f-20 f-md-24 w600 lh140 text-start black-clr2">YouTube Confirms Ads Are Coming to Shorts</div>
                     <div class="f-18 f-md-20 w400 lh140 mt10 text-start black-clr21"> Ads in Shorts would give creators in the YouTube Partner Program the ability to earn more revenue on the platform.
                     </div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
                     <img src="assets/images/facts2.webp" alt="YouTube Confirms Ads Are Coming to Shorts" class="mx-auto img-fluid d-block">
                  </div>
               </div> -->
               <div class="row align-items-center mt20 mt-md80">
                  <div class="col-12 col-md-6 order-md-2">
                     <div class="f-20 f-md-24 w600 lh140 text-start black-clr2">Instagram Reels have 2.5 Billion Monthly Active Users</div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
                     <img src="assets/images/facts3.webp" class="img-fluid d-block mx-auto" alt="Instagram Reels">
                  </div>
               </div>
               <div class="row align-items-center mt20 mt-md80">
                  <div class="col-12 col-md-6">
                     <div class="f-20 f-md-24 w600 lh140 text-start black-clr2">Tiktok has 50 Million Daily Active User in USA & 1 Bn Users Worldwide</div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0">
                     <img src="assets/images/facts4.webp" alt="Tiktok" class="mx-auto img-fluid d-block">
                  </div>
               </div>
               <div class="row align-items-center mt20 mt-md80">
                  <div class="col-12 col-md-6 order-md-2">
                     <div class="f-20 f-md-24 w600 lh140 text-start black-clr2">100 Million hours Daily watch Time by Facebook users.</div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
                     <img src="assets/images/facts5.webp" class="img-fluid d-block mx-auto" alt="Facebook Users">
                  </div>
               </div>
               <div class="row align-items-center mt20 mt-md80">
                  <div class="col-12 col-md-6">
                     <div class="f-20 f-md-24 w600 lh140 text-start black-clr2">Snapchat has over 360+ million daily active users</div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0">
                     <img src="assets/images/facts6.webp" alt="Website Analyser" class="mx-auto img-fluid d-block" alt="Snapchat Users">
                  </div>
               </div>
               <div class="row mt20 mt-md80">
                  <div class="col-12 text-center">
                     <div class="f-28 f-md-45 w600 lh140 ">So, Capitalising on Reels & Shorts in the Video Marketing Strategy of Any Business is the Need of The Hour.</div>
                     <div class="f-md-45 f-28 w600 lh140 mt20 mt-md30 black-clr2  border-cover">
                        <div class="f-22 f-md-26 w600 lh140 black-clr2">It Opens the Door to</div>
                        <div class=""> Tsunami <span class="red-clr"> of FREE </span>Traffic</div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Billion Section End -->

      <section class="prob-sec">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-20 f-md-32 w700 lh140 text-center black-clr2">
                     Now You’re Probably Wondering “Can Anyone Make a Full-Time Income With Short Videos?”                                  
                  </div>
                  <div class="f-20 f-md-32 w400 lh140 text-center mt15">
                  <span class="w700">Answer Is - Of Course, You Can!</span>   
                  </div>
               </div>
            </div>
         </div>
      </section>

      <!-- Power Section Start -->
      <div class="power-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-28 f-md-45 w700 lh140 text-center black-clr2">
                     See How People Are Making Crazy <br class="d-none d-md-block">
                     Money Using the POWER Of Reels & Short Videos
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md150">
                  <div class="power-shape">
                     <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-12 col-md-2">
                           <img src="assets/images/ryan.webp" class="img-fluid d-block mx-auto ryn-img">
                        </div>
                        <div class="col-12 col-md-10 mt30 mt-md0">
                           <div class="f-md-22 f-20 lh140 w400 text-center text-md-start pl-md60">
                              <span class="w700">Here’s “Ryan’s World” </span>– A 10-Year-Old Kid that has over <br class="d-none d-md-block"> 31.9M Subscribers with a whooping <span class="w700">Net Worth of $32M</span><br class="d-none d-md-block"> and growing… As he is now capitalizing on YouTube Shorts. <br><br>
                              Yes! You heard me right... He is just a 10-year-kid but making BIG.
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md150">
                  <div class="power-shape">
                     <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-12 col-md-2 order-md-2">
                           <img src="assets/images/huddakattan.webp" class="img-fluid d-block mx-auto ryn-img1">
                        </div>
                        <div class="col-12 col-md-10 mt30 mt-md0 order-md-1">
                           <div class="f-md-22 f-20 lh140 w400 text-center text-md-start pl-md30 pr-md40">
                              <span class="w700">“HuddaKattan” </span>is an American makeup artist, blogger & entrepreneur, Founder of cosmetic lines Huda Beauty on Instagram has over 51.8M Followers. She is using Instagram to interatct with here followers on daily basis.
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md150">
                  <div class="power-shape">
                     <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-12 col-md-2">
                           <img src="assets/images/khabane-lame.webp" class="img-fluid d-block mx-auto ryn-img">
                        </div>
                        <div class="col-12 col-md-10 mt30 mt-md0">
                           <div class="f-md-22 f-20 lh140 w400 text-center text-md-start pl-md60">
                           <span class="w700">“Khabane Lame”</span> is an Italian born social personality started uploading short video on dancing on Tiktok after laid off during Covid 19 and today he is most followed creator with 142.4M followers.
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md150">
                  <div class="power-shape">
                     <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-12 col-md-2 order-md-2">
                           <img src="assets/images/dj-khaled.webp" class="img-fluid d-block mx-auto ryn-img1">
                        </div>
                        <div class="col-12 col-md-10 mt30 mt-md0 order-md-1">
                           <div class="f-md-22 f-20 lh140 w400 text-center text-md-start pl-md60">
                              <span class="w700"> DJ Khaled known as  ‘King of Snapchat' </span> <br><br>
                              He is an American DJ, record executive, record producer and rapper. Now-a-days He teaches his audience by creating Reels that how anyone can get succeed on social media.
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row mt30 mt-md100">
               <div class="col-12 text-center">
                  <div class="but-design-border">
                     <div class="f-md-45 f-28 lh140 w900 white-clr button-shape">
                        Impressive right?
                     </div>
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md20">
               <div class="col-12 f-md-28 f-20 lh160 w400 text-center">
                  So, it can be safely stated that…<br>
                  <span class="f-22 f-md-32 w700 red-clr">Reels & Shorts Are The BIGGEST, Traffic and Income<br class="d-none d-md-block"> Opportunity Right Now! </span>
               </div>
            </div>
         </div>
      </div>
      <!-- Power Section End -->

      <!-- Affiliate Section Start -->
      <div class="affiliate-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-18 f-md-20 w400 lh140 black-clr2">
                  <span class="w700">The SECRET Is: </span><br><br>When you can combine selling top products, Reels & Shorts, and free viral traffic to make sales & profits, you’re the fastest way possible.
               </div>
               <div class="col-12 f-18 f-md-20 w500 lh140 black-clr2 mt20 mt-md50">
                  <img src="assets/images/fb-viral.webp" class="img-fluid d-block mx-auto" alt="Viral">
               </div>
               <div class="col-12 mt20 mt-md50">
                  <div class="f-md-40 f-24 w700 lh140 text-capitalize text-center black-clr2">
                     Checkout Free Traffic & Commissions <br class="d-none -md-block">
                     We’re Getting By Following This Proven System…
                  </div>
               </div>
               <div class="col-12 col-md-10 mt20 mt-md30 mx-auto">
                  <img src="assets/images/proof-02.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>
         </div>
      </div>
      <!-- Affiliate Section End -->

      <!-----Option Section Start------>
      <!-- <div class="option-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-24 f-md-45 w700 lh140 black-clr2 text-center">That’s A Real Deal But…</div>
                  <div class="f-28 f-md-65 w700 lh140 red-clr1 text-center">Here’s The Big Problem</div>
               </div>
               <div class="col-12 mt-md70 mt20 ">
                  <div class="row align-items-center">
                     <div class="col-md-6">
                        <div class="f-24 f-md-32 w700 red-clr lh140">
                           Creating Content Daily is Painful & Time Consuming!
                        </div>
                        <div class="f-18 w500 lh140 black-clr2 mt20">
                          <ul class="problem-list">
                           <li> You need to research, plan, and write content daily. And staying 
                           up-to-date with the latest niche trends needs lots of effort </li>

                           <li>You Need To Be On Camera. This can be a major blocker in the case 
                           you are shy or introverted and have a fear that people would judge 
                           and laugh at your videos.</li>                           
                          </ul>                        
                        </div>
                     </div>
                     <div class="col-md-6">
                        <img src="assets/images/opt1.webp" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
               <div class="col-12 mt-md70 mt20 ">
                  <div class="row align-items-center">
                     <div class="col-md-6 order-md-2">
                        <div class="f-24 f-md-32 w700 red-clr lh140">
                           Buying Expensive Equipment Can Leave Your Back Accounts Dry                        
                        </div>
                        <div class="f-18 w500 lh140 black-clr2 mt20">
                           To even get started with the first video, you need expensive equipment, like a<br><br>
                           <ul class="problem-list pl0">
                              <li>Nice camera</li>
                              <li>Microphone, and</li>
                              <li>Video-audio editing software That would cost you THOUSANDS of dollars.</li>
                           </ul>
                        </div>
                     </div>
                     <div class="col-md-6 order-md-1">
                        <img src="assets/images/opt2.webp" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>               
            </div>
            <div class="row mt-md70 mt20">
               <div class="col-12">
                  <div class="row align-items-center">
                     <div class="col-md-6">
                        <div class="f-24 f-md-32 w700 red-clr lh140">
                           You Need To Learn Complex Editing Tools
                        </div>
                        <div class="f-18 w500 lh140 black-clr2 mt20">
                           <ul class="problem-list pl0">
                              <li>You need to learn COMPLEX video & audio editing skills. 
                              Most software are complex and difficult to learn, especially 
                              if you are a non-technical guy like us.
                              </li>
                           </ul>
                        </div>
                     </div>
                     <div class="col-md-6">
                        <img src="assets/images/opt3.webp" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
               <div class="col-12 mt-md70 mt20">
                  <div class="row align-items-center">
                     <div class="col-md-6 order-md-2">
                        <div class="f-24 f-md-32 w700 red-clr lh140">
                           Even If You Outsource Any of the Above Tasks!
                        </div>
                        <div class="f-18 w500 lh140 black-clr2 mt20">
                           <ul class="problem-list pl0">
                              <li> 	
                                 Firstly, it can cost you 1000’s Dollars even for a 
                                 small project.                                  
                              </li>
                              <li> 	
                                 Finding a well skilled Freelancer who could handle 
                                 all your task is a big headache. 
                              </li>
                              <li> 	
                                 Even then you have to work for countless hours 
                                 finding ideas and explaining your requirement to them 
                              </li>
                           </ul>
                        </div>
                     </div>
                     <div class="col-md-6 order-md-1">
                        <img src="assets/images/opt4.webp" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>              
            </div>
            <div class="row mt-md50 mt20">
               <div class="col-12 f-md-22 f-20 w400 lh140 black-clr2">
                  <span class="w700"> Problem does not end here, after all these efforts, still there is no security that you will get the desired results.</span> <br><br>
                  There’re tons of areas where marketers can face challenges that we had faced in our journey before creating this masterpiece
               </div>
            </div>
            <div class="row mt-md50 mt20">
               <div class="col-12 col-md-10 mx-auto">
                  <div class="any-shape text-center">
                     <div class="f-md-45 f-28 lh140 orange-clr w700 text-uppercase">
                        BUT NOT ANYMORE!
                     </div>
                     <div class="f-18 lh140 white-clr w400 mt15">
                        Imagine if you had an ultimate ground-breaking beginner friendly technology that’s custom <br class="d-none d-md-block"> created keeping every marketer in mind, and that creates Short Videos for YouTube without<br class="d-none d-md-block"> having to spend money on costly equipment, without creating much content, and without <br class="d-none d-md-block">having to be on camera at all!          
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div> -->
      <!-----Option Section End------>

      <!-- Proudly Section -->
      <div class="proudly-sec" id="product">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center lh140">
                  <div class="f-28 f-md-32 w700 text-center orange-clr lh140 text-capitalize">Presenting…</div>
               </div>
               <div class="col-12 mt-md40 mt20 text-center">
                  <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 644.49 120" style="max-height:120px">
                     <defs>
                        <style>
                           .cls-1 {
                           fill: #fff;
                           }
                     
                           .cls-2 {
                           fill: #0af;
                           }
                        </style>
                     </defs>
                     <g id="Layer_1-2" data-name="Layer 1">
                        <g>
                           <g>
                              <circle class="cls-2" cx="79.02" cy="43.5" r="7.25"></circle>
                              <circle class="cls-2" cx="70.47" cy="78.46" r="5.46"></circle>
                              <path class="cls-2" d="m158.03,45.28l-33.74,13.31v36.4l-45.27,16.86-27.9-10.4c-4.17,3.37-7.04,6.74-8.15,10.11-1.01,3.22-.07,6.21,2.46,8.42-5.44-1.46-8-5.25-6.96-9.76,1.24-3.62,4.79-7.68,10-12.08h.01c3.95-3.31,8.84-6.84,14.46-10.5,2.04,1.67,4.68,2.68,7.53,2.68,6.56,0,11.88-5.32,11.88-11.88s-5.32-11.88-11.88-11.88-11.88,5.32-11.88,11.88c0,1.13.15,2.21.46,3.25-8.01,5.01-14.83,10.36-19.57,15.44l-5.75-2.14v-7.66c-7.84,4.92-11.84,11.06-5.99,16.99-5.17-1.64-7.69-4.49-7.69-8.05,0-4.83,4.67-10.99,13.69-17.26l35.41-23.23c2.71,2.18,6.13,3.48,9.87,3.48,8.7,0,15.76-7.06,15.76-15.76s-7.06-15.76-15.76-15.76-15.76,7.06-15.76,15.76c0,1.59.23,3.12.68,4.57l-30.2,19.53v-9L0,45.28,79.02,0l79.01,45.28Z"></path>
                              <path class="cls-2" d="m149.38,68.32v-19.62l-1.51.6v19.03c-1.66.37-2.9,1.96-2.9,3.85v11.69h7.31v-11.69c0-1.9-1.24-3.48-2.9-3.85Z"></path>
                           </g>
                           <g>
                              <g>
                                <path class="cls-2" d="m178.03,98.08l22.25-71.1h19.3l22.25,71.1h-17.01l-4.36-16.9h-21.7l-4.36,16.9h-16.36Zm25.74-35.99l-1.74,6.54h15.16l-1.64-6.54c-.95-3.56-1.91-7.34-2.89-11.34-.98-4-1.95-7.85-2.89-11.56h-.44c-.87,3.78-1.76,7.65-2.67,11.61-.91,3.96-1.87,7.73-2.89,11.29Z"></path>
                                <path class="cls-2" d="m256.22,36.03c-2.69,0-4.87-.76-6.54-2.29-1.67-1.53-2.51-3.56-2.51-6.11s.83-4.58,2.51-6.11c1.67-1.53,3.85-2.29,6.54-2.29s4.87.76,6.54,2.29c1.67,1.53,2.51,3.56,2.51,6.11s-.84,4.58-2.51,6.11c-1.67,1.53-3.85,2.29-6.54,2.29Zm-7.96,62.05v-54.09h16.03v54.09h-16.03Z"></path>
                              </g>
                              <g>
                                <path class="cls-1" d="m299.89,99.38c-5.02,0-9.54-1.11-13.58-3.33-4.03-2.22-7.23-5.45-9.6-9.71-2.36-4.25-3.54-9.36-3.54-15.32s1.31-11.16,3.93-15.38c2.62-4.22,6.07-7.43,10.36-9.65,4.29-2.22,8.9-3.33,13.85-3.33,3.34,0,6.31.55,8.89,1.64,2.58,1.09,4.89,2.47,6.92,4.14l-7.52,10.36c-2.55-2.11-4.98-3.16-7.31-3.16-3.85,0-6.92,1.38-9.21,4.14-2.29,2.76-3.44,6.51-3.44,11.23s1.15,8.38,3.44,11.18c2.29,2.8,5.18,4.2,8.67,4.2,1.74,0,3.45-.38,5.13-1.15,1.67-.76,3.2-1.69,4.58-2.78l6.33,10.47c-2.69,2.33-5.6,3.98-8.72,4.96-3.13.98-6.18,1.47-9.16,1.47Z"></path>
                                <path class="cls-1" d="m339.81,99.38c-4.94,0-8.87-1.58-11.78-4.74-2.91-3.16-4.36-7.03-4.36-11.61,0-5.67,2.4-10.1,7.2-13.3,4.8-3.2,12.54-5.34,23.23-6.43-.15-2.4-.86-4.31-2.13-5.73-1.27-1.42-3.4-2.13-6.38-2.13-2.25,0-4.54.44-6.87,1.31-2.33.87-4.8,2.07-7.42,3.6l-5.78-10.58c3.42-2.11,7.07-3.82,10.96-5.13,3.89-1.31,7.94-1.96,12.16-1.96,6.91,0,12.21,2,15.92,6,3.71,4,5.56,10.14,5.56,18.43v30.97h-13.09l-1.09-5.56h-.44c-2.25,2.04-4.67,3.69-7.25,4.96-2.58,1.27-5.4,1.91-8.45,1.91Zm5.45-12.43c1.82,0,3.4-.42,4.74-1.25,1.34-.83,2.71-1.94,4.09-3.33v-9.49c-5.67.73-9.6,1.87-11.78,3.44-2.18,1.56-3.27,3.4-3.27,5.51,0,1.74.56,3.04,1.69,3.87,1.13.84,2.63,1.25,4.53,1.25Z"></path>
                                <path class="cls-1" d="m402.95,99.38c-6.69,0-12.05-2.53-16.09-7.58-4.03-5.05-6.05-11.98-6.05-20.77,0-5.89,1.07-10.96,3.22-15.21,2.14-4.25,4.94-7.51,8.4-9.76,3.45-2.25,7.07-3.38,10.85-3.38,2.98,0,5.49.51,7.52,1.53,2.04,1.02,3.96,2.4,5.78,4.14l-.66-8.29v-18.43h16.03v76.45h-13.09l-1.09-5.34h-.44c-1.89,1.89-4.11,3.47-6.65,4.74-2.55,1.27-5.13,1.91-7.74,1.91Zm4.14-13.09c1.74,0,3.33-.36,4.74-1.09,1.42-.73,2.78-2,4.09-3.82v-22.14c-1.38-1.31-2.85-2.22-4.42-2.73-1.56-.51-3.07-.76-4.53-.76-2.55,0-4.8,1.22-6.76,3.65-1.96,2.44-2.94,6.23-2.94,11.4s.85,9.21,2.56,11.72c1.71,2.51,4.13,3.76,7.25,3.76Z"></path>
                                <path class="cls-1" d="m470.34,99.38c-5.16,0-9.81-1.13-13.96-3.38-4.14-2.25-7.42-5.49-9.81-9.71-2.4-4.22-3.6-9.31-3.6-15.27s1.22-10.94,3.65-15.16c2.43-4.22,5.62-7.47,9.54-9.76,3.93-2.29,8.03-3.44,12.32-3.44,5.16,0,9.43,1.15,12.81,3.44,3.38,2.29,5.92,5.38,7.63,9.27,1.71,3.89,2.56,8.31,2.56,13.25,0,1.38-.07,2.74-.22,4.09-.15,1.35-.29,2.34-.44,3h-32.39c.73,3.93,2.36,6.82,4.91,8.67,2.54,1.85,5.6,2.78,9.16,2.78,3.85,0,7.74-1.2,11.67-3.6l5.34,9.71c-2.76,1.89-5.85,3.38-9.27,4.47-3.42,1.09-6.73,1.64-9.92,1.64Zm-12-34.24h19.52c0-2.98-.71-5.43-2.13-7.36-1.42-1.93-3.73-2.89-6.92-2.89-2.47,0-4.69.86-6.65,2.56-1.96,1.71-3.24,4.27-3.82,7.69Z"></path>
                                <path class="cls-1" d="m502.62,98.08v-54.09h13.09l1.09,6.98h.44c2.25-2.25,4.65-4.2,7.2-5.83,2.54-1.64,5.6-2.45,9.16-2.45,3.85,0,6.96.78,9.32,2.34,2.36,1.56,4.23,3.8,5.62,6.71,2.4-2.47,4.94-4.6,7.63-6.38,2.69-1.78,5.82-2.67,9.38-2.67,5.82,0,10.09,1.95,12.81,5.83,2.73,3.89,4.09,9.21,4.09,15.98v33.59h-16.03v-31.52c0-3.93-.53-6.61-1.58-8.07-1.05-1.45-2.74-2.18-5.07-2.18-2.69,0-5.78,1.74-9.27,5.23v36.53h-16.03v-31.52c0-3.93-.53-6.61-1.58-8.07-1.05-1.45-2.74-2.18-5.07-2.18-2.69,0-5.74,1.74-9.16,5.23v36.53h-16.03Z"></path>
                                <path class="cls-1" d="m602.07,119.23c-1.6,0-3-.11-4.2-.33-1.2-.22-2.34-.47-3.44-.76l2.84-12.21c.51.07,1.09.2,1.74.38.65.18,1.27.27,1.85.27,2.69,0,4.76-.65,6.22-1.96,1.45-1.31,2.54-3.02,3.27-5.13l.76-2.84-20.83-52.67h16.14l7.74,23.23c.8,2.47,1.53,4.98,2.18,7.52.65,2.55,1.34,5.16,2.07,7.85h.44c.58-2.54,1.18-5.11,1.8-7.69.62-2.58,1.25-5.14,1.91-7.69l6.54-23.23h15.38l-18.76,54.63c-1.67,4.51-3.53,8.31-5.56,11.4-2.04,3.09-4.49,5.4-7.36,6.92-2.87,1.53-6.45,2.29-10.74,2.29Z"></path>
                              </g>
                           </g>
                        </g>
                     </g>
                  </svg>
               </div>
               <div class="col-12 f-md-40 f-28 mt-md30 mt20 w700 text-center white-clr lh140">
                  A Push-Button Technology Creates Reels & Short Videos to Publish on YouTube, 
                  Instagram, TikTok, Facebook & Snapchat using Just One Keyword and Drive Viral Traffic to Any of Your Offers
                  
               </div>
            </div>
            <div class="row mt20 mt-md50 align-items-center">
               <div class="col-12">
                  <img src="assets/images/product-image.webp" class="img-fluid d-block mx-auto img-animation"
                     alt="Product">
               </div>
            </div>
         </div>
      </div>
      <!-- Proudly Section End -->

      <!-- Step Section Start -->
      <div class="step-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-39 f-28 w600 text-center black-clr2 lh140">
                  Create Stunning Stories, Reels & Shorts for Massive Traffic To Any Offer, Page, or Link <span class="red-clr w700">in Just 3 Easy Steps</span>
               </div>
            </div>
            <div class="row mt20 mt-md50">
               <div class="col-12 col-md-4">
                  <div class="step-block">
                     <div class="step-title f-20 f-md-22 w700">Step 1</div>
                     <img src="assets/images/step-1.webp" class="img-fluid d-block mx-auto mt15 mt-md30">
                     <div class="step-content">
                        <div class="f-22 f-md-28 w600 black-clr2 mt10">Choose Platform</div>
                        <div class="w400 f-18 f-md-20 lh140 mt10 black-clr2">
                           Choose the social platform- YouTube, Instagram, TikTok, Facebook, or Snapchat you want to create Reel or Shorts for
                        </div>
                     </div>  
                  </div>
               </div>
               <div class="col-12 col-md-4 mt20 mt-md0">
                  <div class="step-block">
                     <div class="step-title f-20 f-md-22 w700">Step 2</div>
                     <img src="assets/images/step-2.webp" class="img-fluid d-block mx-auto mt15 mt-md30">
                     <div class="step-content">
                        <div class="f-22 f-md-28 w600 black-clr2 mt10">Enter a Keyword </div>
                        <div class="w400 f-18 f-md-20 lh140 mt10 black-clr2">
                           Enter the desired Keyword & you will get several short videos. Select any video & customize it by adding background, vectors, audio, & fonts to make it engaging
                        </div>
                     </div>  
                  </div>
               </div>
               <div class="col-12 col-md-4 mt20 mt-md0">
                  <div class="step-block">
                     <div class="step-title f-20 f-md-22 w700">Step 3</div>
                     <img src="assets/images/step-3.webp" class="img-fluid d-block mx-auto mt15 mt-md30">
                     <div class="step-content">
                        <div class="f-22 f-md-28 w600 black-clr2 mt10">Publish and Profit</div>
                        <div class="w400 f-18 f-md-20 lh140 mt10 black-clr2">
                           Now publish your Reels & Shortson different social platforms with a click to drive unlimited viral traffic & sales on your offer, page, or website.
                        </div>
                     </div>  
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md70">
               <div class="col-12 text-center">
                  <div class="f-20 f-md-24 w600 lh140">
                     No Download/Installation  |  No Prior Knowledge  |  100% Beginners Friendly
                  </div>
                  <div class="f-18 f-md-20 w400 mt15 mt-md30 lh140">
                     In just 60 seconds, you can create your first profitable Reels & Shorts to enjoy MASSIVE FREE Traffic, Sales & Profits coming into your bank account on autopilot.
                  </div>
               </div>
            </div>
            <div class="col-12 text-center">
                  <div class="f-18 f-md-28 w700 lh140 text-center white-clr">
                     Free Commercial License + Low 1-Time Price For Launch Period Only
                  </div>
                  <!-- <div class="f-18 f-md-20 w400 lh140 text-center mt20 white-clr">
                  Use Coupon Code <span class="red-clr w600">"Aicademy"</span> for an Additional <span class="red-clr w600">$3 Discount</span>
                  </div> -->
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center">
                        <a href="#buynow" class="cta-link-btn">Get Instant Access To Aicademy</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                     <img src="assets/images/compaitable-with.webp" class="img-responsive mx-xs-center md-img-right" alt="visa">
                     <div class="d-md-block d-none visible-md px-md30">
                        <img src="assets/images/v-line.webp" class="img-fluid" alt="line">
                     </div>
                     <img src="assets/images/days-gurantee.webp" class="img-responsive mx-xs-auto mt15 mt-md0" alt="30 days">
                  </div>
               </div>
         </div>
      </div>
      <!-- Step Section End -->

      <!-- Demo Section  -->
      <!-- <div class="demo-sec" id="demo">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="white-clr f-md-45 f-28 w700 text-center">
                     Watch Aicademy in Action
                  </div>
               </div>
               <div class="col-12 col-md-9 mx-auto mt-md40 mt20">
                     <div class="responsive-video">
                     <iframe src="https://vidmaster.dotcompal.com/video/embed/jwjoicqx4d" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                        box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen"
                        allowfullscreen></iframe>
                     </div>
               </div>
             
            </div>
         </div>
      </div> -->
      <!-- Demo Section End -->

      

      <!-- Testimonials Section -->
      <!-- <div class="testimonial-section1">
         <div class="container ">
            <div class="row ">
               <div class="f-md-45 f-28 w700 lh140 text-center black-clr2">
                  Here's What REAL Users Have to <br class="d-none d-md-block"> Say About Aicademy
               </div>
            </div>
            <div class="row mt25 mt-md50 align-items-center">
               <div class="col-12 col-md-10 offset-md-2">
                  <div class="testi-block">
                     <div class="row d-flex align-items-center flex-wrap">
                         <div class="col-12 col-md-3">
                             <img src="assets/images/c4.webp" class="img-fluid d-block mx-auto">
                             <div class="f-22 f-md-24 w700 lh120 red-clr mt20 text-center">Lucas Rodriguez</div>
                         </div>
                         <div class="col-12 col-md-9 mt20 mt-md0 text-center text-md-start">
                             <div class="f-22 f-md-28 w700 lh140">Easy To Use Software with Training Included</div>
                             <div class="f-20 w400 lh140 mt20 quote1">
                              Aicademy is next-level quality software to full fill all my YouTube marketing needs. Also, it is  <span class="w700">so easy to use, and the training included makes it even easier</span> to create highly engaging and professional videos & shorts in a few minutes. I'd say that this is a MUST HAVE technology for marketers.
                             </div>
                            
                         </div>
                     </div>
                 </div>
               </div>
               <div class="col-12 col-md-10 mt20 mt-md40">
                  <div class="testi-block">
                     <div class="row d-flex align-items-center flex-wrap">
                         <div class="col-12 col-md-3 order-md-2">
                             <img src="assets/images/c5.webp" class="img-fluid d-block mx-auto">
                             <div class="f-22 f-md-24 w700 lh120 red-clr mt20 text-center">Theodore Miller</div>
                         </div>
                         <div class="col-12 col-md-9 order-md-1 mt20 mt-md0 text-center text-md-start">
                             <div class="f-22 f-md-28 w700 lh140">Revolutionary software comes at a one-time price</div>
                             <div class="f-20 w400 lh140 mt20 quote1">
                              Aicademy combines ease of use with  <span class="w700">super-powerful video creation technology.</span> I'm using it to create short videos for my YouTube channel that are well-suited to my audience.<br><br>
                              Trust me, it works like a breeze and the best part is, <span class="w700">that this revolutionary software comes at a one-time price.</span> It’s amazing! I am enjoying working with it. Great Job Ayush & Pranshu. 
                              
                             </div>
                           
                         </div>
                     </div>
                 </div>
               </div>
               <div class="col-12 col-md-10 offset-md-2 mt20 mt-md40">
                  <div class="testi-block">
                     <div class="row d-flex align-items-center flex-wrap">
                         <div class="col-12 col-md-3">
                             <img src="assets/images/c6.webp" class="img-fluid d-block mx-auto">
                             <div class="f-22 f-md-24 w700 lh120 red-clr mt20 text-center">Harper Garcia</div>
                         </div>
                         <div class="col-12 col-md-9 mt20 mt-md0 text-center text-md-start">
                             <div class="f-22 f-md-28 w700 lh140">I Can Make Money with Complete freedom</div>
                             <div class="f-20 w400 lh140 mt20 quote1">
                              I have been in the Video Marketing arena for quite some time now, and I must say that Aicademy will take the industry by storm.<br.<br>
                              The coolest part is that <span class="w700">it enables you to create video shorts in just a few minutes which usually takes me a long day to create even a single video</span> to boost engagement and traffic on my YouTube channel. Now, I can make money with Amazon/Affiliate offers, AdSense & my own offers with fewer efforts on video creation.<span class="w700"> Complete freedom is what it provides me.</span> Something to check on a serious note. 
                              
                             </div>
                            
                         </div>
                     </div>
                 </div>
               </div>
            </div>
         </div>
      </div> -->
      <!-- Testimonials Section End -->

      <!-- <div class="feature-highlight" id="features">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-40 f-24 w700 text-center balck-clr2 lh140">
                  Here Are The GROUND - BREAKING Features<br class="d-none d-md-block"> 
                  That Makes Aicademy A CUT ABOVE The Rest  
               </div>
            </div>
         </div>
      </div> -->

      <!-- Features Section Star -->
      <div class="features-section-one">
         <div class="container">           
            <div class="row align-items-center">
               <div class="col-12 mb20 mb-md50">
                  <div class="f-md-45 f-28 w700 lh140 white-clr text-center">
                  Here Are The GROUND - BREAKING Features<br class="d-none d-md-block"> 
                  That Makes Aicademy A CUT ABOVE The Rest 
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="f-md-32 f-24 w700 lh140 white-clr">
                     Create Highly Engaging & Professional Reels & Short Videos 
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 white-clr mt15">
                     Create Reels & Shorts for YouTube, Instagram, TikTok, Facebook & SnapChat                  
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0">
                  <img src="assets/images/f1.webp" class="img-fluid d-block mx-auto" alt="Features">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-md-6 order-md-2">
                  <div class="f-md-32 f-24 w700 lh140 white-clr">
                     Create Shorts by One Keyword
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 white-clr mt15">
                     Create Reels & short videos by entering Just one keyword and you will get number of short videos to choose or upload your own.
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0 order-md-1">
                  <img src="assets/images/f2.webp" class="img-fluid d-block mx-auto" alt="Features">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-md-6">
                  <div class="f-md-32 f-24 w700 lh140 white-clr">
                     Create 9:16 Vertical Video
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 white-clr mt15">
                     Create highly professional & engaging 9:16 vertical video. Perfect for mobile viewing and guaranteed to entertain your audience.                  
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0">
                  <img src="assets/images/f3.webp" class="img-fluid d-block mx-auto" alt="Features">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-md-6 order-md-2">
                  <div class="f-md-32 f-24 w700 lh140 white-clr">
                     Create Boomerang
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 white-clr mt15">
                     Create boomerang short video loops that boomerang forward & reverse through the action.
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0 order-md-1">
                  <!-- <img src="assets/images/f4.webp" class="img-fluid d-block mx-auto" alt="Features"> -->
                  <video width="100%" height="auto" loop autoplay muted="muted" class="mp4-border" >
                   <source src="assets/images/f4.mp4" type="video/mp4">
               </video>
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-md-6">
                  <div class="f-md-32 f-24 w700 lh140 white-clr">
                     Create High-Quality Explanatory Whiteboard Video Shorts
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 white-clr mt15">
                     Create whiteboard video shorts by just adding your text and customizing font, color, and alignment. These videos are highly effective and informative.                    
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0">
                  <img src="assets/images/f5.webp" class="img-fluid d-block mx-auto" alt="Features">
               </div>
            </div>
         </div>
      </div>
      <div class="grey-section1">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-6">
                  <div class="f-md-32 f-24 w700 lh140 balck-clr2">
                     Create Videos by Images
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 balck-clr2 mt15">
                     Find images for videos by searching images with keywords inside Aicademy or upload your own images                   
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0">
                  <img src="assets/images/f6.webp" class="img-fluid d-block mx-auto" alt="Features">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-md-6 order-md-2">
                  <div class="f-md-32 f-24 w700 lh140 balck-clr2">
                     50+ Background Templates
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 balck-clr2 mt15">
                     Easily create Reels & shorts by using 50+ background templates to make your video attractive &get more & more engagement.                  
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0 order-md-1">
                  <img src="assets/images/f7.webp" class="img-fluid d-block mx-auto" alt="Features">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-md-6">
                  <div class="f-md-32 f-24 w700 lh140 balck-clr2">
                     25+ Vector Images
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 balck-clr2 mt15">
                     Create more engaging & attention grabbing Reels & Shorts by using 25+vector images. 
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0">
                  <img src="assets/images/f8.webp" class="img-fluid d-block mx-auto" alt="Features">
               </div>
            </div>
         </div>
      </div>
      <div class="white-feature">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-6 order-md-2">
                  <div class="f-md-32 f-24 w700 lh140 balck-clr2">
                  	100+ Stylish Fonts
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 balck-clr2 mt15">
                     Use 100+ different Stylish fonts to make your shorts more presentable
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0 order-md-1">
                  <img src="assets/images/f9.webp" class="img-fluid d-block mx-auto" alt="Features">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-md-6">
                  <div class="f-md-32 f-24 w700 lh140 balck-clr2">
                     Add Music Waves
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 balck-clr2 mt15">
                     Add Music Waves run with audio of different color to your Short videos to make your video engaging.
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0">
                  <!-- <img src="assets/images/f10.webp" class="img-fluid d-block mx-auto" alt="Features"> -->
                  <video width="100%" height="auto" loop autoplay muted="muted" class="mp4-border" >
                   <source src="assets/images/f10.mp4" type="video/mp4">
               </video>
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-md-6 order-md-2">
                  <div class="f-md-32 f-24 w700 lh140 balck-clr2">
                     Build Authority by Adding Your Branding
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 balck-clr2 mt15">
                     Build your authority in the audience by adding your brand watermarks and logos in the video.                   
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0 order-md-1">
                  <img src="assets/images/f11.webp" class="img-fluid d-block mx-auto" alt="Features">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-md-6">
                  <div class="f-md-32 f-24 w700 lh140 balck-clr2">
                     Share Shorts Directly to YouTube
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 balck-clr2 mt15">
                     You can easily share the video on YouTube or can download it is to use wherever you want.
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0">
                  <img src="assets/images/f12.webp" class="img-fluid d-block mx-auto" alt="Features">
               </div>
            </div>
         </div>
      </div>

      <div class="grey-section1">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-6 order-md-2">
                  <div class="f-md-32 f-24 w700 lh140 balck-clr2">
                     100+ Social Sharing Platforms
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 balck-clr2 mt15">
                     You can alsoshare these Reels & short videos t0  100+ social media platforms directly to get free viral traffic                            
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0 order-md-1">
                  <img src="assets/images/f13.webp" class="img-fluid d-block mx-auto" alt="Features">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md50">
               <div class="col-md-6">
                  <div class="f-md-32 f-24 w700 lh140 balck-clr2">
                     Completely Cloud-Based Software
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 balck-clr2 mt15">
                     Fully Cloud-Based Software with Zero Tech Hassles & 24*7 Customer Support          
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0">
                  <img src="assets/images/f14.webp" class="img-fluid d-block mx-auto" alt="Features">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md50">
               <div class="col-md-6 order-md-2">
                  <div class="f-md-32 f-24 w700 lh140 balck-clr2">
                     100% Newbie Friendly
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 balck-clr2 mt15">
                     Easy and Intuitive to Use Software. Also comes with Step-by-Step Video Training & PDF
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0 order-md-1">
                  <img src="assets/images/f15.webp" class="img-fluid d-block mx-auto" alt="Features">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md50">
               <div class="col-md-6">
                  <div class="f-md-32 f-24 w700 lh140 balck-clr2">
                     No Monthly Fees or Additional Charges
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0">
                  <img src="assets/images/f16.webp" class="img-fluid d-block mx-auto" alt="Features">
               </div>
            </div>
         </div>
      </div>
      <!-- Features Section End -->

      <!-- CTA Section -->
      <div class="cta-section-new">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-18 f-md-28 w700 lh140 text-center white-clr">
                     Free Commercial License + Low 1-Time Price For Launch Period Only
                  </div>
                  <!-- <div class="f-18 f-md-20 w400 lh140 text-center mt20 white-clr">
                     Use Discount Coupon <span class="red-clr w600">"Aicademy"</span> for Instant <span class="red-clr w700">$3 OFF</span>
                 </div> -->
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center">
                        <a href="#buynow" class="cta-link-btn">Get Instant Access To Aicademy</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                     <img src="assets/images/compaitable-with1.webp" class="img-responsive mx-xs-center md-img-right" alt="visa">
                     <div class="d-md-block d-none visible-md px-md30">
                        <img src="assets/images/v-line.webp" class="img-fluid" alt="line">
                     </div>
                     <img src="assets/images/days-gurantee1.webp" class="img-responsive mx-xs-auto mt15 mt-md0" alt="30 days">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- CTA Section End -->


            <!-- Imagine Section Start -->
            <section class="imagine-sec">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-12 col-md-6 f-18 f-md-20 w400 lh140 balck-clr2 order-md-2">
                  A new lightweight piece of technology that’s so incredibly sophisticated yet intuitive & easy to use that allows you to do it all, with literally ONE click!<br><br>
                  <span class="w700">That’s EXACTLY what we’ve designed Aicademy to do for you.</span>
                  <br><br>
                  So, if you want to build super engaging Short Videos for YouTube and other social media at the push of a button, and get viral traffic automatically and convert it into SALES & PROFITS, all from start to finish, then Aicademy is made for you!
               </div>
               <div class="col-md-6 col-12 order-md-1 mt20 mt-md0">
                  <img src="assets/images/imagine-box.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>
         </div>
      </section>
      <!-- Imagine Section End -->

      <div class="targeted-section">
         <div class="container">
             <div class="row">
                 <div class="col-12">
                     <div class="row d-flex align-items-center flex-wrap">
                         <div class="col-12 col-md-7">
                           <div class="f-18 f-md-20 w400 lh140 balck-clr2">
                              <ul class="noneed-listing pl0">
                                 <li>Create instant videos shorts for YouTube and other social media in just 3 clicks</li>
                                 <li>Drive tons of viral traffic from RED-HOT YouTube & other social media giants. </li>
                                 <li>Get the best results without getting into complex video creation & editing hassles</li>
                                 <li>Get more exposure for your offers in a hands-down manner</li>
                                 <li>Boost sales & profits without spending much</li>
                                 <li>Jump into the most trending video marketing strategy with YouTube Shorts.</li>
                              </ul>
                           </div>
                         </div>
                         <div class="col-12 col-md-5 mt20 mt-md0">
                             <img src="assets/images/target-arrow.webp" class="img-fluid d-block mx-auto">
 
                         </div>
                     </div>
                 </div>
                 <div class="col-12 mt-md30 mt20">
                     <div class="f-18 f-md-20 w400 lh140 balck-clr2">
                        <span class="w700">Seriously, having to do all video creation manually is expensive,</span> time-consuming and put frankly downright irritating, especially when you have to do this on a daily basis. <br><br>
                        That's exactly the reason so many people are afraid to even get started with YouTube Shorts and the same  <span class="w700">reason why out of all that DO get started, very few truly "make it".</span><br><br>
                        
                        We have done all the grunt work for you. So, you don’t need to worry at all
                        
 
                     </div>
                 </div>
             </div>
         </div>
     </div>
      <!-- Need Section Start -->

      <div class="need-marketing">
         <div class="container">
             <div class="row">
                 <div class="col-12">
                     <div class="f-md-45 f-28 lh140 w700 text-center balck-clr2">Who Can Benefit from Aicademy?
                     </div>
                 </div>
                 <div class="col-12 mt-md30 mt20">
                     <div class="f-20 f-md-22 w400 lh140 balck-clr2">
                       <span class="w700"> This is Simple.</span> It helps<br><br>
                     </div>
                 </div>
             
              <div class="col-12 mt0 mt-md20">
                     <div class="row flex-wrap">
                         <div class="col-12 col-md-5">
                             <div class="f-20 f-md-20 w400 lh140 balck-clr2">
                                 <ul class="noneed-listing pl0">
<li> <span class="w700">Marketers</span> to drive more traffic</li>
<li> <span class="w700">Affiliates,</span> make more commissions</li>
<li> <span class="w700">SEO marketers</span> get #1 ranking and traffic</li>
<li> <span class="w700">Social & Video Marketers</span> get free viral traffic</li>
<li> <span class="w700">Creators</span> can get more income handsfree without extra efforts in video creation.</li> 
                                    
 
                                 </ul>
                             </div>
                         </div>
                         <div class="col-12 col-md-7 mt0 mt-md0">
                            <div class="f-20 f-md-20 w400 lh140 balck-clr2">
                                 <ul class="noneed-listing pl0">
<li><span class="w700">Product sellers, </span> sell more with RED HOT traffic</li>
<li><span class="w700">List builders</span>  skyrocket their subscribers...</li>
<li><span class="w700">Freelancers and Agencies –</span>  Provide RED Hot service & make profits</li>
<li><span class="w700">Local Business Owners – </span> get more engagement & TRAFFIC</li>
<li>Weight Loss, Gaming, Real Estate, Health, Wealth –</span> <span class="w700">Any Kind of Business</span> </li>

 
                                 </ul>
                             </div>
                         </div>
                     </div>
                 </div>
             
              <div class="col-12 mt20 mt-md70 text-center">
                     
                         <div class="f-22 f-md-32 w700 lh140 red-clr">
                           It Works Seamlessly for ANY NICHE… All at the push of a button. 
                         </div>
                 </div>
             
                 <div class="col-12 mt20 mt-md50">
                     <div class="row align-items-center flex-wrap">
                         <div class="col-12 col-md-7">
                   <div class="f-20 f-md-26 w700 lh140 balck-clr2 text-center text-md-start">
                     Aicademy Is Designed to Meet Every Marketer’s Need:
                         </div>
                             <div class="f-20 f-md-20 w400 lh140 balck-clr2 mt20">
                      
                                 <ul class="noneed-listing pl0">
<li>Anyone who wants a nice PASSIVE income while focusing on bigger projects</li>
<li>Lazy people who want easy traffic & profits</li>
<li>Anyone who wants to value their business and money and is not ready to sacrifice them</li>
<li>People with products or services who want to kickstart online</li>                                    
 
                                 </ul>
                             </div>
                         </div>
                         <div class="col-12 col-md-5 mt20 mt-md0">
                             <img src="assets/images/platfrom.webp" class="img-fluid d-block mx-auto">
                         </div>
                     </div>
                 </div>
                 <div class="col-12 mt20 mt-md70 text-center">
                     <div class="look-shape">
                         <div class="f-22 f-md-30 w700 lh140 balck-clr2">
                           Look, it doesn't matter who you are or what you're doing.
                         </div>
                         <div class="f-20 f-md-22 w500 lh140 balck-clr2 mt10">
                           If you want to finally be successful online, make the money that you want and live in a way<br class="d-none d-md-block"> you dream of, Aicademy is for you.
                         </div>
                     </div>
                 </div>
                 <div class="col-12 mt20 mt-md70">
                     <div class="f-md-45 f-28 lh140 w700 text-center balck-clr2">Even BETTER: It’s Entirely Newbie Friendly
                     </div>
                 </div>
                 <div class="col-12 mt20 mt-md50">
                     <div class="row align-items-center flex-wrap">
                         <div class="col-12 col-md-7">
                             <div class="f-18 f-md-20 w400 lh140 balck-clr2">
                              Whatever your level of technical expertise, this amazing software enables 
                              you to create engaging & highly professional video shorts for any niche in 
                              the 7-minute flat. It really doesn’t matter.
                              <br><br>
                              <span class="w700">The ease of use for Aicademy is STUNNING,</span> allowing users to drive 
                              free viral traffic from YouTube, without ever having to turn their hair 
                              grey for complex video editing skills.
                              <br><br>
                              With Aicademy, not only do you get the BEST possible system for the LOWEST 
                              price around, but you can also sleep safe knowing you CONTROL every aspect of your business, from building a stunning video marketing strategy to promoting affiliate products, to traffic generation, monetization, list building, and TON more - no need to ever outsource those tasks again!
                              
                             </div>
                         </div>
                         <div class="col-12 col-md-5 mt20 mt-md0">
                             <img src="assets/images/better.webp" class="img-fluid d-block mx-auto">
                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </div>
      <!-- Need Section End -->
      <!-- CTA Section -->
      <div class="cta-section-new">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-18 f-md-26 w700 lh140 text-center white-clr">
                     Free Commercial License + Low 1-Time Price For Launch Period Only
                  </div>
                  <!-- <div class="f-20 f-md-24 w400 lh140 text-center mt20 white-clr">
                     Use Discount Coupon <span class="orange-clr w700">"Aicademy"</span> for Instant <span class="orange-clr w700">$3 OFF</span>
                 </div> -->
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center">
                        <a href="#buynow" class="cta-link-btn">Get Instant Access To Aicademy</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                     <img src="assets/images/compaitable-with1.webp" class="img-responsive mx-xs-center md-img-right" alt="visa">
                     <div class="d-md-block d-none visible-md px-md30">
                        <img src="assets/images/v-line.webp" class="img-fluid" alt="line">
                     </div>
                     <img src="assets/images/days-gurantee1.webp" class="img-responsive mx-xs-auto mt15 mt-md0" alt="30 days">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- CTA Section End -->

      <!---Bonus Section Starts-->
      <div class="bonus-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="but-design-border">
                     <div class="f-md-45 f-28 lh140 w700 white-clr button-shape">
                        We’re not done Yet!
                     </div>
                  </div>
               </div>
               <div class="col-12 mt30 mt-md30 f-20 f-md-22 w500 balck-clr2 lh140 text-center">
                  When You Grab Your Aicademy Account Today, You’ll Also Get These <span class="w700">Fast Action Bonuses!</span>
               </div>
               <div class="col-12 ">
                  <div class="row ">
                     <!-------bonus 1------->
                     <div class="col-12 col-md-6 mt-md50 mt20 ">
                        <div class="bonus-section-shape ">
                           <div class="row align-items-center ">
                              <div class="col-md-6 mx-auto">
                                 <img src="assets/images/bonus-box.webp " class="img-fluid d-block mx-auto ">
                              </div>
                              <div class="col-12 mt20 mt-md40 ">
                                 <div class="w700 f-22 f-md-24 balck-clr2 lh140 ">
                                 Niche Maketing Secrets
                                 </div>
                                 <div class="f-18 f-md-20 w400 balck-clr2 lh140 mt15 ">
                                 A lot of online entrepreneurs think that once they’ve discovered a niche, they just need to build a website or a business.
                                 <br><br>
                                 But you have to build a brand. This is what people will gravitate to.
                                 <br><br> Your brand is a set of values that your community and a targeted audience would associate with your business.
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-------bonus 2------->
                     <div class="col-12 col-md-6 mt-md50 mt20 ">
                        <div class="bonus-section-shape col-12 ">
                           <div class="row align-items-center ">
                              <div class="col-md-6 mx-auto col-12">
                                 <img src="assets/images/bonus-box.webp " class="img-fluid d-block mx-auto ">
                              </div>
                              <div class="col-12 mt20 mt-md40 ">
                                 <div class="w700 f-22 f-md-24 balck-clr2 lh140 ">
                                    Video Training on Viral Marketing Secrets
                                 </div>
                                 <div class="f-18 f-md-20 w400 balck-clr2 lh140 mt15 ">
                                    Believe it or not, people interested in whatever it is you are promoting are already congregating online. Social media platforms want you to succeed. The more popular your content becomes, the more traffic they get. 
<br><br>
                                    So, with this video training you will discover a shortcut to online viral marketing secrets. This will cover - The Two-Step Trick to Effective Viral Marketing, How Do You Find Hot Content? Maximize Niche Targeting for Your Curated Content, remember to protect yourself when sharing others’ content, how to share viral content on Facebook, how to share viral content on Twitter, Filter your content format to go viral on many platforms and much more. 
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt-md30 mt20 ">
                  <div class="row ">
                     <!-------bonus 3------->
                     <div class="col-12 col-md-6 ">
                        <div class="bonus-section-shape col-12 ">
                           <div class="row align-items-center ">
                              <div class="col-md-6 mx-auto">
                                 <img src="assets/images/bonus-box.webp " class="img-fluid d-block mx-auto ">
                              </div>
                              <div class="col-12 mt20 mt-md50">
                                 <div class="w700 f-22 f-md-24 balck-clr2 lh140 "> Modern Video Marketing
                                 </div>
                                 <div class="f-18 f-md-20 w400 balck-clr2 lh140 mt15 ">
                                    It's about time for you to learn the ins and outs of successful online video marketing! <br><br>

                                    Regardless of what you've heard, video marketing as a whole is not exactly a new phenomenon. Video marketing content is increasingly plugged into a larger marketing infrastructure.  
                                    <br><br>
                                    You have to wrap your mind around the fact that modern video marketing is both new and old. Depending on how you navigate these factors, you will either succeed or fail. Either your video is going to convert people into buyers or they're just going to sit there on YouTube getting zero views. 
                                    <br><br>
                                    So, with this video training you will discover the secrets of successful video marketing- The Modern and Effective Ways of Video Marketing, Modern Video Marketing Essentials, Types of Video Marketing and much more 
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-------bonus 4------->
                     <div class="col-12 col-md-6 mt20 mt-md0 ">
                        <div class="bonus-section-shape col-12 ">
                           <div class="row align-items-center ">
                              <div class="col-md-6 mx-auto">
                                 <img src="assets/images/bonus-box.webp " class="img-fluid d-block mx-auto ">
                              </div>
                              <div class="col-12 mt20 mt-md40">
                                 <div class="w700 f-22 f-md-24 balck-clr2 lh140 ">
                                    Youtube Authority  
                                 </div>
                                 <div class="f-18 f-md-20 w400 balck-clr2 lh140 mt15 ">
                                    Since its launch in 2005, YouTube has come a long way. It has become an extremely powerful tool for businesses to increase awareness of their brand, drive more traffic to their company sites, and reach a broad audience around the world.So, if you are isn’t already leveraging the power of YouTube there are some massive benefits that you’re missing out on. 
<br><br>
                                    With this video course you will: <br>
                                    <div class="f-20 f-md-20 w400 lh140 balck-clr2">
                      
                                       <ul class="noneed-listing pl0">
<li>A Clear understanding on starting a YouTube channel.  </li>
<li>Determine your target audience.  </li>
<li>Learn about the different types of videos  </li>
<li>Discover how you can increase engagement  </li>
<li>Learn the different avenues for monetizing your YouTube channel </li>
<li>Learn about the different mistakes that you can make on your YouTube channel     </li>                           
       
                                       </ul>
                                   </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt30 mt-md70">
                  <div class="f-24 f-md-50 w700 balck-clr2 lh140 text-center ">
                     That’s A Total Value of <br class="d-none d-md-block "> <span class="f-28 f-md-50 red-clr"> $2285</span>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--Bonus Section ends -->
<!-- License Section Start -->
<!-- <div class="license-section">
   <div class="container">
      <div class="row">

         <div class="col-12 text-center">
            <div class="w700 f-28 f-md-45 balck-clr2 text-center lh140">
               Also Get Our Free Commercial License When <br class="d-none d-md-block">
               You Get Access To Aicademy Today!
            </div>
         </div>
      </div>
      <div class="row align-items-center mt20 mt-md70">
         <div class="col-md-6 order-md-2">
            <div class="f-md-20 f-18 w400 lh140 balck-clr2">
               As we have shown you, there are tons of businesses & marketers – yoga gurus, fitness centers, social media marketers or anybody else who want to start online & Drive FREE Traffic, NEED YOUR SERVICE & would love pay you monthly for your services.<br><br>

               Build their branded & traffic generating video channels. We’ve taken care of everything so that you can deliver those services easily & 100% profits in your pocket.<br><br>
               
              <span class="w700"> Note:</span> This special commercial license is being included in the Advanced plan and ONLY during this launch. Take advantage of it now because it will never be offered again.
            </div>
         </div>
         <div class="col-md-6 col-10 mx-auto mt20 mt-md0 order-md-1">
            <img src="assets/images/commercial-license.webp" class="img-fluid d-block mx-auto" alt="Features">
         </div>
      </div>
   </div>
</div> -->
<!-- License Section End -->
  <!-- Guarantee Section Start -->
  <div class="riskfree-section ">
   <div class="container ">
      <div class="row align-items-center ">
         <div class="col-12 text-center mb20 mb-md40">
            <div class="f-24 f-md-36 w600 lh140 white-clr">
               We’re Backing Everything Up with An Iron Clad...
            </div>
            <div class="f-md-45 f-28 w700 lh140 white-clr">
               "30-Day Risk-Free Money Back Guarantee"
            </div>
         </div>
         <div class="col-md-7 col-12 order-md-2">
            <div class="f-md-20 f-18 w400 lh140 white-clr mt15">
               I'm 100% confident that Aicademy will help you take your online business to the next level. I'm so confident on it that I'm making this offer a risk-free investment for you.
<br><br>
               If you concluded that, HONESTLY nothing of this has helped you in any way, you can take advantage of our "30-day Money Back Guarantee" and simply ask for a refund within 30 days!
               <br><br>
               Note: For refund, we will require a valid reason along with the proof that you tried our system, but it didn't work for you!
               <br><br>
               I am considering your money to be kept safe on the table between us and waiting for you to APPLY and successfully use this product, and get best results, so then you can feel it is a great investment.
            </div>
         </div>
         <div class="col-md-5 order-md-1 col-12 mt20 mt-md0 ">
            <img src="assets/images/riskfree-img.webp " class="img-fluid d-block mx-auto ">
         </div>
      </div>
   </div>
</div>
<!-- Guarantee Section End -->

      <!-- Discounted Price Section Start -->
      <div class="table-section " id="buynow">
         <div class="container ">
            <div class="row ">
               <div class="col-12">
                  <div class="f-20 f-md-24 w500 text-center lh140">
                     Take Advantage of Our Limited Time Deal
                  </div>
                  <div class="f-md-48 f-28 w700 lh140 text-center mt10 ">
                     Get Aicademy For A Low One-Time-<br class="d-none d-md-block">
                     Price, No Monthly Fee.
                  </div>
                  <!-- <div class="f-18 f-md-20 w400 lh140 text-center mt20 balck-clr">
                     Use Discount Coupon <span class="red-clr w700">"Aicademy"</span> for Instant <span class="red-clr w700">$3 OFF</span> on Commercial Plan
                 </div> -->
               </div>
               <div class="col-12 col-md-8 mx-auto mt20 mt-md50 ">
                  <div class="row">
                     <div class="col-12 col-md-10 mx-auto mt20 mt-md0">
                        <div class="table-wrap1 ">
                           <div class="table-head1 ">
                              <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 644.49 120" style="max-height:65px">
                                 <defs>
                                    <style>
                                       .cls-1 {
                                       fill: #fff;
                                       }
                                 
                                       .cls-2 {
                                       fill: #0af;
                                       }
                                    </style>
                                 </defs>
                                 <g id="Layer_1-2" data-name="Layer 1">
                                    <g>
                                       <g>
                                          <circle class="cls-2" cx="79.02" cy="43.5" r="7.25"></circle>
                                          <circle class="cls-2" cx="70.47" cy="78.46" r="5.46"></circle>
                                          <path class="cls-2" d="m158.03,45.28l-33.74,13.31v36.4l-45.27,16.86-27.9-10.4c-4.17,3.37-7.04,6.74-8.15,10.11-1.01,3.22-.07,6.21,2.46,8.42-5.44-1.46-8-5.25-6.96-9.76,1.24-3.62,4.79-7.68,10-12.08h.01c3.95-3.31,8.84-6.84,14.46-10.5,2.04,1.67,4.68,2.68,7.53,2.68,6.56,0,11.88-5.32,11.88-11.88s-5.32-11.88-11.88-11.88-11.88,5.32-11.88,11.88c0,1.13.15,2.21.46,3.25-8.01,5.01-14.83,10.36-19.57,15.44l-5.75-2.14v-7.66c-7.84,4.92-11.84,11.06-5.99,16.99-5.17-1.64-7.69-4.49-7.69-8.05,0-4.83,4.67-10.99,13.69-17.26l35.41-23.23c2.71,2.18,6.13,3.48,9.87,3.48,8.7,0,15.76-7.06,15.76-15.76s-7.06-15.76-15.76-15.76-15.76,7.06-15.76,15.76c0,1.59.23,3.12.68,4.57l-30.2,19.53v-9L0,45.28,79.02,0l79.01,45.28Z"></path>
                                          <path class="cls-2" d="m149.38,68.32v-19.62l-1.51.6v19.03c-1.66.37-2.9,1.96-2.9,3.85v11.69h7.31v-11.69c0-1.9-1.24-3.48-2.9-3.85Z"></path>
                                       </g>
                                       <g>
                                          <g>
                                          <path class="cls-2" d="m178.03,98.08l22.25-71.1h19.3l22.25,71.1h-17.01l-4.36-16.9h-21.7l-4.36,16.9h-16.36Zm25.74-35.99l-1.74,6.54h15.16l-1.64-6.54c-.95-3.56-1.91-7.34-2.89-11.34-.98-4-1.95-7.85-2.89-11.56h-.44c-.87,3.78-1.76,7.65-2.67,11.61-.91,3.96-1.87,7.73-2.89,11.29Z"></path>
                                          <path class="cls-2" d="m256.22,36.03c-2.69,0-4.87-.76-6.54-2.29-1.67-1.53-2.51-3.56-2.51-6.11s.83-4.58,2.51-6.11c1.67-1.53,3.85-2.29,6.54-2.29s4.87.76,6.54,2.29c1.67,1.53,2.51,3.56,2.51,6.11s-.84,4.58-2.51,6.11c-1.67,1.53-3.85,2.29-6.54,2.29Zm-7.96,62.05v-54.09h16.03v54.09h-16.03Z"></path>
                                          </g>
                                          <g>
                                          <path class="cls-1" d="m299.89,99.38c-5.02,0-9.54-1.11-13.58-3.33-4.03-2.22-7.23-5.45-9.6-9.71-2.36-4.25-3.54-9.36-3.54-15.32s1.31-11.16,3.93-15.38c2.62-4.22,6.07-7.43,10.36-9.65,4.29-2.22,8.9-3.33,13.85-3.33,3.34,0,6.31.55,8.89,1.64,2.58,1.09,4.89,2.47,6.92,4.14l-7.52,10.36c-2.55-2.11-4.98-3.16-7.31-3.16-3.85,0-6.92,1.38-9.21,4.14-2.29,2.76-3.44,6.51-3.44,11.23s1.15,8.38,3.44,11.18c2.29,2.8,5.18,4.2,8.67,4.2,1.74,0,3.45-.38,5.13-1.15,1.67-.76,3.2-1.69,4.58-2.78l6.33,10.47c-2.69,2.33-5.6,3.98-8.72,4.96-3.13.98-6.18,1.47-9.16,1.47Z"></path>
                                          <path class="cls-1" d="m339.81,99.38c-4.94,0-8.87-1.58-11.78-4.74-2.91-3.16-4.36-7.03-4.36-11.61,0-5.67,2.4-10.1,7.2-13.3,4.8-3.2,12.54-5.34,23.23-6.43-.15-2.4-.86-4.31-2.13-5.73-1.27-1.42-3.4-2.13-6.38-2.13-2.25,0-4.54.44-6.87,1.31-2.33.87-4.8,2.07-7.42,3.6l-5.78-10.58c3.42-2.11,7.07-3.82,10.96-5.13,3.89-1.31,7.94-1.96,12.16-1.96,6.91,0,12.21,2,15.92,6,3.71,4,5.56,10.14,5.56,18.43v30.97h-13.09l-1.09-5.56h-.44c-2.25,2.04-4.67,3.69-7.25,4.96-2.58,1.27-5.4,1.91-8.45,1.91Zm5.45-12.43c1.82,0,3.4-.42,4.74-1.25,1.34-.83,2.71-1.94,4.09-3.33v-9.49c-5.67.73-9.6,1.87-11.78,3.44-2.18,1.56-3.27,3.4-3.27,5.51,0,1.74.56,3.04,1.69,3.87,1.13.84,2.63,1.25,4.53,1.25Z"></path>
                                          <path class="cls-1" d="m402.95,99.38c-6.69,0-12.05-2.53-16.09-7.58-4.03-5.05-6.05-11.98-6.05-20.77,0-5.89,1.07-10.96,3.22-15.21,2.14-4.25,4.94-7.51,8.4-9.76,3.45-2.25,7.07-3.38,10.85-3.38,2.98,0,5.49.51,7.52,1.53,2.04,1.02,3.96,2.4,5.78,4.14l-.66-8.29v-18.43h16.03v76.45h-13.09l-1.09-5.34h-.44c-1.89,1.89-4.11,3.47-6.65,4.74-2.55,1.27-5.13,1.91-7.74,1.91Zm4.14-13.09c1.74,0,3.33-.36,4.74-1.09,1.42-.73,2.78-2,4.09-3.82v-22.14c-1.38-1.31-2.85-2.22-4.42-2.73-1.56-.51-3.07-.76-4.53-.76-2.55,0-4.8,1.22-6.76,3.65-1.96,2.44-2.94,6.23-2.94,11.4s.85,9.21,2.56,11.72c1.71,2.51,4.13,3.76,7.25,3.76Z"></path>
                                          <path class="cls-1" d="m470.34,99.38c-5.16,0-9.81-1.13-13.96-3.38-4.14-2.25-7.42-5.49-9.81-9.71-2.4-4.22-3.6-9.31-3.6-15.27s1.22-10.94,3.65-15.16c2.43-4.22,5.62-7.47,9.54-9.76,3.93-2.29,8.03-3.44,12.32-3.44,5.16,0,9.43,1.15,12.81,3.44,3.38,2.29,5.92,5.38,7.63,9.27,1.71,3.89,2.56,8.31,2.56,13.25,0,1.38-.07,2.74-.22,4.09-.15,1.35-.29,2.34-.44,3h-32.39c.73,3.93,2.36,6.82,4.91,8.67,2.54,1.85,5.6,2.78,9.16,2.78,3.85,0,7.74-1.2,11.67-3.6l5.34,9.71c-2.76,1.89-5.85,3.38-9.27,4.47-3.42,1.09-6.73,1.64-9.92,1.64Zm-12-34.24h19.52c0-2.98-.71-5.43-2.13-7.36-1.42-1.93-3.73-2.89-6.92-2.89-2.47,0-4.69.86-6.65,2.56-1.96,1.71-3.24,4.27-3.82,7.69Z"></path>
                                          <path class="cls-1" d="m502.62,98.08v-54.09h13.09l1.09,6.98h.44c2.25-2.25,4.65-4.2,7.2-5.83,2.54-1.64,5.6-2.45,9.16-2.45,3.85,0,6.96.78,9.32,2.34,2.36,1.56,4.23,3.8,5.62,6.71,2.4-2.47,4.94-4.6,7.63-6.38,2.69-1.78,5.82-2.67,9.38-2.67,5.82,0,10.09,1.95,12.81,5.83,2.73,3.89,4.09,9.21,4.09,15.98v33.59h-16.03v-31.52c0-3.93-.53-6.61-1.58-8.07-1.05-1.45-2.74-2.18-5.07-2.18-2.69,0-5.78,1.74-9.27,5.23v36.53h-16.03v-31.52c0-3.93-.53-6.61-1.58-8.07-1.05-1.45-2.74-2.18-5.07-2.18-2.69,0-5.74,1.74-9.16,5.23v36.53h-16.03Z"></path>
                                          <path class="cls-1" d="m602.07,119.23c-1.6,0-3-.11-4.2-.33-1.2-.22-2.34-.47-3.44-.76l2.84-12.21c.51.07,1.09.2,1.74.38.65.18,1.27.27,1.85.27,2.69,0,4.76-.65,6.22-1.96,1.45-1.31,2.54-3.02,3.27-5.13l.76-2.84-20.83-52.67h16.14l7.74,23.23c.8,2.47,1.53,4.98,2.18,7.52.65,2.55,1.34,5.16,2.07,7.85h.44c.58-2.54,1.18-5.11,1.8-7.69.62-2.58,1.25-5.14,1.91-7.69l6.54-23.23h15.38l-18.76,54.63c-1.67,4.51-3.53,8.31-5.56,11.4-2.04,3.09-4.49,5.4-7.36,6.92-2.87,1.53-6.45,2.29-10.74,2.29Z"></path>
                                          </g>
                                       </g>
                                    </g>
                                 </g>
                              </svg>
                              <div class="f-22 f-md-32 w700 lh140 text-center text-uppercase mt15 white-clr ">
                              Traffic Premium
                              </div>
                           </div>
                           <div class=" ">
                              <ul class="table-list1 pl0 f-18 f-md-20 lh140 w400 balck-clr2 mb0">
                                 
                              <li>Create Unlimited Reels & Shorts </li>
                              <li>Create Reels & Short Videos Just by Using One Keyword  </li>
                              <li>Create Boomerang Short Videos to Engage your Audience </li>
                              <li>Create Picture Video by Using Keyword Search  </li>
                              <li>Create Video Using Your Own Video Clips or Stock Videos </li>
                              <li>Create High-Quality Explanatory Whiteboard Video Shorts </li>
                              <li>Add VoiceOver to any Video</li>
                              <li>Add Background Music to any Video  </li>
                              <li>50 + Short frame Templates and 25+ Vector Images</li>
                              <li>100+ Stylish Fonts to make video more Presentable  </li>
                              <li>Eye Catchy Header Text Style to grab attention  </li>
                              <li>Add Waves with different color to your Short videos  </li>
                              <li>100+ social sharing platforms to share the videos for viral traffic </li>
                              <li>Add Your Brand Logo and/or Watermark to your Videos </li>
                              <li>Share Videos Directly to YouTube Shorts </li>
                              <li>Store up to 2 GB of Video, Audio, and other media files  </li>
                              <li>24*7 Customer Support</li>
                              <li>Commercial License Included</li>
                              <li>Provide High In-Demand Video Creation Services to your Clients </li>
                                 <li class="headline ">BONUSES WORTH $1,988 FREE!!!</li>
                                 <li>Fast Action Bonus 1 - Niche Maketing Secrets</li>
                                 <li>Fast Action Bonus 2 - Viral Marketing Secrets</li>
                                 <li>Fast Action Bonus 3 - Modern Video Marketing</li>
                                 <li>Fast Action Bonus 4 - Youtube Authority</li>
                              </ul>
                           </div>
                           <div class="table-btn">
                              <div class="hideme-button ">
                                 <a href="https://warriorplus.com/o2/buy/hb06pf/lc8nh1/wrbx6s"><img src="https://warriorplus.com/o2/btn/fn200011000/hb06pf/lc8nh1/352981" class="img-fluid mx-auto d-block"></a> 
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50 ">
                  <div class="f-22 f-md-32 w700 balck-clr2 px0 text-md-center lh140 ">If you have any query, simply reach to us at <a href="mailto:support@bizomart.com" class="red-clr">support@bizomart.com</a>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Discounted Price Section End -->
    
      <!------Final Section------->
      <div class="final-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="but-design-border">
                     <div class="f-md-45 f-28 lh140 w700 balck-clr2 button-shape">
                        FINAL CALL
                     </div>
                  </div>
               </div>
               <div class="col-12 passport-content">
                  <div class="col-12 f-24 f-md-32 lh140 w600 text-center balck-clr2 mt20">
                     You Still Have the Opportunity to Raise Your Game… <br> Remember, It’s Your Make-or-Break Time! 
                  </div>
               </div>
            </div>
            <!-- CTA Btn Section Start -->
            <div class="row mt20 mt-md40">
               <div class="col-12 text-center">
                  <div class="f-18 f-md-26 w700 lh140 text-center balck-clr2">
                     Free Commercial License + Low 1-Time Price For Launch Period Only
                  </div>
                  <!-- <div class="f-20 f-md-24 w400 lh140 text-center mt20 balck-clr2">
                     Use Discount Coupon <span class="red-clr w700">"Aicademy"</span> for Instant <span class="red-clr w700">$3 OFF</span>
                 </div> -->
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center">
                        <a href="#buynow" class="cta-link-btn">Get Instant Access To Aicademy</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                     <img src="assets/images/compaitable-with.webp" class="img-responsive mx-xs-center md-img-right" alt="visa">
                     <div class="d-md-block d-none visible-md px-md30">
                        <img src="assets/images/v-line.webp" class="img-fluid" alt="line">
                     </div>
                     <img src="assets/images/days-gurantee.webp" class="img-responsive mx-xs-auto mt15 mt-md0" alt="30 days">
                  </div>
               </div>
            </div>
            <!-- CTA Btn Section End -->
               <div class="col-12 mt25 mt-md40 text-center">
                    <a class="kapblue f-md-24 f-18 lh140 w500" href="https://warriorplus.com/o/nothanks/lc8nh1" target="_blank" class="link">
                    No thanks - I Don't Want To Creates 100s Reels, Boomerang, & Short Videos With Just One Keyword... For Floods Of FREE Traffic And Sales... Please Take Me To The Next Step To Get Access To My Purchase.
                    </a>
               </div>
         </div>
      </div>
      <!------Final Section End------->

      <!------Footer Section-------->
      <div class="footer-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 644.49 120" style="max-height:55px">
                     <defs>
                        <style>
                           .cls-1 {
                           fill: #fff;
                           }
                     
                           .cls-2 {
                           fill: #0af;
                           }
                        </style>
                     </defs>
                     <g id="Layer_1-2" data-name="Layer 1">
                        <g>
                           <g>
                              <circle class="cls-2" cx="79.02" cy="43.5" r="7.25"></circle>
                              <circle class="cls-2" cx="70.47" cy="78.46" r="5.46"></circle>
                              <path class="cls-2" d="m158.03,45.28l-33.74,13.31v36.4l-45.27,16.86-27.9-10.4c-4.17,3.37-7.04,6.74-8.15,10.11-1.01,3.22-.07,6.21,2.46,8.42-5.44-1.46-8-5.25-6.96-9.76,1.24-3.62,4.79-7.68,10-12.08h.01c3.95-3.31,8.84-6.84,14.46-10.5,2.04,1.67,4.68,2.68,7.53,2.68,6.56,0,11.88-5.32,11.88-11.88s-5.32-11.88-11.88-11.88-11.88,5.32-11.88,11.88c0,1.13.15,2.21.46,3.25-8.01,5.01-14.83,10.36-19.57,15.44l-5.75-2.14v-7.66c-7.84,4.92-11.84,11.06-5.99,16.99-5.17-1.64-7.69-4.49-7.69-8.05,0-4.83,4.67-10.99,13.69-17.26l35.41-23.23c2.71,2.18,6.13,3.48,9.87,3.48,8.7,0,15.76-7.06,15.76-15.76s-7.06-15.76-15.76-15.76-15.76,7.06-15.76,15.76c0,1.59.23,3.12.68,4.57l-30.2,19.53v-9L0,45.28,79.02,0l79.01,45.28Z"></path>
                              <path class="cls-2" d="m149.38,68.32v-19.62l-1.51.6v19.03c-1.66.37-2.9,1.96-2.9,3.85v11.69h7.31v-11.69c0-1.9-1.24-3.48-2.9-3.85Z"></path>
                           </g>
                           <g>
                              <g>
                                <path class="cls-2" d="m178.03,98.08l22.25-71.1h19.3l22.25,71.1h-17.01l-4.36-16.9h-21.7l-4.36,16.9h-16.36Zm25.74-35.99l-1.74,6.54h15.16l-1.64-6.54c-.95-3.56-1.91-7.34-2.89-11.34-.98-4-1.95-7.85-2.89-11.56h-.44c-.87,3.78-1.76,7.65-2.67,11.61-.91,3.96-1.87,7.73-2.89,11.29Z"></path>
                                <path class="cls-2" d="m256.22,36.03c-2.69,0-4.87-.76-6.54-2.29-1.67-1.53-2.51-3.56-2.51-6.11s.83-4.58,2.51-6.11c1.67-1.53,3.85-2.29,6.54-2.29s4.87.76,6.54,2.29c1.67,1.53,2.51,3.56,2.51,6.11s-.84,4.58-2.51,6.11c-1.67,1.53-3.85,2.29-6.54,2.29Zm-7.96,62.05v-54.09h16.03v54.09h-16.03Z"></path>
                              </g>
                              <g>
                                <path class="cls-1" d="m299.89,99.38c-5.02,0-9.54-1.11-13.58-3.33-4.03-2.22-7.23-5.45-9.6-9.71-2.36-4.25-3.54-9.36-3.54-15.32s1.31-11.16,3.93-15.38c2.62-4.22,6.07-7.43,10.36-9.65,4.29-2.22,8.9-3.33,13.85-3.33,3.34,0,6.31.55,8.89,1.64,2.58,1.09,4.89,2.47,6.92,4.14l-7.52,10.36c-2.55-2.11-4.98-3.16-7.31-3.16-3.85,0-6.92,1.38-9.21,4.14-2.29,2.76-3.44,6.51-3.44,11.23s1.15,8.38,3.44,11.18c2.29,2.8,5.18,4.2,8.67,4.2,1.74,0,3.45-.38,5.13-1.15,1.67-.76,3.2-1.69,4.58-2.78l6.33,10.47c-2.69,2.33-5.6,3.98-8.72,4.96-3.13.98-6.18,1.47-9.16,1.47Z"></path>
                                <path class="cls-1" d="m339.81,99.38c-4.94,0-8.87-1.58-11.78-4.74-2.91-3.16-4.36-7.03-4.36-11.61,0-5.67,2.4-10.1,7.2-13.3,4.8-3.2,12.54-5.34,23.23-6.43-.15-2.4-.86-4.31-2.13-5.73-1.27-1.42-3.4-2.13-6.38-2.13-2.25,0-4.54.44-6.87,1.31-2.33.87-4.8,2.07-7.42,3.6l-5.78-10.58c3.42-2.11,7.07-3.82,10.96-5.13,3.89-1.31,7.94-1.96,12.16-1.96,6.91,0,12.21,2,15.92,6,3.71,4,5.56,10.14,5.56,18.43v30.97h-13.09l-1.09-5.56h-.44c-2.25,2.04-4.67,3.69-7.25,4.96-2.58,1.27-5.4,1.91-8.45,1.91Zm5.45-12.43c1.82,0,3.4-.42,4.74-1.25,1.34-.83,2.71-1.94,4.09-3.33v-9.49c-5.67.73-9.6,1.87-11.78,3.44-2.18,1.56-3.27,3.4-3.27,5.51,0,1.74.56,3.04,1.69,3.87,1.13.84,2.63,1.25,4.53,1.25Z"></path>
                                <path class="cls-1" d="m402.95,99.38c-6.69,0-12.05-2.53-16.09-7.58-4.03-5.05-6.05-11.98-6.05-20.77,0-5.89,1.07-10.96,3.22-15.21,2.14-4.25,4.94-7.51,8.4-9.76,3.45-2.25,7.07-3.38,10.85-3.38,2.98,0,5.49.51,7.52,1.53,2.04,1.02,3.96,2.4,5.78,4.14l-.66-8.29v-18.43h16.03v76.45h-13.09l-1.09-5.34h-.44c-1.89,1.89-4.11,3.47-6.65,4.74-2.55,1.27-5.13,1.91-7.74,1.91Zm4.14-13.09c1.74,0,3.33-.36,4.74-1.09,1.42-.73,2.78-2,4.09-3.82v-22.14c-1.38-1.31-2.85-2.22-4.42-2.73-1.56-.51-3.07-.76-4.53-.76-2.55,0-4.8,1.22-6.76,3.65-1.96,2.44-2.94,6.23-2.94,11.4s.85,9.21,2.56,11.72c1.71,2.51,4.13,3.76,7.25,3.76Z"></path>
                                <path class="cls-1" d="m470.34,99.38c-5.16,0-9.81-1.13-13.96-3.38-4.14-2.25-7.42-5.49-9.81-9.71-2.4-4.22-3.6-9.31-3.6-15.27s1.22-10.94,3.65-15.16c2.43-4.22,5.62-7.47,9.54-9.76,3.93-2.29,8.03-3.44,12.32-3.44,5.16,0,9.43,1.15,12.81,3.44,3.38,2.29,5.92,5.38,7.63,9.27,1.71,3.89,2.56,8.31,2.56,13.25,0,1.38-.07,2.74-.22,4.09-.15,1.35-.29,2.34-.44,3h-32.39c.73,3.93,2.36,6.82,4.91,8.67,2.54,1.85,5.6,2.78,9.16,2.78,3.85,0,7.74-1.2,11.67-3.6l5.34,9.71c-2.76,1.89-5.85,3.38-9.27,4.47-3.42,1.09-6.73,1.64-9.92,1.64Zm-12-34.24h19.52c0-2.98-.71-5.43-2.13-7.36-1.42-1.93-3.73-2.89-6.92-2.89-2.47,0-4.69.86-6.65,2.56-1.96,1.71-3.24,4.27-3.82,7.69Z"></path>
                                <path class="cls-1" d="m502.62,98.08v-54.09h13.09l1.09,6.98h.44c2.25-2.25,4.65-4.2,7.2-5.83,2.54-1.64,5.6-2.45,9.16-2.45,3.85,0,6.96.78,9.32,2.34,2.36,1.56,4.23,3.8,5.62,6.71,2.4-2.47,4.94-4.6,7.63-6.38,2.69-1.78,5.82-2.67,9.38-2.67,5.82,0,10.09,1.95,12.81,5.83,2.73,3.89,4.09,9.21,4.09,15.98v33.59h-16.03v-31.52c0-3.93-.53-6.61-1.58-8.07-1.05-1.45-2.74-2.18-5.07-2.18-2.69,0-5.78,1.74-9.27,5.23v36.53h-16.03v-31.52c0-3.93-.53-6.61-1.58-8.07-1.05-1.45-2.74-2.18-5.07-2.18-2.69,0-5.74,1.74-9.16,5.23v36.53h-16.03Z"></path>
                                <path class="cls-1" d="m602.07,119.23c-1.6,0-3-.11-4.2-.33-1.2-.22-2.34-.47-3.44-.76l2.84-12.21c.51.07,1.09.2,1.74.38.65.18,1.27.27,1.85.27,2.69,0,4.76-.65,6.22-1.96,1.45-1.31,2.54-3.02,3.27-5.13l.76-2.84-20.83-52.67h16.14l7.74,23.23c.8,2.47,1.53,4.98,2.18,7.52.65,2.55,1.34,5.16,2.07,7.85h.44c.58-2.54,1.18-5.11,1.8-7.69.62-2.58,1.25-5.14,1.91-7.69l6.54-23.23h15.38l-18.76,54.63c-1.67,4.51-3.53,8.31-5.56,11.4-2.04,3.09-4.49,5.4-7.36,6.92-2.87,1.53-6.45,2.29-10.74,2.29Z"></path>
                              </g>
                           </g>
                        </g>
                     </g>
                  </svg>
                  <div editabletype="text" class="f-16 f-md-18 w400 lh140 mt20 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
                  <div class=" mt20 mt-md40 white-clr"> <script type="text/javascript" src="https://warriorplus.com/o2/disclaimer/hb06pf" defer=""></script><div class="wplus_spdisclaimer f-17 w300 mt20 lh140 white-clr text-center"><strong>Disclaimer:</strong> <em>WarriorPlus is used to help manage the sale of products on this site. While WarriorPlus helps facilitate the sale, all payments are made directly to the product vendor and NOT WarriorPlus. Thus, all product questions, support inquiries and/or refund requests must be sent to the vendor. WarriorPlus's role should not be construed as an endorsement, approval or review of these products or any claim, statement or opinion used in the marketing of these products.</em></div></div>
              </div>
              <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-16 f-md-18 w400 lh140 white-clr text-xs-center">Copyright © Aicademy</div>
                  <ul class="footer-ul f-16 f-md-18 w400 white-clr text-center text-md-right">
                      <li><a href="https://support.bizomart.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                      <li><a href="http://aicademy.live/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                      <li><a href="http://aicademy.live/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                      <li><a href="http://aicademy.live/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                      <li><a href="http://aicademy.live/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                      <li><a href="http://aicademy.live/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                      <li><a href="http://aicademy.live/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
              </div>
          </div>
      </div>
  </div>
      <!------Footer Section End-------->
         <!-- Exit Popup and Timer -->
   <?php include('timer.php'); ?>
   <!-- Exit Popup and Timer End -->
   </body>
</html>
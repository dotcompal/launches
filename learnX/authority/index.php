<!Doctype html>
<html>

<head>
    <title>Aicademy Authority</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=9">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">

    <meta name="title" content="Aicademy | Authority">
    <meta name="description" content="Aicademy">
    <meta name="keywords" content="Aicademy">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta property="og:image" content="https://www.aicademy.live/authority/thumbnail.png">
    <meta name="language" content="English">
    <meta name="revisit-after" content="1 days">
    <meta name="author" content="Pranshu Gupta">

    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:title" content="Aicademy | Authority">
    <meta property="og:description" content="Aicademy">
    <meta property="og:image" content="https://www.aicademy.live/authority/thumbnail.png">

    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:title" content="Aicademy | Authority">
    <meta property="twitter:description" content="Aicademy">
    <meta property="twitter:image" content="https://www.aicademy.live/authority/thumbnail.png">

    <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
    <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
    <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&family=Poppins:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    <!-- Start Editor required -->
    <!-- Start Editor required -->
    <link rel="stylesheet" href="https://cdn.oppyotest.com/launches/sellero/common_assets/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="assets/css/style.css" type="text/css">
    <link rel="stylesheet" href="assets/css/timer.css" type="text/css">
    <script src="https://cdn.oppyotest.com/launches/sellero/common_assets/js/bootstrap.min.js"></script>
    <script src="https://cdn.oppyotest.com/launches/sellero/common_assets/js/jquery.min.js"></script>
    <!-- End -->
</head>

<body>
<div class="warning-section">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <div class="offer">
                        <div>
                            <img src="https://cdn.oppyo.com/launches/appzilo/special/error.webp">
                        </div>
                        <div class="f-22 f-md-30 w500 lh140 white-clr text-center">
                            <span class="w600">HOLD ON:</span>  Your Order Is Not Complete Yet...
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--1. Header Section Start -->
    <div class="header-section">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 644.49 120" style="max-height:55px">
                        <defs>
                          <style>
                            .cls-1 {
                              fill: #fff;
                            }
                      
                            .cls-2 {
                              fill: #0af;
                            }
                          </style>
                        </defs>
                        <g id="Layer_1-2" data-name="Layer 1">
                          <g>
                            <g>
                              <circle class="cls-2" cx="79.02" cy="43.5" r="7.25"/>
                              <circle class="cls-2" cx="70.47" cy="78.46" r="5.46"/>
                              <path class="cls-2" d="m158.03,45.28l-33.74,13.31v36.4l-45.27,16.86-27.9-10.4c-4.17,3.37-7.04,6.74-8.15,10.11-1.01,3.22-.07,6.21,2.46,8.42-5.44-1.46-8-5.25-6.96-9.76,1.24-3.62,4.79-7.68,10-12.08h.01c3.95-3.31,8.84-6.84,14.46-10.5,2.04,1.67,4.68,2.68,7.53,2.68,6.56,0,11.88-5.32,11.88-11.88s-5.32-11.88-11.88-11.88-11.88,5.32-11.88,11.88c0,1.13.15,2.21.46,3.25-8.01,5.01-14.83,10.36-19.57,15.44l-5.75-2.14v-7.66c-7.84,4.92-11.84,11.06-5.99,16.99-5.17-1.64-7.69-4.49-7.69-8.05,0-4.83,4.67-10.99,13.69-17.26l35.41-23.23c2.71,2.18,6.13,3.48,9.87,3.48,8.7,0,15.76-7.06,15.76-15.76s-7.06-15.76-15.76-15.76-15.76,7.06-15.76,15.76c0,1.59.23,3.12.68,4.57l-30.2,19.53v-9L0,45.28,79.02,0l79.01,45.28Z"/>
                              <path class="cls-2" d="m149.38,68.32v-19.62l-1.51.6v19.03c-1.66.37-2.9,1.96-2.9,3.85v11.69h7.31v-11.69c0-1.9-1.24-3.48-2.9-3.85Z"/>
                            </g>
                            <g>
                              <g>
                                <path class="cls-2" d="m178.03,98.08l22.25-71.1h19.3l22.25,71.1h-17.01l-4.36-16.9h-21.7l-4.36,16.9h-16.36Zm25.74-35.99l-1.74,6.54h15.16l-1.64-6.54c-.95-3.56-1.91-7.34-2.89-11.34-.98-4-1.95-7.85-2.89-11.56h-.44c-.87,3.78-1.76,7.65-2.67,11.61-.91,3.96-1.87,7.73-2.89,11.29Z"/>
                                <path class="cls-2" d="m256.22,36.03c-2.69,0-4.87-.76-6.54-2.29-1.67-1.53-2.51-3.56-2.51-6.11s.83-4.58,2.51-6.11c1.67-1.53,3.85-2.29,6.54-2.29s4.87.76,6.54,2.29c1.67,1.53,2.51,3.56,2.51,6.11s-.84,4.58-2.51,6.11c-1.67,1.53-3.85,2.29-6.54,2.29Zm-7.96,62.05v-54.09h16.03v54.09h-16.03Z"/>
                              </g>
                              <g>
                                <path class="cls-1" d="m299.89,99.38c-5.02,0-9.54-1.11-13.58-3.33-4.03-2.22-7.23-5.45-9.6-9.71-2.36-4.25-3.54-9.36-3.54-15.32s1.31-11.16,3.93-15.38c2.62-4.22,6.07-7.43,10.36-9.65,4.29-2.22,8.9-3.33,13.85-3.33,3.34,0,6.31.55,8.89,1.64,2.58,1.09,4.89,2.47,6.92,4.14l-7.52,10.36c-2.55-2.11-4.98-3.16-7.31-3.16-3.85,0-6.92,1.38-9.21,4.14-2.29,2.76-3.44,6.51-3.44,11.23s1.15,8.38,3.44,11.18c2.29,2.8,5.18,4.2,8.67,4.2,1.74,0,3.45-.38,5.13-1.15,1.67-.76,3.2-1.69,4.58-2.78l6.33,10.47c-2.69,2.33-5.6,3.98-8.72,4.96-3.13.98-6.18,1.47-9.16,1.47Z"/>
                                <path class="cls-1" d="m339.81,99.38c-4.94,0-8.87-1.58-11.78-4.74-2.91-3.16-4.36-7.03-4.36-11.61,0-5.67,2.4-10.1,7.2-13.3,4.8-3.2,12.54-5.34,23.23-6.43-.15-2.4-.86-4.31-2.13-5.73-1.27-1.42-3.4-2.13-6.38-2.13-2.25,0-4.54.44-6.87,1.31-2.33.87-4.8,2.07-7.42,3.6l-5.78-10.58c3.42-2.11,7.07-3.82,10.96-5.13,3.89-1.31,7.94-1.96,12.16-1.96,6.91,0,12.21,2,15.92,6,3.71,4,5.56,10.14,5.56,18.43v30.97h-13.09l-1.09-5.56h-.44c-2.25,2.04-4.67,3.69-7.25,4.96-2.58,1.27-5.4,1.91-8.45,1.91Zm5.45-12.43c1.82,0,3.4-.42,4.74-1.25,1.34-.83,2.71-1.94,4.09-3.33v-9.49c-5.67.73-9.6,1.87-11.78,3.44-2.18,1.56-3.27,3.4-3.27,5.51,0,1.74.56,3.04,1.69,3.87,1.13.84,2.63,1.25,4.53,1.25Z"/>
                                <path class="cls-1" d="m402.95,99.38c-6.69,0-12.05-2.53-16.09-7.58-4.03-5.05-6.05-11.98-6.05-20.77,0-5.89,1.07-10.96,3.22-15.21,2.14-4.25,4.94-7.51,8.4-9.76,3.45-2.25,7.07-3.38,10.85-3.38,2.98,0,5.49.51,7.52,1.53,2.04,1.02,3.96,2.4,5.78,4.14l-.66-8.29v-18.43h16.03v76.45h-13.09l-1.09-5.34h-.44c-1.89,1.89-4.11,3.47-6.65,4.74-2.55,1.27-5.13,1.91-7.74,1.91Zm4.14-13.09c1.74,0,3.33-.36,4.74-1.09,1.42-.73,2.78-2,4.09-3.82v-22.14c-1.38-1.31-2.85-2.22-4.42-2.73-1.56-.51-3.07-.76-4.53-.76-2.55,0-4.8,1.22-6.76,3.65-1.96,2.44-2.94,6.23-2.94,11.4s.85,9.21,2.56,11.72c1.71,2.51,4.13,3.76,7.25,3.76Z"/>
                                <path class="cls-1" d="m470.34,99.38c-5.16,0-9.81-1.13-13.96-3.38-4.14-2.25-7.42-5.49-9.81-9.71-2.4-4.22-3.6-9.31-3.6-15.27s1.22-10.94,3.65-15.16c2.43-4.22,5.62-7.47,9.54-9.76,3.93-2.29,8.03-3.44,12.32-3.44,5.16,0,9.43,1.15,12.81,3.44,3.38,2.29,5.92,5.38,7.63,9.27,1.71,3.89,2.56,8.31,2.56,13.25,0,1.38-.07,2.74-.22,4.09-.15,1.35-.29,2.34-.44,3h-32.39c.73,3.93,2.36,6.82,4.91,8.67,2.54,1.85,5.6,2.78,9.16,2.78,3.85,0,7.74-1.2,11.67-3.6l5.34,9.71c-2.76,1.89-5.85,3.38-9.27,4.47-3.42,1.09-6.73,1.64-9.92,1.64Zm-12-34.24h19.52c0-2.98-.71-5.43-2.13-7.36-1.42-1.93-3.73-2.89-6.92-2.89-2.47,0-4.69.86-6.65,2.56-1.96,1.71-3.24,4.27-3.82,7.69Z"/>
                                <path class="cls-1" d="m502.62,98.08v-54.09h13.09l1.09,6.98h.44c2.25-2.25,4.65-4.2,7.2-5.83,2.54-1.64,5.6-2.45,9.16-2.45,3.85,0,6.96.78,9.32,2.34,2.36,1.56,4.23,3.8,5.62,6.71,2.4-2.47,4.94-4.6,7.63-6.38,2.69-1.78,5.82-2.67,9.38-2.67,5.82,0,10.09,1.95,12.81,5.83,2.73,3.89,4.09,9.21,4.09,15.98v33.59h-16.03v-31.52c0-3.93-.53-6.61-1.58-8.07-1.05-1.45-2.74-2.18-5.07-2.18-2.69,0-5.78,1.74-9.27,5.23v36.53h-16.03v-31.52c0-3.93-.53-6.61-1.58-8.07-1.05-1.45-2.74-2.18-5.07-2.18-2.69,0-5.74,1.74-9.16,5.23v36.53h-16.03Z"/>
                                <path class="cls-1" d="m602.07,119.23c-1.6,0-3-.11-4.2-.33-1.2-.22-2.34-.47-3.44-.76l2.84-12.21c.51.07,1.09.2,1.74.38.65.18,1.27.27,1.85.27,2.69,0,4.76-.65,6.22-1.96,1.45-1.31,2.54-3.02,3.27-5.13l.76-2.84-20.83-52.67h16.14l7.74,23.23c.8,2.47,1.53,4.98,2.18,7.52.65,2.55,1.34,5.16,2.07,7.85h.44c.58-2.54,1.18-5.11,1.8-7.69.62-2.58,1.25-5.14,1.91-7.69l6.54-23.23h15.38l-18.76,54.63c-1.67,4.51-3.53,8.31-5.56,11.4-2.04,3.09-4.49,5.4-7.36,6.92-2.87,1.53-6.45,2.29-10.74,2.29Z"/>
                              </g>
                            </g>
                          </g>
                        </g>
                      </svg>
                </div>
                <div class="col-12 text-center lh150 mt20 mt-md50">
                    <div class="pre-heading f-18 f-md-22 w600 lh140 white-clr">
                    Before you access your purchase, I want to ask you a very simple yet important question...
                </div>
                </div>
                <div class="col-12 mt-md30 mt20 f-md-50 f-28 w400 text-center white-clr lh140">
                    How Would You Like To <span class="w700 yellow-clr">Drive More Traffic & Sales To 100X Your Profits With No Extra Efforts? </span>
                </div>
                <div class="col-12 mt-md25 mt20">
                    <div class="f-20 f-md-22 w500 lh140 white-clr text-center">
                    Unlock the Most Powerful Features to Get 100X Better Conversions & Sales<br class="d-none d-md-block"> with this Authority Upgrade
                </div>
                </div>
                <div class="col-12 col-md-10 mx-auto mt-md30 mt20">
                    <img src="assets/images/productbox.webp" class="img-fluid d-block mx-auto">
                    <!-- <div class="responsive-video">
                        <iframe src="https://coursesify.dotcompal.com/video/embed/mws136rt4t" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                    </div> -->
                </div>
                <div class="col-12 mt30 mt-md50 f-md-24 f-20 lh140 w400 text-center white-clr">
                    This is An Exclusive Deal for New<span class="w600 yellow-clr"> "Aicademy"</span> Users Only...
                </div>
                <div class="col-md-12 mx-auto col-12 mt15 mt-md20 f-20 f-md-35 text-center probtn1">
                    <a href="#buynow" class="text-center">Upgrade to Aicademy Authority Now</a>
                </div>
            </div>
        </div>
    </div>
    <!--1. Header Section End -->



    <!--2. Second Section Start -->
    <div class="second-section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="f-28 f-md-45 w500 lh140 text-capitalize text-center black-clr">
                        The Secret Weapon To Boost Your Profits <span class="w700">From Your Same Academy; Same Courses</span> And Never Have To Look Back Again
                    </div>

                    <div class="f-18 f-md-20 w400 lh140 text-center mt15 mt-md40">
                        This Authority Upgrade puts you on whole another level with a touch of a button.
                        <br><br>From empowering your academy site for more engagement, leads and traffic to boosting your authority, automated social sharing campaigns, capturing more attention with popup ads and monetizing your academy sites with banner
                        placements, <span class="w600">this Authority upgrade has it all.</span>
                    </div>
                </div>

                <div class="col-12">
                    <div class="row">
                        <div class="col-md-6 col-12 mt30 mt-md40 ">
                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-center text-center text-md-start mt20 mt-md50">
                                <img src="assets/images/fe1.webp" class="img-fluid d-block mx-auto">
                                <div class="f-18 f-md-18 lh150 w400 ml-md20 mt20 mt-md0 text-capitalize w-100">
                                    <span class="w600">Empower Your Website for More User Interactions, Leads & FREE SEO Traffic</span>
                                </div>
                            </div>

                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-center text-center text-md-start mt20 mt-md50">
                                <img src="assets/images/fe2.webp" class="img-fluid d-block mx-auto">
                                <div class="f-18 f-md-18 lh150 w400 ml-md20 mt20 mt-md0 text-capitalize w-100">
                                    <span class="w600">Setup Automated Social Campaigns to Get More Exposure & Social Traffic Hands Free</span>
                                </div>
                            </div>

                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-center text-center text-md-start mt20 mt-md50">
                                <img src="assets/images/fe3.webp" class="img-fluid d-block mx-auto">
                                <div class="f-18 f-md-18 lh150 w400 ml-md20 mt20 mt-md0 text-capitalize w-100">
                                    <span class="w600">Generate Viral Traffic by Enabling Social Sharing on Your Marketplace & Blog </span>
                                </div>
                            </div>

                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-center text-center text-md-start mt20 mt-md50">
                                <img src="assets/images/fe4.webp" class="img-fluid d-block mx-auto">
                                <div class="f-18 f-md-18 lh150 w400 ml-md20 mt20 mt-md0 text-capitalize w-100">
                                    <span class="w600">Monetize Your Sites with Banner Placements</span>
                                </div>
                            </div>
                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-center text-center text-md-start mt20 mt-md50">
                                <img src="assets/images/fe5.webp" class="img-fluid d-block mx-auto">
                                <div class="f-18 f-md-18 lh150 w400 ml-md20 mt20 mt-md0 text-capitalize w-100">
                                    <span class="w600">Capture More Leads With 30 EXTRA carefully crafted Lead Popup Templates</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-12 mt30 mt-md40">
                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-center text-center text-md-start mt20 mt-md50">
                                <img src="assets/images/fe6.webp" class="img-fluid d-block mx-auto">
                                <div class="f-18 f-md-18 lh150 w400 ml-md20 mt20 mt-md0 text-capitalize w-100">
                                    <span class="w600">Get High PR Backlinks For Faster Indexing And Targeted Traffic </span> <br>
                                </div>
                            </div>

                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-center text-center text-md-start mt20 mt-md50">
                                <img src="assets/images/fe7.webp" class="img-fluid d-block mx-auto">

                                <div class="f-18 f-md-18 lh150 w400 ml-md20 mt20 mt-md0 text-capitalize w-100">
                                    <span class="w600">15+ High Converting Social Sharing Popup Templates To Get Even More FREE Viral Traffic</span>
                                </div>

                            </div>
                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-center text-center text-md-start mt20 mt-md50">
                                <img src="assets/images/fe8.webp" class="img-fluid d-block mx-auto">
                                <div class="f-18 f-md-18 lh150 w400 ml-md20 mt20 mt-md0 text-capitalize w-100">
                                    <span class="w600">20 Premium Promo Popup Templates to get more conversions</span>
                                </div>
                            </div>
                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-center text-center text-md-start mt20 mt-md50">
                                <img src="assets/images/fe9.webp" class="img-fluid d-block mx-auto">
                                <div class="f-18 f-md-18 lh150 w400 ml-md20 mt20 mt-md0 text-capitalize w-100">
                                    <span class="w600">Special Effects for Your Popup Ads To Visually Entice Your Visitors To Do Nothing But Clicking And Buying</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 mt30 mt-md50 f-md-24 f-20 lh140 w400 text-center black-clr">
                        Authority Upgrade… Special Offer <span class="w600">"Aicademy"</span> Users Only...
                    </div>
                    <div class="col-md-12 mx-auto col-12 mt15 mt-md20 f-20 f-md-35 text-center probtn1">
                        <a href="#buynow" class="text-center">Upgrade to Aicademy Authority Now</a>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    <!--2. Second Section End -->

    <!--3. third Section Starts -->
    <div class="proudly-section">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <div class="f-28 f-md-38 w700 white-clr lh140 presenting-head">
                       Proudly Presenting…
                    </div>
                 </div>
                <div class="col-12 f-28 f-md-45 w700 mt20 mt-md30 text-center white-clr lh140">
                   <span class="yellow-clr">Authority Upgrade...</span>  Special Offer
                </div>

                <div class="col-12 col-md-9 mx-auto mt20 mt-md40">
                    <img src="assets/images/productbox.webp" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 mt25 mt-md25 f-md-20 f-18 w400 white-clr mt-md20 text-start lh140">
                    It's  Pranshu & BizOmart here again I am sure you can’t wait to get your hands on Aicademy and start profiting from this multi-billion dollar market.<br><br> And to grab a fair share of this huge market, you just need one product, one
                    funnel, a few integrations and you are set to rule.<br><br> But we went a step ahead and empower you to customise and supercharge multiple courses and funnels in just a matter of minutes.<br><br> And there is a great deal ahead…
                    <br><br> The More Courses and funnels you deploy, the more profit you make. All You needed is traffic, brand power and Authority level solution.<br><br> Aicademy Authority Upgrade ensures you can scale quickly without having to worry
                    about restrictions...
                </div>
            </div>
        </div>
    </div>

    <!--3. third Section End -->

    <!--feature1 -->
    <div class="white-section">
        <div class="container ">
            <div class="row">
                <div class="col-12">
                    <img src="../common_assets/images/1.webp" class="img-fluid d-block">
                </div>
                <div class="col-12 f-24 f-md-36 w500 lh140 mt10 black-clr">
                    Empower Your Website for More User Interactions, Leads & FREE SEO Traffic
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md30">
                    <img src="assets/images/f1.webp" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 f-18 f-md-20 w400 lh140 mt20 mt-md50">
                    Give your audience the reason to love your academy website by allowing them to interact with your masterly crafted blog posts. So, empower your blogs by allowing visitors to like & dislike your posts and leave comment on it. <br><br>                    You also generate their leads and <span class="w600"> get fresh user generated content that will further boost your search engine ranking and FREE Search traffic.</span><br><br> You are also getting 50DFY blog posts with images to
                    promote your courses on 5 hot niches and establish yourself as an <span class="w600">Authority</span>.
                </div>
            </div>
        </div>
    </div>
    <!--feature2 -->
    <div class="grey-section">
        <div class="container ">
            <div class="row">
                <div class="col-12">
                    <img src="../common_assets/images/2.webp" class="img-fluid d-block">
                </div>
                <div class="col-12 f-24 f-md-36 w500 lh140 mt10 black-clr">
                    Setup Automated Social Campaigns to Get More Exposure & Social Traffic Hands Free
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md30">
                    <img src="assets/images/f2.webp" class="img-fluid d-block mx-auto mt20 mt-md20">
                </div>
                <div class="col-12 f-18 f-md-20 w400 lh140 mt20 mt-md50">
                    Setup your social media sharing campaigns to share your blog posts and courses on major platforms for free social traffic on automation.<br> <br> You just need to set the rules once, and <span class="w600">let the software choose your content  to share and drive traffic for you without any manual headache</span>
                </div>
            </div>
        </div>
    </div>

    <!--feature3 -->
    <div class="white-section">
        <div class="container ">
            <div class="row">
                <div class="col-12">
                    <img src="../common_assets/images/3.webp" class="img-fluid d-block">
                </div>
                <div class="col-12 f-24 f-md-36 w500 lh140 mt10 black-clr">
                    Get High PR Backlinks For Faster Indexing And Targeted Traffic
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md30">
                    <img src="assets/images/f3.webp" class="img-fluid d-block mx-auto mt20 mt-md20">
                </div>
                <div class="col-12 f-18 f-md-20 w400 lh140 mt20 mt-md50">
                    <span class="w600"> You get backlinks from High PR sites like Tumblr, Bloggr & social media giants</span> –This will help you index your content faster
                </div>
            </div>
        </div>
    </div>
    </div>

    <!--feature4 -->
    <div class="grey-section">
        <div class="container ">
            <div class="row">
                <div class="col-12">
                    <img src="../common_assets/images/4.webp" class="img-fluid d-block">
                </div>
                <div class="col-12 f-24 f-md-36 w500 lh140 mt10 black-clr">
                    Generate Viral Traffic by Enabling Social Sharing on Your Marketplace & Blog
                </div>
                <div class="col-12 col-md-8 mx-auto mt20 mt-md40">
                    <img src="assets/images/f4.webp" class="img-fluid d-block mx-auto">
                </div>
            </div>

            <div class="col-12 f-18 f-md-20 w400 lh140 mt20 mt-md50">
                Authority edition gives you another cool feature of <span class="w600">sharing your blogs and products to boost traffic.</span> Now stop thinking and use this masterpiece to get results like never before.
            </div>
        </div>
    </div>

    <!--feature5 -->
    <div class="white-section">
        <div class="container ">
            <div class="row">
                <div class="col-12">
                    <img src="../common_assets/images/5.webp" class="img-fluid d-block">
                </div>
                <div class="col-12 f-24 f-md-36 w500 lh140 mt10 black-clr">
                    15+ High Converting Social Sharing Popup Templates To Get Even More FREE Viral Traffic
                </div>
                <div class="col-12 col-md-8 mx-auto mt20 mt-md40">
                    <img src="assets/images/f5.webp" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 f-18 f-md-20 w400 lh140 mt20 mt-md50">
                    When someone comes to your blog post, <span class="w600">lock them to share it to read the complete post and boost your social media results.</span>
                </div>
            </div>
        </div>
    </div>


    <!--feature6 -->
    <div class="grey-section">
        <div class="container ">
            <div class="row">
                <div class="col-12">
                    <img src="../common_assets/images/6.webp" class="img-fluid d-block">
                </div>
                <div class="col-12 f-24 f-md-36 w500 lh140 mt10 black-clr">
                    Monetize Your Sites with Banner Placements
                </div>
            </div>
            <div class="col-12 col-md-8 mx-auto mt20 mt-md40">
                <img src="assets/images/f6.webp" class="img-fluid d-block mx-auto">
            </div>
            <div class="col-12 f-18 f-md-20 w400 lh140 mt20 mt-md50">
                With your elegant academy sites,now you can open additional monetization channels by <span class="w600">placing banner ads on your e-Learning site home page and blog posts and lure your visitors with the irresistible offers</span> to make
                some extra income from same traffic.
            </div>
        </div>
    </div>

    <!--feature7 -->
    <div class="white-section">
        <div class="container ">
            <div class="row">
                <div class="col-12">
                    <img src="../common_assets/images/7.webp" class="img-fluid d-block">
                </div>
                <div class="col-12 f-24 f-md-36 w500 lh140 mt10 black-clr">
                    20 Premium Promo Popup Templates to get more conversions
                </div>
                <div class="col-12 col-md-8 mx-auto mt20 mt-md40">
                    <img src="assets/images/f7.webp" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 f-18 f-md-20 w400 lh140 mt20 mt-md50">
                    <span class="w600"> Show your best offers or affiliate offers when visitors are most engaged</span> and convert them into paying customers.
                </div>
            </div>
        </div>
    </div>

    <!--feature8-->
    <div class="grey-section">
        <div class="container ">
            <div class="row">
                <div class="col-12">
                    <img src="../common_assets/images/8.webp" class="img-fluid d-block">
                </div>
                <div class="col-12 f-24 f-md-36 w500 lh140 mt10 black-clr">
                    Capture More Leads With 30 EXTRA Carefully Crafted Lead Popup Templates
                </div>
                <div class="col-12 col-md-8 mx-auto mt20 mt-md40">
                    <img src="assets/images/f8.webp" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 f-18 f-md-20 w400 lh140 mt20 mt-md50">
                    With the Authority edition you’re getting 30 MORE lead templates that have been <span class="w600"> carefully crafted to capture maximum leads & get you best results </span> in a cost-effective manner. These lead templates add laurels
                    to your upgrade.
                </div>
            </div>
        </div>
    </div>
    <!-- feature9 -->
    <div class="white-section">
        <div class="container ">
            <div class="row">
                <div class="col-12">
                    <img src="../common_assets/images/9.webp" class="img-fluid d-block">
                </div>
                <div class="col-12 f-24 f-md-36 w500 lh140 mt10 black-clr">
                    Special Effects for Your Popup Ads To Visually Entice Your Visitors To Do Nothing But Clicking And Buying
                </div>
            </div>
            <div class="col-12 col-md-8 mx-auto mt20 mt-md40">
                <img src="assets/images/f9.webp" class="img-fluid d-block mx-auto">
            </div>
            <div class="col-12 f-18 f-md-20 w400 lh140 mt20 mt-md50">
                Visually enticing your visitors is the best way to get your audience hitched to your brand. Keeping this in mind, we are providing you with <span class="w600"> eye-catchy effects that literally entice visitors into clicking and opting in for your offers.</span>
            </div>
        </div>
    </div>

    <!--money back Start -->
    <div class="moneyback-section">
        <div class="container ">
            <div class="row align-items-center">
                <div class="col-12 f-28 f-md-45 lh140 w700 text-center white-clr">
                    30 Days Money Back Guarantee
                </div>
            </div>
            <div class="row align-items-center mt20 mt-md50">
                <div class="col-12 col-md-4 mx-auto">
                    <img src="assets/images/mbg.webp" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-md-8 col-12 mt20 mt-md0 f-18 f-md-20 w400 lh140 white-clr">
                    We have absolutely no doubt that you'll love the extra benefits, training and Aicademy Authority upgraded features. <br><br> You can try it out without any risk. If, for any reason, you’re not satisfied with your Aicademy Authority
                    upgrade you can simply ask for a refund. Your investment is covered by my 30-Days money back guarantee.
                    <br><br> You have absolutely nothing to lose and everything to gain with this fantastic offer and guarantee.
                </div>
            </div>
        </div>
    </div>
    <!--money back End-->

    <!---Bonus Section Starts-->
    <div class="bonus-section">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <div class="but-design mb20 mb-md20 mt0 mt-md0">
                        <div class="f-28 f-md-45 w700 lh140 text-center white-clr skew-normal">
                            But That’s Not All
                        </div>
                    </div>
                </div>

                <div class="col-12 mt20 f-20 f-md-24 w400 text-center lh140">
                    In addition, we have a number of bonuses for those who want to take action today and start profiting from this opportunity
                </div>

                <!--bonus1-->
                <div class="col-12 col-md-12 mt20 mt-md60">
                    <div class="row align-items-center d-flex felx-wrap">
                        <div class="col-12 col-md-6">
                            <div class="f-md-36 f-24 w700 blue-clr text-nowrap">
                                Bonus 1
                            </div>
                            <div class="f-24 f-md-30 w500 lh140 mt20 mt-md20 left-start black-clr">
                                7 Figure Mastery-
                            </div>
                            <div class="f-18 f-md-20 w400 lh140 mt10 mt-md10 left-start">
                                Get the proven and tested necessary strategies for you to set up your online business from scratch, even if you are not familiar with Internet Marketing. When used effectively with <span class="w600">Aicademy Authority Upgrade,</span>                                it gives real results that are scalable as well.

                            </div>
                        </div>
                        <div class="col-12 col-md-6 mt20 mt-md0">
                            <img src="assets/images/bonusbox.webp" class="img-fluid d-block mx-auto">
                        </div>
                    </div>
                </div>


                <!--bonus2-->
                <div class="col-12 col-md-12 mt20 mt-md60">
                    <div class="row align-items-center d-flex felx-wrap">
                        <div class="col-12 col-md-6 order-md-2">
                            <div class="f-md-36 f-24 w700 blue-clr text-nowrap">
                                Bonus 2
                            </div>
                            <div class="f-24 f-md-30 w500 lh140 mt20 mt-md20 left-start black-clr">
                                Social Media Automation-
                            </div>
                            <div class="f-18 f-md-20 w400 lh140 mt10 mt-md10 left-start">
                                Proven and tested techniques that make it fast & easy to automate your social media campaigns & drive tons of laser targeted traffic to your offers without costing a fortune.<br><br> When used with the cool social campaign
                                automation techniques of <span class="w600"> Aicademy Authority,</span> the results achieved are worth watching.

                            </div>
                        </div>
                        <div class="col-12 col-md-6 order-md-1 mt20 mt-md0">
                            <img src="assets/images/bonusbox.webp" class="img-fluid d-block mx-auto">
                        </div>
                    </div>
                </div>
            </div>



            <!--bonus3-->
            <div class="col-12 col-md-12 mt20 mt-md60">
                <div class="row align-items-center d-flex felx-wrap">
                    <div class="col-12 col-md-6">
                        <div class="f-md-36 f-24 w700 blue-clr text-nowrap">
                            Bonus 3
                        </div>
                        <div class="f-24 f-md-30 w500 lh140 mt20 mt-md20 left-start black-clr">
                            Unbreakable Links-
                        </div>
                        <div class="f-18 f-md-20 w400 lh140 mt10 mt-md10 left-start">
                            Building backlinks is the most effective way of notifying search engines that you have relevant content to share that people are actively looking for. Now what are you waiting for. <br><br> Now combine it with
                            <span class="w600">Aicademy Authority</span> to from High PR sites like Tumblr, Bloggr & other social media giants to help you index your content faster

                        </div>
                    </div>
                    <div class="col-12 col-md-6 mt20 mt-md0">
                        <img src="assets/images/bonusbox.webp" class="img-fluid d-block mx-auto">
                    </div>
                </div>
            </div>


            <!--bonus4-->
            <div class="col-12 col-md-12 mt20 mt-md60">
                <div class="row align-items-center d-flex felx-wrap">
                    <div class="col-12 col-md-6 order-md-2">
                        <div class="f-md-36 f-24 w700 blue-clr text-nowrap">
                            Bonus 4
                        </div>
                        <div class="f-24 f-md-30 w500 lh140 mt20 mt-md20 text-start black-clr">
                            Entrepreneurial Drive Video Training-
                        </div>
                        <div class="f-18 f-md-20 w400 lh140 mt10 mt-md10 left-start">
                            Entrepreneurship is the art of starting something on your own and converting your dreams into reality. But doing that isn’t that easy for everyone.<br><br> Have a look at this useful video step-by-step video course that has
                            proven techniques on how to become successful by learning from the top echelons of the society.<br><br>
                            <span class="w600"> This bonus when combined with Aicademy Authority</span> becomes an ultimate growth booster for business owners.

                        </div>
                    </div>
                    <div class="col-12 col-md-6 order-md-1 mt20 mt-md0">
                        <img src="assets/images/bonusbox.webp" class="img-fluid d-block mx-auto">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Bonus Section end -->

    <!--10. Table Section Starts -->
    <div class="white-section padding10" id="buynow">
        <div class="container">
            <div class="row">
                <div class="col-12 f-28 f-md-45 w500 lh140 text-center black-clr">
                    Today You Can Get Unrestricted Access To <span class="w700">Aicademy For LESS Than The Price Of Just One Month’s Membership.</span>
                </div>
            </div>
            <div class="row mt50 mt-md-50">
                <div class="col-md-6 col-12 ">
                    <div class="tablebox2">
                        <div class="tbbg2 text-center">
                            <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 644.49 120" style="max-height:55px">
                                <defs>
                                  <style>
                                    .cls-1s {
                                      fill: #051e3a;
                                    }
                              
                                    .cls-2s {
                                      fill: #0af;
                                    }
                                  </style>
                                </defs>
                                <g id="Layer_1-2" data-name="Layer 1">
                                  <g>
                                    <g>
                                      <circle class="cls-2s" cx="79.02" cy="43.5" r="7.25"/>
                                      <circle class="cls-2s" cx="70.47" cy="78.46" r="5.46"/>
                                      <path class="cls-2" d="m158.03,45.28l-33.74,13.31v36.4l-45.27,16.86-27.9-10.4c-4.17,3.37-7.04,6.74-8.15,10.11-1.01,3.22-.07,6.21,2.46,8.42-5.44-1.46-8-5.25-6.96-9.76,1.24-3.62,4.79-7.68,10-12.08h.01c3.95-3.31,8.84-6.84,14.46-10.5,2.04,1.67,4.68,2.68,7.53,2.68,6.56,0,11.88-5.32,11.88-11.88s-5.32-11.88-11.88-11.88-11.88,5.32-11.88,11.88c0,1.13.15,2.21.46,3.25-8.01,5.01-14.83,10.36-19.57,15.44l-5.75-2.14v-7.66c-7.84,4.92-11.84,11.06-5.99,16.99-5.17-1.64-7.69-4.49-7.69-8.05,0-4.83,4.67-10.99,13.69-17.26l35.41-23.23c2.71,2.18,6.13,3.48,9.87,3.48,8.7,0,15.76-7.06,15.76-15.76s-7.06-15.76-15.76-15.76-15.76,7.06-15.76,15.76c0,1.59.23,3.12.68,4.57l-30.2,19.53v-9L0,45.28,79.02,0l79.01,45.28Z"/>
                                      <path class="cls-2s" d="m149.38,68.32v-19.62l-1.51.6v19.03c-1.66.37-2.9,1.96-2.9,3.85v11.69h7.31v-11.69c0-1.9-1.24-3.48-2.9-3.85Z"/>
                                    </g>
                                    <g>
                                      <g>
                                        <path class="cls-2s" d="m178.03,98.08l22.25-71.1h19.3l22.25,71.1h-17.01l-4.36-16.9h-21.7l-4.36,16.9h-16.36Zm25.74-35.99l-1.74,6.54h15.16l-1.64-6.54c-.95-3.56-1.91-7.34-2.89-11.34-.98-4-1.95-7.85-2.89-11.56h-.44c-.87,3.78-1.76,7.65-2.67,11.61-.91,3.96-1.87,7.73-2.89,11.29Z"/>
                                        <path class="cls-2s" d="m256.22,36.03c-2.69,0-4.87-.76-6.54-2.29-1.67-1.53-2.51-3.56-2.51-6.11s.83-4.58,2.51-6.11c1.67-1.53,3.85-2.29,6.54-2.29s4.87.76,6.54,2.29c1.67,1.53,2.51,3.56,2.51,6.11s-.84,4.58-2.51,6.11c-1.67,1.53-3.85,2.29-6.54,2.29Zm-7.96,62.05v-54.09h16.03v54.09h-16.03Z"/>
                                      </g>
                                      <g>
                                        <path class="cls-1s" d="m299.89,99.38c-5.02,0-9.54-1.11-13.58-3.33-4.03-2.22-7.23-5.45-9.6-9.71-2.36-4.25-3.54-9.36-3.54-15.32s1.31-11.16,3.93-15.38c2.62-4.22,6.07-7.43,10.36-9.65,4.29-2.22,8.9-3.33,13.85-3.33,3.34,0,6.31.55,8.89,1.64,2.58,1.09,4.89,2.47,6.92,4.14l-7.52,10.36c-2.55-2.11-4.98-3.16-7.31-3.16-3.85,0-6.92,1.38-9.21,4.14-2.29,2.76-3.44,6.51-3.44,11.23s1.15,8.38,3.44,11.18c2.29,2.8,5.18,4.2,8.67,4.2,1.74,0,3.45-.38,5.13-1.15,1.67-.76,3.2-1.69,4.58-2.78l6.33,10.47c-2.69,2.33-5.6,3.98-8.72,4.96-3.13.98-6.18,1.47-9.16,1.47Z"/>
                                        <path class="cls-1s" d="m339.81,99.38c-4.94,0-8.87-1.58-11.78-4.74-2.91-3.16-4.36-7.03-4.36-11.61,0-5.67,2.4-10.1,7.2-13.3,4.8-3.2,12.54-5.34,23.23-6.43-.15-2.4-.86-4.31-2.13-5.73-1.27-1.42-3.4-2.13-6.38-2.13-2.25,0-4.54.44-6.87,1.31-2.33.87-4.8,2.07-7.42,3.6l-5.78-10.58c3.42-2.11,7.07-3.82,10.96-5.13,3.89-1.31,7.94-1.96,12.16-1.96,6.91,0,12.21,2,15.92,6,3.71,4,5.56,10.14,5.56,18.43v30.97h-13.09l-1.09-5.56h-.44c-2.25,2.04-4.67,3.69-7.25,4.96-2.58,1.27-5.4,1.91-8.45,1.91Zm5.45-12.43c1.82,0,3.4-.42,4.74-1.25,1.34-.83,2.71-1.94,4.09-3.33v-9.49c-5.67.73-9.6,1.87-11.78,3.44-2.18,1.56-3.27,3.4-3.27,5.51,0,1.74.56,3.04,1.69,3.87,1.13.84,2.63,1.25,4.53,1.25Z"/>
                                        <path class="cls-1s" d="m402.95,99.38c-6.69,0-12.05-2.53-16.09-7.58-4.03-5.05-6.05-11.98-6.05-20.77,0-5.89,1.07-10.96,3.22-15.21,2.14-4.25,4.94-7.51,8.4-9.76,3.45-2.25,7.07-3.38,10.85-3.38,2.98,0,5.49.51,7.52,1.53,2.04,1.02,3.96,2.4,5.78,4.14l-.66-8.29v-18.43h16.03v76.45h-13.09l-1.09-5.34h-.44c-1.89,1.89-4.11,3.47-6.65,4.74-2.55,1.27-5.13,1.91-7.74,1.91Zm4.14-13.09c1.74,0,3.33-.36,4.74-1.09,1.42-.73,2.78-2,4.09-3.82v-22.14c-1.38-1.31-2.85-2.22-4.42-2.73-1.56-.51-3.07-.76-4.53-.76-2.55,0-4.8,1.22-6.76,3.65-1.96,2.44-2.94,6.23-2.94,11.4s.85,9.21,2.56,11.72c1.71,2.51,4.13,3.76,7.25,3.76Z"/>
                                        <path class="cls-1s" d="m470.34,99.38c-5.16,0-9.81-1.13-13.96-3.38-4.14-2.25-7.42-5.49-9.81-9.71-2.4-4.22-3.6-9.31-3.6-15.27s1.22-10.94,3.65-15.16c2.43-4.22,5.62-7.47,9.54-9.76,3.93-2.29,8.03-3.44,12.32-3.44,5.16,0,9.43,1.15,12.81,3.44,3.38,2.29,5.92,5.38,7.63,9.27,1.71,3.89,2.56,8.31,2.56,13.25,0,1.38-.07,2.74-.22,4.09-.15,1.35-.29,2.34-.44,3h-32.39c.73,3.93,2.36,6.82,4.91,8.67,2.54,1.85,5.6,2.78,9.16,2.78,3.85,0,7.74-1.2,11.67-3.6l5.34,9.71c-2.76,1.89-5.85,3.38-9.27,4.47-3.42,1.09-6.73,1.64-9.92,1.64Zm-12-34.24h19.52c0-2.98-.71-5.43-2.13-7.36-1.42-1.93-3.73-2.89-6.92-2.89-2.47,0-4.69.86-6.65,2.56-1.96,1.71-3.24,4.27-3.82,7.69Z"/>
                                        <path class="cls-1s" d="m502.62,98.08v-54.09h13.09l1.09,6.98h.44c2.25-2.25,4.65-4.2,7.2-5.83,2.54-1.64,5.6-2.45,9.16-2.45,3.85,0,6.96.78,9.32,2.34,2.36,1.56,4.23,3.8,5.62,6.71,2.4-2.47,4.94-4.6,7.63-6.38,2.69-1.78,5.82-2.67,9.38-2.67,5.82,0,10.09,1.95,12.81,5.83,2.73,3.89,4.09,9.21,4.09,15.98v33.59h-16.03v-31.52c0-3.93-.53-6.61-1.58-8.07-1.05-1.45-2.74-2.18-5.07-2.18-2.69,0-5.78,1.74-9.27,5.23v36.53h-16.03v-31.52c0-3.93-.53-6.61-1.58-8.07-1.05-1.45-2.74-2.18-5.07-2.18-2.69,0-5.74,1.74-9.16,5.23v36.53h-16.03Z"/>
                                        <path class="cls-1s" d="m602.07,119.23c-1.6,0-3-.11-4.2-.33-1.2-.22-2.34-.47-3.44-.76l2.84-12.21c.51.07,1.09.2,1.74.38.65.18,1.27.27,1.85.27,2.69,0,4.76-.65,6.22-1.96,1.45-1.31,2.54-3.02,3.27-5.13l.76-2.84-20.83-52.67h16.14l7.74,23.23c.8,2.47,1.53,4.98,2.18,7.52.65,2.55,1.34,5.16,2.07,7.85h.44c.58-2.54,1.18-5.11,1.8-7.69.62-2.58,1.25-5.14,1.91-7.69l6.54-23.23h15.38l-18.76,54.63c-1.67,4.51-3.53,8.31-5.56,11.4-2.04,3.09-4.49,5.4-7.36,6.92-2.87,1.53-6.45,2.29-10.74,2.29Z"/>
                                      </g>
                                    </g>
                                  </g>
                                </g>
                              </svg>
                            <div class="text-uppercase mt15 mt-md15 w600 f-md-32 f-22 text-center black-clr lh120">
                            Authority   Personal
                            </div>
                        </div>
                        <ul class="f-16 f-md-16 w400 text-center lh130 vgreytick mb0">
                            <li>
                                <span class="w500">Empower Your Website for More User Interactions, Leads & FREE SEO Traffic</span>
                            </li>

                            <li>
                                <span class="w500">Setup Automated Social Campaigns to Get More Exposure & Social Traffic Hands Free</span>
                            </li>
                            <li>
                                <span class="w500">Get High PR Backlinks For Faster Indexing And Targeted Traffic</span>
                            </li>
                            <li>
                                <span class="w500">Generate Viral Traffic by Enabling Social Sharing on Your Marketplace & Blog</span>
                            </li>
                            <li>
                                <span class="w500">15+ High Converting Social Sharing Popup Templates To Get Even More FREE Viral Traffic</span>
                            </li>
                            <li>
                                <span class="w500">Monetize Your Sites with Banner Placements</span>
                            </li>
                            <li>
                                <span class="w500">20 Premium Promo Popup Templates to get more conversions</span>
                            </li>
                            <li>
                                <span class="w500">Capture More Leads With 30 EXTRA carefully crafted Lead Popup Templates</span>
                            </li>
                            <li>
                                <span class="w500">Special Effects for Your Popup Ads To Visually Entice Your Visitors To Do Nothing But Clicking And Buying</span>
                            </li>
                            <br>
                        </ul>
                        <div class="myfeatureslast f-md-25 f-16 w400 text-center lh160 hideme">
                            <div class="f-md-37 f-27 w600 buypad text-center"> Only $47 </div>
                            <div>
                                <a href="https://warriorplus.com/o2/buy/hb06pf/xrn22w/s6fr47"><img src="https://warriorplus.com/o2/btn/fn200011000/hb06pf/xrn22w/352573" border="0" class="img-fluid d-block mx-auto"></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-12 mt-md-0 mt-5">
                    <div class="tablebox3">
                        <div class="tbbg3 text-center">
                            <div class="relative">
                                <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 644.49 120" style="max-height:55px">
                                    <defs>
                                      <style>
                                        .cls-1 {
                                          fill: #fff;
                                        }
                                  
                                        .cls-2 {
                                          fill: #0af;
                                        }
                                      </style>
                                    </defs>
                                    <g id="Layer_1-2" data-name="Layer 1">
                                      <g>
                                        <g>
                                          <circle class="cls-2" cx="79.02" cy="43.5" r="7.25"/>
                                          <circle class="cls-2" cx="70.47" cy="78.46" r="5.46"/>
                                          <path class="cls-2" d="m158.03,45.28l-33.74,13.31v36.4l-45.27,16.86-27.9-10.4c-4.17,3.37-7.04,6.74-8.15,10.11-1.01,3.22-.07,6.21,2.46,8.42-5.44-1.46-8-5.25-6.96-9.76,1.24-3.62,4.79-7.68,10-12.08h.01c3.95-3.31,8.84-6.84,14.46-10.5,2.04,1.67,4.68,2.68,7.53,2.68,6.56,0,11.88-5.32,11.88-11.88s-5.32-11.88-11.88-11.88-11.88,5.32-11.88,11.88c0,1.13.15,2.21.46,3.25-8.01,5.01-14.83,10.36-19.57,15.44l-5.75-2.14v-7.66c-7.84,4.92-11.84,11.06-5.99,16.99-5.17-1.64-7.69-4.49-7.69-8.05,0-4.83,4.67-10.99,13.69-17.26l35.41-23.23c2.71,2.18,6.13,3.48,9.87,3.48,8.7,0,15.76-7.06,15.76-15.76s-7.06-15.76-15.76-15.76-15.76,7.06-15.76,15.76c0,1.59.23,3.12.68,4.57l-30.2,19.53v-9L0,45.28,79.02,0l79.01,45.28Z"/>
                                          <path class="cls-2" d="m149.38,68.32v-19.62l-1.51.6v19.03c-1.66.37-2.9,1.96-2.9,3.85v11.69h7.31v-11.69c0-1.9-1.24-3.48-2.9-3.85Z"/>
                                        </g>
                                        <g>
                                          <g>
                                            <path class="cls-2" d="m178.03,98.08l22.25-71.1h19.3l22.25,71.1h-17.01l-4.36-16.9h-21.7l-4.36,16.9h-16.36Zm25.74-35.99l-1.74,6.54h15.16l-1.64-6.54c-.95-3.56-1.91-7.34-2.89-11.34-.98-4-1.95-7.85-2.89-11.56h-.44c-.87,3.78-1.76,7.65-2.67,11.61-.91,3.96-1.87,7.73-2.89,11.29Z"/>
                                            <path class="cls-2" d="m256.22,36.03c-2.69,0-4.87-.76-6.54-2.29-1.67-1.53-2.51-3.56-2.51-6.11s.83-4.58,2.51-6.11c1.67-1.53,3.85-2.29,6.54-2.29s4.87.76,6.54,2.29c1.67,1.53,2.51,3.56,2.51,6.11s-.84,4.58-2.51,6.11c-1.67,1.53-3.85,2.29-6.54,2.29Zm-7.96,62.05v-54.09h16.03v54.09h-16.03Z"/>
                                          </g>
                                          <g>
                                            <path class="cls-1" d="m299.89,99.38c-5.02,0-9.54-1.11-13.58-3.33-4.03-2.22-7.23-5.45-9.6-9.71-2.36-4.25-3.54-9.36-3.54-15.32s1.31-11.16,3.93-15.38c2.62-4.22,6.07-7.43,10.36-9.65,4.29-2.22,8.9-3.33,13.85-3.33,3.34,0,6.31.55,8.89,1.64,2.58,1.09,4.89,2.47,6.92,4.14l-7.52,10.36c-2.55-2.11-4.98-3.16-7.31-3.16-3.85,0-6.92,1.38-9.21,4.14-2.29,2.76-3.44,6.51-3.44,11.23s1.15,8.38,3.44,11.18c2.29,2.8,5.18,4.2,8.67,4.2,1.74,0,3.45-.38,5.13-1.15,1.67-.76,3.2-1.69,4.58-2.78l6.33,10.47c-2.69,2.33-5.6,3.98-8.72,4.96-3.13.98-6.18,1.47-9.16,1.47Z"/>
                                            <path class="cls-1" d="m339.81,99.38c-4.94,0-8.87-1.58-11.78-4.74-2.91-3.16-4.36-7.03-4.36-11.61,0-5.67,2.4-10.1,7.2-13.3,4.8-3.2,12.54-5.34,23.23-6.43-.15-2.4-.86-4.31-2.13-5.73-1.27-1.42-3.4-2.13-6.38-2.13-2.25,0-4.54.44-6.87,1.31-2.33.87-4.8,2.07-7.42,3.6l-5.78-10.58c3.42-2.11,7.07-3.82,10.96-5.13,3.89-1.31,7.94-1.96,12.16-1.96,6.91,0,12.21,2,15.92,6,3.71,4,5.56,10.14,5.56,18.43v30.97h-13.09l-1.09-5.56h-.44c-2.25,2.04-4.67,3.69-7.25,4.96-2.58,1.27-5.4,1.91-8.45,1.91Zm5.45-12.43c1.82,0,3.4-.42,4.74-1.25,1.34-.83,2.71-1.94,4.09-3.33v-9.49c-5.67.73-9.6,1.87-11.78,3.44-2.18,1.56-3.27,3.4-3.27,5.51,0,1.74.56,3.04,1.69,3.87,1.13.84,2.63,1.25,4.53,1.25Z"/>
                                            <path class="cls-1" d="m402.95,99.38c-6.69,0-12.05-2.53-16.09-7.58-4.03-5.05-6.05-11.98-6.05-20.77,0-5.89,1.07-10.96,3.22-15.21,2.14-4.25,4.94-7.51,8.4-9.76,3.45-2.25,7.07-3.38,10.85-3.38,2.98,0,5.49.51,7.52,1.53,2.04,1.02,3.96,2.4,5.78,4.14l-.66-8.29v-18.43h16.03v76.45h-13.09l-1.09-5.34h-.44c-1.89,1.89-4.11,3.47-6.65,4.74-2.55,1.27-5.13,1.91-7.74,1.91Zm4.14-13.09c1.74,0,3.33-.36,4.74-1.09,1.42-.73,2.78-2,4.09-3.82v-22.14c-1.38-1.31-2.85-2.22-4.42-2.73-1.56-.51-3.07-.76-4.53-.76-2.55,0-4.8,1.22-6.76,3.65-1.96,2.44-2.94,6.23-2.94,11.4s.85,9.21,2.56,11.72c1.71,2.51,4.13,3.76,7.25,3.76Z"/>
                                            <path class="cls-1" d="m470.34,99.38c-5.16,0-9.81-1.13-13.96-3.38-4.14-2.25-7.42-5.49-9.81-9.71-2.4-4.22-3.6-9.31-3.6-15.27s1.22-10.94,3.65-15.16c2.43-4.22,5.62-7.47,9.54-9.76,3.93-2.29,8.03-3.44,12.32-3.44,5.16,0,9.43,1.15,12.81,3.44,3.38,2.29,5.92,5.38,7.63,9.27,1.71,3.89,2.56,8.31,2.56,13.25,0,1.38-.07,2.74-.22,4.09-.15,1.35-.29,2.34-.44,3h-32.39c.73,3.93,2.36,6.82,4.91,8.67,2.54,1.85,5.6,2.78,9.16,2.78,3.85,0,7.74-1.2,11.67-3.6l5.34,9.71c-2.76,1.89-5.85,3.38-9.27,4.47-3.42,1.09-6.73,1.64-9.92,1.64Zm-12-34.24h19.52c0-2.98-.71-5.43-2.13-7.36-1.42-1.93-3.73-2.89-6.92-2.89-2.47,0-4.69.86-6.65,2.56-1.96,1.71-3.24,4.27-3.82,7.69Z"/>
                                            <path class="cls-1" d="m502.62,98.08v-54.09h13.09l1.09,6.98h.44c2.25-2.25,4.65-4.2,7.2-5.83,2.54-1.64,5.6-2.45,9.16-2.45,3.85,0,6.96.78,9.32,2.34,2.36,1.56,4.23,3.8,5.62,6.71,2.4-2.47,4.94-4.6,7.63-6.38,2.69-1.78,5.82-2.67,9.38-2.67,5.82,0,10.09,1.95,12.81,5.83,2.73,3.89,4.09,9.21,4.09,15.98v33.59h-16.03v-31.52c0-3.93-.53-6.61-1.58-8.07-1.05-1.45-2.74-2.18-5.07-2.18-2.69,0-5.78,1.74-9.27,5.23v36.53h-16.03v-31.52c0-3.93-.53-6.61-1.58-8.07-1.05-1.45-2.74-2.18-5.07-2.18-2.69,0-5.74,1.74-9.16,5.23v36.53h-16.03Z"/>
                                            <path class="cls-1" d="m602.07,119.23c-1.6,0-3-.11-4.2-.33-1.2-.22-2.34-.47-3.44-.76l2.84-12.21c.51.07,1.09.2,1.74.38.65.18,1.27.27,1.85.27,2.69,0,4.76-.65,6.22-1.96,1.45-1.31,2.54-3.02,3.27-5.13l.76-2.84-20.83-52.67h16.14l7.74,23.23c.8,2.47,1.53,4.98,2.18,7.52.65,2.55,1.34,5.16,2.07,7.85h.44c.58-2.54,1.18-5.11,1.8-7.69.62-2.58,1.25-5.14,1.91-7.69l6.54-23.23h15.38l-18.76,54.63c-1.67,4.51-3.53,8.31-5.56,11.4-2.04,3.09-4.49,5.4-7.36,6.92-2.87,1.53-6.45,2.29-10.74,2.29Z"/>
                                          </g>
                                        </g>
                                      </g>
                                    </g>
                                  </svg>
                            </div>
                            <div class="text-uppercase mt15 mt-md15 w600 f-md-32 f-22 text-center white-clr lh120">Authority Commercial</div>
                        </div>
                        <ul class="f-16 w400 text-center lh130 grey-tick-last mb0 white-clr">
                            <li>
                                <span class="w500">Empower Your Website for More User Interactions, Leads & FREE SEO Traffic</span>
                            </li>
                            <li>
                                <span class="w500">Setup Automated Social Campaigns to Get More Exposure & Social Traffic Hands Free</span>
                            </li>
                            <li>
                                <span class="w500">Get High PR Backlinks For Faster Indexing And Targeted Traffic</span>
                            </li>
                            <li>
                                <span class="w500">Generate Viral Traffic by Enabling Social Sharing on Your Marketplace & Blog</span>
                            </li>
                            <li>
                                <span class="w500">15+ High Converting Social Sharing Popup Templates To Get Even More FREE Viral Traffic</span>
                            </li>
                            <li>
                                <span class="w500">Monetize Your Sites with Banner Placements</span>
                            </li>
                            <li>
                                <span class="w500">20 Premium Promo Popup Templates to get more conversions</span>
                            </li>
                            <li>
                                <span class="w500">Capture More Leads With 30 EXTRA carefully crafted Lead Popup Templates</span>
                            </li>
                            <li>
                                <span class="w500">Special Effects for Your Popup Ads To Visually Entice Your Visitors To Do Nothing But Clicking And Buying</span>
                            </li>
                            <li>
                                <span class="w600">Commercial License - Start providing services to clients and charge hundreds of dollars every month</span>
                            </li>

                        </ul>
                        <div class="myfeatureslast-com f-md-25 f-16 w400 text-center lh160 hideme">
                            <div class="f-md-37 f-27 w600 buypad text-center black-clr"> Only $49</div>
                            <div>
                                <a href="https://warriorplus.com/o2/buy/hb06pf/xrn22w/qg2jnh"><img src="https://warriorplus.com/o2/btn/fn200011000/hb06pf/xrn22w/352965" border="0" class="img-fluid d-block mx-auto"></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12  mt25 mt-md40 text-center">
                    <a class="kapblue f-md-22 f-18 lh140 w400" href="https://warriorplus.com/o/nothanks/xrn22w" target="_blank" class="link">
                        No thanks – I don’t want to drive more traffic & sales to 100x my profits without any no extra efforts. I know that Aicademy Authority Edition can increase my profits & I duly realize that I won't get this offer again. Please take me to the next step to get access to my purchase
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!--10. Table Section End -->

    <!--Footer Section Start -->
    <div class="footer-section">
        <div class="container ">
            <div class="row">
                <div class="col-12 text-center">
                    <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 644.49 120" style="max-height:55px">
                        <defs>
                          <style>
                            .cls-1 {
                              fill: #fff;
                            }
                      
                            .cls-2 {
                              fill: #0af;
                            }
                          </style>
                        </defs>
                        <g id="Layer_1-2" data-name="Layer 1">
                          <g>
                            <g>
                              <circle class="cls-2" cx="79.02" cy="43.5" r="7.25"/>
                              <circle class="cls-2" cx="70.47" cy="78.46" r="5.46"/>
                              <path class="cls-2" d="m158.03,45.28l-33.74,13.31v36.4l-45.27,16.86-27.9-10.4c-4.17,3.37-7.04,6.74-8.15,10.11-1.01,3.22-.07,6.21,2.46,8.42-5.44-1.46-8-5.25-6.96-9.76,1.24-3.62,4.79-7.68,10-12.08h.01c3.95-3.31,8.84-6.84,14.46-10.5,2.04,1.67,4.68,2.68,7.53,2.68,6.56,0,11.88-5.32,11.88-11.88s-5.32-11.88-11.88-11.88-11.88,5.32-11.88,11.88c0,1.13.15,2.21.46,3.25-8.01,5.01-14.83,10.36-19.57,15.44l-5.75-2.14v-7.66c-7.84,4.92-11.84,11.06-5.99,16.99-5.17-1.64-7.69-4.49-7.69-8.05,0-4.83,4.67-10.99,13.69-17.26l35.41-23.23c2.71,2.18,6.13,3.48,9.87,3.48,8.7,0,15.76-7.06,15.76-15.76s-7.06-15.76-15.76-15.76-15.76,7.06-15.76,15.76c0,1.59.23,3.12.68,4.57l-30.2,19.53v-9L0,45.28,79.02,0l79.01,45.28Z"/>
                              <path class="cls-2" d="m149.38,68.32v-19.62l-1.51.6v19.03c-1.66.37-2.9,1.96-2.9,3.85v11.69h7.31v-11.69c0-1.9-1.24-3.48-2.9-3.85Z"/>
                            </g>
                            <g>
                              <g>
                                <path class="cls-2" d="m178.03,98.08l22.25-71.1h19.3l22.25,71.1h-17.01l-4.36-16.9h-21.7l-4.36,16.9h-16.36Zm25.74-35.99l-1.74,6.54h15.16l-1.64-6.54c-.95-3.56-1.91-7.34-2.89-11.34-.98-4-1.95-7.85-2.89-11.56h-.44c-.87,3.78-1.76,7.65-2.67,11.61-.91,3.96-1.87,7.73-2.89,11.29Z"/>
                                <path class="cls-2" d="m256.22,36.03c-2.69,0-4.87-.76-6.54-2.29-1.67-1.53-2.51-3.56-2.51-6.11s.83-4.58,2.51-6.11c1.67-1.53,3.85-2.29,6.54-2.29s4.87.76,6.54,2.29c1.67,1.53,2.51,3.56,2.51,6.11s-.84,4.58-2.51,6.11c-1.67,1.53-3.85,2.29-6.54,2.29Zm-7.96,62.05v-54.09h16.03v54.09h-16.03Z"/>
                              </g>
                              <g>
                                <path class="cls-1" d="m299.89,99.38c-5.02,0-9.54-1.11-13.58-3.33-4.03-2.22-7.23-5.45-9.6-9.71-2.36-4.25-3.54-9.36-3.54-15.32s1.31-11.16,3.93-15.38c2.62-4.22,6.07-7.43,10.36-9.65,4.29-2.22,8.9-3.33,13.85-3.33,3.34,0,6.31.55,8.89,1.64,2.58,1.09,4.89,2.47,6.92,4.14l-7.52,10.36c-2.55-2.11-4.98-3.16-7.31-3.16-3.85,0-6.92,1.38-9.21,4.14-2.29,2.76-3.44,6.51-3.44,11.23s1.15,8.38,3.44,11.18c2.29,2.8,5.18,4.2,8.67,4.2,1.74,0,3.45-.38,5.13-1.15,1.67-.76,3.2-1.69,4.58-2.78l6.33,10.47c-2.69,2.33-5.6,3.98-8.72,4.96-3.13.98-6.18,1.47-9.16,1.47Z"/>
                                <path class="cls-1" d="m339.81,99.38c-4.94,0-8.87-1.58-11.78-4.74-2.91-3.16-4.36-7.03-4.36-11.61,0-5.67,2.4-10.1,7.2-13.3,4.8-3.2,12.54-5.34,23.23-6.43-.15-2.4-.86-4.31-2.13-5.73-1.27-1.42-3.4-2.13-6.38-2.13-2.25,0-4.54.44-6.87,1.31-2.33.87-4.8,2.07-7.42,3.6l-5.78-10.58c3.42-2.11,7.07-3.82,10.96-5.13,3.89-1.31,7.94-1.96,12.16-1.96,6.91,0,12.21,2,15.92,6,3.71,4,5.56,10.14,5.56,18.43v30.97h-13.09l-1.09-5.56h-.44c-2.25,2.04-4.67,3.69-7.25,4.96-2.58,1.27-5.4,1.91-8.45,1.91Zm5.45-12.43c1.82,0,3.4-.42,4.74-1.25,1.34-.83,2.71-1.94,4.09-3.33v-9.49c-5.67.73-9.6,1.87-11.78,3.44-2.18,1.56-3.27,3.4-3.27,5.51,0,1.74.56,3.04,1.69,3.87,1.13.84,2.63,1.25,4.53,1.25Z"/>
                                <path class="cls-1" d="m402.95,99.38c-6.69,0-12.05-2.53-16.09-7.58-4.03-5.05-6.05-11.98-6.05-20.77,0-5.89,1.07-10.96,3.22-15.21,2.14-4.25,4.94-7.51,8.4-9.76,3.45-2.25,7.07-3.38,10.85-3.38,2.98,0,5.49.51,7.52,1.53,2.04,1.02,3.96,2.4,5.78,4.14l-.66-8.29v-18.43h16.03v76.45h-13.09l-1.09-5.34h-.44c-1.89,1.89-4.11,3.47-6.65,4.74-2.55,1.27-5.13,1.91-7.74,1.91Zm4.14-13.09c1.74,0,3.33-.36,4.74-1.09,1.42-.73,2.78-2,4.09-3.82v-22.14c-1.38-1.31-2.85-2.22-4.42-2.73-1.56-.51-3.07-.76-4.53-.76-2.55,0-4.8,1.22-6.76,3.65-1.96,2.44-2.94,6.23-2.94,11.4s.85,9.21,2.56,11.72c1.71,2.51,4.13,3.76,7.25,3.76Z"/>
                                <path class="cls-1" d="m470.34,99.38c-5.16,0-9.81-1.13-13.96-3.38-4.14-2.25-7.42-5.49-9.81-9.71-2.4-4.22-3.6-9.31-3.6-15.27s1.22-10.94,3.65-15.16c2.43-4.22,5.62-7.47,9.54-9.76,3.93-2.29,8.03-3.44,12.32-3.44,5.16,0,9.43,1.15,12.81,3.44,3.38,2.29,5.92,5.38,7.63,9.27,1.71,3.89,2.56,8.31,2.56,13.25,0,1.38-.07,2.74-.22,4.09-.15,1.35-.29,2.34-.44,3h-32.39c.73,3.93,2.36,6.82,4.91,8.67,2.54,1.85,5.6,2.78,9.16,2.78,3.85,0,7.74-1.2,11.67-3.6l5.34,9.71c-2.76,1.89-5.85,3.38-9.27,4.47-3.42,1.09-6.73,1.64-9.92,1.64Zm-12-34.24h19.52c0-2.98-.71-5.43-2.13-7.36-1.42-1.93-3.73-2.89-6.92-2.89-2.47,0-4.69.86-6.65,2.56-1.96,1.71-3.24,4.27-3.82,7.69Z"/>
                                <path class="cls-1" d="m502.62,98.08v-54.09h13.09l1.09,6.98h.44c2.25-2.25,4.65-4.2,7.2-5.83,2.54-1.64,5.6-2.45,9.16-2.45,3.85,0,6.96.78,9.32,2.34,2.36,1.56,4.23,3.8,5.62,6.71,2.4-2.47,4.94-4.6,7.63-6.38,2.69-1.78,5.82-2.67,9.38-2.67,5.82,0,10.09,1.95,12.81,5.83,2.73,3.89,4.09,9.21,4.09,15.98v33.59h-16.03v-31.52c0-3.93-.53-6.61-1.58-8.07-1.05-1.45-2.74-2.18-5.07-2.18-2.69,0-5.78,1.74-9.27,5.23v36.53h-16.03v-31.52c0-3.93-.53-6.61-1.58-8.07-1.05-1.45-2.74-2.18-5.07-2.18-2.69,0-5.74,1.74-9.16,5.23v36.53h-16.03Z"/>
                                <path class="cls-1" d="m602.07,119.23c-1.6,0-3-.11-4.2-.33-1.2-.22-2.34-.47-3.44-.76l2.84-12.21c.51.07,1.09.2,1.74.38.65.18,1.27.27,1.85.27,2.69,0,4.76-.65,6.22-1.96,1.45-1.31,2.54-3.02,3.27-5.13l.76-2.84-20.83-52.67h16.14l7.74,23.23c.8,2.47,1.53,4.98,2.18,7.52.65,2.55,1.34,5.16,2.07,7.85h.44c.58-2.54,1.18-5.11,1.8-7.69.62-2.58,1.25-5.14,1.91-7.69l6.54-23.23h15.38l-18.76,54.63c-1.67,4.51-3.53,8.31-5.56,11.4-2.04,3.09-4.49,5.4-7.36,6.92-2.87,1.53-6.45,2.29-10.74,2.29Z"/>
                              </g>
                            </g>
                          </g>
                        </g>
                      </svg>
                    <div class="f-14 f-md-16 w400 mt20 lh160 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
                    <div class=" mt20 mt-md40 white-clr"> <script type="text/javascript" src="https://warriorplus.com/o2/disclaimer/hb06pf" defer=""></script><div class="wplus_spdisclaimer f-17 w300 mt20 lh140 white-clr text-center"><strong>Disclaimer:</strong> <em>WarriorPlus is used to help manage the sale of products on this site. While WarriorPlus helps facilitate the sale, all payments are made directly to the product vendor and NOT WarriorPlus. Thus, all product questions, support inquiries and/or refund requests must be sent to the vendor. WarriorPlus's role should not be construed as an endorsement, approval or review of these products or any claim, statement or opinion used in the marketing of these products.</em></div></div>
                </div>
                <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40 align-items-center">
                    <div class="f-14 f-md-16 w400 lh160 white-clr text-xs-center">Copyright © Aicademy </div>
                    <ul class="footer-ul w400 f-14 f-md-16 white-clr text-center text-md-right">
                        <li><a href="https://support.bizomart.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                        <li><a href="http://aicademy.live/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                        <li><a href="http://aicademy.live/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                        <li><a href="http://aicademy.live/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                        <li><a href="http://aicademy.live/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                        <li><a href="http://aicademy.live/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                        <li><a href="http://aicademy.live/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!--Footer Section End -->
     <!-- Exit Popup and Timer -->
   <?php include('timer.php'); ?>
   <!-- Exit Popup and Timer End -->
</body>
</html>
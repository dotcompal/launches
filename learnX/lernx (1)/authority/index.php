<!Doctype html>
<html>

<head>
    <title>LearnX Authority</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=9">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">

    <meta name="title" content="LearnX | Authority">
    <meta name="description" content="LearnX">
    <meta name="keywords" content="LearnX">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta property="og:image" content="https://www.getlearnx.com/authority/thumbnail.png">
    <meta name="language" content="English">
    <meta name="revisit-after" content="1 days">
    <meta name="author" content="Pranshu Gupta">

    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:title" content="LearnX | Authority">
    <meta property="og:description" content="LearnX">
    <meta property="og:image" content="https://www.getlearnx.com/authority/thumbnail.png">

    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:title" content="LearnX | Authority">
    <meta property="twitter:description" content="LearnX">
    <meta property="twitter:image" content="https://www.getlearnx.com/authority/thumbnail.png">

    <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
    <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
    <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&family=Poppins:wght@300;400;500;600;700;800;900&family=Red+Hat+Display:wght@300;400;500;600;700;800;900&display=swap" rel="stylesheet">
    <!-- Start Editor required -->
    <!-- Start Editor required -->
    <link rel="stylesheet" href="https://cdn.oppyotest.com/launches/sellero/common_assets/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="assets/css/style.css" type="text/css">
    <link rel="stylesheet" href="assets/css/timer.css" type="text/css">
    <script src="https://cdn.oppyotest.com/launches/sellero/common_assets/js/bootstrap.min.js"></script>
    <script src="https://cdn.oppyotest.com/launches/sellero/common_assets/js/jquery.min.js"></script>
    <!-- End -->
</head>

<body>
<div class="warning-section">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <div class="offer">
                        <div>
                            <img src="https://cdn.oppyo.com/launches/appzilo/special/error.webp">
                        </div>
                        <div class="f-22 f-md-30 w500 lh140 white-clr text-center">
                            <span class="w600">HOLD ON:</span>  Your Order Is Not Complete Yet...
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--1. Header Section Start -->
    <div class="header-section">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 484.58 200.84" style="max-height:75px;">
                        <defs>
                            <style>
                                .cls-1,.cls-4{fill:#fff;}
                                .cls-2,.cls-3,.cls-4{fill-rule:evenodd;}
                                .cls-2{fill:url(#linear-gradient);}
                                .cls-3{fill:url(#linear-gradient-2);}
                                .cls-5{fill:url(#linear-gradient-3);}
                            </style>
                            <linearGradient id="linear-gradient" x1="-154.67" y1="191.86" x2="249.18" y2="191.86" gradientTransform="matrix(1.19, -0.01, 0.05, 1, 178.46, -4.31)" gradientUnits="userSpaceOnUse">
                                <stop offset="0" stop-color="#ead40c"></stop>
                                <stop offset="1" stop-color="#ff00e9"></stop>
                            </linearGradient>
                            <linearGradient id="linear-gradient-2" x1="23.11" y1="160.56" x2="57.61" y2="160.56" gradientTransform="matrix(1, 0, 0, 1, 0, 0)" xlink:href="#linear-gradient"></linearGradient>
                            <linearGradient id="linear-gradient-3" x1="384.49" y1="98.33" x2="483.38" y2="98.33" gradientTransform="matrix(1, 0, 0, 1, 0, 0)" xlink:href="#linear-gradient"></linearGradient>
                        </defs>
                        <g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1">
                            <path class="cls-1" d="M112.31,154.3q-11.65,0-20.1-4.87a32.56,32.56,0,0,1-13-13.82q-4.53-9-4.54-21.11a46.06,46.06,0,0,1,4.57-21A34.29,34.29,0,0,1,92,79.37a36.11,36.11,0,0,1,19.31-5.06,39.65,39.65,0,0,1,13.54,2.29,31,31,0,0,1,11.3,7.09,33.32,33.32,0,0,1,7.75,12.18,49.23,49.23,0,0,1,2.82,17.57V119H83.26v-12.3h46A19.87,19.87,0,0,0,127,97.38a16.6,16.6,0,0,0-6.18-6.48,17.59,17.59,0,0,0-9.21-2.37,17.88,17.88,0,0,0-9.83,2.7,18.81,18.81,0,0,0-6.58,7.06,20.17,20.17,0,0,0-2.4,9.56v10.74a25.15,25.15,0,0,0,2.47,11.57,17.42,17.42,0,0,0,6.91,7.37,20.52,20.52,0,0,0,10.39,2.55,21.62,21.62,0,0,0,7.22-1.14,15.71,15.71,0,0,0,5.59-3.35,14.11,14.11,0,0,0,3.59-5.5L146,132a26.44,26.44,0,0,1-6.13,11.77,29.72,29.72,0,0,1-11.52,7.77A43.74,43.74,0,0,1,112.31,154.3Z"></path>
                            <path class="cls-1" d="M184.49,154.35a31.78,31.78,0,0,1-13.24-2.65,21.26,21.26,0,0,1-9.28-7.84,22.86,22.86,0,0,1-3.41-12.81A21.92,21.92,0,0,1,161,120.2a18.9,18.9,0,0,1,6.61-6.86,33.85,33.85,0,0,1,9.46-3.9,77.76,77.76,0,0,1,10.92-2q6.81-.7,11-1.28a15.91,15.91,0,0,0,6.18-1.82,4.25,4.25,0,0,0,1.94-3.86v-.3q0-5.7-3.38-8.83T194,88.28q-6.71,0-10.62,2.92a14.55,14.55,0,0,0-5.27,6.91l-17-2.42a27.51,27.51,0,0,1,6.66-11.83,29.46,29.46,0,0,1,11.35-7.16,43.73,43.73,0,0,1,14.83-2.39,47.89,47.89,0,0,1,11.14,1.31,31.6,31.6,0,0,1,10.14,4.31,22.12,22.12,0,0,1,7.39,8.14q2.81,5.15,2.8,12.87v51.85H207.84V142.14h-.61a22.1,22.1,0,0,1-4.66,6,22.35,22.35,0,0,1-7.52,4.49A30,30,0,0,1,184.49,154.35Zm4.74-13.42a19.7,19.7,0,0,0,9.53-2.19,16,16,0,0,0,6.23-5.83,15,15,0,0,0,2.19-7.91v-9.13a8.72,8.72,0,0,1-2.9,1.31,45.56,45.56,0,0,1-4.56,1.06c-1.68.3-3.35.57-5,.8l-4.29.61a32,32,0,0,0-7.32,1.81A12.29,12.29,0,0,0,178,125a9.76,9.76,0,0,0,1.82,13.39A16,16,0,0,0,189.23,140.93Z"></path>
                            <path class="cls-1" d="M243.8,152.79V75.31h17.7V88.23h.81a19.36,19.36,0,0,1,7.29-10.37,20,20,0,0,1,11.83-3.66c1,0,2.14,0,3.4.13a23.83,23.83,0,0,1,3.15.38V91.5a21,21,0,0,0-3.65-.73,38.61,38.61,0,0,0-4.82-.32,18.47,18.47,0,0,0-8.95,2.14,15.94,15.94,0,0,0-6.23,5.93,16.55,16.55,0,0,0-2.27,8.72v45.55Z"></path>
                            <path class="cls-1" d="M318.4,107.39v45.4H300.14V75.31h17.45V88.48h.91a22,22,0,0,1,8.55-10.34q5.86-3.84,14.55-3.83a27.58,27.58,0,0,1,14,3.43,23.19,23.19,0,0,1,9.28,9.93,34.22,34.22,0,0,1,3.26,15.79v49.33H349.87V106.28c0-5.17-1.34-9.23-4-12.15s-6.37-4.39-11.07-4.39a17,17,0,0,0-8.5,2.09,14.67,14.67,0,0,0-5.8,6A20,20,0,0,0,318.4,107.39Z"></path>
                            <path class="cls-2" d="M314.05,172.88c12.69-.14,27.32,2.53,40.82,2.61l-.09,1c1.07.15,1-.88,2-.78,1.42-.18,0,1.57,1.82,1.14.54-.06.08-.56-.32-.68.77,0,2.31.17,3,1.26,1.3,0-.9-1.34.85-.89,6,1.63,13.39,0,20,3.2,11.64.72,24.79,2.38,38,4.27,2.85.41,5.37,1.11,7.57,1.05,2-.05,5.68.78,8,1.08,2.53.34,5.16,1.56,7.13,1.65-1.18-.05,5.49-.78,4.54.76,5-.72,10.9,1.67,15.94,1.84.83-.07.69.53.67,1,1.23-1.17,3.32.56,4.28-.56.25,2,2.63-.48,3.3,1.61,2.23-1.39,4.83.74,7.55,1.37,1.91.44,5.29-.21,5.55,2.14-2.1-.13-5.12.81-11.41-1.09,1,.87-3.35.74-3.39-.64-1.81,1.94-6.28-1-7.79,1.19-1.26-.41,0-.72-.64-1.35-5,.55-8.09-1.33-12.91-1.56,2.57,2.44,6.08,3.16,10.06,3.22,1.28.15,0,.74,1,1.39,1.37-.19.75-.48,1.26-1.17,5.37,2.28,15.36,2.35,21,4.91-4.69-.63-11.38,0-15.15-2.09A148,148,0,0,1,441.58,196c-.53-.1-1.81.12-.7-.71-1.65.73-3.41.1-5.74-.22-37.15-5.15-80.47-7.78-115.75-9.76-39.54-2.22-76.79-3.22-116.44-2.72-61.62.78-119.59,7.93-175.21,13.95-5,.55-10,1.49-15.11,1.47-1.33-.32-2.46-1.91-1.25-3-2-.38-2.93.29-5-.15a9.83,9.83,0,0,1-1.25-3,13.73,13.73,0,0,1,6.42-2.94c.77-.54.12-.8.15-1.6a171,171,0,0,1,45.35-8.58c26.43-1.1,53.68-4.73,79.64-6,6.25-.3,11.72.06,18.41.14,8.31.1,19.09-1,29.19-.12,6.25.54,13.67-.55,21.16-.56,39.35-.09,71.16-.23,112.28,2C317.24,171.93,315.11,173.81,314.05,172.88Zm64.22,16.05-1.6-.2C376.2,190,378.42,190.26,378.27,188.93Z"></path>
                            <polygon class="cls-3" points="52.57 166.06 57.61 151.99 23.11 157.23 32.36 169.12 52.57 166.06"></polygon>
                            <polygon class="cls-4" points="44.69 183.24 51.75 168.35 33.86 171.06 44.69 183.24"></polygon>
                            <path class="cls-4" d="M.12,10.15l22,145.06,35.47-5.39-22-145a2.6,2.6,0,0,0,0-.4C35,.76,26.62-.95,16.81.54S-.52,6.16,0,9.77A2.62,2.62,0,0,0,.12,10.15Z"></path>
                            <path class="cls-5" d="M433.68,79.45l21-36.33h27.56L448.48,97.51l34.9,56H456l-22-37.76-21.94,37.76H384.49l34.9-56L385.72,43.12h27.35Z"></path>
                        </g>
                            </g>
                    </svg>
                </div>
                <div class="col-12 text-center lh150 mt20 mt-md50">
                    <div class="pre-heading f-18 f-md-22 w600 lh140 white-clr">
                    Before you access your purchase, I want to ask you a very simple yet important question...
                </div>
                </div>
                <div class="col-12 col-md-10 offset-md-1 mt-md30 mt20">
                    <div class="main-heading f-28 f-md-40 w400 text-center lh140 black2-clr red-hat-font">
                        How Would You Like To <span class="w700 pink-clr">Drive More Traffic & Sales To 100X Your Profits With No Extra Efforts? </span>
                    </div>
                </div>
                <div class="col-12 mt-md25 mt20">
                    <div class="f-20 f-md-22 w500 lh140 white-clr text-center">
                    Unlock the Most Powerful Features to Get 100X Better Conversions & Sales<br class="d-none d-md-block"> with this Authority Upgrade
                </div>
                </div>
                <div class="col-12 col-md-10 mx-auto mt-md30 mt20">
                    <img src="assets/images/productbox.webp" class="img-fluid d-block mx-auto">
                    <!-- <div class="responsive-video">
                        <iframe src="https://coursesify.dotcompal.com/video/embed/mws136rt4t" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                    </div> -->
                </div>
                <div class="col-12 mt30 mt-md50 f-md-24 f-20 lh140 w400 text-center white-clr">
                    This is An Exclusive Deal for New<span class="w600 yellow-clr"> "LearnX"</span> Users Only...
                </div>
                <div class="col-md-12 mx-auto col-12 mt15 mt-md20 f-20 f-md-35 text-center probtn1">
                    <a href="#buynow" class="text-center">Upgrade to LearnX Authority Now</a>
                </div>
            </div>
        </div>
    </div>
    <!--1. Header Section End -->



    <!--2. Second Section Start -->
    <div class="second-section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="f-28 f-md-45 w500 lh140 text-capitalize text-center black-clr">
                        The Secret Weapon To Boost Your Profits <span class="w700">From Your Same Academy; Same Courses</span> And Never Have To Look Back Again
                    </div>

                    <div class="f-18 f-md-20 w400 lh140 text-center mt15 mt-md40">
                        This Authority Upgrade puts you on whole another level with a touch of a button.
                        <br><br>From empowering your academy site for more engagement, leads and traffic to boosting your authority, automated social sharing campaigns, capturing more attention with popup ads and monetizing your academy sites with banner
                        placements, <span class="w600">this Authority upgrade has it all.</span>
                    </div>
                </div>

                <div class="col-12">
                    <div class="row">
                        <div class="col-md-6 col-12 mt30 mt-md40 ">
                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-center text-center text-md-start mt20 mt-md50">
                                <img src="assets/images/fe1.webp" class="img-fluid d-block mx-auto">
                                <div class="f-18 f-md-18 lh150 w400 ml-md20 mt20 mt-md0 text-capitalize w-100">
                                    <span class="w600">Empower Your Website for More User Interactions, Leads & FREE SEO Traffic</span>
                                </div>
                            </div>

                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-center text-center text-md-start mt20 mt-md50">
                                <img src="assets/images/fe2.webp" class="img-fluid d-block mx-auto">
                                <div class="f-18 f-md-18 lh150 w400 ml-md20 mt20 mt-md0 text-capitalize w-100">
                                    <span class="w600">Setup Automated Social Campaigns to Get More Exposure & Social Traffic Hands Free</span>
                                </div>
                            </div>

                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-center text-center text-md-start mt20 mt-md50">
                                <img src="assets/images/fe3.webp" class="img-fluid d-block mx-auto">
                                <div class="f-18 f-md-18 lh150 w400 ml-md20 mt20 mt-md0 text-capitalize w-100">
                                    <span class="w600">Generate Viral Traffic by Enabling Social Sharing on Your Marketplace & Blog </span>
                                </div>
                            </div>

                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-center text-center text-md-start mt20 mt-md50">
                                <img src="assets/images/fe4.webp" class="img-fluid d-block mx-auto">
                                <div class="f-18 f-md-18 lh150 w400 ml-md20 mt20 mt-md0 text-capitalize w-100">
                                    <span class="w600">Monetize Your Sites with Banner Placements</span>
                                </div>
                            </div>
                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-center text-center text-md-start mt20 mt-md50">
                                <img src="assets/images/fe5.webp" class="img-fluid d-block mx-auto">
                                <div class="f-18 f-md-18 lh150 w400 ml-md20 mt20 mt-md0 text-capitalize w-100">
                                    <span class="w600">Capture More Leads With 30 EXTRA carefully crafted Lead Popup Templates</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-12 mt30 mt-md40">
                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-center text-center text-md-start mt20 mt-md50">
                                <img src="assets/images/fe6.webp" class="img-fluid d-block mx-auto">
                                <div class="f-18 f-md-18 lh150 w400 ml-md20 mt20 mt-md0 text-capitalize w-100">
                                    <span class="w600">Get High PR Backlinks For Faster Indexing And Targeted Traffic </span> <br>
                                </div>
                            </div>

                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-center text-center text-md-start mt20 mt-md50">
                                <img src="assets/images/fe7.webp" class="img-fluid d-block mx-auto">

                                <div class="f-18 f-md-18 lh150 w400 ml-md20 mt20 mt-md0 text-capitalize w-100">
                                    <span class="w600">15+ High Converting Social Sharing Popup Templates To Get Even More FREE Viral Traffic</span>
                                </div>

                            </div>
                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-center text-center text-md-start mt20 mt-md50">
                                <img src="assets/images/fe8.webp" class="img-fluid d-block mx-auto">
                                <div class="f-18 f-md-18 lh150 w400 ml-md20 mt20 mt-md0 text-capitalize w-100">
                                    <span class="w600">20 Premium Promo Popup Templates to get more conversions</span>
                                </div>
                            </div>
                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-center text-center text-md-start mt20 mt-md50">
                                <img src="assets/images/fe9.webp" class="img-fluid d-block mx-auto">
                                <div class="f-18 f-md-18 lh150 w400 ml-md20 mt20 mt-md0 text-capitalize w-100">
                                    <span class="w600">Special Effects for Your Popup Ads To Visually Entice Your Visitors To Do Nothing But Clicking And Buying</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 mt30 mt-md50 f-md-24 f-20 lh140 w400 text-center black-clr">
                        Authority Upgrade… Special Offer <span class="w600 black-clr">"LearnX"</span> Users Only...
                    </div>
                    <div class="col-md-12 mx-auto col-12 mt15 mt-md20 f-20 f-md-35 text-center probtn1">
                        <a href="#buynow" class="text-center">Upgrade to LearnX Authority Now</a>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    <!--2. Second Section End -->

    <!--3. third Section Starts -->
    <div class="proudly-section">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <div class="f-24 w700 white-clr lh140 presenting-head">
                       Proudly Presenting…
                    </div>
                 </div>
                <div class="col-12 f-28 f-md-45 w700 mt20 mt-md30 text-center white-clr lh140">
                   <span class="pink-clr">Authority Upgrade...</span>  Special Offer
                </div>

                <div class="col-12 col-md-9 mx-auto mt20 mt-md40">
                    <img src="assets/images/productbox.webp" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 mt25 mt-md25 f-md-20 f-18 w400 white-clr mt-md20 text-start lh140">
                    It's  Pranshu & BizOmart here again I am sure you can’t wait to get your hands on LearnX and start profiting from this multi-billion dollar market.<br><br> And to grab a fair share of this huge market, you just need one product, one
                    funnel, a few integrations and you are set to rule.<br><br> But we went a step ahead and empower you to customise and supercharge multiple courses and funnels in just a matter of minutes.<br><br> And there is a great deal ahead…
                    <br><br> The More Courses and funnels you deploy, the more profit you make. All You needed is traffic, brand power and Authority level solution.<br><br> LearnX Authority Upgrade ensures you can scale quickly without having to worry
                    about restrictions...
                </div>
            </div>
        </div>
    </div>

    <!--3. third Section End -->

    <!--feature1 -->
    <div class="white-section">
        <div class="container ">
            <div class="row">
                <div class="col-12">
                    <img src="../common_assets/images/1.webp" class="img-fluid d-block">
                </div>
                <div class="col-12 f-24 f-md-36 w500 lh140 mt10 black-clr">
                    Empower Your Website for More User Interactions, Leads & FREE SEO Traffic
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md30">
                    <img src="assets/images/f1.webp" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 f-18 f-md-20 w400 lh140 mt20 mt-md50">
                    Give your audience the reason to love your academy website by allowing them to interact with your masterly crafted blog posts. So, empower your blogs by allowing visitors to like & dislike your posts and leave comment on it. <br><br>                    You also generate their leads and <span class="w600"> get fresh user generated content that will further boost your search engine ranking and FREE Search traffic.</span><br><br> You are also getting 50DFY blog posts with images to
                    promote your courses on 5 hot niches and establish yourself as an <span class="w600">Authority</span>.
                </div>
            </div>
        </div>
    </div>
    <!--feature2 -->
    <div class="grey-section">
        <div class="container ">
            <div class="row">
                <div class="col-12">
                    <img src="../common_assets/images/2.webp" class="img-fluid d-block">
                </div>
                <div class="col-12 f-24 f-md-36 w500 lh140 mt10 black-clr">
                    Setup Automated Social Campaigns to Get More Exposure & Social Traffic Hands Free
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md30">
                    <img src="assets/images/f2.webp" class="img-fluid d-block mx-auto mt20 mt-md20">
                </div>
                <div class="col-12 f-18 f-md-20 w400 lh140 mt20 mt-md50">
                    Setup your social media sharing campaigns to share your blog posts and courses on major platforms for free social traffic on automation.<br> <br> You just need to set the rules once, and <span class="w600">let the software choose your content  to share and drive traffic for you without any manual headache</span>
                </div>
            </div>
        </div>
    </div>

    <!--feature3 -->
    <div class="white-section">
        <div class="container ">
            <div class="row">
                <div class="col-12">
                    <img src="../common_assets/images/3.webp" class="img-fluid d-block">
                </div>
                <div class="col-12 f-24 f-md-36 w500 lh140 mt10 black-clr">
                    Get High PR Backlinks For Faster Indexing And Targeted Traffic
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md30">
                    <img src="assets/images/f3.webp" class="img-fluid d-block mx-auto mt20 mt-md20">
                </div>
                <div class="col-12 f-18 f-md-20 w400 lh140 mt20 mt-md50">
                    <span class="w600"> You get backlinks from High PR sites like Tumblr, Bloggr & social media giants</span> –This will help you index your content faster
                </div>
            </div>
        </div>
    </div>
    </div>

    <!--feature4 -->
    <div class="grey-section">
        <div class="container ">
            <div class="row">
                <div class="col-12">
                    <img src="../common_assets/images/4.webp" class="img-fluid d-block">
                </div>
                <div class="col-12 f-24 f-md-36 w500 lh140 mt10 black-clr">
                    Generate Viral Traffic by Enabling Social Sharing on Your Marketplace & Blog
                </div>
                <div class="col-12 col-md-8 mx-auto mt20 mt-md40">
                    <img src="assets/images/f4.webp" class="img-fluid d-block mx-auto">
                </div>
            </div>

            <div class="col-12 f-18 f-md-20 w400 lh140 mt20 mt-md50">
                Authority edition gives you another cool feature of <span class="w600">sharing your blogs and products to boost traffic.</span> Now stop thinking and use this masterpiece to get results like never before.
            </div>
        </div>
    </div>

    <!--feature5 -->
    <div class="white-section">
        <div class="container ">
            <div class="row">
                <div class="col-12">
                    <img src="../common_assets/images/5.webp" class="img-fluid d-block">
                </div>
                <div class="col-12 f-24 f-md-36 w500 lh140 mt10 black-clr">
                    15+ High Converting Social Sharing Popup Templates To Get Even More FREE Viral Traffic
                </div>
                <div class="col-12 col-md-8 mx-auto mt20 mt-md40">
                    <img src="assets/images/f5.webp" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 f-18 f-md-20 w400 lh140 mt20 mt-md50">
                    When someone comes to your blog post, <span class="w600">lock them to share it to read the complete post and boost your social media results.</span>
                </div>
            </div>
        </div>
    </div>


    <!--feature6 -->
    <div class="grey-section">
        <div class="container ">
            <div class="row">
                <div class="col-12">
                    <img src="../common_assets/images/6.webp" class="img-fluid d-block">
                </div>
                <div class="col-12 f-24 f-md-36 w500 lh140 mt10 black-clr">
                    Monetize Your Sites with Banner Placements
                </div>
            </div>
            <div class="col-12 col-md-8 mx-auto mt20 mt-md40">
                <img src="assets/images/f6.webp" class="img-fluid d-block mx-auto">
            </div>
            <div class="col-12 f-18 f-md-20 w400 lh140 mt20 mt-md50">
                With your elegant academy sites,now you can open additional monetization channels by <span class="w600">placing banner ads on your e-Learning site home page and blog posts and lure your visitors with the irresistible offers</span> to make
                some extra income from same traffic.
            </div>
        </div>
    </div>

    <!--feature7 -->
    <div class="white-section">
        <div class="container ">
            <div class="row">
                <div class="col-12">
                    <img src="../common_assets/images/7.webp" class="img-fluid d-block">
                </div>
                <div class="col-12 f-24 f-md-36 w500 lh140 mt10 black-clr">
                    20 Premium Promo Popup Templates to get more conversions
                </div>
                <div class="col-12 col-md-8 mx-auto mt20 mt-md40">
                    <img src="assets/images/f7.webp" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 f-18 f-md-20 w400 lh140 mt20 mt-md50">
                    <span class="w600"> Show your best offers or affiliate offers when visitors are most engaged</span> and convert them into paying customers.
                </div>
            </div>
        </div>
    </div>

    <!--feature8-->
    <div class="grey-section">
        <div class="container ">
            <div class="row">
                <div class="col-12">
                    <img src="../common_assets/images/8.webp" class="img-fluid d-block">
                </div>
                <div class="col-12 f-24 f-md-36 w500 lh140 mt10 black-clr">
                    Capture More Leads With 30 EXTRA Carefully Crafted Lead Popup Templates
                </div>
                <div class="col-12 col-md-8 mx-auto mt20 mt-md40">
                    <img src="assets/images/f8.webp" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 f-18 f-md-20 w400 lh140 mt20 mt-md50">
                    With the Authority edition you’re getting 30 MORE lead templates that have been <span class="w600"> carefully crafted to capture maximum leads & get you best results </span> in a cost-effective manner. These lead templates add laurels
                    to your upgrade.
                </div>
            </div>
        </div>
    </div>
    <!-- feature9 -->
    <div class="white-section">
        <div class="container ">
            <div class="row">
                <div class="col-12">
                    <img src="../common_assets/images/9.webp" class="img-fluid d-block">
                </div>
                <div class="col-12 f-24 f-md-36 w500 lh140 mt10 black-clr">
                    Special Effects for Your Popup Ads To Visually Entice Your Visitors To Do Nothing But Clicking And Buying
                </div>
            </div>
            <div class="col-12 col-md-8 mx-auto mt20 mt-md40">
                <img src="assets/images/f9.webp" class="img-fluid d-block mx-auto">
            </div>
            <div class="col-12 f-18 f-md-20 w400 lh140 mt20 mt-md50">
                Visually enticing your visitors is the best way to get your audience hitched to your brand. Keeping this in mind, we are providing you with <span class="w600"> eye-catchy effects that literally entice visitors into clicking and opting in for your offers.</span>
            </div>
        </div>
    </div>

    <!--money back Start -->
    <div class="moneyback-section">
        <div class="container ">
            <div class="row align-items-center">
                <div class="col-12 f-28 f-md-45 lh140 w700 text-center white-clr">
                    30 Days Money Back Guarantee
                </div>
            </div>
            <div class="row align-items-center mt20 mt-md50">
                <div class="col-12 col-md-4 mx-auto">
                    <img src="assets/images/mbg.webp" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-md-8 col-12 mt20 mt-md0 f-18 f-md-20 w400 lh140 white-clr">
                    We have absolutely no doubt that you'll love the extra benefits, training and LearnX Authority upgraded features. <br><br> You can try it out without any risk. If, for any reason, you’re not satisfied with your LearnX Authority
                    upgrade you can simply ask for a refund. Your investment is covered by my 30-Days money back guarantee.
                    <br><br> You have absolutely nothing to lose and everything to gain with this fantastic offer and guarantee.
                </div>
            </div>
        </div>
    </div>
    <!--money back End-->

    <!---Bonus Section Starts-->
    <div class="bonus-section">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <div class="but-design mb20 mb-md20 mt0 mt-md0">
                        <div class="f-28 f-md-45 w700 lh140 text-center white-clr skew-normal">
                            But That’s Not All
                        </div>
                    </div>
                </div>

                <div class="col-12 mt20 f-20 f-md-24 w400 text-center lh140">
                    In addition, we have a number of bonuses for those who want to take action today and start profiting from this opportunity
                </div>

                <!--bonus1-->
                <div class="col-12 col-md-12 mt20 mt-md60">
                    <div class="row align-items-center d-flex felx-wrap">
                        <div class="col-12 col-md-6">
                            <div class="f-md-36 f-24 w700 blue-clr text-nowrap">
                                Bonus 1
                            </div>
                            <div class="f-24 f-md-30 w500 lh140 mt20 mt-md20 left-start black-clr">
                                7 Figure Mastery-
                            </div>
                            <div class="f-18 f-md-20 w400 lh140 mt10 mt-md10 left-start">
                                Get the proven and tested necessary strategies for you to set up your online business from scratch, even if you are not familiar with Internet Marketing. When used effectively with <span class="w600">LearnX Authority Upgrade,</span>                                it gives real results that are scalable as well.

                            </div>
                        </div>
                        <div class="col-12 col-md-6 mt20 mt-md0">
                            <img src="assets/images/bonusbox.webp" class="img-fluid d-block mx-auto">
                        </div>
                    </div>
                </div>


                <!--bonus2-->
                <div class="col-12 col-md-12 mt20 mt-md60">
                    <div class="row align-items-center d-flex felx-wrap">
                        <div class="col-12 col-md-6 order-md-2">
                            <div class="f-md-36 f-24 w700 blue-clr text-nowrap">
                                Bonus 2
                            </div>
                            <div class="f-24 f-md-30 w500 lh140 mt20 mt-md20 left-start black-clr">
                                Social Media Automation-
                            </div>
                            <div class="f-18 f-md-20 w400 lh140 mt10 mt-md10 left-start">
                                Proven and tested techniques that make it fast & easy to automate your social media campaigns & drive tons of laser targeted traffic to your offers without costing a fortune.<br><br> When used with the cool social campaign
                                automation techniques of <span class="w600"> LearnX Authority,</span> the results achieved are worth watching.

                            </div>
                        </div>
                        <div class="col-12 col-md-6 order-md-1 mt20 mt-md0">
                            <img src="assets/images/bonusbox.webp" class="img-fluid d-block mx-auto">
                        </div>
                    </div>
                </div>
            </div>



            <!--bonus3-->
            <div class="col-12 col-md-12 mt20 mt-md60">
                <div class="row align-items-center d-flex felx-wrap">
                    <div class="col-12 col-md-6">
                        <div class="f-md-36 f-24 w700 blue-clr text-nowrap">
                            Bonus 3
                        </div>
                        <div class="f-24 f-md-30 w500 lh140 mt20 mt-md20 left-start black-clr">
                            Unbreakable Links-
                        </div>
                        <div class="f-18 f-md-20 w400 lh140 mt10 mt-md10 left-start">
                            Building backlinks is the most effective way of notifying search engines that you have relevant content to share that people are actively looking for. Now what are you waiting for. <br><br> Now combine it with
                            <span class="w600">LearnX Authority</span> to from High PR sites like Tumblr, Bloggr & other social media giants to help you index your content faster

                        </div>
                    </div>
                    <div class="col-12 col-md-6 mt20 mt-md0">
                        <img src="assets/images/bonusbox.webp" class="img-fluid d-block mx-auto">
                    </div>
                </div>
            </div>


            <!--bonus4-->
            <div class="col-12 col-md-12 mt20 mt-md60">
                <div class="row align-items-center d-flex felx-wrap">
                    <div class="col-12 col-md-6 order-md-2">
                        <div class="f-md-36 f-24 w700 blue-clr text-nowrap">
                            Bonus 4
                        </div>
                        <div class="f-24 f-md-30 w500 lh140 mt20 mt-md20 text-start black-clr">
                            Entrepreneurial Drive Video Training-
                        </div>
                        <div class="f-18 f-md-20 w400 lh140 mt10 mt-md10 left-start">
                            Entrepreneurship is the art of starting something on your own and converting your dreams into reality. But doing that isn’t that easy for everyone.<br><br> Have a look at this useful video step-by-step video course that has
                            proven techniques on how to become successful by learning from the top echelons of the society.<br><br>
                            <span class="w600"> This bonus when combined with LearnX Authority</span> becomes an ultimate growth booster for business owners.

                        </div>
                    </div>
                    <div class="col-12 col-md-6 order-md-1 mt20 mt-md0">
                        <img src="assets/images/bonusbox.webp" class="img-fluid d-block mx-auto">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Bonus Section end -->

    <!--10. Table Section Starts -->
    <div class="white-section padding10" id="buynow">
        <div class="container">
            <div class="row">
                <div class="col-12 f-28 f-md-45 w500 lh140 text-center black-clr">
                    Today You Can Get Unrestricted Access To <span class="w700">LearnX For LESS Than The Price Of Just One Month’s Membership.</span>
                </div>
            </div>
            <div class="row mt50 mt-md-50">
                <div class="col-md-6 col-12 ">
                    <div class="tablebox2">
                        <div class="tbbg2 text-center">
                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" 
                        viewBox="0 0 484.6 200.8" style="enable-background:new 0 0 484.6 200.8; max-height:75px;" xml:space="preserve">
                     <style type="text/css">
                        .st0{fill:#000F0F;}
                        .st1{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_1_);}
                        .st2{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_2_);}
                        .st3{fill-rule:evenodd;clip-rule:evenodd;fill:#000F0F;}
                        .st4{fill:url(#SVGID_3_);}
                     </style>
                     <g>
                        <path class="st0" d="M143.9,95.9c-1.7-4.6-4.3-8.7-7.8-12.2c-3.2-3.2-7.1-5.6-11.3-7.1c-4.3-1.5-8.9-2.3-13.5-2.3
                           c-6.8-0.2-13.5,1.6-19.3,5.1c-5.5,3.4-9.9,8.3-12.8,14.1c-3.2,6.5-4.7,13.7-4.6,21c0,8.1,1.5,15.1,4.5,21.1
                           c2.9,5.8,7.4,10.6,13,13.8c5.6,3.3,12.3,4.9,20.1,4.9c5.5,0.1,10.9-0.8,16-2.8c4.4-1.6,8.4-4.3,11.5-7.8c3-3.3,5.1-7.4,6.1-11.8
                           l-17-1.9c-0.8,2.1-2,4-3.6,5.5c-1.6,1.5-3.5,2.6-5.6,3.3c-2.3,0.8-4.8,1.2-7.2,1.1c-3.6,0.1-7.2-0.8-10.4-2.5
                           c-3-1.7-5.4-4.3-6.9-7.4c-1.7-3.5-2.5-7.3-2.5-11.2h53.9v-5.6C146.9,107.5,145.9,101.5,143.9,95.9z M92.8,106.7
                           c0.2-2.9,1-5.8,2.4-8.4c1.5-2.9,3.8-5.3,6.6-7.1c2.9-1.8,6.4-2.8,9.8-2.7c3.2-0.1,6.4,0.8,9.2,2.4c2.6,1.5,4.8,3.8,6.2,6.5
                           c1.5,2.9,2.3,6.1,2.3,9.3H92.8z"/>
                        <path class="st0" d="M184.5,154.4c-4.6,0.1-9.1-0.8-13.2-2.6c-3.8-1.7-7-4.4-9.3-7.8c-2.4-3.8-3.6-8.3-3.4-12.8
                           c-0.1-3.8,0.7-7.5,2.4-10.9c1.6-2.8,3.9-5.2,6.6-6.9c2.9-1.8,6.1-3.1,9.5-3.9c3.6-0.9,7.2-1.6,10.9-2c4.5-0.5,8.2-0.9,11-1.3
                           c2.2-0.2,4.3-0.8,6.2-1.8c1.3-0.8,2-2.3,1.9-3.9v-0.3c0-3.8-1.1-6.7-3.4-8.8c-2.3-2.1-5.5-3.1-9.7-3.1c-4.5,0-8,1-10.6,2.9
                           c-2.4,1.7-4.3,4.1-5.3,6.9l-17-2.4c1.2-4.4,3.5-8.5,6.7-11.8c3.2-3.2,7.1-5.7,11.4-7.2c4.8-1.6,9.8-2.5,14.8-2.4
                           c3.8,0,7.5,0.4,11.1,1.3c3.6,0.8,7,2.3,10.1,4.3c3.1,2,5.7,4.8,7.4,8.1c1.9,3.4,2.8,7.7,2.8,12.9v51.8h-17.6v-10.6h-0.6
                           c-1.2,2.3-2.8,4.3-4.7,6c-2.2,2-4.7,3.5-7.5,4.5C191.7,153.8,188.1,154.4,184.5,154.4z M189.2,140.9c3.3,0.1,6.6-0.7,9.5-2.2
                           c2.6-1.3,4.7-3.4,6.2-5.8c1.5-2.4,2.2-5.1,2.2-7.9v-9.1c-0.9,0.6-1.9,1.1-2.9,1.3c-1.5,0.4-3,0.8-4.6,1.1c-1.7,0.3-3.4,0.6-5,0.8
                           l-4.3,0.6c-2.5,0.3-5,0.9-7.3,1.8c-2,0.7-3.7,1.9-5.1,3.5c-3.1,4.2-2.3,10.1,1.8,13.4C182.6,140.2,185.9,141.1,189.2,140.9
                           L189.2,140.9z"/>
                        <path class="st0" d="M243.8,152.8V75.3h17.7v12.9h0.8c1.2-4.2,3.8-7.8,7.3-10.4c3.5-2.4,7.6-3.7,11.8-3.7c1,0,2.1,0,3.4,0.1
                           c1.1,0.1,2.1,0.2,3.1,0.4v16.8c-1.2-0.4-2.4-0.6-3.6-0.7c-1.6-0.2-3.2-0.3-4.8-0.3c-3.1-0.1-6.2,0.7-9,2.1
                           c-2.6,1.4-4.7,3.4-6.2,5.9c-1.5,2.6-2.3,5.7-2.3,8.7v45.5H243.8z"/>
                        <path class="st0" d="M318.4,107.4v45.4h-18.3V75.3h17.5v13.2h0.9c1.7-4.3,4.7-7.9,8.5-10.3c3.9-2.6,8.8-3.8,14.5-3.8
                           c4.9-0.1,9.7,1.1,14,3.4c4,2.3,7.2,5.8,9.3,9.9c2.3,4.9,3.4,10.3,3.3,15.8v49.3h-18.3v-46.5c0-5.2-1.3-9.2-4-12.2
                           s-6.4-4.4-11.1-4.4c-3-0.1-5.9,0.7-8.5,2.1c-2.5,1.4-4.5,3.5-5.8,6C319,100.8,318.3,104.1,318.4,107.4z"/>
                        
                           <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="-127.5591" y1="13.8429" x2="276.291" y2="13.8429" gradientTransform="matrix(1.19 -1.000000e-02 -5.000000e-02 -1 157.49 201.88)">
                           <stop  offset="0" style="stop-color:#EAD40C"/>
                           <stop  offset="1" style="stop-color:#FF00E9"/>
                        </linearGradient>
                        <path class="st1" d="M314,172.9c12.7-0.1,27.3,2.5,40.8,2.6l-0.1,1c1.1,0.1,1-0.9,2-0.8c1.4-0.2,0,1.6,1.8,1.1
                           c0.5-0.1,0.1-0.6-0.3-0.7c0.8,0,2.3,0.2,3,1.3c1.3,0-0.9-1.3,0.9-0.9c6,1.6,13.4,0,20,3.2c11.6,0.7,24.8,2.4,38,4.3
                           c2.9,0.4,5.4,1.1,7.6,1.1c2-0.1,5.7,0.8,8,1.1c2.5,0.3,5.2,1.6,7.1,1.6c-1.2-0.1,5.5-0.8,4.5,0.8c5-0.7,10.9,1.7,15.9,1.8
                           c0.8-0.1,0.7,0.5,0.7,1c1.2-1.2,3.3,0.6,4.3-0.6c0.3,2,2.6-0.5,3.3,1.6c2.2-1.4,4.8,0.7,7.5,1.4c1.9,0.4,5.3-0.2,5.5,2.1
                           c-2.1-0.1-5.1,0.8-11.4-1.1c1,0.9-3.4,0.7-3.4-0.6c-1.8,1.9-6.3-1-7.8,1.2c-1.3-0.4,0-0.7-0.6-1.4c-5,0.6-8.1-1.3-12.9-1.6
                           c2.6,2.4,6.1,3.2,10.1,3.2c1.3,0.1,0,0.7,1,1.4c1.4-0.2,0.8-0.5,1.3-1.2c5.4,2.3,15.4,2.4,21,4.9c-4.7-0.6-11.4,0-15.1-2.1
                           c-8.4-0.2-16.8-1.1-25.1-2.8c-0.5-0.1-1.8,0.1-0.7-0.7c-1.6,0.7-3.4,0.1-5.7-0.2c-37.1-5.1-80.5-7.8-115.8-9.8
                           c-39.5-2.2-76.8-3.2-116.4-2.7c-61.6,0.8-119.6,7.9-175.2,13.9c-5,0.6-10,1.5-15.1,1.5c-1.3-0.3-2.5-1.9-1.3-3
                           c-2-0.4-2.9,0.3-5-0.1c-0.6-0.9-1-1.9-1.3-3c1.8-1.5,4.1-2.5,6.4-2.9c0.8-0.5,0.1-0.8,0.1-1.6c14.7-4.9,29.9-7.8,45.3-8.6
                           c26.4-1.1,53.7-4.7,79.6-6c6.3-0.3,11.7,0.1,18.4,0.1c8.3,0.1,19.1-1,29.2-0.1c6.3,0.5,13.7-0.6,21.2-0.6
                           c39.4-0.1,71.2-0.2,112.3,2C317.2,171.9,315.1,173.8,314,172.9z M378.3,188.9l-1.6-0.2C376.2,190,378.4,190.3,378.3,188.9
                           L378.3,188.9z"/>
                        
                           <linearGradient id="SVGID_2_" gradientUnits="userSpaceOnUse" x1="23.11" y1="41.445" x2="57.61" y2="41.445" gradientTransform="matrix(1 0 0 -1 0 202)">
                           <stop  offset="0" style="stop-color:#EAD40C"/>
                           <stop  offset="1" style="stop-color:#FF00E9"/>
                        </linearGradient>
                        <polygon class="st2" points="52.6,166.1 57.6,152 23.1,157.2 32.4,169.1 	"/>
                        <polygon class="st3" points="44.7,183.2 51.8,168.4 33.9,171.1 	"/>
                        <path class="st3" d="M0.1,10.1l22,145.1l35.5-5.4l-22-145c0-0.1,0-0.3,0-0.4c-0.6-3.7-9-5.4-18.8-3.9S-0.5,6.2,0,9.8
                           C0,9.9,0.1,10,0.1,10.1z"/>
                        
                           <linearGradient id="SVGID_3_" gradientUnits="userSpaceOnUse" x1="384.49" y1="103.685" x2="483.38" y2="103.685" gradientTransform="matrix(1 0 0 -1 0 202)">
                           <stop  offset="0" style="stop-color:#EAD40C"/>
                           <stop  offset="1" style="stop-color:#FF00E9"/>
                        </linearGradient>
                        <path class="st4" d="M433.7,79.4l21-36.3h27.6l-33.8,54.4l34.9,56H456l-22-37.8l-21.9,37.8h-27.6l34.9-56l-33.7-54.4h27.4
                           L433.7,79.4z"/>
                     </g>
                     </svg>
                            <div class="text-uppercase mt15 mt-md15 w600 f-md-32 f-22 text-center black-clr lh120">
                            Authority   Personal
                            </div>
                        </div>
                        <ul class="f-16 f-md-16 w400 text-center lh130 vgreytick mb0">
                            <li>
                                <span class="w500">Empower Your Website for More User Interactions, Leads & FREE SEO Traffic</span>
                            </li>

                            <li>
                                <span class="w500">Setup Automated Social Campaigns to Get More Exposure & Social Traffic Hands Free</span>
                            </li>
                            <li>
                                <span class="w500">Get High PR Backlinks For Faster Indexing And Targeted Traffic</span>
                            </li>
                            <li>
                                <span class="w500">Generate Viral Traffic by Enabling Social Sharing on Your Marketplace & Blog</span>
                            </li>
                            <li>
                                <span class="w500">15+ High Converting Social Sharing Popup Templates To Get Even More FREE Viral Traffic</span>
                            </li>
                            <li>
                                <span class="w500">Monetize Your Sites with Banner Placements</span>
                            </li>
                            <li>
                                <span class="w500">20 Premium Promo Popup Templates to get more conversions</span>
                            </li>
                            <li>
                                <span class="w500">Capture More Leads With 30 EXTRA carefully crafted Lead Popup Templates</span>
                            </li>
                            <li>
                                <span class="w500">Special Effects for Your Popup Ads To Visually Entice Your Visitors To Do Nothing But Clicking And Buying</span>
                            </li>
                            <br>
                        </ul>
                        <div class="myfeatureslast f-md-25 f-16 w400 text-center lh160 hideme">
                            <div class="f-md-37 f-27 w600 buypad text-center"> Only $47 </div>
                            <div>
                                <a href="https://warriorplus.com/o2/buy/fbmn7d/dhw2nc/nyn85v"><img src="https://warriorplus.com/o2/btn/fn200011000/fbmn7d/dhw2nc/373797" border="0" class="img-fluid d-block mx-auto"></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-12 mt-md-0 mt-5">
                    <div class="tablebox3">
                        <div class="tbbg3 text-center">
                            <div class="relative">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 484.58 200.84" style="max-height:75px;">
                        <defs>
                            <style>
                                .cls-1,.cls-4{fill:#fff;}
                                .cls-2,.cls-3,.cls-4{fill-rule:evenodd;}
                                .cls-2{fill:url(#linear-gradient);}
                                .cls-3{fill:url(#linear-gradient-2);}
                                .cls-5{fill:url(#linear-gradient-3);}
                            </style>
                            <linearGradient id="linear-gradient" x1="-154.67" y1="191.86" x2="249.18" y2="191.86" gradientTransform="matrix(1.19, -0.01, 0.05, 1, 178.46, -4.31)" gradientUnits="userSpaceOnUse">
                                <stop offset="0" stop-color="#ead40c"></stop>
                                <stop offset="1" stop-color="#ff00e9"></stop>
                            </linearGradient>
                            <linearGradient id="linear-gradient-2" x1="23.11" y1="160.56" x2="57.61" y2="160.56" gradientTransform="matrix(1, 0, 0, 1, 0, 0)" xlink:href="#linear-gradient"></linearGradient>
                            <linearGradient id="linear-gradient-3" x1="384.49" y1="98.33" x2="483.38" y2="98.33" gradientTransform="matrix(1, 0, 0, 1, 0, 0)" xlink:href="#linear-gradient"></linearGradient>
                        </defs>
                        <g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1">
                            <path class="cls-1" d="M112.31,154.3q-11.65,0-20.1-4.87a32.56,32.56,0,0,1-13-13.82q-4.53-9-4.54-21.11a46.06,46.06,0,0,1,4.57-21A34.29,34.29,0,0,1,92,79.37a36.11,36.11,0,0,1,19.31-5.06,39.65,39.65,0,0,1,13.54,2.29,31,31,0,0,1,11.3,7.09,33.32,33.32,0,0,1,7.75,12.18,49.23,49.23,0,0,1,2.82,17.57V119H83.26v-12.3h46A19.87,19.87,0,0,0,127,97.38a16.6,16.6,0,0,0-6.18-6.48,17.59,17.59,0,0,0-9.21-2.37,17.88,17.88,0,0,0-9.83,2.7,18.81,18.81,0,0,0-6.58,7.06,20.17,20.17,0,0,0-2.4,9.56v10.74a25.15,25.15,0,0,0,2.47,11.57,17.42,17.42,0,0,0,6.91,7.37,20.52,20.52,0,0,0,10.39,2.55,21.62,21.62,0,0,0,7.22-1.14,15.71,15.71,0,0,0,5.59-3.35,14.11,14.11,0,0,0,3.59-5.5L146,132a26.44,26.44,0,0,1-6.13,11.77,29.72,29.72,0,0,1-11.52,7.77A43.74,43.74,0,0,1,112.31,154.3Z"></path>
                            <path class="cls-1" d="M184.49,154.35a31.78,31.78,0,0,1-13.24-2.65,21.26,21.26,0,0,1-9.28-7.84,22.86,22.86,0,0,1-3.41-12.81A21.92,21.92,0,0,1,161,120.2a18.9,18.9,0,0,1,6.61-6.86,33.85,33.85,0,0,1,9.46-3.9,77.76,77.76,0,0,1,10.92-2q6.81-.7,11-1.28a15.91,15.91,0,0,0,6.18-1.82,4.25,4.25,0,0,0,1.94-3.86v-.3q0-5.7-3.38-8.83T194,88.28q-6.71,0-10.62,2.92a14.55,14.55,0,0,0-5.27,6.91l-17-2.42a27.51,27.51,0,0,1,6.66-11.83,29.46,29.46,0,0,1,11.35-7.16,43.73,43.73,0,0,1,14.83-2.39,47.89,47.89,0,0,1,11.14,1.31,31.6,31.6,0,0,1,10.14,4.31,22.12,22.12,0,0,1,7.39,8.14q2.81,5.15,2.8,12.87v51.85H207.84V142.14h-.61a22.1,22.1,0,0,1-4.66,6,22.35,22.35,0,0,1-7.52,4.49A30,30,0,0,1,184.49,154.35Zm4.74-13.42a19.7,19.7,0,0,0,9.53-2.19,16,16,0,0,0,6.23-5.83,15,15,0,0,0,2.19-7.91v-9.13a8.72,8.72,0,0,1-2.9,1.31,45.56,45.56,0,0,1-4.56,1.06c-1.68.3-3.35.57-5,.8l-4.29.61a32,32,0,0,0-7.32,1.81A12.29,12.29,0,0,0,178,125a9.76,9.76,0,0,0,1.82,13.39A16,16,0,0,0,189.23,140.93Z"></path>
                            <path class="cls-1" d="M243.8,152.79V75.31h17.7V88.23h.81a19.36,19.36,0,0,1,7.29-10.37,20,20,0,0,1,11.83-3.66c1,0,2.14,0,3.4.13a23.83,23.83,0,0,1,3.15.38V91.5a21,21,0,0,0-3.65-.73,38.61,38.61,0,0,0-4.82-.32,18.47,18.47,0,0,0-8.95,2.14,15.94,15.94,0,0,0-6.23,5.93,16.55,16.55,0,0,0-2.27,8.72v45.55Z"></path>
                            <path class="cls-1" d="M318.4,107.39v45.4H300.14V75.31h17.45V88.48h.91a22,22,0,0,1,8.55-10.34q5.86-3.84,14.55-3.83a27.58,27.58,0,0,1,14,3.43,23.19,23.19,0,0,1,9.28,9.93,34.22,34.22,0,0,1,3.26,15.79v49.33H349.87V106.28c0-5.17-1.34-9.23-4-12.15s-6.37-4.39-11.07-4.39a17,17,0,0,0-8.5,2.09,14.67,14.67,0,0,0-5.8,6A20,20,0,0,0,318.4,107.39Z"></path>
                            <path class="cls-2" d="M314.05,172.88c12.69-.14,27.32,2.53,40.82,2.61l-.09,1c1.07.15,1-.88,2-.78,1.42-.18,0,1.57,1.82,1.14.54-.06.08-.56-.32-.68.77,0,2.31.17,3,1.26,1.3,0-.9-1.34.85-.89,6,1.63,13.39,0,20,3.2,11.64.72,24.79,2.38,38,4.27,2.85.41,5.37,1.11,7.57,1.05,2-.05,5.68.78,8,1.08,2.53.34,5.16,1.56,7.13,1.65-1.18-.05,5.49-.78,4.54.76,5-.72,10.9,1.67,15.94,1.84.83-.07.69.53.67,1,1.23-1.17,3.32.56,4.28-.56.25,2,2.63-.48,3.3,1.61,2.23-1.39,4.83.74,7.55,1.37,1.91.44,5.29-.21,5.55,2.14-2.1-.13-5.12.81-11.41-1.09,1,.87-3.35.74-3.39-.64-1.81,1.94-6.28-1-7.79,1.19-1.26-.41,0-.72-.64-1.35-5,.55-8.09-1.33-12.91-1.56,2.57,2.44,6.08,3.16,10.06,3.22,1.28.15,0,.74,1,1.39,1.37-.19.75-.48,1.26-1.17,5.37,2.28,15.36,2.35,21,4.91-4.69-.63-11.38,0-15.15-2.09A148,148,0,0,1,441.58,196c-.53-.1-1.81.12-.7-.71-1.65.73-3.41.1-5.74-.22-37.15-5.15-80.47-7.78-115.75-9.76-39.54-2.22-76.79-3.22-116.44-2.72-61.62.78-119.59,7.93-175.21,13.95-5,.55-10,1.49-15.11,1.47-1.33-.32-2.46-1.91-1.25-3-2-.38-2.93.29-5-.15a9.83,9.83,0,0,1-1.25-3,13.73,13.73,0,0,1,6.42-2.94c.77-.54.12-.8.15-1.6a171,171,0,0,1,45.35-8.58c26.43-1.1,53.68-4.73,79.64-6,6.25-.3,11.72.06,18.41.14,8.31.1,19.09-1,29.19-.12,6.25.54,13.67-.55,21.16-.56,39.35-.09,71.16-.23,112.28,2C317.24,171.93,315.11,173.81,314.05,172.88Zm64.22,16.05-1.6-.2C376.2,190,378.42,190.26,378.27,188.93Z"></path>
                            <polygon class="cls-3" points="52.57 166.06 57.61 151.99 23.11 157.23 32.36 169.12 52.57 166.06"></polygon>
                            <polygon class="cls-4" points="44.69 183.24 51.75 168.35 33.86 171.06 44.69 183.24"></polygon>
                            <path class="cls-4" d="M.12,10.15l22,145.06,35.47-5.39-22-145a2.6,2.6,0,0,0,0-.4C35,.76,26.62-.95,16.81.54S-.52,6.16,0,9.77A2.62,2.62,0,0,0,.12,10.15Z"></path>
                            <path class="cls-5" d="M433.68,79.45l21-36.33h27.56L448.48,97.51l34.9,56H456l-22-37.76-21.94,37.76H384.49l34.9-56L385.72,43.12h27.35Z"></path>
                        </g>
                            </g>
                    </svg>
                            </div>
                            <div class="text-uppercase mt15 mt-md15 w600 f-md-32 f-22 text-center white-clr lh120">Authority Commercial</div>
                        </div>
                        <ul class="f-16 w400 text-center lh130 grey-tick-last mb0 white-clr">
                            <li>
                                <span class="w500">Empower Your Website for More User Interactions, Leads & FREE SEO Traffic</span>
                            </li>
                            <li>
                                <span class="w500">Setup Automated Social Campaigns to Get More Exposure & Social Traffic Hands Free</span>
                            </li>
                            <li>
                                <span class="w500">Get High PR Backlinks For Faster Indexing And Targeted Traffic</span>
                            </li>
                            <li>
                                <span class="w500">Generate Viral Traffic by Enabling Social Sharing on Your Marketplace & Blog</span>
                            </li>
                            <li>
                                <span class="w500">15+ High Converting Social Sharing Popup Templates To Get Even More FREE Viral Traffic</span>
                            </li>
                            <li>
                                <span class="w500">Monetize Your Sites with Banner Placements</span>
                            </li>
                            <li>
                                <span class="w500">20 Premium Promo Popup Templates to get more conversions</span>
                            </li>
                            <li>
                                <span class="w500">Capture More Leads With 30 EXTRA carefully crafted Lead Popup Templates</span>
                            </li>
                            <li>
                                <span class="w500">Special Effects for Your Popup Ads To Visually Entice Your Visitors To Do Nothing But Clicking And Buying</span>
                            </li>
                            <li>
                                <span class="w600">Commercial License - Start providing services to clients and charge hundreds of dollars every month</span>
                            </li>

                        </ul>
                        <div class="myfeatureslast-com f-md-25 f-16 w400 text-center lh160 hideme">
                            <div class="f-md-37 f-27 w600 buypad text-center black-clr"> Only $49</div>
                            <div>
                                <a href="https://warriorplus.com/o2/buy/fbmn7d/dhw2nc/twcv5g"><img src="https://warriorplus.com/o2/btn/fn200011000/fbmn7d/dhw2nc/373998" border="0" class="img-fluid d-block mx-auto"></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12  mt25 mt-md40 text-center">
                    <a class="kapblue f-md-22 f-18 lh140 w400" href="https://warriorplus.com/o/nothanks/dhw2nc" target="_blank" class="link">
                        No thanks – I don’t want to drive more traffic & sales to 100x my profits without any no extra efforts. I know that LearnX Authority Edition can increase my profits & I duly realize that I won't get this offer again. Please take me to the next step to get access to my purchase
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!--10. Table Section End -->

    <!--Footer Section Start -->
    <div class="footer-section">
        <div class="container ">
            <div class="row">
                <div class="col-12 text-center">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 484.58 200.84" style="max-height:75px;">
                        <defs>
                            <style>
                                .cls-1,.cls-4{fill:#fff;}
                                .cls-2,.cls-3,.cls-4{fill-rule:evenodd;}
                                .cls-2{fill:url(#linear-gradient);}
                                .cls-3{fill:url(#linear-gradient-2);}
                                .cls-5{fill:url(#linear-gradient-3);}
                            </style>
                            <linearGradient id="linear-gradient" x1="-154.67" y1="191.86" x2="249.18" y2="191.86" gradientTransform="matrix(1.19, -0.01, 0.05, 1, 178.46, -4.31)" gradientUnits="userSpaceOnUse">
                                <stop offset="0" stop-color="#ead40c"></stop>
                                <stop offset="1" stop-color="#ff00e9"></stop>
                            </linearGradient>
                            <linearGradient id="linear-gradient-2" x1="23.11" y1="160.56" x2="57.61" y2="160.56" gradientTransform="matrix(1, 0, 0, 1, 0, 0)" xlink:href="#linear-gradient"></linearGradient>
                            <linearGradient id="linear-gradient-3" x1="384.49" y1="98.33" x2="483.38" y2="98.33" gradientTransform="matrix(1, 0, 0, 1, 0, 0)" xlink:href="#linear-gradient"></linearGradient>
                        </defs>
                        <g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1">
                            <path class="cls-1" d="M112.31,154.3q-11.65,0-20.1-4.87a32.56,32.56,0,0,1-13-13.82q-4.53-9-4.54-21.11a46.06,46.06,0,0,1,4.57-21A34.29,34.29,0,0,1,92,79.37a36.11,36.11,0,0,1,19.31-5.06,39.65,39.65,0,0,1,13.54,2.29,31,31,0,0,1,11.3,7.09,33.32,33.32,0,0,1,7.75,12.18,49.23,49.23,0,0,1,2.82,17.57V119H83.26v-12.3h46A19.87,19.87,0,0,0,127,97.38a16.6,16.6,0,0,0-6.18-6.48,17.59,17.59,0,0,0-9.21-2.37,17.88,17.88,0,0,0-9.83,2.7,18.81,18.81,0,0,0-6.58,7.06,20.17,20.17,0,0,0-2.4,9.56v10.74a25.15,25.15,0,0,0,2.47,11.57,17.42,17.42,0,0,0,6.91,7.37,20.52,20.52,0,0,0,10.39,2.55,21.62,21.62,0,0,0,7.22-1.14,15.71,15.71,0,0,0,5.59-3.35,14.11,14.11,0,0,0,3.59-5.5L146,132a26.44,26.44,0,0,1-6.13,11.77,29.72,29.72,0,0,1-11.52,7.77A43.74,43.74,0,0,1,112.31,154.3Z"></path>
                            <path class="cls-1" d="M184.49,154.35a31.78,31.78,0,0,1-13.24-2.65,21.26,21.26,0,0,1-9.28-7.84,22.86,22.86,0,0,1-3.41-12.81A21.92,21.92,0,0,1,161,120.2a18.9,18.9,0,0,1,6.61-6.86,33.85,33.85,0,0,1,9.46-3.9,77.76,77.76,0,0,1,10.92-2q6.81-.7,11-1.28a15.91,15.91,0,0,0,6.18-1.82,4.25,4.25,0,0,0,1.94-3.86v-.3q0-5.7-3.38-8.83T194,88.28q-6.71,0-10.62,2.92a14.55,14.55,0,0,0-5.27,6.91l-17-2.42a27.51,27.51,0,0,1,6.66-11.83,29.46,29.46,0,0,1,11.35-7.16,43.73,43.73,0,0,1,14.83-2.39,47.89,47.89,0,0,1,11.14,1.31,31.6,31.6,0,0,1,10.14,4.31,22.12,22.12,0,0,1,7.39,8.14q2.81,5.15,2.8,12.87v51.85H207.84V142.14h-.61a22.1,22.1,0,0,1-4.66,6,22.35,22.35,0,0,1-7.52,4.49A30,30,0,0,1,184.49,154.35Zm4.74-13.42a19.7,19.7,0,0,0,9.53-2.19,16,16,0,0,0,6.23-5.83,15,15,0,0,0,2.19-7.91v-9.13a8.72,8.72,0,0,1-2.9,1.31,45.56,45.56,0,0,1-4.56,1.06c-1.68.3-3.35.57-5,.8l-4.29.61a32,32,0,0,0-7.32,1.81A12.29,12.29,0,0,0,178,125a9.76,9.76,0,0,0,1.82,13.39A16,16,0,0,0,189.23,140.93Z"></path>
                            <path class="cls-1" d="M243.8,152.79V75.31h17.7V88.23h.81a19.36,19.36,0,0,1,7.29-10.37,20,20,0,0,1,11.83-3.66c1,0,2.14,0,3.4.13a23.83,23.83,0,0,1,3.15.38V91.5a21,21,0,0,0-3.65-.73,38.61,38.61,0,0,0-4.82-.32,18.47,18.47,0,0,0-8.95,2.14,15.94,15.94,0,0,0-6.23,5.93,16.55,16.55,0,0,0-2.27,8.72v45.55Z"></path>
                            <path class="cls-1" d="M318.4,107.39v45.4H300.14V75.31h17.45V88.48h.91a22,22,0,0,1,8.55-10.34q5.86-3.84,14.55-3.83a27.58,27.58,0,0,1,14,3.43,23.19,23.19,0,0,1,9.28,9.93,34.22,34.22,0,0,1,3.26,15.79v49.33H349.87V106.28c0-5.17-1.34-9.23-4-12.15s-6.37-4.39-11.07-4.39a17,17,0,0,0-8.5,2.09,14.67,14.67,0,0,0-5.8,6A20,20,0,0,0,318.4,107.39Z"></path>
                            <path class="cls-2" d="M314.05,172.88c12.69-.14,27.32,2.53,40.82,2.61l-.09,1c1.07.15,1-.88,2-.78,1.42-.18,0,1.57,1.82,1.14.54-.06.08-.56-.32-.68.77,0,2.31.17,3,1.26,1.3,0-.9-1.34.85-.89,6,1.63,13.39,0,20,3.2,11.64.72,24.79,2.38,38,4.27,2.85.41,5.37,1.11,7.57,1.05,2-.05,5.68.78,8,1.08,2.53.34,5.16,1.56,7.13,1.65-1.18-.05,5.49-.78,4.54.76,5-.72,10.9,1.67,15.94,1.84.83-.07.69.53.67,1,1.23-1.17,3.32.56,4.28-.56.25,2,2.63-.48,3.3,1.61,2.23-1.39,4.83.74,7.55,1.37,1.91.44,5.29-.21,5.55,2.14-2.1-.13-5.12.81-11.41-1.09,1,.87-3.35.74-3.39-.64-1.81,1.94-6.28-1-7.79,1.19-1.26-.41,0-.72-.64-1.35-5,.55-8.09-1.33-12.91-1.56,2.57,2.44,6.08,3.16,10.06,3.22,1.28.15,0,.74,1,1.39,1.37-.19.75-.48,1.26-1.17,5.37,2.28,15.36,2.35,21,4.91-4.69-.63-11.38,0-15.15-2.09A148,148,0,0,1,441.58,196c-.53-.1-1.81.12-.7-.71-1.65.73-3.41.1-5.74-.22-37.15-5.15-80.47-7.78-115.75-9.76-39.54-2.22-76.79-3.22-116.44-2.72-61.62.78-119.59,7.93-175.21,13.95-5,.55-10,1.49-15.11,1.47-1.33-.32-2.46-1.91-1.25-3-2-.38-2.93.29-5-.15a9.83,9.83,0,0,1-1.25-3,13.73,13.73,0,0,1,6.42-2.94c.77-.54.12-.8.15-1.6a171,171,0,0,1,45.35-8.58c26.43-1.1,53.68-4.73,79.64-6,6.25-.3,11.72.06,18.41.14,8.31.1,19.09-1,29.19-.12,6.25.54,13.67-.55,21.16-.56,39.35-.09,71.16-.23,112.28,2C317.24,171.93,315.11,173.81,314.05,172.88Zm64.22,16.05-1.6-.2C376.2,190,378.42,190.26,378.27,188.93Z"></path>
                            <polygon class="cls-3" points="52.57 166.06 57.61 151.99 23.11 157.23 32.36 169.12 52.57 166.06"></polygon>
                            <polygon class="cls-4" points="44.69 183.24 51.75 168.35 33.86 171.06 44.69 183.24"></polygon>
                            <path class="cls-4" d="M.12,10.15l22,145.06,35.47-5.39-22-145a2.6,2.6,0,0,0,0-.4C35,.76,26.62-.95,16.81.54S-.52,6.16,0,9.77A2.62,2.62,0,0,0,.12,10.15Z"></path>
                            <path class="cls-5" d="M433.68,79.45l21-36.33h27.56L448.48,97.51l34.9,56H456l-22-37.76-21.94,37.76H384.49l34.9-56L385.72,43.12h27.35Z"></path>
                        </g>
                            </g>
                    </svg>
                    <div class="f-14 f-md-16 w400 mt20 lh160 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
                    <div class=" mt20 mt-md40 white-clr"> <div class="f-20 w500 mt20 lh140 white-clr text-center"><script type="text/javascript" src="https://warriorplus.com/o2/disclaimer/fbmn7d" defer></script><div class="wplus_spdisclaimer"></div></div></div>
                </div>
                <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40 align-items-center">
                    <div class="f-14 f-md-16 w400 lh160 white-clr text-xs-center">Copyright © LearnX </div>
                    <ul class="footer-ul w400 f-14 f-md-16 white-clr text-center text-md-right">
                        <li><a href="https://support.bizomart.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                        <li><a href="http://getlearnx.com/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                        <li><a href="http://getlearnx.com/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                        <li><a href="http://getlearnx.com/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                        <li><a href="http://getlearnx.com/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                        <li><a href="http://getlearnx.com/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                        <li><a href="http://getlearnx.com/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!--Footer Section End -->
    <script type="text/javascript" src="assets/js/ouibounce.min.js"></script>
<div id="ouibounce-modal" style="z-index:9999999; display:none;">
   <div class="underlay"></div>
   <div class="modal-wrapper" style="display:block;">
      <div class="modal-bg">
		<button type="button" class="close" onclick="document.getElementById('ouibounce-modal').style.display = 'none';">
			X 
		</button>
		<div class="model-header">
      <div class="d-flex justify-content-center align-items-center gap-3">
            <img src="assets/images/hold.png" alt="" class="img-fluid d-block m131">
            <div>
               <div class="f-md-56 f-24 w700 lh130 white-clr">
                  WAIT! HOLD ON!!
               </div>
               <div class="f-md-28 f-18 w700 lh130 white-clr mt10 mt-md0">
                  Don't Leave Empty Handed
               </div>
            </div>
         </div>
		</div>
         <div class="col-12 for-padding">
			<div class="copun-line f-md-45 f-16 w500 lh130 text-center black-clr mt10">
         You've Qualified For An <span class="w700 red-clr"><br> INSTANT $100 DISCOUNT</span>  
			</div>
         <div class="copun-line f-md-30 f-16 w700 lh130 text-center black-clr mt10" style="background-color:yellow; padding:5px 10px; display:inline-block">
         Regular Price <strike>$149</strike>,
         Now Only $49	
			</div>
			 <div class="mt-md20 mt10 text-center ">
				<a href="https://warriorplus.com/o2/buy/fbmn7d/dhw2nc/twcv5g" class="cta-link-btn1">Grab SPECIAL Discount Now!
				<br>
				<span class="f-12 white-clr w600 text-uppercase">Act Now! Limited to First 100 Buyers Only.</span> </a>
			 </div>
			<!-- <div class="mt-md20 mt10 text-center f-md-26 f-16 w900 lh150 text-center red-clr text-uppercase">
				Coupon Code EXPIRES IN:
			</div>
			<div class="countdown counter-white text-center">
                     <div class="timer-label text-center"><span class="f-24 f-md-42 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
                     <div class="timer-label text-center"><span class="f-24 f-md-42 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                     <div class="timer-label text-center timer-mrgn"><span class="f-24 f-md-42 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                     <div class="timer-label text-center "><span class="f-24 f-md-42 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                  </div> -->
         </div>
         </div>
      </div>
   </div>
</div>
<script type="text/javascript">
    var _ouibounce = ouibounce(document.getElementById('ouibounce-modal'),{
    aggressive: true, //Making this true makes ouibounce not to obey "once per visitor" rule
    });
</script>
<!-- <script type="text/javascript">
   var noob = $('.countdown').length;
   var stimeInSecs;
   var tickers;

   function timerBegin(seconds) {   
      if(seconds>=0)  {
         stimeInSecs = parseInt(seconds);           
         showRemaining();
      }
      //tickers = setInterval("showRemaining()", 1000);
   }

   function showRemaining() {

      var seconds = stimeInSecs;
         
      if (seconds > 0) {         
         stimeInSecs--;       
      } else {        
         clearInterval(tickers);       
         //timerBegin(59 * 60); 
      }

      var days = Math.floor(seconds / 86400);

      seconds %= 86400;
      
      var hours = Math.floor(seconds / 3600);
      
      seconds %= 3600;
      
      var minutes = Math.floor(seconds / 60);
      
      seconds %= 60;


      if (days < 10) {
         days = "0" + days;
      }
      if (hours < 10) {
         hours = "0" + hours;
      }
      if (minutes < 10) {
         minutes = "0" + minutes;
      }
      if (seconds < 10) {
         seconds = "0" + seconds;
      }
      var i;
      var countdown = document.getElementsByClassName('countdown');

      for (i = 0; i < noob; i++) {
         countdown[i].innerHTML = '';
      
         if (days) {
            countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-24 f-md-42 timerbg oswald">' + days + '</span><br><span class="f-14 f-md-14 w400 smmltd">Days</span> </div>';
         }
      
         countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-24 f-md-42 timerbg oswald">' + hours + '</span><br><span class="f-14 f-md-14 w400 smmltd">Hours</span> </div>';
      
         countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"><span class="f-24 f-md-42 timerbg oswald">' + minutes + '</span><br><span class="f-14 f-md-14 w400 smmltd">Mins</span> </div>';
      
         countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-24 f-md-42 timerbg oswald">' + seconds + '</span><br><span class="f-14 f-md-14 w400 smmltd">Sec</span> </div>';
      }
   
   }

const givenTimestamp = 'Mon Dec 4 2023 20:30:00 GMT+0530'
const durationTime = 60;

const date = new Date(givenTimestamp);
const givenTime = date.getTime() / 1000;
const currentTime = new Date().getTime()



let cnt;
const matchInterval = setInterval(() => {
   if(Math.round(givenTime) === Math.round(new Date().getTime() /1000)){
   cnt = durationTime * 60;
   counter();
   clearInterval(matchInterval);
   }else if(Math.round(givenTime) < Math.round(new Date().getTime() /1000)){
      const timeGap = Math.round(new Date().getTime() /1000) - Math.round(givenTime)
      let difference = (durationTime * 60) - timeGap;
      if(difference >= 0){
         cnt = difference
         localStorage.mavasTimer = cnt;

      }else{
         
         difference = difference % (durationTime * 60);
         cnt = (durationTime * 60) + difference
         localStorage.mavasTimer = cnt;

      }

      counter();
      clearInterval(matchInterval);
   }
},1000)





function counter(){
   if(parseInt(localStorage.mavasTimer) > 0){
      cnt = parseInt(localStorage.mavasTimer);
   }
   cnt -= 1;
   localStorage.mavasTimer = cnt;
   if(localStorage.mavasTimer !== NaN){
      timerBegin(parseInt(localStorage.mavasTimer));
   }
   if(cnt>0){
      setTimeout(counter,1000);
  }else if(cnt === 0){
   cnt = durationTime * 60 
   counter()
  }
}

</script> -->
</body>
</html>
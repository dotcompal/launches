<!Doctype html>
<html>

<head>
    <title>LearnX Pro</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=9">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta name="title" content="LearnX | Pro">
    <meta name="description" content="LearnX">
    <meta name="keywords" content="LearnX">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta property="og:image" content="https://www.getlearnx.com/pro/thumbnail.png">
    <meta name="language" content="English">
    <meta name="revisit-after" content="1 days">
    <meta name="author" content="Pranshu Gupta">
    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:title" content="LearnX | Pro">
    <meta property="og:description" content="LearnX">
    <meta property="og:image" content="https://www.getlearnx.com/pro/thumbnail.png">
    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:title" content="LearnX | Pro">
    <meta property="twitter:description" content="LearnX">
    <meta property="twitter:image" content="https://www.getlearnx.com/pro/thumbnail.png">
    <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&family=Poppins:wght@300;400;500;600;700;800;900&family=Red+Hat+Display:wght@300;400;500;600;700;800;900&display=swap" rel="stylesheet">
    <!-- Start Editor required -->
    <link rel="stylesheet" href="https://cdn.oppyotest.com/launches/sellero/common_assets/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="assets/css/style.css" type="text/css">
    <link rel="stylesheet" href="assets/css/timer.css" type="text/css">
    <script src="https://cdn.oppyotest.com/launches/sellero/common_assets/js/bootstrap.min.js"></script>
    <script src="https://cdn.oppyotest.com/launches/sellero/common_assets/js/jquery.min.js"></script>
   


</head>

<body>
     <!--1. Header Section Start -->
     <div class="warning-section">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <div class="offer">
                        <div>
                            <img src="https://cdn.oppyo.com/launches/appzilo/special/error.webp">
                        </div>
                        <div class="f-22 f-md-30 w500 lh140 white-clr text-center">
                            <span class="w600">Warning:</span> This offer will expire automatically once you leave the page
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header-section">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 484.58 200.84" style="max-height:75px;">
                        <defs>
                            <style>
                                .cls-1,.cls-4{fill:#fff;}
                                .cls-2,.cls-3,.cls-4{fill-rule:evenodd;}
                                .cls-2{fill:url(#linear-gradient);}
                                .cls-3{fill:url(#linear-gradient-2);}
                                .cls-5{fill:url(#linear-gradient-3);}
                            </style>
                            <linearGradient id="linear-gradient" x1="-154.67" y1="191.86" x2="249.18" y2="191.86" gradientTransform="matrix(1.19, -0.01, 0.05, 1, 178.46, -4.31)" gradientUnits="userSpaceOnUse">
                                <stop offset="0" stop-color="#ead40c"></stop>
                                <stop offset="1" stop-color="#ff00e9"></stop>
                            </linearGradient>
                            <linearGradient id="linear-gradient-2" x1="23.11" y1="160.56" x2="57.61" y2="160.56" gradientTransform="matrix(1, 0, 0, 1, 0, 0)" xlink:href="#linear-gradient"></linearGradient>
                            <linearGradient id="linear-gradient-3" x1="384.49" y1="98.33" x2="483.38" y2="98.33" gradientTransform="matrix(1, 0, 0, 1, 0, 0)" xlink:href="#linear-gradient"></linearGradient>
                        </defs>
                        <g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1">
                            <path class="cls-1" d="M112.31,154.3q-11.65,0-20.1-4.87a32.56,32.56,0,0,1-13-13.82q-4.53-9-4.54-21.11a46.06,46.06,0,0,1,4.57-21A34.29,34.29,0,0,1,92,79.37a36.11,36.11,0,0,1,19.31-5.06,39.65,39.65,0,0,1,13.54,2.29,31,31,0,0,1,11.3,7.09,33.32,33.32,0,0,1,7.75,12.18,49.23,49.23,0,0,1,2.82,17.57V119H83.26v-12.3h46A19.87,19.87,0,0,0,127,97.38a16.6,16.6,0,0,0-6.18-6.48,17.59,17.59,0,0,0-9.21-2.37,17.88,17.88,0,0,0-9.83,2.7,18.81,18.81,0,0,0-6.58,7.06,20.17,20.17,0,0,0-2.4,9.56v10.74a25.15,25.15,0,0,0,2.47,11.57,17.42,17.42,0,0,0,6.91,7.37,20.52,20.52,0,0,0,10.39,2.55,21.62,21.62,0,0,0,7.22-1.14,15.71,15.71,0,0,0,5.59-3.35,14.11,14.11,0,0,0,3.59-5.5L146,132a26.44,26.44,0,0,1-6.13,11.77,29.72,29.72,0,0,1-11.52,7.77A43.74,43.74,0,0,1,112.31,154.3Z"></path>
                            <path class="cls-1" d="M184.49,154.35a31.78,31.78,0,0,1-13.24-2.65,21.26,21.26,0,0,1-9.28-7.84,22.86,22.86,0,0,1-3.41-12.81A21.92,21.92,0,0,1,161,120.2a18.9,18.9,0,0,1,6.61-6.86,33.85,33.85,0,0,1,9.46-3.9,77.76,77.76,0,0,1,10.92-2q6.81-.7,11-1.28a15.91,15.91,0,0,0,6.18-1.82,4.25,4.25,0,0,0,1.94-3.86v-.3q0-5.7-3.38-8.83T194,88.28q-6.71,0-10.62,2.92a14.55,14.55,0,0,0-5.27,6.91l-17-2.42a27.51,27.51,0,0,1,6.66-11.83,29.46,29.46,0,0,1,11.35-7.16,43.73,43.73,0,0,1,14.83-2.39,47.89,47.89,0,0,1,11.14,1.31,31.6,31.6,0,0,1,10.14,4.31,22.12,22.12,0,0,1,7.39,8.14q2.81,5.15,2.8,12.87v51.85H207.84V142.14h-.61a22.1,22.1,0,0,1-4.66,6,22.35,22.35,0,0,1-7.52,4.49A30,30,0,0,1,184.49,154.35Zm4.74-13.42a19.7,19.7,0,0,0,9.53-2.19,16,16,0,0,0,6.23-5.83,15,15,0,0,0,2.19-7.91v-9.13a8.72,8.72,0,0,1-2.9,1.31,45.56,45.56,0,0,1-4.56,1.06c-1.68.3-3.35.57-5,.8l-4.29.61a32,32,0,0,0-7.32,1.81A12.29,12.29,0,0,0,178,125a9.76,9.76,0,0,0,1.82,13.39A16,16,0,0,0,189.23,140.93Z"></path>
                            <path class="cls-1" d="M243.8,152.79V75.31h17.7V88.23h.81a19.36,19.36,0,0,1,7.29-10.37,20,20,0,0,1,11.83-3.66c1,0,2.14,0,3.4.13a23.83,23.83,0,0,1,3.15.38V91.5a21,21,0,0,0-3.65-.73,38.61,38.61,0,0,0-4.82-.32,18.47,18.47,0,0,0-8.95,2.14,15.94,15.94,0,0,0-6.23,5.93,16.55,16.55,0,0,0-2.27,8.72v45.55Z"></path>
                            <path class="cls-1" d="M318.4,107.39v45.4H300.14V75.31h17.45V88.48h.91a22,22,0,0,1,8.55-10.34q5.86-3.84,14.55-3.83a27.58,27.58,0,0,1,14,3.43,23.19,23.19,0,0,1,9.28,9.93,34.22,34.22,0,0,1,3.26,15.79v49.33H349.87V106.28c0-5.17-1.34-9.23-4-12.15s-6.37-4.39-11.07-4.39a17,17,0,0,0-8.5,2.09,14.67,14.67,0,0,0-5.8,6A20,20,0,0,0,318.4,107.39Z"></path>
                            <path class="cls-2" d="M314.05,172.88c12.69-.14,27.32,2.53,40.82,2.61l-.09,1c1.07.15,1-.88,2-.78,1.42-.18,0,1.57,1.82,1.14.54-.06.08-.56-.32-.68.77,0,2.31.17,3,1.26,1.3,0-.9-1.34.85-.89,6,1.63,13.39,0,20,3.2,11.64.72,24.79,2.38,38,4.27,2.85.41,5.37,1.11,7.57,1.05,2-.05,5.68.78,8,1.08,2.53.34,5.16,1.56,7.13,1.65-1.18-.05,5.49-.78,4.54.76,5-.72,10.9,1.67,15.94,1.84.83-.07.69.53.67,1,1.23-1.17,3.32.56,4.28-.56.25,2,2.63-.48,3.3,1.61,2.23-1.39,4.83.74,7.55,1.37,1.91.44,5.29-.21,5.55,2.14-2.1-.13-5.12.81-11.41-1.09,1,.87-3.35.74-3.39-.64-1.81,1.94-6.28-1-7.79,1.19-1.26-.41,0-.72-.64-1.35-5,.55-8.09-1.33-12.91-1.56,2.57,2.44,6.08,3.16,10.06,3.22,1.28.15,0,.74,1,1.39,1.37-.19.75-.48,1.26-1.17,5.37,2.28,15.36,2.35,21,4.91-4.69-.63-11.38,0-15.15-2.09A148,148,0,0,1,441.58,196c-.53-.1-1.81.12-.7-.71-1.65.73-3.41.1-5.74-.22-37.15-5.15-80.47-7.78-115.75-9.76-39.54-2.22-76.79-3.22-116.44-2.72-61.62.78-119.59,7.93-175.21,13.95-5,.55-10,1.49-15.11,1.47-1.33-.32-2.46-1.91-1.25-3-2-.38-2.93.29-5-.15a9.83,9.83,0,0,1-1.25-3,13.73,13.73,0,0,1,6.42-2.94c.77-.54.12-.8.15-1.6a171,171,0,0,1,45.35-8.58c26.43-1.1,53.68-4.73,79.64-6,6.25-.3,11.72.06,18.41.14,8.31.1,19.09-1,29.19-.12,6.25.54,13.67-.55,21.16-.56,39.35-.09,71.16-.23,112.28,2C317.24,171.93,315.11,173.81,314.05,172.88Zm64.22,16.05-1.6-.2C376.2,190,378.42,190.26,378.27,188.93Z"></path>
                            <polygon class="cls-3" points="52.57 166.06 57.61 151.99 23.11 157.23 32.36 169.12 52.57 166.06"></polygon>
                            <polygon class="cls-4" points="44.69 183.24 51.75 168.35 33.86 171.06 44.69 183.24"></polygon>
                            <path class="cls-4" d="M.12,10.15l22,145.06,35.47-5.39-22-145a2.6,2.6,0,0,0,0-.4C35,.76,26.62-.95,16.81.54S-.52,6.16,0,9.77A2.62,2.62,0,0,0,.12,10.15Z"></path>
                            <path class="cls-5" d="M433.68,79.45l21-36.33h27.56L448.48,97.51l34.9,56H456l-22-37.76-21.94,37.76H384.49l34.9-56L385.72,43.12h27.35Z"></path>
                        </g>
                            </g>
                    </svg>
                </div>
                <div class="col-12 text-center lh150 mt20 mt-md50">
                    <div class="pre-heading f-18 f-md-22 w600 lh140 white-clr">
                    Congratulations! Your order is almost complete. But before you get started...
                </div>
            </div>
            <div class="col-12 mt-md30 mt20 f-md-50">
                <div class="main-heading f-28 f-md-40 w400 text-center lh140 black2-clr red-hat-font">
                Unlock <span class="w800 under pink-clr">Unlimited Potential & Get 10X More Profits Faster & Easier</span> in Just 1-Click Using This Game-Changing Pro Upgrade
                </div>
            </div>
                <div class="col-12 mt-md25 mt20">
                    <div class="f-20 f-md-22 w500 lh140 white-clr text-center">
                    Build Unlimited Academies, Add Unlimited Custom Domains, Create Unlimited Courses, Bring Unlimited Website Visitors, Collect Unlimited Leads, Host Unlimited Video Trainings & Files With Unlimited Downloading/Uploading Bandwidth and 20+ Pro Features
                </div>
            </div>
                <div class="col-12 col-md-10 mx-auto mt-md30 mt20">
                    <img src="assets/images/productbox.webp" class="img-fluid d-block mx-auto">
                    <!-- <div class="responsive-video">
                        <iframe src="https://coursesify.dotcompal.com/video/embed/iuthhxjs8d" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                    </div> -->
                </div>
                <div class="col-12 mt30 mt-md50  f-md-24 f-20 lh140 w400 text-center white-clr">
                    This is An Exclusive Deal for <span class="w600 yellow-clr">"LearnX"</span> Users Only...
                </div>
                <div class="col-md-10 mx-auto col-12 mt15 mt-md20 f-20 f-md-35 text-center probtn1">
                    <a href="#buynow" class="text-center">Get Instant Access to LearnX Pro</a>
                </div>
                
            </div>
        </div>
    </div>
    <!--1. Header Section End -->
    <!--2. Second Section Start -->
    <div class="second-sec">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center text-capitalize f-20 f-md-22 text-center w500 lh140">
                    You Are About To Witness The RAW Power Of LearnX In A Way You Have Never Seen It.
                </div>
                <div class="col-12 mt15 mt-md15 f-28 f-md-45 w500 lh140 text-capitalize text-center black-clr">
                    A Whopping 92% Of The <span class="w700">LearnX Members </span> Upgraded To LearnX Pro... Here's Why
                </div>
                <div class="col-12">
                    <div class="row gx-md-5">
                        <div class="col-md-6 col-12">
                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-top text-center text-md-start mt20 mt-md50">
                                <div><img src="assets/images/fe1.png" class="img-fluid d-block mx-auto"></div>
                                <div class="f-18 f-md-20 lh150 w400 ml-md20 mt20 mt-md0 text-capitalize w-100">
                                    <span class="w600">
                                 Break Free & Go Limitless-  </span>Build Unlimited Academies, Add Unlimited Custom Domains, Create Unlimited Courses, Bring Unlimited Website Visitors, Collect Unlimited Leads, Host
                                    Unlimited Video Trainings & Files With Unlimited Downloading/Uploading Bandwidth.
                                </div>
                            </div>
                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-top text-center text-md-start mt20 mt-md50">
                                <div><img src="assets/images/fe2.png" class="img-fluid d-block mx-auto"></div>
                                <div class="f-18 f-md-20 lh150 w400 ml-md20 mt20 mt-md0 text-capitalize w-100">
                                    Run All Your & Your Client’s Academies On Your Own Domains <span class="w600">With UNLIMITED Custom Domains</span>
                                </div>
                            </div>

                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-center text-center text-md-start mt20 mt-md50">
                                <div><img src="assets/images/fe3.png" class="img-fluid d-block mx-auto"></div>
                                <div class="f-18 f-md-20 lh150 w400 ml-md20 mt20 mt-md0 text-capitalize w-100">
                                    <span class="w600">Get UNLIMITED Website Visitors  </span> on your website, marketplace or blog views
                                </div>
                            </div>

                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-center text-center text-md-start mt20 mt-md50">
                                <div><img src="assets/images/fe4.png" class="img-fluid d-block mx-auto"></div>
                                <div class="f-18 f-md-20 lh150 w400 ml-md20 mt20 mt-md0 text-capitalize w-100">
                                    <span class="w600">Host & Deliver UNLIMITED Video Trainings & Files   </span>To Your Pro Academies
                                </div>
                            </div>

                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-top text-center text-md-start mt20 mt-md50">
                                <div><img src="assets/images/fe5.png" class="img-fluid d-block mx-auto"></div>
                                <div class="f-18 f-md-20 lh150 w400 ml-md20 mt20 mt-md0 text-capitalize w-100">
                                    Get MyDrive By Oppyo To <span class="w600">Host, Manage & Deliver Unlimited Media Content,</span> Video Trainings & All Your Marketing Files With 100% Security
                                </div>
                            </div>

                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-center text-center text-md-start mt20 mt-md50">
                                <div><img src="assets/images/fe6.png" class="img-fluid d-block mx-auto"></div>
                                <div class="f-18 f-md-20 lh150 w400 ml-md20 mt20 mt-md0 text-capitalize w-100">
                                    <span class="w600">Customise your Academy Sites With 20 Unique Color Themes  </span>
                                </div>
                            </div>


                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-top text-center text-md-start mt20 mt-md50">
                                <div><img src="assets/images/fe7.png" class="img-fluid d-block mx-auto"></div>
                                <div class="f-18 f-md-20 lh150 w400 ml-md20 mt20 mt-md0 text-capitalize w-100">
                                    <span class="w600">Get 6 beautiful color templates of all the sales pages;   </span> Affiliate Page& Thank You Pages For Our DFY Video Courses
                                </div>
                            </div>



                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-center text-center text-md-start mt20 mt-md50">
                                <div><img src="assets/images/fe8.png" class="img-fluid d-block mx-auto"></div>
                                <div class="f-18 f-md-20 lh150 w400 ml-md20 mt20 mt-md0 text-capitalize w-100">
                                    <span class="w600">Complete Set Of Professional Graphics For Your DFY Courses </span> to use for consistent branding-
                                </div>
                            </div>



                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-center text-center text-md-start mt20 mt-md50">
                                <div><img src="assets/images/fe9.png" class="img-fluid d-block mx-auto"></div>
                                <div class="f-18 f-md-20 lh150 w400 ml-md20 mt20 mt-md0 text-capitalize w-100">
                                    <span class="w600">Get 10K+ FREE Stock Images   </span> For Your Products, Marketplace Or Social Media Posts
                                </div>
                            </div>

                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-center text-center text-md-start mt20 mt-md50">
                                <div><img src="assets/images/fe10.png" class="img-fluid d-block mx-auto"></div>
                                <div class="f-18 f-md-20 lh150 w400 ml-md20 mt20 mt-md0 text-capitalize w-100">
                                    <span class="w600">Boost Relations & Conversions With Your Students   </span> Using CRM Integrations
                                </div>
                            </div>


                        </div>
                        <div class="col-md-6 col-12">

                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-top text-center text-md-start mt20 mt-md50">
                                <div><img src="assets/images/fe11.png" class="img-fluid d-block mx-auto"></div>
                                <div class="f-18 f-md-20 lh150 w400 ml-md20 mt20 mt-md0 text-capitalize w-100">
                                    <span class="w600">Build UNLIMITED Pro Level Academies</span> to spread the power of your knowledge and get your students spell bound.
                                </div>
                            </div>

                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-center text-center text-md-start mt20 mt-md50">
                                <div><img src="assets/images/fe12.png" class="img-fluid d-block mx-auto"></div>
                                <div class="f-18 f-md-20 lh150 w400 ml-md20 mt20 mt-md0 text-capitalize w-100">
                                    <span class="w600">Create UNLIMITED courses    </span>for each of your academies
                                </div>
                            </div>

                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-center text-center text-md-start mt20 mt-md50">
                                <div><img src="assets/images/fe13.png" class="img-fluid d-block mx-auto"></div>
                                <div class="f-18 f-md-20 lh150 w400 ml-md20 mt20 mt-md0 text-capitalize w-100">
                                    Maximize Lead Generation - <span class="w600"> Capture UNLIMITED Leads </span>
                                </div>
                            </div>

                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-center text-center text-md-start mt20 mt-md50">
                                <div><img src="assets/images/fe14.png" class="img-fluid d-block mx-auto"></div>
                                <div class="f-18 f-md-20 lh150 w400 ml-md20 mt20 mt-md0 text-capitalize w-100">
                                    <span class="w600">Get UNLIMITED bandwidth   </span> To Give Best User Experience
                                </div>
                            </div>

                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-center text-center text-md-start mt20 mt-md50">
                                <div><img src="assets/images/fe15.png" class="img-fluid d-block mx-auto"></div>
                                <div class="f-18 f-md-20 lh150 w400 ml-md20 mt20 mt-md0 text-capitalize w-100">
                                    <span class="w600">Multiply Your Leads With 30 Lead Generation Popup Templates</span>
                                </div>
                            </div>

                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-center text-center text-md-start mt20 mt-md50">
                                <div><img src="assets/images/fe16.png" class="img-fluid d-block mx-auto"></div>
                                <div class="f-18 f-md-20 lh150 w400 ml-md20 mt20 mt-md0 text-capitalize w-100">
                                    <span class="w600">Get Done for you follow up emails   </span> To Multiply Sales & Profits
                                </div>
                            </div>

                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-center text-center text-md-start mt20 mt-md50">
                                <div><img src="assets/images/fe17.png" class="img-fluid d-block mx-auto"></div>
                                <div class="f-18 f-md-20 lh150 w400 ml-md20 mt20 mt-md0 text-capitalize w-100">
                                    <span class="w600">Media Library To Manage All Your Images In One Place</span>
                                </div>
                            </div>

                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-center text-center text-md-start mt20 mt-md50">
                                <div><img src="assets/images/fe18.png" class="img-fluid d-block mx-auto"></div>
                                <div class="f-18 f-md-20 lh150 w400 ml-md20 mt20 mt-md0 text-capitalize w-100">
                                    <span class="w600">Advance And Deep Analytics  </span> To Track Your Growth And Scale It Further
                                </div>
                            </div>


                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-center text-center text-md-start mt20 mt-md50">
                                <div><img src="assets/images/fe19.png" class="img-fluid d-block mx-auto"></div>
                                <div class="f-18 f-md-20 lh150 w400 ml-md20 mt20 mt-md0 text-capitalize w-100">
                                    <span class="w600">Register Your Students Directly For Your Webinars </span> With Webinar Platform Integrations
                                </div>
                            </div>

                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-center text-center text-md-start mt20 mt-md50">
                                <div><img src="assets/images/fe20.png" class="img-fluid d-block mx-auto"></div>
                                <div class="f-18 f-md-20 lh150 w400 ml-md20 mt20 mt-md0 text-capitalize w-100">
                                    <span class="w600">Complete Team Management With Rights Control – Upto 10 Team members </span>
                                </div>
                            </div>

                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-center text-center text-md-start mt20 mt-md50">
                                <div><img src="assets/images/fe21.png" class="img-fluid d-block mx-auto"></div>
                                <div class="f-18 f-md-20 lh150 w400 ml-md20 mt20 mt-md0 text-capitalize w-100">
                                    Get all these benefits <span class="w600">At An Unparalleled Price</span>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
                <div class="col-md-10 mx-auto col-12 mt10 mt-md60 f-22 f-md-36 text-center probtn1">
                    <a href="#buynow" class="text-center">Get Instant Access to LearnX Pro</a>
                </div>
            </div>
        </div>
    </div>
    <!--2. Second Section End -->

    <!-- checkout-section start -->
    <div class="chackout">
      <div class="container">
          <div class="row"> 
              <div class="col-12 f-35 f-md-46 w600 text-center white-clr ">
              Checkout Real Results That Our Customers Got By Upgrading To Pro Today…
              </div> 
          </div>
          <div class="row mt20 mt-md40">
              <div class="col-12">
                  <img src="assets/images/income.webp" class="img-fluid" alt="">
              </div> 
          </div>         
      </div>               
  </div>
    <!-- checkout section end -->
    <!--3. Third Section Start -->
    <div class="third-section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="f-28 f-md-45 lh140 w500 text-center black-clr">
                        This Is Your Chance to <span class="w700">Get Unfair Advantage </span> Over Your Competition
                    </div>
                    <div class="f-18 f-md-21 w400 mt20 mt-md35 lh160 text-center">
                        If you are on this page, you have already realized the power of this revolutionary technology and secured a leap from the competitors. If a basic feature that everyone is getting can bring such massive results…
                        <br><br>
                        <span class="w600">what more this pro-upgrade can bring on the table with the power to Build UNLIMITED Academies, add UNLIMITED Custom Domains, create UNLIMITED Courses, bring UNLIMITED website visitors, collect UNLIMITED leads, host UNLIMITED video trainings & files with UNLIMITED downloading/uploading Bandwidth and 20+ Pro Features</span>
                        <br>
                        <br> I know you're probably very eager to get to your members area and <span class="w600">start using LearnX and start getting more sales and commissions from your courses & funnels. However, if you can give me a few
                        minutes I'll show you how to take this system beyond basic features using it to skyrocket your profit.
                        <br><br>
                        I know you're probably very eager to get to your members area and<span class="w600"> start using LearnX and start getting more sales and commissions from your campaigns</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--3. Third Section End -->
    <!--4. Fourth Section Starts -->
    <div class="fifth-section">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <div class="f-28 f-md-38 w700 white-clr lh140 presenting-head">
                       Proudly Presenting…
                    </div>
                </div>
                <div class="col-12 mt20 mt-md40">
                    <div class="f-22 f-md-36 text-center white-clr lh140 w700">
                        The <span class="yellow-clr">Pro Features Top Marketers</span> Demand & <br class="d-none d-md-block">Need Are Now Available At A Fraction Of The Cost
                    </div>
                </div>
                <div class="col-12  col-md-10 mx-auto mt20 mt-md40">
                    <img src="assets/images/productbox.webp" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-md-10 mx-auto col-12 mt30 mt-md65 f-18 f-md-36 text-center probtn1">
                    <a href="#buynow" class="text-center">Get Instant Access to LearnX Pro</a>
                </div>
            </div>
        </div>
    </div>
    <!--4. Fourth Section End -->
    <div class="unfair-section">
        <div class="container">
            <div class="row">
                <div class="col-12 f-28 f-md-50 lh140 w500 text-center black-clr">
                    Here’s What You’re Getting with <br class="hidden-xs"><span class="w700">This Unfair Advantage
                  Upgrade</span>
                </div>
                <div class="col-12 f-24 f-md-30 w500 lh140 mt20 text-center text-md-start">
                    Go Limitless As You’re Getting Unlimited Everything-
                </div>
                <div class="col-12 mt-md40 mt20">
                    <div class="row">
                        <div class="col-12 col-md-3 mt20 mt-md30">
                            <img src="assets/images/ua1.png" class="img-fluid d-block mx-auto">
                        </div>
                        <div class="col-12 col-md-3 mt20 mt-md27">
                            <img src="assets/images/ua2.png" class="img-fluid d-block mx-auto">
                        </div>
                        <div class="col-12 col-md-3 mt20 mt-md30">
                            <img src="assets/images/ua3.png" class="img-fluid d-block mx-auto">
                        </div>
                        <div class="col-12 col-md-3 mt20 mt-md30">
                            <img src="assets/images/ua4.png" class="img-fluid d-block mx-auto">
                        </div>
                    </div>
                </div>
                <div class="col-12 mt20 mt-md80">
                    <div class="row">
                        <div class="col-12 col-md-4 mt20 mt-md30">
                            <img src="assets/images/ua5.png" class="img-fluid d-block mx-auto">
                        </div>
                        <div class="col-12 col-md-4 mt20 mt-md30">
                            <img src="assets/images/ua6.png" class="img-fluid d-block mx-auto">
                        </div>
                        <div class="col-12 col-md-4 mt20 mt-md27">
                            <img src="assets/images/ua7.png" class="img-fluid d-block mx-auto">
                        </div>
                    </div>
                </div>
                <div class="col-12 f-18 f-md-20 w400 lh140 mt50 mt-md80 text-left">
                    No more worrying about anything and focus on growing your business leaps and bounds.
                </div>
                <div class="col-12 f-18 f-md-20 w400 lh140 mt20 mt-md30 text-left">
                    You’ll have NO LIMITS to what you can do and take your business to the next level. LearnX Pro upgrade enables you stand out of the crowd, and the best part is that <span class="w600">you can go limited for one-time price but ONLY for limited time.</span>
                </div>
            </div>
        </div>
    </div>
    <!-- feature1 -->
    <div class="white-section">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12">
                    <img src="../common_assets/images/1.webp" class="img-fluid">
                    <div class="f-24 f-md-36 w500 lh140 mt10 black-clr">
                        <span class="w500">Build UNLIMITED Pro Level Academies </span> to spread the power of your knowledge and get your students spell bound.
                    </div>
                    <div class="col-12 col-md-10 mx-auto mt20 mt-md30">
                        <img src="assets/images/f1.png" class="img-fluid d-block mx-auto">
                    </div>
                    <div class="col-12 f-18 f-md-20 w400 lh140 mt20 mt-md50">
                        Create a separate academy for each niche, business, or your clients. <span class="w600">You have NO limits on what you create and make with LearnX Pro.</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--feature2 -->
    <div class="grey-section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <img src="../common_assets/images/2.webp" class="img-fluid">
                    <div class="f-24 f-md-36 w500 lh140 mt10  black-clr">
                        Run All Your & Your Client’s Academies On Your Own Domains With UNLIMITED Custom Domains
                    </div>
                    <div class="col-12 col-md-10 mx-auto mt20 mt-md30">
                        <img src="assets/images/f2.png" class="img-fluid d-block mx-auto">
                    </div>
                    <div class="col-12 f-18 f-md-20 w400 lh140 mt20 mt-md50">
                        It will instantly build your authority and credibility by showing your pro academies on your own domain with your own LOGO. <span class="w600">It also gives you better exposure in search engines.</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--feature3 -->
    <div class="white-section">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12">
                    <img src="../common_assets/images/3.webp" class="img-fluid">
                    <div class="f-24 f-md-36 w500 lh140 mt10  black-clr">
                        Create UNLIMITED courses for each of your academies
                    </div>
                    <div class="col-12 col-md-10 mx-auto mt20 mt-md30">
                        <img src="assets/images/f3.png" class="img-fluid d-block mx-auto">
                    </div>
                    <div class="col-12 f-18 f-md-20 w400 lh140 mt20 mt-md50">
                        Yeah, there is no limit at all. Pro edition gives you the power to <span class="w600">create unlimited courses with unlimited lessons in any niche for any audience</span> in a hassle free manner
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--feature3 -->
    <!--feature4 -->
    <div class="grey-section">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12">
                    <div>
                        <img src="../common_assets/images/4.webp" class="img-fluid">
                    </div>
                    <div class="f-24 f-md-36 w500 lh140 mt10  black-clr">
                        Get UNLIMITED website visitors on your website, marketplace or blog views
                    </div>
                    <div class="col-12 col-md-10 mx-auto mt20 mt-md30">
                        <img src="assets/images/f4.png" class="img-fluid d-block mx-auto">
                    </div>
                    <div class="col-12 f-18 f-md-20 w400 lh140 mt20 mt-md50">
                        There are absolutely no limits on your website, marketplace or blog views and number of visitors. <span class="w600">So, sky is the only limit for your growth online.</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--feature5 -->
    <div class="white-section">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12">
                    <img src="../common_assets/images/5.webp" class="img-fluid">
                    <div class="f-24 f-md-36 w500 lh140 mt10  black-clr">
                        Maximize Lead Generation - Capture UNLIMITED Leads
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md30">
                    <img src="assets/images/f5.png" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 f-18 f-md-20 w400 lh140 mt20 mt-md50">
                    When someone grab your FREE courses on your marketplace, grab reports on lead pages and when users subscribe to your academy using social media. <span class="w600">You also can manage contacts easily in one central dashboard.</span>
                </div>
            </div>
        </div>
    </div>
    <!--feature6 -->
    <div class="grey-section">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12">
                    <img src="../common_assets/images/6.webp" class="img-fluid">
                    <div class="f-24 f-md-36 w500 lh140 mt10  black-clr">
                        <span class="w500">Host & Deliver UNLIMITED Video Trainings & Files</span> To Your Pro Academies.
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md30">
                    <img src="assets/images/f6.png" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 f-18 f-md-20 w400 lh140 mt20 mt-md50">
                    LearnX Pro enables you add Unlimited videos and images to your pro academies so as to <span class="w600">make the most of your knowledge & get your students completely amazed.</span>
                </div>
                <div class="col-12 f-24 f-md-36 w400 lh140 mt20 mt-md50 text-center">
                    Going unlimited alone is of <span class="w600">HUGE VALUE</span> for your business <span class="w600">but that’s not all.</span> We’ve ensured you get everything to <span class="w600">get succeed with your e-learning business.  </span>
                </div>
            </div>
        </div>
    </div>
    <!--feature7 -->
    <div class="white-section">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12">
                    <img src="../common_assets/images/7.webp" class="img-fluid">
                    <div class="f-24 f-md-36 w500 lh140 mt10  black-clr">
                        <span class="w500">Get UNLIMITED Bandwidth</span> To Give Best User Experience
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md30">
                    <img src="assets/images/f7.png" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 f-18 f-md-20 w400 lh140 mt20 mt-md50">
                    We mean every word when we say LIMITLESS. So with the Pro edition, you get <span class="w600">UNLIMITED bandwidth to give best user experience without getting worried of any limitations of file size, rich media content and content uploading and downloading bandwidth.</span>
                </div>
            </div>
        </div>
    </div>
    <div class="grey-section">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12">
                    <img src="../common_assets/images/8.webp" class="img-fluid">
                    <div class="f-24 f-md-36 w500 lh140 mt10  black-clr">
                        <span class="w600">Get MyDrive By Oppyo To </span>Host, Manage & Deliver Unlimited Media Content, Video Trainings & All Your Marketing Files With 100% Security
                    </div>
                </div>
                <div class="col-12 mt-md60 mt20">
                    <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-12 col-md-6 f-18 f-md-20 w400 lh140">
                            <span class="w600">It works seamlessly with your LearnX to add videos to your courses quickly & smoothly.</span>
                        </div>
                        <div class="col-12 col-md-6 mt20 mt-md0">
                            <img src="assets/images/f8a.png" class="img-fluid d-block mx-auto">
                        </div>
                    </div>
                </div>
                <div class="col-12 mt-md60 mt20">
                    <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-12 col-md-6 order-md-2 f-18 f-md-20 w400 lh140">
                            <span class="w600">Create Unlimited Folders and their sub-folders. </span> Even share them with your students, customers, clients or team members.
                        </div>
                        <div class="col-12 col-md-6 order-md-1 mt20 mt-md0">
                            <img src="assets/images/f8b.png" class="img-fluid d-block mx-auto">
                        </div>
                    </div>
                </div>
                <div class="col-12 mt-md60 mt20">
                    <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-12 col-md-6 f-18 f-md-20 w400 lh140">
                            Fetch & sync valuable data of your own and your clients effortlessly <span class="w600">with Google Drive, One Drive & Dropbox Integration</span>
                        </div>
                        <div class="col-12 col-md-6 mt20 mt-md0">
                            <img src="assets/images/f8c.png" class="img-fluid d-block mx-auto">
                        </div>
                    </div>
                </div>
                <div class="col-12 mt-md60 mt20">
                    <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-12 col-md-6 order-md-2 f-18 f-md-20 w400 lh140">
                            <span class="w600">It comes with Unbreakable File Security with SSL & OTP Enabled Login</span>
                        </div>
                        <div class="col-12 col-md-6 order-md-1 mt20 mt-md0">
                            <img src="assets/images/f8d.png" class="img-fluid d-block mx-auto">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- feature9 -->
    <div class="white-section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <img src="../common_assets/images/9.webp" class="img-fluid">
                    <div class="f-24 f-md-36 w500 lh140 mt10  black-clr">
                        Give your Academy site a Pro level Look & Feel - Customise your Academy Sites with 20 unique colour themes-
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md30">
                    <img src="assets/images/f9.png" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 f-18 f-md-20 w400 lh140 mt20 mt-md50">
                    Make your Academy website including blog, marketplace & membership site <span class="w600">appear more elegant & appealing for your target audience to enhance the visitor engagement.</span>
                    <br><br> No need to rush into website designers and paying them every time you want to alter your site aesthetics. Simply choose and give it a completely fresh look & feel.
                </div>
            </div>
        </div>
    </div>
    <!-- feature10 -->
    <div class="grey-section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <img src="../common_assets/images/10.webp" class="img-fluid">
                    <div class="f-24 f-md-36 w500 lh140 mt10  black-clr">
                        Multiply Your Leads With 30 Lead Generation Popup Templates
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md30">
                    <img src="assets/images/f10.png" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 f-18 f-md-20 w400 lh140 mt20 mt-md50">
                    These are specially designed to make you the most from your website traffic. <span class="w600">Just add your free stuff to a beautiful popup and you are good to grab your targeted leads.</span>
                    <br><br>
                    <span class="w600">And we are not done yet</span> as there are many other features are there in this upgrade for you.
                </div>
            </div>
        </div>
    </div>
    <!-- feature11 -->
    <div class="white-section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <img src="../common_assets/images/11.webp" class="img-fluid">
                    <div class="f-24 f-md-36 w500 lh140 mt10  black-clr black-clr">
                        <span class="w500">Get 6 Beautiful Color Templates Of All The Sales Pages; Affiliate Page& Thank You Pages For Our DFY Video Courses</span>
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md30">
                    <img src="assets/images/f11.png" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 f-18 f-md-20 w400 lh140 mt20 mt-md50">
                    All these pages come in 6 beautifully formatted and completely ready-to-use colour templates. These comes with a flexible free flow editor to edit almost anything. Just use them and let the magic begin
                </div>
            </div>
        </div>
    </div>
    <!-- feature12 -->
    <div class="grey-section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <img src="../common_assets/images/12.webp" class="img-fluid">
                    <div class="f-24 f-md-36 w500 lh140 mt10  black-clr">
                        Get Done For You Follow Up Emails To Multiply Sales & Profits
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md30">
                    <img src="assets/images/f12.png" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 f-18 f-md-20 w400 lh140 mt20 mt-md50">
                    Send ready-to-use high converting emails to the customers who opted in for free offer or purchased the basic product & <span class="w600">make the most out of every single subscriber effortlessly.</span> You will get those for all
                    5 DFY courses.
                </div>
            </div>
        </div>
    </div>
    <!-- feature13 -->
    <div class="white-section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <img src="../common_assets/images/13.webp" class="img-fluid">
                    <div class="f-24 f-md-36 w500 lh140 mt10  black-clr">
                        Complete Set Of Professional Graphics For Your DFY Courses To Use For Consistent Branding
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md30">
                    <img src="assets/images/f13.png" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 f-18 f-md-20 w400 lh140 mt20 mt-md50">
                    We are serious about giving you every single thing you need to ensure you’re branding consistently.<br><br> You’ll get complete set of <span class="w600">e-cover graphics, download button, header and footer graphics and complete set of graphics
                  for your PDF guides, andpresentation slides.</span>
                </div>
            </div>
        </div>
    </div>
    <!-- feature14 -->
    <div class="grey-section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <img src="../common_assets/images/14.webp" class="img-fluid">
                    <div class="f-24 f-md-36 w500 lh140 mt10  black-clr">
                        Media Library To Manage All Your Images In One Place
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md30">
                    <img src="assets/images/f14.png" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 f-18 f-md-20 w400 lh140 mt20 mt-md50">
                    No more searching files on your desktop or rushing back to the designers. With Pro edition, <span class="w600">you can easily store & manage all your images and videos at a central location.</span> Just point and click and add them
                    to your campaigns in no time.
                </div>
            </div>
        </div>
    </div>
    <!-- feature15 -->
    <div class="white-section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <img src="../common_assets/images/15.webp" class="img-fluid">
                    <div class="f-24 f-md-36 w500 lh140 mt10  black-clr">
                        Get 10K+ FREE Stock Images for your products, marketplace, or social media posts-
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md30">
                    <img src="assets/images/f15.png" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 f-18 f-md-20 w400 lh140 mt20 mt-md50">
                    Eye-catchy images are best way to attract audience and get them hooked to your academies. <br><br> So, with Pro edition, you’re getting the power of <span class="w600">utilizing thousands of royalty free images for your products, marketplace
                  or social media posts.</span>
                </div>
            </div>
        </div>
    </div>
    <!-- feature16 -->
    <div class="grey-section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <img src="../common_assets/images/16.webp" class="img-fluid">
                    <div class="f-24 f-md-36 w500 lh140 mt10  black-clr">
                        Advance And Deep Analytics To Track Your Growth And Scale It Further
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md30">
                    <img src="assets/images/f16.png" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 f-18 f-md-20 w400 lh140 mt20 mt-md50">
                    Analytics are important to find out what went a success with your audience, which content made them to act and how they reacted – they liked, shared, commented, purchased a product, or made an exit. <br><br> Gain each of the crucial
                    metrics regarding your academy, blog and social campaigns right there in your dashboard. <span class="w600">No more hiring a third-party analysis tool and getting confused on how to draw analysis.</span>
                </div>
            </div>
        </div>
    </div>
    <!-- feature17 -->
    <div class="white-section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <img src="../common_assets/images/17.webp" class="img-fluid">
                    <div class="f-24 f-md-36 w500 lh140 mt10  black-clr">
                        </span>
                        <span class="w500">Register Your Students Directly For Your Webinars</span> With Webinar Platform Integrations
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md30">
                    <img src="assets/images/f17.png" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 f-18 f-md-20 w400 lh140 mt20 mt-md50">
                    Create and deliver your next webinar without switching over or exporting another CSV file. Get fast and easy integrations with GoToWebinar and <span class="w600"> collect your registrants and attendee’s data without losing a single lead.</span>
                </div>
            </div>
        </div>
    </div>
    <!-- feature18 -->
    <div class="grey-section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <img src="../common_assets/images/18.webp" class="img-fluid">
                    <div class="f-24 f-md-36 w500 lh140 mt10  black-clr">
                        Boost Relations & Conversions With Your Students Using CRM Integrations
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md30">
                    <img src="assets/images/f18.png" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 f-18 f-md-20 w400 lh140 mt20 mt-md50">
                    Managing leads is one of the most complicated tasks but we are making it easier for you by giving you an option of integrating CRM, <span class="w600">so that you can take customer satisfaction to the next level.</span>
                </div>
            </div>
        </div>
    </div>
    <!-- feature19 -->
    <div class="white-section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <img src="../common_assets/images/19.webp" class="img-fluid">
                    <div class="f-24 f-md-36 w500 lh140 mt10  black-clr">
                        Complete Team Management With Rights Control – Upto 10 Team members
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md30">
                    <img src="assets/images/f19.png" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 f-18 f-md-20 w400 lh140 mt20 mt-md50">
                    You focus on growth and let your team work on your & client’s projects. Now, assign roles and privileges to each of the team members (inhouse or freelancers) and reduce your burden of managing hundreds of accounts and projects yourself. <br><br>                    <span class="w600"> Choose from the defined roles and privileges and can even tailor them according to the project requirements.</span>
                </div>
            </div>
        </div>
    </div>
    <!--feature28 -->
    <div class="grey-section">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12">
                    <img src="../common_assets/images/20.webp" class="img-fluid">
                    <div class="f-24 f-md-36 w500 lh140 mt10  black-clr">
                        Get all these benefits at an unparallel price
                    </div>
                </div>
            </div>
            <div class="row d-flex align-items-center flex-wrap mt20 mt-md30">
                <div class="col-12 col-md-5 order-md-2">
                    <img src="assets/images/f20.png" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 col-md-7 order-md-1 mt-md10 mt0 f-18 f-md-20 w400 lh140 mt20 mt-md0">
                    Ultimately you are saving thousands of dollars monthly with us, & every dollar that you save, adds to your profits. By multiplying engagement & opt-ins, <span class="w600">you’re not only saving money, you’re also taking your business to the next level.</span>
                </div>
            </div>
        </div>
    </div>
    <!--7. Seventh Section Starts -->
    <div class="white-section">
        <div class="container">
            <div class="row">
                <div class="col-12 f-28 f-md-50 w500 lh140 text-center">
                    Act Now For <span class="w700">2 Very Good Reasons:</span>
                </div>
                <div class="col-12 mt20 mt-md30">
                    <div class="row align-items-center d-flex">
                        <div class="col-2 col-md-1 pr10">
                            <img src="assets/images/one.png" class="img-fluid d-block mx-auto">
                        </div>
                        <div class="col-10 col-md-11 f-18 f-md-20 w400 lh140">
                            We’re offering LearnX Pro at a CUSTOMER’S ONLY discount, so you can grab this <span class="w600">game-changing upgrade for a low ONE-TIME fee.</span>
                        </div>
                    </div>
                </div>
                <div class="col-12 mt20 mt-md30">
                    <div class="row align-items-center d-flex">
                        <div class="col-2 col-md-1 pr10">
                            <img src="assets/images/two.png" class="img-fluid d-block mx-auto">
                        </div>
                        <div class="col-10 col-md-11 f-18 f-md-20 w400 lh140">
                            Your only chance to <span class="w600">get access is right now,</span> on this page. When this page closes, so does this exclusive offer.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--7. Seventh Section End -->
    <!--money back Start -->
    <div class="moneyback-section">
        <div class="container">
            <div class="row">
                <div class="col-12 f-28 f-md-45 lh140 w500 text-center white-clr">
                    Let’s Remove All The Risks For You And Secure It With <span class="w700">30 Days Money Back Guarantee</span>
                </div>
            </div>
            <div class="row align-items-center d-flex flex-wrap mt20 mt-md50">
                <div class="col-12 col-md-4">
                    <img src="assets/images/moneyback.png" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 col-md-8 mt20 mt-md0 f-md-20 f-18 lh140 w400 text-left white-clr">
                    You can buy with confidence because if you face any technical issue which we don’t resolve for you, <span class="w600">just raise a ticket within 30 days and we'll refund you everything,</span> down to the last penny.
                    <br><br> <span class="w600">Our team has a 99.21% proven record of solving customer problems and helping them through any issues. </span>
                    <br><br> Guarantees like that don't come along every day, and neither do golden chances like this.
                </div>
            </div>
        </div>
    </div>
    <!--money back End-->
    <!---Bonus Section Starts-->
    <div class="bonus-section">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <div class="black-design">
                        <div class="f-28 f-md-50 w700 lh140 text-center white-clr skew-normal">
                            But That’s Not All
                        </div>
                    </div>
                </div>
                <div class="col-12 mt20 f-20 f-md-24 w400 text-center lh140">
                    In addition, we have a number of bonuses for those who want to take action today and start profiting from this opportunity
                </div>
                <!-- bonus1 -->
                <div class="col-12 col-md-12 mt20 mt-md60">
                    <div class="row align-items-center d-flex felx-wrap">
                        <div class="col-12 col-md-6">
                            <div class="f-md-36 f-24 w700 blue-clr text-nowrap">
                                Bonus 1
                            </div>
                            <div class="f-24 f-md-30 w500 lh140 mt20 mt-md20 text-left black-clr">
                                Brand Your Business For Success
                            </div>
                            <div class="f-18 f-md-20 w400 lh140 mt10 mt-md10 text-left">
                                Branding plays a crucial role for the success of your business and if not done the right way, it can be fatal for your business. So, we’re providing this helpful bonus that enables you to proven and effective methods that you can use to brand yourself
                                and your business for success. Use it with brand building powers of LearnX Pro to take your benefits to the next level.
                            </div>
                        </div>
                        <div class="col-12 col-md-6 mt20 mt-md0">
                            <img src="assets/images/bonusbox.webp" class="img-fluid d-block mx-auto">
                        </div>
                    </div>
                </div>
                <!-- bonus2 -->
                <div class="col-12 col-md-12 mt40 mt-md60">
                    <div class="row align-items-center d-flex felx-wrap">
                        <div class="col-12 col-md-6 order-md-2 mt-md50 mt0">
                            <div class="f-md-36 f-24 w700 blue-clr text-nowrap">
                                Bonus 2
                            </div>
                            <div class="f-24 f-md-30 w500 lh140 mt20 mt-md20 text-left black-clr">
                                The Flipping Code
                            </div>
                            <div class="f-18 f-md-20 w400 lh140 mt10 mt-md10 text-left">
                                Website flipping is a sure-shot way of boosting your business income. So, here is 5 easy-to-follow videos, teaching you how to build and sell websites and generate massive paydays from flipping brand new websites. When combined with LearnX Pro, it
                                becomes a complete no brainer to fuel your business growth.
                            </div>
                        </div>
                        <div class="col-12 col-md-6 order-md-1 mt20 mt-md0">
                            <img src="assets/images/bonusbox.webp" class="img-fluid d-block mx-auto">
                        </div>
                    </div>
                </div>
                <!-- bonus3 -->
                <div class="col-12 col-md-12 mt20 mt-md60">
                    <div class="row align-items-center d-flex felx-wrap">
                        <div class="col-12 col-md-6">
                            <div class="f-md-36 f-24 w700 blue-clr text-nowrap">
                                Bonus 3
                            </div>
                            <div class="f-24 f-md-30 w500 lh140 mt20 mt-md20 text-left black-clr">
                                $1,000,000 Copywriting Secrets
                            </div>
                            <div class="f-18 f-md-20 w400 lh140 mt10 mt-md10 text-left">
                                Copywriting is of immense importance to showcase your brand and get maximum value for your marketing efforts. So, checkout this info-packed e-book that has killer copywriting secrets to supercharge your marketing and your profits and give you the security
                                of knowing you can generate cash anytime you want. When you use them along with LearnX Pro, you get real scalable results for your efforts.
                            </div>
                        </div>
                        <div class="col-12 col-md-6 mt20 mt-md0">
                            <img src="assets/images/bonusbox.webp" class="img-fluid d-block mx-auto">
                        </div>
                    </div>
                </div>
                <!-- bonus2 -->
                <div class="col-12 col-md-12 mt40 mt-md60">
                    <div class="row align-items-center d-flex felx-wrap">
                        <div class="col-12 col-md-6 order-md-2 mt-md50 mt0">
                            <div class="f-md-36 f-24 w700 blue-clr text-nowrap">
                                Bonus 4
                            </div>
                            <div class="f-24 f-md-30 w500 lh140 mt20 mt-md20 text-left black-clr">
                                Triple Your Conversions Instantly
                            </div>
                            <div class="f-18 f-md-20 w400 lh140 mt10 mt-md10 text-left">
                                Boosting conversions is a BIG headache for every marketer today. But its not all that easy as it looks.<br> Keeping this in mind, we’re providing a helpful bonus that includes 28 power packed conversion-boosting secrets
                                to convert visitors into buyers.<br> These techniques when combined with LearnX Pro get best results for your business.
                            </div>
                        </div>
                        <div class="col-12 col-md-6 order-md-1 mt20 mt-md0">
                            <img src="assets/images/bonusbox.webp" class="img-fluid d-block mx-auto">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Bonus Section Starts -->
    <!--10. Table Section Starts -->
    <div class="table-section" id="buynow">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="f-18 f-md-20 w500 lh140 text-center">
                        Yup..! Take action on this while you can as you won’t be seeing this offer again.
                    </div>
                    <div class="f-28 f-md-45 w500 lh140 mt20 mt-md30 text-center black-clr">
                        Today You Can <span class="w700">Get Unrestricted Access to LearnX</span> For LESS Than the Price of Just One Month’s Membership.
                    </div>
                </div>
                <div class="col-md-8 mx-auto col-12 mt30 mt-md70">
                    <div class="tablebox2">
                        <div class="tbbg2 text-center">
                            <div class="tbbg2-text mt0 mt-md0 w600 f-28 f-md-45 text-center" editabletype="text" style="z-index:10;">
                                LearnX Pro Plan
                            </div>
                        </div>
                        <div class="text-center white-bg">
                            <ul class="f-18 f-md-18 w400 lh140 text-center vgreytick mb0">
                                <li class="odd">
                                    <span class="w600">Break Free & Go Limitless-</span> Build Unlimited Academies, Add Unlimited Custom Domains, Create Unlimited Courses, Bring Unlimited Website Visitors, Collect Unlimited Leads, Host Unlimited Video
                                    Trainings & Files With Unlimited Downloading/Uploading Bandwidth.
                                </li>
                                <li class="odd">
                                    <span class="w600">Build UNLIMITED Pro Level Academies</span> to spread the power of your knowledge and get your students spell bound.
                                </li>
                                <li class="even">
                                    <span class="w600">Run All Your & Your Client’s Academies  </span> On Your Own Domains With UNLIMITED Custom Domains
                                </li>
                                <li class="even">
                                    <span class="w600">Create UNLIMITED courses  </span> For Each Of Your Academies
                                </li>
                                <li class="odd">
                                    <span class="w600">Get UNLIMITED Website Visitors </span> on your website, marketplace or blog views
                                </li>
                                <li class="even">
                                    <span class="w600">Maximize Lead Generation - Capture UNLIMITED Leads </span>
                                </li>
                                <li class="even">
                                    <span class="w600">Host & deliver UNLIMITED Video Trainings & Files  </span> To Your Pro Academies
                                </li>
                                <li class="odd">
                                    <span class="w600">Get UNLIMITED bandwidth  </span> To Give Best User Experience
                                </li>
                                <li class="odd">
                                    <span class="w600">Give your Academy site a Pro level Look & Feel - Customise your 
                              Academy Sites with 20 unique colour themes
                              </span>
                                </li>
                                <li class="odd">
                                    <span class="w600">Multiply your leads with 30 lead generation popup templates</span>
                                </li>
                                <li class="even">
                                    <span class="w600">Get 6 beautiful color templates of all the sales pages;  </span> Affiliate Page & Thank You Pages For Our DFY Video Courses
                                </li>
                                <li class="even">
                                    <span class="w600">Get Done for you follow up emails  </span> To Multiply Sales & Profits
                                </li>
                                <li class="odd">
                                    <span class="w600">Complete Set Of Professional Graphics For Your DFY Courses </span> to use for consistent branding-
                                </li>
                                <li class="odd">
                                    <span class="w600">Media Library To Manage All Your Images In One Place</span>
                                </li>
                                <li class="even">
                                    <span class="w600">Get 10K+ FREE Stock Images  </span> For Your Products, Marketplace Or Social Media Posts
                                </li>
                                <li class="even">
                                    <span class="w600">Advance And Deep Analytics </span> To Track Your Growth And Scale It Further
                                </li>
                                <li class="odd">
                                    <span class="w600">Register Your Students Directly For Your Webinars  </span> With Webinar Platform Integrations
                                </li>
                                <li class="even">
                                    <span class="w600">Boost Relations & Conversions With Your Students  </span> Using CRM Integrations
                                </li>
                                <li class="odd">
                                    <span class="w600">Complete Team Management With Rights Control – Upto 10 Team members  </span>
                                </li>
                                <li class="even">
                                    Get all these benefits At <span class="w600">An Unparalleled Price</span>
                                </li>
                            </ul>
                        </div>
                        <div class="myfeatureslast f-md-25 f-16 w400 text-center lh140 hideme relative mb-md15">
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-12 text-center">
                                        <div class="f-18 f-md-32 w500 lh140 mt20 mt-md20 text-left black-clr">
                                            Regular Price - <strike>$147</strike>
                                        </div>
                                        <div class="f-24 f-md-30 w700 lh140 mt20 mt-md20 text-left black-clr">
                                            Get It Today For ONLY- $47 One Time
                                        </div>
                                    </div>
                                    <div class="col-12 mb-md0 mb30 mb-md0 mx-auto mt20 mt-md50">
                                        <!-- <div editabletype="button" style="z-index: 10;">
                                            <a href="https://warriorplus.com/o2/buy/hb06pf/f0znfl/kpcb3m" id="buyButton"><img src="https://warriorplus.com/o2/btn/fn200011000/hb06pf/f0znfl/352568" class="img-fluid d-block mx-auto" border="0"></a>
                                        </div> -->
                                        <div editabletype="button" style="z-index: 10;">
                                            <a href="https://warriorplus.com/o2/buy/fbmn7d/gf0sgx/xpfqk3" id="buyButton"><img src="https://warriorplus.com/o2/btn/fn200011000/fbmn7d/gf0sgx/373796" class="img-fluid d-block mx-auto" border="0"></a>
                                        </div>
                                    </div>
                    <!-- <div class="table-border-content mt20 mt-md50">
                     <div class="tb-check d-flex align-items-center f-18">
                        <label class="checkbox inline">
                           <img src="https://assets.clickfunnels.com/templates/listhacking-sales/images/arrow-flash-small.gif" alt="">
                           <input type="checkbox" id="check" onclick="myFunction()">
                        </label>
                        <label class="checkbox inline">
                           <h5> &nbsp; &nbsp;Yes, Add CDN Fast Unlimited Hosting & 300+DFY Lead Popups</h5>
                        </label>
                     </div>
                     <div class="p20">
                        <p class="f-18 text-center text-md-start"><b class="red-clr underline">LIMITED TIME OFFER :</b> Active CDN Fast DOUBLE Unlimited Hosting (Video & Web Hosting) + One Click Lead Popups with 300+ Ready To Use Templates + Commercial License to Serve Your Clients   <br><b>(92% Of Customers Pick Up This Add-on & Save $1,000s Per Year!)</b></p>
                        <p class="f-18 text-center text-md-start"> <span class="green-clr w600"> Just $9.93 One Time  (Regular $397)</span> </p>
                     </div>
                  </div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 mt40 mt-md50 thanks-button text-center">
                    <a class="kapblue f-md-22 f-18 lh140 w400" href="https://warriorplus.com/o/nothanks/gf0sgx" target="_blank">
                        No thanks - I Don't Want To Remove All Limitations To Go Unlimited & Get 10X More Profits Faster & Easier with This Pro Upgrade? Please Take Me To The Next Step To Get Access To My Purchase.
                  </a>
                </div>
            </div>
        </div>
    </div>
    <!--10. Table Section End -->
    <!--Footer Section Start -->
    <div class="footer-section">
        <div class="container ">
            <div class="row">
                <div class="col-12 text-center">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 484.58 200.84" style="max-height:75px;">
                        <defs>
                            <style>
                                .cls-1,.cls-4{fill:#fff;}
                                .cls-2,.cls-3,.cls-4{fill-rule:evenodd;}
                                .cls-2{fill:url(#linear-gradient);}
                                .cls-3{fill:url(#linear-gradient-2);}
                                .cls-5{fill:url(#linear-gradient-3);}
                            </style>
                            <linearGradient id="linear-gradient" x1="-154.67" y1="191.86" x2="249.18" y2="191.86" gradientTransform="matrix(1.19, -0.01, 0.05, 1, 178.46, -4.31)" gradientUnits="userSpaceOnUse">
                                <stop offset="0" stop-color="#ead40c"></stop>
                                <stop offset="1" stop-color="#ff00e9"></stop>
                            </linearGradient>
                            <linearGradient id="linear-gradient-2" x1="23.11" y1="160.56" x2="57.61" y2="160.56" gradientTransform="matrix(1, 0, 0, 1, 0, 0)" xlink:href="#linear-gradient"></linearGradient>
                            <linearGradient id="linear-gradient-3" x1="384.49" y1="98.33" x2="483.38" y2="98.33" gradientTransform="matrix(1, 0, 0, 1, 0, 0)" xlink:href="#linear-gradient"></linearGradient>
                        </defs>
                        <g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1">
                            <path class="cls-1" d="M112.31,154.3q-11.65,0-20.1-4.87a32.56,32.56,0,0,1-13-13.82q-4.53-9-4.54-21.11a46.06,46.06,0,0,1,4.57-21A34.29,34.29,0,0,1,92,79.37a36.11,36.11,0,0,1,19.31-5.06,39.65,39.65,0,0,1,13.54,2.29,31,31,0,0,1,11.3,7.09,33.32,33.32,0,0,1,7.75,12.18,49.23,49.23,0,0,1,2.82,17.57V119H83.26v-12.3h46A19.87,19.87,0,0,0,127,97.38a16.6,16.6,0,0,0-6.18-6.48,17.59,17.59,0,0,0-9.21-2.37,17.88,17.88,0,0,0-9.83,2.7,18.81,18.81,0,0,0-6.58,7.06,20.17,20.17,0,0,0-2.4,9.56v10.74a25.15,25.15,0,0,0,2.47,11.57,17.42,17.42,0,0,0,6.91,7.37,20.52,20.52,0,0,0,10.39,2.55,21.62,21.62,0,0,0,7.22-1.14,15.71,15.71,0,0,0,5.59-3.35,14.11,14.11,0,0,0,3.59-5.5L146,132a26.44,26.44,0,0,1-6.13,11.77,29.72,29.72,0,0,1-11.52,7.77A43.74,43.74,0,0,1,112.31,154.3Z"></path>
                            <path class="cls-1" d="M184.49,154.35a31.78,31.78,0,0,1-13.24-2.65,21.26,21.26,0,0,1-9.28-7.84,22.86,22.86,0,0,1-3.41-12.81A21.92,21.92,0,0,1,161,120.2a18.9,18.9,0,0,1,6.61-6.86,33.85,33.85,0,0,1,9.46-3.9,77.76,77.76,0,0,1,10.92-2q6.81-.7,11-1.28a15.91,15.91,0,0,0,6.18-1.82,4.25,4.25,0,0,0,1.94-3.86v-.3q0-5.7-3.38-8.83T194,88.28q-6.71,0-10.62,2.92a14.55,14.55,0,0,0-5.27,6.91l-17-2.42a27.51,27.51,0,0,1,6.66-11.83,29.46,29.46,0,0,1,11.35-7.16,43.73,43.73,0,0,1,14.83-2.39,47.89,47.89,0,0,1,11.14,1.31,31.6,31.6,0,0,1,10.14,4.31,22.12,22.12,0,0,1,7.39,8.14q2.81,5.15,2.8,12.87v51.85H207.84V142.14h-.61a22.1,22.1,0,0,1-4.66,6,22.35,22.35,0,0,1-7.52,4.49A30,30,0,0,1,184.49,154.35Zm4.74-13.42a19.7,19.7,0,0,0,9.53-2.19,16,16,0,0,0,6.23-5.83,15,15,0,0,0,2.19-7.91v-9.13a8.72,8.72,0,0,1-2.9,1.31,45.56,45.56,0,0,1-4.56,1.06c-1.68.3-3.35.57-5,.8l-4.29.61a32,32,0,0,0-7.32,1.81A12.29,12.29,0,0,0,178,125a9.76,9.76,0,0,0,1.82,13.39A16,16,0,0,0,189.23,140.93Z"></path>
                            <path class="cls-1" d="M243.8,152.79V75.31h17.7V88.23h.81a19.36,19.36,0,0,1,7.29-10.37,20,20,0,0,1,11.83-3.66c1,0,2.14,0,3.4.13a23.83,23.83,0,0,1,3.15.38V91.5a21,21,0,0,0-3.65-.73,38.61,38.61,0,0,0-4.82-.32,18.47,18.47,0,0,0-8.95,2.14,15.94,15.94,0,0,0-6.23,5.93,16.55,16.55,0,0,0-2.27,8.72v45.55Z"></path>
                            <path class="cls-1" d="M318.4,107.39v45.4H300.14V75.31h17.45V88.48h.91a22,22,0,0,1,8.55-10.34q5.86-3.84,14.55-3.83a27.58,27.58,0,0,1,14,3.43,23.19,23.19,0,0,1,9.28,9.93,34.22,34.22,0,0,1,3.26,15.79v49.33H349.87V106.28c0-5.17-1.34-9.23-4-12.15s-6.37-4.39-11.07-4.39a17,17,0,0,0-8.5,2.09,14.67,14.67,0,0,0-5.8,6A20,20,0,0,0,318.4,107.39Z"></path>
                            <path class="cls-2" d="M314.05,172.88c12.69-.14,27.32,2.53,40.82,2.61l-.09,1c1.07.15,1-.88,2-.78,1.42-.18,0,1.57,1.82,1.14.54-.06.08-.56-.32-.68.77,0,2.31.17,3,1.26,1.3,0-.9-1.34.85-.89,6,1.63,13.39,0,20,3.2,11.64.72,24.79,2.38,38,4.27,2.85.41,5.37,1.11,7.57,1.05,2-.05,5.68.78,8,1.08,2.53.34,5.16,1.56,7.13,1.65-1.18-.05,5.49-.78,4.54.76,5-.72,10.9,1.67,15.94,1.84.83-.07.69.53.67,1,1.23-1.17,3.32.56,4.28-.56.25,2,2.63-.48,3.3,1.61,2.23-1.39,4.83.74,7.55,1.37,1.91.44,5.29-.21,5.55,2.14-2.1-.13-5.12.81-11.41-1.09,1,.87-3.35.74-3.39-.64-1.81,1.94-6.28-1-7.79,1.19-1.26-.41,0-.72-.64-1.35-5,.55-8.09-1.33-12.91-1.56,2.57,2.44,6.08,3.16,10.06,3.22,1.28.15,0,.74,1,1.39,1.37-.19.75-.48,1.26-1.17,5.37,2.28,15.36,2.35,21,4.91-4.69-.63-11.38,0-15.15-2.09A148,148,0,0,1,441.58,196c-.53-.1-1.81.12-.7-.71-1.65.73-3.41.1-5.74-.22-37.15-5.15-80.47-7.78-115.75-9.76-39.54-2.22-76.79-3.22-116.44-2.72-61.62.78-119.59,7.93-175.21,13.95-5,.55-10,1.49-15.11,1.47-1.33-.32-2.46-1.91-1.25-3-2-.38-2.93.29-5-.15a9.83,9.83,0,0,1-1.25-3,13.73,13.73,0,0,1,6.42-2.94c.77-.54.12-.8.15-1.6a171,171,0,0,1,45.35-8.58c26.43-1.1,53.68-4.73,79.64-6,6.25-.3,11.72.06,18.41.14,8.31.1,19.09-1,29.19-.12,6.25.54,13.67-.55,21.16-.56,39.35-.09,71.16-.23,112.28,2C317.24,171.93,315.11,173.81,314.05,172.88Zm64.22,16.05-1.6-.2C376.2,190,378.42,190.26,378.27,188.93Z"></path>
                            <polygon class="cls-3" points="52.57 166.06 57.61 151.99 23.11 157.23 32.36 169.12 52.57 166.06"></polygon>
                            <polygon class="cls-4" points="44.69 183.24 51.75 168.35 33.86 171.06 44.69 183.24"></polygon>
                            <path class="cls-4" d="M.12,10.15l22,145.06,35.47-5.39-22-145a2.6,2.6,0,0,0,0-.4C35,.76,26.62-.95,16.81.54S-.52,6.16,0,9.77A2.62,2.62,0,0,0,.12,10.15Z"></path>
                            <path class="cls-5" d="M433.68,79.45l21-36.33h27.56L448.48,97.51l34.9,56H456l-22-37.76-21.94,37.76H384.49l34.9-56L385.72,43.12h27.35Z"></path>
                        </g>
                            </g>
                    </svg>
                    <div class="f-14 f-md-16 w400 mt20 lh160 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
                    <div class=" mt20 mt-md40 white-clr"> <div class="f-20 w500 mt20 lh140 white-clr text-center">
                        <script type="text/javascript" src="https://warriorplus.com/o2/disclaimer/fbmn7d" defer></script><div class="wplus_spdisclaimer"></div></div></div>
                </div>
                <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                    <div class="f-14 f-md-16 w400 lh160 white-clr text-xs-center">Copyright © LearnX</div>
                    <ul class="footer-ul w400 f-14 f-md-16 white-clr text-center text-md-right">
                        <li><a href="https://support.bizomart.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                        <li><a href="http://getlearnx.com/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                        <li><a href="http://getlearnx.com/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                        <li><a href="http://getlearnx.com/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                        <li><a href="http://getlearnx.com/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                        <li><a href="http://getlearnx.com/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                        <li><a href="http://getlearnx.com/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!--Footer Section End -->
     
</body>
</html>


<script type="text/javascript" src="assets/js/ouibounce.min.js"></script>
<div id="ouibounce-modal" style="z-index:9999999; display:none;">
   <div class="underlay"></div>
   <div class="modal-wrapper" style="display:block;">
      <div class="modal-bg">
		<button type="button" class="close" onclick="document.getElementById('ouibounce-modal').style.display = 'none';">
			X 
		</button>
		<div class="model-header">
      <div class="d-flex justify-content-center align-items-center gap-3">
            <img src="assets/images/hold.png" alt="" class="img-fluid d-block m131">
            <div>
               <div class="f-md-50 f-20 w700 lh130 white-clr">
                  WAIT! HOLD ON!!
               </div>
               <div class="f-md-18 f-16 w700 lh130 white-clr mt10 mt-md0">
                  Don't Leave Empty Handed
               </div>
            </div>
         </div>
		</div>
         <div class="col-12 for-padding">
			<div class="copun-line f-md-32 f-16 w500 lh130 text-center black-clr">
         You've Qualified For An <span class="w700 red-clr"> <br> INSTANT $100 DISCOUNT</span>  
			</div>
         <div class="copun-line f-md-30 f-16 w700 lh130 text-center black-clr mt10" style="background-color:yellow; padding:5px 10px; display:inline-block">
         Regular Price <strike>$147</strike>,
         Now Only $47	
			</div>
			 <div class="mt-md20 mt10 text-center">
				<a href="https://warriorplus.com/o2/buy/fbmn7d/gf0sgx/xpfqk3"  id="buyButtonpopup"class="cta-link-btn1">Grab SPECIAL Discount Now!
				<br>
				<span class="f-12 black-clr w600 text-uppercase">Act Now! Limited to First 100 Buyers Only.</span> </a>
			 </div>
			<!-- <div class="mt-md20 mt10 text-center f-md-26 f-16 w900 lh150 text-center red-clr text-uppercase">
				Coupon Code EXPIRES IN:
			</div> -->
			<!-- <div class="countdown counter-white text-center">
                     <div class="timer-label text-center"><span class="f-24 f-md-42 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
                     <div class="timer-label text-center"><span class="f-24 f-md-42 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                     <div class="timer-label text-center timer-mrgn"><span class="f-24 f-md-42 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                     <div class="timer-label text-center "><span class="f-24 f-md-42 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
            </div> -->
         </div>
      </div>
   </div>
</div>

    <script type="text/javascript">
        var _ouibounce = ouibounce(document.getElementById('ouibounce-modal'),{
        aggressive: true,
        });
    </script>
<!-- <script type="text/javascript">
   var noob = $('.countdown').length;
   var stimeInSecs;
   var tickers;

   function timerBegin(seconds) {   
      if(seconds>=0)  {
         stimeInSecs = parseInt(seconds);           
         showRemaining();
      }
      //tickers = setInterval("showRemaining()", 1000);
   }

   function showRemaining() {

      var seconds = stimeInSecs;
         
      if (seconds > 0) {         
         stimeInSecs--;       
      } else {        
         clearInterval(tickers);       
         //timerBegin(59 * 60); 
      }

      var days = Math.floor(seconds / 86400);

      seconds %= 86400;
      
      var hours = Math.floor(seconds / 3600);
      
      seconds %= 3600;
      
      var minutes = Math.floor(seconds / 60);
      
      seconds %= 60;


      if (days < 10) {
         days = "0" + days;
      }
      if (hours < 10) {
         hours = "0" + hours;
      }
      if (minutes < 10) {
         minutes = "0" + minutes;
      }
      if (seconds < 10) {
         seconds = "0" + seconds;
      }
      var i;
      var countdown = document.getElementsByClassName('countdown');

      for (i = 0; i < noob; i++) {
         countdown[i].innerHTML = '';
      
         if (days) {
            countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-24 f-md-42 timerbg oswald">' + days + '</span><br><span class="f-14 f-md-14 w400 smmltd">Days</span> </div>';
         }
      
         countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-24 f-md-42 timerbg oswald">' + hours + '</span><br><span class="f-14 f-md-14 w400 smmltd">Hours</span> </div>';
      
         countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"><span class="f-24 f-md-42 timerbg oswald">' + minutes + '</span><br><span class="f-14 f-md-14 w400 smmltd">Mins</span> </div>';
      
         countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-24 f-md-42 timerbg oswald">' + seconds + '</span><br><span class="f-14 f-md-14 w400 smmltd">Sec</span> </div>';
      }
   
   }

const givenTimestamp = 'Mon Dec 4 2023 20:30:00 GMT+0530'
const durationTime = 60;

const date = new Date(givenTimestamp);
const givenTime = date.getTime() / 1000;
const currentTime = new Date().getTime()



let cnt;
const matchInterval = setInterval(() => {
   if(Math.round(givenTime) === Math.round(new Date().getTime() /1000)){
   cnt = durationTime * 60;
   counter();
   clearInterval(matchInterval);
   }else if(Math.round(givenTime) < Math.round(new Date().getTime() /1000)){
      const timeGap = Math.round(new Date().getTime() /1000) - Math.round(givenTime)
      let difference = (durationTime * 60) - timeGap;
      if(difference >= 0){
         cnt = difference
         localStorage.mavasTimer = cnt;

      }else{
         
         difference = difference % (durationTime * 60);
         cnt = (durationTime * 60) + difference
         localStorage.mavasTimer = cnt;

      }

      counter();
      clearInterval(matchInterval);
   }
},1000)





function counter(){
   if(parseInt(localStorage.mavasTimer) > 0){
      cnt = parseInt(localStorage.mavasTimer);
   }
   cnt -= 1;
   localStorage.mavasTimer = cnt;
   if(localStorage.mavasTimer !== NaN){
      timerBegin(parseInt(localStorage.mavasTimer));
   }
   if(cnt>0){
      setTimeout(counter,1000);
  }else if(cnt === 0){
   cnt = durationTime * 60 
   counter()
  }
}

</script> -->
<!--- timer end-->



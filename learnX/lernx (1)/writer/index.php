<!Doctype html>
<html>
   <head>
      <title> LearnX | Writer</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=9">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
      <meta name="title" content="LearnX Writer">
      <meta name="description" content="Futuristic A.I. Technology Creates Stunning Content for Any Local or Online Niche 10X Faster…Just by Using a Keyword">
      <meta name="keywords" content="LearnX | Writer">
      <meta property="og:image" content="https://www.getlearnx.com/writer/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Pranshu Gupta">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="Website">
      <meta property="og:title" content="LearnX Writer">
      <meta property="og:description" content=" Futuristic A.I. Technology Creates Stunning Content for Any Local or Online Niche 10X Faster…Just by Using a Keyword">
      <meta property="og:image" content="https://www.getlearnx.com/writer/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="LearnX Writer">
      <meta property="twitter:description" content=" Futuristic A.I. Technology Creates Stunning Content for Any Local or Online Niche 10X Faster…Just by Using a Keyword">
      <meta property="twitter:image" content="https://www.getlearnx.com/writer/thumbnail.png">
      <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&family=Poppins:wght@300;400;500;600;700;800;900&family=Red+Hat+Display:wght@300;400;500;600;700;800;900&display=swap" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css2?family=Pacifico&display=swap" rel="stylesheet">
      <!-- required -->
      <link rel="stylesheet" href="https://cdn.oppyo.com/launches/yodrive/common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style.css" type="text/css">
      <link rel="stylesheet" href="assets/css/timer.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style-feature.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style-bottom.css" type="text/css">
      <!-- <script>
         (function(w, i, d, g, e, t, s) {
             if(window.businessDomain != undefined){
                 console.log("Your page have duplicate embed code. Please check it.");
                 return false;
             }
             businessDomain = 'writerarc';
             allowedDomain = 'getlearnx.com';
             if(!window.location.hostname.includes(allowedDomain)){
                 console.log("Your page have not authorized. Please check it.");
                 return false;
             }
             console.log("Your script is ready...");
             w[d] = w[d] || [];
             t = i.createElement(g);
             t.async = 1;
             t.src = e;
             s = i.getElementsByTagName(g)[0];
             s.parentNode.insertBefore(t, s);
         })(window, document, '_gscq', 'script', 'https://cdn.staticdcp.com/apps/engage/smart_engage/js/config-loader.js');
         </script> -->
   </head>
   <body>
   <div class="warning-section">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <div class="offer">
                        <div>
                            <img src="https://cdn.oppyo.com/launches/appzilo/special/error.webp">
                        </div>
                        <div class="f-22 f-md-30 w600 lh140 white-clr text-center">
                            This EXCLUSIVE Offer Has Never Been Offered Before...
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
      <!-- Header Section Start -->
      <div class="header-section">
         <div class="container">
            <div class="row">
               <div class="col-12 d-flex align-items-center justify-content-center  justify-content-md-between flex-wrap flex-md-nowrap">
               <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 484.58 200.84" style="max-height:75px;">
                        <defs>
                            <style>
                                .cls-1,.cls-4{fill:#fff;}
                                .cls-2,.cls-3,.cls-4{fill-rule:evenodd;}
                                .cls-2{fill:url(#linear-gradient);}
                                .cls-3{fill:url(#linear-gradient-2);}
                                .cls-5{fill:url(#linear-gradient-3);}
                            </style>
                            <linearGradient id="linear-gradient" x1="-154.67" y1="191.86" x2="249.18" y2="191.86" gradientTransform="matrix(1.19, -0.01, 0.05, 1, 178.46, -4.31)" gradientUnits="userSpaceOnUse">
                                <stop offset="0" stop-color="#ead40c"></stop>
                                <stop offset="1" stop-color="#ff00e9"></stop>
                            </linearGradient>
                            <linearGradient id="linear-gradient-2" x1="23.11" y1="160.56" x2="57.61" y2="160.56" gradientTransform="matrix(1, 0, 0, 1, 0, 0)" xlink:href="#linear-gradient"></linearGradient>
                            <linearGradient id="linear-gradient-3" x1="384.49" y1="98.33" x2="483.38" y2="98.33" gradientTransform="matrix(1, 0, 0, 1, 0, 0)" xlink:href="#linear-gradient"></linearGradient>
                        </defs>
                        <g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1">
                            <path class="cls-1" d="M112.31,154.3q-11.65,0-20.1-4.87a32.56,32.56,0,0,1-13-13.82q-4.53-9-4.54-21.11a46.06,46.06,0,0,1,4.57-21A34.29,34.29,0,0,1,92,79.37a36.11,36.11,0,0,1,19.31-5.06,39.65,39.65,0,0,1,13.54,2.29,31,31,0,0,1,11.3,7.09,33.32,33.32,0,0,1,7.75,12.18,49.23,49.23,0,0,1,2.82,17.57V119H83.26v-12.3h46A19.87,19.87,0,0,0,127,97.38a16.6,16.6,0,0,0-6.18-6.48,17.59,17.59,0,0,0-9.21-2.37,17.88,17.88,0,0,0-9.83,2.7,18.81,18.81,0,0,0-6.58,7.06,20.17,20.17,0,0,0-2.4,9.56v10.74a25.15,25.15,0,0,0,2.47,11.57,17.42,17.42,0,0,0,6.91,7.37,20.52,20.52,0,0,0,10.39,2.55,21.62,21.62,0,0,0,7.22-1.14,15.71,15.71,0,0,0,5.59-3.35,14.11,14.11,0,0,0,3.59-5.5L146,132a26.44,26.44,0,0,1-6.13,11.77,29.72,29.72,0,0,1-11.52,7.77A43.74,43.74,0,0,1,112.31,154.3Z"></path>
                            <path class="cls-1" d="M184.49,154.35a31.78,31.78,0,0,1-13.24-2.65,21.26,21.26,0,0,1-9.28-7.84,22.86,22.86,0,0,1-3.41-12.81A21.92,21.92,0,0,1,161,120.2a18.9,18.9,0,0,1,6.61-6.86,33.85,33.85,0,0,1,9.46-3.9,77.76,77.76,0,0,1,10.92-2q6.81-.7,11-1.28a15.91,15.91,0,0,0,6.18-1.82,4.25,4.25,0,0,0,1.94-3.86v-.3q0-5.7-3.38-8.83T194,88.28q-6.71,0-10.62,2.92a14.55,14.55,0,0,0-5.27,6.91l-17-2.42a27.51,27.51,0,0,1,6.66-11.83,29.46,29.46,0,0,1,11.35-7.16,43.73,43.73,0,0,1,14.83-2.39,47.89,47.89,0,0,1,11.14,1.31,31.6,31.6,0,0,1,10.14,4.31,22.12,22.12,0,0,1,7.39,8.14q2.81,5.15,2.8,12.87v51.85H207.84V142.14h-.61a22.1,22.1,0,0,1-4.66,6,22.35,22.35,0,0,1-7.52,4.49A30,30,0,0,1,184.49,154.35Zm4.74-13.42a19.7,19.7,0,0,0,9.53-2.19,16,16,0,0,0,6.23-5.83,15,15,0,0,0,2.19-7.91v-9.13a8.72,8.72,0,0,1-2.9,1.31,45.56,45.56,0,0,1-4.56,1.06c-1.68.3-3.35.57-5,.8l-4.29.61a32,32,0,0,0-7.32,1.81A12.29,12.29,0,0,0,178,125a9.76,9.76,0,0,0,1.82,13.39A16,16,0,0,0,189.23,140.93Z"></path>
                            <path class="cls-1" d="M243.8,152.79V75.31h17.7V88.23h.81a19.36,19.36,0,0,1,7.29-10.37,20,20,0,0,1,11.83-3.66c1,0,2.14,0,3.4.13a23.83,23.83,0,0,1,3.15.38V91.5a21,21,0,0,0-3.65-.73,38.61,38.61,0,0,0-4.82-.32,18.47,18.47,0,0,0-8.95,2.14,15.94,15.94,0,0,0-6.23,5.93,16.55,16.55,0,0,0-2.27,8.72v45.55Z"></path>
                            <path class="cls-1" d="M318.4,107.39v45.4H300.14V75.31h17.45V88.48h.91a22,22,0,0,1,8.55-10.34q5.86-3.84,14.55-3.83a27.58,27.58,0,0,1,14,3.43,23.19,23.19,0,0,1,9.28,9.93,34.22,34.22,0,0,1,3.26,15.79v49.33H349.87V106.28c0-5.17-1.34-9.23-4-12.15s-6.37-4.39-11.07-4.39a17,17,0,0,0-8.5,2.09,14.67,14.67,0,0,0-5.8,6A20,20,0,0,0,318.4,107.39Z"></path>
                            <path class="cls-2" d="M314.05,172.88c12.69-.14,27.32,2.53,40.82,2.61l-.09,1c1.07.15,1-.88,2-.78,1.42-.18,0,1.57,1.82,1.14.54-.06.08-.56-.32-.68.77,0,2.31.17,3,1.26,1.3,0-.9-1.34.85-.89,6,1.63,13.39,0,20,3.2,11.64.72,24.79,2.38,38,4.27,2.85.41,5.37,1.11,7.57,1.05,2-.05,5.68.78,8,1.08,2.53.34,5.16,1.56,7.13,1.65-1.18-.05,5.49-.78,4.54.76,5-.72,10.9,1.67,15.94,1.84.83-.07.69.53.67,1,1.23-1.17,3.32.56,4.28-.56.25,2,2.63-.48,3.3,1.61,2.23-1.39,4.83.74,7.55,1.37,1.91.44,5.29-.21,5.55,2.14-2.1-.13-5.12.81-11.41-1.09,1,.87-3.35.74-3.39-.64-1.81,1.94-6.28-1-7.79,1.19-1.26-.41,0-.72-.64-1.35-5,.55-8.09-1.33-12.91-1.56,2.57,2.44,6.08,3.16,10.06,3.22,1.28.15,0,.74,1,1.39,1.37-.19.75-.48,1.26-1.17,5.37,2.28,15.36,2.35,21,4.91-4.69-.63-11.38,0-15.15-2.09A148,148,0,0,1,441.58,196c-.53-.1-1.81.12-.7-.71-1.65.73-3.41.1-5.74-.22-37.15-5.15-80.47-7.78-115.75-9.76-39.54-2.22-76.79-3.22-116.44-2.72-61.62.78-119.59,7.93-175.21,13.95-5,.55-10,1.49-15.11,1.47-1.33-.32-2.46-1.91-1.25-3-2-.38-2.93.29-5-.15a9.83,9.83,0,0,1-1.25-3,13.73,13.73,0,0,1,6.42-2.94c.77-.54.12-.8.15-1.6a171,171,0,0,1,45.35-8.58c26.43-1.1,53.68-4.73,79.64-6,6.25-.3,11.72.06,18.41.14,8.31.1,19.09-1,29.19-.12,6.25.54,13.67-.55,21.16-.56,39.35-.09,71.16-.23,112.28,2C317.24,171.93,315.11,173.81,314.05,172.88Zm64.22,16.05-1.6-.2C376.2,190,378.42,190.26,378.27,188.93Z"></path>
                            <polygon class="cls-3" points="52.57 166.06 57.61 151.99 23.11 157.23 32.36 169.12 52.57 166.06"></polygon>
                            <polygon class="cls-4" points="44.69 183.24 51.75 168.35 33.86 171.06 44.69 183.24"></polygon>
                            <path class="cls-4" d="M.12,10.15l22,145.06,35.47-5.39-22-145a2.6,2.6,0,0,0,0-.4C35,.76,26.62-.95,16.81.54S-.52,6.16,0,9.77A2.62,2.62,0,0,0,.12,10.15Z"></path>
                            <path class="cls-5" d="M433.68,79.45l21-36.33h27.56L448.48,97.51l34.9,56H456l-22-37.76-21.94,37.76H384.49l34.9-56L385.72,43.12h27.35Z"></path>
                        </g>
                            </g>
                    </svg>
                  <div class="d-flex align-items-center flex-wrap justify-content-center  justify-content-md-end mt15 mt-md0">
                      <ul class="leader-ul f-16 f-md-18">
                        <!--<li>
                           <a href="#features" class="t-decoration-none">Features</a><span class="pl9 white-clr">|</span>
                        </li>
                        <li>
                           <a href="#demo" class="t-decoration-none">Demo</a>
                        </li> -->
                        <li class="affiliate-link-btn"><a href="#buynow" class="t-decoration-none">Buy Now</a></li>
                     </ul>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md40 text-center">
                  <div class="pre-heading f-18 f-md-22 w600 white-clr lh140">
                     Never Waste Your Time & Money on Manually Writing Boring Content Again…
                  </div>
               </div>
               <div class="col-12 mt-md30 mt20">
               <div class="main-heading f-28 f-md-40 w400 text-center lh140 black2-clr red-hat-font  text-capitalize">
                 <span style="border-bottom:4px solid #fff">Futuristic A.I. Technology</span>  Creates Stunning Content for <span class="pink-clr w700"> Any Local or Online Niche 10X Faster…Just by Using a Keyword </span>
            </div>
               </div>
               <div class="col-12 mt20 text-center">
                  <div class="f-22 f-md-40 w700 lh140 white-clr post-heading">
                  And Makes $700-$800 Extra
                  </div>
               </div>
               <div class="col-12 mt-md30 mt20 text-center">
                  <div class="post-headline f-18 f-md-22 w700 text-center lh160 white-clr text-capitalize">
                     You Sit Back & Relax, LearnX Will Create Top Converting Content for You <br class="d-none d-md-block">  No Prior Skill Needed | No Monthly Fee Ever…
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md50 align-items-center">
               <div class="col-md-7 col-12 min-md-video-width-left">
                  <img src="assets/images/product-box.webp" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-md-5 col-12 min-md-video-width-right mt20 mt-md0">
                  <div class="proudly-list-bg">
                     <ul class="list-head pl0 m0 f-18 lh150 w500 black-clr text-capitalize">
                        <li><span class="w700">Create Content</span> for Any Local or Online Business</li>
                        <li><span class="w700"> Let Our Super Powerful</span> A.I. Engine Do the Heavy Lifting</li> 
                        <li><span class="w700">Preloaded with 50+ Copywriting Templates, </span> like Ads, Video Scripts, Website Content, and much more.</li> 
                        <li><span class="w700">Works in 35+ Languages</span>  and 22+ Tons of Writing</li>
                        <li><span class="w700">Download Your Content</span> in Word/PDF Format</li>
                        <li><span class="w700">Free Commercial License – </span>Sell Service to Local Clients for BIG Profits</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Header Section End -->
      <!-- Step Section Start -->
      <div class="step-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center f-28 f-md-50 w500 lh130 black-clr1">
                  The Writer’s A.I. Engine Will <span class="w700">Accomplish All 
                  The Marketing Content Needs In 3 Easy Steps</span> 
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-12 col-md-6 relative">
                  <div class="f-28 f-md-50 step-text pacifico gradient-orange">
                     Step #1   
                  </div>
                  <div class="f-28 f-md-65 w700 lh140 black-clr1 mt10">
                     Choose
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 black-clr1 mt10">
                     Choose from 35+ Use Cases, 22+ Tones, and Languages for which you want to create content. The LearnX Artificial intelligence is trained by highly professional copywriters to create top converting & plagiarism-free content.                     
                  </div>
                  <img src="assets/images/arrow-left.webp" class="img-fluid d-none ms-md-auto d-md-block" style="position: absolute; right: -140px; top: 340px;">
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                  <img src="assets/images/choose.webp" class="img-fluid d-block mx-auto" alt="Sales &amp; Profits">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md90">
               <div class="col-12 col-md-6 order-md-2 relative">
                  <div class="f-28 f-md-50 step-text pacifico gradient-orange">
                     Step #2 
                  </div>
                  <div class="f-28 f-md-65 w700 lh140 black-clr1 mt10">
                     Enter Keyword
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 black-clr1 mt10">
                     Enter your business name, & then choose purpose (sales copy, email, or ad etc) to create relative and effective content
                  </div>
                  <img src="assets/images/arrow-right.webp" class="img-fluid d-none me-md-auto d-md-block" style="position: absolute; top: 320px; left: -240px;">
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
                  <img src="assets/images/enter.webp" class="img-fluid d-block mx-auto" alt="Udemy, Udacity">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md90">
               <div class="col-12 col-md-6 relative">
                  <div class="f-28 f-md-50 step-text pacifico gradient-orange">
                     Step #3
                  </div>
                  <div class="f-28 f-md-65 w700 lh140 black-clr1 mt10">
                     Publish & Profit 
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 black-clr1 mt10">
                     LearnX will automatically create professional, fresh and 100% plagiarism-free content of your need in just a few seconds.                  
                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                  <img src="assets/images/create.webp" class="img-fluid d-block mx-auto" alt="Sales &amp; Profits">
               </div>
            </div>
            <div class="row mt20 mt-md90">
               <div class="col-12">
                  <div class="f-20 f-md-24 w700 lh140 text-center">
                     LearnX is Built for Everyone Across All Levels
                  </div>
                  <div class="mt20 d-flex gap-2 gap-md-4 justify-content-center flex-wrap">
                     <div class="d-flex align-items-center gap-3 justify-content-center">
                        <img src="https://cdn.oppyo.com/launches/writerarc/bizdrive/cross.webp" alt="Cross" class="img-fluid d-block">
                        <div class="f-md-26 w700 lh140 f-20 red-clr">No installation</div>
                     </div>
                     <div class="d-flex align-items-center gap-3 justify-content-center">
                        <img src="https://cdn.oppyo.com/launches/writerarc/bizdrive/cross.webp" alt="Cross" class="img-fluid d-block">
                        <div class="f-md-26 w700 lh140 f-20 red-clr">No Other Techie Stuff</div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Step Section End -->
      <div class="grey-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-40 f-28 w700 lh140 text-center black-clr2">   
                  And Making  Profits Like This      
               </div>
               <div class="col-12 mx-auto mt20 mt-md50">
                  <img src="assets/images/proof1.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>
         </div>
      </div>
      <!-- CTA Btn -->
      <div class="cta-section-new">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-20 f-md-26 w600 lh140 text-center white-clr">
                     Hurry! Limited Time Only. No Monthly Fees
                  </div>
                  <div class="f-16 f-md-20 lh140 w400 text-center mt10 white-clr">
                     Use Coupon Code <span class="w700 orange-clr">LearnX</span> for an Additional <span class="w700 orange-clr"> $3 Discount</span>
                  </div>
                  <div class="row">
                     <div class="col-12 col-md-7 mx-auto mt20">
                        <div class="f-20 f-md-35 text-center probtn1">
                           <a href="#buynow" class="cta-link-btn d-block">Get Instant Access To LearnX</a>
                        </div>
                        <img src="https://cdn.oppyo.com/launches/writerarc/bizdrive/payment-options.webp" class="img-fluid mx-auto d-block mt20">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- CTA Btn End -->
      
      <!-- Features List Section End -->
      <!-- Proven Section -->
      <div class="proof-section">
         <div class="container">
            <div class="row">
               <div class="f-md-28 f-22 w500 lh140 text-capitalize text-center black-clr col-12">
                  Let’s agree on one thing… 
               </div>
               <div class="w700 f-md-38 f-28 lh140 text-capitalize text-center black-clr col-12">
                  Today’s World Has Become Too Packed With Old, Repetitive <br class="d-none d-md-block"> & Boring Content!
               </div>
               <div class="f-20 f-md-22 w500 lh140 black-clr text-center mt10">
                 <i>So, having fresh, engaging & better content than your opposition <br class="d-none d-md-block">
                  has become important for every business. Because…</i>  
               </div>
               <div class="col-12 text-center mt20 mt-md40">
                  <div class="reason-box">
                     <div class="f-24 f-md-38 w700 lh140 blue-clr2">
                        Content is a Backbone for Every Form of Marketing…
                     </div>
                     <div class="f-18 f-md-20 w500 lh140 black-clr">
                        Whatever you do to promote your business, whether Social Media Posts, Facebook & Google Ads, YouTube Marketing, Blogging, Email Marketing, or having Landing Page or Website. You need Unique, Engaging, <br class="d-none d-md-block"> and SEO-friendly content. 

                     </div>
                  </div>
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md50">
               <div class="col-12 col-md-6">
                  <!-- <img src="assets/images/proof1.webp" class="img-fluid d-block mx-auto"> -->
                  <div class="f-md-28 f-24 w600 black-clr lh140">
                     And according to Forbes, Here Are Top 10 Reasons Why Content Matters 
                  </div>
                  <ul class="agree-list f-md-18 f-16 w400 mb0 mt20 lh140 black-clr">
                     <li>Help in building customer’s trust</li>
                     <li>Makes you look like an expert</li>
                     <li>You can build a library of content that helps in branding</li>
                     <li>Content creation is a key element of SEO</li>
                     <li>Customers like helpful data and are more likely to follow your brand</li>
                     <li>Content is an excellent and core part of your marketing campaign</li>
                     <li>Establishes your Brand </li>
                     <li>Content is a great way to spread your brand and increase market share</li>
                     <li>Helps in quality lead generation</li>
                     <li>Drives more and more web traffic</li>
                  </ul>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                  <img src="assets/images/proof2.webp" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 f-md-22 f-20 w500 lh140 text-center mt15 mt-md30">
                  And that’s the reason why businesses capitalize on content that is <span class="w700">above 70% of the Total Web Traffic</span>
               </div>
            </div>
         </div>
      </div>
      <!-- Proven Section End -->

       <!-- Old & Boring Content Section Start -->
       <section class="old-content-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="old-content-box ">
                     <div class="f-24 f-md-38 w500 lh140">And You Would Agree That </div>
                     <div class="f-md-65 f-38 w800 lh120 mt15">OLD & BORING CONTENT 
                        <br class="d-none d-md-block">IS NOT WORKING IN 2022! </div>
                  </div>
               </div>
               <div class="col-12">
                  <div class="f-18 f-md-20 w500 white-clr lh140 mt50 mt-md60 text-center">
                     Those are not working on social media, not on YouTube, and not even worth publishing on your <br class="d-none d-md-block">
                     own website or blogs. And that’s the reason why you don’t get FREE traffic, lose<br class="d-none d-md-block">
                     sales &revenue, and must think of PAID traffic methods, my friend.<br class="d-none d-md-block">
                  </div>
               </div>
               <div class="col-12 text-center">
                  <div class="old-headline f-md-28 f-24 w600 lh140 white-clr mt-md30 mt20 ">
                     But before revealing our secret. Let me ask you a question!
                  </div>
               </div>
               <div class="col-12">
                  <div class="w700 f-md-38 f-24 lh140 yellow-clr mt-md30 mt20 text-center">
                     How many times have you clicked & read these <br class="d-none d-md-block">
                     amazing headlines?
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md50">
               <div class="col-12 col-md-8">
                  <img src="assets/images/seo.webp" alt="SEO" class="img-fluid mx-auto d-block">
               </div>
               <div class="col-12 col-md-4">
                  <img src="assets/images/elon.webp" alt="Elon" class="img-fluid mx-auto d-block">
               </div>
               <div class="col-12 mt20 mt-md30">
                  <div class="f-20 f-md-24 w500 lh140 white-clr text-center">
                     We all have clicked & read such trending posts while surfing online regularly. <br class="d-none d-md-block">
                     <u>People only want fresh & unique content, news & articles. This is working like a miracle.</u>
                  </div>
                  <div class="f-24 f-md-38 w700 lh140 yellow-clr text-center mt20 mt-md30">Fresh & Unique Content is The WINNER In 2022 & Beyond...</div>
               </div>
            </div>
         </div>
      </section>
      <!-- Old & Boring Content Section End -->
  <!-- Business Section Start  -->
      <section class="business-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-24 f-md-32 w700 lh140 black-clr text-center">
                     Now, it is safe to say that if you want to grow your business you
                     must create content better than your opposition.
                  </div>
               </div>
               <div class="col-12 text-center mt20 mt-md40">
                  <div class="reason-box">
                     <div class="f-28 f-md-45 w800 lh140 black-clr">
                        Out of 550,000 New Business Every Month
                     </div>
                     <div class="f-24 f-md-32 w700 lh140 black-clr text-center mt10">
                        <span class="blue-clr2">90% of Businesses</span>  Embrace <span class="blue-clr2">Content Marketing</span>  <br class="d-none d-md-block">
                        for their Growth.
                     </div>
                  </div>
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md50">
               <div class="col-12 col-md-6">
                  <div class="f-20 f-md-22 w400 lh140 black-clr text-center text-md-start">
                     However, even after so many efforts, and investing
                     time and money in copywriting, business owners do
                     not get the desired results
                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                  <img src="assets/images/invest.webp" alt="Invest" class="img-fluid mx-auto d-block">
               </div>
               <div class="col-12 text-center mt20 mt-md50">
                  <div class="billion-content">
                     <div class="f-24 f-md-36 w800 lh140 black-clr">
                        This Opens the Door to the $413 Billion Content <br class="d-none d-md-block">
                        Marketing Industry
                     </div>
                  </div>
                  <div class="f-22 f-md-28 w400 lh140 black-clr mt20 mt-md30">
                     and it’s a huge opportunity for freelance copywriters and newbies <br class="d-none d-md-block">
                     who are trying to build a regular income source.
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- Business Section End -->
      
      <div class="wearenot-alone-section ">
         <div class="container ">
            <div class="row mx-0 ">
               <div class="col-12 col-md-10 mx-auto alone-bg ">
                  <div class="row align-items-center">
                     <div class="col-md-1 ">
                        <img src="assets/images/dollars-image.webp " class="img-fluid d-block dollars-sm-image ">
                     </div>
                     <div class="col-md-11 f-28 f-md-45 w700 lh140 white-clr text-center mt15 mt-md0 ">You can build a profitable 6-figure Agency by just serving 1 client a day.
                     </div>
                  </div>
               </div>
               <div class="col-12 px10 f-28 f-md-45 w600 lh140 black-clr text-center mt20 mt-md80 ">Here’s a simple math</div>
               <div class="col-12 mt20 mt-md50 ">
                  <img src="assets/images/make-icon.webp " class="img-fluid d-none d-md-block mx-auto ">
                  <img src="assets/images/make-icon-xs.webp" alt="Make Icon" class="img-fluid d-block d-md-none mx-auto">
               </div>
               <div class="col-12 mt20 mt-md70">
                  <div class="f-md-45 f-28 w600 lh140 text-center white-clr">
                     <span class="title-shape">And It’s Not A 1-Time Income</span>
                  </div>
               </div>
               <div class="col-12 f-20 f-md-24 w400 lh140 mt40 mt-md40 text-center ">
                  You can do this for life and charge your new as well as existing clients for your <br class="d-none d-md-block">
                  services again and again forever.                  
               </div>
            </div>
         </div>
      </div>
      <div class="potential-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-34 f-24 w700 lh150 black-clr text-center">
                  Here’re some examples of freelance copywriters on Fiverr, Odesk, Freelancers, etc who are making 5-6 figures income by helping businesses with their content.
               </div>
            </div>
            <div class="row mt-md50 mt20 align-items-center">
               <div class="col-md-7">
                  <img src="assets/images/meet-smith.webp" alt="" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-md-5 f-md-20 f-18 w400 mt-md0 mt20">
                  <b>Meet Smith</b> has made $10,000 on Fiverr in just 2 months by providing 
                  copywriting services to local businesses
               </div>
            </div>
            <div class="row mt-md50 mt20 align-items-center">
               <div class="col-md-7 order-md-2">
                  <img src="assets/images/boomer-dock.webp" alt="" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-md-5 f-md-20 f-18 w400 mt-md0 mt20 order-md-1">
                  <b>Boomer Dock</b> has made almost $28k on Freelancer and has completed 
                  Over 18 successfully completed Freelancer projects in under 2 months.  
               </div>
            </div>
            <div class="row mt-md0 mt20 align-items-center">
               <div class="col-md-6">
                  <img src="assets/images/bright-brian.webp" alt="" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-md-6 f-md-20 f-18 w400 mt-md0 mt20">
                  <b> Bright Brian</b> has earned almost $17,500 in just 47 Days of his start 
                  as a content writer on Upwork.  
               </div>
            </div>
            
         </div>
      </div>
      <!-- Problem Section End -->
       <!-- Proudly Section -->
       <div class="proudly-sec" id="product">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center lh140">
                  <div class="per-shape f-28 f-md-34 w700 white-clr">
                     Introducing… 
                  </div>
              </div>
               <div class="col-12 mt-md30 mt20 mt-md40 text-center">
               <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 484.58 200.84" style="max-height:100px;">
                        <defs>
                            <style>
                                .cls-1,.cls-4{fill:#fff;}
                                .cls-2,.cls-3,.cls-4{fill-rule:evenodd;}
                                .cls-2{fill:url(#linear-gradient);}
                                .cls-3{fill:url(#linear-gradient-2);}
                                .cls-5{fill:url(#linear-gradient-3);}
                            </style>
                            <linearGradient id="linear-gradient" x1="-154.67" y1="191.86" x2="249.18" y2="191.86" gradientTransform="matrix(1.19, -0.01, 0.05, 1, 178.46, -4.31)" gradientUnits="userSpaceOnUse">
                                <stop offset="0" stop-color="#ead40c"></stop>
                                <stop offset="1" stop-color="#ff00e9"></stop>
                            </linearGradient>
                            <linearGradient id="linear-gradient-2" x1="23.11" y1="160.56" x2="57.61" y2="160.56" gradientTransform="matrix(1, 0, 0, 1, 0, 0)" xlink:href="#linear-gradient"></linearGradient>
                            <linearGradient id="linear-gradient-3" x1="384.49" y1="98.33" x2="483.38" y2="98.33" gradientTransform="matrix(1, 0, 0, 1, 0, 0)" xlink:href="#linear-gradient"></linearGradient>
                        </defs>
                        <g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1">
                            <path class="cls-1" d="M112.31,154.3q-11.65,0-20.1-4.87a32.56,32.56,0,0,1-13-13.82q-4.53-9-4.54-21.11a46.06,46.06,0,0,1,4.57-21A34.29,34.29,0,0,1,92,79.37a36.11,36.11,0,0,1,19.31-5.06,39.65,39.65,0,0,1,13.54,2.29,31,31,0,0,1,11.3,7.09,33.32,33.32,0,0,1,7.75,12.18,49.23,49.23,0,0,1,2.82,17.57V119H83.26v-12.3h46A19.87,19.87,0,0,0,127,97.38a16.6,16.6,0,0,0-6.18-6.48,17.59,17.59,0,0,0-9.21-2.37,17.88,17.88,0,0,0-9.83,2.7,18.81,18.81,0,0,0-6.58,7.06,20.17,20.17,0,0,0-2.4,9.56v10.74a25.15,25.15,0,0,0,2.47,11.57,17.42,17.42,0,0,0,6.91,7.37,20.52,20.52,0,0,0,10.39,2.55,21.62,21.62,0,0,0,7.22-1.14,15.71,15.71,0,0,0,5.59-3.35,14.11,14.11,0,0,0,3.59-5.5L146,132a26.44,26.44,0,0,1-6.13,11.77,29.72,29.72,0,0,1-11.52,7.77A43.74,43.74,0,0,1,112.31,154.3Z"></path>
                            <path class="cls-1" d="M184.49,154.35a31.78,31.78,0,0,1-13.24-2.65,21.26,21.26,0,0,1-9.28-7.84,22.86,22.86,0,0,1-3.41-12.81A21.92,21.92,0,0,1,161,120.2a18.9,18.9,0,0,1,6.61-6.86,33.85,33.85,0,0,1,9.46-3.9,77.76,77.76,0,0,1,10.92-2q6.81-.7,11-1.28a15.91,15.91,0,0,0,6.18-1.82,4.25,4.25,0,0,0,1.94-3.86v-.3q0-5.7-3.38-8.83T194,88.28q-6.71,0-10.62,2.92a14.55,14.55,0,0,0-5.27,6.91l-17-2.42a27.51,27.51,0,0,1,6.66-11.83,29.46,29.46,0,0,1,11.35-7.16,43.73,43.73,0,0,1,14.83-2.39,47.89,47.89,0,0,1,11.14,1.31,31.6,31.6,0,0,1,10.14,4.31,22.12,22.12,0,0,1,7.39,8.14q2.81,5.15,2.8,12.87v51.85H207.84V142.14h-.61a22.1,22.1,0,0,1-4.66,6,22.35,22.35,0,0,1-7.52,4.49A30,30,0,0,1,184.49,154.35Zm4.74-13.42a19.7,19.7,0,0,0,9.53-2.19,16,16,0,0,0,6.23-5.83,15,15,0,0,0,2.19-7.91v-9.13a8.72,8.72,0,0,1-2.9,1.31,45.56,45.56,0,0,1-4.56,1.06c-1.68.3-3.35.57-5,.8l-4.29.61a32,32,0,0,0-7.32,1.81A12.29,12.29,0,0,0,178,125a9.76,9.76,0,0,0,1.82,13.39A16,16,0,0,0,189.23,140.93Z"></path>
                            <path class="cls-1" d="M243.8,152.79V75.31h17.7V88.23h.81a19.36,19.36,0,0,1,7.29-10.37,20,20,0,0,1,11.83-3.66c1,0,2.14,0,3.4.13a23.83,23.83,0,0,1,3.15.38V91.5a21,21,0,0,0-3.65-.73,38.61,38.61,0,0,0-4.82-.32,18.47,18.47,0,0,0-8.95,2.14,15.94,15.94,0,0,0-6.23,5.93,16.55,16.55,0,0,0-2.27,8.72v45.55Z"></path>
                            <path class="cls-1" d="M318.4,107.39v45.4H300.14V75.31h17.45V88.48h.91a22,22,0,0,1,8.55-10.34q5.86-3.84,14.55-3.83a27.58,27.58,0,0,1,14,3.43,23.19,23.19,0,0,1,9.28,9.93,34.22,34.22,0,0,1,3.26,15.79v49.33H349.87V106.28c0-5.17-1.34-9.23-4-12.15s-6.37-4.39-11.07-4.39a17,17,0,0,0-8.5,2.09,14.67,14.67,0,0,0-5.8,6A20,20,0,0,0,318.4,107.39Z"></path>
                            <path class="cls-2" d="M314.05,172.88c12.69-.14,27.32,2.53,40.82,2.61l-.09,1c1.07.15,1-.88,2-.78,1.42-.18,0,1.57,1.82,1.14.54-.06.08-.56-.32-.68.77,0,2.31.17,3,1.26,1.3,0-.9-1.34.85-.89,6,1.63,13.39,0,20,3.2,11.64.72,24.79,2.38,38,4.27,2.85.41,5.37,1.11,7.57,1.05,2-.05,5.68.78,8,1.08,2.53.34,5.16,1.56,7.13,1.65-1.18-.05,5.49-.78,4.54.76,5-.72,10.9,1.67,15.94,1.84.83-.07.69.53.67,1,1.23-1.17,3.32.56,4.28-.56.25,2,2.63-.48,3.3,1.61,2.23-1.39,4.83.74,7.55,1.37,1.91.44,5.29-.21,5.55,2.14-2.1-.13-5.12.81-11.41-1.09,1,.87-3.35.74-3.39-.64-1.81,1.94-6.28-1-7.79,1.19-1.26-.41,0-.72-.64-1.35-5,.55-8.09-1.33-12.91-1.56,2.57,2.44,6.08,3.16,10.06,3.22,1.28.15,0,.74,1,1.39,1.37-.19.75-.48,1.26-1.17,5.37,2.28,15.36,2.35,21,4.91-4.69-.63-11.38,0-15.15-2.09A148,148,0,0,1,441.58,196c-.53-.1-1.81.12-.7-.71-1.65.73-3.41.1-5.74-.22-37.15-5.15-80.47-7.78-115.75-9.76-39.54-2.22-76.79-3.22-116.44-2.72-61.62.78-119.59,7.93-175.21,13.95-5,.55-10,1.49-15.11,1.47-1.33-.32-2.46-1.91-1.25-3-2-.38-2.93.29-5-.15a9.83,9.83,0,0,1-1.25-3,13.73,13.73,0,0,1,6.42-2.94c.77-.54.12-.8.15-1.6a171,171,0,0,1,45.35-8.58c26.43-1.1,53.68-4.73,79.64-6,6.25-.3,11.72.06,18.41.14,8.31.1,19.09-1,29.19-.12,6.25.54,13.67-.55,21.16-.56,39.35-.09,71.16-.23,112.28,2C317.24,171.93,315.11,173.81,314.05,172.88Zm64.22,16.05-1.6-.2C376.2,190,378.42,190.26,378.27,188.93Z"></path>
                            <polygon class="cls-3" points="52.57 166.06 57.61 151.99 23.11 157.23 32.36 169.12 52.57 166.06"></polygon>
                            <polygon class="cls-4" points="44.69 183.24 51.75 168.35 33.86 171.06 44.69 183.24"></polygon>
                            <path class="cls-4" d="M.12,10.15l22,145.06,35.47-5.39-22-145a2.6,2.6,0,0,0,0-.4C35,.76,26.62-.95,16.81.54S-.52,6.16,0,9.77A2.62,2.62,0,0,0,.12,10.15Z"></path>
                            <path class="cls-5" d="M433.68,79.45l21-36.33h27.56L448.48,97.51l34.9,56H456l-22-37.76-21.94,37.76H384.49l34.9-56L385.72,43.12h27.35Z"></path>
                        </g>
                            </g>
                    </svg>
               </div>
               <div class="col-12 mt20 mt-md50 f-24 f-md-30 text-center white-clr lh140 w500">
                  Breath-Taking A.I. Based Content Creator That Smartly Creates Any Type of  <br class="d-none d-md-block">
                  Marketing Content for Any Local or Online Niche with Just a Click of a Button 
               </div>
            </div>
            <div class="row mt30 mt-md50 align-items-center">
               <div class="col-12 col-md-12">
                  <img src="assets/images/product-box.webp" class="img-fluid d-block mx-auto img-animation">
               </div>
               <div class="f-18 f-md-20 w400 lh140 white-clr mt20 text-center">
                  A Better and 10X Faster Way to Write Copies. <br>
                  So, stop wasting time and money on content and copywriting anymore. As, now you can ensure the growth of your <br class="d-none d-md-block"> business and embrace content marketing using the power of Artificial Intelligence. 
               </div>
            </div>
            <div class="prouldly-li-box">
               <div class="row mt20 mt-md30 g-0">
                  <div class="col-12 col-md-6">
                     <div class="boxbg boxborder-rb boxborder top-first ">
                        <div class="f-md-20 f-18 w400 lh140 white-clr">
                        Without Investing Time & Money in learning <br class="d-none d-md-block">
                        copywriting.
                        </div>
                     </div>
                     <div class="boxbg boxborder-r boxborder">
                        <div class="f-md-20 f-18 w400 lh140 white-clr">
                        Without Indulging yourself in Hard Work and getting <br class="d-none d-md-block"> Frustrated.  
                        </div>
                     </div>
                  </div>
                  <div class="col-12 col-md-6">
                     <div class="boxbg boxborder-b boxborder ">
                        <div class="f-md-20 f-18 w400 lh140 white-clr">
                        Without Wasting countless weeks in Researching & <br class="d-none d-md-block"> Writing yourself. 
                        </div>
                     </div>
                     <div class="boxbg boxborder ">
                        <div class="f-md-20 f-18 w400 lh140 white-clr">
                        Without Spending thousands of dollars on Hiring <br class="d-none d-md-block"> Copywriting Experts.
                        </div>
                     </div>
                  </div>
               </div>   
            </div>
         </div>
      </div>
      <!-- Proudly Section End -->

      <!-- Features Headline Start -->
      <div class="features-headline" id="features">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-50 w700 lh140 white-clr">
                     Checkout The Power Packed Features <br class="d-none d-md-block"> of This Disruptive A.I. Technology
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Features Headline End -->
      <img src="assets/images/down-arrow.webp" class="img-fluid mx-auto d-none d-md-block vert-move" alt="Arrow" style="margin-top:-35px;">
      <!-- Features 1 Section Start -->
      <div class="features-one">
         <div class="container">
            <div class="row align-items-center flex-wrap">
               <div class="col-12 col-md-6">
                  <div class="f-24 f-md-36 w700 lh140 black-clr text-capitalize">
                     Create Fresh & SEO-Friendly Copies 
                  </div>
                  <div class="f-18 f-md-20  w400 lh140 mt10 text-left">                
                     LearnX A.I. Engine Generate 100% Fresh and Unique Copy 
                     which is search engine optimized and easy to rank.  
                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                  <img src="assets/images/f1.webp" class="img-fluid mx-auto d-block" alt="Feature 1">
               </div>
            </div>
         </div>
      </div>
      <!-- Features 1 Section End -->
      <div class="potential-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-24 f-md-36 w700 lh140 black-clr text-capitalize">
                     Create Content for Any Local or Online Niche 
                  </div>
                  <div class="f-18 f-md-20  w400 lh140 mt10 text-left">                
                     LearnX has tons of predefined Local and Online business categories with <br class="d-none d-md-block">
                     predefine keywords for 36+ Niches. So, you can create content with just a click of a button. 
                  </div>
                  <div class="f-18 f-md-20 w700 lh140 black-clr text-capitalize mt20">
                     Here is the List of Potential Use Cases LearnX can be Used For.
                  </div>
               </div>
            </div>
            <div class="row mt-md50 mt20">
               <div class="col-md-2 col-6">
                  <div class="list-shape">
                     <img src="assets/images/pn1.webp" class="img-fluid mx-auto d-block" alt="Business Coaches">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Business Coaches
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt0">
                  <div class="list-shape">
                     <img src="assets/images/pn2.webp" class="img-fluid mx-auto d-block" alt="Courses Sellers">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Courses Sellers
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn3.webp" class="img-fluid mx-auto d-block" alt="Architect">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Architect
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn4.webp" class="img-fluid mx-auto d-block" alt="Hotels">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Hotels
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn5.webp" class="img-fluid mx-auto d-block" alt="E-Book Sellers">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     E-Book Sellers
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn6.webp" class="img-fluid mx-auto d-block" alt="Yoga Classes">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Yoga Classes
                  </div>
               </div>
            </div>
            <div class="row mt-md50 mt0">
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn7.webp" class="img-fluid mx-auto d-block" alt="Painters &amp; Decorators">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Painters &amp; Decorators
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn8.webp" class="img-fluid mx-auto d-block" alt="Schools">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Schools
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn9.webp" class="img-fluid mx-auto d-block" alt="Freelancers">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Freelancers
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn10.webp" class="img-fluid mx-auto d-block" alt="Home Tutors">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Home Tutors
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn11.webp" class="img-fluid mx-auto d-block" alt="Computer Repair">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Computer Repair
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn12.webp" class="img-fluid mx-auto d-block" alt="Gym">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Gym
                  </div>
               </div>
            </div>
            <div class="row mt-md50 mt0">
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn13.webp" class="img-fluid mx-auto d-block" alt="Real Estate">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Real Estate
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn14.webp" class="img-fluid mx-auto d-block" alt="Dance &amp; Music Trainers">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Dance &amp; Music Trainers
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn15.webp" class="img-fluid mx-auto d-block" alt="Home Security">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Home Security
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn16.webp" class="img-fluid mx-auto d-block" alt="Dentists">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Dentists
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn17.webp" class="img-fluid mx-auto d-block" alt="E-Com Sellers">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     E-Com Sellers
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn18.webp" class="img-fluid mx-auto d-block" alt="Cooking Classes">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Cooking Classes
                  </div>
               </div>
            </div>
            <div class="row mt-md50 mt0">
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn19.webp" class="img-fluid mx-auto d-block" alt="Lock Smith">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Lock Smith
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn20.webp" class="img-fluid mx-auto d-block" alt="Lawyers">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Lawyers
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn21.webp" class="img-fluid mx-auto d-block" alt="Digital Product Sellers">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Digital Product Sellers
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn22.webp" class="img-fluid mx-auto d-block" alt="Affiliate Marketers">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Affiliate Marketers
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn23.webp" class="img-fluid mx-auto d-block" alt="Veterinarians">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Veterinarians
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn24.webp" class="img-fluid mx-auto d-block" alt="Financial Adviser">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Financial Adviser
                  </div>
               </div>
            </div>
            <div class="row mt-md50 mt0">
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn25.webp" class="img-fluid mx-auto d-block" alt="YouTubers">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     YouTubers
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn26.webp" class="img-fluid mx-auto d-block" alt="Digital Marketers">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Digital Marketers
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn27.webp" class="img-fluid mx-auto d-block" alt="Florist">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Florist
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn28.webp" class="img-fluid mx-auto d-block" alt="Dermatologists">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Dermatologists
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn29.webp" class="img-fluid mx-auto d-block" alt="Car &amp;Taxi Services">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Car &amp; Taxi Services
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn30.webp" class="img-fluid mx-auto d-block" alt="Plumbers">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Plumbers
                  </div>
               </div>
            </div>
            <div class="row mt-md50 mt0">
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn31.webp" class="img-fluid mx-auto d-block" alt="Sports Clubs">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Sports Clubs
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn32.webp" class="img-fluid mx-auto d-block" alt="Counsellors">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Counsellors
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn33.webp" class="img-fluid mx-auto d-block" alt="Garage Owners">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Garage Owners
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn34.webp" class="img-fluid mx-auto d-block" alt="Carpenter">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Carpenter
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn35.webp" class="img-fluid mx-auto d-block" alt="Bars &amp; Restaurants">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Bars &amp; Restaurants
                  </div>
               </div>
               <div class="col-md-2 col-6 mt-md0 mt30">
                  <div class="list-shape">
                     <img src="assets/images/pn36.webp" class="img-fluid mx-auto d-block" alt="Chiropractors">
                  </div>
                  <div class="f-18 lh140 w600 black-clr text-center mt5">
                     Chiropractors
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Features 3 Section Start -->
      <div class="features-three">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-24 f-md-36 w700 lh140 white-clr text-capitalize">
                     Advance Copywriting Templates 
                  </div>
                  <div class="f-18 f-md-20  w400 lh140 mt10 text-left white-clr">                
                     LearnX is trained by highly experienced copywriters, powered with 20 Billion plus data samples and has 50+ advanced pre-defined copywriting templates based on different use cases like Google Ads, Facebook Ads, Blogs, Sales Copy, YouTube descriptions, Video Scripts, Website Content, and much more.
                  </div>
               </div>
            </div>
            <div class="row row-cols-1 row-cols-md-3 mt-md50">
               <div class="col">
                  <div class="feature-blog">
                     <img src="assets/images/ic1.webp" class="img-fluid" alt="">
                     <div class="f-18 f-md-20 w400 lh150 white-clr">
                        Blog Ideas
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-blog">
                     <img src="assets/images/ic2.webp" class="img-fluid" alt="">
                     <div class="f-18 f-md-20 w400 lh150 white-clr">
                        Brand Description
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-blog">
                     <img src="assets/images/ic3.webp" class="img-fluid" alt="">
                     <div class="f-18 f-md-20 w400 lh150 white-clr">
                        Copywriting Framework
                        AIDA & PAS
                     </div>
                  </div>
               </div>
            </div>
            <div class="row row-cols-1 row-cols-md-3">
               <div class="col">
                  <div class="feature-blog">
                     <img src="assets/images/ic4.webp" class="img-fluid" alt="">
                     <div class="f-18 f-md-20 w400 lh150 white-clr">
                        Promotional & Newsletter Emails
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-blog">
                     <img src="assets/images/ic5.webp" class="img-fluid" alt="">
                     <div class="f-18 f-md-20 w400 lh150 white-clr">
                        Social Media Captions
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-blog">
                     <img src="assets/images/ic6.webp" class="img-fluid" alt="">
                     <div class="f-18 f-md-20 w400 lh150 white-clr">
                        Facebook Ads
                     </div>
                  </div>
               </div>
            </div>
            <div class="row row-cols-1 row-cols-md-3">
               <div class="col">
                  <div class="feature-blog">
                     <img src="assets/images/ic7.webp" class="img-fluid" alt="">
                     <div class="f-18 f-md-20 w400 lh150 white-clr">
                        Google Ads
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-blog">
                     <img src="assets/images/ic8.webp" class="img-fluid" alt="">
                     <div class="f-18 f-md-20 w400 lh150 white-clr">
                        Social Media Posts
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-blog">
                     <img src="assets/images/ic9.webp" class="img-fluid" alt="">
                     <div class="f-18 f-md-20 w400 lh150 white-clr">
                        Product Description
                     </div>
                  </div>
               </div>
            </div>
            <div class="row row-cols-1 row-cols-md-3">
               <div class="col">
                  <div class="feature-blog">
                     <img src="assets/images/ic10.webp" class="img-fluid" alt="">
                     <div class="f-18 f-md-20 w400 lh150 white-clr">
                        Product Reviews
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-blog">
                     <img src="assets/images/ic11.webp" class="img-fluid" alt="">
                     <div class="f-18 f-md-20 w400 lh150 white-clr">
                        Testimonials
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="feature-blog">
                     <img src="assets/images/ic12.webp" class="img-fluid" alt="">
                     <div class="f-18 f-md-20 w400 lh150 white-clr">
                        And Much More
                     </div>
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md50">
               <div class="col-12">
                  <div class="but-design">
                     <div class="f-24 f-md-38 w700 black-clr text-center lh140">
                        Here are some examples of what LearnX can do for you
                     </div>
                  </div>
               </div>
            </div>
            <div class="row row-cols-1 mt50 mt-md50">
               <div class="col">
                  <img src="assets/images/te1.webp" class="img-fluid mx-auto d-block" alt="">
               </div>
            </div>
         </div>
      </div>
      <!-- Features 3 Section End -->
      <!-- Features 4 Section Start -->
      <div class="features-four">
         <div class="container">
            <div class="row align-items-center flex-wrap">
               <div class="col-12 col-md-6">
                  <div class="f-24 f-md-36 w700 lh140 black-clr text-capitalize">
                     35+ Languages & 22+ Tones 
                  </div>
                  <div class="f-18 f-md-20  w400 lh140 mt10 text-left">                
                     LearnX can write copy in 35+ languages and over 
                     22+ tones of voice available to fit your business need.  
                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                  <img src="assets/images/f4.webp" class="img-fluid mx-auto d-block" alt="Feature 1">
               </div>
            </div>
            <div class="row align-items-center flex-wrap mt20 mt-md70">
               <div class="col-12 col-md-6 order-md-2">
                  <div class="f-24 f-md-36 w700 lh140 black-clr text-capitalize">
                     Generate 1000 Write Up Monthly 
                  </div>
                  <div class="f-18 f-md-20  w400 lh140 mt10 text-left">                
                     You can generate as many as up to 1000 Copies each 
                     month through LearnX. 
                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
                  <img src="assets/images/f5.webp" class="img-fluid mx-auto d-block" alt="Feature 1">
               </div>
            </div>
            <div class="row align-items-center flex-wrap mt20 mt-md70">
               <div class="col-12 col-md-6">
                  <div class="f-24 f-md-36 w700 lh140 black-clr text-capitalize">
                     Social Media Integration 
                  </div>
                  <div class="f-18 f-md-20  w400 lh140 mt10 text-left">                
                     LearnX provides Social Media Integration to 
                     Directly Share Your posts on Social Media
                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                  <img src="assets/images/f6.webp" class="img-fluid mx-auto d-block" alt="Feature 1">
               </div>
            </div>
            <div class="row align-items-center flex-wrap mt20 mt-md70">
               <div class="col-12 col-md-6 order-md-2">
                  <div class="f-24 f-md-36 w700 lh140 black-clr text-capitalize">
                     Download Your Content
                  </div>
                  <div class="f-18 f-md-20  w400 lh140 mt10 text-left">                
                     You can easily download your content in Word or PDF format
                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
                  <img src="assets/images/f7.webp" class="img-fluid mx-auto d-block" alt="Feature 1">
               </div>
            </div>
            <div class="row align-items-center flex-wrap mt20 mt-md70">
               <div class="col-12 col-md-6">
                  <div class="f-24 f-md-36 w700 lh140 black-clr text-capitalize">
                     Commercial License Included 
                  </div>
                  <div class="f-18 f-md-20  w400 lh140 mt10 text-left">                
                     LearnX comes with Commercial use license so that you can use 
                     LearnX for commercial purposes and sell copies to your clients.
                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                  <img src="assets/images/commercial-license-img.webp" class="img-fluid mx-auto d-block" alt="Feature 1">
               </div>
            </div>
         </div>
      </div>
      <!-- Features 4 Section End -->
      <!-- CTA  Section Start -->
      <div class="cta-section-new">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-20 f-md-26 w600 lh140 text-center white-clr">
                     Hurry! Limited Time Only. No Monthly Fees
                  </div>
                  <div class="f-16 f-md-20 lh140 w400 text-center mt10 white-clr">
                     Use Coupon Code <span class="w700 orange-clr">LearnX</span> for an Additional <span class="w700 orange-clr"> $3 Discount</span>
                  </div>
                  <div class="row">
                     <div class="col-12 col-md-7 mx-auto mt20">
                        <div class="f-20 f-md-35 text-center probtn1">
                           <a href="#buynow" class="cta-link-btn d-block">Get Instant Access To LearnX</a>
                        </div>
                        <img src="https://cdn.oppyo.com/launches/writerarc/bizdrive/payment-options.webp" class="img-fluid mx-auto d-block mt20">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- CTA  Section End -->
      <!-- Suport Section Start -->
      <div class="support-section">
         <div class="container ">
            <div class="row ">
               <div class="col-12 ">
                  <div class="f-md-50 f-28 w700 lh140 black-clr text-center ">
                     Here’re Some More Features
                  </div>
               </div>
            </div>
            <div class="row row-cols-md-3 row-cols-xl-3 row-cols-2">
               <div class="col mt20 mt-md30">
                  <div class="feature-wrapper">
                     <img src="assets/images/k1.webp" alt="Round-the-Clock Support" class="img-fluid">
                     <div class="white-clr lh140 f-18 f-md-20 w500">Round-the-Clock <br class="d-none d-md-block"> Support</div>
                  </div>
               </div>
               <div class="col mt20 mt-md30">
                  <div class="feature-wrapper">
                     <img src="assets/images/k2.webp" alt="No Technical Skills Required" class="img-fluid">
                     <div class="white-clr lh140 f-18 f-md-20 w500">No Coding, Design or <br class="d-none d-md-block">
                        Technical Skills Required
                     </div>
                  </div>
               </div>
               <div class="col mt20 mt-md30">
                  <div class="feature-wrapper">
                     <img src="assets/images/k3.webp" alt="Regular Updates" class="img-fluid">
                     <div class="white-clr lh140 f-18 f-md-20 w500">Regular Updates</div>
                  </div>
               </div>
               <div class="col mt20 mt-md30">
                  <div class="feature-wrapper">
                     <img src="assets/images/k4.webp" alt="Training & Tutorial Video" class="img-fluid">
                     <div class="white-clr lh140 f-18 f-md-20 w500">Complete Step-by-Step Video <br class="d-none d-md-block"> Training And Tutorials Included</div>
                  </div>
               </div>
               <div class="col mt20 mt-md30">
                  <div class="feature-wrapper">
                     <img src="assets/images/k5.webp" alt="Cloud-Based Software" class="img-fluid">
                     <div class="white-clr lh140 f-18 f-md-20 w500">Cloud-Based Software</div>
                  </div>
               </div>
               <div class="col mt20 mt-md30">
                  <div class="feature-wrapper">
                     <img src="assets/images/k6.webp" alt="Limited Commercial License" class="img-fluid">
                     <div class="white-clr lh140 f-18 f-md-20 w500">Limited Commercial License</div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Suport Section End -->
      <!-- Compare Table Section -->
      <section class="comp-tablse">
         <div class="comparetable-section">
            <div class="container ">
               <div class="row ">
                  <div class="col-12">
                     <div class="f-md-38 f-28 w700 lh140 text-capitalize text-center black-clr">
                        A.I. Based Content Creation Technology in your hands… 
                     </div>
                     <div class="f-md-22 f-20 w500 lh140 text-capitalize text-center black-clr mt20">
                        Create and Export All Type of Marketing Content with Just a Click of a Button Effortlessly. 
                     </div>
                  </div>
               </div>
               <div class="row mt20 mt-md50 row-cols-1 row-cols-md-2">
                  <div class="col relative">
                     <div class="compare-red-wrap">
                        <div class="f-md-28 f-22 w700 lh140 text-center"> <u>Without LearnX</u> </div>
                        <ul class="compare-red pl0 f-20 f-md-22 w400 lh160 black-clr text-capitalize mt20">
                           <li>Keep thinking where to start from</li>
                           <li>Old and copied content that fails to engage and rank on Google. </li>
                           <li>Less ROI due to non-performing ads, posts or emails</li>
                           <li>Wasting thousands of dollars on copywriting experts and still not getting desired results. </li>
                           <li>Lower brand reputation, lesser sales and leads. </li>
                        </ul>                        
                     </div>
                     <img src="assets/images/ai-robot1.webp" alt="A.I Robot" class="img-fluid mx-auto d-none d-md-block" style="position:absolute; top: 100px; left: -160px;">
                  </div>
                  <div class="col mt20 mt-md0 relative">
                     <div class="compare-red-wrap1">
                        <div class="f-md-28 f-22 w700 lh140 text-center"> <u>With LearnX</u> </div>
                        <ul class="compare-red1 pl0 f-20 f-md-22 w400 lh160 black-clr text-capitalize mt20">
                           <li>Never have to think where to start from. </li>
                           <li>Higher social media engagement, higher SEO ranking, lower bounce rate and more</li>
                           <li>High returns on Facebook & Google Ads</li>
                           <li>Manage all your marketing content under one dashboard.</li>
                           <li>Trained AI writes fresh and unique copy every time. </li>
                           <li>Write marketing content for any product or service in any niche</li>
                           <li>No fear of plagiarism. </li>
                           <li>No tech skills required, follow just 3 simple steps to get your content done.</li>
                        </ul>
                     </div>
                     <img src="assets/images/ai-robot2.webp" alt="A.I Robot" class="img-fluid mx-auto d-none d-md-block" style="position:absolute; top: 200px; right: -230px;">
                  </div>
               </div>
               <div class="row mt20 mt-md100">
                  <div class="col-12 col-md-12 no-comparison-shape">
                     <div class="text-capitalize f-md-45 f-28 w700 lh140 black-clr text-center text-md-start">
                        There is ABSOLUTELY NO Comparison of LearnX Writer at Any Price.
                     </div>
                     <div class="f-18 f-md-20 w400 lh140 mt20 text-center text-md-start">
                        That’s correct! There’s absolutely no match to the power packed LearnX. It’s built with years of experience, designing, coding, debugging & real-user-testing to help get you maximum results.<br><br>
                        When it comes to creating super engaging content for any business need, no other software even comes close! LearnX gives you the power to literally crush your competition so…<br><br>
                        Grab it today for this low one-time price because it will never be available again after launch at this price.
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- Compare Table Section End -->
      <div class="cta-section-new white-bg">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-20 f-md-26 w600 lh140 text-center white-clr">
                     Hurry! Limited Time Only. No Monthly Fees
                  </div>
                  <div class="f-16 f-md-20 lh140 w400 text-center mt10 white-clr">
                     Use Coupon Code <span class="w700 orange-clr">LearnX</span> for an Additional <span class="w700 orange-clr"> $3 Discount</span>
                  </div>
                  <div class="row">
                     <div class="col-12 col-md-7 mx-auto mt20">
                        <div class="f-20 f-md-35 text-center probtn1">
                           <a href="#buynow" class="cta-link-btn d-block">Get Instant Access To LearnX</a>
                        </div>
                        <img src="https://cdn.oppyo.com/launches/writerarc/bizdrive/payment-options.webp" class="img-fluid mx-auto d-block mt20">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
     

      <!-- Bonus Section Start -->
      <div class="bonus-section">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-12 text-center">
                  <div class="but-design">
                     <div class="f-24 f-md-45 w700 black-clr text-center lh140">
                        Plus You’ll Also Receive These Amazing Bonuses With Your Purchase Today…
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-12 mt50 mt-md60">
                  <div class="f-18 f-md-22 w400 lh140 text-center">
                     LearnX is already worth 10X the price we’re charging today. But just to sweeten the pot a bit more and over deliver, we’re going to include these special bonuses that will enhance your investment in LearnX
                  </div>
               </div>
               <div class="col-12 col-md-12 mt60 mt-md100">
                  <div class="bonus-section-shape text-center">
                     <div class="col-12 margin-t-60">
                        <div class="bonus-headline">
                           <div class="f-md-24 f-20 w600 white-clr text-nowrap text-center">
                              Bonus #1
                           </div>
                        </div>
                     </div>
                     <div class="row d-flex align-items-center flex-wrap mt15 mt-md30">
                        <div class="col-12 col-md-7 order-md-2">
                           <div class="w600 f-22 f-md-24 black-clr lh140 text-start">
                              How To Start a Freelance Business
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-start">
                              Being a freelancer is one of the most liberating career paths. It is also one of the toughest. Starting a freelance business is not just as simple as waking up in the morning, rolling out of bed, and opening your laptop. Freelancing is not just an easy way to monetize your hobbies and nor is it easier than a “proper” job. This video course will give you all the tools that you need to be a successful freelancer. It will tackle common problems and answer the most common questions that new freelancers have.
                           </div>
                        </div>
                        <div class="col-12 col-md-5 order-md-1 mt20 mt-md0">
                           <img src="assets/images/bonus1.webp" class="img-fluid d-block mx-auto max-h450" alt="Bonus">
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-12 mt60 mt-md100">
                  <div class="bonus-section-shape text-center">
                     <div class="col-12 margin-t-60">
                        <div class="bonus-headline">
                           <div class="f-md-24 f-20 w600 white-clr text-nowrap text-center">
                              Bonus #2 
                           </div>
                        </div>
                     </div>
                     <div class="row d-flex align-items-center flex-wrap mt15 mt-md30">
                        <div class="col-12 col-md-7">
                           <div class="w600 f-22 f-md-24 black-clr lh140 text-start">
                              Content Marketing Formula 
                           </div>
                           <div class="f-18 f-md-20 w400 lh140 mt10 text-start">
                              Content marketing is currently one of the biggest trends in digital marketing as a whole and is an area that many website owners and brands are investing in heavily right now thanks to the impressive returns that they are seeing. In this guide you will  learn how content marketing works, why it is crucial for your business and how to harness it in a way that completely transforms your success. You’ll receive a completely content marketing formula that you can adapt to your own brand and that you can use to build immense authority and a huge list of readers 
                           </div>
                        </div>
                        <div class="col-12 col-md-5 mt20 mt-md0">
                           <img src="assets/images/bonus1.webp" class="img-fluid d-block mx-auto max-h450" alt="Bonus">
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-12 mt60 mt-md100">
                  <div class="bonus-section-shape text-center">
                     <div class="col-12 margin-t-60">
                        <div class="bonus-headline">
                           <div class="f-md-24 f-20 w600 white-clr text-nowrap text-center">
                              Bonus #3
                           </div>
                        </div>
                     </div>
                     <div class="row d-flex align-items-center flex-wrap mt15 mt-md30">
                        <div class="col-12 col-md-7 order-md-2">
                           <div class="w600 f-22 f-md-24 black-clr lh140 text-start">
                              Blogging Traffic Mantra
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-start">
                              Making a living as a blogger has to be one of the sweetest gigs out there. As a blogger, This is all about working hard and smart now so that you can reap the benefits later. Too many people approach blogging in the wrong way, thinking that they can just write a few posts on a semi-regular basis and that that will be enough to ensure their success. In reality, you need to approach blogging as a full-time job if you ever want it to be your main source of income.
                           </div>
                        </div>
                        <div class="col-12 col-md-5 order-md-1 mt20 mt-md0">
                           <img src="assets/images/bonus1.webp" class="img-fluid d-block mx-auto max-h450" alt="Bonus">
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-12 mt60 mt-md100">
                  <div class="bonus-section-shape text-center">
                     <div class="col-12 margin-t-60">
                        <div class="bonus-headline">
                           <div class="f-md-24 f-20 w600 white-clr text-nowrap text-center">
                              Bonus #4
                           </div>
                        </div>
                     </div>
                     <div class="row d-flex align-items-center flex-wrap mt15 mt-md30">
                        <div class="col-12 col-md-7">
                           <div class="w600 f-22 f-md-24 black-clr lh140 text-start">
                              How To Launch Your Online Course 
                           </div>
                           <div class="f-18 w400 lh140 mt10 text-start">
                              Knowing the best online tools and marketing techniques are the keys to creating a financially successful online course. Launching an online course does not have to be difficult. With the help of this guide, you will learn key aspects of a successful online course launch. You will learn beginner-friendly tools and tips, gain key insights on course design, creation, editing, and launching. 
                           </div>
                        </div>
                        <div class="col-12 col-md-5 mt20 mt-md0">
                           <img src="assets/images/bonus1.webp" class="img-fluid d-block mx-auto max-h450" alt="Bonus">
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 text-center">
                  <div class="bonus-gradient">
                     <div class="f-22 f-md-36 w600 white-clr lh240 text-center ">
                        That’s A Total Value of <br class="d-none d-md-block"><span class="f-60 f-md-100 w600">$3300</span>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!---Bonus Section End-->
      <!-- amazing-sound -->
      <div class="amazing-sound">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-12">
                  <div class="col-12 f-28 f-md-45 w600 white-clr text-center lh140">
                     <div class="title-shape">
                        <div class="f-22 f-md-28 w700 white-clr lh140 text-center">
                           “Wow! That Sounds Like an Amazing Deal!”
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-12 mt20 mt-md30">
                  <div class="f-24 f-md-40 w600 lh140 text-center">
                     But “I’m Just Getting Started Online. Can LearnX Writer
                     Help Me Build an Online Business Too?”                     
                  </div>
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md50">
               <div class="col-12 col-md-7">
                  <div class="f-md-20 f-18 w400 lh140 black-clr ">
                     <span class="w600 f-20 f-md-36 "><u>Absolutely</u>,</span>
                     <br>
                     You easily can tap into the huge content writing service selling industry right 
                     from the comfort of your home. Help local businesses to get great ROI with 
                     their digital marketing and make a sustainable income stream.
                     <br><br>
                     <span class="w600">You can literally create a highly engaging marketing content by your 
                     own and start an online business today</span> or for local clients without any 
                     tech hassles or specialiezed skills. Even better, you won’t go bankrupt in 
                     the process by hiring money sucking copywriting experts. 
                     <br><br>
                     <span class="w600">This is something great we are offering you today that will <u> open a HUGE Business Opportunity For YOU!</u> </span>
                     </span>
                  </div>
               </div>
               <div class="col-12 col-md-5 mt20 mt-md0 ">
                  <img src="assets/images/sounds-great-img.webp" alt="Sounds Great" class="img-fluid mx-auto d-block">
               </div>
            </div>
         </div>
      </div>
      <!-- amazing-sound-end -->

      <div class="riskfree-section">
         <div class="container ">
            <div class="row align-items-center ">
               <div class="f-28 f-md-50 w700 white-clr text-center col-12 mb-md40">Test Drive It Risk FREE For 30 Days
               </div>
               <div class="col-md-6 col-12 mt15 mt-md0">
                  <div class="f-18 f-md-20 w400 lh140 white-clr"><span class="w600">Your purchase is secured with our 30-day money back guarantee.</span>
                     <br><br>
                     So, you can buy with full confidence and try it for yourself and for your clients risk free. If you face any Issue or don’t get results you desired after using it, just raise a ticket on support desk within 30 days and we'll refund you everything, down to the last penny.
                     <br><br>
                     We would like to clearly state that we do NOT offer a “no questions asked” money back guarantee. You must provide a genuine reason when you contact our support and we will refund your hard-earned money.
                  </div>
               </div>
               <div class="col-md-6 col-12 mt20 mt-md0">
                  <img src="assets/images/riskfree-img.webp " class="img-fluid d-block mx-auto" alt="Money Back Guarantee">
               </div>
            </div>
         </div>
      </div>
      <!-- Guarantee Section End -->
      <!-- Discounted Price Section Start -->
      <div class="table-section" id="buynow">
         <div class="container ">
            <div class="row ">
               <div class="col-12 ">
                  <div class="f-20 f-md-24 w400 text-center lh140">
                     Take Advantage of Our Limited Time Deal
                  </div>
                  <div class="f-md-50 f-28 w700 text-center mt10 ">
                     Get LearnX For A Low <br class="d-none d-md-block"> One-Time- Price, No Monthly Fee.
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 text-center mt20 black-clr">
                     Use Coupon Code <span class="orange-clr w700">"LearnX"</span> for an Additional <span class="orange-clr w700 "> $3 Discount</span> on Commercial Licence
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md50 ">
                  <div class="row mx-auto">
                     <div class="col-12 col-md-8 mt20 mt-md0 mx-auto">
                        <div class="table-wrap1 ">
                           <div class="table-head1 ">
                           <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 484.58 200.84" style="max-height:75px;">
                        <defs>
                            <style>
                                .cls-1,.cls-4{fill:#fff;}
                                .cls-2,.cls-3,.cls-4{fill-rule:evenodd;}
                                .cls-2{fill:url(#linear-gradient);}
                                .cls-3{fill:url(#linear-gradient-2);}
                                .cls-5{fill:url(#linear-gradient-3);}
                            </style>
                            <linearGradient id="linear-gradient" x1="-154.67" y1="191.86" x2="249.18" y2="191.86" gradientTransform="matrix(1.19, -0.01, 0.05, 1, 178.46, -4.31)" gradientUnits="userSpaceOnUse">
                                <stop offset="0" stop-color="#ead40c"></stop>
                                <stop offset="1" stop-color="#ff00e9"></stop>
                            </linearGradient>
                            <linearGradient id="linear-gradient-2" x1="23.11" y1="160.56" x2="57.61" y2="160.56" gradientTransform="matrix(1, 0, 0, 1, 0, 0)" xlink:href="#linear-gradient"></linearGradient>
                            <linearGradient id="linear-gradient-3" x1="384.49" y1="98.33" x2="483.38" y2="98.33" gradientTransform="matrix(1, 0, 0, 1, 0, 0)" xlink:href="#linear-gradient"></linearGradient>
                        </defs>
                        <g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1">
                            <path class="cls-1" d="M112.31,154.3q-11.65,0-20.1-4.87a32.56,32.56,0,0,1-13-13.82q-4.53-9-4.54-21.11a46.06,46.06,0,0,1,4.57-21A34.29,34.29,0,0,1,92,79.37a36.11,36.11,0,0,1,19.31-5.06,39.65,39.65,0,0,1,13.54,2.29,31,31,0,0,1,11.3,7.09,33.32,33.32,0,0,1,7.75,12.18,49.23,49.23,0,0,1,2.82,17.57V119H83.26v-12.3h46A19.87,19.87,0,0,0,127,97.38a16.6,16.6,0,0,0-6.18-6.48,17.59,17.59,0,0,0-9.21-2.37,17.88,17.88,0,0,0-9.83,2.7,18.81,18.81,0,0,0-6.58,7.06,20.17,20.17,0,0,0-2.4,9.56v10.74a25.15,25.15,0,0,0,2.47,11.57,17.42,17.42,0,0,0,6.91,7.37,20.52,20.52,0,0,0,10.39,2.55,21.62,21.62,0,0,0,7.22-1.14,15.71,15.71,0,0,0,5.59-3.35,14.11,14.11,0,0,0,3.59-5.5L146,132a26.44,26.44,0,0,1-6.13,11.77,29.72,29.72,0,0,1-11.52,7.77A43.74,43.74,0,0,1,112.31,154.3Z"></path>
                            <path class="cls-1" d="M184.49,154.35a31.78,31.78,0,0,1-13.24-2.65,21.26,21.26,0,0,1-9.28-7.84,22.86,22.86,0,0,1-3.41-12.81A21.92,21.92,0,0,1,161,120.2a18.9,18.9,0,0,1,6.61-6.86,33.85,33.85,0,0,1,9.46-3.9,77.76,77.76,0,0,1,10.92-2q6.81-.7,11-1.28a15.91,15.91,0,0,0,6.18-1.82,4.25,4.25,0,0,0,1.94-3.86v-.3q0-5.7-3.38-8.83T194,88.28q-6.71,0-10.62,2.92a14.55,14.55,0,0,0-5.27,6.91l-17-2.42a27.51,27.51,0,0,1,6.66-11.83,29.46,29.46,0,0,1,11.35-7.16,43.73,43.73,0,0,1,14.83-2.39,47.89,47.89,0,0,1,11.14,1.31,31.6,31.6,0,0,1,10.14,4.31,22.12,22.12,0,0,1,7.39,8.14q2.81,5.15,2.8,12.87v51.85H207.84V142.14h-.61a22.1,22.1,0,0,1-4.66,6,22.35,22.35,0,0,1-7.52,4.49A30,30,0,0,1,184.49,154.35Zm4.74-13.42a19.7,19.7,0,0,0,9.53-2.19,16,16,0,0,0,6.23-5.83,15,15,0,0,0,2.19-7.91v-9.13a8.72,8.72,0,0,1-2.9,1.31,45.56,45.56,0,0,1-4.56,1.06c-1.68.3-3.35.57-5,.8l-4.29.61a32,32,0,0,0-7.32,1.81A12.29,12.29,0,0,0,178,125a9.76,9.76,0,0,0,1.82,13.39A16,16,0,0,0,189.23,140.93Z"></path>
                            <path class="cls-1" d="M243.8,152.79V75.31h17.7V88.23h.81a19.36,19.36,0,0,1,7.29-10.37,20,20,0,0,1,11.83-3.66c1,0,2.14,0,3.4.13a23.83,23.83,0,0,1,3.15.38V91.5a21,21,0,0,0-3.65-.73,38.61,38.61,0,0,0-4.82-.32,18.47,18.47,0,0,0-8.95,2.14,15.94,15.94,0,0,0-6.23,5.93,16.55,16.55,0,0,0-2.27,8.72v45.55Z"></path>
                            <path class="cls-1" d="M318.4,107.39v45.4H300.14V75.31h17.45V88.48h.91a22,22,0,0,1,8.55-10.34q5.86-3.84,14.55-3.83a27.58,27.58,0,0,1,14,3.43,23.19,23.19,0,0,1,9.28,9.93,34.22,34.22,0,0,1,3.26,15.79v49.33H349.87V106.28c0-5.17-1.34-9.23-4-12.15s-6.37-4.39-11.07-4.39a17,17,0,0,0-8.5,2.09,14.67,14.67,0,0,0-5.8,6A20,20,0,0,0,318.4,107.39Z"></path>
                            <path class="cls-2" d="M314.05,172.88c12.69-.14,27.32,2.53,40.82,2.61l-.09,1c1.07.15,1-.88,2-.78,1.42-.18,0,1.57,1.82,1.14.54-.06.08-.56-.32-.68.77,0,2.31.17,3,1.26,1.3,0-.9-1.34.85-.89,6,1.63,13.39,0,20,3.2,11.64.72,24.79,2.38,38,4.27,2.85.41,5.37,1.11,7.57,1.05,2-.05,5.68.78,8,1.08,2.53.34,5.16,1.56,7.13,1.65-1.18-.05,5.49-.78,4.54.76,5-.72,10.9,1.67,15.94,1.84.83-.07.69.53.67,1,1.23-1.17,3.32.56,4.28-.56.25,2,2.63-.48,3.3,1.61,2.23-1.39,4.83.74,7.55,1.37,1.91.44,5.29-.21,5.55,2.14-2.1-.13-5.12.81-11.41-1.09,1,.87-3.35.74-3.39-.64-1.81,1.94-6.28-1-7.79,1.19-1.26-.41,0-.72-.64-1.35-5,.55-8.09-1.33-12.91-1.56,2.57,2.44,6.08,3.16,10.06,3.22,1.28.15,0,.74,1,1.39,1.37-.19.75-.48,1.26-1.17,5.37,2.28,15.36,2.35,21,4.91-4.69-.63-11.38,0-15.15-2.09A148,148,0,0,1,441.58,196c-.53-.1-1.81.12-.7-.71-1.65.73-3.41.1-5.74-.22-37.15-5.15-80.47-7.78-115.75-9.76-39.54-2.22-76.79-3.22-116.44-2.72-61.62.78-119.59,7.93-175.21,13.95-5,.55-10,1.49-15.11,1.47-1.33-.32-2.46-1.91-1.25-3-2-.38-2.93.29-5-.15a9.83,9.83,0,0,1-1.25-3,13.73,13.73,0,0,1,6.42-2.94c.77-.54.12-.8.15-1.6a171,171,0,0,1,45.35-8.58c26.43-1.1,53.68-4.73,79.64-6,6.25-.3,11.72.06,18.41.14,8.31.1,19.09-1,29.19-.12,6.25.54,13.67-.55,21.16-.56,39.35-.09,71.16-.23,112.28,2C317.24,171.93,315.11,173.81,314.05,172.88Zm64.22,16.05-1.6-.2C376.2,190,378.42,190.26,378.27,188.93Z"></path>
                            <polygon class="cls-3" points="52.57 166.06 57.61 151.99 23.11 157.23 32.36 169.12 52.57 166.06"></polygon>
                            <polygon class="cls-4" points="44.69 183.24 51.75 168.35 33.86 171.06 44.69 183.24"></polygon>
                            <path class="cls-4" d="M.12,10.15l22,145.06,35.47-5.39-22-145a2.6,2.6,0,0,0,0-.4C35,.76,26.62-.95,16.81.54S-.52,6.16,0,9.77A2.62,2.62,0,0,0,.12,10.15Z"></path>
                            <path class="cls-5" d="M433.68,79.45l21-36.33h27.56L448.48,97.51l34.9,56H456l-22-37.76-21.94,37.76H384.49l34.9-56L385.72,43.12h27.35Z"></path>
                        </g>
                            </g>
                    </svg>
                              <div class="f-22 f-md-32 w600 lh120 text-center text-uppercase mt30 commercial-shape white-clr">Writer Commercial</div>
                           </div>
                           
                              <ul class="table-list1 pl0 f-18 lh140 w400 black-clr mb0">
                                 <li>Create Fresh & SEO-Friendly Copies</li>
                                 <li>Create Content for Any Local or Online Niche (36+ preloaded niches) </li>
                                 <li>50+ Use Case Based Advance Copywriting Templates</li>
                                 <li>Write Copy in 35+ Languages & 22+ Tones of Voice</li>
                                 <li>Generate 1000 Write Up Monthly </li>
                                 <li>Social Media Integration to Directly Share Your posts on Social Media</li>
                                 <li>Download Your Content in Word or PDF format.</li>
                                 <li>Round The Clock Support </li>
                                 <li>Regular Updates</li>
                                 <li>Step-By-Step Video Training</li>
                                 <li>Commercial License to Serve Your Clients </li>
                                 <li class="headline f-md-24">Fast Action Bonuses</li>
                                 <li>Bonus #1 - Live Training</li>
                                 <li>Bonus #2 - Video Training To Start E-Learning/Course Selling Business</li>
                                 <li>Bonus #3 - Lead Generation Workshop</li>
                                 <li>Bonus #4 - Training on How to Make Money throug Affiliate Markeitng</li>
                              </ul>
                           
                           <div class="table-btn">
                              <div class="hideme-button ">
                                 <a href="https://warriorplus.com/o2/buy/fbmn7d/hsp2pz/hy7fsz"><img src="https://warriorplus.com/o2/btn/fn200011000/fbmn7d/hsp2pz/373829" alt="LearnX WriterArc Commercial" border="0" class="img-fluid d-block mx-auto" /></a>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt30 ">
                  <div class="col-12 mt20 mt-md50 ">
                     <div class="f-22 f-md-28 w600 black-clr px0 text-center lh140 ">If you have any query, simply reach to us at <a href="mailto:support@bizomart.com " class="orange-clr ">support@bizomart.com</a></div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Discounted Price Section End -->
      
      <!--final section-->
      <div class="final-section ">
         <div class="container ">
            <div class="row ">
               <div class="col-12 passport-content">
                  <div class="text-center">
                     <div class="f-45 f-md-45 lh140 w600 text-center white-clr final-shape">
                        FINAL CALL
                     </div>
                  </div>
                  <div class="col-12 f-22 f-md-32 lh140 w600 text-center white-clr mt20 p0 mb20 mb-md50">
                     You Still Have The Opportunity To Take Your Game To The Next Level… <br class="d-none d-md-block"> Remember, It’s Your Make Or Break Time!
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-12">
                  <div class="f-20 f-md-26 w600 lh140 text-center white-clr">
                     Hurry! Limited Time Only. No Monthly Fees
                  </div>
                  <div class="f-16 f-md-20 lh140 w400 text-center mt10 white-clr">
                     Use Coupon Code <span class="w700 orange-clr">LearnX</span> for an Additional <span class="w700 orange-clr"> $3 Discount</span>
                  </div>
                  <div class="row">
                     <div class="col-12 col-md-7 mx-auto mt20">
                        <div class="f-20 f-md-35 text-center probtn1">
                           <a href="#buynow" class="cta-link-btn d-block">Get Instant Access To LearnX</a>
                        </div>
                        <img src="https://cdn.oppyo.com/launches/writerarc/bizdrive/payment-options.webp" class="img-fluid mx-auto d-block mt20">
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-12 mt25 mt-md40 text-center">
               <a class="white-clr f-md-24 f-18 lh140 w600" href="https://warriorplus.com/o/nothanks/hsp2pz" target="_blank" class="link">
               No thanks - I Don't Want To Creates Stunning Content for Any Local or Online Niche 10X Faster... Please Take Me To The Next Step To Get Access To My Purchase.
               </a>
          </div>
         </div>
      </div>
      <!--final section-->
      <!--Footer Section Start -->
      <div class="footer-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
               <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 484.58 200.84" style="max-height:75px;">
                        <defs>
                            <style>
                                .cls-1,.cls-4{fill:#fff;}
                                .cls-2,.cls-3,.cls-4{fill-rule:evenodd;}
                                .cls-2{fill:url(#linear-gradient);}
                                .cls-3{fill:url(#linear-gradient-2);}
                                .cls-5{fill:url(#linear-gradient-3);}
                            </style>
                            <linearGradient id="linear-gradient" x1="-154.67" y1="191.86" x2="249.18" y2="191.86" gradientTransform="matrix(1.19, -0.01, 0.05, 1, 178.46, -4.31)" gradientUnits="userSpaceOnUse">
                                <stop offset="0" stop-color="#ead40c"></stop>
                                <stop offset="1" stop-color="#ff00e9"></stop>
                            </linearGradient>
                            <linearGradient id="linear-gradient-2" x1="23.11" y1="160.56" x2="57.61" y2="160.56" gradientTransform="matrix(1, 0, 0, 1, 0, 0)" xlink:href="#linear-gradient"></linearGradient>
                            <linearGradient id="linear-gradient-3" x1="384.49" y1="98.33" x2="483.38" y2="98.33" gradientTransform="matrix(1, 0, 0, 1, 0, 0)" xlink:href="#linear-gradient"></linearGradient>
                        </defs>
                        <g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1">
                            <path class="cls-1" d="M112.31,154.3q-11.65,0-20.1-4.87a32.56,32.56,0,0,1-13-13.82q-4.53-9-4.54-21.11a46.06,46.06,0,0,1,4.57-21A34.29,34.29,0,0,1,92,79.37a36.11,36.11,0,0,1,19.31-5.06,39.65,39.65,0,0,1,13.54,2.29,31,31,0,0,1,11.3,7.09,33.32,33.32,0,0,1,7.75,12.18,49.23,49.23,0,0,1,2.82,17.57V119H83.26v-12.3h46A19.87,19.87,0,0,0,127,97.38a16.6,16.6,0,0,0-6.18-6.48,17.59,17.59,0,0,0-9.21-2.37,17.88,17.88,0,0,0-9.83,2.7,18.81,18.81,0,0,0-6.58,7.06,20.17,20.17,0,0,0-2.4,9.56v10.74a25.15,25.15,0,0,0,2.47,11.57,17.42,17.42,0,0,0,6.91,7.37,20.52,20.52,0,0,0,10.39,2.55,21.62,21.62,0,0,0,7.22-1.14,15.71,15.71,0,0,0,5.59-3.35,14.11,14.11,0,0,0,3.59-5.5L146,132a26.44,26.44,0,0,1-6.13,11.77,29.72,29.72,0,0,1-11.52,7.77A43.74,43.74,0,0,1,112.31,154.3Z"></path>
                            <path class="cls-1" d="M184.49,154.35a31.78,31.78,0,0,1-13.24-2.65,21.26,21.26,0,0,1-9.28-7.84,22.86,22.86,0,0,1-3.41-12.81A21.92,21.92,0,0,1,161,120.2a18.9,18.9,0,0,1,6.61-6.86,33.85,33.85,0,0,1,9.46-3.9,77.76,77.76,0,0,1,10.92-2q6.81-.7,11-1.28a15.91,15.91,0,0,0,6.18-1.82,4.25,4.25,0,0,0,1.94-3.86v-.3q0-5.7-3.38-8.83T194,88.28q-6.71,0-10.62,2.92a14.55,14.55,0,0,0-5.27,6.91l-17-2.42a27.51,27.51,0,0,1,6.66-11.83,29.46,29.46,0,0,1,11.35-7.16,43.73,43.73,0,0,1,14.83-2.39,47.89,47.89,0,0,1,11.14,1.31,31.6,31.6,0,0,1,10.14,4.31,22.12,22.12,0,0,1,7.39,8.14q2.81,5.15,2.8,12.87v51.85H207.84V142.14h-.61a22.1,22.1,0,0,1-4.66,6,22.35,22.35,0,0,1-7.52,4.49A30,30,0,0,1,184.49,154.35Zm4.74-13.42a19.7,19.7,0,0,0,9.53-2.19,16,16,0,0,0,6.23-5.83,15,15,0,0,0,2.19-7.91v-9.13a8.72,8.72,0,0,1-2.9,1.31,45.56,45.56,0,0,1-4.56,1.06c-1.68.3-3.35.57-5,.8l-4.29.61a32,32,0,0,0-7.32,1.81A12.29,12.29,0,0,0,178,125a9.76,9.76,0,0,0,1.82,13.39A16,16,0,0,0,189.23,140.93Z"></path>
                            <path class="cls-1" d="M243.8,152.79V75.31h17.7V88.23h.81a19.36,19.36,0,0,1,7.29-10.37,20,20,0,0,1,11.83-3.66c1,0,2.14,0,3.4.13a23.83,23.83,0,0,1,3.15.38V91.5a21,21,0,0,0-3.65-.73,38.61,38.61,0,0,0-4.82-.32,18.47,18.47,0,0,0-8.95,2.14,15.94,15.94,0,0,0-6.23,5.93,16.55,16.55,0,0,0-2.27,8.72v45.55Z"></path>
                            <path class="cls-1" d="M318.4,107.39v45.4H300.14V75.31h17.45V88.48h.91a22,22,0,0,1,8.55-10.34q5.86-3.84,14.55-3.83a27.58,27.58,0,0,1,14,3.43,23.19,23.19,0,0,1,9.28,9.93,34.22,34.22,0,0,1,3.26,15.79v49.33H349.87V106.28c0-5.17-1.34-9.23-4-12.15s-6.37-4.39-11.07-4.39a17,17,0,0,0-8.5,2.09,14.67,14.67,0,0,0-5.8,6A20,20,0,0,0,318.4,107.39Z"></path>
                            <path class="cls-2" d="M314.05,172.88c12.69-.14,27.32,2.53,40.82,2.61l-.09,1c1.07.15,1-.88,2-.78,1.42-.18,0,1.57,1.82,1.14.54-.06.08-.56-.32-.68.77,0,2.31.17,3,1.26,1.3,0-.9-1.34.85-.89,6,1.63,13.39,0,20,3.2,11.64.72,24.79,2.38,38,4.27,2.85.41,5.37,1.11,7.57,1.05,2-.05,5.68.78,8,1.08,2.53.34,5.16,1.56,7.13,1.65-1.18-.05,5.49-.78,4.54.76,5-.72,10.9,1.67,15.94,1.84.83-.07.69.53.67,1,1.23-1.17,3.32.56,4.28-.56.25,2,2.63-.48,3.3,1.61,2.23-1.39,4.83.74,7.55,1.37,1.91.44,5.29-.21,5.55,2.14-2.1-.13-5.12.81-11.41-1.09,1,.87-3.35.74-3.39-.64-1.81,1.94-6.28-1-7.79,1.19-1.26-.41,0-.72-.64-1.35-5,.55-8.09-1.33-12.91-1.56,2.57,2.44,6.08,3.16,10.06,3.22,1.28.15,0,.74,1,1.39,1.37-.19.75-.48,1.26-1.17,5.37,2.28,15.36,2.35,21,4.91-4.69-.63-11.38,0-15.15-2.09A148,148,0,0,1,441.58,196c-.53-.1-1.81.12-.7-.71-1.65.73-3.41.1-5.74-.22-37.15-5.15-80.47-7.78-115.75-9.76-39.54-2.22-76.79-3.22-116.44-2.72-61.62.78-119.59,7.93-175.21,13.95-5,.55-10,1.49-15.11,1.47-1.33-.32-2.46-1.91-1.25-3-2-.38-2.93.29-5-.15a9.83,9.83,0,0,1-1.25-3,13.73,13.73,0,0,1,6.42-2.94c.77-.54.12-.8.15-1.6a171,171,0,0,1,45.35-8.58c26.43-1.1,53.68-4.73,79.64-6,6.25-.3,11.72.06,18.41.14,8.31.1,19.09-1,29.19-.12,6.25.54,13.67-.55,21.16-.56,39.35-.09,71.16-.23,112.28,2C317.24,171.93,315.11,173.81,314.05,172.88Zm64.22,16.05-1.6-.2C376.2,190,378.42,190.26,378.27,188.93Z"></path>
                            <polygon class="cls-3" points="52.57 166.06 57.61 151.99 23.11 157.23 32.36 169.12 52.57 166.06"></polygon>
                            <polygon class="cls-4" points="44.69 183.24 51.75 168.35 33.86 171.06 44.69 183.24"></polygon>
                            <path class="cls-4" d="M.12,10.15l22,145.06,35.47-5.39-22-145a2.6,2.6,0,0,0,0-.4C35,.76,26.62-.95,16.81.54S-.52,6.16,0,9.77A2.62,2.62,0,0,0,.12,10.15Z"></path>
                            <path class="cls-5" d="M433.68,79.45l21-36.33h27.56L448.48,97.51l34.9,56H456l-22-37.76-21.94,37.76H384.49l34.9-56L385.72,43.12h27.35Z"></path>
                        </g>
                            </g>
                    </svg>
                    <div class="f-14 f-md-16 w400 mt20 lh160 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
                    <div class=" mt20 mt-md40 white-clr"> <div class="f-20 w500 mt20 lh140 white-clr text-center"><script type="text/javascript" src="https://warriorplus.com/o2/disclaimer/fbmn7d" defer></script><div class="wplus_spdisclaimer"></div></div></div>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-16 f-md-16 w300 lh140 white-clr text-xs-center">Copyright © LearnX</div>
                  <ul class="footer-ul w300 f-16 f-md-16 white-clr text-center text-md-right">
                     <li><a href="https://support.bizomart.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://getlearnx.com/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://getlearnx.com/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://getlearnx.com/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://getlearnx.com/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://getlearnx.com/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://getlearnx.com/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!--Footer Section End -->
      <script type="text/javascript" src="assets/js/ouibounce.min.js"></script>
<div id="ouibounce-modal" style="z-index:9999999; display:none;">
   <div class="underlay"></div>
   <div class="modal-wrapper" style="display:block;">
      <div class="modal-bg">
		<button type="button" class="close" onclick="document.getElementById('ouibounce-modal').style.display = 'none';">
			X 
		</button>
		<div class="model-header">
      <div class="d-flex justify-content-center align-items-center gap-3">
            <img src="assets/images/hold.png" alt="" class="img-fluid d-block m131">
            <div>
               <div class="f-md-56 f-24 w700 lh130 white-clr">
                  WAIT! HOLD ON!!
               </div>
               <div class="f-md-28 f-18 w700 lh130 white-clr mt10 mt-md0">
                  Don't Leave Empty Handed
               </div>
            </div>
         </div>
		</div>
      <div class="col-12 for-padding">
			<div class="copun-line f-md-45 f-16 w500 lh130 text-center black-clr mt10">
         You've Qualified For An <span class="w700 red-clr"><br> INSTANT $70 DISCOUNT</span>  
			</div>
         <div class="copun-line f-md-30 f-16 w700 lh130 text-center black-clr mt10" style="background-color:yellow; padding:5px 10px; display:inline-block">
         Regular Price <strike>$97</strike>,
         Now Only $27
			</div>
			 <div class="mt-md20 mt10 text-center ">
				<a href="https://warriorplus.com/o2/buy/fbmn7d/hsp2pz/hy7fsz" class="cta-link-btn1">Grab SPECIAL Discount Now!
				<br>
				<span class="f-12 white-clr w600 text-uppercase">Act Now! Limited to First 100 Buyers Only.</span> </a>
			 </div>
			 <!-- <div class="mt-md20 mt10 text-center f-md-26 f-16 w900 lh150 text-center red-clr text-uppercase">
				Coupon Code EXPIRES IN:
			</div>
			<div class="countdown counter-white text-center">
                     <div class="timer-label text-center"><span class="f-24 f-md-42 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
                     <div class="timer-label text-center"><span class="f-24 f-md-42 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                     <div class="timer-label text-center timer-mrgn"><span class="f-24 f-md-42 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                     <div class="timer-label text-center "><span class="f-24 f-md-42 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
            </div> -->
      </div>
   </div>
</div>
<script type="text/javascript">
        var _ouibounce = ouibounce(document.getElementById('ouibounce-modal'),{
        aggressive: true,
        });
    </script>
<!-- <script type="text/javascript">
   var noob = $('.countdown').length;
   var stimeInSecs;
   var tickers;

   function timerBegin(seconds) {   
      if(seconds>=0)  {
         stimeInSecs = parseInt(seconds);           
         showRemaining();
      }
      //tickers = setInterval("showRemaining()", 1000);
   }

   function showRemaining() {

      var seconds = stimeInSecs;
         
      if (seconds > 0) {         
         stimeInSecs--;       
      } else {        
         clearInterval(tickers);       
         //timerBegin(59 * 60); 
      }

      var days = Math.floor(seconds / 86400);

      seconds %= 86400;
      
      var hours = Math.floor(seconds / 3600);
      
      seconds %= 3600;
      
      var minutes = Math.floor(seconds / 60);
      
      seconds %= 60;


      if (days < 10) {
         days = "0" + days;
      }
      if (hours < 10) {
         hours = "0" + hours;
      }
      if (minutes < 10) {
         minutes = "0" + minutes;
      }
      if (seconds < 10) {
         seconds = "0" + seconds;
      }
      var i;
      var countdown = document.getElementsByClassName('countdown');

      for (i = 0; i < noob; i++) {
         countdown[i].innerHTML = '';
      
         if (days) {
            countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-24 f-md-42 timerbg oswald">' + days + '</span><br><span class="f-14 f-md-14 w400 smmltd">Days</span> </div>';
         }
      
         countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-24 f-md-42 timerbg oswald">' + hours + '</span><br><span class="f-14 f-md-14 w400 smmltd">Hours</span> </div>';
      
         countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"><span class="f-24 f-md-42 timerbg oswald">' + minutes + '</span><br><span class="f-14 f-md-14 w400 smmltd">Mins</span> </div>';
      
         countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-24 f-md-42 timerbg oswald">' + seconds + '</span><br><span class="f-14 f-md-14 w400 smmltd">Sec</span> </div>';
      }
   
   }

const givenTimestamp = 'Mon Dec 4 2023 20:30:00 GMT+0530'
const durationTime = 60;

const date = new Date(givenTimestamp);
const givenTime = date.getTime() / 1000;
const currentTime = new Date().getTime()



let cnt;
const matchInterval = setInterval(() => {
   if(Math.round(givenTime) === Math.round(new Date().getTime() /1000)){
   cnt = durationTime * 60;
   counter();
   clearInterval(matchInterval);
   }else if(Math.round(givenTime) < Math.round(new Date().getTime() /1000)){
      const timeGap = Math.round(new Date().getTime() /1000) - Math.round(givenTime)
      let difference = (durationTime * 60) - timeGap;
      if(difference >= 0){
         cnt = difference
         localStorage.mavasTimer = cnt;

      }else{
         
         difference = difference % (durationTime * 60);
         cnt = (durationTime * 60) + difference
         localStorage.mavasTimer = cnt;

      }

      counter();
      clearInterval(matchInterval);
   }
},1000)





function counter(){
   if(parseInt(localStorage.mavasTimer) > 0){
      cnt = parseInt(localStorage.mavasTimer);
   }
   cnt -= 1;
   localStorage.mavasTimer = cnt;
   if(localStorage.mavasTimer !== NaN){
      timerBegin(parseInt(localStorage.mavasTimer));
   }
   if(cnt>0){
      setTimeout(counter,1000);
  }else if(cnt === 0){
   cnt = durationTime * 60 
   counter()
  }
}

</script> -->

</body>
</html>
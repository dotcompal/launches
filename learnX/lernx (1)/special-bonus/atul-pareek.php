<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- Device & IE Compatibility Meta -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=9">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=9">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta name="title" content="LearnX Special Bonuses">
    <meta name="description" content="LearnX Special Bonuses">
    <meta name="keywords" content="LearnX Special Bonuses">
    <meta property="og:image" content="https://www.getlearnx.com/special-bonus/thumbnail.png">
    <meta name="language" content="English">
    <meta name="revisit-after" content="1 days">
    <meta name="author" content="Atul Pareek">
    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:title" content="LearnX Special Bonuses">
    <meta property="og:description" content="LearnX Special Bonuses">
    <meta property="og:image" content="https://www.getlearnx.com/special-bonus/thumbnail.png">
    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:title" content="LearnX Special Bonuses">
    <meta property="twitter:description" content="LearnX Special Bonuses">
    <meta property="twitter:image" content="https://www.getlearnx.com/special-bonus/thumbnail.png">


<title>LearnX  Bonuses</title>
<!-- Shortcut Icon  -->
<link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
<!-- Css CDN Load Link -->
<link rel="stylesheet" href="assets/css/font-awesome.min.css" type="text/css" />
<link rel="stylesheet" href="../common_assets/css/bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="../common_assets/css/general.css">
<link rel="stylesheet" href="assets/css/timer.css" type="text/css" />
<link rel="stylesheet" type="text/css" href="assets/css/bonus-style.css">
<link rel="stylesheet" type="text/css" href="assets/css/style.css">
<link rel="stylesheet" type="text/css" href="assets/css/bonus.css">
<link rel="stylesheet" href="assets/css/style-feature.css" type="text/css">
<link rel="stylesheet" href="assets/css/style-bottom.css" type="text/css">
<!-- Font Family CDN Load Links -->
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&amp;family=Poppins:wght@400;500;600;700;800;900&amp;family=Red+Hat+Display:wght@300;400;500;600;700;800;900&amp;display=swap" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<!-- Javascript File Load -->
<script src="../common_assets/js/jquery.min.js"></script>
<script src="../common_assets/js/popper.min.js"></script>
<script src="../common_assets/js/bootstrap.min.js"></script>   
<!-- Buy Button Lazy load Script -->


<script>
 $(document).ready(function() {
  /* Every time the window is scrolled ... */
  $(window).scroll(function() {
	 /* Check the location of each desired element */
	 $('.hideme').each(function(i) {
		 var bottom_of_object = $(this).offset().top + $(this).outerHeight();
		 var bottom_of_window = $(window).scrollTop() + $(window).height();
		 /* If the object is completely visible in the window, fade it it */
		 if ((bottom_of_window - bottom_of_object) > -200) {
			 $(this).animate({
				 'opacity': '1'
			 }, 300);
		 }
	 });
  });
 });
</script>
<!-- Smooth Scrolling Script -->
<script>
 $(document).ready(function() {
  // Add smooth scrolling to all links
  $("a").on('click', function(event) {
 
	 // Make sure this.hash has a value before overriding default behavior
	 if (this.hash !== "") {
		 // Prevent default anchor click behavior
		 event.preventDefault();
 
		 // Store hash
		 var hash = this.hash;
 
		 // Using jQuery's animate() method to add smooth page scroll
		 // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
		 $('html, body').animate({
			 scrollTop: $(hash).offset().top
		 }, 800, function() {
 
			 // Add hash (#) to URL when done scrolling (default click behavior)
			 window.location.hash = hash;
		 });
	 } // End if
  });
 });
</script>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-K6H5FJP');</script>
<!-- End Google Tag Manager -->
</head>
<body>


<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K6H5FJP"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

  <!-- New Timer  Start-->
  <?php
	 $date = 'June 26 2023 11:59 PM EST';
	 $exp_date = strtotime($date);
	 $now = time();  
	 /*
	 
	 $date = date('F d Y g:i:s A eO');
	 $rand_time_add = rand(700, 1200);
	 $exp_date = strtotime($date) + $rand_time_add;
	 $now = time();*/
	 
	 if ($now < $exp_date) {
	 ?>
  <?php
	 } else {
		//   echo "Times Up";
	 }
	 ?>
  <!-- New Timer End -->
  <?php
	 if(!isset($_GET['afflink'])){
	 $_GET['afflink'] = 'https://warriorplus.com/o2/a/k6rjm1/0';
	 $_GET['name'] = 'Atul Pareek';      
	 }
	 ?>

<div class="main-header">
        <div class="container">
            <div class="row">
				<div class="col-12">
					<div class="f-md-32 f-20 lh160 w400 text-center white-clr">
						<span class="yellow-clr w600"><?php echo $_GET['name'];?>'s </span> special bonus for &nbsp; 	  
						<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                            viewBox="0 0 484.58 200.84" style="max-height:75px;">
                            <defs>
                                <style>
                                    .cls-l1,
                                    .cls-l4 {
                                        fill: #fff;
                                    }

                                    .cls-l2,
                                    .cls-l3,
                                    .cls-l4 {
                                        fill-rule: evenodd;
                                    }

                                    .cls-l2 {
                                        fill: url(#linear-gradient);
                                    }

                                    .cls-l3 {
                                        fill: url(#linear-gradient-2);
                                    }

                                    .cls-l5 {
                                        fill: url(#linear-gradient-3);
                                    }
                                </style>
                                <linearGradient id="linear-gradient" x1="-154.67" y1="191.86" x2="249.18" y2="191.86"
                                    gradientTransform="matrix(1.19, -0.01, 0.05, 1, 178.46, -4.31)"
                                    gradientUnits="userSpaceOnUse">
                                    <stop offset="0" stop-color="#ead40c"></stop>
                                    <stop offset="1" stop-color="#ff00e9"></stop>
                                </linearGradient>
                                <linearGradient id="linear-gradient-2" x1="23.11" y1="160.56" x2="57.61" y2="160.56"
                                    gradientTransform="matrix(1, 0, 0, 1, 0, 0)" xlink:href="#linear-gradient">
                                </linearGradient>
                                <linearGradient id="linear-gradient-3" x1="384.49" y1="98.33" x2="483.38" y2="98.33"
                                    gradientTransform="matrix(1, 0, 0, 1, 0, 0)" xlink:href="#linear-gradient">
                                </linearGradient>
                            </defs>
                            <g id="Layer_2" data-name="Layer 2">
                                <g id="Layer_1-2" data-name="Layer 1">
                                    <path class="cls-l1"
                                        d="M112.31,154.3q-11.65,0-20.1-4.87a32.56,32.56,0,0,1-13-13.82q-4.53-9-4.54-21.11a46.06,46.06,0,0,1,4.57-21A34.29,34.29,0,0,1,92,79.37a36.11,36.11,0,0,1,19.31-5.06,39.65,39.65,0,0,1,13.54,2.29,31,31,0,0,1,11.3,7.09,33.32,33.32,0,0,1,7.75,12.18,49.23,49.23,0,0,1,2.82,17.57V119H83.26v-12.3h46A19.87,19.87,0,0,0,127,97.38a16.6,16.6,0,0,0-6.18-6.48,17.59,17.59,0,0,0-9.21-2.37,17.88,17.88,0,0,0-9.83,2.7,18.81,18.81,0,0,0-6.58,7.06,20.17,20.17,0,0,0-2.4,9.56v10.74a25.15,25.15,0,0,0,2.47,11.57,17.42,17.42,0,0,0,6.91,7.37,20.52,20.52,0,0,0,10.39,2.55,21.62,21.62,0,0,0,7.22-1.14,15.71,15.71,0,0,0,5.59-3.35,14.11,14.11,0,0,0,3.59-5.5L146,132a26.44,26.44,0,0,1-6.13,11.77,29.72,29.72,0,0,1-11.52,7.77A43.74,43.74,0,0,1,112.31,154.3Z">
                                    </path>
                                    <path class="cls-l1"
                                        d="M184.49,154.35a31.78,31.78,0,0,1-13.24-2.65,21.26,21.26,0,0,1-9.28-7.84,22.86,22.86,0,0,1-3.41-12.81A21.92,21.92,0,0,1,161,120.2a18.9,18.9,0,0,1,6.61-6.86,33.85,33.85,0,0,1,9.46-3.9,77.76,77.76,0,0,1,10.92-2q6.81-.7,11-1.28a15.91,15.91,0,0,0,6.18-1.82,4.25,4.25,0,0,0,1.94-3.86v-.3q0-5.7-3.38-8.83T194,88.28q-6.71,0-10.62,2.92a14.55,14.55,0,0,0-5.27,6.91l-17-2.42a27.51,27.51,0,0,1,6.66-11.83,29.46,29.46,0,0,1,11.35-7.16,43.73,43.73,0,0,1,14.83-2.39,47.89,47.89,0,0,1,11.14,1.31,31.6,31.6,0,0,1,10.14,4.31,22.12,22.12,0,0,1,7.39,8.14q2.81,5.15,2.8,12.87v51.85H207.84V142.14h-.61a22.1,22.1,0,0,1-4.66,6,22.35,22.35,0,0,1-7.52,4.49A30,30,0,0,1,184.49,154.35Zm4.74-13.42a19.7,19.7,0,0,0,9.53-2.19,16,16,0,0,0,6.23-5.83,15,15,0,0,0,2.19-7.91v-9.13a8.72,8.72,0,0,1-2.9,1.31,45.56,45.56,0,0,1-4.56,1.06c-1.68.3-3.35.57-5,.8l-4.29.61a32,32,0,0,0-7.32,1.81A12.29,12.29,0,0,0,178,125a9.76,9.76,0,0,0,1.82,13.39A16,16,0,0,0,189.23,140.93Z">
                                    </path>
                                    <path class="cls-l1"
                                        d="M243.8,152.79V75.31h17.7V88.23h.81a19.36,19.36,0,0,1,7.29-10.37,20,20,0,0,1,11.83-3.66c1,0,2.14,0,3.4.13a23.83,23.83,0,0,1,3.15.38V91.5a21,21,0,0,0-3.65-.73,38.61,38.61,0,0,0-4.82-.32,18.47,18.47,0,0,0-8.95,2.14,15.94,15.94,0,0,0-6.23,5.93,16.55,16.55,0,0,0-2.27,8.72v45.55Z">
                                    </path>
                                    <path class="cls-l1"
                                        d="M318.4,107.39v45.4H300.14V75.31h17.45V88.48h.91a22,22,0,0,1,8.55-10.34q5.86-3.84,14.55-3.83a27.58,27.58,0,0,1,14,3.43,23.19,23.19,0,0,1,9.28,9.93,34.22,34.22,0,0,1,3.26,15.79v49.33H349.87V106.28c0-5.17-1.34-9.23-4-12.15s-6.37-4.39-11.07-4.39a17,17,0,0,0-8.5,2.09,14.67,14.67,0,0,0-5.8,6A20,20,0,0,0,318.4,107.39Z">
                                    </path>
                                    <path class="cls-l2"
                                        d="M314.05,172.88c12.69-.14,27.32,2.53,40.82,2.61l-.09,1c1.07.15,1-.88,2-.78,1.42-.18,0,1.57,1.82,1.14.54-.06.08-.56-.32-.68.77,0,2.31.17,3,1.26,1.3,0-.9-1.34.85-.89,6,1.63,13.39,0,20,3.2,11.64.72,24.79,2.38,38,4.27,2.85.41,5.37,1.11,7.57,1.05,2-.05,5.68.78,8,1.08,2.53.34,5.16,1.56,7.13,1.65-1.18-.05,5.49-.78,4.54.76,5-.72,10.9,1.67,15.94,1.84.83-.07.69.53.67,1,1.23-1.17,3.32.56,4.28-.56.25,2,2.63-.48,3.3,1.61,2.23-1.39,4.83.74,7.55,1.37,1.91.44,5.29-.21,5.55,2.14-2.1-.13-5.12.81-11.41-1.09,1,.87-3.35.74-3.39-.64-1.81,1.94-6.28-1-7.79,1.19-1.26-.41,0-.72-.64-1.35-5,.55-8.09-1.33-12.91-1.56,2.57,2.44,6.08,3.16,10.06,3.22,1.28.15,0,.74,1,1.39,1.37-.19.75-.48,1.26-1.17,5.37,2.28,15.36,2.35,21,4.91-4.69-.63-11.38,0-15.15-2.09A148,148,0,0,1,441.58,196c-.53-.1-1.81.12-.7-.71-1.65.73-3.41.1-5.74-.22-37.15-5.15-80.47-7.78-115.75-9.76-39.54-2.22-76.79-3.22-116.44-2.72-61.62.78-119.59,7.93-175.21,13.95-5,.55-10,1.49-15.11,1.47-1.33-.32-2.46-1.91-1.25-3-2-.38-2.93.29-5-.15a9.83,9.83,0,0,1-1.25-3,13.73,13.73,0,0,1,6.42-2.94c.77-.54.12-.8.15-1.6a171,171,0,0,1,45.35-8.58c26.43-1.1,53.68-4.73,79.64-6,6.25-.3,11.72.06,18.41.14,8.31.1,19.09-1,29.19-.12,6.25.54,13.67-.55,21.16-.56,39.35-.09,71.16-.23,112.28,2C317.24,171.93,315.11,173.81,314.05,172.88Zm64.22,16.05-1.6-.2C376.2,190,378.42,190.26,378.27,188.93Z">
                                    </path>
                                    <polygon class="cls-l3"
                                        points="52.57 166.06 57.61 151.99 23.11 157.23 32.36 169.12 52.57 166.06">
                                    </polygon>
                                    <polygon class="cls-l4" points="44.69 183.24 51.75 168.35 33.86 171.06 44.69 183.24">
                                    </polygon>
                                    <path class="cls-l4"
                                        d="M.12,10.15l22,145.06,35.47-5.39-22-145a2.6,2.6,0,0,0,0-.4C35,.76,26.62-.95,16.81.54S-.52,6.16,0,9.77A2.62,2.62,0,0,0,.12,10.15Z">
                                    </path>
                                    <path class="cls-l5"
                                        d="M433.68,79.45l21-36.33h27.56L448.48,97.51l34.9,56H456l-22-37.76-21.94,37.76H384.49l34.9-56L385.72,43.12h27.35Z">
                                    </path>
                                </g>
                            </g>
                        </svg>
					</div>
				</div>

				<!-- <div class="col-12 mt20 mt-md40 text-center">
					<div class="f-16 f-md-18 w700 lh160 white-clr"><span>Grab My 10 Exclusive Bonuses Before the Deal Ends…</span></div>
				</div> -->

				<div class="col-12 text-center lh150 mt20 mt-md40">
                    <div class="pre-heading-box f-18 f-md-22 w600 lh140 white-clr">
					We Exploited ChatGPT’s AI To Get A Share Of A $300 Billion Industry…
					</div>
                </div>

				<div class="col-12 col-md-12 px-md-0">
                    <div class="main-heading mt-md50 mt20 f-md-40 f-25 w900 text-center black2-clr lh150 red-hat-font">
                        <span class="main-heading-inner w700 f-md-40 white-clr">Next Gen AI App:</span> <span class="w400">Creates
                        </span>100's of AI-Boosted Courses <span class="w400">on Any Topic, and</span>
                        Sell Them on <span class="w400">Their</span>
                        <span class="pink">Own Self-Updating, Udemy-Like E-Learning Sites</span>
                        <span class="w400">in Just 60 Seconds!</span>
                    </div>
                </div>
			   <!-- <div class="col-12 mt-md15 mt15 text-center ">
                  <div class="f-22 f-md-28 w700 lh140 white-clr">
				  Then Promote Them To 495 MILLION Buyers For 100% Free…
                  </div>
               </div> -->
			   <div class="col-12 mt20 text-center mt-md40">
			   <div class="f-22 f-md-26 w700 lh140 white-clr post-heading red-hat-font">
                  Without Creating Anything…. Even A 100% Beginner Or Camera SHY Can Do It.
                  </div>
               </div>
			   <!-- <div class="col-12 mt15 text-center">
			   <div class="f-20 f-md-22 w500 lh140 white-clr text-center">
                  Without Us Creating Anything…. Even A 100% Beginner Or Camera SHY Can Do It.
                  </div></div> -->
			   <div class="col-12 mt15 text-center">
			   <div class="f-20 f-md-26 w600 lh140 yellow-clr red-hat-font">
                  Huge Opportunity to Transform Learnings into Earnings Today!
                  </div>
               </div>
				
            </div>
            <div class="row mt20 mt-md40">
                    <div class="video-frame col-12 col-md-10 mx-auto">
					<img src="assets/images/productbox.webp" class="img-fluid mx-auto d-block">
					   <!-- <div class="responsive-video">
						   <iframe src="https://coursesify.dotcompal.com/video/embed/o4eartift7" style="width:100%; height:100%" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
					   </div> -->
                </div>
            </div>
        </div>
    </div>
	
	<div class="second-section">
         <div class="container">
            <div class="row">
			<div class="col-12 col-md-6 ">
				<div class="f-16 f-md-20 lh160 w400">
					<ul class="kaptick pl0">
						<li>Tap Into $300 Billion Industry With Just 3 Steps </li>
						<li>Let AI Create Hundreds Of Courses For You On Autopilot </li>
						<li>Ai Finds Best Profitable Niches Ideas for You </li>
						<li>Ai Creates Our Websites Too, No Tech Setup Whatsoever</span></li>
						<li>50,000+ SEO Optimized Done For You Smoking Hot AI Courses </li>
						<li><span class="w600"> Even Traffic Is DFY. Without Spending A Dime </span></li>
						<li>Integrate Any Payment Processor Your Want </li>
						
                    </ul>
				</div>
            </div>
			<div class="col-12 col-md-6">
				<div class="f-16 f-md-20 lh160 w400">
					<ul class="kaptick pl0">
						<li>No Complicated Setup - Get Up And Running In 2 Minutes </li>
						<li>Build Your List With Autoresponder Integration </li>
						<li>AI ChatBots That Will Handle All Customer Support For You </li>
						<li>So Easy, Anyone Can Do it. </li>
                        <li>Cancel All Your Costly Subscriptions </li>
						<li>ZERO Upfront Cost </li>
						<li>30 Days Money-Back Guaran </li>
						<li>Free Commercial License Included - That's Priceless </li>
						<!-- <li><span class="w600">30 Days Money-Back Guarantee</span></li> -->
					</ul>
				</div>
			</div>
        </div>
            <div class="row mt20 mt-md30">
               <!-- CTA Btn Section Start -->
                <!-- CTA Btn Section Start -->
				<div class="col-md-12 col-md-12 col-12 text-center">
					<div class="f-md-26 f-18 text-center black lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
					<div class="f-md-24 f-22 text-center purple lh120 w700 mt15 mt-md20 lite-black-clr">TAKE ACTION NOW!</div>
					<div class="f-md-22 f-17 lh120 w600 mt15 mt-md20 black-clr">Use Special Coupon <span class="w800 lite-black-clr">HOLIDAY35</span> For Massive <span class="w800 lite-black-clr">35% Off</span> On Entire Funnel </div> 
				</div>
				<div class="col-md-12 col-12 text-center mt15 mt-md20">
					<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
						<span class="text-center">Grab LearnX + My 10 Exclusive Bonuses</span> <br>
					</a>
				</div>
				<div class="col-12 mt15 mt-md20 text-center">
					<img src="assets/images/payment.png" class="img-fluid">
				</div>
				<div class="col-12 mt15 mt-md20" align="center">
					<h3 class="f-md-28 f-20 w500 text-center black">Coupon Is Expiring In... </h3>
				</div>
				<!-- Timer -->
				<div class="col-12 text-center">
					<div class="countdown counter-black">
						<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Days</span> </div>
						<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
						<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
						<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
					</div>
				</div>
				<!-- Timer End -->
            </div>
         </div>
	</div>
  <!-- Step Section End -->
  <div class="step-section">
      <div class="container ">
         <div class="row ">
            <div class="col-12 f-28 f-md-50 w700 lh140 black-clr text-center">
               All It Takes Is 3 Fail-Proof Steps<br class="d-none d-md-block">
            </div>
            <div class="col-12 f-24 f-md-28 w600 lh140 white-clr text-center mt20">
               <div class="niche-design">
                  Start Your E-Learning Empire Today.
               </div>
            </div>
            <div class="col-12 mt30 mt-md90">
               <div class="row align-items-center ">
                  <div class="col-12 col-md-6">
                     <div class="step-shapes f-20 f-md-22 w600 white-clr">
                        Step 1
                     </div>
                     <div class="f-34 f-md-65 w700 lh140 mt15 caveat">
                        Login
                     </div>
                     <div class="f-20 f-md-24 w400 lh140 mt5 mt-md10">
                        Click On Any Of The Button Below To Get Instant Access To LearnX
                     </div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 ">
                     <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 2px solid #F27D66;">
                        <source src="assets/images/step1.mp4" type="video/mp4">
                     </video>
                  </div>
               </div>
            </div>
            <div class="col-12">
               <img src="assets/images/left-arrow.webp" class="img-fluid d-none d-md-block mx-auto " alt="Left Arrow">
            </div>
            <div class="col-12 mt30 mt-md30">
               <div class="row align-items-center ">
                  <div class="col-12 col-md-6 order-md-2">
                     <div class="step-shapes f-20 f-md-22 w600 white-clr">
                        Step 2
                     </div>
                     <div class="f-34 f-md-65 w700 lh140 mt15 caveat">
                        Deploy
                     </div>
                     <div class="f-20 f-md-24 w400 lh140 mt5 mt-md10">
                        Just select any niche you want, and let AI create your Udemy-Like website in 30 seconds or less (prefilled With AI Courses, And With DFY Traffic)
                     </div>
                  </div>
                  <div class="col-md-6 col-md-pull-6 mt20 mt-md0 order-md-1 ">
                     <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 2px solid #F27D66;">
                        <source src="assets/images/step2.mp4" type="video/mp4">
                     </video>
                  </div>
               </div>
            </div>
            <div class="col-12">
               <img src="assets/images/right-arrow.webp" class="img-fluid d-none d-md-block mx-auto" alt="Right Arrow">
            </div>
            <div class="col-12 mt30 mt-md30">
               <div class="row align-items-center ">
                  <div class="col-12 col-md-6">
                     <div class="step-shapes f-20 f-md-22 w600 white-clr">
                        Step 3
                     </div>
                     <div class="f-34 f-md-65 w700 lh140 mt15 caveat">
                        Profit
                     </div>
                     <div class="f-20 f-md-24 w400 lh140 mt5 mt-md10">
                        Sit back and relax. And let AI do the rest of the work. While you enjoy the results
                     </div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 ">
                     <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 2px solid #F27D66;">
                        <source src="https://cdn.oppyotest.com/launches/webgenie/preview/images/step3.mp4" type="video/mp4">
                     </video>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
	<!-----step section end---------->
	<!-----proof section---------->
	<div class="cta-btn-section dark-cta-bg">
	   <div class="container">
		  <div class="row">
			 <div class="col-md-12 col-md-12 col-12 text-center">
				<div class="f-md-20 f-18 text-center mt3 white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
				<div class="f-md-22 f-22 text-center purple lh120 w700 mt15 mt-md20 orange-clr">TAKE ACTION NOW!</div>
				<div class="f-md-22 f-17 lh120 w600 mt15 mt-md20 white-clr">Use Special Coupon <span class="w800 orange-clr">HOLIDAY35</span> For Massive <span class="w800 orange-clr">35% Off</span> On Entire Funnel </div>
			 </div>
			 <div class="col-md-12 col-12 text-center mt15 mt-md20">
			 <a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
				<span class="text-center">Grab LearnX + My 10 Exclusive Bonuses</span> <br>
				</a>
			 </div>
			 <div class="col-12 mt15 mt-md20 text-center">
				<img src="assets/images/payment.png" class="img-fluid">
			 </div>
			 <div class="col-12 mt15 mt-md20" align="center">
				<h3 class="f-md-28 f-20 w500 text-center white-clr">Coupon Is Expiring In... </h3>
			 </div>
			 <!-- Timer -->
			 <div class="col-12 text-center">
				<div class="countdown counter-white white-clr">
				   <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Days</span> </div>
				   <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
				   <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
				   <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
				</div>
			 </div>
			 <!-- Timer End -->
		  </div>
	   </div>
	</div>
	<!-----proof section---------->
	<div class="profitwall-sec">
      <div class="container">
	  <div class="row">
            <div class="col-12 text-center">
               <div class="f-28 f-md-45 w700 lh140 black-clr text-center dollars-head">
               LearnX Made It <u>Fail-Proof</u>  To Create A <br class="d-none d-md-block"> Profitable E-Learning Business Today. 
               </div>
               <div class="f-26 f-md-32 lh140 w600 text-center white-clr mt20">
               (Let AI Do All The Work For You)
               </div>
            </div>
         </div>
         <div class="row row-cols-1 row-cols-sm-2 row-cols-md-2 gap30 mt20 mt-md50">
            <div class="col mt20 mt-md50">
               <div class="text-center">
                  <img src="assets/images/proof1.webp" alt="No Coding" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-26 f-md-32 lh140 w600 text-center white-clr mt20 yellow-head">
                     Ai Creates Website For Us
                  </div>
               </div>
            </div>
            <div class="col mt20 mt-md50">
               <div class="text-center">
                  <img src="assets/images/proof2.webp" alt="Ai Gives Us Best Niches" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-26 f-md-32 lh140 w600 text-center white-clr mt20 yellow-head">
                  Ai Gives Us Best Niches
                  </div>
               </div>
            </div>
            <div class="col mt20 mt-md50">
               <div class="text-center">
                  <img src="assets/images/proof3.webp" alt="Ai Create Courses For Us" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-26 f-md-32 lh140 w600 text-center white-clr mt20 yellow-head">
                  Ai Create Courses For Us
                  </div>
               </div>
            </div>
            <div class="col mt20 mt-md50">
               <div class="text-center">
                  <img src="assets/images/proof4.webp" alt="Ai Generate Traffic For Us" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-26 f-md-32 lh140 w600 text-center white-clr mt20 yellow-head">
                  Ai Generate Traffic For Us
                  </div>
               </div>
            </div>
            <div class="col mt20 mt-md50">
               <div class="text-center">
                  <img src="assets/images/proof5.webp" alt="Ai Generate Sales Pages For Us" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-26 f-md-32 lh140 w600 text-center white-clr mt20 yellow-head">
                  Ai Generate Sales Pages For Us
                  </div>
               </div>
            </div>
            <div class="col mt20 mt-md50">
               <div class="text-center">
                  <img src="assets/images/proof6.webp" alt="Ai Generate Chat Bots For Customer Support For Us" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-26 f-md-32 lh140 w600 text-center white-clr mt20 yellow-head">
                  Ai Generate Chat Bots For Customer Support For Us
                  </div>
               </div>
            </div>

            <div class="col mt20 mt-md50">
               <div class="text-center">
                  <img src="assets/images/proof7.webp" alt="No Coding" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-26 f-md-32 lh140 w600 text-center white-clr mt20 red-head">
                  No Coding
                  </div>
               </div>
            </div>
            <div class="col mt20 mt-md50">
               <div class="text-center">
                  <img src="assets/images/proof8.webp" alt="No Designing" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-26 f-md-32 lh140 w600 text-center white-clr mt20 red-head">
                  No Designing
                  </div>
               </div>
            </div>
            <div class="col mt20 mt-md50">
               <div class="text-center">
                  <img src="assets/images/proof9.webp" alt="No Content Writing" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-26 f-md-32 lh140 w600 text-center white-clr mt20 red-head">
                  No Content Writing
                  </div>
               </div>
            </div>
            <div class="col mt20 mt-md50">
               <div class="text-center">
                  <img src="assets/images/proof10.webp" alt="No Optimizing" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-26 f-md-32 lh140 w600 text-center white-clr mt20 red-head">
                  No Optimizing
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
    
	<!---proof section end---->
	  
<!-- Video Testimonial -->
	<!-- CTA Button Section Start -->
	<div class="cta-btn-section dark-cta-bg">
	   <div class="container">
		  <div class="row">
			 <div class="col-md-12 col-md-12 col-12 text-center">
				<div class="f-md-20 f-18 text-center mt3 white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
				<div class="f-md-22 f-22 text-center purple lh120 w700 mt15 mt-md20 orange-clr">TAKE ACTION NOW!</div>
				<div class="f-md-22 f-17 lh120 w600 mt15 mt-md20 white-clr">Use Special Coupon <span class="w800 orange-clr">HOLIDAY35</span> For Massive <span class="w800 orange-clr">35% Off</span> On Entire Funnel </div>
			 </div>
			 <div class="col-md-12 col-12 text-center mt15 mt-md20">
			 <a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
				<span class="text-center">Grab LearnX + My 10 Exclusive Bonuses</span> <br>
				</a>
			 </div>
			 <div class="col-12 mt15 mt-md20 text-center">
				<img src="assets/images/payment.png" class="img-fluid">
			 </div>
			 <div class="col-12 mt15 mt-md20" align="center">
				<h3 class="f-md-28 f-20 w500 text-center white-clr">Coupon Is Expiring In... </h3>
			 </div>
			 <!-- Timer -->
			 <div class="col-12 text-center">
				<div class="countdown counter-white white-clr">
				   <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Days</span> </div>
				   <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
				   <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
				   <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
				</div>
			 </div>
			 <!-- Timer End -->
		  </div>
	   </div>
	</div>
	<!-- CTA Button Section End -->
	
	<div class="proudly-section" id="product">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="prdly-pres f-28 f-md-38 w700 lh140 text-center white-clr">
                  Introducing
                  </div>
               </div>
               <div class="col-12 mt-md50 mt20 text-center">
			   <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                            viewBox="0 0 484.58 200.84" style="max-height:167px;">
                            <defs>
                                <style>
                                    .cls-l1,
                                    .cls-l4 {
                                        fill: #fff;
                                    }

                                    .cls-l2,
                                    .cls-l3,
                                    .cls-l4 {
                                        fill-rule: evenodd;
                                    }

                                    .cls-l2 {
                                        fill: url(#linear-gradient);
                                    }

                                    .cls-l3 {
                                        fill: url(#linear-gradient-2);
                                    }

                                    .cls-l5 {
                                        fill: url(#linear-gradient-3);
                                    }
                                </style>
                                <linearGradient id="linear-gradient" x1="-154.67" y1="191.86" x2="249.18" y2="191.86"
                                    gradientTransform="matrix(1.19, -0.01, 0.05, 1, 178.46, -4.31)"
                                    gradientUnits="userSpaceOnUse">
                                    <stop offset="0" stop-color="#ead40c"></stop>
                                    <stop offset="1" stop-color="#ff00e9"></stop>
                                </linearGradient>
                                <linearGradient id="linear-gradient-2" x1="23.11" y1="160.56" x2="57.61" y2="160.56"
                                    gradientTransform="matrix(1, 0, 0, 1, 0, 0)" xlink:href="#linear-gradient">
                                </linearGradient>
                                <linearGradient id="linear-gradient-3" x1="384.49" y1="98.33" x2="483.38" y2="98.33"
                                    gradientTransform="matrix(1, 0, 0, 1, 0, 0)" xlink:href="#linear-gradient">
                                </linearGradient>
                            </defs>
                            <g id="Layer_2" data-name="Layer 2">
                                <g id="Layer_1-2" data-name="Layer 1">
                                    <path class="cls-l1"
                                        d="M112.31,154.3q-11.65,0-20.1-4.87a32.56,32.56,0,0,1-13-13.82q-4.53-9-4.54-21.11a46.06,46.06,0,0,1,4.57-21A34.29,34.29,0,0,1,92,79.37a36.11,36.11,0,0,1,19.31-5.06,39.65,39.65,0,0,1,13.54,2.29,31,31,0,0,1,11.3,7.09,33.32,33.32,0,0,1,7.75,12.18,49.23,49.23,0,0,1,2.82,17.57V119H83.26v-12.3h46A19.87,19.87,0,0,0,127,97.38a16.6,16.6,0,0,0-6.18-6.48,17.59,17.59,0,0,0-9.21-2.37,17.88,17.88,0,0,0-9.83,2.7,18.81,18.81,0,0,0-6.58,7.06,20.17,20.17,0,0,0-2.4,9.56v10.74a25.15,25.15,0,0,0,2.47,11.57,17.42,17.42,0,0,0,6.91,7.37,20.52,20.52,0,0,0,10.39,2.55,21.62,21.62,0,0,0,7.22-1.14,15.71,15.71,0,0,0,5.59-3.35,14.11,14.11,0,0,0,3.59-5.5L146,132a26.44,26.44,0,0,1-6.13,11.77,29.72,29.72,0,0,1-11.52,7.77A43.74,43.74,0,0,1,112.31,154.3Z">
                                    </path>
                                    <path class="cls-l1"
                                        d="M184.49,154.35a31.78,31.78,0,0,1-13.24-2.65,21.26,21.26,0,0,1-9.28-7.84,22.86,22.86,0,0,1-3.41-12.81A21.92,21.92,0,0,1,161,120.2a18.9,18.9,0,0,1,6.61-6.86,33.85,33.85,0,0,1,9.46-3.9,77.76,77.76,0,0,1,10.92-2q6.81-.7,11-1.28a15.91,15.91,0,0,0,6.18-1.82,4.25,4.25,0,0,0,1.94-3.86v-.3q0-5.7-3.38-8.83T194,88.28q-6.71,0-10.62,2.92a14.55,14.55,0,0,0-5.27,6.91l-17-2.42a27.51,27.51,0,0,1,6.66-11.83,29.46,29.46,0,0,1,11.35-7.16,43.73,43.73,0,0,1,14.83-2.39,47.89,47.89,0,0,1,11.14,1.31,31.6,31.6,0,0,1,10.14,4.31,22.12,22.12,0,0,1,7.39,8.14q2.81,5.15,2.8,12.87v51.85H207.84V142.14h-.61a22.1,22.1,0,0,1-4.66,6,22.35,22.35,0,0,1-7.52,4.49A30,30,0,0,1,184.49,154.35Zm4.74-13.42a19.7,19.7,0,0,0,9.53-2.19,16,16,0,0,0,6.23-5.83,15,15,0,0,0,2.19-7.91v-9.13a8.72,8.72,0,0,1-2.9,1.31,45.56,45.56,0,0,1-4.56,1.06c-1.68.3-3.35.57-5,.8l-4.29.61a32,32,0,0,0-7.32,1.81A12.29,12.29,0,0,0,178,125a9.76,9.76,0,0,0,1.82,13.39A16,16,0,0,0,189.23,140.93Z">
                                    </path>
                                    <path class="cls-l1"
                                        d="M243.8,152.79V75.31h17.7V88.23h.81a19.36,19.36,0,0,1,7.29-10.37,20,20,0,0,1,11.83-3.66c1,0,2.14,0,3.4.13a23.83,23.83,0,0,1,3.15.38V91.5a21,21,0,0,0-3.65-.73,38.61,38.61,0,0,0-4.82-.32,18.47,18.47,0,0,0-8.95,2.14,15.94,15.94,0,0,0-6.23,5.93,16.55,16.55,0,0,0-2.27,8.72v45.55Z">
                                    </path>
                                    <path class="cls-l1"
                                        d="M318.4,107.39v45.4H300.14V75.31h17.45V88.48h.91a22,22,0,0,1,8.55-10.34q5.86-3.84,14.55-3.83a27.58,27.58,0,0,1,14,3.43,23.19,23.19,0,0,1,9.28,9.93,34.22,34.22,0,0,1,3.26,15.79v49.33H349.87V106.28c0-5.17-1.34-9.23-4-12.15s-6.37-4.39-11.07-4.39a17,17,0,0,0-8.5,2.09,14.67,14.67,0,0,0-5.8,6A20,20,0,0,0,318.4,107.39Z">
                                    </path>
                                    <path class="cls-l2"
                                        d="M314.05,172.88c12.69-.14,27.32,2.53,40.82,2.61l-.09,1c1.07.15,1-.88,2-.78,1.42-.18,0,1.57,1.82,1.14.54-.06.08-.56-.32-.68.77,0,2.31.17,3,1.26,1.3,0-.9-1.34.85-.89,6,1.63,13.39,0,20,3.2,11.64.72,24.79,2.38,38,4.27,2.85.41,5.37,1.11,7.57,1.05,2-.05,5.68.78,8,1.08,2.53.34,5.16,1.56,7.13,1.65-1.18-.05,5.49-.78,4.54.76,5-.72,10.9,1.67,15.94,1.84.83-.07.69.53.67,1,1.23-1.17,3.32.56,4.28-.56.25,2,2.63-.48,3.3,1.61,2.23-1.39,4.83.74,7.55,1.37,1.91.44,5.29-.21,5.55,2.14-2.1-.13-5.12.81-11.41-1.09,1,.87-3.35.74-3.39-.64-1.81,1.94-6.28-1-7.79,1.19-1.26-.41,0-.72-.64-1.35-5,.55-8.09-1.33-12.91-1.56,2.57,2.44,6.08,3.16,10.06,3.22,1.28.15,0,.74,1,1.39,1.37-.19.75-.48,1.26-1.17,5.37,2.28,15.36,2.35,21,4.91-4.69-.63-11.38,0-15.15-2.09A148,148,0,0,1,441.58,196c-.53-.1-1.81.12-.7-.71-1.65.73-3.41.1-5.74-.22-37.15-5.15-80.47-7.78-115.75-9.76-39.54-2.22-76.79-3.22-116.44-2.72-61.62.78-119.59,7.93-175.21,13.95-5,.55-10,1.49-15.11,1.47-1.33-.32-2.46-1.91-1.25-3-2-.38-2.93.29-5-.15a9.83,9.83,0,0,1-1.25-3,13.73,13.73,0,0,1,6.42-2.94c.77-.54.12-.8.15-1.6a171,171,0,0,1,45.35-8.58c26.43-1.1,53.68-4.73,79.64-6,6.25-.3,11.72.06,18.41.14,8.31.1,19.09-1,29.19-.12,6.25.54,13.67-.55,21.16-.56,39.35-.09,71.16-.23,112.28,2C317.24,171.93,315.11,173.81,314.05,172.88Zm64.22,16.05-1.6-.2C376.2,190,378.42,190.26,378.27,188.93Z">
                                    </path>
                                    <polygon class="cls-l3"
                                        points="52.57 166.06 57.61 151.99 23.11 157.23 32.36 169.12 52.57 166.06">
                                    </polygon>
                                    <polygon class="cls-l4" points="44.69 183.24 51.75 168.35 33.86 171.06 44.69 183.24">
                                    </polygon>
                                    <path class="cls-l4"
                                        d="M.12,10.15l22,145.06,35.47-5.39-22-145a2.6,2.6,0,0,0,0-.4C35,.76,26.62-.95,16.81.54S-.52,6.16,0,9.77A2.62,2.62,0,0,0,.12,10.15Z">
                                    </path>
                                    <path class="cls-l5"
                                        d="M433.68,79.45l21-36.33h27.56L448.48,97.51l34.9,56H456l-22-37.76-21.94,37.76H384.49l34.9-56L385.72,43.12h27.35Z">
                                    </path>
                                </g>
                            </g>
                        </svg>
               </div>
               <div class="text-center mt20 mt-md50">
                  <div class="pre-heading1 f-md-22 f-20 w600 lh140 white-clr">
                  Exploit The Emerging E-Learning Business With A Click…  
                  </div>
               </div>
               <div class="f-md-50 f-28 w700 white-clr lh140 mt-md30 mt20 text-center">
                  First To Market AI System <span class="yellow-clr"> Creates Self-Managed Academy Sites, And Prefill It With Tons of Unique AI-Generated Courses</span>
               </div>
               <div class="f-22 f-md-28 lh140 w400 text-center white-clr mt20 mt-md30">
			   Finally, Let AI Build You A Passive Income Stream Without Doing Any Work
               </div>
            </div>
            <div class="row d-flex flex-wrap align-items-center mt20 mt-md50">
               <div class="col-12 col-md-10 mx-auto">
                  <img src="assets/images/productbox.webp" class="img-fluid d-block mx-auto margin-bottom-15">
               </div>
            </div>
         </div>
      </div>
	<!-------Exclusive Bonus----------->
<div class="exclusive-bonus">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <div class="white-clr f-24 f-md-36 lh140 w600">
                  Exclusive Bonus #1 : Ninja AI
               </div>
               <div class="f-18 f-md-20 w500 mt20 white-clr">
                  10 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-------Exclusive Bonus----------->
   <div class="section-Profitable">
        <div class="container">
            <div class="row">
               	<div class="col-md-12 text-center">
				   <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 645.02 151.26" style="max-height:70px">
    <defs>
        <style>
        .cls-n1 {
            fill: url(#linear-gradient-2n);
        }
    
        .cls-n2 {
            fill: #fff;
        }
    
        .cls-n3 {
            fill-rule: evenodd;
        }
    
        .cls-n3, .cls-n4 {
            fill: #ff813b;
        }
    
        .cls-n5 {
            fill: url(#linear-gradient-3n);
        }
    
        .cls-n6 {
            fill: url(#linear-gradientn);
        }
    
        .cls-n7 {
            fill: url(#linear-gradient-4n);
        }
        </style>
        <linearGradient id="linear-gradientn" x1="47.81" y1="75" x2="185.48" y2="75" gradientUnits="userSpaceOnUse">
        <stop offset="0" stop-color="#b956ff"></stop>
        <stop offset="1" stop-color="#6e33ff"></stop>
        </linearGradient>
        <linearGradient id="linear-gradient-2n" x1="0" y1="75" x2="85.18" y2="75" gradientUnits="userSpaceOnUse">
        <stop offset="0" stop-color="#b956ff"></stop>
        <stop offset="1" stop-color="#a356fa"></stop>
        </linearGradient>
        <linearGradient id="linear-gradient-3n" x1="55.61" y1="62.54" x2="62.86" y2="62.54" gradientUnits="userSpaceOnUse">
        <stop offset="0" stop-color="#b956ff"></stop>
        <stop offset="1" stop-color="#ac59fa"></stop>
        </linearGradient>
        <linearGradient id="linear-gradient-4n" x1="54.62" y1="79" x2="61.87" y2="79" gradientTransform="translate(19.64 -11.24) rotate(13.24)" xlink:href="#linear-gradient-3n"></linearGradient>
    </defs>
    <g id="Layer_1-2" data-name="Layer 1">
        <g>
        <g>
            <g>
            <path class="cls-n6" d="m181.49,58.53h-10.72c-1.4-5.13-3.43-9.99-6.01-14.51l7.58-7.58c1.56-1.56,1.56-4.09,0-5.65l-17.65-17.65c-1.56-1.56-4.09-1.56-5.65,0l-7.58,7.58c-4.51-2.58-9.38-4.61-14.51-6.01V4c0-2.21-1.79-4-4-4h-24.96c-2.21,0-4,1.79-4,4v10.72c-.16.04-.31.09-.47.14-.27.08-.53.15-.8.23-.27.08-.55.16-.82.24-.55.17-1.09.35-1.63.53-.11.04-.22.08-.33.12-.48.17-.96.34-1.44.52-.12.04-.24.09-.36.14-.52.2-1.04.4-1.56.62-.04.02-.08.03-.12.05-2.41,1-4.74,2.15-6.99,3.43l-7.57-7.57c-1.56-1.56-4.09-1.56-5.65,0l-17.65,17.65c-.36.36-.63.76-.82,1.2h61.99v.02c.23,0,.45-.02.68-.02,23.76,0,43.01,19.26,43.01,43.01s-19.26,43.01-43.01,43.01c-.5,0-1-.02-1.5-.04v.04h-61.17c.19.43.46.84.82,1.2l17.65,17.65c1.56,1.56,4.09,1.56,5.65,0l7.57-7.57c2.25,1.28,4.58,2.43,6.99,3.43.04.02.08.03.12.05.51.21,1.03.42,1.55.62.12.05.24.09.36.14.47.18.95.35,1.43.52.11.04.23.08.34.12.54.18,1.08.36,1.63.53.27.08.55.16.83.25.26.08.53.16.79.23.16.04.31.09.47.14v10.72c0,2.21,1.79,4,4,4h24.96c2.21,0,4-1.79,4-4v-10.72c5.13-1.4,9.99-3.43,14.51-6.01l7.58,7.58c1.56,1.56,4.09,1.56,5.65,0l17.65-17.65c1.56-1.56,1.56-4.09,0-5.65l-7.58-7.58c2.58-4.51,4.61-9.38,6.01-14.51h10.72c2.21,0,4-1.79,4-4v-24.96c0-2.21-1.79-4-4-4Z"></path>
            <path class="cls-n1" d="m76.64,101.54c-5.74-7.31-9.18-16.52-9.18-26.54s3.44-19.23,9.18-26.54c2.45-3.13,5.33-5.9,8.53-8.24h-48.39c-1.31-1.89-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.13h17.07c-1.51,3.21-2.76,6.58-3.71,10.07h3.1c1.31-1.89,3.49-3.13,5.97-3.13,4.01,0,7.25,3.25,7.25,7.25s-3.22,7.23-7.21,7.25h-.04c-2.48,0-4.66-1.24-5.97-3.13H13.22c-.44-.64-.98-1.19-1.59-1.65-1.21-.92-2.73-1.48-4.38-1.48-4,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.14h38.82c1.31-1.89,3.49-3.13,5.97-3.13.43,0,.84.05,1.25.12,3.41.59,6,3.56,6,7.14,0,2.78-1.56,5.19-3.86,6.41-1.01.53-2.16.84-3.39.84-2.48,0-4.66-1.24-5.97-3.13h-17.85c-1.31-1.9-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25s3.25,7.25,7.25,7.25c2.48,0,4.66-1.24,5.97-3.13h15.96c.95,3.48,2.2,6.85,3.71,10.07h-5.63c-1.31-1.89-3.49-3.13-5.97-3.13-4.01,0-7.25,3.25-7.25,7.25,0,1.53.48,2.95,1.28,4.12,1.31,1.9,3.49,3.14,5.97,3.14s4.66-1.24,5.97-3.14h36.95c-3.21-2.34-6.08-5.11-8.53-8.24ZM30.64,47.97c-2,0-3.62-1.62-3.62-3.62s1.62-3.62,3.62-3.62,3.63,1.62,3.63,3.62-1.63,3.62-3.63,3.62Zm-23.39,26.47c-2,0-3.62-1.63-3.62-3.62s1.63-3.63,3.62-3.63,3.63,1.63,3.63,3.63-1.63,3.62-3.63,3.62Zm20.97,16.45c-2,0-3.63-1.63-3.63-3.63s1.63-3.62,3.63-3.62,3.62,1.63,3.62,3.62-1.63,3.63-3.62,3.63Zm14.08,18.39c-2,0-3.62-1.62-3.62-3.62s1.62-3.62,3.62-3.62,3.62,1.62,3.62,3.62-1.62,3.62-3.62,3.62Z"></path>
            <circle class="cls-n5" cx="59.23" cy="62.54" r="3.63"></circle>
            <circle class="cls-n7" cx="58.24" cy="79" r="3.63" transform="translate(-16.54 15.44) rotate(-13.24)"></circle>
            </g>
            <g>
            <g>
                <path class="cls-n3" d="m104.63,78.16c-9.44,6-14.52,2.86-15.25-9.4,4.93,3.38,10.02,6.52,15.25,9.4Z"></path>
                <path class="cls-n3" d="m131.01,68.76c-.72,12.26-5.81,15.4-15.25,9.4,5.24-2.88,10.32-6.02,15.25-9.4Z"></path>
            </g>
            <path class="cls-n4" d="m110.16,40.13c-.89,0-1.78.03-2.66.1-.08,0-.16,0-.24.02-9.59.79-18.07,5.45-23.89,12.42-.25.3-.49.59-.73.89-.14.19-.29.38-.43.57-4.02,5.37-6.52,11.93-6.88,19.07,0,.01,0,.03,0,.04-.01.22-.02.45-.03.68-.01.3-.02.6-.02.9v.18c0,.28,0,.55,0,.83.44,18.64,15.5,33.66,34.15,34.04.24,0,.48,0,.72,0,.3,0,.6,0,.9,0,18.85-.48,33.98-15.9,33.98-34.87s-15.61-34.88-34.88-34.88Zm.19,39.35c-14.41,14.37-29.15,1.23-29.15-9.45s13.05,0,29.15,0,29.15-10.67,29.15,0-14.74,23.81-29.15,9.45Z"></path>
            </g>
        </g>
        <g>
            <path class="cls-n2" d="m286.65,118.51h-17.37l-39.32-59.42v59.42h-17.37V31.8h17.37l39.32,59.54V31.8h17.37v86.71Z"></path>
            <path class="cls-n2" d="m304.95,38.69c-2.03-1.94-3.04-4.36-3.04-7.26s1.01-5.31,3.04-7.26c2.03-1.94,4.57-2.92,7.63-2.92s5.6.97,7.63,2.92c2.02,1.94,3.04,4.36,3.04,7.26s-1.02,5.31-3.04,7.26c-2.03,1.94-4.57,2.91-7.63,2.91s-5.6-.97-7.63-2.91Zm16.19,11.1v68.72h-17.37V49.79h17.37Z"></path>
            <path class="cls-n2" d="m396.18,56.55c5.04,5.17,7.57,12.38,7.57,21.65v40.31h-17.36v-37.96c0-5.46-1.37-9.65-4.1-12.59-2.73-2.94-6.45-4.4-11.16-4.4s-8.58,1.47-11.35,4.4c-2.77,2.94-4.15,7.13-4.15,12.59v37.96h-17.37V49.79h17.37v8.56c2.31-2.98,5.27-5.31,8.87-7.01,3.6-1.69,7.55-2.54,11.85-2.54,8.19,0,14.8,2.59,19.85,7.75Z"></path>
            <path class="cls-n2" d="m437.61,129.8c0,7.61-1.88,13.09-5.64,16.44-3.77,3.35-9.16,5.02-16.19,5.02h-7.69v-14.76h4.96c2.64,0,4.51-.52,5.58-1.55,1.07-1.03,1.61-2.71,1.61-5.02V49.79h17.37v80.01Zm-16.31-91.11c-2.03-1.94-3.04-4.36-3.04-7.26s1.01-5.31,3.04-7.26c2.02-1.94,4.61-2.92,7.75-2.92s5.58.97,7.57,2.92c1.98,1.94,2.98,4.36,2.98,7.26s-.99,5.31-2.98,7.26c-1.98,1.94-4.51,2.91-7.57,2.91s-5.73-.97-7.75-2.91Z"></path>
            <path class="cls-n2" d="m454.42,65.42c2.77-5.37,6.53-9.51,11.29-12.4,4.76-2.89,10.07-4.34,15.94-4.34,5.13,0,9.61,1.04,13.46,3.1,3.85,2.07,6.92,4.67,9.24,7.82v-9.8h17.49v68.72h-17.49v-10.05c-2.23,3.23-5.31,5.89-9.24,8-3.93,2.11-8.46,3.16-13.58,3.16-5.79,0-11.06-1.49-15.82-4.47-4.76-2.98-8.52-7.17-11.29-12.59-2.77-5.41-4.16-11.64-4.16-18.67s1.38-13.11,4.16-18.48Zm47.45,7.88c-1.65-3.02-3.89-5.33-6.7-6.95-2.81-1.61-5.83-2.42-9.06-2.42s-6.2.79-8.93,2.36c-2.73,1.57-4.94,3.87-6.64,6.88-1.69,3.02-2.54,6.6-2.54,10.73s.85,7.75,2.54,10.85c1.69,3.1,3.93,5.48,6.7,7.13,2.77,1.66,5.72,2.48,8.87,2.48s6.24-.81,9.06-2.42c2.81-1.61,5.04-3.93,6.7-6.95,1.65-3.02,2.48-6.64,2.48-10.85s-.83-7.83-2.48-10.85Z"></path>
            <path class="cls-n4" d="m591.93,102.01h-34.48l-5.71,16.5h-18.24l31.14-86.71h20.22l31.13,86.71h-18.36l-5.71-16.5Zm-4.71-13.89l-12.53-36.22-12.53,36.22h25.06Z"></path>
            <path class="cls-n4" d="m645.02,31.93v86.58h-17.37V31.93h17.37Z"></path>
        </g>
        </g>
    </g>
</svg>
               </div>
               <div class="col-12 text-center lh150 mt20 mt-md70">
                  <div class="pre-heading f-18 f-md-22 w600 lh140 white-clr">
                     Legally... Swipe Guru's Profitable Funnels with Our “ChatGPT4 Secret Agent”
                  </div>
               </div>
               <div class="col-12 mt-md30 mt20 f-md-42 f-28 w400 text-center white-clr lh140">
                  World's First Google-Killer Ai App Builds Us <br class="d-none d-md-block">
                  <span class="white-clr w700">DFY Profitable Affiliate Funnel Sites, Prefilled with Content, Reviews &amp; Lead Magnet in Just 60 Seconds.</span>
               </div>
               <div class="col-12 mt-md15 mt15 text-center">
                  <div class="f-22 f-md-28 w700 lh140 black-clr yellow-brush">
                     Then Promote Them to Our Secret Buyers Network of 496 million Users...
                  </div>
               </div>
               <div class="col-12 mt20 text-center">
                  <div class="f-22 f-md-28 w400 lh140 white-clr">
                     &amp; Making Us <span class="w600 underline">$955.45 Commissions Daily</span>  with Literally Zero Work                 
                  </div>
               </div>
               <div class="col-12 mt20 text-center">
                  <div class="f-20 f-md-22 w400 lh140 red-bg white-clr">
                     No Designing, No Copywriting, &amp; No Hosting…. Even A 100% Beginner Can Do It.
                  </div>
               </div>
            </div>

            <div class="row mt20 mt-md40 gx-md-5">
               <div class="col-md-7 col-12">
                  <div class="col-12">
                     <div class="f-18 f-md-19 w500 lh140 white-clr shape-head">
                     Watch How We Swiped A Profitable Funnel With Just One Click And Made $30,345.34 Per Month On Complete Autopilot…
                     </div>
                     
                     <div class="video-box mt20 mt-md30">
                        <!-- <img src="assets/images/video-thumb.webp" alt="Video" class="mx-auto d-block img-fluid"> -->
                        <div style="padding-bottom: 56.25%;position: relative;"><iframe src="https://ninjaai.dotcompal.com/video/embed/q5429rqlji" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe></div>
                     </div>
                  </div>
               </div>
               <div class="col-md-5 col-12 mt20 mt-md0">
                  <div class="key-features-bg">
                     <ul class="list-head pl0 m0 f-16 f-md-18 lh160 w400 white-clr">
                        <li><span class="w600">Eliminate All The Guesswork</span> And Jump Straight to Results</li>
                        <li><span class="w600">Let Ai Find</span> Hundreds Of The Guru's Profitable Funnels</li>
                        <li><span class="w600">Let Ai Create</span> Proven Money Making Funnels On Autopilot</li>
                        <li class="w600">All The Copywriting And Designing Is Done For You</li>
                        <li>Generate 100% <span class="w600">SEO &amp; Mobile Optimized</span> Funnels</li>
                        <li><span class="w600">Instant High Converting Traffic For 100% Free…</span> No Need To Run Ads</li>
                        <li>98% Of Beta Testers Made At Least One Sale <span class="w600 underline">Within 24 Hours</span> Of Using Ninja AI.</li>
                        <li><span class="w600">A True Ai App</span> That Leverages ChatGPT4</li>
                        <li>Connect Your Favourite Autoresponders And <span class="w600">Build Your List FAST.</span></li>
                        <li>Seamless Integration With Any Payment Processor You Want</li>
                        <li class="w600">ZERO Upfront Cost</li>
                        <li>No Complicated Setup - Get Up And Running In 2 Minutes</li>
                        <li class="w600">So Easy, Anyone Can Do it.</li>
                        <li>30 Days Money-Back Guarantee</li>
                        <li><span class="w600">Free Commercial License Included -</span> That's Priceless</li>
                     </ul>
                  </div>
               </div>
            </div> 

			<!--<div class="row mt20 mt-md40">-->
			<!--	<div class="col-md-12 mx-auto col-12 mt20 mt-md20 text-center affiliate-link-btn1">-->
			<!--		<a href="https://www.getninjaai.com/signup"> Please Signup For Special Gift </a>-->
			<!--	</div>-->
   <!--         </div>-->
        </div>
    </div>
	<div class="gr-1">
      <div class="container-fluid p0">
         <picture>
            <source media="(min-width:768px)" srcset="assets/images/ninja-ai.webp">
            <source media="(min-width:320px)" srcset="assets/images/ninja-ai-mview.webp">
            <img src="assets/images/ninja-ai.webp" alt="Steps" style="width:100%;">
         </picture>
      </div>
   </div>
   <!-------Exclusive Bonus----------->
   <div class="exclusive-bonus">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <div class="white-clr f-24 f-md-36 lh140 w600">
                  Exclusive Bonus #2 : TubeTraffic AI 
               </div>
               <div class="f-18 f-md-20 w500 mt20 white-clr">
                  10 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
               </div>
            </div>
         </div>
      </div>
   </div>


   <div class="header-section-mailergpt">
         <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
            <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 800 131.43" style="max-height:75px;">
                        <defs>
                            <style>
                            .cls-1tb {
                                fill: #fff;
                            }
        
                            .cls-2tb {
                                fill: #fd2f15;
                            }
        
                            .cls-3tb {
                                fill: #fbbf33;
                            }
                            </style>
                        </defs>
                        <g id="Layer_1-2" data-name="Layer 1">
                            <g>
                            <g>
                                <path class="cls-3tb" d="m105.61,78.06l40.05,23.12c6.01,3.47,13.51-.87,13.51-7.8v-46.25c0-6.93-7.51-11.27-13.51-7.8l-40.05,23.12c-6.01,3.47-6.01,12.14,0,15.6Z"></path>
                                <g>
                                <rect class="cls-3tb" x="26.92" y="58.86" width="16.03" height="46.81"></rect>
                                <path class="cls-2tb" d="m125.22,26.34h-7.16v46.06c0,32.6-26.43,59.03-59.03,59.03H0v-65.94C0,32.89,26.43,6.45,59.03,6.45h38.52l-12.38,19.88h-26.14c-21.59,0-39.16,17.57-39.16,39.15v46.07h39.16c21.59,0,39.16-17.57,39.16-39.16V26.34h-5.79L108.81,0l16.41,26.34Z"></path>
                                <path class="cls-3tb" d="m67.04,46.21h-16.02v59.46h8.01c2.76,0,5.45-.35,8.01-.99v-58.47Z"></path>
                                <path class="cls-3tb" d="m90.77,33.56h-16.03v68.14c7.57-4.07,13.39-10.99,16.03-19.31v-48.83Z"></path>
                                </g>
                            </g>
                            <g>
                                <g>
                                <path class="cls-1tb" d="m195.69,38.68v-5.58h49.35v5.58h-21.57v62.48h-6.21v-62.48h-21.57Z"></path>
                                <path class="cls-1tb" d="m283.29,81.65v-31.54h5.92v51.05h-5.92v-8.47h-.47c-1.2,2.61-3.09,4.79-5.68,6.53-2.59,1.74-5.76,2.61-9.5,2.61-3.26,0-6.15-.71-8.67-2.14-2.53-1.43-4.51-3.53-5.95-6.3-1.44-2.77-2.16-6.17-2.16-10.2v-33.07h5.92v32.7c0,3.99,1.17,7.2,3.52,9.62,2.35,2.43,5.36,3.64,9.04,3.64,2.3,0,4.52-.55,6.65-1.66,2.13-1.11,3.88-2.73,5.25-4.87,1.37-2.14,2.06-4.77,2.06-7.89Z"></path>
                                <path class="cls-1tb" d="m306.05,101.16V33.09h5.95v26.75h.53c.66-1.53,1.62-3.1,2.87-4.72,1.25-1.62,2.9-2.97,4.95-4.07,2.05-1.1,4.64-1.65,7.76-1.65,4.19,0,7.86,1.1,11.03,3.31,3.17,2.21,5.64,5.28,7.41,9.24,1.77,3.95,2.66,8.56,2.66,13.81s-.88,9.89-2.64,13.86c-1.76,3.97-4.22,7.06-7.38,9.27-3.16,2.22-6.82,3.32-10.98,3.32-3.1,0-5.69-.55-7.76-1.66-2.07-1.11-3.74-2.48-5-4.1-1.26-1.63-2.24-3.22-2.92-4.77h-.73v9.47h-5.75Zm5.85-25.49c0,4.12.61,7.77,1.84,10.95,1.23,3.18,3,5.67,5.32,7.48,2.31,1.81,5.12,2.71,8.42,2.71s6.26-.94,8.61-2.81c2.35-1.87,4.13-4.4,5.35-7.59,1.22-3.19,1.83-6.77,1.83-10.73s-.6-7.46-1.81-10.6c-1.21-3.15-2.99-5.64-5.33-7.49-2.35-1.85-5.23-2.78-8.64-2.78s-6.14.89-8.46,2.68c-2.32,1.78-4.08,4.24-5.3,7.38-1.22,3.14-1.83,6.74-1.83,10.82Z"></path>
                                <path class="cls-1tb" d="m382.86,102.22c-4.76,0-8.88-1.11-12.36-3.34-3.48-2.23-6.16-5.32-8.06-9.27s-2.84-8.51-2.84-13.68.95-9.71,2.84-13.71c1.89-4,4.52-7.13,7.88-9.41,3.36-2.27,7.24-3.41,11.65-3.41,2.77,0,5.44.5,8.01,1.51,2.57,1.01,4.88,2.55,6.93,4.62,2.05,2.07,3.67,4.68,4.87,7.81,1.2,3.14,1.79,6.84,1.79,11.12v2.92h-39.88v-5.22h33.83c0-3.28-.66-6.23-1.98-8.86-1.32-2.63-3.15-4.7-5.48-6.23-2.34-1.53-5.04-2.29-8.09-2.29-3.23,0-6.08.86-8.54,2.59-2.46,1.73-4.38,4.01-5.77,6.85-1.38,2.84-2.09,5.94-2.11,9.31v3.12c0,4.05.7,7.59,2.11,10.62,1.41,3.02,3.41,5.37,6,7.03,2.59,1.66,5.66,2.49,9.21,2.49,2.41,0,4.54-.38,6.36-1.13,1.83-.75,3.37-1.77,4.62-3.04,1.25-1.27,2.2-2.68,2.84-4.2l5.62,1.83c-.78,2.15-2.04,4.13-3.81,5.95-1.76,1.82-3.95,3.27-6.58,4.37-2.63,1.1-5.65,1.65-9.06,1.65Z"></path>
                                <path class="cls-1tb" d="m412.07,44.96v-11.86h55.9v11.86h-20.84v56.2h-14.22v-56.2h-20.84Z"></path>
                                <path class="cls-1tb" d="m471.62,101.16v-51.05h13.73v8.91h.53c.93-3.17,2.49-5.57,4.69-7.2,2.19-1.63,4.72-2.44,7.58-2.44.71,0,1.47.04,2.29.13.82.09,1.54.21,2.16.37v12.56c-.66-.2-1.58-.38-2.76-.53-1.17-.15-2.25-.23-3.22-.23-2.08,0-3.94.45-5.57,1.35-1.63.9-2.91,2.14-3.86,3.74-.94,1.6-1.41,3.43-1.41,5.52v28.88h-14.16Z"></path>
                                <path class="cls-1tb" d="m523,102.12c-3.26,0-6.16-.57-8.71-1.71-2.55-1.14-4.56-2.84-6.03-5.08-1.47-2.25-2.21-5.06-2.21-8.42,0-2.84.52-5.22,1.56-7.15,1.04-1.93,2.46-3.48,4.25-4.65,1.79-1.17,3.84-2.06,6.13-2.66,2.29-.6,4.7-1.02,7.23-1.26,2.97-.31,5.36-.6,7.18-.88,1.82-.28,3.13-.69,3.95-1.25.82-.55,1.23-1.37,1.23-2.46v-.2c0-2.1-.66-3.73-1.98-4.89-1.32-1.15-3.19-1.73-5.6-1.73-2.55,0-4.58.56-6.08,1.68-1.51,1.12-2.5,2.52-2.99,4.2l-13.09-1.06c.66-3.1,1.97-5.79,3.92-8.06,1.95-2.27,4.47-4.02,7.56-5.25,3.09-1.23,6.67-1.84,10.75-1.84,2.84,0,5.55.33,8.16,1,2.6.66,4.92,1.7,6.95,3.09,2.03,1.4,3.63,3.19,4.8,5.37,1.17,2.18,1.76,4.79,1.76,7.83v34.43h-13.43v-7.08h-.4c-.82,1.6-1.92,3-3.29,4.2-1.37,1.21-3.02,2.15-4.95,2.82-1.93.68-4.15,1.01-6.68,1.01Zm4.05-9.77c2.08,0,3.92-.42,5.52-1.25s2.85-1.96,3.76-3.37c.91-1.42,1.36-3.02,1.36-4.82v-5.42c-.44.29-1.05.55-1.81.78s-1.62.44-2.58.63c-.95.19-1.91.35-2.86.5-.95.14-1.82.27-2.59.38-1.66.24-3.11.63-4.35,1.16-1.24.53-2.21,1.25-2.89,2.14-.69.9-1.03,2.01-1.03,3.34,0,1.93.7,3.4,2.11,4.4,1.41,1.01,3.2,1.51,5.37,1.51Z"></path>
                                <path class="cls-1tb" d="m580.03,50.11h10.17v10.63h-10.17v40.41h-14.12v-40.41h-7.21v-10.63h7.21v-3.69c0-3.7.72-6.77,2.18-9.21,1.45-2.44,3.44-4.26,5.97-5.48,2.53-1.22,5.4-1.83,8.61-1.83,2.17,0,4.15.17,5.97.5,1.8.33,3.15.63,4.04.9l-2.53,10.63c-.55-.18-1.23-.34-2.04-.5s-1.64-.23-2.48-.23c-2.08,0-3.53.48-4.35,1.45-.83.96-1.23,2.31-1.23,4.04v3.42Z"></path>
                                <path class="cls-1tb" d="m616.12,50.11h10.17v10.63h-10.17v40.41h-14.12v-40.41h-7.21v-10.63h7.21v-3.69c0-3.7.72-6.77,2.18-9.21,1.45-2.44,3.44-4.26,5.97-5.48,2.53-1.22,5.4-1.83,8.61-1.83,2.17,0,4.15.17,5.97.5,1.8.33,3.15.63,4.04.9l-2.53,10.63c-.55-.18-1.23-.34-2.04-.5-.81-.15-1.64-.23-2.48-.23-2.08,0-3.53.48-4.35,1.45-.83.96-1.23,2.31-1.23,4.04v3.42Z"></path>
                                <path class="cls-1tb" d="m642.05,43.53c-2.11,0-3.9-.7-5.4-2.09-1.5-1.4-2.24-3.08-2.24-5.05s.75-3.66,2.26-5.07c1.51-1.41,3.3-2.11,5.38-2.11s3.91.7,5.4,2.09c1.5,1.4,2.24,3.08,2.24,5.05s-.75,3.66-2.24,5.07c-1.5,1.41-3.3,2.11-5.4,2.11Zm-7.11,57.63v-51.05h14.16v51.05h-14.16Z"></path>
                                <path class="cls-1tb" d="m683.46,102.15c-5.23,0-9.72-1.11-13.48-3.34-3.76-2.23-6.64-5.32-8.64-9.29-2.01-3.97-3.01-8.53-3.01-13.69s1.01-9.82,3.04-13.78c2.03-3.95,4.91-7.05,8.66-9.27,3.74-2.23,8.2-3.34,13.36-3.34,4.45,0,8.35.81,11.7,2.43,3.35,1.62,5.99,3.89,7.94,6.81,1.95,2.92,3.02,6.36,3.22,10.3h-13.36c-.38-2.55-1.37-4.6-2.97-6.16-1.61-1.56-3.71-2.34-6.3-2.34-2.19,0-4.1.59-5.73,1.78-1.63,1.19-2.9,2.91-3.81,5.17s-1.36,5-1.36,8.21.45,6.03,1.35,8.31c.9,2.28,2.17,4.02,3.81,5.22,1.64,1.2,3.56,1.79,5.75,1.79,1.62,0,3.07-.33,4.37-1,1.3-.66,2.37-1.63,3.22-2.91.85-1.27,1.41-2.81,1.68-4.6h13.36c-.22,3.9-1.28,7.33-3.17,10.29-1.89,2.96-4.5,5.27-7.83,6.93-3.32,1.66-7.26,2.49-11.8,2.49Z"></path>
                                </g>
                                <g>
                                <path class="cls-3tb" d="m800,37.72c0,4.31-3.5,7.81-7.82,7.81s-7.81-3.5-7.81-7.81,3.5-7.82,7.81-7.82,7.82,3.5,7.82,7.82Z"></path>
                                <path class="cls-2tb" d="m736.11,85.52c-.09,0-.18,0-.27-.01-.09,0-.18-.02-.26-.03.18.01.35.05.53.05Z"></path>
                                <path class="cls-2tb" d="m784.37,50.18v32.89c0,1.31-1.05,2.37-2.35,2.43-.03,0-.07.02-.11.02h-.37l-18.68-41.36-3.1-6.86c-2.03-4.5-6.52-7.4-11.47-7.4s-9.42,2.9-11.46,7.4l-11.96,26.47-10.4,23.03c-3.04,6.73,1.88,14.36,9.28,14.36h66.01c5.65,0,10.25-4.58,10.25-10.24v-40.74h-15.63Zm-48.25,35.34c-.09,0-.18,0-.27-.01-.09,0-.18-.02-.26-.03-2.25-.33-3.65-2.75-2.67-4.91l12.93-28.64c.95-2.1,3.93-2.1,4.89,0l15.17,33.59h-29.79Z"></path>
                                </g>
                            </g>
                            </g>
                        </g>
                        </svg>
            </div>
            <div class="col-12 text-center mt20 mt-md50 lh130">
               <div class=" f-md-20 f-18 w600 white-clr lh130 pre-heading">
               YouTube has been dominated by Ai…
               </div>
            </div>
            <div class="col-12 f-md-42 f-28 w700 text-center white-clr lh140 mt-md40 mt20 p0">
                    World's First Google-Bard Powered AI App That 
                    <span class="red-brush-top"> *Legally* Hacks Into YouTube's 800 Million Videos… </span> 
                </div>
                <div class="col-12 mt-md20 mt20 text-center">
                    <div class="f-18 f-md-28 w600 lh140 white-clr ">
                        Then Redirect Their Views To <u>ANY Link or Offer</u> of Our Choice &amp; Sent Us…
                    </div>
                </div>
                <div class="col-12 mt-md20 mt20 text-center">
                    <div class="f-md-24 f-18 w500 yellow-clr lh140 post-heading">
                    Thousands Of Views In 3 Hours or Less… Making Us $346.34 Daily on Autopilot
                    </div>
                </div>
                <div class="col-12 mt-md30 mt20 text-center">
                <div class="col-12 f-md-24 f-18 w400 white-clr lh140">
                Without Creating Videos | Without Uploading | Without Paid Ads | Without Any Upfront Cost
                </div>
            </div>
            </div>

            <div class="row mt20 mt-md40 gx-md-5">
               <div class="col-md-7 col-12 mt-md80">
                  <div class="col-12">
                     <div class="f-18 f-md-19 w500 lh140 white-clr shape-head">
                     Click Play And Watch TubeTraffic Ai In Action Making Us Hundreds Of Dollars…
                     </div>
                     <div class=" mt20 ">
                        <!-- <img src="assets/images/product-image.webp" alt="Video" class="mx-auto d-block img-fluid"> -->
                        <div style="padding-bottom: 56.25%;position: relative;">
                        <iframe src="https://tubetrafficai.oppyo.com/video/embed/mvuk8vau96" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe></div>
                     </div>
                  </div>
               </div>
               <div class="col-md-5 col-12 mt20 mt-md0">
                  <div class="key-features-bg">
                     <ul class="list-head pl0 m0 f-16 f-md-18 lh160 w400 white-clr">
                     <li>Let AI Do the Research and Pull HOT Profitable Videos for You.</li>

                     <li>No Video Creation Whatsoever, Ai Does It All…</li>

                     <li>Built-In AI Traffic Generator Sends Thousands of Clicks on Any Link. 100% Free</li>

                     <li>Get Instant Access to 800 million+  Videos</li>

                     <li>Have 100% Control on Every Video Legally</li>

                    <li> One Clicks Ai Monetization with Our DFY High-Ticket Offers</li>

                     

                     <li>Works in Any Niche and Any Business</li>

                     <li>Create Self-growing Video Channels Without Creating Any Video</li>

                     <li>Capture Leads or Promote Affiliate Offers with Stunning DFY Templates.</li>

                    

                     <li>Every Video &amp; Page You Generate with TubeTrafficAI is 100% Mobile Optimized.</li>

                     <li>Build a Huge List &amp; Integrates with All Top Autoresponders</li>

                    <li> Instantly Tap Into 5.3 billion YouTube Buyers</li>

                    <li> No Ads or Promotions Required</li>

                     <li>No Complicated Setup - Get Up and Running In 30 Seconds</li>

                     <li>ZERO Upfront Cost</li>

                     <li>30 Days Money-Back Guarantee</li>
                     </ul>
                  </div>
               </div>
         </div>
         </div>
      </div>

	  <div class="gr-1">
      <div class="container-fluid p0">
         <picture>
            <source media="(min-width:768px)" srcset="assets/images/tubetrafficai.webp">
            <source media="(min-width:320px)" srcset="assets/images/tubetrafficai-mview.webp" style="width:100%" class="vidvee-mview">
            <img src="assets/images/tubetrafficai.webp" alt="Vidvee Steps" class="img-fluid">
         </picture>
      </div>
   </div>
      <!-------Exclusive Bonus----------->
<div class="exclusive-bonus">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <div class="white-clr f-24 f-md-36 lh140 w600">
                  Exclusive Bonus #3 : LinkPro
               </div>
               <div class="f-18 f-md-20 w500 mt20 white-clr">
                  10 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
               </div>
            </div>
         </div>
      </div>
   </div>

   <div class="linkpro-section">
         <div class="container">
            <div class="row">
               <div class="col-12 d-flex justify-content-center">
                  <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" style="max-height: 55px; width: 214px;" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1500 380" enable-background="new 0 0 1500 380" xml:space="preserve">
              <g>
                 <g id="Layer_1-2">
                    
                       <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="-689.5011" y1="10559.4189" x2="-790.6911" y2="10663.2998" gradientTransform="matrix(-0.7071 0.7071 0.7071 0.7071 -7815.8701 -6918.3799)">
                       <stop offset="0" style="stop-color:#3BB6FF"></stop>
                       <stop offset="1" style="stop-color:#006DE3"></stop>
                    </linearGradient>
                    <path fill="url(#SVGID_1_)" d="M137.2,100.2l53.2-8.5c13.1-28.5,42.8-45.7,74-42.9c-20.2-24.4-53.7-33.3-83.3-21.9
                       c-3.5,2.8-6.8,5.8-9.9,8.9C153.6,53.4,141.8,75.9,137.2,100.2z"></path>
                    
                       <linearGradient id="SVGID_2_" gradientUnits="userSpaceOnUse" x1="-526.0667" y1="10729.4053" x2="-433.6907" y2="10729.4053" gradientTransform="matrix(-0.7071 0.7071 0.7071 0.7071 -7815.8701 -6918.3799)">
                       <stop offset="0" style="stop-color:#EFA222"></stop>
                       <stop offset="1" style="stop-color:#F0592D"></stop>
                    </linearGradient>
                    <path fill="url(#SVGID_2_)" d="M167.9,371.5c11.4-4.5,22.1-10.7,31.7-18.4c-29.6,11.3-63.1,2.5-83.3-21.9
                       c-31.6-2.6-58-25.2-65.5-56c-2,27.5,5.4,55,21,77.7c5.8,8.6,12.8,16.4,20.6,23.2C117.4,382.5,143.8,380.8,167.9,371.5L167.9,371.5
                       z"></path>
                    
                       <linearGradient id="SVGID_3_" gradientUnits="userSpaceOnUse" x1="-789.4672" y1="10592.5967" x2="-794.1872" y2="10698.4463" gradientTransform="matrix(-0.7071 0.7071 0.7071 0.7071 -7815.8701 -6918.3799)">
                       <stop offset="0" style="stop-color:#3BB6FF"></stop>
                       <stop offset="1" style="stop-color:#006DE3"></stop>
                    </linearGradient>
                    <path fill="url(#SVGID_3_)" d="M181.1,27c29.6-11.4,63.2-2.6,83.4,21.9c31.6,2.6,57.9,25.1,65.5,55.9c0.9-12.2-0.1-24.6-3-36.5
                       c-6-25.1-19.6-47.7-39.1-64.6c-10.1-2.5-20.4-3.7-30.8-3.7C229.4,0.1,202.6,9.7,181.1,27z"></path>
                    
                       <linearGradient id="SVGID_4_" gradientUnits="userSpaceOnUse" x1="-498.1561" y1="10793.4258" x2="-595.8061" y2="10605.5264" gradientTransform="matrix(-0.7071 0.7071 0.7071 0.7071 -7815.8701 -6918.3799)">
                       <stop offset="0" style="stop-color:#EFA222"></stop>
                       <stop offset="1" style="stop-color:#F0592D"></stop>
                    </linearGradient>
                    <path fill="url(#SVGID_4_)" d="M75.1,370.4c5.6,2.4,11.5,4.3,17.4,5.8c-7.8-6.8-14.7-14.6-20.6-23.2c-15.6-22.8-23-50.2-21-77.7
                       c-6.2-25.2,1.2-51.8,19.5-70.1c9.8-9.9,22.3-16.8,35.9-19.8l76.6-11l58-8.3l7.1-1l-2.9-2.9l-23.6-23.6l-2.6-2.6l-14.1-14.1
                       l-31.9,4.7l-3.3,0.5l-9,1.4l-54.8,8.3l-11.5,1.7l-2,0.5C71,144.3,51.6,155.3,36,170.8c-47.9,47.8-48,125.3-0.2,173.2
                       C47.1,355.3,60.4,364.3,75.1,370.4L75.1,370.4z"></path>
                    
                       <linearGradient id="SVGID_5_" gradientUnits="userSpaceOnUse" x1="-608.9395" y1="10772.7686" x2="-482.3742" y2="10772.7686" gradientTransform="matrix(-0.7071 0.7071 0.7071 0.7071 -7815.8701 -6918.3799)">
                       <stop offset="0" style="stop-color:#EFA222"></stop>
                       <stop offset="1" style="stop-color:#F0592D"></stop>
                    </linearGradient>
                    <path fill="url(#SVGID_5_)" d="M174.9,310.3c-14.1,13.9-33.1,21.5-52.9,21.2c-1.9,0-3.8-0.1-5.6-0.3
                       c20.2,24.4,53.7,33.2,83.3,21.9c3.2-2.6,6.4-5.4,9.3-8.3l0.6-0.6c17.5-17.5,29.4-40,33.9-64.3l-53.2,8.5
                       C186.6,296.6,181.3,304,174.9,310.3z"></path>
                    
                       <linearGradient id="SVGID_6_" gradientUnits="userSpaceOnUse" x1="-810.9832" y1="10699.4775" x2="-631.0933" y2="10745.2881" gradientTransform="matrix(-0.7071 0.7071 0.7071 0.7071 -7815.8701 -6918.3799)">
                       <stop offset="0" style="stop-color:#3BB6FF"></stop>
                       <stop offset="1" style="stop-color:#006DE3"></stop>
                    </linearGradient>
                    <path fill="url(#SVGID_6_)" d="M208,253.7l3.2-0.5l9-1.4l54.8-8.3l11.4-1.7l2-0.5c21.6-5.5,41.3-16.9,57-32.7
                       c11.6-11.7,20.7-25.7,26.7-41c24.9-63-6-134.2-69-159.1c-5-2-10-3.6-15.2-4.9C307.3,20.5,321,43.2,327,68.3
                       c2.9,11.9,3.9,24.2,3,36.5c1.4,5.6,2.1,11.3,2.1,17c0.3,19.7-7.3,38.8-21.2,52.8c-0.1,0.1-0.3,0.3-0.4,0.4
                       c-9.8,9.9-22.3,16.8-35.8,19.8l-76.7,11l-58,8.3l-7.1,1l2.9,2.9l23.6,23.6l2.6,2.6l13.9,14.1L208,253.7z"></path>
                    <rect x="613.5" y="166.6" fill="#FFFFFF" width="44.4" height="159.8"></rect>
                    <path fill="#FFFFFF" d="M652.8,107.8c8.7,7.9,9.4,21.4,1.5,30.1c-0.5,0.5-1,1.1-1.5,1.5c-9.6,8.8-24.3,8.8-33.9,0
                       c-8.7-7.9-9.4-21.4-1.5-30.1c0.5-0.6,1-1.1,1.6-1.6C628.5,99,643.2,99,652.8,107.8z"></path>
                    <path fill="#FFFFFF" d="M834,192.6c4.6,9,6.9,19.7,6.9,32.1v101.7h-44.4v-93.8c0.1-9.8-2.4-17.4-7.5-22.9s-12.1-8.3-21-8.3
                       c-5.5-0.1-11,1.2-15.8,3.9c-4.6,2.6-8.3,6.5-10.6,11.2c-2.5,4.9-3.8,10.8-3.9,17.6v92.4h-44.4V166.6h42.3v28.2h1.9
                       c3.4-9.1,9.6-16.9,17.8-22.1c8.3-5.4,18.4-8.2,30.3-8.2c11.1,0,20.8,2.4,29.1,7.3C823,176.7,829.7,183.9,834,192.6z"></path>
                    <polygon fill="#FFFFFF" points="493,289.3 584.5,289.3 584.5,326.4 447.9,326.4 447.9,113.4 493,113.4 		"></polygon>
                    <polygon fill="#FFFFFF" points="1027.8,326.4 975.8,326.4 932.2,262 920.2,275.7 920.2,326.4 875.8,326.4 875.8,113.4 
                       920.2,113.4 920.2,227.3 922.6,227.3 973.8,166.6 1024.8,166.6 965.4,235.8 		"></polygon>
                    <path fill="#FFFFFF" d="M1198.9,148.1c-6-10.9-15.1-19.7-26.1-25.5c-11.4-6.1-25.2-9.2-41.4-9.2h-84.2v213h45.1v-69h37.9
                       c16.4,0,30.4-3,41.9-9c11.1-5.6,20.4-14.5,26.5-25.3c6.1-10.8,9.2-23.3,9.2-37.4S1204.9,158.9,1198.9,148.1z M1157.3,204.1
                       c-2.8,5.4-7.3,9.8-12.7,12.6c-5.7,3-12.8,4.5-21.5,4.5h-30.6v-71h30.4c8.8,0,16,1.5,21.7,4.4c5.4,2.7,9.9,7,12.8,12.3
                       c2.9,5.7,4.4,12.1,4.2,18.6C1161.6,192,1160.2,198.4,1157.3,204.1z"></path>
                    <path fill="#FFFFFF" d="M1333.6,165.9v39.3c-2.8-0.8-5.7-1.3-8.6-1.7c-3.4-0.5-6.7-0.7-10.1-0.7c-6.1-0.1-12.1,1.3-17.4,4.2
                       c-5,2.7-9.2,6.8-12.1,11.7c-3,5.2-4.5,11.2-4.4,17.3v90.4h-44.4V166.6h43v27.9h1.7c2.9-9.9,7.8-17.4,14.7-22.5
                       c6.9-5.1,15.2-7.8,23.8-7.6c2.4,0,4.8,0.2,7.2,0.4C1329.1,165,1331.4,165.4,1333.6,165.9z"></path>
                    <path fill="#FFFFFF" d="M1421.2,329.5c-16.2,0-30.2-3.5-41.9-10.4c-11.7-6.8-21.2-16.8-27.3-28.9c-6.4-12.4-9.6-26.8-9.6-43.1
                       c0-16.5,3.2-31,9.6-43.3c6.1-12.1,15.6-22.1,27.3-28.9c11.8-6.9,25.7-10.4,41.9-10.4s30.2,3.4,41.9,10.4
                       c11.7,6.8,21.1,16.8,27.2,28.9c6.4,12.4,9.6,26.8,9.6,43.3c0,16.4-3.2,30.7-9.6,43.1c-6.1,12.1-15.6,22.1-27.2,28.9
                       C1451.4,326.1,1437.4,329.5,1421.2,329.5z M1421.4,295.2c7.3,0,13.5-2.1,18.4-6.3c5-4.2,8.7-9.9,11.2-17.2
                       c5.1-16.2,5.1-33.5,0-49.7c-2.5-7.3-6.3-13-11.2-17.3c-4.9-4.2-11.1-6.3-18.4-6.4c-7.4,0-13.7,2.1-18.7,6.4s-8.8,10-11.3,17.3
                       c-5.1,16.2-5.1,33.5,0,49.7c2.5,7.3,6.3,13,11.3,17.2S1414,295.2,1421.4,295.2z"></path>
                 </g>
              </g>
              </svg>
               </div>
            </div>
            <div class="row mt20 mt-md50">
               <div class="col-12 text-center">
                  <div class="f-20 f-md-24 w700 black-clr lh140 linkpro-preheadline">
                    <span class="orange-gradient"> Are you sick &amp; tired of losing clicks </span> with long, broken, or <br class="d-none d-md-block"> suspicious links in your emails, website, etc.?
                     <!-- First to JVZoo & <span class="under">MUST HAVE Solution</span> for All Marketers…   -->
                  </div>
               </div>


               <div class="col-12 mt20 mt-md80 relative">
                  <div class="linkpro-gametext d-none d-md-block">
                     Next-Gen Technology
                  </div>
                  <div class="linkpro-heading">
                      <div class=" f-md-45 f-28 w700 text-center white-clr lh140 ">
                           <span class="orange-gradient"> Convert ANY Long &amp; Ugly Marketing URL into Profit-Pulling SMART Link </span> (Clean, Trackable, &amp; QR Code Ready) <u>in Just 30 Seconds</u>... 
                      </div>
                    
                  </div>
               </div>

               <div class="col-12 mt20 mt-md50 text-center">
                  <div class="f-20 f-md-28 w500 text-center lh140 white-clr text-capitalize">               
                     Introducing The Industry-First, 5-In-One Builder – <span class="orange-gradient w700">QR Code, Link Cloaker, Shortener, Checkout Links &amp; Bio-Pages Creator - </span> All Rolled into One Easy to Use App!
                  </div>
               </div>
            </div>

            <div class="row mt20 mt-md70 align-items-center">
               <div class="col-md-8 col-12 mx-auto">
                  <div class="video-box">
                       <!--<video width="100%" height="auto" controls autoplay muted="muted">-->
                       <!--       <source src="assets/images/coming-soon.mp4" type="video/mp4">-->
                       <!--    </video> -->
                      <div style="padding-bottom: 56.25%;position: relative;">
                         <iframe src="https://linkpro.dotcompal.com/video/embed/x8ipwmt2vf" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                  box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe> No         Matter       What You Do Online for Living
                                       </div>
                  </div>
               </div>
               <div class="col-md-4 col-12 mx-auto">
                  <div class="key-features-bg">
                     <ul class="linkpro-list pl0 m0 f-16 f-md-18 lh150 w400 white-clr">
                        <li><span class="orange-gradient w700">	Boost Your Click Through Rates</span> By Cloaking Long &amp; Suspicious Links</li>
                        <li><span class="orange-gradient w700">	Create QR Codes</span> For Touch-Less Payments &amp; Branding For Local Businesses</li>
                        <li><span class="orange-gradient w700">	Double Your Email Clicks &amp; Profits</span></li>
                        <li><span class="orange-gradient w700">	Boost Followership</span> Using Ready-To-Use Bio Pages.</li>
                        <li><span class="orange-gradient w700">	Fix All Broken Links on Any Website</span> In Just A Few Clicks </li>
                        <li><span class="orange-gradient w700">	Track Even A Single Click</span> So Never Lose Affiliate Commissions Again</li>
                        <li>	Sell High In-Demand Services With <span class="orange-gradient w700">Included Commercial License.</span></li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>

	  <div class="tr-all">
         <picture>
            <source media="(min-width:767px)" srcset="assets/images/linkpro.png" class="img-fluid d-block mx-auto" style="width:100%">
            <source media="(min-width:320px)" srcset="assets/images/linkpro-mview.webp" class="img-fluid d-block mx-auto">
            <img src="assets/images/linkpro.png" alt="Steps" class="img-fluid d-block mx-auto" style="width:100%">
         </picture>
      </div>
   <!-------Exclusive Bonus----------->


 <!-------Exclusive Bonus----------->
 <div class="exclusive-bonus">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <div class="white-clr f-24 f-md-36 lh140 w600">
                  Exclusive Bonus #4 : MailGPT
               </div>
               <div class="f-18 f-md-20 w500 mt20 white-clr">
                  10 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-------Exclusive Bonus----------->

   <div class="header-section-mailgpt">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                  <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 620.04 134.95" style="max-height:60px;"><defs><style>.cls-1{fill:none;}.cls-2{clip-path:url(#clip-path);}.cls-3{clip-path:url(#clip-path-2);}.cls-4{clip-path:url(#clip-path-3);}.cls-5{clip-path:url(#clip-path-4);}.cls-6{fill:#fff;}</style><clipPath id="clip-path"><path class="cls-1" d="M40.14,36.57l13,12.24v77.25a6.51,6.51,0,0,1-6.51,6.52h0a6.52,6.52,0,0,1-6.52-6.52Z"></path></clipPath><clipPath id="clip-path-2"><path class="cls-1" d="M170.36,36.57l-13,12.24v77.25a6.52,6.52,0,0,0,6.52,6.52h0a6.51,6.51,0,0,0,6.51-6.52Z"></path></clipPath><clipPath id="clip-path-3"><path class="cls-1" d="M169,15,108,72.83a4,4,0,0,1-5.49,0L41.59,15A6.51,6.51,0,0,1,46.07,3.73h0a6.56,6.56,0,0,1,4.49,1.78l50.57,48a6,6,0,0,0,8.33,0L160,5.52a6.5,6.5,0,0,1,4.48-1.79h0A6.51,6.51,0,0,1,169,15Z"></path></clipPath><clipPath id="clip-path-4"><path class="cls-1" d="M101.27,81.56,35,19A9.6,9.6,0,0,0,18.8,26v99.87a6.52,6.52,0,0,0,6.51,6.52h0a6.52,6.52,0,0,0,6.52-6.52V34L95.92,94.44l5.35,5a6.06,6.06,0,0,0,8.31,0l5.34-5L179,34v91.91a6.52,6.52,0,0,0,6.52,6.52h0a6.52,6.52,0,0,0,6.51-6.52V26a9.6,9.6,0,0,0-16.19-7L109.58,81.56A6.06,6.06,0,0,1,101.27,81.56Z"></path></clipPath></defs><title>MailGPT White Logo</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><g class="cls-2"><image width="90" height="99" transform="translate(-0.75 36.16)" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFoAAABjCAYAAAAb3RdnAAAACXBIWXMAAAsSAAALEgHS3X78AAAS0klEQVR4Xu2cTahtyVXHf2ufeyOKiNLddhJbE6XVNtF8dqskDsSRM0eRiIKoiIgfAyeKIgTEiYIDJQMRBBUxmpEzBw4caBvtl26bjtEmrbZGOunOaxQRUfuevRysj1pVZ597zv0495wHFrx7a9de9bH/9a//WlV7vyuqyimnp773LxTgmT/6oOyyPeUkpwz0kw5yJOHBBXzaZXDUJOUfoMCTH/pzfWqYgAchnSyjn/ywg1mH1+UVEXlgGH6yjNaArzC6z0syfKx7iukkGf3+73tag76yldE1f/rsPttlcJQk/kNBC9IdihXowu5TBfzkGP3+7396izY/2Aw/KY1+3w88vUxfyR+oFExH/c7y09PvkwIaFoCseXFkpTjLajfyV+RkwsGTkY73/uBfmhyHTOgumdDML2r3+Fhq83QsOTkdoH/oE4oWgBXQfXS5qI2yCHB/fRz9Pgmg3/Mjn9AGsDrIztR5B3MzPwA+2i0Afu/j33FnYJ+ERuskMIFOltdJYOU3RdLpLepy5lvBVv0eHOZd6vfRGf3uH/srDQaj2tg4MhyQmevJyWgzXt+BnBwV6Hf9+F9v6jIgRT5MMtq1zF75KoBrIfORAD+qdOgETAKTuGzgEiEpJ0x+7RISNiEpl8bVJRzMNEI4yIlymPj7aIz+lp98xmgaTE0Gqz17ve7YbmXMNDtvZyu7q52nnRHKLbP7aEB/8089owjIPIANdHICPeCArLWBotcHfC/9RhFuDvhRgH7nz9yzcK6CPI/gag9i2NHqgNtH3bDdBfgS2KPNeH1Dhh9Fo3XlmrxqWqyTtPLQ10lMg0N7U6/9OjR6anqdoV7ci07zXuStYCMU3HZ9Q/2+c0Z/088+q53uqjJdaEMkmLqN4dVuDAdjhUDR4F3sbheHZPedA/3Ezz/bdoFzD7jFyQXkuisswLU6druGg7WO3Wv1LgU8r7W3Ge2uCfidAv2Nv/hcp80ZQSgN5LwewFN6vV4CfG2ZbLfY763ftLq7JyXyuwG/M6C/4SN/o8wFyABjbvmR3R07twE+hoMUhkd/tY0CdrYVaQAvyrbajNeXAH5nztCcHebwwulNAisv9zOO2MSkc4yzjqnUrY6vOsy64REBb5faxoRfizvXMsguL1m21Wa8FkFVWTo/uRNGP/7Lz3encyETlcVLDE8nmSwu113eO9rmMH3bXqWqXbthrRd183e7cV05uTugRazzNQPAA2BbpYWi7aXORnTi7WfeijvNp/UDOwCPdvK3tjpLNht5A/zgQH/dr76gAU7d0cm6d2ZL4FXAR7C3Ak5rM4Gbe1soE0ntp9XJtiJp/V3qLdkMeeEOPjfQM/Gdm+mmOEsB++0gqu8uZFbTVQdSFUTFMFEFkYwuVIqd5+NFgQiEuOrK+q8TZO15fdRMvczXuts4mB2i4mOxKsnuaGPIm80BGf32X/9brbFyi4+haXVj+k45mWv+khUx1ifsiqRE/bzn9aj9+kVtJ+zzd7uxVb/1gEC/7aOf1g3HVxgcoVd3f9DvXstpIF9UkHZL0KKcLNhD6Qc6u65etBdJ88ci2Pf+8INyMOnQFaDiS0iRWVDx3woiis4WmsnaHEYu3wTcbctSFwU9x8tHe5oE6Q45EbefrdNQBre0CZCGp6iAaIdvTmBoi1q7Zm9F9z5mUcdBgH7st/5OZYpRagN8htAuA9vYyUSSSBB0FROyAF4CruhsNViBzKSO7wIcceAcTPAJb4NAfVLxJWET209HTkJcLAAe6SBA68ofLNCMhyx5UWO4BfnFSa7LxCjGVmfc+FMm60MVaztWxg7AZXZm5jitD+Y2bo9GSYcpzTmmwyyAS7v0sQuf/IMPxMzcPtBf9TsvKitB12rsnEyHdWoslMnYyiy2m1MHF4xt6xIVVMD1EjmJSV2HDAhKlQRBApgYiwMTkqKTTQIxWVFffMK81SWGJ9h2WS4s3TrQMamcCVwUsBV0FmTyByx5UUFnRSZ7INPtHswR8BauaWPZWm1L7mU1LNyUoIHdeBcraWcsPjWSFsOEpZUNTiG1+ZO/39gMtwz0mz/2GZXzAAA4MxymCxtGAMmMg+xsnNVZDslaMLsZGmOqfhfbAE8i75QbVsEGyLogJ2AvjOeezZtgQ0Bti6mBvZRuLbx79OMvlXDOGBQPEBuUOLtYDMEcuMjLG9rbdXnvYyMcdDuaXe5GO7uFOmU8GXLS22W90n6uKNr1s7/XsxlukdF9OEfu+sDKZFbmM+t/uiAfUEWabdFbPZdkpijOzvinpu+DnDR50LwVUiCzaTaQi79JQK/fVtkttJKhrCRvH5EEWIHnfncTZLgloB/5439QJvHtsw8IX7IOspat83zm+QBFsHg2mSVe1pa96XYD07CIGwyANzlBscn08eAT2U1K5n3yYwy+OnQlyBoi9mbUfmiAb0m3AnSGcxPOzj6vE0xvaHp7AD2zwZp+g0yaDxB6KaNjA1gbE3UiwdCZRf3O0OxSJ7mHw1RFVwyrwn4qII72c7/97YtshlsA+qE/+SdjcwwM2kPSHmw+p7ExdVCZ3XnuIye4DG1IRSz1ueW7DU/G1jYGwm84kD3gCyCHnSiytj7apJUxXJJuDLSuxHtrg0Gl7QIDsAoeYstwLR7DbgLetVlAlIsiTwmSPWzIzYacjEs+cFGTCWurBy5CvE6/EZjczpFVTPqe/81vuxTqGwH9FX/6stJpMjhybYanNvAl/WZtTx4PE/rdb0wc5NkmpANmTZOrmNBhchJw3ImJ+451AGuTk/kZC/Fq/5HP9mk7XHana4d3X/5n/6zRYXhdWYck2KBsMC0fy1ZioGr22w7z26usVmcj5JvpXiJEWbXtj2WH9lxGFsPHHOdCv6X+Cx/91kvZDDdgdDjAcEaxXFVpEcWMU6awrDIumS45+DzM0V5OtNRp/Uq+kO12jPRMzDGI2eQJ4lqtOFad95HOFc2Fqd4NWkCY4IXf2A0yXBPoL3v6s2pLS/O8Ih5MZrVJcJBte10eXCDeuIjC/CacUdaeVMAc0BHw6ng7aVljS18gDog2AC8O0ybdnSbAhY9LvJ0gixZnGW142/umawFtbHZmxKBVzFGJDzRYtNay9XaNrIc645mHKKwlJaA6o/nMnVScspXVEk4ytZbmJA0o2kQHQTwfEYpmfO/tVL/i4xEkz24+9WtPyRaINtKVgf7Se/+q5rWtQ7QM5Mwe2kAjI49YpiLiMlMAEpOA6pTA69IzMpyRTjC5PEUIZ6x3cBRfGRQgaYDXyZ0DOB+7YLG699WFmZXpuonNZenKQHdbVBXswN07F9LZGXu0sK8AHhoZIFLaU1CbAVPJCnLJz6710xpfGW21WOPSzra9/aXln6siHaagZ8CFVesOv9TqCfDpX3nf3myGKwL9Jc+/4uGcswKc1T6QWRdP4Wyn58Ct+okaJQBVmATxnWSycJSAEfDiMBug5Na5O9RXD+/m7Q4zn9P7TTkRB/yKae/w7os/9Tm1GScf0jonw7Pw2pkvodxSCNd/2+H5MTSLeyV0rLbdOMYwr5Z3Y7ykDVUvazay1uIU4cVfeu+V2AxXYbSAvXnwJVp/VudWl5mzLZ0kiklIsAg0AXIW0lhb20TEo4JBAnzZp34HMMH6tfc5VT8AcW6RoV3dCVb9xlahYP2++JH3XBlk2BPoL/r7z6u9n8Pes9EeRucKOSYPnsv8hpPsJSAncJvDVLWyM/EQjg7wcKhpWwEXocpOOslRrsYdZbGRmXSS1017AR3b6HyQCninjaPdwKKinXEKB/5MiJ1pR/y9xWHGcWs96wiHtxihaB+hIOKhYPErCCqNKsYGH5gX6SS89AvvuhabYQ+g3/TSqxoRAJAPqkZjYyvLgC/KSQAuCuth8lQg2sVsk9XiE3eh/ZHnIAGoabnDB8Ach/8+OXpu400mz9oYXfMxlii/QdoJtHbe10EJNgd4vqxZOgwagOxYtKJ4/pggSE1W9RDNy1ThXEpbmru5WPbjJmpJvwWxEC7iZRoRBGnPlLtf+Mefuz6bYQfQ5y+/pogzxssUIQ66bY6b86j6vS0cUyUZY04SP/BrUtADjnU0yIVNmKBnwrRWNmP5kKllwO1FBF2drr/K5okbp63h3dm/vKYGDo0tYeplGRbhdrG81Ma5/GK01Mv27Xox3NPW9vjitwsfs65uhGeN7Qv9e9iXL45rGOjtv/zT77wRm+ESRm+EQ5VNAELPWiDetIA/x0oMcH9dBepEceZjUYl4qR1GBThFgmIZF3YDuTkKprJWNs5OAmCk/74kRuTvOudzYboIGdH80Ofln3jHjUGGLUCvXvmCYqu7geAPuQl4XJPhVMCK5/SsAB511SZqQ7+Fzc/A8nTQ7Jb0W9YeSeBOU2lyQvS5XU5QmM8o4R8+2NtJ2zVa7J+uQeLB6j167QbcSfpDKLkJAK++p8NEGOJlttg1wO0j97DTDOmowEZ783bAdfLXaSif/dHbYTMsAD197r4aK61zVj7W8aUkNDYH4OIPik/DSogvjYi52sNhssYdFW35q133uzho0UsZmwpzxNuzdjJop3U0wKEdHEW7Z3KjzclS2mS0kxLIJSVg/z1htgfbABxBpV5HXfXPDsTLG0Cp3wvMVaEASznyxAsaO+23EJ835DGp+hguyFUQR6LdufIcE2b2Jj3wyg8/cWtshgHo6dX7hkSiZXkFUHcy+IO7Fscg42cvJ5Uxdm3V9gM8GJ6OTkmGt3wDEhFSTuIQ69zAtoGoTXoQJQ//477/voVwbkwZ3smr98NtEIzeyFuFdrF2eINZNCZCgIE5SWhtaWnH7QVcv6Os5TNUyzOQsay0OdSZLjQ1GKXLL4VyKHz+w19/q2yGymjfmKCERLdZjjyk0wFN/aZ8n6GQGxCz0jxUqoAkYwHEGTkeWCW7vT1R8xXESip6qzDuMEXFIol6L3aSsz9bTJ4v3Vc/9PitgwwOtHzhdYdRvL/LAY+HTPRX5JtjYllGlS0OMx2PT0oFfFNOMJDSSdbwjd1yEhudWdBzse+2xSc7HsOJcqhkjB7nUHcALjEmY4GgdsIHJidFv1vzkiUC3ScG0aUBrtl+A1whIgmvo2oT0QAP8PHKkhOEt20OU4tue6zu/b72PYdhM8CZ3A82lxSzjCTLpCunmxytgG+TEyCiBUXdIQn5wXdpSxRcn0hJ6c6wPZ9OconhuN0yw/MEz8fY2HSYJKpKB3aFXUsmwL7ULvApBeXcIYrTyUVk4HWaI/X70VbYE+x2Owc826mObrAZ8+kA3QG//t1fe1CkJwB9+KHWSe1OSkbI1bho59dmUwomLxPJ8woVyL+ptLINR/x9pXzLPrlNhGzezrwS21BMUP/cRNpMQv939CwutyG1vIrX9zEcOmV4lwU72e0XusDwvim3KYWV3XE/bgfj5rjej+GsY7sdNrqxiiJvLC52fu/fvuttB0d6A+i8cR3At9mljRdeIiewEBtTrin14rrGw4rLgduMclKPVmfl37/z8CDDZcekLidy//XmrRS6PAKigc9mdBJgS2DoYZzvMEeHac3Zsm/hnzighqq6oQ3OHdzkwebanawI8X9eNr6E8vbyldUdpa2M7oyuw+56f6gzshsKw4H80DHLo+7+ciIYsze+24iYWuE/PvDVd4b0zneGwBXYDRl7e9E+7AbahgfIv68Rbcdyj82POnjBcC0rQ8kdplVRui9Lxex7khw+7cXoMV2J4WPzC9cjw6te5/1Bh61cM592g37L0v9tVOU/n3zsztgMezJ6TFdiuBSwxuR1DNNBv2cQo59dT+JHmurshTz79j5Svwu74+2OqriUKLla7jBdi9E1bWV3d62Zl9FuyI/shmBxMNNvh16HubIZoahfxDXG8P9691vvHOkbAx3pSnKyUc5GnUXA46XsLrC9rAshFRD473e85c5BhmtKx1K6kpw4Eh3gQx0j4+Aw8wYGcvnAHMFDOEnArUvpAT9SujVGj2mnpGibjw2bBfuR4cnksgHpwj9lw1n+zxNvPgqb4SAvbSxtPT+JaxE0/0VZvd/bm//ywjw/gfiTmUTezznwcxL1M5DDPel+6WCMrmknu8OujmUbwxfYDfSvpSq73fR/H3/0aGyGW9Toy9JW/YZOm+PNeZJ3wWabfhOhoNerH7YfU5sj3Qmjx7QzQrlMv8frkeFK9zJYFN54+1celc1wJKBhD7CdidcFPM5LLr7m+CDDEYGOdKl+A7lR6cq25KFtUgBmWD/2yP8DXdNuhzkAvgXsAFoR5rc+fBIgw9GDnpb04YckQ8II1yKFd5SCabUp+XxtdmLpZBhd05X1e8FmfsvpsBlOiNE1bWV3QifEi1ZdsjkpiC2dJKPHtPHtiQ6/B4bPj54Wm+EBATrSIuAD2HqCIMOJSse21J2fQJOJdJYniTHwgDG6piWHqY88dLJI38lZxyHS4vnJCaf/A7yQ0Ala9tJfAAAAAElFTkSuQmCC"></image></g><g class="cls-3"><image width="90" height="99" transform="translate(121.25 36.16)" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFoAAABjCAYAAAAb3RdnAAAACXBIWXMAAAsSAAALEgHS3X78AAAO2klEQVR4XtWbzet1VRXHv2uf+1MQokDNxExD1MpAyMd8Cfoznhw1bRA0cBRUA2kSURGRVFhkb1aOmjVrEDyE+LxqKiYSRophDaJBFHl3g73X2mutvc85+/7u2/ktuM+5Z5919stnf/d3n3t/96EYI85yPHj+QgSA55/9FM3lHjPorIJ+8PyFqucXFwx7NZewtGAFnzV5nBlF9wJeqqrPhKJbNlHFIvGWWLSie1XsIV/89fJUvUhFnxbwkmNRit4l4KWpOswlHCrYh3cBGQSce+zCbFWHjKNbxy5V3JVzpDiadXQDBvoAqpyozi/9chkWcnBFdwPuxePyopxTfx0HiIMquut5GOgD1AuYgEu/eLSnxr3GQRS9bxWXcnUhv51t80CxV0XvG3BSca1gez3F5Z8fV9V7Ad0NGOiD7AFLWRuy3gxLOeHyzx7paW0vsXPr6PbhnmhgmVLxGOCuydxz7EzR3SruHXTTJvIFKm/r61xOJocn4cpPjqPqrRXdDRjogzwKWF0cAyzX2pCPGadW9EaAgfmBTgEee5qoVKzecJ5WPxEigKs/fniuNzuPUyl6Ix+eG5IHbMpo3ibk2gRgAJFyXXP92VNspOh9qrgLsDufBKzLgs0BAVd/eFhVdyl6I8A93fcqNuc15C7AOocAEDVUncqPEZOKPhpgfb2h0FEVM2Cdm+tIZWTKrv3goZ5e7ySaoDcCDMxD9gBNmQLcUqfP1XnuvpaKQdmffVk+vvD9w8CurGMjyD1dVDmjgPPpqQGjsdkRENEqA8C5BwxR9E4Bu+uTgHNR15OEem+P2S4CZgDnf8iWvfjkJ+dGtHWsjg7Y5Lh8dRi1iQqmva8JGbq+uUHtJgJwBMgakCnLF5QNxPySe4gQm5DJQjb3ljpjSC8AiIEQA/DxLzzfhWCbWHW1MAV5U8BwNkHmhjpHjumNWES+1lKxfwqR3HxuJg4om+UeY/qv4Kozc9cilPK0Kl1urMrInEs96hI/skWC9DipVbXD98t7EsgxtCFHovRhhoD7Hr/YpbnTBsUYce68+9M8YT4YnHqfANc5VZ4HzDkjgOHKW5/0mj6sle8m3j+l8PnL33igZ/QbRwDcDwPHmiH3wgaQzb2qAhlknWN8OPtq8lQFiPS9hMqHVV2Sz3WEsirkfI+/cilVq/FX4cq7bYLzJMYg8csBJiQA3IaqvwWYIUseygcYDTjdT4ABnur66Bcv78VCBPToT6hUaQ9gzhE/ZRCh5E75sK6v+DDMa8wmBHyGGgcgDoS4IgUYRsGpn3zP/lQ9vxmiDzBMDtCyCJMjr4aKGYaDPG0TBN7s+NxYBvGkkZxLP6XedP6RL13ZuaoNaFE1NwpYlSkFSR6cQgH4ZQ5oSPxqA7YQynk34JCAFo/W9+rzUsbnMeR6sPuoFU3lbaVQda09ESqPr5uBYRqw+ChV3tn24RYolUe1ijVkOVebbAzJau554upOeTe/vTv32IVYFEpNwIBXcbloJ4iP7Uc1UJ4sfmSDqpfrzOcVYF8PkUyQXlWVTfB9wU54VBPCk/HaV+4vHdsimh598Ve8MZZBcmcApQwuz35qFAN9bEAmpTj1XGyXdR64sglZ3oS8oVHa8Ia8+TUVTAI1EgDO13XIBkllM93hxjhelbaAzKECzIOCUol5TdgECpAKSgOwUR8/QZjHNc5T5wHytKPPeQwxWBVH1S7n3vW1F3ZiIaOgLz3zKAlgKMBABp87xdfMa3qjqz+VwQCWwYLr8hOpPFXq0+elPukfq5TryPXLIyCviIHVTOAPSbuIycVhH+sggwScTWgAo4CpAMlKlYEwYLDKIDArFROVe3V9uj8tm+D78/l6IMRVkIks/UdRfl4xH/7mi1urehL0Zf65a+4EQBYg9LFPwQUkSn0o4EdtQqmY22t+6OB+OMACmSdXFIui9iHfp67JU9CWMQkagAHMg7JA2yr2S5rLCmBVZwbsNzoBlyGYjU8pH837dF9K3dpiSv+5X3DP0KkwBuDOb/9xK1V3/a7jE5/9Q3nckyNNPK5RVZ4815aXOu1gGU65lwQaUK+I5uOa5GlVUxuwbo/PkRWu7YQIb3z+Y9zLjWJe0QAu/9T9MFAPzh3N3+tkECqPYNQoj1Fyjcyg46DeU7mPAbRsIuZ6zPcr3E5QbQYuJ7VvsH9zLoqdDDh1dIEGoMBZEHpg/IHBlPGA2fc0YBqpl+vIoAxgbRMacEiqFP/lTW9F+UulecBF/SXX3EOE2596Zd4CGtEN+srTj5B4MWDVSlSXqeU+ClgmhQdY7o+5TlahmShWJul7qNQ3FIBlEl2u638MfF++BqjHvqzy/DpNdIMGIAMpj1MEv9kJZKhzoBpsAazuD3pwyEuVChCgrUwqOfyJz0zshIoFXkhtrgfC+oQQT2yunujbnn51Y1VvBPrqjx4mEKpPdUUZkHKjJh6sAG0AZoCmHq1iqm1CWY1MkkBpA5Z7NWC5l4BB9UWVi1efUtUbgQZglqhRsQczBlivgDxo8cbQqGcMsEAsdQAobTrAZjIHbgdlFei6cq5shhp8Pt76zJ82UnXX452P+z/3XNR/5RCYAMQi+DrlzqtzQE0OkO/PA9d5zm7GvLWyJUznljIGWvJlcnUfgmpfCQkEvP2Zu3OL07GxooH8K0zXCQD1gLlz0OdKgZQhg8sB2ejGlKnujwMclHRt0odzOX8YAjgHKadlP7pOHvegBNURpwINwEIm3SmIUsSH1aDBqmC/yyBS3ghgPUEMa0igeHKaNjEGeJUf+/Sml+sy6hfgKH0Y0v0xAPEk4ObfvN5lCacG/cL38g8D/awyENLnMApmsKJEhqIBq6XMgOpNzwIuFsBwFLycux5KvvbcSsXqfQyQ53Ep4yejlQfQjlODBpAH0gYkUFnFMmCtnHJurIbByARZtQEKsNyX7rVfddp+JXXz5FFRa3D5anOUDzxsd0hwdRs3/vbPs6reCvSL332QWoDKORXghOJrDNjDcEoDqybkwfGLrYZKrrcJ7fXI93M7MtmtPghkbicd1yf5dX0oeatynIutQAMNQOTPkYCH3Jk5wA0FJ0XlugkltwG4qdDctix3s+m5PrAguM6VujaUI7fF7bzvd29MqvpUj3c+7nv8YjQ+zBMsXgmBV60AQn6kU/fq/OByfTsjbbCNWNuqc4z1ZAXb8zxRlCdR2ivtluuEf376Q4RGbK1oAHjpW+eoAhV4oDCDaw0QSk06n33QQ7ZqRnuVqG/fROUMx62i5MPB5MvGx+odyL1s2ZotbSS6/vtbTxhVAZV6moDhJsflS72c5+sM7fpFiVWdVFQoeSN9ANJkTSmYqAgk9+U9z/01/uuhD+YaSuxE0QDwytcfIOnk0FCY9uFAonivSL3hlc2oqNvUSY36xT8b+byxBlW3Uvx6FeQLpfVJUbidID8x+Tr3dyR2BhooHS4eaDvW3OhkOVsl6onRfq2XvfZvqR8MGfUkKCDedtar0l6ZFBp9z0JZn4QkDABxRXj3OsIN196qNr6dbIY67v3ylThqFX4Zy/usREI6IQU41Pn+WVvUqeszbWeoesJ0fdy2XiXShsoLrm/s1SKWcvz3fbcSVOzMozm4ExquXWo1EPO98wRgDVDAVKuIVG6ZBHOPrm+gUqdbPb6cYccVqbphVh/79vWvvh3/c+8HiLnsXNEAcM8TV+vHvXzeel+pMvhcB5jfDyrHWVZrE6tWRS5f85+6+Bor10/AamQV6HZyPQz/v3fdQsCeQAPA3V+9FiuoWiW8cVRgVL66Vh7/0v3rrCYNhN+LTQR1Pejrtj6fV02UAlwgK7DqKHXk8//d8X4C9mAdHGYZEkYUg1HATRVr2Cj1jAEem+BeizDK1fUB8g2eV7BX+fDmO/Hd226mvSkaQPqBoAJmOz6mYFLqc9fkqUTVNwVYAQOpVbCpTQQqE+SswuwRrGrZNNNY1rfcRHtTNOAVx6DGAKfzSlmcN9SADRhdZ6hzfNveJuTxzwEWcTTVS2IT0o6DHIlAf/9H3KuiAaQfCMoAe1RMCjDS4PTyBaZVHFwdyqbiSajalIlctfuzHkip2IK0Y+LJ0HXn40037lfRAOyyYlhjUPzgiZUIAeD/fFVN0gy8ph8Pjf7wKhhTMTXG1lL5TTcSgP09dei48zsvxVEwatBGxSpfNh6v4Il6ypMFlYlS6m75cGsVrK8L9npLxbovDjDHQUADwB1PvhyrQWnAWk0CIPXVbDjq/jGlaiVOKj3U98tHcfJH1QetYO3JVAPm2L915NCbWWUlGor/pNZrE1zP0LjGE+oBcxu8CtQHF+u7DcDcxgxgjoMpGgBuf+qVWA1edbx8pNU59n2P3axPQplQzvc+7NQqf0khrVLXV+5Dp4p1HEzRgBpsA5B/hBpXsIOQIVVguZ0Jm+A6artQ/dhCxToOqmgg/UCwpc7KV2VDsoMXiKFA7gKswDZXk/y+o5HnlbwBYI6DKhpQyiWo5TwOuIJN7n4Nf0rBTqnWKnS921nEWBxc0UD6gWD1jVmnTRg/V/DtxFlI3gLWq9AAXE+yWUVbQAaOBBoAbnn2tdj6MDOrMjUJ4s1zG52GvqK+dnYEmOPg1sFRnjA6Bj7mxzOA/UQWW1HHPdhEK46maADpB4IOUgJbg1uvaB5wNVEKHNejN709WMRYHE3RQEthGlgBFzPkKrdSpl8JDT8muAnA3iEDR1Y0kH4gOLXbi8V0bnTej0f3gAMB5jiqogEY3603PK16ZxMhq73yZlJWo8GWOg8JmOPoigbyDwQ9JG8noYZlNjq1KsRqxlbKAQFzLAI0ALz393+JoPS1ZBfgEaspvq7rSfnHggwswDo44kDWJjo2uqjK1icJpNiEUvkxAXMsRtFA+oGg8d3Ax1ZZmQiGvETAHItRNOA2sBbgfM348QkVwBn8kgBzLErRAHDDtbeisQzZ7Ap8hszfO2s1LxEysDBFAxCfFrB+I9Q+jaL0pQLmWJyigfQDQbPZZcDmQ8jCFexjkaAB4LrX/xZjsKD19x1nBTDH4qyDw39o4e8skNGeJcjAghUNAMOb7yRVnxSmZw0wx2IVDeSNcSAgAvHmswmY4/+kE9rZ2xC1UQAAAABJRU5ErkJggg=="></image></g><g class="cls-4"><image width="132" height="132" transform="translate(39.25 -0.84)" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIQAAACECAYAAABRRIOnAAAACXBIWXMAAAsSAAALEgHS3X78AAAIXUlEQVR4Xu2dW5rbKgyART7vf6PnbGGGPiTOgA3oDtjmf2hTB4QkFCGwkwb47/8Ii8WH7fvqGBYRAF5A40khNb2tOgU3+I1fGeH47s/n78IY8dSYh7K7Pzq/zk/Fvi38AJ0kcEJ64dgsTD/dOcTJp1lFFOaJQoUt/DJ7E5qHWqNaJtrfpi5RWgg2TE1Bf1qw4mzhF2uSEE8v5BxFvAKwdJFwUtvADm86q/iuIRAC3kTOLhvRQ5M9AkB3x4oYrmNsLBmOymXpjTgOlj3QgCGO48kprU+g05H3kjFSMdXYf51LAZNOQHwF5ViGqPWgCWjWFRURW/ihCTdlwJDFTJhewjKMFQNsBwDyuBsICjlRRYsqhDawJx0SXZJwq/EWUDHTwXahSP62k4KDyB2S0ykwdcT8hNYwR7jjYw12mHKP8Ladx9GUgw/BQecAlKK3NKUx+0sNQQ4WWHiGIAwyNUP1T4teRJE0YDQ6k/pWGkWADYyKSizyumFjTn9aAcOxaV+6OH2StswlYxI4xh44B65CmCcCteIrfIvjakZqrFwAlCVjFJOq5QbBXiwLY3MZXwAQ2/XONTPEQgRlrov3Mkj3LpI27C2XlGRM7NNyayjzI6SRIWLx5YnQ4S6lxgOKrldCcn+ohLyGiKcXZa72URa641I0bCQcXbc9pI1Mu6erBIM3IGllO+R4oiZDcKnugm6WYUp0cjEFzJ3bqYAsKu9skUZ86UgYlYc26MtE6uBPTDkpi0XqiZoelL03hpONR9g273TSD4B8DnHQqKOCbA66ofaht7ULxs5kv6EuATxqCGNxbux6YvZTMkyNkb4Qjq3eZbAwFAWgSMEcGl9WAoD2bW0KjKZqewlj2WcILZOpg9F8BJFjSy0TcWRwKciuZIh3S3VEeuHppAHEsN+ljLnP07nxDpiPnPkyhCUC00Z8CLCzmBjg9Mzn8bjg9F3b5H2OTX/nEFHgvTsxsfnohL5Cfuu7ZEuoXD+An0PsIM1Qpe8I0XXuUM5ifj9zlDVN//GOmHsvGS0eZDb6Df8A3xWCeDB1IR400absRWX9HKLu2XdyGbVIzDXjVS/MpSaZLWDFZOVtrDLuGi8Xdb4bCn8QTirrnOa8WrCcsXsOwhaRVk1TFbMzgHqGcLajOG56iXsPwVnfaXC2k5UhyJ8eC6WRynj4j55Z2NgdXOl3hsDbjaNawyB8voNwaQbov30PLLQMUL4JIcPc0u4WBF0NzyEIo0lxEJ0GgyYwzsWxkbJGYri24dvOI8zml4Jr26tSHO/8An9GLOHaA8yisisCY97gHc3mCF2WQlOdECNt+03c1hIkofxlCNyP98fYB+jh3QvqX9DdL2tmWWDP2AwhULiGxm/D+GaYxBHpy8Yv5+2BdMwwfD/EbIzn3u0EMA1ID9BfCAyEA749Qoi2btSGZITy+JF9Q4S+q7H/HsQuNwCcxwj5NcNt52iMvTkLCrPw5yD2JemvjtwgKSoDgPpIOAPPeAtvmnNwfvOUIUg/FrLQMY2PCwHBfbi2uA4lmGaYAXzV57nlNpy//X0Ee/8AKs8rYLBxF3XSorL7OUQsxESi0NUzjBSS2dWgt/s08O9laCAMNSzDeILZNBE25xAWMqhgY3kFDDbuxHBcktcQlzG6VdUi/6ZCuenUFaYhzOY79G9uzYS3ygGAu/typaMqzSXD7TPS0UARiH6lwlfsq8l8gW87d6jtujFOoaLP0muiH0KjYiSoImbokiH+VFlj7QLkLiV3a81s/kZoEz1DzMQVdU5AfS6JAEwmEZttZ4qpPFNhXyT+7gpawxwsOLXPL5Ds/XR59gMyPXBwL/poHikCoKibfYZ4OjP4U6HDoYaIubApDmcY1jGaLsq0b39z/t+m2VjBIYK+ZKTtktel2OBuq7pAtdOAGc2nwt92Eto3Zc7gLYIN96fsBPYTU1+E3dB+IwIG0+lB0JeMGo3+orlV6mOyXAl0sBh2BvhLRgkLGRYEwimgFC+5k6HPECmWsjKIgrFm1ttobDw17gOckNcQNZAdyFAQWy2XGwtRI7BZMo54yOwA6gvpLGNyJ8J2ydjxkDkDmF2cgMFkDUIVEFX7TzIVg1wJxMzaj4NkV+PpRVf8HsN3EntlUF+3Mkwnf6oyRBUPmU8A81vxPkHhmoJVVF6Jol/TbV0SMew5eHew33YCCJRZmGAwlz5LRove4y1YnAKCsnMwOcA54CCyHzcKclENIemzmJDCPDosGVKBl84RDaT+GMOmUljRNSMA2AlTMIEKoyFnCNfPb6pDQR/LmsVQVH+Ic6VBVEP05go63oWt+e5dJgJLC3ex0wDyktENtT6lNed8KcP6wRkumH5NVJ1PqE8qB7vSBuT3ork1DLN5PwhTfYkaYjRP8lG7hrDgQc6swkkZg/01Xw0xAm8fYPI5AcMBG7eAW0B42XgfYvGlmAiC4vgwcPR8YmrRH4O59K8heqD3w/3ZkwfiK7clYzEZxHnOMwSxU7pSNffoRHkLHdzKoYX6HELbfzEX8hqCEgiWodsbin03xLeGaMqOcO2IGYnfpOkekFFz3gdnrHjJ6TBVvhlCy8y63ZQX1mDxLETbTk96rhKReFjTg552t1BvO6/Mk22vId92arjLRFh9rCfyx9xF5ezc0HdjMsRsjJzYWpZJdZJkIqFNk2SIKZQYA2p6KLRBO4kZfDC1wGnMT+mwt9SckmE+/VwyBGX8hRHI/MXPH9Q52agNF9eEO7/rpHKRsXYZd8Bw2XepIRbXZS0Zi4wVEIuMFRCLjBUQi4wVEIuM52w7PXZT3FMfbwxsXNtODTf03VoyFhkrIBYZKyAWGSsgFhkrIBYZKyAeSWV7FON6QOaZVGY9BPgHK/KZW2aWrhUAAAAASUVORK5CYII="></image></g><g class="cls-5"><image width="174" height="119" transform="translate(18.25 14.16)" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAK4AAAB3CAYAAACaPXsJAAAACXBIWXMAAAsSAAALEgHS3X78AAAIRElEQVR4Xu2d7YLjKgiGsSf3f6O7t7Dj+dGkYxM/QAEx4fkzu50EX5BQYmwnwJ+/EVKO/wVwLPE9S49nKwYknn4e/9h/nPM6viZl+rITKiyc2fyk2S2yhZ/EQ6qzx/EvgPATAX6qR38lfXxVj3wG1HjfEWIMjgtoC//SlwlW0kNbCXsmBAiocwh6tDAoSRSj/m7wg1AWud8q4rUF4SICxP8aajND8vo3kWw4mWM8i8SNLfyTdyoA6MUu7m0LAUtti2qspBH0Y8O9ZR9UlAiKPHOpjoNj02IApxtRxOCIQ2aSfbcxrnnLVieLotk04QzVWofwE7FmAOCc6JMg6MWBN4jyHm8OAJRahSyThiXDoJPSunSvuOCHkEdYSwByq3ACJRB1kC4GJaVU5ySnndq6jCBsvshp3A2EKy7qbaIHWdnCnMQP+BJfAVArQzuk+cCb7SB+/aCS73GlUBqKNDmcKPmXIj1/sRJMUpyZZeZbBeZBeBAWJWCeNLHaIP2l+nDpzwvjoO0Wzp93c9bLYnJFYIhBPnHGDVPvmWLIKElkhPMLO3ytApOZ2+FxKRMBAjJAR3pHOFYVvvYqOI5tPptsqnekEdmLZEx0rUVqcnk7clDgCqQ4lXXck0Ki4KOSY5+9054uIQxiYDJzJ7gfp0tB63EJh6LZbbZ1nH5PSnQEreGfxLRY4AeutgpTr77WWM1E/+ZoXVDpTjN9HxbyG//IV8GpZlIVNbTFof3cubQu7SGm04xfDZR/qINUaLcKdrRekdL2wrQuO8jDTDPZh54LrlxxJztTpiCMUy+xOpPJLbqroRA/BX57XCXholOm5MM4tHc5zNIiW1wtxzDRNukBhGB0BE0DMCYIga85Qvh3XVpEnJSj8zTWGBU0tHvc1biZOyhOPpPnFFHRAUA/tpXxtu9+7n0k6xWjiXZgrUKNA7WnR+4AG6Jhc/2KOyh/2YsUSxwM0In3xvXWUTvJ0F1xrmhfL3EXk2sGRNwwySWdL5eN6zGvC/8AwiKyMVyUCUFhHBJz8QBUHvmGkpjkdcwyDTuFK9BRhDFRe8FX3LPYkDxGrfQi6e4w2g4wYxiYrCsmRVVpZgDSpf4eN3dazVSQ749ug4epyfX7cYWChmk9RgbPfnbppgx5igox6qCp9FXcjlNESHRgP7v0gdqfE80/hklx2dBrcjtdV/sk564kQs6Puhsac/15VyxKmIkRJwSnCIcC9FZcTZTllZKR+jH+2hdpqEKTLYJEKLav3tOAk2Io+/aeLMKglnt0ghvj4AbDfSN5CwYTvUydbla/aca4KzqzuV9obqHBr+NqIOTkktRi8aqs0lDhssMFSk/EV1yxK1ISnGvrIbGHOo0VdsVlRnz3MW1V3NUQnLiphYJ4YaDblkK8sKenbP1vOacTu+04KAzHl5x4tROQfqJbhakwSCQH13nDEPthAlx0eKvwhYVZMsqs0ISwj70LOHpciBEgdlSkjCNpr0O258xhVkKiyQvsq7ilJvvzOjIalhfde0G6PgfT4khs57206f5ZcdKxEWPGV1LJEcc7SkyYi76K28ugg+TvgLhbQR+M35247sdNKM771zmGo5lKw8jEfEKjagcziAMAw6GyvzvsjJTcAOSvLl2ehd2tVtwDRB3Kg7BthqLWDidyp2CqeUrHsLbpdKhw2noVVwKNEBD38/ZXC0WILnGCqrgHarEkaLot1BhQJodq2yALbyRfSKxFqag1dAHhTCa79ipgXDYB3bXnQP1OMYaNMZz07w7rPc+RR2RuiEZRFb1CdrjfF7sq7sGgtDXpD9djiAE+FR2VIx0x7a+4vWiP5+ixzy0qWQchf6+C48wmAMAWqE36AfY0jcvvjmDj+1BI67hZaudndq4/FgNxuFMNGe9xa+eP2v5QMDR658oFm59PYTxgl/24KDpOEYGqfTTPicM5crRbhdbvYTwf1ED48iEC/mPXGQZOdVpE7Y3ki/Fpo7AJ79naDzbGO+2Ki4LFiF0S96q5SQzD9cuoiQZWARs/Av3LYU9AODTk2HPNuhREd0ZgqriLs0IMLC4tTtRzi8RtFqKLjws6LSwZ8zc0vo6Il3+osm6rsKhsq5Dz4Mhi4mlc2Ki4FjQ4NE5z1qrXXFN8jDP+5AyAT5WjiO6ktRL7wyHrq3U5aY2D+3EdR4xG69LVKlSuBefBoKsqA1t2MEI2ksWSTzhB0Obcl66Ke4Fig3IsRDs7wEYh+e2UeQeyb3eYFDkpVH03yXNViCG2AE/FJSCeVyV/Cq+P7ADTZBGZYxBykWc5bGGe7v+qbK0DiviEt/HlFzHUWwUS7NrYDdahDid1I0rVUYTN0DDZmzOh8K2H9jytfiNKlD/C43vc5Ujnizp31hK9RsM3262Cw8uN5rr/5izHjQKjwUoFEEPP9PfGwFCroCBEYYjHEkIhCU9BZ5oDW0/OnCurTI9yHnmP6yzJtcc9JzLiE9SXQ3oblxJ+cS0D99SXaPe4rd9naNp0nEHoqwrcSal1ifbA7avDhnKPmxlMfHzLVwYW8SAtxyYaFEHTeIgitPOcKM95o1xxF8DjsQT0HlcLwwk0UpTZV1xKGI7fwUgo2qsKDisebx7sVlwuVk6UkZLEicEYeo9rGZ+bIvevuBI8NaF63wEE4qVUcVUGcaQxNI2y67hOP8ampbfYUvisuCD2w/iqgmMGSi6+Wgc4jkU8cZ0lKa8qEMo2O9SGaqZWZwpKqwpELGpyTOGtgrMknrjOknjiOkviiessyRp7Ffxm7RfqikuOG8TT5qqCU8bnCwC8VXAWxRPXWRJPXGdJPHGdJfHEddYjxsKfRHUcy4QA/wM0JGxAD1YgyQAAAABJRU5ErkJggg=="></image></g><path class="cls-6" d="M256.56,127.2v-.08a1,1,0,0,0-1.62-.53,26.11,26.11,0,0,1-18.42,7.14q-11.31,0-18.46-6.46a20.64,20.64,0,0,1-7.14-16q0-12,8.91-18.42t25.5-6.42h9.36a1,1,0,0,0,1-1v-4a13.18,13.18,0,0,0-3.26-9.33q-3.27-3.5-9.92-3.5A14.86,14.86,0,0,0,233,71.6a9,9,0,0,0-3.48,5.34,2.45,2.45,0,0,1-2.37,2H215.11a2.42,2.42,0,0,1-2.4-2.76,19.27,19.27,0,0,1,3.89-8.8,27.37,27.37,0,0,1,11.13-8.43,39.22,39.22,0,0,1,15.72-3q13.17,0,21,6.63t8,18.63V115q0,10,2.75,16a.93.93,0,0,1,.09.4h0a1,1,0,0,1-1,1H262.93A6.48,6.48,0,0,1,256.56,127.2Zm-16.93-7a19.17,19.17,0,0,0,9.4-2.43,16.22,16.22,0,0,0,6.5-6.29,1.05,1.05,0,0,0,.13-.49V98.07a1,1,0,0,0-1-1h-8.11q-9.37,0-14.09,3.26a10.58,10.58,0,0,0-4.71,9.23,9.86,9.86,0,0,0,3.22,7.74A12.49,12.49,0,0,0,239.63,120.2Z"></path><path class="cls-6" d="M290.21,37.77a9,9,0,0,1,2.46-6.45q2.46-2.57,7-2.57t7.08,2.57a8.85,8.85,0,0,1,2.5,6.45,8.65,8.65,0,0,1-2.5,6.35q-2.49,2.54-7.08,2.53t-7-2.53A8.74,8.74,0,0,1,290.21,37.77Zm9.85,94.57h-.76a8,8,0,0,1-8.05-8V59.68a2.41,2.41,0,0,1,2.41-2.41h6.4a8,8,0,0,1,8.05,8v59A8,8,0,0,1,300.06,132.34Z"></path><path class="cls-6" d="M336.35,132.34h-.77a8,8,0,0,1-8-8V28.19A2.41,2.41,0,0,1,330,25.77h6.4a8,8,0,0,1,8,8.05v90.47A8,8,0,0,1,336.35,132.34Z"></path><path class="cls-6" d="M439.79,119.13a3.69,3.69,0,0,1-.82,2.3q-4.46,5.53-13.09,8.76a60.15,60.15,0,0,1-21.19,3.54,39.44,39.44,0,0,1-21.1-5.69,37.88,37.88,0,0,1-14.29-16.1,54.4,54.4,0,0,1-5.13-23.87V75.31q0-21,10.61-33.2T403.3,29.93q15.6,0,25.11,8a31.52,31.52,0,0,1,10.52,17.51,3.59,3.59,0,0,1-3.53,4.38h-1.26a3.57,3.57,0,0,1-3.5-2.7q-2.22-9.15-8.51-14.18-7.17-5.74-18.76-5.74-14.16,0-22.41,10.07t-8.26,28.4V87.56a50,50,0,0,0,3.92,20.38,31.06,31.06,0,0,0,11.24,13.71,29.72,29.72,0,0,0,16.83,4.86q11,0,18.94-3.47a22.56,22.56,0,0,0,6.59-4.22,3.51,3.51,0,0,0,1-2.53V95.22a3.61,3.61,0,0,0-3.61-3.61H407.74A3.61,3.61,0,0,1,404.13,88h0a3.61,3.61,0,0,1,3.61-3.6h28.44a3.6,3.6,0,0,1,3.61,3.6Z"></path><path class="cls-6" d="M472.54,95.46v32.61a4.27,4.27,0,0,1-4.27,4.27h0a4.27,4.27,0,0,1-4.27-4.27V35.59a4.27,4.27,0,0,1,4.27-4.27h30.15q15.75,0,24.87,8t9.12,22.13q0,14.22-8.77,22T498.21,91.2h-21.4A4.27,4.27,0,0,0,472.54,95.46Zm0-15.75A4.27,4.27,0,0,0,476.81,84h21.61q12.42,0,18.94-5.9t6.52-16.47q0-10.5-6.49-16.71T499,38.54H476.81a4.27,4.27,0,0,0-4.27,4.26Z"></path><path class="cls-6" d="M616.43,38.54H589a3.61,3.61,0,0,0-3.61,3.6v86.59a3.61,3.61,0,0,1-3.61,3.61h-1.32a3.61,3.61,0,0,1-3.6-3.61V42.14a3.61,3.61,0,0,0-3.61-3.6H545.94a3.61,3.61,0,0,1-3.61-3.61h0a3.61,3.61,0,0,1,3.61-3.61h70.49A3.61,3.61,0,0,1,620,34.93h0A3.61,3.61,0,0,1,616.43,38.54Z"></path></g></g></svg>
				
               </div>
               <div class="col-12 text-center mt20 mt-md60">
                  <div class="f-md-24 f-18 w500 text-center white-clr pre-heading lh150">
                  Finally Aweber, GetResponse &amp; MailChimp Killer Is Here ...
                  </div>
               </div>
               <div class="col-12 mt-md50 mt20 f-md-44 f-28 w700 text-center white-clr lh150 main-heading">
                
                  <span class="blue-gradient"> World’s First ChatGPT AI  -  Powered Email <br class="d-none d-md-block">Marketing App</span> Writes, Design &amp; Send <br class="d-none d-md-block"> Unlimited Profit Pulling  <span class="blue-gradient">Newsletter</span><span class="cursor typing">&nbsp;</span> <br class="d-none d-md-block"> <span class="under"> with Just One Keyword </span>for Tons of Traffic, Commissions, &amp; Sales on Autopilot                
                  
               </div>
               <div class="col-12 mt-md50 mt20 text-center">
                  <div class="post-heading f-16 f-md-24 w500 text-center lh140 white-clr text-capitalize">
                  Say Goodbye to Expensive Email Marketing Apps, Writers, &amp; Freelancers. Even A Kid Can Start &amp; Make 6 Figure Email Marketing Income with MailGPT. No Monthly Fee Ever…
                  </div>
               </div>
               <!--<div class="col-12 mt-md30 mt20 text-center">-->
               <!--   <div class="f-16 f-md-24 w500 text-center lh140 white-clr text-capitalize">-->
               <!--   Say No To Expensive Email Marketing Tools / Autoresponders, Email Writers & Designers-->
               <!--   </div>-->
               <!--</div>-->
               <div class="col-12 mt20 mt-md80">
                  <div class="row">
                     <div class="col-md-7 col-12">
                        <!--<img src="assets/images/product-box.webp" class="img-fluid d-block mx-auto">-->
                         <div class="col-12 responsive-video mt20 mt-md50">
                           <iframe src="https://mailgpt.dotcompal.com/video/embed/xajnf33ejg" style="width:100%; height:100%" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                        </div> 
                     </div>
                     <div class="col-md-5 col-12 mt20 mt-md0">
                        <div class="key-features-bg">
                           <ul class="list-head pl0 m0 f-20 lh150 w400 white-clr">
                              <li> <span class="list-highlight">	Create 1000s Of  </span> High-Converting Emails  <span class="list-highlight">With Just One Keyword </span> </li>
                              <li> <span class="list-highlight"> 	Just Enter Keyword &amp; MailGPT AI Plans And  </span> Writes Email Content For You</li> 
                              <li><span class="list-highlight">	Send Unlimited Emails</span> To Unlimited Subscribers. </li>
                            
                              <li> <span class="list-highlight"> 	AI Enabled Smart Tagging </span>For Lead Personalization &amp; Traffic</li> 
                              <li><span class="list-highlight"> Collect Unlimited Leads  </span> With Inbuilt Forms </li>
                              <li> <span class="list-highlight"> 	Design Newsletter &amp; Autoresponder </span> Mails Using The Power Of AI</li>
                              <li> <span class="list-highlight"> Free SMTP </span> For Unrestricted Mailing </li>
                              <li> <span class="list-highlight"> 	Upload Unlimited List  </span> with Zero Restrictions </li>
                             
                              <li> <span class="list-highlight"> 100% Control </span> on your online business</li> 
                              <li> <span class="list-highlight"> 	100+ High Converting Templates  </span>For Webforms &amp; Email </li>
                              <li> <span class="list-highlight"> 	No Monthly Fee Forever. Pay One Time</span></li>
                              <li> <span class="list-highlight"> 	GDPR &amp; Can-Spam Compliant</span></li>
                              <li> <span class="list-highlight">Commercial License Included </span></li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
	  <div class="gr-1">
      <div class="container-fluid p0">
         <picture>
            <source media="(min-width:768px)" srcset="assets/images/mailgpt.webp">
            <source media="(min-width:320px)" srcset="assets/images/mailgpt-mview.webp">
            <img src="assets/images/mailgpt.webp" alt="Steps" style="width:100%;">
         </picture>
      </div>
   </div>

  	<!-- Bonus Section Header Start -->
	<div class="bonus-header">
		<div class="container">
			<div class="row">
				<div class="col-12 col-md-10 mx-auto heading-bg text-center">
					<div class="f-24 f-md-36 lh160 w700">When You Purchase LearnX, You Also Get <br class="d-lg-block d-none"> Instant Access To These 10 Exclusive Bonuses</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Bonus Section Header End -->

	
	<!-- Bonus #1 Section Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
			 <div class="col-12 text-center">
			 </div>
			 <div class="col-12 mt20 mt-md30">
				<div class="row d-flex align-items-center flex-wrap">
				   <div class="col-12 col-md-5 order-3 order-md-0 text-center mt20 mt-md0">
					  <img src="assets/images/bonus1.webp" class="img-fluid">
				   </div>
				   <div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				  <div class="text-md-start text-center">
				  <div class="bonus-title-bg mb-4">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 1</div>
				</div>
				  </div>
					  <div class="f-22 f-md-34 w600 lh160 bonus-title-color mt20 mt-md30">
					  Course Engagement Hacks
					  </div>
					  <div class="f-18 f-md-22 w400 lh140 bonus-title-color mt20">
					  Supercharge your learning with our Course Engagement Hacks Ebook. Unlock proven strategies and techniques to boost participation, understanding, and retention. Perfectly complement LearnX for an enriched educational journey. Elevate your learning experience today!
					  </div>
					  <!-- <ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
						<li>Finding the right niche for you is crucial to your success!</li>
						<li>If you want to make money online, there are many techniques to do it. But the thing is that, on every technique to apply, market saturation is always an issue.</li>
						<li>The good news is that, as time passed by internet marketers and online business owner a way to at least walk a different path to avoid this huge competition and dominate the market.</li>
					  </ul> -->
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #1 Section End -->

	<!-- Bonus #2 Section Start -->
	<div class="section-bonus">
	   <div class="container">
		
			 <div class="col-12 mt20 mt-md30">
				<div class="row align-items-center flex-wrap">
				   <div class="col-12 col-md-5 order-3 order-md-3 text-center mt20 mt-md0">
					  <img src="assets/images/bonus2.webp" class="img-fluid">
				   </div>
				   <div class="col-12 col-md-7 mt-md0 mt20">
				   <div class=" text-md-start text-center">
				<div class="bonus-title-bg mb-4">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 2</div>
				</div></div>
			
					  <div class="f-22 f-md-34 w600 lh160 bonus-title-color mt20 mt-md30">
					  20 Online Business Ideas
					  </div>
					  <div class="f-18 f-md-22 w400 lh140 bonus-title-color mt20">
					  Explore 20 innovative online business ideas that perfectly complement LearnX. From tutoring platforms to niche skill courses, this bonus guide offers diverse opportunities to monetize your expertise and thrive in the digital landscape.
					  </div>
					  <!-- <ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
					  	<li>Discover how to get the most out of Google Plus and gain exposure for your brand! </li> 
						<li>Debuting as a social networking element the Google plus site is something that was launched to rival the Facebook popularity. </li>
						<li>Launched only recently it is a fairly new introduction to the internet world and should be explored for its potential to providing a good platform for internet marketing.</li>
					</ul> -->
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #2 Section End -->

	<!-- Bonus #3 Section Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row ">
		
			 <div class="col-12 mt20 mt-md30">
				<div class="row align-items-center flex-wrap">
				   <div class="col-12 col-md-5 order-3 order-md-0 text-center mt20 mt-md0">
					  <img src="assets/images/bonus3.webp" class="img-fluid">
				   </div>
				   <div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				   <div class="text-center text-md-start">
				<div class="bonus-title-bg mb-4">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 3</div>
				</div>
			 </div>
					  <div class="f-22 f-md-34 w600 lh160 bonus-title-color mt20 mt-md30">
					  Course Ninja
					  </div>
					  <div class="f-18 f-md-22 w400 lh140 bonus-title-color mt20">
					  Elevate your mastery with Course Ninja, the perfect complement to LearnX. Unleash advanced techniques, ninja-level strategies, and exclusive insights. Sharpen your skills, dominate your goals, and become a learning legend
					  </div>
					  <!-- <ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
					  	<li>These 220 success principles will shape your business and your life in a positive way. You can use these principles in blog posts, articles, videos, as bonuses, or even as a stand alone product that you sell! </li>
						<li>When it comes to life and business, it is no coincidence that some people always seem to fail while others always seem to flourish. For sure, chance plays a role in everything. But as individuals, as business-owners, as thinkers, and as parents, we have a significant degree of control over our lives. </li>
						<li>Now, we can use the control that we have to influence outcomes in bad ways. Or we can use it to influence outcomes in our favor; and in the favor of those we care about most.</li>
					  </ul> -->
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #3 Section End -->

	<!-- Bonus #4 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row ">
		
			 <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
				<div class="row align-items-center flex-wrap">
				   <div class="col-12 col-md-5 order-3 order-md-3 text-center mt20 mt-md0">
					  <img src="assets/images/bonus4.webp" class="img-fluid">
				   </div>
				   <div class="col-md-7 col-12 mt20 mt-md0 ">
				   <div class="text-md-start text-center">
				<div class="bonus-title-bg mb-4">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 4</div>
				</div>
			 </div>
					  <div class="f-22 f-md-34 w600 lh160 bonus-title-color mt20 mt-md30">
					  Killer Headlines
					  </div>
					  <div class="f-18 f-md-22 w400 lh140 bonus-title-color mt20">
					  Supercharge your LearnX experience with Bonus Killer Headlines! Elevate your academy's appeal and engagement effortlessly. Unleash the power of compelling headlines to captivate learners and boost your educational content's impact. Upgrade now!
					  </div>
					  <!-- <ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
					  <li>Membership sites have become a popular way for online business owners to generate substantial recurring income with as little as ten hours of work a week.</li>
					  <li>Creating a membership site for your business is an excellent way for you to create recurring revenue, build a large following of loyal customers, and become an authority in your industry. </li>
					  <li>One of the most critical benefits that you gain from starting a membership site is the opportunity it can provide you to establish your reputation and your brand within your industry. </li>
					</ul> -->
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #4 End -->

	<!-- Bonus #5 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row ">
		
			 <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
				<div class="row align-items-center flex-wrap">
				   <div class="col-12 col-md-5 order-3 order-md-0 text-center mt20 mt-md0 ">
					  <img src="assets/images/bonus5.webp" class="img-fluid">
				   </div>
				   <div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				   <div class="text-md-start text-center">
				<div class="bonus-title-bg mb-4">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 5</div>
				</div>
			 </div>
					  <div class="f-22 f-md-34 w600 lh160 bonus-title-color mt20 mt-md30">
					  100 Ecourse Publishing Tips 
					  </div>
					  <div class="f-18 f-md-22 w400 lh140 bonus-title-color mt20">
					  Unlock the full potential of LearnX with '100 Ecourse Publishing Tips.' Elevate your online courses with strategic insights, practical advice, and proven techniques for effective e-course creation and publishing. Maximize impact and engagement effortlessly.
					  </div>
					  <!-- <ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
					  	<li>Uncover the powerful tools that will eliminate guesswork and virtually guarantee success!</li>
						<li>Find out how savvy email marketers use free resources to automate their online income!</li>
						<li>How to instantly save time and money by following a powerful system proven to work!</li>
						<li>What you need to know about choosing an autoresponder provider, hosting and more!</li>
					  </ul> -->
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #5 End -->

	<!-- CTA Button Section Start -->
	<div class="cta-btn-section dark-cta-bg">
	   <div class="container">
		  <div class="row">
			 <div class="col-md-12 col-md-12 col-12 text-center">
				<div class="f-md-20 f-18 text-center mt3 white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
				<div class="f-md-22 f-22 text-center purple lh120 w700 mt15 mt-md20 orange-clr">TAKE ACTION NOW!</div>
				<div class="f-md-22 f-17 lh120 w600 mt15 mt-md20 white-clr">Use Special Coupon <span class="w800 orange-clr">HOLIDAY35</span> For Massive <span class="w800 orange-clr">35% Off</span> On Entire Funnel </div>
			 </div>
			 <div class="col-md-12 col-12 text-center mt15 mt-md20">
				<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
				<span class="text-center">Grab LearnX + My 10 Exclusive Bonuses</span> <br>
				</a>
			 </div>
			 <div class="col-12 mt15 mt-md20 text-center">
				<img src="assets/images/payment.png" class="img-fluid">
			 </div>
			 <div class="col-12 mt15 mt-md20" align="center">
				<h3 class="f-md-28 f-20 w500 text-center white-clr">Coupon Is Expiring In... </h3>
			 </div>
			 <!-- Timer -->
			 <div class="col-12 text-center">
				<div class="countdown counter-white white-clr">
				   <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Days</span> </div>
				   <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
				   <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
				   <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
				</div>
			 </div>
			 <!-- Timer End -->
		  </div>
	   </div>
	</div>
	<!-- CTA Button Section End -->

	<!-- Bonus #6 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
		
			 </div>
			 <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
				<div class="row align-items-center flex-wrap">
				   <div class="col-12 col-md-5 order-3 order-md-3 text-center mt20 mt-md0">
					  <img src="assets/images/bonus6.webp" class="img-fluid">
				   </div>
				   <div class="col-md-7 col-12 mt20 mt-md0">
				   <div class="text-center text-md-start">
				<div class="bonus-title-bg mb-4">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 6</div>
				</div>
					  <div class="f-22 f-md-34 w600 lh160 bonus-title-color mt20 mt-md30">
					  Animated GIFs
					  </div>
					  <div class="f-18 f-md-22 w400 lh140 bonus-title-color mt20">
					  Elevate your LearnX with dynamic Animated GIFs! Enhance engagement and understanding as these captivating visuals complement LearnX , making learning an immersive and enjoyable experience
					  </div>
					  <!-- <ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
					  	<li>Web marketing does not have to be tedious and expensive! Attract targeted traffic to your site! All you need is to know the secrets of powerful link exchange. Learn some quick link exchanging tactics to earn high ROIs at low investment!</li>
						<li> Developing a standalone website does not make much sense these days. With the increasing number of websites worldwide, individual websites tend to suffer from decreasing traffic. Webmasters have tested so many different techniques to attract visitors to their websites and gradually, these techniques have emerged into strategies that people use in order to generate traffic on a regular basis.  </li>
					  </ul> -->
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #6 End -->

	<!-- Bonus #7 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
		
			 <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
				<div class="row align-items-center flex-wrap">
				   <div class="col-12 col-md-5 order-3 order-md-0 text-center mt20 mt-md0">
					  <img src="assets/images/bonus7.webp" class="img-fluid">
				   </div>
				   <div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				   <div class="text-md-start text-center">
				<div class="bonus-title-bg mb-4">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 7</div>
				</div>
			 </div>
					  <div class="f-22 f-md-34 w600 lh160 bonus-title-color mt20 mt-md30">
					  Creating Digital Products
					  </div>
					  <div class="f-18 f-md-22 w400 lh140 bonus-title-color mt20">
					  Create engaging digital products effortlessly to complement LearnX, offering a seamless learning experience. Elevate your content, enhance interactivity, and captivate learners with innovative digital solutions..
					  </div>
					  <!-- <ul class="bonus-list f-18 f-md-20 lh160 w400">
					  	<li>If you're looking to make some fast cash, or you're interested in building a long-term sustainable business, consulting is one of the most lucrative opportunities available online.</li>
						<li>The dictionary defines consultant as: “a person who provides expert advice professionally.”  </li>
						<li>As a consultant or coach, you'll be responsible for guiding your students or clients through a learning curve until they've accomplished a specific goal. </li>
					</ul> -->
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #7 End -->

	<!-- Bonus #8 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
		
			 <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
				<div class="row align-items-center flex-wrap">
				   <div class="col-12 col-md-5 order-3 order-md-3 text-center mt20 mt-md0">
					  <img src="assets/images/bonus8.webp" class="img-fluid">
				   </div>
				   <div class="col-md-7 col-12 mt20 mt-md0">
				   <div class="text-center text-md-start">
				<div class="bonus-title-bg mb-4">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 8</div>
				</div>
			 </div>
					  <div class="f-22 f-md-34 w600 lh160 bonus-title-color mt20 mt-md30">
					  ChatGPT Blueprint
					  </div>
					  <div class="f-18 f-md-22 w400 lh140 bonus-title-color mt20">
					  The perfect Bonus to complement the LearnX to Enhance engagement, foster dynamic interactions, and personalize learning journeys using advanced language models for an unparalleled educational synergy.
					  </div>
					  <!-- <ul class="bonus-list f-18 f-md-20 lh160 w400">
					  	<li>Discover 20 Online Business Ideas You Can Start Today So You Can Have The Freedom To Work Anywhere!</li>
						<li>Without an idea, there is no chance to start your own online business. With tons of entrepreneurs out there this first step is one of the hardest challenges, where you might find yourself wandering around the web to get your creative juices flowing. </li>
						<li>Don't look any further, as we put together a list of 20 online business ideas which you can start tomorrow. Obviously, there is a need for tremendous preparation and research, but hopefully, inside this list can get you started.</li>
					  </ul> -->
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #8 End -->

	<!-- Bonus #9 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
		
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row align-items-center flex-wrap">
				   		<div class="col-12 col-md-5 order-3 order-md-0 text-center mt20 mt-md0">
					  		<img src="assets/images/bonus9.webp" class="img-fluid">
				   		</div>
				   		<div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				   			<div class="text-md-start text-center">
								<div class="bonus-title-bg mb-4">
				   					<div class="f-22 f-md-28 lh120 w700">Bonus 9</div>
								</div>
			 				</div>
					  		<div class="f-22 f-md-34 w600 lh160 bonus-title-color  mt20 mt-md30">					 
							  Launch your online course 
					  		</div>
							<div class="f-18 f-md-22 w400 lh140 bonus-title-color mt20">
								Launch your online course seamlessly, complementing LearnX's user-friendly interface and powerful features. Empower learners and build a thriving online learning community effortlessly.
							</div>
					  		<!-- <ul class="bonus-list f-18 f-md-20 lh160 w400">
					  			<li>In the main report you will learn what digital products are and why it's a great business model. You will learn how to create a digital product that people need and want.</li>
								<li>3 Options For Selling Your Digital Products. Defining The Target Audience For Your Info Product. How To Come Up With Info Product Ideas That Sell. How To Launch A Digital Product. </li>
							</ul> -->
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #9 End -->

	<!-- Bonus #10 start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
		
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row align-items-center flex-wrap">
				   		<div class="col-12 col-md-5 order-3 order-md-3 text-center mt20 mt-md0">
					  		<img src="assets/images/bonus10.webp" class="img-fluid">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
				   			<div class="text-md-start text-center">
								<div class="bonus-title-bg mb-4">
				   					<div class="f-22 f-md-28 lh120 w700">Bonus 10</div>
								</div>
			 				</div>
					  		<div class="f-22 f-md-34 w600 lh160 bonus-title-color mt20 mt-md30">
							  Passive Cash Profits
					  		</div>
							  <div class="f-18 f-md-22 w400 lh140 bonus-title-color mt20">
							  Unlock financial success effortlessly with Passive Cash Profits, the perfect companion to LearnX. Seamlessly integrate wealth-building strategies into your learning platform and empower users to earn while they learn
							</div>
					  		<!-- <ul class="bonus-list f-18 f-md-20 lh160 w400">
							  	<li>Learn How To Write Attention Grabbing Headlines!</li>
								<li>If you have an online business, and even if you have a brick and mortar store with an online presence, you should be marketing to your customers and prospects through email. It is one of the most effective ways to increase sales and profits.  </li>
								<li>You have to earn the click that opens the email and you earn it with an interesting email subject line. !Think about that each time you sit down to write an email to your subscribers.</li>
							</ul> -->
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #10 End -->

	<!-- Huge Woth Section Start -->
	<div class="huge-area mt30 mt-md10">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-12 text-center">
                    <div class="f-md-60 f-30 lh120 w700 white-clr">That’s Huge Worth of</div>
                    <br>
                    <div class="f-md-60 f-30 lh120 w800 yellow-clr">$2465!</div>
                </div>
            </div>
        </div>
    </div>
	<!-- Huge Worth Section End -->

	<!-- text Area Start -->
	<div class="white-section pb-0">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-12 text-center p0">
                	<div class="f-md-40 f-25 lh160 w400">So what are you waiting for? You have a great opportunity <br class="d-lg-block d-none"> ahead + <span class="w600">My 10 Bonus Products</span> are making it a <br class="d-lg-block d-none"> <b>completely NO Brainer!!</b></div>
            	</div>
				<div class="col-md-12 col-md-12 col-12 text-center mt20 mt-md50">
					<div class="f-md-26 f-18 text-center black lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
					<div class="f-md-24 f-22 text-center purple lh120 w700 mt15 mt-md20 lite-black-clr">TAKE ACTION NOW!</div>
					<div class="f-md-22 f-17 lh120 w600 mt15 mt-md20 black-clr">Use Special Coupon <span class="w800 lite-black-clr">HOLIDAY35</span></span> For Massive <span class="w800 lite-black-clr">35% Off</span> On Entire Funnel </div> 
				</div>
			
			</div>
			
		</div>
	</div>
	<!-- text Area End -->

	<!-- CTA Button Section Start -->
	<div class="cta-btn-section pt-3 ">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-md-12 col-12 text-center">
					<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
						<span class="text-center">Grab LearnX + My 10 Exclusive Bonuses</span> <br>
					</a>
			 	</div>
			 
			 	<div class="col-12 mt15 mt-md20" align="center">
					<h3 class="f-md-30 f-20 w500 text-center black">My Exclusive Bonuses Are Expiring in...</h3>
			 	</div>
			 	<!-- Timer -->
			 	<div class="col-12 text-center">
					<div class="countdown counter-black">
						<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
						<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
						<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
						<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
					</div>
			 	</div>

				 <!-- <div class="countdown counter-white text-center">
                  <div class="timer-label text-center"><span class="f-24 f-md-42 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
                  <div class="timer-label text-center"><span class="f-24 f-md-42 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                  <div class="timer-label text-center timer-mrgn"><span class="f-24 f-md-42 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                  <div class="timer-label text-center "><span class="f-24 f-md-42 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
         </div> -->
			 	<!-- Timer End -->
		  	</div>
	   	</div>
	</div>
	<!-- CTA Button Section End -->

	
	<!--Footer Section Start -->
    <div class="footer-section">
        <div class="container ">
            <div class="row">
                <div class="col-12 text-center">
				<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                            viewBox="0 0 484.58 200.84" style="max-height:75px;">
                            <defs>
                                <style>
                                    .cls-l1,
                                    .cls-l4 {
                                        fill: #fff;
                                    }

                                    .cls-l2,
                                    .cls-l3,
                                    .cls-l4 {
                                        fill-rule: evenodd;
                                    }

                                    .cls-l2 {
                                        fill: url(#linear-gradient);
                                    }

                                    .cls-l3 {
                                        fill: url(#linear-gradient-2);
                                    }

                                    .cls-l5 {
                                        fill: url(#linear-gradient-3);
                                    }
                                </style>
                                <linearGradient id="linear-gradient" x1="-154.67" y1="191.86" x2="249.18" y2="191.86"
                                    gradientTransform="matrix(1.19, -0.01, 0.05, 1, 178.46, -4.31)"
                                    gradientUnits="userSpaceOnUse">
                                    <stop offset="0" stop-color="#ead40c"></stop>
                                    <stop offset="1" stop-color="#ff00e9"></stop>
                                </linearGradient>
                                <linearGradient id="linear-gradient-2" x1="23.11" y1="160.56" x2="57.61" y2="160.56"
                                    gradientTransform="matrix(1, 0, 0, 1, 0, 0)" xlink:href="#linear-gradient">
                                </linearGradient>
                                <linearGradient id="linear-gradient-3" x1="384.49" y1="98.33" x2="483.38" y2="98.33"
                                    gradientTransform="matrix(1, 0, 0, 1, 0, 0)" xlink:href="#linear-gradient">
                                </linearGradient>
                            </defs>
                            <g id="Layer_2" data-name="Layer 2">
                                <g id="Layer_1-2" data-name="Layer 1">
                                    <path class="cls-l1"
                                        d="M112.31,154.3q-11.65,0-20.1-4.87a32.56,32.56,0,0,1-13-13.82q-4.53-9-4.54-21.11a46.06,46.06,0,0,1,4.57-21A34.29,34.29,0,0,1,92,79.37a36.11,36.11,0,0,1,19.31-5.06,39.65,39.65,0,0,1,13.54,2.29,31,31,0,0,1,11.3,7.09,33.32,33.32,0,0,1,7.75,12.18,49.23,49.23,0,0,1,2.82,17.57V119H83.26v-12.3h46A19.87,19.87,0,0,0,127,97.38a16.6,16.6,0,0,0-6.18-6.48,17.59,17.59,0,0,0-9.21-2.37,17.88,17.88,0,0,0-9.83,2.7,18.81,18.81,0,0,0-6.58,7.06,20.17,20.17,0,0,0-2.4,9.56v10.74a25.15,25.15,0,0,0,2.47,11.57,17.42,17.42,0,0,0,6.91,7.37,20.52,20.52,0,0,0,10.39,2.55,21.62,21.62,0,0,0,7.22-1.14,15.71,15.71,0,0,0,5.59-3.35,14.11,14.11,0,0,0,3.59-5.5L146,132a26.44,26.44,0,0,1-6.13,11.77,29.72,29.72,0,0,1-11.52,7.77A43.74,43.74,0,0,1,112.31,154.3Z">
                                    </path>
                                    <path class="cls-l1"
                                        d="M184.49,154.35a31.78,31.78,0,0,1-13.24-2.65,21.26,21.26,0,0,1-9.28-7.84,22.86,22.86,0,0,1-3.41-12.81A21.92,21.92,0,0,1,161,120.2a18.9,18.9,0,0,1,6.61-6.86,33.85,33.85,0,0,1,9.46-3.9,77.76,77.76,0,0,1,10.92-2q6.81-.7,11-1.28a15.91,15.91,0,0,0,6.18-1.82,4.25,4.25,0,0,0,1.94-3.86v-.3q0-5.7-3.38-8.83T194,88.28q-6.71,0-10.62,2.92a14.55,14.55,0,0,0-5.27,6.91l-17-2.42a27.51,27.51,0,0,1,6.66-11.83,29.46,29.46,0,0,1,11.35-7.16,43.73,43.73,0,0,1,14.83-2.39,47.89,47.89,0,0,1,11.14,1.31,31.6,31.6,0,0,1,10.14,4.31,22.12,22.12,0,0,1,7.39,8.14q2.81,5.15,2.8,12.87v51.85H207.84V142.14h-.61a22.1,22.1,0,0,1-4.66,6,22.35,22.35,0,0,1-7.52,4.49A30,30,0,0,1,184.49,154.35Zm4.74-13.42a19.7,19.7,0,0,0,9.53-2.19,16,16,0,0,0,6.23-5.83,15,15,0,0,0,2.19-7.91v-9.13a8.72,8.72,0,0,1-2.9,1.31,45.56,45.56,0,0,1-4.56,1.06c-1.68.3-3.35.57-5,.8l-4.29.61a32,32,0,0,0-7.32,1.81A12.29,12.29,0,0,0,178,125a9.76,9.76,0,0,0,1.82,13.39A16,16,0,0,0,189.23,140.93Z">
                                    </path>
                                    <path class="cls-l1"
                                        d="M243.8,152.79V75.31h17.7V88.23h.81a19.36,19.36,0,0,1,7.29-10.37,20,20,0,0,1,11.83-3.66c1,0,2.14,0,3.4.13a23.83,23.83,0,0,1,3.15.38V91.5a21,21,0,0,0-3.65-.73,38.61,38.61,0,0,0-4.82-.32,18.47,18.47,0,0,0-8.95,2.14,15.94,15.94,0,0,0-6.23,5.93,16.55,16.55,0,0,0-2.27,8.72v45.55Z">
                                    </path>
                                    <path class="cls-l1"
                                        d="M318.4,107.39v45.4H300.14V75.31h17.45V88.48h.91a22,22,0,0,1,8.55-10.34q5.86-3.84,14.55-3.83a27.58,27.58,0,0,1,14,3.43,23.19,23.19,0,0,1,9.28,9.93,34.22,34.22,0,0,1,3.26,15.79v49.33H349.87V106.28c0-5.17-1.34-9.23-4-12.15s-6.37-4.39-11.07-4.39a17,17,0,0,0-8.5,2.09,14.67,14.67,0,0,0-5.8,6A20,20,0,0,0,318.4,107.39Z">
                                    </path>
                                    <path class="cls-l2"
                                        d="M314.05,172.88c12.69-.14,27.32,2.53,40.82,2.61l-.09,1c1.07.15,1-.88,2-.78,1.42-.18,0,1.57,1.82,1.14.54-.06.08-.56-.32-.68.77,0,2.31.17,3,1.26,1.3,0-.9-1.34.85-.89,6,1.63,13.39,0,20,3.2,11.64.72,24.79,2.38,38,4.27,2.85.41,5.37,1.11,7.57,1.05,2-.05,5.68.78,8,1.08,2.53.34,5.16,1.56,7.13,1.65-1.18-.05,5.49-.78,4.54.76,5-.72,10.9,1.67,15.94,1.84.83-.07.69.53.67,1,1.23-1.17,3.32.56,4.28-.56.25,2,2.63-.48,3.3,1.61,2.23-1.39,4.83.74,7.55,1.37,1.91.44,5.29-.21,5.55,2.14-2.1-.13-5.12.81-11.41-1.09,1,.87-3.35.74-3.39-.64-1.81,1.94-6.28-1-7.79,1.19-1.26-.41,0-.72-.64-1.35-5,.55-8.09-1.33-12.91-1.56,2.57,2.44,6.08,3.16,10.06,3.22,1.28.15,0,.74,1,1.39,1.37-.19.75-.48,1.26-1.17,5.37,2.28,15.36,2.35,21,4.91-4.69-.63-11.38,0-15.15-2.09A148,148,0,0,1,441.58,196c-.53-.1-1.81.12-.7-.71-1.65.73-3.41.1-5.74-.22-37.15-5.15-80.47-7.78-115.75-9.76-39.54-2.22-76.79-3.22-116.44-2.72-61.62.78-119.59,7.93-175.21,13.95-5,.55-10,1.49-15.11,1.47-1.33-.32-2.46-1.91-1.25-3-2-.38-2.93.29-5-.15a9.83,9.83,0,0,1-1.25-3,13.73,13.73,0,0,1,6.42-2.94c.77-.54.12-.8.15-1.6a171,171,0,0,1,45.35-8.58c26.43-1.1,53.68-4.73,79.64-6,6.25-.3,11.72.06,18.41.14,8.31.1,19.09-1,29.19-.12,6.25.54,13.67-.55,21.16-.56,39.35-.09,71.16-.23,112.28,2C317.24,171.93,315.11,173.81,314.05,172.88Zm64.22,16.05-1.6-.2C376.2,190,378.42,190.26,378.27,188.93Z">
                                    </path>
                                    <polygon class="cls-l3"
                                        points="52.57 166.06 57.61 151.99 23.11 157.23 32.36 169.12 52.57 166.06">
                                    </polygon>
                                    <polygon class="cls-l4" points="44.69 183.24 51.75 168.35 33.86 171.06 44.69 183.24">
                                    </polygon>
                                    <path class="cls-l4"
                                        d="M.12,10.15l22,145.06,35.47-5.39-22-145a2.6,2.6,0,0,0,0-.4C35,.76,26.62-.95,16.81.54S-.52,6.16,0,9.77A2.62,2.62,0,0,0,.12,10.15Z">
                                    </path>
                                    <path class="cls-l5"
                                        d="M433.68,79.45l21-36.33h27.56L448.48,97.51l34.9,56H456l-22-37.76-21.94,37.76H384.49l34.9-56L385.72,43.12h27.35Z">
                                    </path>
                                </g>
                            </g>
                        </svg>
						<div class="f-14 f-md-16 w400 mt20 lh160 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
                    <div class=" mt20 mt-md40 white-clr"> <div class="f-17 w300 mt20 lh140 white-clr text-center"><script type="text/javascript" src="
                    https://warriorplus.com/o2/disclaimer/fbmn7d" defer></script><div class="wplus_spdisclaimer"></div></div></div>
			</div>
            <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                <div class="f-14 f-md-16 w400 lh160 white-clr text-center">Copyright © LearnX</div>
                    <ul class="footer-ul w400 f-14 f-md-16 white-clr text-center text-md-right">
                        <li><a href="https://support.bizomart.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                        <li><a href="http://getlearnx.com/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                        <li><a href="http://getlearnx.com/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                        <li><a href="http://getlearnx.com/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                        <li><a href="http://getlearnx.com/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                        <li><a href="http://getlearnx.com/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                        <li><a href="http://getlearnx.com/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!--Footer Section End -->


	<script type="text/javascript">
   var noob = $('.countdown').length;
   var stimeInSecs;
   var tickers;

   function timerBegin(seconds) {   
      if(seconds>=0)  {
         stimeInSecs = parseInt(seconds);           
         showRemaining();
      }
      //tickers = setInterval("showRemaining()", 1000);
   }

   function showRemaining() {

      var seconds = stimeInSecs;
         
      if (seconds > 0) {         
         stimeInSecs--;       
      } else {        
         clearInterval(tickers);       
         //timerBegin(59 * 60); 
      }

      var days = Math.floor(seconds / 86400);

      seconds %= 86400;
      
      var hours = Math.floor(seconds / 3600);
      
      seconds %= 3600;
      
      var minutes = Math.floor(seconds / 60);
      
      seconds %= 60;


      if (days < 10) {
         days = "0" + days;
      }
      if (hours < 10) {
         hours = "0" + hours;
      }
      if (minutes < 10) {
         minutes = "0" + minutes;
      }
      if (seconds < 10) {
         seconds = "0" + seconds;
      }
      var i;
      var countdown = document.getElementsByClassName('countdown');

      for (i = 0; i < noob; i++) {
         countdown[i].innerHTML = '';
      
         if (days) {
            countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-24 f-md-42 timerbg oswald">' + days + '</span><br> &nbsp;&nbsp;&nbsp;&nbsp;<span class="f-14 f-md-14 w400 smmltd">Days</span> </div>';
         }
      
         countdown[i].innerHTML += '<div class="timer-label text-center">&nbsp;&nbsp;&nbsp;&nbsp;<span class="f-24 f-md-42 timerbg oswald">' + hours + '</span><br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="f-14 f-md-14 w400 smmltd">Hours</span> </div>';
      
         countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"> &nbsp;&nbsp;&nbsp;&nbsp;<span class="f-24 f-md-42 timerbg oswald">' + minutes + '</span><br> &nbsp;&nbsp;&nbsp;&nbsp;<span class="f-14 f-md-14 w400 smmltd">Mins</span> </div>';
      
         countdown[i].innerHTML += '<div class="timer-label text-center "> &nbsp;&nbsp;&nbsp;&nbsp; <span class="f-24 f-md-42 timerbg oswald">' + seconds + '</span><br><span class="f-14 f-md-14 w400 smmltd">Sec</span> </div>';
      }
   
   }

const givenTimestamp = 'Mon Dec 4 2023 20:30:00 GMT+0530'
const durationTime = 60;

const date = new Date(givenTimestamp);
const givenTime = date.getTime() / 1000;
const currentTime = new Date().getTime()



let cnt;
const matchInterval = setInterval(() => {
   if(Math.round(givenTime) === Math.round(new Date().getTime() /1000)){
   cnt = durationTime * 60;
   counter();
   clearInterval(matchInterval);
   }else if(Math.round(givenTime) < Math.round(new Date().getTime() /1000)){
      const timeGap = Math.round(new Date().getTime() /1000) - Math.round(givenTime)
      let difference = (durationTime * 60) - timeGap;
      if(difference >= 0){
         cnt = difference
         localStorage.mavasTimer = cnt;

      }else{
         
         difference = difference % (durationTime * 60);
         cnt = (durationTime * 60) + difference
         localStorage.mavasTimer = cnt;

      }

      counter();
      clearInterval(matchInterval);
   }
},1000)





function counter(){
   if(parseInt(localStorage.mavasTimer) > 0){
      cnt = parseInt(localStorage.mavasTimer);
   }
   cnt -= 1;
   localStorage.mavasTimer = cnt;
   if(localStorage.mavasTimer !== NaN){
      timerBegin(parseInt(localStorage.mavasTimer));
   }
   if(cnt>0){
      setTimeout(counter,1000);
  }else if(cnt === 0){
   cnt = durationTime * 60 
   counter()
  }
}

</script>
  <!--- timer end-->
</body>
</html>

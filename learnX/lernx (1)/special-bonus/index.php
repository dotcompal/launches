<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- Device & IE Compatibility Meta -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=9">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=9">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta name="title" content="LearnX Special Bonuses">
    <meta name="description" content="Grab My 10 Exclusive Bonuses Before the Deal Ends…">
    <meta name="keywords" content="LearnX Special Bonuses">
    <meta property="og:image" content="https://www.getlearnx.com/special-bonus/thumbnail.png">
    <meta name="language" content="English">
    <meta name="revisit-after" content="1 days">
    <meta name="author" content="Dr. Amit Pareek">
    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:title" content="LearnX Special Bonuses">
    <meta property="og:description" content="Grab My 10 Exclusive Bonuses Before the Deal Ends…">
    <meta property="og:image" content="https://www.getlearnx.com/special-bonus/thumbnail.png">
    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:title" content="LearnX Special Bonuses">
    <meta property="twitter:description" content="Grab My 10 Exclusive Bonuses Before the Deal Ends…">
    <meta property="twitter:image" content="https://www.getlearnx.com/special-bonus/thumbnail.png">


<title>LearnX Bonuses</title>
<!-- Shortcut Icon  -->
<link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
<!-- Css CDN Load Link -->
<link rel="stylesheet" href="assets/css/font-awesome.min.css" type="text/css" />
<link rel="stylesheet" href="https://cdn.oppyotest.com/launches/sellero/common_assets/css/bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="../common_assets/css/general.css">
<link rel="stylesheet" href="assets/css/timer.css" type="text/css" />
<link rel="stylesheet" type="text/css" href="assets/css/bonus-style.css">
<link rel="stylesheet" type="text/css" href="assets/css/style.css">
<link rel="stylesheet" type="text/css" href="assets/css/bonus.css">
<link rel="stylesheet" href="assets/css/style-feature.css" type="text/css">
<link rel="stylesheet" href="assets/css/style-bottom.css" type="text/css">
<!-- Font Family CDN Load Links -->
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&amp;family=Poppins:wght@400;500;600;700;800;900&amp;family=Red+Hat+Display:wght@300;400;500;600;700;800;900&amp;display=swap" rel="stylesheet">
<!-- Javascript File Load -->
<script src="../common_assets/js/jquery.min.js"></script>
<script src="../common_assets/js/popper.min.js"></script>
<script src="../common_assets/js/bootstrap.min.js"></script>   
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Buy Button Lazy load Script -->


<script>
 $(document).ready(function() {
  /* Every time the window is scrolled ... */
  $(window).scroll(function() {
	 /* Check the location of each desired element */
	 $('.hideme').each(function(i) {
		 var bottom_of_object = $(this).offset().top + $(this).outerHeight();
		 var bottom_of_window = $(window).scrollTop() + $(window).height();
		 /* If the object is completely visible in the window, fade it it */
		 if ((bottom_of_window - bottom_of_object) > -200) {
			 $(this).animate({
				 'opacity': '1'
			 }, 300);
		 }
	 });
  });
 });
</script>
<!-- Smooth Scrolling Script -->
<script>
 $(document).ready(function() {
  // Add smooth scrolling to all links
  $("a").on('click', function(event) {
 
	 // Make sure this.hash has a value before overriding default behavior
	 if (this.hash !== "") {
		 // Prevent default anchor click behavior
		 event.preventDefault();
 
		 // Store hash
		 var hash = this.hash;
 
		 // Using jQuery's animate() method to add smooth page scroll
		 // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
		 $('html, body').animate({
			 scrollTop: $(hash).offset().top
		 }, 800, function() {
 
			 // Add hash (#) to URL when done scrolling (default click behavior)
			 window.location.hash = hash;
		 });
	 } // End if
  });
 });
</script>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-K6H5FJP');</script>
<!-- End Google Tag Manager -->
</head>
<body>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K6H5FJP"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->


  <!-- New Timer  Start-->
  <?php
	 $date = 'June 26 2023 11:59 PM EST';
	 $exp_date = strtotime($date);
	 $now = time();  
	 /*
	 
	 $date = date('F d Y g:i:s A eO');
	 $rand_time_add = rand(700, 1200);
	 $exp_date = strtotime($date) + $rand_time_add;
	 $now = time();*/
	 
	 if ($now < $exp_date) {
	 ?>
  <?php
	 } else {
		 echo "";
	 }
	 ?>
  <!-- New Timer End -->
  <?php
	 if(!isset($_GET['afflink'])){
	 $_GET['afflink'] = 'https://warriorplus.com/o2/a/bddrps/0';
	 $_GET['name'] = 'Dr. Amit Pareek';      
	 }
	 ?>


	<div class="main-header">
        <div class="container">
            <div class="row">
				<div class="col-12">
					<div class="f-md-32 f-20 lh160 w400 text-center white-clr">
						<span class="yellow-clr w600"><?php echo $_GET['name'];?>'s </span> special bonus for &nbsp; 	  
						<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                            viewBox="0 0 484.58 200.84" style="max-height:75px;">
                            <defs>
                                <style>
                                    .cls-1,
                                    .cls-4 {
                                        fill: #fff;
                                    }

                                    .cls-2,
                                    .cls-3,
                                    .cls-4 {
                                        fill-rule: evenodd;
                                    }

                                    .cls-2 {
                                        fill: url(#linear-gradient);
                                    }

                                    .cls-3 {
                                        fill: url(#linear-gradient-2);
                                    }

                                    .cls-5 {
                                        fill: url(#linear-gradient-3);
                                    }
                                </style>
                                <linearGradient id="linear-gradient" x1="-154.67" y1="191.86" x2="249.18" y2="191.86"
                                    gradientTransform="matrix(1.19, -0.01, 0.05, 1, 178.46, -4.31)"
                                    gradientUnits="userSpaceOnUse">
                                    <stop offset="0" stop-color="#ead40c"></stop>
                                    <stop offset="1" stop-color="#ff00e9"></stop>
                                </linearGradient>
                                <linearGradient id="linear-gradient-2" x1="23.11" y1="160.56" x2="57.61" y2="160.56"
                                    gradientTransform="matrix(1, 0, 0, 1, 0, 0)" xlink:href="#linear-gradient">
                                </linearGradient>
                                <linearGradient id="linear-gradient-3" x1="384.49" y1="98.33" x2="483.38" y2="98.33"
                                    gradientTransform="matrix(1, 0, 0, 1, 0, 0)" xlink:href="#linear-gradient">
                                </linearGradient>
                            </defs>
                            <g id="Layer_2" data-name="Layer 2">
                                <g id="Layer_1-2" data-name="Layer 1">
                                    <path class="cls-1"
                                        d="M112.31,154.3q-11.65,0-20.1-4.87a32.56,32.56,0,0,1-13-13.82q-4.53-9-4.54-21.11a46.06,46.06,0,0,1,4.57-21A34.29,34.29,0,0,1,92,79.37a36.11,36.11,0,0,1,19.31-5.06,39.65,39.65,0,0,1,13.54,2.29,31,31,0,0,1,11.3,7.09,33.32,33.32,0,0,1,7.75,12.18,49.23,49.23,0,0,1,2.82,17.57V119H83.26v-12.3h46A19.87,19.87,0,0,0,127,97.38a16.6,16.6,0,0,0-6.18-6.48,17.59,17.59,0,0,0-9.21-2.37,17.88,17.88,0,0,0-9.83,2.7,18.81,18.81,0,0,0-6.58,7.06,20.17,20.17,0,0,0-2.4,9.56v10.74a25.15,25.15,0,0,0,2.47,11.57,17.42,17.42,0,0,0,6.91,7.37,20.52,20.52,0,0,0,10.39,2.55,21.62,21.62,0,0,0,7.22-1.14,15.71,15.71,0,0,0,5.59-3.35,14.11,14.11,0,0,0,3.59-5.5L146,132a26.44,26.44,0,0,1-6.13,11.77,29.72,29.72,0,0,1-11.52,7.77A43.74,43.74,0,0,1,112.31,154.3Z">
                                    </path>
                                    <path class="cls-1"
                                        d="M184.49,154.35a31.78,31.78,0,0,1-13.24-2.65,21.26,21.26,0,0,1-9.28-7.84,22.86,22.86,0,0,1-3.41-12.81A21.92,21.92,0,0,1,161,120.2a18.9,18.9,0,0,1,6.61-6.86,33.85,33.85,0,0,1,9.46-3.9,77.76,77.76,0,0,1,10.92-2q6.81-.7,11-1.28a15.91,15.91,0,0,0,6.18-1.82,4.25,4.25,0,0,0,1.94-3.86v-.3q0-5.7-3.38-8.83T194,88.28q-6.71,0-10.62,2.92a14.55,14.55,0,0,0-5.27,6.91l-17-2.42a27.51,27.51,0,0,1,6.66-11.83,29.46,29.46,0,0,1,11.35-7.16,43.73,43.73,0,0,1,14.83-2.39,47.89,47.89,0,0,1,11.14,1.31,31.6,31.6,0,0,1,10.14,4.31,22.12,22.12,0,0,1,7.39,8.14q2.81,5.15,2.8,12.87v51.85H207.84V142.14h-.61a22.1,22.1,0,0,1-4.66,6,22.35,22.35,0,0,1-7.52,4.49A30,30,0,0,1,184.49,154.35Zm4.74-13.42a19.7,19.7,0,0,0,9.53-2.19,16,16,0,0,0,6.23-5.83,15,15,0,0,0,2.19-7.91v-9.13a8.72,8.72,0,0,1-2.9,1.31,45.56,45.56,0,0,1-4.56,1.06c-1.68.3-3.35.57-5,.8l-4.29.61a32,32,0,0,0-7.32,1.81A12.29,12.29,0,0,0,178,125a9.76,9.76,0,0,0,1.82,13.39A16,16,0,0,0,189.23,140.93Z">
                                    </path>
                                    <path class="cls-1"
                                        d="M243.8,152.79V75.31h17.7V88.23h.81a19.36,19.36,0,0,1,7.29-10.37,20,20,0,0,1,11.83-3.66c1,0,2.14,0,3.4.13a23.83,23.83,0,0,1,3.15.38V91.5a21,21,0,0,0-3.65-.73,38.61,38.61,0,0,0-4.82-.32,18.47,18.47,0,0,0-8.95,2.14,15.94,15.94,0,0,0-6.23,5.93,16.55,16.55,0,0,0-2.27,8.72v45.55Z">
                                    </path>
                                    <path class="cls-1"
                                        d="M318.4,107.39v45.4H300.14V75.31h17.45V88.48h.91a22,22,0,0,1,8.55-10.34q5.86-3.84,14.55-3.83a27.58,27.58,0,0,1,14,3.43,23.19,23.19,0,0,1,9.28,9.93,34.22,34.22,0,0,1,3.26,15.79v49.33H349.87V106.28c0-5.17-1.34-9.23-4-12.15s-6.37-4.39-11.07-4.39a17,17,0,0,0-8.5,2.09,14.67,14.67,0,0,0-5.8,6A20,20,0,0,0,318.4,107.39Z">
                                    </path>
                                    <path class="cls-2"
                                        d="M314.05,172.88c12.69-.14,27.32,2.53,40.82,2.61l-.09,1c1.07.15,1-.88,2-.78,1.42-.18,0,1.57,1.82,1.14.54-.06.08-.56-.32-.68.77,0,2.31.17,3,1.26,1.3,0-.9-1.34.85-.89,6,1.63,13.39,0,20,3.2,11.64.72,24.79,2.38,38,4.27,2.85.41,5.37,1.11,7.57,1.05,2-.05,5.68.78,8,1.08,2.53.34,5.16,1.56,7.13,1.65-1.18-.05,5.49-.78,4.54.76,5-.72,10.9,1.67,15.94,1.84.83-.07.69.53.67,1,1.23-1.17,3.32.56,4.28-.56.25,2,2.63-.48,3.3,1.61,2.23-1.39,4.83.74,7.55,1.37,1.91.44,5.29-.21,5.55,2.14-2.1-.13-5.12.81-11.41-1.09,1,.87-3.35.74-3.39-.64-1.81,1.94-6.28-1-7.79,1.19-1.26-.41,0-.72-.64-1.35-5,.55-8.09-1.33-12.91-1.56,2.57,2.44,6.08,3.16,10.06,3.22,1.28.15,0,.74,1,1.39,1.37-.19.75-.48,1.26-1.17,5.37,2.28,15.36,2.35,21,4.91-4.69-.63-11.38,0-15.15-2.09A148,148,0,0,1,441.58,196c-.53-.1-1.81.12-.7-.71-1.65.73-3.41.1-5.74-.22-37.15-5.15-80.47-7.78-115.75-9.76-39.54-2.22-76.79-3.22-116.44-2.72-61.62.78-119.59,7.93-175.21,13.95-5,.55-10,1.49-15.11,1.47-1.33-.32-2.46-1.91-1.25-3-2-.38-2.93.29-5-.15a9.83,9.83,0,0,1-1.25-3,13.73,13.73,0,0,1,6.42-2.94c.77-.54.12-.8.15-1.6a171,171,0,0,1,45.35-8.58c26.43-1.1,53.68-4.73,79.64-6,6.25-.3,11.72.06,18.41.14,8.31.1,19.09-1,29.19-.12,6.25.54,13.67-.55,21.16-.56,39.35-.09,71.16-.23,112.28,2C317.24,171.93,315.11,173.81,314.05,172.88Zm64.22,16.05-1.6-.2C376.2,190,378.42,190.26,378.27,188.93Z">
                                    </path>
                                    <polygon class="cls-3"
                                        points="52.57 166.06 57.61 151.99 23.11 157.23 32.36 169.12 52.57 166.06">
                                    </polygon>
                                    <polygon class="cls-4" points="44.69 183.24 51.75 168.35 33.86 171.06 44.69 183.24">
                                    </polygon>
                                    <path class="cls-4"
                                        d="M.12,10.15l22,145.06,35.47-5.39-22-145a2.6,2.6,0,0,0,0-.4C35,.76,26.62-.95,16.81.54S-.52,6.16,0,9.77A2.62,2.62,0,0,0,.12,10.15Z">
                                    </path>
                                    <path class="cls-5"
                                        d="M433.68,79.45l21-36.33h27.56L448.48,97.51l34.9,56H456l-22-37.76-21.94,37.76H384.49l34.9-56L385.72,43.12h27.35Z">
                                    </path>
                                </g>
                            </g>
                        </svg>
					</div>
				</div>

				<!-- <div class="col-12 mt20 mt-md40 text-center">
					<div class="f-16 f-md-18 w700 lh160 white-clr"><span>Grab My 10 Exclusive Bonuses Before the Deal Ends…</span></div>
				</div> -->

				<div class="col-12 text-center lh150 mt20 mt-md40">
                    <div class="pre-heading-box f-18 f-md-22 w600 lh140 white-clr">
					We Exploited ChatGPT’s AI To Get A Share Of A $300 Billion Industry…
					</div>
                </div>

				<div class="col-12 col-md-12 px-md-0">
                    <div class="main-heading mt-md50 mt20 f-md-40 f-25 w900 text-center black2-clr lh150 red-hat-font">
                        <span class="main-heading-inner w700 f-md-40 white-clr">Next Gen AI App:</span> <span class="w400">Creates
                        </span>100's of AI-Boosted Courses <span class="w400">on Any Topic, and</span>
                        Sell Them on <span class="w400">Their</span>
                        <span class="pink">Own Self-Updating, Udemy-Like E-Learning Sites</span>
                        <span class="w400">in Just 60 Seconds!</span>
                    </div>
                </div>
			   <!-- <div class="col-12 mt-md15 mt15 text-center ">
                  <div class="f-22 f-md-28 w700 lh140 white-clr">
				  Then Promote Them To 495 MILLION Buyers For 100% Free…
                  </div>
               </div> -->
			   <div class="col-12 mt20 text-center mt-md40">
			   <div class="f-22 f-md-26 w700 lh140 white-clr post-heading red-hat-font">
                  Without Creating Anything…. Even A 100% Beginner Or Camera SHY Can Do It.
                  </div>
               </div>
			   <!-- <div class="col-12 mt15 text-center">
			   <div class="f-20 f-md-22 w500 lh140 white-clr text-center">
                  Without Us Creating Anything…. Even A 100% Beginner Or Camera SHY Can Do It.
                  </div></div> -->
			   <div class="col-12 mt15 text-center">
			   <div class="f-20 f-md-26 w600 lh140 yellow-clr red-hat-font">
                  Huge Opportunity to Transform Learnings into Earnings Today!
                  </div>
               </div>
				
            </div>
            <div class="row mt20 mt-md40">
                    <div class="video-frame col-12 col-md-10 mx-auto">
					<img src="assets/images/productbox.webp" class="img-fluid mx-auto d-block">
					   <!-- <div class="responsive-video">
						   <iframe src="https://coursesify.dotcompal.com/video/embed/o4eartift7" style="width:100%; height:100%" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
					   </div> -->
                </div>
            </div>
        </div>
    </div>
	
	<div class="second-section">
         <div class="container">
            <div class="row">
			<div class="col-12 col-md-6 ">
				<div class="f-16 f-md-20 lh160 w400">
					<ul class="kaptick pl0">
						<li>Tap Into $300 Billion Industry With Just 3 Steps </li>
						<li>Let AI Create Hundreds Of Courses For You On Autopilot </li>
						<li>Ai Finds Best Profitable Niches Ideas for You </li>
						<li>Ai Creates Our Websites Too, No Tech Setup Whatsoever</span></li>
						<li>50,000+ SEO Optimized Done For You Smoking Hot AI Courses </li>
						<li><span class="w600"> Even Traffic Is DFY. Without Spending A Dime </span></li>
						<li>Integrate Any Payment Processor Your Want </li>
						
                    </ul>
				</div>
            </div>
			<div class="col-12 col-md-6">
				<div class="f-16 f-md-20 lh160 w400">
					<ul class="kaptick pl0">
						<li>No Complicated Setup - Get Up And Running In 2 Minutes </li>
						<li>Build Your List With Autoresponder Integration </li>
						<li>AI ChatBots That Will Handle All Customer Support For You </li>
						<li>So Easy, Anyone Can Do it. </li>
                        <li>Cancel All Your Costly Subscriptions </li>
						<li>ZERO Upfront Cost </li>
						<li>30 Days Money-Back Guaran </li>
						<li>Free Commercial License Included - That's Priceless </li>
						<!-- <li><span class="w600">30 Days Money-Back Guarantee</span></li> -->
					</ul>
				</div>
			</div>
        </div>
            <div class="row mt20 mt-md30">
               <!-- CTA Btn Section Start -->
                <!-- CTA Btn Section Start -->
				<div class="col-md-12 col-md-12 col-12 text-center">
					<div class="f-md-26 f-18 text-center black lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
					<div class="f-md-24 f-22 text-center purple lh120 w700 mt15 mt-md20 lite-black-clr">TAKE ACTION NOW!</div>
					<div class="f-md-22 f-17 lh120 w600 mt15 mt-md20 black-clr">Use Special Coupon <span class="w800 lite-black-clr">"PRANSHULX"</span> For Massive <span class="w800 lite-black-clr">35% Off</span> On Entire Funnel </div> 
				</div>
				<div class="col-md-12 col-12 text-center mt15 mt-md20">
					<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
						<span class="text-center">Grab LearnX + My 10 Exclusive Bonuses</span> <br>
					</a>
				</div>
				<div class="col-12 mt15 mt-md20 text-center">
					<img src="assets/images/payment.png" class="img-fluid">
				</div>
				<div class="col-12 mt15 mt-md20" align="center">
					<h3 class="f-md-28 f-20 w500 text-center black">Coupon Is Expiring In... </h3>
				</div>
				<!-- Timer -->
				<div class="col-12 text-center">
					<div class="countdown counter-black">
						<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Days</span> </div>
						<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
						<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
						<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
					</div>
				</div>
				<!-- Timer End -->
            </div>
         </div>
	</div>
	  <!-- Step Section End -->
	  <div class="step-section">
      <div class="container ">
         <div class="row ">
            <div class="col-12 f-28 f-md-50 w700 lh140 black-clr text-center">
               All It Takes Is 3 Fail-Proof Steps<br class="d-none d-md-block">
            </div>
            <div class="col-12 f-24 f-md-28 w600 lh140 white-clr text-center mt20">
               <div class="niche-design">
                  Start Your E-Learning Empire Today.
               </div>
            </div>
            <div class="col-12 mt30 mt-md90">
               <div class="row align-items-center ">
                  <div class="col-12 col-md-6">
                     <div class="step-shapes f-20 f-md-22 w600 white-clr">
                           Step 1
                        </div>
                        <div class="f-34 f-md-65 w700 lh140 mt15 caveat">
                           Login
                        </div>
                        <div class="f-20 f-md-24 w400 lh140 mt5 mt-md10">
                           Click On Any Of The Button Below To Get Instant Access To LearnX
                        </div>
                     </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 ">
                     <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 2px solid #F27D66;">
                        <source src="assets/images/step1.mp4" type="video/mp4">
                     </video>
                  </div>
               </div>
            </div>
            <div class="col-12">
               <img src="assets/images/left-arrow.webp" class="img-fluid d-none d-md-block mx-auto " alt="Left Arrow">
            </div>
            <div class="col-12 mt30 mt-md30">
               <div class="row align-items-center ">
                  <div class="col-12 col-md-6 order-md-2">
                     <div class="step-shapes f-20 f-md-22 w600 white-clr">
                        Step 2
                     </div>
                     <div class="f-34 f-md-65 w700 lh140 mt15 caveat">
                        Deploy
                     </div>
                     <div class="f-20 f-md-24 w400 lh140 mt5 mt-md10">
                        Just select any niche you want, and let AI create your Udemy-Like website in 30 seconds or less (prefilled With AI Courses, And With DFY Traffic)
                     </div>
                  </div>
                  <div class="col-md-6 col-md-pull-6 mt20 mt-md0 order-md-1 ">
                     <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 2px solid #F27D66;">
                        <source src="assets/images/step2.mp4" type="video/mp4">
                     </video>
                  </div>
               </div>
            </div>
            <div class="col-12">
               <img src="assets/images/right-arrow.webp" class="img-fluid d-none d-md-block mx-auto" alt="Right Arrow">
            </div>
            <div class="col-12 mt30 mt-md30">
               <div class="row align-items-center ">
                  <div class="col-12 col-md-6">
                     <div class="step-shapes f-20 f-md-22 w600 white-clr">
                        Step 3
                     </div>
                     <div class="f-34 f-md-65 w700 lh140 mt15 caveat">
                        Profit
                     </div>
                     <div class="f-20 f-md-24 w400 lh140 mt5 mt-md10">
                        Sit back and relax. And let AI do the rest of the work. While you enjoy the results
                     </div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 ">
                     <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 2px solid #F27D66;">
                        <source src="https://cdn.oppyotest.com/launches/webgenie/preview/images/step3.mp4" type="video/mp4">
                     </video>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>

	<!-----step section end---------->
	<div class="cta-btn-section dark-cta-bg">
	   <div class="container">
		  <div class="row">
			 <div class="col-md-12 col-md-12 col-12 text-center">
				<div class="f-md-20 f-18 text-center mt3 white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
				<div class="f-md-22 f-22 text-center purple lh120 w700 mt15 mt-md20 orange-clr">TAKE ACTION NOW!</div>
				<div class="f-md-22 f-17 lh120 w600 mt15 mt-md20 white-clr">Use Special Coupon <span class="w800 orange-clr">"PRANSHULX"</span> For Massive <span class="w800 orange-clr">35% Off</span> On Entire Funnel </div>
			 </div>
			 <div class="col-md-12 col-12 text-center mt15 mt-md20">
			 <a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
				<span class="text-center">Grab LearnX + My 10 Exclusive Bonuses</span> <br>
				</a>
			 </div>
			 <div class="col-12 mt15 mt-md20 text-center">
				<img src="assets/images/payment.png" class="img-fluid">
			 </div>
			 <div class="col-12 mt15 mt-md20" align="center">
				<h3 class="f-md-28 f-20 w500 text-center white-clr">Coupon Is Expiring In... </h3>
			 </div>
			 <!-- Timer -->
			 <div class="col-12 text-center">
				<div class="countdown counter-white white-clr">
				   <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Days</span> </div>
				   <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
				   <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
				   <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
				</div>
			 </div>
			 <!-- Timer End -->
		  </div>
	   </div>
	</div>
	<!-----proof section---------->
	<div class="profitwall-sec">
      <div class="container">
	  <div class="row">
            <div class="col-12 text-center">
               <div class="f-28 f-md-45 w700 lh140 black-clr text-center dollars-head">
               LearnX Made It <u>Fail-Proof</u>  To Create A <br class="d-none d-md-block"> Profitable E-Learning Business Today. 
               </div>
               <div class="f-26 f-md-32 lh140 w600 text-center white-clr mt20">
               (Let AI Do All The Work For You)
               </div>
            </div>
         </div>
         <div class="row row-cols-1 row-cols-sm-2 row-cols-md-2 gap30 mt20 mt-md50">
            <div class="col mt20 mt-md50">
               <div class="text-center">
                  <img src="assets/images/proof1.webp" alt="No Coding" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-26 f-md-32 lh140 w600 text-center white-clr mt20 yellow-head">
                     Ai Creates Website For Us
                  </div>
               </div>
            </div>
            <div class="col mt20 mt-md50">
               <div class="text-center">
                  <img src="assets/images/proof2.webp" alt="Ai Gives Us Best Niches" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-26 f-md-32 lh140 w600 text-center white-clr mt20 yellow-head">
                  Ai Gives Us Best Niches
                  </div>
               </div>
            </div>
            <div class="col mt20 mt-md50">
               <div class="text-center">
                  <img src="assets/images/proof3.webp" alt="Ai Create Courses For Us" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-26 f-md-32 lh140 w600 text-center white-clr mt20 yellow-head">
                  Ai Create Courses For Us
                  </div>
               </div>
            </div>
            <div class="col mt20 mt-md50">
               <div class="text-center">
                  <img src="assets/images/proof4.webp" alt="Ai Generate Traffic For Us" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-26 f-md-32 lh140 w600 text-center white-clr mt20 yellow-head">
                  Ai Generate Traffic For Us
                  </div>
               </div>
            </div>
            <div class="col mt20 mt-md50">
               <div class="text-center">
                  <img src="assets/images/proof5.webp" alt="Ai Generate Sales Pages For Us" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-26 f-md-32 lh140 w600 text-center white-clr mt20 yellow-head">
                  Ai Generate Sales Pages For Us
                  </div>
               </div>
            </div>
            <div class="col mt20 mt-md50">
               <div class="text-center">
                  <img src="assets/images/proof6.webp" alt="Ai Generate Chat Bots For Customer Support For Us" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-26 f-md-32 lh140 w600 text-center white-clr mt20 yellow-head">
                  Ai Generate Chat Bots For Customer Support For Us
                  </div>
               </div>
            </div>

            <div class="col mt20 mt-md50">
               <div class="text-center">
                  <img src="assets/images/proof7.webp" alt="No Coding" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-26 f-md-32 lh140 w600 text-center white-clr mt20 red-head">
                  No Coding
                  </div>
               </div>
            </div>
            <div class="col mt20 mt-md50">
               <div class="text-center">
                  <img src="assets/images/proof8.webp" alt="No Designing" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-26 f-md-32 lh140 w600 text-center white-clr mt20 red-head">
                  No Designing
                  </div>
               </div>
            </div>
            <div class="col mt20 mt-md50">
               <div class="text-center">
                  <img src="assets/images/proof9.webp" alt="No Content Writing" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-26 f-md-32 lh140 w600 text-center white-clr mt20 red-head">
                  No Content Writing
                  </div>
               </div>
            </div>
            <div class="col mt20 mt-md50">
               <div class="text-center">
                  <img src="assets/images/proof10.webp" alt="No Optimizing" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-26 f-md-32 lh140 w600 text-center white-clr mt20 red-head">
                  No Optimizing
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
    
	<!---proof section end---->
	  
<!-- Video Testimonial -->
	<!-- CTA Button Section Start -->
	<div class="cta-btn-section dark-cta-bg">
	   <div class="container">
		  <div class="row">
			 <div class="col-md-12 col-md-12 col-12 text-center">
				<div class="f-md-20 f-18 text-center mt3 white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
				<div class="f-md-22 f-22 text-center purple lh120 w700 mt15 mt-md20 orange-clr">TAKE ACTION NOW!</div>
				<div class="f-md-22 f-17 lh120 w600 mt15 mt-md20 white-clr">Use Special Coupon <span class="w800 orange-clr">"PRANSHULX"</span> For Massive <span class="w800 orange-clr">35% Off</span> On Entire Funnel </div>
			 </div>
			 <div class="col-md-12 col-12 text-center mt15 mt-md20">
			 <a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
				<span class="text-center">Grab LearnX + My 10 Exclusive Bonuses</span> <br>
				</a>
			 </div>
			 <div class="col-12 mt15 mt-md20 text-center">
				<img src="assets/images/payment.png" class="img-fluid">
			 </div>
			 <div class="col-12 mt15 mt-md20" align="center">
				<h3 class="f-md-28 f-20 w500 text-center white-clr">Coupon Is Expiring In... </h3>
			 </div>
			 <!-- Timer -->
			 <div class="col-12 text-center">
				<div class="countdown counter-white white-clr">
				   <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Days</span> </div>
				   <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
				   <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
				   <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
				</div>
			 </div>
			 <!-- Timer End -->
		  </div>
	   </div>
	</div>
	<!-- CTA Button Section End -->
	
	<div class="proudly-section" id="product">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="prdly-pres f-28 f-md-38 w700 lh140 text-center white-clr">
                  Introducing
                  </div>
               </div>
               <div class="col-12 mt-md50 mt20 text-center">
			   <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                            viewBox="0 0 484.58 200.84" style="max-height:167px;">
                            <defs>
                                <style>
                                    .cls-1,
                                    .cls-4 {
                                        fill: #fff;
                                    }

                                    .cls-2,
                                    .cls-3,
                                    .cls-4 {
                                        fill-rule: evenodd;
                                    }

                                    .cls-2 {
                                        fill: url(#linear-gradient);
                                    }

                                    .cls-3 {
                                        fill: url(#linear-gradient-2);
                                    }

                                    .cls-5 {
                                        fill: url(#linear-gradient-3);
                                    }
                                </style>
                                <linearGradient id="linear-gradient" x1="-154.67" y1="191.86" x2="249.18" y2="191.86"
                                    gradientTransform="matrix(1.19, -0.01, 0.05, 1, 178.46, -4.31)"
                                    gradientUnits="userSpaceOnUse">
                                    <stop offset="0" stop-color="#ead40c"></stop>
                                    <stop offset="1" stop-color="#ff00e9"></stop>
                                </linearGradient>
                                <linearGradient id="linear-gradient-2" x1="23.11" y1="160.56" x2="57.61" y2="160.56"
                                    gradientTransform="matrix(1, 0, 0, 1, 0, 0)" xlink:href="#linear-gradient">
                                </linearGradient>
                                <linearGradient id="linear-gradient-3" x1="384.49" y1="98.33" x2="483.38" y2="98.33"
                                    gradientTransform="matrix(1, 0, 0, 1, 0, 0)" xlink:href="#linear-gradient">
                                </linearGradient>
                            </defs>
                            <g id="Layer_2" data-name="Layer 2">
                                <g id="Layer_1-2" data-name="Layer 1">
                                    <path class="cls-1"
                                        d="M112.31,154.3q-11.65,0-20.1-4.87a32.56,32.56,0,0,1-13-13.82q-4.53-9-4.54-21.11a46.06,46.06,0,0,1,4.57-21A34.29,34.29,0,0,1,92,79.37a36.11,36.11,0,0,1,19.31-5.06,39.65,39.65,0,0,1,13.54,2.29,31,31,0,0,1,11.3,7.09,33.32,33.32,0,0,1,7.75,12.18,49.23,49.23,0,0,1,2.82,17.57V119H83.26v-12.3h46A19.87,19.87,0,0,0,127,97.38a16.6,16.6,0,0,0-6.18-6.48,17.59,17.59,0,0,0-9.21-2.37,17.88,17.88,0,0,0-9.83,2.7,18.81,18.81,0,0,0-6.58,7.06,20.17,20.17,0,0,0-2.4,9.56v10.74a25.15,25.15,0,0,0,2.47,11.57,17.42,17.42,0,0,0,6.91,7.37,20.52,20.52,0,0,0,10.39,2.55,21.62,21.62,0,0,0,7.22-1.14,15.71,15.71,0,0,0,5.59-3.35,14.11,14.11,0,0,0,3.59-5.5L146,132a26.44,26.44,0,0,1-6.13,11.77,29.72,29.72,0,0,1-11.52,7.77A43.74,43.74,0,0,1,112.31,154.3Z">
                                    </path>
                                    <path class="cls-1"
                                        d="M184.49,154.35a31.78,31.78,0,0,1-13.24-2.65,21.26,21.26,0,0,1-9.28-7.84,22.86,22.86,0,0,1-3.41-12.81A21.92,21.92,0,0,1,161,120.2a18.9,18.9,0,0,1,6.61-6.86,33.85,33.85,0,0,1,9.46-3.9,77.76,77.76,0,0,1,10.92-2q6.81-.7,11-1.28a15.91,15.91,0,0,0,6.18-1.82,4.25,4.25,0,0,0,1.94-3.86v-.3q0-5.7-3.38-8.83T194,88.28q-6.71,0-10.62,2.92a14.55,14.55,0,0,0-5.27,6.91l-17-2.42a27.51,27.51,0,0,1,6.66-11.83,29.46,29.46,0,0,1,11.35-7.16,43.73,43.73,0,0,1,14.83-2.39,47.89,47.89,0,0,1,11.14,1.31,31.6,31.6,0,0,1,10.14,4.31,22.12,22.12,0,0,1,7.39,8.14q2.81,5.15,2.8,12.87v51.85H207.84V142.14h-.61a22.1,22.1,0,0,1-4.66,6,22.35,22.35,0,0,1-7.52,4.49A30,30,0,0,1,184.49,154.35Zm4.74-13.42a19.7,19.7,0,0,0,9.53-2.19,16,16,0,0,0,6.23-5.83,15,15,0,0,0,2.19-7.91v-9.13a8.72,8.72,0,0,1-2.9,1.31,45.56,45.56,0,0,1-4.56,1.06c-1.68.3-3.35.57-5,.8l-4.29.61a32,32,0,0,0-7.32,1.81A12.29,12.29,0,0,0,178,125a9.76,9.76,0,0,0,1.82,13.39A16,16,0,0,0,189.23,140.93Z">
                                    </path>
                                    <path class="cls-1"
                                        d="M243.8,152.79V75.31h17.7V88.23h.81a19.36,19.36,0,0,1,7.29-10.37,20,20,0,0,1,11.83-3.66c1,0,2.14,0,3.4.13a23.83,23.83,0,0,1,3.15.38V91.5a21,21,0,0,0-3.65-.73,38.61,38.61,0,0,0-4.82-.32,18.47,18.47,0,0,0-8.95,2.14,15.94,15.94,0,0,0-6.23,5.93,16.55,16.55,0,0,0-2.27,8.72v45.55Z">
                                    </path>
                                    <path class="cls-1"
                                        d="M318.4,107.39v45.4H300.14V75.31h17.45V88.48h.91a22,22,0,0,1,8.55-10.34q5.86-3.84,14.55-3.83a27.58,27.58,0,0,1,14,3.43,23.19,23.19,0,0,1,9.28,9.93,34.22,34.22,0,0,1,3.26,15.79v49.33H349.87V106.28c0-5.17-1.34-9.23-4-12.15s-6.37-4.39-11.07-4.39a17,17,0,0,0-8.5,2.09,14.67,14.67,0,0,0-5.8,6A20,20,0,0,0,318.4,107.39Z">
                                    </path>
                                    <path class="cls-2"
                                        d="M314.05,172.88c12.69-.14,27.32,2.53,40.82,2.61l-.09,1c1.07.15,1-.88,2-.78,1.42-.18,0,1.57,1.82,1.14.54-.06.08-.56-.32-.68.77,0,2.31.17,3,1.26,1.3,0-.9-1.34.85-.89,6,1.63,13.39,0,20,3.2,11.64.72,24.79,2.38,38,4.27,2.85.41,5.37,1.11,7.57,1.05,2-.05,5.68.78,8,1.08,2.53.34,5.16,1.56,7.13,1.65-1.18-.05,5.49-.78,4.54.76,5-.72,10.9,1.67,15.94,1.84.83-.07.69.53.67,1,1.23-1.17,3.32.56,4.28-.56.25,2,2.63-.48,3.3,1.61,2.23-1.39,4.83.74,7.55,1.37,1.91.44,5.29-.21,5.55,2.14-2.1-.13-5.12.81-11.41-1.09,1,.87-3.35.74-3.39-.64-1.81,1.94-6.28-1-7.79,1.19-1.26-.41,0-.72-.64-1.35-5,.55-8.09-1.33-12.91-1.56,2.57,2.44,6.08,3.16,10.06,3.22,1.28.15,0,.74,1,1.39,1.37-.19.75-.48,1.26-1.17,5.37,2.28,15.36,2.35,21,4.91-4.69-.63-11.38,0-15.15-2.09A148,148,0,0,1,441.58,196c-.53-.1-1.81.12-.7-.71-1.65.73-3.41.1-5.74-.22-37.15-5.15-80.47-7.78-115.75-9.76-39.54-2.22-76.79-3.22-116.44-2.72-61.62.78-119.59,7.93-175.21,13.95-5,.55-10,1.49-15.11,1.47-1.33-.32-2.46-1.91-1.25-3-2-.38-2.93.29-5-.15a9.83,9.83,0,0,1-1.25-3,13.73,13.73,0,0,1,6.42-2.94c.77-.54.12-.8.15-1.6a171,171,0,0,1,45.35-8.58c26.43-1.1,53.68-4.73,79.64-6,6.25-.3,11.72.06,18.41.14,8.31.1,19.09-1,29.19-.12,6.25.54,13.67-.55,21.16-.56,39.35-.09,71.16-.23,112.28,2C317.24,171.93,315.11,173.81,314.05,172.88Zm64.22,16.05-1.6-.2C376.2,190,378.42,190.26,378.27,188.93Z">
                                    </path>
                                    <polygon class="cls-3"
                                        points="52.57 166.06 57.61 151.99 23.11 157.23 32.36 169.12 52.57 166.06">
                                    </polygon>
                                    <polygon class="cls-4" points="44.69 183.24 51.75 168.35 33.86 171.06 44.69 183.24">
                                    </polygon>
                                    <path class="cls-4"
                                        d="M.12,10.15l22,145.06,35.47-5.39-22-145a2.6,2.6,0,0,0,0-.4C35,.76,26.62-.95,16.81.54S-.52,6.16,0,9.77A2.62,2.62,0,0,0,.12,10.15Z">
                                    </path>
                                    <path class="cls-5"
                                        d="M433.68,79.45l21-36.33h27.56L448.48,97.51l34.9,56H456l-22-37.76-21.94,37.76H384.49l34.9-56L385.72,43.12h27.35Z">
                                    </path>
                                </g>
                            </g>
                        </svg>
               </div>
               <div class="text-center mt20 mt-md50">
                  <div class="pre-heading1 f-md-22 f-20 w600 lh140 white-clr">
                  Exploit The Emerging E-Learning Business With A Click…  
                  </div>
               </div>
               <div class="f-md-50 f-28 w700 white-clr lh140 mt-md30 mt20 text-center">
                  First To Market AI System <span class="yellow-clr"> Creates Self-Managed Academy Sites, And Prefill It With Tons of Unique AI-Generated Courses</span>
               </div>
               <div class="f-22 f-md-28 lh140 w400 text-center white-clr mt20 mt-md30">
			   Finally, Let AI Build You A Passive Income Stream Without Doing Any Work
               </div>
            </div>
            <div class="row d-flex flex-wrap align-items-center mt20 mt-md50">
               <div class="col-12 col-md-10 mx-auto">
                  <img src="assets/images/productbox.webp" class="img-fluid d-block mx-auto margin-bottom-15">
               </div>
            </div>
         </div>
      </div>
  	<!-- Bonus Section Header Start -->
	<div class="bonus-header">
		<div class="container">
			<div class="row">
				<div class="col-12 col-md-10 mx-auto heading-bg text-center">
					<div class="f-24 f-md-36 lh160 w700">When You Purchase LearnX, You Also Get <br class="d-lg-block d-none"> Instant Access To These 10 Exclusive Bonuses</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Bonus Section Header End -->

	<!-- Bonus #1 Section Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
			 <div class="col-12 text-center">
			 </div>
			 <div class="col-12 mt20 mt-md30">
				<div class="row d-flex align-items-center flex-wrap">
				   <div class="col-12 col-md-5 order-3 order-md-0 text-center mt20 mt-md0">
					  <img src="assets/images/bonus1.webp" class="img-fluid">
				   </div>
				   <div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				  <div class="text-md-start text-center">
				  <div class="bonus-title-bg mb-4">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 1</div>
				</div>
				  </div>
					  <div class="f-22 f-md-34 w600 lh160 bonus-title-color mt20 mt-md30">
					  Course Engagement Hacks
					  </div>
					  <div class="f-18 f-md-22 w400 lh140 bonus-title-color mt20">
					  Supercharge your learning with our Course Engagement Hacks Ebook. Unlock proven strategies and techniques to boost participation, understanding, and retention. Perfectly complement LearnX for an enriched educational journey. Elevate your learning experience today!
					  </div>
					  <!-- <ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
						<li>Finding the right niche for you is crucial to your success!</li>
						<li>If you want to make money online, there are many techniques to do it. But the thing is that, on every technique to apply, market saturation is always an issue.</li>
						<li>The good news is that, as time passed by internet marketers and online business owner a way to at least walk a different path to avoid this huge competition and dominate the market.</li>
					  </ul> -->
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #1 Section End -->

	<!-- Bonus #2 Section Start -->
	<div class="section-bonus">
	   <div class="container">
		
			 <div class="col-12 mt20 mt-md30">
				<div class="row align-items-center flex-wrap">
				   <div class="col-12 col-md-5 order-3 order-md-3 text-center mt20 mt-md0">
					  <img src="assets/images/bonus2.webp" class="img-fluid">
				   </div>
				   <div class="col-12 col-md-7 mt-md0 mt20">
				   <div class=" text-md-start text-center">
				<div class="bonus-title-bg mb-4">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 2</div>
				</div></div>
			
					  <div class="f-22 f-md-34 w600 lh160 bonus-title-color mt20 mt-md30">
					  20 Online Business Ideas
					  </div>
					  <div class="f-18 f-md-22 w400 lh140 bonus-title-color mt20">
					  Explore 20 innovative online business ideas that perfectly complement LearnX. From tutoring platforms to niche skill courses, this bonus guide offers diverse opportunities to monetize your expertise and thrive in the digital landscape.
					  </div>
					  <!-- <ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
					  	<li>Discover how to get the most out of Google Plus and gain exposure for your brand! </li> 
						<li>Debuting as a social networking element the Google plus site is something that was launched to rival the Facebook popularity. </li>
						<li>Launched only recently it is a fairly new introduction to the internet world and should be explored for its potential to providing a good platform for internet marketing.</li>
					</ul> -->
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #2 Section End -->

	<!-- Bonus #3 Section Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row ">
		
			 <div class="col-12 mt20 mt-md30">
				<div class="row align-items-center flex-wrap">
				   <div class="col-12 col-md-5 order-3 order-md-0 text-center mt20 mt-md0">
					  <img src="assets/images/bonus3.webp" class="img-fluid">
				   </div>
				   <div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				   <div class="text-center text-md-start">
				<div class="bonus-title-bg mb-4">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 3</div>
				</div>
			 </div>
					  <div class="f-22 f-md-34 w600 lh160 bonus-title-color mt20 mt-md30">
					  Course Ninja
					  </div>
					  <div class="f-18 f-md-22 w400 lh140 bonus-title-color mt20">
					  Elevate your mastery with Course Ninja, the perfect complement to LearnX. Unleash advanced techniques, ninja-level strategies, and exclusive insights. Sharpen your skills, dominate your goals, and become a learning legend
					  </div>
					  <!-- <ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
					  	<li>These 220 success principles will shape your business and your life in a positive way. You can use these principles in blog posts, articles, videos, as bonuses, or even as a stand alone product that you sell! </li>
						<li>When it comes to life and business, it is no coincidence that some people always seem to fail while others always seem to flourish. For sure, chance plays a role in everything. But as individuals, as business-owners, as thinkers, and as parents, we have a significant degree of control over our lives. </li>
						<li>Now, we can use the control that we have to influence outcomes in bad ways. Or we can use it to influence outcomes in our favor; and in the favor of those we care about most.</li>
					  </ul> -->
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #3 Section End -->

	<!-- Bonus #4 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row ">
		
			 <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
				<div class="row align-items-center flex-wrap">
				   <div class="col-12 col-md-5 order-3 order-md-3 text-center mt20 mt-md0">
					  <img src="assets/images/bonus4.webp" class="img-fluid">
				   </div>
				   <div class="col-md-7 col-12 mt20 mt-md0 ">
				   <div class="text-md-start text-center">
				<div class="bonus-title-bg mb-4">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 4</div>
				</div>
			 </div>
					  <div class="f-22 f-md-34 w600 lh160 bonus-title-color mt20 mt-md30">
					  Killer Headlines
					  </div>
					  <div class="f-18 f-md-22 w400 lh140 bonus-title-color mt20">
					  Supercharge your LearnX experience with Bonus Killer Headlines! Elevate your academy's appeal and engagement effortlessly. Unleash the power of compelling headlines to captivate learners and boost your educational content's impact. Upgrade now!
					  </div>
					  <!-- <ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
					  <li>Membership sites have become a popular way for online business owners to generate substantial recurring income with as little as ten hours of work a week.</li>
					  <li>Creating a membership site for your business is an excellent way for you to create recurring revenue, build a large following of loyal customers, and become an authority in your industry. </li>
					  <li>One of the most critical benefits that you gain from starting a membership site is the opportunity it can provide you to establish your reputation and your brand within your industry. </li>
					</ul> -->
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #4 End -->

	<!-- Bonus #5 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row ">
		
			 <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
				<div class="row align-items-center flex-wrap">
				   <div class="col-12 col-md-5 order-3 order-md-0 text-center mt20 mt-md0 ">
					  <img src="assets/images/bonus5.webp" class="img-fluid">
				   </div>
				   <div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				   <div class="text-md-start text-center">
				<div class="bonus-title-bg mb-4">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 5</div>
				</div>
			 </div>
					  <div class="f-22 f-md-34 w600 lh160 bonus-title-color mt20 mt-md30">
					  100 Ecourse Publishing Tips 
					  </div>
					  <div class="f-18 f-md-22 w400 lh140 bonus-title-color mt20">
					  Unlock the full potential of LearnX with '100 Ecourse Publishing Tips.' Elevate your online courses with strategic insights, practical advice, and proven techniques for effective e-course creation and publishing. Maximize impact and engagement effortlessly.
					  </div>
					  <!-- <ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
					  	<li>Uncover the powerful tools that will eliminate guesswork and virtually guarantee success!</li>
						<li>Find out how savvy email marketers use free resources to automate their online income!</li>
						<li>How to instantly save time and money by following a powerful system proven to work!</li>
						<li>What you need to know about choosing an autoresponder provider, hosting and more!</li>
					  </ul> -->
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #5 End -->

	<!-- CTA Button Section Start -->
	<div class="cta-btn-section dark-cta-bg">
	   <div class="container">
		  <div class="row">
			 <div class="col-md-12 col-md-12 col-12 text-center">
				<div class="f-md-20 f-18 text-center mt3 white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
				<div class="f-md-22 f-22 text-center purple lh120 w700 mt15 mt-md20 orange-clr">TAKE ACTION NOW!</div>
				<div class="f-md-22 f-17 lh120 w600 mt15 mt-md20 white-clr">Use Special Coupon <span class="w800 orange-clr">"PRANSHULX"</span> For Massive <span class="w800 orange-clr">35% Off</span> On Entire Funnel </div>
			 </div>
			 <div class="col-md-12 col-12 text-center mt15 mt-md20">
				<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
				<span class="text-center">Grab LearnX + My 10 Exclusive Bonuses</span> <br>
				</a>
			 </div>
			 <div class="col-12 mt15 mt-md20 text-center">
				<img src="assets/images/payment.png" class="img-fluid">
			 </div>
			 <div class="col-12 mt15 mt-md20" align="center">
				<h3 class="f-md-28 f-20 w500 text-center white-clr">Coupon Is Expiring In... </h3>
			 </div>
			 <!-- Timer -->
			 <div class="col-12 text-center">
				<div class="countdown counter-white white-clr">
				   <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Days</span> </div>
				   <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
				   <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
				   <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
				</div>
			 </div>
			 <!-- Timer End -->
		  </div>
	   </div>
	</div>
	<!-- CTA Button Section End -->

	<!-- Bonus #6 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
		
			 </div>
			 <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
				<div class="row align-items-center flex-wrap">
				   <div class="col-12 col-md-5 order-3 order-md-3 text-center mt20 mt-md0">
					  <img src="assets/images/bonus6.webp" class="img-fluid">
				   </div>
				   <div class="col-md-7 col-12 mt20 mt-md0">
				   <div class="text-center text-md-start">
				<div class="bonus-title-bg mb-4">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 6</div>
				</div>
					  <div class="f-22 f-md-34 w600 lh160 bonus-title-color mt20 mt-md30">
					  Animated GIFs
					  </div>
					  <div class="f-18 f-md-22 w400 lh140 bonus-title-color mt20">
					  Elevate your LearnX with dynamic Animated GIFs! Enhance engagement and understanding as these captivating visuals complement LearnX , making learning an immersive and enjoyable experience
					  </div>
					  <!-- <ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
					  	<li>Web marketing does not have to be tedious and expensive! Attract targeted traffic to your site! All you need is to know the secrets of powerful link exchange. Learn some quick link exchanging tactics to earn high ROIs at low investment!</li>
						<li> Developing a standalone website does not make much sense these days. With the increasing number of websites worldwide, individual websites tend to suffer from decreasing traffic. Webmasters have tested so many different techniques to attract visitors to their websites and gradually, these techniques have emerged into strategies that people use in order to generate traffic on a regular basis.  </li>
					  </ul> -->
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #6 End -->

	<!-- Bonus #7 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
		
			 <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
				<div class="row align-items-center flex-wrap">
				   <div class="col-12 col-md-5 order-3 order-md-0 text-center mt20 mt-md0">
					  <img src="assets/images/bonus7.webp" class="img-fluid">
				   </div>
				   <div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				   <div class="text-md-start text-center">
				<div class="bonus-title-bg mb-4">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 7</div>
				</div>
			 </div>
					  <div class="f-22 f-md-34 w600 lh160 bonus-title-color mt20 mt-md30">
					  Creating Digital Products
					  </div>
					  <div class="f-18 f-md-22 w400 lh140 bonus-title-color mt20">
					  Create engaging digital products effortlessly to complement LearnX, offering a seamless learning experience. Elevate your content, enhance interactivity, and captivate learners with innovative digital solutions..
					  </div>
					  <!-- <ul class="bonus-list f-18 f-md-20 lh160 w400">
					  	<li>If you're looking to make some fast cash, or you're interested in building a long-term sustainable business, consulting is one of the most lucrative opportunities available online.</li>
						<li>The dictionary defines consultant as: “a person who provides expert advice professionally.”  </li>
						<li>As a consultant or coach, you'll be responsible for guiding your students or clients through a learning curve until they've accomplished a specific goal. </li>
					</ul> -->
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #7 End -->

	<!-- Bonus #8 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
		
			 <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
				<div class="row align-items-center flex-wrap">
				   <div class="col-12 col-md-5 order-3 order-md-3 text-center mt20 mt-md0">
					  <img src="assets/images/bonus8.webp" class="img-fluid">
				   </div>
				   <div class="col-md-7 col-12 mt20 mt-md0">
				   <div class="text-center text-md-start">
				<div class="bonus-title-bg mb-4">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 8</div>
				</div>
			 </div>
					  <div class="f-22 f-md-34 w600 lh160 bonus-title-color mt20 mt-md30">
					  ChatGPT Blueprint
					  </div>
					  <div class="f-18 f-md-22 w400 lh140 bonus-title-color mt20">
					  The perfect Bonus to complement the LearnX to Enhance engagement, foster dynamic interactions, and personalize learning journeys using advanced language models for an unparalleled educational synergy.
					  </div>
					  <!-- <ul class="bonus-list f-18 f-md-20 lh160 w400">
					  	<li>Discover 20 Online Business Ideas You Can Start Today So You Can Have The Freedom To Work Anywhere!</li>
						<li>Without an idea, there is no chance to start your own online business. With tons of entrepreneurs out there this first step is one of the hardest challenges, where you might find yourself wandering around the web to get your creative juices flowing. </li>
						<li>Don't look any further, as we put together a list of 20 online business ideas which you can start tomorrow. Obviously, there is a need for tremendous preparation and research, but hopefully, inside this list can get you started.</li>
					  </ul> -->
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #8 End -->

	<!-- Bonus #9 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
		
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row align-items-center flex-wrap">
				   		<div class="col-12 col-md-5 order-3 order-md-0 text-center mt20 mt-md0">
					  		<img src="assets/images/bonus9.webp" class="img-fluid">
				   		</div>
				   		<div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				   			<div class="text-md-start text-center">
								<div class="bonus-title-bg mb-4">
				   					<div class="f-22 f-md-28 lh120 w700">Bonus 9</div>
								</div>
			 				</div>
					  		<div class="f-22 f-md-34 w600 lh160 bonus-title-color  mt20 mt-md30">					 
							  Launch your online course 
					  		</div>
							<div class="f-18 f-md-22 w400 lh140 bonus-title-color mt20">
								Launch your online course seamlessly, complementing LearnX's user-friendly interface and powerful features. Empower learners and build a thriving online learning community effortlessly.
							</div>
					  		<!-- <ul class="bonus-list f-18 f-md-20 lh160 w400">
					  			<li>In the main report you will learn what digital products are and why it's a great business model. You will learn how to create a digital product that people need and want.</li>
								<li>3 Options For Selling Your Digital Products. Defining The Target Audience For Your Info Product. How To Come Up With Info Product Ideas That Sell. How To Launch A Digital Product. </li>
							</ul> -->
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #9 End -->

	<!-- Bonus #10 start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
		
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row align-items-center flex-wrap">
				   		<div class="col-12 col-md-5 order-3 order-md-3 text-center mt20 mt-md0">
					  		<img src="assets/images/bonus10.webp" class="img-fluid">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
				   			<div class="text-md-start text-center">
								<div class="bonus-title-bg mb-4">
				   					<div class="f-22 f-md-28 lh120 w700">Bonus 10</div>
								</div>
			 				</div>
					  		<div class="f-22 f-md-34 w600 lh160 bonus-title-color mt20 mt-md30">
							  Passive Cash Profits
					  		</div>
							  <div class="f-18 f-md-22 w400 lh140 bonus-title-color mt20">
							  Unlock financial success effortlessly with Passive Cash Profits, the perfect companion to LearnX. Seamlessly integrate wealth-building strategies into your learning platform and empower users to earn while they learn
							</div>
					  		<!-- <ul class="bonus-list f-18 f-md-20 lh160 w400">
							  	<li>Learn How To Write Attention Grabbing Headlines!</li>
								<li>If you have an online business, and even if you have a brick and mortar store with an online presence, you should be marketing to your customers and prospects through email. It is one of the most effective ways to increase sales and profits.  </li>
								<li>You have to earn the click that opens the email and you earn it with an interesting email subject line. !Think about that each time you sit down to write an email to your subscribers.</li>
							</ul> -->
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #10 End -->

	<!-- Huge Woth Section Start -->
	<div class="huge-area mt30 mt-md10">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-12 text-center">
                    <div class="f-md-60 f-30 lh120 w700 white-clr">That's Huge Worth of</div>
                    <br>
                    <div class="f-md-60 f-30 lh120 w800 yellow-clr">$2465!</div>
                </div>
            </div>
        </div>
    </div>
	<!-- Huge Worth Section End -->

	<!-- text Area Start -->
	<div class="white-section pb-0">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-12 text-center p0">
                	<div class="f-md-40 f-25 lh160 w400">So what are you waiting for? You have a great opportunity <br class="d-lg-block d-none"> ahead + <span class="w600">My 10 Bonus Products</span> are making it a <br class="d-lg-block d-none"> <b>completely NO Brainer!!</b></div>
            	</div>
			</div>
			<div class="col-md-12 col-md-12 col-12 text-center mt20 mt-md50">
					<div class="f-md-26 f-18 text-center black lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
					<div class="f-md-24 f-22 text-center purple lh120 w700 mt15 mt-md20 lite-black-clr">TAKE ACTION NOW!</div>
					<div class="f-md-22 f-17 lh120 w600 mt15 mt-md20 black-clr">Use Special Coupon <span class="w800 lite-black-clr">"PRANSHULX"</span> For Massive <span class="w800 lite-black-clr">35% Off</span> On Entire Funnel </div>
				</div>
			<div class="row">
			 <div class="col-md-12 col-12 text-center mt20 mt-md30">
				<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
				<span class="text-center">Grab LearnX + My 10 Exclusive Bonuses</span> <br>
				</a>
			 </div>
			 
			 <div class="col-12 mt15 mt-md20" align="center">
				<h3 class="f-md-30 f-20 w500 text-center black">My Exclusive Bonuses Are Expiring in...</h3>
			 </div>
			 <!-- Timer -->
			 <div class="col-12 text-center">
				<div class="countdown counter-black">
				   <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
				   <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
				   <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
				   <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
				</div>
			 </div>
			 <!-- Timer End -->
		  </div>
		</div>
	</div>
	<!-- text Area End -->

	<!-- CTA Button Section Start -->
	<!-- <div class="cta-btn-section pt-3 ">
	   <div class="container">
		  
	   </div>
	</div> -->
	<!-- CTA Button Section End -->

	
	  <!--Footer Section Start -->
    <div class="footer-section">
        <div class="container ">
            <div class="row">
                <div class="col-12 text-center">
				<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                            viewBox="0 0 484.58 200.84" style="max-height:75px;">
                            <defs>
                                <style>
                                    .cls-1,
                                    .cls-4 {
                                        fill: #fff;
                                    }

                                    .cls-2,
                                    .cls-3,
                                    .cls-4 {
                                        fill-rule: evenodd;
                                    }

                                    .cls-2 {
                                        fill: url(#linear-gradient);
                                    }

                                    .cls-3 {
                                        fill: url(#linear-gradient-2);
                                    }

                                    .cls-5 {
                                        fill: url(#linear-gradient-3);
                                    }
                                </style>
                                <linearGradient id="linear-gradient" x1="-154.67" y1="191.86" x2="249.18" y2="191.86"
                                    gradientTransform="matrix(1.19, -0.01, 0.05, 1, 178.46, -4.31)"
                                    gradientUnits="userSpaceOnUse">
                                    <stop offset="0" stop-color="#ead40c"></stop>
                                    <stop offset="1" stop-color="#ff00e9"></stop>
                                </linearGradient>
                                <linearGradient id="linear-gradient-2" x1="23.11" y1="160.56" x2="57.61" y2="160.56"
                                    gradientTransform="matrix(1, 0, 0, 1, 0, 0)" xlink:href="#linear-gradient">
                                </linearGradient>
                                <linearGradient id="linear-gradient-3" x1="384.49" y1="98.33" x2="483.38" y2="98.33"
                                    gradientTransform="matrix(1, 0, 0, 1, 0, 0)" xlink:href="#linear-gradient">
                                </linearGradient>
                            </defs>
                            <g id="Layer_2" data-name="Layer 2">
                                <g id="Layer_1-2" data-name="Layer 1">
                                    <path class="cls-1"
                                        d="M112.31,154.3q-11.65,0-20.1-4.87a32.56,32.56,0,0,1-13-13.82q-4.53-9-4.54-21.11a46.06,46.06,0,0,1,4.57-21A34.29,34.29,0,0,1,92,79.37a36.11,36.11,0,0,1,19.31-5.06,39.65,39.65,0,0,1,13.54,2.29,31,31,0,0,1,11.3,7.09,33.32,33.32,0,0,1,7.75,12.18,49.23,49.23,0,0,1,2.82,17.57V119H83.26v-12.3h46A19.87,19.87,0,0,0,127,97.38a16.6,16.6,0,0,0-6.18-6.48,17.59,17.59,0,0,0-9.21-2.37,17.88,17.88,0,0,0-9.83,2.7,18.81,18.81,0,0,0-6.58,7.06,20.17,20.17,0,0,0-2.4,9.56v10.74a25.15,25.15,0,0,0,2.47,11.57,17.42,17.42,0,0,0,6.91,7.37,20.52,20.52,0,0,0,10.39,2.55,21.62,21.62,0,0,0,7.22-1.14,15.71,15.71,0,0,0,5.59-3.35,14.11,14.11,0,0,0,3.59-5.5L146,132a26.44,26.44,0,0,1-6.13,11.77,29.72,29.72,0,0,1-11.52,7.77A43.74,43.74,0,0,1,112.31,154.3Z">
                                    </path>
                                    <path class="cls-1"
                                        d="M184.49,154.35a31.78,31.78,0,0,1-13.24-2.65,21.26,21.26,0,0,1-9.28-7.84,22.86,22.86,0,0,1-3.41-12.81A21.92,21.92,0,0,1,161,120.2a18.9,18.9,0,0,1,6.61-6.86,33.85,33.85,0,0,1,9.46-3.9,77.76,77.76,0,0,1,10.92-2q6.81-.7,11-1.28a15.91,15.91,0,0,0,6.18-1.82,4.25,4.25,0,0,0,1.94-3.86v-.3q0-5.7-3.38-8.83T194,88.28q-6.71,0-10.62,2.92a14.55,14.55,0,0,0-5.27,6.91l-17-2.42a27.51,27.51,0,0,1,6.66-11.83,29.46,29.46,0,0,1,11.35-7.16,43.73,43.73,0,0,1,14.83-2.39,47.89,47.89,0,0,1,11.14,1.31,31.6,31.6,0,0,1,10.14,4.31,22.12,22.12,0,0,1,7.39,8.14q2.81,5.15,2.8,12.87v51.85H207.84V142.14h-.61a22.1,22.1,0,0,1-4.66,6,22.35,22.35,0,0,1-7.52,4.49A30,30,0,0,1,184.49,154.35Zm4.74-13.42a19.7,19.7,0,0,0,9.53-2.19,16,16,0,0,0,6.23-5.83,15,15,0,0,0,2.19-7.91v-9.13a8.72,8.72,0,0,1-2.9,1.31,45.56,45.56,0,0,1-4.56,1.06c-1.68.3-3.35.57-5,.8l-4.29.61a32,32,0,0,0-7.32,1.81A12.29,12.29,0,0,0,178,125a9.76,9.76,0,0,0,1.82,13.39A16,16,0,0,0,189.23,140.93Z">
                                    </path>
                                    <path class="cls-1"
                                        d="M243.8,152.79V75.31h17.7V88.23h.81a19.36,19.36,0,0,1,7.29-10.37,20,20,0,0,1,11.83-3.66c1,0,2.14,0,3.4.13a23.83,23.83,0,0,1,3.15.38V91.5a21,21,0,0,0-3.65-.73,38.61,38.61,0,0,0-4.82-.32,18.47,18.47,0,0,0-8.95,2.14,15.94,15.94,0,0,0-6.23,5.93,16.55,16.55,0,0,0-2.27,8.72v45.55Z">
                                    </path>
                                    <path class="cls-1"
                                        d="M318.4,107.39v45.4H300.14V75.31h17.45V88.48h.91a22,22,0,0,1,8.55-10.34q5.86-3.84,14.55-3.83a27.58,27.58,0,0,1,14,3.43,23.19,23.19,0,0,1,9.28,9.93,34.22,34.22,0,0,1,3.26,15.79v49.33H349.87V106.28c0-5.17-1.34-9.23-4-12.15s-6.37-4.39-11.07-4.39a17,17,0,0,0-8.5,2.09,14.67,14.67,0,0,0-5.8,6A20,20,0,0,0,318.4,107.39Z">
                                    </path>
                                    <path class="cls-2"
                                        d="M314.05,172.88c12.69-.14,27.32,2.53,40.82,2.61l-.09,1c1.07.15,1-.88,2-.78,1.42-.18,0,1.57,1.82,1.14.54-.06.08-.56-.32-.68.77,0,2.31.17,3,1.26,1.3,0-.9-1.34.85-.89,6,1.63,13.39,0,20,3.2,11.64.72,24.79,2.38,38,4.27,2.85.41,5.37,1.11,7.57,1.05,2-.05,5.68.78,8,1.08,2.53.34,5.16,1.56,7.13,1.65-1.18-.05,5.49-.78,4.54.76,5-.72,10.9,1.67,15.94,1.84.83-.07.69.53.67,1,1.23-1.17,3.32.56,4.28-.56.25,2,2.63-.48,3.3,1.61,2.23-1.39,4.83.74,7.55,1.37,1.91.44,5.29-.21,5.55,2.14-2.1-.13-5.12.81-11.41-1.09,1,.87-3.35.74-3.39-.64-1.81,1.94-6.28-1-7.79,1.19-1.26-.41,0-.72-.64-1.35-5,.55-8.09-1.33-12.91-1.56,2.57,2.44,6.08,3.16,10.06,3.22,1.28.15,0,.74,1,1.39,1.37-.19.75-.48,1.26-1.17,5.37,2.28,15.36,2.35,21,4.91-4.69-.63-11.38,0-15.15-2.09A148,148,0,0,1,441.58,196c-.53-.1-1.81.12-.7-.71-1.65.73-3.41.1-5.74-.22-37.15-5.15-80.47-7.78-115.75-9.76-39.54-2.22-76.79-3.22-116.44-2.72-61.62.78-119.59,7.93-175.21,13.95-5,.55-10,1.49-15.11,1.47-1.33-.32-2.46-1.91-1.25-3-2-.38-2.93.29-5-.15a9.83,9.83,0,0,1-1.25-3,13.73,13.73,0,0,1,6.42-2.94c.77-.54.12-.8.15-1.6a171,171,0,0,1,45.35-8.58c26.43-1.1,53.68-4.73,79.64-6,6.25-.3,11.72.06,18.41.14,8.31.1,19.09-1,29.19-.12,6.25.54,13.67-.55,21.16-.56,39.35-.09,71.16-.23,112.28,2C317.24,171.93,315.11,173.81,314.05,172.88Zm64.22,16.05-1.6-.2C376.2,190,378.42,190.26,378.27,188.93Z">
                                    </path>
                                    <polygon class="cls-3"
                                        points="52.57 166.06 57.61 151.99 23.11 157.23 32.36 169.12 52.57 166.06">
                                    </polygon>
                                    <polygon class="cls-4" points="44.69 183.24 51.75 168.35 33.86 171.06 44.69 183.24">
                                    </polygon>
                                    <path class="cls-4"
                                        d="M.12,10.15l22,145.06,35.47-5.39-22-145a2.6,2.6,0,0,0,0-.4C35,.76,26.62-.95,16.81.54S-.52,6.16,0,9.77A2.62,2.62,0,0,0,.12,10.15Z">
                                    </path>
                                    <path class="cls-5"
                                        d="M433.68,79.45l21-36.33h27.56L448.48,97.51l34.9,56H456l-22-37.76-21.94,37.76H384.49l34.9-56L385.72,43.12h27.35Z">
                                    </path>
                                </g>
                            </g>
                        </svg>
						<div class="f-14 f-md-16 w400 mt20 lh160 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
                    <div class=" mt20 mt-md40 white-clr"> <div class="f-17 w300 mt20 lh140 white-clr text-center"><script type="text/javascript" src="
                    https://warriorplus.com/o2/disclaimer/fbmn7d" defer></script><div class="wplus_spdisclaimer"></div></div></div>
				</div>
                <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                    <div class="f-14 f-md-16 w400 lh160 white-clr text-center">Copyright © LearnX</div>
                    <ul class="footer-ul w400 f-14 f-md-16 white-clr text-center text-md-right">
                        <li><a href="https://support.bizomart.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                        <li><a href="http://getlearnx.com/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                        <li><a href="http://getlearnx.com/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                        <li><a href="http://getlearnx.com/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                        <li><a href="http://getlearnx.com/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                        <li><a href="http://getlearnx.com/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                        <li><a href="http://getlearnx.com/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!--Footer Section End -->

	<script type="text/javascript">
   var noob = $('.countdown').length;
   var stimeInSecs;
   var tickers;

   function timerBegin(seconds) {   
      if(seconds>=0)  {
         stimeInSecs = parseInt(seconds);           
         showRemaining();
      }
      //tickers = setInterval("showRemaining()", 1000);
   }

   function showRemaining() {

      var seconds = stimeInSecs;
         
      if (seconds > 0) {         
         stimeInSecs--;       
      } else {        
         clearInterval(tickers);       
         //timerBegin(59 * 60); 
      }

      var days = Math.floor(seconds / 86400);

      seconds %= 86400;
      
      var hours = Math.floor(seconds / 3600);
      
      seconds %= 3600;
      
      var minutes = Math.floor(seconds / 60);
      
      seconds %= 60;


      if (days < 10) {
         days = "0" + days;
      }
      if (hours < 10) {
         hours = "0" + hours;
      }
      if (minutes < 10) {
         minutes = "0" + minutes;
      }
      if (seconds < 10) {
         seconds = "0" + seconds;
      }
      var i;
      var countdown = document.getElementsByClassName('countdown');

      for (i = 0; i < noob; i++) {
         countdown[i].innerHTML = '';
      
         if (days) {
            countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-24 f-md-42 timerbg oswald">' + days + '</span><br> &nbsp;&nbsp;&nbsp;&nbsp;<span class="f-14 f-md-14 w400 smmltd">Days</span> </div>';
         }
      
         countdown[i].innerHTML += '<div class="timer-label text-center">&nbsp;&nbsp;&nbsp;&nbsp;<span class="f-24 f-md-42 timerbg oswald">' + hours + '</span><br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="f-14 f-md-14 w400 smmltd">Hours</span> </div>';
      
         countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"> &nbsp;&nbsp;&nbsp;&nbsp;<span class="f-24 f-md-42 timerbg oswald">' + minutes + '</span><br> &nbsp;&nbsp;&nbsp;&nbsp;<span class="f-14 f-md-14 w400 smmltd">Mins</span> </div>';
      
         countdown[i].innerHTML += '<div class="timer-label text-center "> &nbsp;&nbsp;&nbsp;&nbsp; <span class="f-24 f-md-42 timerbg oswald">' + seconds + '</span><br><span class="f-14 f-md-14 w400 smmltd">Sec</span> </div>';
      }
   
   }

const givenTimestamp = 'Mon Dec 4 2023 20:30:00 GMT+0530'
const durationTime = 60;

const date = new Date(givenTimestamp);
const givenTime = date.getTime() / 1000;
const currentTime = new Date().getTime()



let cnt;
const matchInterval = setInterval(() => {
   if(Math.round(givenTime) === Math.round(new Date().getTime() /1000)){
   cnt = durationTime * 60;
   counter();
   clearInterval(matchInterval);
   }else if(Math.round(givenTime) < Math.round(new Date().getTime() /1000)){
      const timeGap = Math.round(new Date().getTime() /1000) - Math.round(givenTime)
      let difference = (durationTime * 60) - timeGap;
      if(difference >= 0){
         cnt = difference
         localStorage.mavasTimer = cnt;

      }else{
         
         difference = difference % (durationTime * 60);
         cnt = (durationTime * 60) + difference
         localStorage.mavasTimer = cnt;

      }

      counter();
      clearInterval(matchInterval);
   }
},1000)





function counter(){
   if(parseInt(localStorage.mavasTimer) > 0){
      cnt = parseInt(localStorage.mavasTimer);
   }
   cnt -= 1;
   localStorage.mavasTimer = cnt;
   if(localStorage.mavasTimer !== NaN){
      timerBegin(parseInt(localStorage.mavasTimer));
   }
   if(cnt>0){
      setTimeout(counter,1000);
  }else if(cnt === 0){
   cnt = durationTime * 60 
   counter()
  }
}

</script>
  <!--- timer end-->
</body>
</html>

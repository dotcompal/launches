<!Doctype html>
<html>
   <head>
      <title>LearnX | Traffic </title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=9">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
      <meta name="title" content="LearnX | Traffic">
      <meta name="description" content="Brand New AI APP Creates 100s of Reels, Boomerang, & Short Videos With Just One Keyword & Drive Floods Of FREE Traffic And Sales">
      <meta name="keywords" content="LearnX | Traffic">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta property="og:image" content="https://www.getlearnx.com/traffic/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Pranshu Gupta">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="LearnX | Traffic">
      <meta property="og:description" content="Brand New AI APP Creates 100s of Reels, Boomerang, & Short Videos With Just One Keyword & Drive Floods Of FREE Traffic And Sales">
      <meta property="og:image" content="https://www.getlearnx.com/traffic/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="LearnX | Traffic">
      <meta property="twitter:description" content="Brand New AI APP Creates 100s of Reels, Boomerang, & Short Videos With Just One Keyword & Drive Floods Of FREE Traffic And Sales">
      <meta property="twitter:image" content="https://www.getlearnx.com/traffic/thumbnail.png">
      <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
      <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&family=Poppins:wght@300;400;500;600;700;800;900&family=Red+Hat+Display:wght@300;400;500;600;700;800;900&display=swap" rel="stylesheet">
      <link rel="stylesheet" href="https://cdn.oppyotest.com/launches/sellero/common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style.css" type="text/css">
      <link rel="stylesheet" href="assets/css/timer.css" type="text/css">
      <script src="../common_assets/js/jquery.min.js"></script>
      <script src="../common_assets/js/bootstrap.min.js"></script>
      <script src="assets/js/timer.js"></script>
   </head>
   <body>
   <div class="warning-section">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <div class="offer">
                        <div>
                            <img src="https://cdn.oppyo.com/launches/appzilo/special/error.webp">
                        </div>
                        <div class="f-22 f-md-30 w500 lh140 white-clr text-center">
                            <span class="w600">ATTENTION: </span> Avoid This Offer & Be Ready To Pay A Fortune...
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
      <!-- Header Section Start -->
      <div class="header-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
               <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 484.58 200.84" style="max-height:75px;">
                        <defs>
                            <style>
                                .cls-1,.cls-4{fill:#fff;}
                                .cls-2,.cls-3,.cls-4{fill-rule:evenodd;}
                                .cls-2{fill:url(#linear-gradient);}
                                .cls-3{fill:url(#linear-gradient-2);}
                                .cls-5{fill:url(#linear-gradient-3);}
                            </style>
                            <linearGradient id="linear-gradient" x1="-154.67" y1="191.86" x2="249.18" y2="191.86" gradientTransform="matrix(1.19, -0.01, 0.05, 1, 178.46, -4.31)" gradientUnits="userSpaceOnUse">
                                <stop offset="0" stop-color="#ead40c"></stop>
                                <stop offset="1" stop-color="#ff00e9"></stop>
                            </linearGradient>
                            <linearGradient id="linear-gradient-2" x1="23.11" y1="160.56" x2="57.61" y2="160.56" gradientTransform="matrix(1, 0, 0, 1, 0, 0)" xlink:href="#linear-gradient"></linearGradient>
                            <linearGradient id="linear-gradient-3" x1="384.49" y1="98.33" x2="483.38" y2="98.33" gradientTransform="matrix(1, 0, 0, 1, 0, 0)" xlink:href="#linear-gradient"></linearGradient>
                        </defs>
                        <g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1">
                            <path class="cls-1" d="M112.31,154.3q-11.65,0-20.1-4.87a32.56,32.56,0,0,1-13-13.82q-4.53-9-4.54-21.11a46.06,46.06,0,0,1,4.57-21A34.29,34.29,0,0,1,92,79.37a36.11,36.11,0,0,1,19.31-5.06,39.65,39.65,0,0,1,13.54,2.29,31,31,0,0,1,11.3,7.09,33.32,33.32,0,0,1,7.75,12.18,49.23,49.23,0,0,1,2.82,17.57V119H83.26v-12.3h46A19.87,19.87,0,0,0,127,97.38a16.6,16.6,0,0,0-6.18-6.48,17.59,17.59,0,0,0-9.21-2.37,17.88,17.88,0,0,0-9.83,2.7,18.81,18.81,0,0,0-6.58,7.06,20.17,20.17,0,0,0-2.4,9.56v10.74a25.15,25.15,0,0,0,2.47,11.57,17.42,17.42,0,0,0,6.91,7.37,20.52,20.52,0,0,0,10.39,2.55,21.62,21.62,0,0,0,7.22-1.14,15.71,15.71,0,0,0,5.59-3.35,14.11,14.11,0,0,0,3.59-5.5L146,132a26.44,26.44,0,0,1-6.13,11.77,29.72,29.72,0,0,1-11.52,7.77A43.74,43.74,0,0,1,112.31,154.3Z"></path>
                            <path class="cls-1" d="M184.49,154.35a31.78,31.78,0,0,1-13.24-2.65,21.26,21.26,0,0,1-9.28-7.84,22.86,22.86,0,0,1-3.41-12.81A21.92,21.92,0,0,1,161,120.2a18.9,18.9,0,0,1,6.61-6.86,33.85,33.85,0,0,1,9.46-3.9,77.76,77.76,0,0,1,10.92-2q6.81-.7,11-1.28a15.91,15.91,0,0,0,6.18-1.82,4.25,4.25,0,0,0,1.94-3.86v-.3q0-5.7-3.38-8.83T194,88.28q-6.71,0-10.62,2.92a14.55,14.55,0,0,0-5.27,6.91l-17-2.42a27.51,27.51,0,0,1,6.66-11.83,29.46,29.46,0,0,1,11.35-7.16,43.73,43.73,0,0,1,14.83-2.39,47.89,47.89,0,0,1,11.14,1.31,31.6,31.6,0,0,1,10.14,4.31,22.12,22.12,0,0,1,7.39,8.14q2.81,5.15,2.8,12.87v51.85H207.84V142.14h-.61a22.1,22.1,0,0,1-4.66,6,22.35,22.35,0,0,1-7.52,4.49A30,30,0,0,1,184.49,154.35Zm4.74-13.42a19.7,19.7,0,0,0,9.53-2.19,16,16,0,0,0,6.23-5.83,15,15,0,0,0,2.19-7.91v-9.13a8.72,8.72,0,0,1-2.9,1.31,45.56,45.56,0,0,1-4.56,1.06c-1.68.3-3.35.57-5,.8l-4.29.61a32,32,0,0,0-7.32,1.81A12.29,12.29,0,0,0,178,125a9.76,9.76,0,0,0,1.82,13.39A16,16,0,0,0,189.23,140.93Z"></path>
                            <path class="cls-1" d="M243.8,152.79V75.31h17.7V88.23h.81a19.36,19.36,0,0,1,7.29-10.37,20,20,0,0,1,11.83-3.66c1,0,2.14,0,3.4.13a23.83,23.83,0,0,1,3.15.38V91.5a21,21,0,0,0-3.65-.73,38.61,38.61,0,0,0-4.82-.32,18.47,18.47,0,0,0-8.95,2.14,15.94,15.94,0,0,0-6.23,5.93,16.55,16.55,0,0,0-2.27,8.72v45.55Z"></path>
                            <path class="cls-1" d="M318.4,107.39v45.4H300.14V75.31h17.45V88.48h.91a22,22,0,0,1,8.55-10.34q5.86-3.84,14.55-3.83a27.58,27.58,0,0,1,14,3.43,23.19,23.19,0,0,1,9.28,9.93,34.22,34.22,0,0,1,3.26,15.79v49.33H349.87V106.28c0-5.17-1.34-9.23-4-12.15s-6.37-4.39-11.07-4.39a17,17,0,0,0-8.5,2.09,14.67,14.67,0,0,0-5.8,6A20,20,0,0,0,318.4,107.39Z"></path>
                            <path class="cls-2" d="M314.05,172.88c12.69-.14,27.32,2.53,40.82,2.61l-.09,1c1.07.15,1-.88,2-.78,1.42-.18,0,1.57,1.82,1.14.54-.06.08-.56-.32-.68.77,0,2.31.17,3,1.26,1.3,0-.9-1.34.85-.89,6,1.63,13.39,0,20,3.2,11.64.72,24.79,2.38,38,4.27,2.85.41,5.37,1.11,7.57,1.05,2-.05,5.68.78,8,1.08,2.53.34,5.16,1.56,7.13,1.65-1.18-.05,5.49-.78,4.54.76,5-.72,10.9,1.67,15.94,1.84.83-.07.69.53.67,1,1.23-1.17,3.32.56,4.28-.56.25,2,2.63-.48,3.3,1.61,2.23-1.39,4.83.74,7.55,1.37,1.91.44,5.29-.21,5.55,2.14-2.1-.13-5.12.81-11.41-1.09,1,.87-3.35.74-3.39-.64-1.81,1.94-6.28-1-7.79,1.19-1.26-.41,0-.72-.64-1.35-5,.55-8.09-1.33-12.91-1.56,2.57,2.44,6.08,3.16,10.06,3.22,1.28.15,0,.74,1,1.39,1.37-.19.75-.48,1.26-1.17,5.37,2.28,15.36,2.35,21,4.91-4.69-.63-11.38,0-15.15-2.09A148,148,0,0,1,441.58,196c-.53-.1-1.81.12-.7-.71-1.65.73-3.41.1-5.74-.22-37.15-5.15-80.47-7.78-115.75-9.76-39.54-2.22-76.79-3.22-116.44-2.72-61.62.78-119.59,7.93-175.21,13.95-5,.55-10,1.49-15.11,1.47-1.33-.32-2.46-1.91-1.25-3-2-.38-2.93.29-5-.15a9.83,9.83,0,0,1-1.25-3,13.73,13.73,0,0,1,6.42-2.94c.77-.54.12-.8.15-1.6a171,171,0,0,1,45.35-8.58c26.43-1.1,53.68-4.73,79.64-6,6.25-.3,11.72.06,18.41.14,8.31.1,19.09-1,29.19-.12,6.25.54,13.67-.55,21.16-.56,39.35-.09,71.16-.23,112.28,2C317.24,171.93,315.11,173.81,314.05,172.88Zm64.22,16.05-1.6-.2C376.2,190,378.42,190.26,378.27,188.93Z"></path>
                            <polygon class="cls-3" points="52.57 166.06 57.61 151.99 23.11 157.23 32.36 169.12 52.57 166.06"></polygon>
                            <polygon class="cls-4" points="44.69 183.24 51.75 168.35 33.86 171.06 44.69 183.24"></polygon>
                            <path class="cls-4" d="M.12,10.15l22,145.06,35.47-5.39-22-145a2.6,2.6,0,0,0,0-.4C35,.76,26.62-.95,16.81.54S-.52,6.16,0,9.77A2.62,2.62,0,0,0,.12,10.15Z"></path>
                            <path class="cls-5" d="M433.68,79.45l21-36.33h27.56L448.48,97.51l34.9,56H456l-22-37.76-21.94,37.76H384.49l34.9-56L385.72,43.12h27.35Z"></path>
                        </g>
                            </g>
                    </svg>
                           </div>
               <div class="col-12 text-center mt20 mt-md50">
                  <div class="pre-heading f-18 f-md-18 w400 lh140 white-clr">
                     Make BIG Profits in 2023 by Copying a Proven System That Make Us $245/Day Over and Over Again…
                  </div>
               </div>
               <div class="col-12 mt-md50 mt30">
                     <div class="main-heading f-28 f-md-40 w400 text-center lh140 black2-clr red-hat-font  text-capitalize">
                        Brand New AI APP <span class="pink-clr w900">Creates 100s  Reels, Boomerang, & Short Videos with Just One Keyword…</span> for Floods of FREE Traffic and Sales
                     </div>
               </div>
               <div class="col-12 mt-md25 mt20 f-16 f-md-20 w400 text-center lh140 white-clr text-capitalize">
               Even A Complete Beginner Can Drive Thousands of Visitors to Any Offer or Link  Instantly <br> |  No Camera | No Writing & Editing | No Paid Traffic Ever…
               </div>
            </div>
            <div class="row d-flex align-items-center flex-wrap mt20 mt-md40">
               <div class="col-md-7 col-12 min-md-video-width-left">
                  <img src="assets/images/product-image.webp" class="img-fluid mx-auto d-block">
                  <div class="col-12 mt40 d-none d-md-block">
                     <div class="f-20 f-md-28 w700 lh140 text-center white-clr">
                        Bonus Agency License Included for Time!
                     </div>
                     <div class="row">
                        <div class="col-md-11 mx-auto col-12 mt20 mt-md20 text-center">
                           <a href="#buynow" class="cta-link-btn d-block px0">Get Instant Access To LearnX</a>
                        </div>
                     </div>
                     <div class="col-12 col-md-10 mx-auto d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                        <img src="assets/images/compaitable-with1.webp"
                           class="img-fluid mx-xs-center md-img-right" alt="visa">
                        <div class="d-md-block d-none visible-md px-md30"><img
                           src="assets/images/v-line.webp" class="img-fluid"
                           alt="line">
                        </div>
                        <img src="assets/images/days-gurantee1.webp"
                           class="img-fluid mx-xs-auto mt15 mt-md0" alt="30 days">
                     </div>
                  </div>
               </div>
               <div class="col-md-5 col-12 mt20 mt-md0 min-md-video-width-right">
                  <div class="calendar-wrap mb20 mb-md0">
                     <ul class="list-head pl0 m0 f-18 lh140 w400 white-clr">
                     <li>Drive Tsunami of FREE Traffic in Any Niche</li>
                     <li>Create Reels & Shorts Using Just One Keyword </li>
                     <li>Create Boomerang Video to Engage Audience</li>
                     <li>Get Thousands of Visitors & Sales to Any Offer or Page</li>
                     <li>Make Tons of Affiliate Commissions or Ad Profits</li> 
                     <li>Create Short Videos from Any Video, Text, or Just a Keyword</li>
                     <li>Add Background Music and/or Voiceover to Any Video</li>
                     <li>Pre-designed 50+ Shorts Video templates</li>
                     <li>25+ Vector Images to Choose From</li>
                     <li>100+ Social Sharing Platforms ForFREE Viral Traffic</li>
                     <li>100% Newbie Friendly, No Prior Experience Or Tech Skills Needed</li>
                     <li>No Camera Recording, No Voice, Or Complex Editing Required</li>
                     <li>Free Commercial License To Sell Video Services For High Profits</li>
                     </ul>
                  </div>
                  <div class="d-md-none">
                     <div class="f-18 f-md-26 w700 lh140 text-center white-clr">
                        Free Commercial License + Low 1-Time Price For Launch Period Only 
                     </div>
                     <div class="row">
                        <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center">
                           <a href="#buynow" class="cta-link-btn d-block px0">Get Instant Access To LearnX</a>
                        </div>
                     </div>
                     <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                        <img src="assets/images/compaitable-with1.webp"
                           class="img-responsive mx-xs-center md-img-right" alt="visa">
                        <div class="d-md-block d-none visible-md px-md30"><img
                           src="assets/images/v-line.webp" class="img-fluid"
                           alt="line">
                        </div>
                        <img src="assets/images/days-gurantee1.webp"
                           class="img-responsive mx-xs-auto mt15 mt-md0" alt="30 days">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Header Section End -->

      <!-- Step Section -->
      <div class="step-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-39 f-28 w600 text-center black-clr2 lh140">
                  Create Stunning Stories, Reels & Shorts for Massive Traffic To Any Offer, Page, or Link <span class="red-clr w700">in Just 3 Easy Steps</span>
               </div>
            </div>
            <div class="row mt20 mt-md50">
               <div class="col-12 col-md-4">
                  <div class="step-block">
                     <div class="step-title f-20 f-md-22 w700">Step 1</div>
                     <img src="assets/images/step-1.webp" class="img-fluid d-block mx-auto mt15 mt-md30">
                     <div class="step-content">
                        <div class="f-22 f-md-28 w600 black-clr2 mt10">Choose Platform</div>
                        <div class="w400 f-18 f-md-20 lh140 mt10 black-clr2">
                           Choose the social platform- YouTube, Instagram, TikTok, Facebook, or Snapchat you want to create Reel or Shorts for
                        </div>
                     </div>  
                  </div>
               </div>
               <div class="col-12 col-md-4 mt20 mt-md0">
                  <div class="step-block">
                     <div class="step-title f-20 f-md-22 w700">Step 2</div>
                     <img src="assets/images/step-2.webp" class="img-fluid d-block mx-auto mt15 mt-md30">
                     <div class="step-content">
                        <div class="f-22 f-md-28 w600 black-clr2 mt10">Enter a Keyword </div>
                        <div class="w400 f-18 f-md-20 lh140 mt10 black-clr2">
                           Enter the desired Keyword & you will get several short videos. Select any video & customize it by adding background, vectors, audio, & fonts to make it engaging
                        </div>
                     </div>  
                  </div>
               </div>
               <div class="col-12 col-md-4 mt20 mt-md0">
                  <div class="step-block">
                     <div class="step-title f-20 f-md-22 w700">Step 3</div>
                     <img src="assets/images/step-3.webp" class="img-fluid d-block mx-auto mt15 mt-md30">
                     <div class="step-content">
                        <div class="f-22 f-md-28 w600 black-clr2 mt10">Publish and Profit</div>
                        <div class="w400 f-18 f-md-20 lh140 mt10 black-clr2">
                           Now publish your Reels & Shortson different social platforms with a click to drive unlimited viral traffic & sales on your offer, page, or website.
                        </div>
                     </div>  
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md70">
               <div class="col-12 text-center">
                  <div class="f-20 f-md-24 w600 lh140">
                     No Download/Installation  |  No Prior Knowledge  |  100% Beginners Friendly
                  </div>
                  <div class="f-18 f-md-20 w400 mt15 mt-md30 lh140">
                     In just 60 seconds, you can create your first profitable Reels & Shorts to enjoy MASSIVE FREE Traffic, Sales & Profits coming into your bank account on autopilot.
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Step Section End -->

      <!-- Proof Section Start -->
      <div class="proof-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-40 f-28 w700 lh140 text-center black-clr2">
                  Got 34,309 Targeted Visitors in Last 30 Days…                
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
                  <img src="assets/images/proof.webp" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 mt20 mt-md50 f-md-40 f-28 w700 lh140 text-center black-clr2">   
                  And Making Consistent $245 In Profits <br class="d-none d-md-block"> Each & Every Day        
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
                  <img src="assets/images/proof1.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>
         </div>
      </div>
      <!-- Proof Section End -->

      <!-- CTA Section-->
      <div class="cta-section-new">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-18 f-md-28 w700 lh140 text-center white-clr">
                     Free Commercial License + Low 1-Time Price For Launch Period Only
                  </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 mt-md30 text-center">
                        <a href="#buynow" class="cta-link-btn">Get Instant Access To LearnX</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                     <img src="assets/images/compaitable-with1.webp" class="img-fluid mx-xs-center md-img-right" alt="visa">
                     <div class="d-md-block d-none visible-md px-md30">
                        <img src="assets/images/v-line.webp" class="img-fluid" alt="line">
                     </div>
                     <img src="assets/images/days-gurantee1.webp" class="img-fluid mx-xs-auto mt15 mt-md0" alt="30 days">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- CTA Section End-->

      <!-- Reels Section Start -->
      <section class="reel-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-45 w700 lh140 black-clr2">
                     Checkout Some of Reels & Shorts <br class="d-none d-md-block">
                     Videos Created Using LearnX
                  </div>
               </div>
            </div>
            <div class="row row-cols-md-3 row-cols-xl-3 row-cols-1  mt-md50">
               <div class="col mt20 mt-md100">
               <video width="100%" height="auto" loop autoplay muted="muted" class="mp4-border" controls>
                   <source src="assets/images/reel1.mp4" type="video/mp4">
               </video>
               </div>
               <div class="col mt20 mt-md0">
               <video width="100%" height="auto" loop autoplay muted="muted" class="mp4-border" controls>
                   <source src="assets/images/reel2.mp4" type="video/mp4">
               </video>
               </div>
               <div class="col mt20 mt-md60">
               <video width="100%" height="auto" loop autoplay muted="muted" class="mp4-border" controls>
                   <source src="assets/images/reel3.mp4" type="video/mp4">
               </video>
               </div>
               <div class="col mt20 mt-md30">
               <video width="100%" height="auto" loop autoplay muted="muted" class="mp4-border" controls>
                   <source src="assets/images/reel4.mp4" type="video/mp4">
               </video>
               </div>
               <div class="col mt20">
               <video width="100%" height="auto" loop autoplay muted="muted" class="mt-md-90 mp4-border" controls>
                   <source src="assets/images/reel5.mp4" type="video/mp4">
               </video>
               </div>
               <div class="col mt20 mt-md40">
               <video width="100%" height="auto" loop autoplay muted="muted" class="mt-md-50 mp4-border" controls>
                   <source src="assets/images/reel6.mp4" type="video/mp4">
               </video>
               </div>
            </div>
         </div>
      </section>
      <!-- Reels Section End -->

      <!-- Ground-Breaking Section Start -->
      <div class="ground-breaking">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-40 f-28 w700 text-center black-clr2 lh140">
                  LearnX Is Packed with Tons of Cool Features
               </div>
            </div>
            <div class="row row-cols-md-3 row-cols-xl-3 row-cols-1">
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat1.webp" alt="Create Niche based short video" class="img-fluid">
                     <div class="sep-line">
                     </div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Create Niche based short video</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat2.webp" alt="Create Video from Text" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">
                        Create Video from Text
                     </div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat3.webp" alt="Create Video Using Video Clips" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Create Video Using Video Clips</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat4.webp" alt="Create Video from Images" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Create Video from Images</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat5.webp" alt="Create Boomerang Video" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Create Boomerang Video</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat6.webp" alt="Create Podcasts" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Create Podcasts</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat7.webp" alt="50+ Short Video Templates" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">50+ Short Video Templates</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat8.webp" alt="Add 25+ Vector Images" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Add 25+ Vector Images</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat9.webp" alt="Add Sound Waves in Video" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Add Sound Waves in Video</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat10.webp" alt="Add Voiceover to Any Video" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Add Voiceover to Any Video</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat11.webp" alt="Add Background Music to Any video" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Add Background Music to Any video</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat12.webp" alt="Create Voiceover in 150+ Voices in 30+ Languages" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Create Voiceover in 150+ Voices in 30+ Languages</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat13.webp" alt="Simple Video Editor" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Simple Video Editor</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat14.webp" alt="Add Logo &amp; Watermark" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Add Logo &amp; Watermark</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat15.webp" alt="Free Stock Images &amp; Videos" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Free Stock Images &amp; Videos</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat16.webp" alt="Multiple Video Quality (HD, 720p, 1080p, etc)" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Multiple Video Quality (HD, 720p, 1080p, etc)</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat17.webp" alt="Inbuilt Music Library" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Inbuilt Music Library</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat18.webp" alt="Built-in Biz Drive to Store &amp; Share Media" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Built-in Biz Drive to Store &amp; Share Media</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat19.webp" alt="Post Videos on YouTube" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Post Videos on YouTube</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat20.webp" alt=" 100+ Social Sharing Platforms" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500"> 100+ Social Sharing Platforms</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat21.webp" alt="Drive Unlimited Viral Traffic" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Drive Unlimited Viral Traffic</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat22.webp" alt="Capture Unlimited Leads" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">Capture Unlimited Leads</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat23.webp" alt="100% Cloud-Based Software" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">100% Cloud-Based Software</div>
                  </div>
               </div>
               <div class="col mt20 mt-md40">
                  <div class="feature-wrapper support-img">
                     <img src="assets/images/feat24.webp" alt="100% Newbie Friendly" class="img-fluid">
                     <div class="sep-line"></div>
                     <div class="black-clr2 lh140 f-18 f-md-20 w500">100% Newbie Friendly</div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Ground-Breaking Section Start -->

      <!-- CTA Section-->
      <div class="cta-section-new">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-18 f-md-28 w700 lh140 text-center white-clr">
                     Free Commercial License + Low 1-Time Price For Launch Period Only
                  </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 mt-md30 text-center">
                        <a href="#buynow" class="cta-link-btn">Get Instant Access To LearnX</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                     <img src="assets/images/compaitable-with1.webp" class="img-fluid mx-xs-center md-img-right" alt="visa">
                     <div class="d-md-block d-none visible-md px-md30">
                        <img src="assets/images/v-line.webp" class="img-fluid" alt="line">
                     </div>
                     <img src="assets/images/days-gurantee1.webp" class="img-fluid mx-xs-auto mt15 mt-md0" alt="30 days">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- CTA Section End-->


      <!-- Billion Section Start-->
      <div class="billion-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-45 w600 white-clr lh140 heading-design">Did You Know ? YouTube Shorts Hit</div>
                  <div class="f-45 f-md-100 w600 black-clr2 mt20 lh140"> <span class="red-clr">30 Billion</span> View</div>
                  <div class="f-28 f-md-45 w600 lh140 black-clr2">Every Single Day!</div>
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-12 col-md-7">
                  <div class="f-18 f-md-24 w600 black-clr2 lh140">Yes, YouTube Shorts are Generating 30 Billion+ Views Per Day, A 4X Increase From This Time Last Year
                  </div>
                  <div class="f-18 f-md-24 w400 lh140 black-clr21 mt20">This clearly shows the trend is changing. Internet users prefer short videos to consume content instead of watching long videos like earlier…</div>
                  <div class="f-28 f-md-45 w600 lh140 red-clr mt20 mt-md50">No Doubt!</div>
                  <div class="f-20 f-md-24 w600 lh140 black-clr2 mt10">Reels & Shorts Are Present and Future of Video Marketing…</div>
               </div>
               <div class="col-12 col-md-5 mt20 mt-md0">
                  <img src="assets/images/girl.webp" class="img-fluid mx-auto d-block">
               </div>
            </div>
         </div>
         <div class="billion-wrap">
            <div class="container">
               <div class="row">
                  <div class="col-12 text-center">
                     <div class="f-28 f-md-45 w600 lh140 black-clr2">Here Are Some More Facts - <br class="d-none d-md-block">Why One Should Get Started with Reels & Shorts! </div>
                  </div>
               </div>
               <div class="row align-items-center mt20 mt-md80">
                  <div class="col-12 col-md-6">
                     <div class="f-20 f-md-24 w600 lh140 text-start black-clr2">YouTube has 2+ Bn Users & 70% of Videos are Viewed on Mobile</div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 ">
                     <img src="assets/images/facts1.webp" class="img-fluid d-block mx-auto">
                  </div>
               </div>
               <div class="row align-items-center mt20 mt-md80">
                  <div class="col-12 col-md-6 order-md-2">
                     <div class="f-20 f-md-24 w600 lh140 text-start black-clr2">Instagram Reels have 2.5 Billion Monthly Active Users</div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
                     <img src="assets/images/facts3.webp" class="img-fluid d-block mx-auto" alt="Instagram Reels">
                  </div>
               </div>
               <div class="row align-items-center mt20 mt-md80">
                  <div class="col-12 col-md-6">
                     <div class="f-20 f-md-24 w600 lh140 text-start black-clr2">Tiktok has 50 Million Daily Active User in USA & 1 Bn Users Worldwide</div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0">
                     <img src="assets/images/facts4.webp" alt="Tiktok" class="mx-auto img-fluid d-block">
                  </div>
               </div>
               <div class="row align-items-center mt20 mt-md80">
                  <div class="col-12 col-md-6 order-md-2">
                     <div class="f-20 f-md-24 w600 lh140 text-start black-clr2">100 Million hours Daily watch Time by Facebook users.</div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
                     <img src="assets/images/facts5.webp" class="img-fluid d-block mx-auto" alt="Facebook Users">
                  </div>
               </div>
               <div class="row align-items-center mt20 mt-md80">
                  <div class="col-12 col-md-6">
                     <div class="f-20 f-md-24 w600 lh140 text-start black-clr2">Snapchat has over 360+ million daily active users</div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0">
                     <img src="assets/images/facts6.webp" alt="Website Analyser" class="mx-auto img-fluid d-block" alt="Snapchat Users">
                  </div>
               </div>
               <div class="row mt20 mt-md80">
                  <div class="col-12 text-center">
                     <div class="f-28 f-md-45 w600 lh140 ">So, Capitalising on Reels & Shorts in the Video Marketing Strategy of Any Business is the Need of The Hour.</div>
                     <div class="f-md-45 f-28 w600 lh140 mt20 mt-md30 black-clr2  border-cover">
                        <div class="f-22 f-md-26 w600 lh140 black-clr2">It Opens the Door to</div>
                        <div class=""> Tsunami <span class="red-clr"> of FREE </span>Traffic</div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Billion Section End -->

      <section class="prob-sec">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-20 f-md-32 w700 lh140 text-center black-clr2">
                     Now You’re Probably Wondering “Can Anyone Make a Full-Time Income With Short Videos?”                                  
                  </div>
                  <div class="f-20 f-md-32 w400 lh140 text-center mt15">
                  <span class="w700">Answer Is - Of Course, You Can!</span>   
                  </div>
               </div>
            </div>
         </div>
      </section>

      <!-- Power Section Start -->
      <div class="power-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-28 f-md-45 w700 lh140 text-center black-clr2">
                     See How People Are Making Crazy <br class="d-none d-md-block">
                     Money Using the POWER Of Reels & Short Videos
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md150">
                  <div class="power-shape">
                     <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-12 col-md-2">
                           <img src="assets/images/ryan.webp" class="img-fluid d-block mx-auto ryn-img">
                        </div>
                        <div class="col-12 col-md-10 mt30 mt-md0">
                           <div class="f-md-22 f-20 lh140 w400 text-center text-md-start pl-md60">
                              <span class="w700">Here’s “Ryan’s World” </span>– A 10-Year-Old Kid that has over <br class="d-none d-md-block"> 31.9M Subscribers with a whooping <span class="w700">Net Worth of $32M</span><br class="d-none d-md-block"> and growing… As he is now capitalizing on YouTube Shorts. <br><br>
                              Yes! You heard me right... He is just a 10-year-kid but making BIG.
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md150">
                  <div class="power-shape">
                     <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-12 col-md-2 order-md-2">
                           <img src="assets/images/huddakattan.webp" class="img-fluid d-block mx-auto ryn-img1">
                        </div>
                        <div class="col-12 col-md-10 mt30 mt-md0 order-md-1">
                           <div class="f-md-22 f-20 lh140 w400 text-center text-md-start pl-md30 pr-md40">
                              <span class="w700">“HuddaKattan” </span>is an American makeup artist, blogger & entrepreneur, Founder of cosmetic lines Huda Beauty on Instagram has over 51.8M Followers. She is using Instagram to interatct with here followers on daily basis.
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md150">
                  <div class="power-shape">
                     <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-12 col-md-2">
                           <img src="assets/images/khabane-lame.webp" class="img-fluid d-block mx-auto ryn-img">
                        </div>
                        <div class="col-12 col-md-10 mt30 mt-md0">
                           <div class="f-md-22 f-20 lh140 w400 text-center text-md-start pl-md60">
                           <span class="w700">“Khabane Lame”</span> is an Italian born social personality started uploading short video on dancing on Tiktok after laid off during Covid 19 and today he is most followed creator with 142.4M followers.
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md150">
                  <div class="power-shape">
                     <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-12 col-md-2 order-md-2">
                           <img src="assets/images/dj-khaled.webp" class="img-fluid d-block mx-auto ryn-img1">
                        </div>
                        <div class="col-12 col-md-10 mt30 mt-md0 order-md-1">
                           <div class="f-md-22 f-20 lh140 w400 text-center text-md-start pl-md60">
                              <span class="w700"> DJ Khaled known as  ‘King of Snapchat' </span> <br><br>
                              He is an American DJ, record executive, record producer and rapper. Now-a-days He teaches his audience by creating Reels that how anyone can get succeed on social media.
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row mt30 mt-md100">
               <div class="col-12 text-center">
                  <div class="but-design-border">
                     <div class="f-md-45 f-28 lh140 w900 white-clr button-shape">
                        Impressive right?
                     </div>
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md20">
               <div class="col-12 f-md-28 f-20 lh160 w400 text-center">
                  So, it can be safely stated that…<br>
                  <span class="f-22 f-md-32 w700 red-clr">Reels & Shorts Are The BIGGEST, Traffic and Income<br class="d-none d-md-block"> Opportunity Right Now! </span>
               </div>
            </div>
         </div>
      </div>
      <!-- Power Section End -->

      <!-- Affiliate Section Start -->
      <div class="affiliate-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-18 f-md-20 w400 lh140 black-clr2">
                  <span class="w700">The SECRET Is: </span><br><br>When you can combine selling top products, Reels & Shorts, and free viral traffic to make sales & profits, you’re the fastest way possible.
               </div>
               <div class="col-12 f-18 f-md-20 w500 lh140 black-clr2 mt20 mt-md50">
                  <img src="assets/images/fb-viral.webp" class="img-fluid d-block mx-auto" alt="Viral">
               </div>
            </div>
         </div>
      </div>
      <!-- Affiliate Section End -->

  
      <!-----Option Section End------>

      <!-- Proudly Section -->
      <div class="proudly-sec" id="product">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center lh140">
                  <div class="f-28 f-md-32 w700 text-center white-clr lh140 text-capitalize presenting-head">Presenting…</div>
               </div>
               <div class="col-12 mt-md40 mt20 text-center">
               <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 484.58 200.84" style="max-height:100px;">
                        <defs>
                            <style>
                                .cls-1,.cls-4{fill:#fff;}
                                .cls-2,.cls-3,.cls-4{fill-rule:evenodd;}
                                .cls-2{fill:url(#linear-gradient);}
                                .cls-3{fill:url(#linear-gradient-2);}
                                .cls-5{fill:url(#linear-gradient-3);}
                            </style>
                            <linearGradient id="linear-gradient" x1="-154.67" y1="191.86" x2="249.18" y2="191.86" gradientTransform="matrix(1.19, -0.01, 0.05, 1, 178.46, -4.31)" gradientUnits="userSpaceOnUse">
                                <stop offset="0" stop-color="#ead40c"></stop>
                                <stop offset="1" stop-color="#ff00e9"></stop>
                            </linearGradient>
                            <linearGradient id="linear-gradient-2" x1="23.11" y1="160.56" x2="57.61" y2="160.56" gradientTransform="matrix(1, 0, 0, 1, 0, 0)" xlink:href="#linear-gradient"></linearGradient>
                            <linearGradient id="linear-gradient-3" x1="384.49" y1="98.33" x2="483.38" y2="98.33" gradientTransform="matrix(1, 0, 0, 1, 0, 0)" xlink:href="#linear-gradient"></linearGradient>
                        </defs>
                        <g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1">
                            <path class="cls-1" d="M112.31,154.3q-11.65,0-20.1-4.87a32.56,32.56,0,0,1-13-13.82q-4.53-9-4.54-21.11a46.06,46.06,0,0,1,4.57-21A34.29,34.29,0,0,1,92,79.37a36.11,36.11,0,0,1,19.31-5.06,39.65,39.65,0,0,1,13.54,2.29,31,31,0,0,1,11.3,7.09,33.32,33.32,0,0,1,7.75,12.18,49.23,49.23,0,0,1,2.82,17.57V119H83.26v-12.3h46A19.87,19.87,0,0,0,127,97.38a16.6,16.6,0,0,0-6.18-6.48,17.59,17.59,0,0,0-9.21-2.37,17.88,17.88,0,0,0-9.83,2.7,18.81,18.81,0,0,0-6.58,7.06,20.17,20.17,0,0,0-2.4,9.56v10.74a25.15,25.15,0,0,0,2.47,11.57,17.42,17.42,0,0,0,6.91,7.37,20.52,20.52,0,0,0,10.39,2.55,21.62,21.62,0,0,0,7.22-1.14,15.71,15.71,0,0,0,5.59-3.35,14.11,14.11,0,0,0,3.59-5.5L146,132a26.44,26.44,0,0,1-6.13,11.77,29.72,29.72,0,0,1-11.52,7.77A43.74,43.74,0,0,1,112.31,154.3Z"></path>
                            <path class="cls-1" d="M184.49,154.35a31.78,31.78,0,0,1-13.24-2.65,21.26,21.26,0,0,1-9.28-7.84,22.86,22.86,0,0,1-3.41-12.81A21.92,21.92,0,0,1,161,120.2a18.9,18.9,0,0,1,6.61-6.86,33.85,33.85,0,0,1,9.46-3.9,77.76,77.76,0,0,1,10.92-2q6.81-.7,11-1.28a15.91,15.91,0,0,0,6.18-1.82,4.25,4.25,0,0,0,1.94-3.86v-.3q0-5.7-3.38-8.83T194,88.28q-6.71,0-10.62,2.92a14.55,14.55,0,0,0-5.27,6.91l-17-2.42a27.51,27.51,0,0,1,6.66-11.83,29.46,29.46,0,0,1,11.35-7.16,43.73,43.73,0,0,1,14.83-2.39,47.89,47.89,0,0,1,11.14,1.31,31.6,31.6,0,0,1,10.14,4.31,22.12,22.12,0,0,1,7.39,8.14q2.81,5.15,2.8,12.87v51.85H207.84V142.14h-.61a22.1,22.1,0,0,1-4.66,6,22.35,22.35,0,0,1-7.52,4.49A30,30,0,0,1,184.49,154.35Zm4.74-13.42a19.7,19.7,0,0,0,9.53-2.19,16,16,0,0,0,6.23-5.83,15,15,0,0,0,2.19-7.91v-9.13a8.72,8.72,0,0,1-2.9,1.31,45.56,45.56,0,0,1-4.56,1.06c-1.68.3-3.35.57-5,.8l-4.29.61a32,32,0,0,0-7.32,1.81A12.29,12.29,0,0,0,178,125a9.76,9.76,0,0,0,1.82,13.39A16,16,0,0,0,189.23,140.93Z"></path>
                            <path class="cls-1" d="M243.8,152.79V75.31h17.7V88.23h.81a19.36,19.36,0,0,1,7.29-10.37,20,20,0,0,1,11.83-3.66c1,0,2.14,0,3.4.13a23.83,23.83,0,0,1,3.15.38V91.5a21,21,0,0,0-3.65-.73,38.61,38.61,0,0,0-4.82-.32,18.47,18.47,0,0,0-8.95,2.14,15.94,15.94,0,0,0-6.23,5.93,16.55,16.55,0,0,0-2.27,8.72v45.55Z"></path>
                            <path class="cls-1" d="M318.4,107.39v45.4H300.14V75.31h17.45V88.48h.91a22,22,0,0,1,8.55-10.34q5.86-3.84,14.55-3.83a27.58,27.58,0,0,1,14,3.43,23.19,23.19,0,0,1,9.28,9.93,34.22,34.22,0,0,1,3.26,15.79v49.33H349.87V106.28c0-5.17-1.34-9.23-4-12.15s-6.37-4.39-11.07-4.39a17,17,0,0,0-8.5,2.09,14.67,14.67,0,0,0-5.8,6A20,20,0,0,0,318.4,107.39Z"></path>
                            <path class="cls-2" d="M314.05,172.88c12.69-.14,27.32,2.53,40.82,2.61l-.09,1c1.07.15,1-.88,2-.78,1.42-.18,0,1.57,1.82,1.14.54-.06.08-.56-.32-.68.77,0,2.31.17,3,1.26,1.3,0-.9-1.34.85-.89,6,1.63,13.39,0,20,3.2,11.64.72,24.79,2.38,38,4.27,2.85.41,5.37,1.11,7.57,1.05,2-.05,5.68.78,8,1.08,2.53.34,5.16,1.56,7.13,1.65-1.18-.05,5.49-.78,4.54.76,5-.72,10.9,1.67,15.94,1.84.83-.07.69.53.67,1,1.23-1.17,3.32.56,4.28-.56.25,2,2.63-.48,3.3,1.61,2.23-1.39,4.83.74,7.55,1.37,1.91.44,5.29-.21,5.55,2.14-2.1-.13-5.12.81-11.41-1.09,1,.87-3.35.74-3.39-.64-1.81,1.94-6.28-1-7.79,1.19-1.26-.41,0-.72-.64-1.35-5,.55-8.09-1.33-12.91-1.56,2.57,2.44,6.08,3.16,10.06,3.22,1.28.15,0,.74,1,1.39,1.37-.19.75-.48,1.26-1.17,5.37,2.28,15.36,2.35,21,4.91-4.69-.63-11.38,0-15.15-2.09A148,148,0,0,1,441.58,196c-.53-.1-1.81.12-.7-.71-1.65.73-3.41.1-5.74-.22-37.15-5.15-80.47-7.78-115.75-9.76-39.54-2.22-76.79-3.22-116.44-2.72-61.62.78-119.59,7.93-175.21,13.95-5,.55-10,1.49-15.11,1.47-1.33-.32-2.46-1.91-1.25-3-2-.38-2.93.29-5-.15a9.83,9.83,0,0,1-1.25-3,13.73,13.73,0,0,1,6.42-2.94c.77-.54.12-.8.15-1.6a171,171,0,0,1,45.35-8.58c26.43-1.1,53.68-4.73,79.64-6,6.25-.3,11.72.06,18.41.14,8.31.1,19.09-1,29.19-.12,6.25.54,13.67-.55,21.16-.56,39.35-.09,71.16-.23,112.28,2C317.24,171.93,315.11,173.81,314.05,172.88Zm64.22,16.05-1.6-.2C376.2,190,378.42,190.26,378.27,188.93Z"></path>
                            <polygon class="cls-3" points="52.57 166.06 57.61 151.99 23.11 157.23 32.36 169.12 52.57 166.06"></polygon>
                            <polygon class="cls-4" points="44.69 183.24 51.75 168.35 33.86 171.06 44.69 183.24"></polygon>
                            <path class="cls-4" d="M.12,10.15l22,145.06,35.47-5.39-22-145a2.6,2.6,0,0,0,0-.4C35,.76,26.62-.95,16.81.54S-.52,6.16,0,9.77A2.62,2.62,0,0,0,.12,10.15Z"></path>
                            <path class="cls-5" d="M433.68,79.45l21-36.33h27.56L448.48,97.51l34.9,56H456l-22-37.76-21.94,37.76H384.49l34.9-56L385.72,43.12h27.35Z"></path>
                        </g>
                            </g>
                    </svg>
               </div>
               <div class="col-12 mt-md50 mt30 f-md-45 f-28 w700 text-center white-clr lh140 text-capitalize">
                  Brand New AI APP <span class="pink-clr">Creates 100s  Reels, Boomerang, & Short Videos with Just One Keyword…</span> for Floods of FREE Traffic and Sales
               </div>

            </div>
            <div class="row mt20 mt-md50 align-items-center">
               <div class="col-12">
                  <img src="assets/images/product-image.webp" class="img-fluid d-block mx-auto img-animation"
                     alt="Product">
               </div>
            </div>
         </div>
      </div>
      <!-- Proudly Section End -->

      <!-- Step Section Start -->
      <div class="step-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-39 f-28 w600 text-center black-clr2 lh140">
                  Create Stunning Stories, Reels & Shorts for Massive Traffic To Any Offer, Page, or Link <span class="red-clr w700">in Just 3 Easy Steps</span>
               </div>
            </div>
            <div class="row mt20 mt-md50">
               <div class="col-12 col-md-4">
                  <div class="step-block">
                     <div class="step-title f-20 f-md-22 w700">Step 1</div>
                     <img src="assets/images/step-1.webp" class="img-fluid d-block mx-auto mt15 mt-md30">
                     <div class="step-content">
                        <div class="f-22 f-md-28 w600 black-clr2 mt10">Choose Platform</div>
                        <div class="w400 f-18 f-md-20 lh140 mt10 black-clr2">
                           Choose the social platform- YouTube, Instagram, TikTok, Facebook, or Snapchat you want to create Reel or Shorts for
                        </div>
                     </div>  
                  </div>
               </div>
               <div class="col-12 col-md-4 mt20 mt-md0">
                  <div class="step-block">
                     <div class="step-title f-20 f-md-22 w700">Step 2</div>
                     <img src="assets/images/step-2.webp" class="img-fluid d-block mx-auto mt15 mt-md30">
                     <div class="step-content">
                        <div class="f-22 f-md-28 w600 black-clr2 mt10">Enter a Keyword </div>
                        <div class="w400 f-18 f-md-20 lh140 mt10 black-clr2">
                           Enter the desired Keyword & you will get several short videos. Select any video & customize it by adding background, vectors, audio, & fonts to make it engaging
                        </div>
                     </div>  
                  </div>
               </div>
               <div class="col-12 col-md-4 mt20 mt-md0">
                  <div class="step-block">
                     <div class="step-title f-20 f-md-22 w700">Step 3</div>
                     <img src="assets/images/step-3.webp" class="img-fluid d-block mx-auto mt15 mt-md30">
                     <div class="step-content">
                        <div class="f-22 f-md-28 w600 black-clr2 mt10">Publish and Profit</div>
                        <div class="w400 f-18 f-md-20 lh140 mt10 black-clr2">
                           Now publish your Reels & Shortson different social platforms with a click to drive unlimited viral traffic & sales on your offer, page, or website.
                        </div>
                     </div>  
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md70">
               <div class="col-12 text-center">
                  <div class="f-20 f-md-24 w600 lh140">
                     No Download/Installation  |  No Prior Knowledge  |  100% Beginners Friendly
                  </div>
                  <div class="f-18 f-md-20 w400 mt15 mt-md30 lh140">
                     In just 60 seconds, you can create your first profitable Reels & Shorts to enjoy MASSIVE FREE Traffic, Sales & Profits coming into your bank account on autopilot.
                  </div>
               </div>
            </div>
            <div class="col-12 text-center">
                  <div class="f-18 f-md-28 w700 lh140 text-center white-clr">
                     Free Commercial License + Low 1-Time Price For Launch Period Only
                  </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center">
                        <a href="#buynow" class="cta-link-btn">Get Instant Access To LearnX</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                     <img src="assets/images/compaitable-with.webp" class="img-responsive mx-xs-center md-img-right" alt="visa">
                     <div class="d-md-block d-none visible-md px-md30">
                        <img src="assets/images/v-line.webp" class="img-fluid" alt="line">
                     </div>
                     <img src="assets/images/days-gurantee.webp" class="img-responsive mx-xs-auto mt15 mt-md0" alt="30 days">
                  </div>
               </div>
         </div>
      </div>
      <!-- Step Section End -->

      <!-- Features Section Star -->
      <div class="features-section-one">
         <div class="container">           
            <div class="row align-items-center">
               <div class="col-12 mb20 mb-md50">
                  <div class="f-md-45 f-28 w700 lh140 white-clr text-center">
                  Here Are The GROUND - BREAKING Features<br class="d-none d-md-block"> 
                  That Makes LearnX A CUT ABOVE The Rest 
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="f-md-32 f-24 w700 lh140 white-clr">
                     Create Highly Engaging & Professional Reels & Short Videos 
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 white-clr mt15">
                     Create Reels & Shorts for YouTube, Instagram, TikTok, Facebook & SnapChat                  
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0">
                  <img src="assets/images/f1.webp" class="img-fluid d-block mx-auto" alt="Features">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-md-6 order-md-2">
                  <div class="f-md-32 f-24 w700 lh140 white-clr">
                     Create Shorts by One Keyword
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 white-clr mt15">
                     Create Reels & short videos by entering Just one keyword and you will get number of short videos to choose or upload your own.
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0 order-md-1">
                  <img src="assets/images/f2.webp" class="img-fluid d-block mx-auto" alt="Features">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-md-6">
                  <div class="f-md-32 f-24 w700 lh140 white-clr">
                     Create 9:16 Vertical Video
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 white-clr mt15">
                     Create highly professional & engaging 9:16 vertical video. Perfect for mobile viewing and guaranteed to entertain your audience.                  
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0">
                  <img src="assets/images/f3.webp" class="img-fluid d-block mx-auto" alt="Features">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-md-6 order-md-2">
                  <div class="f-md-32 f-24 w700 lh140 white-clr">
                     Create Boomerang
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 white-clr mt15">
                     Create boomerang short video loops that boomerang forward & reverse through the action.
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0 order-md-1">
                  <!-- <img src="assets/images/f4.webp" class="img-fluid d-block mx-auto" alt="Features"> -->
                  <video width="100%" height="auto" loop autoplay muted="muted" class="mp4-border" >
                   <source src="assets/images/f4.mp4" type="video/mp4">
               </video>
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-md-6">
                  <div class="f-md-32 f-24 w700 lh140 white-clr">
                     Create High-Quality Explanatory Whiteboard Video Shorts
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 white-clr mt15">
                     Create whiteboard video shorts by just adding your text and customizing font, color, and alignment. These videos are highly effective and informative.                    
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0">
                  <img src="assets/images/f5.webp" class="img-fluid d-block mx-auto" alt="Features">
               </div>
            </div>
         </div>
      </div>
      <div class="grey-section1">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-6">
                  <div class="f-md-32 f-24 w700 lh140 balck-clr2">
                     Create Videos by Images
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 balck-clr2 mt15">
                     Find images for videos by searching images with keywords inside LearnX or upload your own images                   
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0">
                  <img src="assets/images/f6.webp" class="img-fluid d-block mx-auto" alt="Features">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-md-6 order-md-2">
                  <div class="f-md-32 f-24 w700 lh140 balck-clr2">
                     50+ Background Templates
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 balck-clr2 mt15">
                     Easily create Reels & shorts by using 50+ background templates to make your video attractive &get more & more engagement.                  
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0 order-md-1">
                  <img src="assets/images/f7.webp" class="img-fluid d-block mx-auto" alt="Features">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-md-6">
                  <div class="f-md-32 f-24 w700 lh140 balck-clr2">
                     25+ Vector Images
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 balck-clr2 mt15">
                     Create more engaging & attention grabbing Reels & Shorts by using 25+vector images. 
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0">
                  <img src="assets/images/f8.webp" class="img-fluid d-block mx-auto" alt="Features">
               </div>
            </div>
         </div>
      </div>
      <div class="white-feature">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-6 order-md-2">
                  <div class="f-md-32 f-24 w700 lh140 balck-clr2">
                  	100+ Stylish Fonts
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 balck-clr2 mt15">
                     Use 100+ different Stylish fonts to make your shorts more presentable
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0 order-md-1">
                  <img src="assets/images/f9.webp" class="img-fluid d-block mx-auto" alt="Features">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-md-6">
                  <div class="f-md-32 f-24 w700 lh140 balck-clr2">
                     Add Music Waves
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 balck-clr2 mt15">
                     Add Music Waves run with audio of different color to your Short videos to make your video engaging.
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0">
                  <!-- <img src="assets/images/f10.webp" class="img-fluid d-block mx-auto" alt="Features"> -->
                  <video width="100%" height="auto" loop autoplay muted="muted" class="mp4-border" >
                   <source src="assets/images/f10.mp4" type="video/mp4">
               </video>
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-md-6 order-md-2">
                  <div class="f-md-32 f-24 w700 lh140 balck-clr2">
                     Build Authority by Adding Your Branding
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 balck-clr2 mt15">
                     Build your authority in the audience by adding your brand watermarks and logos in the video.                   
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0 order-md-1">
                  <img src="assets/images/f11.webp" class="img-fluid d-block mx-auto" alt="Features">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-md-6">
                  <div class="f-md-32 f-24 w700 lh140 balck-clr2">
                     Share Shorts Directly to YouTube
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 balck-clr2 mt15">
                     You can easily share the video on YouTube or can download it is to use wherever you want.
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0">
                  <img src="assets/images/f12.webp" class="img-fluid d-block mx-auto" alt="Features">
               </div>
            </div>
         </div>
      </div>

      <div class="grey-section1">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-6 order-md-2">
                  <div class="f-md-32 f-24 w700 lh140 balck-clr2">
                     100+ Social Sharing Platforms
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 balck-clr2 mt15">
                     You can alsoshare these Reels & short videos t0  100+ social media platforms directly to get free viral traffic                            
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0 order-md-1">
                  <img src="assets/images/f13.webp" class="img-fluid d-block mx-auto" alt="Features">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md50">
               <div class="col-md-6">
                  <div class="f-md-32 f-24 w700 lh140 balck-clr2">
                     Completely Cloud-Based Software
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 balck-clr2 mt15">
                     Fully Cloud-Based Software with Zero Tech Hassles & 24*7 Customer Support          
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0">
                  <img src="assets/images/f14.webp" class="img-fluid d-block mx-auto" alt="Features">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md50">
               <div class="col-md-6 order-md-2">
                  <div class="f-md-32 f-24 w700 lh140 balck-clr2">
                     100% Newbie Friendly
                  </div>
                  <div class="f-md-18 f-18 w400 lh140 balck-clr2 mt15">
                     Easy and Intuitive to Use Software. Also comes with Step-by-Step Video Training & PDF
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0 order-md-1">
                  <img src="assets/images/f15.webp" class="img-fluid d-block mx-auto" alt="Features">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md50">
               <div class="col-md-6">
                  <div class="f-md-32 f-24 w700 lh140 balck-clr2">
                     No Monthly Fees or Additional Charges
                  </div>
               </div>
               <div class="col-md-6 col-10 mx-auto mt20 mt-md0">
                  <img src="assets/images/f16.webp" class="img-fluid d-block mx-auto" alt="Features">
               </div>
            </div>
         </div>
      </div>
      <!-- Features Section End -->

      <!-- CTA Section -->
      <div class="cta-section-new">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-18 f-md-28 w700 lh140 text-center white-clr">
                     Free Commercial License + Low 1-Time Price For Launch Period Only
                  </div>

                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center">
                        <a href="#buynow" class="cta-link-btn">Get Instant Access To LearnX</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                     <img src="assets/images/compaitable-with1.webp" class="img-responsive mx-xs-center md-img-right" alt="visa">
                     <div class="d-md-block d-none visible-md px-md30">
                        <img src="assets/images/v-line.webp" class="img-fluid" alt="line">
                     </div>
                     <img src="assets/images/days-gurantee1.webp" class="img-responsive mx-xs-auto mt15 mt-md0" alt="30 days">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- CTA Section End -->

      <!-- Need Section Start -->

      <div class="need-marketing">
         <div class="container">
             <div class="row">
                 <div class="col-12 col-md-10 offset-md-1">
                     <div class="f-md-45 f-28 lh140 w700 text-center balck-clr2">Who Can Benefit from the Vidmaster with LearnX Traffic Plan?
                     </div>
                 </div>
                 <div class="col-12 mt-md30 mt20">
                     <div class="f-20 f-md-22 w400 lh140 balck-clr2">
                       <span class="w700"> This is Simple.</span> It helps<br><br>
                     </div>
                 </div>
             
              <div class="col-12 mt0 mt-md20">
                     <div class="row flex-wrap">
                         <div class="col-12 col-md-5">
                             <div class="f-20 f-md-20 w400 lh140 balck-clr2">
                                 <ul class="noneed-listing pl0">
                                    <li> <span class="w700">Marketers</span> to drive more traffic</li>
                                    <li> <span class="w700">Affiliates,</span> make more commissions</li>
                                    <li> <span class="w700">SEO marketers</span> get #1 ranking and traffic</li>
                                    <li> <span class="w700">Social & Video Marketers</span> get free viral traffic</li>
                                    <li> <span class="w700">Creators</span> can get more income handsfree without extra efforts in video creation.</li> 
                                 </ul>
                             </div>
                         </div>
                         <div class="col-12 col-md-7 mt0 mt-md0">
                            <div class="f-20 f-md-20 w400 lh140 balck-clr2">
                                 <ul class="noneed-listing pl0">
                                    <li><span class="w700">Product sellers, </span> sell more with RED HOT traffic</li>
                                    <li><span class="w700">List builders</span>  skyrocket their subscribers...</li>
                                    <li><span class="w700">Freelancers and Agencies –</span>  Provide RED Hot service & make profits</li>
                                    <li><span class="w700">Local Business Owners – </span> get more engagement & TRAFFIC</li>
                                    <li>Weight Loss, Gaming, Real Estate, Health, Wealth –</span> <span class="w700">Any Kind of Business</span> </li>
                                 </ul>
                             </div>
                         </div>
                     </div>
                 </div>
             
              <div class="col-12 mt20 mt-md70 text-center">
                     
                         <div class="f-22 f-md-32 w700 lh140 red-clr">
                           It Works Seamlessly for ANY NICHE… All at the push of a button. 
                         </div>
                 </div>
             
                 <div class="col-12 mt20 mt-md50">
                     <div class="row align-items-center flex-wrap">
                         <div class="col-12 col-md-7">
                   <div class="f-20 f-md-26 w700 lh140 balck-clr2 text-center text-md-start">
                  LearnX Traffic Is Designed to Meet Every Marketer’s Need:
                         </div>
                             <div class="f-20 f-md-20 w400 lh140 balck-clr2 mt20">
                                 <ul class="noneed-listing pl0">
                                    <li>Anyone who wants a nice PASSIVE income while focusing on bigger projects</li>
                                    <li>Lazy people who want easy traffic & profits</li>
                                    <li>Anyone who wants to value their business and money and is not ready to sacrifice them</li>
                                    <li>People with products or services who want to kickstart online</li>  
                                 </ul>
                             </div>
                         </div>
                         <div class="col-12 col-md-5 mt20 mt-md0">
                             <img src="assets/images/platfrom.webp" class="img-fluid d-block mx-auto">
                         </div>
                     </div>
                 </div>
                 <div class="col-12 mt20 mt-md70 text-center">
                     <div class="look-shape">
                         <div class="f-22 f-md-30 w700 lh140 balck-clr2">
                           Look, it doesn't matter who you are or what you're doing.
                         </div>
                         <div class="f-20 f-md-22 w500 lh140 balck-clr2 mt10">
                           If you want to finally be successful online, make the money that you want and live in a way<br class="d-none d-md-block"> you dream of, LearnX is for you.
                         </div>
                     </div>
                 </div>
                 <div class="col-12 mt20 mt-md70">
                     <div class="f-md-45 f-28 lh140 w700 text-center balck-clr2">Even BETTER: It’s Entirely Newbie Friendly
                     </div>
                 </div>
                 <div class="col-12 mt20 mt-md50">
                     <div class="row align-items-center flex-wrap">
                         <div class="col-12 col-md-7">
                             <div class="f-18 f-md-20 w400 lh140 balck-clr2">
                              Whatever your level of technical expertise, this amazing software enables 
                              you to create engaging & highly professional video shorts for any niche in 
                              the 7-minute flat. It really doesn’t matter.
                              <br><br>
                              <span class="w700">The ease of use for LearnX is STUNNING,</span> allowing users to drive 
                              free viral traffic from YouTube, without ever having to turn their hair 
                              grey for complex video editing skills.
                              <br><br>
                              With LearnX, not only do you get the BEST possible system for the LOWEST 
                              price around, but you can also sleep safe knowing you CONTROL every aspect of your business, from building a stunning video marketing strategy to promoting affiliate products, to traffic generation, monetization, list building, and TON more - no need to ever outsource those tasks again!
                              
                             </div>
                         </div>
                         <div class="col-12 col-md-5 mt20 mt-md0">
                             <img src="assets/images/better.webp" class="img-fluid d-block mx-auto">
                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </div>
      <!-- Need Section End -->
      <!-- CTA Section -->
      <div class="cta-section-new">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-18 f-md-26 w700 lh140 text-center white-clr">
                     Free Commercial License + Low 1-Time Price For Launch Period Only
                  </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center">
                        <a href="#buynow" class="cta-link-btn">Get Instant Access To LearnX</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                     <img src="assets/images/compaitable-with1.webp" class="img-responsive mx-xs-center md-img-right" alt="visa">
                     <div class="d-md-block d-none visible-md px-md30">
                        <img src="assets/images/v-line.webp" class="img-fluid" alt="line">
                     </div>
                     <img src="assets/images/days-gurantee1.webp" class="img-responsive mx-xs-auto mt15 mt-md0" alt="30 days">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- CTA Section End -->

      <!---Bonus Section Starts-->
      <div class="bonus-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="but-design-border">
                     <div class="f-md-45 f-28 lh140 w700 white-clr button-shape">
                        We’re not done Yet!
                     </div>
                  </div>
               </div>
               <div class="col-12 mt30 mt-md30 f-20 f-md-22 w500 balck-clr2 lh140 text-center">
                  When You Grab Your LearnX Account Today, You’ll Also Get These <span class="w700">Fast Action Bonuses!</span>
               </div>
               <div class="col-12 ">
                  <div class="row ">
                     <!-------bonus 1------->
                     <div class="col-12 col-md-6 mt-md50 mt20 ">
                        <div class="bonus-section-shape ">
                           <div class="row align-items-center ">
                              <div class="col-md-6 mx-auto">
                                 <img src="assets/images/bonus-box.webp " class="img-fluid d-block mx-auto ">
                              </div>
                              <div class="col-12 mt20 mt-md40 ">
                                 <div class="w700 f-22 f-md-24 balck-clr2 lh140 ">
                                 Niche Maketing Secrets
                                 </div>
                                 <div class="f-18 f-md-20 w400 balck-clr2 lh140 mt15 ">
                                 A lot of online entrepreneurs think that once they’ve discovered a niche, they just need to build a website or a business.
                                 <br><br>
                                 But you have to build a brand. This is what people will gravitate to.
                                 <br><br> Your brand is a set of values that your community and a targeted audience would associate with your business.
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-------bonus 2------->
                     <div class="col-12 col-md-6 mt-md50 mt20 ">
                        <div class="bonus-section-shape col-12 ">
                           <div class="row align-items-center ">
                              <div class="col-md-6 mx-auto col-12">
                                 <img src="assets/images/bonus-box.webp " class="img-fluid d-block mx-auto ">
                              </div>
                              <div class="col-12 mt20 mt-md40 ">
                                 <div class="w700 f-22 f-md-24 balck-clr2 lh140 ">
                                    Video Training on Viral Marketing Secrets
                                 </div>
                                 <div class="f-18 f-md-20 w400 balck-clr2 lh140 mt15 ">
                                    Believe it or not, people interested in whatever it is you are promoting are already congregating online. Social media platforms want you to succeed. The more popular your content becomes, the more traffic they get. 
<br><br>
                                    So, with this video training you will discover a shortcut to online viral marketing secrets. This will cover - The Two-Step Trick to Effective Viral Marketing, How Do You Find Hot Content? Maximize Niche Targeting for Your Curated Content, remember to protect yourself when sharing others’ content, how to share viral content on Facebook, how to share viral content on Twitter, Filter your content format to go viral on many platforms and much more. 
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt-md30 mt20 ">
                  <div class="row ">
                     <!-------bonus 3------->
                     <div class="col-12 col-md-6 ">
                        <div class="bonus-section-shape col-12 ">
                           <div class="row align-items-center ">
                              <div class="col-md-6 mx-auto">
                                 <img src="assets/images/bonus-box.webp " class="img-fluid d-block mx-auto ">
                              </div>
                              <div class="col-12 mt20 mt-md50">
                                 <div class="w700 f-22 f-md-24 balck-clr2 lh140 "> Modern Video Marketing
                                 </div>
                                 <div class="f-18 f-md-20 w400 balck-clr2 lh140 mt15 ">
                                    It's about time for you to learn the ins and outs of successful online video marketing! <br><br>

                                    Regardless of what you've heard, video marketing as a whole is not exactly a new phenomenon. Video marketing content is increasingly plugged into a larger marketing infrastructure.  
                                    <br><br>
                                    You have to wrap your mind around the fact that modern video marketing is both new and old. Depending on how you navigate these factors, you will either succeed or fail. Either your video is going to convert people into buyers or they're just going to sit there on YouTube getting zero views. 
                                    <br><br>
                                    So, with this video training you will discover the secrets of successful video marketing- The Modern and Effective Ways of Video Marketing, Modern Video Marketing Essentials, Types of Video Marketing and much more 
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-------bonus 4------->
                     <div class="col-12 col-md-6 mt20 mt-md0 ">
                        <div class="bonus-section-shape col-12 ">
                           <div class="row align-items-center ">
                              <div class="col-md-6 mx-auto">
                                 <img src="assets/images/bonus-box.webp " class="img-fluid d-block mx-auto ">
                              </div>
                              <div class="col-12 mt20 mt-md40">
                                 <div class="w700 f-22 f-md-24 balck-clr2 lh140 ">
                                    Youtube Authority  
                                 </div>
                                 <div class="f-18 f-md-20 w400 balck-clr2 lh140 mt15 ">
                                    Since its launch in 2005, YouTube has come a long way. It has become an extremely powerful tool for businesses to increase awareness of their brand, drive more traffic to their company sites, and reach a broad audience around the world.So, if you are isn’t already leveraging the power of YouTube there are some massive benefits that you’re missing out on. 
<br><br>
                                    With this video course you will: <br>
                                    <div class="f-20 f-md-20 w400 lh140 balck-clr2">
                      
                                       <ul class="noneed-listing pl0">
<li>A Clear understanding on starting a YouTube channel.  </li>
<li>Determine your target audience.  </li>
<li>Learn about the different types of videos  </li>
<li>Discover how you can increase engagement  </li>
<li>Learn the different avenues for monetizing your YouTube channel </li>
<li>Learn about the different mistakes that you can make on your YouTube channel     </li>                           
       
                                       </ul>
                                   </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt30 mt-md70">
                  <div class="f-24 f-md-50 w700 balck-clr2 lh140 text-center ">
                     That’s A Total Value of <br class="d-none d-md-block "> <span class="f-28 f-md-50 red-clr"> $2285</span>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--Bonus Section ends -->

  <!-- Guarantee Section Start -->
  <div class="riskfree-section ">
   <div class="container ">
      <div class="row align-items-center ">
         <div class="col-12 text-center mb20 mb-md40">
            <div class="f-24 f-md-36 w600 lh140 white-clr">
               We’re Backing Everything Up with An Iron Clad...
            </div>
            <div class="f-md-45 f-28 w700 lh140 white-clr">
               "30-Day Risk-Free Money Back Guarantee"
            </div>
         </div>
         <div class="col-md-7 col-12 order-md-2">
            <div class="f-md-20 f-18 w400 lh140 white-clr mt15">
               I'm 100% confident that LearnX will help you take your online business to the next level. I'm so confident on it that I'm making this offer a risk-free investment for you.
<br><br>
               If you concluded that, HONESTLY nothing of this has helped you in any way, you can take advantage of our "30-day Money Back Guarantee" and simply ask for a refund within 30 days!
               <br><br>
               Note: For refund, we will require a valid reason along with the proof that you tried our system, but it didn't work for you!
               <br><br>
               I am considering your money to be kept safe on the table between us and waiting for you to APPLY and successfully use this product, and get best results, so then you can feel it is a great investment.
            </div>
         </div>
         <div class="col-md-5 order-md-1 col-12 mt20 mt-md0 ">
            <img src="assets/images/riskfree-img.webp " class="img-fluid d-block mx-auto ">
         </div>
      </div>
   </div>
</div>
<!-- Guarantee Section End -->

      <!-- Discounted Price Section Start -->
      <div class="table-section " id="buynow">
         <div class="container ">
            <div class="row ">
               <div class="col-12">
                  <div class="f-20 f-md-24 w500 text-center lh140">
                     Take Advantage of Our Limited Time Deal
                  </div>
                  <div class="f-md-48 f-28 w700 lh140 text-center mt10 ">
                     Get LearnX For A Low One-Time-<br class="d-none d-md-block">
                     Price, No Monthly Fee.
                  </div>
                  
               </div>
               <div class="col-12 col-md-8 mx-auto mt20 mt-md50 ">
                  <div class="row">
                     <div class="col-12 col-md-10 mx-auto mt20 mt-md0">
                        <div class="table-wrap1 ">
                           <div class="table-head1 ">
                           <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 484.58 200.84" style="max-height:75px;">
                        <defs>
                            <style>
                                .cls-1,.cls-4{fill:#fff;}
                                .cls-2,.cls-3,.cls-4{fill-rule:evenodd;}
                                .cls-2{fill:url(#linear-gradient);}
                                .cls-3{fill:url(#linear-gradient-2);}
                                .cls-5{fill:url(#linear-gradient-3);}
                            </style>
                            <linearGradient id="linear-gradient" x1="-154.67" y1="191.86" x2="249.18" y2="191.86" gradientTransform="matrix(1.19, -0.01, 0.05, 1, 178.46, -4.31)" gradientUnits="userSpaceOnUse">
                                <stop offset="0" stop-color="#ead40c"></stop>
                                <stop offset="1" stop-color="#ff00e9"></stop>
                            </linearGradient>
                            <linearGradient id="linear-gradient-2" x1="23.11" y1="160.56" x2="57.61" y2="160.56" gradientTransform="matrix(1, 0, 0, 1, 0, 0)" xlink:href="#linear-gradient"></linearGradient>
                            <linearGradient id="linear-gradient-3" x1="384.49" y1="98.33" x2="483.38" y2="98.33" gradientTransform="matrix(1, 0, 0, 1, 0, 0)" xlink:href="#linear-gradient"></linearGradient>
                        </defs>
                        <g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1">
                            <path class="cls-1" d="M112.31,154.3q-11.65,0-20.1-4.87a32.56,32.56,0,0,1-13-13.82q-4.53-9-4.54-21.11a46.06,46.06,0,0,1,4.57-21A34.29,34.29,0,0,1,92,79.37a36.11,36.11,0,0,1,19.31-5.06,39.65,39.65,0,0,1,13.54,2.29,31,31,0,0,1,11.3,7.09,33.32,33.32,0,0,1,7.75,12.18,49.23,49.23,0,0,1,2.82,17.57V119H83.26v-12.3h46A19.87,19.87,0,0,0,127,97.38a16.6,16.6,0,0,0-6.18-6.48,17.59,17.59,0,0,0-9.21-2.37,17.88,17.88,0,0,0-9.83,2.7,18.81,18.81,0,0,0-6.58,7.06,20.17,20.17,0,0,0-2.4,9.56v10.74a25.15,25.15,0,0,0,2.47,11.57,17.42,17.42,0,0,0,6.91,7.37,20.52,20.52,0,0,0,10.39,2.55,21.62,21.62,0,0,0,7.22-1.14,15.71,15.71,0,0,0,5.59-3.35,14.11,14.11,0,0,0,3.59-5.5L146,132a26.44,26.44,0,0,1-6.13,11.77,29.72,29.72,0,0,1-11.52,7.77A43.74,43.74,0,0,1,112.31,154.3Z"></path>
                            <path class="cls-1" d="M184.49,154.35a31.78,31.78,0,0,1-13.24-2.65,21.26,21.26,0,0,1-9.28-7.84,22.86,22.86,0,0,1-3.41-12.81A21.92,21.92,0,0,1,161,120.2a18.9,18.9,0,0,1,6.61-6.86,33.85,33.85,0,0,1,9.46-3.9,77.76,77.76,0,0,1,10.92-2q6.81-.7,11-1.28a15.91,15.91,0,0,0,6.18-1.82,4.25,4.25,0,0,0,1.94-3.86v-.3q0-5.7-3.38-8.83T194,88.28q-6.71,0-10.62,2.92a14.55,14.55,0,0,0-5.27,6.91l-17-2.42a27.51,27.51,0,0,1,6.66-11.83,29.46,29.46,0,0,1,11.35-7.16,43.73,43.73,0,0,1,14.83-2.39,47.89,47.89,0,0,1,11.14,1.31,31.6,31.6,0,0,1,10.14,4.31,22.12,22.12,0,0,1,7.39,8.14q2.81,5.15,2.8,12.87v51.85H207.84V142.14h-.61a22.1,22.1,0,0,1-4.66,6,22.35,22.35,0,0,1-7.52,4.49A30,30,0,0,1,184.49,154.35Zm4.74-13.42a19.7,19.7,0,0,0,9.53-2.19,16,16,0,0,0,6.23-5.83,15,15,0,0,0,2.19-7.91v-9.13a8.72,8.72,0,0,1-2.9,1.31,45.56,45.56,0,0,1-4.56,1.06c-1.68.3-3.35.57-5,.8l-4.29.61a32,32,0,0,0-7.32,1.81A12.29,12.29,0,0,0,178,125a9.76,9.76,0,0,0,1.82,13.39A16,16,0,0,0,189.23,140.93Z"></path>
                            <path class="cls-1" d="M243.8,152.79V75.31h17.7V88.23h.81a19.36,19.36,0,0,1,7.29-10.37,20,20,0,0,1,11.83-3.66c1,0,2.14,0,3.4.13a23.83,23.83,0,0,1,3.15.38V91.5a21,21,0,0,0-3.65-.73,38.61,38.61,0,0,0-4.82-.32,18.47,18.47,0,0,0-8.95,2.14,15.94,15.94,0,0,0-6.23,5.93,16.55,16.55,0,0,0-2.27,8.72v45.55Z"></path>
                            <path class="cls-1" d="M318.4,107.39v45.4H300.14V75.31h17.45V88.48h.91a22,22,0,0,1,8.55-10.34q5.86-3.84,14.55-3.83a27.58,27.58,0,0,1,14,3.43,23.19,23.19,0,0,1,9.28,9.93,34.22,34.22,0,0,1,3.26,15.79v49.33H349.87V106.28c0-5.17-1.34-9.23-4-12.15s-6.37-4.39-11.07-4.39a17,17,0,0,0-8.5,2.09,14.67,14.67,0,0,0-5.8,6A20,20,0,0,0,318.4,107.39Z"></path>
                            <path class="cls-2" d="M314.05,172.88c12.69-.14,27.32,2.53,40.82,2.61l-.09,1c1.07.15,1-.88,2-.78,1.42-.18,0,1.57,1.82,1.14.54-.06.08-.56-.32-.68.77,0,2.31.17,3,1.26,1.3,0-.9-1.34.85-.89,6,1.63,13.39,0,20,3.2,11.64.72,24.79,2.38,38,4.27,2.85.41,5.37,1.11,7.57,1.05,2-.05,5.68.78,8,1.08,2.53.34,5.16,1.56,7.13,1.65-1.18-.05,5.49-.78,4.54.76,5-.72,10.9,1.67,15.94,1.84.83-.07.69.53.67,1,1.23-1.17,3.32.56,4.28-.56.25,2,2.63-.48,3.3,1.61,2.23-1.39,4.83.74,7.55,1.37,1.91.44,5.29-.21,5.55,2.14-2.1-.13-5.12.81-11.41-1.09,1,.87-3.35.74-3.39-.64-1.81,1.94-6.28-1-7.79,1.19-1.26-.41,0-.72-.64-1.35-5,.55-8.09-1.33-12.91-1.56,2.57,2.44,6.08,3.16,10.06,3.22,1.28.15,0,.74,1,1.39,1.37-.19.75-.48,1.26-1.17,5.37,2.28,15.36,2.35,21,4.91-4.69-.63-11.38,0-15.15-2.09A148,148,0,0,1,441.58,196c-.53-.1-1.81.12-.7-.71-1.65.73-3.41.1-5.74-.22-37.15-5.15-80.47-7.78-115.75-9.76-39.54-2.22-76.79-3.22-116.44-2.72-61.62.78-119.59,7.93-175.21,13.95-5,.55-10,1.49-15.11,1.47-1.33-.32-2.46-1.91-1.25-3-2-.38-2.93.29-5-.15a9.83,9.83,0,0,1-1.25-3,13.73,13.73,0,0,1,6.42-2.94c.77-.54.12-.8.15-1.6a171,171,0,0,1,45.35-8.58c26.43-1.1,53.68-4.73,79.64-6,6.25-.3,11.72.06,18.41.14,8.31.1,19.09-1,29.19-.12,6.25.54,13.67-.55,21.16-.56,39.35-.09,71.16-.23,112.28,2C317.24,171.93,315.11,173.81,314.05,172.88Zm64.22,16.05-1.6-.2C376.2,190,378.42,190.26,378.27,188.93Z"></path>
                            <polygon class="cls-3" points="52.57 166.06 57.61 151.99 23.11 157.23 32.36 169.12 52.57 166.06"></polygon>
                            <polygon class="cls-4" points="44.69 183.24 51.75 168.35 33.86 171.06 44.69 183.24"></polygon>
                            <path class="cls-4" d="M.12,10.15l22,145.06,35.47-5.39-22-145a2.6,2.6,0,0,0,0-.4C35,.76,26.62-.95,16.81.54S-.52,6.16,0,9.77A2.62,2.62,0,0,0,.12,10.15Z"></path>
                            <path class="cls-5" d="M433.68,79.45l21-36.33h27.56L448.48,97.51l34.9,56H456l-22-37.76-21.94,37.76H384.49l34.9-56L385.72,43.12h27.35Z"></path>
                        </g>
                            </g>
                    </svg>
                              <div class="f-22 f-md-32 w700 lh140 text-center text-uppercase mt15 white-clr ">
                              Traffic Premium
                              </div>
                           </div>
                           <div class=" ">
                              <ul class="table-list1 pl0 f-18 f-md-20 lh140 w400 balck-clr2 mb0">
                                 
                              <li>Create Unlimited Reels & Shorts </li>
                              <li>Create Reels & Short Videos Just by Using One Keyword  </li>
                              <li>Create Boomerang Short Videos to Engage your Audience </li>
                              <li>Create Picture Video by Using Keyword Search  </li>
                              <li>Create Video Using Your Own Video Clips or Stock Videos </li>
                              <li>Create High-Quality Explanatory Whiteboard Video Shorts </li>
                              <li>Add VoiceOver to any Video</li>
                              <li>Add Background Music to any Video  </li>
                              <li>50 + Short frame Templates and 25+ Vector Images</li>
                              <li>100+ Stylish Fonts to make video more Presentable  </li>
                              <li>Eye Catchy Header Text Style to grab attention  </li>
                              <li>Add Waves with different color to your Short videos  </li>
                              <li>100+ social sharing platforms to share the videos for viral traffic </li>
                              <li>Add Your Brand Logo and/or Watermark to your Videos </li>
                              <li>Share Videos Directly to YouTube Shorts </li>
                              <li>Store up to 2 GB of Video, Audio, and other media files  </li>
                              <li>24*7 Customer Support</li>
                              <li>Commercial License Included</li>
                              <li>Provide High In-Demand Video Creation Services to your Clients </li>
                                 <li class="headline ">BONUSES WORTH $1,988 FREE!!!</li>
                                 <li>Fast Action Bonus 1 - Niche Maketing Secrets</li>
                                 <li>Fast Action Bonus 2 - Viral Marketing Secrets</li>
                                 <li>Fast Action Bonus 3 - Modern Video Marketing</li>
                                 <li>Fast Action Bonus 4 - Youtube Authority</li>
                              </ul>
                           </div>
                           <div class="table-btn">
                              <div class="hideme-button ">
                                 <a href="https://warriorplus.com/o2/buy/fbmn7d/zq6vyv/fbfhmp"><img src="https://warriorplus.com/o2/btn/fn200011000/fbmn7d/zq6vyv/373815" class="img-fluid mx-auto d-block"></a> 
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50 ">
                  <div class="f-22 f-md-32 w700 balck-clr2 px0 text-md-center lh140 ">If you have any query, simply reach to us at <a href="mailto:support@bizomart.com" class="red-clr">support@bizomart.com</a>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Discounted Price Section End -->
    
      <!------Final Section------->
      <div class="final-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="but-design-border">
                     <div class="f-md-45 f-28 lh140 w700 balck-clr2 button-shape">
                        FINAL CALL
                     </div>
                  </div>
               </div>
               <div class="col-12 passport-content">
                  <div class="col-12 f-24 f-md-32 lh140 w600 text-center balck-clr2 mt20">
                     You Still Have the Opportunity to Raise Your Game… <br> Remember, It’s Your Make-or-Break Time! 
                  </div>
               </div>
            </div>
            <!-- CTA Btn Section Start -->
            <div class="row mt20 mt-md40">
               <div class="col-12 text-center">
                  <div class="f-18 f-md-26 w700 lh140 text-center balck-clr2">
                     Free Commercial License + Low 1-Time Price For Launch Period Only
                  </div>
                  <div class="row">
                     <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center">
                        <a href="#buynow" class="cta-link-btn">Get Instant Access To LearnX</a>
                     </div>
                  </div>
                  <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                     <img src="assets/images/compaitable-with.webp" class="img-responsive mx-xs-center md-img-right" alt="visa">
                     <div class="d-md-block d-none visible-md px-md30">
                        <img src="assets/images/v-line.webp" class="img-fluid" alt="line">
                     </div>
                     <img src="assets/images/days-gurantee.webp" class="img-responsive mx-xs-auto mt15 mt-md0" alt="30 days">
                  </div>
               </div>
            </div>
            <!-- CTA Btn Section End -->
               <div class="col-12 mt25 mt-md40 text-center">
                    <a class="kapblue f-md-24 f-18 lh140 w500" href="https://warriorplus.com/o/nothanks/zq6vyv" target="_blank" class="link">
                    No thanks - I Don't Want To Creates 100s Reels, Boomerang, & Short Videos With Just One Keyword... For Floods Of FREE Traffic And Sales... Please Take Me To The Next Step To Get Access To My Purchase.
                    </a>
               </div>
         </div>
      </div>
      <!------Final Section End------->

      <!------Footer Section-------->
      <div class="footer-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
               <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 484.58 200.84" style="max-height:75px;">
                        <defs>
                            <style>
                                .cls-1,.cls-4{fill:#fff;}
                                .cls-2,.cls-3,.cls-4{fill-rule:evenodd;}
                                .cls-2{fill:url(#linear-gradient);}
                                .cls-3{fill:url(#linear-gradient-2);}
                                .cls-5{fill:url(#linear-gradient-3);}
                            </style>
                            <linearGradient id="linear-gradient" x1="-154.67" y1="191.86" x2="249.18" y2="191.86" gradientTransform="matrix(1.19, -0.01, 0.05, 1, 178.46, -4.31)" gradientUnits="userSpaceOnUse">
                                <stop offset="0" stop-color="#ead40c"></stop>
                                <stop offset="1" stop-color="#ff00e9"></stop>
                            </linearGradient>
                            <linearGradient id="linear-gradient-2" x1="23.11" y1="160.56" x2="57.61" y2="160.56" gradientTransform="matrix(1, 0, 0, 1, 0, 0)" xlink:href="#linear-gradient"></linearGradient>
                            <linearGradient id="linear-gradient-3" x1="384.49" y1="98.33" x2="483.38" y2="98.33" gradientTransform="matrix(1, 0, 0, 1, 0, 0)" xlink:href="#linear-gradient"></linearGradient>
                        </defs>
                        <g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1">
                            <path class="cls-1" d="M112.31,154.3q-11.65,0-20.1-4.87a32.56,32.56,0,0,1-13-13.82q-4.53-9-4.54-21.11a46.06,46.06,0,0,1,4.57-21A34.29,34.29,0,0,1,92,79.37a36.11,36.11,0,0,1,19.31-5.06,39.65,39.65,0,0,1,13.54,2.29,31,31,0,0,1,11.3,7.09,33.32,33.32,0,0,1,7.75,12.18,49.23,49.23,0,0,1,2.82,17.57V119H83.26v-12.3h46A19.87,19.87,0,0,0,127,97.38a16.6,16.6,0,0,0-6.18-6.48,17.59,17.59,0,0,0-9.21-2.37,17.88,17.88,0,0,0-9.83,2.7,18.81,18.81,0,0,0-6.58,7.06,20.17,20.17,0,0,0-2.4,9.56v10.74a25.15,25.15,0,0,0,2.47,11.57,17.42,17.42,0,0,0,6.91,7.37,20.52,20.52,0,0,0,10.39,2.55,21.62,21.62,0,0,0,7.22-1.14,15.71,15.71,0,0,0,5.59-3.35,14.11,14.11,0,0,0,3.59-5.5L146,132a26.44,26.44,0,0,1-6.13,11.77,29.72,29.72,0,0,1-11.52,7.77A43.74,43.74,0,0,1,112.31,154.3Z"></path>
                            <path class="cls-1" d="M184.49,154.35a31.78,31.78,0,0,1-13.24-2.65,21.26,21.26,0,0,1-9.28-7.84,22.86,22.86,0,0,1-3.41-12.81A21.92,21.92,0,0,1,161,120.2a18.9,18.9,0,0,1,6.61-6.86,33.85,33.85,0,0,1,9.46-3.9,77.76,77.76,0,0,1,10.92-2q6.81-.7,11-1.28a15.91,15.91,0,0,0,6.18-1.82,4.25,4.25,0,0,0,1.94-3.86v-.3q0-5.7-3.38-8.83T194,88.28q-6.71,0-10.62,2.92a14.55,14.55,0,0,0-5.27,6.91l-17-2.42a27.51,27.51,0,0,1,6.66-11.83,29.46,29.46,0,0,1,11.35-7.16,43.73,43.73,0,0,1,14.83-2.39,47.89,47.89,0,0,1,11.14,1.31,31.6,31.6,0,0,1,10.14,4.31,22.12,22.12,0,0,1,7.39,8.14q2.81,5.15,2.8,12.87v51.85H207.84V142.14h-.61a22.1,22.1,0,0,1-4.66,6,22.35,22.35,0,0,1-7.52,4.49A30,30,0,0,1,184.49,154.35Zm4.74-13.42a19.7,19.7,0,0,0,9.53-2.19,16,16,0,0,0,6.23-5.83,15,15,0,0,0,2.19-7.91v-9.13a8.72,8.72,0,0,1-2.9,1.31,45.56,45.56,0,0,1-4.56,1.06c-1.68.3-3.35.57-5,.8l-4.29.61a32,32,0,0,0-7.32,1.81A12.29,12.29,0,0,0,178,125a9.76,9.76,0,0,0,1.82,13.39A16,16,0,0,0,189.23,140.93Z"></path>
                            <path class="cls-1" d="M243.8,152.79V75.31h17.7V88.23h.81a19.36,19.36,0,0,1,7.29-10.37,20,20,0,0,1,11.83-3.66c1,0,2.14,0,3.4.13a23.83,23.83,0,0,1,3.15.38V91.5a21,21,0,0,0-3.65-.73,38.61,38.61,0,0,0-4.82-.32,18.47,18.47,0,0,0-8.95,2.14,15.94,15.94,0,0,0-6.23,5.93,16.55,16.55,0,0,0-2.27,8.72v45.55Z"></path>
                            <path class="cls-1" d="M318.4,107.39v45.4H300.14V75.31h17.45V88.48h.91a22,22,0,0,1,8.55-10.34q5.86-3.84,14.55-3.83a27.58,27.58,0,0,1,14,3.43,23.19,23.19,0,0,1,9.28,9.93,34.22,34.22,0,0,1,3.26,15.79v49.33H349.87V106.28c0-5.17-1.34-9.23-4-12.15s-6.37-4.39-11.07-4.39a17,17,0,0,0-8.5,2.09,14.67,14.67,0,0,0-5.8,6A20,20,0,0,0,318.4,107.39Z"></path>
                            <path class="cls-2" d="M314.05,172.88c12.69-.14,27.32,2.53,40.82,2.61l-.09,1c1.07.15,1-.88,2-.78,1.42-.18,0,1.57,1.82,1.14.54-.06.08-.56-.32-.68.77,0,2.31.17,3,1.26,1.3,0-.9-1.34.85-.89,6,1.63,13.39,0,20,3.2,11.64.72,24.79,2.38,38,4.27,2.85.41,5.37,1.11,7.57,1.05,2-.05,5.68.78,8,1.08,2.53.34,5.16,1.56,7.13,1.65-1.18-.05,5.49-.78,4.54.76,5-.72,10.9,1.67,15.94,1.84.83-.07.69.53.67,1,1.23-1.17,3.32.56,4.28-.56.25,2,2.63-.48,3.3,1.61,2.23-1.39,4.83.74,7.55,1.37,1.91.44,5.29-.21,5.55,2.14-2.1-.13-5.12.81-11.41-1.09,1,.87-3.35.74-3.39-.64-1.81,1.94-6.28-1-7.79,1.19-1.26-.41,0-.72-.64-1.35-5,.55-8.09-1.33-12.91-1.56,2.57,2.44,6.08,3.16,10.06,3.22,1.28.15,0,.74,1,1.39,1.37-.19.75-.48,1.26-1.17,5.37,2.28,15.36,2.35,21,4.91-4.69-.63-11.38,0-15.15-2.09A148,148,0,0,1,441.58,196c-.53-.1-1.81.12-.7-.71-1.65.73-3.41.1-5.74-.22-37.15-5.15-80.47-7.78-115.75-9.76-39.54-2.22-76.79-3.22-116.44-2.72-61.62.78-119.59,7.93-175.21,13.95-5,.55-10,1.49-15.11,1.47-1.33-.32-2.46-1.91-1.25-3-2-.38-2.93.29-5-.15a9.83,9.83,0,0,1-1.25-3,13.73,13.73,0,0,1,6.42-2.94c.77-.54.12-.8.15-1.6a171,171,0,0,1,45.35-8.58c26.43-1.1,53.68-4.73,79.64-6,6.25-.3,11.72.06,18.41.14,8.31.1,19.09-1,29.19-.12,6.25.54,13.67-.55,21.16-.56,39.35-.09,71.16-.23,112.28,2C317.24,171.93,315.11,173.81,314.05,172.88Zm64.22,16.05-1.6-.2C376.2,190,378.42,190.26,378.27,188.93Z"></path>
                            <polygon class="cls-3" points="52.57 166.06 57.61 151.99 23.11 157.23 32.36 169.12 52.57 166.06"></polygon>
                            <polygon class="cls-4" points="44.69 183.24 51.75 168.35 33.86 171.06 44.69 183.24"></polygon>
                            <path class="cls-4" d="M.12,10.15l22,145.06,35.47-5.39-22-145a2.6,2.6,0,0,0,0-.4C35,.76,26.62-.95,16.81.54S-.52,6.16,0,9.77A2.62,2.62,0,0,0,.12,10.15Z"></path>
                            <path class="cls-5" d="M433.68,79.45l21-36.33h27.56L448.48,97.51l34.9,56H456l-22-37.76-21.94,37.76H384.49l34.9-56L385.72,43.12h27.35Z"></path>
                        </g>
                            </g>
                    </svg>
                    <div class="f-14 f-md-16 w400 mt20 lh160 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
                    <div class=" mt20 mt-md40 white-clr"> <div class="f-20 w500 mt20 lh140 white-clr text-center"><script type="text/javascript" src="https://warriorplus.com/o2/disclaimer/fbmn7d" defer></script><div class="wplus_spdisclaimer"></div></div></div>
              </div>
              <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-16 f-md-18 w400 lh140 white-clr text-xs-center">Copyright © LearnX</div>
                  <ul class="footer-ul f-16 f-md-18 w400 white-clr text-center text-md-right">
                      <li><a href="https://support.bizomart.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                      <li><a href="http://getlearnx.com/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                      <li><a href="http://getlearnx.com/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                      <li><a href="http://getlearnx.com/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                      <li><a href="http://getlearnx.com/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                      <li><a href="http://getlearnx.com/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                      <li><a href="http://getlearnx.com/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
              </div>
          </div>
      </div>
  </div>
      <!------Footer Section End-------->
       
      <script type="text/javascript" src="assets/js/ouibounce.min.js"></script>
<div id="ouibounce-modal" style="z-index:9999999; display:none;">
   <div class="underlay"></div>
   <div class="modal-wrapper" style="display:block;">
      <div class="modal-bg">
		<button type="button" class="close" onclick="document.getElementById('ouibounce-modal').style.display = 'none';">
			X 
		</button>
		<div class="model-header">
      <div class="d-flex justify-content-center align-items-center gap-3">
            <img src="assets/images/hold.png" alt="" class="img-fluid d-block m131">
            <div>
               <div class="f-md-56 f-24 w700 lh130 white-clr">
                  WAIT! HOLD ON!!
               </div>
               <div class="f-md-28 f-18 w700 lh130 white-clr mt10 mt-md0">
                  Don't Leave Empty Handed
               </div>
            </div>
         </div>
		</div>
         <div class="col-12 for-padding">
			<div class="copun-line f-md-45 f-16 w500 lh130 text-center black-clr mt10">
         You've Qualified For An <span class="w700 red-clr"><br> INSTANT $40 DISCOUNT</span>  
			</div>
         <div class="copun-line f-md-30 f-16 w700 lh130 text-center black-clr mt10" style="background-color:yellow; padding:5px 10px; display:inline-block">
         Regular Price <strike>$77</strike>,
         Now Only $37
			</div>
			 <div class="mt-md20 mt10 text-center ">
				<a href="https://warriorplus.com/o2/buy/fbmn7d/zq6vyv/fbfhmp" class="cta-link-btn1">Grab SPECIAL Discount Now!
				<br>
				<span class="f-12 white-clr w600 text-uppercase">Act Now! Limited to First 100 Buyers Only.</span> </a>
			 </div>
      </div>
   </div>
</div>
<script type="text/javascript">
        var _ouibounce = ouibounce(document.getElementById('ouibounce-modal'),{
        aggressive: true,
        });
    </script>

   </body>
</html>
<!Doctype html>
<html>

<head>
    <title>VoiceFusion Ai Reseller</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=9">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta name="title" content="VoiceFusion Ai | Reseller">
    <meta name="description" content="VoiceFusion Ai">
    <meta name="keywords" content="VoiceFusion Ai">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="icon" href="../common_assets/images/favicon.webp" type="image/webp">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Spartan:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">

    <meta name="description" content="We’ve brought you a revolutionary method where you can Build Your Online Business worth 6-7 Figure EASILY By Selling VoiceFusion Ai Accounts!">
    <meta property="og:image" content="https://voicefusionai.com/reseller/thumbnail.png">
    <meta name="language" content="English">
    <meta name="revisit-after" content="1 days">
    <meta name="author" content="Pranshu Gupta">
    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:title" content="VoiceFusion Ai | Reseller">
    <meta property="og:description" content="We’ve brought you a revolutionary method where you can Build Your Online Business worth 6-7 Figure EASILY By Selling VoiceFusion Ai Accounts!">
    <meta property="og:description" content="VoiceFusion Ai">
    <meta property="og:image" content="https://voicefusionai.com/reseller/thumbnail.png">
    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:title" content="VoiceFusion Ai | Reseller">
    <meta property="twitter:description" content="We’ve brought you a revolutionary method where you can Build Your Online Business worth 6-7 Figure EASILY By Selling VoiceFusion Ai Accounts!">
    <meta property="twitter:description" content="VoiceFusion Ai">
    <meta property="twitter:image" content="https://voicefusionai.com/reseller/thumbnail.png">

    <!-- Start Editor required -->
    
    <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    <!-- Start Editor required -->
    <link rel="stylesheet" href="https://cdn.oppyotest.com/launches/sellero/common_assets/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="assets/css/style.css" type="text/css">
    <link rel="stylesheet" href="assets/css/timer.css" type="text/css">
    <script src="https://cdn.oppyotest.com/launches/sellero/common_assets/js/bootstrap.min.js"></script>
    <script src="https://cdn.oppyotest.com/launches/sellero/common_assets/js/jquery.min.js"></script>
    <!-- End -->

</head>
<body>
<div class="warning-section">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <div class="offer">
                        <div>
                            <img src="https://cdn.oppyo.com/launches/appzilo/special/error.webp">
                        </div>
                        <div class="f-22 f-md-30 w600 lh140 white-clr text-center">
                            WAIT! WAIT! YOUR ORDER IS NOT YET COMPLETED... *DO NOT CLOSE*
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--1. Header Section Start -->
    <div class="header-section">
        <div class="container">
            <div class="row ">
                <div class="col-12 px-md15">
                    <div class="text-center">
                      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 795 123.19" style="max-height: 70px;">
                          <defs>
                            <style>
                              .cls-1{fill:#fff;}
                              .cls-2,
                              .cls-4{fill:#ff7c24;}
                              .cls-3{fill:#15c5f3;}
                              .cls-4{fill-rule:evenodd;}
                            </style>
                          </defs>
                          <g id="Layer_2" data-name="Layer 2">
                          <g id="Layer_1-2" data-name="Layer 1">
                          <path class="cls-1" d="M151.7,38.52,168.39,91H169l16.73-52.49H202l-23.82,69.09H159.32L135.47,38.52Z"/>
                          <path class="cls-1" d="M227.5,108.62a26.33,26.33,0,0,1-13.58-3.36,22.68,22.68,0,0,1-8.82-9.38,30.07,30.07,0,0,1-3.1-14,30.25,30.25,0,0,1,3.1-14.05,22.68,22.68,0,0,1,8.82-9.38,29.12,29.12,0,0,1,27.16,0,22.68,22.68,0,0,1,8.82,9.38A30.25,30.25,0,0,1,253,81.9a30.07,30.07,0,0,1-3.1,14,22.68,22.68,0,0,1-8.82,9.38A26.33,26.33,0,0,1,227.5,108.62Zm.07-11.13a8.85,8.85,0,0,0,6-2,12.6,12.6,0,0,0,3.63-5.58,27,27,0,0,0,0-16.12,12.67,12.67,0,0,0-3.63-5.6,8.81,8.81,0,0,0-6-2.06,9,9,0,0,0-6.06,2.06,12.4,12.4,0,0,0-3.67,5.6,26.81,26.81,0,0,0,0,16.12,12.33,12.33,0,0,0,3.67,5.58A9.09,9.09,0,0,0,227.57,97.49Z"/>
                          <path class="cls-1" d="M269.6,49.11A7.75,7.75,0,0,1,264.12,47a6.9,6.9,0,0,1,0-10.27,8.09,8.09,0,0,1,10.94,0,6.93,6.93,0,0,1,0,10.28A7.72,7.72,0,0,1,269.6,49.11Zm-7.22,58.5V55.79h14.37v51.82Z"/>
                          <path class="cls-1" d="M311.64,108.62A26.34,26.34,0,0,1,298,105.23a22.59,22.59,0,0,1-8.78-9.43,30.48,30.48,0,0,1-3.05-13.9,30.19,30.19,0,0,1,3.09-14A22.84,22.84,0,0,1,298,58.5a26,26,0,0,1,13.56-3.39,26.84,26.84,0,0,1,11.87,2.47,19.81,19.81,0,0,1,8.07,6.91A20.31,20.31,0,0,1,334.78,75H321.22a10.66,10.66,0,0,0-3-6.26,8.81,8.81,0,0,0-6.4-2.38A9.63,9.63,0,0,0,306,68.12a11.59,11.59,0,0,0-3.87,5.24,22.47,22.47,0,0,0-1.38,8.34,22.91,22.91,0,0,0,1.37,8.43,11.56,11.56,0,0,0,3.86,5.3,10.25,10.25,0,0,0,10.27.81,8.64,8.64,0,0,0,3.27-2.95,11.27,11.27,0,0,0,1.71-4.68h13.56a21.3,21.3,0,0,1-3.22,10.44,19.64,19.64,0,0,1-7.95,7A26.54,26.54,0,0,1,311.64,108.62Z"/>
                          <path class="cls-1" d="M367.64,108.62a27.48,27.48,0,0,1-13.75-3.26A22,22,0,0,1,345,96.12a30.44,30.44,0,0,1-3.1-14.19,30.28,30.28,0,0,1,3.1-14,22.88,22.88,0,0,1,8.76-9.41,25.4,25.4,0,0,1,13.27-3.38,27.48,27.48,0,0,1,9.57,1.64,21.68,21.68,0,0,1,7.76,4.91,22.48,22.48,0,0,1,5.18,8.21,33,33,0,0,1,1.85,11.56v3.94H347.67v-8.9h30.22a11,11,0,0,0-1.35-5.5,9.81,9.81,0,0,0-3.72-3.76,10.92,10.92,0,0,0-5.52-1.37,11.1,11.1,0,0,0-5.79,1.5,10.82,10.82,0,0,0-3.93,4,11.42,11.42,0,0,0-1.45,5.58v8.47a14.33,14.33,0,0,0,1.44,6.64,10.22,10.22,0,0,0,4.06,4.32,12.33,12.33,0,0,0,6.24,1.52,13.61,13.61,0,0,0,4.39-.67,9.14,9.14,0,0,0,3.41-2,8.94,8.94,0,0,0,2.16-3.3l13.29.87a18.14,18.14,0,0,1-4.14,8.35,20.71,20.71,0,0,1-8,5.53A30.14,30.14,0,0,1,367.64,108.62Z"/>
                          <path class="cls-2" d="M401.07,107.61V38.52h45.74v12H415.68V67h28.1v12h-28.1v28.54Z"/>
                          <path class="cls-2" d="M486.35,85.54V55.79h14.37v51.82H486.93V98.19h-.54a15.3,15.3,0,0,1-5.82,7.32,17.14,17.14,0,0,1-9.9,2.77,17.51,17.51,0,0,1-9.15-2.36,16.07,16.07,0,0,1-6.15-6.71,23,23,0,0,1-2.25-10.43v-33h14.37V86.22A10.51,10.51,0,0,0,470,93.47a8.37,8.37,0,0,0,6.51,2.67,10.23,10.23,0,0,0,4.86-1.2A9.39,9.39,0,0,0,485,91.38,11.1,11.1,0,0,0,486.35,85.54Z"/>
                          <path class="cls-2" d="M555.31,70.56l-13.16.81a6.9,6.9,0,0,0-1.45-3.05,7.89,7.89,0,0,0-2.92-2.19,10.24,10.24,0,0,0-4.3-.83,10.64,10.64,0,0,0-5.63,1.4,4.23,4.23,0,0,0-2.3,3.73A4,4,0,0,0,527,73.57a11.72,11.72,0,0,0,5.09,2.05l9.38,1.89q7.56,1.56,11.27,5a11.75,11.75,0,0,1,3.71,9,14.2,14.2,0,0,1-3,8.94,19.55,19.55,0,0,1-8.16,6,31.16,31.16,0,0,1-11.93,2.14q-10.29,0-16.38-4.3a16.67,16.67,0,0,1-7.13-11.73L524,91.85a7.26,7.26,0,0,0,3.11,4.78,13.18,13.18,0,0,0,12.4.16A4.43,4.43,0,0,0,541.88,93a4,4,0,0,0-1.65-3.22,12,12,0,0,0-5-1.94l-9-1.79q-7.59-1.51-11.28-5.26a13,13,0,0,1-3.7-9.55,13.88,13.88,0,0,1,2.72-8.6,17.16,17.16,0,0,1,7.66-5.57,31.41,31.41,0,0,1,11.58-2q9.82,0,15.47,4.15A16.06,16.06,0,0,1,555.31,70.56Z"/>
                          <path class="cls-2" d="M572.78,49.11A7.75,7.75,0,0,1,567.3,47a6.9,6.9,0,0,1,0-10.27,8.09,8.09,0,0,1,10.94,0,6.93,6.93,0,0,1,0,10.28A7.72,7.72,0,0,1,572.78,49.11Zm-7.22,58.5V55.79h14.37v51.82Z"/>
                          <path class="cls-2" d="M614.82,108.62a26.3,26.3,0,0,1-13.58-3.36,22.64,22.64,0,0,1-8.83-9.38,30.19,30.19,0,0,1-3.1-14,30.37,30.37,0,0,1,3.1-14.05,22.64,22.64,0,0,1,8.83-9.38,29.1,29.1,0,0,1,27.15,0,22.64,22.64,0,0,1,8.83,9.38,30.37,30.37,0,0,1,3.1,14.05,30.19,30.19,0,0,1-3.1,14,22.64,22.64,0,0,1-8.83,9.38A26.29,26.29,0,0,1,614.82,108.62Zm.06-11.13a8.82,8.82,0,0,0,6-2,12.52,12.52,0,0,0,3.63-5.58,27,27,0,0,0,0-16.12,12.59,12.59,0,0,0-3.63-5.6,8.78,8.78,0,0,0-6-2.06,9,9,0,0,0-6,2.06,12.41,12.41,0,0,0-3.68,5.6,27,27,0,0,0,0,16.12,12.35,12.35,0,0,0,3.68,5.58A9.09,9.09,0,0,0,614.88,97.49Z"/>
                          <path class="cls-2" d="M664.07,77.65v30H649.7V55.79h13.7v9.14h.6a14.6,14.6,0,0,1,5.77-7.17,17.59,17.59,0,0,1,9.82-2.65A18.26,18.26,0,0,1,689,57.47a16,16,0,0,1,6.24,6.74,22.57,22.57,0,0,1,2.23,10.4v33H683.1V77.18c0-3.18-.79-5.65-2.43-7.44a8.73,8.73,0,0,0-6.78-2.68,10.2,10.2,0,0,0-5.11,1.24,8.56,8.56,0,0,0-3.44,3.63A12.45,12.45,0,0,0,664.07,77.65Z"/>
                          <path class="cls-1" d="M721,107.61H705.29l23.86-69.09H748l23.82,69.09H756.13L738.83,54.3h-.54Zm-1-27.16h37v11.4H720Z"/>
                          <path class="cls-1" d="M787.24,49.11A7.76,7.76,0,0,1,781.75,47a6.92,6.92,0,0,1,0-10.27,8.1,8.1,0,0,1,11,0,6.93,6.93,0,0,1,0,10.28A7.76,7.76,0,0,1,787.24,49.11ZM780,107.61V55.79h14.37v51.82Z"/>
                          <path class="cls-3" d="M.46,79.85a45.62,45.62,0,0,0,91.13,0,2.16,2.16,0,0,0-2.15-2.28H58.74a12.72,12.72,0,0,1-25.43,0H2.61A2.17,2.17,0,0,0,.46,79.85ZM46,96.62A18.94,18.94,0,0,0,59.49,91l15,14.95A40,40,0,0,1,46,117.77Z"/>
                          <path class="cls-3" d="M46,83.77a6.21,6.21,0,0,0,6.2-6.2H39.82A6.21,6.21,0,0,0,46,83.77Z"/>
                          <path class="cls-3" d="M52.9,3v7.55a3,3,0,0,1-3,3H48.32v6.79H43.74V13.57H42.2a3,3,0,0,1-3-3V3a3,3,0,0,1,3-3h7.65A3,3,0,0,1,52.9,3Z"/>
                          <path class="cls-2" d="M96.76,36.77V29.46a2.05,2.05,0,0,1,2-2.06h0a2.06,2.06,0,0,1,2.06,2.06v7.31a2.06,2.06,0,0,1-2.06,2.06h0A2.05,2.05,0,0,1,96.76,36.77Z"/>
                          <path class="cls-2" d="M103.2,36.77V29.46a2.06,2.06,0,0,1,2.06-2.06h0a2.06,2.06,0,0,1,2.06,2.06v7.31a2.06,2.06,0,0,1-2.06,2.06h0A2.06,2.06,0,0,1,103.2,36.77Z"/>
                          <rect class="cls-2" x="109.65" y="27.4" width="4.11" height="11.43" rx="0.91"/>
                          <path class="cls-2" d="M116.1,36.77V29.46a2.05,2.05,0,0,1,2.05-2.06h0a2.06,2.06,0,0,1,2.06,2.06v7.31a2.06,2.06,0,0,1-2.06,2.06h0A2.05,2.05,0,0,1,116.1,36.77Z"/>
                          <path class="cls-2" d="M96.76,50.86V43.55a2.05,2.05,0,0,1,2-2.06h0a2.06,2.06,0,0,1,2.06,2.06v7.31a2.06,2.06,0,0,1-2.06,2.06h0A2.05,2.05,0,0,1,96.76,50.86Z"/>
                          <path class="cls-2" d="M103.2,50.86V43.55a2.06,2.06,0,0,1,2.06-2.06h0a2.06,2.06,0,0,1,2.06,2.06v7.31a2.06,2.06,0,0,1-2.06,2.06h0A2.06,2.06,0,0,1,103.2,50.86Z"/>
                          <path class="cls-2" d="M96.76,65V57.64a2.05,2.05,0,0,1,2-2h0a2.05,2.05,0,0,1,2.06,2V65A2.05,2.05,0,0,1,98.81,67h0A2.05,2.05,0,0,1,96.76,65Z"/>
                          <path class="cls-2" d="M103.2,65V57.64a2.05,2.05,0,0,1,2.06-2h0a2.05,2.05,0,0,1,2.06,2V65A2.05,2.05,0,0,1,105.26,67h0A2.05,2.05,0,0,1,103.2,65Z"/>
                          <rect class="cls-2" x="109.65" y="55.59" width="4.11" height="11.43" rx="0.91"/>
                          <path class="cls-4" d="M83.53,20.36h-75A8.52,8.52,0,0,0,0,28.87v40a2.3,2.3,0,0,0,2.29,2.29H89.76a2.3,2.3,0,0,0,2.29-2.29v-40A8.52,8.52,0,0,0,83.53,20.36ZM28.85,56.08A10.31,10.31,0,1,1,39.16,45.77,10.31,10.31,0,0,1,28.85,56.08Zm34.35,0A10.31,10.31,0,1,1,73.5,45.77,10.31,10.31,0,0,1,63.2,56.08Z"/>
                          </g>
                        </g>
                      </svg>
                    </div>
                    <!-- <div class="col-12 text-center mt20 mt-md50 f-16 f-md-18 w700 lh160 white-clr">
                        We've NEVER done this before...
                    </div> -->
                    <div class="col-12 text-center lh150 mt20 mt-md50">
                        <div class="pre-heading f-18 f-md-22 w600 lh140 white-clr">
                            We've NEVER done this before...
                          </div>
                     </div>
                     <div class="col-12 mt-md30 mt20 f-md-45 f-28 w400 text-center white-clr lh140">
                        We’ve brought you <span class="w800 under orange-clr"> a revolutionary method </span> 
                        where you can <span class="w800 under orange-clr">Build Your Online Business </span>
                        worth 6-7 Figure EASILY By <span class="w800 under orange-clr">Selling VoiceFusion Ai Accounts! </span>
                    </div>
                    <div class="col-12 mt-md25 mt20">
                        <div class="f-20 f-md-22 w500 lh140 white-clr text-center">
                          You just have to Sit Back & Get Paid.. Where We Will Do All The Hardwork FOR YOU! 
                        </div>
                    </div>
                    <div class="col-12 mt-md40 mt20 text-center">
                      <div class="f-18 f-md-22 w600 lh140 black-clr post-heading">
                        Zero Experience Needed | Zero Technical Skills Required | Get 100% Instant Commissions
                      </div>
                    </div>
                <div class="col-12 col-md-10 mx-auto mt-md30 mt20 mx-auto">
                    <!-- <div class="responsive-video" editabletype="video">
                        <iframe src="https://coursesify.dotcompal.com/video/embed/97z38agmqx" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                     box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                    </div> -->
                    <div>
                        <img src="assets/images/product-box.webp" alt="" class="img-fluid d-block mx-auto">
                    </div>
                </div>
                <div class="col-12 mt30 mt-md50 f-md-24 f-20 lh140 w400 text-center white-clr">
                    Start your own software business to get results like Gurus starting Today
                </div>
                <div class="col-md-10 mx-auto col-12 mt20 f-20 f-md-35 text-center up-btn-header">
                    <a href="#buynow" class="text-center"> Get Instant Access to VoiceFusion Ai Reseller </a>
                    <img src="assets/images/home1_2.webp" class="img-fluid mx-auto d-block mt20">
                </div>
            </div>
        </div>
    </div></div>
    <!--1. Header Section End -->
    <!--2. Second Section Start -->
    <div class="second-sec">
        <div class="container">
            <div class="row m0">
                <div class="col-12 text-center text-capitalize p0">
                    <div class="f-md-45 f-28 w700 lh160 text-capitalize text-center black-clr">
                       MAKE 6-7 Figures In 3 Easy Steps Using <br class="d-none d-lg-block">VoiceFusion Ai Reseller License
                    </div>
                </div>
                <div class="col-12 col-md-10 boxes mt20 mt-md30 mx-auto">
                    <div class="row align-items-center flex-wrap">
                        <div class="col-md-8 col-12 ">
                            <ul class="bigboxes">
                                <li class="f-20 f-md-26">
                                  <span class="w700 black-clr lh140">Use Our Pre-provided,Tested & High Converting Sales Material (Pages, Funnel, Videos) to Sell VoiceFusion Ai.</span> <br>
                                    <span class="f-16 f-md-18 w400 mt5 lh160 pt0 pt-md-20 d-block">
                                      Use all the similar High converting Sales Page and other material which we use to CONVERT YOU! Get complimentary access to tested and proven marketing material to reap the best Conversions!
                                    </span>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-4 col-12 mt20 mt-md0">
                            <img src="assets/images/profit.webp" class="img-fluid mx-auto d-block">
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-10 boxes mt20 mt-md30 mx-auto">
                    <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-md-8 col-12 order-md-2">
                            <ul class="bigboxes">
                                <li class="f-20 f-md-26"><span class="w700 black-clr lh160">Accept Payments directly In your Paypal or Stripe or Bank Account.</span> <br>
                                    <span class="f-16 f-md-18 w400 mt5 lh160 pt0 pt-md-20 d-block">Today you start selling a winning software to succeed like gurus do.<br>
                              And even better thing is, you can do it all without an initial investment of $5000-10,000 in building it. We already covered it for you.
                              </span>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-4 col-12 mt20 mt-md0 order-md-1">
                            <img src="assets/images/invest.webp" class="img-fluid mx-auto d-block">
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-10 boxes mt20 mt-md30 mx-auto">
                    <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-md-7 col-12">
                            <ul class="bigboxes">
                                <li class="f-20 f-md-26">
                                  <span class="w700 black-clr lh140">Create Unlimited accounts for your customers with a Simple 1 Click procedure!  </span> <br>
                                    <span class="f-16 f-md-18 w400 mt5 lh160 pt0 pt-md-20 d-block">
                                    Simply enter the Email ID of Your New Customer In VoiceFusion Ai Reseller Panel and Create their Account In Seconds with Just a Click.
                                    </span>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-5 col-12 mt20 mt-md0">
                            <img src="assets/images/skills.webp" class="img-fluid mx-auto d-block">
                        </div>
                    </div>
                </div>
               
                <!-- <div class="col-12 f-md-45 f-28 w700 lh160 text-capitalize text-center black-clr mt20 mt-md50 px-md15 px0">
                    YES- You'll get to use all our...
                </div>
                <div class="col-12 col-md-8 text-center mt20 mt-md30 mx-auto">
                    <div class="row">
                        <div class="col-md-6 col-12">
                            <ul class="bigboxes1 f-18 f-md-20 w500">
                                <li>Marketing Pages</li>
                                <li>Sales Videos</li>
                            </ul>
                        </div>
                        <div class="col-md-6 col-12">
                            <ul class="bigboxes1 f-18 f-md-20 w500">
                                <li>Members Area</li>
                                <li>Sales funnels etc.</li>
                            </ul>
                        </div>
                    </div>
                </div> -->
                <!-- <div class="col-12 f-18 f-md-21 w400 lh160 text-center mt20 mt-md30">
                    It’s extremely simple and easy. This means you don’t need to invest your time and money in any grunt work. All you need to do is use everything we’ve provided and sit back and see your benefits pouring in.
                </div> -->
            </div>
        </div>
    </div>
    <!--2. Second Section End -->

    <!--3. Third Section Start -->
    <div class="grey-section">
        <div class="container">
            <div class="row">
              <div class="col-12 text-center">
                <div class="f-24 f-md-45 lh160 w600">Here’s a Cherry On Top for YOU!</div>
                <div class="f-22 f-md-32 w600 lh160 mt10 red_clr">
                   Our Professionals will handle all the Support Tickets for you.
                </div>
                <div class="f-18 f-md-20 w400 lh160 mt10 ">
                  So,That You Can Simple Concentrate on Marketing, Sales & Making More Profits.
                </div>
                <div class="col-md-10 mx-auto col-12 mt20 mt-md40 f-20 f-md-35 text-center up-btn-header">
                    <a href="#buynow" class="text-center"> Get Instant Access to VoiceFusion Ai Reseller </a>
                    <img src="assets/images/home1_3.webp" class="img-fluid mx-auto d-block mt20">
                </div>
              </div>
                <!-- <div class="col-12">
                    <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-md-7 col-12">
                            <div class="f-24 f-md-35 lh160 w600">And You Know What The Best Part Is?
                            </div>
                            <div class="f-18 f-md-20 w400 lh160 mt20 mt-md30 text-left">
                                We will take care of all the customer support
                            </div>
                            <div class="f-18 f-md-20 w400 lh160 mt20 mt-md30 text-left">
                                We have a dedicated help-desk available to handle support, product delivery and training to make it Hassle FREE for you. You don’t even need to do anything…
                            </div>
                        </div>
                        <div class="col-md-5 col-12 mt20 mt-md0">
                            <img src="assets/images/telephone.webp" class="img-fluid mx-auto d-block">
                        </div>
                    </div>
                </div>
                <div class="col-12 mt20 mt-md70">
                    <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-md-7 col-12 order-md-2">
                            <div class="f-24 f-md-36 lh160 w600">And YES- You Keep 100% Profits. We have SOME MORE for you.</div>
                            <div class="f-18 f-md-22 w400 lh160 mt20 mt-md30 text-left">
                                You’ll be able to start getting sales, right after the 4-day launch period gets over & keep 100% profits.
                                <br><br> Along with this, <span class="w600">you’ll be delighted to know that, we’re also giving 50% commissions</span> over the entire funnel as a complementary gesture.
                                <br><br> So, you can make up for your investment only by having one sale itself.
                            </div>
                        </div>
                        <div class="col-md-5 col-12 mt20 mt-md0 order-md-1">
                            <img src="assets/images/commision.webp" class="img-fluid mx-auto d-block">
                        </div>
                    </div>
                </div>
                <div class="col-12 mt20 mt-md70">
                    <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-12">
                            <div class="f-18 f-md-20 w400 lh160 text-left">
                                And, VoiceFusion Ai removes all the hassles and streamlines academy site creation that skyrockets commissions and profits. SO, isn’t that a BIG SOLUTION for marketers.<br><br>
                            </div>
                            <div class="f-24 f-md-36 w700 lh160 text-center">
                                There's a HUGE DEMAND<br><br>
                            </div>
                            <div class="f-18 f-md-20 w400 lh160 text-left">
                                Website building is HOT… marketers whether newbie or experienced, can make tons of money & list by creating stunning funnel sites
                                <br><br>
                                <span class="w600">Marketers & businesses of all size - whether newbie or experienced need this.</span> There are OVER 50Mn businesses & marketers out there - Hungry & looking for a solution like this.
                                <br><br> They can drive FREE traffic & make tons of money by creating beautiful funnel sites. And, VoiceFusion Ai removes all the hassles and streamlines this process that skyrockets commissions and profits.
                                <br><br> What if you could give those hungry buyers a feature-packed software that creates stunning funnel sites pre loaded with done for you courses that gets targeted traffic and sales?
                                <br><br>
                                <span class="w600">They will bite your hands to get VoiceFusion Ai from you for any amount and you keep all benefits in your pocket with this, opportunity.</span>
                            </div>

                        </div>
                    </div>
                </div> -->
            </div>
        </div>
    </div>
    <!--3. Third Section End -->
    <!--4. Fourth Section Starts -->
    <div class="fifth-section">
        <div class="container">
            <div class="row">
                <div class="col-12 f-28 f-md-45 w700 text-center white-clr lh160">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 795 123.19" style="max-height: 70px;">
                          <defs>
                            <style>
                              .cls-1{fill:#fff;}
                              .cls-2,
                              .cls-4{fill:#ff7c24;}
                              .cls-3{fill:#15c5f3;}
                              .cls-4{fill-rule:evenodd;}
                            </style>
                          </defs>
                          <g id="Layer_2" data-name="Layer 2">
                          <g id="Layer_1-2" data-name="Layer 1">
                          <path class="cls-1" d="M151.7,38.52,168.39,91H169l16.73-52.49H202l-23.82,69.09H159.32L135.47,38.52Z"/>
                          <path class="cls-1" d="M227.5,108.62a26.33,26.33,0,0,1-13.58-3.36,22.68,22.68,0,0,1-8.82-9.38,30.07,30.07,0,0,1-3.1-14,30.25,30.25,0,0,1,3.1-14.05,22.68,22.68,0,0,1,8.82-9.38,29.12,29.12,0,0,1,27.16,0,22.68,22.68,0,0,1,8.82,9.38A30.25,30.25,0,0,1,253,81.9a30.07,30.07,0,0,1-3.1,14,22.68,22.68,0,0,1-8.82,9.38A26.33,26.33,0,0,1,227.5,108.62Zm.07-11.13a8.85,8.85,0,0,0,6-2,12.6,12.6,0,0,0,3.63-5.58,27,27,0,0,0,0-16.12,12.67,12.67,0,0,0-3.63-5.6,8.81,8.81,0,0,0-6-2.06,9,9,0,0,0-6.06,2.06,12.4,12.4,0,0,0-3.67,5.6,26.81,26.81,0,0,0,0,16.12,12.33,12.33,0,0,0,3.67,5.58A9.09,9.09,0,0,0,227.57,97.49Z"/>
                          <path class="cls-1" d="M269.6,49.11A7.75,7.75,0,0,1,264.12,47a6.9,6.9,0,0,1,0-10.27,8.09,8.09,0,0,1,10.94,0,6.93,6.93,0,0,1,0,10.28A7.72,7.72,0,0,1,269.6,49.11Zm-7.22,58.5V55.79h14.37v51.82Z"/>
                          <path class="cls-1" d="M311.64,108.62A26.34,26.34,0,0,1,298,105.23a22.59,22.59,0,0,1-8.78-9.43,30.48,30.48,0,0,1-3.05-13.9,30.19,30.19,0,0,1,3.09-14A22.84,22.84,0,0,1,298,58.5a26,26,0,0,1,13.56-3.39,26.84,26.84,0,0,1,11.87,2.47,19.81,19.81,0,0,1,8.07,6.91A20.31,20.31,0,0,1,334.78,75H321.22a10.66,10.66,0,0,0-3-6.26,8.81,8.81,0,0,0-6.4-2.38A9.63,9.63,0,0,0,306,68.12a11.59,11.59,0,0,0-3.87,5.24,22.47,22.47,0,0,0-1.38,8.34,22.91,22.91,0,0,0,1.37,8.43,11.56,11.56,0,0,0,3.86,5.3,10.25,10.25,0,0,0,10.27.81,8.64,8.64,0,0,0,3.27-2.95,11.27,11.27,0,0,0,1.71-4.68h13.56a21.3,21.3,0,0,1-3.22,10.44,19.64,19.64,0,0,1-7.95,7A26.54,26.54,0,0,1,311.64,108.62Z"/>
                          <path class="cls-1" d="M367.64,108.62a27.48,27.48,0,0,1-13.75-3.26A22,22,0,0,1,345,96.12a30.44,30.44,0,0,1-3.1-14.19,30.28,30.28,0,0,1,3.1-14,22.88,22.88,0,0,1,8.76-9.41,25.4,25.4,0,0,1,13.27-3.38,27.48,27.48,0,0,1,9.57,1.64,21.68,21.68,0,0,1,7.76,4.91,22.48,22.48,0,0,1,5.18,8.21,33,33,0,0,1,1.85,11.56v3.94H347.67v-8.9h30.22a11,11,0,0,0-1.35-5.5,9.81,9.81,0,0,0-3.72-3.76,10.92,10.92,0,0,0-5.52-1.37,11.1,11.1,0,0,0-5.79,1.5,10.82,10.82,0,0,0-3.93,4,11.42,11.42,0,0,0-1.45,5.58v8.47a14.33,14.33,0,0,0,1.44,6.64,10.22,10.22,0,0,0,4.06,4.32,12.33,12.33,0,0,0,6.24,1.52,13.61,13.61,0,0,0,4.39-.67,9.14,9.14,0,0,0,3.41-2,8.94,8.94,0,0,0,2.16-3.3l13.29.87a18.14,18.14,0,0,1-4.14,8.35,20.71,20.71,0,0,1-8,5.53A30.14,30.14,0,0,1,367.64,108.62Z"/>
                          <path class="cls-2" d="M401.07,107.61V38.52h45.74v12H415.68V67h28.1v12h-28.1v28.54Z"/>
                          <path class="cls-2" d="M486.35,85.54V55.79h14.37v51.82H486.93V98.19h-.54a15.3,15.3,0,0,1-5.82,7.32,17.14,17.14,0,0,1-9.9,2.77,17.51,17.51,0,0,1-9.15-2.36,16.07,16.07,0,0,1-6.15-6.71,23,23,0,0,1-2.25-10.43v-33h14.37V86.22A10.51,10.51,0,0,0,470,93.47a8.37,8.37,0,0,0,6.51,2.67,10.23,10.23,0,0,0,4.86-1.2A9.39,9.39,0,0,0,485,91.38,11.1,11.1,0,0,0,486.35,85.54Z"/>
                          <path class="cls-2" d="M555.31,70.56l-13.16.81a6.9,6.9,0,0,0-1.45-3.05,7.89,7.89,0,0,0-2.92-2.19,10.24,10.24,0,0,0-4.3-.83,10.64,10.64,0,0,0-5.63,1.4,4.23,4.23,0,0,0-2.3,3.73A4,4,0,0,0,527,73.57a11.72,11.72,0,0,0,5.09,2.05l9.38,1.89q7.56,1.56,11.27,5a11.75,11.75,0,0,1,3.71,9,14.2,14.2,0,0,1-3,8.94,19.55,19.55,0,0,1-8.16,6,31.16,31.16,0,0,1-11.93,2.14q-10.29,0-16.38-4.3a16.67,16.67,0,0,1-7.13-11.73L524,91.85a7.26,7.26,0,0,0,3.11,4.78,13.18,13.18,0,0,0,12.4.16A4.43,4.43,0,0,0,541.88,93a4,4,0,0,0-1.65-3.22,12,12,0,0,0-5-1.94l-9-1.79q-7.59-1.51-11.28-5.26a13,13,0,0,1-3.7-9.55,13.88,13.88,0,0,1,2.72-8.6,17.16,17.16,0,0,1,7.66-5.57,31.41,31.41,0,0,1,11.58-2q9.82,0,15.47,4.15A16.06,16.06,0,0,1,555.31,70.56Z"/>
                          <path class="cls-2" d="M572.78,49.11A7.75,7.75,0,0,1,567.3,47a6.9,6.9,0,0,1,0-10.27,8.09,8.09,0,0,1,10.94,0,6.93,6.93,0,0,1,0,10.28A7.72,7.72,0,0,1,572.78,49.11Zm-7.22,58.5V55.79h14.37v51.82Z"/>
                          <path class="cls-2" d="M614.82,108.62a26.3,26.3,0,0,1-13.58-3.36,22.64,22.64,0,0,1-8.83-9.38,30.19,30.19,0,0,1-3.1-14,30.37,30.37,0,0,1,3.1-14.05,22.64,22.64,0,0,1,8.83-9.38,29.1,29.1,0,0,1,27.15,0,22.64,22.64,0,0,1,8.83,9.38,30.37,30.37,0,0,1,3.1,14.05,30.19,30.19,0,0,1-3.1,14,22.64,22.64,0,0,1-8.83,9.38A26.29,26.29,0,0,1,614.82,108.62Zm.06-11.13a8.82,8.82,0,0,0,6-2,12.52,12.52,0,0,0,3.63-5.58,27,27,0,0,0,0-16.12,12.59,12.59,0,0,0-3.63-5.6,8.78,8.78,0,0,0-6-2.06,9,9,0,0,0-6,2.06,12.41,12.41,0,0,0-3.68,5.6,27,27,0,0,0,0,16.12,12.35,12.35,0,0,0,3.68,5.58A9.09,9.09,0,0,0,614.88,97.49Z"/>
                          <path class="cls-2" d="M664.07,77.65v30H649.7V55.79h13.7v9.14h.6a14.6,14.6,0,0,1,5.77-7.17,17.59,17.59,0,0,1,9.82-2.65A18.26,18.26,0,0,1,689,57.47a16,16,0,0,1,6.24,6.74,22.57,22.57,0,0,1,2.23,10.4v33H683.1V77.18c0-3.18-.79-5.65-2.43-7.44a8.73,8.73,0,0,0-6.78-2.68,10.2,10.2,0,0,0-5.11,1.24,8.56,8.56,0,0,0-3.44,3.63A12.45,12.45,0,0,0,664.07,77.65Z"/>
                          <path class="cls-1" d="M721,107.61H705.29l23.86-69.09H748l23.82,69.09H756.13L738.83,54.3h-.54Zm-1-27.16h37v11.4H720Z"/>
                          <path class="cls-1" d="M787.24,49.11A7.76,7.76,0,0,1,781.75,47a6.92,6.92,0,0,1,0-10.27,8.1,8.1,0,0,1,11,0,6.93,6.93,0,0,1,0,10.28A7.76,7.76,0,0,1,787.24,49.11ZM780,107.61V55.79h14.37v51.82Z"/>
                          <path class="cls-3" d="M.46,79.85a45.62,45.62,0,0,0,91.13,0,2.16,2.16,0,0,0-2.15-2.28H58.74a12.72,12.72,0,0,1-25.43,0H2.61A2.17,2.17,0,0,0,.46,79.85ZM46,96.62A18.94,18.94,0,0,0,59.49,91l15,14.95A40,40,0,0,1,46,117.77Z"/>
                          <path class="cls-3" d="M46,83.77a6.21,6.21,0,0,0,6.2-6.2H39.82A6.21,6.21,0,0,0,46,83.77Z"/>
                          <path class="cls-3" d="M52.9,3v7.55a3,3,0,0,1-3,3H48.32v6.79H43.74V13.57H42.2a3,3,0,0,1-3-3V3a3,3,0,0,1,3-3h7.65A3,3,0,0,1,52.9,3Z"/>
                          <path class="cls-2" d="M96.76,36.77V29.46a2.05,2.05,0,0,1,2-2.06h0a2.06,2.06,0,0,1,2.06,2.06v7.31a2.06,2.06,0,0,1-2.06,2.06h0A2.05,2.05,0,0,1,96.76,36.77Z"/>
                          <path class="cls-2" d="M103.2,36.77V29.46a2.06,2.06,0,0,1,2.06-2.06h0a2.06,2.06,0,0,1,2.06,2.06v7.31a2.06,2.06,0,0,1-2.06,2.06h0A2.06,2.06,0,0,1,103.2,36.77Z"/>
                          <rect class="cls-2" x="109.65" y="27.4" width="4.11" height="11.43" rx="0.91"/>
                          <path class="cls-2" d="M116.1,36.77V29.46a2.05,2.05,0,0,1,2.05-2.06h0a2.06,2.06,0,0,1,2.06,2.06v7.31a2.06,2.06,0,0,1-2.06,2.06h0A2.05,2.05,0,0,1,116.1,36.77Z"/>
                          <path class="cls-2" d="M96.76,50.86V43.55a2.05,2.05,0,0,1,2-2.06h0a2.06,2.06,0,0,1,2.06,2.06v7.31a2.06,2.06,0,0,1-2.06,2.06h0A2.05,2.05,0,0,1,96.76,50.86Z"/>
                          <path class="cls-2" d="M103.2,50.86V43.55a2.06,2.06,0,0,1,2.06-2.06h0a2.06,2.06,0,0,1,2.06,2.06v7.31a2.06,2.06,0,0,1-2.06,2.06h0A2.06,2.06,0,0,1,103.2,50.86Z"/>
                          <path class="cls-2" d="M96.76,65V57.64a2.05,2.05,0,0,1,2-2h0a2.05,2.05,0,0,1,2.06,2V65A2.05,2.05,0,0,1,98.81,67h0A2.05,2.05,0,0,1,96.76,65Z"/>
                          <path class="cls-2" d="M103.2,65V57.64a2.05,2.05,0,0,1,2.06-2h0a2.05,2.05,0,0,1,2.06,2V65A2.05,2.05,0,0,1,105.26,67h0A2.05,2.05,0,0,1,103.2,65Z"/>
                          <rect class="cls-2" x="109.65" y="55.59" width="4.11" height="11.43" rx="0.91"/>
                          <path class="cls-4" d="M83.53,20.36h-75A8.52,8.52,0,0,0,0,28.87v40a2.3,2.3,0,0,0,2.29,2.29H89.76a2.3,2.3,0,0,0,2.29-2.29v-40A8.52,8.52,0,0,0,83.53,20.36ZM28.85,56.08A10.31,10.31,0,1,1,39.16,45.77,10.31,10.31,0,0,1,28.85,56.08Zm34.35,0A10.31,10.31,0,1,1,73.5,45.77,10.31,10.31,0,0,1,63.2,56.08Z"/>
                          </g>
                        </g>
                      </svg>
                </div>
                <div class="col-12 mt20 mt-md30">
                    <div class="col-12 f-md-50 f-24 mt-md30 mt20 w600 text-center white-clr lh140">
                      Here’s what you’ll get inside <br class="d-none d-lg-block"><span class="d-block w800 under orange-clr">VoiceFusion Ai Reseller License</span> 
                    </div>
                </div>
                <div class="row mt30 mt-md50 align-items-center">
                  <div class="col-12 col-md-10 mx-auto">
                      <img src="assets/images/product-box.webp" class="img-fluid d-block mx-auto img-animation">
                  </div>
                </div>  
                <!-- <div class="col-12 col-md-11 mx-auto mt20 mt-md20">
                    <div class="f-20 f-md-35 text-center up-btn-header">
                        <a href="#buynow" class="text-center">
                        GET YOUR RESELLER LICENSE - RIGHT NOW
                     </a>
                    </div>
                </div> -->
            </div>
        </div>
    </div>
    <div class="presenting-feature">
      <div class="container">
          <div class="row feature-list-block">
            <div class="col-12 col-md-6">
              <div class="d-flex justify-content-md-start  gap-4">
                  <div class="mt5">
                    <img src="assets/images/home01.webp" class="img-sm">
                  </div>
                  <div class="f-20 f-md-20 lh140 w300 white-clr">
                    Host, Manage &amp; Publish All Your Videos, Pdfs, Docs, Audios, Zip Files Or Any Other Marketing &amp; Training Files
                  </div>
              </div>
              <div class="d-flex justify-content-md-start  gap-4 mt20 mt-md30">
                  <div class="mt5">
                    <img src="assets/images/home02.webp" class="img-sm">
                  </div>
                  <div class="f-16 f-md-20 lh140 w300 white-clr">
                    Supercharge All Your Websites, Pages, E-Com Stores &amp; Mobile Apps By Delivering All Your Files At Lightning Fast Speed With Fast CDNs
                  </div>
              </div>
              <div class="d-flex justify-content-md-start  gap-4 mt20 mt-md30">
                  <div class="mt5">
                    <img src="assets/images/home03.webp" class="img-sm">
                  </div>
                  <div class="f-16 f-md-20 lh140 w300 white-clr">
                    Online Back-Up &amp; 30 Days File Recovery
                  </div>
              </div>
            </div>
            <div class="col-12 col-md-6">
              <div class="d-flex justify-content-md-start  gap-4 mt20 mt-md0">
                  <div class="mt5">
                    <img src="assets/images/home04.webp" class="img-sm">
                  </div>
                  <div class="f-16 f-md-20 lh140 w300 white-clr">
                    Powerful Battle-Tested Architecture That’s Happily Served 133 Million+ File Views &amp; Downloads And Counting…
                  </div>
              </div>
              <div class="d-flex justify-content-md-start  gap-4 mt20 mt-md30">
                  <div class="mt5">
                    <img src="assets/images/home05.webp" class="img-sm">
                  </div>
                  <div class="f-16 f-md-22 lh140 w300 white-clr">
                    No Other Cloud Service Comes Close. These Features Will Absolutely Amaze You!
                  </div>
              </div>
              <div class="d-flex justify-content-md-start  gap-4 mt20 mt-md30">
                  <div class="mt5">
                    <img src="assets/images/home06.webp" class="img-sm">
                  </div>
                  <div class="f-16 f-md-22 lh140 w300 white-clr">
                    No Other Cloud Service Comes Close. These Features Will Absolutely Amaze You!
                  </div>
              </div>
            </div>
          </div>              
      </div>
    </div>
    <!--4. Fourth Section End -->
    <!--10. Table Section Starts -->
    <div class="white-section">
        <div class="container">
            <div class="header">
                <div class="row align-items-center">
                    <div class="col-md-4">
                        <img src="assets/images/home5_2.webp" class="img-fluid">
                    </div>
                    <div class="col-md-8">
                        <h2 class="text-center pt100 lg36 f-22 f-md-36">We’ve already told you that it’s a once in a lifetime opportunity to become an Official VoiceFusion Ai Reseller and join this Top 5% Club to ever Reap Profit LIKE THIS! </h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--10. Table Section End -->
    <!--11. Services Section Starts --> 
    <div class="services">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="text-center f-32 f-md45 w600 mb40 mb-md50">
                        To start a business it takes Investment, Hardwork and TIME!
                        But today we totally <span class="w800 under orange-clr">Fool-Proofed it FOR YOU!</span> 
                    </div>
                </div>
                <div class="col-md-6 col-12 mt20 mt-md50 gx-5">
                    <div class="bs-service text-center mb20 mb-md30 ">
                        <div class="ser-icon">
                            <img src="assets/images/ser01.webp" class="img-fluid mx-auto d-block" alt="">
                        </div>
                        <div class="f-18 f-md-24 w400">
                            You don’t need to hire
                            Expensive Developers
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-12 mt20 mt-md50 gx-5">
                    <div class="bs-service text-center mb20 mb-md30">
                        <div class="ser-icon">
                            <img src="assets/images/ser02.webp" class="img-fluid mx-auto d-block" alt="">
                        </div>
                        <div class="f-18 f-md-24 w400">
                            You don’t have to
                            research ‘Anything’
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-12 mt20 mt-md50 gx-5">
                    <div class="bs-service text-center mb20 mb-md30">
                        <div class="ser-icon">
                            <img src="assets/images/ser03.webp" class="img-fluid mx-auto d-block" alt="">
                        </div>
                        <div class="f-18 f-md-24 w400">
                        You don’t need to hire any costly Support Desk Professional!
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-12 mt20 mt-md50 gx-5">
                    <div class="bs-service text-center mb20 mb-md30">
                        <div class="ser-icon">
                            <img src="assets/images/ser04.webp" class="img-fluid mx-auto d-block" alt="">
                        </div>
                        <div class="f-18 f-md-24 w400">
                        You don’t have to even pay for any type of sales material  like sales pitch, Sales page design, videos, mockups, social posts, complex graphics etc.
                        </div>
                    </div>
                </div>
            </div>
        </div>
     </div>
     <!--11. Services Section End -->
     <!--12. Moneyback Section Start -->
     <div class="moneyback-section">
		<div class="container ">
			<div class="row inner-content align-items-center">
			   <div class="col-12">
				  <div class="f-32 f-md-45 lh140 w700 text-center white-clr">
					 Let’s Remove All The Risks For You And Secure It With <span class="w700">30 Days Money Back Guarantee</span>
				  </div>
			   </div>
			   <div class="col-12 col-md-4 mt30 px-md15">
				  <div class="mt20 mt-md30">
					 <img src="assets/images/moneyback.webp" class="mx-auto d-block img-fluid">
				  </div>
			   </div>
			   <div class="col-12 col-md-8 mt30 px-md15">
				  <div class="f-18 f-md-22 lh140 w400 mt-md20 white-clr">
					 You can buy with confidence because if you face any technical issue which we don’t resolve for you, <span class="w600">just raise a ticket within 30 days and we'll refund you everything,</span> down to the last penny.
					 <br><br> <span class="w600">Our team has a 99.21% proven record of solving customer problems and helping them through any issues. </span>
					 <br><br> Guarantees like that don't come along every day, and neither do golden chances like this.
				  </div>
			   </div>
			</div>
		 </div>
	</div>
    <!--12. Moneyback Section End -->
    <section id="buynow">
            <div class="home9">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-2"></div>
                        <div class="col-sm-8">
                            <div class="content text-center">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 795 123.19" style="max-height: 70px;">
                                <defs>
                                    <style>
                                        .cls-1{fill:#fff;}
                                        .cls-2,
                                        .cls-4{fill:#ff7c24;}
                                        .cls-3{fill:#15c5f3;}
                                        .cls-4{fill-rule:evenodd;}
                                    </style>
                                </defs>
                                    <g id="Layer_2" data-name="Layer 2">
                                        <g id="Layer_1-2" data-name="Layer 1">
                                        <path class="cls-1" d="M151.7,38.52,168.39,91H169l16.73-52.49H202l-23.82,69.09H159.32L135.47,38.52Z"/>
                                        <path class="cls-1" d="M227.5,108.62a26.33,26.33,0,0,1-13.58-3.36,22.68,22.68,0,0,1-8.82-9.38,30.07,30.07,0,0,1-3.1-14,30.25,30.25,0,0,1,3.1-14.05,22.68,22.68,0,0,1,8.82-9.38,29.12,29.12,0,0,1,27.16,0,22.68,22.68,0,0,1,8.82,9.38A30.25,30.25,0,0,1,253,81.9a30.07,30.07,0,0,1-3.1,14,22.68,22.68,0,0,1-8.82,9.38A26.33,26.33,0,0,1,227.5,108.62Zm.07-11.13a8.85,8.85,0,0,0,6-2,12.6,12.6,0,0,0,3.63-5.58,27,27,0,0,0,0-16.12,12.67,12.67,0,0,0-3.63-5.6,8.81,8.81,0,0,0-6-2.06,9,9,0,0,0-6.06,2.06,12.4,12.4,0,0,0-3.67,5.6,26.81,26.81,0,0,0,0,16.12,12.33,12.33,0,0,0,3.67,5.58A9.09,9.09,0,0,0,227.57,97.49Z"/>
                                        <path class="cls-1" d="M269.6,49.11A7.75,7.75,0,0,1,264.12,47a6.9,6.9,0,0,1,0-10.27,8.09,8.09,0,0,1,10.94,0,6.93,6.93,0,0,1,0,10.28A7.72,7.72,0,0,1,269.6,49.11Zm-7.22,58.5V55.79h14.37v51.82Z"/>
                                        <path class="cls-1" d="M311.64,108.62A26.34,26.34,0,0,1,298,105.23a22.59,22.59,0,0,1-8.78-9.43,30.48,30.48,0,0,1-3.05-13.9,30.19,30.19,0,0,1,3.09-14A22.84,22.84,0,0,1,298,58.5a26,26,0,0,1,13.56-3.39,26.84,26.84,0,0,1,11.87,2.47,19.81,19.81,0,0,1,8.07,6.91A20.31,20.31,0,0,1,334.78,75H321.22a10.66,10.66,0,0,0-3-6.26,8.81,8.81,0,0,0-6.4-2.38A9.63,9.63,0,0,0,306,68.12a11.59,11.59,0,0,0-3.87,5.24,22.47,22.47,0,0,0-1.38,8.34,22.91,22.91,0,0,0,1.37,8.43,11.56,11.56,0,0,0,3.86,5.3,10.25,10.25,0,0,0,10.27.81,8.64,8.64,0,0,0,3.27-2.95,11.27,11.27,0,0,0,1.71-4.68h13.56a21.3,21.3,0,0,1-3.22,10.44,19.64,19.64,0,0,1-7.95,7A26.54,26.54,0,0,1,311.64,108.62Z"/>
                                        <path class="cls-1" d="M367.64,108.62a27.48,27.48,0,0,1-13.75-3.26A22,22,0,0,1,345,96.12a30.44,30.44,0,0,1-3.1-14.19,30.28,30.28,0,0,1,3.1-14,22.88,22.88,0,0,1,8.76-9.41,25.4,25.4,0,0,1,13.27-3.38,27.48,27.48,0,0,1,9.57,1.64,21.68,21.68,0,0,1,7.76,4.91,22.48,22.48,0,0,1,5.18,8.21,33,33,0,0,1,1.85,11.56v3.94H347.67v-8.9h30.22a11,11,0,0,0-1.35-5.5,9.81,9.81,0,0,0-3.72-3.76,10.92,10.92,0,0,0-5.52-1.37,11.1,11.1,0,0,0-5.79,1.5,10.82,10.82,0,0,0-3.93,4,11.42,11.42,0,0,0-1.45,5.58v8.47a14.33,14.33,0,0,0,1.44,6.64,10.22,10.22,0,0,0,4.06,4.32,12.33,12.33,0,0,0,6.24,1.52,13.61,13.61,0,0,0,4.39-.67,9.14,9.14,0,0,0,3.41-2,8.94,8.94,0,0,0,2.16-3.3l13.29.87a18.14,18.14,0,0,1-4.14,8.35,20.71,20.71,0,0,1-8,5.53A30.14,30.14,0,0,1,367.64,108.62Z"/>
                                        <path class="cls-2" d="M401.07,107.61V38.52h45.74v12H415.68V67h28.1v12h-28.1v28.54Z"/>
                                        <path class="cls-2" d="M486.35,85.54V55.79h14.37v51.82H486.93V98.19h-.54a15.3,15.3,0,0,1-5.82,7.32,17.14,17.14,0,0,1-9.9,2.77,17.51,17.51,0,0,1-9.15-2.36,16.07,16.07,0,0,1-6.15-6.71,23,23,0,0,1-2.25-10.43v-33h14.37V86.22A10.51,10.51,0,0,0,470,93.47a8.37,8.37,0,0,0,6.51,2.67,10.23,10.23,0,0,0,4.86-1.2A9.39,9.39,0,0,0,485,91.38,11.1,11.1,0,0,0,486.35,85.54Z"/>
                                        <path class="cls-2" d="M555.31,70.56l-13.16.81a6.9,6.9,0,0,0-1.45-3.05,7.89,7.89,0,0,0-2.92-2.19,10.24,10.24,0,0,0-4.3-.83,10.64,10.64,0,0,0-5.63,1.4,4.23,4.23,0,0,0-2.3,3.73A4,4,0,0,0,527,73.57a11.72,11.72,0,0,0,5.09,2.05l9.38,1.89q7.56,1.56,11.27,5a11.75,11.75,0,0,1,3.71,9,14.2,14.2,0,0,1-3,8.94,19.55,19.55,0,0,1-8.16,6,31.16,31.16,0,0,1-11.93,2.14q-10.29,0-16.38-4.3a16.67,16.67,0,0,1-7.13-11.73L524,91.85a7.26,7.26,0,0,0,3.11,4.78,13.18,13.18,0,0,0,12.4.16A4.43,4.43,0,0,0,541.88,93a4,4,0,0,0-1.65-3.22,12,12,0,0,0-5-1.94l-9-1.79q-7.59-1.51-11.28-5.26a13,13,0,0,1-3.7-9.55,13.88,13.88,0,0,1,2.72-8.6,17.16,17.16,0,0,1,7.66-5.57,31.41,31.41,0,0,1,11.58-2q9.82,0,15.47,4.15A16.06,16.06,0,0,1,555.31,70.56Z"/>
                                        <path class="cls-2" d="M572.78,49.11A7.75,7.75,0,0,1,567.3,47a6.9,6.9,0,0,1,0-10.27,8.09,8.09,0,0,1,10.94,0,6.93,6.93,0,0,1,0,10.28A7.72,7.72,0,0,1,572.78,49.11Zm-7.22,58.5V55.79h14.37v51.82Z"/>
                                        <path class="cls-2" d="M614.82,108.62a26.3,26.3,0,0,1-13.58-3.36,22.64,22.64,0,0,1-8.83-9.38,30.19,30.19,0,0,1-3.1-14,30.37,30.37,0,0,1,3.1-14.05,22.64,22.64,0,0,1,8.83-9.38,29.1,29.1,0,0,1,27.15,0,22.64,22.64,0,0,1,8.83,9.38,30.37,30.37,0,0,1,3.1,14.05,30.19,30.19,0,0,1-3.1,14,22.64,22.64,0,0,1-8.83,9.38A26.29,26.29,0,0,1,614.82,108.62Zm.06-11.13a8.82,8.82,0,0,0,6-2,12.52,12.52,0,0,0,3.63-5.58,27,27,0,0,0,0-16.12,12.59,12.59,0,0,0-3.63-5.6,8.78,8.78,0,0,0-6-2.06,9,9,0,0,0-6,2.06,12.41,12.41,0,0,0-3.68,5.6,27,27,0,0,0,0,16.12,12.35,12.35,0,0,0,3.68,5.58A9.09,9.09,0,0,0,614.88,97.49Z"/>
                                        <path class="cls-2" d="M664.07,77.65v30H649.7V55.79h13.7v9.14h.6a14.6,14.6,0,0,1,5.77-7.17,17.59,17.59,0,0,1,9.82-2.65A18.26,18.26,0,0,1,689,57.47a16,16,0,0,1,6.24,6.74,22.57,22.57,0,0,1,2.23,10.4v33H683.1V77.18c0-3.18-.79-5.65-2.43-7.44a8.73,8.73,0,0,0-6.78-2.68,10.2,10.2,0,0,0-5.11,1.24,8.56,8.56,0,0,0-3.44,3.63A12.45,12.45,0,0,0,664.07,77.65Z"/>
                                        <path class="cls-1" d="M721,107.61H705.29l23.86-69.09H748l23.82,69.09H756.13L738.83,54.3h-.54Zm-1-27.16h37v11.4H720Z"/>
                                        <path class="cls-1" d="M787.24,49.11A7.76,7.76,0,0,1,781.75,47a6.92,6.92,0,0,1,0-10.27,8.1,8.1,0,0,1,11,0,6.93,6.93,0,0,1,0,10.28A7.76,7.76,0,0,1,787.24,49.11ZM780,107.61V55.79h14.37v51.82Z"/>
                                        <path class="cls-3" d="M.46,79.85a45.62,45.62,0,0,0,91.13,0,2.16,2.16,0,0,0-2.15-2.28H58.74a12.72,12.72,0,0,1-25.43,0H2.61A2.17,2.17,0,0,0,.46,79.85ZM46,96.62A18.94,18.94,0,0,0,59.49,91l15,14.95A40,40,0,0,1,46,117.77Z"/>
                                        <path class="cls-3" d="M46,83.77a6.21,6.21,0,0,0,6.2-6.2H39.82A6.21,6.21,0,0,0,46,83.77Z"/>
                                        <path class="cls-3" d="M52.9,3v7.55a3,3,0,0,1-3,3H48.32v6.79H43.74V13.57H42.2a3,3,0,0,1-3-3V3a3,3,0,0,1,3-3h7.65A3,3,0,0,1,52.9,3Z"/>
                                        <path class="cls-2" d="M96.76,36.77V29.46a2.05,2.05,0,0,1,2-2.06h0a2.06,2.06,0,0,1,2.06,2.06v7.31a2.06,2.06,0,0,1-2.06,2.06h0A2.05,2.05,0,0,1,96.76,36.77Z"/>
                                        <path class="cls-2" d="M103.2,36.77V29.46a2.06,2.06,0,0,1,2.06-2.06h0a2.06,2.06,0,0,1,2.06,2.06v7.31a2.06,2.06,0,0,1-2.06,2.06h0A2.06,2.06,0,0,1,103.2,36.77Z"/>
                                        <rect class="cls-2" x="109.65" y="27.4" width="4.11" height="11.43" rx="0.91"/>
                                        <path class="cls-2" d="M116.1,36.77V29.46a2.05,2.05,0,0,1,2.05-2.06h0a2.06,2.06,0,0,1,2.06,2.06v7.31a2.06,2.06,0,0,1-2.06,2.06h0A2.05,2.05,0,0,1,116.1,36.77Z"/>
                                        <path class="cls-2" d="M96.76,50.86V43.55a2.05,2.05,0,0,1,2-2.06h0a2.06,2.06,0,0,1,2.06,2.06v7.31a2.06,2.06,0,0,1-2.06,2.06h0A2.05,2.05,0,0,1,96.76,50.86Z"/>
                                        <path class="cls-2" d="M103.2,50.86V43.55a2.06,2.06,0,0,1,2.06-2.06h0a2.06,2.06,0,0,1,2.06,2.06v7.31a2.06,2.06,0,0,1-2.06,2.06h0A2.06,2.06,0,0,1,103.2,50.86Z"/>
                                        <path class="cls-2" d="M96.76,65V57.64a2.05,2.05,0,0,1,2-2h0a2.05,2.05,0,0,1,2.06,2V65A2.05,2.05,0,0,1,98.81,67h0A2.05,2.05,0,0,1,96.76,65Z"/>
                                        <path class="cls-2" d="M103.2,65V57.64a2.05,2.05,0,0,1,2.06-2h0a2.05,2.05,0,0,1,2.06,2V65A2.05,2.05,0,0,1,105.26,67h0A2.05,2.05,0,0,1,103.2,65Z"/>
                                        <rect class="cls-2" x="109.65" y="55.59" width="4.11" height="11.43" rx="0.91"/>
                                        <path class="cls-4" d="M83.53,20.36h-75A8.52,8.52,0,0,0,0,28.87v40a2.3,2.3,0,0,0,2.29,2.29H89.76a2.3,2.3,0,0,0,2.29-2.29v-40A8.52,8.52,0,0,0,83.53,20.36ZM28.85,56.08A10.31,10.31,0,1,1,39.16,45.77,10.31,10.31,0,0,1,28.85,56.08Zm34.35,0A10.31,10.31,0,1,1,73.5,45.77,10.31,10.31,0,0,1,63.2,56.08Z"/>
                                        </g>
                                    </g>
                            </svg>
                        </div>
                        <div class="list mt50 white-clr">
                            <ul>
                                <li class="f-24">Unlimited Account Licenses <div class="bg_gradent"></div></li>
                                <li class="f-24">All Sales Materials For Bestest Conversions <div class="bg_gradent"></div></li>
                                <li class="f-24">We Will Handle Support of Your All Clients <div class="bg_gradent"></div></li>
                                <li class="f-24">Accept Payment on Paypal, Stripe &amp; Bank Account ETC. <div class="bg_gradent"></div></li>
                                <li class="f-24">ONE TIME FEE.. NEVER PAY AGAIN <div class="bg_gradent"></div></li>
                                <li class="f-24">5 Fast Action Hand Picked Bonuses</li>
                            </ul>
                        </div>

                        </div>
                    </div>
                </div><br>
				<div class="col-sm-12 text-center">
                    <h2 class="f-24 f-md-32 white-clr">Click Below To Secure Unlimited Access To<br> VoiceFusionAi AI Reseller  Advance For This Highly Discounted Price:</h2>
                </div>
                <div class="footer_content mt50">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 text-center">
                                
                                <a href="https://warriorplus.com/o2/buy/qyptv4/h2fq01/zx9426"><img src="https://warriorplus.com/o2/btn/fn200011000/qyptv4/h2fq01/360826" class="img-fluid mx-auto d-block btn-bg">
                                </a>                              
                                <p class="white-clr f-18 mt20"></p>
                                
                            </div>  
                        </div>
                    </div><br><br>
					<center><a href="https://warriorplus.com/o/nothanks/h2fq01" class="white-clr f-24 no-thanks" target="_blank">No Thanks</a></center>
                </div>
            </div>
        </section>                       
    <!--Footer Section Start -->
    <div class="footer-section">
         <div class="container ">
            <div class="row">
               <div class="col-12 text-center">
               <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 795 123.19" style="max-height: 70px;">
						<defs>
							<style>
								.cls-1{fill:#fff;}
								.cls-2,
								.cls-4{fill:#ff7c24;}
								.cls-3{fill:#15c5f3;}
								.cls-4{fill-rule:evenodd;}
							</style>
						</defs>
						<g id="Layer_2" data-name="Layer 2">
						<g id="Layer_1-2" data-name="Layer 1">
						<path class="cls-1" d="M151.7,38.52,168.39,91H169l16.73-52.49H202l-23.82,69.09H159.32L135.47,38.52Z"/>
						<path class="cls-1" d="M227.5,108.62a26.33,26.33,0,0,1-13.58-3.36,22.68,22.68,0,0,1-8.82-9.38,30.07,30.07,0,0,1-3.1-14,30.25,30.25,0,0,1,3.1-14.05,22.68,22.68,0,0,1,8.82-9.38,29.12,29.12,0,0,1,27.16,0,22.68,22.68,0,0,1,8.82,9.38A30.25,30.25,0,0,1,253,81.9a30.07,30.07,0,0,1-3.1,14,22.68,22.68,0,0,1-8.82,9.38A26.33,26.33,0,0,1,227.5,108.62Zm.07-11.13a8.85,8.85,0,0,0,6-2,12.6,12.6,0,0,0,3.63-5.58,27,27,0,0,0,0-16.12,12.67,12.67,0,0,0-3.63-5.6,8.81,8.81,0,0,0-6-2.06,9,9,0,0,0-6.06,2.06,12.4,12.4,0,0,0-3.67,5.6,26.81,26.81,0,0,0,0,16.12,12.33,12.33,0,0,0,3.67,5.58A9.09,9.09,0,0,0,227.57,97.49Z"/>
						<path class="cls-1" d="M269.6,49.11A7.75,7.75,0,0,1,264.12,47a6.9,6.9,0,0,1,0-10.27,8.09,8.09,0,0,1,10.94,0,6.93,6.93,0,0,1,0,10.28A7.72,7.72,0,0,1,269.6,49.11Zm-7.22,58.5V55.79h14.37v51.82Z"/>
						<path class="cls-1" d="M311.64,108.62A26.34,26.34,0,0,1,298,105.23a22.59,22.59,0,0,1-8.78-9.43,30.48,30.48,0,0,1-3.05-13.9,30.19,30.19,0,0,1,3.09-14A22.84,22.84,0,0,1,298,58.5a26,26,0,0,1,13.56-3.39,26.84,26.84,0,0,1,11.87,2.47,19.81,19.81,0,0,1,8.07,6.91A20.31,20.31,0,0,1,334.78,75H321.22a10.66,10.66,0,0,0-3-6.26,8.81,8.81,0,0,0-6.4-2.38A9.63,9.63,0,0,0,306,68.12a11.59,11.59,0,0,0-3.87,5.24,22.47,22.47,0,0,0-1.38,8.34,22.91,22.91,0,0,0,1.37,8.43,11.56,11.56,0,0,0,3.86,5.3,10.25,10.25,0,0,0,10.27.81,8.64,8.64,0,0,0,3.27-2.95,11.27,11.27,0,0,0,1.71-4.68h13.56a21.3,21.3,0,0,1-3.22,10.44,19.64,19.64,0,0,1-7.95,7A26.54,26.54,0,0,1,311.64,108.62Z"/>
						<path class="cls-1" d="M367.64,108.62a27.48,27.48,0,0,1-13.75-3.26A22,22,0,0,1,345,96.12a30.44,30.44,0,0,1-3.1-14.19,30.28,30.28,0,0,1,3.1-14,22.88,22.88,0,0,1,8.76-9.41,25.4,25.4,0,0,1,13.27-3.38,27.48,27.48,0,0,1,9.57,1.64,21.68,21.68,0,0,1,7.76,4.91,22.48,22.48,0,0,1,5.18,8.21,33,33,0,0,1,1.85,11.56v3.94H347.67v-8.9h30.22a11,11,0,0,0-1.35-5.5,9.81,9.81,0,0,0-3.72-3.76,10.92,10.92,0,0,0-5.52-1.37,11.1,11.1,0,0,0-5.79,1.5,10.82,10.82,0,0,0-3.93,4,11.42,11.42,0,0,0-1.45,5.58v8.47a14.33,14.33,0,0,0,1.44,6.64,10.22,10.22,0,0,0,4.06,4.32,12.33,12.33,0,0,0,6.24,1.52,13.61,13.61,0,0,0,4.39-.67,9.14,9.14,0,0,0,3.41-2,8.94,8.94,0,0,0,2.16-3.3l13.29.87a18.14,18.14,0,0,1-4.14,8.35,20.71,20.71,0,0,1-8,5.53A30.14,30.14,0,0,1,367.64,108.62Z"/>
						<path class="cls-2" d="M401.07,107.61V38.52h45.74v12H415.68V67h28.1v12h-28.1v28.54Z"/>
						<path class="cls-2" d="M486.35,85.54V55.79h14.37v51.82H486.93V98.19h-.54a15.3,15.3,0,0,1-5.82,7.32,17.14,17.14,0,0,1-9.9,2.77,17.51,17.51,0,0,1-9.15-2.36,16.07,16.07,0,0,1-6.15-6.71,23,23,0,0,1-2.25-10.43v-33h14.37V86.22A10.51,10.51,0,0,0,470,93.47a8.37,8.37,0,0,0,6.51,2.67,10.23,10.23,0,0,0,4.86-1.2A9.39,9.39,0,0,0,485,91.38,11.1,11.1,0,0,0,486.35,85.54Z"/>
						<path class="cls-2" d="M555.31,70.56l-13.16.81a6.9,6.9,0,0,0-1.45-3.05,7.89,7.89,0,0,0-2.92-2.19,10.24,10.24,0,0,0-4.3-.83,10.64,10.64,0,0,0-5.63,1.4,4.23,4.23,0,0,0-2.3,3.73A4,4,0,0,0,527,73.57a11.72,11.72,0,0,0,5.09,2.05l9.38,1.89q7.56,1.56,11.27,5a11.75,11.75,0,0,1,3.71,9,14.2,14.2,0,0,1-3,8.94,19.55,19.55,0,0,1-8.16,6,31.16,31.16,0,0,1-11.93,2.14q-10.29,0-16.38-4.3a16.67,16.67,0,0,1-7.13-11.73L524,91.85a7.26,7.26,0,0,0,3.11,4.78,13.18,13.18,0,0,0,12.4.16A4.43,4.43,0,0,0,541.88,93a4,4,0,0,0-1.65-3.22,12,12,0,0,0-5-1.94l-9-1.79q-7.59-1.51-11.28-5.26a13,13,0,0,1-3.7-9.55,13.88,13.88,0,0,1,2.72-8.6,17.16,17.16,0,0,1,7.66-5.57,31.41,31.41,0,0,1,11.58-2q9.82,0,15.47,4.15A16.06,16.06,0,0,1,555.31,70.56Z"/>
						<path class="cls-2" d="M572.78,49.11A7.75,7.75,0,0,1,567.3,47a6.9,6.9,0,0,1,0-10.27,8.09,8.09,0,0,1,10.94,0,6.93,6.93,0,0,1,0,10.28A7.72,7.72,0,0,1,572.78,49.11Zm-7.22,58.5V55.79h14.37v51.82Z"/>
						<path class="cls-2" d="M614.82,108.62a26.3,26.3,0,0,1-13.58-3.36,22.64,22.64,0,0,1-8.83-9.38,30.19,30.19,0,0,1-3.1-14,30.37,30.37,0,0,1,3.1-14.05,22.64,22.64,0,0,1,8.83-9.38,29.1,29.1,0,0,1,27.15,0,22.64,22.64,0,0,1,8.83,9.38,30.37,30.37,0,0,1,3.1,14.05,30.19,30.19,0,0,1-3.1,14,22.64,22.64,0,0,1-8.83,9.38A26.29,26.29,0,0,1,614.82,108.62Zm.06-11.13a8.82,8.82,0,0,0,6-2,12.52,12.52,0,0,0,3.63-5.58,27,27,0,0,0,0-16.12,12.59,12.59,0,0,0-3.63-5.6,8.78,8.78,0,0,0-6-2.06,9,9,0,0,0-6,2.06,12.41,12.41,0,0,0-3.68,5.6,27,27,0,0,0,0,16.12,12.35,12.35,0,0,0,3.68,5.58A9.09,9.09,0,0,0,614.88,97.49Z"/>
						<path class="cls-2" d="M664.07,77.65v30H649.7V55.79h13.7v9.14h.6a14.6,14.6,0,0,1,5.77-7.17,17.59,17.59,0,0,1,9.82-2.65A18.26,18.26,0,0,1,689,57.47a16,16,0,0,1,6.24,6.74,22.57,22.57,0,0,1,2.23,10.4v33H683.1V77.18c0-3.18-.79-5.65-2.43-7.44a8.73,8.73,0,0,0-6.78-2.68,10.2,10.2,0,0,0-5.11,1.24,8.56,8.56,0,0,0-3.44,3.63A12.45,12.45,0,0,0,664.07,77.65Z"/>
						<path class="cls-1" d="M721,107.61H705.29l23.86-69.09H748l23.82,69.09H756.13L738.83,54.3h-.54Zm-1-27.16h37v11.4H720Z"/>
						<path class="cls-1" d="M787.24,49.11A7.76,7.76,0,0,1,781.75,47a6.92,6.92,0,0,1,0-10.27,8.1,8.1,0,0,1,11,0,6.93,6.93,0,0,1,0,10.28A7.76,7.76,0,0,1,787.24,49.11ZM780,107.61V55.79h14.37v51.82Z"/>
						<path class="cls-3" d="M.46,79.85a45.62,45.62,0,0,0,91.13,0,2.16,2.16,0,0,0-2.15-2.28H58.74a12.72,12.72,0,0,1-25.43,0H2.61A2.17,2.17,0,0,0,.46,79.85ZM46,96.62A18.94,18.94,0,0,0,59.49,91l15,14.95A40,40,0,0,1,46,117.77Z"/>
						<path class="cls-3" d="M46,83.77a6.21,6.21,0,0,0,6.2-6.2H39.82A6.21,6.21,0,0,0,46,83.77Z"/>
						<path class="cls-3" d="M52.9,3v7.55a3,3,0,0,1-3,3H48.32v6.79H43.74V13.57H42.2a3,3,0,0,1-3-3V3a3,3,0,0,1,3-3h7.65A3,3,0,0,1,52.9,3Z"/>
						<path class="cls-2" d="M96.76,36.77V29.46a2.05,2.05,0,0,1,2-2.06h0a2.06,2.06,0,0,1,2.06,2.06v7.31a2.06,2.06,0,0,1-2.06,2.06h0A2.05,2.05,0,0,1,96.76,36.77Z"/>
						<path class="cls-2" d="M103.2,36.77V29.46a2.06,2.06,0,0,1,2.06-2.06h0a2.06,2.06,0,0,1,2.06,2.06v7.31a2.06,2.06,0,0,1-2.06,2.06h0A2.06,2.06,0,0,1,103.2,36.77Z"/>
						<rect class="cls-2" x="109.65" y="27.4" width="4.11" height="11.43" rx="0.91"/>
						<path class="cls-2" d="M116.1,36.77V29.46a2.05,2.05,0,0,1,2.05-2.06h0a2.06,2.06,0,0,1,2.06,2.06v7.31a2.06,2.06,0,0,1-2.06,2.06h0A2.05,2.05,0,0,1,116.1,36.77Z"/>
						<path class="cls-2" d="M96.76,50.86V43.55a2.05,2.05,0,0,1,2-2.06h0a2.06,2.06,0,0,1,2.06,2.06v7.31a2.06,2.06,0,0,1-2.06,2.06h0A2.05,2.05,0,0,1,96.76,50.86Z"/>
						<path class="cls-2" d="M103.2,50.86V43.55a2.06,2.06,0,0,1,2.06-2.06h0a2.06,2.06,0,0,1,2.06,2.06v7.31a2.06,2.06,0,0,1-2.06,2.06h0A2.06,2.06,0,0,1,103.2,50.86Z"/>
						<path class="cls-2" d="M96.76,65V57.64a2.05,2.05,0,0,1,2-2h0a2.05,2.05,0,0,1,2.06,2V65A2.05,2.05,0,0,1,98.81,67h0A2.05,2.05,0,0,1,96.76,65Z"/>
						<path class="cls-2" d="M103.2,65V57.64a2.05,2.05,0,0,1,2.06-2h0a2.05,2.05,0,0,1,2.06,2V65A2.05,2.05,0,0,1,105.26,67h0A2.05,2.05,0,0,1,103.2,65Z"/>
						<rect class="cls-2" x="109.65" y="55.59" width="4.11" height="11.43" rx="0.91"/>
						<path class="cls-4" d="M83.53,20.36h-75A8.52,8.52,0,0,0,0,28.87v40a2.3,2.3,0,0,0,2.29,2.29H89.76a2.3,2.3,0,0,0,2.29-2.29v-40A8.52,8.52,0,0,0,83.53,20.36ZM28.85,56.08A10.31,10.31,0,1,1,39.16,45.77,10.31,10.31,0,0,1,28.85,56.08Zm34.35,0A10.31,10.31,0,1,1,73.5,45.77,10.31,10.31,0,0,1,63.2,56.08Z"/>
						</g>
						</g>
                    </svg>
                  <div editabletype="text" class="f-14 f-md-16 w400 mt20 lh160 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
                  <div class=" mt20 mt-md40 white-clr"> <script type="text/javascript" src="https://warriorplus.com/o2/disclaimer/qyptv4" defer=""></script><div class="wplus_spdisclaimer f-17 w300 mt20 lh140 white-clr text-center"><strong>Disclaimer:</strong> <em>WarriorPlus is used to help manage the sale of products on this site. While WarriorPlus helps facilitate the sale, all payments are made directly to the product vendor and NOT WarriorPlus. Thus, all product questions, support inquiries and/or refund requests must be sent to the vendor. WarriorPlus's role should not be construed as an endorsement, approval or review of these products or any claim, statement or opinion used in the marketing of these products.</em></div></div>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-14 f-md-16 w400 lh160 white-clr text-xs-center">Copyright ©VoiceFusion Ai</div>
                  <ul class="footer-ul w400 f-14 f-md-16 white-clr text-center text-md-right">
                     <li><a href="https://support.bizomart.com/hc/en-us" class="white-clr  t-decoration-none" target="_blank">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://voicefusionai.com/legal/privacy-policy.html" class="white-clr t-decoration-none" target="_blank">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://voicefusionai.com/legal/terms-of-service.html" class="white-clr t-decoration-none" target="_blank">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://voicefusionai.com/legal/disclaimer.html" class="white-clr t-decoration-none" target="_blank">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://voicefusionai.com/legal/gdpr.html" class="white-clr t-decoration-none" target="_blank">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://voicefusionai.com/legal/dmca.html" class="white-clr t-decoration-none" target="_blank">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://voicefusionai.com/legal/anti-spam.html" class="white-clr t-decoration-none" target="_blank">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
    <!--Footer Section End -->
  <!-- New Timer  Start-->
<!-- timer --->
<!-- Exit Popup -->	
<script type="text/javascript" src="assets/js/ouibounce.min.js"></script>
<div id="ouibounce-modal" style="z-index:9999999; display:none;">
   <div class="underlay"></div>
   <div class="modal-wrapper" style="display:block;">
      <div class="modal-bg">
		<button type="button" class="close" onclick="document.getElementById('ouibounce-modal').style.display = 'none';">
			X 
		</button>
		<div class="model-header">
      <div class="d-flex justify-content-center align-items-center gap-3">
            <img src="assets/images/hold.webp" alt="" class="img-fluid d-block m131">
            <div>
               <div class="f-md-56 f-24 w700 lh130 white-clr">
                  WAIT! HOLD ON!!
               </div>
               <div class="f-md-28 f-18 w700 lh130 black-clr mt10 mt-md0" style="background-color:yellow; padding:5px 10px; display:inline-block">
                  Don't Leave Empty Handed
               </div>
            </div>
         </div>
		</div>
         <div class="col-12 for-padding">
			<div class="copun-line f-md-45 f-16 w500 lh130 text-center black-clr mt10">
         You've Qualified For An <span class="w700 red-clr"><br> INSTANT $200 DISCOUNT</span>  
			</div>
         <div class="copun-line f-md-30 f-16 w700 lh130 text-center black-clr mt10" style="background-color:yellow; padding:5px 10px; display:inline-block">
         Regular Price <strike>$267</strike>,
         Now Only $67	
			</div>
			 <div class="mt-md20 mt10 text-center">
				<a href="https://warriorplus.com/o2/buy/qyptv4/h2fq01/zx9426" class="cta-link-btn1">Grab SPECIAL Discount Now!
				<br>
				<span class="f-12 black-clr w600 text-uppercase">Act Now! Limited to First 100 Buyers Only.</span> </a>
			 </div>
			<div class="mt-md20 mt10 text-center f-md-26 f-16 w900 lh150 text-center red-clr text-uppercase">
				Coupon Code EXPIRES IN:
			</div>
			<div class="timer-new">
            <div class="days" class="timer-label">
               <span id="days"></span> <span class="text">Days</span>
            </div> 
            <div class="hours" class="timer-label">
               <span id="hours"></span> <span  class="text">Hours</span>
            </div>
            <div class="days" class="timer-label">
               <span id="mins" ></span> <span class="text">Min</span>
            </div>
            <div class="secs" class="timer-label">
               <span id="secs" ></span> <span class="text">Sec</span>
            </div>
         </div>
      </div>
   </div>
</div>
<script type="text/javascript">
      var timeInSecs;
      var ticker;
      function startTimer(secs) {
      timeInSecs = parseInt(secs);
	  tick();
      //ticker = setInterval("tick()", 1000);
      }
      function tick() {
      var secs = timeInSecs;
      if (secs > 0) {
      timeInSecs--;
      } else {
      clearInterval(ticker);
      //startTimer(20 * 60); // 4 minutes in seconds
      }
      var days = Math.floor(secs / 86400);
      secs %= 86400;
      var hours = Math.floor(secs / 3600);
      secs %= 3600;
      var mins = Math.floor(secs / 60);
      secs %= 60;
      var pretty = ((days < 10) ? "0" : "") + days + ":" + ((hours < 10) ? "0" : "") + hours + ":" + ((mins < 10) ? "0" : "") + mins + ":" + ((secs < 10) ? "0" : "") + secs;
      document.getElementById("days").innerHTML = days
      document.getElementById("hours").innerHTML = hours
      document.getElementById("mins").innerHTML = mins
      document.getElementById("secs").innerHTML = secs;
      }
      
	  //startTimer(60 * 60); 
      // 4 minutes in seconds
	  
	  function getCookie(cname) {
            var name = cname + "=";
            var decodedCookie = decodeURIComponent(document.cookie);
            var ca = decodedCookie.split(';');
            for(var i = 0; i <ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        }


      var cnt = 60*60;

      function counter(){
       
         if(getCookie("cnt") > 0){
            cnt = getCookie("cnt");
         }

         cnt -= 1;
         document.cookie = "cnt="+ cnt;

         startTimer(getCookie("cnt"));
         //jQuery("#counter").val(getCookie("cnt"));

         if(cnt>0){
            setTimeout(counter,1000);
         }
      
      }
      
      counter();
	  
	  
      </script>
      <script type="text/javascript">
         var _ouibounce = ouibounce(document.getElementById('ouibounce-modal'),{
         aggressive: true, //Making this true makes ouibounce not to obey "once per visitor" rule
         });
      </script>
      <!-- Exit Popup Ends-->
            <!--- timer end-->
   <!-- Exit Popup and Timer End -->
</body>
</html>
<script> 
function myFunction() {
  var checkBox = document.getElementById("check");
  var buybutton = document.getElementById("buyButton");
  if (checkBox.checked == true){
 
	    buybutton.getAttribute("href");
	    buybutton.setAttribute("href","https://warriorplus.com/o2/buy/zh5gzv/pbsfwc/m8vq9f");
	} else {
		 buybutton.getAttribute("href");
		 buybutton.setAttribute("href","https://warriorplus.com/o2/buy/zh5gzv/pbsfwc/m8vq9f");
	  }
}
</script>
<!Doctype html>
<html>
   <head>
      <title>VoiceFusion Ai Special</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=9">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
      <meta name="title" content="VoiceFusionAI Speical">
      <meta name="description" content="World’s First Super-Trending IBM's Watson & ChatGPT4 Powered Ai App Generates Us Real Human Emotion Based Voices, Audiobooks, Podcasts, & Even Unique Content with Just 1 Keyword.">
      <meta name="keywords" content="VoiceFusion Ai">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta property="og:image" content="https://www.voicefusionai.com/special/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Pranshu Gupta">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="VoiceFusionAI Speical">
      <meta property="og:description" content="World’s First Super-Trending IBM's Watson & ChatGPT4 Powered Ai App Generates Us Real Human Emotion Based Voices, Audiobooks, Podcasts, & Even Unique Content with Just 1 Keyword.">
      <meta property="og:image" content="https://www.voicefusionai.com/special/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="VoiceFusionAI Speical">
      <meta property="twitter:description" content="World’s First Super-Trending IBM's Watson & ChatGPT4 Powered Ai App Generates Us Real Human Emotion Based Voices, Audiobooks, Podcasts, & Even Unique Content with Just 1 Keyword.">
      <meta property="twitter:image" content="https://www.voicefusionai.com/special/thumbnail.png">
      <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&family=Poppins:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
      <!-- Start Editor required -->
      <link rel="stylesheet" href="https://cdn.oppyotest.com/launches/sellero/common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style-feature.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style-pawan.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style-bottom.css" type="text/css">
      <link rel="stylesheet" href="assets/css/timer.css" type="text/css">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <script src="https://cdn.oppyotest.com/launches/sellero/common_assets/js/bootstrap.min.js"></script>
      <script src="https://cdn.oppyotest.com/launches/sellero/common_assets/js/jquery.min.js"></script>
	  
	  <script>
(function(w, i, d, g, e, t, s) {
if(window.businessDomain != undefined){
console.log("Your page have duplicate embed code. Please check it.");
return false;
}
businessDomain = 'aicademy';
allowedDomain = 'voicefusionai.com';
if(!window.location.hostname.includes(allowedDomain)){
console.log("Your page have not authorized. Please check it.");
return false;
}
console.log("Your script is ready...");
w[d] = w[d] || [];
t = i.createElement(g);
t.async = 1;
t.src = e;
s = i.getElementsByTagName(g)[0];
s.parentNode.insertBefore(t, s);
})(window, document, '_gscq', 'script', 'https://cdn.staticdcp.com/apps/engage/smart_engage/js/config-loader.js');
</script>

   </head>
   <body>

      <div class="timer-header-top fixed-top">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-12 col-md-3">
                  <div class="tht-left f-14 f-md-14 text-md-start text-center white-clr">
                     Use Coupon <span class="orange-clr">"VoiceFusion"</span>  for <br class="d-block d-md-none">Extra <span class="orange-clr"> $3 Discount </span>
                  </div>
               </div>
               <div class="col-8 col-md-6">
                  <div class="countdown counter-white text-center">
                     <div class="timer-label text-center"><span class="f-24 f-md-28 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-16 w500 smmltd">Days</span> </div>
                     <div class="timer-label text-center"><span class="f-24 f-md-28 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-16 w500 smmltd">Hours</span> </div>
                     <div class="timer-label text-center timer-mrgn"><span class="f-24 f-md-28 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-16 w500 smmltd">Mins</span> </div>
                     <div class="timer-label text-center "><span class="f-24 f-md-28 timerbg oswald">00</span><br><span class="f-14 f-md-16 w500 smmltd">Sec</span> </div>
                  </div>
               </div>
               <div class="col-4 col-md-3 text-center text-md-right">
                  <div class="affiliate-link-btn"><a href="#buynow" class="t-decoration-none">Buy Now</a></div>
               </div>
            </div>
         </div>
      </div>
      <!--1. Header Section Start -->
      <div class="header-section">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-3 text-md-start text-center">
               <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 795 123.19" style="max-height: 80px;">
                    <defs>
                    <style>
                    .cls-1{fill:#fff;}
                    .cls-2,
                    .cls-4{fill:#ff7c24;}
                    .cls-3{fill:#15c5f3;}
                    .cls-4{fill-rule:evenodd;}
                    </style>
                    </defs>
                    <g id="Layer_2" data-name="Layer 2">
                    <g id="Layer_1-2" data-name="Layer 1">
                    <path class="cls-1" d="M151.7,38.52,168.39,91H169l16.73-52.49H202l-23.82,69.09H159.32L135.47,38.52Z"/>
                    <path class="cls-1" d="M227.5,108.62a26.33,26.33,0,0,1-13.58-3.36,22.68,22.68,0,0,1-8.82-9.38,30.07,30.07,0,0,1-3.1-14,30.25,30.25,0,0,1,3.1-14.05,22.68,22.68,0,0,1,8.82-9.38,29.12,29.12,0,0,1,27.16,0,22.68,22.68,0,0,1,8.82,9.38A30.25,30.25,0,0,1,253,81.9a30.07,30.07,0,0,1-3.1,14,22.68,22.68,0,0,1-8.82,9.38A26.33,26.33,0,0,1,227.5,108.62Zm.07-11.13a8.85,8.85,0,0,0,6-2,12.6,12.6,0,0,0,3.63-5.58,27,27,0,0,0,0-16.12,12.67,12.67,0,0,0-3.63-5.6,8.81,8.81,0,0,0-6-2.06,9,9,0,0,0-6.06,2.06,12.4,12.4,0,0,0-3.67,5.6,26.81,26.81,0,0,0,0,16.12,12.33,12.33,0,0,0,3.67,5.58A9.09,9.09,0,0,0,227.57,97.49Z"/>
                    <path class="cls-1" d="M269.6,49.11A7.75,7.75,0,0,1,264.12,47a6.9,6.9,0,0,1,0-10.27,8.09,8.09,0,0,1,10.94,0,6.93,6.93,0,0,1,0,10.28A7.72,7.72,0,0,1,269.6,49.11Zm-7.22,58.5V55.79h14.37v51.82Z"/>
                    <path class="cls-1" d="M311.64,108.62A26.34,26.34,0,0,1,298,105.23a22.59,22.59,0,0,1-8.78-9.43,30.48,30.48,0,0,1-3.05-13.9,30.19,30.19,0,0,1,3.09-14A22.84,22.84,0,0,1,298,58.5a26,26,0,0,1,13.56-3.39,26.84,26.84,0,0,1,11.87,2.47,19.81,19.81,0,0,1,8.07,6.91A20.31,20.31,0,0,1,334.78,75H321.22a10.66,10.66,0,0,0-3-6.26,8.81,8.81,0,0,0-6.4-2.38A9.63,9.63,0,0,0,306,68.12a11.59,11.59,0,0,0-3.87,5.24,22.47,22.47,0,0,0-1.38,8.34,22.91,22.91,0,0,0,1.37,8.43,11.56,11.56,0,0,0,3.86,5.3,10.25,10.25,0,0,0,10.27.81,8.64,8.64,0,0,0,3.27-2.95,11.27,11.27,0,0,0,1.71-4.68h13.56a21.3,21.3,0,0,1-3.22,10.44,19.64,19.64,0,0,1-7.95,7A26.54,26.54,0,0,1,311.64,108.62Z"/>
                    <path class="cls-1" d="M367.64,108.62a27.48,27.48,0,0,1-13.75-3.26A22,22,0,0,1,345,96.12a30.44,30.44,0,0,1-3.1-14.19,30.28,30.28,0,0,1,3.1-14,22.88,22.88,0,0,1,8.76-9.41,25.4,25.4,0,0,1,13.27-3.38,27.48,27.48,0,0,1,9.57,1.64,21.68,21.68,0,0,1,7.76,4.91,22.48,22.48,0,0,1,5.18,8.21,33,33,0,0,1,1.85,11.56v3.94H347.67v-8.9h30.22a11,11,0,0,0-1.35-5.5,9.81,9.81,0,0,0-3.72-3.76,10.92,10.92,0,0,0-5.52-1.37,11.1,11.1,0,0,0-5.79,1.5,10.82,10.82,0,0,0-3.93,4,11.42,11.42,0,0,0-1.45,5.58v8.47a14.33,14.33,0,0,0,1.44,6.64,10.22,10.22,0,0,0,4.06,4.32,12.33,12.33,0,0,0,6.24,1.52,13.61,13.61,0,0,0,4.39-.67,9.14,9.14,0,0,0,3.41-2,8.94,8.94,0,0,0,2.16-3.3l13.29.87a18.14,18.14,0,0,1-4.14,8.35,20.71,20.71,0,0,1-8,5.53A30.14,30.14,0,0,1,367.64,108.62Z"/>
                    <path class="cls-2" d="M401.07,107.61V38.52h45.74v12H415.68V67h28.1v12h-28.1v28.54Z"/>
                    <path class="cls-2" d="M486.35,85.54V55.79h14.37v51.82H486.93V98.19h-.54a15.3,15.3,0,0,1-5.82,7.32,17.14,17.14,0,0,1-9.9,2.77,17.51,17.51,0,0,1-9.15-2.36,16.07,16.07,0,0,1-6.15-6.71,23,23,0,0,1-2.25-10.43v-33h14.37V86.22A10.51,10.51,0,0,0,470,93.47a8.37,8.37,0,0,0,6.51,2.67,10.23,10.23,0,0,0,4.86-1.2A9.39,9.39,0,0,0,485,91.38,11.1,11.1,0,0,0,486.35,85.54Z"/>
                    <path class="cls-2" d="M555.31,70.56l-13.16.81a6.9,6.9,0,0,0-1.45-3.05,7.89,7.89,0,0,0-2.92-2.19,10.24,10.24,0,0,0-4.3-.83,10.64,10.64,0,0,0-5.63,1.4,4.23,4.23,0,0,0-2.3,3.73A4,4,0,0,0,527,73.57a11.72,11.72,0,0,0,5.09,2.05l9.38,1.89q7.56,1.56,11.27,5a11.75,11.75,0,0,1,3.71,9,14.2,14.2,0,0,1-3,8.94,19.55,19.55,0,0,1-8.16,6,31.16,31.16,0,0,1-11.93,2.14q-10.29,0-16.38-4.3a16.67,16.67,0,0,1-7.13-11.73L524,91.85a7.26,7.26,0,0,0,3.11,4.78,13.18,13.18,0,0,0,12.4.16A4.43,4.43,0,0,0,541.88,93a4,4,0,0,0-1.65-3.22,12,12,0,0,0-5-1.94l-9-1.79q-7.59-1.51-11.28-5.26a13,13,0,0,1-3.7-9.55,13.88,13.88,0,0,1,2.72-8.6,17.16,17.16,0,0,1,7.66-5.57,31.41,31.41,0,0,1,11.58-2q9.82,0,15.47,4.15A16.06,16.06,0,0,1,555.31,70.56Z"/>
                    <path class="cls-2" d="M572.78,49.11A7.75,7.75,0,0,1,567.3,47a6.9,6.9,0,0,1,0-10.27,8.09,8.09,0,0,1,10.94,0,6.93,6.93,0,0,1,0,10.28A7.72,7.72,0,0,1,572.78,49.11Zm-7.22,58.5V55.79h14.37v51.82Z"/>
                    <path class="cls-2" d="M614.82,108.62a26.3,26.3,0,0,1-13.58-3.36,22.64,22.64,0,0,1-8.83-9.38,30.19,30.19,0,0,1-3.1-14,30.37,30.37,0,0,1,3.1-14.05,22.64,22.64,0,0,1,8.83-9.38,29.1,29.1,0,0,1,27.15,0,22.64,22.64,0,0,1,8.83,9.38,30.37,30.37,0,0,1,3.1,14.05,30.19,30.19,0,0,1-3.1,14,22.64,22.64,0,0,1-8.83,9.38A26.29,26.29,0,0,1,614.82,108.62Zm.06-11.13a8.82,8.82,0,0,0,6-2,12.52,12.52,0,0,0,3.63-5.58,27,27,0,0,0,0-16.12,12.59,12.59,0,0,0-3.63-5.6,8.78,8.78,0,0,0-6-2.06,9,9,0,0,0-6,2.06,12.41,12.41,0,0,0-3.68,5.6,27,27,0,0,0,0,16.12,12.35,12.35,0,0,0,3.68,5.58A9.09,9.09,0,0,0,614.88,97.49Z"/>
                    <path class="cls-2" d="M664.07,77.65v30H649.7V55.79h13.7v9.14h.6a14.6,14.6,0,0,1,5.77-7.17,17.59,17.59,0,0,1,9.82-2.65A18.26,18.26,0,0,1,689,57.47a16,16,0,0,1,6.24,6.74,22.57,22.57,0,0,1,2.23,10.4v33H683.1V77.18c0-3.18-.79-5.65-2.43-7.44a8.73,8.73,0,0,0-6.78-2.68,10.2,10.2,0,0,0-5.11,1.24,8.56,8.56,0,0,0-3.44,3.63A12.45,12.45,0,0,0,664.07,77.65Z"/>
                    <path class="cls-1" d="M721,107.61H705.29l23.86-69.09H748l23.82,69.09H756.13L738.83,54.3h-.54Zm-1-27.16h37v11.4H720Z"/>
                    <path class="cls-1" d="M787.24,49.11A7.76,7.76,0,0,1,781.75,47a6.92,6.92,0,0,1,0-10.27,8.1,8.1,0,0,1,11,0,6.93,6.93,0,0,1,0,10.28A7.76,7.76,0,0,1,787.24,49.11ZM780,107.61V55.79h14.37v51.82Z"/>
                    <path class="cls-3" d="M.46,79.85a45.62,45.62,0,0,0,91.13,0,2.16,2.16,0,0,0-2.15-2.28H58.74a12.72,12.72,0,0,1-25.43,0H2.61A2.17,2.17,0,0,0,.46,79.85ZM46,96.62A18.94,18.94,0,0,0,59.49,91l15,14.95A40,40,0,0,1,46,117.77Z"/>
                    <path class="cls-3" d="M46,83.77a6.21,6.21,0,0,0,6.2-6.2H39.82A6.21,6.21,0,0,0,46,83.77Z"/>
                    <path class="cls-3" d="M52.9,3v7.55a3,3,0,0,1-3,3H48.32v6.79H43.74V13.57H42.2a3,3,0,0,1-3-3V3a3,3,0,0,1,3-3h7.65A3,3,0,0,1,52.9,3Z"/>
                    <path class="cls-2" d="M96.76,36.77V29.46a2.05,2.05,0,0,1,2-2.06h0a2.06,2.06,0,0,1,2.06,2.06v7.31a2.06,2.06,0,0,1-2.06,2.06h0A2.05,2.05,0,0,1,96.76,36.77Z"/>
                    <path class="cls-2" d="M103.2,36.77V29.46a2.06,2.06,0,0,1,2.06-2.06h0a2.06,2.06,0,0,1,2.06,2.06v7.31a2.06,2.06,0,0,1-2.06,2.06h0A2.06,2.06,0,0,1,103.2,36.77Z"/>
                    <rect class="cls-2" x="109.65" y="27.4" width="4.11" height="11.43" rx="0.91"/>
                    <path class="cls-2" d="M116.1,36.77V29.46a2.05,2.05,0,0,1,2.05-2.06h0a2.06,2.06,0,0,1,2.06,2.06v7.31a2.06,2.06,0,0,1-2.06,2.06h0A2.05,2.05,0,0,1,116.1,36.77Z"/>
                    <path class="cls-2" d="M96.76,50.86V43.55a2.05,2.05,0,0,1,2-2.06h0a2.06,2.06,0,0,1,2.06,2.06v7.31a2.06,2.06,0,0,1-2.06,2.06h0A2.05,2.05,0,0,1,96.76,50.86Z"/>
                    <path class="cls-2" d="M103.2,50.86V43.55a2.06,2.06,0,0,1,2.06-2.06h0a2.06,2.06,0,0,1,2.06,2.06v7.31a2.06,2.06,0,0,1-2.06,2.06h0A2.06,2.06,0,0,1,103.2,50.86Z"/>
                    <path class="cls-2" d="M96.76,65V57.64a2.05,2.05,0,0,1,2-2h0a2.05,2.05,0,0,1,2.06,2V65A2.05,2.05,0,0,1,98.81,67h0A2.05,2.05,0,0,1,96.76,65Z"/>
                    <path class="cls-2" d="M103.2,65V57.64a2.05,2.05,0,0,1,2.06-2h0a2.05,2.05,0,0,1,2.06,2V65A2.05,2.05,0,0,1,105.26,67h0A2.05,2.05,0,0,1,103.2,65Z"/>
                    <rect class="cls-2" x="109.65" y="55.59" width="4.11" height="11.43" rx="0.91"/>
                    <path class="cls-4" d="M83.53,20.36h-75A8.52,8.52,0,0,0,0,28.87v40a2.3,2.3,0,0,0,2.29,2.29H89.76a2.3,2.3,0,0,0,2.29-2.29v-40A8.52,8.52,0,0,0,83.53,20.36ZM28.85,56.08A10.31,10.31,0,1,1,39.16,45.77,10.31,10.31,0,0,1,28.85,56.08Zm34.35,0A10.31,10.31,0,1,1,73.5,45.77,10.31,10.31,0,0,1,63.2,56.08Z"/>
                    </g>
                    </g>
                    </svg>
               </div>
               <div class="col-md-9 mt-md0 mt15">
                  <ul class="leader-ul f-20 w400 white-clr text-md-end text-center">
                     <li>
                        <a href="#features" class="white-clr t-decoration-none">Features</a><span class="pl15">|</span>
                     </li>
                     <li>
                        <a href="#productdemo" class="white-clr t-decoration-none">Demo</a>
                     </li>
                     <li class="affiliate-link-btn">
                        <a href="#buynow">Buy Now</a>
                     </li>
                  </ul>
               </div>
               <div class="col-12 text-center lh150 mt20 mt-md50">
                  <div class="pre-heading f-18 f-md-22 w500 lh140 white-clr">
                  Finally! The Days of Dull & Robotic Voices Are Over…
                  </div>
               </div>
               <div class="col-12 mt-md30 mt20 f-md-40 f-24 w400 text-center white-clr lh140">
               World’s First Super-Trending <span class="orange-clr">"IBM's Watson & ChatGPT4" Powered Ai App</span>  Generates Us
                  <span class="w700">Real Human Emotion Based Voices, Audiobooks, Podcasts, & Even Unique Content</span>  <u> with Just 1 Keyword. </u>
               </div>
               <div class="col-12 mt-md30 mt15 text-center">
                  <div class="f-20 f-md-24 w500 lh200 yellow-clr">
                   <span class="yellow-brush black-clr w600">Then Sell Them to Our Secret Buyers Network of 3.2 million Users...</span> <br>
                    <span class="w700 white-clr">& Making Us <u>$382.38 Daily with Literally Zero Work.</u> Even A 100% Beginner Can Do It.</span>
                  </div>
               </div>
               <!-- <div class="col-12 mt20 mt-md35 text-center">
                  <div class="f-20 f-md-26 w400 lh140 white-clr">
                  Without Creating Anything | Without Writing Content | Without Recording               
                  </div>
               </div> -->
               <div class="col-12 mt20 text-center">
                  <div class="f-18 f-md-22 w400 lh140 red-bg white-clr">
                  Without Hiring Anyone - No Writing Content - No Experience – No Hidden Fee - No BS
                  </div>
               </div>
            </div>

            <div class="row mt20 mt-md40 gx-md-5 align-items-center">
               <div class="col-md-7 col-12">
                  <div class="col-12">
                     <!-- <div class="f-18 f-md-19 w500 lh140 white-clr shape-head">
                     Watch How We Swiped A Profitable Funnel With Just One Click And Made $30,345.34 Per Month On Complete Autopilot…
                     </div> -->
                     <div class="video-box mt20 mt-md30">
                        <!-- <img src="assets/images/video-thumb.webp" alt="Video" class="mx-auto d-block img-fluid"> -->
                        <div style="padding-bottom: 56.25%;position: relative;">
                        <iframe src="https://voicefusionai.oppyo.com/video/embed/i6871gjjt7" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div>
                     </div>
                  </div>
                  <div class="col-12 mt20 mt-md50">
                     <div class="border-dashed">
                        <div class="f-18 f-md-20 w500 lh140 text-center grey-clr">
                        First 99 Action Takers Get Instant Access To Our Top 5 Profitable DFY AudioBooks For
                        <span class="orange-clr"><u>FREE</u><br class="d-none d-md-block"> (Average User Saw 475% Increase In Profit worth $1,997)</span>
                        </div>
                     </div>
                     <div class="f-22 f-md-24 w700 lh150 text-center mt20 mt-md30 white-clr">
                     Get VoiceFusionAi For Low One Time Fee… <br class="d-none d-md-block"> Just Pay Once Instead Of <del class="red-clr w500"> $97 Monthly</del>
                     </div>
                     <div class="row">
                        <div class="col-md-12 mx-auto col-12 mt20 mt-md20 text-center affiliate-link-btn1">
                           <a href="#buynow">>>> Get Instant Access To VoiceFusionAi <<<</a>
                        </div>
                        <div class="col-12 mt20 text-center">
                           <div class="f-18 f-md-20 w300 lh140 white-clr">
                              Special Bonus* <span class="w600">"30 Reseller Licenses"</span> If You Buy Today         
                           </div>
                        </div>
                     </div>
                     <div class="col-12 mt20 mt-md30">
                        <img src="assets/images/compaitable-gurantee1.webp" alt="Compaitable Gurantee" class="mx-auto d-block img-fluid">
                     </div>

                     <div class="mt20 mt-md30">
                        <div class="countdown counter-white text-center">
                           <div class="timer-label text-center"><span class="f-24 f-md-42 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
                           <div class="timer-label text-center"><span class="f-24 f-md-42 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                           <div class="timer-label text-center timer-mrgn"><span class="f-24 f-md-42 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                           <div class="timer-label text-center "><span class="f-24 f-md-42 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-5 col-12 mt20 mt-md0">
                  <div class="key-features-bg">
                     <ul class="list-head pl0 m0 f-16 f-md-18 lh160 w400 white-clr">
                        <li><span class="w700">Eliminate All the Guesswork</span> and Jump Straight to Results</li>
                        <li>Let Ai Create Real Human Emotion Based Voices</li>
                        <li>Create Complete <span class="w700">AudioBooks & Podcasts</span> Easily</li>
                        <li><span class="w700">Even Ai Write High Quality Unique Content for You</span></li>
                        <li><span class="w700">A True Ai App</span> That Leverages IBM's Watson & ChatGPT4 Technology</li>
                        <li><span class="w700">Instant High Converting Traffic For 100% Free…</span> No Need to Run Ads</li>
                        <li><span class="w700">300+ Real Voices</span> to Choose from with Human Emotions</li>
                        <li><span classs="w700">100+ Languages</span> to Choose From</li>
                        <li><span class="w700">DFY 1 million Articles</span> with Full PLR License</li>
                        <li><span class="w700">Create Unlimited</span> VSL’s, Sales Copies, Emails, Ads Copy Etc.</li>
                        <li><span class="w700">Turn Your Old Boring Robotic Voices</span> into a Real Human Voices</li>
                        <li>98% Of Beta Testers Made <span class="w700">At Least One Sale Within 24 Hours</span> of Using VoiceFusionAi</li>
                        <li><span class="w700">Pay Only Once</span> & Get Profit Forever</li>
                        <li><span class="w700">No Complicated Setup</span> - Get Up and Running In 2 Minutes</li>
                        <li> <span class="w700">30 Days Money-Back Guarantee</span></li>
                        <li><span class="w700">Free COMMERCIAL LICENSE Included</span> - Serve Unlimited Clients</li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--1. Header Section End -->

   <!-- Limited Section Start    -->
      <div class="limited-time-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="row align-items-center flex-column flex-md-row justify-content-center">
                     <div class="col-md-1">
                        <img src="assets/images/caution-icon.webp" class="img-fluid mr-md5 mb5 mb-md0" alt="Caution Icon">
                     </div>
                     <div class="col-md-8 col-12 f-20 f-md-22 w300 text-center white-clr">
                        <span class="f-20 f-md-22 w600">FAIR WARNING! After Every Hour The Price Goes Up,</span>
                        <br> So ActNow To Claim Your 81% DISCOUNT Or Come Later & Pay More
                     </div>
                     <div class="col-md-1">
                        <img src="assets/images/caution-icon.webp" class="img-fluid mr-md5 mb5 mb-md0" alt="Caution Icon">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div> 
   <!-- Limited Section End -->

   <div class="step-section">
      <div class="container ">
         <div class="row ">
            <div class="col-12 f-28 f-md-48 w700 lh140 black-clr text-center">
            Start Your Own Content & Voice Over Agency <br class="d-none d-md-block"> In 3 Simple Steps
            </div>
            <!-- <div class="col-12 f-24 f-md-28 w600 lh140 white-clr text-center mt20">
               <div class="post-heading">
               Start Dominating ANY Niche With DFY AI Funnels…
               </div>
            </div> -->
            <div class="col-12 mt30 mt-md100">
               <div class="row align-items-center ">
                  <div class="col-12 col-md-6">
                     <div class="step-shapes f-20 f-md-22 w600 white-clr">
                        Step 1
                     </div>
                     <div class="f-20 f-md-24 w500 lh140 mt5 mt-md10 text-capitalize">
                        Login To Members Area
                     </div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 ">
                     <img src="assets/images/step1-img.png" alt="Video" class="mx-auto d-block img-fluid">
                  </div>
               </div>
            </div>
            <div class="col-12">
               <img src="assets/images/left-arrow.webp" class="img-fluid d-none d-md-block mx-auto " alt="Left Arrow">
            </div>
            <div class="col-12 mt30 mt-md30">
               <div class="row align-items-center ">
                  <div class="col-12 col-md-6 order-md-2">
                     <div class="step-shapes f-20 f-md-22 w600 white-clr">
                        Step 2
                     </div>
                     <div class="f-20 f-md-24 w500 lh140 mt5 mt-md10 text-capitalize">
                     Just Enter A Keyword Or Upload Your Old Voice, Text File etc
                     </div>
                  </div>
                  <div class="col-md-6 col-md-pull-6 mt20 mt-md0 order-md-1 ">
                     <img src="assets/images/step2-img.png" alt="Video" class="mx-auto d-block img-fluid">
                  </div>
               </div>
            </div>
            <div class="col-12">
               <img src="assets/images/right-arrow.webp" class="img-fluid d-none d-md-block mx-auto" alt="Right Arrow">
            </div>
            <div class="col-12 mt30 mt-md30">
               <div class="row align-items-center ">
                  <div class="col-12 col-md-6">
                     <div class="step-shapes f-20 f-md-22 w600 white-clr">
                        Step 3
                     </div>
                     <div class="f-20 f-md-24 w500 lh140 mt5 mt-md10 text-capitalize">
                        GPT4 Technology Will Generate Unique High Quality Content & Emotion Based Real Human Voice Overs..<span class="red-clr"> Also Convert Them To Classy AudioBooks</span>
                     </div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 ">
                     <img src="assets/images/step3-img.png" alt="Video" class="mx-auto d-block img-fluid">
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>

   <div class="grey-section">
      <div class="container">
         <div class="row">
            <div class="col-12 f-28 f-md-36 w700 lh140 black-clr text-center">
               Thousands of People Who Are Tired of Using Same Expensive <br class="d-none d-md-block"> Old Boring Robotic Voices & Copyrighted Content
            </div>
            <div class="col-12 f-24 f-md-28 w600 lh140 white-clr text-center mt20">
               <div class="post-heading">
                  Shifted to VoiceFusion Ai For Unbelievable Results
               </div>
            </div>
         </div>
         <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 gap30 mt20 mt-md10 mx5">
            <div class="col">
               <div class="t-box">
                  <div class="figure">
                     <img src="assets/images/t1.png" alt="DFY Websites" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption text-center">
                        <div class="f-18 f-md-20 w400 lh140 mt20 black-clr">Professional & Fully Cloud-Based Platform</div>
                     </div>
                  </div>
               </div>
            </div>

            <div class="col">
               <div class="t-box">
                  <div class="figure">
                     <img src="assets/images/t2.png" alt="DFY Content" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption text-center">
                        <div class="f-18 f-md-20 w400 lh140 mt20 black-clr">Latest ChatGPT4 Version Powered Platform</div>
                     </div>
                  </div>
               </div>
            </div>      

            <div class="col">
               <div class="t-box">
                  <div class="figure">
                     <img src="assets/images/t3.png" alt="DFY Translation" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption text-center">
                        <div class="f-18 f-md-20 w400 lh140 mt20 black-clr">ChatGPT4 Powered Content Generator Feature</div>
                     </div>
                  </div>
               </div>
            </div>

            <div class="col">
               <div class="t-box">
                  <div class="figure">
                     <img src="assets/images/t4.png" alt="DFY Designs" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption text-center">
                        <div class="f-18 f-md-20 w400 lh140 mt20 black-clr">ChatGPT4 Powered Real Human & Emotion Based Voice Over Feature</div>
                     </div>
                  </div>
               </div>
            </div>

            <div class="col">
               <div class="t-box">
                  <div class="figure">
                     <img src="assets/images/t5.png" alt="DFY SEO Optimization" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption text-center">
                        <div class="f-18 f-md-20 w400 lh140 mt20 black-clr">ChatGPT4 Powered AudioBooks Creator Feature</div>
                     </div>
                  </div>
               </div>
            </div>

            <div class="col">
               <div class="t-box">
                  <div class="figure">
                     <img src="assets/images/t6.png" alt="DFY Mobile Optimization" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption text-center">
                        <div class="f-18 f-md-20 w400 lh140 mt20 black-clr">300+ Real Voices To Choose From</div>
                     </div>
                  </div>
               </div>
            </div>

            <div class="col">
               <div class="t-box">
                  <div class="figure">
                     <img src="assets/images/t7.png" alt="DFY Traffic" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption text-center">
                        <div class="f-18 f-md-20 w400 lh140 mt20 black-clr">100+ Languages To Choose From</div>
                     </div>
                  </div>
               </div>
            </div>

            <div class="col">
               <div class="t-box">
                  <div class="figure">
                     <img src="assets/images/t8.png" alt="DFY Social Media Syndication" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption text-center">
                        <div class="f-18 f-md-20 w400 lh140 mt20 black-clr">Inbuilt DFY 1 Million Articles With Full PLR License</div>
                     </div>
                  </div>
               </div>
            </div>

            <div class="col">
               <div class="t-box">
                  <div class="figure">
                     <img src="assets/images/t9.png" alt="DFY CRM" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption text-center">
                        <div class="f-18 f-md-20 w400 lh140 mt20 black-clr">Create Unlimited VSL's, Sales Copies, Emails, Ads Copy Etc.</div>
                     </div>
                  </div>
               </div>
            </div>

            <div class="col">
               <div class="t-box">
                  <div class="figure">
                     <img src="assets/images/t10.png" alt="DFY Appointment Schedule" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption text-center">
                        <div class="f-18 f-md-20 w400 lh140 mt20 black-clr">Switch Your Old Boring Robotic Voices into a Real Human Like Emotion Based Voices</div>
                     </div>
                  </div>
               </div>
            </div>

            <div class="col">
               <div class="t-box">
                  <div class="figure">
                     <img src="assets/images/t11.png" alt="DFY Templates" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption text-center">
                        <div class="f-18 f-md-20 w400 lh140 mt20 black-clr">Pay Once & Get Profit Forever Without Any Restrictions</div>
                     </div>
                  </div>
               </div>
            </div>

            <div class="col">
               <div class="t-box">
                  <div class="figure">
                     <img src="assets/images/t12.png" alt="DFY WooComerce Integration" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption text-center">
                        <div class="f-18 f-md-20 w400 lh140 mt20 black-clr">Inbuilt “Social Sharing” Feature For Getting Limitless Traffic</div>
                     </div>
                  </div>
               </div>
            </div>

            <div class="col">
               <div class="t-box">
                  <div class="figure">
                     <img src="assets/images/t13.png" alt="DFY WooComerce Integration" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption text-center">
                        <div class="f-18 f-md-20 w400 lh140 mt20 black-clr">COMMERCIAL LICENSE Included</div>
                     </div>
                  </div>
               </div>
            </div>

            <div class="col">
               <div class="t-box">
                  <div class="figure">
                     <img src="assets/images/t14.png" alt="DFY WooComerce Integration" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption text-center">
                        <div class="f-18 f-md-20 w400 lh140 mt20 black-clr">No Limitations - Completely Free</div>
                     </div>
                  </div>
               </div>
            </div>

            <div class="col">
               <div class="t-box">
                  <div class="figure">
                     <img src="assets/images/t15.png" alt="DFY WooComerce Integration" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption text-center">
                        <div class="f-18 f-md-20 w400 lh140 mt20 black-clr">No Special Skills or Experience Required</div>
                     </div>
                  </div>
               </div>
            </div>

            <div class="col">
               <div class="t-box">
                  <div class="figure">
                     <img src="assets/images/t16.png" alt="DFY WooComerce Integration" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption text-center">
                        <div class="f-18 f-md-20 w400 lh140 mt20 black-clr">Sell Unlimited Content, Voice Overs, AudioBooks & Earn Like The Big Boys</div>
                     </div>
                  </div>
               </div>
            </div>

            <div class="col">
               <div class="t-box">
                  <div class="figure">
                     <img src="assets/images/t17.png" alt="DFY WooComerce Integration" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption text-center">
                        <div class="f-18 f-md-20 w400 lh140 mt20 black-clr">Step by Step Training</div>
                     </div>
                  </div>
               </div>
            </div>

            <div class="col">
               <div class="t-box">
                  <div class="figure">
                     <img src="assets/images/t18.png" alt="DFY WooComerce Integration" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption text-center">
                        <div class="f-18 f-md-20 w400 lh140 mt20 black-clr">And much more…</div>
                     </div>
                  </div>
               </div>
            </div>

         </div>
      </div>
   </div>
   
   <!-- CTA Section Start -->
   <div class="cta-section-white">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-34 w700 lh140 text-center white-clr">
                  Get VoiceFusion Ai For Low One Time Fee… <br class="d-none d-md-block"> 
               </div>
               <div class="f-22 f-md-24 w400 lh140 text-center white-clr mt10">
                  <span class="w700">Just Pay Once</span> Instead Of <s class="red-clr">$97 Monthly</s>
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md50">
            <div class="col-md-7 text-center">
               <a href="#buynow" class="cta-link-btn"> Get Instant Access To VoiceFusion Ai </a>
               <div class="f-18 f-md-20 w300 lh140 white-clr text-center mt20">
                  Special Bonus* <span class="w600">"30 Reseller Licenses"</span> If You Buy Today         
               </div>
               <img src="assets/images/compaitable-gurantee1.webp" alt="Compaitable Gurantee" class="mx-auto d-block img-fluid mt20 mt-md30">
            </div>
            <div class="col-md-5 mt20 mt-md0">
               <div class="countdown-container">
                  <div class="f-20 f-md-24 w400 lh140 text-center white-clr mb10">
                     <span class="w700 yellow-clr">Hurry up!</span>  Price Increases In
                  </div>
                  <div class="countdown counter-white text-center">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 smmltd">Days</span> 
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-45 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> 
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- CTA Section End -->

   <!-- Limited Section Start    -->
    <div class="limited-time-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="row align-items-center flex-column flex-md-row justify-content-center">
                     <div class="col-md-1">
                        <img src="assets/images/caution-icon.webp" class="img-fluid mr-md5 mb5 mb-md0" alt="Caution Icon">
                     </div>
                     <div class="col-md-8 col-12 f-20 f-md-22 w300 text-center white-clr">
                        <span class="f-20 f-md-22 w600">FAIR WARNING! After Every Hour The Price Goes Up,</span>
                        <br> So ActNow To Claim Your 81% DISCOUNT Or Come Later & Pay More
                     </div>
                     <div class="col-md-1">
                        <img src="assets/images/caution-icon.webp" class="img-fluid mr-md5 mb5 mb-md0" alt="Caution Icon">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div> 
   <!-- Limited Section End -->
   
   <!-- Testimonial Section Start -->
   <div class="testimonial-sec">
      <div class="container ">
         <div class="row ">
            <div class="col-12 f-28 f-md-50 w500 lh140 black-clr text-center">
               People on The Internet are
            </div>
            <div class="col-12 f-22 f-md-50 w700 lh140 red-clr text-center">
               Loving VoiceFusion Ai 
               <img src="assets/images/red-underline-brush.png" class="img-fluid mx-auto d-block" alt="brush">
            </div>
         </div>
         <div class="row row-cols-md-2 row-cols-1 gx4 mt-md100 mt0">
            <div class="col mt20 mt-md50">
               <div class="single-testimonial">
                  <div class="st-img">
                     <img src="assets/images/craig-mcintosh.webp" class="img-fluid d-block mx-auto " alt="Craig McIntosh">
                  </div>
                  <div class="mt20 md-md50 f-24 f-md-28 w600 lh140 black-clr">Craig McIntosh </div>
                  <div class="mt10 f-20 f-md-22 w400 lh140 black-clr">
                     Digital Marketer and Internet Marketer.
                  </div>
                  <div class="stars mt10">
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                  </div>
                  <img src="assets/images/quote.webp" alt="Quotes" class="mx-auto d-block img-fluid mt20 mt-md50 ">
                  <p class="mt20 f-18 f-md-20 lh140 w400 text-center">
                  This software is a life saver, A wide range of voices and languages that blow your mind, I tried some other tools that offer lifetime deal but t hey are very limited for voices/languages and you should pay again to add more features... VoiceFusion Ai is way better and very useful for me.
                  </p>
               </div>
            </div>
            <div class="col mt20 mt-md50">
               <div class="single-testimonial">
                  <div class="st-img">
                     <img src="assets/images/edward-reid.webp" class="img-fluid d-block mx-auto " alt="Edward Reid">
                  </div>
                  <div class="mt20 md-md50 f-24 f-md-28 w600 lh140 black-clr"> James Milo  </div>
                  <div class="mt10 f-20 f-md-22 w400 lh140 black-clr">
                  Founder,  AI Marketing and Sales Software
                  </div>
                  <div class="stars mt10">
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                  </div>
                  <img src="assets/images/quote.webp" alt="Quotes" class="mx-auto d-block img-fluid mt20 mt-md50 ">
                  <p class="mt20 f-18 f-md-20 lh140 w400 text-center">
                     I can say that VoiceFusion Ai has been optimized and developed nicely since their launch, I tried it a days ago and now it become more mature with high quality voices and more features, Great work guys keep it up
                  </p>
               </div>
            </div>
            <div class="col mt-md120 mx-auto">
               <div class="single-testimonial">
                  <div class="st-img">
                     <img src="assets/images/jaeden-downs.webp" class="img-fluid d-block mx-auto " alt="Jaeden Downs">
                  </div>
                  <div class="mt20 md-md50 f-24 f-md-28 w600 lh140 black-clr"> Jaeden Downs  </div>
                  <div class="stars mt10">
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                  </div>
                  <img src="assets/images/quote.webp" alt="Quotes" class="mx-auto d-block img-fluid mt20 mt-md50 ">
                  <p class="mt20 f-18 f-md-20 lh140 w400 text-center">
                     Best Text To Speech ever, literally love this thing. I have dumped my old robotic voice overs subscriptions with this.. High quality audio, never experienced tech like this before. Well done to the VoiceFusion Ai team!
                  </p>
               </div>
            </div>
            <div class="col mt-md120 mx-auto">
               <div class="single-testimonial">
                  <div class="st-img">
                     <img src="assets/images/stuart-johnson.webp" class="img-fluid d-block mx-auto " alt="Drew Warren">
                  </div>
                  <div class="mt20 md-md50 f-24 f-md-28 w600 lh140 black-clr"> Drew Warren  </div>
                  <div class="stars mt10">
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                  </div>
                  <img src="assets/images/quote.webp" alt="Quotes" class="mx-auto d-block img-fluid mt20 mt-md50 ">
                  <p class="mt20 f-18 f-md-20 lh140 w400 text-center">
                     Best real time voice changer out there right now. Extremely realistic voices with an amazing community and incredibly receptive development team. Kudos To Team VoiceFusion Ai.
                  </p>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Testimonial Section End -->


   <div class="grey-section">
      <div class="container">
         <div class="row">
            <div class="col-12 f-28 f-md-50 w500 lh140 black-clr text-center">
               Even We Used VoiceFusion Ai To Generate
            </div>
            <div class="col-12 f-22 f-md-50 w700 lh140 red-clr text-center">
               $17,975 In Just 7 Days
            <img src="assets/images/red-underline-brush.png" class="img-fluid mx-auto d-block" alt="brush">
            </div>
            <div class="col-12 mt30 mt-md80">
               <img src="assets/images/generate-girl.png" class="img-fluid mx-auto d-block" alt="brush">
            </div>
         </div>
      </div>
   </div>


   <div class="notalong-section">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center f-20 f-md-28 w500 lh140 black-clr text-center">
               <div class="notalong-design">
                  <span class="w600 red-clr f-md-50 f-28">And We're not along</span><br>
                     Over <span class="w600"> 150+ TOP Agency Owners, <br class="d-none d-md-block"> Freelancers, </span> & more than 8000 customers<br class="d-none d-md-block"> have <span class="red-clr">shifted to VoiceFusionAi </span>for unique & <br class="d-none d-md-block"> Never Seen Before Features.   
               </div>
            </div>
         </div>
      </div>
   </div>


   <div class="itshuge-section">
      <div class="container">
         <div class="row">
            <div class="col-12 f-22 f-md-36 w600 lh140 black-clr text-center">
               Till Now VoiceFusion Ai Has Generated Over 10 Million Contents, AudioBooks & Voice Overs In Any Niche.. TOTALLY UNIQUE
            </div>
            <div class="col-12 mt20 mt-md40 text-center">
               <div class="f-22 f-md-50 w700 lh140 red-clr text-center its-huge">
                  IT'S HUGE!!
               </div>
            </div>
            <div class="col-12 f-18 f-md-28 w600 lh140 black-clr text-center mt20 mt-md20">
               And now it's your turn to use it for your Business to
            </div>
            <div class="row align-items-center">
               <div class="col-md-6 col-12">
                  <img src="assets/images/itshuge-img.png" class="img-fluid mx-auto d-block" alt="brush">
               </div>
               <div class="col-md-6 col-12">
                  <div class="col-12 f-20 f-md-28 w700 lh140 red-clr">
                     Skyrocket your conversion & profit
                  </div>
                  <div class="col-12 f-20 f-md-28 w400 lh140 black-clr mt10">
                     And now it's your turn to use it for your Business to
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>


    <!-- CTA Section Start -->
    <div class="cta-section-white">
      <div class="container">
         <div class="row">
            <div class="col-12">
            <div class="f-28 f-md-34 w700 lh140 text-center white-clr">
                  Get VoiceFusion Ai For Low One Time Fee… <br class="d-none d-md-block"> 
               </div>
               <div class="f-22 f-md-24 w400 lh140 text-center white-clr mt10">
                  <span class="w700">Just Pay Once</span> Instead Of <s class="red-clr">$97 Monthly</s>
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md50">
            <div class="col-md-7 text-center">
               <a href="#buynow" class="cta-link-btn"> Get Instant Access To VoiceFusion Ai </a>
               <div class="f-18 f-md-20 w300 lh140 white-clr text-center mt20">
                  Special Bonus* <span class="w600">"30 Reseller Licenses"</span> If You Buy Today         
               </div>
               <img src="assets/images/compaitable-gurantee1.webp" alt="Compaitable Gurantee" class="mx-auto d-block img-fluid mt20 mt-md30">
            </div>
            <div class="col-md-5 mt20 mt-md0">
               <div class="countdown-container">
                  <div class="f-20 f-md-24 w400 lh140 text-center white-clr mb10">
                     <span class="w700 yellow-clr">Hurry up!</span>  Price Increases In
                  </div>
                  <div class="countdown counter-white text-center">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 smmltd">Days</span> 
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-45 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> 
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- CTA Section End -->
 <!-- Limited Section Start    -->
 <div class="limited-time-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="row align-items-center flex-column flex-md-row justify-content-center">
                     <div class="col-md-1">
                        <img src="assets/images/caution-icon.webp" class="img-fluid mr-md5 mb5 mb-md0" alt="Caution Icon">
                     </div>
                     <div class="col-md-8 col-12 f-20 f-md-22 w300 text-center white-clr">
                        <span class="f-20 f-md-22 w600">FAIR WARNING! After Every Hour The Price Goes Up,</span>
                        <br> So ActNow To Claim Your 81% DISCOUNT Or Come Later & Pay More
                     </div>
                     <div class="col-md-1">
                        <img src="assets/images/caution-icon.webp" class="img-fluid mr-md5 mb5 mb-md0" alt="Caution Icon">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div> 
   <!-- Limited Section End -->



   <div class="wordsfirst-section">
      <div class="container">
         <div class="row">
            <div class="col-12 f-22 f-md-36 w400 lh140 black-clr text-center">
               Old Text-To-Speech sounds were robotic, dull & totally emotionless... RIGHT? THEN HERE COMES THE VoiceFusion AI
            </div>
            <div class="col-12 f-22 f-md-50 w600 lh140 red-clr text-center mt20 mt-md40">
               The World's first True AI Voice & Content Generation App Which IsBased On Real Human Emotions
            </div>
            <div class="col-12 f-18 f-md-22 w400 lh140 black-clr text-center mt20 mt-md20">
               With a realistic online voice generator, you can turn text into natural sounding speech in minutes. <span class="w600">It works as a sound generator and helps create realistic synthetic sounds that mimic the nuance and prosody of human speech and voices.</span> Unlike other computer generated voices, VoiceGPT's AI voices do not sound monotonous and robotic.
            </div>
         </div>
      </div>
   </div>

   <div class="white-section">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-22 f-md-45 w600 lh140 black-clr text-center">
                  Still Not Believing? <br>SEE The Difference Between Old Robotic Voice VS VoiceFusion Ai's Human Emotion Based Voice
               </div>
            </div>
         </div>
         <div class="col-md-10 text-center mx-auto">
            <img src="assets/images/green-arrow.png" class="img-fluid mr-md5 mb5 mb-md0" alt="Caution Icon">
         </div>
         <div class="row gx-md-5 align-items-center">
            <div class="col-md-6 col-12">
               <div class="video-box">
                  <div style="padding-bottom: 56.25%;position: relative;"><iframe src="https://voicefusionai.oppyo.com/video/embed/2dw93twgc4" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe></div>
               </div>
               <div class="f-20 f-md-24 w600 lh140 red-clr text-center">
                  WITHOUT VoiceFusion Ai
               </div>
            </div>
            <div class="col-md-6 col-12">
               <div class="video-box">
                  <div style="padding-bottom: 56.25%;position: relative;"><iframe src="https://voicefusionai.oppyo.com/video/embed/icbu28e4jx" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe></div>
               </div>
               <div class="f-20 f-md-24 w600 lh140 red-clr text-center">
                  WITH VoiceFusion Ai
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="frountend">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="f-35 w700 f-md-42 text-center mb30 mb-md-50">
						Checkout Some Real AI Voices By <br><span class="border-v">VoiceFusion Ai</span>
					</div>
				</div>
				<div class="col-md-6 mt30">
					<div class="item bg1 text-center">
						<div class="row align-items-center">
							<div class="col-12">
                        <img src="assets/images/jane.webp" class="img-fluid mx-auto d-block">
                     </div>
						</div>
						<div id="audioContainer">
							
						</div>
					</div>
				</div>			
				<div class="col-md-6 mt30">
					<div class="item bg2 text-center">
						<div class="row align-items-center">
                  <div class="col-12">
                        <img src="assets/images/jason.webp" class="img-fluid mx-auto d-block">
                     </div>
						</div>
						<div id="audioContainermale">	
						</div>
					</div>		
				</div>
            <div class="col-sm-12 text-center">
               <div class="f-md-24 f-16 red-clr mt30 main_box w700">
                     These Are Only Two Samples, Inside "VoiceFusion AI" There Are 300+ Male &amp; Female Voices With 100+ Languages Along With Different Emotions Or Moods
               </div>
            </div>
			</div>
		</div>
	</div>		

   <div class="howmuch-section">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-20 f-md-45 w600 lh140 black-clr text-center">
                  This Is How Much Other Hefty Content & Voice Over Guys Are Charging For Very Less Features With Limited Technology..
               </div>
            </div>  
            <div class="col-md-10 text-center mx-auto mt20 mt-md70">
               <img src="assets/images/howmuch-img.png" class="img-fluid d-block mx-auto" alt="Caution Icon">
            </div>  
         </div>
      </div>
   </div>


   <section class="grey-section">
      <div class="container hi-their-box">
         <div class="row">
            <div class="col-12 text-center">
               <div class="hi-their-shadow">
                  <div class="f-28 f-md-45 lh140 black-clr caveat w700 hi-their-head">
                     Hey There,
                  </div>
               </div>
            </div>
         </div>
         <div class="row mt20 mt-md80">
            <div class="col-md-3 text-center">
               <div class="figure mr15 mr-md0">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/pranshu-gupta.webp" alt="Pranshu Gupta" class="mx-auto d-block img-fluid  img-shadow">
                  <div class="figure-caption mt10">
                     <div class="f-24 f-md-28 w700 lh140 black-clr caveat text-center">
                        Pranshu Gupta
                     </div>
                  </div>
               </div>
               <div class="figure mt20 mt-md30">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/bizomart-team.webp" alt="Bizomart" class="mx-auto d-block img-fluid  img-shadow">
                  <div class="figure-caption mt10">
                     <div class="f-24 f-md-28 w700 lh140 black-clr caveat text-center">
                        Bizomart
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-md-9 mt20 mt-md0">
               <div class="f-28 f-md-38 lh140 black-clr w400">
                  It's <span class="w600">Pranshu</span> along with <span class="w600">Bizomart.</span> 
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr mt20 mt-md30">
                  I'm a full time internet marketer…
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr mt20">
                  Over the years, I tried dozens (it could be hundreds even…) of methods to make money online…
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr mt20">
                  But the only thing that gave me the life I always wanted was…
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr mt20">
                  Creating websites…
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr mt20">
                  It's the easiest, and fastest way to build wealth…
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr mt20">
                  Not just make some pocket change…&nbsp;
               </div>
               <div class="f-20 f-md-22 lh140 w600 black-clr mt20">
                  Thanks to that…
               </div>
            </div>
         </div>

         <div class="row mt20 mt-md50">
            <div class="col-12">
               <div class="blue-border"></div>
            </div>
         </div>

         <div class="row mt20 mt-md50">
            <div class="col-12 text-center">
               <div class="f-22 f-md-28 lh140 black-clr w600">
                  I can spend my time as I please
               </div>
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/red-underline.webp" alt="Quotes" class="mx-auto d-block img-fluid  mt10">
            </div>
         </div>

         <div class="row mt20 mt-md50">
            <div class="col-md-3">
               <figure>
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/family.webp" alt="Family" class="mx-auto d-block img-fluid ">
                  <figure-caption>
                     <div class="f-20 f-md-22 w600 lh140 black-clr mt10 text-center">
                        Spend it with my family
                     </div>
                  </figure-caption>
               </figure>
            </div>
            <div class="col-md-3">
               <figure>
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/travel.webp" alt="Travel" class="mx-auto d-block img-fluid ">
                  <figure-caption>
                     <div class="f-20 f-md-22 w600 lh140 black-clr mt10 text-center">
                        Travel
                     </div>
                  </figure-caption>
               </figure>
            </div>
            <div class="col-md-3">
               <figure>
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/sunset-beach.webp" alt="Enjoy a sunset on the beach" class="mx-auto d-block img-fluid ">
                  <figure-caption>
                     <div class="f-20 f-md-22 w600 lh140 black-clr mt10 text-center">
                        Enjoy a sunset on the beach
                     </div>
                  </figure-caption>
               </figure>
            </div>
            <div class="col-md-3">
               <figure>
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/video-games.webp" alt="Or even play video games on my PS5" class="mx-auto d-block img-fluid ">
                  <figure-caption>
                     <div class="f-20 f-md-22 w600 lh140 black-clr mt10 text-center">
                        Or even play video games on my PS5
                     </div>
                  </figure-caption>
               </figure>
            </div>
         </div>
         <div class="row mt20 mt-md80">
            <div class="col-12 text-center">
               <div class="f-20 f-md-24 w600 lh140 black-clr">
                  And the best part is… No matter what I do with my day… I still make a lot of money…
               </div>
               <div class="f-20 f-md-24 w400 lh140 black-clr mt20">
                  And I'm not telling you this to brag… Not at all… I'm telling you this to demonstrate why you should even listen to me…
               </div>
            </div>
         </div>
      </div>
   </section>


   <div class="dollars-section">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-20 f-md-45 w700 lh140 black-clr text-center">
                  Even I've Used to Spent 1000s of Dollars Every Month for Good High Quality Unique Content & Real Human Voice Overs For Creating VSL's or any Sales Scripts  
               </div>
            </div>  
            <div class="col-md-10 text-center mx-auto mt20 mt-md70">
               <img src="assets/images/dollars-img.png" class="img-fluid d-block mx-auto" alt="Caution Icon">
            </div>  
         </div>
      </div>
   </div>


   <!-- <section class="ruined-sec">
      <div class="container">
         <div class="row">
         <div class="col-12 text-center">
            <div class="f-28 f-md-50 w700 white-clr lh140 red-box text-center d-inline-block">
                  ClickFunnel RUINED Our Lives…
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md50">
            <div class="col-md-7">
              
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20 mt-md22">
                  I know you might hate me for that…
               </div>
               <div class="f-20 f-md-24 w400 lh140 black-clr mt20">
                  But that's my opinion and I'm sticking to it…
               </div>
               <div class="f-20 f-md-24 w400 lh140 black-clr mt20">
                  As a matter of fact…
               </div>
               <div class="f-20 f-md-24 w400 lh140 black-clr mt20">
                  It didn't just ruin my life…
               </div>
               <div class="f-20 f-md-24 w400 lh140 black-clr mt20">
                  It ruined yours too… 
               </div>
               <div class="f-20 f-md-24 w400 lh140 black-clr mt20">
                  You just don't realize it yet…
               </div>
            </div>
            <div class="col-md-5 mt20 mt-md0">
               <img src="assets/images/ruined-women.webp" alt="Ruined Women" class="mx-auto d-block img-fluid">
            </div>
         </div>
      </div>
   </section>

   <section class="brainwashed-sec">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-md-6 order-md-2">
               <div class="f-28 f-md-50 w700 black-clr lh140">
                  They Brainwashed Our Minds…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20 mt-md22">
                  “You are just one funnel away from becoming a millionaire..” 
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  How many times did you see this bullsh!t or something similar? 
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  They conditioned us that it's all about the funnel…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  It doesn't matter what you are doing… All you need is to create a new funnel… 
               </div>
               <div class="f-20 f-md-22 w700 lh140 black-clr mt20">
                  But what if it didn't make any money?
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  Doesn't matter, just create a new one… 
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  As long as you keep paying us that monthly payment… 
               </div>
            </div>
            <div class="col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/brainwashed-man.webp" alt="BrainWashed Men" class="mx-auto d-block img-fluid">
            </div>
         </div>
      </div>
   </section>

   <section class="grey-sec">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-md-6">
               <div class="f-28 f-md-42 w400 lh140 black-clr">
               But It Gets Worse… <br class="d-none d-md-block">
               <span class="w700">They</span> <span class="w700 red-clr underline">ALWAYS Blame YOU.</span>
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  It's always your fault…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  They will always tell you
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  “Oh you did the funnel wrong, you need better copywriting or design”
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  It doesn't matter, they will make shit up…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  But they will NEVER admit that maybe this thing doesn't work…
               </div>
            </div>
            <div class="col-md-6 mt20 mt-md0">
               <img src="assets/images/blame-women.webp" alt="Blame Women" class="mx-auto d-block img-fluid">
            </div>
         </div>
      </div>
   </section>

   <section class="white-sec">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-md-6 order-md-2">
               <div class="f-28 f-md-38 w700 lh140 black-clr">
                  They Ever Dare To… <br class="d-none d-md-block">
                  Sell You More Useless Stuff…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  Can you believe it?
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  They have the audacity to sell you MORE stuff…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  And try to convenience you that you didn't make money because you don't have this funnel upgrade…
               </div>
               <div class="f-20 f-md-22 w700 lh140 black-clr mt20">
                  There are horror stories all over the internet…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  Of people paying thousands of dollars for the “best funnel”
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  And as you might have guessed…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  It's more useless shit that they sell you to extract more money from you…
               </div>
            </div>
            <div class="col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/dare-man.webp" alt="Dare Men" class="mx-auto d-block img-fluid">
            </div>
         </div>
      </div>
   </section>


   <section class="grey-sec">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-md-6">
               <div class="f-28 f-md-45 w400 lh140 black-clr">
                  Meanwhile… <br>
                  <span class="w700">They're Making BANK With Their Funnels…</span>
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  Believe it or not…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  All gurus (including Russell Brunson) make most of their money from FUNNELS…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  Don't get confused, just hear me out…
               </div>
               <div class="f-20 f-md-22 w700 lh140 black-clr mt20">
                  They do make millions off of funnels, but not the funnels they show you…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  Not the funnel they show you how to create in clickfunnels…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  Nop, their business is very different than that…
               </div>
            </div>
            <div class="col-md-6 mt20 mt-md0">
               <img src="assets/images/bank-men.webp" alt="Bank Men" class="mx-auto d-block img-fluid">
            </div>
         </div>
      </div>
   </section>

   <section class="white-sec">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-md-6 order-md-2">
               <div class="f-28 f-md-45 w400 lh140 black-clr">
                  Does That Means… <br class="d-none d-md-block">
                 <span class="w700 red-clr">We Can't Use Funnels??</span> 
               </div>
               <div class="f-20 f-md-22 w600 lh140 black-clr mt20">
                  The short answer is… wrong.
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  Let me explain… 
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  Funnels -if done right- is one of the most profitable things you can do online…
               </div>
               <div class="f-20 f-md-22 w700 lh140 black-clr mt20">
                  The issue is not the funnels…
               </div>
               <div class="f-20 f-md-22 w700 lh140 black-clr mt20">
                  The issue is that those gurus hide what's really working…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  They keep their good stuff hidden…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  And try to sell you outdated funnels only…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  It's disgusting I know…
               </div>
            </div>
            <div class="col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/confused-women.webp" alt="Confused Women" class="mx-auto d-block img-fluid">
            </div>
         </div>
      </div>
   </section>

   <section class="blue-sec">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <div class="f-28 f-md-50 w700 white-clr lh140">
                  Here Is The Issue…
               </div>
               <div class="f-28 f-md-50 w700 white-clr lh140 mt20 mt-md30 red-brush">
                  You Build Your Dreams On Guesswork…
               </div>
            </div>
         </div>
         <div class="row mt20 mt-md80 align-items-center">
            <div class="col-md-6">
               <div class="f-20 f-md-22 w400 lh140 white-clr">
                  I'm sorry, but it's true…
               </div>
               <div class="f-20 f-md-22 w400 lh140 white-clr mt20">
                  You go in, create a funnel and hope it works… 
               </div>
               <div class="f-20 f-md-22 w400 lh140 white-clr mt20">
                  Spend all your time and money on it. 
               </div>
               <div class="f-20 f-md-22 w400 lh140 white-clr mt20">
                  HOPING that it will work…
               </div>
               <div class="f-20 f-md-22 w400 lh140 white-clr mt20">
                  Some of us even don't dare to try it…
               </div>
               <div class="f-20 f-md-22 w400 lh140 white-clr mt20">
                  We don't know if it will work or not… 
               </div>
               <div class="f-20 f-md-22 w400 lh140 white-clr mt20">
                  So why should we risk spending money or time creating one…
               </div>
               <div class="f-20 f-md-22 w700 lh140 white-clr mt20">
                  However, there is some good news…
               </div>
            </div>
            <div class="col-md-6 mt20 mt-md0">
               <img src="assets/images/male-coworking-space.webp" alt="Male Coworking" class="mx-auto d-block img-fluid">
            </div>
         </div>
      </div>
   </section>


   <section class="white-sec">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-md-6 order-md-2">
               <div class="f-28 f-md-38 w700 lh140 black-clr">
                  <span class="red-clr">What If…?</span> <br class="d-none d-md-block">
                  There Is A Way To Eliminate ANY Guess Work… 
               </div>
               <div class="f-20 f-md-22 w600 lh140 black-clr mt20">
                  Imagine for a second…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  That there is a way that would allow you to GUARANTEE a funnel is profitable…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  Even before building it…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  Imagine that there is a way to remove any doubt… 
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  And be 100% sure, that this funnel will make you money…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  Before spending a minute working on it…
               </div>
               <div class="f-20 f-md-22 w600 lh140 white-clr mt20 mt-md30 thumsup-text">
                  That would be great, huh?
               </div>
               
            </div>
            <div class="col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/bulb.webp" alt="Bulb" class="mx-auto d-block img-fluid">
            </div>
         </div>
      </div>
   </section>


   <section class="grey-sec">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-md-6">
               <div class="f-28 f-md-45 w700 lh120 black-clr">
                  Well, You Don't Have To Imagine <span class="orange-clr w700">Because AI Did Exactly That…</span> 
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  Unless you been living under a rock…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  You know that AI is taking the world by a storm…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  They are changing the way we do everything…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  And funnels is one of them…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  In fact I'd argue that funnels got the MOST impact from AI…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  It leveled the playing field…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  And gave us the power back to create profitable funnels…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  Just like the gurus, without any of the hard work, or guesswork
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  Let me explain how…
               </div>
            </div>
            <div class="col-md-6 mt20 mt-md0">
               <img src="assets/images/ai-hand.webp" alt="AI Hand" class="mx-auto d-block img-fluid">
            </div>
         </div>
      </div>
   </section>

   <section class="white-sec">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-md-6 order-md-2">
               <div class="f-28 f-md-38 w700 lh140 black-clr">
                  Our AI Model <span class="red-clr">EXPOSED</span>  All Top Guru's Secret… 
               </div>
               <div class="f-20 f-md-22 w600 lh140 black-clr mt20">
                  What we did is… GENIUS…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  We created the world's first AI model that spied on all of the top gurus…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  And got exactly what they are doing online, what funnel they have, and how much money they are making…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  Can you believe it?
               </div>
            </div>
            <div class="col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/exposed.webp" alt="Exposed" class="mx-auto d-block img-fluid">
            </div>
         </div>
      </div>
   </section>

   <section class="grey-sec">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-md-6">
               <div class="f-28 f-md-50 w400 lh140 black-clr">
                 <span class="orange-brush white-clr w600"> Ninja AI </span>Allowed Us  <br class="d-none d-md-block"> <span class="w700"> To Dominate ANY Niche… </span> 
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  This is never done before…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  All we need to do is just choose a niche…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  And Ninja AI will do everything else…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  It will find all the top gurus in that niche…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  And find exactly how they are making the money…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  Then, it will give you all of that on a silver platter…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  All you have to do is just click one button and swipe everything…
               </div>
               <div class="f-20 f-md-22 w600 lh140 black-clr mt20">
                  Isn't that awesome?
               </div>
            </div>
            <div class="col-md-6 mt20 mt-md0">
               <img src="assets/images/dominate-ninjaai.webp" alt="Dominate NinjaAI" class="mx-auto d-block img-fluid">
            </div>
         </div>
      </div>
   </section>

   <section class="white-sec">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-md-6 order-md-2">
               <div class="f-28 f-md-45 w700 lh140 black-clr">
                  The World's First AI “Secret Agent” 
               </div>
               <div class="f-20 f-md-22 w600 lh140 black-clr mt20">
                  It's like having 007 working for you privately…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  How powerful would that be?
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  That's exactly what NinjaAI would do for you…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  It will spy and steal ONLY the profitable campaigns for you… 
               </div>
            </div>
            <div class="col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/secret-agent.webp" alt="Secret Agent" class="mx-auto d-block img-fluid">
            </div>
         </div>
      </div>
   </section>

   <section class="blue-sec1">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-28 f-md-50 w700 lh140 white-clr">
                  Finally…
               </div>
               <div class="f-28 f-md-50 w700 lh140 white-clr purple-brush mt20">
                  ELIMINATE All The Guesswork…
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md50">
            <div class="col-md-6">
               <div class="f-20 f-md-22 w400 lh140 white-clr">
               With Ninja AI…
               </div>
               <div class="f-20 f-md-22 w600 lh140 white-clr mt20">
               You don't have to guess or hope for a working funnel…
               </div>
               <div class="f-20 f-md-22 w400 lh140 white-clr mt20">
               Our AI model with Ninja AI will give you ONLY the funnels that we know for sure works…
               </div>
               <div class="f-20 f-md-22 w400 lh140 white-clr mt20">
               No more split-testing, stressing, and hoping for success…
               </div>
            </div>
            <div class="col-md-6 mt20 mt-md0">
               <img src="assets/images/guesswork-women.webp" alt="GuessWork Women" class="mx-auto d-block img-fluid">
            </div>
         </div>
      </div>
   </section>

   <section class="white-sec">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-md-6 order-md-2">
               <div class="f-28 f-md-50 w700 lh140 black-clr">
                  Ninja AI Blow Any Other Funnel Creator Out Of Water… 
               </div>
               <div class="f-20 f-md-22 w600 lh140 black-clr mt20">
                  Here is the big difference…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  Not one does Ninja AI create stunning funnels… 
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  It's the only app on the market that will give you WORKING funnels… 
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  Funnels that are proven to work and convert NOW. not a year ago… 
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20 funeel-money">
                  Funnels that constantly makes us money like this: 
               </div>
            </div>
            <div class="col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/out-of-water.webp" alt="Men Pulling" class="mx-auto d-block img-fluid">
            </div>
           
         </div>
         <div class="col-12 col-md-12 mt20 mt-md80">
               <img src="assets/images/funnel-money.webp" alt="Funnel Money" class="mx-auto d-block img-fluid">
            </div>
      </div>
   </section>

   <section class="blue-sec1">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-24 f-md-45 w700 lh140 white-clr">
                  Just check yourself and see how the funnels looks in Ninja AI
               </div>
               <div class="mt20 mt-md80">
                  <img src="assets/images/templates.webp" alt="Templates" class="mx-auto d-block img-fluid">
               </div>
            </div>
         </div>
      </div>
   </section>

   <section class="grey-sec">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-md-6">
               <div class="f-28 f-md-50 w700 lh140 black-clr">
                  No Writing & No Designing 
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  We already did all the heavylifting for you…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  All the copy was done by our AI model. And designed it for you too…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  You just attach your affiliate link…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  And you are done.
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  No more work…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  Maybe just keep refreshing your account…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  To see how much you made…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  Us personall…
               </div>
               <div class="f-20 f-md-22 w600 lh140 red-clr mt20">
                  We make about $300-$500 per each funnel…
               </div>
               <div class="f-20 f-md-22 w600 lh140 black-clr mt20 daily-text">
                  DAILY
               </div>
            </div>
            <div class="col-md-6 mt20 mt-md0">
               <img src="assets/images/no-writing-no-designing.webp" alt="No Writing & No Designing" class="mx-auto d-block img-fluid">
            </div>
            <div class="col-md-12 mt20 mt-md50">
               <img src="assets/images/no-writing.webp" alt="No Writing" class="mx-auto d-block img-fluid">
            </div>
         </div>
      </div>
   </section>

   <section class="white-sec">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-md-6 order-md-2">
               <div class="f-28 f-md-45 w400 lh140 black-clr">
                  Unlike Clickfunnels, <br  class="d-none d-md-block">
                  Ninja AI Comes With <span class="w700"> ZERO Monthly Fees…</span> 
               </div>
               <div class="f-20 f-md-22 w600 lh140 black-clr mt20">
                  Yea, you read that right…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  All you need to do to access the most advanced piece of software on the market…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  Is to pay one, small fee…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  That's it, now you own it forever…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  And can use it as much as you need
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  With zero restrictions…
               </div>
            </div>
            <div class="col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/zero-monthly.webp" alt="Zero Monthly Fee" class="mx-auto d-block img-fluid">
            </div>
         </div>
      </div>
   </section>

   <section class="blue-sec2" id="productdemo">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-28 f-md-50 w700 lh140 white-clr">
                  Watch Ninja AI In Action… And See What It Can Do For You…
               </div>
               <img src="assets/images/floating-arrow.webp" alt="floating Arrow" class="mx-auto d-block img-fluid mt20 mt-md50">
            </div>
            <div class="col-12 col-md-9 mx-auto mt20 mt-md50">
              <img src="assets/images/watch-ninjaai.webp" alt="Watch Ninjaai" class="mx-auto d-block img-fluid "> 
               <div class="video-box">
                  <div style="padding-bottom: 56.25%;position: relative;"><iframe src="https://ninjaai.dotcompal.com/video/embed/n4mhnex94f" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div>
               </div>
            </div>
         </div>
         <div class="row mt20 mt-md80">
            <div class="col-12">
               <div class="f-28 f-md-38 w700 lh140 text-center white-clr">
                  Get <span class="step-shapes"> VoiceFusion Ai </span> For Low One Time Fee… <br class="d-none d-md-block"> 
               </div>
               <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                  <span class="w700">Just Pay Once</span> Instead Of <s class="red-clr">Monthly</s>
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md50">
            <div class="col-md-7">
               <a href="#buynow" class="cta-link-btn">&gt;&gt;&gt; Get Instant Access To VoiceFusion Ai &lt;&lt;&lt;</a>
               <img src="assets/images/compaitable-gurantee1.webp" alt="Compaitable Gurantee" class="mx-auto d-block img-fluid mt20 mt-md30">
            </div>
            <div class="col-md-5 mt20 mt-md0">
               <div class="countdown-container">
                  <div class="f-20 f-md-24 w400 lh140 text-center white-clr mb10">
                     <span class="w700 red-clr">Hurry up!</span>  Price Increases In
                  </div>
                  <div class="countdown counter-white text-center"><div class="timer-label text-center"><span class="f-24 f-md-32 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-14 w500 smmltd">Days</span> </div><div class="timer-label text-center"><span class="f-24 f-md-32 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-14 w500 smmltd">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-24 f-md-32 timerbg oswald">32&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-14 w500 smmltd">Mins</span> </div><div class="timer-label text-center "><span class="f-24 f-md-32 timerbg oswald">41</span><br><span class="f-14 f-md-14 w500 smmltd">Sec</span> </div></div>
               </div>
            </div>
         </div>
      </div>
   </section>

   <section class="experienced-sec">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-28 f-md-50 w700 lh140 black-clr">
                  Ninja AI Is For Everyone! Regardless Of <br class="d-none d-md-block">
                  Your Experience…
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md80">
            <div class="col-md-6 order-md-2">
               <div class="f-20 f-md-22 lh140 black-clr w400">
                  It doesn't matter if you have made money online before or not…
               </div>
               <div class="f-20 f-md-22 lh140 black-clr w400 mt20">
                  It doesn't matter if you know how a funnel is made or not…
               </div>
               <div class="f-20 f-md-22 lh140 black-clr w400 mt20">
                  As long as you can follow fool-proof instructions…
               </div>
               <div class="f-20 f-md-22 lh140 black-clr w700 mt20">
                  And can click 3 clicks with your mouse…
               </div>
               <div class="f-20 f-md-22 lh140 black-clr w400 mt20">
                  You're all set…
               </div>
            </div>
            <div class="col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/experience-women.webp" alt="Experienced Women" class="mx-auto d-block img-fluid">
            </div>
         </div>
      </div>
   </section>

   <section class="grey-sec">
      <div class="container hi-their-box">
         <div class="row">
            <div class="col-12 text-center">
               <div class="hi-their-shadow">
                  <div class="f-28 f-md-45 lh140 black-clr caveat w700 hi-their-head">
                     Hey There,
                  </div>
               </div>
            </div>
         </div>
         <div class="row mt20 mt-md80">
            <div class="col-md-3 text-center">
               <div class="figure mr15 mr-md0">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/pranshu-gupta.webp" alt="Pranshu Gupta" class="mx-auto d-block img-fluid  img-shadow">
                  <div class="figure-caption mt10">
                     <div class="f-24 f-md-28 w700 lh140 black-clr caveat text-center">
                        Pranshu Gupta
                     </div>
                  </div>
               </div>
               <div class="figure mt20 mt-md30">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/bizomart-team.webp" alt="Bizomart" class="mx-auto d-block img-fluid  img-shadow">
                  <div class="figure-caption mt10">
                     <div class="f-24 f-md-28 w700 lh140 black-clr caveat text-center">
                        Bizomart
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-md-9 mt20 mt-md0">
               <div class="f-28 f-md-38 lh140 black-clr w400">
                  It's <span class="w600">Pranshu</span> along with <span class="w600">Bizomart.</span> 
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr mt20 mt-md30">
                  I'm a full time internet marketer…
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr mt20">
                  Over the years, I tried dozens (it could be hundreds even…) of methods to make money online…
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr mt20">
                  But the only thing that gave me the life I always wanted was…
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr mt20">
                  Creating websites…
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr mt20">
                  It's the easiest, and fastest way to build wealth…
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr mt20">
                  Not just make some pocket change…&nbsp;
               </div>
               <div class="f-20 f-md-22 lh140 w600 black-clr mt20">
                  Thanks to that…
               </div>
            </div>
         </div>

         <div class="row mt20 mt-md50">
            <div class="col-12">
               <div class="blue-border"></div>
            </div>
         </div>

         <div class="row mt20 mt-md50">
            <div class="col-12 text-center">
               <div class="f-22 f-md-28 lh140 black-clr w600">
                  I can spend my time as I please
               </div>
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/red-underline.webp" alt="Quotes" class="mx-auto d-block img-fluid  mt10">
            </div>
         </div>

         <div class="row mt20 mt-md50">
            <div class="col-md-3">
               <figure>
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/family.webp" alt="Family" class="mx-auto d-block img-fluid ">
                  <figure-caption>
                     <div class="f-20 f-md-22 w600 lh140 black-clr mt10 text-center">
                        Spend it with my family
                     </div>
                  </figure-caption>
               </figure>
            </div>
            <div class="col-md-3">
               <figure>
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/travel.webp" alt="Travel" class="mx-auto d-block img-fluid ">
                  <figure-caption>
                     <div class="f-20 f-md-22 w600 lh140 black-clr mt10 text-center">
                        Travel
                     </div>
                  </figure-caption>
               </figure>
            </div>
            <div class="col-md-3">
               <figure>
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/sunset-beach.webp" alt="Enjoy a sunset on the beach" class="mx-auto d-block img-fluid ">
                  <figure-caption>
                     <div class="f-20 f-md-22 w600 lh140 black-clr mt10 text-center">
                        Enjoy a sunset on the beach
                     </div>
                  </figure-caption>
               </figure>
            </div>
            <div class="col-md-3">
               <figure>
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/video-games.webp" alt="Or even play video games on my PS5" class="mx-auto d-block img-fluid ">
                  <figure-caption>
                     <div class="f-20 f-md-22 w600 lh140 black-clr mt10 text-center">
                        Or even play video games on my PS5
                     </div>
                  </figure-caption>
               </figure>
            </div>
         </div>
         <div class="row mt20 mt-md80">
            <div class="col-12 text-center">
               <div class="f-20 f-md-24 w600 lh140 black-clr">
                  And the best part is… No matter what I do with my day… I still make a lot of money…
               </div>
               <div class="f-20 f-md-24 w400 lh140 black-clr mt20">
                  And I'm not telling you this to brag… Not at all… I'm telling you this to demonstrate why you should even listen to me…
               </div>
            </div>
         </div>
      </div>
   </section>

   <section class="white-sec">
      <div class="container">
         <div class="row">
         <div class="col-12 text-center">
               <div class="f-28 f-md-50 w700 lh140 black-clr">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/blub.webp" class="img-fluid mr-md5 mb5 mb-md0" alt="blub">
                  I have One Goal This Year…
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/blub.webp" class="img-fluid ml-md5 mb5 mb-md0" alt="blub">
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md80">
            <div class="col-md-6">
               <div class="f-20 f-md-22 w400 lh140 black-clr ">
                  You see…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  Making money online is good any everything…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  But what's 10x better… Is helping someone else make it…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  This is why I decided that in 2023 I will help 100 new people…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  Quit their job, and have the financial freedom they always wanted…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  So far, it's been going GREAT…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  I've managed to help countless people…
               </div>
            </div>
            <div class="col-md-6 mt20 mt-md0">
               <img src="assets/images/goal-man.webp" alt="Goal Man" class="mx-auto d-block img-fluid">
            </div>
         </div>
      </div>
   </section>

   <section class="success-sec">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-28 f-md-50 w700 lh140 black-clr">
                  And I'm Here To Help You My Next <br class="d-none d-md-block">
                  Success Story…
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md80">
            <div class="col-md-6 order-md-2">
               <div class="f-20 f-md-22 w400 lh140 black-clr ">
                  Listen, if you're reading this page right now…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  It means one thing, you're dedicated to making money online…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  And I have some good news for you…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  On this page, I will give you everything you need…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  To get started and join me and thousands of other profitable members…
               </div>
               <div class="f-20 f-md-22 w400 lh140 black-clr mt20">
                  Who changed their lives forever…
               </div>
            </div>
            <div class="col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/success-man.webp" alt="Success Man" class="mx-auto d-block img-fluid">
            </div>
         </div>
      </div>
   </section>

   <section class="cool-sec">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-28 f-md-50 w700 black-clr lh140">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/cool.webp" class="img-fluid mr-md5 mb5 mb-md0" alt="Cool">
                  Let Me Show You Something Cool…
                  <img src="assets/images/orange-underline.webp" alt="Quotes" class="mx-auto d-block img-fluid ">
               </div>
            </div>
             <div class="col-12 col-md-8 mx-auto mt20 mt-md80">
               <img src="assets/images/cool-img.webp" alt="Cool Image" class="mx-auto d-block img-fluid">
            </div> 
            <div class="col-12 col-md-10 mx-auto mt20 mt-md80">
               <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 1px solid #15c6fc;">
                  <source src="https://cdn.oppyotest.com/launches/webgenie/preview/images/cool.mp4" type="video/mp4">
               </video>
            </div>
            <div class="col-12 mt20 mt-md50 text-center">
               <div class="f-22 f-md-28 w600 lh140 black-clr">
                  You see this endless stream of payments?
               </div>
               <div class="f-22 f-md-28 w400 lh140 black-clr mt10">
                  We got all of those doing only one thing…
               </div>
               <div class="smile-text f-22 f-md-28 w600 lh140 white-clr mt20 mt-md50">Using NinjaAI</div>
               <div class="f-22 f-md-28 w400 lh140 black-clr mt20 mt-md49">
                  It allowed me to swipe all the top gurus funnels…
               </div>
               <div class="f-22 f-md-28 w400 lh140 black-clr mt20">
                  And guarantee a profitable funnel, before even creating it…
               </div>
               <div class="f-22 f-md-28 w400 lh140 black-clr mt20">
                  All i do is just turn it on… And enjoy my life…
               </div>
               <div class="f-22 f-md-28 w400 lh140 black-clr mt20">
                  This month alone…
               </div>
               <div class="f-28 f-md-38 w400 lh140 black-clr mt20 mt-md30">
                  We made <span class="red-clr w600 affliliate-underline"> $29,232.33 on one of our affiliate accounts…</span>
               </div>
               <div class="d-flex justify-content-md-end justify-content-center">
                  <img src="assets/images/out-water-arrow.webp" alt="Demo Proof" class="d-block img-fluid  mt20 ">
               </div>
               <img src="assets/images/demo-proof.webp" alt="Demo Proof" class="mx-auto d-block img-fluid  ">
               <div class="f-24 f-md-28 w400 lh140 black-clr mt20 mt-md30">
               And I'm gonna show you exactly how I do it…
               </div>
            </div>
         </div>
      </div>
   </section> -->

      <div class="proudly-section" id="product">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="prdly-pres f-28 f-md-38 w700 lh140 text-center white-clr text-uppercase">
                     Introducing
                  </div>
               </div>
               <div class="col-12 mt-md50 mt20 text-center">
               <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 795 123.19" style="max-height: 120px;">
                    <defs>
                    <style>
                    .cls-1{fill:#fff;}
                    .cls-2,
                    .cls-4{fill:#ff7c24;}
                    .cls-3{fill:#15c5f3;}
                    .cls-4{fill-rule:evenodd;}
                    </style>
                    </defs>
                    <g id="Layer_2" data-name="Layer 2">
                    <g id="Layer_1-2" data-name="Layer 1">
                    <path class="cls-1" d="M151.7,38.52,168.39,91H169l16.73-52.49H202l-23.82,69.09H159.32L135.47,38.52Z"/>
                    <path class="cls-1" d="M227.5,108.62a26.33,26.33,0,0,1-13.58-3.36,22.68,22.68,0,0,1-8.82-9.38,30.07,30.07,0,0,1-3.1-14,30.25,30.25,0,0,1,3.1-14.05,22.68,22.68,0,0,1,8.82-9.38,29.12,29.12,0,0,1,27.16,0,22.68,22.68,0,0,1,8.82,9.38A30.25,30.25,0,0,1,253,81.9a30.07,30.07,0,0,1-3.1,14,22.68,22.68,0,0,1-8.82,9.38A26.33,26.33,0,0,1,227.5,108.62Zm.07-11.13a8.85,8.85,0,0,0,6-2,12.6,12.6,0,0,0,3.63-5.58,27,27,0,0,0,0-16.12,12.67,12.67,0,0,0-3.63-5.6,8.81,8.81,0,0,0-6-2.06,9,9,0,0,0-6.06,2.06,12.4,12.4,0,0,0-3.67,5.6,26.81,26.81,0,0,0,0,16.12,12.33,12.33,0,0,0,3.67,5.58A9.09,9.09,0,0,0,227.57,97.49Z"/>
                    <path class="cls-1" d="M269.6,49.11A7.75,7.75,0,0,1,264.12,47a6.9,6.9,0,0,1,0-10.27,8.09,8.09,0,0,1,10.94,0,6.93,6.93,0,0,1,0,10.28A7.72,7.72,0,0,1,269.6,49.11Zm-7.22,58.5V55.79h14.37v51.82Z"/>
                    <path class="cls-1" d="M311.64,108.62A26.34,26.34,0,0,1,298,105.23a22.59,22.59,0,0,1-8.78-9.43,30.48,30.48,0,0,1-3.05-13.9,30.19,30.19,0,0,1,3.09-14A22.84,22.84,0,0,1,298,58.5a26,26,0,0,1,13.56-3.39,26.84,26.84,0,0,1,11.87,2.47,19.81,19.81,0,0,1,8.07,6.91A20.31,20.31,0,0,1,334.78,75H321.22a10.66,10.66,0,0,0-3-6.26,8.81,8.81,0,0,0-6.4-2.38A9.63,9.63,0,0,0,306,68.12a11.59,11.59,0,0,0-3.87,5.24,22.47,22.47,0,0,0-1.38,8.34,22.91,22.91,0,0,0,1.37,8.43,11.56,11.56,0,0,0,3.86,5.3,10.25,10.25,0,0,0,10.27.81,8.64,8.64,0,0,0,3.27-2.95,11.27,11.27,0,0,0,1.71-4.68h13.56a21.3,21.3,0,0,1-3.22,10.44,19.64,19.64,0,0,1-7.95,7A26.54,26.54,0,0,1,311.64,108.62Z"/>
                    <path class="cls-1" d="M367.64,108.62a27.48,27.48,0,0,1-13.75-3.26A22,22,0,0,1,345,96.12a30.44,30.44,0,0,1-3.1-14.19,30.28,30.28,0,0,1,3.1-14,22.88,22.88,0,0,1,8.76-9.41,25.4,25.4,0,0,1,13.27-3.38,27.48,27.48,0,0,1,9.57,1.64,21.68,21.68,0,0,1,7.76,4.91,22.48,22.48,0,0,1,5.18,8.21,33,33,0,0,1,1.85,11.56v3.94H347.67v-8.9h30.22a11,11,0,0,0-1.35-5.5,9.81,9.81,0,0,0-3.72-3.76,10.92,10.92,0,0,0-5.52-1.37,11.1,11.1,0,0,0-5.79,1.5,10.82,10.82,0,0,0-3.93,4,11.42,11.42,0,0,0-1.45,5.58v8.47a14.33,14.33,0,0,0,1.44,6.64,10.22,10.22,0,0,0,4.06,4.32,12.33,12.33,0,0,0,6.24,1.52,13.61,13.61,0,0,0,4.39-.67,9.14,9.14,0,0,0,3.41-2,8.94,8.94,0,0,0,2.16-3.3l13.29.87a18.14,18.14,0,0,1-4.14,8.35,20.71,20.71,0,0,1-8,5.53A30.14,30.14,0,0,1,367.64,108.62Z"/>
                    <path class="cls-2" d="M401.07,107.61V38.52h45.74v12H415.68V67h28.1v12h-28.1v28.54Z"/>
                    <path class="cls-2" d="M486.35,85.54V55.79h14.37v51.82H486.93V98.19h-.54a15.3,15.3,0,0,1-5.82,7.32,17.14,17.14,0,0,1-9.9,2.77,17.51,17.51,0,0,1-9.15-2.36,16.07,16.07,0,0,1-6.15-6.71,23,23,0,0,1-2.25-10.43v-33h14.37V86.22A10.51,10.51,0,0,0,470,93.47a8.37,8.37,0,0,0,6.51,2.67,10.23,10.23,0,0,0,4.86-1.2A9.39,9.39,0,0,0,485,91.38,11.1,11.1,0,0,0,486.35,85.54Z"/>
                    <path class="cls-2" d="M555.31,70.56l-13.16.81a6.9,6.9,0,0,0-1.45-3.05,7.89,7.89,0,0,0-2.92-2.19,10.24,10.24,0,0,0-4.3-.83,10.64,10.64,0,0,0-5.63,1.4,4.23,4.23,0,0,0-2.3,3.73A4,4,0,0,0,527,73.57a11.72,11.72,0,0,0,5.09,2.05l9.38,1.89q7.56,1.56,11.27,5a11.75,11.75,0,0,1,3.71,9,14.2,14.2,0,0,1-3,8.94,19.55,19.55,0,0,1-8.16,6,31.16,31.16,0,0,1-11.93,2.14q-10.29,0-16.38-4.3a16.67,16.67,0,0,1-7.13-11.73L524,91.85a7.26,7.26,0,0,0,3.11,4.78,13.18,13.18,0,0,0,12.4.16A4.43,4.43,0,0,0,541.88,93a4,4,0,0,0-1.65-3.22,12,12,0,0,0-5-1.94l-9-1.79q-7.59-1.51-11.28-5.26a13,13,0,0,1-3.7-9.55,13.88,13.88,0,0,1,2.72-8.6,17.16,17.16,0,0,1,7.66-5.57,31.41,31.41,0,0,1,11.58-2q9.82,0,15.47,4.15A16.06,16.06,0,0,1,555.31,70.56Z"/>
                    <path class="cls-2" d="M572.78,49.11A7.75,7.75,0,0,1,567.3,47a6.9,6.9,0,0,1,0-10.27,8.09,8.09,0,0,1,10.94,0,6.93,6.93,0,0,1,0,10.28A7.72,7.72,0,0,1,572.78,49.11Zm-7.22,58.5V55.79h14.37v51.82Z"/>
                    <path class="cls-2" d="M614.82,108.62a26.3,26.3,0,0,1-13.58-3.36,22.64,22.64,0,0,1-8.83-9.38,30.19,30.19,0,0,1-3.1-14,30.37,30.37,0,0,1,3.1-14.05,22.64,22.64,0,0,1,8.83-9.38,29.1,29.1,0,0,1,27.15,0,22.64,22.64,0,0,1,8.83,9.38,30.37,30.37,0,0,1,3.1,14.05,30.19,30.19,0,0,1-3.1,14,22.64,22.64,0,0,1-8.83,9.38A26.29,26.29,0,0,1,614.82,108.62Zm.06-11.13a8.82,8.82,0,0,0,6-2,12.52,12.52,0,0,0,3.63-5.58,27,27,0,0,0,0-16.12,12.59,12.59,0,0,0-3.63-5.6,8.78,8.78,0,0,0-6-2.06,9,9,0,0,0-6,2.06,12.41,12.41,0,0,0-3.68,5.6,27,27,0,0,0,0,16.12,12.35,12.35,0,0,0,3.68,5.58A9.09,9.09,0,0,0,614.88,97.49Z"/>
                    <path class="cls-2" d="M664.07,77.65v30H649.7V55.79h13.7v9.14h.6a14.6,14.6,0,0,1,5.77-7.17,17.59,17.59,0,0,1,9.82-2.65A18.26,18.26,0,0,1,689,57.47a16,16,0,0,1,6.24,6.74,22.57,22.57,0,0,1,2.23,10.4v33H683.1V77.18c0-3.18-.79-5.65-2.43-7.44a8.73,8.73,0,0,0-6.78-2.68,10.2,10.2,0,0,0-5.11,1.24,8.56,8.56,0,0,0-3.44,3.63A12.45,12.45,0,0,0,664.07,77.65Z"/>
                    <path class="cls-1" d="M721,107.61H705.29l23.86-69.09H748l23.82,69.09H756.13L738.83,54.3h-.54Zm-1-27.16h37v11.4H720Z"/>
                    <path class="cls-1" d="M787.24,49.11A7.76,7.76,0,0,1,781.75,47a6.92,6.92,0,0,1,0-10.27,8.1,8.1,0,0,1,11,0,6.93,6.93,0,0,1,0,10.28A7.76,7.76,0,0,1,787.24,49.11ZM780,107.61V55.79h14.37v51.82Z"/>
                    <path class="cls-3" d="M.46,79.85a45.62,45.62,0,0,0,91.13,0,2.16,2.16,0,0,0-2.15-2.28H58.74a12.72,12.72,0,0,1-25.43,0H2.61A2.17,2.17,0,0,0,.46,79.85ZM46,96.62A18.94,18.94,0,0,0,59.49,91l15,14.95A40,40,0,0,1,46,117.77Z"/>
                    <path class="cls-3" d="M46,83.77a6.21,6.21,0,0,0,6.2-6.2H39.82A6.21,6.21,0,0,0,46,83.77Z"/>
                    <path class="cls-3" d="M52.9,3v7.55a3,3,0,0,1-3,3H48.32v6.79H43.74V13.57H42.2a3,3,0,0,1-3-3V3a3,3,0,0,1,3-3h7.65A3,3,0,0,1,52.9,3Z"/>
                    <path class="cls-2" d="M96.76,36.77V29.46a2.05,2.05,0,0,1,2-2.06h0a2.06,2.06,0,0,1,2.06,2.06v7.31a2.06,2.06,0,0,1-2.06,2.06h0A2.05,2.05,0,0,1,96.76,36.77Z"/>
                    <path class="cls-2" d="M103.2,36.77V29.46a2.06,2.06,0,0,1,2.06-2.06h0a2.06,2.06,0,0,1,2.06,2.06v7.31a2.06,2.06,0,0,1-2.06,2.06h0A2.06,2.06,0,0,1,103.2,36.77Z"/>
                    <rect class="cls-2" x="109.65" y="27.4" width="4.11" height="11.43" rx="0.91"/>
                    <path class="cls-2" d="M116.1,36.77V29.46a2.05,2.05,0,0,1,2.05-2.06h0a2.06,2.06,0,0,1,2.06,2.06v7.31a2.06,2.06,0,0,1-2.06,2.06h0A2.05,2.05,0,0,1,116.1,36.77Z"/>
                    <path class="cls-2" d="M96.76,50.86V43.55a2.05,2.05,0,0,1,2-2.06h0a2.06,2.06,0,0,1,2.06,2.06v7.31a2.06,2.06,0,0,1-2.06,2.06h0A2.05,2.05,0,0,1,96.76,50.86Z"/>
                    <path class="cls-2" d="M103.2,50.86V43.55a2.06,2.06,0,0,1,2.06-2.06h0a2.06,2.06,0,0,1,2.06,2.06v7.31a2.06,2.06,0,0,1-2.06,2.06h0A2.06,2.06,0,0,1,103.2,50.86Z"/>
                    <path class="cls-2" d="M96.76,65V57.64a2.05,2.05,0,0,1,2-2h0a2.05,2.05,0,0,1,2.06,2V65A2.05,2.05,0,0,1,98.81,67h0A2.05,2.05,0,0,1,96.76,65Z"/>
                    <path class="cls-2" d="M103.2,65V57.64a2.05,2.05,0,0,1,2.06-2h0a2.05,2.05,0,0,1,2.06,2V65A2.05,2.05,0,0,1,105.26,67h0A2.05,2.05,0,0,1,103.2,65Z"/>
                    <rect class="cls-2" x="109.65" y="55.59" width="4.11" height="11.43" rx="0.91"/>
                    <path class="cls-4" d="M83.53,20.36h-75A8.52,8.52,0,0,0,0,28.87v40a2.3,2.3,0,0,0,2.29,2.29H89.76a2.3,2.3,0,0,0,2.29-2.29v-40A8.52,8.52,0,0,0,83.53,20.36ZM28.85,56.08A10.31,10.31,0,1,1,39.16,45.77,10.31,10.31,0,0,1,28.85,56.08Zm34.35,0A10.31,10.31,0,1,1,73.5,45.77,10.31,10.31,0,0,1,63.2,56.08Z"/>
                    </g>
                    </g>
                    </svg>
                  </div>
                  <div class="col-12 mt-md30 mt20 f-md-40 f-24 w400 text-center white-clr lh140">
                  World’s First Super-Trending <span class="orange-clr">"IBM's Watson & ChatGPT4" Powered Ai App</span>  Generates Us
                  <span class="w700">Real Human Emotion Based Voices, Audiobooks, Podcasts, & Even Unique Content</span>  <u> with Just 1 Keyword. </u>
                  </div>
               </div>
               <div class="row d-flex flex-wrap align-items-center mt20 mt-md50">
                  <div class="col-12 col-md-10 mx-auto">
                     <img src="assets/images/productbox.png" class="img-fluid d-block mx-auto margin-bottom-15">
                  </div>
               </div>
            </div>
         </div>


         <div class="audio-book">
		<div class="container">
			<div class="row">
				<div class="col-12 text-center">
					<div class="f-md-38 f-28 w600 mb20 mb-md30">
						WORLD'S First ChatGPT4 Powered (OpenAI) Cloud Based App That Generates Unique High Quality Content & Converts It To Real Human Emotion Based Voices & Even AudioBooks		
					</div>
					<div class="f-md-50 f-35 w700 mb20 mb-md30">
						<span class="red-clr">!!</span> With A Few Clicks <span class="red-clr">!!</span> 
					</div>
					<div class="f-md-22 f-18 w700 red-bursh p10 white-clr mb20 mb-md30">
						3-In-1 ChatGPT4 Powered App | Pay Once | No Hidden Charges | FREE Commercial License 
					</div>
					<div class="mb30 mb-md50">
						<img src="assets/images/arrow-down.png"  alt="arrow">
					</div>					
				</div>
            <div class="col-12 col-md-9 mx-auto">
				<div class="video-box11">
               <div style="padding-bottom: 56.25%;position: relative;">
               <iframe src="https://voicefusionai.oppyo.com/video/embed/15uckptfuq" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
               </div>
            </div>
			</div>	
         <div class="row">
            <div class="col-12 mt20 mt-md30">
               <div class="f-28 f-md-34 w700 lh140 text-center black-clr">
                  Get VoiceFusion Ai For Low One Time Fee… <br class="d-none d-md-block"> 
               </div>
               <div class="f-22 f-md-24 w400 lh140 text-center black-clr mt10">
                  <span class="w700">Just Pay Once</span> Instead Of <s class="red-clr">$97 Monthly</s>
               </div>
            </div>
            <div class="col-12 col-md-9 text-center mx-auto mt20">
               <a href="#buynow" class="cta-link-btn"> Get Instant Access To VoiceFusion Ai </a>
               <div class="f-18 f-md-20 w300 lh140 black-clr text-center mt20">
                  Special Bonus* <span class="w600">"30 Reseller Licenses"</span> If You Buy Today         
               </div>
               <img src="assets/images/compaitable-gurantee1.png" alt="Compaitable Gurantee" class="mx-auto d-block img-fluid mt20">
            </div>
         </div>				
		</div>					
	</div></div>


   <div class="step-section">
      <div class="container ">
         <div class="row ">
            <div class="col-12 f-28 f-md-48 w700 lh140 black-clr text-center">
            Start Your <u>Own Content & Voice Over Agency</u> <br class="d-none d-md-block"> In 3 Simple Steps
            </div>
            <!-- <div class="col-12 f-24 f-md-28 w600 lh140 white-clr text-center mt20">
               <div class="post-heading">
               Start Dominating ANY Niche With DFY AI Funnels…
               </div>
            </div> -->
            <div class="col-12 mt30 mt-md100">
               <div class="row align-items-center ">
                  <div class="col-12 col-md-6">
                     <div class="step-shapes f-20 f-md-22 w600 white-clr">
                        Step 1
                     </div>
                     <div class="f-20 f-md-24 w500 lh140 mt5 mt-md10 text-capitalize">
                        Login To Members Area
                     </div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 ">
               <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 1px solid #15c6fc;"><source src="assets/images/step1-img.mp4" type="video/mp4"></video>
                     <!-- <img src="assets/images/step1-img.png" alt="Video" class="mx-auto d-block img-fluid"> -->
                  </div>
               </div>
            </div>
            <div class="col-12">
               <img src="assets/images/left-arrow.webp" class="img-fluid d-none d-md-block mx-auto " alt="Left Arrow">
            </div>
            <div class="col-12 mt30 mt-md30">
               <div class="row align-items-center ">
                  <div class="col-12 col-md-6 order-md-2">
                     <div class="step-shapes f-20 f-md-22 w600 white-clr">
                        Step 2
                     </div>
                     <div class="f-20 f-md-24 w500 lh140 mt5 mt-md10 text-capitalize">
                     Just Enter A Keyword Or Upload Your Old Voice, Text File etc
                     </div>
                  </div>
                  <div class="col-md-6 col-md-pull-6 mt20 mt-md0 order-md-1 ">
                  <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 1px solid #15c6fc;"><source src="assets/images/step2-img.mp4" type="video/mp4"></video>
                     <!-- <img src="assets/images/step1-img.png" alt="Video" class="mx-auto d-block img-fluid"> -->
                  </div>
               </div>
            </div>
            <div class="col-12">
               <img src="assets/images/right-arrow.webp" class="img-fluid d-none d-md-block mx-auto" alt="Right Arrow">
            </div>
            <div class="col-12 mt30 mt-md30">
               <div class="row align-items-center ">
                  <div class="col-12 col-md-6">
                     <div class="step-shapes f-20 f-md-22 w600 white-clr">
                        Step 3
                     </div>
                     <div class="f-20 f-md-24 w500 lh140 mt5 mt-md10 text-capitalize">
                        GPT4 Technology Will Generate Unique High Quality Content & Emotion Based Real Human Voice Overs..<span class="red-clr"> Also Convert Them To Classy AudioBooks</span>
                     </div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 ">
                  <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 1px solid #15c6fc;"><source src="assets/images/step3-img.mp4" type="video/mp4"></video>
                     <!-- <img src="assets/images/step1-img.png" alt="Video" class="mx-auto d-block img-fluid"> -->
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>

 <!-- CTA Section Start -->
 <div class="cta-section-white">
      <div class="container">
         <div class="row">
            <div class="col-12">
            <div class="f-28 f-md-34 w700 lh140 text-center white-clr">
                  Get VoiceFusion Ai For Low One Time Fee… <br class="d-none d-md-block"> 
               </div>
               <div class="f-22 f-md-24 w400 lh140 text-center white-clr mt10">
                  <span class="w700">Just Pay Once</span> Instead Of <s class="red-clr">$97 Monthly</s>
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md50">
            <div class="col-md-7 text-center">
               <a href="#buynow" class="cta-link-btn"> Get Instant Access To VoiceFusion Ai </a>
               <div class="f-18 f-md-20 w300 lh140 white-clr text-center mt20">
                  Special Bonus* <span class="w600">"30 Reseller Licenses"</span> If You Buy Today         
               </div>
               <img src="assets/images/compaitable-gurantee1.webp" alt="Compaitable Gurantee" class="mx-auto d-block img-fluid mt20 mt-md30">
            </div>
            <div class="col-md-5 mt20 mt-md0">
               <div class="countdown-container">
                  <div class="f-20 f-md-24 w400 lh140 text-center white-clr mb10">
                     <span class="w700 yellow-clr">Hurry up!</span>  Price Increases In
                  </div>
                  <div class="countdown counter-white text-center">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 smmltd">Days</span> 
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-45 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-45 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> 
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- CTA Section End -->

   <!-- Limited Section Start    -->
    <div class="limited-time-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="row align-items-center flex-column flex-md-row justify-content-center">
                     <div class="col-md-1">
                        <img src="assets/images/caution-icon.webp" class="img-fluid mr-md5 mb5 mb-md0" alt="Caution Icon">
                     </div>
                     <div class="col-md-8 col-12 f-20 f-md-22 w300 text-center white-clr">
                        <span class="f-20 f-md-22 w600">FAIR WARNING! After Every Hour The Price Goes Up,</span>
                        <br> So ActNow To Claim Your 81% DISCOUNT Or Come Later & Pay More
                     </div>
                     <div class="col-md-1">
                        <img src="assets/images/caution-icon.webp" class="img-fluid mr-md5 mb5 mb-md0" alt="Caution Icon">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div> 
   <!-- Limited Section End -->



   <div class="testimonial-sec">
      <div class="container ">
         <div class="row ">
            <div class="col-12 f-28 f-md-50 w500 lh140 black-clr text-center">
               People on The Internet are
            </div>
            <div class="col-12 f-22 f-md-50 w700 lh140 red-clr text-center">
               Loving VoiceFusion Ai 
               <img src="assets/images/red-underline-brush.png" class="img-fluid mx-auto d-block" alt="brush">
            </div>
         </div>
         <div class="row row-cols-md-2 row-cols-1 gx4 mt-md100 mt0">
            <div class="col mt20 mt-md50">
               <div class="single-testimonial">
                  <div class="st-img">
                     <img src="assets/images/craig-mcintosh.webp" class="img-fluid d-block mx-auto " alt="Craig McIntosh">
                  </div>
                  <div class="mt20 md-md50 f-24 f-md-28 w600 lh140 black-clr">Craig McIntosh </div>
                  <div class="mt10 f-20 f-md-22 w400 lh140 black-clr">
                     Digital Marketer and Internet Marketer.
                  </div>
                  <div class="stars mt10">
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                  </div>
                  <img src="assets/images/quote.webp" alt="Quotes" class="mx-auto d-block img-fluid mt20 mt-md50 ">
                  <p class="mt20 f-18 f-md-20 lh140 w400 text-center">
                  This software is a life saver, A wide range of voices and languages that blow your mind, I tried some other tools that offer lifetime deal but t hey are very limited for voices/languages and you should pay again to add more features... VoiceFusion Ai is way better and very useful for me.
                  </p>
               </div>
            </div>
            <div class="col mt20 mt-md50">
               <div class="single-testimonial">
                  <div class="st-img">
                     <img src="assets/images/edward-reid.webp" class="img-fluid d-block mx-auto " alt="Edward Reid">
                  </div>
                  <div class="mt20 md-md50 f-24 f-md-28 w600 lh140 black-clr"> James Milo  </div>
                  <div class="mt10 f-20 f-md-22 w400 lh140 black-clr">
                  Founder,  AI Marketing and Sales Software
                  </div>
                  <div class="stars mt10">
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                  </div>
                  <img src="assets/images/quote.webp" alt="Quotes" class="mx-auto d-block img-fluid mt20 mt-md50 ">
                  <p class="mt20 f-18 f-md-20 lh140 w400 text-center">
                     I can say that VoiceFusion Ai has been optimized and developed nicely since their launch, I tried it a days ago and now it become more mature with high quality voices and more features, Great work guys keep it up
                  </p>
               </div>
            </div>
            <div class="col mt-md120 mx-auto">
               <div class="single-testimonial">
                  <div class="st-img">
                     <img src="assets/images/jaeden-downs.webp" class="img-fluid d-block mx-auto " alt="Jaeden Downs">
                  </div>
                  <div class="mt20 md-md50 f-24 f-md-28 w600 lh140 black-clr"> Jaeden Downs  </div>
                  <div class="stars mt10">
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                  </div>
                  <img src="assets/images/quote.webp" alt="Quotes" class="mx-auto d-block img-fluid mt20 mt-md50 ">
                  <p class="mt20 f-18 f-md-20 lh140 w400 text-center">
                     Best Text To Speech ever, literally love this thing. I have dumped my old robotic voice overs subscriptions with this.. High quality audio, never experienced tech like this before. Well done to the VoiceFusion Ai team!
                  </p>
               </div>
            </div>
            <div class="col mt-md120 mx-auto">
               <div class="single-testimonial">
                  <div class="st-img">
                     <img src="assets/images/stuart-johnson.webp" class="img-fluid d-block mx-auto " alt="Drew Warren">
                  </div>
                  <div class="mt20 md-md50 f-24 f-md-28 w600 lh140 black-clr"> Drew Warren  </div>
                  <div class="stars mt10">
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                  </div>
                  <img src="assets/images/quote.webp" alt="Quotes" class="mx-auto d-block img-fluid mt20 mt-md50 ">
                  <p class="mt20 f-18 f-md-20 lh140 w400 text-center">
                     Best real time voice changer out there right now. Extremely realistic voices with an amazing community and incredibly receptive development team. Kudos To Team VoiceFusion Ai.
                  </p>
               </div>
            </div>
         </div>
      </div>
   </div>

   <section class="ground-breaking-features">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-20 f-md-28 w400 lh140 black-clr still-headline">
                     VoiceFusion AI is All You Need to Generate Unique High Quality Content & Emotion Based Human Voice Overs
                  </div>
                  <div class="col-12 f-22 f-md-38 w700 lh140 black-clr text-center">
                     WITH THE POWER OF REAL CHATGPT4 LATEST VERSION
                     <img src="assets/images/red-underline-brush-big.png" class="img-fluid mx-auto d-block" alt="brush">
                  </div>
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md100">
               <div class="col-md-6 relative">
                  <div class="f-24 f-md-30 lh140 black-clr w600">
                      Professional & Fully Cloud-Based Platform
                  </div>
                  <img src="assets/images/arrow-left.webp" class="img-fluid d-none ms-md-auto d-md-block" style="position: absolute; right: -160px; top: 210px;">
               </div>
               <div class="col-md-6 mt20 mt-md0">
                  <img src="assets/images/gb-feat1.webp" alt="Build Huge" class="mx-auto d-block img-fluid">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md100">
               <div class="col-md-6 order-md-2 relative">
                  <div class="f-24 f-md-30 lh140 black-clr w600">
                     Latest ChatGPT4 Version Powered Platform
                  </div>
                  <img src="assets/images/arrow-right.webp" class="img-fluid d-none me-md-auto d-md-block" style="position: absolute; top: 280px; left: -240px;">
               </div>
               <div class="col-md-6 mt20 mt-md0 order-md-1">
                  <img src="assets/images/gb-feat2.webp" alt="Sell" class="mx-auto d-block img-fluid">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md100">
               <div class="col-md-6 relative">
                  <div class="f-24 f-md-30 lh140 black-clr w600">
                     ChatGPT4 Powered Content Generator Feature
                  </div>
                  <img src="assets/images/arrow-left.webp" class="img-fluid d-none ms-md-auto d-md-block" style="position: absolute; right: -160px; top: 190px;">
               </div>
               <div class="col-md-6 mt20 mt-md0">
                  <img src="assets/images/gb-feat3.webp" alt="Build Huge" class="mx-auto d-block img-fluid">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md100">
               <div class="col-md-6 order-md-2 relative">
                  <div class="f-24 f-md-30 lh140 black-clr w600">
                     ChatGPT4 Powered Real Human & Emotion Based Voice Over Feature
                  </div>
                  <img src="assets/images/arrow-right.webp" class="img-fluid d-none me-md-auto d-md-block" style="position: absolute; top: 280px; left: -240px;">
               </div>
               <div class="col-md-6 mt20 mt-md0 order-md-1">
                  <img src="assets/images/gb-feat4.webp" alt="Sell" class="mx-auto d-block img-fluid">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md100">
               <div class="col-md-6 relative">
                  <div class="f-24 f-md-30 lh140 black-clr w600">
                     ChatGPT4 Powered AudioBooks Creator Feature
                  </div>
                  <img src="assets/images/arrow-left.webp" class="img-fluid d-none ms-md-auto d-md-block" style="position: absolute; right: -160px; top: 240px;">
               </div>
               <div class="col-md-6 mt20 mt-md0">
                  <img src="assets/images/gb-feat5.webp" alt="Build Huge" class="mx-auto d-block img-fluid">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md100">
               <div class="col-md-6 order-md-2 relative">
                  <div class="f-24 f-md-30 lh140 black-clr w600">
                     300+ Real Voices To Choose From
                  </div>
                  <img src="assets/images/arrow-right.webp" class="img-fluid d-none me-md-auto d-md-block" style="position: absolute; top: 280px; left: -240px;">
               </div>
               <div class="col-md-6 mt20 mt-md0 order-md-1">
                  <img src="assets/images/gb-feat6.webp" alt="Sell" class="mx-auto d-block img-fluid">
               </div>
            </div>
         </div>  
      </section>
      <section class="ef-sec">
         <div class="container">
            <div class="row align-items-center mt20 mt-md40">
               <div class="col-md-6 relative">
                  <div class="f-24 f-md-30 lh140 black-clr w600">
                     100+ Languages To Choose From
                  </div>
                  <img src="assets/images/arrow-left.webp" class="img-fluid d-none ms-md-auto d-md-block" style="position: absolute; right: -160px; top: 210px;">
               </div>
               <div class="col-md-6 mt20 mt-md0">
                  <img src="assets/images/ef1.webp" alt="Build Huge" class="mx-auto d-block img-fluid">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md100">
               <div class="col-md-6 order-md-2 relative">
                  <div class="f-24 f-md-30 lh140 black-clr w600">
                     Inbuilt DFY 1 Million Articles With Full PLR License
                  </div>
                  <img src="assets/images/arrow-right.webp" class="img-fluid d-none me-md-auto d-md-block" style="position: absolute; top: 250px; left: -180px;">
               </div>
               <div class="col-md-6 mt20 mt-md0 order-md-1">
                  <img src="assets/images/ef2.webp" alt="Sell" class="mx-auto d-block img-fluid">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md100">
               <div class="col-md-6 relative">
                  <div class="f-24 f-md-30 lh140 black-clr w600">
                     Create Unlimited VSL's, Sales Copies, Emails, Ads Copy Etc
                  </div>
                  <img src="assets/images/arrow-left.webp" class="img-fluid d-none ms-md-auto d-md-block" style="position: absolute; right: -180px; top: 220px;">
               </div>
               <div class="col-md-6 mt20 mt-md0">
                  <img src="assets/images/ef3.webp" alt="Build Huge" class="mx-auto d-block img-fluid">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md100">
               <div class="col-md-6 order-md-2 relative">
                  <div class="f-24 f-md-30 lh140 black-clr w600">
                     Switch Your Old Boring Robotic Voices into a Real Human Like Emotion Based Voices
                  </div>
                  <img src="assets/images/arrow-right.webp" class="img-fluid d-none me-md-auto d-md-block" style="position: absolute; top: 280px; left: -240px;">
               </div>
               <div class="col-md-6 mt20 mt-md0 order-md-1">
                  <img src="assets/images/ef4.webp" alt="Sell" class="mx-auto d-block img-fluid">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md100">
               <div class="col-md-6 relative">
                  <div class="f-24 f-md-30 lh140 black-clr w600">
                     Pay Once & Get Profit Forever Without Any Restrictions
                  </div>
                  <img src="assets/images/arrow-left.webp" class="img-fluid d-none ms-md-auto d-md-block" style="position: absolute; right: -160px; top: 210px;">
               </div>
               <div class="col-md-6 mt20 mt-md0">
                  <img src="assets/images/ef5.webp" alt="Build Huge" class="mx-auto d-block img-fluid">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md100">
               <div class="col-md-6 order-md-2 relative">
                  <div class="f-24 f-md-30 lh140 black-clr w600">
                     Inbuilt “Social Sharing” Feature For Getting Limitless Traffic
                  </div>
                  <img src="assets/images/arrow-right.webp" class="img-fluid d-none me-md-auto d-md-block" style="position: absolute; top: 250px; left: -240px;">
               </div>
               <div class="col-md-6 mt20 mt-md0 order-md-1">
                  <img src="assets/images/ef6.webp" alt="Sell" class="mx-auto d-block img-fluid">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md100">
               <div class="col-md-6 relative">
                  <div class="f-24 f-md-30 lh140 black-clr w600">
                     COMMERCIAL LICENSE Included
                  </div>
                  <img src="assets/images/arrow-left.webp" class="img-fluid d-none ms-md-auto d-md-block" style="position: absolute; right: -160px; top: 260px;">
               </div>
               <div class="col-md-6 mt20 mt-md0">
                  <img src="assets/images/ef7.webp" alt="Build Huge" class="mx-auto d-block img-fluid">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md100">
               <div class="col-md-6 order-md-2 relative">
                  <div class="f-24 f-md-30 lh140 black-clr w600">
                  No Limitations - Completely Free
                  </div>
                  <img src="assets/images/arrow-right.webp" class="img-fluid d-none me-md-auto d-md-block" style="position: absolute; top: 250px; left: -240px;">
               </div>
               <div class="col-md-6 mt20 mt-md0 order-md-1">
                  <img src="assets/images/ef8.webp" alt="Sell" class="mx-auto d-block img-fluid">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md100">
               <div class="col-md-6 relative">
                  <div class="f-24 f-md-30 lh140 black-clr w600">
                  No Special Skills or Experience Required
                  </div>
                  <img src="assets/images/arrow-left.webp" class="img-fluid d-none ms-md-auto d-md-block" style="position: absolute; right: -160px; top: 220px;">
               </div>
               <div class="col-md-6 mt20 mt-md0">
                  <img src="assets/images/ef9.webp" alt="Build Huge" class="mx-auto d-block img-fluid">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md100">
               <div class="col-md-6 order-md-2 relative">
                  <div class="f-24 f-md-30 lh140 black-clr w600">
                  Sell Unlimited Content, Voice Overs, AudioBooks & Earn Like The Big Boys
                  </div>
                  <img src="assets/images/arrow-right.webp" class="img-fluid d-none me-md-auto d-md-block" style="position: absolute; top: 240px; left: -240px;">
               </div>
               <div class="col-md-6 mt20 mt-md0 order-md-1">
                  <img src="assets/images/ef10.webp" alt="Sell" class="mx-auto d-block img-fluid">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md100">
               <div class="col-md-6 relative">
                  <div class="f-24 f-md-30 lh140 black-clr w600">
                  Step by Step Training
                  </div>
                  <img src="assets/images/arrow-left.webp" class="img-fluid d-none ms-md-auto d-md-block" style="position: absolute; right: -160px; top: 230px;">
               </div>
               <div class="col-md-6 mt20 mt-md0">
                  <img src="assets/images/ef11.webp" alt="Build Huge" class="mx-auto d-block img-fluid">
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md100">
               <div class="col-md-6 order-md-2 relative">
                  <div class="f-24 f-md-30 lh140 black-clr w600">
                  And much more…
                  </div>
               </div>
               <div class="col-md-6 mt20 mt-md0 order-md-1">
                  <img src="assets/images/ef12.webp" alt="Sell" class="mx-auto d-block img-fluid">
               </div>
            </div>
         </div>  
      </section>

   <!-- <div class="profitwall-sec" id="features">
       <div class="container">
         <div class="row">
            <div class="mt-md30 mt20 f-28 f-md-50 w700 text-center lh140 text-capitalize white-clr">
               <div class="post-heading">Let Me Show You The Power Of Ninja AI</div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md80 white-bg">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-22 f-md-34 w700 lh140 black-clr text-center text-md-start">
               Find The Perfect Funnel In Any Niche
               </div>
               <div class="f-18 f-md-20 w400 lh140 black-clr text-center text-md-start mt10">
               Just enter your niche. It's as easy as selecting from a menu. Or writing it yourself…
               </div>
               <div class="f-18 f-md-20 w400 lh140 black-clr text-center text-md-start mt10">
               Ninja AI covers over 990+ niches
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 1px solid #a94eff;">
                  <source src="assets/images/doitall-img1.mp4" type="video/mp4">
               </video>
            </div>
         </div>

         <div class="row align-items-center mt20 mt-md80 purple-bgg">
            <div class="col-12 col-md-6">
               <div class="f-22 f-md-36 w700 lh140 white-clr text-center text-md-start">
               Improve The Funnel Copywriting For 10x Conversions
               </div>
               <div class="f-18 f-md-20 w400 lh140 white-clr text-center text-md-start mt20">
               We don't want similar results like the guru, we want way better…
               </div>
               <div class="f-18 f-md-20 w400 lh140 white-clr text-center text-md-start mt20">
               That's why we let our AI model, rewrite their copy and improve it drasitcally.
               </div>
               <div class="f-18 f-md-20 w400 lh140 white-clr text-center text-md-start mt20">
               This alone, gave us 10x conversions…
               </div>
               <div class="f-18 f-md-20 w400 lh140 white-clr text-center text-md-start mt20">
               Which means 10x the sales and money.
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
            <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 1px solid #a94eff;">
                  <source src="assets/images/doitall-img2.mp4" type="video/mp4">
               </video>
            </div>
         </div>

         <div class="row align-items-center mt20 mt-md80 white-bg">
            <div class="col-12 col-md-7 order-md-2">
               <div class="f-22 f-md-38 w700 lh140 black-clr text-center text-md-start">
               Redesign The Funnel In HIGHER Quality
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               In order to make a funnel work nowadays…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               It must be unique, and that's why we always redesign every funnel…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               And improve it's quality. That's how we can make sure that we are outperforming all of our competitors
               </div>
            </div>
            <div class="col-12 col-md-5 mt20 mt-md0 order-md-1">
               <img src="assets/images/doitall-img3.png" alt="imagine-img" class="mx-auto d-block img-fluid">
            </div>
         </div>

         <div class="row align-items-center mt20 mt-md80 purple-bgg">
            <div class="col-12 col-md-6">
               <div class="f-22 f-md-36 w700 lh140 white-clr text-center text-md-start">
               Find The Perfect Product To Promote
               </div>
               <div class="f-18 f-md-20 w400 lh140 white-clr text-center text-md-start mt5">
               If you don't have a product already to promote…
               </div>
               <div class="f-18 f-md-20 w400 lh140 white-clr text-center text-md-start mt5">
               Don't worry…
               </div>
               <div class="f-18 f-md-20 w400 lh140 white-clr text-center text-md-start mt5">
               Because Ninja AI will do that for you too…
               </div>
               <div class="f-18 f-md-20 w400 lh140 white-clr text-center text-md-start mt5">
               It will give you a list of the affiliate products that the AI believe will sell
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/doitall-img4.webp" alt="imagine-img" class="mx-auto d-block img-fluid">
            </div>
         </div>

         <div class="row align-items-center mt20 mt-md80 white-bg">
            <div class="col-12 col-md-7 order-md-2">
               <div class="f-22 f-md-38 w700 lh140 black-clr text-center text-md-start">
               Completely Cloud Based Software
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               VoiceFusion Ai is fully cloud-based software that means there is no need of a domain name, hosting or downloading anything. It creates affiliate funnels ensuring they receive maximum viewership.
               </div>
            </div>
            <div class="col-12 col-md-5 mt20 mt-md0 order-md-1">
               <img src="assets/images/doitall-img6.png" alt="imagine-img" class="mx-auto d-block img-fluid">
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md80 purple-bgg">
            <div class="col-12 col-md-7">
               <div class="f-22 f-md-38 w700 lh140 white-clr text-center text-md-start">
               Creates Stunning Affiliate Funnels Within Minutes
               </div>
               <div class="f-18 f-md-22 w400 lh140 white-clr text-center text-md-start mt20">
               We've built VoiceFusion Ai from the ground up to be marketer-friendly, meaning our funnels are tweaked to perfection to help you get traffic and sales.
               </div>
            </div>
            <div class="col-12 col-md-5 mt20 mt-md0">
               <img src="assets/images/doitall-img7.png" alt="imagine-img" class="mx-auto d-block img-fluid">
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md80 white-bg">
            <div class="col-12 col-md-7 order-md-2">
               <div class="f-22 f-md-34 w700 lh140 black-clr text-center text-md-start">
               Inbuilt Text & Inline Editor To Create Hundreds of Unique Campaigns
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Just drag and drop the elements to the place on your page you want them to go. You can add, delete, modify text and images as per your brand identity and need and create stunning, high-converting & unique campaigns within minutes. 
               </div>
            </div>
            <div class="col-12 col-md-5 mt20 mt-md0 order-md-1">
               <img src="assets/images/doitall-img8.png" alt="imagine-img" class="mx-auto d-block img-fluid">
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md80 purple-bgg">
            <div class="col-12 col-md-7">
               <div class="f-22 f-md-38 w700 lh140 white-clr text-center text-md-start">
               Works Seamlessly with All Major Autoresponders
               </div>
               <div class="f-18 f-md-22 w400 lh140 white-clr text-center text-md-start mt20">
               Easily build your targeted email list with Ninja Ai AR integration…
               <br><br>Simply connect your autoresponder, and let Ninja Ai do the rest…
               </div>
            </div>
            <div class="col-12 col-md-5 mt20 mt-md0">
               <img src="https://aicademy.live/special/assets/images/ar-integration.webp" alt="imagine-img" class="mx-auto d-block img-fluid">
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md80 white-bg">
            <div class="col-12 col-md-7 order-md-2">
               <div class="f-22 f-md-38 w700 lh140 black-clr text-center text-md-start">
               Drive Thousands Of FREE Clicks To It
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               With Ninja AI you don't have to worry about traffic…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Or paying for ads in any way shape or form…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               It drives thousands of clicks for 100% free
               </div>
            </div>
            <div class="col-12 col-md-5 mt20 mt-md0 order-md-1">
               <img src="assets/images/doitall-img5.png" alt="imagine-img" class="mx-auto d-block img-fluid">
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md80 purple-bgg">
            <div class="col-12 col-md-7">
               <div class="f-22 f-md-38 w700 lh140 white-clr text-center text-md-start">
               Built-in SEO to Drive Targeted Traffic
               </div>
               <div class="f-18 f-md-22 w400 lh140 white-clr text-center text-md-start mt20">
               VoiceFusion Ai has been designed from the ground up with the best SEO practices in mind. So, our platform is completely SEO compatible to get your offers high rankings in Google and other search engines.
               </div>
            </div>
            <div class="col-12 col-md-5 mt20 mt-md0">
               <img src="assets/images/doitall-img9.png" alt="imagine-img" class="mx-auto d-block img-fluid">
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md80 white-bg">
            <div class="col-12 col-md-7 order-md-2">
               <div class="f-22 f-md-38 w700 lh140 black-clr text-center text-md-start">
               100% Mobile Friendly
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Each funnel you create is 100% mobile optimized
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               There are no glitches, no errors…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               AI will turn it into a fully functional mobile site in SECONDS…
               </div>
            </div>
            <div class="col-12 col-md-5 mt20 mt-md0 order-md-1">
               <img src="assets/images/doitall-img10.png" alt="imagine-img" class="mx-auto d-block img-fluid">
            </div>
         </div>
      </div>
   </div>

   <div class="cta-section-white">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-38 w700 lh140 text-center white-clr">
                  Get <span class="step-shapes"> VoiceFusion Ai </span> For Low One Time Fee… <br class="d-none d-md-block"> 
               </div>
               <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                  <span class="w700">Just Pay Once</span> Instead Of <s class="red-clr">Monthly</s>
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md50">
            <div class="col-md-7">
               <a href="#buynow" class="cta-link-btn">&gt;&gt;&gt; Get Instant Access To VoiceFusion Ai &lt;&lt;&lt;</a>
               <img src="assets/images/compaitable-gurantee1.webp" alt="Compaitable Gurantee" class="mx-auto d-block img-fluid mt20 mt-md30">
            </div>
            <div class="col-md-5 mt20 mt-md0">
               <div class="countdown-container">
                  <div class="f-20 f-md-24 w400 lh140 text-center white-clr mb10">
                     <span class="w700 red-clr">Hurry up!</span>  Price Increases In
                  </div>
                  <div class="countdown counter-white text-center"><div class="timer-label text-center"><span class="f-24 f-md-32 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-14 w500 smmltd">Days</span> </div><div class="timer-label text-center"><span class="f-24 f-md-32 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-14 w500 smmltd">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-24 f-md-32 timerbg oswald">18&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-14 w500 smmltd">Mins</span> </div><div class="timer-label text-center "><span class="f-24 f-md-32 timerbg oswald">38</span><br><span class="f-14 f-md-14 w500 smmltd">Sec</span> </div></div>
               </div>
            </div>
         </div>
      </div>
   </div>

   <div class="limited-time-sec">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-20 f-md-24 w700 text-center white-clr align-items-center flex-column flex-md-row justify-content-center">
                  <img src="assets/images/caution-icon.webp" class="img-fluid mr-md5 mb5 mb-md0" alt="Caution Icon">
                  Your $2,475.34 Bonuses Package Expires When Timer Hits ZERO 
                  <img src="assets/images/caution-icon.webp" class="img-fluid ml-md5 mb5 mb-md0" alt="Caution Icon">
               </div>
            </div>
         </div>
      </div>
   </div> -->
<!-- 
   <div class="areyouready-section">
      <div class="container">
         <div class="row">
            <div class="col-12 f-28 f-md-50 w700 lh140 black-clr text-center">
               Are You Ready For An A.I. Revolution?
            </div>
            <div class="col-12 col-md-7 mx-auto f-28 f-md-38 lh140 w600 text-center white-clr red-head mt20">
            VoiceFusion Ai  And AI Pay Us Daily…
            </div>
            <div class="col-12 f-18 f-md-22 w400 lh140 black-clr text-center mt20 mt-md50">
               There is nothing that AI can't do in the digital world… It's the most powerful technology that exists now… And VoiceFusion Ai is the only app on the market…<br><br>
               <span class="w600 lh120">
               That harnessed that power, and created something that makes us money day after day
            </span>
            </div>
            <img src="assets/images/proof1.webp" alt="Quotes" class="mx-auto d-block img-fluid mt20 mt-md30 ">
            <div class="col-12 f-18 f-md-22 w400 lh140 black-clr text-center mt20 mt-md50">
               Every day we make money without doing any work… And not pocket change…<br><br>
               <span class="w600 lh120">
               Last month alone we made a bit over $17,000 with VoiceFusion Ai 
               </span>
            </div>
            <img src="assets/images/proof2.webp" alt="Quotes" class="mx-auto d-block img-fluid mt20 mt-md30 ">
            <div class="col-12 f-28 f-md-50 w700 lh140 black-clr text-center">
               Wanna know how?
               <img src="assets/images/red-underline.webp" alt="Quotes" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div> -->


   <!-- Imagine Section Start -->
   <!-- <div class="imagine-section">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-28 f-md-50 w400 lh140 black-clr text-center">
               Imagine If You Can Conquer Multi-Billion Industry… <span class="w700"> With ONE App…</span>
               </div>
            </div>
         </div>
         <div class="row align-items-center">
            <div class="col-12 col-md-6  order-md-2">
               <div class="f-22 f-md-50 w600 lh140 black-clr text-center text-md-start">
               No More <strike class="red-clr">“Luck”</strike>
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Luck is not in our dictionary…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               We don't test, and we don't try…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               We turn on Ninja AI and we immediately get what we want…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               We get profitable funnels that we can swipe with a click…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               This funnels is secretly used by all the big gurus you see..
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               And we finally made it available to anyone…
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/imagine-img.png" alt="imagine-img" class="mx-auto d-block img-fluid ">
            </div>
         </div>

         <div class="row mt20 mt-md50">
         <div class="col-12 f-28 f-md-50 w700 lh140 black-clr text-center">
               Wanna know how?
               <img src="assets/images/red-underline.webp" alt="Quotes" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div> -->
   <!-- Imagine Section End -->
<!-- 
   <div class="niche-section">
      <div class="container">
         <div class="row align-items-center mt20 mt-md30">
            <div class="col-12 col-md-6">
               <div class="f-26 f-md-50 w700 lh140 black-clr text-center text-md-start mt20">
                  Works In Any Niche…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
                  Not everyone is in the MMO niche…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
                  And if you are one of those people…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
                  Don't worry, we got you covered…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
                  Because we made sure that ninja AI works in any niche no matter what…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
                  In fact, it works for anyone no matter what…
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/niche-girl.png" alt="Matter" class="mx-auto d-block img-fluid ">
            </div>
         </div>
        
         <div class="regardless-sec">
            <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3">
               <div class="col">
                  <div class="figure">
                     <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/product-creator.webp" alt="Product Creators" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption">
                        <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                        Product Creators
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="figure">
                     <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/affiliate-marketers.webp" alt="Affiliate Marketers" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption">
                        <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                           Affiliate Marketers
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="figure">
                     <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/ecom-store-owners.webp" alt="eCom Store Owners" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption">
                        <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                           eCom Store Owners
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="figure">
                     <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/blog-owners.webp" alt="Blog Owners" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption">
                        <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                           Blog Owners
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="figure">
                     <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/marketers.webp" alt="CPA Marketers" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption">
                        <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                           CPA Marketers
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="figure">
                     <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/video-marketers.webp" alt="Video Marketers" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption">
                        <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                           Video Marketers
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="figure">
                     <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/content-creators.webp" alt="Artists/Content Creators" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption">
                        <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                           Artists/Content Creators
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="figure">
                     <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/personal-brands.webp" alt="Personal Brands" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption">
                        <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                           Personal Brands
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col">
                  <div class="figure">
                     <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/freelancers.webp" alt="Freelancers" class="mx-auto d-block img-fluid ">
                     <div class="figure-caption">
                        <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                           Freelancers
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>

      </div>
   </div>
   
   <div class="justoneclick-section">
      <div class="container">
         <div class="row align-items-center mt20 mt-md50">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-26 f-md-50 w700 lh140 black-clr text-center text-md-start">
               All It Takes Is <br class="d-none d-md-block"><span class="orange-clr">Just One Click...</span>
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               No need for complicated setup…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               All you need is your finger to click a button…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               And immediately you will swipe any profitable funnel you want…
               </div>
               <div class="f-18 f-md-22 w600 lh140 black-clr text-center text-md-start mt20">
               No need for:
               </div>
               <ul class="cross-head pl0 m0 f-18 f-md-22 lh160 w400 black-clr">
                  <li>Hosting</li>
                  <li>Domain</li>
                  <li>Design</li>
                  <li>Copywriting</li>
                  <li>Vsl creation</li>
                  <li>Optimizing</li>
                  <li>Spending on ads</li>
               </ul>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
                  None of that, you just swipe and enjoy… 
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
                  At least that we been doing along all of our beta members…  
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
                  And daily, we make money like this:  
               </div>
               
                <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Within seconds, WebGenie will create you a unique, and stunning website in any niche, in any language.
               </div> 
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/oneclick-img.png" alt="Profitable" class="mx-auto d-block img-fluid">
            </div>
         </div>
      </div>
   </div> -->


   <div class="support-section">
   <div class="container ">
      <div class="row ">
         <div class="col-12 ">
            <div class="row">
               <div class="col-12">
                  <div class="f-md-50 f-28 w700 lh130 red-clr text-center ">
                     That's Not All! 
                     <img src="assets/images/notall-brush.png" alt="Profitable" class="mx-auto d-block img-fluid">
                  </div>
                  <div class="f-md-38 f-24 w700 black-clr text-center mt20 mt-md30">
                     It's Packed With Tons Of Additional<br class="-none d-md-block"> Awesome Features Like…
                  </div>
              </div>
            </div>
            <div class="row g-0 mt20 mt-md40">
               <!-- first -->
               <div class="col-12 col-md-4 ">
                  <div class="boxbg boxborder-rb boxborder top-first ">
                     <img src="assets/images/k1.png" class="img-fluid d-block mx-auto" alt="24x7 High Priority Support">
                     <div class="f-20 f-md-22 w400 lh150 mt15 mt-md40 text-center black-clr ">
                     24x7 High Priority Support
                     </div>
                  </div>
               </div>
               <!-- second -->
               <div class="col-12 col-md-4 ">
                  <div class="boxbg boxborder-rb boxborder ">
                     <img src="assets/images/k2.png" class="img-fluid d-block mx-auto" alt="Skills">
                     <div class="f-20 f-md-22 w400 lh150 mt15 mt-md40 text-center black-clr ">
                           No Need to Write a Single Line<br class="visible-lg "> Of Code
                     </div>
                  </div>
               </div>
               <!-- third -->
               <div class="col-12 col-md-4 ">
                  <div class="boxbg boxborder-b boxborder ">
                     <img src="assets/images/k3.png" class="img-fluid d-block mx-auto" alt="Newbie Friendly">
                     <div class="f-20 f-md-22 w400 lh150 mt15 mt-md40 text-center black-clr ">
                     Newbie Friendly
                     </div>
                  </div>
               </div>
               <!-- fourth -->
               <div class="col-12 col-md-4 ">
                  <div class="boxbg boxborder-r boxborder ">
                     <img src="assets/images/k4.png" class="img-fluid d-block mx-auto" alt="Monthly Updates">
                     <div class="f-20 f-md-22 w400 lh150 mt15 mt-md40 text-center black-clr ">
                     Monthly Updates
                     </div>
                  </div>
               </div>
               <!-- fifth -->
               <div class="col-12 col-md-4 ">
                  <div class="boxbg boxborder-r boxborder ">
                     <img src="assets/images/k5.png" class="img-fluid d-block mx-auto" alt="Step-by-Step Tutorials">
                     <div class="f-20 f-md-22 w400 lh150 mt15 mt-md40 text-center black-clr ">
                     Step-by-Step Tutorials
                     </div>
                  </div>
               </div>
               <!-- sixth -->
               <div class="col-12 col-md-4 ">
                  <div class="boxbg boxborder ">
                     <img src="assets/images/k6.png" class="img-fluid d-block mx-auto" alt="Comes with a Commercial License">
                     <div class="f-20 f-md-22 w400 lh150 mt15 mt-md40 text-center black-clr ">
                     Comes with a Commercial<br class="d-none d-md-block"> License
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- Don't Section Start -->
<div class="dont">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<div class="w700 f-md-38 lh150 line-bursh mb10">
						You Don't Have To Be An <br class="d-none d-md-block">Expert To Use VoiceFusion AI
					</div>
					<ul class="dont-list mt20 mt-md40">
						<li class="f-md-22 w400 mt20 light-black">No Technical Skills Required</li>
						<li class="f-md-22 w400 mt20 light-black">No Marketing or Sales Skills Required</li>
						<li class="f-md-22 w400 mt20 light-black">No Domain or Hosting</li>
						<li class="f-md-22 w400 mt20 light-black">No Website Required</li>
						<li class="f-md-22 w400 mt20 light-black">No Need to be In Front of a Camera</li>
						<li class="f-md-22 w400 mt20 light-black">No Content Creation Required</li>
					</ul>
				</div>
				<div class="col-md-6">
					<img src="assets/images/dont-img.webp" class="img-fluid" alt="">	
				</div>			
			</div>				
		</div>					
	</div>
	<!-- Don't Section End -->
   <div class="grey-section">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="bonus-head f-28 f-md-48 w600 lh140 text-center white-clr">
                  But That's Not All
               </div>
               <div class="f-20 f-md-22 w500 lh140 black-clr mt20">
                  In addition, we have several bonuses for those who want to take action <br class="d-none d-md-block">
                  today and start profiting from this opportunity.
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md80">
            <div class="col-12 col-md-6">
               <div class="">
                  <img src="assets/images/bonus1.webp" alt="Bonus 1" class="img-fluid d-block ">
                  <div class="mt20 f-20 f-md-24 w700 lh140 black-clr">
                  AI Logo Maker App
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 black-clr mt15">
                  Try this Ture AI logo maker. Design and download high-quality free logos in minutes. 20M+ logos made till now.
                  </div>
                  <div class="mt20 f-20 f-md-24 w700 lh140 black-clr mt15">
                  Value - $227
                  </div>
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/bonus.png" alt="Bonus Box" class="mx-auto d-block img-fluid ">
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md80">
            <div class="col-12 col-md-6 order-md-2">
               <div class="">
                  <img src="assets/images/bonus2.webp" alt="Bonus 2" class="img-fluid d-block ">
                  <div class="mt20 f-20 f-md-24 w700 lh140 black-clr">
                  AI Image Generator
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 black-clr mt15">
                  Create beautiful art and images with AI. Our AI image generator brings imagination to life, producing stunning art, illustrations, and images.
                  </div>
                  <div class="mt20 f-20 f-md-24 w700 lh140 black-clr mt15">
                  Value - $667
                  </div>
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/bonus.png" alt="Bonus Box" class="mx-auto d-block img-fluid ">
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md80">
            <div class="col-12 col-md-6">
               <div class="">
                  <img src="assets/images/bonus3.webp" alt="Bonus 3" class="img-fluid d-block ">
                  <div class="mt20 f-20 f-md-24 w700 lh140 black-clr">
                  AI Website Builder
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 black-clr mt15">
                  World's First ChatGPT Powered App Creates Unlimited High Quality SEO Websites & Funnels In Just 30 Seconds + Includes 1,000+ In-built, Ready To Use Templates
                  </div>
                  <div class="mt20 f-20 f-md-24 w700 lh140 black-clr mt15">
                  Value - $567
                  </div>
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/bonus.png" alt="Bonus Box" class="mx-auto d-block img-fluid ">
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md80">
            <div class="col-12 col-md-6 order-md-2">
               <div class="">
                  <img src="assets/images/bonus4.webp" alt="Bonus 4" class="img-fluid d-block ">
                  <div class="mt20 f-20 f-md-24 w700 lh140 black-clr">
                  81% Discount on ALL Upgrades
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 black-clr mt15">
                  Get 81% instant discount on purchase of All Upgrades. This is very exclusive bonus which will be taken down soon.
                  </div>
                  <div class="mt20 f-20 f-md-24 w700 lh140 black-clr mt15">
                  Value - $297
                  </div>
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/bonus.png" alt="Bonus Box" class="mx-auto d-block img-fluid ">
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md80">
            <div class="col-12 col-md-6">
               <div class="">
                  <img src="assets/images/bonus5.webp" alt="Bonus 3" class="img-fluid d-block ">
                  <div class="mt20 f-20 f-md-24 w700 lh140 black-clr">
                  UNLIMITED Commercial License
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 black-clr mt15">
                  You have full rights to use this software.
                  <br><br>
                  You can use it for anyone whether for individuals or for companies. Generate massive free traffic, Sales & Leads to yourself and for others as well.
                  </div>
                  <div class="mt20 f-20 f-md-24 w700 lh140 black-clr mt15">
                  Value - $997
                  </div>
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/bonus.png" alt="Bonus Box" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <!-- <div class="white-section">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-50 w700 lh140 text-center black-clr">
               Eliminate All The Guesswork With Ninja AI
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md50">
            <div class="col-12 col-md-6">
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20 mt-md30">
               Aren't you tired of wasting your money and time……on half-complete apps?
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               If you are…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               Then this is your chance to finally jump on something that has been proven to work…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               Not just for us…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               But for HUNDREDS of beta-testers…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               With %100 success rate…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               We're confident to say that Ninja AI is the BEST app on the market right now…
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/question-girl.png" alt="Future Start" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <div class="grey-section">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-50 w700 lh140 text-center black-clr">
               Wanna Know <span class="orange-clr"> How Much It Costs? </span>
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md80">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr">
               Look, I'm gonna be honest with you…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               We can charge anything we want for Ninja AI
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               Even $997 a month…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               And it will be a no-brainer…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               After all, our beta testers make so much more than that…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               And on top of that, there is no work for you to do.
               </div>
               <div class="f-20 f-md-22 w600 lh140 text-center text-md-start black-clr mt20">
               NinjaAI does it all…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               But I make enough money from using Ninja AI
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               So I don't need to charge you that
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               I use it on a daily basis…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               So I don't see why not I shouldn't give you access to it…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               So, for a limited time only…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               I'm willing to give you full access to Ninja AI
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               For a fraction of the price
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               Less than the price of a cheap dinner
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start black-clr mt20">
               Which is enough for me to cover the cost of the servers running Ninja AI
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/cost.png" alt="Costs" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>

   <div class="quick-section">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-6 ">
               <div class="f-28 f-md-50 w700 lh140 text-center text-md-start white-clr">
               You Have To Be Quick Tho…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start white-clr mt20 mt-md30">
               The last thing on earth I want is to get Ninja AI saturated…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start white-clr mt20">
               So, sadly I'll have to limit the number of licenses I can give…
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start white-clr mt20">
               After that, I'm raising the price to <span class="w600 orange-clr">$97 monthly.</span> 
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start white-clr mt20">
               And trust me, that day will come very…VERY SOON.
               </div>
               <div class="f-20 f-md-22 w400 lh140 text-center text-md-start white-clr mt20">
               So you better act quickly and grab your copy while you can.
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/quick-img.png" alt="Money Scale" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div> 
   <div class="cta-section-white">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="f-28 f-md-38 w700 lh140 text-center white-clr">
                  Get <span class="step-shapes"> VoiceFusion Ai </span> For Low One Time Fee… <br class="d-none d-md-block"> 
               </div>
               <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                  <span class="w700">Just Pay Once</span> Instead Of <s class="red-clr">Monthly</s>
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md50">
            <div class="col-md-7">
               <a href="#buynow" class="cta-link-btn">&gt;&gt;&gt; Get Instant Access To VoiceFusion Ai &lt;&lt;&lt;</a>
               <img src="assets/images/compaitable-gurantee1.webp" alt="Compaitable Gurantee" class="mx-auto d-block img-fluid mt20 mt-md30">
            </div>
            <div class="col-md-5 mt20 mt-md0">
               <div class="countdown-container">
                  <div class="f-20 f-md-24 w400 lh140 text-center white-clr mb10">
                     <span class="w700 red-clr">Hurry up!</span>  Price Increases In
                  </div>
                  <div class="countdown counter-white text-center"><div class="timer-label text-center"><span class="f-24 f-md-32 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-14 w500 smmltd">Days</span> </div><div class="timer-label text-center"><span class="f-24 f-md-32 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-14 w500 smmltd">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-24 f-md-32 timerbg oswald">34&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-14 w500 smmltd">Mins</span> </div><div class="timer-label text-center "><span class="f-24 f-md-32 timerbg oswald">41</span><br><span class="f-14 f-md-14 w500 smmltd">Sec</span> </div></div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="limited-time-sec">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-20 f-md-24 w700 text-center white-clr align-items-center flex-column flex-md-row justify-content-center">
                  <img src="assets/images/caution-icon.webp" class="img-fluid mr-md5 mb5 mb-md0" alt="Caution Icon">
                  Your $2,475.34 Bonuses Package Expires When Timer Hits ZERO 
                  <img src="assets/images/caution-icon.webp" class="img-fluid ml-md5 mb5 mb-md0" alt="Caution Icon">
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="butif-section">
      <div class="container">
         <div class="row align-items-center">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-26 f-md-50 w700 lh140 black-clr text-center text-md-start">
               Can't Decide?
               </div>
               <div class="f-18 f-md-22 w600 lh140 black-clr text-center text-md-start mt20">
               Listen…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               It's really simple…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               If you're sick of all the BS that is going on the market right now…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               If you're worried about inflation and the prices that are going up like there is no tomorrow
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Ninja AI IS for you <span class="w600"> PERIOD </span>
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               It was designed to be the life-changing system that you've been looking for…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Right now, you have the option to change your life
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               With just a few clicks
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               We created this for people exactly like you. To help them finally break through.
               </div>
               <div class="f-18 f-md-22 w600 lh140 red-clr text-center text-md-start mt20">
               Worried what will happen if it didn't work for you?
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/cantdecide-img.png" alt="Profitable" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>
   <div class="imagine-section">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-20 f-md-38 w400 lh140 black-clr text-center">
               We Will Pay You To Fail With Ninja AI
               </div>
               <div class="f-28 f-md-45 w700 lh140 black-clr text-center">
               Our 30 Days Iron Clad  Money Back Guarantee
               </div>
            </div>
         </div>
         <div class="row align-items-center mt30 mt-md50">
            <div class="col-12 col-md-6">
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start">
               We trust our app blindly…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               We know it works, after all we been using it for a year…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               And not just us…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               But hey, I know you probably don't know me… and you may be hesitant…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               And i understand that. A bit of skepticism is always healthy…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               But I can help…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Here is the deal, get access to Ninja AI now…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Use it, and enjoy its features to the fullest…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               And if for any reason you don't think Ninja AI is worth its weight in gold…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Just send us a message to our 24/7 customer support…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               And we will refund every single penny back to you…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               No questions asked… Not just that…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               We will send you a bundle of premium software as a gift for wasting your time.
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
              Worst case scenario, you get Aicademy and don't make any money
               </div>
               <div class="f-18 f-md-22 w600 lh140 red-clr text-center text-md-start mt20">
               You will still get extra bundle of premium software for trying it out.
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/money-back-guarantee.png" alt="imagine-img" class="mx-auto d-block img-fluid ">
            </div>
         </div>

          <div class="row mt20 mt-md50">
         <div class="col-12 f-28 f-md-50 w700 lh140 black-clr text-center">
               Wanna know how?
               <img src="assets/images/red-underline.webp" alt="Quotes" class="mx-auto d-block img-fluid ">
            </div>
         </div> 
      </div>
   </div> -->


   <div class="cta-section-white">
      <div class="container">
         <div class="row">
            <div class="col-12">
            <div class="f-28 f-md-34 w700 lh140 text-center white-clr">
                  Get VoiceFusion Ai For Low One Time Fee… <br class="d-none d-md-block"> 
               </div>
               <div class="f-22 f-md-24 w400 lh140 text-center white-clr mt10">
                  <span class="w700">Just Pay Once</span> Instead Of <s class="red-clr">$97 Monthly</s>
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md50">
            <div class="col-md-7">
               <a href="#buynow" class="cta-link-btn">&gt;&gt;&gt; Get Instant Access To VoiceFusion Ai &lt;&lt;&lt;</a>
               <img src="assets/images/compaitable-gurantee1.webp" alt="Compaitable Gurantee" class="mx-auto d-block img-fluid mt20 mt-md30">
            </div>
            <div class="col-md-5 mt20 mt-md0">
               <div class="countdown-container">
                  <div class="f-20 f-md-24 w400 lh140 text-center white-clr mb10">
                     <span class="w700 red-clr">Hurry up!</span>  Price Increases In
                  </div>
                  <div class="countdown counter-white text-center"><div class="timer-label text-center"><span class="f-24 f-md-32 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-14 w500 smmltd">Days</span> </div><div class="timer-label text-center"><span class="f-24 f-md-32 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-14 w500 smmltd">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-24 f-md-32 timerbg oswald">34&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-14 w500 smmltd">Mins</span> </div><div class="timer-label text-center "><span class="f-24 f-md-32 timerbg oswald">41</span><br><span class="f-14 f-md-14 w500 smmltd">Sec</span> </div></div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="limited-time-sec">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-20 f-md-24 w700 text-center white-clr align-items-center flex-column flex-md-row justify-content-center">
                  <img src="assets/images/caution-icon.webp" class="img-fluid mr-md5 mb5 mb-md0" alt="Caution Icon">
                  Your $2,475.34 Bonuses Package Expires When Timer Hits ZERO 
                  <img src="assets/images/caution-icon.webp" class="img-fluid ml-md5 mb5 mb-md0" alt="Caution Icon">
               </div>
            </div>
         </div>
      </div>
   </div>



   <div class="white-section" id="buynow">
      <div class="container">
         <div class="row">
            <div class="col-12 col-md-9 mx-auto text-center">
               <div class="table-wrap">
                  <div class="table-head">
                     <div class="f-28 f-md-45 lh140 white-clr w700">
                        Today with VoiceFusion Ai, You're Getting
                     </div>
                  </div>
                  <div class=" ">
                     <ul class="table-list pl0 f-18 f-md-20 w400 text-center lh140 black-clr mt20 mt-md40">
                        <li>A True Ai App That Leverages Super Trending IBM's Watson & ChatGPT4 Technology - <span class="w600">That’s PRICELESS</span> </li>
                        <li>IBM's Watson & ChatGPT 4 Powered Real Human Emotion Based Voice Creator - <span class="w600">Worth $997</span> </li>
                        <li>Let Ai Create 100s of AudioBooks & Podcasts for Yourself & Clients - <span class="w600">Worth $997</span> </li>
                        <li>Even Ai Generates High Quality Unique Content for You/Clients - <span class="w600">That’s Worth $997</span> </li>
                        <li>VoiceFusion Ai Built-In Traffic: Instant High Converting Traffic For 100% Free - <span class="w600"> Worth $997 </span></li>
                        <li>300+ Real Voices with Human Emotions to Choose - <span class="w600">Worth $497</span> </li>
                        <li>100+ Languages to Choose From - <span class="w600">Worth $297</span> </li>
                        <li>Inbuilt DFY 1 million Articles with Full PLR License - <span class="w600">Worth $267</span> </li>
                        <li>Step-By-Step Training Videos - <span class="w600">Worth $297</span> </li>
                        <li>Round-The-Clock World-Class Support <span class="w600">(Priceless – It's Worth A LOT)</span> </li>
                        <li>Create Unlimited VSL’s, Sales Copies, Emails, Ads Copy Etc.</li>
                        <li>Pay Once & Get Profit Forever Without Any Restrictions - <span class="w600">Beyond A Value</span></li>
                        <li>100% Newbie Friendly - <span class="w600">Beyond A Value</span></li>
                        <li>Step-By-Step Training Videos - <span class="w600">Worth $297</span></li>
                        <li>Round-The-Clock World-Class Support <span class="w600">(Priceless – It’s Worth A LOT)</span></li>
                        <li>Commercial License to Sell Unlimited Content, Voice Overs, AudioBooks & Earn Like the Big Boys - <span class="w600">Priceless</span></li>
                        <li class="headline my15">BONUSES WORTH $2,475.34 FREE!!! </li>
                        <li>AI Logo Maker App</li>
                        <li>AI Image Generator</li>
                        <li>AI Website Builder</li>
                        <li>81% Discount on ALL Upgrades</li>
                        <li>UNLIMITED Commercial License</li>
                     </ul>
                  </div>
                  <div class="table-btn ">
                     <div class="f-22 f-md-28 w400 lh140 black-clr">
                        Total Value of Everything
                     </div>
                     <div class="f-26 f-md-36 w700 lh140 black-clr mt12">
                        YOU GET TODAY:
                     </div>
                     <div class="f-28 f-md-70 w700 red-clr mt12 l140">
                        $5,346.00
                     </div>
                     <div class="f-22 f-md-28 w400 lh140 black-clr mt20">
                        For Limited Time Only, Grab It For:
                     </div>
                     <div class="f-21 f-md-40 w700 lh140 red-clr mt20">
                       <del>$97 Monthly</del> 
                     </div>
                     <div class="f-24 f-md-34 w700 lh140 black-clr mt20">
                        Today, <span class="blue-clr">Just $17 One-Time</span> 
                     </div>
                     
                     <div class="col-12 mx-auto text-center">
                     <a href="https://warriorplus.com/o2/buy/qyptv4/xw7477/tbq1nm" id="buyButton" class="cta-link-btn mt20">&gt;&gt;&gt; Get Instant Access To VoiceFusion Ai &lt;&lt;&lt;</a>
                           </div>
                     <a href="https://warriorplus.com/o2/buy/qyptv4/xw7477/tbq1nm"><img src="https://warriorplus.com/o2/btn/fn200011000/qyptv4/xw7477/360705" class="d-block img-fluid mx-auto mt20 mt-md30"></a> 
                  </div>
                  <div class="table-border-content">
                     <div class="tb-check d-flex align-items-center f-18">
                        <label class="checkbox inline">
                           <img src="https://assets.clickfunnels.com/templates/listhacking-sales/images/arrow-flash-small.gif" alt="">
                           <input type="checkbox" id="check" onclick="myFunction()">
                        </label>
                        <label class="checkbox inline">
                           <h5>Yes, Add Ninja Ai Funnel + Unlock Our $538/Day Secret AI Traffic System</h5>
                        </label>
                     </div>
                     <div class="p20">
                        <p class="f-18 text-center text-md-start"><b class="red-clr underline">LIMITED TIME OFFER :</b> Activate Ai Traffic Source To Your Order To Make Additional $538/Day with FREE Traffic. And Also Get 5 DFY Profitable High Ticket Funnels + Put Your Funnel Site On A Faster Server for More Conversions, Sales & Commissions. <br><b>(97% Of Customers Pick This Up And Get Immediate Results With It)</b></p>
                        <p class="f-18 text-center text-md-start">Just <span class="green-clr w600">  $9.97 One Time Only</span> </p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- <div class="imagine-section">
      <div class="container">
         <div class="row align-items-center mt30 mt-md50">
            <div class="col-12 col-md-6 order-md-2">
               <div class="f-28 f-md-50 w700 lh140 black-clr text-center text-md-start">
               Remember…
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               You don't have a lot of time. We will remove this huge discount once we hit our limit.
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               After that, you will have to <span class="w600"> pay $97/mo for it.</span>
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               While it will still be worth it.
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Why pay more, when you can just pay a small one-time fee today and get access to everything?
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
               <img src="assets/images/remember-img.png" alt="imagine-img" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div>
   <div class="white-section">
      <div class="container">
         <div class="row align-items-center mt30 mt-md50">
            <div class="col-12 col-md-6">
               <div class="f-28 f-md-45 w700 lh140 black-clr text-center text-md-start">
               So, Are you ready yet?
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               You reading this far into the page means one thing.
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               You're interested.
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               The good news is, you're just minutes away from your breakthrough
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Just imagine waking up every day to thousands of dollars in your account Instead of ZERO.
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               The bad news is, it will sell out FAST
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               So you need to act now.
               </div>
               <div class="f-18 f-md-22 w400 lh140 black-clr text-center text-md-start mt20">
               Ready to join us?
               </div>
            </div>
            <div class="col-12 col-md-6 mt20 mt-md0">
               <img src="assets/images/are-you-ready-img.png" alt="imagine-img" class="mx-auto d-block img-fluid ">
            </div>
         </div>
      </div>
   </div> -->


   <div class="cta-section-white">
      <div class="container">
         <div class="row">
            <div class="col-12">
            <div class="f-28 f-md-34 w700 lh140 text-center white-clr">
                  Get VoiceFusion Ai For Low One Time Fee… <br class="d-none d-md-block"> 
               </div>
               <div class="f-22 f-md-24 w400 lh140 text-center white-clr mt10">
                  <span class="w700">Just Pay Once</span> Instead Of <s class="red-clr">$97 Monthly</s>
               </div>
            </div>
         </div>
         <div class="row align-items-center mt20 mt-md50">
            <div class="col-md-7">
               <a href="#buynow" class="cta-link-btn">&gt;&gt;&gt; Get Instant Access To VoiceFusion Ai &lt;&lt;&lt;</a>
               <img src="assets/images/compaitable-gurantee1.webp" alt="Compaitable Gurantee" class="mx-auto d-block img-fluid mt20 mt-md30">
            </div>
            <div class="col-md-5 mt20 mt-md0">
               <div class="countdown-container">
                  <div class="f-20 f-md-24 w400 lh140 text-center white-clr mb10">
                     <span class="w700 red-clr">Hurry up!</span>  Price Increases In
                  </div>
                  <div class="countdown counter-white text-center"><div class="timer-label text-center"><span class="f-24 f-md-32 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-14 w500 smmltd">Days</span> </div><div class="timer-label text-center"><span class="f-24 f-md-32 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-14 w500 smmltd">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-24 f-md-32 timerbg oswald">20&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-14 w500 smmltd">Mins</span> </div><div class="timer-label text-center "><span class="f-24 f-md-32 timerbg oswald">14</span><br><span class="f-14 f-md-14 w500 smmltd">Sec</span> </div></div>
               </div>
            </div>
         </div>
      </div>
   </div>

   <div class="limited-time-sec">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-20 f-md-24 w700 text-center white-clr align-items-center flex-column flex-md-row justify-content-center">
                  <img src="assets/images/caution-icon.webp" class="img-fluid mr-md5 mb5 mb-md0" alt="Caution Icon">
                  Your $2,475.34 Bonuses Package Expires When Timer Hits ZERO 
                  <img src="assets/images/caution-icon.webp" class="img-fluid ml-md5 mb5 mb-md0" alt="Caution Icon">
               </div>
            </div>
         </div>
      </div>
   </div>
  
 

   <div class="inside-section">
      <div class="container">
         <div class="row">
            <div class="col-12 col-md-12">
               <div class="f-28 f-md-45 lh140 w700 text-center black-clr">
                  We'll see you inside,
               </div>
               <img src="assets/images/orange-line.webp" alt="Orange Line" class="mx-auto d-block img-fluid mt5 ">
            </div>
         </div>
         <div class="row mt20 mt-md70">
            <div class="col-12 col-md-8 mx-auto">
               <div class="row justify-content-between">
                  <div class="col-md-5">
                     <div class="inside-box">
                        <img src="assets/images/pranshu-gupta.webp" alt="Pranshu Gupta" class="mx-auto d-block mx-auto ">
                        <div class="f-22 f-md-28 lh140 w600 text-center black-clr mt20">
                           Pranshu Gupta
                        </div>
                     </div>
                  </div>
                  <div class="col-md-5 mt20 mt-md0">
                     <div class="inside-box">
                        <img src="assets/images/bizomart-team.webp" alt="Bizomart" class="mx-auto d-block mx-auto ">
                        <div class="f-22 f-md-28 lh140 w600 text-center black-clr mt20">
                           Bizomart
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="row mt20 mt-md70">
            <div class="col-12">
               <div class="f-20 f-md-22 lh140 w400 black-clr text-center text-md-start">
                  <span class="w600">PS:</span> If you act now, you will instantly receive [bonuses] worth over  <span class="w600">$2,475.34</span> This bonus is designed specifically to help you get 10x the results, in half the time required
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr text-center text-md-start mt20">
                 <span class="w600">PPS:</span>There is nothing else required for you to start earning with Ninja AINo hidden fees, no monthly costs, nothing.
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr text-center text-md-start mt20">
                 <span class="w600">PPPS:</span> Remember, you're protected by our money-back guarantee If you fail, Not only will we refund your money.
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr text-center text-md-start mt20">
               We will send you <span class="red-clr">$300 out of our own pockets just for wasting your time</span> 
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr text-center text-md-start mt20">
                 <span class="w600">PPPPS:</span>   If you put off the decision till later, you will end up paying $997/mo Instead of just a one-time flat fee.
               </div>
               <div class="f-20 f-md-22 lh140 w400 black-clr text-center text-md-start mt20">
               And why pay more, when you can pay less?
               </div>
            </div>
         </div>
      </div>
   </div>

   <div class="faq-section ">
      <div class="container ">
         <div class="row ">
            <div class="col-12 f-28 f-md-45 lh140 w600 text-center black-clr ">
               Frequently Asked <span class="orange-clr ">Questions</span>
            </div>
            <div class="col-12 mt20 mt-md30 ">
               <div class="row ">
                  <div class="col-md-9 mx-auto col-12 ">
                     <div class="faq-list ">
                        <div class="f-md-22 f-20 w600 black-clr lh140 ">
                           Do I need any experience to get started? 
                        </div>
                        <div class="f-18 w400 lh140 mt10 text-start ">
                           None, all you need is just an internet connection. And you're good to go
                        </div>
                     </div>
                     <div class="faq-list mt20">
                        <div class="f-md-22 f-20 w600 black-clr lh140 ">
                           Is there any monthly cost?
                        </div>
                        <div class="f-18 w400 lh140 mt10 text-start ">
                           Depends, If you act now, NONE. 
                           But if you wait, you might end up paying $997/month
                           <br><br>
                           It's up to you. 
                        </div>
                     </div>
                     <div class="faq-list mt20">
                        <div class="f-md-22 f-20 w600 black-clr lh140 ">
                           How long does it take to make money?
                        </div>
                        <div class="f-18 w400 lh140 mt10 text-start ">
                           Our average member made their first sale the same day they got access to VoiceFusion Ai.
                        </div>
                     </div>
                     <div class="faq-list mt20">
                        <div class="f-md-22 f-20 w600 black-clr lh140 ">
                           Do I need to purchase anything else for it to work?
                        </div>
                        <div class="f-18 w400 lh140 mt10 text-start ">
                           Nop, VoiceFusion Ai is the complete thing. 
                           <br><br>
                           You get everything you need to make it work. Nothing is left behind. 
                        </div>
                     </div>
                     <div class="faq-list mt20">
                        <div class="f-md-22 f-20 w600 black-clr lh140 ">
                           What if I failed?
                        </div>
                        <div class="f-18 w400 lh140 mt10 text-start ">
                           While that is unlikely, we removed all the risk for you.
                           <br><br>
                           If you tried VoiceFusion Ai and failed, we will refund you every cent you paid
                           <br><br>
                           And send you $300 on top of that just to apologize for wasting your time.
                        </div>
                     </div>
                     <div class="faq-list mt20">
                        <div class="f-md-22 f-20 w600 black-clr lh140 ">
                           How can I get started?
                        </div>
                        <div class="f-18 w400 lh140 mt10 text-start ">
                           Awesome, I like your excitement, All you have to do is click any of the buy buttons on the page, and secure your copy of VoiceFusion Ai at a one-time fee
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-12 mt20 mt-md50 ">
               <div class="f-22 f-md-28 w600 black-clr px0 text-center lh140 ">If you have any query, simply reach to us at <a href="mailto:support@bizomart.com " class="kapblue ">support@bizomart.com</a>
               </div>
            </div>
         </div>
      </div>
   </div>


   <!-- CTA Section Start -->
   <div class="cta-section-white">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-45 f-md-45 lh140 w700 white-clr final-shape">
                  FINAL CALL
               </div>
               <div class="f-22 f-md-36 lh140 w600 white-clr mt20 mt-md30">
                  You Still Have the Opportunity To Raise Your Game… <br> Remember, It's Your Make-or-Break Time!
               </div>
               <div class="f-28 f-md-38 w700 lh140 text-center white-clr mt20 mt-md30">
                  Get VoiceFusion Ai And Save $293 Now <br class="d-none d-md-block"> 
               </div>
               <div class="f-22 f-md-28 w400 lh140 text-center white-clr mt10">
                  <span class="w700">Just Pay Once</span> Instead Of a <s>Monthly Fee</s>
               </div>
               <div class="row">
                  <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center">
                     <a href="#buynow" class="cta-link-btn">>>> Get Instant Access To VoiceFusion Ai <<<</a>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30">
                  <img src="assets/images/compaitable-gurantee1.webp" alt="Compaitable Gurantee" class="mx-auto d-block img-fluid">
               </div>
               <!--<div class="col-12 mt20 mt-md30">-->
               <!--    <a href="https://warriorplus.com/o2/buy/hb06pf/wmp7ql/pzsb6y"><img src="https://warriorplus.com/o2/btn/fn200011000/hb06pf/wmp7ql/352565" class="d-block img-fluid mx-auto mt20 mt-md30"></a>-->
               <!--</div>-->
            </div>
         </div>
      </div>
   </div>
   <!-- CTA Section End -->

   <!--Limited Section Start -->
   <div class="limited-time-sec">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
               <div class="f-20 f-md-24 w700 text-center white-clr align-items-center flex-column flex-md-row justify-content-center">
                  <img src="assets/images/caution-icon.webp" class="img-fluid mr-md5 mb5 mb-md0" alt="Caution Icon">
                  Your $2,475.34 Bonuses Package Expires When Timer Hits ZERO 
                  <img src="assets/images/caution-icon.webp" class="img-fluid ml-md5 mb5 mb-md0" alt="Caution Icon">
               </div>
            </div>
         </div>
      </div>
   </div>
   <!--Limited Section End -->

      
      <div class="footer-section">
         <div class="container ">
            <div class="row">
               <div class="col-12 text-center">
               <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 795 123.19" style="max-height: 70px;">
                    <defs>
                    <style>
                    .cls-1{fill:#fff;}
                    .cls-2,
                    .cls-4{fill:#ff7c24;}
                    .cls-3{fill:#15c5f3;}
                    .cls-4{fill-rule:evenodd;}
                    </style>
                    </defs>
                    <g id="Layer_2" data-name="Layer 2">
                    <g id="Layer_1-2" data-name="Layer 1">
                    <path class="cls-1" d="M151.7,38.52,168.39,91H169l16.73-52.49H202l-23.82,69.09H159.32L135.47,38.52Z"/>
                    <path class="cls-1" d="M227.5,108.62a26.33,26.33,0,0,1-13.58-3.36,22.68,22.68,0,0,1-8.82-9.38,30.07,30.07,0,0,1-3.1-14,30.25,30.25,0,0,1,3.1-14.05,22.68,22.68,0,0,1,8.82-9.38,29.12,29.12,0,0,1,27.16,0,22.68,22.68,0,0,1,8.82,9.38A30.25,30.25,0,0,1,253,81.9a30.07,30.07,0,0,1-3.1,14,22.68,22.68,0,0,1-8.82,9.38A26.33,26.33,0,0,1,227.5,108.62Zm.07-11.13a8.85,8.85,0,0,0,6-2,12.6,12.6,0,0,0,3.63-5.58,27,27,0,0,0,0-16.12,12.67,12.67,0,0,0-3.63-5.6,8.81,8.81,0,0,0-6-2.06,9,9,0,0,0-6.06,2.06,12.4,12.4,0,0,0-3.67,5.6,26.81,26.81,0,0,0,0,16.12,12.33,12.33,0,0,0,3.67,5.58A9.09,9.09,0,0,0,227.57,97.49Z"/>
                    <path class="cls-1" d="M269.6,49.11A7.75,7.75,0,0,1,264.12,47a6.9,6.9,0,0,1,0-10.27,8.09,8.09,0,0,1,10.94,0,6.93,6.93,0,0,1,0,10.28A7.72,7.72,0,0,1,269.6,49.11Zm-7.22,58.5V55.79h14.37v51.82Z"/>
                    <path class="cls-1" d="M311.64,108.62A26.34,26.34,0,0,1,298,105.23a22.59,22.59,0,0,1-8.78-9.43,30.48,30.48,0,0,1-3.05-13.9,30.19,30.19,0,0,1,3.09-14A22.84,22.84,0,0,1,298,58.5a26,26,0,0,1,13.56-3.39,26.84,26.84,0,0,1,11.87,2.47,19.81,19.81,0,0,1,8.07,6.91A20.31,20.31,0,0,1,334.78,75H321.22a10.66,10.66,0,0,0-3-6.26,8.81,8.81,0,0,0-6.4-2.38A9.63,9.63,0,0,0,306,68.12a11.59,11.59,0,0,0-3.87,5.24,22.47,22.47,0,0,0-1.38,8.34,22.91,22.91,0,0,0,1.37,8.43,11.56,11.56,0,0,0,3.86,5.3,10.25,10.25,0,0,0,10.27.81,8.64,8.64,0,0,0,3.27-2.95,11.27,11.27,0,0,0,1.71-4.68h13.56a21.3,21.3,0,0,1-3.22,10.44,19.64,19.64,0,0,1-7.95,7A26.54,26.54,0,0,1,311.64,108.62Z"/>
                    <path class="cls-1" d="M367.64,108.62a27.48,27.48,0,0,1-13.75-3.26A22,22,0,0,1,345,96.12a30.44,30.44,0,0,1-3.1-14.19,30.28,30.28,0,0,1,3.1-14,22.88,22.88,0,0,1,8.76-9.41,25.4,25.4,0,0,1,13.27-3.38,27.48,27.48,0,0,1,9.57,1.64,21.68,21.68,0,0,1,7.76,4.91,22.48,22.48,0,0,1,5.18,8.21,33,33,0,0,1,1.85,11.56v3.94H347.67v-8.9h30.22a11,11,0,0,0-1.35-5.5,9.81,9.81,0,0,0-3.72-3.76,10.92,10.92,0,0,0-5.52-1.37,11.1,11.1,0,0,0-5.79,1.5,10.82,10.82,0,0,0-3.93,4,11.42,11.42,0,0,0-1.45,5.58v8.47a14.33,14.33,0,0,0,1.44,6.64,10.22,10.22,0,0,0,4.06,4.32,12.33,12.33,0,0,0,6.24,1.52,13.61,13.61,0,0,0,4.39-.67,9.14,9.14,0,0,0,3.41-2,8.94,8.94,0,0,0,2.16-3.3l13.29.87a18.14,18.14,0,0,1-4.14,8.35,20.71,20.71,0,0,1-8,5.53A30.14,30.14,0,0,1,367.64,108.62Z"/>
                    <path class="cls-2" d="M401.07,107.61V38.52h45.74v12H415.68V67h28.1v12h-28.1v28.54Z"/>
                    <path class="cls-2" d="M486.35,85.54V55.79h14.37v51.82H486.93V98.19h-.54a15.3,15.3,0,0,1-5.82,7.32,17.14,17.14,0,0,1-9.9,2.77,17.51,17.51,0,0,1-9.15-2.36,16.07,16.07,0,0,1-6.15-6.71,23,23,0,0,1-2.25-10.43v-33h14.37V86.22A10.51,10.51,0,0,0,470,93.47a8.37,8.37,0,0,0,6.51,2.67,10.23,10.23,0,0,0,4.86-1.2A9.39,9.39,0,0,0,485,91.38,11.1,11.1,0,0,0,486.35,85.54Z"/>
                    <path class="cls-2" d="M555.31,70.56l-13.16.81a6.9,6.9,0,0,0-1.45-3.05,7.89,7.89,0,0,0-2.92-2.19,10.24,10.24,0,0,0-4.3-.83,10.64,10.64,0,0,0-5.63,1.4,4.23,4.23,0,0,0-2.3,3.73A4,4,0,0,0,527,73.57a11.72,11.72,0,0,0,5.09,2.05l9.38,1.89q7.56,1.56,11.27,5a11.75,11.75,0,0,1,3.71,9,14.2,14.2,0,0,1-3,8.94,19.55,19.55,0,0,1-8.16,6,31.16,31.16,0,0,1-11.93,2.14q-10.29,0-16.38-4.3a16.67,16.67,0,0,1-7.13-11.73L524,91.85a7.26,7.26,0,0,0,3.11,4.78,13.18,13.18,0,0,0,12.4.16A4.43,4.43,0,0,0,541.88,93a4,4,0,0,0-1.65-3.22,12,12,0,0,0-5-1.94l-9-1.79q-7.59-1.51-11.28-5.26a13,13,0,0,1-3.7-9.55,13.88,13.88,0,0,1,2.72-8.6,17.16,17.16,0,0,1,7.66-5.57,31.41,31.41,0,0,1,11.58-2q9.82,0,15.47,4.15A16.06,16.06,0,0,1,555.31,70.56Z"/>
                    <path class="cls-2" d="M572.78,49.11A7.75,7.75,0,0,1,567.3,47a6.9,6.9,0,0,1,0-10.27,8.09,8.09,0,0,1,10.94,0,6.93,6.93,0,0,1,0,10.28A7.72,7.72,0,0,1,572.78,49.11Zm-7.22,58.5V55.79h14.37v51.82Z"/>
                    <path class="cls-2" d="M614.82,108.62a26.3,26.3,0,0,1-13.58-3.36,22.64,22.64,0,0,1-8.83-9.38,30.19,30.19,0,0,1-3.1-14,30.37,30.37,0,0,1,3.1-14.05,22.64,22.64,0,0,1,8.83-9.38,29.1,29.1,0,0,1,27.15,0,22.64,22.64,0,0,1,8.83,9.38,30.37,30.37,0,0,1,3.1,14.05,30.19,30.19,0,0,1-3.1,14,22.64,22.64,0,0,1-8.83,9.38A26.29,26.29,0,0,1,614.82,108.62Zm.06-11.13a8.82,8.82,0,0,0,6-2,12.52,12.52,0,0,0,3.63-5.58,27,27,0,0,0,0-16.12,12.59,12.59,0,0,0-3.63-5.6,8.78,8.78,0,0,0-6-2.06,9,9,0,0,0-6,2.06,12.41,12.41,0,0,0-3.68,5.6,27,27,0,0,0,0,16.12,12.35,12.35,0,0,0,3.68,5.58A9.09,9.09,0,0,0,614.88,97.49Z"/>
                    <path class="cls-2" d="M664.07,77.65v30H649.7V55.79h13.7v9.14h.6a14.6,14.6,0,0,1,5.77-7.17,17.59,17.59,0,0,1,9.82-2.65A18.26,18.26,0,0,1,689,57.47a16,16,0,0,1,6.24,6.74,22.57,22.57,0,0,1,2.23,10.4v33H683.1V77.18c0-3.18-.79-5.65-2.43-7.44a8.73,8.73,0,0,0-6.78-2.68,10.2,10.2,0,0,0-5.11,1.24,8.56,8.56,0,0,0-3.44,3.63A12.45,12.45,0,0,0,664.07,77.65Z"/>
                    <path class="cls-1" d="M721,107.61H705.29l23.86-69.09H748l23.82,69.09H756.13L738.83,54.3h-.54Zm-1-27.16h37v11.4H720Z"/>
                    <path class="cls-1" d="M787.24,49.11A7.76,7.76,0,0,1,781.75,47a6.92,6.92,0,0,1,0-10.27,8.1,8.1,0,0,1,11,0,6.93,6.93,0,0,1,0,10.28A7.76,7.76,0,0,1,787.24,49.11ZM780,107.61V55.79h14.37v51.82Z"/>
                    <path class="cls-3" d="M.46,79.85a45.62,45.62,0,0,0,91.13,0,2.16,2.16,0,0,0-2.15-2.28H58.74a12.72,12.72,0,0,1-25.43,0H2.61A2.17,2.17,0,0,0,.46,79.85ZM46,96.62A18.94,18.94,0,0,0,59.49,91l15,14.95A40,40,0,0,1,46,117.77Z"/>
                    <path class="cls-3" d="M46,83.77a6.21,6.21,0,0,0,6.2-6.2H39.82A6.21,6.21,0,0,0,46,83.77Z"/>
                    <path class="cls-3" d="M52.9,3v7.55a3,3,0,0,1-3,3H48.32v6.79H43.74V13.57H42.2a3,3,0,0,1-3-3V3a3,3,0,0,1,3-3h7.65A3,3,0,0,1,52.9,3Z"/>
                    <path class="cls-2" d="M96.76,36.77V29.46a2.05,2.05,0,0,1,2-2.06h0a2.06,2.06,0,0,1,2.06,2.06v7.31a2.06,2.06,0,0,1-2.06,2.06h0A2.05,2.05,0,0,1,96.76,36.77Z"/>
                    <path class="cls-2" d="M103.2,36.77V29.46a2.06,2.06,0,0,1,2.06-2.06h0a2.06,2.06,0,0,1,2.06,2.06v7.31a2.06,2.06,0,0,1-2.06,2.06h0A2.06,2.06,0,0,1,103.2,36.77Z"/>
                    <rect class="cls-2" x="109.65" y="27.4" width="4.11" height="11.43" rx="0.91"/>
                    <path class="cls-2" d="M116.1,36.77V29.46a2.05,2.05,0,0,1,2.05-2.06h0a2.06,2.06,0,0,1,2.06,2.06v7.31a2.06,2.06,0,0,1-2.06,2.06h0A2.05,2.05,0,0,1,116.1,36.77Z"/>
                    <path class="cls-2" d="M96.76,50.86V43.55a2.05,2.05,0,0,1,2-2.06h0a2.06,2.06,0,0,1,2.06,2.06v7.31a2.06,2.06,0,0,1-2.06,2.06h0A2.05,2.05,0,0,1,96.76,50.86Z"/>
                    <path class="cls-2" d="M103.2,50.86V43.55a2.06,2.06,0,0,1,2.06-2.06h0a2.06,2.06,0,0,1,2.06,2.06v7.31a2.06,2.06,0,0,1-2.06,2.06h0A2.06,2.06,0,0,1,103.2,50.86Z"/>
                    <path class="cls-2" d="M96.76,65V57.64a2.05,2.05,0,0,1,2-2h0a2.05,2.05,0,0,1,2.06,2V65A2.05,2.05,0,0,1,98.81,67h0A2.05,2.05,0,0,1,96.76,65Z"/>
                    <path class="cls-2" d="M103.2,65V57.64a2.05,2.05,0,0,1,2.06-2h0a2.05,2.05,0,0,1,2.06,2V65A2.05,2.05,0,0,1,105.26,67h0A2.05,2.05,0,0,1,103.2,65Z"/>
                    <rect class="cls-2" x="109.65" y="55.59" width="4.11" height="11.43" rx="0.91"/>
                    <path class="cls-4" d="M83.53,20.36h-75A8.52,8.52,0,0,0,0,28.87v40a2.3,2.3,0,0,0,2.29,2.29H89.76a2.3,2.3,0,0,0,2.29-2.29v-40A8.52,8.52,0,0,0,83.53,20.36ZM28.85,56.08A10.31,10.31,0,1,1,39.16,45.77,10.31,10.31,0,0,1,28.85,56.08Zm34.35,0A10.31,10.31,0,1,1,73.5,45.77,10.31,10.31,0,0,1,63.2,56.08Z"/>
                    </g>
                    </g>
                    </svg>
                  <div class="f-14 f-md-16 w400 mt20 lh150 white-clr text-center" style="color:#7d8398;">
                     <span class="w600">Note:</span> This site is not a part of the Facebook website or Facebook Inc. Additionally, this site is NOT endorsed by Facebook in any way. FACEBOOK & INSTAGRAM are the trademarks of FACEBOOK, Inc.<br>
                  </div>
                  
                  <div class="mt20 white-clr"><script type="text/javascript" src="https://warriorplus.com/o2/disclaimer/qyptv4" defer></script><div class="wplus_spdisclaimer"></div></div>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-14 f-md-16 w400 lh150 white-clr text-xs-center">Copyright © VoiceFusion Ai 2023</div>
                  <ul class="footer-ul w400 f-14 f-md-16 white-clr text-center text-md-right">
                     <li><a href="https://support.bizomart.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://voicefusionai.com/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://voicefusionai.com/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://voicefusionai.com/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://voicefusionai.com/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://voicefusionai.com/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://voicefusionai.com/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                    
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!-- Exit Popup and Timer -->
      <?php include('timer.php'); ?>	  
      <!-- Exit Popup and Timer End -->
   </body>
</html>

<script> 
function myFunction() {
  var checkBox = document.getElementById("check");
  var buybutton = document.getElementById("buyButton");
  if (checkBox.checked == true){
 
	    buybutton.getAttribute("href");
	    buybutton.setAttribute("href","https://warriorplus.com/o2/buy/qyptv4/xw7477/mrtpmt");
	} else {
		 buybutton.getAttribute("href");
		 buybutton.setAttribute("href","https://warriorplus.com/o2/buy/qyptv4/xw7477/tbq1nm");
	  }
}
</script>

<script>
	// Define the audio data as a JSON object
	var audioData = {
	"Angry" : "https://gptvoicer.s3.amazonaws.com/voices/en-US-JaneNeural-angry.mp3",
	"Cheerful" : "https://gptvoicer.s3.amazonaws.com/voices/en-US-JaneNeural-cheerful.mp3",
	"Excited" : "https://gptvoicer.s3.amazonaws.com/voices/en-US-JaneNeural-excited.mp3",
	"Friendly" : "https://gptvoicer.s3.amazonaws.com/voices/en-US-JaneNeural-friendly.mp3",
	"Hopeful" : "https://gptvoicer.s3.amazonaws.com/voices/en-US-JaneNeural-hopeful.mp3",
	"Sad" : "https://gptvoicer.s3.amazonaws.com/voices/en-US-JaneNeural-sad.mp3",
	"Shouting" : "https://gptvoicer.s3.amazonaws.com/voices/en-US-JaneNeural-shouting.mp3",
	"Terrified" : "https://gptvoicer.s3.amazonaws.com/voices/en-US-JaneNeural-terrified.mp3",
	"Unfriendly" : "https://gptvoicer.s3.amazonaws.com/voices/en-US-JaneNeural-unfriendly.mp3",
	"Whispering" : "https://gptvoicer.s3.amazonaws.com/voices/en-US-JaneNeural-whispering.mp3",
	"Normal" : "https://gptvoicer.s3.amazonaws.com/voices/en-US-JaneNeural.mp3"
};
  
	// Get the container for the audio boxes
	var container = document.getElementById('audioContainer');
  
	// Loop through the audio data and generate the HTML for each audio box
	for (var voice in audioData) {
	  var box = '<div class="audiobox" style="background-color: #f6d89e; border-radius: 6px; padding: 5px 5px; margin-top:10px">' +
				  '<div class="row align-items-center">' +
					'<div class="col-5">' +
					  '<div class="f-md-22 f-12 text-center w400">' +
						voice +
					  '</div>' +
					'</div>' +
					'<div class="col-7">' +
					  '<button class="playButton plybtn" data-audio="' + audioData[voice] + '">' +
						'<span class="playIcon" style="font-size: 20px;">▶</span>' +
					  '</button>' +
					  '<audio class="myAudio" data-state="paused">' +
						'<source src="" type="audio/mpeg">' +
					  '</audio>' +
					'</div>' +
				  '</div>' +
				'</div>';
  
	  container.innerHTML += box;
	}
  
	// Get all the play buttons, play icons, and audio elements
	var playButtons = document.querySelectorAll('.playButton');
	var playIcons = document.querySelectorAll('.playIcon');
	var audioElements = document.querySelectorAll('.myAudio');
  
	// Add event listeners to the play buttons
	playButtons.forEach(function(button, index) {
	  button.addEventListener('click', function() {
		var audio = audioElements[index];
		var playIcon = playIcons[index];
  
		if (audio.dataset.state === 'paused') {
		  // Pause any currently playing audio elements
		  audioElements.forEach(function(element) {
			if (element.dataset.state === 'playing') {
			  element.pause();
			  var index = Array.from(audioElements).indexOf(element);
			  playIcons[index].innerHTML = '▶';
			  element.dataset.state = 'paused';
			}
		  });
  
		  // Start playing the new audio element
		  audio.src = button.dataset.audio;
		  audio.play();
		  playIcon.innerHTML = '❚❚';
		  audio.dataset.state = 'playing';
		} else {
		  audio.pause();
		  playIcon.innerHTML = '▶';
		  audio.dataset.state = 'paused';
		}
	  });
	});
  </script>
  <script>
	// Define the audio data as a JSON object
	var audioDatamale = {
	"Angry" : "https://gptvoicer.s3.amazonaws.com/voices/en-US-JasonNeural-angry.mp3",
	"Cheerful" : "https://gptvoicer.s3.amazonaws.com/voices/en-US-JasonNeural-cheerful.mp3",
	"Excited" : "https://gptvoicer.s3.amazonaws.com/voices/en-US-JasonNeural-excited.mp3",
	"Friendly" : "https://gptvoicer.s3.amazonaws.com/voices/en-US-JasonNeural-friendly.mp3",
	"Hopeful" : "https://gptvoicer.s3.amazonaws.com/voices/en-US-JasonNeural-hopeful.mp3",
	"Sad" : "https://gptvoicer.s3.amazonaws.com/voices/en-US-JasonNeural-sad.mp3",
	"Shouting" : "https://gptvoicer.s3.amazonaws.com/voices/en-US-JasonNeural-shouting.mp3",
	"Terrified" : "https://gptvoicer.s3.amazonaws.com/voices/en-US-JasonNeural-terrified.mp3",
	"Unfriendly" : "https://gptvoicer.s3.amazonaws.com/voices/en-US-JasonNeural-unfriendly.mp3",
	"Whispering" : "https://gptvoicer.s3.amazonaws.com/voices/en-US-JasonNeural-whispering.mp3",
	"Normal" : "https://gptvoicer.s3.amazonaws.com/voices/en-US-JasonNeural.mp3"
}
  
	// Get the container for the audio boxes
	var containermale = document.getElementById('audioContainermale');
  
	// Loop through the audio data and generate the HTML for each audio box
	for (var voicemale in audioDatamale) {
	  var boxmale = '<div class="audioboxmale" style="background-color: #bddbec; border-radius: 6px; padding: 5px 5px; margin-top:10px">' +
				  '<div class="row align-items-center">' +
					'<div class="col-5">' +
					  '<div class="f-md-22 f-12 text-center w400">' +
						voicemale +
					  '</div>' +
					'</div>' +
					'<div class="col-7">' +
					  '<button class="playButtonmale plybtnmale" data-audio="' + audioDatamale[voicemale] + '">' +
						'<span class="playIconmale" style="font-size: 20px;">▶</span>' +
					  '</button>' +
					  '<audio class="myAudiomale" data-state="paused">' +
						'<source src="" type="audio/mpeg">' +
					  '</audio>' +
					'</div>' +
				  '</div>' +
				'</div>';
  
		containermale.innerHTML += boxmale;
	}
  
	// Get all the play buttons, play icons, and audio elements
	var playButtonsmale = document.querySelectorAll('.playButtonmale');
	var playIconsmale = document.querySelectorAll('.playIconmale');
	var audioElementsmale = document.querySelectorAll('.myAudiomale');
  
	// Add event listeners to the play buttons
	playButtonsmale.forEach(function(button, index) {
	  button.addEventListener('click', function() {
		var audio = audioElementsmale[index];
		var playIcon = playIconsmale[index];
  
		if (audio.dataset.state === 'paused') {
		  // Pause any currently playing audio elements
		  audioElementsmale.forEach(function(element) {
			if (element.dataset.state === 'playing') {
			  element.pause();
			  var index = Array.from(audioElementsmale).indexOf(element);
			  playIconsmale[index].innerHTML = '▶';
			  element.dataset.state = 'paused';
			}
		  });
  
		  // Start playing the new audio element
		  audio.src = button.dataset.audio;
		  audio.play();
		  playIcon.innerHTML = '❚❚';
		  audio.dataset.state = 'playing';
		} else {
		  audio.pause();
		  playIcon.innerHTML = '▶';
		  audio.dataset.state = 'paused';
		}
	  });
	});
  </script>
<!Doctype html>
<html>
   <head>
      <title>Aicademy Agency</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=9">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
      <meta name="title" content="Aicademy | Agency">
      <meta name="description" content="Aicademy">
      <meta name="keywords" content="Aicademy">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta property="og:image" content="https://www.aicademy.live/agency/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Pranshu Gupta">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="Aicademy | Agency">
      <meta property="og:description" content="Aicademy">
      <meta property="og:image" content="https://www.aicademy.live/agency/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="Aicademy | Agency">
      <meta property="twitter:description" content="Aicademy">
      <meta property="twitter:image" content="https://www.aicademy.live/agency/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Pranshu Gupta">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="Aicademy | Reseller">
      <meta property="og:description" content="Aicademy">
      <meta property="og:image" content="https://aicademy.live/reseller/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="Aicademy | Reseller">
      <meta property="twitter:description" content="Aicademy">
      <meta property="twitter:image" content="https://aicademy.live/reseller/thumbnail.png">
      <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&family=Poppins:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
      <!-- Start Editor required -->
      <link rel="stylesheet" href="https://cdn.oppyotest.com/launches/sellero/common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style.css" type="text/css">
      <link rel="stylesheet" href="assets/css/timer.css" type="text/css">
      <script src="https://cdn.oppyotest.com/launches/sellero/common_assets/js/bootstrap.min.js"></script>
      <script src="https://cdn.oppyotest.com/launches/sellero/common_assets/js/jquery.min.js"></script>
      <!-- End -->
   </head>
   <body>
      <!--1. Header Section Start -->
      <div class="warning-section">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <div class="offer">
                        <div>
                            <img src="https://cdn.oppyo.com/launches/appzilo/special/error.webp">
                        </div>
                        <div class="f-22 f-md-30 w500 lh140 white-clr text-center">
                            <span class="w600">WAIT!</span>  YOUR ORDER IS NOT YET COMPLETED... *DO NOT CLOSE*
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
      <div class="header-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div>
                     <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 644.49 120" style="max-height:55px">
                        <defs>
                          <style>
                            .cls-1 {
                              fill: #fff;
                            }
                            .cls-2 {
                              fill: #0af;
                            }
                          </style>
                        </defs>
                        <g id="Layer_1-2" data-name="Layer 1">
                          <g>
                            <g>
                              <circle class="cls-2" cx="79.02" cy="43.5" r="7.25"></circle>
                              <circle class="cls-2" cx="70.47" cy="78.46" r="5.46"></circle>
                              <path class="cls-2" d="m158.03,45.28l-33.74,13.31v36.4l-45.27,16.86-27.9-10.4c-4.17,3.37-7.04,6.74-8.15,10.11-1.01,3.22-.07,6.21,2.46,8.42-5.44-1.46-8-5.25-6.96-9.76,1.24-3.62,4.79-7.68,10-12.08h.01c3.95-3.31,8.84-6.84,14.46-10.5,2.04,1.67,4.68,2.68,7.53,2.68,6.56,0,11.88-5.32,11.88-11.88s-5.32-11.88-11.88-11.88-11.88,5.32-11.88,11.88c0,1.13.15,2.21.46,3.25-8.01,5.01-14.83,10.36-19.57,15.44l-5.75-2.14v-7.66c-7.84,4.92-11.84,11.06-5.99,16.99-5.17-1.64-7.69-4.49-7.69-8.05,0-4.83,4.67-10.99,13.69-17.26l35.41-23.23c2.71,2.18,6.13,3.48,9.87,3.48,8.7,0,15.76-7.06,15.76-15.76s-7.06-15.76-15.76-15.76-15.76,7.06-15.76,15.76c0,1.59.23,3.12.68,4.57l-30.2,19.53v-9L0,45.28,79.02,0l79.01,45.28Z"></path>
                              <path class="cls-2" d="m149.38,68.32v-19.62l-1.51.6v19.03c-1.66.37-2.9,1.96-2.9,3.85v11.69h7.31v-11.69c0-1.9-1.24-3.48-2.9-3.85Z"></path>
                            </g>
                            <g>
                              <g>
                                <path class="cls-2" d="m178.03,98.08l22.25-71.1h19.3l22.25,71.1h-17.01l-4.36-16.9h-21.7l-4.36,16.9h-16.36Zm25.74-35.99l-1.74,6.54h15.16l-1.64-6.54c-.95-3.56-1.91-7.34-2.89-11.34-.98-4-1.95-7.85-2.89-11.56h-.44c-.87,3.78-1.76,7.65-2.67,11.61-.91,3.96-1.87,7.73-2.89,11.29Z"></path>
                                <path class="cls-2" d="m256.22,36.03c-2.69,0-4.87-.76-6.54-2.29-1.67-1.53-2.51-3.56-2.51-6.11s.83-4.58,2.51-6.11c1.67-1.53,3.85-2.29,6.54-2.29s4.87.76,6.54,2.29c1.67,1.53,2.51,3.56,2.51,6.11s-.84,4.58-2.51,6.11c-1.67,1.53-3.85,2.29-6.54,2.29Zm-7.96,62.05v-54.09h16.03v54.09h-16.03Z"></path>
                              </g>
                              <g>
                                <path class="cls-1" d="m299.89,99.38c-5.02,0-9.54-1.11-13.58-3.33-4.03-2.22-7.23-5.45-9.6-9.71-2.36-4.25-3.54-9.36-3.54-15.32s1.31-11.16,3.93-15.38c2.62-4.22,6.07-7.43,10.36-9.65,4.29-2.22,8.9-3.33,13.85-3.33,3.34,0,6.31.55,8.89,1.64,2.58,1.09,4.89,2.47,6.92,4.14l-7.52,10.36c-2.55-2.11-4.98-3.16-7.31-3.16-3.85,0-6.92,1.38-9.21,4.14-2.29,2.76-3.44,6.51-3.44,11.23s1.15,8.38,3.44,11.18c2.29,2.8,5.18,4.2,8.67,4.2,1.74,0,3.45-.38,5.13-1.15,1.67-.76,3.2-1.69,4.58-2.78l6.33,10.47c-2.69,2.33-5.6,3.98-8.72,4.96-3.13.98-6.18,1.47-9.16,1.47Z"></path>
                                <path class="cls-1" d="m339.81,99.38c-4.94,0-8.87-1.58-11.78-4.74-2.91-3.16-4.36-7.03-4.36-11.61,0-5.67,2.4-10.1,7.2-13.3,4.8-3.2,12.54-5.34,23.23-6.43-.15-2.4-.86-4.31-2.13-5.73-1.27-1.42-3.4-2.13-6.38-2.13-2.25,0-4.54.44-6.87,1.31-2.33.87-4.8,2.07-7.42,3.6l-5.78-10.58c3.42-2.11,7.07-3.82,10.96-5.13,3.89-1.31,7.94-1.96,12.16-1.96,6.91,0,12.21,2,15.92,6,3.71,4,5.56,10.14,5.56,18.43v30.97h-13.09l-1.09-5.56h-.44c-2.25,2.04-4.67,3.69-7.25,4.96-2.58,1.27-5.4,1.91-8.45,1.91Zm5.45-12.43c1.82,0,3.4-.42,4.74-1.25,1.34-.83,2.71-1.94,4.09-3.33v-9.49c-5.67.73-9.6,1.87-11.78,3.44-2.18,1.56-3.27,3.4-3.27,5.51,0,1.74.56,3.04,1.69,3.87,1.13.84,2.63,1.25,4.53,1.25Z"></path>
                                <path class="cls-1" d="m402.95,99.38c-6.69,0-12.05-2.53-16.09-7.58-4.03-5.05-6.05-11.98-6.05-20.77,0-5.89,1.07-10.96,3.22-15.21,2.14-4.25,4.94-7.51,8.4-9.76,3.45-2.25,7.07-3.38,10.85-3.38,2.98,0,5.49.51,7.52,1.53,2.04,1.02,3.96,2.4,5.78,4.14l-.66-8.29v-18.43h16.03v76.45h-13.09l-1.09-5.34h-.44c-1.89,1.89-4.11,3.47-6.65,4.74-2.55,1.27-5.13,1.91-7.74,1.91Zm4.14-13.09c1.74,0,3.33-.36,4.74-1.09,1.42-.73,2.78-2,4.09-3.82v-22.14c-1.38-1.31-2.85-2.22-4.42-2.73-1.56-.51-3.07-.76-4.53-.76-2.55,0-4.8,1.22-6.76,3.65-1.96,2.44-2.94,6.23-2.94,11.4s.85,9.21,2.56,11.72c1.71,2.51,4.13,3.76,7.25,3.76Z"></path>
                                <path class="cls-1" d="m470.34,99.38c-5.16,0-9.81-1.13-13.96-3.38-4.14-2.25-7.42-5.49-9.81-9.71-2.4-4.22-3.6-9.31-3.6-15.27s1.22-10.94,3.65-15.16c2.43-4.22,5.62-7.47,9.54-9.76,3.93-2.29,8.03-3.44,12.32-3.44,5.16,0,9.43,1.15,12.81,3.44,3.38,2.29,5.92,5.38,7.63,9.27,1.71,3.89,2.56,8.31,2.56,13.25,0,1.38-.07,2.74-.22,4.09-.15,1.35-.29,2.34-.44,3h-32.39c.73,3.93,2.36,6.82,4.91,8.67,2.54,1.85,5.6,2.78,9.16,2.78,3.85,0,7.74-1.2,11.67-3.6l5.34,9.71c-2.76,1.89-5.85,3.38-9.27,4.47-3.42,1.09-6.73,1.64-9.92,1.64Zm-12-34.24h19.52c0-2.98-.71-5.43-2.13-7.36-1.42-1.93-3.73-2.89-6.92-2.89-2.47,0-4.69.86-6.65,2.56-1.96,1.71-3.24,4.27-3.82,7.69Z"></path>
                                <path class="cls-1" d="m502.62,98.08v-54.09h13.09l1.09,6.98h.44c2.25-2.25,4.65-4.2,7.2-5.83,2.54-1.64,5.6-2.45,9.16-2.45,3.85,0,6.96.78,9.32,2.34,2.36,1.56,4.23,3.8,5.62,6.71,2.4-2.47,4.94-4.6,7.63-6.38,2.69-1.78,5.82-2.67,9.38-2.67,5.82,0,10.09,1.95,12.81,5.83,2.73,3.89,4.09,9.21,4.09,15.98v33.59h-16.03v-31.52c0-3.93-.53-6.61-1.58-8.07-1.05-1.45-2.74-2.18-5.07-2.18-2.69,0-5.78,1.74-9.27,5.23v36.53h-16.03v-31.52c0-3.93-.53-6.61-1.58-8.07-1.05-1.45-2.74-2.18-5.07-2.18-2.69,0-5.74,1.74-9.16,5.23v36.53h-16.03Z"></path>
                                <path class="cls-1" d="m602.07,119.23c-1.6,0-3-.11-4.2-.33-1.2-.22-2.34-.47-3.44-.76l2.84-12.21c.51.07,1.09.2,1.74.38.65.18,1.27.27,1.85.27,2.69,0,4.76-.65,6.22-1.96,1.45-1.31,2.54-3.02,3.27-5.13l.76-2.84-20.83-52.67h16.14l7.74,23.23c.8,2.47,1.53,4.98,2.18,7.52.65,2.55,1.34,5.16,2.07,7.85h.44c.58-2.54,1.18-5.11,1.8-7.69.62-2.58,1.25-5.14,1.91-7.69l6.54-23.23h15.38l-18.76,54.63c-1.67,4.51-3.53,8.31-5.56,11.4-2.04,3.09-4.49,5.4-7.36,6.92-2.87,1.53-6.45,2.29-10.74,2.29Z"></path>
                              </g>
                            </g>
                          </g>
                        </g>
                      </svg>
                  </div>
               </div>
               <div class="col-12 text-center lh150 mt20 mt-md50">
                  <div class="pre-heading f-18 f-md-22 w600 lh140 white-clr">
                     Before you access your purchase, I want to ask you an important question...
                  </div>
               </div>
               <div class="col-12 mt-md30 mt20 f-md-45 f-28 w400 text-center white-clr lh140">
                  How would you like to <span class="yellow-clr w700">Setup Your Own Profitable Pro-Agency Almost Instantly</span> And Charge $1000 Per Client Without Any Hard Work?
               </div>
               <div class="col-12 mt-md25 mt20">
                  <div class="f-20 f-md-22 w500 lh140 white-clr text-center">
                     <span class="w600">We Did All the Work… You Sell High In Demand DFY Services</span> To Hungry<br class="d-none d-md-block"> Clients & Keep 100% Profits
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
                  <img src="assets/images/product-image.webp" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 mt30 mt-md50 f-md-22 f-20 lh140 w400 text-center white-clr">
                  This is An Exclusive Deal for New<span class="w600 yellow-clr"> "Aicademy"</span> Users Only...
               </div>
               <div class="col-md-9 mx-auto col-12 mt20 f-20 f-md-35 text-center probtn1">
                  <a href="#buynow" class="text-center">Upgrade to Aicademy Agency Now</a>
               </div>
            </div>
         </div>
      </div>
      <!--1. Header Section End -->
      <!--2. Second Section Start -->
      <div class="second-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-md-45 f-28 w500 lh140 text-capitalize text-center black-clr">
                     The Secret Weapon To 2X Your Profits <span class="w700"> From Your Aicademy Account </span>
                  </div>
               </div>
               <div class="col-12 text-center">
                  <div class="f-20 f-md-22 text-center w500 lh140 text-left mt15 mt-md40">
                     This Agency upgrade puts you on whole another level with a touch of a button and <span class="w700"> enables you to enter in $250 Billion freelancing industry.</span><br><br> From breakthrough academy site creation technology,
                     complete business panel to manage your client’s biz from single dashboard, adding your team members or freelancing staff to manage everything easily, <span class="w600">the agency upgrade has it all.</span>
                  </div>
               </div>
               <div class="col-md-6 col-12 mt30 mt-md70">
                  <div class="row">
                     <div class="col-md-2 col-12 px5">
                        <div>
                           <img src="assets/images/fe1.webp" class="img-fluid mx-auto d-block" width="70">
                        </div>
                     </div>
                     <div class="col-md-10 col-12 mt20 mt-md0">
                        <div class="f-18 f-md-20 lh150 w400 d-left-m-center">
                           <span class="w600">Sell high-in-demand Academy Site creation services To Your Clients</span> with Aicademy & DFY Support for Our Platform.
                        </div>
                     </div>
                  </div>
                  <!---3-->
                  <div class="row mt20 mt-md20 align-items-center">
                     <div class="col-md-2 col-12 px5">
                        <div>
                           <img src="assets/images/fe3.webp" class="img-fluid mx-auto d-block" width="70">
                        </div>
                     </div>
                     <div class="col-md-10 col-12 mt20 mt-md0">
                        <div class="f-18 f-md-20 lh150 w400 d-left-m-center">
                           <span class="w600">Serve Unlimited Clients with Agency License  </span>
                        </div>
                     </div>
                  </div>
                  <!--4-->
                  <div class="row mt20 mt-md50">
                     <div class="col-md-2 col-12 px5">
                        <div><img src="assets/images/fe5.webp" class="img-fluid mx-auto d-block" width="70"></div>
                     </div>
                     <div class="col-md-10 col-12 mt20 mt-md0">
                        <div class="f-18 f-md-20 lh150 w400 d-left-m-center mb0 mb-md20">
                           <span class="w600">Add Unlimited Team Members – Inhouse & freelancers </span>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-6 col-12 mt30 mt-md70">
                  <div class="row mt20 mt-md0">
                     <div class="col-md-2 col-12 px5">
                        <div><img src="assets/images/fe2.webp" class="img-fluid mx-auto d-block" width="70"></div>
                     </div>
                     <div class="col-md-10 col-12 mt20 mt-md0">
                        <div class="f-18 f-md-20 lh150 w400 d-left-m-center">
                           <span class="w600">Done-For-Your Business Management Panel -</span>Manage all your client’s businesses from a single dashboard to have full control.
                        </div>
                     </div>
                  </div>
                  <!--11-->
                  <div class="row mt20 mt-md50">
                     <div class="col-md-2 col-12 px5">
                        <div><img src="assets/images/fe4.webp" class="img-fluid mx-auto d-block" width="70"></div>
                     </div>
                     <div class="col-md-10  col-12 mt20 mt-md15">
                        <div class="f-18 f-md-20 lh150 w400 d-left-m-center">
                           <span class="w600">Get All These Benefits</span> For One Time Price
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-9 mx-auto col-12 mt10 mt-md60 f-22 f-md-36 text-center probtn1">
                  <a href="#buynow" class="text-center">Upgrade to Aicademy Agency Now</a>
               </div>
            </div>
         </div>
      </div>
      <!--2. Second Section End -->
      <!--4. Fourth Section Starts -->
      <div class="fifth-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-38 w700 white-clr lh140 presenting-head">
                     Proudly Presenting…
                  </div>
               </div>
               <div class="col-12 mt-md30 mt20 text-center">
                  <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 644.49 120" style="max-height: 135px;">
                     <defs>
                       <style>
                         .cls-1 {
                           fill: #fff;
                         }
                   
                         .cls-2 {
                           fill: #0af;
                         }
                       </style>
                     </defs>
                     <g id="Layer_1-2" data-name="Layer 1">
                       <g>
                         <g>
                           <circle class="cls-2" cx="79.02" cy="43.5" r="7.25"></circle>
                           <circle class="cls-2" cx="70.47" cy="78.46" r="5.46"></circle>
                           <path class="cls-2" d="m158.03,45.28l-33.74,13.31v36.4l-45.27,16.86-27.9-10.4c-4.17,3.37-7.04,6.74-8.15,10.11-1.01,3.22-.07,6.21,2.46,8.42-5.44-1.46-8-5.25-6.96-9.76,1.24-3.62,4.79-7.68,10-12.08h.01c3.95-3.31,8.84-6.84,14.46-10.5,2.04,1.67,4.68,2.68,7.53,2.68,6.56,0,11.88-5.32,11.88-11.88s-5.32-11.88-11.88-11.88-11.88,5.32-11.88,11.88c0,1.13.15,2.21.46,3.25-8.01,5.01-14.83,10.36-19.57,15.44l-5.75-2.14v-7.66c-7.84,4.92-11.84,11.06-5.99,16.99-5.17-1.64-7.69-4.49-7.69-8.05,0-4.83,4.67-10.99,13.69-17.26l35.41-23.23c2.71,2.18,6.13,3.48,9.87,3.48,8.7,0,15.76-7.06,15.76-15.76s-7.06-15.76-15.76-15.76-15.76,7.06-15.76,15.76c0,1.59.23,3.12.68,4.57l-30.2,19.53v-9L0,45.28,79.02,0l79.01,45.28Z"></path>
                           <path class="cls-2" d="m149.38,68.32v-19.62l-1.51.6v19.03c-1.66.37-2.9,1.96-2.9,3.85v11.69h7.31v-11.69c0-1.9-1.24-3.48-2.9-3.85Z"></path>
                         </g>
                         <g>
                           <g>
                             <path class="cls-2" d="m178.03,98.08l22.25-71.1h19.3l22.25,71.1h-17.01l-4.36-16.9h-21.7l-4.36,16.9h-16.36Zm25.74-35.99l-1.74,6.54h15.16l-1.64-6.54c-.95-3.56-1.91-7.34-2.89-11.34-.98-4-1.95-7.85-2.89-11.56h-.44c-.87,3.78-1.76,7.65-2.67,11.61-.91,3.96-1.87,7.73-2.89,11.29Z"></path>
                             <path class="cls-2" d="m256.22,36.03c-2.69,0-4.87-.76-6.54-2.29-1.67-1.53-2.51-3.56-2.51-6.11s.83-4.58,2.51-6.11c1.67-1.53,3.85-2.29,6.54-2.29s4.87.76,6.54,2.29c1.67,1.53,2.51,3.56,2.51,6.11s-.84,4.58-2.51,6.11c-1.67,1.53-3.85,2.29-6.54,2.29Zm-7.96,62.05v-54.09h16.03v54.09h-16.03Z"></path>
                           </g>
                           <g>
                             <path class="cls-1" d="m299.89,99.38c-5.02,0-9.54-1.11-13.58-3.33-4.03-2.22-7.23-5.45-9.6-9.71-2.36-4.25-3.54-9.36-3.54-15.32s1.31-11.16,3.93-15.38c2.62-4.22,6.07-7.43,10.36-9.65,4.29-2.22,8.9-3.33,13.85-3.33,3.34,0,6.31.55,8.89,1.64,2.58,1.09,4.89,2.47,6.92,4.14l-7.52,10.36c-2.55-2.11-4.98-3.16-7.31-3.16-3.85,0-6.92,1.38-9.21,4.14-2.29,2.76-3.44,6.51-3.44,11.23s1.15,8.38,3.44,11.18c2.29,2.8,5.18,4.2,8.67,4.2,1.74,0,3.45-.38,5.13-1.15,1.67-.76,3.2-1.69,4.58-2.78l6.33,10.47c-2.69,2.33-5.6,3.98-8.72,4.96-3.13.98-6.18,1.47-9.16,1.47Z"></path>
                             <path class="cls-1" d="m339.81,99.38c-4.94,0-8.87-1.58-11.78-4.74-2.91-3.16-4.36-7.03-4.36-11.61,0-5.67,2.4-10.1,7.2-13.3,4.8-3.2,12.54-5.34,23.23-6.43-.15-2.4-.86-4.31-2.13-5.73-1.27-1.42-3.4-2.13-6.38-2.13-2.25,0-4.54.44-6.87,1.31-2.33.87-4.8,2.07-7.42,3.6l-5.78-10.58c3.42-2.11,7.07-3.82,10.96-5.13,3.89-1.31,7.94-1.96,12.16-1.96,6.91,0,12.21,2,15.92,6,3.71,4,5.56,10.14,5.56,18.43v30.97h-13.09l-1.09-5.56h-.44c-2.25,2.04-4.67,3.69-7.25,4.96-2.58,1.27-5.4,1.91-8.45,1.91Zm5.45-12.43c1.82,0,3.4-.42,4.74-1.25,1.34-.83,2.71-1.94,4.09-3.33v-9.49c-5.67.73-9.6,1.87-11.78,3.44-2.18,1.56-3.27,3.4-3.27,5.51,0,1.74.56,3.04,1.69,3.87,1.13.84,2.63,1.25,4.53,1.25Z"></path>
                             <path class="cls-1" d="m402.95,99.38c-6.69,0-12.05-2.53-16.09-7.58-4.03-5.05-6.05-11.98-6.05-20.77,0-5.89,1.07-10.96,3.22-15.21,2.14-4.25,4.94-7.51,8.4-9.76,3.45-2.25,7.07-3.38,10.85-3.38,2.98,0,5.49.51,7.52,1.53,2.04,1.02,3.96,2.4,5.78,4.14l-.66-8.29v-18.43h16.03v76.45h-13.09l-1.09-5.34h-.44c-1.89,1.89-4.11,3.47-6.65,4.74-2.55,1.27-5.13,1.91-7.74,1.91Zm4.14-13.09c1.74,0,3.33-.36,4.74-1.09,1.42-.73,2.78-2,4.09-3.82v-22.14c-1.38-1.31-2.85-2.22-4.42-2.73-1.56-.51-3.07-.76-4.53-.76-2.55,0-4.8,1.22-6.76,3.65-1.96,2.44-2.94,6.23-2.94,11.4s.85,9.21,2.56,11.72c1.71,2.51,4.13,3.76,7.25,3.76Z"></path>
                             <path class="cls-1" d="m470.34,99.38c-5.16,0-9.81-1.13-13.96-3.38-4.14-2.25-7.42-5.49-9.81-9.71-2.4-4.22-3.6-9.31-3.6-15.27s1.22-10.94,3.65-15.16c2.43-4.22,5.62-7.47,9.54-9.76,3.93-2.29,8.03-3.44,12.32-3.44,5.16,0,9.43,1.15,12.81,3.44,3.38,2.29,5.92,5.38,7.63,9.27,1.71,3.89,2.56,8.31,2.56,13.25,0,1.38-.07,2.74-.22,4.09-.15,1.35-.29,2.34-.44,3h-32.39c.73,3.93,2.36,6.82,4.91,8.67,2.54,1.85,5.6,2.78,9.16,2.78,3.85,0,7.74-1.2,11.67-3.6l5.34,9.71c-2.76,1.89-5.85,3.38-9.27,4.47-3.42,1.09-6.73,1.64-9.92,1.64Zm-12-34.24h19.52c0-2.98-.71-5.43-2.13-7.36-1.42-1.93-3.73-2.89-6.92-2.89-2.47,0-4.69.86-6.65,2.56-1.96,1.71-3.24,4.27-3.82,7.69Z"></path>
                             <path class="cls-1" d="m502.62,98.08v-54.09h13.09l1.09,6.98h.44c2.25-2.25,4.65-4.2,7.2-5.83,2.54-1.64,5.6-2.45,9.16-2.45,3.85,0,6.96.78,9.32,2.34,2.36,1.56,4.23,3.8,5.62,6.71,2.4-2.47,4.94-4.6,7.63-6.38,2.69-1.78,5.82-2.67,9.38-2.67,5.82,0,10.09,1.95,12.81,5.83,2.73,3.89,4.09,9.21,4.09,15.98v33.59h-16.03v-31.52c0-3.93-.53-6.61-1.58-8.07-1.05-1.45-2.74-2.18-5.07-2.18-2.69,0-5.78,1.74-9.27,5.23v36.53h-16.03v-31.52c0-3.93-.53-6.61-1.58-8.07-1.05-1.45-2.74-2.18-5.07-2.18-2.69,0-5.74,1.74-9.16,5.23v36.53h-16.03Z"></path>
                             <path class="cls-1" d="m602.07,119.23c-1.6,0-3-.11-4.2-.33-1.2-.22-2.34-.47-3.44-.76l2.84-12.21c.51.07,1.09.2,1.74.38.65.18,1.27.27,1.85.27,2.69,0,4.76-.65,6.22-1.96,1.45-1.31,2.54-3.02,3.27-5.13l.76-2.84-20.83-52.67h16.14l7.74,23.23c.8,2.47,1.53,4.98,2.18,7.52.65,2.55,1.34,5.16,2.07,7.85h.44c.58-2.54,1.18-5.11,1.8-7.69.62-2.58,1.25-5.14,1.91-7.69l6.54-23.23h15.38l-18.76,54.63c-1.67,4.51-3.53,8.31-5.56,11.4-2.04,3.09-4.49,5.4-7.36,6.92-2.87,1.53-6.45,2.29-10.74,2.29Z"></path>
                           </g>
                         </g>
                       </g>
                     </g>
                   </svg>
               </div>
               <div class="f-md-50 f-28 w700 white-clr lh150 col-12 mt-md30 mt20 text-center">
                  Aicademy <span class="yellow-clr">Agency Upgrade</span>
               </div>
               <div class="col-12 col-md-7 mt20 mt-md70">
                  <img src="assets/images/product-image.webp" class="img-fluid d-block mx-auto margin-bottom-15">
               </div>
               <div class="col-md-5 col-12 mt20 mt-md70">
                  <div class="proudly-list-bg">
                     <ul class="proudly-tick pl0 m0 f-18 w500 black-clr lh150 text-capitalize">
                        <li><span class="w600">Build your Pro Academy with an In-built</span> Marketplace, Courses, Members Area, Lead Management, and Help Desk</li>
                        <li><span class="w600">Create Beautiful and Proven Converting</span> E-Learning Sites 
                        </li>
                        <li><span class="w600">Preloaded with 400 HD Video Courses and 30 DFY Sales Funnels </span>
                           to Start Selling Instantly and Keep 100% of Profit</li>
                        <li><span class="w600">Don’t Lose Traffic, Leads, and Profit </span>with Any 3rd Party Marketplace</li>
                        <li><span class="w600">Commercial License Included</span> </li>
                        <li><span class="w600">50+ More Features</span></li>
                     </ul>
                  </div>
               </div>
               <div class="col-md-9 mx-auto col-12 mt30 mt-md65">
                  <div class="f-18 f-md-36 text-center probtn1" editabletype="button" style="z-index: 10;">
                     <a href="#buynow" class="text-center">Upgrade to Aicademy Agency Now</a>
                  </div>
                  <br class="visible-md">
               </div>
            </div>
         </div>
      </div>
      <!--4. Fourth Section End -->
      <div class="thats-not-all-section">
         <div class="container">
            <div class="row">
               <div class="f-md-45 f-28 w700 lh140 text-center black-clr">
                  With Aicademy Agency Upgrade, You Can Tap Into The Very <span class="w700 kapblue">Fast-Growing Freelancing Industry.</span>
               </div>
               <div class="col-12 col-md-12  mt20 mt-md60">
                  <div class="row d-flex align-items-center flex-wrap">
                     <div class="col-12 col-md-7">
                        <div class="w400 f-20 f-md-22 black-clr text-xs-center lh140 mt20 mt-md0">
                           <span class="w700">Start Your Own Profitable Freelancing services or Agency From Your Home</span> and Tap into 250 Billion Industry.
                        </div>
                     </div>
                     <div class="col-12 col-md-5 mt20 mt-md0">
                        <div editabletype="image" style="z-index:10;">
                           <img src="assets/images/freelancing-img.webp" class="img-fluid mx-auto d-block">
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12">
                  <div class="f-md-45 f-28 w700 lh150 text-center black-clr mt20 mt-md50 text-capitalize">
                     See how much freelancers are charging just to create basic academy websites
                  </div>
                  <div class="mt10 mt-md15" editabletype="image" style="z-index:10;">
                     <img src="assets/images/sales-funnels-img.webp" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-18 f-md-20 w400 lh150 black-clr text-xs-center mt15">
                     <span class="w500">With Aicademy agency not only you can find clients in your local area and sell using a proven funnel using Aicademy, but also you can find them on websites like Fiverr, Freelancer, Odesk, Upwork where tons of businesses are looking for a legitimate service provider. 
                  </div>
               </div>
               <div class="col-12 text-center mt30 mt-md80">
                  <div class="one-time-shape" editabletype="shape" style="z-index: 8;">
                     <div class="f-24 f-md-36 w600 text-center lh120 white-clr text-center">And it’s not a 1-Time Income</div>
                  </div>
               </div>
               <div class="col-12  f-30 f-md-70 w600 lh120 black-clr text-center mt30 mt-md5">534,000 </div>
               <div class="col-12 f-18 f-md-20 w400 lh150 text-center black-clr mt30 mt-md30">
                  New Businesses Starts Every Month. You can do this for life and Charge Your New as well as Existing Clients for Your Services again, and again and again forever
               </div>
               <div class="col-12 f-18 f-md-20 w400 lh150 text-center black-clr mt10">
                  <span class="w700">Yes, Tons of Businesses Are Looking for A Solution...</span>
                  <br class="visible-md">
                  <span class="w500">Absolutely No Limitations!</span>
               </div>
            </div>
         </div>
      </div>
      <div class="amazing-software-section1">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-12">
                  <div class="f-md-40 f-28 w700 lh150 text-capitalize text-center black-clr">
                     Nothing to Host, Build or Manage Support... Setup A Complete <span class="w700 kapblue">'Done For You' System in 3 Simple Steps</span>
                  </div>
               </div>
               <!-- feature 1 -->
               <div class="col-12 col-md-12  mt20 mt-md60 d-flex align-items-center flex-wrap">
                  <div class="col-12 col-md-6 order-md-1">
                     <div>
                        <img src="../common_assets/images/1.webp" class="img-responsive">
                     </div>
                     <div class="f-24 f-md-36 w600 lh150 text-left mt10">
                        Add a new client’s business in 15 Minutes
                     </div>
                     <div class="f-18 f-md-20 w400 lh150 mt10">
                        All you need to do is add a new client’s business, insert details from where you want to fetch trendy content & videos, & you're all set.
                        <br><br> ZERO Marketing Experience Needed! We did All the HARD Part!
                     </div>
                  </div>
                  <div class="col-12 col-md-6  order-md-6 mt20 mt-md50">
                     <img src="assets/images/step1.webp" class="img-fluid mx-auto d-block imahe-h-250">
                  </div>
               </div>
               <!-- feature 2 -->
               <div class="col-12 col-md-12  mt20 mt-md60 d-flex align-items-center flex-wrap">
                  <div class="col-12 col-md-6 px0">
                     <div class="mt-md30">
                        <img src="../common_assets/images/2.webp" class="img-responsive">
                     </div>
                     <div class="f-24 f-md-36 w600 lh150 text-left mt10">
                        Accept the Monthly Payments & Keep 100% Profits
                     </div>
                     <div class="f-18 f-md-20 w400 lh150 mt10">
                        Charge them monthly, yearly or one-time high fee for providing them high-in-demand services & Keep 100% profits with you.
                     </div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md50 p-md0">
                     <img src="assets/images/step2.webp" class="img-fluid mx-auto d-block imahe-h-250">
                  </div>
               </div>
               <!-- feature 3 -->
               <div class="col-12 col-md-12  mt20 mt-md60 d-flex align-items-center flex-wrap">
                  <div class="col-12 col-md-6 order-md-1">
                     <div class="mt-md30">
                        <img src="../common_assets/images/3.webp" class="img-responsive">
                     </div>
                     <div class="f-24 f-md-36 w600 lh150 text-left mt10">
                        We did all the Hard work.
                     </div>
                     <div class="f-18 f-md-20 w400 lh150 w400 mt10">
                        We invested a lot of money to get this revolutionary technology ready. And you know what the best part is, we will take care of all the customer support about your and your client’s Aicademy account queries. You don’t even need to lift a finger…
                     </div>
                  </div>
                  <div class="col-12 col-md-6 order-md-6 mt20 mt-md0">
                     <img src="assets/images/step3.webp" class="img-fluid mx-auto d-block imahe-h-300">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- feature1 -->
      <div class="white-section">
         <div class="container">
            <div class="row">
               <div class="f-md-45 f-28 w700 lh150 text-center black-clr mb-md0 mb20">
                  Here’ What You are Getting with <br class="d-lg-block d-none"><span class="w700">This Agency Upgrade Today</span>
               </div>
               <div class="col-12 col-md-12 mt30 mt-md50">
                  <div>
                     <img src="../common_assets/images/1.webp" class="img-responsive">
                  </div>
                  <div class="col-12 col-md-12 f-24 f-md-36 w500 lh140 mt10 mt-md10 text-left black-clr">
                     A Top-Notch Software, Aicademy With Agency License To Provide Academy Website Creation Services To Your Clients.
                  </div>
                  <div class="col-12 col-md-6 mx-auto mt20 mt-md30">
                     <img src="assets/images/feature1.webp" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt10 mt-md30 text-left col-12">
                     Providing high-in-demand academy website creation services couldn’t get easier than this. You’re getting the never offered before Aicademy Agency License to provide these services to your clients with DFY support for our software. This is something that
                     will get you way above your competitors.
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--feature1 -->
      <!--feature3 -->
      <div class="grey-section">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-12">
                  <div>
                     <img src="../common_assets/images/2.webp" class="img-responsive">
                  </div>
                  <div class="col-md-12 col-12 f-24 f-md-36 w500 lh140 mt10 mt-md10 text-left black-clr pl0">
                     Done-For-Your Business Management Panel
                  </div>
                  <div class="col-12 col-md-10 mx-auto mt20 mt-md30">
                     <img src="assets/images/bussiness-manage.webp" class="img-fluid mx-auto d-block">
                  </div>
                  <div class="f-18 f-md-20 w400 lh140 mt15 mt-md30 mt10 mt-md60 text-left col-12">
                     Oh yeah! You can manage all your client’s businesses from a single dashboard to have full control. Simply switch between the businesses or copy-paste a proven funnel or high converting landing page within few clicks.
                     <span class="w600">It’s EASY & FAST.</span><br><br> You also can add your client to their business as an admin & Impress them by giving them a separate business dashboard with their personal login details to let them check
                     their business performance live
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--feature7 -->
      <div class="white-section">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-12">
                  <div>
                     <img src="../common_assets/images/3.webp" class="img-responsive">
                  </div>
                  <div class="col-md-12 col-12 f-24 f-md-36 w500 lh140 mt10 mt-md10 text-left black-clr ">
                     Add Unlimited team members – Inhouse & freelancers
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto">
                  <div class="mt20 mt-md50">
                     <img src="assets/images/unlimited-clients.webp" class="img-fluid mx-auto d-block">
                  </div>
               </div>
               <div class="col-12 col-md-12">
                  <div class="f-18 f-md-20 w400 lh140 mt10 mt-md40 text-left col-12 ">
                     You can assign them limited or full access to features according to their role with separate login details to outsource your manual work.
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="grey-section">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-12">
                  <div>
                     <img src="../common_assets/images/4.webp" class="img-responsive">
                  </div>
                  <div class="col-md-12 col-12 f-24 f-md-36 w500 lh140 mt10 mt-md10 text-left black-clr ">
                     Serve Unlimited Clients with Agency License
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto">
                  <div class="mt20 mt-md50">
                     <img src="assets/images/team-manage.webp" class="img-fluid mx-auto d-block">
                  </div>
               </div>
               <div class="col-12 col-md-12">
                  <div class="f-18 f-md-20 w400 lh140 mt10 mt-md40 text-left">
                     <span class="w600">Aicademy Agency</span> gives you the complete power to go beyond the normal and have a passive income source by serving unlimited clients in a hands down manner
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="white-section">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-12">
                  <div>
                     <img src="../common_assets/images/5.webp" class="img-responsive">
                  </div>
                  <div class="col-md-12 col-12 f-24 f-md-36 w500 lh140 mt10 mt-md10 text-left black-clr ">
                     Get All These Benefits For One Time Price
                  </div>
               </div>
               <div class="col-12 col-md-5 order-md-1">
                  <div class="mt20 mt-md20">
                     <img src="assets/images/benefit-bg.webp" class="img-fluid mx-auto d-block">
                  </div>
               </div>
               <div class="col-12 col-md-7 order-md-6 mt-md10 mt0">
                  <div class="f-18 f-md-20 w400 lh140 mt20 mt-md80">
                     And here's the best part. When you get access to <span class="w600">Aicademy Agency,</span> you can get all these benefits by paying just a small one time price. Now that's something you can't afford to miss out on
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--money back Start -->
      <div class="moneyback-section">
         <div class="container">
            <div class="row align-items-center d-flex flex-wrap">
               <div class="col-12 mb-md30">
                  <div class="f-28 f-md-45 lh140 w700 text-center white-clr">
                     30 Days Money Back Guarantee
                  </div>
               </div>
               <div class="col-md-4 col-12 mt30 mt-md40">
                  <img src="assets/images/moneyback.webp" class="img-fluid mx-auto d-block">
               </div>
               <div class="col-md-8 col-12 mt30">
                  <div class="f-18 f-md-20 w400 lh140 white-clr">
                     We have absolutely no doubt that you'll love the extra benefits, training and Aicademy Agency upgraded features.
                     <br>
                     <br> You can try it out without any risk. If, for any reason, you’re not satisfied with your Aicademy Agency upgrade you can simply ask for a refund. Your investment is covered by my 30-Days money back guarantee.
                     <br>
                     <br> You have absolutely nothing to lose and everything to gain with this fantastic offer and guarantee.
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--money back End-->
      <!---Bonus Section Starts-->
      <div class="bonus-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="black-design">
                     <div class="f-28 f-md-50 w700 lh140 text-center white-clr skew-normal">
                        But That’s Not All
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 f-20 f-md-24 w400 text-center lh140">
                  In addition, we have a number of bonuses for those who want to take action today and start profiting from this opportunity
               </div>
               <!-- bonus1 -->
               <div class="col-12 col-md-12  mt20 mt-md60 d-flex align-items-center flex-wrap">
                  <div class="col-12 col-md-6 px-md15">
                     <div class="f-md-36 f-24 w700 blue-clr text-nowrap">
                        Bonus 1
                     </div>
                     <div class="f-24 f-md-30 w500 lh140 mt20 mt-md20 text-left black-clr">
                        Best Marketing Strategies
                     </div>
                     <div class="f-18 f-md-20 w400 lh140 mt10 mt-md10 text-left">
                        This useful package that includes source Ebook document, 25 PLR articles, Product Analysis PDF and fast action ideas PDF is something that can scale your marketing agency. Use it without fail with Aicademy Agency upgrade, and see results like you always
                        wanted.
                     </div>
                  </div>
                  <div class="col-12 col-md-6">
                     <div class="mt20 mt-md0">
                        <img src="assets/images/bonusbox.webp" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>
               <!-- bonus2 -->
               <div class="col-12 col-md-12  mt20 mt-md60 d-flex align-items-center flex-wrap">
                  <div class="col-12 col-md-6 order-md-1">
                     <div class="f-md-36 f-24 w700 blue-clr text-nowrap">
                        Bonus 2
                     </div>
                     <div class="f-24 f-md-30 w500 lh140 mt20 mt-md20 text-left black-clr">
                        Outsourcing Fundamentals Development and Strategy
                     </div>
                     <div class="f-18 f-md-20 w400 lh140 mt10 mt-md10 text-left">
                        This internet marketing report based on outsourcing fundamentals development and strategy includes blog posts, forums, YouTube videos and other related stuff for your business growth. Use it with the immense powers of Aicademy Agency Upgrade to get results
                        that your competitors envy.
                     </div>
                  </div>
                  <div class="col-12 col-md-6 order-md-6">
                     <div class="mt20 mt-md50">
                        <img src="assets/images/bonusbox.webp" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>
               <!-- bonus3 -->
               <div class="col-12 col-md-12  mt20 mt-md60 d-flex align-items-center flex-wrap">
                  <div class="col-12 col-md-6 px-md15">
                     <div class="f-md-36 f-24 w700 blue-clr text-nowrap">
                        Bonus 3
                     </div>
                     <div class="f-24 f-md-30 w500 lh140 mt20 mt-md20 text-left black-clr">
                        Success in Business
                     </div>
                     <div class="f-18 f-md-20 w400 lh140 mt10 mt-md10 text-left">
                        Wondering how to dominate your competition with success in business PLR content package, you’re at the right place. This Package includes source Ebook document, 25 PLR articles, product analysis PDF and a fast action ideas PDF. Don’t spend time thinking,
                        just use this with Aicademy Agency Upgrade and scale your business to new heights.
                     </div>
                  </div>
                  <div class="col-12 col-md-6">
                     <div class="mt20 mt-md0">
                        <img src="assets/images/bonusbox.webp" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>
               <!-- bonus4 -->
               <div class="col-12 col-md-12  mt40 mt-md60 d-flex align-items-center flex-wrap">
                  <div class="col-12 col-md-6 order-md-1">
                     <div class="f-md-36 f-24 w700 blue-clr text-nowrap">
                        Bonus 4
                     </div>
                     <div class="f-24 f-md-30 w500 lh140 mt20 mt-md20 text-left black-clr">
                        The Copywriter's Handbook
                     </div>
                     <div class="f-18 f-md-20 w400 lh140 mt10 mt-md10 text-left">
                        Get your hands on proven tips and tricks used by world’s top marketers and learn why they became such envious success stories. Use this information with Aicademy Agency Upgrade, and see yourself on the road to online marketing success.
                     </div>
                  </div>
                  <div class="col-12 col-md-6 order-md-6 mt-md30">
                     <div class="mt20 mt-md0">
                        <img src="assets/images/bonusbox.webp" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>
               <!-- bonus5 -->
               <div class="col-12 col-md-12  mt20 mt-md60 d-flex align-items-center flex-wrap">
                  <div class="col-12 col-md-6 px-md15">
                     <div class="f-md-36 f-24 w700 blue-clr text-nowrap">
                        Bonus 5
                     </div>
                     <div class="f-24 f-md-30 w500 lh140 mt20 mt-md20 text-left black-clr">
                        Recognizing Target Markets
                     </div>
                     <div class="f-18 f-md-20 w400 lh140 mt10 mt-md10 text-left">
                        Venture into the right market and tap into tons of untapped clients that are right there to be grabbed. When combined with Aicademy Agency Upgrade powers, this package becomes a sure shot business booster.
                     </div>
                  </div>
                  <div class="col-12 col-md-6">
                     <div class="mt20 mt-md0">
                        <img src="assets/images/bonusbox.webp" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--Bonus Section ends -->
      <!--10. Table Section Starts -->
      <div class="table-section padding10" id="buynow">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-12">
                  <div class="f-md-45 f-28 w500 lh140 mt20 mt-md30 text-center">
                     Today You Can Get Unrestricted Access To <span class="w700">Aicademy Agency Suite For <span class="">LESS Than The Price Of Just One Month’s</span> Membership.</span>
                  </div>
               </div>
               <div class="col-md-6 col-12  mt30 mt-md-90">
                  <div class="tablebox2" editabletype="shape" style="z-index: 8;">
                     <div class="tbbg2 text-center">
                        <div>
                           <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 644.49 120" style="max-height:55px">
                              <defs>
                                <style>
                                  .cls-1s {
                                    fill: #051e3a;
                                  }
                            
                                  .cls-2s {
                                    fill: #0af;
                                  }
                                </style>
                              </defs>
                              <g id="Layer_1-2" data-name="Layer 1">
                                <g>
                                  <g>
                                    <circle class="cls-2s" cx="79.02" cy="43.5" r="7.25"/>
                                    <circle class="cls-2s" cx="70.47" cy="78.46" r="5.46"/>
                                    <path class="cls-2" d="m158.03,45.28l-33.74,13.31v36.4l-45.27,16.86-27.9-10.4c-4.17,3.37-7.04,6.74-8.15,10.11-1.01,3.22-.07,6.21,2.46,8.42-5.44-1.46-8-5.25-6.96-9.76,1.24-3.62,4.79-7.68,10-12.08h.01c3.95-3.31,8.84-6.84,14.46-10.5,2.04,1.67,4.68,2.68,7.53,2.68,6.56,0,11.88-5.32,11.88-11.88s-5.32-11.88-11.88-11.88-11.88,5.32-11.88,11.88c0,1.13.15,2.21.46,3.25-8.01,5.01-14.83,10.36-19.57,15.44l-5.75-2.14v-7.66c-7.84,4.92-11.84,11.06-5.99,16.99-5.17-1.64-7.69-4.49-7.69-8.05,0-4.83,4.67-10.99,13.69-17.26l35.41-23.23c2.71,2.18,6.13,3.48,9.87,3.48,8.7,0,15.76-7.06,15.76-15.76s-7.06-15.76-15.76-15.76-15.76,7.06-15.76,15.76c0,1.59.23,3.12.68,4.57l-30.2,19.53v-9L0,45.28,79.02,0l79.01,45.28Z"/>
                                    <path class="cls-2s" d="m149.38,68.32v-19.62l-1.51.6v19.03c-1.66.37-2.9,1.96-2.9,3.85v11.69h7.31v-11.69c0-1.9-1.24-3.48-2.9-3.85Z"/>
                                  </g>
                                  <g>
                                    <g>
                                      <path class="cls-2s" d="m178.03,98.08l22.25-71.1h19.3l22.25,71.1h-17.01l-4.36-16.9h-21.7l-4.36,16.9h-16.36Zm25.74-35.99l-1.74,6.54h15.16l-1.64-6.54c-.95-3.56-1.91-7.34-2.89-11.34-.98-4-1.95-7.85-2.89-11.56h-.44c-.87,3.78-1.76,7.65-2.67,11.61-.91,3.96-1.87,7.73-2.89,11.29Z"/>
                                      <path class="cls-2s" d="m256.22,36.03c-2.69,0-4.87-.76-6.54-2.29-1.67-1.53-2.51-3.56-2.51-6.11s.83-4.58,2.51-6.11c1.67-1.53,3.85-2.29,6.54-2.29s4.87.76,6.54,2.29c1.67,1.53,2.51,3.56,2.51,6.11s-.84,4.58-2.51,6.11c-1.67,1.53-3.85,2.29-6.54,2.29Zm-7.96,62.05v-54.09h16.03v54.09h-16.03Z"/>
                                    </g>
                                    <g>
                                      <path class="cls-1s" d="m299.89,99.38c-5.02,0-9.54-1.11-13.58-3.33-4.03-2.22-7.23-5.45-9.6-9.71-2.36-4.25-3.54-9.36-3.54-15.32s1.31-11.16,3.93-15.38c2.62-4.22,6.07-7.43,10.36-9.65,4.29-2.22,8.9-3.33,13.85-3.33,3.34,0,6.31.55,8.89,1.64,2.58,1.09,4.89,2.47,6.92,4.14l-7.52,10.36c-2.55-2.11-4.98-3.16-7.31-3.16-3.85,0-6.92,1.38-9.21,4.14-2.29,2.76-3.44,6.51-3.44,11.23s1.15,8.38,3.44,11.18c2.29,2.8,5.18,4.2,8.67,4.2,1.74,0,3.45-.38,5.13-1.15,1.67-.76,3.2-1.69,4.58-2.78l6.33,10.47c-2.69,2.33-5.6,3.98-8.72,4.96-3.13.98-6.18,1.47-9.16,1.47Z"/>
                                      <path class="cls-1s" d="m339.81,99.38c-4.94,0-8.87-1.58-11.78-4.74-2.91-3.16-4.36-7.03-4.36-11.61,0-5.67,2.4-10.1,7.2-13.3,4.8-3.2,12.54-5.34,23.23-6.43-.15-2.4-.86-4.31-2.13-5.73-1.27-1.42-3.4-2.13-6.38-2.13-2.25,0-4.54.44-6.87,1.31-2.33.87-4.8,2.07-7.42,3.6l-5.78-10.58c3.42-2.11,7.07-3.82,10.96-5.13,3.89-1.31,7.94-1.96,12.16-1.96,6.91,0,12.21,2,15.92,6,3.71,4,5.56,10.14,5.56,18.43v30.97h-13.09l-1.09-5.56h-.44c-2.25,2.04-4.67,3.69-7.25,4.96-2.58,1.27-5.4,1.91-8.45,1.91Zm5.45-12.43c1.82,0,3.4-.42,4.74-1.25,1.34-.83,2.71-1.94,4.09-3.33v-9.49c-5.67.73-9.6,1.87-11.78,3.44-2.18,1.56-3.27,3.4-3.27,5.51,0,1.74.56,3.04,1.69,3.87,1.13.84,2.63,1.25,4.53,1.25Z"/>
                                      <path class="cls-1s" d="m402.95,99.38c-6.69,0-12.05-2.53-16.09-7.58-4.03-5.05-6.05-11.98-6.05-20.77,0-5.89,1.07-10.96,3.22-15.21,2.14-4.25,4.94-7.51,8.4-9.76,3.45-2.25,7.07-3.38,10.85-3.38,2.98,0,5.49.51,7.52,1.53,2.04,1.02,3.96,2.4,5.78,4.14l-.66-8.29v-18.43h16.03v76.45h-13.09l-1.09-5.34h-.44c-1.89,1.89-4.11,3.47-6.65,4.74-2.55,1.27-5.13,1.91-7.74,1.91Zm4.14-13.09c1.74,0,3.33-.36,4.74-1.09,1.42-.73,2.78-2,4.09-3.82v-22.14c-1.38-1.31-2.85-2.22-4.42-2.73-1.56-.51-3.07-.76-4.53-.76-2.55,0-4.8,1.22-6.76,3.65-1.96,2.44-2.94,6.23-2.94,11.4s.85,9.21,2.56,11.72c1.71,2.51,4.13,3.76,7.25,3.76Z"/>
                                      <path class="cls-1s" d="m470.34,99.38c-5.16,0-9.81-1.13-13.96-3.38-4.14-2.25-7.42-5.49-9.81-9.71-2.4-4.22-3.6-9.31-3.6-15.27s1.22-10.94,3.65-15.16c2.43-4.22,5.62-7.47,9.54-9.76,3.93-2.29,8.03-3.44,12.32-3.44,5.16,0,9.43,1.15,12.81,3.44,3.38,2.29,5.92,5.38,7.63,9.27,1.71,3.89,2.56,8.31,2.56,13.25,0,1.38-.07,2.74-.22,4.09-.15,1.35-.29,2.34-.44,3h-32.39c.73,3.93,2.36,6.82,4.91,8.67,2.54,1.85,5.6,2.78,9.16,2.78,3.85,0,7.74-1.2,11.67-3.6l5.34,9.71c-2.76,1.89-5.85,3.38-9.27,4.47-3.42,1.09-6.73,1.64-9.92,1.64Zm-12-34.24h19.52c0-2.98-.71-5.43-2.13-7.36-1.42-1.93-3.73-2.89-6.92-2.89-2.47,0-4.69.86-6.65,2.56-1.96,1.71-3.24,4.27-3.82,7.69Z"/>
                                      <path class="cls-1s" d="m502.62,98.08v-54.09h13.09l1.09,6.98h.44c2.25-2.25,4.65-4.2,7.2-5.83,2.54-1.64,5.6-2.45,9.16-2.45,3.85,0,6.96.78,9.32,2.34,2.36,1.56,4.23,3.8,5.62,6.71,2.4-2.47,4.94-4.6,7.63-6.38,2.69-1.78,5.82-2.67,9.38-2.67,5.82,0,10.09,1.95,12.81,5.83,2.73,3.89,4.09,9.21,4.09,15.98v33.59h-16.03v-31.52c0-3.93-.53-6.61-1.58-8.07-1.05-1.45-2.74-2.18-5.07-2.18-2.69,0-5.78,1.74-9.27,5.23v36.53h-16.03v-31.52c0-3.93-.53-6.61-1.58-8.07-1.05-1.45-2.74-2.18-5.07-2.18-2.69,0-5.74,1.74-9.16,5.23v36.53h-16.03Z"/>
                                      <path class="cls-1s" d="m602.07,119.23c-1.6,0-3-.11-4.2-.33-1.2-.22-2.34-.47-3.44-.76l2.84-12.21c.51.07,1.09.2,1.74.38.65.18,1.27.27,1.85.27,2.69,0,4.76-.65,6.22-1.96,1.45-1.31,2.54-3.02,3.27-5.13l.76-2.84-20.83-52.67h16.14l7.74,23.23c.8,2.47,1.53,4.98,2.18,7.52.65,2.55,1.34,5.16,2.07,7.85h.44c.58-2.54,1.18-5.11,1.8-7.69.62-2.58,1.25-5.14,1.91-7.69l6.54-23.23h15.38l-18.76,54.63c-1.67,4.51-3.53,8.31-5.56,11.4-2.04,3.09-4.49,5.4-7.36,6.92-2.87,1.53-6.45,2.29-10.74,2.29Z"/>
                                    </g>
                                  </g>
                                </g>
                              </g>
                            </svg>
                        </div>
                        <div class="text-uppercase mt15 mt-md15 w600 f-md-30 f-22 text-center black-clr lh120">Agency 100 Client Plan</div>
                     </div>
                     <div>
                        <ul class="f-18 w400 lh140 text-center vgreytick mb0">
                           <li>Directly Sell Aicademy Services and Charge Monthly or Recurring Amount For 100% Profits</li>
                           <li>Comes with Dedicated Dashboard to Create Accounts for the Customers in 3 Simple Clicks</li>
                           <li>Completely Cloud-Based Tool, so no additional domain or hosting required</li>
                           <li>Serve Up to 100 Clients with Agency License </li>
                           <li>Unparallel Price with No Recurring Fee</li>
                        </ul>
                     </div>
                     <div class="myfeatureslast f-md-25 f-16 w400 text-center lh150 hideme" editabletype="shape" style="opacity: 1; z-index: 9;">
                        <div class="f-md-26 f-20 w700 lh150 mb-md30 mb15 text-center">Only $67!</div>
                        <div class="" editabletype="button" style="z-index: 10;">
                           <a href="https://warriorplus.com/o2/buy/hb06pf/kwjv8b/c46szl"><img src="https://warriorplus.com/o2/btn/fn200011000/hb06pf/kwjv8b/353911" class="img-fluid mx-auto d-block"></a>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-6 col-12  mt30 mt-md-90">
                  <div class="tablebox3" editabletype="shape" style="z-index: 8;">
                     <div class="tbbg3 text-center">
                        <div>
                           <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 644.49 120" style="max-height:55px">
                              <defs>
                                <style>
                                  .cls-1 {
                                    fill: #fff;
                                  }
                            
                                  .cls-2 {
                                    fill: #0af;
                                  }
                                </style>
                              </defs>
                              <g id="Layer_1-2" data-name="Layer 1">
                                <g>
                                  <g>
                                    <circle class="cls-2" cx="79.02" cy="43.5" r="7.25"></circle>
                                    <circle class="cls-2" cx="70.47" cy="78.46" r="5.46"></circle>
                                    <path class="cls-2" d="m158.03,45.28l-33.74,13.31v36.4l-45.27,16.86-27.9-10.4c-4.17,3.37-7.04,6.74-8.15,10.11-1.01,3.22-.07,6.21,2.46,8.42-5.44-1.46-8-5.25-6.96-9.76,1.24-3.62,4.79-7.68,10-12.08h.01c3.95-3.31,8.84-6.84,14.46-10.5,2.04,1.67,4.68,2.68,7.53,2.68,6.56,0,11.88-5.32,11.88-11.88s-5.32-11.88-11.88-11.88-11.88,5.32-11.88,11.88c0,1.13.15,2.21.46,3.25-8.01,5.01-14.83,10.36-19.57,15.44l-5.75-2.14v-7.66c-7.84,4.92-11.84,11.06-5.99,16.99-5.17-1.64-7.69-4.49-7.69-8.05,0-4.83,4.67-10.99,13.69-17.26l35.41-23.23c2.71,2.18,6.13,3.48,9.87,3.48,8.7,0,15.76-7.06,15.76-15.76s-7.06-15.76-15.76-15.76-15.76,7.06-15.76,15.76c0,1.59.23,3.12.68,4.57l-30.2,19.53v-9L0,45.28,79.02,0l79.01,45.28Z"></path>
                                    <path class="cls-2" d="m149.38,68.32v-19.62l-1.51.6v19.03c-1.66.37-2.9,1.96-2.9,3.85v11.69h7.31v-11.69c0-1.9-1.24-3.48-2.9-3.85Z"></path>
                                  </g>
                                  <g>
                                    <g>
                                      <path class="cls-2" d="m178.03,98.08l22.25-71.1h19.3l22.25,71.1h-17.01l-4.36-16.9h-21.7l-4.36,16.9h-16.36Zm25.74-35.99l-1.74,6.54h15.16l-1.64-6.54c-.95-3.56-1.91-7.34-2.89-11.34-.98-4-1.95-7.85-2.89-11.56h-.44c-.87,3.78-1.76,7.65-2.67,11.61-.91,3.96-1.87,7.73-2.89,11.29Z"></path>
                                      <path class="cls-2" d="m256.22,36.03c-2.69,0-4.87-.76-6.54-2.29-1.67-1.53-2.51-3.56-2.51-6.11s.83-4.58,2.51-6.11c1.67-1.53,3.85-2.29,6.54-2.29s4.87.76,6.54,2.29c1.67,1.53,2.51,3.56,2.51,6.11s-.84,4.58-2.51,6.11c-1.67,1.53-3.85,2.29-6.54,2.29Zm-7.96,62.05v-54.09h16.03v54.09h-16.03Z"></path>
                                    </g>
                                    <g>
                                      <path class="cls-1" d="m299.89,99.38c-5.02,0-9.54-1.11-13.58-3.33-4.03-2.22-7.23-5.45-9.6-9.71-2.36-4.25-3.54-9.36-3.54-15.32s1.31-11.16,3.93-15.38c2.62-4.22,6.07-7.43,10.36-9.65,4.29-2.22,8.9-3.33,13.85-3.33,3.34,0,6.31.55,8.89,1.64,2.58,1.09,4.89,2.47,6.92,4.14l-7.52,10.36c-2.55-2.11-4.98-3.16-7.31-3.16-3.85,0-6.92,1.38-9.21,4.14-2.29,2.76-3.44,6.51-3.44,11.23s1.15,8.38,3.44,11.18c2.29,2.8,5.18,4.2,8.67,4.2,1.74,0,3.45-.38,5.13-1.15,1.67-.76,3.2-1.69,4.58-2.78l6.33,10.47c-2.69,2.33-5.6,3.98-8.72,4.96-3.13.98-6.18,1.47-9.16,1.47Z"></path>
                                      <path class="cls-1" d="m339.81,99.38c-4.94,0-8.87-1.58-11.78-4.74-2.91-3.16-4.36-7.03-4.36-11.61,0-5.67,2.4-10.1,7.2-13.3,4.8-3.2,12.54-5.34,23.23-6.43-.15-2.4-.86-4.31-2.13-5.73-1.27-1.42-3.4-2.13-6.38-2.13-2.25,0-4.54.44-6.87,1.31-2.33.87-4.8,2.07-7.42,3.6l-5.78-10.58c3.42-2.11,7.07-3.82,10.96-5.13,3.89-1.31,7.94-1.96,12.16-1.96,6.91,0,12.21,2,15.92,6,3.71,4,5.56,10.14,5.56,18.43v30.97h-13.09l-1.09-5.56h-.44c-2.25,2.04-4.67,3.69-7.25,4.96-2.58,1.27-5.4,1.91-8.45,1.91Zm5.45-12.43c1.82,0,3.4-.42,4.74-1.25,1.34-.83,2.71-1.94,4.09-3.33v-9.49c-5.67.73-9.6,1.87-11.78,3.44-2.18,1.56-3.27,3.4-3.27,5.51,0,1.74.56,3.04,1.69,3.87,1.13.84,2.63,1.25,4.53,1.25Z"></path>
                                      <path class="cls-1" d="m402.95,99.38c-6.69,0-12.05-2.53-16.09-7.58-4.03-5.05-6.05-11.98-6.05-20.77,0-5.89,1.07-10.96,3.22-15.21,2.14-4.25,4.94-7.51,8.4-9.76,3.45-2.25,7.07-3.38,10.85-3.38,2.98,0,5.49.51,7.52,1.53,2.04,1.02,3.96,2.4,5.78,4.14l-.66-8.29v-18.43h16.03v76.45h-13.09l-1.09-5.34h-.44c-1.89,1.89-4.11,3.47-6.65,4.74-2.55,1.27-5.13,1.91-7.74,1.91Zm4.14-13.09c1.74,0,3.33-.36,4.74-1.09,1.42-.73,2.78-2,4.09-3.82v-22.14c-1.38-1.31-2.85-2.22-4.42-2.73-1.56-.51-3.07-.76-4.53-.76-2.55,0-4.8,1.22-6.76,3.65-1.96,2.44-2.94,6.23-2.94,11.4s.85,9.21,2.56,11.72c1.71,2.51,4.13,3.76,7.25,3.76Z"></path>
                                      <path class="cls-1" d="m470.34,99.38c-5.16,0-9.81-1.13-13.96-3.38-4.14-2.25-7.42-5.49-9.81-9.71-2.4-4.22-3.6-9.31-3.6-15.27s1.22-10.94,3.65-15.16c2.43-4.22,5.62-7.47,9.54-9.76,3.93-2.29,8.03-3.44,12.32-3.44,5.16,0,9.43,1.15,12.81,3.44,3.38,2.29,5.92,5.38,7.63,9.27,1.71,3.89,2.56,8.31,2.56,13.25,0,1.38-.07,2.74-.22,4.09-.15,1.35-.29,2.34-.44,3h-32.39c.73,3.93,2.36,6.82,4.91,8.67,2.54,1.85,5.6,2.78,9.16,2.78,3.85,0,7.74-1.2,11.67-3.6l5.34,9.71c-2.76,1.89-5.85,3.38-9.27,4.47-3.42,1.09-6.73,1.64-9.92,1.64Zm-12-34.24h19.52c0-2.98-.71-5.43-2.13-7.36-1.42-1.93-3.73-2.89-6.92-2.89-2.47,0-4.69.86-6.65,2.56-1.96,1.71-3.24,4.27-3.82,7.69Z"></path>
                                      <path class="cls-1" d="m502.62,98.08v-54.09h13.09l1.09,6.98h.44c2.25-2.25,4.65-4.2,7.2-5.83,2.54-1.64,5.6-2.45,9.16-2.45,3.85,0,6.96.78,9.32,2.34,2.36,1.56,4.23,3.8,5.62,6.71,2.4-2.47,4.94-4.6,7.63-6.38,2.69-1.78,5.82-2.67,9.38-2.67,5.82,0,10.09,1.95,12.81,5.83,2.73,3.89,4.09,9.21,4.09,15.98v33.59h-16.03v-31.52c0-3.93-.53-6.61-1.58-8.07-1.05-1.45-2.74-2.18-5.07-2.18-2.69,0-5.78,1.74-9.27,5.23v36.53h-16.03v-31.52c0-3.93-.53-6.61-1.58-8.07-1.05-1.45-2.74-2.18-5.07-2.18-2.69,0-5.74,1.74-9.16,5.23v36.53h-16.03Z"></path>
                                      <path class="cls-1" d="m602.07,119.23c-1.6,0-3-.11-4.2-.33-1.2-.22-2.34-.47-3.44-.76l2.84-12.21c.51.07,1.09.2,1.74.38.65.18,1.27.27,1.85.27,2.69,0,4.76-.65,6.22-1.96,1.45-1.31,2.54-3.02,3.27-5.13l.76-2.84-20.83-52.67h16.14l7.74,23.23c.8,2.47,1.53,4.98,2.18,7.52.65,2.55,1.34,5.16,2.07,7.85h.44c.58-2.54,1.18-5.11,1.8-7.69.62-2.58,1.25-5.14,1.91-7.69l6.54-23.23h15.38l-18.76,54.63c-1.67,4.51-3.53,8.31-5.56,11.4-2.04,3.09-4.49,5.4-7.36,6.92-2.87,1.53-6.45,2.29-10.74,2.29Z"></path>
                                    </g>
                                  </g>
                                </g>
                              </g>
                            </svg>
                        </div>
                        <div class="text-uppercase mt15 mt-md15 w600 f-md-30 f-22 text-center white-clr lh120">Agency Unlimited Client Plan</div>
                     </div>
                     <div>
                        <ul class="f-18 w400 lh140 text-center grey-tick-last mb0 white-clr">
                           <li>Directly Sell Aicademy Services and Charge Monthly or Recurring Amount For 100% Profits</li>
                           <li>Comes with Dedicated Dashboard to Create Accounts for the Customers in 3 Simple Clicks</li>
                           <li>Completely Cloud-Based Tool, so no additional domain or hosting required</li>
                           <li>Serve Unlimited Clients with Agency License </li>
                           <li>Unparallel Price with No Recurring Fee</li>
                        </ul>
                     </div>
                     <div class="myfeatureslast myfeatureslastborder f-md-25 f-16 w400 text-center lh150 hideme" editabletype="shape" style="opacity: 1; z-index: 9;">
                        <div class="f-md-26 f-20 w700 lh150 text-center black-clr mb-md30 mb15">Only $97!</div>
                        <div class="" editabletype="button" style="z-index: 10;">
                           <a href="https://warriorplus.com/o2/buy/hb06pf/kwjv8b/zv8y10"><img src="https://warriorplus.com/o2/btn/fn200011000/hb06pf/kwjv8b/353912" class="img-fluid mx-auto d-block"></a>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt40 mt-md50 text-left thanks-button text-center">
                  <a href="https://warriorplus.com/o/nothanks/kwjv8b" target="_blank" class="kapblue f-18 f-md-22 lh140 w400">
                  No thanks - I don't want to use the untapped POWER to setup my own pro agency without doing any extra work. I know that Aicademy Agency Upgrade can increase my profits & I duly realize that I won't get this offer again. Please take me to the next step to get access to my purchase. 
                  </a>
               </div>
            </div>
         </div>
      </div>
      <!--10. Table Section End -->
      <!--Footer Section Start -->
      <div class="footer-section">
         <div class="container ">
            <div class="row">
               <div class="col-12 text-center">
                  <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 644.49 120" style="max-height:55px">
                     <defs>
                       <style>
                         .cls-1 {
                           fill: #fff;
                         }
                   
                         .cls-2 {
                           fill: #0af;
                         }
                       </style>
                     </defs>
                     <g id="Layer_1-2" data-name="Layer 1">
                       <g>
                         <g>
                           <circle class="cls-2" cx="79.02" cy="43.5" r="7.25"></circle>
                           <circle class="cls-2" cx="70.47" cy="78.46" r="5.46"></circle>
                           <path class="cls-2" d="m158.03,45.28l-33.74,13.31v36.4l-45.27,16.86-27.9-10.4c-4.17,3.37-7.04,6.74-8.15,10.11-1.01,3.22-.07,6.21,2.46,8.42-5.44-1.46-8-5.25-6.96-9.76,1.24-3.62,4.79-7.68,10-12.08h.01c3.95-3.31,8.84-6.84,14.46-10.5,2.04,1.67,4.68,2.68,7.53,2.68,6.56,0,11.88-5.32,11.88-11.88s-5.32-11.88-11.88-11.88-11.88,5.32-11.88,11.88c0,1.13.15,2.21.46,3.25-8.01,5.01-14.83,10.36-19.57,15.44l-5.75-2.14v-7.66c-7.84,4.92-11.84,11.06-5.99,16.99-5.17-1.64-7.69-4.49-7.69-8.05,0-4.83,4.67-10.99,13.69-17.26l35.41-23.23c2.71,2.18,6.13,3.48,9.87,3.48,8.7,0,15.76-7.06,15.76-15.76s-7.06-15.76-15.76-15.76-15.76,7.06-15.76,15.76c0,1.59.23,3.12.68,4.57l-30.2,19.53v-9L0,45.28,79.02,0l79.01,45.28Z"></path>
                           <path class="cls-2" d="m149.38,68.32v-19.62l-1.51.6v19.03c-1.66.37-2.9,1.96-2.9,3.85v11.69h7.31v-11.69c0-1.9-1.24-3.48-2.9-3.85Z"></path>
                         </g>
                         <g>
                           <g>
                             <path class="cls-2" d="m178.03,98.08l22.25-71.1h19.3l22.25,71.1h-17.01l-4.36-16.9h-21.7l-4.36,16.9h-16.36Zm25.74-35.99l-1.74,6.54h15.16l-1.64-6.54c-.95-3.56-1.91-7.34-2.89-11.34-.98-4-1.95-7.85-2.89-11.56h-.44c-.87,3.78-1.76,7.65-2.67,11.61-.91,3.96-1.87,7.73-2.89,11.29Z"></path>
                             <path class="cls-2" d="m256.22,36.03c-2.69,0-4.87-.76-6.54-2.29-1.67-1.53-2.51-3.56-2.51-6.11s.83-4.58,2.51-6.11c1.67-1.53,3.85-2.29,6.54-2.29s4.87.76,6.54,2.29c1.67,1.53,2.51,3.56,2.51,6.11s-.84,4.58-2.51,6.11c-1.67,1.53-3.85,2.29-6.54,2.29Zm-7.96,62.05v-54.09h16.03v54.09h-16.03Z"></path>
                           </g>
                           <g>
                             <path class="cls-1" d="m299.89,99.38c-5.02,0-9.54-1.11-13.58-3.33-4.03-2.22-7.23-5.45-9.6-9.71-2.36-4.25-3.54-9.36-3.54-15.32s1.31-11.16,3.93-15.38c2.62-4.22,6.07-7.43,10.36-9.65,4.29-2.22,8.9-3.33,13.85-3.33,3.34,0,6.31.55,8.89,1.64,2.58,1.09,4.89,2.47,6.92,4.14l-7.52,10.36c-2.55-2.11-4.98-3.16-7.31-3.16-3.85,0-6.92,1.38-9.21,4.14-2.29,2.76-3.44,6.51-3.44,11.23s1.15,8.38,3.44,11.18c2.29,2.8,5.18,4.2,8.67,4.2,1.74,0,3.45-.38,5.13-1.15,1.67-.76,3.2-1.69,4.58-2.78l6.33,10.47c-2.69,2.33-5.6,3.98-8.72,4.96-3.13.98-6.18,1.47-9.16,1.47Z"></path>
                             <path class="cls-1" d="m339.81,99.38c-4.94,0-8.87-1.58-11.78-4.74-2.91-3.16-4.36-7.03-4.36-11.61,0-5.67,2.4-10.1,7.2-13.3,4.8-3.2,12.54-5.34,23.23-6.43-.15-2.4-.86-4.31-2.13-5.73-1.27-1.42-3.4-2.13-6.38-2.13-2.25,0-4.54.44-6.87,1.31-2.33.87-4.8,2.07-7.42,3.6l-5.78-10.58c3.42-2.11,7.07-3.82,10.96-5.13,3.89-1.31,7.94-1.96,12.16-1.96,6.91,0,12.21,2,15.92,6,3.71,4,5.56,10.14,5.56,18.43v30.97h-13.09l-1.09-5.56h-.44c-2.25,2.04-4.67,3.69-7.25,4.96-2.58,1.27-5.4,1.91-8.45,1.91Zm5.45-12.43c1.82,0,3.4-.42,4.74-1.25,1.34-.83,2.71-1.94,4.09-3.33v-9.49c-5.67.73-9.6,1.87-11.78,3.44-2.18,1.56-3.27,3.4-3.27,5.51,0,1.74.56,3.04,1.69,3.87,1.13.84,2.63,1.25,4.53,1.25Z"></path>
                             <path class="cls-1" d="m402.95,99.38c-6.69,0-12.05-2.53-16.09-7.58-4.03-5.05-6.05-11.98-6.05-20.77,0-5.89,1.07-10.96,3.22-15.21,2.14-4.25,4.94-7.51,8.4-9.76,3.45-2.25,7.07-3.38,10.85-3.38,2.98,0,5.49.51,7.52,1.53,2.04,1.02,3.96,2.4,5.78,4.14l-.66-8.29v-18.43h16.03v76.45h-13.09l-1.09-5.34h-.44c-1.89,1.89-4.11,3.47-6.65,4.74-2.55,1.27-5.13,1.91-7.74,1.91Zm4.14-13.09c1.74,0,3.33-.36,4.74-1.09,1.42-.73,2.78-2,4.09-3.82v-22.14c-1.38-1.31-2.85-2.22-4.42-2.73-1.56-.51-3.07-.76-4.53-.76-2.55,0-4.8,1.22-6.76,3.65-1.96,2.44-2.94,6.23-2.94,11.4s.85,9.21,2.56,11.72c1.71,2.51,4.13,3.76,7.25,3.76Z"></path>
                             <path class="cls-1" d="m470.34,99.38c-5.16,0-9.81-1.13-13.96-3.38-4.14-2.25-7.42-5.49-9.81-9.71-2.4-4.22-3.6-9.31-3.6-15.27s1.22-10.94,3.65-15.16c2.43-4.22,5.62-7.47,9.54-9.76,3.93-2.29,8.03-3.44,12.32-3.44,5.16,0,9.43,1.15,12.81,3.44,3.38,2.29,5.92,5.38,7.63,9.27,1.71,3.89,2.56,8.31,2.56,13.25,0,1.38-.07,2.74-.22,4.09-.15,1.35-.29,2.34-.44,3h-32.39c.73,3.93,2.36,6.82,4.91,8.67,2.54,1.85,5.6,2.78,9.16,2.78,3.85,0,7.74-1.2,11.67-3.6l5.34,9.71c-2.76,1.89-5.85,3.38-9.27,4.47-3.42,1.09-6.73,1.64-9.92,1.64Zm-12-34.24h19.52c0-2.98-.71-5.43-2.13-7.36-1.42-1.93-3.73-2.89-6.92-2.89-2.47,0-4.69.86-6.65,2.56-1.96,1.71-3.24,4.27-3.82,7.69Z"></path>
                             <path class="cls-1" d="m502.62,98.08v-54.09h13.09l1.09,6.98h.44c2.25-2.25,4.65-4.2,7.2-5.83,2.54-1.64,5.6-2.45,9.16-2.45,3.85,0,6.96.78,9.32,2.34,2.36,1.56,4.23,3.8,5.62,6.71,2.4-2.47,4.94-4.6,7.63-6.38,2.69-1.78,5.82-2.67,9.38-2.67,5.82,0,10.09,1.95,12.81,5.83,2.73,3.89,4.09,9.21,4.09,15.98v33.59h-16.03v-31.52c0-3.93-.53-6.61-1.58-8.07-1.05-1.45-2.74-2.18-5.07-2.18-2.69,0-5.78,1.74-9.27,5.23v36.53h-16.03v-31.52c0-3.93-.53-6.61-1.58-8.07-1.05-1.45-2.74-2.18-5.07-2.18-2.69,0-5.74,1.74-9.16,5.23v36.53h-16.03Z"></path>
                             <path class="cls-1" d="m602.07,119.23c-1.6,0-3-.11-4.2-.33-1.2-.22-2.34-.47-3.44-.76l2.84-12.21c.51.07,1.09.2,1.74.38.65.18,1.27.27,1.85.27,2.69,0,4.76-.65,6.22-1.96,1.45-1.31,2.54-3.02,3.27-5.13l.76-2.84-20.83-52.67h16.14l7.74,23.23c.8,2.47,1.53,4.98,2.18,7.52.65,2.55,1.34,5.16,2.07,7.85h.44c.58-2.54,1.18-5.11,1.8-7.69.62-2.58,1.25-5.14,1.91-7.69l6.54-23.23h15.38l-18.76,54.63c-1.67,4.51-3.53,8.31-5.56,11.4-2.04,3.09-4.49,5.4-7.36,6.92-2.87,1.53-6.45,2.29-10.74,2.29Z"></path>
                           </g>
                         </g>
                       </g>
                     </g>
                   </svg>
                  <div editabletype="text" class="f-14 f-md-16 w400 mt20 lh160 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
                  <div class=" mt20 mt-md40 white-clr"> <script type="text/javascript" src="https://warriorplus.com/o2/disclaimer/hb06pf" defer=""></script><div class="wplus_spdisclaimer f-17 w300 mt20 lh140 white-clr text-center"><strong>Disclaimer:</strong> <em>WarriorPlus is used to help manage the sale of products on this site. While WarriorPlus helps facilitate the sale, all payments are made directly to the product vendor and NOT WarriorPlus. Thus, all product questions, support inquiries and/or refund requests must be sent to the vendor. WarriorPlus's role should not be construed as an endorsement, approval or review of these products or any claim, statement or opinion used in the marketing of these products.</em></div></div>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-14 f-md-16 w400 lh160 white-clr text-xs-center">Copyright © Aicademy</div>
                  <ul class="footer-ul w400 f-14 f-md-16 white-clr text-center text-md-right">
                     <li><a href="https://support.bizomart.com/hc/en-us" class="white-clr  t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://aicademy.live/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://aicademy.live/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://aicademy.live/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://aicademy.live/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://aicademy.live/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://aicademy.live/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!--Footer Section End -->
        <!-- Exit Popup and Timer -->
   <?php include('timer.php'); ?>
   <!-- Exit Popup and Timer End -->
   </body>
</html>
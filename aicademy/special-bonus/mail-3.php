<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- Device & IE Compatibility Meta -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=9">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=9">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta name="title" content="Aicademy Special Bonuses">
    <meta name="description" content="Aicademy Special Bonuses">
    <meta name="keywords" content="Aicademy Special Bonuses">
    <meta property="og:image" content="https://www.aicademy.live/special-bonus/thumbnail.png">
    <meta name="language" content="English">
    <meta name="revisit-after" content="1 days">
    <meta name="author" content="Pranshu Gupta">
    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:title" content="Aicademy Special Bonuses">
    <meta property="og:description" content="Aicademy Special Bonuses">
    <meta property="og:image" content="https://www.aicademy.live/special-bonus/thumbnail.png">
    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:title" content="Aicademy Special Bonuses">
    <meta property="twitter:description" content="Aicademy Special Bonuses">
    <meta property="twitter:image" content="https://www.aicademy.live/special-bonus/thumbnail.png">


<title>Aicademy  Bonuses</title>
<!-- Shortcut Icon  -->
<link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
<!-- Css CDN Load Link -->
<link rel="stylesheet" href="assets/css/font-awesome.min.css" type="text/css" />
<link rel="stylesheet" href="../common_assets/css/bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="../common_assets/css/general.css">
<link rel="stylesheet" href="assets/css/timer.css" type="text/css" />
<link rel="stylesheet" type="text/css" href="assets/css/bonus-style.css">
<link rel="stylesheet" type="text/css" href="assets/css/style.css">
<link rel="stylesheet" type="text/css" href="assets/css/bonus.css">
<link rel="stylesheet" href="assets/css/style-feature.css" type="text/css">
<link rel="stylesheet" href="assets/css/style-bottom.css" type="text/css">
<!-- Font Family CDN Load Links -->
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&family=Poppins:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<!-- Javascript File Load -->
<script src="../common_assets/js/jquery.min.js"></script>
<script src="../common_assets/js/popper.min.js"></script>
<script src="../common_assets/js/bootstrap.min.js"></script>   
<!-- Buy Button Lazy load Script -->


<script>
 $(document).ready(function() {
  /* Every time the window is scrolled ... */
  $(window).scroll(function() {
	 /* Check the location of each desired element */
	 $('.hideme').each(function(i) {
		 var bottom_of_object = $(this).offset().top + $(this).outerHeight();
		 var bottom_of_window = $(window).scrollTop() + $(window).height();
		 /* If the object is completely visible in the window, fade it it */
		 if ((bottom_of_window - bottom_of_object) > -200) {
			 $(this).animate({
				 'opacity': '1'
			 }, 300);
		 }
	 });
  });
 });
</script>
<!-- Smooth Scrolling Script -->
<script>
 $(document).ready(function() {
  // Add smooth scrolling to all links
  $("a").on('click', function(event) {
 
	 // Make sure this.hash has a value before overriding default behavior
	 if (this.hash !== "") {
		 // Prevent default anchor click behavior
		 event.preventDefault();
 
		 // Store hash
		 var hash = this.hash;
 
		 // Using jQuery's animate() method to add smooth page scroll
		 // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
		 $('html, body').animate({
			 scrollTop: $(hash).offset().top
		 }, 800, function() {
 
			 // Add hash (#) to URL when done scrolling (default click behavior)
			 window.location.hash = hash;
		 });
	 } // End if
  });
 });
</script>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-K6H5FJP');</script>
<!-- End Google Tag Manager -->
</head>
<body>


<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K6H5FJP"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

  <!-- New Timer  Start-->
  <?php
	 $date = 'June 26 2023 11:59 PM EST';
	 $exp_date = strtotime($date);
	 $now = time();  
	 /*
	 
	 $date = date('F d Y g:i:s A eO');
	 $rand_time_add = rand(700, 1200);
	 $exp_date = strtotime($date) + $rand_time_add;
	 $now = time();*/
	 
	 if ($now < $exp_date) {
	 ?>
  <?php
	 } else {
		 echo "Times Up";
	 }
	 ?>
  <!-- New Timer End -->
  <?php
	 if(!isset($_GET['afflink'])){
	 $_GET['afflink'] = 'https://warriorplus.com/o2/a/jl4qst/0';
	 $_GET['name'] = 'Pranshu Gupta';      
	 }
	 ?>

<div class="main-header">
        <div class="container">
            <div class="row">
				<div class="col-12">
					<div class="f-md-32 f-20 lh160 w400 text-center white-clr">
						<span class="yellow-clr w600"><?php echo $_GET['name'];?>'s </span> special bonus for &nbsp; 	  
						<svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 644.49 120" style="max-height:55px">
                        <defs>
                          <style>
                            .cls-1i {
                              fill: #fff;
                            }
                      
                            .cls-2i {
                              fill: #0af;
                            }
                          </style>
                        </defs>
                        <g id="Layer_1-2" data-name="Layer 1">
                          <g>
                            <g>
                              <circle class="cls-2i" cx="79.02" cy="43.5" r="7.25"></circle>
                              <circle class="cls-2i" cx="70.47" cy="78.46" r="5.46"></circle>
                              <path class="cls-2i" d="m158.03,45.28l-33.74,13.31v36.4l-45.27,16.86-27.9-10.4c-4.17,3.37-7.04,6.74-8.15,10.11-1.01,3.22-.07,6.21,2.46,8.42-5.44-1.46-8-5.25-6.96-9.76,1.24-3.62,4.79-7.68,10-12.08h.01c3.95-3.31,8.84-6.84,14.46-10.5,2.04,1.67,4.68,2.68,7.53,2.68,6.56,0,11.88-5.32,11.88-11.88s-5.32-11.88-11.88-11.88-11.88,5.32-11.88,11.88c0,1.13.15,2.21.46,3.25-8.01,5.01-14.83,10.36-19.57,15.44l-5.75-2.14v-7.66c-7.84,4.92-11.84,11.06-5.99,16.99-5.17-1.64-7.69-4.49-7.69-8.05,0-4.83,4.67-10.99,13.69-17.26l35.41-23.23c2.71,2.18,6.13,3.48,9.87,3.48,8.7,0,15.76-7.06,15.76-15.76s-7.06-15.76-15.76-15.76-15.76,7.06-15.76,15.76c0,1.59.23,3.12.68,4.57l-30.2,19.53v-9L0,45.28,79.02,0l79.01,45.28Z"></path>
                              <path class="cls-2i" d="m149.38,68.32v-19.62l-1.51.6v19.03c-1.66.37-2.9,1.96-2.9,3.85v11.69h7.31v-11.69c0-1.9-1.24-3.48-2.9-3.85Z"></path>
                            </g>
                            <g>
                              <g>
                                <path class="cls-2i" d="m178.03,98.08l22.25-71.1h19.3l22.25,71.1h-17.01l-4.36-16.9h-21.7l-4.36,16.9h-16.36Zm25.74-35.99l-1.74,6.54h15.16l-1.64-6.54c-.95-3.56-1.91-7.34-2.89-11.34-.98-4-1.95-7.85-2.89-11.56h-.44c-.87,3.78-1.76,7.65-2.67,11.61-.91,3.96-1.87,7.73-2.89,11.29Z"></path>
                                <path class="cls-2i" d="m256.22,36.03c-2.69,0-4.87-.76-6.54-2.29-1.67-1.53-2.51-3.56-2.51-6.11s.83-4.58,2.51-6.11c1.67-1.53,3.85-2.29,6.54-2.29s4.87.76,6.54,2.29c1.67,1.53,2.51,3.56,2.51,6.11s-.84,4.58-2.51,6.11c-1.67,1.53-3.85,2.29-6.54,2.29Zm-7.96,62.05v-54.09h16.03v54.09h-16.03Z"></path>
                              </g>
                              <g>
                                <path class="cls-1i" d="m299.89,99.38c-5.02,0-9.54-1.11-13.58-3.33-4.03-2.22-7.23-5.45-9.6-9.71-2.36-4.25-3.54-9.36-3.54-15.32s1.31-11.16,3.93-15.38c2.62-4.22,6.07-7.43,10.36-9.65,4.29-2.22,8.9-3.33,13.85-3.33,3.34,0,6.31.55,8.89,1.64,2.58,1.09,4.89,2.47,6.92,4.14l-7.52,10.36c-2.55-2.11-4.98-3.16-7.31-3.16-3.85,0-6.92,1.38-9.21,4.14-2.29,2.76-3.44,6.51-3.44,11.23s1.15,8.38,3.44,11.18c2.29,2.8,5.18,4.2,8.67,4.2,1.74,0,3.45-.38,5.13-1.15,1.67-.76,3.2-1.69,4.58-2.78l6.33,10.47c-2.69,2.33-5.6,3.98-8.72,4.96-3.13.98-6.18,1.47-9.16,1.47Z"></path>
                                <path class="cls-1i" d="m339.81,99.38c-4.94,0-8.87-1.58-11.78-4.74-2.91-3.16-4.36-7.03-4.36-11.61,0-5.67,2.4-10.1,7.2-13.3,4.8-3.2,12.54-5.34,23.23-6.43-.15-2.4-.86-4.31-2.13-5.73-1.27-1.42-3.4-2.13-6.38-2.13-2.25,0-4.54.44-6.87,1.31-2.33.87-4.8,2.07-7.42,3.6l-5.78-10.58c3.42-2.11,7.07-3.82,10.96-5.13,3.89-1.31,7.94-1.96,12.16-1.96,6.91,0,12.21,2,15.92,6,3.71,4,5.56,10.14,5.56,18.43v30.97h-13.09l-1.09-5.56h-.44c-2.25,2.04-4.67,3.69-7.25,4.96-2.58,1.27-5.4,1.91-8.45,1.91Zm5.45-12.43c1.82,0,3.4-.42,4.74-1.25,1.34-.83,2.71-1.94,4.09-3.33v-9.49c-5.67.73-9.6,1.87-11.78,3.44-2.18,1.56-3.27,3.4-3.27,5.51,0,1.74.56,3.04,1.69,3.87,1.13.84,2.63,1.25,4.53,1.25Z"></path>
                                <path class="cls-1i" d="m402.95,99.38c-6.69,0-12.05-2.53-16.09-7.58-4.03-5.05-6.05-11.98-6.05-20.77,0-5.89,1.07-10.96,3.22-15.21,2.14-4.25,4.94-7.51,8.4-9.76,3.45-2.25,7.07-3.38,10.85-3.38,2.98,0,5.49.51,7.52,1.53,2.04,1.02,3.96,2.4,5.78,4.14l-.66-8.29v-18.43h16.03v76.45h-13.09l-1.09-5.34h-.44c-1.89,1.89-4.11,3.47-6.65,4.74-2.55,1.27-5.13,1.91-7.74,1.91Zm4.14-13.09c1.74,0,3.33-.36,4.74-1.09,1.42-.73,2.78-2,4.09-3.82v-22.14c-1.38-1.31-2.85-2.22-4.42-2.73-1.56-.51-3.07-.76-4.53-.76-2.55,0-4.8,1.22-6.76,3.65-1.96,2.44-2.94,6.23-2.94,11.4s.85,9.21,2.56,11.72c1.71,2.51,4.13,3.76,7.25,3.76Z"></path>
                                <path class="cls-1i" d="m470.34,99.38c-5.16,0-9.81-1.13-13.96-3.38-4.14-2.25-7.42-5.49-9.81-9.71-2.4-4.22-3.6-9.31-3.6-15.27s1.22-10.94,3.65-15.16c2.43-4.22,5.62-7.47,9.54-9.76,3.93-2.29,8.03-3.44,12.32-3.44,5.16,0,9.43,1.15,12.81,3.44,3.38,2.29,5.92,5.38,7.63,9.27,1.71,3.89,2.56,8.31,2.56,13.25,0,1.38-.07,2.74-.22,4.09-.15,1.35-.29,2.34-.44,3h-32.39c.73,3.93,2.36,6.82,4.91,8.67,2.54,1.85,5.6,2.78,9.16,2.78,3.85,0,7.74-1.2,11.67-3.6l5.34,9.71c-2.76,1.89-5.85,3.38-9.27,4.47-3.42,1.09-6.73,1.64-9.92,1.64Zm-12-34.24h19.52c0-2.98-.71-5.43-2.13-7.36-1.42-1.93-3.73-2.89-6.92-2.89-2.47,0-4.69.86-6.65,2.56-1.96,1.71-3.24,4.27-3.82,7.69Z"></path>
                                <path class="cls-1i" d="m502.62,98.08v-54.09h13.09l1.09,6.98h.44c2.25-2.25,4.65-4.2,7.2-5.83,2.54-1.64,5.6-2.45,9.16-2.45,3.85,0,6.96.78,9.32,2.34,2.36,1.56,4.23,3.8,5.62,6.71,2.4-2.47,4.94-4.6,7.63-6.38,2.69-1.78,5.82-2.67,9.38-2.67,5.82,0,10.09,1.95,12.81,5.83,2.73,3.89,4.09,9.21,4.09,15.98v33.59h-16.03v-31.52c0-3.93-.53-6.61-1.58-8.07-1.05-1.45-2.74-2.18-5.07-2.18-2.69,0-5.78,1.74-9.27,5.23v36.53h-16.03v-31.52c0-3.93-.53-6.61-1.58-8.07-1.05-1.45-2.74-2.18-5.07-2.18-2.69,0-5.74,1.74-9.16,5.23v36.53h-16.03Z"></path>
                                <path class="cls-1i" d="m602.07,119.23c-1.6,0-3-.11-4.2-.33-1.2-.22-2.34-.47-3.44-.76l2.84-12.21c.51.07,1.09.2,1.74.38.65.18,1.27.27,1.85.27,2.69,0,4.76-.65,6.22-1.96,1.45-1.31,2.54-3.02,3.27-5.13l.76-2.84-20.83-52.67h16.14l7.74,23.23c.8,2.47,1.53,4.98,2.18,7.52.65,2.55,1.34,5.16,2.07,7.85h.44c.58-2.54,1.18-5.11,1.8-7.69.62-2.58,1.25-5.14,1.91-7.69l6.54-23.23h15.38l-18.76,54.63c-1.67,4.51-3.53,8.31-5.56,11.4-2.04,3.09-4.49,5.4-7.36,6.92-2.87,1.53-6.45,2.29-10.74,2.29Z"></path>
                              </g>
                            </g>
                          </g>
                        </g>
                      </svg>
					</div>
				</div>

				<!-- <div class="col-12 mt20 mt-md40 text-center">
					<div class="f-16 f-md-18 w700 lh160 white-clr"><span>Grab My 20 Exclusive Bonuses Before the Deal Ends…</span></div>
				</div> -->

				<div class="col-12 text-center lh150 mt20 mt-md40">
                    <div class="pre-heading-box f-18 f-md-22 w600 lh140 white-clr">
						Grab My 20 Exclusive Bonuses Before the Deal Ends…
					</div>
                </div>

				<div class="col-12 mt-md30 mt20 f-md-48 f-28 w400 text-center white-clr lh140">
				World's First App <span class="w700 yellow-clr">Builds Us DFY Auto-Updating Academy Sites, Prefill Them With 50,000+ Smoking Hot AI Courses...</span>
               </div>
			   <div class="col-12 mt-md15 mt15 text-center ">
                  <div class="f-22 f-md-28 w700 lh140 white-clr">
				  Then Promote Them To 495 MILLION Buyers For 100% Free…
                  </div>
               </div>
			   <div class="col-12 mt20 text-center">
                  <div class="f-22 f-md-28 w700 lh140 white-clr post-heading">
				  Resulting In $872.36 Daily With Zero Work
                  </div>
               </div>
			   <div class="col-12 mt15 text-center">
			   <div class="f-20 f-md-22 w500 lh140 white-clr text-center">
                  Without Us Creating Anything…. Even A 100% Beginner Or Camera SHY Can Do It.
                  </div></div>
			   <div class="col-12 mt15 text-center">
                  <div class="f-20 f-md-22 w600 lh140 yellow-clr">
				  No Tech Skills Needed - No Experience Required - No Hidden Fees - No BS
                  </div>
               </div>
				
            </div>
            <div class="row mt20 mt-md30">
                    <div class="video-frame col-12 col-md-10 mx-auto">
					<img src="assets/images/productbox.webp" class="img-fluid mx-auto d-block">
					   <!-- <div class="responsive-video">
						   <iframe src="https://coursesify.dotcompal.com/video/embed/o4eartift7" style="width:100%; height:100%" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
					   </div> -->
                </div>
            </div>
        </div>
    </div>
	
	<div class="second-section">
         <div class="container">
            <div class="row">
			<div class="col-12 col-md-6 ">
				<div class="f-16 f-md-20 lh160 w400">
					<ul class="kaptick pl0">
						<li><span class="w600">Dominate The E-Learning</span> Industry With 3 Steps. </li>
						<li><span class="w600">Let AI Create Hundreds Of Courses </span> For You On Autopilot.</li>
						<li>Ai Creates Our Websites too, <span class="w600">No tech setup whatsoever</span></li>
						<li><span class="w600">Integrate Any Payment Processor</span> Your Want </li>
						<li>No Complicated Setup -<span class="w600"> Get Up And Running In 2 Minutes </span></li>
						<li><span class="w600">Build Your List </span> With Autoresponder Integration</li>
                    </ul>
				</div>
            </div>
			<div class="col-12 col-md-6">
				<div class="f-16 f-md-20 lh160 w400">
					<ul class="kaptick pl0">
						<li><span class="w600">Works In Any Niche</span> No Matter What. </li>
						<li><span class="w600">AI ChatBots </span>That Will Handle All Customer Support For You </li>
                        <li><span class="w600">Even Traffic Is DFY. </span> Without Spending A Dime</li>
						<li><span class="w600">So Easy,  </span> Anyone Can Do it. </li>
						<li><span class="w600">Cancel All Your Costly </span> Subscriptions</li>
						<li><span class="w600">ZERO </span> Upfront Cost </li>
						<li><span class="w600">30 Days Money-Back Guarantee</span></li>
					</ul>
				</div>
			</div>
        </div>
            <div class="row mt20 mt-md30">
               <!-- CTA Btn Section Start -->
                <!-- CTA Btn Section Start -->
				<div class="col-md-12 col-md-12 col-12 text-center">
					<div class="f-md-26 f-18 text-center black lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
					<div class="f-md-24 f-22 text-center purple lh120 w700 mt15 mt-md20 lite-black-clr">TAKE ACTION NOW!</div>
					<div class="f-md-22 f-17 lh120 w600 mt15 mt-md20 black-clr">Use Special Coupon <span class="w800 lite-black-clr">"SUPERADMINOFF"</span> For <span class="w800 lite-black-clr">$7 Off</span> On Full Funnel </div>
				</div>
				<div class="col-md-12 col-12 text-center mt15 mt-md20">
					<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
						<span class="text-center">Grab Aicademy + My 20 Exclusive Bonuses</span> <br>
					</a>
				</div>
				<div class="col-12 mt15 mt-md20 text-center">
					<img src="assets/images/payment.png" class="img-fluid">
				</div>
				<div class="col-12 mt15 mt-md20" align="center">
					<h3 class="f-md-28 f-20 w500 text-center black">Coupon Is Expiring In... </h3>
				</div>
				<!-- Timer -->
				<div class="col-12 text-center">
					<div class="countdown counter-black">
						<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Days</span> </div>
						<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
						<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
						<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
					</div>
				</div>
				<!-- Timer End -->
            </div>
         </div>
	</div>
  <!-- Step Section End -->
  <div class="step-section">
      <div class="container ">
         <div class="row ">
            <div class="col-12 f-28 f-md-50 w700 lh140 black-clr text-center">
               All It Takes Is 3 Fail-Proof Steps<br class="d-none d-md-block">
            </div>
            <div class="col-12 f-24 f-md-28 w600 lh140 white-clr text-center mt20">
               <div class="niche-design">
                  Start Your E-Learning Empire Today.
               </div>
            </div>
            <div class="col-12 mt30 mt-md90">
               <div class="row align-items-center ">
                  <div class="col-12 col-md-6">
                     <div class="step-shapes f-20 f-md-22 w600 white-clr">
                        Step 1
                     </div>
                     <div class="f-34 f-md-65 w700 lh140 mt15 caveat">
                        Login
                     </div>
                     <div class="f-20 f-md-24 w400 lh140 mt5 mt-md10">
                        Click On Any Of The Button Below To Get Instant Access To Aicademy
                     </div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 ">
                     <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 1px solid #15c6fc;">
                        <source src="assets/images/step1.mp4" type="video/mp4">
                     </video>
                  </div>
               </div>
            </div>
            <div class="col-12">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/left-arrow.webp" class="img-fluid d-none d-md-block mx-auto " alt="Left Arrow">
            </div>
            <div class="col-12 mt30 mt-md30">
               <div class="row align-items-center ">
                  <div class="col-12 col-md-6 order-md-2">
                     <div class="step-shapes f-20 f-md-22 w600 white-clr">
                        Step 2
                     </div>
                     <div class="f-34 f-md-65 w700 lh140 mt15 caveat">
                        Deploy
                     </div>
                     <div class="f-20 f-md-24 w400 lh140 mt5 mt-md10">
                        Just select any niche you want, and let AI create your Udemy-Like website in 30 seconds or less (prefilled With AI Courses, And With DFY Traffic)
                     </div>
                  </div>
                  <div class="col-md-6 col-md-pull-6 mt20 mt-md0 order-md-1 ">
                     <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 1px solid #15c6fc;">
                        <source src="assets/images/step2.mp4" type="video/mp4">
                     </video>
                  </div>
               </div>
            </div>
            <div class="col-12">
               <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/right-arrow.webp" class="img-fluid d-none d-md-block mx-auto" alt="Right Arrow">
            </div>
            <div class="col-12 mt30 mt-md30">
               <div class="row align-items-center ">
                  <div class="col-12 col-md-6">
                     <div class="step-shapes f-20 f-md-22 w600 white-clr">
                        Step 3
                     </div>
                     <div class="f-34 f-md-65 w700 lh140 mt15 caveat">
                        Profit
                     </div>
                     <div class="f-20 f-md-24 w400 lh140 mt5 mt-md10">
                        Sit back and relax. And let AI do the rest of the work. While you enjoy the results
                     </div>
                  </div>
                  <div class="col-12 col-md-6 mt20 mt-md0 ">
                     <video class="border-video" width="100%" height="auto" loop="" autoplay="" muted="muted" style="border-radius:20px; border: 1px solid #15c6fc;">
                        <source src="https://cdn.oppyotest.com/launches/webgenie/preview/images/step3.mp4" type="video/mp4">
                     </video>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
	<!-----step section end---------->
	<!-----proof section---------->
	<div class="cta-btn-section dark-cta-bg">
	   <div class="container">
		  <div class="row">
			 <div class="col-md-12 col-md-12 col-12 text-center">
				<div class="f-md-30 f-18 text-center mt3 white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
				<div class="f-md-38 f-22 text-center purple lh120 w700 mt15 mt-md20 orange-clr">TAKE ACTION NOW!</div>
				<div class="f-md-22 f-17 lh120 w600 mt15 mt-md20 white-clr">Use Special Coupon <span class="w800 orange-clr">"SUPERADMINOFF"</span> For <span class="w800 orange-clr">$7 Off</span> On Full Funnel </div>
			 </div>
			 <div class="col-md-12 col-12 text-center mt15 mt-md20">
			 <a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
				<span class="text-center">Grab Aicademy + My 20 Exclusive Bonuses</span> <br>
				</a>
			 </div>
			 <div class="col-12 mt15 mt-md20 text-center">
				<img src="assets/images/payment.png" class="img-fluid">
			 </div>
			 <div class="col-12 mt15 mt-md20" align="center">
				<h3 class="f-md-28 f-20 w500 text-center white-clr">Coupon Is Expiring In... </h3>
			 </div>
			 <!-- Timer -->
			 <div class="col-12 text-center">
				<div class="countdown counter-white white-clr">
				   <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Days</span> </div>
				   <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
				   <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
				   <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
				</div>
			 </div>
			 <!-- Timer End -->
		  </div>
	   </div>
	</div>
	<!---proof section end---->
	  <!-- Video Testimonial -->
	  <div class="profitwall-sec">
      <div class="container">
         <div class="row">
            <div class="col-12">
               <div class="col-12 f-28 f-md-45 w700 lh140 white-clr text-center">
                  Dominate The Online Learning Niche Today. <br class="d-none d-md-block"> With <u>ZERO</u> Experience 
               </div>
               <div class="col-12 f-22 f-md-28 w400 lh140 white-clr mt10 text-center">
               (Let AI Do All The Work For You)
               </div>
            </div>
         </div>
         <div class="row row-cols-1 row-cols-sm-2 row-cols-md-2 gap30 mt20 mt-md50">
            <div class="col mt20 mt-md50">
               <div class="text-center">
                  <img src="assets/images/no-coding.webp" alt="No Coding" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-26 f-md-32 lh140 w600 text-center black-clr mt20 yellow-head">
                     Ai Creates Website For Us
                  </div>
               </div>
            </div>
            <div class="col mt20 mt-md50">
               <div class="text-center">
                  <img src="assets/images/no-server-configurations.webp" alt="Ai Gives Us Best Niches" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-26 f-md-32 lh140 w600 text-center black-clr mt20 yellow-head">
                  Ai Gives Us Best Niches
                  </div>
               </div>
            </div>
            <div class="col mt20 mt-md50">
               <div class="text-center">
                  <img src="assets/images/no-content-writing.webp" alt="Ai Create Courses For Us" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-26 f-md-32 lh140 w600 text-center black-clr mt20 yellow-head">
                  Ai Create Courses For Us
                  </div>
               </div>
            </div>
            <div class="col mt20 mt-md50">
               <div class="text-center">
                  <img src="assets/images/no-optimizing.webp" alt="Ai Generate Traffic For Us" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-26 f-md-32 lh140 w600 text-center black-clr mt20 yellow-head">
                  Ai Generate Traffic For Us
                  </div>
               </div>
            </div>
            <div class="col mt20 mt-md50">
               <div class="text-center">
                  <img src="assets/images/no-designing.webp" alt="Ai Generate Sales Pages For Us" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-26 f-md-32 lh140 w600 text-center black-clr mt20 yellow-head">
                  Ai Generate Sales Pages For Us
                  </div>
               </div>
            </div>
            <div class="col mt20 mt-md50">
               <div class="text-center">
                  <img src="assets/images/no-translating.webp" alt="Ai Generate Chat Bots For Customer Support For Us" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-26 f-md-32 lh140 w600 text-center black-clr mt20 yellow-head">
                  Ai Generate Chat Bots For Customer Support For Us
                  </div>
               </div>
            </div>

            <div class="col mt20 mt-md50">
               <div class="text-center">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/no-coding.webp" alt="No Coding" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-26 f-md-32 lh140 w600 text-center white-clr mt20 red-head">
                  No Coding
                  </div>
               </div>
            </div>
            <div class="col mt20 mt-md50">
               <div class="text-center">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/no-designing.webp" alt="No Designing" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-26 f-md-32 lh140 w600 text-center white-clr mt20 red-head">
                  No Designing
                  </div>
               </div>
            </div>
            <div class="col mt20 mt-md50">
               <div class="text-center">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/no-content-writing.webp" alt="No Content Writing" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-26 f-md-32 lh140 w600 text-center white-clr mt20 red-head">
                  No Content Writing
                  </div>
               </div>
            </div>
            <div class="col mt20 mt-md50">
               <div class="text-center">
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/no-optimizing.webp" alt="No Optimizing" class="img-fluid mr-md5 mb5 mb-md0 ">
                  <div class="f-26 f-md-32 lh140 w600 text-center white-clr mt20 red-head">
                  No Optimizing
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
    
<!-- Video Testimonial -->
<div class="testimonial-sec">
      <div class="container ">
         <div class="row ">
            <div class="col-12 f-28 f-md-45 w700 lh140 black-clr text-center">
               Join Over 2,463 Members Who Uses Aicademy Every Day
            </div>
            <div class="col-12 f-22 f-md-28 w400 lh140 black-clr text-center">
               (And You Can Join Them In 10 Seconds)
            </div>
         </div>
         <div class="row row-cols-md-2 row-cols-1 gx4 mt-md100 mt0">
            <div class="col mt20 mt-md50">
               <div class="single-testimonial">
                  <div class="st-img">
                     <img src="assets/images/craig-mcintosh.webp" class="img-fluid d-block mx-auto " alt="Craig McIntosh">
                  </div>
                  <div class="mt20 md-md50 f-24 f-md-28 w600 lh140 black-clr">Craig McIntosh </div>
                  <div class="stars mt10">
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                  </div>
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/quote.webp" alt="Quotes" class="mx-auto d-block img-fluid mt20 mt-md50 ">
                  <p class="mt20 f-18 f-md-20 lh140 w400 text-center">
                     "AICADEMY's AI bot is a breakthrough in e-learning, it allowed me to create top-notch, ready-to-go academy websites in minutes, and being preloaded with 50,000+ engaging courses and e-books, Made it effortless, fast to create my online course website, I loved the fact that it gave me complete control over traffic, leads, which led to greater profits. It's undeniably a game-changer in online education!" Don’t miss out on this, or you will regret it. 
                  </p>
               </div>
            </div>
            <div class="col mt20 mt-md50">
               <div class="single-testimonial">
                  <div class="st-img">
                     <img src="assets/images/edward-reid.webp" class="img-fluid d-block mx-auto " alt="Edward Reid">
                  </div>
                  <div class="mt20 md-md50 f-24 f-md-28 w600 lh140 black-clr"> Edward Reid  </div>
                  <div class="stars mt10">
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                  </div>
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/quote.webp" alt="Quotes" class="mx-auto d-block img-fluid mt20 mt-md50 ">
                  <p class="mt20 f-18 f-md-20 lh140 w400 text-center">
                  "I can't believe the incredible results I've achieved with Aicademy! It's truly the world's first AI system that effortlessly creates self-managed academy sites, packed with over 50,000 smoking hot AI courses. Not only does it save me time and effort, but it also generates a staggering $872.36 per day for me. This revolutionary platform is a game-changer for anyone looking to maximize their profits in the digital education industry. Highly recommended!"
                  </p>
               </div>
            </div>
            <div class="col mt-md120 mx-auto">
               <div class="single-testimonial">
                  <div class="st-img">
                     <img src="assets/images/jaeden-downs.webp" class="img-fluid d-block mx-auto " alt="Jaeden Downs">
                  </div>
                  <div class="mt20 md-md50 f-24 f-md-28 w600 lh140 black-clr"> Jaeden Downs  </div>
                  <div class="stars mt10">
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                  </div>
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/quote.webp" alt="Quotes" class="mx-auto d-block img-fluid mt20 mt-md50 ">
                  <p class="mt20 f-18 f-md-20 lh140 w400 text-center">
                     Aicademy has truly revolutionized my online business! This world-first AI system effortlessly creates self-managed academy sites and fills them with over 50,000 smoking hot AI courses. The results have been astonishing. Not only am I providing valuable content to my audience, but I'm also earning an impressive $872.36 per day! The simplicity and profitability of Aicademy are unmatched. Thank you for this game-changing solution!
                  </p>
               </div>
            </div>
            <div class="col mt-md120 mx-auto">
               <div class="single-testimonial">
                  <div class="st-img">
                     <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/stuart-johnson.webp" class="img-fluid d-block mx-auto " alt="Drew Warren">
                  </div>
                  <div class="mt20 md-md50 f-24 f-md-28 w600 lh140 black-clr"> Drew Warren  </div>
                  <div class="stars mt10">
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                     <a href="#"><i class="fa fa-star"></i></a>
                  </div>
                  <img src="https://cdn.oppyotest.com/launches/webgenie/preview/images/quote.webp" alt="Quotes" class="mx-auto d-block img-fluid mt20 mt-md50 ">
                  <p class="mt20 f-18 f-md-20 lh140 w400 text-center">
                     Aicademy is a game-changer! Their world-first AI system effortlessly creates academy sites and prefill them with unique smoking hot AI courses. The convenience and variety are unparalleled. I was blown away by the quality and relevance of the courses offered. With Aicademy, learning has become a breeze, and I've gained invaluable knowledge in various fields. This innovative platform has revolutionized online education, and I can't recommend it enough! Thank you, Aicademy, for making learning accessible and exciting.
                  </p>
               </div>
            </div>
         </div>
      </div>
   </div>

   <!-- CTA Button Section Start -->
	<div class="cta-btn-section dark-cta-bg">
	   <div class="container">
		  <div class="row">
			 <div class="col-md-12 col-md-12 col-12 text-center">
				<div class="f-md-30 f-18 text-center mt3 white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
				<div class="f-md-38 f-22 text-center purple lh120 w700 mt15 mt-md20 orange-clr">TAKE ACTION NOW!</div>
				<div class="f-md-22 f-17 lh120 w600 mt15 mt-md20 white-clr">Use Special Coupon <span class="w800 orange-clr">"SUPERADMINOFF"</span> For <span class="w800 orange-clr">$7 Off</span> On Full Funnel </div>
			 </div>
			 <div class="col-md-12 col-12 text-center mt15 mt-md20">
			 <a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
				<span class="text-center">Grab Aicademy + My 20 Exclusive Bonuses</span> <br>
				</a>
			 </div>
			 <div class="col-12 mt15 mt-md20 text-center">
				<img src="assets/images/payment.png" class="img-fluid">
			 </div>
			 <div class="col-12 mt15 mt-md20" align="center">
				<h3 class="f-md-28 f-20 w500 text-center white-clr">Coupon Is Expiring In... </h3>
			 </div>
			 <!-- Timer -->
			 <div class="col-12 text-center">
				<div class="countdown counter-white white-clr">
				   <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Days</span> </div>
				   <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
				   <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
				   <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
				</div>
			 </div>
			 <!-- Timer End -->
		  </div>
	   </div>
	</div>
	<!-- CTA Button Section End -->
	
	<div class="proudly-section" id="product">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="prdly-pres f-28 f-md-38 w700 lh140 text-center light-blue">
                  Introducing
                  </div>
               </div>
               <div class="col-12 mt-md50 mt20 text-center">
			   <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 644.49 120" style="max-height:55px">
                        <defs>
                          <style>
                            .cls-1i {
                              fill: #fff;
                            }
                      
                            .cls-2i {
                              fill: #0af;
                            }
                          </style>
                        </defs>
                        <g id="Layer_1-2" data-name="Layer 1">
                          <g>
                            <g>
                              <circle class="cls-2i" cx="79.02" cy="43.5" r="7.25"></circle>
                              <circle class="cls-2i" cx="70.47" cy="78.46" r="5.46"></circle>
                              <path class="cls-2i" d="m158.03,45.28l-33.74,13.31v36.4l-45.27,16.86-27.9-10.4c-4.17,3.37-7.04,6.74-8.15,10.11-1.01,3.22-.07,6.21,2.46,8.42-5.44-1.46-8-5.25-6.96-9.76,1.24-3.62,4.79-7.68,10-12.08h.01c3.95-3.31,8.84-6.84,14.46-10.5,2.04,1.67,4.68,2.68,7.53,2.68,6.56,0,11.88-5.32,11.88-11.88s-5.32-11.88-11.88-11.88-11.88,5.32-11.88,11.88c0,1.13.15,2.21.46,3.25-8.01,5.01-14.83,10.36-19.57,15.44l-5.75-2.14v-7.66c-7.84,4.92-11.84,11.06-5.99,16.99-5.17-1.64-7.69-4.49-7.69-8.05,0-4.83,4.67-10.99,13.69-17.26l35.41-23.23c2.71,2.18,6.13,3.48,9.87,3.48,8.7,0,15.76-7.06,15.76-15.76s-7.06-15.76-15.76-15.76-15.76,7.06-15.76,15.76c0,1.59.23,3.12.68,4.57l-30.2,19.53v-9L0,45.28,79.02,0l79.01,45.28Z"></path>
                              <path class="cls-2i" d="m149.38,68.32v-19.62l-1.51.6v19.03c-1.66.37-2.9,1.96-2.9,3.85v11.69h7.31v-11.69c0-1.9-1.24-3.48-2.9-3.85Z"></path>
                            </g>
                            <g>
                              <g>
                                <path class="cls-2i" d="m178.03,98.08l22.25-71.1h19.3l22.25,71.1h-17.01l-4.36-16.9h-21.7l-4.36,16.9h-16.36Zm25.74-35.99l-1.74,6.54h15.16l-1.64-6.54c-.95-3.56-1.91-7.34-2.89-11.34-.98-4-1.95-7.85-2.89-11.56h-.44c-.87,3.78-1.76,7.65-2.67,11.61-.91,3.96-1.87,7.73-2.89,11.29Z"></path>
                                <path class="cls-2i" d="m256.22,36.03c-2.69,0-4.87-.76-6.54-2.29-1.67-1.53-2.51-3.56-2.51-6.11s.83-4.58,2.51-6.11c1.67-1.53,3.85-2.29,6.54-2.29s4.87.76,6.54,2.29c1.67,1.53,2.51,3.56,2.51,6.11s-.84,4.58-2.51,6.11c-1.67,1.53-3.85,2.29-6.54,2.29Zm-7.96,62.05v-54.09h16.03v54.09h-16.03Z"></path>
                              </g>
                              <g>
                                <path class="cls-1i" d="m299.89,99.38c-5.02,0-9.54-1.11-13.58-3.33-4.03-2.22-7.23-5.45-9.6-9.71-2.36-4.25-3.54-9.36-3.54-15.32s1.31-11.16,3.93-15.38c2.62-4.22,6.07-7.43,10.36-9.65,4.29-2.22,8.9-3.33,13.85-3.33,3.34,0,6.31.55,8.89,1.64,2.58,1.09,4.89,2.47,6.92,4.14l-7.52,10.36c-2.55-2.11-4.98-3.16-7.31-3.16-3.85,0-6.92,1.38-9.21,4.14-2.29,2.76-3.44,6.51-3.44,11.23s1.15,8.38,3.44,11.18c2.29,2.8,5.18,4.2,8.67,4.2,1.74,0,3.45-.38,5.13-1.15,1.67-.76,3.2-1.69,4.58-2.78l6.33,10.47c-2.69,2.33-5.6,3.98-8.72,4.96-3.13.98-6.18,1.47-9.16,1.47Z"></path>
                                <path class="cls-1i" d="m339.81,99.38c-4.94,0-8.87-1.58-11.78-4.74-2.91-3.16-4.36-7.03-4.36-11.61,0-5.67,2.4-10.1,7.2-13.3,4.8-3.2,12.54-5.34,23.23-6.43-.15-2.4-.86-4.31-2.13-5.73-1.27-1.42-3.4-2.13-6.38-2.13-2.25,0-4.54.44-6.87,1.31-2.33.87-4.8,2.07-7.42,3.6l-5.78-10.58c3.42-2.11,7.07-3.82,10.96-5.13,3.89-1.31,7.94-1.96,12.16-1.96,6.91,0,12.21,2,15.92,6,3.71,4,5.56,10.14,5.56,18.43v30.97h-13.09l-1.09-5.56h-.44c-2.25,2.04-4.67,3.69-7.25,4.96-2.58,1.27-5.4,1.91-8.45,1.91Zm5.45-12.43c1.82,0,3.4-.42,4.74-1.25,1.34-.83,2.71-1.94,4.09-3.33v-9.49c-5.67.73-9.6,1.87-11.78,3.44-2.18,1.56-3.27,3.4-3.27,5.51,0,1.74.56,3.04,1.69,3.87,1.13.84,2.63,1.25,4.53,1.25Z"></path>
                                <path class="cls-1i" d="m402.95,99.38c-6.69,0-12.05-2.53-16.09-7.58-4.03-5.05-6.05-11.98-6.05-20.77,0-5.89,1.07-10.96,3.22-15.21,2.14-4.25,4.94-7.51,8.4-9.76,3.45-2.25,7.07-3.38,10.85-3.38,2.98,0,5.49.51,7.52,1.53,2.04,1.02,3.96,2.4,5.78,4.14l-.66-8.29v-18.43h16.03v76.45h-13.09l-1.09-5.34h-.44c-1.89,1.89-4.11,3.47-6.65,4.74-2.55,1.27-5.13,1.91-7.74,1.91Zm4.14-13.09c1.74,0,3.33-.36,4.74-1.09,1.42-.73,2.78-2,4.09-3.82v-22.14c-1.38-1.31-2.85-2.22-4.42-2.73-1.56-.51-3.07-.76-4.53-.76-2.55,0-4.8,1.22-6.76,3.65-1.96,2.44-2.94,6.23-2.94,11.4s.85,9.21,2.56,11.72c1.71,2.51,4.13,3.76,7.25,3.76Z"></path>
                                <path class="cls-1i" d="m470.34,99.38c-5.16,0-9.81-1.13-13.96-3.38-4.14-2.25-7.42-5.49-9.81-9.71-2.4-4.22-3.6-9.31-3.6-15.27s1.22-10.94,3.65-15.16c2.43-4.22,5.62-7.47,9.54-9.76,3.93-2.29,8.03-3.44,12.32-3.44,5.16,0,9.43,1.15,12.81,3.44,3.38,2.29,5.92,5.38,7.63,9.27,1.71,3.89,2.56,8.31,2.56,13.25,0,1.38-.07,2.74-.22,4.09-.15,1.35-.29,2.34-.44,3h-32.39c.73,3.93,2.36,6.82,4.91,8.67,2.54,1.85,5.6,2.78,9.16,2.78,3.85,0,7.74-1.2,11.67-3.6l5.34,9.71c-2.76,1.89-5.85,3.38-9.27,4.47-3.42,1.09-6.73,1.64-9.92,1.64Zm-12-34.24h19.52c0-2.98-.71-5.43-2.13-7.36-1.42-1.93-3.73-2.89-6.92-2.89-2.47,0-4.69.86-6.65,2.56-1.96,1.71-3.24,4.27-3.82,7.69Z"></path>
                                <path class="cls-1i" d="m502.62,98.08v-54.09h13.09l1.09,6.98h.44c2.25-2.25,4.65-4.2,7.2-5.83,2.54-1.64,5.6-2.45,9.16-2.45,3.85,0,6.96.78,9.32,2.34,2.36,1.56,4.23,3.8,5.62,6.71,2.4-2.47,4.94-4.6,7.63-6.38,2.69-1.78,5.82-2.67,9.38-2.67,5.82,0,10.09,1.95,12.81,5.83,2.73,3.89,4.09,9.21,4.09,15.98v33.59h-16.03v-31.52c0-3.93-.53-6.61-1.58-8.07-1.05-1.45-2.74-2.18-5.07-2.18-2.69,0-5.78,1.74-9.27,5.23v36.53h-16.03v-31.52c0-3.93-.53-6.61-1.58-8.07-1.05-1.45-2.74-2.18-5.07-2.18-2.69,0-5.74,1.74-9.16,5.23v36.53h-16.03Z"></path>
                                <path class="cls-1i" d="m602.07,119.23c-1.6,0-3-.11-4.2-.33-1.2-.22-2.34-.47-3.44-.76l2.84-12.21c.51.07,1.09.2,1.74.38.65.18,1.27.27,1.85.27,2.69,0,4.76-.65,6.22-1.96,1.45-1.31,2.54-3.02,3.27-5.13l.76-2.84-20.83-52.67h16.14l7.74,23.23c.8,2.47,1.53,4.98,2.18,7.52.65,2.55,1.34,5.16,2.07,7.85h.44c.58-2.54,1.18-5.11,1.8-7.69.62-2.58,1.25-5.14,1.91-7.69l6.54-23.23h15.38l-18.76,54.63c-1.67,4.51-3.53,8.31-5.56,11.4-2.04,3.09-4.49,5.4-7.36,6.92-2.87,1.53-6.45,2.29-10.74,2.29Z"></path>
                              </g>
                            </g>
                          </g>
                        </g>
                      </svg>
               </div>
               <div class="text-center mt20 mt-md50">
                  <div class="pre-heading f-md-22 f-20 w600 lh140 white-clr">
                  Exploit The Emerging E-Learning Business With A Click…  
                  </div>
               </div>
               <div class="f-md-50 f-28 w700 white-clr lh140 mt-md30 mt20 text-center">
                  First To Market AI System <span class="yellow-clr">Creates Self-Managed Courses Site And Prefill It With Unique AI-Generated Courses</span> 
               </div>
               <div class="f-22 f-md-28 lh140 w400 text-center white-clr mt20">
                  This Is The Easiest Way To Capitalize On A Multi-Billion Industry With Ease…
               </div>
            </div>
            <div class="row d-flex flex-wrap align-items-center mt20 mt-md50">
               <div class="col-12 col-md-10 mx-auto">
                  <img src="assets/images/product-image.webp" class="img-fluid d-block mx-auto margin-bottom-15">
               </div>
            </div>
         </div>
      </div>
	<!-------Exclusive Bonus----------->
<div class="exclusive-bonus">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <div class="white-clr f-24 f-md-36 lh140 w600">
                  Exclusive Bonus #1 : WebGenie
               </div>
               <div class="f-18 f-md-20 w500 mt20 white-clr">
                  20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-------Exclusive Bonus----------->
   <div class="header-section-webgenie">
      <div class="container">
         <div class="row">
            <div class="col-12 text-center">
            <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 705.07 130" style="max-height: 60px;">
                     <defs>
                       <style>
                         .cls-1 {
                           fill: #fff;
                         }
                   
                         .cls-2 {
                           fill: #fc8548;
                         }
                   
                         .cls-3 {
                           fill: #15c6fc;
                         }
                       </style>
                     </defs>
                     <g id="Layer_1-2" data-name="Layer 1">
                       <g>
                         <g>
                           <path class="cls-3" d="m106.3,101.8c-15.16,10.74-31.47,19.22-46.87,24.66,5.84,1.82,12.05,2.8,18.49,2.8,32.56,0,59.25-25.09,61.82-56.98-9.69,10.65-21.17,20.83-33.44,29.52Z"></path>
                           <path class="cls-3" d="m131.29,50.75c-1.46-2.75-5.89-11.12-4.97-15.56.43-2.08,2.28-1.79,4.61.7.51.54.91.95,1.23,1.26.76,1.37,1.48,2.77,2.14,4.21-.7-.7-1.36-.95-1.91-.49-1.58,2.28,1.02,10.37,1.05,10.45.19.56.35,1.09.49,1.6-.4.47-.81.94-1.22,1.41-.34-1.22-.8-2.42-1.41-3.58Zm-113.89,30.07c-.97-4.37-1.49-8.91-1.49-13.57,0-10.04,2.39-19.53,6.63-27.92-.53,1.52-.72,2.51-.72,2.51h0c-.07.75-.1,1.15-.04,1.61.42,3.43,4.17,1.59,7.87-3.89.06-.09,6.17-9.63,4.38-12.75-.49-.3-1.06-.32-1.82,0-1.37.86-2.56,1.83-3.6,2.85,5.59-7.32,12.77-13.36,21.03-17.6-2.43,1.69-3.77,3.22-3.77,3.22h0c-.44.61-.71,1.01-.9,1.44-1.38,3.17,2.76,3.53,8.74.69.1-.05,10.22-5.18,10.26-8.78-.27-.51-.73-.83-1.55-.94-.86.03-1.69.1-2.5.22,3.93-1.19,8.03-2,12.25-2.39-1.04.21-1.7.51-1.7.85,0,.62,2.17,1.12,4.86,1.12s4.86-.5,4.86-1.12c0-.54-1.67-1-3.89-1.1.55-.01,1.1-.02,1.65-.02,17.08,0,32.55,6.91,43.77,18.08.37.64.9,1.48,1.65,2.59,3.36,5.03,1.75,6.47-.79,5.21-3.97-1.96-8.77-7.73-10.34-9.62-1.61-1.94-4.03-3.88-6.47-5.38-.42-.13-.85-.26-1.26-.43-.75-.3-1.5-.6-2.27-.85-.24,0-.48-.02-.72-.01-.46.02-1.3.03-1.41.62-.08.44.15.97.32,1.36.42.95,1.02,1.82,1.58,2.69.51.8.99,1.67,1.64,2.36.59.62,1.29,1.11,1.84,1.77.06.07.09.15.11.23,2.46,2.16,5.15,3.79,7.43,3.8,3.84.02,17.78,10.55,9.91,19.31-3.06,3.41-2.18,11.92.78,16.83-4.75,4.65-9.96,9.26-15.54,13.76-.71-.62-1.24-1.49-1.64-2.65-.28-1.05-.55-2.1-.82-3.17-.47-2.74-.15-2.56,2.9-8.5,3.7-7.2-4.29-13.57-11.24-11.68-7.7,2.08-10.61,13.04-2.47,16.92.04,0,3.93.97,5.47,5.74.22.88.44,1.75.67,2.62.51,2.64.17,4.62-1.3,7.21-1.23.9-2.47,1.8-3.72,2.69-7.5,5.31-15.16,10.09-22.71,14.23-.91-3.2.33-6.08.85-6.84,2.11-3.06,3.7-4.14,7.39-5.01,1.14-.27,3.26-.77,5.09-2.42,6.2-5.58,4.7-18.32-6.72-19.11-7.22.35-11.57,8.15-8.61,15.47,2.2,5.45.52,9.12-.68,10.82-1.03,1.46-4.01,4.69-9.6,3.2-4.95-1.32-6.61-6.92-11.18-8.73-7.77-3.09-11.6,5.37-7.27,11.76,3.24,4.79,6.49,6.02,8.64,6.21,2.23.2,3.34-.51,4.08-.97,1.51-.95,1.99-2.85,3.5-3.8,2.73-1.73,7.54-1.65,11.19,1.22-2.99,1.57-5.95,3.04-8.86,4.39-.05,0-.09,0-.14.01-1.76.17-3.05.83-3.86,1.79-3.1,1.35-6.15,2.56-9.11,3.62-.05-.13-.09-.27-.14-.4-2.09-5.4-7.96-8.45-9.6-14.13-.31-1.27-.58-2.58-.81-3.91-.19-1.49-.58-4.58,1.8-7.18,4.3-4.72,3.76-16.42-4.35-19.62-8.03-.92-10.21,9.81-5.52,17.26.02,0,2.56,3.13,3.92,8.96.21,1.02.44,2.02.69,3.01.34,1.51.97,4.33-.15,6.41,0,0-.02,0-.03,0-1.57,3.43,3.31,8.91,7.67,11.78-.87.26-1.74.51-2.59.74-7.91-6.41-14.2-14.73-18.19-24.25.11.17.22.33.33.51,0,0,.06.1.17.27.05.08.1.16.16.24.59.87,1.9,2.67,3.43,3.83,3.14,2.36,5.07.48,4.49-4.38-.01-.1-1.53-10.46-8.2-14.5-1.63-.57-2.56-.27-3.23,1.12-.01.03-.39.92-.46,2.52Zm59.78-22.16c0-5.16-4.18-9.34-9.34-9.34s-9.34,4.18-9.34,9.34,4.18,9.34,9.34,9.34,9.34-4.18,9.34-9.34Zm20.01-20.24c-.99.12-1.99.23-3,.33q-2.56.07-7.37-3.76c-6.07-4.83-11.86,2.62-11.06,9.21.89,7.3,9.43,10.91,14.03,3.99,0-.03,1.4-3.46,5.95-4.24.83-.08,1.66-.17,2.48-.27,2.75-.13,4.63.58,7.16,2.74.02.05,1.12,1.06,2.31,1.67,5.4,2.74,9.26-1.79,7-8.21-1.03-2.91-5.1-9.19-10.54-8.84-1.45.47-2.36,1.51-2.71,3.1-.55,2.56-1.79,3.8-4.27,4.28Zm-2.86-28.9c3.81,1.68,2.69.64,8.36,3.46,5.67,2.82,5.71,1.59,1.86-1.26-3.85-2.86-12.22-4.75-12.22-4.75-3.3-.41-1.81.87,2,2.55Zm-7.76.64s1.78,3.84,6.7,7.38c4.91,3.54,7.42,1.79,3.73-3.84-3.69-5.63-12.28-7.25-10.42-3.54Zm3.83,22.99c5.69,3.02,10.81-.48,8.6-6.63-2.21-6.15-11.64-11.3-13.31-3.27,0,0-.98,6.87,4.72,9.89Zm-29.35-12.88c1.14,3.13,7.38-3.8,10.18-3.93,2.8-.14,3.62.55,5.18,3.69,1.56,3.14,7.83,2.91,6.91-3.13-.93-6.04-4.76-7.26-7.14-5.14-2.38,2.12-4.01.96-4.94-.67s-4.12-.93-8.44,3.49c0,0-2.89,2.57-1.75,5.69Zm-21.21,15.26c.95,4.71,7.81,3.19,12.26-4.31,1.08-1.82,4.46-8.89,1.25-10.18-4.96-.88-14.73,8.46-13.51,14.49Zm-12.46,24.36c.42,1.17,3.89,6.17,9.04.12,3.77-4.42,7.76-2.55,8.53-2.14,3.4,1.87,6.59,8.49,3.46,15.56-1.35,3.04-1.15,8.01,1.84,11.62,5.21,6.29,13.94,3.93,15.33-4.15.07-.4,1.57-9.85-7.82-12.86-5.42-1.74-7.7-6.56-6.1-12.92.28-1.13,1.66-4.03,4.12-5.78,2.18-1.56,4.99-1.19,7.29-2.38,5.97-3.09,7.53-15.39.59-17.07-3.95-.31-9.49,5.43-9.53,11.51-.05,7.82-4.68,11.09-6.1,11.91-2.27,1.31-7.56,1.9-8.52-4.86-.66-4.71-3.02-5.36-4.37-5.33-4.27.1-10.81,8.28-7.75,16.77Zm-7.62,5.64c.03-.1,2.79-10.55,1.54-15.37-.44-.77-.9-.58-1.6.71-.04.08-2.4,4.62-3.06,13.03-.01.22-.03.42-.04.65,0,0,0,.11-.02.29,0,.09-.01.17-.02.26-.06.94-.14,2.88.08,4.21.44,2.73,1.79,1.1,3.13-3.78Z"></path>
                           <path class="cls-2" d="m162.67,10.07c-7.36-10.39-23-12.38-42.41-7.63-.97-.64-2.26-.86-3.55-.51-2.23.61-3.59,2.71-3.05,4.69.54,1.98,2.78,3.09,5.01,2.48,1.5-.41,2.61-1.5,3.01-2.77,14.79-3.45,26.12-2.24,30.9,4.5,10.38,14.65-14.1,49.83-54.68,78.57-40.57,28.74-81.88,40.16-92.26,25.51-4.43-6.25-2.49-16.25,4.32-27.89-.5-1.18-.98-2.38-1.42-3.59C-.57,98.79-2.88,112.77,3.99,122.47c10.65,15.03,56.61,7.71,100.43-23.33,43.82-31.04,68.79-74.19,58.25-89.06Z"></path>
                         </g>
                         <g>
                           <g>
                             <path class="cls-1" d="m284.28,35.17l-18.1,69.44h-20.48l-11.08-45.7-11.47,45.7h-20.48l-17.61-69.44h18.1l9.99,50.55,12.36-50.55h18.6l11.87,50.55,10.09-50.55h18.2Z"></path>
                             <path class="cls-1" d="m344.52,81.07h-38.28c.26,3.43,1.37,6.05,3.31,7.86,1.94,1.81,4.34,2.72,7.17,2.72,4.22,0,7.15-1.78,8.8-5.34h18c-.92,3.63-2.59,6.89-5,9.79-2.41,2.9-5.43,5.18-9.05,6.83-3.63,1.65-7.68,2.47-12.17,2.47-5.41,0-10.22-1.15-14.44-3.46-4.22-2.31-7.52-5.6-9.89-9.89-2.37-4.29-3.56-9.3-3.56-15.04s1.17-10.75,3.51-15.04c2.34-4.29,5.62-7.58,9.84-9.89,4.22-2.31,9.07-3.46,14.54-3.46s10.09,1.12,14.24,3.36c4.15,2.24,7.4,5.44,9.74,9.6,2.34,4.15,3.51,9,3.51,14.54,0,1.58-.1,3.23-.3,4.95Zm-17.01-9.4c0-2.9-.99-5.21-2.97-6.92-1.98-1.71-4.45-2.57-7.42-2.57s-5.23.83-7.17,2.47c-1.95,1.65-3.15,3.99-3.61,7.02h21.17Z"></path>
                             <path class="cls-1" d="m377.46,51c2.97-1.58,6.36-2.37,10.19-2.37,4.55,0,8.67,1.15,12.36,3.46,3.69,2.31,6.61,5.61,8.75,9.89,2.14,4.29,3.21,9.27,3.21,14.94s-1.07,10.67-3.21,14.99c-2.14,4.32-5.06,7.65-8.75,9.99-3.69,2.34-7.81,3.51-12.36,3.51-3.89,0-7.29-.78-10.19-2.32-2.9-1.55-5.18-3.61-6.83-6.18v7.72h-16.91V31.41h16.91v25.82c1.58-2.57,3.86-4.65,6.83-6.23Zm13.8,15.98c-2.34-2.41-5.23-3.61-8.66-3.61s-6.22,1.22-8.56,3.66c-2.34,2.44-3.51,5.77-3.51,9.99s1.17,7.55,3.51,9.99c2.34,2.44,5.19,3.66,8.56,3.66s6.23-1.24,8.61-3.71c2.37-2.47,3.56-5.82,3.56-10.04s-1.17-7.53-3.51-9.94Z"></path>
                             <path class="cls-3" d="m466.98,57.13c-1.25-2.31-3.05-4.07-5.39-5.29-2.34-1.22-5.09-1.83-8.26-1.83-5.47,0-9.86,1.8-13.16,5.39-3.3,3.59-4.95,8.39-4.95,14.39,0,6.4,1.73,11.39,5.19,14.99,3.46,3.6,8.23,5.39,14.29,5.39,4.15,0,7.67-1.05,10.53-3.17,2.87-2.11,4.96-5.14,6.28-9.1h-21.47v-12.46h36.8v15.73c-1.25,4.22-3.38,8.15-6.38,11.77-3,3.63-6.81,6.56-11.42,8.8-4.62,2.24-9.83,3.36-15.63,3.36-6.86,0-12.98-1.5-18.35-4.5-5.38-3-9.56-7.17-12.56-12.51-3-5.34-4.5-11.44-4.5-18.3s1.5-12.97,4.5-18.35c3-5.37,7.17-9.56,12.51-12.56,5.34-3,11.44-4.5,18.3-4.5,8.31,0,15.32,2.01,21.02,6.03,5.7,4.02,9.48,9.59,11.33,16.72h-18.7Z"></path>
                             <path class="cls-3" d="m547.99,81.07h-38.28c.26,3.43,1.37,6.05,3.31,7.86,1.94,1.81,4.34,2.72,7.17,2.72,4.22,0,7.15-1.78,8.8-5.34h18c-.92,3.63-2.59,6.89-5,9.79-2.41,2.9-5.43,5.18-9.05,6.83-3.63,1.65-7.68,2.47-12.17,2.47-5.41,0-10.22-1.15-14.44-3.46-4.22-2.31-7.52-5.6-9.89-9.89-2.37-4.29-3.56-9.3-3.56-15.04s1.17-10.75,3.51-15.04c2.34-4.29,5.62-7.58,9.84-9.89,4.22-2.31,9.07-3.46,14.54-3.46s10.09,1.12,14.24,3.36c4.15,2.24,7.4,5.44,9.74,9.6,2.34,4.15,3.51,9,3.51,14.54,0,1.58-.1,3.23-.3,4.95Zm-17.01-9.4c0-2.9-.99-5.21-2.97-6.92-1.98-1.71-4.45-2.57-7.42-2.57s-5.23.83-7.17,2.47c-1.95,1.65-3.15,3.99-3.61,7.02h21.17Z"></path>
                             <path class="cls-3" d="m606.3,55.1c3.86,4.19,5.79,9.94,5.79,17.26v32.25h-16.82v-29.97c0-3.69-.96-6.56-2.87-8.61-1.91-2.04-4.49-3.07-7.72-3.07s-5.8,1.02-7.72,3.07c-1.91,2.04-2.87,4.91-2.87,8.61v29.97h-16.91v-55.2h16.91v7.32c1.71-2.44,4.02-4.37,6.92-5.79,2.9-1.42,6.17-2.13,9.79-2.13,6.46,0,11.62,2.09,15.48,6.28Z"></path>
                             <path class="cls-3" d="m640.77,59.74v44.87h-16.91v-44.87h16.91Z"></path>
                             <path class="cls-3" d="m704.77,81.07h-38.28c.26,3.43,1.37,6.05,3.31,7.86,1.94,1.81,4.34,2.72,7.17,2.72,4.22,0,7.15-1.78,8.8-5.34h18c-.92,3.63-2.59,6.89-5,9.79-2.41,2.9-5.42,5.18-9.05,6.83-3.63,1.65-7.68,2.47-12.17,2.47-5.41,0-10.22-1.15-14.44-3.46-4.22-2.31-7.52-5.6-9.89-9.89-2.37-4.29-3.56-9.3-3.56-15.04s1.17-10.75,3.51-15.04c2.34-4.29,5.62-7.58,9.84-9.89,4.22-2.31,9.07-3.46,14.54-3.46s10.09,1.12,14.24,3.36c4.15,2.24,7.4,5.44,9.74,9.6,2.34,4.15,3.51,9,3.51,14.54,0,1.58-.1,3.23-.3,4.95Zm-17.01-9.4c0-2.9-.99-5.21-2.97-6.92-1.98-1.71-4.45-2.57-7.42-2.57s-5.23.83-7.17,2.47c-1.95,1.65-3.15,3.99-3.61,7.02h21.17Z"></path>
                           </g>
                           <g>
                             <path class="cls-3" d="m613.45,33.05c-1.76.24-3.17,1.63-3.5,3.45-.48,2.66,1.41,4.37,1.49,4.45,1.5,1.45,3.85,1.42,6.61-.15.52-.3,1.07-.44,1.65-.42l.67.02c.24.29.45.55.63.8,2.9,3.88,5.97,5.37,8.03,5.94-.44.92-1.18,2.04-2.12,2.31h-3.15c0,3.09,0,5.06,0,5.06h16.98q0-5.06,0-5.06h-3.36c-.3-.11-1.38-.59-2.23-2.33,4.79-1.07,7.98-4.21,8.43-4.68,6.25-5.05,11.29-5.51,11.34-5.51.37-.03.69-.28.82-.62.12-.35.04-.75-.22-1.01l-2.04-2.03c-.21-.21-.52-.32-.81-.27-11.25,1.69-13.55.72-13.85.55-.18-.22-.45-.34-.74-.34h-11.72c-2.4,1.09-8.38.39-10.55.03-.16-.06-.32-.1-.48-.12-.65-.12-1.28-.14-1.87-.07h0Zm16.42,16.4c.53-.7.89-1.44,1.11-1.98.31,0,.5,0,.54,0,.14,0,.29,0,.42,0,.42,0,.83-.02,1.24-.06.34.84.76,1.51,1.18,2.03h-4.49Zm-17.13-9.91c-.08-.07-1.2-1.12-.91-2.7.18-.99.95-1.78,1.87-1.9.38-.05.8-.03,1.25.06.08,0,.15.03.23.06.02,0,.04,0,.06.02.24.09.46.24.67.43.64.62,1.77,1.81,2.85,3.02-.57.11-1.14.31-1.66.6-1.13.64-3.19,1.55-4.36.41h0Z"></path>
                             <path class="cls-3" d="m638.11,30.84c.13,0,.27,0,.4.04-.7-1.53-2.06-2.74-3.73-3.33.22-.39.34-.82.34-1.29,0-1.48-1.2-2.7-2.7-2.7s-2.7,1.21-2.7,2.7c0,.48.14.92.36,1.3-1.73.61-3.1,1.89-3.79,3.48.31-.13.64-.19.98-.19h10.83Zm-5.68-5.49c.5,0,.9.4.9.9,0,.49-.39.88-.87.89h-.04c-.49,0-.88-.4-.88-.89,0-.49.4-.9.9-.9h0Z"></path>
                           </g>
                         </g>
                       </g>
                     </g>
                   </svg>
            </div>
            
            <div class="col-12 text-center mt20 mt-md50">
               <div class="pre-heading f-md-22 f-20 w600 lh140 white-clr">
                  We Exploited ChatGPT4 To Create A Better And Smarter AI Model...
               </div>
            </div>
            <div class="col-12 mt-md25 mt20 f-md-50 f-28 w700 text-center white-clr lh140">
               World's First A.I Bot <span class="orange-gradient"> Builds Us DFY Websites Prefilled With Smoking Hot Content…</span> 
            </div>
            <div class="col-12 mt-md15 mt20 f-22 f-md-28 w700 text-center lh140 white-clr text-capitalize">
               Then Promote It To 495 MILLION Buyers For 100% Free…
            </div>
            <div class="col-12 mt-md20 mt20 f-22 f-md-28 w700 text-center lh140 black-clr text-capitalize">
               <div class="head-yellow">
                  Banking Us $578.34 Daily With Zero Work
               </div> 
            </div>
            <div class="col-12 mt-md15 mt20 f-20 f-md-22 w600 text-center lh140 orange-gradient text-capitalize">
               Without Creating Anything | Without Writing Content | Without Promoting | Without Paid Ads
            </div>
            <div class="col-12 mt-md15 mt20 f-20 f-md-22 w500 text-center lh140 white-clr text-capitalize">
               No Tech Skills Needed - No Experience Required - No Hidden Fees - No BS
            </div>
            <div class="col-12 mt20 mt-md40">
               <div class="row">
                  <div class="col-md-7 col-12 min-md-video-width-left">
                     <div class="f-18 f-md-20 w600 lh140 text-center white-clr blue-design">
                     Watch How We Deploy Profitable AI-Managed Websites That Makes Us Over $17,349 Monthly In Less Than 30 Seconds…
                     </div>
                     <div class="col-12 mt20 mt-md30">
                        <div class="video-box">
                           <div style="padding-bottom: 56.25%;position: relative;">
                              <iframe src="https://webgenie.oppyo.com/video/embed/9xupab3svh" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                           </div>
                        </div>      
                     </div>
                     <div class="col-12 mt20 mt-md40">
                        <div class="border-dashed">
                           <div class="f-18 f-md-20 w500 lh140 text-center grey-clr">
                              First 99 Action Taker Get Instant Access To Our Top 5 <br class="d-none d-md-block"> Profitable DFY Websites For <span class="orange-gradient underline w700">FREE</span> 
                           </div>
                           <div class="f-18 f-md-20 w500 lh140 text-center yellow-clr">
                              (Average User Saw 475% Increase In Profit <span class="w700">worth $1,997</span>)
                           </div>
                        </div>
                     </div>

                     
                  </div>
                  <div class="col-md-5 col-12 mt20 mt-md0 min-md-video-width-right">
                     <div class="key-features-bg">
                        <ul class="list-head pl0 m0 f-18 f-md-20 lh150 w400 white-clr">
                           <li>Deploy Stunning Websites By Leveraging ChatGPT…</li>
                           <li><span class="w600">All Websites Are Prefilled With Smoking Hot, AI, Human-Like Content</span></li>
                           <li>Instant High Convertring Traffic For 100% Free</li>
                           <li><span class="w600">100% Of Beta Testers Made At Least $100 Within 24 Hours Of Using WebGenie.</span></li>
                           <li>HighTicket Offers For DFY Monetization</li>
                           <li><span class="w600">No Complicated Setup - Get Up And Running In 2 Minutes</span></li>
                           <li><span class="w600">We Let AI Do All The Work For Us.</span></li>
                           <li>Get Up And Running In 30 Seconds Or Less </li>
                           <li><span class="w600">Instantly Tap Into $1.3 Trillion Platform</span> </li>
                           <li>Leverage A Better And Smarter AI Model Than ChatGPT4</li>
                           <li><span class="w600">99.99% Up Time Guaranteed</span> </li>
                           <li>ZERO Upfront Cost</li>
                           <li><span class="w600">30 Days Money-Back Guarantee</span></li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
	<div class="gr-1">
      <div class="container-fluid p0">
         <picture>
            <source media="(min-width:768px)" srcset="assets/images/webgenie.webp">
            <source media="(min-width:320px)" srcset="assets/images/webgenie-mview.webp">
            <img src="assets/images/webgenie.webp" alt="Steps" style="width:100%;">
         </picture>
      </div>
   </div>
   <!-------Exclusive Bonus----------->
<div class="exclusive-bonus">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <div class="white-clr f-24 f-md-36 lh140 w600">
                  Exclusive Bonus #2 : Kyza
               </div>
               <div class="f-18 f-md-20 w500 mt20 white-clr">
                  20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-------Exclusive Bonus----------->
   <div class="header-section-ky">
         <div class="container">
            <div class="row">
               <div class="col-md-3 col-md-12 mx-auto">
                  <img src="assets/images/kyza-logo.webp" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 text-center">
                  <div class="mt20 mt-md60 text-center px-sm15 px0">
						   <div class="f-20 f-sm-22 w600 text-center lh140 d-inline-flex align-items-center justify-content-center relative">
                        <img src="assets/images/stop.webp" class="img-responsive mx-xs-center stop-img">
						      <div class="post-heading1">
						         <i>Paying 100s of Dollar Monthly To Multiple Marketing Apps! </i>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md60 text-center">
                  <div class="f-md-41 f-28 w700 black-clr lh140 line-center">	
						   Game Changer All-In-One Growth Suite... <br class="visible-lg">
						   ✔️Builds Beautiful Pages &amp; Websites ✔️Unlocks Branding Designs Kit ✔️Creates AI-Powered Optin Forms &amp; Pop-Ups, &amp; ✔️Lets You Send UNLIMITED Emails 					
                  </div>
                  <div class="mt-md25 mt20 f-20 f-md-26 w600 black-clr text-center lh140">
                     All From Single Dashboard Without Any Tech Skills and <u>Zero Monthly Fees</u>
                  </div>
                  <div class="col-12 col-md-12  mt20 mt-md40">
							<div class="row d-flex flex-wrap align-items-center">
							   <div class="col-md-7 col-12 mt20 mt-md0">
									<div class="responsive-video">  
										<iframe class="embed-responsive-item" src="https://kyza.dotcompal.com/video/embed/nr4t9jffvd" style="width: 100%;height: 100%; background: transparent !important;
										box-shadow: none !important;" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
								  </div>
							   </div>
							   <div class="col-md-5 col-12 mt20 mt-md0">
									<div class="key-features-bg-ky">
									   <ul class="list-head-ky pl0 m0 f-18 f-md-19 lh140 w500 black-clr">
                                 <li>Create Unlimited Stunning Pages, Popups, And Forms </li>
                                 <li>Mobile, SEO, And Social Media Optimized Pages </li>
                                 <li>Send Unlimited Beautiful Emails For Tons Of Traffic &amp; Sales </li>
                                 <li>Over 100 Done-For-You High Converting Templates </li>
                                 <li>2 Million+ Stock Images &amp; Branding Graphics Kit </li>
                                 <li>Comes With Commercial License To Serve &amp; Charge Your Clients </li>
                                 <li>100% Newbie Friendly &amp; Step-By-Step Training Included  </li>
									   </ul>
								   </div>
							   </div>
						   </div>
                  		</div>
					</div>
            	</div>
         	</div>
      	</div>
	<div class="gr-1">
      <div class="container-fluid p0">
         <picture>
            <source media="(min-width:768px)" srcset="assets/images/kyza.webp">
            <source media="(min-width:320px)" srcset="assets/images/kyza-mview.webp">
            <img src="assets/images/kyza.webp" alt="Steps" style="width:100%;">
         </picture>
      </div>
   </div>

 <!-------Exclusive Bonus----------->
 <div class="exclusive-bonus">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <div class="white-clr f-24 f-md-36 lh140 w600">
                  Exclusive Bonus #3 : MailGPT
               </div>
               <div class="f-18 f-md-20 w500 mt20 white-clr">
                  20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-------Exclusive Bonus----------->

   <div class="header-section-mailgpt">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                  <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 620.04 134.95" style="max-height:60px;"><defs><style>.cls-1{fill:none;}.cls-2{clip-path:url(#clip-path);}.cls-3{clip-path:url(#clip-path-2);}.cls-4{clip-path:url(#clip-path-3);}.cls-5{clip-path:url(#clip-path-4);}.cls-6{fill:#fff;}</style><clipPath id="clip-path"><path class="cls-1" d="M40.14,36.57l13,12.24v77.25a6.51,6.51,0,0,1-6.51,6.52h0a6.52,6.52,0,0,1-6.52-6.52Z"></path></clipPath><clipPath id="clip-path-2"><path class="cls-1" d="M170.36,36.57l-13,12.24v77.25a6.52,6.52,0,0,0,6.52,6.52h0a6.51,6.51,0,0,0,6.51-6.52Z"></path></clipPath><clipPath id="clip-path-3"><path class="cls-1" d="M169,15,108,72.83a4,4,0,0,1-5.49,0L41.59,15A6.51,6.51,0,0,1,46.07,3.73h0a6.56,6.56,0,0,1,4.49,1.78l50.57,48a6,6,0,0,0,8.33,0L160,5.52a6.5,6.5,0,0,1,4.48-1.79h0A6.51,6.51,0,0,1,169,15Z"></path></clipPath><clipPath id="clip-path-4"><path class="cls-1" d="M101.27,81.56,35,19A9.6,9.6,0,0,0,18.8,26v99.87a6.52,6.52,0,0,0,6.51,6.52h0a6.52,6.52,0,0,0,6.52-6.52V34L95.92,94.44l5.35,5a6.06,6.06,0,0,0,8.31,0l5.34-5L179,34v91.91a6.52,6.52,0,0,0,6.52,6.52h0a6.52,6.52,0,0,0,6.51-6.52V26a9.6,9.6,0,0,0-16.19-7L109.58,81.56A6.06,6.06,0,0,1,101.27,81.56Z"></path></clipPath></defs><title>MailGPT White Logo</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><g class="cls-2"><image width="90" height="99" transform="translate(-0.75 36.16)" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFoAAABjCAYAAAAb3RdnAAAACXBIWXMAAAsSAAALEgHS3X78AAAS0klEQVR4Xu2cTahtyVXHf2ufeyOKiNLddhJbE6XVNtF8dqskDsSRM0eRiIKoiIgfAyeKIgTEiYIDJQMRBBUxmpEzBw4caBvtl26bjtEmrbZGOunOaxQRUfuevRysj1pVZ597zv0495wHFrx7a9de9bH/9a//WlV7vyuqyimnp773LxTgmT/6oOyyPeUkpwz0kw5yJOHBBXzaZXDUJOUfoMCTH/pzfWqYgAchnSyjn/ywg1mH1+UVEXlgGH6yjNaArzC6z0syfKx7iukkGf3+73tag76yldE1f/rsPttlcJQk/kNBC9IdihXowu5TBfzkGP3+7396izY/2Aw/KY1+3w88vUxfyR+oFExH/c7y09PvkwIaFoCseXFkpTjLajfyV+RkwsGTkY73/uBfmhyHTOgumdDML2r3+Fhq83QsOTkdoH/oE4oWgBXQfXS5qI2yCHB/fRz9Pgmg3/Mjn9AGsDrIztR5B3MzPwA+2i0Afu/j33FnYJ+ERuskMIFOltdJYOU3RdLpLepy5lvBVv0eHOZd6vfRGf3uH/srDQaj2tg4MhyQmevJyWgzXt+BnBwV6Hf9+F9v6jIgRT5MMtq1zF75KoBrIfORAD+qdOgETAKTuGzgEiEpJ0x+7RISNiEpl8bVJRzMNEI4yIlymPj7aIz+lp98xmgaTE0Gqz17ve7YbmXMNDtvZyu7q52nnRHKLbP7aEB/8089owjIPIANdHICPeCArLWBotcHfC/9RhFuDvhRgH7nz9yzcK6CPI/gag9i2NHqgNtH3bDdBfgS2KPNeH1Dhh9Fo3XlmrxqWqyTtPLQ10lMg0N7U6/9OjR6anqdoV7ci07zXuStYCMU3HZ9Q/2+c0Z/088+q53uqjJdaEMkmLqN4dVuDAdjhUDR4F3sbheHZPedA/3Ezz/bdoFzD7jFyQXkuisswLU6druGg7WO3Wv1LgU8r7W3Ge2uCfidAv2Nv/hcp80ZQSgN5LwewFN6vV4CfG2ZbLfY763ftLq7JyXyuwG/M6C/4SN/o8wFyABjbvmR3R07twE+hoMUhkd/tY0CdrYVaQAvyrbajNeXAH5nztCcHebwwulNAisv9zOO2MSkc4yzjqnUrY6vOsy64REBb5faxoRfizvXMsguL1m21Wa8FkFVWTo/uRNGP/7Lz3encyETlcVLDE8nmSwu113eO9rmMH3bXqWqXbthrRd183e7cV05uTugRazzNQPAA2BbpYWi7aXORnTi7WfeijvNp/UDOwCPdvK3tjpLNht5A/zgQH/dr76gAU7d0cm6d2ZL4FXAR7C3Ak5rM4Gbe1soE0ntp9XJtiJp/V3qLdkMeeEOPjfQM/Gdm+mmOEsB++0gqu8uZFbTVQdSFUTFMFEFkYwuVIqd5+NFgQiEuOrK+q8TZO15fdRMvczXuts4mB2i4mOxKsnuaGPIm80BGf32X/9brbFyi4+haXVj+k45mWv+khUx1ifsiqRE/bzn9aj9+kVtJ+zzd7uxVb/1gEC/7aOf1g3HVxgcoVd3f9DvXstpIF9UkHZL0KKcLNhD6Qc6u65etBdJ88ci2Pf+8INyMOnQFaDiS0iRWVDx3woiis4WmsnaHEYu3wTcbctSFwU9x8tHe5oE6Q45EbefrdNQBre0CZCGp6iAaIdvTmBoi1q7Zm9F9z5mUcdBgH7st/5OZYpRagN8htAuA9vYyUSSSBB0FROyAF4CruhsNViBzKSO7wIcceAcTPAJb4NAfVLxJWET209HTkJcLAAe6SBA68ofLNCMhyx5UWO4BfnFSa7LxCjGVmfc+FMm60MVaztWxg7AZXZm5jitD+Y2bo9GSYcpzTmmwyyAS7v0sQuf/IMPxMzcPtBf9TsvKitB12rsnEyHdWoslMnYyiy2m1MHF4xt6xIVVMD1EjmJSV2HDAhKlQRBApgYiwMTkqKTTQIxWVFffMK81SWGJ9h2WS4s3TrQMamcCVwUsBV0FmTyByx5UUFnRSZ7INPtHswR8BauaWPZWm1L7mU1LNyUoIHdeBcraWcsPjWSFsOEpZUNTiG1+ZO/39gMtwz0mz/2GZXzAAA4MxymCxtGAMmMg+xsnNVZDslaMLsZGmOqfhfbAE8i75QbVsEGyLogJ2AvjOeezZtgQ0Bti6mBvZRuLbx79OMvlXDOGBQPEBuUOLtYDMEcuMjLG9rbdXnvYyMcdDuaXe5GO7uFOmU8GXLS22W90n6uKNr1s7/XsxlukdF9OEfu+sDKZFbmM+t/uiAfUEWabdFbPZdkpijOzvinpu+DnDR50LwVUiCzaTaQi79JQK/fVtkttJKhrCRvH5EEWIHnfncTZLgloB/5439QJvHtsw8IX7IOspat83zm+QBFsHg2mSVe1pa96XYD07CIGwyANzlBscn08eAT2U1K5n3yYwy+OnQlyBoi9mbUfmiAb0m3AnSGcxPOzj6vE0xvaHp7AD2zwZp+g0yaDxB6KaNjA1gbE3UiwdCZRf3O0OxSJ7mHw1RFVwyrwn4qII72c7/97YtshlsA+qE/+SdjcwwM2kPSHmw+p7ExdVCZ3XnuIye4DG1IRSz1ueW7DU/G1jYGwm84kD3gCyCHnSiytj7apJUxXJJuDLSuxHtrg0Gl7QIDsAoeYstwLR7DbgLetVlAlIsiTwmSPWzIzYacjEs+cFGTCWurBy5CvE6/EZjczpFVTPqe/81vuxTqGwH9FX/6stJpMjhybYanNvAl/WZtTx4PE/rdb0wc5NkmpANmTZOrmNBhchJw3ImJ+451AGuTk/kZC/Fq/5HP9mk7XHana4d3X/5n/6zRYXhdWYck2KBsMC0fy1ZioGr22w7z26usVmcj5JvpXiJEWbXtj2WH9lxGFsPHHOdCv6X+Cx/91kvZDDdgdDjAcEaxXFVpEcWMU6awrDIumS45+DzM0V5OtNRp/Uq+kO12jPRMzDGI2eQJ4lqtOFad95HOFc2Fqd4NWkCY4IXf2A0yXBPoL3v6s2pLS/O8Ih5MZrVJcJBte10eXCDeuIjC/CacUdaeVMAc0BHw6ng7aVljS18gDog2AC8O0ybdnSbAhY9LvJ0gixZnGW142/umawFtbHZmxKBVzFGJDzRYtNay9XaNrIc645mHKKwlJaA6o/nMnVScspXVEk4ytZbmJA0o2kQHQTwfEYpmfO/tVL/i4xEkz24+9WtPyRaINtKVgf7Se/+q5rWtQ7QM5Mwe2kAjI49YpiLiMlMAEpOA6pTA69IzMpyRTjC5PEUIZ6x3cBRfGRQgaYDXyZ0DOB+7YLG699WFmZXpuonNZenKQHdbVBXswN07F9LZGXu0sK8AHhoZIFLaU1CbAVPJCnLJz6710xpfGW21WOPSzra9/aXln6siHaagZ8CFVesOv9TqCfDpX3nf3myGKwL9Jc+/4uGcswKc1T6QWRdP4Wyn58Ct+okaJQBVmATxnWSycJSAEfDiMBug5Na5O9RXD+/m7Q4zn9P7TTkRB/yKae/w7os/9Tm1GScf0jonw7Pw2pkvodxSCNd/2+H5MTSLeyV0rLbdOMYwr5Z3Y7ykDVUvazay1uIU4cVfeu+V2AxXYbSAvXnwJVp/VudWl5mzLZ0kiklIsAg0AXIW0lhb20TEo4JBAnzZp34HMMH6tfc5VT8AcW6RoV3dCVb9xlahYP2++JH3XBlk2BPoL/r7z6u9n8Pes9EeRucKOSYPnsv8hpPsJSAncJvDVLWyM/EQjg7wcKhpWwEXocpOOslRrsYdZbGRmXSS1017AR3b6HyQCninjaPdwKKinXEKB/5MiJ1pR/y9xWHGcWs96wiHtxihaB+hIOKhYPErCCqNKsYGH5gX6SS89AvvuhabYQ+g3/TSqxoRAJAPqkZjYyvLgC/KSQAuCuth8lQg2sVsk9XiE3eh/ZHnIAGoabnDB8Ach/8+OXpu400mz9oYXfMxlii/QdoJtHbe10EJNgd4vqxZOgwagOxYtKJ4/pggSE1W9RDNy1ThXEpbmru5WPbjJmpJvwWxEC7iZRoRBGnPlLtf+Mefuz6bYQfQ5y+/pogzxssUIQ66bY6b86j6vS0cUyUZY04SP/BrUtADjnU0yIVNmKBnwrRWNmP5kKllwO1FBF2drr/K5okbp63h3dm/vKYGDo0tYeplGRbhdrG81Ma5/GK01Mv27Xox3NPW9vjitwsfs65uhGeN7Qv9e9iXL45rGOjtv/zT77wRm+ESRm+EQ5VNAELPWiDetIA/x0oMcH9dBepEceZjUYl4qR1GBThFgmIZF3YDuTkKprJWNs5OAmCk/74kRuTvOudzYboIGdH80Ofln3jHjUGGLUCvXvmCYqu7geAPuQl4XJPhVMCK5/SsAB511SZqQ7+Fzc/A8nTQ7Jb0W9YeSeBOU2lyQvS5XU5QmM8o4R8+2NtJ2zVa7J+uQeLB6j167QbcSfpDKLkJAK++p8NEGOJlttg1wO0j97DTDOmowEZ783bAdfLXaSif/dHbYTMsAD197r4aK61zVj7W8aUkNDYH4OIPik/DSogvjYi52sNhssYdFW35q133uzho0UsZmwpzxNuzdjJop3U0wKEdHEW7Z3KjzclS2mS0kxLIJSVg/z1htgfbABxBpV5HXfXPDsTLG0Cp3wvMVaEASznyxAsaO+23EJ835DGp+hguyFUQR6LdufIcE2b2Jj3wyg8/cWtshgHo6dX7hkSiZXkFUHcy+IO7Fscg42cvJ5Uxdm3V9gM8GJ6OTkmGt3wDEhFSTuIQ69zAtoGoTXoQJQ//477/voVwbkwZ3smr98NtEIzeyFuFdrF2eINZNCZCgIE5SWhtaWnH7QVcv6Os5TNUyzOQsay0OdSZLjQ1GKXLL4VyKHz+w19/q2yGymjfmKCERLdZjjyk0wFN/aZ8n6GQGxCz0jxUqoAkYwHEGTkeWCW7vT1R8xXESip6qzDuMEXFIol6L3aSsz9bTJ4v3Vc/9PitgwwOtHzhdYdRvL/LAY+HTPRX5JtjYllGlS0OMx2PT0oFfFNOMJDSSdbwjd1yEhudWdBzse+2xSc7HsOJcqhkjB7nUHcALjEmY4GgdsIHJidFv1vzkiUC3ScG0aUBrtl+A1whIgmvo2oT0QAP8PHKkhOEt20OU4tue6zu/b72PYdhM8CZ3A82lxSzjCTLpCunmxytgG+TEyCiBUXdIQn5wXdpSxRcn0hJ6c6wPZ9OconhuN0yw/MEz8fY2HSYJKpKB3aFXUsmwL7ULvApBeXcIYrTyUVk4HWaI/X70VbYE+x2Owc826mObrAZ8+kA3QG//t1fe1CkJwB9+KHWSe1OSkbI1bho59dmUwomLxPJ8woVyL+ptLINR/x9pXzLPrlNhGzezrwS21BMUP/cRNpMQv939CwutyG1vIrX9zEcOmV4lwU72e0XusDwvim3KYWV3XE/bgfj5rjej+GsY7sdNrqxiiJvLC52fu/fvuttB0d6A+i8cR3At9mljRdeIiewEBtTrin14rrGw4rLgduMclKPVmfl37/z8CDDZcekLidy//XmrRS6PAKigc9mdBJgS2DoYZzvMEeHac3Zsm/hnzighqq6oQ3OHdzkwebanawI8X9eNr6E8vbyldUdpa2M7oyuw+56f6gzshsKw4H80DHLo+7+ciIYsze+24iYWuE/PvDVd4b0zneGwBXYDRl7e9E+7AbahgfIv68Rbcdyj82POnjBcC0rQ8kdplVRui9Lxex7khw+7cXoMV2J4WPzC9cjw6te5/1Bh61cM592g37L0v9tVOU/n3zsztgMezJ6TFdiuBSwxuR1DNNBv2cQo59dT+JHmurshTz79j5Svwu74+2OqriUKLla7jBdi9E1bWV3d62Zl9FuyI/shmBxMNNvh16HubIZoahfxDXG8P9691vvHOkbAx3pSnKyUc5GnUXA46XsLrC9rAshFRD473e85c5BhmtKx1K6kpw4Eh3gQx0j4+Aw8wYGcvnAHMFDOEnArUvpAT9SujVGj2mnpGibjw2bBfuR4cnksgHpwj9lw1n+zxNvPgqb4SAvbSxtPT+JaxE0/0VZvd/bm//ywjw/gfiTmUTezznwcxL1M5DDPel+6WCMrmknu8OujmUbwxfYDfSvpSq73fR/H3/0aGyGW9Toy9JW/YZOm+PNeZJ3wWabfhOhoNerH7YfU5sj3Qmjx7QzQrlMv8frkeFK9zJYFN54+1celc1wJKBhD7CdidcFPM5LLr7m+CDDEYGOdKl+A7lR6cq25KFtUgBmWD/2yP8DXdNuhzkAvgXsAFoR5rc+fBIgw9GDnpb04YckQ8II1yKFd5SCabUp+XxtdmLpZBhd05X1e8FmfsvpsBlOiNE1bWV3QifEi1ZdsjkpiC2dJKPHtPHtiQ6/B4bPj54Wm+EBATrSIuAD2HqCIMOJSse21J2fQJOJdJYniTHwgDG6piWHqY88dLJI38lZxyHS4vnJCaf/A7yQ0Ala9tJfAAAAAElFTkSuQmCC"></image></g><g class="cls-3"><image width="90" height="99" transform="translate(121.25 36.16)" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFoAAABjCAYAAAAb3RdnAAAACXBIWXMAAAsSAAALEgHS3X78AAAO2klEQVR4XtWbzet1VRXHv2uf+1MQokDNxExD1MpAyMd8Cfoznhw1bRA0cBRUA2kSURGRVFhkb1aOmjVrEDyE+LxqKiYSRophDaJBFHl3g73X2mutvc85+/7u2/ktuM+5Z5919stnf/d3n3t/96EYI85yPHj+QgSA55/9FM3lHjPorIJ+8PyFqucXFwx7NZewtGAFnzV5nBlF9wJeqqrPhKJbNlHFIvGWWLSie1XsIV/89fJUvUhFnxbwkmNRit4l4KWpOswlHCrYh3cBGQSce+zCbFWHjKNbxy5V3JVzpDiadXQDBvoAqpyozi/9chkWcnBFdwPuxePyopxTfx0HiIMquut5GOgD1AuYgEu/eLSnxr3GQRS9bxWXcnUhv51t80CxV0XvG3BSca1gez3F5Z8fV9V7Ad0NGOiD7AFLWRuy3gxLOeHyzx7paW0vsXPr6PbhnmhgmVLxGOCuydxz7EzR3SruHXTTJvIFKm/r61xOJocn4cpPjqPqrRXdDRjogzwKWF0cAyzX2pCPGadW9EaAgfmBTgEee5qoVKzecJ5WPxEigKs/fniuNzuPUyl6Ix+eG5IHbMpo3ibk2gRgAJFyXXP92VNspOh9qrgLsDufBKzLgs0BAVd/eFhVdyl6I8A93fcqNuc15C7AOocAEDVUncqPEZOKPhpgfb2h0FEVM2Cdm+tIZWTKrv3goZ5e7ySaoDcCDMxD9gBNmQLcUqfP1XnuvpaKQdmffVk+vvD9w8CurGMjyD1dVDmjgPPpqQGjsdkRENEqA8C5BwxR9E4Bu+uTgHNR15OEem+P2S4CZgDnf8iWvfjkJ+dGtHWsjg7Y5Lh8dRi1iQqmva8JGbq+uUHtJgJwBMgakCnLF5QNxPySe4gQm5DJQjb3ljpjSC8AiIEQA/DxLzzfhWCbWHW1MAV5U8BwNkHmhjpHjumNWES+1lKxfwqR3HxuJg4om+UeY/qv4Kozc9cilPK0Kl1urMrInEs96hI/skWC9DipVbXD98t7EsgxtCFHovRhhoD7Hr/YpbnTBsUYce68+9M8YT4YnHqfANc5VZ4HzDkjgOHKW5/0mj6sle8m3j+l8PnL33igZ/QbRwDcDwPHmiH3wgaQzb2qAhlknWN8OPtq8lQFiPS9hMqHVV2Sz3WEsirkfI+/cilVq/FX4cq7bYLzJMYg8csBJiQA3IaqvwWYIUseygcYDTjdT4ABnur66Bcv78VCBPToT6hUaQ9gzhE/ZRCh5E75sK6v+DDMa8wmBHyGGgcgDoS4IgUYRsGpn3zP/lQ9vxmiDzBMDtCyCJMjr4aKGYaDPG0TBN7s+NxYBvGkkZxLP6XedP6RL13ZuaoNaFE1NwpYlSkFSR6cQgH4ZQ5oSPxqA7YQynk34JCAFo/W9+rzUsbnMeR6sPuoFU3lbaVQda09ESqPr5uBYRqw+ChV3tn24RYolUe1ijVkOVebbAzJau554upOeTe/vTv32IVYFEpNwIBXcbloJ4iP7Uc1UJ4sfmSDqpfrzOcVYF8PkUyQXlWVTfB9wU54VBPCk/HaV+4vHdsimh598Ve8MZZBcmcApQwuz35qFAN9bEAmpTj1XGyXdR64sglZ3oS8oVHa8Ia8+TUVTAI1EgDO13XIBkllM93hxjhelbaAzKECzIOCUol5TdgECpAKSgOwUR8/QZjHNc5T5wHytKPPeQwxWBVH1S7n3vW1F3ZiIaOgLz3zKAlgKMBABp87xdfMa3qjqz+VwQCWwYLr8hOpPFXq0+elPukfq5TryPXLIyCviIHVTOAPSbuIycVhH+sggwScTWgAo4CpAMlKlYEwYLDKIDArFROVe3V9uj8tm+D78/l6IMRVkIks/UdRfl4xH/7mi1urehL0Zf65a+4EQBYg9LFPwQUkSn0o4EdtQqmY22t+6OB+OMACmSdXFIui9iHfp67JU9CWMQkagAHMg7JA2yr2S5rLCmBVZwbsNzoBlyGYjU8pH837dF9K3dpiSv+5X3DP0KkwBuDOb/9xK1V3/a7jE5/9Q3nckyNNPK5RVZ4815aXOu1gGU65lwQaUK+I5uOa5GlVUxuwbo/PkRWu7YQIb3z+Y9zLjWJe0QAu/9T9MFAPzh3N3+tkECqPYNQoj1Fyjcyg46DeU7mPAbRsIuZ6zPcr3E5QbQYuJ7VvsH9zLoqdDDh1dIEGoMBZEHpg/IHBlPGA2fc0YBqpl+vIoAxgbRMacEiqFP/lTW9F+UulecBF/SXX3EOE2596Zd4CGtEN+srTj5B4MWDVSlSXqeU+ClgmhQdY7o+5TlahmShWJul7qNQ3FIBlEl2u638MfF++BqjHvqzy/DpNdIMGIAMpj1MEv9kJZKhzoBpsAazuD3pwyEuVChCgrUwqOfyJz0zshIoFXkhtrgfC+oQQT2yunujbnn51Y1VvBPrqjx4mEKpPdUUZkHKjJh6sAG0AZoCmHq1iqm1CWY1MkkBpA5Z7NWC5l4BB9UWVi1efUtUbgQZglqhRsQczBlivgDxo8cbQqGcMsEAsdQAobTrAZjIHbgdlFei6cq5shhp8Pt76zJ82UnXX452P+z/3XNR/5RCYAMQi+DrlzqtzQE0OkO/PA9d5zm7GvLWyJUznljIGWvJlcnUfgmpfCQkEvP2Zu3OL07GxooH8K0zXCQD1gLlz0OdKgZQhg8sB2ejGlKnujwMclHRt0odzOX8YAjgHKadlP7pOHvegBNURpwINwEIm3SmIUsSH1aDBqmC/yyBS3ghgPUEMa0igeHKaNjEGeJUf+/Sml+sy6hfgKH0Y0v0xAPEk4ObfvN5lCacG/cL38g8D/awyENLnMApmsKJEhqIBq6XMgOpNzwIuFsBwFLycux5KvvbcSsXqfQyQ53Ep4yejlQfQjlODBpAH0gYkUFnFMmCtnHJurIbByARZtQEKsNyX7rVfddp+JXXz5FFRa3D5anOUDzxsd0hwdRs3/vbPs6reCvSL332QWoDKORXghOJrDNjDcEoDqybkwfGLrYZKrrcJ7fXI93M7MtmtPghkbicd1yf5dX0oeatynIutQAMNQOTPkYCH3Jk5wA0FJ0XlugkltwG4qdDctix3s+m5PrAguM6VujaUI7fF7bzvd29MqvpUj3c+7nv8YjQ+zBMsXgmBV60AQn6kU/fq/OByfTsjbbCNWNuqc4z1ZAXb8zxRlCdR2ivtluuEf376Q4RGbK1oAHjpW+eoAhV4oDCDaw0QSk06n33QQ7ZqRnuVqG/fROUMx62i5MPB5MvGx+odyL1s2ZotbSS6/vtbTxhVAZV6moDhJsflS72c5+sM7fpFiVWdVFQoeSN9ANJkTSmYqAgk9+U9z/01/uuhD+YaSuxE0QDwytcfIOnk0FCY9uFAonivSL3hlc2oqNvUSY36xT8b+byxBlW3Uvx6FeQLpfVJUbidID8x+Tr3dyR2BhooHS4eaDvW3OhkOVsl6onRfq2XvfZvqR8MGfUkKCDedtar0l6ZFBp9z0JZn4QkDABxRXj3OsIN196qNr6dbIY67v3ylThqFX4Zy/usREI6IQU41Pn+WVvUqeszbWeoesJ0fdy2XiXShsoLrm/s1SKWcvz3fbcSVOzMozm4ExquXWo1EPO98wRgDVDAVKuIVG6ZBHOPrm+gUqdbPb6cYccVqbphVh/79vWvvh3/c+8HiLnsXNEAcM8TV+vHvXzeel+pMvhcB5jfDyrHWVZrE6tWRS5f85+6+Bor10/AamQV6HZyPQz/v3fdQsCeQAPA3V+9FiuoWiW8cVRgVL66Vh7/0v3rrCYNhN+LTQR1Pejrtj6fV02UAlwgK7DqKHXk8//d8X4C9mAdHGYZEkYUg1HATRVr2Cj1jAEem+BeizDK1fUB8g2eV7BX+fDmO/Hd226mvSkaQPqBoAJmOz6mYFLqc9fkqUTVNwVYAQOpVbCpTQQqE+SswuwRrGrZNNNY1rfcRHtTNOAVx6DGAKfzSlmcN9SADRhdZ6hzfNveJuTxzwEWcTTVS2IT0o6DHIlAf/9H3KuiAaQfCMoAe1RMCjDS4PTyBaZVHFwdyqbiSajalIlctfuzHkip2IK0Y+LJ0HXn40037lfRAOyyYlhjUPzgiZUIAeD/fFVN0gy8ph8Pjf7wKhhTMTXG1lL5TTcSgP09dei48zsvxVEwatBGxSpfNh6v4Il6ypMFlYlS6m75cGsVrK8L9npLxbovDjDHQUADwB1PvhyrQWnAWk0CIPXVbDjq/jGlaiVOKj3U98tHcfJH1QetYO3JVAPm2L915NCbWWUlGor/pNZrE1zP0LjGE+oBcxu8CtQHF+u7DcDcxgxgjoMpGgBuf+qVWA1edbx8pNU59n2P3axPQplQzvc+7NQqf0khrVLXV+5Dp4p1HEzRgBpsA5B/hBpXsIOQIVVguZ0Jm+A6artQ/dhCxToOqmgg/UCwpc7KV2VDsoMXiKFA7gKswDZXk/y+o5HnlbwBYI6DKhpQyiWo5TwOuIJN7n4Nf0rBTqnWKnS921nEWBxc0UD6gWD1jVmnTRg/V/DtxFlI3gLWq9AAXE+yWUVbQAaOBBoAbnn2tdj6MDOrMjUJ4s1zG52GvqK+dnYEmOPg1sFRnjA6Bj7mxzOA/UQWW1HHPdhEK46maADpB4IOUgJbg1uvaB5wNVEKHNejN709WMRYHE3RQEthGlgBFzPkKrdSpl8JDT8muAnA3iEDR1Y0kH4gOLXbi8V0bnTej0f3gAMB5jiqogEY3603PK16ZxMhq73yZlJWo8GWOg8JmOPoigbyDwQ9JG8noYZlNjq1KsRqxlbKAQFzLAI0ALz393+JoPS1ZBfgEaspvq7rSfnHggwswDo44kDWJjo2uqjK1icJpNiEUvkxAXMsRtFA+oGg8d3Ax1ZZmQiGvETAHItRNOA2sBbgfM348QkVwBn8kgBzLErRAHDDtbeisQzZ7Ap8hszfO2s1LxEysDBFAxCfFrB+I9Q+jaL0pQLmWJyigfQDQbPZZcDmQ8jCFexjkaAB4LrX/xZjsKD19x1nBTDH4qyDw39o4e8skNGeJcjAghUNAMOb7yRVnxSmZw0wx2IVDeSNcSAgAvHmswmY4/+kE9rZ2xC1UQAAAABJRU5ErkJggg=="></image></g><g class="cls-4"><image width="132" height="132" transform="translate(39.25 -0.84)" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIQAAACECAYAAABRRIOnAAAACXBIWXMAAAsSAAALEgHS3X78AAAIXUlEQVR4Xu2dW5rbKgyART7vf6PnbGGGPiTOgA3oDtjmf2hTB4QkFCGwkwb47/8Ii8WH7fvqGBYRAF5A40khNb2tOgU3+I1fGeH47s/n78IY8dSYh7K7Pzq/zk/Fvi38AJ0kcEJ64dgsTD/dOcTJp1lFFOaJQoUt/DJ7E5qHWqNaJtrfpi5RWgg2TE1Bf1qw4mzhF2uSEE8v5BxFvAKwdJFwUtvADm86q/iuIRAC3kTOLhvRQ5M9AkB3x4oYrmNsLBmOymXpjTgOlj3QgCGO48kprU+g05H3kjFSMdXYf51LAZNOQHwF5ViGqPWgCWjWFRURW/ihCTdlwJDFTJhewjKMFQNsBwDyuBsICjlRRYsqhDawJx0SXZJwq/EWUDHTwXahSP62k4KDyB2S0ykwdcT8hNYwR7jjYw12mHKP8Ladx9GUgw/BQecAlKK3NKUx+0sNQQ4WWHiGIAwyNUP1T4teRJE0YDQ6k/pWGkWADYyKSizyumFjTn9aAcOxaV+6OH2StswlYxI4xh44B65CmCcCteIrfIvjakZqrFwAlCVjFJOq5QbBXiwLY3MZXwAQ2/XONTPEQgRlrov3Mkj3LpI27C2XlGRM7NNyayjzI6SRIWLx5YnQ4S6lxgOKrldCcn+ohLyGiKcXZa72URa641I0bCQcXbc9pI1Mu6erBIM3IGllO+R4oiZDcKnugm6WYUp0cjEFzJ3bqYAsKu9skUZ86UgYlYc26MtE6uBPTDkpi0XqiZoelL03hpONR9g273TSD4B8DnHQqKOCbA66ofaht7ULxs5kv6EuATxqCGNxbux6YvZTMkyNkb4Qjq3eZbAwFAWgSMEcGl9WAoD2bW0KjKZqewlj2WcILZOpg9F8BJFjSy0TcWRwKciuZIh3S3VEeuHppAHEsN+ljLnP07nxDpiPnPkyhCUC00Z8CLCzmBjg9Mzn8bjg9F3b5H2OTX/nEFHgvTsxsfnohL5Cfuu7ZEuoXD+An0PsIM1Qpe8I0XXuUM5ifj9zlDVN//GOmHsvGS0eZDb6Df8A3xWCeDB1IR400absRWX9HKLu2XdyGbVIzDXjVS/MpSaZLWDFZOVtrDLuGi8Xdb4bCn8QTirrnOa8WrCcsXsOwhaRVk1TFbMzgHqGcLajOG56iXsPwVnfaXC2k5UhyJ8eC6WRynj4j55Z2NgdXOl3hsDbjaNawyB8voNwaQbov30PLLQMUL4JIcPc0u4WBF0NzyEIo0lxEJ0GgyYwzsWxkbJGYri24dvOI8zml4Jr26tSHO/8An9GLOHaA8yisisCY97gHc3mCF2WQlOdECNt+03c1hIkofxlCNyP98fYB+jh3QvqX9DdL2tmWWDP2AwhULiGxm/D+GaYxBHpy8Yv5+2BdMwwfD/EbIzn3u0EMA1ID9BfCAyEA749Qoi2btSGZITy+JF9Q4S+q7H/HsQuNwCcxwj5NcNt52iMvTkLCrPw5yD2JemvjtwgKSoDgPpIOAPPeAtvmnNwfvOUIUg/FrLQMY2PCwHBfbi2uA4lmGaYAXzV57nlNpy//X0Ee/8AKs8rYLBxF3XSorL7OUQsxESi0NUzjBSS2dWgt/s08O9laCAMNSzDeILZNBE25xAWMqhgY3kFDDbuxHBcktcQlzG6VdUi/6ZCuenUFaYhzOY79G9uzYS3ygGAu/typaMqzSXD7TPS0UARiH6lwlfsq8l8gW87d6jtujFOoaLP0muiH0KjYiSoImbokiH+VFlj7QLkLiV3a81s/kZoEz1DzMQVdU5AfS6JAEwmEZttZ4qpPFNhXyT+7gpawxwsOLXPL5Ds/XR59gMyPXBwL/poHikCoKibfYZ4OjP4U6HDoYaIubApDmcY1jGaLsq0b39z/t+m2VjBIYK+ZKTtktel2OBuq7pAtdOAGc2nwt92Eto3Zc7gLYIN96fsBPYTU1+E3dB+IwIG0+lB0JeMGo3+orlV6mOyXAl0sBh2BvhLRgkLGRYEwimgFC+5k6HPECmWsjKIgrFm1ttobDw17gOckNcQNZAdyFAQWy2XGwtRI7BZMo54yOwA6gvpLGNyJ8J2ydjxkDkDmF2cgMFkDUIVEFX7TzIVg1wJxMzaj4NkV+PpRVf8HsN3EntlUF+3Mkwnf6oyRBUPmU8A81vxPkHhmoJVVF6Jol/TbV0SMew5eHew33YCCJRZmGAwlz5LRove4y1YnAKCsnMwOcA54CCyHzcKclENIemzmJDCPDosGVKBl84RDaT+GMOmUljRNSMA2AlTMIEKoyFnCNfPb6pDQR/LmsVQVH+Ic6VBVEP05go63oWt+e5dJgJLC3ex0wDyktENtT6lNed8KcP6wRkumH5NVJ1PqE8qB7vSBuT3ork1DLN5PwhTfYkaYjRP8lG7hrDgQc6swkkZg/01Xw0xAm8fYPI5AcMBG7eAW0B42XgfYvGlmAiC4vgwcPR8YmrRH4O59K8heqD3w/3ZkwfiK7clYzEZxHnOMwSxU7pSNffoRHkLHdzKoYX6HELbfzEX8hqCEgiWodsbin03xLeGaMqOcO2IGYnfpOkekFFz3gdnrHjJ6TBVvhlCy8y63ZQX1mDxLETbTk96rhKReFjTg552t1BvO6/Mk22vId92arjLRFh9rCfyx9xF5ezc0HdjMsRsjJzYWpZJdZJkIqFNk2SIKZQYA2p6KLRBO4kZfDC1wGnMT+mwt9SckmE+/VwyBGX8hRHI/MXPH9Q52agNF9eEO7/rpHKRsXYZd8Bw2XepIRbXZS0Zi4wVEIuMFRCLjBUQi4wVEIuM52w7PXZT3FMfbwxsXNtODTf03VoyFhkrIBYZKyAWGSsgFhkrIBYZKyAeSWV7FON6QOaZVGY9BPgHK/KZW2aWrhUAAAAASUVORK5CYII="></image></g><g class="cls-5"><image width="174" height="119" transform="translate(18.25 14.16)" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAK4AAAB3CAYAAACaPXsJAAAACXBIWXMAAAsSAAALEgHS3X78AAAIRElEQVR4Xu2d7YLjKgiGsSf3f6O7t7Dj+dGkYxM/QAEx4fkzu50EX5BQYmwnwJ+/EVKO/wVwLPE9S49nKwYknn4e/9h/nPM6viZl+rITKiyc2fyk2S2yhZ/EQ6qzx/EvgPATAX6qR38lfXxVj3wG1HjfEWIMjgtoC//SlwlW0kNbCXsmBAiocwh6tDAoSRSj/m7wg1AWud8q4rUF4SICxP8aajND8vo3kWw4mWM8i8SNLfyTdyoA6MUu7m0LAUtti2qspBH0Y8O9ZR9UlAiKPHOpjoNj02IApxtRxOCIQ2aSfbcxrnnLVieLotk04QzVWofwE7FmAOCc6JMg6MWBN4jyHm8OAJRahSyThiXDoJPSunSvuOCHkEdYSwByq3ACJRB1kC4GJaVU5ySnndq6jCBsvshp3A2EKy7qbaIHWdnCnMQP+BJfAVArQzuk+cCb7SB+/aCS73GlUBqKNDmcKPmXIj1/sRJMUpyZZeZbBeZBeBAWJWCeNLHaIP2l+nDpzwvjoO0Wzp93c9bLYnJFYIhBPnHGDVPvmWLIKElkhPMLO3ytApOZ2+FxKRMBAjJAR3pHOFYVvvYqOI5tPptsqnekEdmLZEx0rUVqcnk7clDgCqQ4lXXck0Ki4KOSY5+9054uIQxiYDJzJ7gfp0tB63EJh6LZbbZ1nH5PSnQEreGfxLRY4AeutgpTr77WWM1E/+ZoXVDpTjN9HxbyG//IV8GpZlIVNbTFof3cubQu7SGm04xfDZR/qINUaLcKdrRekdL2wrQuO8jDTDPZh54LrlxxJztTpiCMUy+xOpPJLbqroRA/BX57XCXholOm5MM4tHc5zNIiW1wtxzDRNukBhGB0BE0DMCYIga85Qvh3XVpEnJSj8zTWGBU0tHvc1biZOyhOPpPnFFHRAUA/tpXxtu9+7n0k6xWjiXZgrUKNA7WnR+4AG6Jhc/2KOyh/2YsUSxwM0In3xvXWUTvJ0F1xrmhfL3EXk2sGRNwwySWdL5eN6zGvC/8AwiKyMVyUCUFhHBJz8QBUHvmGkpjkdcwyDTuFK9BRhDFRe8FX3LPYkDxGrfQi6e4w2g4wYxiYrCsmRVVpZgDSpf4eN3dazVSQ749ug4epyfX7cYWChmk9RgbPfnbppgx5igox6qCp9FXcjlNESHRgP7v0gdqfE80/hklx2dBrcjtdV/sk564kQs6Puhsac/15VyxKmIkRJwSnCIcC9FZcTZTllZKR+jH+2hdpqEKTLYJEKLav3tOAk2Io+/aeLMKglnt0ghvj4AbDfSN5CwYTvUydbla/aca4KzqzuV9obqHBr+NqIOTkktRi8aqs0lDhssMFSk/EV1yxK1ISnGvrIbGHOo0VdsVlRnz3MW1V3NUQnLiphYJ4YaDblkK8sKenbP1vOacTu+04KAzHl5x4tROQfqJbhakwSCQH13nDEPthAlx0eKvwhYVZMsqs0ISwj70LOHpciBEgdlSkjCNpr0O258xhVkKiyQvsq7ilJvvzOjIalhfde0G6PgfT4khs57206f5ZcdKxEWPGV1LJEcc7SkyYi76K28ugg+TvgLhbQR+M35247sdNKM771zmGo5lKw8jEfEKjagcziAMAw6GyvzvsjJTcAOSvLl2ehd2tVtwDRB3Kg7BthqLWDidyp2CqeUrHsLbpdKhw2noVVwKNEBD38/ZXC0WILnGCqrgHarEkaLot1BhQJodq2yALbyRfSKxFqag1dAHhTCa79ipgXDYB3bXnQP1OMYaNMZz07w7rPc+RR2RuiEZRFb1CdrjfF7sq7sGgtDXpD9djiAE+FR2VIx0x7a+4vWiP5+ixzy0qWQchf6+C48wmAMAWqE36AfY0jcvvjmDj+1BI67hZaudndq4/FgNxuFMNGe9xa+eP2v5QMDR658oFm59PYTxgl/24KDpOEYGqfTTPicM5crRbhdbvYTwf1ED48iEC/mPXGQZOdVpE7Y3ki/Fpo7AJ79naDzbGO+2Ki4LFiF0S96q5SQzD9cuoiQZWARs/Av3LYU9AODTk2HPNuhREd0ZgqriLs0IMLC4tTtRzi8RtFqKLjws6LSwZ8zc0vo6Il3+osm6rsKhsq5Dz4Mhi4mlc2Ki4FjQ4NE5z1qrXXFN8jDP+5AyAT5WjiO6ktRL7wyHrq3U5aY2D+3EdR4xG69LVKlSuBefBoKsqA1t2MEI2ksWSTzhB0Obcl66Ke4Fig3IsRDs7wEYh+e2UeQeyb3eYFDkpVH03yXNViCG2AE/FJSCeVyV/Cq+P7ADTZBGZYxBykWc5bGGe7v+qbK0DiviEt/HlFzHUWwUS7NrYDdahDid1I0rVUYTN0DDZmzOh8K2H9jytfiNKlD/C43vc5Ujnizp31hK9RsM3262Cw8uN5rr/5izHjQKjwUoFEEPP9PfGwFCroCBEYYjHEkIhCU9BZ5oDW0/OnCurTI9yHnmP6yzJtcc9JzLiE9SXQ3oblxJ+cS0D99SXaPe4rd9naNp0nEHoqwrcSal1ifbA7avDhnKPmxlMfHzLVwYW8SAtxyYaFEHTeIgitPOcKM95o1xxF8DjsQT0HlcLwwk0UpTZV1xKGI7fwUgo2qsKDisebx7sVlwuVk6UkZLEicEYeo9rGZ+bIvevuBI8NaF63wEE4qVUcVUGcaQxNI2y67hOP8ampbfYUvisuCD2w/iqgmMGSi6+Wgc4jkU8cZ0lKa8qEMo2O9SGaqZWZwpKqwpELGpyTOGtgrMknrjOknjiOkviiessyRp7Ffxm7RfqikuOG8TT5qqCU8bnCwC8VXAWxRPXWRJPXGdJPHGdJfHEddYjxsKfRHUcy4QA/wM0JGxAD1YgyQAAAABJRU5ErkJggg=="></image></g><path class="cls-6" d="M256.56,127.2v-.08a1,1,0,0,0-1.62-.53,26.11,26.11,0,0,1-18.42,7.14q-11.31,0-18.46-6.46a20.64,20.64,0,0,1-7.14-16q0-12,8.91-18.42t25.5-6.42h9.36a1,1,0,0,0,1-1v-4a13.18,13.18,0,0,0-3.26-9.33q-3.27-3.5-9.92-3.5A14.86,14.86,0,0,0,233,71.6a9,9,0,0,0-3.48,5.34,2.45,2.45,0,0,1-2.37,2H215.11a2.42,2.42,0,0,1-2.4-2.76,19.27,19.27,0,0,1,3.89-8.8,27.37,27.37,0,0,1,11.13-8.43,39.22,39.22,0,0,1,15.72-3q13.17,0,21,6.63t8,18.63V115q0,10,2.75,16a.93.93,0,0,1,.09.4h0a1,1,0,0,1-1,1H262.93A6.48,6.48,0,0,1,256.56,127.2Zm-16.93-7a19.17,19.17,0,0,0,9.4-2.43,16.22,16.22,0,0,0,6.5-6.29,1.05,1.05,0,0,0,.13-.49V98.07a1,1,0,0,0-1-1h-8.11q-9.37,0-14.09,3.26a10.58,10.58,0,0,0-4.71,9.23,9.86,9.86,0,0,0,3.22,7.74A12.49,12.49,0,0,0,239.63,120.2Z"></path><path class="cls-6" d="M290.21,37.77a9,9,0,0,1,2.46-6.45q2.46-2.57,7-2.57t7.08,2.57a8.85,8.85,0,0,1,2.5,6.45,8.65,8.65,0,0,1-2.5,6.35q-2.49,2.54-7.08,2.53t-7-2.53A8.74,8.74,0,0,1,290.21,37.77Zm9.85,94.57h-.76a8,8,0,0,1-8.05-8V59.68a2.41,2.41,0,0,1,2.41-2.41h6.4a8,8,0,0,1,8.05,8v59A8,8,0,0,1,300.06,132.34Z"></path><path class="cls-6" d="M336.35,132.34h-.77a8,8,0,0,1-8-8V28.19A2.41,2.41,0,0,1,330,25.77h6.4a8,8,0,0,1,8,8.05v90.47A8,8,0,0,1,336.35,132.34Z"></path><path class="cls-6" d="M439.79,119.13a3.69,3.69,0,0,1-.82,2.3q-4.46,5.53-13.09,8.76a60.15,60.15,0,0,1-21.19,3.54,39.44,39.44,0,0,1-21.1-5.69,37.88,37.88,0,0,1-14.29-16.1,54.4,54.4,0,0,1-5.13-23.87V75.31q0-21,10.61-33.2T403.3,29.93q15.6,0,25.11,8a31.52,31.52,0,0,1,10.52,17.51,3.59,3.59,0,0,1-3.53,4.38h-1.26a3.57,3.57,0,0,1-3.5-2.7q-2.22-9.15-8.51-14.18-7.17-5.74-18.76-5.74-14.16,0-22.41,10.07t-8.26,28.4V87.56a50,50,0,0,0,3.92,20.38,31.06,31.06,0,0,0,11.24,13.71,29.72,29.72,0,0,0,16.83,4.86q11,0,18.94-3.47a22.56,22.56,0,0,0,6.59-4.22,3.51,3.51,0,0,0,1-2.53V95.22a3.61,3.61,0,0,0-3.61-3.61H407.74A3.61,3.61,0,0,1,404.13,88h0a3.61,3.61,0,0,1,3.61-3.6h28.44a3.6,3.6,0,0,1,3.61,3.6Z"></path><path class="cls-6" d="M472.54,95.46v32.61a4.27,4.27,0,0,1-4.27,4.27h0a4.27,4.27,0,0,1-4.27-4.27V35.59a4.27,4.27,0,0,1,4.27-4.27h30.15q15.75,0,24.87,8t9.12,22.13q0,14.22-8.77,22T498.21,91.2h-21.4A4.27,4.27,0,0,0,472.54,95.46Zm0-15.75A4.27,4.27,0,0,0,476.81,84h21.61q12.42,0,18.94-5.9t6.52-16.47q0-10.5-6.49-16.71T499,38.54H476.81a4.27,4.27,0,0,0-4.27,4.26Z"></path><path class="cls-6" d="M616.43,38.54H589a3.61,3.61,0,0,0-3.61,3.6v86.59a3.61,3.61,0,0,1-3.61,3.61h-1.32a3.61,3.61,0,0,1-3.6-3.61V42.14a3.61,3.61,0,0,0-3.61-3.6H545.94a3.61,3.61,0,0,1-3.61-3.61h0a3.61,3.61,0,0,1,3.61-3.61h70.49A3.61,3.61,0,0,1,620,34.93h0A3.61,3.61,0,0,1,616.43,38.54Z"></path></g></g></svg>
				
               </div>
               <div class="col-12 text-center mt20 mt-md60">
                  <div class="f-md-24 f-18 w500 text-center white-clr pre-heading lh150">
                  Finally Aweber, GetResponse &amp; MailChimp Killer Is Here ...
                  </div>
               </div>
               <div class="col-12 mt-md50 mt20 f-md-44 f-28 w700 text-center white-clr lh150 main-heading">
                
                  <span class="blue-gradient"> World’s First ChatGPT AI  -  Powered Email <br class="d-none d-md-block">Marketing App</span> Writes, Design &amp; Send <br class="d-none d-md-block"> Unlimited Profit Pulling  <span class="blue-gradient">Newsletter</span><span class="cursor typing">&nbsp;</span> <br class="d-none d-md-block"> <span class="under"> with Just One Keyword </span>for Tons of Traffic, Commissions, &amp; Sales on Autopilot                
                  
               </div>
               <div class="col-12 mt-md50 mt20 text-center">
                  <div class="post-heading f-16 f-md-24 w500 text-center lh140 white-clr text-capitalize">
                  Say Goodbye to Expensive Email Marketing Apps, Writers, &amp; Freelancers. Even A Kid Can Start &amp; Make 6 Figure Email Marketing Income with MailGPT. No Monthly Fee Ever…
                  </div>
               </div>
               <!--<div class="col-12 mt-md30 mt20 text-center">-->
               <!--   <div class="f-16 f-md-24 w500 text-center lh140 white-clr text-capitalize">-->
               <!--   Say No To Expensive Email Marketing Tools / Autoresponders, Email Writers & Designers-->
               <!--   </div>-->
               <!--</div>-->
               <div class="col-12 mt20 mt-md80">
                  <div class="row">
                     <div class="col-md-7 col-12">
                        <!--<img src="assets/images/product-box.webp" class="img-fluid d-block mx-auto">-->
                         <div class="col-12 responsive-video mt20 mt-md50">
                           <iframe src="https://mailgpt.dotcompal.com/video/embed/xajnf33ejg" style="width:100%; height:100%" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                        </div> 
                     </div>
                     <div class="col-md-5 col-12 mt20 mt-md0">
                        <div class="key-features-bg">
                           <ul class="list-head pl0 m0 f-20 lh150 w400 white-clr">
                              <li> <span class="list-highlight">	Create 1000s Of  </span> High-Converting Emails  <span class="list-highlight">With Just One Keyword </span> </li>
                              <li> <span class="list-highlight"> 	Just Enter Keyword &amp; MailGPT AI Plans And  </span> Writes Email Content For You</li> 
                              <li><span class="list-highlight">	Send Unlimited Emails</span> To Unlimited Subscribers. </li>
                            
                              <li> <span class="list-highlight"> 	AI Enabled Smart Tagging </span>For Lead Personalization &amp; Traffic</li> 
                              <li><span class="list-highlight"> Collect Unlimited Leads  </span> With Inbuilt Forms </li>
                              <li> <span class="list-highlight"> 	Design Newsletter &amp; Autoresponder </span> Mails Using The Power Of AI</li>
                              <li> <span class="list-highlight"> Free SMTP </span> For Unrestricted Mailing </li>
                              <li> <span class="list-highlight"> 	Upload Unlimited List  </span> with Zero Restrictions </li>
                             
                              <li> <span class="list-highlight"> 100% Control </span> on your online business</li> 
                              <li> <span class="list-highlight"> 	100+ High Converting Templates  </span>For Webforms &amp; Email </li>
                              <li> <span class="list-highlight"> 	No Monthly Fee Forever. Pay One Time</span></li>
                              <li> <span class="list-highlight"> 	GDPR &amp; Can-Spam Compliant</span></li>
                              <li> <span class="list-highlight">Commercial License Included </span></li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
	  <div class="gr-1">
      <div class="container-fluid p0">
         <picture>
            <source media="(min-width:768px)" srcset="assets/images/mailgpt.webp">
            <source media="(min-width:320px)" srcset="assets/images/mailgpt-mview.webp">
            <img src="assets/images/mailgpt.webp" alt="Steps" style="width:100%;">
         </picture>
      </div>
   </div>

  	<!-- Bonus Section Header Start -->
	<div class="bonus-header">
		<div class="container">
			<div class="row">
				<div class="col-12 col-md-10 mx-auto heading-bg text-center">
					<div class="f-24 f-md-36 lh160 w700">When You Purchase Aicademy, You Also Get <br class="d-lg-block d-none"> Instant Access To These 20 Exclusive Bonuses</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Bonus Section Header End -->

	
	<!-- Bonus #1 Section Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
			 <div class="col-12 text-center">
			 </div>
			 <div class="col-12 mt20 mt-md30">
				<div class="row d-flex align-items-center flex-wrap">
				   <div class="col-12 col-md-5 order-3 order-md-0 text-center">
					  <img src="assets/images/bonus1.webp" class="img-fluid">
				   </div>
				   <div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				  <div class="text-md-start text-center">
				  <div class="bonus-title-bg mb-4">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 1</div>
				</div>
				  </div>
					  <div class="f-22 f-md-34 w600 lh160 bonus-title-color mt20 mt-md30">
					  Niche Marketing Pro
					  </div>
					  <ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
						<li>Finding the right niche for you is crucial to your success!</li>
						<li>If you want to make money online, there are many techniques to do it. But the thing is that, on every technique to apply, market saturation is always an issue.</li>
						<li>The good news is that, as time passed by internet marketers and online business owner a way to at least walk a different path to avoid this huge competition and dominate the market.</li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #1 Section End -->

	<!-- Bonus #2 Section Start -->
	<div class="section-bonus">
	   <div class="container">
		
			 <div class="col-12 mt20 mt-md30">
				<div class="row align-items-center flex-wrap">
				   <div class="col-12 col-md-5 order-3 order-md-3 text-center">
					  <img src="assets/images/bonus2.webp" class="img-fluid">
				   </div>
				   <div class="col-12 col-md-7 mt-md0 mt20">
				   <div class=" text-md-start text-center">
				<div class="bonus-title-bg mb-4">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 2</div>
				</div></div>
			
					  <div class="f-22 f-md-34 w600 lh160 bonus-title-color mt20 mt-md30">
					  Google Plus Mastery 
					  </div>
					  <ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
					  	<li>Discover how to get the most out of Google Plus and gain exposure for your brand! </li> 
						<li>Debuting as a social networking element the Google plus site is something that was launched to rival the Facebook popularity. </li>
						<li>Launched only recently it is a fairly new introduction to the internet world and should be explored for its potential to providing a good platform for internet marketing.</li>
					</ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #2 Section End -->

	<!-- Bonus #3 Section Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row ">
		
			 <div class="col-12 mt20 mt-md30">
				<div class="row align-items-center flex-wrap">
				   <div class="col-12 col-md-5 order-3 order-md-0 text-center">
					  <img src="assets/images/bonus3.webp" class="img-fluid">
				   </div>
				   <div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				   <div class="text-center text-md-start">
				<div class="bonus-title-bg mb-4">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 3</div>
				</div>
			 </div>
					  <div class="f-22 f-md-34 w600 lh160 bonus-title-color mt20 mt-md30">
					  	220 Success Principles
					  </div>
					  <ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
					  	<li>These 220 success principles will shape your business and your life in a positive way. You can use these principles in blog posts, articles, videos, as bonuses, or even as a stand alone product that you sell! </li>
						<li>When it comes to life and business, it is no coincidence that some people always seem to fail while others always seem to flourish. For sure, chance plays a role in everything. But as individuals, as business-owners, as thinkers, and as parents, we have a significant degree of control over our lives. </li>
						<li>Now, we can use the control that we have to influence outcomes in bad ways. Or we can use it to influence outcomes in our favor; and in the favor of those we care about most.</li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #3 Section End -->

	<!-- Bonus #4 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row ">
		
			 <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
				<div class="row align-items-center flex-wrap">
				   <div class="col-12 col-md-5 order-3 order-md-3 text-center">
					  <img src="assets/images/bonus4.webp" class="img-fluid">
				   </div>
				   <div class="col-md-7 col-12 mt20 mt-md0 ">
				   <div class="text-md-start text-center">
				<div class="bonus-title-bg mb-4">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 4</div>
				</div>
			 </div>
					  <div class="f-22 f-md-34 w600 lh160 bonus-title-color mt20 mt-md30">
					  		Membership Site Formula 
					  </div>
					  <ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
					  <li>Membership sites have become a popular way for online business owners to generate substantial recurring income with as little as ten hours of work a week.</li>
					  <li>Creating a membership site for your business is an excellent way for you to create recurring revenue, build a large following of loyal customers, and become an authority in your industry. </li>
					  <li>One of the most critical benefits that you gain from starting a membership site is the opportunity it can provide you to establish your reputation and your brand within your industry. </li>
					</ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #4 End -->

	<!-- Bonus #5 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row ">
		
			 <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
				<div class="row align-items-center flex-wrap">
				   <div class="col-12 col-md-5 order-3 order-md-0 text-center ">
					  <img src="assets/images/bonus5.webp" class="img-fluid">
				   </div>
				   <div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				   <div class="text-md-start text-center">
				<div class="bonus-title-bg mb-4">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 5</div>
				</div>
			 </div>
					  <div class="f-22 f-md-34 w600 lh160 bonus-title-color mt20 mt-md30">
					  List Building Essentials 
					  </div>
					  <ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
					  	<li>Uncover the powerful tools that will eliminate guesswork and virtually guarantee success!</li>
						<li>Find out how savvy email marketers use free resources to automate their online income!</li>
						<li>How to instantly save time and money by following a powerful system proven to work!</li>
						<li>What you need to know about choosing an autoresponder provider, hosting and more!</li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #5 End -->

	<!-- CTA Button Section Start -->
	<div class="cta-btn-section dark-cta-bg">
	   <div class="container">
		  <div class="row">
			 <div class="col-md-12 col-md-12 col-12 text-center">
				<div class="f-md-30 f-18 text-center mt3 white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
				<div class="f-md-38 f-22 text-center purple lh120 w700 mt15 mt-md20 orange-clr">TAKE ACTION NOW!</div>
				<div class="f-md-22 f-17 lh120 w600 mt15 mt-md20 white-clr">Use Special Coupon <span class="w800 orange-clr">"SUPERADMINOFF"</span> For <span class="w800 orange-clr">$7 Off</span> On Full Funnel </div>
			 </div>
			 <div class="col-md-12 col-12 text-center mt15 mt-md20">
				<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
				<span class="text-center">Grab Aicademy + My 20 Exclusive Bonuses</span> <br>
				</a>
			 </div>
			 <div class="col-12 mt15 mt-md20 text-center">
				<img src="assets/images/payment.png" class="img-fluid">
			 </div>
			 <div class="col-12 mt15 mt-md20" align="center">
				<h3 class="f-md-28 f-20 w500 text-center white-clr">Coupon Is Expiring In... </h3>
			 </div>
			 <!-- Timer -->
			 <div class="col-12 text-center">
				<div class="countdown counter-white white-clr">
				   <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Days</span> </div>
				   <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
				   <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
				   <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
				</div>
			 </div>
			 <!-- Timer End -->
		  </div>
	   </div>
	</div>
	<!-- CTA Button Section End -->

	<!-- Bonus #6 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
		
			 </div>
			 <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
				<div class="row align-items-center flex-wrap">
				   <div class="col-12 col-md-5 order-3 order-md-3 text-center">
					  <img src="assets/images/bonus6.webp" class="img-fluid">
				   </div>
				   <div class="col-md-7 col-12 mt20 mt-md0">
				   <div class="text-center text-md-start">
				<div class="bonus-title-bg mb-4">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 6</div>
				</div>
					  <div class="f-22 f-md-34 w600 lh160 bonus-title-color mt20 mt-md30">
					  Generating Big Traffic Using Link Exchanging
					  </div>
					  <ul class="bonus-list f-18 f-md-20 lh160 w400 mt20 mt-md30 p0">
					  	<li>Web marketing does not have to be tedious and expensive! Attract targeted traffic to your site! All you need is to know the secrets of powerful link exchange. Learn some quick link exchanging tactics to earn high ROIs at low investment!</li>
						<li> Developing a standalone website does not make much sense these days. With the increasing number of websites worldwide, individual websites tend to suffer from decreasing traffic. Webmasters have tested so many different techniques to attract visitors to their websites and gradually, these techniques have emerged into strategies that people use in order to generate traffic on a regular basis.  </li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #6 End -->

	<!-- Bonus #7 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
		
			 <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
				<div class="row align-items-center flex-wrap">
				   <div class="col-12 col-md-5 order-3 order-md-0 text-center">
					  <img src="assets/images/bonus7.webp" class="img-fluid">
				   </div>
				   <div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				   <div class="text-md-start text-center">
				<div class="bonus-title-bg mb-4">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 7</div>
				</div>
			 </div>
					  <div class="f-22 f-md-34 w600 lh160 bonus-title-color mt20 mt-md30">
					  Consulting Wizardy 
					  </div>
					  <ul class="bonus-list f-18 f-md-20 lh160 w400">
					  	<li>If you're looking to make some fast cash, or you're interested in building a long-term sustainable business, consulting is one of the most lucrative opportunities available online.</li>
						<li>The dictionary defines consultant as: “a person who provides expert advice professionally.”  </li>
						<li>As a consultant or coach, you'll be responsible for guiding your students or clients through a learning curve until they've accomplished a specific goal. </li>
					</ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #7 End -->

	<!-- Bonus #8 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
		
			 <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
				<div class="row align-items-center flex-wrap">
				   <div class="col-12 col-md-5 order-3 order-md-3 text-center">
					  <img src="assets/images/bonus8.webp" class="img-fluid">
				   </div>
				   <div class="col-md-7 col-12 mt20 mt-md0">
				   <div class="text-center text-md-start">
				<div class="bonus-title-bg mb-4">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 8</div>
				</div>
			 </div>
					  <div class="f-22 f-md-34 w600 lh160 bonus-title-color mt20 mt-md30">
					  20 Online Business Ideas
					  </div>
					  <ul class="bonus-list f-18 f-md-20 lh160 w400">
					  	<li>Discover 20 Online Business Ideas You Can Start Today So You Can Have The Freedom To Work Anywhere!</li>
						<li>Without an idea, there is no chance to start your own online business. With tons of entrepreneurs out there this first step is one of the hardest challenges, where you might find yourself wandering around the web to get your creative juices flowing. </li>
						<li>Don't look any further, as we put together a list of 20 online business ideas which you can start tomorrow. Obviously, there is a need for tremendous preparation and research, but hopefully, inside this list can get you started.</li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #8 End -->

	<!-- Bonus #9 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row align-items-center flex-wrap">
		
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-12 col-md-5 order-3 order-md-0 text-center">
					  		<img src="assets/images/bonus9.webp" class="img-fluid">
				   		</div>
				   		<div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				   			<div class="text-md-start text-center">
								<div class="bonus-title-bg mb-4">
				   					<div class="f-22 f-md-28 lh120 w700">Bonus 9</div>
								</div>
			 				</div>
					  		<div class="f-22 f-md-34 w600 lh160 bonus-title-color  mt20 mt-md30">					 
							  Creating Digital Products 
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 lh160 w400">
					  			<li>In the main report you will learn what digital products are and why it's a great business model. You will learn how to create a digital product that people need and want.</li>
								<li>3 Options For Selling Your Digital Products. Defining The Target Audience For Your Info Product. How To Come Up With Info Product Ideas That Sell. How To Launch A Digital Product. </li>
							</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #9 End -->

	<!-- Bonus #10 start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row align-items-center flex-wrap">
		
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row">
				   		<div class="col-12 col-md-5 order-3 order-md-3 text-center">
					  		<img src="assets/images/bonus10.webp" class="img-fluid">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
				   			<div class="text-md-start text-center">
								<div class="bonus-title-bg mb-4">
				   					<div class="f-22 f-md-28 lh120 w700">Bonus 10</div>
								</div>
			 				</div>
					  		<div class="f-22 f-md-34 w600 lh160 bonus-title-color mt20 mt-md30">
							  Killer Headlines 
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 lh160 w400">
							  	<li>Learn How To Write Attention Grabbing Headlines!</li>
								<li>If you have an online business, and even if you have a brick and mortar store with an online presence, you should be marketing to your customers and prospects through email. It is one of the most effective ways to increase sales and profits.  </li>
								<li>You have to earn the click that opens the email and you earn it with an interesting email subject line. !Think about that each time you sit down to write an email to your subscribers.</li>
							</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #10 End -->

<!-- CTA Button Section Start -->
<div class="cta-btn-section dark-cta-bg">
	   <div class="container">
		  <div class="row">
			 <div class="col-md-12 col-md-12 col-12 text-center">
				<div class="f-md-30 f-18 text-center mt3 white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
				<div class="f-md-38 f-22 text-center purple lh120 w700 mt15 mt-md20 orange-clr">TAKE ACTION NOW!</div>
				<div class="f-md-22 f-17 lh120 w600 mt15 mt-md20 white-clr">Use Special Coupon <span class="w800 orange-clr">"SUPERADMINOFF"</span> For <span class="w800 orange-clr">$7 Off</span> On Full Funnel </div>
			 </div>
			 <div class="col-md-12 col-12 text-center mt15 mt-md20">
				<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
				<span class="text-center">Grab Aicademy + My 20 Exclusive Bonuses</span> <br>
				</a>
			 </div>
			 <div class="col-12 mt15 mt-md20 text-center">
				<img src="assets/images/payment.png" class="img-fluid">
			 </div>
			 <div class="col-12 mt15 mt-md20" align="center">
				<h3 class="f-md-28 f-20 w500 text-center white-clr">Coupon Is Expiring In... </h3>
			 </div>
			 <!-- Timer -->
			 <div class="col-12 text-center">
				<div class="countdown counter-white white-clr">
				   <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Days</span> </div>
				   <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
				   <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
				   <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
				</div>
			 </div>
			 <!-- Timer End -->
		  </div>
	   </div>
	</div>
	<!-- CTA Button Section End -->

<!-- Bonus #11 start -->
<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
	
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row align-items-center flex-wrap">
				   		<div class="col-12 col-md-5 order-3 order-md-0 text-center">
					  		<img src="assets/images/bonus11.webp" class="img-fluid">
				   		</div>
				   		<div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				   			<div class="text-md-start text-center">
								<div class="bonus-title-bg mb-4">
				   					<div class="f-22 f-md-28 lh120 w700">Bonus 11</div>
								</div>
			 				</div>
					  		<div class="f-22 f-md-34 w600 lh160 bonus-title-color mt20 mt-md30">
							  Animated GIFs 
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 lh160 w400">
							  	<li>Discover How To Create Funny, Creepy Or Even Useful Animated GIF Files!</li>
								<li>Graphics is very important in creating a webpage or to attract and engage to your prospective leads. That's why picking the right image is the key to build relationship and influence other people online.</li>
								<li>The is that, if you are not that good at graphic design, you might want to hire someone to do it for you and it costs a log of money.</li>		
							</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #11 End -->


	<!-- Bonus #12 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
		
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row align-items-center flex-wrap">
				   		<div class="col-12 col-md-5 order-3 order-md-3 text-center">
					  		<img src="assets/images/bonus12.webp" class="img-fluid">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
				   			<div class="text-md-start text-center">
								<div class="bonus-title-bg mb-4">
				   					<div class="f-22 f-md-28 lh120 w700">Bonus 12</div>
								</div>
			 				</div>
					  		<div class="f-22 f-md-34 w600 lh160 bonus-title-color mt20 mt-md30">
							  Passive Cash Profits 
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 lh160 w400">
					  			<li>Make $500 - $1000 per month in passive income by Creating Simple Membership Sites!</li>
 								<li>There may be some tools that you might need to invest in but it is up to you. If you know of some free options that you can use instead of the tools that I have used then use them if you like it's up to you. </li>
								<li>The important thing to take away from this report though is the whole system I use for creating membership sites, not just a tool I use or how I do one particular thing.</li>	
							</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #12 End -->

	<!-- Bonus #13 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">

			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row align-items-center flex-wrap">
				   		<div class="col-12 col-md-5 order-3 order-md-0 text-center">
					  		<img src="assets/images/bonus13.webp" class="img-fluid">
				   		</div>
				   		<div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				   			<div class="text-md-start text-center">
								<div class="bonus-title-bg mb-4">
				   					<div class="f-22 f-md-28 lh120 w700">Bonus 13</div>
								</div>
			 				</div>
					  		<div class="f-22 f-md-34 w600 lh160 bonus-title-color mt20 mt-md30">
							  Hashtag Traffic Secrets 
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 lh160 w400">
					  			<li>Discover the inside secrets to drive masses of traffic to your website and business using the power of Hashtags!</li>
								<li>While #hashtags are relatively new 'buzz word' over the last decade its a word that a vast majority of us use daily in our own lives. So what exactly are #hashtags? Let's take a look at the official definition of a hashtag.</li>
								<li>Let's be honest hashtags have become a part of modern culture, their place hasn't been restricted to appearing just on our social media accounts, #hashtagged phrases have been used on everything from t-shirts to mugs and cushions.</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #13 End -->

	<!-- Bonus #14 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">

			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row align-items-center flex-wrap">
				   		<div class="col-12 col-md-5 order-3 order-md-3 text-center">
					  		<img src="assets/images/bonus14.webp" class="img-fluid">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
				   			<div class="text-md-start text-center">
								<div class="bonus-title-bg mb-4">
				   					<div class="f-22 f-md-28 lh120 w700">Bonus 14</div>
								</div>
			 				</div>	  
				   			<div class="f-22 f-md-34 w600 lh160 bonus-title-color mt20 mt-md30">
							   Survey Guru 
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 lh160 w400">
								<li>Learn How to Make Money Online With Surveys!</li>
								<li>Surveys have a bad reputation online, mostly because in the 1990s and early 2000s, many companies scammed people with paid surveys, leaving a bad taste in their mouths.</li>
								<li>The industry was full of scams. Survey companies used the promise of paid surveys to collect email addresses they would then spam the inboxes of people who took surveys. </li>
								<li>Often, they wouldn't even pay the people they promised to pay. This practice is still in use today, but the practice isn't as profitable as it used to be, because people are now wary of the practice and are less likely to fall for the scams.</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #14 End -->


	<!-- Bonus #15 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">

			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row align-items-center flex-wrap">
				   		<div class="col-12 col-md-5 order-3 order-md-0 text-center">
					  		<img src="assets/images/bonus15.webp" class="img-fluid">
				   		</div>
				   		<div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				   			<div class="text-md-start text-center">
								<div class="bonus-title-bg mb-4">
				   					<div class="f-22 f-md-28 lh120 w700">Bonus 15</div>
								</div>
			 				</div>
				   			<div class="f-22 f-md-34 w600 lh160 bonus-title-color mt20 mt-md30">
							   Supercharged Productivity 
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 lh160 w400">
					  			<li>Supercharged productivity is a guide that contains the world's best productivity hacks by the world's leading productivity experts.</li>
								<li>Inside this guide, you'll discover mind-blowing productivity hacks you can use this instance to exponentially improve your productivity and crush all those stubborn projects that you've laid off for so long.</li>
								<li>You'll discover incredible insight and profound knowledge that will enable you to operate with incredible energy beyond the average capability of a human being.</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #15 End -->



	<!-- CTA Button Section Start -->
	<div class="cta-btn-section dark-cta-bg">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-md-12 col-md-12 col-12 text-center">
					<div class="f-md-30 f-18 text-center mt3 white-clr lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
					<div class="f-md-38 f-22 text-center purple lh120 w700 mt15 mt-md20 orange-clr">TAKE ACTION NOW!</div>
					<div class="f-md-22 f-17 lh120 w600 mt15 mt-md20 white-clr">Use Special Coupon <span class="w800 orange-clr">"SUPERADMINOFF"</span> For <span class="w800 orange-clr">$7 Off</span> On Full Funnel </div>
			 	</div>
			 	<div class="col-md-12 col-12 text-center mt15 mt-md20">
					<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
						<span class="text-center">Grab Aicademy + My 20 Exclusive Bonuses</span> <br>
					</a>
			 	</div>
			 	<div class="col-12 mt15 mt-md20 text-center">
					<img src="assets/images/payment.png" class="img-fluid">
			 	</div>
			 	<div class="col-12 mt15 mt-md20" align="center">
					<h3 class="f-md-28 f-20 w500 text-center white-clr">Coupon Is Expiring In... </h3>
			 	</div>
			 	<!-- Timer -->
			 	<div class="col-12 text-center">
					<div class="countdown counter-white white-clr">
				   		<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Days</span> </div>
				   		<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
				   		<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
				   		<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
					</div>
			 	</div>
			 	<!-- Timer End -->
		  	</div>
	   	</div>
	</div>
	<!-- CTA Button Section End -->
	
	<!-- Bonus #16 start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
	
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row align-items-center flex-wrap">
				   		<div class="col-12 col-md-5 order-3 order-md-0 text-center">
					  		<img src="assets/images/bonus16.webp" class="img-fluid">
				   		</div>
				   		<div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				   			<div class="text-md-start text-center">
								<div class="bonus-title-bg mb-4">
				   					<div class="f-22 f-md-28 lh120 w700">Bonus 16</div>
								</div>
			 				</div>
					  		<div class="f-22 f-md-34 w600 lh160 bonus-title-color  mt20 mt-md30">
							  10K Blueprint 
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 lh160 w400">
							    <li>Many people have a hard time believing that it is possible to make $10,000 per month after only 90 days.</li>
								<li>They have tried a few different approaches in Internet Marketing and have not made a single dollar, so how are they going to make $10,000 in 3 short months?</li>
								<li>There have been people that have followed the method described in this training that have made more than $10,000 by the end of 90 days.</li>
								<li>There are others that have made $10,000 a month faster than 90 days. It's certainly possible.</li>
							</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #16 End -->


	<!-- Bonus #17 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">
		
			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row align-items-center flex-wrap">
				   		<div class="col-12 col-md-5 order-3 order-md-3 text-center">
					  		<img src="assets/images/bonus17.webp" class="img-fluid">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
				   			<div class="text-md-start text-center">
								<div class="bonus-title-bg mb-4">
				   					<div class="f-22 f-md-28 lh120 w700">Bonus 17</div>
								</div>
			 				</div>
					  		<div class="f-22 f-md-34 w600 lh160 bonus-title-color mt20 mt-md30">
							  Conversion Equalizer 
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 lh160 w400">
							  <li>A Foolproof Method That Will Automatically Boost All Your Google AdWords Conversion Ratios To The HIGHEST Possible Level! Improve The Most Neglected Aspect Of AdWords Campaigns And You Are Guaranteed To Increase Your Conversion Ratio!</li>
			   				</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #17 End -->

	<!-- Bonus #18 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">

			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row align-items-center flex-wrap">
				   		<div class="col-12 col-md-5 order-3 order-md-0 text-center">
					  		<img src="assets/images/bonus18.webp" class="img-fluid">
				   		</div>
				   		<div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				   			<div class="text-md-start text-center">
								<div class="bonus-title-bg mb-4">
				   					<div class="f-22 f-md-28 lh120 w700">Bonus 18</div>
								</div>
			 				</div>
					  		<div class="f-22 f-md-34 w600 lh160 bonus-title-color mt20 mt-md30">
							  Viral Content Crusher 
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 lh160 w400">
							  	<li>Just one highly effective viral marketing campaign can be more significant than years of regular advertising. In this guide you are going to learn a number of things concerning making viral content. The aim is to give you a comprehensive knowledge on the subject matter that will help you come up with content that will go viral.</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #18 End -->

	<!-- Bonus #19 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">

			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row align-items-center flex-wrap">
				   		<div class="col-12 col-md-5 order-3 order-md-3 text-center">
					  		<img src="assets/images/bonus19.webp" class="img-fluid">
				   		</div>
				   		<div class="col-md-7 col-12 mt20 mt-md0">
				   			<div class="text-md-start text-center">
								<div class="bonus-title-bg mb-4">
				   					<div class="f-22 f-md-28 lh120 w700">Bonus 19</div>
								</div>
			 				</div>	  
				   			<div class="f-22 f-md-34 w600 lh160 bonus-title-color mt20 mt-md30">
							   Site Speed Secrets 
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 lh160 w400">
							  	<li>Everybody hates slow websites. So, why haven't you done anything about your slow site? </li>
								<li>Don't you realize that it's putting people off from visiting your site and checking out everything that you have to offer?  </li>
								<li>It doesn't matter if you've got a well-designed site or a life-changing product. </li>
								<li>If people are leaving your site long before it loads on their browsers, then you may as well NOT have spent all that time and money creating your awesome website! </li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #19 End -->


	<!-- Bonus #20 Start -->
	<div class="section-bonus">
	   	<div class="container">
		  	<div class="row">

			 	<div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
					<div class="row align-items-center flex-wrap">
				   		<div class="col-12 col-md-5 order-3 order-md-0 text-center">
					  		<img src="assets/images/bonus20.webp" class="img-fluid">
				   		</div>
				   		<div class="col-12 col-md-7 mt-md0 mt20 order-2 order-md-1">
				   			<div class="text-md-start text-center">
								<div class="bonus-title-bg mb-4">
				   					<div class="f-22 f-md-28 lh120 w700">Bonus 20</div>
								</div>
			 				</div>
				   			<div class="f-22 f-md-34 w600 lh160 bonus-title-color mt20 mt-md30">
							   List Segmentation Master 
					  		</div>
					  		<ul class="bonus-list f-18 f-md-20 lh160 w400">
							 	<li>Unleach the Full Potential of Your Email List!</li>
								<li>While it's very easy to make money in email marketing, it's even easier when you know exactly how to take advantage of powerful tools that willinstantly bolster your ability to make money with your emails (and these are the same tools that are used by the most successful email marketers).</li>
								<li>With just one powerful technique, you'll be able to maximize the value of every email you deliver, while making sure that your email open rates and click-through rates instantly soar!</li>
					  		</ul>
				   		</div>
					</div>
			 	</div>
		  	</div>
	   	</div>
	</div>
	<!-- Bonus #20 End -->

	<!-- Huge Woth Section Start -->
	<div class="huge-area mt30 mt-md10">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-12 text-center">
                    <div class="f-md-60 f-30 lh120 w700 white-clr">That’s Huge Worth of</div>
                    <br>
                    <div class="f-md-60 f-30 lh120 w800 yellow-clr">$2465!</div>
                </div>
            </div>
        </div>
    </div>
	<!-- Huge Worth Section End -->

	<!-- text Area Start -->
	<div class="white-section pb-0">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-12 text-center p0">
                	<div class="f-md-40 f-25 lh160 w400">So what are you waiting for? You have a great opportunity <br class="d-lg-block d-none"> ahead + <span class="w600">My 20 Bonus Products</span> are making it a <br class="d-lg-block d-none"> <b>completely NO Brainer!!</b></div>
            	</div>
				<div class="col-md-12 col-md-12 col-12 text-center mt20 mt-md50">
					<div class="f-md-26 f-18 text-center black lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
					<div class="f-md-24 f-22 text-center purple lh120 w700 mt15 mt-md20 lite-black-clr">TAKE ACTION NOW!</div>
					<div class="f-md-22 f-17 lh120 w600 mt15 mt-md20 black-clr">Use Special Coupon <span class="w800 lite-black-clr">"SUPERADMINOFF"</span> For <span class="w800 lite-black-clr">$7 Off</span> On Full Funnel </div>
				</div>
			
			</div>
			
		</div>
	</div>
	<!-- text Area End -->

	<!-- CTA Button Section Start -->
	<div class="cta-btn-section pt-3 ">
	   	<div class="container">
		  	<div class="row">
			 	<div class="col-md-12 col-12 text-center">
					<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
						<span class="text-center">Grab Aicademy + My 20 Exclusive Bonuses</span> <br>
					</a>
			 	</div>
			 
			 	<div class="col-12 mt15 mt-md20" align="center">
					<h3 class="f-md-30 f-20 w500 text-center black">My Exclusive Bonuses Are Expiring in...</h3>
			 	</div>
			 	<!-- Timer -->
			 	<div class="col-12 text-center">
					<div class="countdown counter-black">
						<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
						<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
						<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
						<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
					</div>
			 	</div>
			 	<!-- Timer End -->
		  	</div>
	   	</div>
	</div>
	<!-- CTA Button Section End -->

	
	<!--Footer Section Start -->
    <div class="footer-section">
        <div class="container ">
            <div class="row">
                <div class="col-12 text-center">
				<svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 644.49 120" style="max-height:55px">
                        <defs>
                          <style>
                            .cls-1i {
                              fill: #fff;
                            }
                      
                            .cls-2i {
                              fill: #0af;
                            }
                          </style>
                        </defs>
                        <g id="Layer_1-2" data-name="Layer 1">
                          <g>
                            <g>
                              <circle class="cls-2i" cx="79.02" cy="43.5" r="7.25"></circle>
                              <circle class="cls-2i" cx="70.47" cy="78.46" r="5.46"></circle>
                              <path class="cls-2i" d="m158.03,45.28l-33.74,13.31v36.4l-45.27,16.86-27.9-10.4c-4.17,3.37-7.04,6.74-8.15,10.11-1.01,3.22-.07,6.21,2.46,8.42-5.44-1.46-8-5.25-6.96-9.76,1.24-3.62,4.79-7.68,10-12.08h.01c3.95-3.31,8.84-6.84,14.46-10.5,2.04,1.67,4.68,2.68,7.53,2.68,6.56,0,11.88-5.32,11.88-11.88s-5.32-11.88-11.88-11.88-11.88,5.32-11.88,11.88c0,1.13.15,2.21.46,3.25-8.01,5.01-14.83,10.36-19.57,15.44l-5.75-2.14v-7.66c-7.84,4.92-11.84,11.06-5.99,16.99-5.17-1.64-7.69-4.49-7.69-8.05,0-4.83,4.67-10.99,13.69-17.26l35.41-23.23c2.71,2.18,6.13,3.48,9.87,3.48,8.7,0,15.76-7.06,15.76-15.76s-7.06-15.76-15.76-15.76-15.76,7.06-15.76,15.76c0,1.59.23,3.12.68,4.57l-30.2,19.53v-9L0,45.28,79.02,0l79.01,45.28Z"></path>
                              <path class="cls-2i" d="m149.38,68.32v-19.62l-1.51.6v19.03c-1.66.37-2.9,1.96-2.9,3.85v11.69h7.31v-11.69c0-1.9-1.24-3.48-2.9-3.85Z"></path>
                            </g>
                            <g>
                              <g>
                                <path class="cls-2i" d="m178.03,98.08l22.25-71.1h19.3l22.25,71.1h-17.01l-4.36-16.9h-21.7l-4.36,16.9h-16.36Zm25.74-35.99l-1.74,6.54h15.16l-1.64-6.54c-.95-3.56-1.91-7.34-2.89-11.34-.98-4-1.95-7.85-2.89-11.56h-.44c-.87,3.78-1.76,7.65-2.67,11.61-.91,3.96-1.87,7.73-2.89,11.29Z"></path>
                                <path class="cls-2i" d="m256.22,36.03c-2.69,0-4.87-.76-6.54-2.29-1.67-1.53-2.51-3.56-2.51-6.11s.83-4.58,2.51-6.11c1.67-1.53,3.85-2.29,6.54-2.29s4.87.76,6.54,2.29c1.67,1.53,2.51,3.56,2.51,6.11s-.84,4.58-2.51,6.11c-1.67,1.53-3.85,2.29-6.54,2.29Zm-7.96,62.05v-54.09h16.03v54.09h-16.03Z"></path>
                              </g>
                              <g>
                                <path class="cls-1i" d="m299.89,99.38c-5.02,0-9.54-1.11-13.58-3.33-4.03-2.22-7.23-5.45-9.6-9.71-2.36-4.25-3.54-9.36-3.54-15.32s1.31-11.16,3.93-15.38c2.62-4.22,6.07-7.43,10.36-9.65,4.29-2.22,8.9-3.33,13.85-3.33,3.34,0,6.31.55,8.89,1.64,2.58,1.09,4.89,2.47,6.92,4.14l-7.52,10.36c-2.55-2.11-4.98-3.16-7.31-3.16-3.85,0-6.92,1.38-9.21,4.14-2.29,2.76-3.44,6.51-3.44,11.23s1.15,8.38,3.44,11.18c2.29,2.8,5.18,4.2,8.67,4.2,1.74,0,3.45-.38,5.13-1.15,1.67-.76,3.2-1.69,4.58-2.78l6.33,10.47c-2.69,2.33-5.6,3.98-8.72,4.96-3.13.98-6.18,1.47-9.16,1.47Z"></path>
                                <path class="cls-1i" d="m339.81,99.38c-4.94,0-8.87-1.58-11.78-4.74-2.91-3.16-4.36-7.03-4.36-11.61,0-5.67,2.4-10.1,7.2-13.3,4.8-3.2,12.54-5.34,23.23-6.43-.15-2.4-.86-4.31-2.13-5.73-1.27-1.42-3.4-2.13-6.38-2.13-2.25,0-4.54.44-6.87,1.31-2.33.87-4.8,2.07-7.42,3.6l-5.78-10.58c3.42-2.11,7.07-3.82,10.96-5.13,3.89-1.31,7.94-1.96,12.16-1.96,6.91,0,12.21,2,15.92,6,3.71,4,5.56,10.14,5.56,18.43v30.97h-13.09l-1.09-5.56h-.44c-2.25,2.04-4.67,3.69-7.25,4.96-2.58,1.27-5.4,1.91-8.45,1.91Zm5.45-12.43c1.82,0,3.4-.42,4.74-1.25,1.34-.83,2.71-1.94,4.09-3.33v-9.49c-5.67.73-9.6,1.87-11.78,3.44-2.18,1.56-3.27,3.4-3.27,5.51,0,1.74.56,3.04,1.69,3.87,1.13.84,2.63,1.25,4.53,1.25Z"></path>
                                <path class="cls-1i" d="m402.95,99.38c-6.69,0-12.05-2.53-16.09-7.58-4.03-5.05-6.05-11.98-6.05-20.77,0-5.89,1.07-10.96,3.22-15.21,2.14-4.25,4.94-7.51,8.4-9.76,3.45-2.25,7.07-3.38,10.85-3.38,2.98,0,5.49.51,7.52,1.53,2.04,1.02,3.96,2.4,5.78,4.14l-.66-8.29v-18.43h16.03v76.45h-13.09l-1.09-5.34h-.44c-1.89,1.89-4.11,3.47-6.65,4.74-2.55,1.27-5.13,1.91-7.74,1.91Zm4.14-13.09c1.74,0,3.33-.36,4.74-1.09,1.42-.73,2.78-2,4.09-3.82v-22.14c-1.38-1.31-2.85-2.22-4.42-2.73-1.56-.51-3.07-.76-4.53-.76-2.55,0-4.8,1.22-6.76,3.65-1.96,2.44-2.94,6.23-2.94,11.4s.85,9.21,2.56,11.72c1.71,2.51,4.13,3.76,7.25,3.76Z"></path>
                                <path class="cls-1i" d="m470.34,99.38c-5.16,0-9.81-1.13-13.96-3.38-4.14-2.25-7.42-5.49-9.81-9.71-2.4-4.22-3.6-9.31-3.6-15.27s1.22-10.94,3.65-15.16c2.43-4.22,5.62-7.47,9.54-9.76,3.93-2.29,8.03-3.44,12.32-3.44,5.16,0,9.43,1.15,12.81,3.44,3.38,2.29,5.92,5.38,7.63,9.27,1.71,3.89,2.56,8.31,2.56,13.25,0,1.38-.07,2.74-.22,4.09-.15,1.35-.29,2.34-.44,3h-32.39c.73,3.93,2.36,6.82,4.91,8.67,2.54,1.85,5.6,2.78,9.16,2.78,3.85,0,7.74-1.2,11.67-3.6l5.34,9.71c-2.76,1.89-5.85,3.38-9.27,4.47-3.42,1.09-6.73,1.64-9.92,1.64Zm-12-34.24h19.52c0-2.98-.71-5.43-2.13-7.36-1.42-1.93-3.73-2.89-6.92-2.89-2.47,0-4.69.86-6.65,2.56-1.96,1.71-3.24,4.27-3.82,7.69Z"></path>
                                <path class="cls-1i" d="m502.62,98.08v-54.09h13.09l1.09,6.98h.44c2.25-2.25,4.65-4.2,7.2-5.83,2.54-1.64,5.6-2.45,9.16-2.45,3.85,0,6.96.78,9.32,2.34,2.36,1.56,4.23,3.8,5.62,6.71,2.4-2.47,4.94-4.6,7.63-6.38,2.69-1.78,5.82-2.67,9.38-2.67,5.82,0,10.09,1.95,12.81,5.83,2.73,3.89,4.09,9.21,4.09,15.98v33.59h-16.03v-31.52c0-3.93-.53-6.61-1.58-8.07-1.05-1.45-2.74-2.18-5.07-2.18-2.69,0-5.78,1.74-9.27,5.23v36.53h-16.03v-31.52c0-3.93-.53-6.61-1.58-8.07-1.05-1.45-2.74-2.18-5.07-2.18-2.69,0-5.74,1.74-9.16,5.23v36.53h-16.03Z"></path>
                                <path class="cls-1i" d="m602.07,119.23c-1.6,0-3-.11-4.2-.33-1.2-.22-2.34-.47-3.44-.76l2.84-12.21c.51.07,1.09.2,1.74.38.65.18,1.27.27,1.85.27,2.69,0,4.76-.65,6.22-1.96,1.45-1.31,2.54-3.02,3.27-5.13l.76-2.84-20.83-52.67h16.14l7.74,23.23c.8,2.47,1.53,4.98,2.18,7.52.65,2.55,1.34,5.16,2.07,7.85h.44c.58-2.54,1.18-5.11,1.8-7.69.62-2.58,1.25-5.14,1.91-7.69l6.54-23.23h15.38l-18.76,54.63c-1.67,4.51-3.53,8.31-5.56,11.4-2.04,3.09-4.49,5.4-7.36,6.92-2.87,1.53-6.45,2.29-10.74,2.29Z"></path>
                              </g>
                            </g>
                          </g>
                        </g>
                      </svg>
                <div class="f-14 f-md-16 w400 mt20 lh160 white-clr text-center">
					Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. 
				</div>
				<div class=" mt20 mt-md40 white-clr"> <script type="text/javascript" src="https://warriorplus.com/o2/disclaimer/rbhnkl" defer=""></script><div class="wplus_spdisclaimer"><strong>Disclaimer:</strong> <em>WarriorPlus is used to help manage the sale of products on this site. While WarriorPlus helps facilitate the sale, all payments are made directly to the product vendor and NOT WarriorPlus. Thus, all product questions, support inquiries and/or refund requests must be sent to the vendor. WarriorPlus's role should not be construed as an endorsement, approval or review of these products or any claim, statement or opinion used in the marketing of these products.</em></div></div>
			</div>
            <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                <div class="f-14 f-md-16 w400 lh160 white-clr text-center">Copyright © Aicademy</div>
                    <ul class="footer-ul w400 f-14 f-md-16 white-clr text-center text-md-right">
                        <li><a href="https://support.bizomart.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                        <li><a href="http://aicademy.live/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                        <li><a href="http://aicademy.live/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                        <li><a href="http://aicademy.live/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                        <li><a href="http://aicademy.live/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                        <li><a href="http://aicademy.live/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                        <li><a href="http://aicademy.live/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!--Footer Section End -->


	<!-- Exit Pop-Box Start -->
	<!-- Load UC Bounce Modal CDN Start -->
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/ouibounce/0.0.11/ouibounce.min.js"></script>

	<!-- <div id="ouibounce-modal" style="z-index:77777; display:none;">
	    <div class="underlay"></div>
	    	<div class="modal popupbg" style="display:block; ">
	        <div class="modal-header" style="border:0; padding:0;">
	            <button type="button" class="close" onclick="document.getElementById('ouibounce-modal').style.display = 'none';">&times;</button>
	       	</div>

	        <div class="modal-body">
	            <div class="innerbody">
	                <div class="col-lg-12 col-md-12 col-md-12 col-md-offset-0 col-12 col-offset-0 mt2 xsmt2">
	                    <img src="assets/images/waitimg.png" class="img-fluid mt2 xsmt2">
	                    <h2 class="text-center mt2 w500 sm25 xs20 lh160">I HAVE SOMETHING SPECIAL<br /> FOR YOU</h2>
	                    <a href="<?php echo $_GET['afflink']; ?>" class="createfree md20 sm18 xs16" style="color:#ffffff;">Stay</a>
	                </div>
	            </div>
	        </div>
	    </div>
	</div> -->

	<div class="modal fade in" id="ouibounce-modal" role="dialog" style="display:none; overflow-y:auto;">
		<div class="modal-dialog modal-dg">
			<button type="button" class="close" onclick="document.getElementById('ouibounce-modal').style.display = 'none';">&times;</button>	 
			<div class="col-12 modal-content pop-bg pop-padding">			
				<div class="col-12 modal-body text-center border-pop ">	
					<div class="col-12 ">
						<img src="assets/images/waitimg.png" class="img-fluid">
					</div>
					
					<div class="col-12 text-center mt20 mt-md25 ">
						<div class="f-20 f-md-28 lh160 w600">I HAVE SOMETHING SPECIAL <br class="d-lg-block d-none"> FOR YOU</div>
					</div>

					<div class="col-12 mt20 mt-md20">
						<div class="link-btn">
							<a href="<?php echo $_GET['afflink'];?>">STAY</a>
						</div>
					</div>
				</div>		
			</div>
		</div>
	</div>

	<!-- <script type="text/javascript">
        var _ouibounce = ouibounce(document.getElementById('ouibounce-modal'),{
        aggressive: true, //Making this true makes ouibounce not to obey "once per visitor" rule
        });
    </script> -->
	<!-- Load UC Bounce Modal CDN End -->
	<!-- Exit Pop-Box End -->


	
  <!-- timer --->
  <?php
	 if ($now < $exp_date) {
	 
	 ?>
  <script type="text/javascript">
	 // Count down milliseconds = server_end - server_now = client_end - client_now
	 var server_end = <?php echo $exp_date; ?> * 1000;
	 var server_now = <?php echo time(); ?> * 1000;
	 var client_now = new Date().getTime();
	 var end = server_end - server_now + client_now; // this is the real end time
	 
	 var noob = $('.countdown').length;
	 
	 var _second = 1000;
	 var _minute = _second * 60;
	 var _hour = _minute * 60;
	 var _day = _hour * 24
	 var timer;
	 
	 function showRemaining() {
	  var now = new Date();
	  var distance = end - now;
	  if (distance < 0) {
		 clearInterval(timer);
		 document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
		 return;
	  }
	 
	  var days = Math.floor(distance / _day);
	  var hours = Math.floor((distance % _day) / _hour);
	  var minutes = Math.floor((distance % _hour) / _minute);
	  var seconds = Math.floor((distance % _minute) / _second);
	  if (days < 10) {
		 days = "0" + days;
	  }
	  if (hours < 10) {
		 hours = "0" + hours;
	  }
	  if (minutes < 10) {
		 minutes = "0" + minutes;
	  }
	  if (seconds < 10) {
		 seconds = "0" + seconds;
	  }
	  var i;
	  var countdown = document.getElementsByClassName('countdown');
	  for (i = 0; i < noob; i++) {
		 countdown[i].innerHTML = '';
	 
		 if (days) {
			 countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + days + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 smmltd">Days</span> </div>';
		 }
	 
		 countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + hours + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>';
	 
		 countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">' + minutes + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>';
	 
		 countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">' + seconds + '</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>';
	  }
	 
	 }
	 timer = setInterval(showRemaining, 1000);
  </script>
  <?php
	 } else {
	 echo "Times Up";
	 }
	 ?>
  <!--- timer end-->
</body>
</html>

<!Doctype html>
<html>

<head>
    <title>Aicademy Lite</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=9">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">

    <meta name="title" content="Aicademy | Lite">
    <meta name="description" content="Aicademy">
    <meta name="keywords" content="Aicademy">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta property="og:image" content="https://www.aicademy.live/lite/thumbnail.png">
    <meta name="language" content="English">
    <meta name="revisit-after" content="1 days">
    <meta name="author" content="Pranshu Gupta">

    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:title" content="Aicademy | Lite">
    <meta property="og:description" content="Aicademy">
    <meta property="og:image" content="https://www.aicademy.live/lite/thumbnail.png">

    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:title" content="Aicademy | Lite">
    <meta property="twitter:description" content="Aicademy">
    <meta property="twitter:image" content="https://www.aicademy.live/lite/thumbnail.png">

    <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&family=Poppins:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    <!-- Start Editor required -->
    <link rel="stylesheet" href="https://cdn.oppyotest.com/launches/sellero/common_assets/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="assets/css/style.css" type="text/css">
    <link rel="stylesheet" href="assets/css/timer.css" type="text/css">
    <script src="https://cdn.oppyotest.com/launches/sellero/common_assets/js/bootstrap.min.js"></script>
    <script src="https://cdn.oppyotest.com/launches/sellero/common_assets/js/jquery.min.js"></script>
    <!-- End -->
</head>
<body>
    <div class="warning-section">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <div class="offer">
                        <div>
                            <img src="https://cdn.oppyo.com/launches/appzilo/special/error.webp">
                        </div>
                        <div class="f-22 f-md-30 w500 lh140 white-clr text-center">
                            <span class="w600">HOLD ON:</span>  Your Order Is Not Complete Yet...
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>      
    <!--1. Header Section Start -->
    <div class="header-section">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 644.49 120" style="max-height:55px">
                        <defs>
                          <style>
                            .cls-1 {
                              fill: #fff;
                            }
                      
                            .cls-2 {
                              fill: #0af;
                            }
                          </style>
                        </defs>
                        <g id="Layer_1-2" data-name="Layer 1">
                          <g>
                            <g>
                              <circle class="cls-2" cx="79.02" cy="43.5" r="7.25"/>
                              <circle class="cls-2" cx="70.47" cy="78.46" r="5.46"/>
                              <path class="cls-2" d="m158.03,45.28l-33.74,13.31v36.4l-45.27,16.86-27.9-10.4c-4.17,3.37-7.04,6.74-8.15,10.11-1.01,3.22-.07,6.21,2.46,8.42-5.44-1.46-8-5.25-6.96-9.76,1.24-3.62,4.79-7.68,10-12.08h.01c3.95-3.31,8.84-6.84,14.46-10.5,2.04,1.67,4.68,2.68,7.53,2.68,6.56,0,11.88-5.32,11.88-11.88s-5.32-11.88-11.88-11.88-11.88,5.32-11.88,11.88c0,1.13.15,2.21.46,3.25-8.01,5.01-14.83,10.36-19.57,15.44l-5.75-2.14v-7.66c-7.84,4.92-11.84,11.06-5.99,16.99-5.17-1.64-7.69-4.49-7.69-8.05,0-4.83,4.67-10.99,13.69-17.26l35.41-23.23c2.71,2.18,6.13,3.48,9.87,3.48,8.7,0,15.76-7.06,15.76-15.76s-7.06-15.76-15.76-15.76-15.76,7.06-15.76,15.76c0,1.59.23,3.12.68,4.57l-30.2,19.53v-9L0,45.28,79.02,0l79.01,45.28Z"/>
                              <path class="cls-2" d="m149.38,68.32v-19.62l-1.51.6v19.03c-1.66.37-2.9,1.96-2.9,3.85v11.69h7.31v-11.69c0-1.9-1.24-3.48-2.9-3.85Z"/>
                            </g>
                            <g>
                              <g>
                                <path class="cls-2" d="m178.03,98.08l22.25-71.1h19.3l22.25,71.1h-17.01l-4.36-16.9h-21.7l-4.36,16.9h-16.36Zm25.74-35.99l-1.74,6.54h15.16l-1.64-6.54c-.95-3.56-1.91-7.34-2.89-11.34-.98-4-1.95-7.85-2.89-11.56h-.44c-.87,3.78-1.76,7.65-2.67,11.61-.91,3.96-1.87,7.73-2.89,11.29Z"/>
                                <path class="cls-2" d="m256.22,36.03c-2.69,0-4.87-.76-6.54-2.29-1.67-1.53-2.51-3.56-2.51-6.11s.83-4.58,2.51-6.11c1.67-1.53,3.85-2.29,6.54-2.29s4.87.76,6.54,2.29c1.67,1.53,2.51,3.56,2.51,6.11s-.84,4.58-2.51,6.11c-1.67,1.53-3.85,2.29-6.54,2.29Zm-7.96,62.05v-54.09h16.03v54.09h-16.03Z"/>
                              </g>
                              <g>
                                <path class="cls-1" d="m299.89,99.38c-5.02,0-9.54-1.11-13.58-3.33-4.03-2.22-7.23-5.45-9.6-9.71-2.36-4.25-3.54-9.36-3.54-15.32s1.31-11.16,3.93-15.38c2.62-4.22,6.07-7.43,10.36-9.65,4.29-2.22,8.9-3.33,13.85-3.33,3.34,0,6.31.55,8.89,1.64,2.58,1.09,4.89,2.47,6.92,4.14l-7.52,10.36c-2.55-2.11-4.98-3.16-7.31-3.16-3.85,0-6.92,1.38-9.21,4.14-2.29,2.76-3.44,6.51-3.44,11.23s1.15,8.38,3.44,11.18c2.29,2.8,5.18,4.2,8.67,4.2,1.74,0,3.45-.38,5.13-1.15,1.67-.76,3.2-1.69,4.58-2.78l6.33,10.47c-2.69,2.33-5.6,3.98-8.72,4.96-3.13.98-6.18,1.47-9.16,1.47Z"/>
                                <path class="cls-1" d="m339.81,99.38c-4.94,0-8.87-1.58-11.78-4.74-2.91-3.16-4.36-7.03-4.36-11.61,0-5.67,2.4-10.1,7.2-13.3,4.8-3.2,12.54-5.34,23.23-6.43-.15-2.4-.86-4.31-2.13-5.73-1.27-1.42-3.4-2.13-6.38-2.13-2.25,0-4.54.44-6.87,1.31-2.33.87-4.8,2.07-7.42,3.6l-5.78-10.58c3.42-2.11,7.07-3.82,10.96-5.13,3.89-1.31,7.94-1.96,12.16-1.96,6.91,0,12.21,2,15.92,6,3.71,4,5.56,10.14,5.56,18.43v30.97h-13.09l-1.09-5.56h-.44c-2.25,2.04-4.67,3.69-7.25,4.96-2.58,1.27-5.4,1.91-8.45,1.91Zm5.45-12.43c1.82,0,3.4-.42,4.74-1.25,1.34-.83,2.71-1.94,4.09-3.33v-9.49c-5.67.73-9.6,1.87-11.78,3.44-2.18,1.56-3.27,3.4-3.27,5.51,0,1.74.56,3.04,1.69,3.87,1.13.84,2.63,1.25,4.53,1.25Z"/>
                                <path class="cls-1" d="m402.95,99.38c-6.69,0-12.05-2.53-16.09-7.58-4.03-5.05-6.05-11.98-6.05-20.77,0-5.89,1.07-10.96,3.22-15.21,2.14-4.25,4.94-7.51,8.4-9.76,3.45-2.25,7.07-3.38,10.85-3.38,2.98,0,5.49.51,7.52,1.53,2.04,1.02,3.96,2.4,5.78,4.14l-.66-8.29v-18.43h16.03v76.45h-13.09l-1.09-5.34h-.44c-1.89,1.89-4.11,3.47-6.65,4.74-2.55,1.27-5.13,1.91-7.74,1.91Zm4.14-13.09c1.74,0,3.33-.36,4.74-1.09,1.42-.73,2.78-2,4.09-3.82v-22.14c-1.38-1.31-2.85-2.22-4.42-2.73-1.56-.51-3.07-.76-4.53-.76-2.55,0-4.8,1.22-6.76,3.65-1.96,2.44-2.94,6.23-2.94,11.4s.85,9.21,2.56,11.72c1.71,2.51,4.13,3.76,7.25,3.76Z"/>
                                <path class="cls-1" d="m470.34,99.38c-5.16,0-9.81-1.13-13.96-3.38-4.14-2.25-7.42-5.49-9.81-9.71-2.4-4.22-3.6-9.31-3.6-15.27s1.22-10.94,3.65-15.16c2.43-4.22,5.62-7.47,9.54-9.76,3.93-2.29,8.03-3.44,12.32-3.44,5.16,0,9.43,1.15,12.81,3.44,3.38,2.29,5.92,5.38,7.63,9.27,1.71,3.89,2.56,8.31,2.56,13.25,0,1.38-.07,2.74-.22,4.09-.15,1.35-.29,2.34-.44,3h-32.39c.73,3.93,2.36,6.82,4.91,8.67,2.54,1.85,5.6,2.78,9.16,2.78,3.85,0,7.74-1.2,11.67-3.6l5.34,9.71c-2.76,1.89-5.85,3.38-9.27,4.47-3.42,1.09-6.73,1.64-9.92,1.64Zm-12-34.24h19.52c0-2.98-.71-5.43-2.13-7.36-1.42-1.93-3.73-2.89-6.92-2.89-2.47,0-4.69.86-6.65,2.56-1.96,1.71-3.24,4.27-3.82,7.69Z"/>
                                <path class="cls-1" d="m502.62,98.08v-54.09h13.09l1.09,6.98h.44c2.25-2.25,4.65-4.2,7.2-5.83,2.54-1.64,5.6-2.45,9.16-2.45,3.85,0,6.96.78,9.32,2.34,2.36,1.56,4.23,3.8,5.62,6.71,2.4-2.47,4.94-4.6,7.63-6.38,2.69-1.78,5.82-2.67,9.38-2.67,5.82,0,10.09,1.95,12.81,5.83,2.73,3.89,4.09,9.21,4.09,15.98v33.59h-16.03v-31.52c0-3.93-.53-6.61-1.58-8.07-1.05-1.45-2.74-2.18-5.07-2.18-2.69,0-5.78,1.74-9.27,5.23v36.53h-16.03v-31.52c0-3.93-.53-6.61-1.58-8.07-1.05-1.45-2.74-2.18-5.07-2.18-2.69,0-5.74,1.74-9.16,5.23v36.53h-16.03Z"/>
                                <path class="cls-1" d="m602.07,119.23c-1.6,0-3-.11-4.2-.33-1.2-.22-2.34-.47-3.44-.76l2.84-12.21c.51.07,1.09.2,1.74.38.65.18,1.27.27,1.85.27,2.69,0,4.76-.65,6.22-1.96,1.45-1.31,2.54-3.02,3.27-5.13l.76-2.84-20.83-52.67h16.14l7.74,23.23c.8,2.47,1.53,4.98,2.18,7.52.65,2.55,1.34,5.16,2.07,7.85h.44c.58-2.54,1.18-5.11,1.8-7.69.62-2.58,1.25-5.14,1.91-7.69l6.54-23.23h15.38l-18.76,54.63c-1.67,4.51-3.53,8.31-5.56,11.4-2.04,3.09-4.49,5.4-7.36,6.92-2.87,1.53-6.45,2.29-10.74,2.29Z"/>
                              </g>
                            </g>
                          </g>
                        </g>
                      </svg>
                </div>
                <div class="col-12 text-center lh150 mt20 mt-md50">
                    <div class="pre-heading f-18 f-md-22 w600 lh140 white-clr">
                        Maybe you don’t need unlimited capacity for now but you still can......
                    </div>
                </div>
                <div class="col-12 mt-md30 mt20 f-md-50 f-28 w400 text-center white-clr lh140">
                    Supercharge Your Aicademy Experience To <span class="w700 yellow-clr">Get Ten Times More Engagement, Leads & Profits</span> Faster And Easier With This Lite Upgrade At 1/3 Price
                </div>
                <div class="col-12 mt-md25 mt20">
                    <div class="f-20 f-md-22 w500 lh140 white-clr text-center">
                    Unlock The Power Of Driving More Website Visitors, More Targeted Leads, Customizing Your Academy Sites With 20 More Colour Themes, Better Monetization With Banner Placements, Generating More Sales From Follow Up Emails, Advanced Analytics, Powerful Integrations
                    And Other Lite Features
                </div>
                </div>
                <!-- <div class="col-12 col-md-10 mx-auto mt-md30 mt20">
                    <div class="responsive-video">
                        <iframe src="https://Aicademy.dotcompal.com/video/embed/wzieiql74w1" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                        box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                    </div>
                </div> -->
                <div class="col-12 col-md-10 mx-auto mt-md30 mt20">
                    <img src="assets/images/productbox.webp" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12  mt30 mt-md50 f-md-22 f-20 lh140 w400 text-center white-clr">
                    This is An Exclusive Deal for <span class="w600 yellow-clr">"Aicademy"</span> Users only...
                </div>
                <div class="col-md-10 mx-auto col-12 mt20 f-20 f-md-35 text-center probtn1">
                    <a href="#buynow" class="text-center">Get Instant Access to Aicademy Lite</a>
                </div>
            </div>
        </div>
    </div>
    <!--1. Header Section End -->
    <div class="second-sec">
        <div class="container">
            <div class="row">
                <div class="col-12 mt15 mt-md15 f-28 f-md-45 lh140 w500 text-capitalize text-center black-clr">
                    Your Success Matters To Us, PERIOD.
                </div>
                <div class="col-12 f-md-20 f-18 lh160 w400 mt20 mt-md35 text-center">
                    As a customer, you mean everything to us. You've had the chance to see the great power what Aicademy holds for you, but we noticed you didn’t take the opportunity to get Aicademy Elite Edition.<br><br> Maybe you don’t need Unlimited
                    Capacity for now, as you just started doing business online or your business is in growing phase.<br><br> Don’t worry, whatever the reason may be, we’re always here to help any way we can. That’s why we’re presenting this never seen
                    before opportunity to <span class="w600">get almost all the major benefits of the Elite edition for a HUGE discounted price.</span>
                </div>
            </div>
        </div>
    </div>
    <!--2. Second Section Start -->
    <div class="second-sec-1">
        <div class="container">
            <div class="row">
                <div class="col-12 f-28 f-md-45 lh140 w500 text-capitalize text-center black-clr">
                    Let’s Have A Look At <span class="w700">The Major Features You’ll Be 
							Getting With The Aicademy</span> Lite Upgrade –<br class="d-none d-md-block"> ONLY Today -
                </div>
                <div class="col-12">
                    <div class="row">
                        <div class="col-md-6 col-12">
                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-center text-center text-md-start mt20 mt-md50">
                                <div><img src="assets/images/fe1.webp" class="img-fluid d-block mx-auto"></div>
                                <div class="f-18 f-md-20 lh150 w400 ml-md20 mt20 mt-md0 text-capitalize w-100">
                                    <span class="w600">Get 3 times More Website Visitors </span> On Your Website, Marketplace, Or Blogs
                                </div>
                            </div>
                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-center text-center text-md-start mt20 mt-md50">
                                <div><img src="assets/images/fe2.webp" class="img-fluid d-block mx-auto"></div>
                                <div class="f-18 f-md-20 lh150 w400 ml-md20 mt20 mt-md0 text-capitalize w-100">
                                    <span class="w600"> Customise your Academy Sites With 20 Unique Color Themes</span>
                                </div>
                            </div>
                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-center text-center text-md-start mt20 mt-md50">
                                <div><img src="assets/images/fe3.webp" class="img-fluid d-block mx-auto"></div>
                                <div class="f-18 f-md-20 lh150 w400 ml-md20 mt20 mt-md0 text-capitalize w-100">
                                    <span class="w600">Get 6 beautiful color templates of all the sales pages; </span> Affiliate Page& Thank You Pages For Our DFY Video Courses
                                </div>
                            </div>
                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-center text-center text-md-start mt20 mt-md50">
                                <div><img src="assets/images/fe4.webp" class="img-fluid d-block mx-auto"></div>
                                <div class="f-18 f-md-20 lh150 w400 ml-md20 mt20 mt-md0 text-capitalize w-100">
                                    <span class="w600">Complete Set Of Professional Graphics For Your DFY Courses </span> to use for consistent branding-
                                </div>
                            </div>
                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-center text-center text-md-start mt20 mt-md50">
                                <div><img src="assets/images/fe5.webp" class="img-fluid d-block mx-auto"></div>
                                <div class="f-18 f-md-20 lh150 w400 ml-md20 mt20 mt-md0 text-capitalize w-100">
                                    <span class="w600">Get 10K+ FREE Stock Images </span>For Your Products, Marketplace Or Social Media Posts
                                </div>
                            </div>
                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-center text-center text-md-start mt20 mt-md50">
                                <div><img src="assets/images/fe6.webp" class="img-fluid d-block mx-auto"></div>
                                <div class="f-18 f-md-20 lh150 w400 ml-md20 mt20 mt-md0 text-capitalize w-100">
                                    <span class="w600">Boost Relations & Conversions With Your Students </span> Using CRM Integrations
                                </div>
                            </div>
                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-center text-center text-md-start mt20 mt-md50">
                                <div><img src="assets/images/fe7.webp" class="img-fluid d-block mx-auto"></div>
                                <div class="f-18 f-md-20 lh150 w400 ml-md20 mt20 mt-md0 text-capitalize w-100">
                                    <span class="w600">Complete Team Management With Rights Control – Upto 10 Team members</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-center text-center text-md-start mt20 mt-md50">
                                <div><img src="assets/images/fe8.webp" class="img-fluid d-block mx-auto"></div>
                                <div class="f-18 f-md-20 lh150 w400 ml-md20 mt20 mt-md0 text-capitalize w-100">
                                    <span class="w600">Capture 3 Times More Potential Buyer Leads  </span>For Your Offers
                                </div>
                            </div>
                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-center text-center text-md-start mt20 mt-md50">
                                <div><img src="assets/images/fe9.webp" class="img-fluid d-block mx-auto"></div>
                                <div class="f-18 f-md-20 lh150 w400 ml-md20 mt20 mt-md0 text-capitalize w-100">
                                    <span class="w600">Multiply Your Leads With 30 Lead Generation Popup Templates</span>
                                </div>
                            </div>
                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-center text-center text-md-start mt20 mt-md50">
                                <div><img src="assets/images/fe10.webp" class="img-fluid d-block mx-auto"></div>
                                <div class="f-18 f-md-20 lh150 w400 ml-md20 mt20 mt-md0 text-capitalize w-100">
                                    <span class="w600"> Get Done for you follow up emails  </span> To Multiply Sales & Profits
                                </div>
                            </div>
                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-center text-center text-md-start mt20 mt-md50">
                                <div><img src="assets/images/fe11.webp" class="img-fluid d-block mx-auto"></div>
                                <div class="f-18 f-md-20 lh150 w400 ml-md20 mt20 mt-md0 text-capitalize w-100">
                                    <span class="w600">Media Library To Manage All Your Images In One Place</span>
                                </div>
                            </div>
                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-center text-center text-md-start mt20 mt-md50">
                                <div><img src="assets/images/fe12.webp" class="img-fluid d-block mx-auto"></div>
                                <div class="f-18 f-md-20 lh150 w400 ml-md20 mt20 mt-md0 text-capitalize w-100">
                                    <span class="w600">Advance And Deep Analytics </span> To Track Your Growth And Scale It Further
                                </div>
                            </div>
                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-center text-center text-md-start mt20 mt-md50">
                                <div><img src="assets/images/fe13.webp" class="img-fluid d-block mx-auto"></div>
                                <div class="f-18 f-md-20 lh150 w400 ml-md20 mt20 mt-md0 text-capitalize w-100">
                                    <span class="w600">Register Your Students Directly For Your Webinars </span> With Webinar Platform Integrations
                                </div>
                            </div>
                            <div class="d-flex justify-content-md-start justify-content-center flex-wrap flex-md-nowrap align-items-center text-center text-md-start mt20 mt-md50">
                                <div><img src="assets/images/fe14.webp" class="img-fluid d-block mx-auto"></div>
                                <div class="f-18 f-md-20 lh150 w400 ml-md20 mt20 mt-md0 text-capitalize w-100">
                                    Get all these benefits <span class="w600"> At An Unparalleled Price</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 mt-md40 mt20 f-20 f-md-24 lh140 w600 text-center black-clr lh140">
                    This Is Your Only Chance to Get Aicademy Lite Features at A Crazy Low <br class="d-none d-md-block">Discounted Price That We’ve Never Offered Before
                </div>
                <div class="col-md-10 mx-auto col-12 mt20 f-20 f-md-35 text-center probtn1">
                    <a href="#buynow" class="text-center">Get Instant Access to Aicademy Lite</a>
                </div>
            </div>
        </div>
    </div>
    <!--2. Second Section End -->


    <!--4. Fourth Section Starts -->
    <div class="fifth-section">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12">
                    <div class="col-12 text-center">
                        <div class="f-28 f-md-38 w700 white-clr lh140 presenting-head">
                           Proudly Presenting…
                        </div>
                     </div>
                    <div class="f-22 f-md-36 text-center white-clr lh140 w600 mt20 mt-md40">
                        <span class="yellow-clr">The Prime Features Top Marketers Demand</span> And Need <br class="d-none d-md-block">Now Available at a Fraction of the Cost
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md40">
                    <img src="assets/images/productbox.webp" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-md-10 mx-auto col-12 mt30 mt-md65 f-18 f-md-36  text-center probtn1">
                    <a href="#buynow" class="text-center">Get Instant Access to Aicademy Lite</a>
                </div>
            </div>
        </div>
    </div>


    <!-- feature1 -->
    <div class="grey-section">
        <div class="container">
            <div class="row">
                <div class="col-12 f-28 f-md-50 lh140 w500 text-center black-clr">

                    Here’s What You’re Getting<br class="hidden-xs"> <span class="w700">With This Offer Today</span>

                </div>
                <div class="col-12 col-md-12">

                    <img src="../common_assets/images/1.webp" class="img-responsive">

                    <div class="f-24 f-md-36 w500 lh140 mt10 black-clr">
                        Get 3 times more Website Visitors on Your Website, Marketplace, or Blogs
                    </div>

                    <div class="col-12 col-md-10 mx-auto mt20 mt-md30">
                        <img src="assets/images/f1.webp" class="img-fluid d-block mx-auto">
                    </div>

                    <div class="col-12 f-18 f-md-20 w400 lh140 mt20 mt-md50">

                        <span class="w600">	Get 3 times more website visitors upto 300,000/month on </span> your website, marketplace, or blogs and 3X your limits for your growth online. Aicademy Lite gives to the power to drive 3X more visitors on your
                        website, marketplace, or blogs and 3X your limits for your growth online

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--feature1 -->

    <!--feature2 -->
    <div class="white-section">
        <div class="container">
            <div class="row">
                <div class="col-12">

                    <img src="../common_assets/images/2.webp" class="img-responsive">

                    <div class="f-24 f-md-36 w500 lh140 mt10 black-clr">
                        Capture 3 Times More Potential Buyer Leads For Your Offers
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md30">
                    <img src="assets/images/f2.webp" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 f-18 f-md-20 w400 lh140 mt20 mt-md50">

                    Getting targeted leads for your offers just got simpler. With Aicademy Lite, you can easily drive targeted leads upto 100,000 when someone grab your FREE courses on your marketplace, grab reports on lead pages and
                    <span class="w600"> when users subscribe to your academy using social media.</span> You also can manage contacts easily in one central dashboard

                </div>
            </div>
        </div>
    </div>



    <div class="grey-section">
        <div class="container">
            <div class="row">

                <div class="col-12">

                    <img src="../common_assets/images/3.webp" class="img-responsive">

                    <div class="f-24 f-md-36 w500 lh140 mt10 black-clr">
                        Give your Academy site a Pro level Look & Feel - Customise your Academy Sites with 20 unique colour themes-
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md30">
                    <img src="assets/images/f3.webp" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 f-18 f-md-20 w400 lh140 mt20 mt-md50">

                    Make your Academy website including blog, marketplace & membership site <span class="w600">appear more elegant & appealing for your target audience to enhance the visitor engagement.</span><br><br> No need to rush into website designers
                    and paying them every time you want to alter your site aesthetics. Simply choose and give it a completely fresh look & feel.

                </div>

            </div>
        </div>
    </div>


    <!--feature4 -->
    <div class="white-section">
        <div class="container">
            <div class="row">

                <div class="col-12">

                    <img src="../common_assets/images/4.webp" class="img-responsive">

                    <div class="f-24 f-md-36 w500 lh140 mt10 black-clr">
                        Multiply Your Leads With 30 Lead Generation Popup Templates
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md30">
                    <img src="assets/images/f4.webp" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 f-18 f-md-20 w400 lh140 mt20 mt-md50">

                    These are specially designed to make you the most from your website traffic. <span class="w600">Just add your free stuff to a beautiful popup and you are good to grab your targeted leads.</span><br><br>

                    <span class="w600">And we are not done yet </span>as there are many other features are there in this upgrade for you.

                </div>

            </div>
        </div>
    </div>

    <!--feature5 -->
    <div class="grey-section">
        <div class="container">
            <div class="row">

                <div class="col-12">

                    <img src="../common_assets/images/5.webp" class="img-responsive">

                    <div class="f-24 f-md-36 w500 lh140 mt10 black-clr">
                        Get 6 Beautiful Color Templates Of All The Sales Pages; Affiliate Page& Thank You Pages For Our DFY Video Courses
                    </div>

                    <div class="col-12 col-md-10 mx-auto mt20 mt-md30">
                        <img src="assets/images/f5.webp" class="img-fluid d-block mx-auto">
                    </div>
                    <div class="col-12 f-18 f-md-20 w400 lh140 mt20 mt-md50">

                        All these pages come in 6 beautifully formatted and completely ready-to-use colour templates. <span class="w600">These comes with a flexible free flow editor to edit almost anything.</span> Just use them and let the magic begin

                    </div>
                </div>

            </div>
        </div>
    </div>


    <!--feature6 -->
    <div class="white-section">
        <div class="container">
            <div class="row">

                <div class="col-12 ">

                    <img src="../common_assets/images/6.webp" class="img-responsive">

                    <div class="f-24 f-md-36 w500 lh140 mt10 black-clr">
                        Get Done For You Follow Up Emails To Multiply Sales & Profits
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md30">
                    <img src="assets/images/f6.webp" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 f-18 f-md-20 w400 lh140 mt20 mt-md50">

                    Send ready-to-use high converting emails to the customers who opted in for free offer or purchased the basic product & <span class="w600">make the most out of every single subscriber effortlessly.</span> You will get those for all
                    5 DFY courses.

                </div>

            </div>
        </div>
    </div>

    <!--feature7 -->
    <div class="grey-section">
        <div class="container">
            <div class="row">

                <div class="col-12">

                    <img src="../common_assets/images/7.webp" class="img-responsive">

                    <div class="f-24 f-md-36 w500 lh140 mt10 black-clr">
                        Complete Set Of Professional Graphics For Your DFY Courses To Use For Consistent Branding
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md30">
                    <img src="assets/images/f7.webp" class="img-fluid d-block mx-auto">
                </div>

                <div class="col-12 f-18 f-md-20 w400 lh140 mt20 mt-md50">

                    We are serious about giving you every single thing you need to ensure you’re branding consistently.<br><br> You’ll get complete set of <span class="w600"> e-cover graphics, download button, header and footer graphics and complete set of graphics for your PDF guides, andpresentation slides.</span>

                </div>

            </div>
        </div>
    </div>
    <!--feature8 -->
    <div class="white-section">
        <div class="container">
            <div class="row">

                <div class="col-12">

                    <img src="../common_assets/images/8.webp" class="img-responsive">

                    <div class="f-24 f-md-36 w500 lh140 mt10 black-clr">
                        Media Library To Manage All Your Images In One Place
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md30">
                    <img src="assets/images/f8.webp" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 f-18 f-md-20 w400 lh140 mt20 mt-md50">

                    No more searching files on your desktop or rushing back to the designers. With Lite edition, <span class="w600">you can easily store & manage all your images and videos at a central location.</span> Just point and click and add them
                    to your campaigns in no time.

                </div>

            </div>
        </div>
    </div>

    <!--feature9 -->
    <div class="grey-section">
        <div class="container">
            <div class="row">

                <div class="col-12">

                    <img src="../common_assets/images/9.webp" class="img-responsive">

                    <div class="f-24 f-md-36 w500 lh140 mt10 black-clr">
                        Get 10K+ FREE Stock Images for your products, marketplace, or social media posts-
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md30">
                    <img src="assets/images/f9.webp" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 f-18 f-md-20 w400 lh140 mt20 mt-md50">

                    Eye-catchy images are best way to attract audience and get them hooked to your academies.<br><br> So, with Lite edition, you’re getting the power of <span class="w600"> utilizing thousands of royalty free images for your products, marketplace or social media posts.</span>

                </div>

            </div>
        </div>
    </div>

    <!--feature10 -->
    <div class="white-section">
        <div class="container">
            <div class="row">

                <div class="col-12">

                    <img src="../common_assets/images/10.webp" class="img-responsive">

                    <div class="f-24 f-md-36 w500 lh140 mt10 black-clr">
                        Advance And Deep Analytics To Track Your Growth And Scale It Further
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md30">
                    <img src="assets/images/f10.webp" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 f-18 f-md-20 w400 lh140 mt20 mt-md50">

                    Analytics are important to find out what went a success with your audience, which content made them to act and how they reacted – they liked, shared, commented, purchased a product, or made an exit.<br><br> Gain each of the crucial
                    metrics regarding your academy, blog and social campaigns right there in your dashboard. <span class="w600">No more hiring a third-party analysis tool and getting confused on how to draw analysis.</span>

                </div>

            </div>
        </div>
    </div>

    <!--feature11 -->
    <div class="grey-section">
        <div class="container">
            <div class="row">

                <div class="col-12">

                    <img src="../common_assets/images/11.webp" class="img-responsive">

                    <div class="f-24 f-md-36 w500 lh140 mt10 black-clr">
                        Register Your Students Directly For Your Webinars With Webinar Platform Integrations
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md30">
                    <img src="assets/images/f11.webp" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 f-18 f-md-20 w400 lh140 mt20 mt-md50">

                    Create and deliver your next webinar without switching over or exporting another CSV file. Get fast and easy integrations with GoToWebinar and <span class="w600"> collect your registrants and attendee’s data without losing a single lead.</span>

                </div>

            </div>
        </div>
    </div>

    <!--feature12 -->
    <div class="white-section">
        <div class="container">
            <div class="row">

                <div class="col-12">

                    <img src="../common_assets/images/12.webp" class="img-responsive">

                    <div class="f-24 f-md-36 w500 lh140 mt10 black-clr">
                        Boost Relations & Conversions With Your Students Using CRM Integrations

                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md30">
                    <img src="assets/images/f12.webp" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 f-18 f-md-20 w400 lh140 mt20 mt-md50">

                    Managing leads is one of the most complicated tasks but we are making it easier for you by giving you an option of integrating CRM, <span class="w600"> so that you can take customer satisfaction to the next level.</span>

                </div>

            </div>
        </div>
    </div>
    <!--feature13 -->
    <div class="grey-section">
        <div class="container">
            <div class="row">

                <div class="col-12">

                    <img src="../common_assets/images/13.webp" class="img-responsive">

                    <div class="f-24 f-md-36 w500 lh140 mt10 black-clr">
                        Complete Team Management With Rights Control – Upto 10 Team members
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md30">
                    <img src="assets/images/f13.webp" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 f-18 f-md-20 w400 lh140 mt20 mt-md50">

                    You focus on growth and let your team work on your & client’s projects. Now, assign roles and privileges to each of the team members (inhouse or freelancers) and reduce your burden of managing hundreds of accounts and projects yourself.<br><br>

                    <span class="w600"> Choose from the defined roles and privileges and can even tailor them according to the project requirements.</span>

                </div>

            </div>
        </div>
    </div>



    <div class="white-section">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12">
                    <img src="../common_assets/images/14.webp" class="img-fluid">
                    <div class="f-24 f-md-36 w500 lh140 mt10  black-clr">
                        Get all these benefits at an unparallel price
                    </div>
                </div>
            </div>
            <div class="row d-flex align-items-center flex-wrap mt20 mt-md30">
                <div class="col-12 col-md-5 order-md-2">
                    <img src="assets/images/f14.webp" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 col-md-7 order-md-1 mt-md10 mt0 f-18 f-md-20 w400 lh140 mt20 mt-md0">
                    Ultimately you are saving thousands of dollars monthly with us, &amp; every dollar that you save, adds to your profits. By multiplying engagement &amp; opt-ins, <span class="w600">you’re not only saving money, you’re also taking your business to the next level.</span>
                </div>
            </div>
        </div>
    </div>

    <!--money back Start -->
    <div class="moneyback-section">
        <div class="container">
            <div class="row">
                <div class="col-12 f-28 f-md-45 lh140 w500 text-center white-clr">
                    Let’s Remove All The Risks For You And Secure It With <span class="w700">30 Days Money Back Guarantee</span>
                </div>
            </div>
            <div class="row align-items-center d-flex flex-wrap mt20 mt-md50">
                <div class="col-12 col-md-4">
                    <img src="assets/images/moneyback.webp" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 col-md-8 mt20 mt-md0 f-md-20 f-18 lh140 w400 text-left white-clr">
                    You can buy with confidence because if you face any technical issue which we don’t resolve for you, <span class="w600">just raise a ticket within 30 days and we'll refund you everything,</span> down to the last penny.
                    <br><br> <span class="w600">Our team has a 99.21% proven record of solving customer problems and helping them through any issues. </span>
                    <br><br> Guarantees like that don't come along every day, and neither do golden chances like this.
                </div>
            </div>
        </div>
    </div>
    <!--money back End-->

    <!---Bonus Section Starts-->
    <div class="bonus-section">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center px30">
                    <div class="black-design">
                        <div class="f-28 f-md-50 w700 lh140 text-center white-clr skew-normal">
                            But That’s Not All
                        </div>
                    </div>
                </div>
                <div class="col-12 f-20 f-md-24 w400 mt20 mt-md20 text-center lh140">
                    In addition, we have a number of bonuses for those who want to take action today and start profiting from this opportunity
                </div>
                <!-- bonus1 -->
                <div class="col-12 col-md-12 mt20 mt-md60">
                    <div class="row align-items-center d-flex felx-wrap">
                        <div class="col-12 col-md-6">
                            <div class="f-md-36 f-24 w700 blue-clr text-nowrap">
                                Bonus 1
                            </div>
                            <div class="f-24 f-md-30 w500 lh140 mt20 mt-md20 text-left black-clr">
                                Brand Your Business For Success
                            </div>
                            <div class="f-18 f-md-20 w400 lh140 mt10 mt-md10 text-left">
                                Branding plays a crucial role for the success of your business and if not done the right way, it can be fatal for your business. So, we’re providing this helpful bonus that enables you to proven and effective methods that you can use to brand yourself
                                and your business for success. Use it with brand building powers of Aicademy Elite to take your benefits to the next level.
                            </div>
                        </div>
                        <div class="col-12 col-md-6 mt20 mt-md0">
                            <img src="assets/images/bonusbox.webp" class="img-fluid d-block mx-auto">
                        </div>
                    </div>
                </div>
                <!-- bonus2 -->
                <div class="col-12 col-md-12 mt40 mt-md60">
                    <div class="row align-items-center d-flex felx-wrap">
                        <div class="col-12 col-md-6 order-md-2 mt-md50 mt0">
                            <div class="f-md-36 f-24 w700 blue-clr text-nowrap">
                                Bonus 2
                            </div>
                            <div class="f-24 f-md-30 w500 lh140 mt20 mt-md20 text-left black-clr">
                                The Flipping Code
                            </div>
                            <div class="f-18 f-md-20 w400 lh140 mt10 mt-md10 text-left">
                                Website flipping is a sure-shot way of boosting your business income. So, here is 5 easy-to-follow videos, teaching you how to build and sell websites and generate massive paydays from flipping brand new websites. When combined with Aicademy Elite, it
                                becomes a complete no brainer to fuel your business growth.
                            </div>
                        </div>
                        <div class="col-12 col-md-6 order-md-1 mt20 mt-md0">
                            <img src="assets/images/bonusbox.webp" class="img-fluid d-block mx-auto">
                        </div>
                    </div>
                </div>
                <!-- bonus3 -->
                <div class="col-12 col-md-12 mt20 mt-md60">
                    <div class="row align-items-center d-flex felx-wrap">
                        <div class="col-12 col-md-6">
                            <div class="f-md-36 f-24 w700 blue-clr text-nowrap">
                                Bonus 3
                            </div>
                            <div class="f-24 f-md-30 w500 lh140 mt20 mt-md20 text-left black-clr">
                                $1,000,000 Copywriting Secrets
                            </div>
                            <div class="f-18 f-md-20 w400 lh140 mt10 mt-md10 text-left">
                                Copywriting is of immense importance to showcase your brand and get maximum value for your marketing efforts. So, checkout this info-packed e-book that has killer copywriting secrets to supercharge your marketing and your profits and give you the security
                                of knowing you can generate cash anytime you want. When you use them along with Aicademy Elite, you get real scalable results for your efforts.
                            </div>
                        </div>
                        <div class="col-12 col-md-6 mt20 mt-md0">
                            <img src="assets/images/bonusbox.webp" class="img-fluid d-block mx-auto">
                        </div>
                    </div>
                </div>
                <!-- bonus2 -->
                <div class="col-12 col-md-12 mt40 mt-md60">
                    <div class="row align-items-center d-flex felx-wrap">
                        <div class="col-12 col-md-6 order-md-2 mt-md50 mt0">
                            <div class="f-md-36 f-24 w700 blue-clr text-nowrap">
                                Bonus 4
                            </div>
                            <div class="f-24 f-md-30 w500 lh140 mt20 mt-md20 text-left black-clr">
                                Triple Your Conversions Instantly
                            </div>
                            <div class="f-18 f-md-20 w400 lh140 mt10 mt-md10 text-left">
                                Boosting conversions is a BIG headache for every marketer today. But its not all that easy as it looks.<br> Keeping this in mind, we’re providing a helpful bonus that includes 28 power packed conversion-boosting secrets
                                to convert visitors into buyers.<br> These techniques when combined with Aicademy Elite get best results for your business.
                            </div>
                        </div>
                        <div class="col-12 col-md-6 order-md-1 mt20 mt-md0">
                            <img src="assets/images/bonusbox.webp" class="img-fluid d-block mx-auto">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Bonus Section Starts -->

    <!--10. Table Section Starts -->
    <div class="table-section" id="buynow">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12">
                    <div class="f-18 f-md-20 w500 lh140 text-center">
                        Yup..! Take action on this while you can as you won’t be seeing this offer again.
                    </div>
                    <div class="f-28 f-md-45 lh140 w500 mt20 mt-md30 text-center black-clr">
                        Today You Can <span class="w700">Get Unrestricted Access to Aicademy Lite</span> For LESS Than the A Dinner Date Only Today
                    </div>
                </div>


                <div class="col-md-8 mx-auto col-12 mt30 mt-md70">
                    <div class="tablebox2">
                        <div class="tbbg2 text-center">
                            <div class="white-clr w600 f-md-50 f-28 text-center">Aicademy Lite Plan</div>
                        </div>
                        <div class="text-center">
                            <ul class="f-18 f-md-18 w400 lh140 vgreytick mb0">
                                <li class="odd">
                                    <span class="w600">Get 3 times More Website Visitors  </span> On Your Website, Marketplace, Or Blogs
                                </li>
                                <li class="even">
                                    <span class="w600">Capture 3 Times More Potential Buyer Leads </span>For Your Offers
                                </li>
                                <li class="odd">
                                    <span class="w600">Give your Academy site a Pro level Look & Feel - Customise your Academy Sites with 20 unique colour themes </span>
                                </li>
                                <li class="even">
                                    <span class="w600">Multiply your leads with 30 lead generation popup templates</span>
                                </li>
                                <li class="odd">
                                    <span class="w600">Get 6 beautiful color templates of all the sales pages; </span> Affiliate Page & Thank You Pages For Our DFY Video Courses
                                </li>
                                <li class="even">
                                    <span class="w600">Get Done for you follow up emails  </span> To Multiply Sales & Profits
                                </li>
                                <li class="odd">
                                    <span class="w600">Complete Set Of Professional Graphics For Your DFY Courses  </span> to use for consistent branding-
                                </li>
                                <li class="even">
                                    <span class="w600">Media Library To Manage All Your Images In One Place </span>
                                </li>
                                <li class="odd">
                                    <span class="w600">Get 10K+ FREE Stock Images   </span> For Your Products, Marketplace Or Social Media Posts
                                </li>
                                <li class="even">
                                    <span class="w600">Advance And Deep Analytics  </span> To Track Your Growth And Scale It Further
                                </li>
                                <li class="odd">
                                    <span class="w600">Register Your Students Directly For Your Webinars </span> With Webinar Platform Integrations
                                </li>
                                <li class="even">
                                    <span class="w600">Boost Relations & Conversions With Your Students </span> Using CRM Integrations
                                </li>
                                <li class="odd">
                                    <span class="w600"> Complete Team Management With Rights Control – Upto 10 Team members </span>
                                </li>
                                <li class="even">
                                    Get all these benefits At <span class="w600"> An Unparalleled Price</span>
                                </li>
                            </ul>
                        </div>
                        <div class="myfeatureslast f-md-25 f-16 w400 text-center lh140 hideme mb-md15 mt-md15 relative">
                            <a href="https://warriorplus.com/o2/buy/hb06pf/yly0k1/t1qry0"><img src="https://warriorplus.com/o2/btn/fn200011000/hb06pf/yly0k1/352972" alt="Aicademy Lite" border="0" class="img-fluid d-block mx-auto"></a>
                        </div>
                        <div class="table-border-content mt20 mt-md50">
                                        <div class="tb-check d-flex align-items-center f-18">
                                        <label class="checkbox inline">
                                            <img src="https://assets.clickfunnels.com/templates/listhacking-sales/images/arrow-flash-small.gif" alt="">
                                            <input type="checkbox" id="check" onclick="myFunction()">
                                        </label>
                                        <label class="checkbox inline">
                                            <h5> &nbsp; &nbsp;Yes, Add CDN Fast Unlimited Hosting & 300+DFY Lead Popups</h5>
                                        </label>
                                        </div>
                                        <div class="p20">
                                        <p class="f-18 text-center text-md-start"><b class="red-clr underline">LIMITED TIME OFFER :</b> Active CDN Fast DOUBLE Unlimited Hosting (Video & Web Hosting) + One Click Lead Popups with 300+ Ready To Use Templates + Commercial License to Serve Your Clients   <br><b>(92% Of Customers Pick Up This Add-on & Save $1,000s Per Year!)</b></p>
                                        <p class="f-18 text-center text-md-start"> <span class="green-clr w600"> Just $9.93 One Time  (Regular $397)</span> </p>
                                        </div>
                                    </div>
                    </div>
                </div>
                <div class="col-12 mt40 mt-md50 text-left thanks-button text-center">
                    <a class="kapblue f-18 f-md-22 lh140 w400" href="https://warriorplus.com/o/nothanks/yly0k1" target="_blank">
                        No thanks - I don't want to Supercharge my Aicademy experience to get three times more engagement, leads & profits faster and easier with this Lite upgrade at one third price. I know that I won't get this offer again. Please take me to the next step to get access to my purchase.
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!--10. Table Section End -->

    <!--Footer Section Start -->
    <div class="footer-section">
        <div class="container ">
            <div class="row">
                <div class="col-12 text-center">
                    <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 644.49 120" style="max-height:55px">
                        <defs>
                          <style>
                            .cls-1 {
                              fill: #fff;
                            }
                      
                            .cls-2 {
                              fill: #0af;
                            }
                          </style>
                        </defs>
                        <g id="Layer_1-2" data-name="Layer 1">
                          <g>
                            <g>
                              <circle class="cls-2" cx="79.02" cy="43.5" r="7.25"/>
                              <circle class="cls-2" cx="70.47" cy="78.46" r="5.46"/>
                              <path class="cls-2" d="m158.03,45.28l-33.74,13.31v36.4l-45.27,16.86-27.9-10.4c-4.17,3.37-7.04,6.74-8.15,10.11-1.01,3.22-.07,6.21,2.46,8.42-5.44-1.46-8-5.25-6.96-9.76,1.24-3.62,4.79-7.68,10-12.08h.01c3.95-3.31,8.84-6.84,14.46-10.5,2.04,1.67,4.68,2.68,7.53,2.68,6.56,0,11.88-5.32,11.88-11.88s-5.32-11.88-11.88-11.88-11.88,5.32-11.88,11.88c0,1.13.15,2.21.46,3.25-8.01,5.01-14.83,10.36-19.57,15.44l-5.75-2.14v-7.66c-7.84,4.92-11.84,11.06-5.99,16.99-5.17-1.64-7.69-4.49-7.69-8.05,0-4.83,4.67-10.99,13.69-17.26l35.41-23.23c2.71,2.18,6.13,3.48,9.87,3.48,8.7,0,15.76-7.06,15.76-15.76s-7.06-15.76-15.76-15.76-15.76,7.06-15.76,15.76c0,1.59.23,3.12.68,4.57l-30.2,19.53v-9L0,45.28,79.02,0l79.01,45.28Z"/>
                              <path class="cls-2" d="m149.38,68.32v-19.62l-1.51.6v19.03c-1.66.37-2.9,1.96-2.9,3.85v11.69h7.31v-11.69c0-1.9-1.24-3.48-2.9-3.85Z"/>
                            </g>
                            <g>
                              <g>
                                <path class="cls-2" d="m178.03,98.08l22.25-71.1h19.3l22.25,71.1h-17.01l-4.36-16.9h-21.7l-4.36,16.9h-16.36Zm25.74-35.99l-1.74,6.54h15.16l-1.64-6.54c-.95-3.56-1.91-7.34-2.89-11.34-.98-4-1.95-7.85-2.89-11.56h-.44c-.87,3.78-1.76,7.65-2.67,11.61-.91,3.96-1.87,7.73-2.89,11.29Z"/>
                                <path class="cls-2" d="m256.22,36.03c-2.69,0-4.87-.76-6.54-2.29-1.67-1.53-2.51-3.56-2.51-6.11s.83-4.58,2.51-6.11c1.67-1.53,3.85-2.29,6.54-2.29s4.87.76,6.54,2.29c1.67,1.53,2.51,3.56,2.51,6.11s-.84,4.58-2.51,6.11c-1.67,1.53-3.85,2.29-6.54,2.29Zm-7.96,62.05v-54.09h16.03v54.09h-16.03Z"/>
                              </g>
                              <g>
                                <path class="cls-1" d="m299.89,99.38c-5.02,0-9.54-1.11-13.58-3.33-4.03-2.22-7.23-5.45-9.6-9.71-2.36-4.25-3.54-9.36-3.54-15.32s1.31-11.16,3.93-15.38c2.62-4.22,6.07-7.43,10.36-9.65,4.29-2.22,8.9-3.33,13.85-3.33,3.34,0,6.31.55,8.89,1.64,2.58,1.09,4.89,2.47,6.92,4.14l-7.52,10.36c-2.55-2.11-4.98-3.16-7.31-3.16-3.85,0-6.92,1.38-9.21,4.14-2.29,2.76-3.44,6.51-3.44,11.23s1.15,8.38,3.44,11.18c2.29,2.8,5.18,4.2,8.67,4.2,1.74,0,3.45-.38,5.13-1.15,1.67-.76,3.2-1.69,4.58-2.78l6.33,10.47c-2.69,2.33-5.6,3.98-8.72,4.96-3.13.98-6.18,1.47-9.16,1.47Z"/>
                                <path class="cls-1" d="m339.81,99.38c-4.94,0-8.87-1.58-11.78-4.74-2.91-3.16-4.36-7.03-4.36-11.61,0-5.67,2.4-10.1,7.2-13.3,4.8-3.2,12.54-5.34,23.23-6.43-.15-2.4-.86-4.31-2.13-5.73-1.27-1.42-3.4-2.13-6.38-2.13-2.25,0-4.54.44-6.87,1.31-2.33.87-4.8,2.07-7.42,3.6l-5.78-10.58c3.42-2.11,7.07-3.82,10.96-5.13,3.89-1.31,7.94-1.96,12.16-1.96,6.91,0,12.21,2,15.92,6,3.71,4,5.56,10.14,5.56,18.43v30.97h-13.09l-1.09-5.56h-.44c-2.25,2.04-4.67,3.69-7.25,4.96-2.58,1.27-5.4,1.91-8.45,1.91Zm5.45-12.43c1.82,0,3.4-.42,4.74-1.25,1.34-.83,2.71-1.94,4.09-3.33v-9.49c-5.67.73-9.6,1.87-11.78,3.44-2.18,1.56-3.27,3.4-3.27,5.51,0,1.74.56,3.04,1.69,3.87,1.13.84,2.63,1.25,4.53,1.25Z"/>
                                <path class="cls-1" d="m402.95,99.38c-6.69,0-12.05-2.53-16.09-7.58-4.03-5.05-6.05-11.98-6.05-20.77,0-5.89,1.07-10.96,3.22-15.21,2.14-4.25,4.94-7.51,8.4-9.76,3.45-2.25,7.07-3.38,10.85-3.38,2.98,0,5.49.51,7.52,1.53,2.04,1.02,3.96,2.4,5.78,4.14l-.66-8.29v-18.43h16.03v76.45h-13.09l-1.09-5.34h-.44c-1.89,1.89-4.11,3.47-6.65,4.74-2.55,1.27-5.13,1.91-7.74,1.91Zm4.14-13.09c1.74,0,3.33-.36,4.74-1.09,1.42-.73,2.78-2,4.09-3.82v-22.14c-1.38-1.31-2.85-2.22-4.42-2.73-1.56-.51-3.07-.76-4.53-.76-2.55,0-4.8,1.22-6.76,3.65-1.96,2.44-2.94,6.23-2.94,11.4s.85,9.21,2.56,11.72c1.71,2.51,4.13,3.76,7.25,3.76Z"/>
                                <path class="cls-1" d="m470.34,99.38c-5.16,0-9.81-1.13-13.96-3.38-4.14-2.25-7.42-5.49-9.81-9.71-2.4-4.22-3.6-9.31-3.6-15.27s1.22-10.94,3.65-15.16c2.43-4.22,5.62-7.47,9.54-9.76,3.93-2.29,8.03-3.44,12.32-3.44,5.16,0,9.43,1.15,12.81,3.44,3.38,2.29,5.92,5.38,7.63,9.27,1.71,3.89,2.56,8.31,2.56,13.25,0,1.38-.07,2.74-.22,4.09-.15,1.35-.29,2.34-.44,3h-32.39c.73,3.93,2.36,6.82,4.91,8.67,2.54,1.85,5.6,2.78,9.16,2.78,3.85,0,7.74-1.2,11.67-3.6l5.34,9.71c-2.76,1.89-5.85,3.38-9.27,4.47-3.42,1.09-6.73,1.64-9.92,1.64Zm-12-34.24h19.52c0-2.98-.71-5.43-2.13-7.36-1.42-1.93-3.73-2.89-6.92-2.89-2.47,0-4.69.86-6.65,2.56-1.96,1.71-3.24,4.27-3.82,7.69Z"/>
                                <path class="cls-1" d="m502.62,98.08v-54.09h13.09l1.09,6.98h.44c2.25-2.25,4.65-4.2,7.2-5.83,2.54-1.64,5.6-2.45,9.16-2.45,3.85,0,6.96.78,9.32,2.34,2.36,1.56,4.23,3.8,5.62,6.71,2.4-2.47,4.94-4.6,7.63-6.38,2.69-1.78,5.82-2.67,9.38-2.67,5.82,0,10.09,1.95,12.81,5.83,2.73,3.89,4.09,9.21,4.09,15.98v33.59h-16.03v-31.52c0-3.93-.53-6.61-1.58-8.07-1.05-1.45-2.74-2.18-5.07-2.18-2.69,0-5.78,1.74-9.27,5.23v36.53h-16.03v-31.52c0-3.93-.53-6.61-1.58-8.07-1.05-1.45-2.74-2.18-5.07-2.18-2.69,0-5.74,1.74-9.16,5.23v36.53h-16.03Z"/>
                                <path class="cls-1" d="m602.07,119.23c-1.6,0-3-.11-4.2-.33-1.2-.22-2.34-.47-3.44-.76l2.84-12.21c.51.07,1.09.2,1.74.38.65.18,1.27.27,1.85.27,2.69,0,4.76-.65,6.22-1.96,1.45-1.31,2.54-3.02,3.27-5.13l.76-2.84-20.83-52.67h16.14l7.74,23.23c.8,2.47,1.53,4.98,2.18,7.52.65,2.55,1.34,5.16,2.07,7.85h.44c.58-2.54,1.18-5.11,1.8-7.69.62-2.58,1.25-5.14,1.91-7.69l6.54-23.23h15.38l-18.76,54.63c-1.67,4.51-3.53,8.31-5.56,11.4-2.04,3.09-4.49,5.4-7.36,6.92-2.87,1.53-6.45,2.29-10.74,2.29Z"/>
                              </g>
                            </g>
                          </g>
                        </g>
                      </svg>
                    <div class="f-14 f-md-16 w400 mt20 lh160 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
                    <div class=" mt20 mt-md40 white-clr"> <script type="text/javascript" src="https://warriorplus.com/o2/disclaimer/hb06pf" defer=""></script><div class="wplus_spdisclaimer f-17 w300 mt20 lh140 white-clr text-center"><strong>Disclaimer:</strong> <em>WarriorPlus is used to help manage the sale of products on this site. While WarriorPlus helps facilitate the sale, all payments are made directly to the product vendor and NOT WarriorPlus. Thus, all product questions, support inquiries and/or refund requests must be sent to the vendor. WarriorPlus's role should not be construed as an endorsement, approval or review of these products or any claim, statement or opinion used in the marketing of these products.</em></div></div>
                </div>
                <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                    <div class="f-14 f-md-16 w400 lh160 white-clr text-xs-center">Copyright © Aicademy</div>
                    <ul class="footer-ul w400 f-14 f-md-16 white-clr text-center text-md-right">
                        <li><a href="https://support.bizomart.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                        <li><a href="http://aicademy.live/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                        <li><a href="http://aicademy.live/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                        <li><a href="http://aicademy.live/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                        <li><a href="http://aicademy.live/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                        <li><a href="http://aicademy.live/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                        <li><a href="http://aicademy.live/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!--Footer Section End -->

 <!-- Exit Popup and Timer -->
 <?php include('timer.php'); ?>
 <!-- Exit Popup and Timer End -->
</body>

</html>
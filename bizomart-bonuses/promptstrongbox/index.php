<!Doctype html>
<html>
<head>
    <title>Atul Pareek - PromptStrongbox </title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=9">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta name="title" content="Special Bonuses">
    <meta name="description" content="My Special Bonuses You Will Be Getting...">
    <meta name="keywords" content="Special Bonuses">
    <meta property="og:image" content="">
    <meta name="language" content="English">
    <meta name="revisit-after" content="1 days">
    <meta name="author" content="Atul Pareek">
    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:title" content="Special Bonuses">
    <meta property="og:description" content="My Special Bonuses You Will Be Getting...">
    <meta property="og:image" content="">
    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:title" content="Atul Pareek Bounses">
    <meta property="twitter:description" content="My Special Bonuses You Will Be Getting...">
    <meta property="twitter:image" content="">
    <link rel="icon" href="assets/images/favicon.png" type="image/png"/>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.oppyotest.com/launches/webgenie/common_assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/style.css" type="text/css">
    <link rel="stylesheet" href="assets/css/timer.css" type="text/css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" type="text/css">
   
</head>
<body>
    <!---Bonus Section Starts-->
    <nav class="navbar navbar-expand-lg navbar-dark nav-position fixed-top" style="background-color: #f1f1f1;">
        <div class="container" style="gap:500px">
          <div>
            <a href="https://jvz1.com/c/1188867/382593/" target="_blank">
                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 214.51 43.96" style="enable-background:new 0 0 214.51 43.96; width:100%; max-height:35px;" xml:space="preserve">
                    <style type="text/css">
                        .st0{fill:#FE6E09;}
                        .st1{fill:#FFFFFF;}
                    </style>
                        <g>
                        <g>
                            <path d="M55.08,21.98c0-9.75,7.32-16.75,17.57-16.75c10.23,0,17.52,6.98,17.52,16.75s-7.29,16.75-17.52,16.75
                                C62.4,38.73,55.08,31.73,55.08,21.98z M81.02,21.98c0-4.99-3.51-8.58-8.37-8.58c-4.86,0-8.42,3.61-8.42,8.58s3.56,8.58,8.42,8.58
                                C77.5,30.55,81.02,26.97,81.02,21.98z"></path>
                            <path d="M120.05,17.55c0,6.68-5.04,11.56-11.82,11.56h-4.77v8.81h-8.81V6.03h13.58C115.02,6.03,120.05,10.87,120.05,17.55z
                                 M111.02,17.57c0-2.27-1.52-3.87-3.84-3.87h-3.72v7.74h3.72C109.5,21.44,111.02,19.84,111.02,17.57z"></path>
                            <path d="M149.44,17.55c0,6.68-5.04,11.56-11.82,11.56h-4.77v8.81h-8.81V6.03h13.58C144.4,6.03,149.44,10.87,149.44,17.55z
                                 M140.41,17.57c0-2.27-1.53-3.87-3.84-3.87h-3.72v7.74h3.72C138.88,21.44,140.41,19.84,140.41,17.57z"></path>
                            <path d="M169.23,25.45v12.47h-8.95v-12.5l-11.06-19.4h9.75l5.83,11.53l5.83-11.53h9.75L169.23,25.45z"></path>
                            <path d="M179.42,21.98c0-9.75,7.32-16.75,17.57-16.75c10.23,0,17.52,6.98,17.52,16.75s-7.29,16.75-17.52,16.75
                                C186.74,38.73,179.42,31.73,179.42,21.98z M205.36,21.98c0-4.99-3.51-8.58-8.37-8.58c-4.86,0-8.42,3.61-8.42,8.58
                                s3.56,8.58,8.42,8.58C201.84,30.55,205.36,26.97,205.36,21.98z"></path>
                        </g>
                        <g>
                            <path class="st0" d="M43.96,39.27V2.45C43.96,1.1,42.86,0,41.5,0L4.69,0C2.5,0,1.41,2.64,2.95,4.19l11.38,11.38L2.81,27.09
                                c-3.75,3.75-3.75,9.82,0,13.57l0.49,0.49c3.75,3.75,9.82,3.75,13.57,0l11.52-11.52L39.77,41C41.31,42.55,43.96,41.45,43.96,39.27z
                                "></path>
                            <circle class="st1" cx="22.2" cy="21.76" r="9.94"></circle>
                        </g>
                    </g>
                    </svg>
            </a>
          </div>
          <!-- <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button> -->
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav me-auto mb-2 mb-lg-0">
              <li class="nav-item">
                <a class="nav-link active" aria-current="page" href="https://atulpareek.com/promptstrongbox/" style="color: #000; font-size: 22px; padding-right: 40px;">Home</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="aboutus/index.html" target="_blank" style="color: #000; font-size: 22px; padding-right: 40px;">About</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#contact" style="color: #000; font-size: 22px; padding-right: 40px;">Contact</a>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link" href="bonuses/index.html" target="_blank" style="color: #000; font-size: 22px; padding-right: 40px;">Bonuses</a>
                
              </li>
            </ul>
          </div>
        </div>
      </nav> 



      <div class="bonus-section mt30">
        <div class="container">
            <div class="row">   
                <div class="col-12 col-md-8 mx-auto">
                    
                    <div class="video-box mt20 mt-md30">
                      
                       <div style="padding-bottom: 56.25%;position: relative; border-radius: 20px;">
                       <iframe src="https://www.youtube.com/embed/mYkQ_x6sFwE?modestbranding=1&rel=0&showinfo=0&iv_load_policy=3&disablekb=1&controls=0&fs=0&autoplay=1&mute=1&widget_referrer=https%3A%2F%2Fpromptstrongbox.com%2F&enablejsapi=1&origin=https%3A%2F%2Fapp.explaindioplayer.com&widgetid=1" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div>
                    </div>
                 </div>
                 <div class="col-12 mt-md30 mt20 f-md-40 f-24 w400 text-center black-clr lh140">
                    Want To Take Your Biz Or Agency To The Next Level With ChatGPT?  <span class="yellow-clr w600">You Need The Ultimate Power Combo: </span>
                       <span class="w700">ChatGPT4 And PromptStrongbox</span> with Just 1 Keyword.
                </div>
                <div class="col-12 col-md-12 text-center mt20 mt-md30" >
                    
                    <div class="bonus-design f-22 f-md-30 w600 lh140 text-center white-clr">
                         Grab All My Premium Software's as a Free Bonus When you Grab <u class="blue-clr"><b>PromptStrongbox </b></u>
                    </div>
                </div>
                <!-- <div class="col-12 text-center mt20 mt-md40">
                    <img src="assets/images/image.png" WIDTH="522" alt="logo" class="img-fluid d-block mx-auto mt2">
                </div> -->
                <div class="row row-cols-1 row-cols-md-2 gap30 justify-content-center mt0 mt-md-70">
                    <div class="col mt20 mt-md40">
                        <div class="profit-box">
                            <div class="logo">
                                <img src="assets/images/legelsuites.webp" alt="Share Contact Cards" class="img-fluid d-block">
                            </div>
                            <div class="text">
                                <div class="f-20 f-md-24 w700 lh140">
                                    LegelSuites
                                </div>
                                <div class="f-14 f-md-18 w400 lh140 mt10">
                                    Analyse, Detect, & Fix Deadly Legal Flaws on Your Website & For Clients Just by Copy-Pasting a Single Line of Code
                                    <br><br>
                                    Charge BIG For Fixing Deadly Law Issues In Minutes Like ADA, GDPR, Policies, T&C & More | No Coding | 100% Newbie Friendly
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col mt20 mt-md40">
                        <div class="profit-box">
                            <div class="logo">
                                <img src="assets/images/pbox.webp" alt="Share Contact Cards" class="img-fluid d-block">
                            </div>
                            <div class="text">
                                <div class="f-20 f-md-24 w700 lh140">
                                    LinkPro
                                </div>
                                <div class="f-14 f-md-18 w400 lh140 mt10">
                                    Convert ANY Long & Ugly Marketing URL into Profit-Pulling SMART Link (Clean, Trackable, & Easy-To-Share) in Just 30 Seconds…
                                    <br><br>
                                    Introducing The Industry-First, 5-In-One Smart Link Builder – Link Cloaker, Shortener, QR Code, Checkout Links & Bio-Pages Creator - All Rolled Into One Easy To Use App!
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col mt20 mt-md40">
                        <div class="profit-box">
                        <div class="logo">
                            <img src="assets/images/restrosuite.webp" alt="Share Contact Cards" class="img-fluid d-block">
                        </div>
                        <div class="text">
                            <div class="f-20 f-md-24 w700 lh140">
                                Restrosuite
                            </div>
                            <div class="f-14 f-md-18 w400 lh140 mt10">
                                All-In-One Marketing Suite Lets YOU Help Desperate Local Restaurants Grow Online & Charge BIG Fee for Your 7-Minute Work.
                                <br><br>
                                Auto Create Modern Websites | Zero-Contact Menu | Online Ordering & Payments |Table Booking | QR Code I Bio Links | Email Marketing System | 100% Newbie Friendly
                            </div>
                        </div>
                        </div>
                    </div>
                    <div class="col mt20 mt-md40">
                        <div class="profit-box">
                        <div class="logo">
                            <img src="assets/images/trendio-box.png" alt="Share Contact Cards" class="img-fluid d-block">
                        </div>
                        <div class="text">
                            <div class="f-20 f-md-24 w700 lh140">
                                Trendio
                            </div>
                            <div class="f-14 f-md-18 w400 lh140 mt10">
                                Breakthrough A.I. Technology Creates Traffic Pulling Websites Packed with Trendy Content & Videos from Just One Keyword in 60 Seconds....
                                <br><br>
                                Create Beautiful, Traffic Pulling Sites With Trendy Content & Videos In 3 Simple Steps Set And Forget System With Single Keyword - Set Rules To Find & Publish Trending Posts
                            </div>
                        </div>
                        </div>
                    </div>
                </div>   
            </div>
           
           
        
            <div class="row">
                <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt20 mt-md50">
                    <a href="https://jvz8.com/c/1188867/398705/" target="_blank" class="text-center bonusbuy-btn">
                    <span class="text-center"> Checkout PromptStrongbox Sales Page</span>
                    </a>
                </div>
                <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt20 mt-md50">
                <a href="https://jvz8.com/c/1188867/398720/" target="_blank" class="text-center bonusbuy-btn">
                <span class="text-center"> Checkout PromptStrongbox Bundle Page</span>
                </a>
                </div>
                <div class="col-md-12 col-md-12 col-12 text-center mt10">
                    <div class="f-16 f-md-20 lh150 w400 text-center black-clr">
                        Use Coupon Code <span class="w800 black-clr"> PSB80OFF </span> for <span class="w800 black-clr"> $80 OFF </span> on Bundle Deal
                    </div>
                </div>
                <div class="col-8 col-md-6 mx-auto">
                        <div class="f20 f-md-30 text-center black-clr mt20 w700">Bonuses Expire In...</div>
                        <div class="countdown counter-white text-center ">
                           <div class="timer-label text-center"><span class="f-24 f-md-28 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-16 w500 smmltd">Days</span> </div>
                           <div class="timer-label text-center"><span class="f-24 f-md-28 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-16 w500 smmltd">Hours</span> </div>
                           <div class="timer-label text-center timer-mrgn"><span class="f-24 f-md-28 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-16 w500 smmltd">Mins</span> </div>
                           <div class="timer-label text-center "><span class="f-24 f-md-28 timerbg oswald">00</span><br><span class="f-14 f-md-16 w500 smmltd">Sec</span> </div>
                        </div>
                     </div>
                    
            </div>
            <!-- <div class="row">
                <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt20 mt-md50">
                    <a href="" class="text-center bonusbuy-btn">
                        <span class="text-center"> Get Exclusive 500 Bonuses if you buy from our link 💝 </span>
                    </a>
                </div>
            </div> -->
        </div>
    </div>
    <div class="bonus-section2">
        <div class="container">
            <div class="row">   
                <div class="col-12 col-md-12 text-center" >
                    <div class="bonus-design f-22 f-md-30 w600 lh140 text-center white-clr">
                        Also Get Exclusive Bonuses for My Valued Subscribers, ONLY FOR TODAY
                    </div>
                </div>



                <!-- bonus1 Starts-->
                <div class="col-12 mt20 mt-md60 relative">
                    <div class="bonus-wrap row d-flex align-items-center flex-wrap ">
                        <div class="col-12 col-md-6 mt30 mt-md90">
                            <div class="w700 f-22 f-md-40 black-clr lh140 mt20">
                                EmailEngage
                            </div>
                            <div class="f-18 f-md-22 w400 black-clr lh140 mt15">
                                New Revolutionary Software That's Designed To Skyrocket Clicks On Email Links Push Button 4-in-1 Email App Helps Skyrocket Clicks, Boost Engagement, Create Urgency, Make More Sales & Commissions.

                                <br><br>
                            </div>
                            <div class="worth-box align-items-center">
                                <div class="worth_area">
                                    <div class="worth_area_img">
                                        <img decoding="async" src="https://bonus.brightrweb.com/wp-content/uploads/2022/06/Vector.png">
                                    </div>
                                    <div class="worth_text_area">
                                        <div class="worth_text f-20 f-md-38 white-clr w500">Exclusive Bonus 1</div>
                                        <!-- <div class="worth_price f-24 f-md-23 w700 white-clr"> WORTH $347</div> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="mt20 mt-md0">
                                <!-- <a href="https://jvz6.com/c/1188867/396842/"> -->
                                    <img class="lazyload img-fluid d-block mx-auto" data-src="assets/images/bonusbox.png" alt="bonusbox" style="max-height: 480px;">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- bonus1 Ends -->

                <!-- bonus2 Starts-->
                <div class="col-12 col-md-12 mt20 mt-md60 relative">
                    <div class="bonus-wrap row d-flex align-items-center flex-wrap">
                        <div class="col-12 col-md-6 order-md-2">
                            <div class="w700 f-22 f-md-40 black-clr lh140 mt20">
                                1 Page Commissions
                            </div>
                            <div class="f-18 f-md-22 w400 black-clr lh140 mt15">
                                100% Done-For-You Commissions Sites Pre-Loaded With Reviews & Bonuses! Snatch Our Commission Sites & Profit In No Time BEGINNER Friendly And Set & Forget SIMPLE!       
                            </div>
                        </div>
                        <div class="col-12 col-md-6 order-md-1">
                            <div class="mt20 mt-md0">
                                <img class="lazyload img-fluid d-block mx-auto" data-src="assets/images/bonusbox.png" alt="bonusbox" style="max-height: 450px;">
                            </div>
                        </div>
                    </div>
                    <div class="worth-box1">
                        <div class="worth_area1">
                            <div class="worth_area_img1">
                                <img decoding="async" src="https://bonus.brightrweb.com/wp-content/uploads/2022/06/Vector.png">
                            </div>
                            <div class="worth_text_area1">
                                <div class="f-20 f-md-38 white-clr w500">Exclusive Bonus 2</div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!-- bonus2 Ends -->

                <!-- bonus3 Starts-->
                <div class="col-12 mt20 mt-md60 relative">
                    <div class="bonus-wrap row d-flex align-items-center flex-wrap ">
                        <div class="col-12 col-md-6 mt30 mt-md90">
                            <div class="w700 f-22 f-md-40 black-clr lh140 mt20">
                                MultiNetworkPoster 
                            </div>
                            <div class="f-18 f-md-22 w400 black-clr lh140 mt15">
                                MultiNetworkPoster is a tool which allows to anyone to publish posts on 16 social networks. Users can register and pay with PayPal to upgrade their plan. You just have to create the plans and limit the platform features for each plan.
                            </div>
                            <div class="worth-box">
                                <div class="worth_area">
                                    <div class="worth_area_img">
                                        <img decoding="async" src="https://bonus.brightrweb.com/wp-content/uploads/2022/06/Vector.png">
                                    </div>
                                    <div class="worth_text_area">
                                        <div class="worth_text f-20 f-md-38 white-clr w500">Exclusive Bonus 3</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 ">
                            <div class="mt20 mt-md0">
                                <!-- <a href="https://jvz6.com/c/1188867/396842/"> -->
                                    <img class="lazyload img-fluid d-block mx-auto" data-src="assets/images/bonusbox.png" alt="bonusbox" style="max-height: 480px;">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- bonus3 Ends -->

                
                <!-- bonus4 Starts-->
                <div class="col-12 col-md-12 mt20 mt-md60 relative">
                    <div class="bonus-wrap row d-flex align-items-center flex-wrap">
                        <div class="col-12 col-md-6 order-md-2">
                            <div class="w700 f-22 f-md-40 black-clr lh140 mt20">
                                AUDIO CONVERTER
                            </div>
                            <div class="f-18 f-md-22 w400 black-clr lh140 mt15">
                                An audio converter is a versatile tool designed to transform audio files between different formats, ensuring seamless compatibility across devices. It simplifies the process of sharing and enjoying audio content!       
                            </div>
                        </div>
                        <div class="col-12 col-md-6 order-md-1">
                            <div class="mt20 mt-md0">
                                <img class="lazyload img-fluid d-block mx-auto" data-src="assets/images/bonusbox.png" alt="bonusbox" style="max-height: 450px;">
                            </div>
                        </div>
                    </div>
                    <div class="worth-box1">
                        <div class="worth_area1">
                            <div class="worth_area_img1">
                                <img decoding="async" src="https://bonus.brightrweb.com/wp-content/uploads/2022/06/Vector.png">
                            </div>
                            <div class="worth_text_area1">
                                <div class="f-20 f-md-38 white-clr w500">Exclusive Bonus 4</div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- bonus4 Ends -->

              <!-- bonus5 Starts-->
              <div class="col-12 mt20 mt-md60 relative">
                <div class="bonus-wrap row d-flex align-items-center flex-wrap ">
                    <div class="col-12 col-md-6 mt30 mt-md90">
                        <div class="w700 f-22 f-md-40 black-clr lh140 mt20">
                            EXPLAINDIO GREEN SCREEN REPLACER
                        </div>
                        <div class="f-18 f-md-22 w400 black-clr lh140 mt15">
                            Green Screen Replacer allows you to replace any color in video including green with image or video. For example you can replace green background with live footage, animation or just an image.
                            <br><br><br>                                 
                        </div>
                        <div class="worth-box">
                            <div class="worth_area">
                                <div class="worth_area_img">
                                    <img decoding="async" src="https://bonus.brightrweb.com/wp-content/uploads/2022/06/Vector.png">
                                </div>
                                <div class="worth_text_area">
                                    <div class="worth_text f-20 f-md-38 white-clr w500">Exclusive Bonus 5</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 ">
                        <div class="mt20 mt-md0">
                            <!-- <a href="https://jvz6.com/c/1188867/396842/"> -->
                                <img class="lazyload img-fluid d-block mx-auto" data-src="assets/images/bonusbox.png" alt="bonusbox" style="max-height: 450px;">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- bonus5 Ends -->
            
            <div class="row">
                <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt20 mt-md50">
                    <a href="https://jvz8.com/c/1188867/398705/" target="_blank" class="text-center bonusbuy-btn">
                    <span class="text-center"> Checkout PromptStrongbox Sales Page</span>
                    </a>
                </div>
                <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt20 mt-md50">
                <a href="https://jvz8.com/c/1188867/398720/" target="_blank" class="text-center bonusbuy-btn">
                <span class="text-center"> Checkout PromptStrongbox Bundle Page</span>
                </a>
                </div>
                <div class="col-md-12 col-md-12 col-12 text-center mt10">
                    <div class="f-16 f-md-20 lh150 w400 text-center black-clr">
                        Use Coupon Code <span class="w800 black-clr"> PSB80OFF </span> for <span class="w800 black-clr"> $80 OFF </span> on Bundle Deal
                    </div>
                </div>
                <div class="col-8 col-md-6 mx-auto">
                        <div class="f20 f-md-30 text-center black-clr mt20 w700">Bonuses Expire In...</div>
                        <div class="countdown counter-white text-center ">
                           <div class="timer-label text-center"><span class="f-24 f-md-28 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-16 w500 smmltd">Days</span> </div>
                           <div class="timer-label text-center"><span class="f-24 f-md-28 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-16 w500 smmltd">Hours</span> </div>
                           <div class="timer-label text-center timer-mrgn"><span class="f-24 f-md-28 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-16 w500 smmltd">Mins</span> </div>
                           <div class="timer-label text-center "><span class="f-24 f-md-28 timerbg oswald">00</span><br><span class="f-14 f-md-16 w500 smmltd">Sec</span> </div>
                        </div>
                     </div>
                    
            </div>
            
            
            <!-- bonus6 Starts-->
            <div class="col-12 col-md-12 mt20 mt-md60 relative">
                <div class="bonus-wrap row d-flex align-items-center flex-wrap">
                    <div class="col-12 col-md-6 order-md-2  mt20 mt-md50">
                        <div class="w700 f-22 f-md-40 black-clr lh140 mt20">
                            VIDEO CONVERTER
                        </div>
                        <div class="f-18 f-md-22 w400 black-clr lh140 mt15">
                            A versatile video converter simplifies file format transitions, allowing seamless playback across devices. Effortlessly transform videos to preferred formats while preserving quality, enhancing compatibility, and expanding viewing possibilities.
                            <br><br><br>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 order-md-1 mt20 mt-md50">
                        <div class="mt20 mt-md0">
                            <img class="lazyload img-fluid d-block mx-auto" data-src="assets/images/bonusbox.png" alt="bonusbox" style="max-height: 450px;">
                        </div>
                    </div>
                </div>
                <div class="worth-box1">
                    <div class="worth_area1">
                        <div class="worth_area_img1">
                            <img decoding="async" src="https://bonus.brightrweb.com/wp-content/uploads/2022/06/Vector.png">
                        </div>
                        <div class="worth_text_area1 ml10">
                            <div class="worth_text f-20 f-md-38 white-clr w500">Exclusive Bonus 6</div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- bonus6 Ends -->


                 <!-- bonus7 Starts-->
                 <div class="col-12 mt20 mt-md60 relative">
                    <div class="bonus-wrap row d-flex align-items-center flex-wrap ">
                        <div class="col-12 col-md-6 mt30 mt-md90">
                            <div class="w700 f-22 f-md-40 black-clr lh140 mt20">
                                SKETCH LINE COLOR CHANGER
                            </div>
                            <div class="f-18 f-md-22 w400 black-clr lh140 mt15">
                                The Sketch Line Color Changer is a versatile tool that empowers artists and designers to effortlessly modify the colors of their sketches, enhancing creativity and enabling dynamic visual transformations.
                            </div>
                            <div class="worth-box">
                                <div class="worth_area">
                                    <div class="worth_area_img">
                                        <img decoding="async" src="https://bonus.brightrweb.com/wp-content/uploads/2022/06/Vector.png">
                                    </div>
                                    <div class="worth_text_area">
                                        <div class="worth_text f-20 f-md-38 white-clr w500">Exclusive Bonus 7</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 ">
                            <div class="mt20 mt-md0">
                                <!-- <a href="https://jvz6.com/c/1188867/396842/"> -->
                                    <img class="lazyload img-fluid d-block mx-auto" data-src="assets/images/bonusbox.png" alt="bonusbox" style="max-height: 480px;">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            <!-- bonus7 Ends -->

            
            <!-- bonus8 Starts-->
            <div class="col-12 col-md-12 mt20 mt-md60 relative">
                <div class="bonus-wrap row d-flex align-items-center flex-wrap">
                    <div class="col-12 col-md-6 order-md-2">
                        <br><br><br>
                        <div class="w700 f-22 f-md-40 black-clr lh140 mt20">
                            Server and Website Monitoring
                        </div>
                        <div class="f-18 f-md-22 w400 black-clr lh140 mt15">
                            Explaindio Video Marker is a versatile software tool for creating engaging videos. It offers an array of features, including animations, sketches, text, and images, enabling users to craft captivating and interactive video content effortlessly!

                            <br><br><br>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 order-md-1">
                        <div class="mt20 mt-md0">
                            <img class="lazyload img-fluid d-block mx-auto" data-src="assets/images/bonusbox.png" alt="bonusbox" style="max-height: 450px;">
                        </div>
                    </div>
                </div>
                <div class="worth-box1">
                    <div class="worth_area1">
                        <div class="worth_area_img1">
                            <img decoding="async" src="https://bonus.brightrweb.com/wp-content/uploads/2022/06/Vector.png">
                        </div>
                        <div class="worth_text_area1">
                            <div class="worth_text f-20 f-md-38 white-clr w500">Exclusive Bonus 8</div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- bonus8 Ends -->
            
              <!-- bonus9 Starts-->
              <div class="col-12 mt20 mt-md60 relative">
                <div class="bonus-wrap row d-flex align-items-center flex-wrap ">
                    <div class="col-12 col-md-6 mt30 mt-md90">
                        <div class="w700 f-22 f-md-40 black-clr lh140 mt20">
                            Web Video Player WHITELABEL
                        </div>
                        <div class="f-18 f-md-22 w400 black-clr lh140 mt15">
                            OUR NEW SOFTWARE WEB VIDEO PLAYER (WP-PLUGIN) WITH WHITELABEL AND LAUNCH RIGHTS. THIS IS THE VERY FIRST WP PLUGIN WHICH CAN DISPLAY GIF IMAGE INSTEAD OF SHORT VIDEO ON Mobile.                         
                        </div>
                        <div class="worth-box">
                            <div class="worth_area">
                                <div class="worth_area_img">
                                    <img decoding="async" src="https://bonus.brightrweb.com/wp-content/uploads/2022/06/Vector.png">
                                </div>
                                <div class="worth_text_area">
                                    <div class="worth_text f-20 f-md-38 white-clr w500">Exclusive Bonus 9</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 ">
                        <div class="mt20 mt-md0">
                            <!-- <a href="https://jvz6.com/c/1188867/396842/"> -->
                                <img class="lazyload img-fluid d-block mx-auto" data-src="assets/images/bonusbox.png" alt="bonusbox" style="max-height: 450px;">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- bonus9 Ends -->

            
            <!-- bonus10 Starts-->
            <div class="col-12 col-md-12 mt20 mt-md60 relative">
                <div class="bonus-wrap row d-flex align-items-center flex-wrap">
                    <div class="col-12 col-md-6 order-md-2">
                        <div class="w700 f-22 f-md-40 black-clr lh140 mt20">
                            Speedlir Bundle (FE + All upsells)
                        </div>
                        <div class="f-18 f-md-22 w400 black-clr lh140 mt15">
                            The Speedlir Bundle offers the full package, including the Front-End and all upsells. Unlock blazing website speed and a range of powerful features to enhance your online presence significantly.

                        </div>
                    </div>
                    <div class="col-12 col-md-6 order-md-1">
                        <div class="mt20 mt-md0">
                            <img class="lazyload img-fluid d-block mx-auto" data-src="assets/images/bonusbox.png" alt="bonusbox" style="max-height: 450px;">
                        </div>
                    </div>
                </div>
                <div class="worth-box1">
                    <div class="worth_area1">
                        <div class="worth_area_img1">
                            <img decoding="async" src="https://bonus.brightrweb.com/wp-content/uploads/2022/06/Vector.png">
                        </div>
                        <div class="worth_text_area1">
                            <div class="worth_text f-20 f-md-38 white-clr w500">Exclusive Bonus 10</div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- bonus10 Ends -->
            
    
            <div class="row">
                <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt20 mt-md50">
                    <a href="https://jvz8.com/c/1188867/398705/" target="_blank" class="text-center bonusbuy-btn">
                    <span class="text-center"> Checkout PromptStrongbox Sales Page</span>
                    </a>
                </div>
                <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt20 mt-md50">
                <a href="https://jvz8.com/c/1188867/398720/" target="_blank" class="text-center bonusbuy-btn">
                <span class="text-center"> Checkout PromptStrongbox Bundle Page</span>
                </a>
                </div>
                <div class="col-md-12 col-md-12 col-12 text-center mt10">
                    <div class="f-16 f-md-20 lh150 w400 text-center black-clr">
                        Use Coupon Code <span class="w800 black-clr"> PSB80OFF </span> for <span class="w800 black-clr"> $80 OFF </span> on Bundle Deal
                    </div>
                </div>
                <div class="col-8 col-md-6 mx-auto">
                        <div class="f20 f-md-30 text-center black-clr mt20 w700">Bonuses Expire In...</div>
                        <div class="countdown counter-white text-center ">
                           <div class="timer-label text-center"><span class="f-24 f-md-28 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-16 w500 smmltd">Days</span> </div>
                           <div class="timer-label text-center"><span class="f-24 f-md-28 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-16 w500 smmltd">Hours</span> </div>
                           <div class="timer-label text-center timer-mrgn"><span class="f-24 f-md-28 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-16 w500 smmltd">Mins</span> </div>
                           <div class="timer-label text-center "><span class="f-24 f-md-28 timerbg oswald">00</span><br><span class="f-14 f-md-16 w500 smmltd">Sec</span> </div>
                        </div>
                     </div>
                    
            </div>
            <!-- bonus11 Starts-->
            <div class="col-12 col-md-12 mt20 mt-md60 relative">
                <div class="bonus-wrap row d-flex align-items-center flex-wrap">
                    <div class="col-12 col-md-6 order-md-2  mt20 mt-md50">
                        <div class="w700 f-22 f-md-40 black-clr lh140 mt20">
                            LinksSeam Bundle (FE + All upsells)
                        </div>
                        <div class="f-18 f-md-22 w400 black-clr lh140 mt15">
                            The LinksSeam Bundle offers the complete package, including the front-end product and all associated upsells. Unlock the full potential of seamless link management with this comprehensive bundle.

                            <br><br><br>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 order-md-1 mt20 mt-md50">
                        <div class="mt20 mt-md0">
                            <img class="lazyload img-fluid d-block mx-auto" data-src="assets/images/bonusbox.png" alt="bonusbox" style="max-height: 450px;">
                        </div>
                    </div>
                </div>
                <div class="worth-box1">
                    <div class="worth_area1">
                        <div class="worth_area_img1">
                            <img decoding="async" src="https://bonus.brightrweb.com/wp-content/uploads/2022/06/Vector.png">
                        </div>
                        <div class="worth_text_area1 ml10">
                            <div class="worth_text f-20 f-md-38 white-clr w500">Exclusive Bonus 11</div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- bonus11 Ends -->


                 <!-- bonus12 Starts-->
                 <div class="col-12 mt20 mt-md60 relative">
                    <div class="bonus-wrap row d-flex align-items-center flex-wrap ">
                        <div class="col-12 col-md-6 mt30 mt-md90">
                            <div class="w700 f-22 f-md-40 black-clr lh140 mt20">
                                PromptStrongbox Notifier For Expired Domains
                            </div>
                            <div class="f-18 f-md-22 w400 black-clr lh140 mt15">
                                Domain Notifier is a Windows app that can help you to extract external domains from a website like CNN or Wikipedia and check to see which domains are available to register.
                            </div>
                            <div class="worth-box">
                                <div class="worth_area">
                                    <div class="worth_area_img">
                                        <img decoding="async" src="https://bonus.brightrweb.com/wp-content/uploads/2022/06/Vector.png">
                                    </div>
                                    <div class="worth_text_area">
                                        <div class="worth_text f-20 f-md-38 white-clr w500">Exclusive Bonus 12</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 ">
                            <div class="mt20 mt-md0">
                                <!-- <a href="https://jvz6.com/c/1188867/396842/"> -->
                                    <img class="lazyload img-fluid d-block mx-auto" data-src="assets/images/bonusbox.png" alt="bonusbox" style="max-height: 480px;">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            <!-- bonus12 Ends -->

            
            <!-- bonus13 Starts-->
            <div class="col-12 col-md-12 mt20 mt-md60 relative">
                <div class="bonus-wrap row d-flex align-items-center flex-wrap">
                    <div class="col-12 col-md-6 order-md-2">
                        <br><br><br>
                        <div class="w700 f-22 f-md-40 black-clr lh140 mt20">
                            Youtube Faceless Videos Channel Builder
                        </div>
                        <div class="f-18 f-md-22 w400 black-clr lh140 mt15">
                            Unlock exponential growth with the YouTube Faceless Videos Traffic Machine & Channel Builder. Transform your content into a captivating powerhouse, driving traffic and forging an influential channel.

                           <br><br><br>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 order-md-1">
                        <div class="mt20 mt-md0">
                            <img class="lazyload img-fluid d-block mx-auto" data-src="assets/images/bonusbox.png" alt="bonusbox" style="max-height: 450px;">
                        </div>
                    </div>
                </div>
                <div class="worth-box1">
                    <div class="worth_area1">
                        <div class="worth_area_img1">
                            <img decoding="async" src="https://bonus.brightrweb.com/wp-content/uploads/2022/06/Vector.png">
                        </div>
                        <div class="worth_text_area1">
                            <div class="worth_text f-20 f-md-38 white-clr w500">Exclusive Bonus 13</div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- bonus13 Ends -->
            
              <!-- bonus14 Starts-->
              <div class="col-12 mt20 mt-md60 relative">
                <div class="bonus-wrap row d-flex align-items-center flex-wrap ">
                    <div class="col-12 col-md-6 mt30 mt-md90">
                        <div class="w700 f-22 f-md-40 black-clr lh140 mt20">
                            PromptStrongbox Time For WordPress
                        </div>
                        <div class="f-18 f-md-22 w400 black-clr lh140 mt15">
                            This is a responsive, modern, and clean domain sale/coming soon Wordpress plugin. This minimal design is packed with a countdown timer, social icons, and about page where you can write a little bit about yourself and add other information                        </div>
                        <div class="worth-box">
                            <div class="worth_area">
                                <div class="worth_area_img">
                                    <img decoding="async" src="https://bonus.brightrweb.com/wp-content/uploads/2022/06/Vector.png">
                                </div>
                                <div class="worth_text_area">
                                    <div class="worth_text f-20 f-md-38 white-clr w500">Exclusive Bonus 14</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 ">
                        <div class="mt20 mt-md0">
                            <!-- <a href="https://jvz6.com/c/1188867/396842/"> -->
                                <img class="lazyload img-fluid d-block mx-auto" data-src="assets/images/bonusbox.png" alt="bonusbox" style="max-height: 450px;">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- bonus14 Ends -->

            
            <!-- bonus15 Starts-->
            <div class="col-12 col-md-12 mt20 mt-md60 relative">
                <div class="bonus-wrap row d-flex align-items-center flex-wrap">
                    <div class="col-12 col-md-6 order-md-2">
                        <div class="w700 f-22 f-md-40 black-clr lh140 mt20">
                            PromptStrongbox Website Calculator
                        </div>
                        <div class="f-18 f-md-22 w400 black-clr lh140 mt15">
                            PromptStrongbox Website Calculator is a tool that calculates the estimated price of any website. It’s useful for webmasters to see how their work positively or negatively affects the result. In addition, the script allows users to sell their websites. 
                        </div>
                    </div>
                    <div class="col-12 col-md-6 order-md-1">
                        <div class="mt20 mt-md0">
                            <img class="lazyload img-fluid d-block mx-auto" data-src="assets/images/bonusbox.png" alt="bonusbox" style="max-height: 450px;">
                        </div>
                    </div>
                </div>
                <div class="worth-box1">
                    <div class="worth_area1">
                        <div class="worth_area_img1">
                            <img decoding="async" src="https://bonus.brightrweb.com/wp-content/uploads/2022/06/Vector.png">
                        </div>
                        <div class="worth_text_area1">
                            <div class="worth_text f-20 f-md-38 white-clr w500">Exclusive Bonus 15</div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- bonus15 Ends -->
           
            
            <div class="row">
                <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt20 mt-md50">
                    <a href="https://jvz8.com/c/1188867/398705/" target="_blank" class="text-center bonusbuy-btn">
                    <span class="text-center"> Checkout PromptStrongbox Sales Page</span>
                    </a>
                </div>
                <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt20 mt-md50">
                <a href="https://jvz8.com/c/1188867/398720/" target="_blank" class="text-center bonusbuy-btn">
                <span class="text-center"> Checkout PromptStrongbox Bundle Page</span>
                </a>
                </div>
                <div class="col-md-12 col-md-12 col-12 text-center mt10">
                    <div class="f-16 f-md-20 lh150 w400 text-center black-clr">
                        Use Coupon Code <span class="w800 black-clr"> PSB80OFF </span> for <span class="w800 black-clr"> $80 OFF </span> on Bundle Deal
                    </div>
                </div>
                <div class="col-8 col-md-6 mx-auto">
                        <div class="f20 f-md-30 text-center black-clr mt20 w700">Bonuses Expire In...</div>
                        <div class="countdown counter-white text-center ">
                           <div class="timer-label text-center"><span class="f-24 f-md-28 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-16 w500 smmltd">Days</span> </div>
                           <div class="timer-label text-center"><span class="f-24 f-md-28 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-16 w500 smmltd">Hours</span> </div>
                           <div class="timer-label text-center timer-mrgn"><span class="f-24 f-md-28 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-16 w500 smmltd">Mins</span> </div>
                           <div class="timer-label text-center "><span class="f-24 f-md-28 timerbg oswald">00</span><br><span class="f-14 f-md-16 w500 smmltd">Sec</span> </div>
                        </div>
                     </div>
                    
            </div>
            <!-- bonus16 Starts-->
            <div class="col-12 mt20 mt-md60 relative">
                <div class="bonus-wrap row d-flex align-items-center flex-wrap ">
                    <div class="col-12 col-md-6 mt30 mt-md90">
                        <div class="w700 f-22 f-md-40 black-clr lh140 mt20">
                            WP Funnel Profit
                        </div>
                        <div class="f-18 f-md-22 w400 black-clr lh140 mt15">
                            Get Instant Access to the 30 Brand New WordPress 'How To' Videos!                         </div>
                        <div class="worth-box">
                            <div class="worth_area">
                                <div class="worth_area_img">
                                    <img decoding="async" src="https://bonus.brightrweb.com/wp-content/uploads/2022/06/Vector.png">
                                </div>
                                <div class="worth_text_area">
                                    <div class="worth_text f-20 f-md-38 white-clr w500">Exclusive Bonus 16</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 ">
                        <div class="mt20 mt-md0">
                            <!-- <a href="https://jvz6.com/c/1188867/396842/"> -->
                                <img class="lazyload img-fluid d-block mx-auto" data-src="assets/images/bonusbox.png" alt="bonusbox" style="max-height: 450px;">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- bonus16 Ends -->


                 <!-- bonus17 Starts-->
                <div class="col-12 col-md-12 mt20 mt-md60 relative">
                <div class="bonus-wrap row d-flex align-items-center flex-wrap">
                    <div class="col-12 col-md-6 order-md-2">
                        <div class="w700 f-22 f-md-40 black-clr lh140 mt20">
                            Unique Exit Popup
                        </div>
                        <div class="f-18 f-md-22 w400 black-clr lh140 mt15">
                            Grab the Attention of Your Visitors When They Are About to Leave Your Website! Traffic is very important to every website. But what if those people who visit your website will just go away doing nothing?                        </div>
                    </div>
                    <div class="col-12 col-md-6 order-md-1">
                        <div class="mt20 mt-md0">
                            <img class="lazyload img-fluid d-block mx-auto" data-src="assets/images/bonusbox.png" alt="bonusbox" style="max-height: 450px;">
                        </div>
                    </div>
                </div>
                <div class="worth-box1">
                    <div class="worth_area1">
                        <div class="worth_area_img1">
                            <img decoding="async" src="https://bonus.brightrweb.com/wp-content/uploads/2022/06/Vector.png">
                        </div>
                        <div class="worth_text_area1">
                            <div class="worth_text f-20 f-md-38 white-clr w500">Exclusive Bonus 17</div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- bonus17 Ends -->

            
            <!-- bonus18 Starts-->
            <div class="col-12 mt20 mt-md60 relative">
                <div class="bonus-wrap row d-flex align-items-center flex-wrap ">
                    <div class="col-12 col-md-6 mt30 mt-md90">
                        <div class="w700 f-22 f-md-40 black-clr lh140 mt20">
                            WP Membership Plugin
                        </div>
                        <div class="f-18 f-md-22 w400 black-clr lh140 mt15">
                            The effortless way to create professional sites in Wordpress using your favorite membership plugin!                        </div>
                        <div class="worth-box">
                            <div class="worth_area">
                                <div class="worth_area_img">
                                    <img decoding="async" src="https://bonus.brightrweb.com/wp-content/uploads/2022/06/Vector.png">
                                </div>
                                <div class="worth_text_area">
                                    <div class="worth_text f-20 f-md-38 white-clr w500">Exclusive Bonus 18</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 ">
                        <div class="mt20 mt-md0">
                            <!-- <a href="https://jvz6.com/c/1188867/396842/"> -->
                                <img class="lazyload img-fluid d-block mx-auto" data-src="assets/images/bonusbox.png" alt="bonusbox" style="max-height: 450px;">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- bonus18 Ends -->
            
              <!-- bonus19 Starts-->
            <div class="col-12 col-md-12 mt20 mt-md60 relative">
                <div class="bonus-wrap row d-flex align-items-center flex-wrap">
                    <div class="col-12 col-md-6 order-md-2">
                        <div class="w700 f-22 f-md-40 black-clr lh140 mt20">
                            Latest Humans Stock Images
                        </div>
                        <div class="f-18 f-md-22 w400 black-clr lh140 mt15">
                            Stock Images For You To Use In Your Projects And Your Clients Projects. Plus You Can Resell Them!                        </div>
                    </div>
                    <div class="col-12 col-md-6 order-md-1">
                        <div class="mt20 mt-md0">
                            <img class="lazyload img-fluid d-block mx-auto" data-src="assets/images/bonusbox.png" alt="bonusbox" style="max-height: 450px;">
                        </div>
                    </div>
                </div>
                <div class="worth-box1">
                    <div class="worth_area1">
                        <div class="worth_area_img1">
                            <img decoding="async" src="https://bonus.brightrweb.com/wp-content/uploads/2022/06/Vector.png">
                        </div>
                        <div class="worth_text_area1">
                            <div class="worth_text f-20 f-md-38 white-clr w500">Exclusive Bonus 19</div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- bonus19 Ends -->

            
            <!-- bonus20 Starts-->
            <div class="col-12 mt20 mt-md60 relative">
                <div class="bonus-wrap row d-flex align-items-center flex-wrap ">
                    <div class="col-12 col-md-6 mt30 mt-md90">
                        <div class="w700 f-22 f-md-40 black-clr lh140 mt20">
                            100+ WebPage Templates
                        </div>
                        <div class="f-18 f-md-22 w400 black-clr lh140 mt15">
                            Why would you want to pay a lot for a website when you can use these 100+ Webpage templates to create professional looking websites for only pennies!                        </div>
                        <div class="worth-box">
                            <div class="worth_area">
                                <div class="worth_area_img">
                                    <img decoding="async" src="https://bonus.brightrweb.com/wp-content/uploads/2022/06/Vector.png">
                                </div>
                                <div class="worth_text_area">
                                    <div class="worth_text f-20 f-md-38 white-clr w500">Exclusive Bonus 20</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 ">
                        <div class="mt20 mt-md0">
                            <!-- <a href="https://jvz6.com/c/1188867/396842/"> -->
                                <img class="lazyload img-fluid d-block mx-auto" data-src="assets/images/bonusbox.png" alt="bonusbox" style="max-height: 450px;">
                            </a>
                        </div>
                    </div>
                </div>
            </div>

           
            <div class="row">
                <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt20 mt-md50">
                    <a href="https://jvz8.com/c/1188867/398705/" target="_blank" class="text-center bonusbuy-btn">
                    <span class="text-center"> Checkout PromptStrongbox Sales Page</span>
                    </a>
                </div>
                <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt20 mt-md50">
                <a href="https://jvz8.com/c/1188867/398720/" target="_blank" class="text-center bonusbuy-btn">
                <span class="text-center"> Checkout PromptStrongbox Bundle Page</span>
                </a>
                </div>
                <div class="col-md-12 col-md-12 col-12 text-center mt10">
                    <div class="f-16 f-md-20 lh150 w400 text-center black-clr">
                        Use Coupon Code <span class="w800 black-clr"> PSB80OFF </span> for <span class="w800 black-clr"> $80 OFF </span> on Bundle Deal
                    </div>
                </div>
                <div class="col-8 col-md-6 mx-auto">
                        <div class="f20 f-md-30 text-center black-clr mt20 w700">Bonuses Expire In...</div>
                        <div class="countdown counter-white text-center ">
                           <div class="timer-label text-center"><span class="f-24 f-md-28 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-16 w500 smmltd">Days</span> </div>
                           <div class="timer-label text-center"><span class="f-24 f-md-28 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-16 w500 smmltd">Hours</span> </div>
                           <div class="timer-label text-center timer-mrgn"><span class="f-24 f-md-28 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-16 w500 smmltd">Mins</span> </div>
                           <div class="timer-label text-center "><span class="f-24 f-md-28 timerbg oswald">00</span><br><span class="f-14 f-md-16 w500 smmltd">Sec</span> </div>
                        </div>
                     </div>
                    
            </div>
                <!-- <div class="row">
                    <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt20 mt-md50">
                       <a href="" target="_blank" class="text-center bonusbuy-btn">
                       <span class="text-center"> 🎁 Surprise Download Launch Special 500+ bonuses 💝 </span>
                       </a>
                    </div>
                </div> -->
                <br><br>
         </div>
               
    </div>
</div>
            <!-- bonus20 Ends -->
            <div class="footer-section" >
                <div class="container">
                   <div class="row">
                      <div class="col-12 text-center">
                        <img data-src="https://cdn.oppyotest.com/launches/sellero/jv/images/atul-pareek.webp" class="img-fluid d-block mx-auto minus lazyloaded" alt="Atul Pareek" src="https://cdn.oppyotest.com/launches/sellero/jv/images/atul-pareek.webp" >
                        <div class="f-24 f-md-30 w600  lh140 text-center white-clr mt20">
                            Atul Pareek
                         </div>
                         <div editabletype="text" class="f-16 f-md-20 w300 mt10 lh140 white-clr text-center">If you have any inquiries or concerns, please feel free to reach out to me. I'm here to assist you at all times.</div>
                      </div>
                      <div class="col-12 d-flex justify-content-center mt20" >
                        <div class="col-12 d-flex justify-content-center">
                            <a href="https://www.facebook.com/profile.php?id=100076904623319" class="link-text mr20">
                            <img data-src="https://cdn.oppyotest.com/launches/sellero/jv/images/am-fb.webp" class="center-block ls-is-cached lazyloaded" alt="Facebook" src="https://cdn.oppyotest.com/launches/sellero/jv/images/am-fb.webp">
                            </a>
                            <a href="skype:live:.cid.c3d2302c4a2815e0" class="link-text mr20">
                               <div class="col-12 ">
                                  <img data-src="https://cdn.oppyotest.com/launches/sellero/jv/images/am-skype.webp" class="center-block ls-is-cached lazyloaded" alt="Skype" src="https://cdn.oppyotest.com/launches/sellero/jv/images/am-skype.webp">
                               </div>
                            </a>
                            <a href="https://support.oppyo.com/hc/en-us" class="link-text mr20" id="contact">
                                <img src="assets/images/contact.webp" class="center-block" alt="contacts">
                            </a>
                         </div>
                      </div>
                   </div>
                </div>
            </div>
        

            <script src="https://cdn.eduncle.com/webfiles/js/lazysizes.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap/5.2.3/js/bootstrap.min.js"></script>
    <script src="https://cdn.oppyotest.com/launches/sellero/common_assets/js/bootstrap.min.js"></script>
    <script src="https://cdn.oppyotest.com/launches/sellero/common_assets/js/jquery.min.js"></script>
    <!-- Exit Popup and Timer -->
    <?php include('timer.php'); ?>	  
    <!-- Exit Popup and Timer End -->

    </body>
    </html>
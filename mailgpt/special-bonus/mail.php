<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <!-- Tell the browser to be responsive to screen width -->
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
      <meta name="title" content="MailGPT Bonuses">
      <meta name="description" content="MailGPT Bonuses">
      <meta name="keywords" content="World’s First ChatGPT AI Powered Email Marketing App Writes, Design & Send Unlimited Profit Pulling Emails and Messages with Just One Keyword for Tons of Traffic, Commissions, & Sales on Autopilot">
      <meta property="og:image" content="https://www.mailgpt.live/special-bonus/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Pranshu Gupta">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="MailGPT Bonuses">
      <meta property="og:description" content="World’s First ChatGPT AI Powered Email Marketing App Writes, Design & Send Unlimited Profit Pulling Emails and Messages with Just One Keyword for Tons of Traffic, Commissions, & Sales on Autopilot">
      <meta property="og:image" content="https://www.mailgpt.live/special-bonus/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="MailGPT Bonuses">
      <meta property="twitter:description" content="World’s First ChatGPT AI Powered Email Marketing App Writes, Design & Send Unlimited Profit Pulling Emails and Messages with Just One Keyword for Tons of Traffic, Commissions, & Sales on Autopilot">
      <meta property="og:image" content="https://www.mailgpt.live/special-bonus/thumbnail.png">
      <title>MailGPT Bonuses</title>
      <!-- Shortcut Icon  -->

      <!-- Css CDN Load Link -->
      <link rel="stylesheet" type="text/css" href="assets/css/style.css">
      <link rel="stylesheet" type="text/css" href="assets/css/bonus-style.css">
      <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
      <link rel="preconnect" href="https://fonts.googleapis.com">
       <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&family=Inter:wght@300;400;500;600;700;800;900&display=swap" rel="stylesheet">
      <link rel="stylesheet" href="../common_assets/css/bootstrap.min.css" type="text/css">
      <script src="https://cdn.oppyo.com/launches/writerarc/common_assets/js/jquery.min.js"></script>
   </head>
   <body>
   
      <!-- New Timer  Start-->
      <?php
         $date = 'March 7 2023 11:59 AM EST';
         $exp_date = strtotime($date);
         $now = time();  
         /*
         
         $date = date('F d Y g:i:s A eO');
         $rand_time_add = rand(700, 1200);
         $exp_date = strtotime($date) + $rand_time_add;
         $now = time();*/
         
         if ($now < $exp_date) {
         ?>
      <?php
         } else {
         	echo "Times Up";
         }
         ?>
      <!-- New Timer End -->
      <?php
         if(!isset($_GET['afflink'])){
         $_GET['afflink'] = 'https://warriorplus.com/o2/a/cz90pc/0';
         $_GET['name'] = 'Pranshu Gupta';      
         }
         ?>

            <!-- Header Section Start -->   
            <div class="main-header">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="text-center">
                     <div class="f-md-32 f-20 lh140 w400 white-clr d-block d-md-flex align-items-center justify-content-center">
                        <span class="w600 blue-gradient"><?php echo $_GET['name'];?>'s</span> &nbsp;special bonus for &nbsp;
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 620.04 134.95" style="max-height:60px;"><defs><style>.cls-1{fill:none;}.cls-2{clip-path:url(#clip-path);}.cls-3{clip-path:url(#clip-path-2);}.cls-4{clip-path:url(#clip-path-3);}.cls-5{clip-path:url(#clip-path-4);}.cls-6{fill:#fff;}</style><clipPath id="clip-path"><path class="cls-1" d="M40.14,36.57l13,12.24v77.25a6.51,6.51,0,0,1-6.51,6.52h0a6.52,6.52,0,0,1-6.52-6.52Z"></path></clipPath><clipPath id="clip-path-2"><path class="cls-1" d="M170.36,36.57l-13,12.24v77.25a6.52,6.52,0,0,0,6.52,6.52h0a6.51,6.51,0,0,0,6.51-6.52Z"></path></clipPath><clipPath id="clip-path-3"><path class="cls-1" d="M169,15,108,72.83a4,4,0,0,1-5.49,0L41.59,15A6.51,6.51,0,0,1,46.07,3.73h0a6.56,6.56,0,0,1,4.49,1.78l50.57,48a6,6,0,0,0,8.33,0L160,5.52a6.5,6.5,0,0,1,4.48-1.79h0A6.51,6.51,0,0,1,169,15Z"></path></clipPath><clipPath id="clip-path-4"><path class="cls-1" d="M101.27,81.56,35,19A9.6,9.6,0,0,0,18.8,26v99.87a6.52,6.52,0,0,0,6.51,6.52h0a6.52,6.52,0,0,0,6.52-6.52V34L95.92,94.44l5.35,5a6.06,6.06,0,0,0,8.31,0l5.34-5L179,34v91.91a6.52,6.52,0,0,0,6.52,6.52h0a6.52,6.52,0,0,0,6.51-6.52V26a9.6,9.6,0,0,0-16.19-7L109.58,81.56A6.06,6.06,0,0,1,101.27,81.56Z"></path></clipPath></defs><title>MailGPT White Logo</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><g class="cls-2"><image width="90" height="99" transform="translate(-0.75 36.16)" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFoAAABjCAYAAAAb3RdnAAAACXBIWXMAAAsSAAALEgHS3X78AAAS0klEQVR4Xu2cTahtyVXHf2ufeyOKiNLddhJbE6XVNtF8dqskDsSRM0eRiIKoiIgfAyeKIgTEiYIDJQMRBBUxmpEzBw4caBvtl26bjtEmrbZGOunOaxQRUfuevRysj1pVZ597zv0495wHFrx7a9de9bH/9a//WlV7vyuqyimnp773LxTgmT/6oOyyPeUkpwz0kw5yJOHBBXzaZXDUJOUfoMCTH/pzfWqYgAchnSyjn/ywg1mH1+UVEXlgGH6yjNaArzC6z0syfKx7iukkGf3+73tag76yldE1f/rsPttlcJQk/kNBC9IdihXowu5TBfzkGP3+7396izY/2Aw/KY1+3w88vUxfyR+oFExH/c7y09PvkwIaFoCseXFkpTjLajfyV+RkwsGTkY73/uBfmhyHTOgumdDML2r3+Fhq83QsOTkdoH/oE4oWgBXQfXS5qI2yCHB/fRz9Pgmg3/Mjn9AGsDrIztR5B3MzPwA+2i0Afu/j33FnYJ+ERuskMIFOltdJYOU3RdLpLepy5lvBVv0eHOZd6vfRGf3uH/srDQaj2tg4MhyQmevJyWgzXt+BnBwV6Hf9+F9v6jIgRT5MMtq1zF75KoBrIfORAD+qdOgETAKTuGzgEiEpJ0x+7RISNiEpl8bVJRzMNEI4yIlymPj7aIz+lp98xmgaTE0Gqz17ve7YbmXMNDtvZyu7q52nnRHKLbP7aEB/8089owjIPIANdHICPeCArLWBotcHfC/9RhFuDvhRgH7nz9yzcK6CPI/gag9i2NHqgNtH3bDdBfgS2KPNeH1Dhh9Fo3XlmrxqWqyTtPLQ10lMg0N7U6/9OjR6anqdoV7ci07zXuStYCMU3HZ9Q/2+c0Z/088+q53uqjJdaEMkmLqN4dVuDAdjhUDR4F3sbheHZPedA/3Ezz/bdoFzD7jFyQXkuisswLU6druGg7WO3Wv1LgU8r7W3Ge2uCfidAv2Nv/hcp80ZQSgN5LwewFN6vV4CfG2ZbLfY763ftLq7JyXyuwG/M6C/4SN/o8wFyABjbvmR3R07twE+hoMUhkd/tY0CdrYVaQAvyrbajNeXAH5nztCcHebwwulNAisv9zOO2MSkc4yzjqnUrY6vOsy64REBb5faxoRfizvXMsguL1m21Wa8FkFVWTo/uRNGP/7Lz3encyETlcVLDE8nmSwu113eO9rmMH3bXqWqXbthrRd183e7cV05uTugRazzNQPAA2BbpYWi7aXORnTi7WfeijvNp/UDOwCPdvK3tjpLNht5A/zgQH/dr76gAU7d0cm6d2ZL4FXAR7C3Ak5rM4Gbe1soE0ntp9XJtiJp/V3qLdkMeeEOPjfQM/Gdm+mmOEsB++0gqu8uZFbTVQdSFUTFMFEFkYwuVIqd5+NFgQiEuOrK+q8TZO15fdRMvczXuts4mB2i4mOxKsnuaGPIm80BGf32X/9brbFyi4+haXVj+k45mWv+khUx1ifsiqRE/bzn9aj9+kVtJ+zzd7uxVb/1gEC/7aOf1g3HVxgcoVd3f9DvXstpIF9UkHZL0KKcLNhD6Qc6u65etBdJ88ci2Pf+8INyMOnQFaDiS0iRWVDx3woiis4WmsnaHEYu3wTcbctSFwU9x8tHe5oE6Q45EbefrdNQBre0CZCGp6iAaIdvTmBoi1q7Zm9F9z5mUcdBgH7st/5OZYpRagN8htAuA9vYyUSSSBB0FROyAF4CruhsNViBzKSO7wIcceAcTPAJb4NAfVLxJWET209HTkJcLAAe6SBA68ofLNCMhyx5UWO4BfnFSa7LxCjGVmfc+FMm60MVaztWxg7AZXZm5jitD+Y2bo9GSYcpzTmmwyyAS7v0sQuf/IMPxMzcPtBf9TsvKitB12rsnEyHdWoslMnYyiy2m1MHF4xt6xIVVMD1EjmJSV2HDAhKlQRBApgYiwMTkqKTTQIxWVFffMK81SWGJ9h2WS4s3TrQMamcCVwUsBV0FmTyByx5UUFnRSZ7INPtHswR8BauaWPZWm1L7mU1LNyUoIHdeBcraWcsPjWSFsOEpZUNTiG1+ZO/39gMtwz0mz/2GZXzAAA4MxymCxtGAMmMg+xsnNVZDslaMLsZGmOqfhfbAE8i75QbVsEGyLogJ2AvjOeezZtgQ0Bti6mBvZRuLbx79OMvlXDOGBQPEBuUOLtYDMEcuMjLG9rbdXnvYyMcdDuaXe5GO7uFOmU8GXLS22W90n6uKNr1s7/XsxlukdF9OEfu+sDKZFbmM+t/uiAfUEWabdFbPZdkpijOzvinpu+DnDR50LwVUiCzaTaQi79JQK/fVtkttJKhrCRvH5EEWIHnfncTZLgloB/5439QJvHtsw8IX7IOspat83zm+QBFsHg2mSVe1pa96XYD07CIGwyANzlBscn08eAT2U1K5n3yYwy+OnQlyBoi9mbUfmiAb0m3AnSGcxPOzj6vE0xvaHp7AD2zwZp+g0yaDxB6KaNjA1gbE3UiwdCZRf3O0OxSJ7mHw1RFVwyrwn4qII72c7/97YtshlsA+qE/+SdjcwwM2kPSHmw+p7ExdVCZ3XnuIye4DG1IRSz1ueW7DU/G1jYGwm84kD3gCyCHnSiytj7apJUxXJJuDLSuxHtrg0Gl7QIDsAoeYstwLR7DbgLetVlAlIsiTwmSPWzIzYacjEs+cFGTCWurBy5CvE6/EZjczpFVTPqe/81vuxTqGwH9FX/6stJpMjhybYanNvAl/WZtTx4PE/rdb0wc5NkmpANmTZOrmNBhchJw3ImJ+451AGuTk/kZC/Fq/5HP9mk7XHana4d3X/5n/6zRYXhdWYck2KBsMC0fy1ZioGr22w7z26usVmcj5JvpXiJEWbXtj2WH9lxGFsPHHOdCv6X+Cx/91kvZDDdgdDjAcEaxXFVpEcWMU6awrDIumS45+DzM0V5OtNRp/Uq+kO12jPRMzDGI2eQJ4lqtOFad95HOFc2Fqd4NWkCY4IXf2A0yXBPoL3v6s2pLS/O8Ih5MZrVJcJBte10eXCDeuIjC/CacUdaeVMAc0BHw6ng7aVljS18gDog2AC8O0ybdnSbAhY9LvJ0gixZnGW142/umawFtbHZmxKBVzFGJDzRYtNay9XaNrIc645mHKKwlJaA6o/nMnVScspXVEk4ytZbmJA0o2kQHQTwfEYpmfO/tVL/i4xEkz24+9WtPyRaINtKVgf7Se/+q5rWtQ7QM5Mwe2kAjI49YpiLiMlMAEpOA6pTA69IzMpyRTjC5PEUIZ6x3cBRfGRQgaYDXyZ0DOB+7YLG699WFmZXpuonNZenKQHdbVBXswN07F9LZGXu0sK8AHhoZIFLaU1CbAVPJCnLJz6710xpfGW21WOPSzra9/aXln6siHaagZ8CFVesOv9TqCfDpX3nf3myGKwL9Jc+/4uGcswKc1T6QWRdP4Wyn58Ct+okaJQBVmATxnWSycJSAEfDiMBug5Na5O9RXD+/m7Q4zn9P7TTkRB/yKae/w7os/9Tm1GScf0jonw7Pw2pkvodxSCNd/2+H5MTSLeyV0rLbdOMYwr5Z3Y7ykDVUvazay1uIU4cVfeu+V2AxXYbSAvXnwJVp/VudWl5mzLZ0kiklIsAg0AXIW0lhb20TEo4JBAnzZp34HMMH6tfc5VT8AcW6RoV3dCVb9xlahYP2++JH3XBlk2BPoL/r7z6u9n8Pes9EeRucKOSYPnsv8hpPsJSAncJvDVLWyM/EQjg7wcKhpWwEXocpOOslRrsYdZbGRmXSS1017AR3b6HyQCninjaPdwKKinXEKB/5MiJ1pR/y9xWHGcWs96wiHtxihaB+hIOKhYPErCCqNKsYGH5gX6SS89AvvuhabYQ+g3/TSqxoRAJAPqkZjYyvLgC/KSQAuCuth8lQg2sVsk9XiE3eh/ZHnIAGoabnDB8Ach/8+OXpu400mz9oYXfMxlii/QdoJtHbe10EJNgd4vqxZOgwagOxYtKJ4/pggSE1W9RDNy1ThXEpbmru5WPbjJmpJvwWxEC7iZRoRBGnPlLtf+Mefuz6bYQfQ5y+/pogzxssUIQ66bY6b86j6vS0cUyUZY04SP/BrUtADjnU0yIVNmKBnwrRWNmP5kKllwO1FBF2drr/K5okbp63h3dm/vKYGDo0tYeplGRbhdrG81Ma5/GK01Mv27Xox3NPW9vjitwsfs65uhGeN7Qv9e9iXL45rGOjtv/zT77wRm+ESRm+EQ5VNAELPWiDetIA/x0oMcH9dBepEceZjUYl4qR1GBThFgmIZF3YDuTkKprJWNs5OAmCk/74kRuTvOudzYboIGdH80Ofln3jHjUGGLUCvXvmCYqu7geAPuQl4XJPhVMCK5/SsAB511SZqQ7+Fzc/A8nTQ7Jb0W9YeSeBOU2lyQvS5XU5QmM8o4R8+2NtJ2zVa7J+uQeLB6j167QbcSfpDKLkJAK++p8NEGOJlttg1wO0j97DTDOmowEZ783bAdfLXaSif/dHbYTMsAD197r4aK61zVj7W8aUkNDYH4OIPik/DSogvjYi52sNhssYdFW35q133uzho0UsZmwpzxNuzdjJop3U0wKEdHEW7Z3KjzclS2mS0kxLIJSVg/z1htgfbABxBpV5HXfXPDsTLG0Cp3wvMVaEASznyxAsaO+23EJ835DGp+hguyFUQR6LdufIcE2b2Jj3wyg8/cWtshgHo6dX7hkSiZXkFUHcy+IO7Fscg42cvJ5Uxdm3V9gM8GJ6OTkmGt3wDEhFSTuIQ69zAtoGoTXoQJQ//477/voVwbkwZ3smr98NtEIzeyFuFdrF2eINZNCZCgIE5SWhtaWnH7QVcv6Os5TNUyzOQsay0OdSZLjQ1GKXLL4VyKHz+w19/q2yGymjfmKCERLdZjjyk0wFN/aZ8n6GQGxCz0jxUqoAkYwHEGTkeWCW7vT1R8xXESip6qzDuMEXFIol6L3aSsz9bTJ4v3Vc/9PitgwwOtHzhdYdRvL/LAY+HTPRX5JtjYllGlS0OMx2PT0oFfFNOMJDSSdbwjd1yEhudWdBzse+2xSc7HsOJcqhkjB7nUHcALjEmY4GgdsIHJidFv1vzkiUC3ScG0aUBrtl+A1whIgmvo2oT0QAP8PHKkhOEt20OU4tue6zu/b72PYdhM8CZ3A82lxSzjCTLpCunmxytgG+TEyCiBUXdIQn5wXdpSxRcn0hJ6c6wPZ9OconhuN0yw/MEz8fY2HSYJKpKB3aFXUsmwL7ULvApBeXcIYrTyUVk4HWaI/X70VbYE+x2Owc826mObrAZ8+kA3QG//t1fe1CkJwB9+KHWSe1OSkbI1bho59dmUwomLxPJ8woVyL+ptLINR/x9pXzLPrlNhGzezrwS21BMUP/cRNpMQv939CwutyG1vIrX9zEcOmV4lwU72e0XusDwvim3KYWV3XE/bgfj5rjej+GsY7sdNrqxiiJvLC52fu/fvuttB0d6A+i8cR3At9mljRdeIiewEBtTrin14rrGw4rLgduMclKPVmfl37/z8CDDZcekLidy//XmrRS6PAKigc9mdBJgS2DoYZzvMEeHac3Zsm/hnzighqq6oQ3OHdzkwebanawI8X9eNr6E8vbyldUdpa2M7oyuw+56f6gzshsKw4H80DHLo+7+ciIYsze+24iYWuE/PvDVd4b0zneGwBXYDRl7e9E+7AbahgfIv68Rbcdyj82POnjBcC0rQ8kdplVRui9Lxex7khw+7cXoMV2J4WPzC9cjw6te5/1Bh61cM592g37L0v9tVOU/n3zsztgMezJ6TFdiuBSwxuR1DNNBv2cQo59dT+JHmurshTz79j5Svwu74+2OqriUKLla7jBdi9E1bWV3d62Zl9FuyI/shmBxMNNvh16HubIZoahfxDXG8P9691vvHOkbAx3pSnKyUc5GnUXA46XsLrC9rAshFRD473e85c5BhmtKx1K6kpw4Eh3gQx0j4+Aw8wYGcvnAHMFDOEnArUvpAT9SujVGj2mnpGibjw2bBfuR4cnksgHpwj9lw1n+zxNvPgqb4SAvbSxtPT+JaxE0/0VZvd/bm//ywjw/gfiTmUTezznwcxL1M5DDPel+6WCMrmknu8OujmUbwxfYDfSvpSq73fR/H3/0aGyGW9Toy9JW/YZOm+PNeZJ3wWabfhOhoNerH7YfU5sj3Qmjx7QzQrlMv8frkeFK9zJYFN54+1celc1wJKBhD7CdidcFPM5LLr7m+CDDEYGOdKl+A7lR6cq25KFtUgBmWD/2yP8DXdNuhzkAvgXsAFoR5rc+fBIgw9GDnpb04YckQ8II1yKFd5SCabUp+XxtdmLpZBhd05X1e8FmfsvpsBlOiNE1bWV3QifEi1ZdsjkpiC2dJKPHtPHtiQ6/B4bPj54Wm+EBATrSIuAD2HqCIMOJSse21J2fQJOJdJYniTHwgDG6piWHqY88dLJI38lZxyHS4vnJCaf/A7yQ0Ala9tJfAAAAAElFTkSuQmCC"></image></g><g class="cls-3"><image width="90" height="99" transform="translate(121.25 36.16)" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFoAAABjCAYAAAAb3RdnAAAACXBIWXMAAAsSAAALEgHS3X78AAAO2klEQVR4XtWbzet1VRXHv2uf+1MQokDNxExD1MpAyMd8Cfoznhw1bRA0cBRUA2kSURGRVFhkb1aOmjVrEDyE+LxqKiYSRophDaJBFHl3g73X2mutvc85+/7u2/ktuM+5Z5919stnf/d3n3t/96EYI85yPHj+QgSA55/9FM3lHjPorIJ+8PyFqucXFwx7NZewtGAFnzV5nBlF9wJeqqrPhKJbNlHFIvGWWLSie1XsIV/89fJUvUhFnxbwkmNRit4l4KWpOswlHCrYh3cBGQSce+zCbFWHjKNbxy5V3JVzpDiadXQDBvoAqpyozi/9chkWcnBFdwPuxePyopxTfx0HiIMquut5GOgD1AuYgEu/eLSnxr3GQRS9bxWXcnUhv51t80CxV0XvG3BSca1gez3F5Z8fV9V7Ad0NGOiD7AFLWRuy3gxLOeHyzx7paW0vsXPr6PbhnmhgmVLxGOCuydxz7EzR3SruHXTTJvIFKm/r61xOJocn4cpPjqPqrRXdDRjogzwKWF0cAyzX2pCPGadW9EaAgfmBTgEee5qoVKzecJ5WPxEigKs/fniuNzuPUyl6Ix+eG5IHbMpo3ibk2gRgAJFyXXP92VNspOh9qrgLsDufBKzLgs0BAVd/eFhVdyl6I8A93fcqNuc15C7AOocAEDVUncqPEZOKPhpgfb2h0FEVM2Cdm+tIZWTKrv3goZ5e7ySaoDcCDMxD9gBNmQLcUqfP1XnuvpaKQdmffVk+vvD9w8CurGMjyD1dVDmjgPPpqQGjsdkRENEqA8C5BwxR9E4Bu+uTgHNR15OEem+P2S4CZgDnf8iWvfjkJ+dGtHWsjg7Y5Lh8dRi1iQqmva8JGbq+uUHtJgJwBMgakCnLF5QNxPySe4gQm5DJQjb3ljpjSC8AiIEQA/DxLzzfhWCbWHW1MAV5U8BwNkHmhjpHjumNWES+1lKxfwqR3HxuJg4om+UeY/qv4Kozc9cilPK0Kl1urMrInEs96hI/skWC9DipVbXD98t7EsgxtCFHovRhhoD7Hr/YpbnTBsUYce68+9M8YT4YnHqfANc5VZ4HzDkjgOHKW5/0mj6sle8m3j+l8PnL33igZ/QbRwDcDwPHmiH3wgaQzb2qAhlknWN8OPtq8lQFiPS9hMqHVV2Sz3WEsirkfI+/cilVq/FX4cq7bYLzJMYg8csBJiQA3IaqvwWYIUseygcYDTjdT4ABnur66Bcv78VCBPToT6hUaQ9gzhE/ZRCh5E75sK6v+DDMa8wmBHyGGgcgDoS4IgUYRsGpn3zP/lQ9vxmiDzBMDtCyCJMjr4aKGYaDPG0TBN7s+NxYBvGkkZxLP6XedP6RL13ZuaoNaFE1NwpYlSkFSR6cQgH4ZQ5oSPxqA7YQynk34JCAFo/W9+rzUsbnMeR6sPuoFU3lbaVQda09ESqPr5uBYRqw+ChV3tn24RYolUe1ijVkOVebbAzJau554upOeTe/vTv32IVYFEpNwIBXcbloJ4iP7Uc1UJ4sfmSDqpfrzOcVYF8PkUyQXlWVTfB9wU54VBPCk/HaV+4vHdsimh598Ve8MZZBcmcApQwuz35qFAN9bEAmpTj1XGyXdR64sglZ3oS8oVHa8Ia8+TUVTAI1EgDO13XIBkllM93hxjhelbaAzKECzIOCUol5TdgECpAKSgOwUR8/QZjHNc5T5wHytKPPeQwxWBVH1S7n3vW1F3ZiIaOgLz3zKAlgKMBABp87xdfMa3qjqz+VwQCWwYLr8hOpPFXq0+elPukfq5TryPXLIyCviIHVTOAPSbuIycVhH+sggwScTWgAo4CpAMlKlYEwYLDKIDArFROVe3V9uj8tm+D78/l6IMRVkIks/UdRfl4xH/7mi1urehL0Zf65a+4EQBYg9LFPwQUkSn0o4EdtQqmY22t+6OB+OMACmSdXFIui9iHfp67JU9CWMQkagAHMg7JA2yr2S5rLCmBVZwbsNzoBlyGYjU8pH837dF9K3dpiSv+5X3DP0KkwBuDOb/9xK1V3/a7jE5/9Q3nckyNNPK5RVZ4815aXOu1gGU65lwQaUK+I5uOa5GlVUxuwbo/PkRWu7YQIb3z+Y9zLjWJe0QAu/9T9MFAPzh3N3+tkECqPYNQoj1Fyjcyg46DeU7mPAbRsIuZ6zPcr3E5QbQYuJ7VvsH9zLoqdDDh1dIEGoMBZEHpg/IHBlPGA2fc0YBqpl+vIoAxgbRMacEiqFP/lTW9F+UulecBF/SXX3EOE2596Zd4CGtEN+srTj5B4MWDVSlSXqeU+ClgmhQdY7o+5TlahmShWJul7qNQ3FIBlEl2u638MfF++BqjHvqzy/DpNdIMGIAMpj1MEv9kJZKhzoBpsAazuD3pwyEuVChCgrUwqOfyJz0zshIoFXkhtrgfC+oQQT2yunujbnn51Y1VvBPrqjx4mEKpPdUUZkHKjJh6sAG0AZoCmHq1iqm1CWY1MkkBpA5Z7NWC5l4BB9UWVi1efUtUbgQZglqhRsQczBlivgDxo8cbQqGcMsEAsdQAobTrAZjIHbgdlFei6cq5shhp8Pt76zJ82UnXX452P+z/3XNR/5RCYAMQi+DrlzqtzQE0OkO/PA9d5zm7GvLWyJUznljIGWvJlcnUfgmpfCQkEvP2Zu3OL07GxooH8K0zXCQD1gLlz0OdKgZQhg8sB2ejGlKnujwMclHRt0odzOX8YAjgHKadlP7pOHvegBNURpwINwEIm3SmIUsSH1aDBqmC/yyBS3ghgPUEMa0igeHKaNjEGeJUf+/Sml+sy6hfgKH0Y0v0xAPEk4ObfvN5lCacG/cL38g8D/awyENLnMApmsKJEhqIBq6XMgOpNzwIuFsBwFLycux5KvvbcSsXqfQyQ53Ep4yejlQfQjlODBpAH0gYkUFnFMmCtnHJurIbByARZtQEKsNyX7rVfddp+JXXz5FFRa3D5anOUDzxsd0hwdRs3/vbPs6reCvSL332QWoDKORXghOJrDNjDcEoDqybkwfGLrYZKrrcJ7fXI93M7MtmtPghkbicd1yf5dX0oeatynIutQAMNQOTPkYCH3Jk5wA0FJ0XlugkltwG4qdDctix3s+m5PrAguM6VujaUI7fF7bzvd29MqvpUj3c+7nv8YjQ+zBMsXgmBV60AQn6kU/fq/OByfTsjbbCNWNuqc4z1ZAXb8zxRlCdR2ivtluuEf376Q4RGbK1oAHjpW+eoAhV4oDCDaw0QSk06n33QQ7ZqRnuVqG/fROUMx62i5MPB5MvGx+odyL1s2ZotbSS6/vtbTxhVAZV6moDhJsflS72c5+sM7fpFiVWdVFQoeSN9ANJkTSmYqAgk9+U9z/01/uuhD+YaSuxE0QDwytcfIOnk0FCY9uFAonivSL3hlc2oqNvUSY36xT8b+byxBlW3Uvx6FeQLpfVJUbidID8x+Tr3dyR2BhooHS4eaDvW3OhkOVsl6onRfq2XvfZvqR8MGfUkKCDedtar0l6ZFBp9z0JZn4QkDABxRXj3OsIN196qNr6dbIY67v3ylThqFX4Zy/usREI6IQU41Pn+WVvUqeszbWeoesJ0fdy2XiXShsoLrm/s1SKWcvz3fbcSVOzMozm4ExquXWo1EPO98wRgDVDAVKuIVG6ZBHOPrm+gUqdbPb6cYccVqbphVh/79vWvvh3/c+8HiLnsXNEAcM8TV+vHvXzeel+pMvhcB5jfDyrHWVZrE6tWRS5f85+6+Bor10/AamQV6HZyPQz/v3fdQsCeQAPA3V+9FiuoWiW8cVRgVL66Vh7/0v3rrCYNhN+LTQR1Pejrtj6fV02UAlwgK7DqKHXk8//d8X4C9mAdHGYZEkYUg1HATRVr2Cj1jAEem+BeizDK1fUB8g2eV7BX+fDmO/Hd226mvSkaQPqBoAJmOz6mYFLqc9fkqUTVNwVYAQOpVbCpTQQqE+SswuwRrGrZNNNY1rfcRHtTNOAVx6DGAKfzSlmcN9SADRhdZ6hzfNveJuTxzwEWcTTVS2IT0o6DHIlAf/9H3KuiAaQfCMoAe1RMCjDS4PTyBaZVHFwdyqbiSajalIlctfuzHkip2IK0Y+LJ0HXn40037lfRAOyyYlhjUPzgiZUIAeD/fFVN0gy8ph8Pjf7wKhhTMTXG1lL5TTcSgP09dei48zsvxVEwatBGxSpfNh6v4Il6ypMFlYlS6m75cGsVrK8L9npLxbovDjDHQUADwB1PvhyrQWnAWk0CIPXVbDjq/jGlaiVOKj3U98tHcfJH1QetYO3JVAPm2L915NCbWWUlGor/pNZrE1zP0LjGE+oBcxu8CtQHF+u7DcDcxgxgjoMpGgBuf+qVWA1edbx8pNU59n2P3axPQplQzvc+7NQqf0khrVLXV+5Dp4p1HEzRgBpsA5B/hBpXsIOQIVVguZ0Jm+A6artQ/dhCxToOqmgg/UCwpc7KV2VDsoMXiKFA7gKswDZXk/y+o5HnlbwBYI6DKhpQyiWo5TwOuIJN7n4Nf0rBTqnWKnS921nEWBxc0UD6gWD1jVmnTRg/V/DtxFlI3gLWq9AAXE+yWUVbQAaOBBoAbnn2tdj6MDOrMjUJ4s1zG52GvqK+dnYEmOPg1sFRnjA6Bj7mxzOA/UQWW1HHPdhEK46maADpB4IOUgJbg1uvaB5wNVEKHNejN709WMRYHE3RQEthGlgBFzPkKrdSpl8JDT8muAnA3iEDR1Y0kH4gOLXbi8V0bnTej0f3gAMB5jiqogEY3603PK16ZxMhq73yZlJWo8GWOg8JmOPoigbyDwQ9JG8noYZlNjq1KsRqxlbKAQFzLAI0ALz393+JoPS1ZBfgEaspvq7rSfnHggwswDo44kDWJjo2uqjK1icJpNiEUvkxAXMsRtFA+oGg8d3Ax1ZZmQiGvETAHItRNOA2sBbgfM348QkVwBn8kgBzLErRAHDDtbeisQzZ7Ap8hszfO2s1LxEysDBFAxCfFrB+I9Q+jaL0pQLmWJyigfQDQbPZZcDmQ8jCFexjkaAB4LrX/xZjsKD19x1nBTDH4qyDw39o4e8skNGeJcjAghUNAMOb7yRVnxSmZw0wx2IVDeSNcSAgAvHmswmY4/+kE9rZ2xC1UQAAAABJRU5ErkJggg=="></image></g><g class="cls-4"><image width="132" height="132" transform="translate(39.25 -0.84)" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIQAAACECAYAAABRRIOnAAAACXBIWXMAAAsSAAALEgHS3X78AAAIXUlEQVR4Xu2dW5rbKgyART7vf6PnbGGGPiTOgA3oDtjmf2hTB4QkFCGwkwb47/8Ii8WH7fvqGBYRAF5A40khNb2tOgU3+I1fGeH47s/n78IY8dSYh7K7Pzq/zk/Fvi38AJ0kcEJ64dgsTD/dOcTJp1lFFOaJQoUt/DJ7E5qHWqNaJtrfpi5RWgg2TE1Bf1qw4mzhF2uSEE8v5BxFvAKwdJFwUtvADm86q/iuIRAC3kTOLhvRQ5M9AkB3x4oYrmNsLBmOymXpjTgOlj3QgCGO48kprU+g05H3kjFSMdXYf51LAZNOQHwF5ViGqPWgCWjWFRURW/ihCTdlwJDFTJhewjKMFQNsBwDyuBsICjlRRYsqhDawJx0SXZJwq/EWUDHTwXahSP62k4KDyB2S0ykwdcT8hNYwR7jjYw12mHKP8Ladx9GUgw/BQecAlKK3NKUx+0sNQQ4WWHiGIAwyNUP1T4teRJE0YDQ6k/pWGkWADYyKSizyumFjTn9aAcOxaV+6OH2StswlYxI4xh44B65CmCcCteIrfIvjakZqrFwAlCVjFJOq5QbBXiwLY3MZXwAQ2/XONTPEQgRlrov3Mkj3LpI27C2XlGRM7NNyayjzI6SRIWLx5YnQ4S6lxgOKrldCcn+ohLyGiKcXZa72URa641I0bCQcXbc9pI1Mu6erBIM3IGllO+R4oiZDcKnugm6WYUp0cjEFzJ3bqYAsKu9skUZ86UgYlYc26MtE6uBPTDkpi0XqiZoelL03hpONR9g273TSD4B8DnHQqKOCbA66ofaht7ULxs5kv6EuATxqCGNxbux6YvZTMkyNkb4Qjq3eZbAwFAWgSMEcGl9WAoD2bW0KjKZqewlj2WcILZOpg9F8BJFjSy0TcWRwKciuZIh3S3VEeuHppAHEsN+ljLnP07nxDpiPnPkyhCUC00Z8CLCzmBjg9Mzn8bjg9F3b5H2OTX/nEFHgvTsxsfnohL5Cfuu7ZEuoXD+An0PsIM1Qpe8I0XXuUM5ifj9zlDVN//GOmHsvGS0eZDb6Df8A3xWCeDB1IR400absRWX9HKLu2XdyGbVIzDXjVS/MpSaZLWDFZOVtrDLuGi8Xdb4bCn8QTirrnOa8WrCcsXsOwhaRVk1TFbMzgHqGcLajOG56iXsPwVnfaXC2k5UhyJ8eC6WRynj4j55Z2NgdXOl3hsDbjaNawyB8voNwaQbov30PLLQMUL4JIcPc0u4WBF0NzyEIo0lxEJ0GgyYwzsWxkbJGYri24dvOI8zml4Jr26tSHO/8An9GLOHaA8yisisCY97gHc3mCF2WQlOdECNt+03c1hIkofxlCNyP98fYB+jh3QvqX9DdL2tmWWDP2AwhULiGxm/D+GaYxBHpy8Yv5+2BdMwwfD/EbIzn3u0EMA1ID9BfCAyEA749Qoi2btSGZITy+JF9Q4S+q7H/HsQuNwCcxwj5NcNt52iMvTkLCrPw5yD2JemvjtwgKSoDgPpIOAPPeAtvmnNwfvOUIUg/FrLQMY2PCwHBfbi2uA4lmGaYAXzV57nlNpy//X0Ee/8AKs8rYLBxF3XSorL7OUQsxESi0NUzjBSS2dWgt/s08O9laCAMNSzDeILZNBE25xAWMqhgY3kFDDbuxHBcktcQlzG6VdUi/6ZCuenUFaYhzOY79G9uzYS3ygGAu/typaMqzSXD7TPS0UARiH6lwlfsq8l8gW87d6jtujFOoaLP0muiH0KjYiSoImbokiH+VFlj7QLkLiV3a81s/kZoEz1DzMQVdU5AfS6JAEwmEZttZ4qpPFNhXyT+7gpawxwsOLXPL5Ds/XR59gMyPXBwL/poHikCoKibfYZ4OjP4U6HDoYaIubApDmcY1jGaLsq0b39z/t+m2VjBIYK+ZKTtktel2OBuq7pAtdOAGc2nwt92Eto3Zc7gLYIN96fsBPYTU1+E3dB+IwIG0+lB0JeMGo3+orlV6mOyXAl0sBh2BvhLRgkLGRYEwimgFC+5k6HPECmWsjKIgrFm1ttobDw17gOckNcQNZAdyFAQWy2XGwtRI7BZMo54yOwA6gvpLGNyJ8J2ydjxkDkDmF2cgMFkDUIVEFX7TzIVg1wJxMzaj4NkV+PpRVf8HsN3EntlUF+3Mkwnf6oyRBUPmU8A81vxPkHhmoJVVF6Jol/TbV0SMew5eHew33YCCJRZmGAwlz5LRove4y1YnAKCsnMwOcA54CCyHzcKclENIemzmJDCPDosGVKBl84RDaT+GMOmUljRNSMA2AlTMIEKoyFnCNfPb6pDQR/LmsVQVH+Ic6VBVEP05go63oWt+e5dJgJLC3ex0wDyktENtT6lNed8KcP6wRkumH5NVJ1PqE8qB7vSBuT3ork1DLN5PwhTfYkaYjRP8lG7hrDgQc6swkkZg/01Xw0xAm8fYPI5AcMBG7eAW0B42XgfYvGlmAiC4vgwcPR8YmrRH4O59K8heqD3w/3ZkwfiK7clYzEZxHnOMwSxU7pSNffoRHkLHdzKoYX6HELbfzEX8hqCEgiWodsbin03xLeGaMqOcO2IGYnfpOkekFFz3gdnrHjJ6TBVvhlCy8y63ZQX1mDxLETbTk96rhKReFjTg552t1BvO6/Mk22vId92arjLRFh9rCfyx9xF5ezc0HdjMsRsjJzYWpZJdZJkIqFNk2SIKZQYA2p6KLRBO4kZfDC1wGnMT+mwt9SckmE+/VwyBGX8hRHI/MXPH9Q52agNF9eEO7/rpHKRsXYZd8Bw2XepIRbXZS0Zi4wVEIuMFRCLjBUQi4wVEIuM52w7PXZT3FMfbwxsXNtODTf03VoyFhkrIBYZKyAWGSsgFhkrIBYZKyAeSWV7FON6QOaZVGY9BPgHK/KZW2aWrhUAAAAASUVORK5CYII="></image></g><g class="cls-5"><image width="174" height="119" transform="translate(18.25 14.16)" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAK4AAAB3CAYAAACaPXsJAAAACXBIWXMAAAsSAAALEgHS3X78AAAIRElEQVR4Xu2d7YLjKgiGsSf3f6O7t7Dj+dGkYxM/QAEx4fkzu50EX5BQYmwnwJ+/EVKO/wVwLPE9S49nKwYknn4e/9h/nPM6viZl+rITKiyc2fyk2S2yhZ/EQ6qzx/EvgPATAX6qR38lfXxVj3wG1HjfEWIMjgtoC//SlwlW0kNbCXsmBAiocwh6tDAoSRSj/m7wg1AWud8q4rUF4SICxP8aajND8vo3kWw4mWM8i8SNLfyTdyoA6MUu7m0LAUtti2qspBH0Y8O9ZR9UlAiKPHOpjoNj02IApxtRxOCIQ2aSfbcxrnnLVieLotk04QzVWofwE7FmAOCc6JMg6MWBN4jyHm8OAJRahSyThiXDoJPSunSvuOCHkEdYSwByq3ACJRB1kC4GJaVU5ySnndq6jCBsvshp3A2EKy7qbaIHWdnCnMQP+BJfAVArQzuk+cCb7SB+/aCS73GlUBqKNDmcKPmXIj1/sRJMUpyZZeZbBeZBeBAWJWCeNLHaIP2l+nDpzwvjoO0Wzp93c9bLYnJFYIhBPnHGDVPvmWLIKElkhPMLO3ytApOZ2+FxKRMBAjJAR3pHOFYVvvYqOI5tPptsqnekEdmLZEx0rUVqcnk7clDgCqQ4lXXck0Ki4KOSY5+9054uIQxiYDJzJ7gfp0tB63EJh6LZbbZ1nH5PSnQEreGfxLRY4AeutgpTr77WWM1E/+ZoXVDpTjN9HxbyG//IV8GpZlIVNbTFof3cubQu7SGm04xfDZR/qINUaLcKdrRekdL2wrQuO8jDTDPZh54LrlxxJztTpiCMUy+xOpPJLbqroRA/BX57XCXholOm5MM4tHc5zNIiW1wtxzDRNukBhGB0BE0DMCYIga85Qvh3XVpEnJSj8zTWGBU0tHvc1biZOyhOPpPnFFHRAUA/tpXxtu9+7n0k6xWjiXZgrUKNA7WnR+4AG6Jhc/2KOyh/2YsUSxwM0In3xvXWUTvJ0F1xrmhfL3EXk2sGRNwwySWdL5eN6zGvC/8AwiKyMVyUCUFhHBJz8QBUHvmGkpjkdcwyDTuFK9BRhDFRe8FX3LPYkDxGrfQi6e4w2g4wYxiYrCsmRVVpZgDSpf4eN3dazVSQ749ug4epyfX7cYWChmk9RgbPfnbppgx5igox6qCp9FXcjlNESHRgP7v0gdqfE80/hklx2dBrcjtdV/sk564kQs6Puhsac/15VyxKmIkRJwSnCIcC9FZcTZTllZKR+jH+2hdpqEKTLYJEKLav3tOAk2Io+/aeLMKglnt0ghvj4AbDfSN5CwYTvUydbla/aca4KzqzuV9obqHBr+NqIOTkktRi8aqs0lDhssMFSk/EV1yxK1ISnGvrIbGHOo0VdsVlRnz3MW1V3NUQnLiphYJ4YaDblkK8sKenbP1vOacTu+04KAzHl5x4tROQfqJbhakwSCQH13nDEPthAlx0eKvwhYVZMsqs0ISwj70LOHpciBEgdlSkjCNpr0O258xhVkKiyQvsq7ilJvvzOjIalhfde0G6PgfT4khs57206f5ZcdKxEWPGV1LJEcc7SkyYi76K28ugg+TvgLhbQR+M35247sdNKM771zmGo5lKw8jEfEKjagcziAMAw6GyvzvsjJTcAOSvLl2ehd2tVtwDRB3Kg7BthqLWDidyp2CqeUrHsLbpdKhw2noVVwKNEBD38/ZXC0WILnGCqrgHarEkaLot1BhQJodq2yALbyRfSKxFqag1dAHhTCa79ipgXDYB3bXnQP1OMYaNMZz07w7rPc+RR2RuiEZRFb1CdrjfF7sq7sGgtDXpD9djiAE+FR2VIx0x7a+4vWiP5+ixzy0qWQchf6+C48wmAMAWqE36AfY0jcvvjmDj+1BI67hZaudndq4/FgNxuFMNGe9xa+eP2v5QMDR658oFm59PYTxgl/24KDpOEYGqfTTPicM5crRbhdbvYTwf1ED48iEC/mPXGQZOdVpE7Y3ki/Fpo7AJ79naDzbGO+2Ki4LFiF0S96q5SQzD9cuoiQZWARs/Av3LYU9AODTk2HPNuhREd0ZgqriLs0IMLC4tTtRzi8RtFqKLjws6LSwZ8zc0vo6Il3+osm6rsKhsq5Dz4Mhi4mlc2Ki4FjQ4NE5z1qrXXFN8jDP+5AyAT5WjiO6ktRL7wyHrq3U5aY2D+3EdR4xG69LVKlSuBefBoKsqA1t2MEI2ksWSTzhB0Obcl66Ke4Fig3IsRDs7wEYh+e2UeQeyb3eYFDkpVH03yXNViCG2AE/FJSCeVyV/Cq+P7ADTZBGZYxBykWc5bGGe7v+qbK0DiviEt/HlFzHUWwUS7NrYDdahDid1I0rVUYTN0DDZmzOh8K2H9jytfiNKlD/C43vc5Ujnizp31hK9RsM3262Cw8uN5rr/5izHjQKjwUoFEEPP9PfGwFCroCBEYYjHEkIhCU9BZ5oDW0/OnCurTI9yHnmP6yzJtcc9JzLiE9SXQ3oblxJ+cS0D99SXaPe4rd9naNp0nEHoqwrcSal1ifbA7avDhnKPmxlMfHzLVwYW8SAtxyYaFEHTeIgitPOcKM95o1xxF8DjsQT0HlcLwwk0UpTZV1xKGI7fwUgo2qsKDisebx7sVlwuVk6UkZLEicEYeo9rGZ+bIvevuBI8NaF63wEE4qVUcVUGcaQxNI2y67hOP8ampbfYUvisuCD2w/iqgmMGSi6+Wgc4jkU8cZ0lKa8qEMo2O9SGaqZWZwpKqwpELGpyTOGtgrMknrjOknjiOkviiessyRp7Ffxm7RfqikuOG8TT5qqCU8bnCwC8VXAWxRPXWRJPXGdJPHGdJfHEddYjxsKfRHUcy4QA/wM0JGxAD1YgyQAAAABJRU5ErkJggg=="></image></g><path class="cls-6" d="M256.56,127.2v-.08a1,1,0,0,0-1.62-.53,26.11,26.11,0,0,1-18.42,7.14q-11.31,0-18.46-6.46a20.64,20.64,0,0,1-7.14-16q0-12,8.91-18.42t25.5-6.42h9.36a1,1,0,0,0,1-1v-4a13.18,13.18,0,0,0-3.26-9.33q-3.27-3.5-9.92-3.5A14.86,14.86,0,0,0,233,71.6a9,9,0,0,0-3.48,5.34,2.45,2.45,0,0,1-2.37,2H215.11a2.42,2.42,0,0,1-2.4-2.76,19.27,19.27,0,0,1,3.89-8.8,27.37,27.37,0,0,1,11.13-8.43,39.22,39.22,0,0,1,15.72-3q13.17,0,21,6.63t8,18.63V115q0,10,2.75,16a.93.93,0,0,1,.09.4h0a1,1,0,0,1-1,1H262.93A6.48,6.48,0,0,1,256.56,127.2Zm-16.93-7a19.17,19.17,0,0,0,9.4-2.43,16.22,16.22,0,0,0,6.5-6.29,1.05,1.05,0,0,0,.13-.49V98.07a1,1,0,0,0-1-1h-8.11q-9.37,0-14.09,3.26a10.58,10.58,0,0,0-4.71,9.23,9.86,9.86,0,0,0,3.22,7.74A12.49,12.49,0,0,0,239.63,120.2Z"></path><path class="cls-6" d="M290.21,37.77a9,9,0,0,1,2.46-6.45q2.46-2.57,7-2.57t7.08,2.57a8.85,8.85,0,0,1,2.5,6.45,8.65,8.65,0,0,1-2.5,6.35q-2.49,2.54-7.08,2.53t-7-2.53A8.74,8.74,0,0,1,290.21,37.77Zm9.85,94.57h-.76a8,8,0,0,1-8.05-8V59.68a2.41,2.41,0,0,1,2.41-2.41h6.4a8,8,0,0,1,8.05,8v59A8,8,0,0,1,300.06,132.34Z"></path><path class="cls-6" d="M336.35,132.34h-.77a8,8,0,0,1-8-8V28.19A2.41,2.41,0,0,1,330,25.77h6.4a8,8,0,0,1,8,8.05v90.47A8,8,0,0,1,336.35,132.34Z"></path><path class="cls-6" d="M439.79,119.13a3.69,3.69,0,0,1-.82,2.3q-4.46,5.53-13.09,8.76a60.15,60.15,0,0,1-21.19,3.54,39.44,39.44,0,0,1-21.1-5.69,37.88,37.88,0,0,1-14.29-16.1,54.4,54.4,0,0,1-5.13-23.87V75.31q0-21,10.61-33.2T403.3,29.93q15.6,0,25.11,8a31.52,31.52,0,0,1,10.52,17.51,3.59,3.59,0,0,1-3.53,4.38h-1.26a3.57,3.57,0,0,1-3.5-2.7q-2.22-9.15-8.51-14.18-7.17-5.74-18.76-5.74-14.16,0-22.41,10.07t-8.26,28.4V87.56a50,50,0,0,0,3.92,20.38,31.06,31.06,0,0,0,11.24,13.71,29.72,29.72,0,0,0,16.83,4.86q11,0,18.94-3.47a22.56,22.56,0,0,0,6.59-4.22,3.51,3.51,0,0,0,1-2.53V95.22a3.61,3.61,0,0,0-3.61-3.61H407.74A3.61,3.61,0,0,1,404.13,88h0a3.61,3.61,0,0,1,3.61-3.6h28.44a3.6,3.6,0,0,1,3.61,3.6Z"></path><path class="cls-6" d="M472.54,95.46v32.61a4.27,4.27,0,0,1-4.27,4.27h0a4.27,4.27,0,0,1-4.27-4.27V35.59a4.27,4.27,0,0,1,4.27-4.27h30.15q15.75,0,24.87,8t9.12,22.13q0,14.22-8.77,22T498.21,91.2h-21.4A4.27,4.27,0,0,0,472.54,95.46Zm0-15.75A4.27,4.27,0,0,0,476.81,84h21.61q12.42,0,18.94-5.9t6.52-16.47q0-10.5-6.49-16.71T499,38.54H476.81a4.27,4.27,0,0,0-4.27,4.26Z"></path><path class="cls-6" d="M616.43,38.54H589a3.61,3.61,0,0,0-3.61,3.6v86.59a3.61,3.61,0,0,1-3.61,3.61h-1.32a3.61,3.61,0,0,1-3.6-3.61V42.14a3.61,3.61,0,0,0-3.61-3.6H545.94a3.61,3.61,0,0,1-3.61-3.61h0a3.61,3.61,0,0,1,3.61-3.61h70.49A3.61,3.61,0,0,1,620,34.93h0A3.61,3.61,0,0,1,616.43,38.54Z"></path></g></g></svg>
				
                     </div>
                  </div>
                  <div class="col-12 text-center mt20 mt-md50">
						   <div class="f-md-20 f-18 w500 text-center white-clr pre-heading lh150">
                        <span>Grab My 20 Exclusive Bonuses Before the Deal Ends…</span>
						   </div>
                  </div>
                  <div class="col-12 mt-md30 mt20 f-md-45 f-28 w600 text-center white-clr lh140">
                     <span class="blue-gradient under"> World’s First ChatGPT AI</span> Powered Email Marketing App Writes, Design & Send Unlimited Profit Pulling <span class="blue-gradient"> Emails and Messages  </span> with Just One Keyword for Tons of Traffic, Commissions, & Sales on Autopilot.
                  </div>
                  <div class="col-12 mt-md30 mt20  text-center">
                     <div class="f-18 f-md-22 w600 text-center lh150 white-clr">
                        Watch My Quick Review Video
                     </div>
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md30">
               <div class="col-md-10 col-12 mx-auto">
                  <!-- <div class="col-12 responsive-video border-video">
                     <iframe src="https://mailzilo.dotcompal.com/video/embed/fy9w5ktacr" style="width:100%; height:100%" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                  </div>  -->
                   <img src="assets/images/product-box.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>
         </div>
      </div>
      <!-- Header Section End -->

      <!-- header List Section Start -->
      <div class="header-list-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row header-list-block" >
                     <div class="col-12 col-md-6">
                        <div class="f-16 f-md-18 lh140 w400">
                           <ul class="bonus-list pl0 m0">
                              <li><span class="w600">Create 100s of High-Quality Emails and Messages</span> With Just One Keyword.</li>
                              <li><span class="w600">Create Engaging Emails </span> for Marketing, Sales, Promotion and many more...</li>
                              <li> <span class="w600">AI Enabled Smart Tagging </span>for Lead Personalization & Traffic.</li>
                              <li><span class="w600">Send Unlimited Emails </span>to Unlimited Subscribers.</li>
                              <li><span class="w600">Collect Unlimited Leads </span> with Built-In Lead Form.</li>
                              <li><span class="w600">Free SMTP </span> for unrestricted Mailing.</li>
                              <li><span class="w600">Upload Unlimited List </span> with Zero Restrictions.</li>
                           </ul>
                        </div>
                     </div>
                     <div class="col-12 col-md-6">
                        <div class="f-16 f-md-18 lh140 w400">
                           <ul class="bonus-list pl0 m0">
                              <li><span class="w600">Get 4X More ROI </span> and Profits than Ever.</li>
                              <li><span class="w600">Advance Inbuilt AI Text & Inline Editor </span> to Craft Beautiful Email.</li>
                              <li><span class="w600">100% Control on your online business</span> GDPR & Can-Spam Compliant.</li>
                              <li><span class="w600">100+ High Converting </span> Templates for Webforms & Email.</span></li>
                              <li><span class="w600">No Monthly Fees, Pay One Time Only.</span></li>
                              <li><span class="w600">Commercial License Included.</li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- header List Section End -->

      <!-- Step Section Start -->
    
      <!-- Step Section Start -->
      <div class="revolutionary-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-28 f-md-45 w700 lh140 black-clr text-center">
               MailGPT Is Disrupting the Email Industry
               </div>
               <div class="col-12 f-20 f-md-28 w400 lh140 black-clr text-center mt20 mt-md30">
               Join the club of 6,458 expert marketers & business owners who are leveraging the power of this unbeatable, fully MailGPT AI driven technology for…
               </div>
               <div class="col-md-8 col-12 mx-auto f-20 f-md-28 w600 lh140 white-clr blue-bg text-center mt20 mt-md30">
               Saving 100s of Dollars Monthly & Getting Tons of Traffic, Commissions & Sales on Autopilot
               </div>
               <div class="mt20 mt-md50">
                  <img src="assets/images/arrow.png" class="img-fluid mx-auto d-block downarrow">
               </div>
            </div>
            <div class="row mt30">
               <div class="col-12 col-md-6 mt30">
                  <div class="testimonial-box">
                     <div>
                        <img src="assets/images/quotes.png" class="img-fluid mx-auto d-block">
                     </div>
                     <div class="mt20 f-18 f-md-20 w400 lh140 white-clr text-center">
                     As someone who is always on the go, I often struggle to find the time and energy to craft thoughtful and engaging emails. That's why MailGPT has been a game-changer for me. The AI-powered app is incredibly intuitive and has allowed me to streamline my communication like never before. I highly recommend MailGPT.
                     </div>
                     <div class="mt20 mt-md30">
                        <img src="assets/images/testi1.webp" class="img-fluid mx-auto d-block">
                     </div>
                     <div class="mt20 mt-md30 f-18 f-md-24 w400 lh140 white-clr text-center">
                     Robert John  
                     </div>
                     <div class="mt20 mt-md30">
                        <img src="assets/images/four-star.png" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>
            
               <div class="col-12 col-md-6 mt30">
                  <div class="testimonial-box">
                     <div>
                        <img src="assets/images/quotes.png" class="img-fluid mx-auto d-block">
                     </div>
                     <div class="mt20 f-18 f-md-20 w400 lh140 white-clr text-center">
                     Finally, I am getting my emails delivered, clicked, and opened. Yes, MailGPT is one of the BEST tools I’ve used for my business yet! No more dependency on other email software. Stupidly simple to use. MailGPT made me go crazy. Five Stars from my side for this product…
                     </div>
                     <div class="mt20 mt-md30">
                        <img src="assets/images/testi2.webp" class="img-fluid mx-auto d-block">
                     </div>
                     <div class="mt20 mt-md30 f-18 f-md-24 w400 lh140 white-clr text-center">
                     William Smith 
                     </div>
                     <div class="mt20 mt-md30">
                        <img src="assets/images/five-star.png" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>
               
           
               <div class="col-12 col-md-6 mt30">
                  <div class="testimonial-box">
                     <div>
                        <img src="assets/images/quotes.png" class="img-fluid mx-auto d-block">
                     </div>
                     <div class="mt20 f-18 f-md-20 w400 lh140 white-clr text-center">
                     Importing Lists without losing any Leads? DONE! Sending UNLIMITED Emails? DONE! Want to Generate More Leads? DONE! Automating Email Marketing Campaigns? That’s DONE as well! This software really impressed me with its amazing features. A great option for anyone looking to make the most from email marketing...
                     </div>
                     <div class="mt20 mt-md30">
                        <img src="assets/images/testi3.webp" class="img-fluid mx-auto d-block">
                     </div>
                     <div class="mt20 mt-md30 f-18 f-md-24 w400 lh140 white-clr text-center">
                     Rodney Paul 
                     </div>
                     <div class="mt20 mt-md30">
                        <img src="assets/images/five-star.png" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>

               <div class="col-12 col-md-6 mt30">
                  <div class="testimonial-box">
                     <div>
                        <img src="assets/images/quotes.png" class="img-fluid mx-auto d-block">
                     </div>
                     <div class="mt20 f-18 f-md-20 w400 lh140 white-clr text-center">
                     MailGPT will surely take the industry by storm and will help marketers to generate more leads from any Blog, eCommerce, or WordPress site. I like its latest smart tag feature which enables me to build targeted marketing campaigns. Yet another killer product from Team Pranshu & Chandraprakash...
                     </div>
                     <div class="mt20 mt-md30">
                        <img src="assets/images/testi4.webp" class="img-fluid mx-auto d-block">
                     </div>
                     <div class="mt20 mt-md30 f-18 f-md-24 w400 lh140 white-clr text-center">
                     Alex Holland
                     </div>
                     <div class="mt20 mt-md30">
                        <img src="assets/images/five-star.png" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>

               <div class="col-12 col-md-6 mt30">
                  <div class="testimonial-box">
                     <div>
                        <img src="assets/images/quotes.png" class="img-fluid mx-auto d-block">
                     </div>
                     <div class="mt20 f-18 f-md-20 w400 lh140 white-clr text-center">
                     BINGO, I've tested MailGPT for myself, and man, it delivered exactly as promised. I just recently sent mail to my entire list and got 20% more open rates with this super-amazing Email Marketing Technology. Two-Thumbs up for this one… 
                     </div>
                     <div class="mt20 mt-md30">
                        <img src="assets/images/testi5.webp" class="img-fluid mx-auto d-block">
                     </div>
                     <div class="mt20 mt-md30 f-18 f-md-24 w400 lh140 white-clr text-center">
                     Tom Garfield 
                     </div>
                     <div class="mt20 mt-md30">
                        <img src="assets/images/four-star.png" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>

               <div class="col-12 col-md-6 mt30">
                  <div class="testimonial-box">
                     <div>
                        <img src="assets/images/quotes.png" class="img-fluid mx-auto d-block">
                     </div>
                     <div class="mt20 f-18 f-md-20 w400 lh140 white-clr text-center">
                     I always struggle to find the time to write the perfect email. That's where MailGPT comes in - this AI-powered app has completely changed My Way of Writing Mail! It helps me write professional and Attractive emails that truly Interact with my audience. With MailGPT, I never have to worry about the stress of crafting the perfect message .
                     </div>
                     <div class="mt20 mt-md30">
                        <img src="assets/images/testi6.webp" class="img-fluid mx-auto d-block">
                     </div>
                     <div class="mt20 mt-md30 f-18 f-md-24 w400 lh140 white-clr text-center">
                     Andrew Hardy 
                     </div>
                     <div class="mt20 mt-md30">
                        <img src="assets/images/five-star.png" class="img-fluid mx-auto d-block">
                     </div>
                  </div>
               </div>
         </div>
      </div>
   </div>
      <!-- Step Section End -->
      <div class="step-section">
         <div class="container ">
            <div class="row ">
               <div class="col-12 f-28 f-md-45 w600 lh140 black-clr text-center">
               Create & Send Profit-Pulling Emails with <br class="d-none d-md-block"><span class="blue-gradient d-inline-grid"> MailGPT in Just 3 Simple Clicks
                  <img src="assets/images/let-line.webp" class="img-fluid mx-auto"></span>
               </div>
               <div class="col-12 mt30 mt-md80">
                  <div class="row align-items-center">
                     <div class="col-12 col-md-5 relative">
                        <img src="assets/images/step1.webp" class="img-fluid">
                        <div class=" step1 f-28 f-md-65 w600 mt15">
                        Login &amp; Enter a <br class="d-none d-md-block"> Keyword
                        </div>
                        <div class="f-18 f-md-20 w400 lh140 mt15">
                        Login to your MailGPT account & just enter a desired keyword for which you want to send an Email 
                        </div>
                        <img src="assets/images/left-arrow.webp" class="img-fluid d-none d-md-block ms-auto step-arrow1">
                     </div>
                     <div class="col-12 col-md-7 mt20 mt-md0 ">
                        <img src="assets/images/step1.gif" class="img-fluid d-block mx-auto gif-bg">
                     </div>
                  </div>
               </div>
               <div class="col-12 mt-md150">
                  <div class="row align-items-center ">
                     <div class="col-12 col-md-5 order-md-2 relative">
                        <img src="assets/images/step2.webp" class="img-fluid">
                        <div class=" step2 f-28 f-md-65 w600 mt15">
                        Generate
                        </div>
                        <div class="f-18 f-md-20 w400 lh140 mt15">
                        MailGPT AI generates profitable, engaging & high-quality email for you in seconds.
                        </div>
                        <img src="assets/images/right-arrow.webp" class="img-fluid d-none d-md-block step-arrow2">
                     </div>
                     <div class="col-md-7 mt20 mt-md0 order-md-1 ">
                        <img src="assets/images/step2.gif" class="img-fluid d-block mx-auto gif-bg">
                     </div>
                  </div>
               </div>
               <div class="col-12 mt-md150">
                  <div class="row align-items-center">
                     <div class="col-12 col-md-5">
                        <img src="assets/images/step3.webp" class="img-fluid">
                        <div class=" step3 f-28 f-md-65 w600 mt15">
                        Send and Profit
                        </div>
                        <div class="f-18 f-md-20 w400 lh140 mt15">
                        Send unlimited emails directly into inbox for tons of autopilot profits with just push of a button.
                        </div>
                     </div>
                     <div class="col-12 col-md-7 mt20 mt-md0">
                     <video width="100%" height="auto" loop="" autoplay="" muted="muted" class="gif-bg" >
                   <source src="assets/images/step3.mp4" type="video/mp4">
               </video>
                     </div>
                  </div>
               </div>

               <div class="col-12 f-md-24 f-20 w700 lh140 text-center mt20 mt-md40 aos-init aos-animate" data-aos="fade-up">
               <div class="smile-text">It Just Takes Less than 60 Seconds...</div>
            </div>
            <div class="col-12 text-center">
               
               <div class="d-flex gap-2 gap-md-1 justify-content-center flex-wrap mt20 mt-md30">
                  <div class="d-flex align-items-center gap-2 justify-content-center">
                     <img src="assets/images/red-cross.webp" alt="Cross" class="img-fluid d-block">
                     <div class="f-md-28 w700 lh140 f-20 red-clr"> No Email Writing &nbsp; </div>
                  </div>
                  <div class="d-flex align-items-center gap-2 justify-content-center">
                     <img src="assets/images/red-cross.webp" alt="Cross" class="img-fluid d-block">
                     <div class="f-md-28 w700 lh140 f-20 red-clr"> No Designer &nbsp;</div>
                  </div>
                  <div class="d-flex align-items-center gap-2 justify-content-center">
                     <img src="assets/images/red-cross.webp" alt="Cross" class="img-fluid d-block">
                     <div class="f-md-28 w700 lh140 f-20 red-clr">No Freelancer &nbsp;</div>
                  </div>
                  <div class="d-flex align-items-center gap-2 justify-content-center">
                     <img src="assets/images/red-cross.webp" alt="Cross" class="img-fluid d-block">
                     <div class="f-md-28 w700 lh140 f-20 red-clr">No 3rd Party Apps &nbsp;</div>
                  </div>
                  <div class="d-flex align-items-center gap-2 justify-content-center">
                     <img src="assets/images/red-cross.webp" alt="Cross" class="img-fluid d-block">
                     <div class="f-md-28 w700 lh140 f-20 red-clr">No SMTP </div>
                  </div>
               </div>
            </div>
            <div class="col-12 f-18 f-md-24 w400 lh140 black-clr text-center mt20 mt-md30">
                  Plus, with included free commercial license, this is the easiest &amp; fastest way to start 6 figure business and help desperate businesses in no time! 
               </div>
            </div>
         </div>
      </div>

<!-- CTA Btn Start-->
<div class="cta-section-new">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 blue-gradient">"PRANSHUGPT"</span> for an Additional <span class="w700 blue-gradient">$5 Discount</span> on Commercial Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                     <span class="text-center">Grab MailGPT + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                           <br>
                        <span class="f-14 f-md-18 smmltd">Days</span>
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Sec</span>
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Btn End -->

      <section class="future-section">
         <div class="container">
            <div class="row">
                  <div class="col-12 f-28 f-md-45 w700 lh130 black-clr text-center thunder">
                     MailGPT Is FUTURE Of Email Marketing
                     <img src="assets/images/double-blue-line.png" alt="line" class="img-fluid d-block mx-auto">
                  </div>
                  <div class="col-12 f-18 f-md-28 w400 lh140 black-clr text-center mt20 mt-md30">
                  Here you Can See What you Can Do With This Fully MailGPT Cutting <br class="d-none d-md-block"> Edge Software.
                  </div>
                  <div class="col-md-12 mt-md80 mt20">
                  <img src="assets/images/future-img.png" alt="" class="img-fluid d-block mx-auto">
                  </div>
                  <div class="col-md-12 mt-md80 mt20">
                     <img src="assets/images/yes-you-can.png" alt="" class="img-fluid d-block mx-auto">
                  </div>
                  <div class="f-18 f-md-28 w400 lh150 black-clr text-center mt20 mt-md60">
                 <span class="blue-gradient1 under w600"> The Best Thing is You Just Need to Enter a Keyword </span> And MailGPT Will Create, <br class="d-none d-md-block">Design &amp; Send Emails For You In Just 60 Seconds Flat.
               </div>
            </div>
         </div>
      </section>



      <div class="proof-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 ">
                  <div class="f-28 f-md-45 w700 lh130 black-clr text-center">
                  Checkout How We’ve Got 20,000+ Clicks & Made Over $25,233 Income by Using the Power of MailGPT
                  </div>
                 
               </div>
               <div class="col-12 mx-auto mt20 mt-md50">
                  <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/proof.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>
            <div class="row mt-md70 mt30 align-items-center">
               <div class="col-md-6 f-md-32 f-22 w600 text-center text-md-start">
                  Got 20K+ Visitors in Just 15 Days on My Affiliate Offers Using the Power of Emails  
               </div>
               <div class="col-md-6 mt-md0 mt20">
                  <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/proof1.webp" alt="" class="img-fluid d-block mx-auto">
               </div>
            </div>
            <div class="row mt-md70 mt30 align-items-center">
               <div class="col-md-6 f-md-32 f-22 w600 order-md-2 text-center text-md-start">
                  And Let’s Check Out the Crazy Open And Click Rates 
                  We’ve Got for A Simple Email I Sent Using MailGPT.
               </div>
               <div class="col-md-6 mt-md0 mt20 order-md-1">
                  <img src="https://cdn.oppyo.com/launches/mailzilo/advanced/proof2.webp" alt="" class="img-fluid d-block mx-auto">
               </div>
            </div>
         </div>
      </div>
      

      <!-- Proudly Section Start -->
      <div class="next-gen-sec" id="product">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <!-- <div class="pre-heading f-md-32 f-24 w600 text-center white-clr lh140">
                     Proudly Presenting... 
                  </div> -->
                  <div class="proudly-sec f-26 w700 text-center white-clr">
                     Proudly <br> Presenting...
                  </div>
               </div>
               <div class="col-12 mt-md20 mt20  mt-md50 text-center">
                  <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 620.04 134.95" style="max-height:50px;"><defs><style>.cls-1{fill:none;}.cls-2{clip-path:url(#clip-path);}.cls-3{clip-path:url(#clip-path-2);}.cls-4{clip-path:url(#clip-path-3);}.cls-5{clip-path:url(#clip-path-4);}.cls-6{fill:#fff;}</style><clipPath id="clip-path"><path class="cls-1" d="M40.14,36.57l13,12.24v77.25a6.51,6.51,0,0,1-6.51,6.52h0a6.52,6.52,0,0,1-6.52-6.52Z"></path></clipPath><clipPath id="clip-path-2"><path class="cls-1" d="M170.36,36.57l-13,12.24v77.25a6.52,6.52,0,0,0,6.52,6.52h0a6.51,6.51,0,0,0,6.51-6.52Z"></path></clipPath><clipPath id="clip-path-3"><path class="cls-1" d="M169,15,108,72.83a4,4,0,0,1-5.49,0L41.59,15A6.51,6.51,0,0,1,46.07,3.73h0a6.56,6.56,0,0,1,4.49,1.78l50.57,48a6,6,0,0,0,8.33,0L160,5.52a6.5,6.5,0,0,1,4.48-1.79h0A6.51,6.51,0,0,1,169,15Z"></path></clipPath><clipPath id="clip-path-4"><path class="cls-1" d="M101.27,81.56,35,19A9.6,9.6,0,0,0,18.8,26v99.87a6.52,6.52,0,0,0,6.51,6.52h0a6.52,6.52,0,0,0,6.52-6.52V34L95.92,94.44l5.35,5a6.06,6.06,0,0,0,8.31,0l5.34-5L179,34v91.91a6.52,6.52,0,0,0,6.52,6.52h0a6.52,6.52,0,0,0,6.51-6.52V26a9.6,9.6,0,0,0-16.19-7L109.58,81.56A6.06,6.06,0,0,1,101.27,81.56Z"></path></clipPath></defs><title>MailGPT White Logo</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><g class="cls-2"><image width="90" height="99" transform="translate(-0.75 36.16)" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFoAAABjCAYAAAAb3RdnAAAACXBIWXMAAAsSAAALEgHS3X78AAAS0klEQVR4Xu2cTahtyVXHf2ufeyOKiNLddhJbE6XVNtF8dqskDsSRM0eRiIKoiIgfAyeKIgTEiYIDJQMRBBUxmpEzBw4caBvtl26bjtEmrbZGOunOaxQRUfuevRysj1pVZ597zv0495wHFrx7a9de9bH/9a//WlV7vyuqyimnp773LxTgmT/6oOyyPeUkpwz0kw5yJOHBBXzaZXDUJOUfoMCTH/pzfWqYgAchnSyjn/ywg1mH1+UVEXlgGH6yjNaArzC6z0syfKx7iukkGf3+73tag76yldE1f/rsPttlcJQk/kNBC9IdihXowu5TBfzkGP3+7396izY/2Aw/KY1+3w88vUxfyR+oFExH/c7y09PvkwIaFoCseXFkpTjLajfyV+RkwsGTkY73/uBfmhyHTOgumdDML2r3+Fhq83QsOTkdoH/oE4oWgBXQfXS5qI2yCHB/fRz9Pgmg3/Mjn9AGsDrIztR5B3MzPwA+2i0Afu/j33FnYJ+ERuskMIFOltdJYOU3RdLpLepy5lvBVv0eHOZd6vfRGf3uH/srDQaj2tg4MhyQmevJyWgzXt+BnBwV6Hf9+F9v6jIgRT5MMtq1zF75KoBrIfORAD+qdOgETAKTuGzgEiEpJ0x+7RISNiEpl8bVJRzMNEI4yIlymPj7aIz+lp98xmgaTE0Gqz17ve7YbmXMNDtvZyu7q52nnRHKLbP7aEB/8089owjIPIANdHICPeCArLWBotcHfC/9RhFuDvhRgH7nz9yzcK6CPI/gag9i2NHqgNtH3bDdBfgS2KPNeH1Dhh9Fo3XlmrxqWqyTtPLQ10lMg0N7U6/9OjR6anqdoV7ci07zXuStYCMU3HZ9Q/2+c0Z/088+q53uqjJdaEMkmLqN4dVuDAdjhUDR4F3sbheHZPedA/3Ezz/bdoFzD7jFyQXkuisswLU6druGg7WO3Wv1LgU8r7W3Ge2uCfidAv2Nv/hcp80ZQSgN5LwewFN6vV4CfG2ZbLfY763ftLq7JyXyuwG/M6C/4SN/o8wFyABjbvmR3R07twE+hoMUhkd/tY0CdrYVaQAvyrbajNeXAH5nztCcHebwwulNAisv9zOO2MSkc4yzjqnUrY6vOsy64REBb5faxoRfizvXMsguL1m21Wa8FkFVWTo/uRNGP/7Lz3encyETlcVLDE8nmSwu113eO9rmMH3bXqWqXbthrRd183e7cV05uTugRazzNQPAA2BbpYWi7aXORnTi7WfeijvNp/UDOwCPdvK3tjpLNht5A/zgQH/dr76gAU7d0cm6d2ZL4FXAR7C3Ak5rM4Gbe1soE0ntp9XJtiJp/V3qLdkMeeEOPjfQM/Gdm+mmOEsB++0gqu8uZFbTVQdSFUTFMFEFkYwuVIqd5+NFgQiEuOrK+q8TZO15fdRMvczXuts4mB2i4mOxKsnuaGPIm80BGf32X/9brbFyi4+haXVj+k45mWv+khUx1ifsiqRE/bzn9aj9+kVtJ+zzd7uxVb/1gEC/7aOf1g3HVxgcoVd3f9DvXstpIF9UkHZL0KKcLNhD6Qc6u65etBdJ88ci2Pf+8INyMOnQFaDiS0iRWVDx3woiis4WmsnaHEYu3wTcbctSFwU9x8tHe5oE6Q45EbefrdNQBre0CZCGp6iAaIdvTmBoi1q7Zm9F9z5mUcdBgH7st/5OZYpRagN8htAuA9vYyUSSSBB0FROyAF4CruhsNViBzKSO7wIcceAcTPAJb4NAfVLxJWET209HTkJcLAAe6SBA68ofLNCMhyx5UWO4BfnFSa7LxCjGVmfc+FMm60MVaztWxg7AZXZm5jitD+Y2bo9GSYcpzTmmwyyAS7v0sQuf/IMPxMzcPtBf9TsvKitB12rsnEyHdWoslMnYyiy2m1MHF4xt6xIVVMD1EjmJSV2HDAhKlQRBApgYiwMTkqKTTQIxWVFffMK81SWGJ9h2WS4s3TrQMamcCVwUsBV0FmTyByx5UUFnRSZ7INPtHswR8BauaWPZWm1L7mU1LNyUoIHdeBcraWcsPjWSFsOEpZUNTiG1+ZO/39gMtwz0mz/2GZXzAAA4MxymCxtGAMmMg+xsnNVZDslaMLsZGmOqfhfbAE8i75QbVsEGyLogJ2AvjOeezZtgQ0Bti6mBvZRuLbx79OMvlXDOGBQPEBuUOLtYDMEcuMjLG9rbdXnvYyMcdDuaXe5GO7uFOmU8GXLS22W90n6uKNr1s7/XsxlukdF9OEfu+sDKZFbmM+t/uiAfUEWabdFbPZdkpijOzvinpu+DnDR50LwVUiCzaTaQi79JQK/fVtkttJKhrCRvH5EEWIHnfncTZLgloB/5439QJvHtsw8IX7IOspat83zm+QBFsHg2mSVe1pa96XYD07CIGwyANzlBscn08eAT2U1K5n3yYwy+OnQlyBoi9mbUfmiAb0m3AnSGcxPOzj6vE0xvaHp7AD2zwZp+g0yaDxB6KaNjA1gbE3UiwdCZRf3O0OxSJ7mHw1RFVwyrwn4qII72c7/97YtshlsA+qE/+SdjcwwM2kPSHmw+p7ExdVCZ3XnuIye4DG1IRSz1ueW7DU/G1jYGwm84kD3gCyCHnSiytj7apJUxXJJuDLSuxHtrg0Gl7QIDsAoeYstwLR7DbgLetVlAlIsiTwmSPWzIzYacjEs+cFGTCWurBy5CvE6/EZjczpFVTPqe/81vuxTqGwH9FX/6stJpMjhybYanNvAl/WZtTx4PE/rdb0wc5NkmpANmTZOrmNBhchJw3ImJ+451AGuTk/kZC/Fq/5HP9mk7XHana4d3X/5n/6zRYXhdWYck2KBsMC0fy1ZioGr22w7z26usVmcj5JvpXiJEWbXtj2WH9lxGFsPHHOdCv6X+Cx/91kvZDDdgdDjAcEaxXFVpEcWMU6awrDIumS45+DzM0V5OtNRp/Uq+kO12jPRMzDGI2eQJ4lqtOFad95HOFc2Fqd4NWkCY4IXf2A0yXBPoL3v6s2pLS/O8Ih5MZrVJcJBte10eXCDeuIjC/CacUdaeVMAc0BHw6ng7aVljS18gDog2AC8O0ybdnSbAhY9LvJ0gixZnGW142/umawFtbHZmxKBVzFGJDzRYtNay9XaNrIc645mHKKwlJaA6o/nMnVScspXVEk4ytZbmJA0o2kQHQTwfEYpmfO/tVL/i4xEkz24+9WtPyRaINtKVgf7Se/+q5rWtQ7QM5Mwe2kAjI49YpiLiMlMAEpOA6pTA69IzMpyRTjC5PEUIZ6x3cBRfGRQgaYDXyZ0DOB+7YLG699WFmZXpuonNZenKQHdbVBXswN07F9LZGXu0sK8AHhoZIFLaU1CbAVPJCnLJz6710xpfGW21WOPSzra9/aXln6siHaagZ8CFVesOv9TqCfDpX3nf3myGKwL9Jc+/4uGcswKc1T6QWRdP4Wyn58Ct+okaJQBVmATxnWSycJSAEfDiMBug5Na5O9RXD+/m7Q4zn9P7TTkRB/yKae/w7os/9Tm1GScf0jonw7Pw2pkvodxSCNd/2+H5MTSLeyV0rLbdOMYwr5Z3Y7ykDVUvazay1uIU4cVfeu+V2AxXYbSAvXnwJVp/VudWl5mzLZ0kiklIsAg0AXIW0lhb20TEo4JBAnzZp34HMMH6tfc5VT8AcW6RoV3dCVb9xlahYP2++JH3XBlk2BPoL/r7z6u9n8Pes9EeRucKOSYPnsv8hpPsJSAncJvDVLWyM/EQjg7wcKhpWwEXocpOOslRrsYdZbGRmXSS1017AR3b6HyQCninjaPdwKKinXEKB/5MiJ1pR/y9xWHGcWs96wiHtxihaB+hIOKhYPErCCqNKsYGH5gX6SS89AvvuhabYQ+g3/TSqxoRAJAPqkZjYyvLgC/KSQAuCuth8lQg2sVsk9XiE3eh/ZHnIAGoabnDB8Ach/8+OXpu400mz9oYXfMxlii/QdoJtHbe10EJNgd4vqxZOgwagOxYtKJ4/pggSE1W9RDNy1ThXEpbmru5WPbjJmpJvwWxEC7iZRoRBGnPlLtf+Mefuz6bYQfQ5y+/pogzxssUIQ66bY6b86j6vS0cUyUZY04SP/BrUtADjnU0yIVNmKBnwrRWNmP5kKllwO1FBF2drr/K5okbp63h3dm/vKYGDo0tYeplGRbhdrG81Ma5/GK01Mv27Xox3NPW9vjitwsfs65uhGeN7Qv9e9iXL45rGOjtv/zT77wRm+ESRm+EQ5VNAELPWiDetIA/x0oMcH9dBepEceZjUYl4qR1GBThFgmIZF3YDuTkKprJWNs5OAmCk/74kRuTvOudzYboIGdH80Ofln3jHjUGGLUCvXvmCYqu7geAPuQl4XJPhVMCK5/SsAB511SZqQ7+Fzc/A8nTQ7Jb0W9YeSeBOU2lyQvS5XU5QmM8o4R8+2NtJ2zVa7J+uQeLB6j167QbcSfpDKLkJAK++p8NEGOJlttg1wO0j97DTDOmowEZ783bAdfLXaSif/dHbYTMsAD197r4aK61zVj7W8aUkNDYH4OIPik/DSogvjYi52sNhssYdFW35q133uzho0UsZmwpzxNuzdjJop3U0wKEdHEW7Z3KjzclS2mS0kxLIJSVg/z1htgfbABxBpV5HXfXPDsTLG0Cp3wvMVaEASznyxAsaO+23EJ835DGp+hguyFUQR6LdufIcE2b2Jj3wyg8/cWtshgHo6dX7hkSiZXkFUHcy+IO7Fscg42cvJ5Uxdm3V9gM8GJ6OTkmGt3wDEhFSTuIQ69zAtoGoTXoQJQ//477/voVwbkwZ3smr98NtEIzeyFuFdrF2eINZNCZCgIE5SWhtaWnH7QVcv6Os5TNUyzOQsay0OdSZLjQ1GKXLL4VyKHz+w19/q2yGymjfmKCERLdZjjyk0wFN/aZ8n6GQGxCz0jxUqoAkYwHEGTkeWCW7vT1R8xXESip6qzDuMEXFIol6L3aSsz9bTJ4v3Vc/9PitgwwOtHzhdYdRvL/LAY+HTPRX5JtjYllGlS0OMx2PT0oFfFNOMJDSSdbwjd1yEhudWdBzse+2xSc7HsOJcqhkjB7nUHcALjEmY4GgdsIHJidFv1vzkiUC3ScG0aUBrtl+A1whIgmvo2oT0QAP8PHKkhOEt20OU4tue6zu/b72PYdhM8CZ3A82lxSzjCTLpCunmxytgG+TEyCiBUXdIQn5wXdpSxRcn0hJ6c6wPZ9OconhuN0yw/MEz8fY2HSYJKpKB3aFXUsmwL7ULvApBeXcIYrTyUVk4HWaI/X70VbYE+x2Owc826mObrAZ8+kA3QG//t1fe1CkJwB9+KHWSe1OSkbI1bho59dmUwomLxPJ8woVyL+ptLINR/x9pXzLPrlNhGzezrwS21BMUP/cRNpMQv939CwutyG1vIrX9zEcOmV4lwU72e0XusDwvim3KYWV3XE/bgfj5rjej+GsY7sdNrqxiiJvLC52fu/fvuttB0d6A+i8cR3At9mljRdeIiewEBtTrin14rrGw4rLgduMclKPVmfl37/z8CDDZcekLidy//XmrRS6PAKigc9mdBJgS2DoYZzvMEeHac3Zsm/hnzighqq6oQ3OHdzkwebanawI8X9eNr6E8vbyldUdpa2M7oyuw+56f6gzshsKw4H80DHLo+7+ciIYsze+24iYWuE/PvDVd4b0zneGwBXYDRl7e9E+7AbahgfIv68Rbcdyj82POnjBcC0rQ8kdplVRui9Lxex7khw+7cXoMV2J4WPzC9cjw6te5/1Bh61cM592g37L0v9tVOU/n3zsztgMezJ6TFdiuBSwxuR1DNNBv2cQo59dT+JHmurshTz79j5Svwu74+2OqriUKLla7jBdi9E1bWV3d62Zl9FuyI/shmBxMNNvh16HubIZoahfxDXG8P9691vvHOkbAx3pSnKyUc5GnUXA46XsLrC9rAshFRD473e85c5BhmtKx1K6kpw4Eh3gQx0j4+Aw8wYGcvnAHMFDOEnArUvpAT9SujVGj2mnpGibjw2bBfuR4cnksgHpwj9lw1n+zxNvPgqb4SAvbSxtPT+JaxE0/0VZvd/bm//ywjw/gfiTmUTezznwcxL1M5DDPel+6WCMrmknu8OujmUbwxfYDfSvpSq73fR/H3/0aGyGW9Toy9JW/YZOm+PNeZJ3wWabfhOhoNerH7YfU5sj3Qmjx7QzQrlMv8frkeFK9zJYFN54+1celc1wJKBhD7CdidcFPM5LLr7m+CDDEYGOdKl+A7lR6cq25KFtUgBmWD/2yP8DXdNuhzkAvgXsAFoR5rc+fBIgw9GDnpb04YckQ8II1yKFd5SCabUp+XxtdmLpZBhd05X1e8FmfsvpsBlOiNE1bWV3QifEi1ZdsjkpiC2dJKPHtPHtiQ6/B4bPj54Wm+EBATrSIuAD2HqCIMOJSse21J2fQJOJdJYniTHwgDG6piWHqY88dLJI38lZxyHS4vnJCaf/A7yQ0Ala9tJfAAAAAElFTkSuQmCC"></image></g><g class="cls-3"><image width="90" height="99" transform="translate(121.25 36.16)" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFoAAABjCAYAAAAb3RdnAAAACXBIWXMAAAsSAAALEgHS3X78AAAO2klEQVR4XtWbzet1VRXHv2uf+1MQokDNxExD1MpAyMd8Cfoznhw1bRA0cBRUA2kSURGRVFhkb1aOmjVrEDyE+LxqKiYSRophDaJBFHl3g73X2mutvc85+/7u2/ktuM+5Z5919stnf/d3n3t/96EYI85yPHj+QgSA55/9FM3lHjPorIJ+8PyFqucXFwx7NZewtGAFnzV5nBlF9wJeqqrPhKJbNlHFIvGWWLSie1XsIV/89fJUvUhFnxbwkmNRit4l4KWpOswlHCrYh3cBGQSce+zCbFWHjKNbxy5V3JVzpDiadXQDBvoAqpyozi/9chkWcnBFdwPuxePyopxTfx0HiIMquut5GOgD1AuYgEu/eLSnxr3GQRS9bxWXcnUhv51t80CxV0XvG3BSca1gez3F5Z8fV9V7Ad0NGOiD7AFLWRuy3gxLOeHyzx7paW0vsXPr6PbhnmhgmVLxGOCuydxz7EzR3SruHXTTJvIFKm/r61xOJocn4cpPjqPqrRXdDRjogzwKWF0cAyzX2pCPGadW9EaAgfmBTgEee5qoVKzecJ5WPxEigKs/fniuNzuPUyl6Ix+eG5IHbMpo3ibk2gRgAJFyXXP92VNspOh9qrgLsDufBKzLgs0BAVd/eFhVdyl6I8A93fcqNuc15C7AOocAEDVUncqPEZOKPhpgfb2h0FEVM2Cdm+tIZWTKrv3goZ5e7ySaoDcCDMxD9gBNmQLcUqfP1XnuvpaKQdmffVk+vvD9w8CurGMjyD1dVDmjgPPpqQGjsdkRENEqA8C5BwxR9E4Bu+uTgHNR15OEem+P2S4CZgDnf8iWvfjkJ+dGtHWsjg7Y5Lh8dRi1iQqmva8JGbq+uUHtJgJwBMgakCnLF5QNxPySe4gQm5DJQjb3ljpjSC8AiIEQA/DxLzzfhWCbWHW1MAV5U8BwNkHmhjpHjumNWES+1lKxfwqR3HxuJg4om+UeY/qv4Kozc9cilPK0Kl1urMrInEs96hI/skWC9DipVbXD98t7EsgxtCFHovRhhoD7Hr/YpbnTBsUYce68+9M8YT4YnHqfANc5VZ4HzDkjgOHKW5/0mj6sle8m3j+l8PnL33igZ/QbRwDcDwPHmiH3wgaQzb2qAhlknWN8OPtq8lQFiPS9hMqHVV2Sz3WEsirkfI+/cilVq/FX4cq7bYLzJMYg8csBJiQA3IaqvwWYIUseygcYDTjdT4ABnur66Bcv78VCBPToT6hUaQ9gzhE/ZRCh5E75sK6v+DDMa8wmBHyGGgcgDoS4IgUYRsGpn3zP/lQ9vxmiDzBMDtCyCJMjr4aKGYaDPG0TBN7s+NxYBvGkkZxLP6XedP6RL13ZuaoNaFE1NwpYlSkFSR6cQgH4ZQ5oSPxqA7YQynk34JCAFo/W9+rzUsbnMeR6sPuoFU3lbaVQda09ESqPr5uBYRqw+ChV3tn24RYolUe1ijVkOVebbAzJau554upOeTe/vTv32IVYFEpNwIBXcbloJ4iP7Uc1UJ4sfmSDqpfrzOcVYF8PkUyQXlWVTfB9wU54VBPCk/HaV+4vHdsimh598Ve8MZZBcmcApQwuz35qFAN9bEAmpTj1XGyXdR64sglZ3oS8oVHa8Ia8+TUVTAI1EgDO13XIBkllM93hxjhelbaAzKECzIOCUol5TdgECpAKSgOwUR8/QZjHNc5T5wHytKPPeQwxWBVH1S7n3vW1F3ZiIaOgLz3zKAlgKMBABp87xdfMa3qjqz+VwQCWwYLr8hOpPFXq0+elPukfq5TryPXLIyCviIHVTOAPSbuIycVhH+sggwScTWgAo4CpAMlKlYEwYLDKIDArFROVe3V9uj8tm+D78/l6IMRVkIks/UdRfl4xH/7mi1urehL0Zf65a+4EQBYg9LFPwQUkSn0o4EdtQqmY22t+6OB+OMACmSdXFIui9iHfp67JU9CWMQkagAHMg7JA2yr2S5rLCmBVZwbsNzoBlyGYjU8pH837dF9K3dpiSv+5X3DP0KkwBuDOb/9xK1V3/a7jE5/9Q3nckyNNPK5RVZ4815aXOu1gGU65lwQaUK+I5uOa5GlVUxuwbo/PkRWu7YQIb3z+Y9zLjWJe0QAu/9T9MFAPzh3N3+tkECqPYNQoj1Fyjcyg46DeU7mPAbRsIuZ6zPcr3E5QbQYuJ7VvsH9zLoqdDDh1dIEGoMBZEHpg/IHBlPGA2fc0YBqpl+vIoAxgbRMacEiqFP/lTW9F+UulecBF/SXX3EOE2596Zd4CGtEN+srTj5B4MWDVSlSXqeU+ClgmhQdY7o+5TlahmShWJul7qNQ3FIBlEl2u638MfF++BqjHvqzy/DpNdIMGIAMpj1MEv9kJZKhzoBpsAazuD3pwyEuVChCgrUwqOfyJz0zshIoFXkhtrgfC+oQQT2yunujbnn51Y1VvBPrqjx4mEKpPdUUZkHKjJh6sAG0AZoCmHq1iqm1CWY1MkkBpA5Z7NWC5l4BB9UWVi1efUtUbgQZglqhRsQczBlivgDxo8cbQqGcMsEAsdQAobTrAZjIHbgdlFei6cq5shhp8Pt76zJ82UnXX452P+z/3XNR/5RCYAMQi+DrlzqtzQE0OkO/PA9d5zm7GvLWyJUznljIGWvJlcnUfgmpfCQkEvP2Zu3OL07GxooH8K0zXCQD1gLlz0OdKgZQhg8sB2ejGlKnujwMclHRt0odzOX8YAjgHKadlP7pOHvegBNURpwINwEIm3SmIUsSH1aDBqmC/yyBS3ghgPUEMa0igeHKaNjEGeJUf+/Sml+sy6hfgKH0Y0v0xAPEk4ObfvN5lCacG/cL38g8D/awyENLnMApmsKJEhqIBq6XMgOpNzwIuFsBwFLycux5KvvbcSsXqfQyQ53Ep4yejlQfQjlODBpAH0gYkUFnFMmCtnHJurIbByARZtQEKsNyX7rVfddp+JXXz5FFRa3D5anOUDzxsd0hwdRs3/vbPs6reCvSL332QWoDKORXghOJrDNjDcEoDqybkwfGLrYZKrrcJ7fXI93M7MtmtPghkbicd1yf5dX0oeatynIutQAMNQOTPkYCH3Jk5wA0FJ0XlugkltwG4qdDctix3s+m5PrAguM6VujaUI7fF7bzvd29MqvpUj3c+7nv8YjQ+zBMsXgmBV60AQn6kU/fq/OByfTsjbbCNWNuqc4z1ZAXb8zxRlCdR2ivtluuEf376Q4RGbK1oAHjpW+eoAhV4oDCDaw0QSk06n33QQ7ZqRnuVqG/fROUMx62i5MPB5MvGx+odyL1s2ZotbSS6/vtbTxhVAZV6moDhJsflS72c5+sM7fpFiVWdVFQoeSN9ANJkTSmYqAgk9+U9z/01/uuhD+YaSuxE0QDwytcfIOnk0FCY9uFAonivSL3hlc2oqNvUSY36xT8b+byxBlW3Uvx6FeQLpfVJUbidID8x+Tr3dyR2BhooHS4eaDvW3OhkOVsl6onRfq2XvfZvqR8MGfUkKCDedtar0l6ZFBp9z0JZn4QkDABxRXj3OsIN196qNr6dbIY67v3ylThqFX4Zy/usREI6IQU41Pn+WVvUqeszbWeoesJ0fdy2XiXShsoLrm/s1SKWcvz3fbcSVOzMozm4ExquXWo1EPO98wRgDVDAVKuIVG6ZBHOPrm+gUqdbPb6cYccVqbphVh/79vWvvh3/c+8HiLnsXNEAcM8TV+vHvXzeel+pMvhcB5jfDyrHWVZrE6tWRS5f85+6+Bor10/AamQV6HZyPQz/v3fdQsCeQAPA3V+9FiuoWiW8cVRgVL66Vh7/0v3rrCYNhN+LTQR1Pejrtj6fV02UAlwgK7DqKHXk8//d8X4C9mAdHGYZEkYUg1HATRVr2Cj1jAEem+BeizDK1fUB8g2eV7BX+fDmO/Hd226mvSkaQPqBoAJmOz6mYFLqc9fkqUTVNwVYAQOpVbCpTQQqE+SswuwRrGrZNNNY1rfcRHtTNOAVx6DGAKfzSlmcN9SADRhdZ6hzfNveJuTxzwEWcTTVS2IT0o6DHIlAf/9H3KuiAaQfCMoAe1RMCjDS4PTyBaZVHFwdyqbiSajalIlctfuzHkip2IK0Y+LJ0HXn40037lfRAOyyYlhjUPzgiZUIAeD/fFVN0gy8ph8Pjf7wKhhTMTXG1lL5TTcSgP09dei48zsvxVEwatBGxSpfNh6v4Il6ypMFlYlS6m75cGsVrK8L9npLxbovDjDHQUADwB1PvhyrQWnAWk0CIPXVbDjq/jGlaiVOKj3U98tHcfJH1QetYO3JVAPm2L915NCbWWUlGor/pNZrE1zP0LjGE+oBcxu8CtQHF+u7DcDcxgxgjoMpGgBuf+qVWA1edbx8pNU59n2P3axPQplQzvc+7NQqf0khrVLXV+5Dp4p1HEzRgBpsA5B/hBpXsIOQIVVguZ0Jm+A6artQ/dhCxToOqmgg/UCwpc7KV2VDsoMXiKFA7gKswDZXk/y+o5HnlbwBYI6DKhpQyiWo5TwOuIJN7n4Nf0rBTqnWKnS921nEWBxc0UD6gWD1jVmnTRg/V/DtxFlI3gLWq9AAXE+yWUVbQAaOBBoAbnn2tdj6MDOrMjUJ4s1zG52GvqK+dnYEmOPg1sFRnjA6Bj7mxzOA/UQWW1HHPdhEK46maADpB4IOUgJbg1uvaB5wNVEKHNejN709WMRYHE3RQEthGlgBFzPkKrdSpl8JDT8muAnA3iEDR1Y0kH4gOLXbi8V0bnTej0f3gAMB5jiqogEY3603PK16ZxMhq73yZlJWo8GWOg8JmOPoigbyDwQ9JG8noYZlNjq1KsRqxlbKAQFzLAI0ALz393+JoPS1ZBfgEaspvq7rSfnHggwswDo44kDWJjo2uqjK1icJpNiEUvkxAXMsRtFA+oGg8d3Ax1ZZmQiGvETAHItRNOA2sBbgfM348QkVwBn8kgBzLErRAHDDtbeisQzZ7Ap8hszfO2s1LxEysDBFAxCfFrB+I9Q+jaL0pQLmWJyigfQDQbPZZcDmQ8jCFexjkaAB4LrX/xZjsKD19x1nBTDH4qyDw39o4e8skNGeJcjAghUNAMOb7yRVnxSmZw0wx2IVDeSNcSAgAvHmswmY4/+kE9rZ2xC1UQAAAABJRU5ErkJggg=="></image></g><g class="cls-4"><image width="132" height="132" transform="translate(39.25 -0.84)" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIQAAACECAYAAABRRIOnAAAACXBIWXMAAAsSAAALEgHS3X78AAAIXUlEQVR4Xu2dW5rbKgyART7vf6PnbGGGPiTOgA3oDtjmf2hTB4QkFCGwkwb47/8Ii8WH7fvqGBYRAF5A40khNb2tOgU3+I1fGeH47s/n78IY8dSYh7K7Pzq/zk/Fvi38AJ0kcEJ64dgsTD/dOcTJp1lFFOaJQoUt/DJ7E5qHWqNaJtrfpi5RWgg2TE1Bf1qw4mzhF2uSEE8v5BxFvAKwdJFwUtvADm86q/iuIRAC3kTOLhvRQ5M9AkB3x4oYrmNsLBmOymXpjTgOlj3QgCGO48kprU+g05H3kjFSMdXYf51LAZNOQHwF5ViGqPWgCWjWFRURW/ihCTdlwJDFTJhewjKMFQNsBwDyuBsICjlRRYsqhDawJx0SXZJwq/EWUDHTwXahSP62k4KDyB2S0ykwdcT8hNYwR7jjYw12mHKP8Ladx9GUgw/BQecAlKK3NKUx+0sNQQ4WWHiGIAwyNUP1T4teRJE0YDQ6k/pWGkWADYyKSizyumFjTn9aAcOxaV+6OH2StswlYxI4xh44B65CmCcCteIrfIvjakZqrFwAlCVjFJOq5QbBXiwLY3MZXwAQ2/XONTPEQgRlrov3Mkj3LpI27C2XlGRM7NNyayjzI6SRIWLx5YnQ4S6lxgOKrldCcn+ohLyGiKcXZa72URa641I0bCQcXbc9pI1Mu6erBIM3IGllO+R4oiZDcKnugm6WYUp0cjEFzJ3bqYAsKu9skUZ86UgYlYc26MtE6uBPTDkpi0XqiZoelL03hpONR9g273TSD4B8DnHQqKOCbA66ofaht7ULxs5kv6EuATxqCGNxbux6YvZTMkyNkb4Qjq3eZbAwFAWgSMEcGl9WAoD2bW0KjKZqewlj2WcILZOpg9F8BJFjSy0TcWRwKciuZIh3S3VEeuHppAHEsN+ljLnP07nxDpiPnPkyhCUC00Z8CLCzmBjg9Mzn8bjg9F3b5H2OTX/nEFHgvTsxsfnohL5Cfuu7ZEuoXD+An0PsIM1Qpe8I0XXuUM5ifj9zlDVN//GOmHsvGS0eZDb6Df8A3xWCeDB1IR400absRWX9HKLu2XdyGbVIzDXjVS/MpSaZLWDFZOVtrDLuGi8Xdb4bCn8QTirrnOa8WrCcsXsOwhaRVk1TFbMzgHqGcLajOG56iXsPwVnfaXC2k5UhyJ8eC6WRynj4j55Z2NgdXOl3hsDbjaNawyB8voNwaQbov30PLLQMUL4JIcPc0u4WBF0NzyEIo0lxEJ0GgyYwzsWxkbJGYri24dvOI8zml4Jr26tSHO/8An9GLOHaA8yisisCY97gHc3mCF2WQlOdECNt+03c1hIkofxlCNyP98fYB+jh3QvqX9DdL2tmWWDP2AwhULiGxm/D+GaYxBHpy8Yv5+2BdMwwfD/EbIzn3u0EMA1ID9BfCAyEA749Qoi2btSGZITy+JF9Q4S+q7H/HsQuNwCcxwj5NcNt52iMvTkLCrPw5yD2JemvjtwgKSoDgPpIOAPPeAtvmnNwfvOUIUg/FrLQMY2PCwHBfbi2uA4lmGaYAXzV57nlNpy//X0Ee/8AKs8rYLBxF3XSorL7OUQsxESi0NUzjBSS2dWgt/s08O9laCAMNSzDeILZNBE25xAWMqhgY3kFDDbuxHBcktcQlzG6VdUi/6ZCuenUFaYhzOY79G9uzYS3ygGAu/typaMqzSXD7TPS0UARiH6lwlfsq8l8gW87d6jtujFOoaLP0muiH0KjYiSoImbokiH+VFlj7QLkLiV3a81s/kZoEz1DzMQVdU5AfS6JAEwmEZttZ4qpPFNhXyT+7gpawxwsOLXPL5Ds/XR59gMyPXBwL/poHikCoKibfYZ4OjP4U6HDoYaIubApDmcY1jGaLsq0b39z/t+m2VjBIYK+ZKTtktel2OBuq7pAtdOAGc2nwt92Eto3Zc7gLYIN96fsBPYTU1+E3dB+IwIG0+lB0JeMGo3+orlV6mOyXAl0sBh2BvhLRgkLGRYEwimgFC+5k6HPECmWsjKIgrFm1ttobDw17gOckNcQNZAdyFAQWy2XGwtRI7BZMo54yOwA6gvpLGNyJ8J2ydjxkDkDmF2cgMFkDUIVEFX7TzIVg1wJxMzaj4NkV+PpRVf8HsN3EntlUF+3Mkwnf6oyRBUPmU8A81vxPkHhmoJVVF6Jol/TbV0SMew5eHew33YCCJRZmGAwlz5LRove4y1YnAKCsnMwOcA54CCyHzcKclENIemzmJDCPDosGVKBl84RDaT+GMOmUljRNSMA2AlTMIEKoyFnCNfPb6pDQR/LmsVQVH+Ic6VBVEP05go63oWt+e5dJgJLC3ex0wDyktENtT6lNed8KcP6wRkumH5NVJ1PqE8qB7vSBuT3ork1DLN5PwhTfYkaYjRP8lG7hrDgQc6swkkZg/01Xw0xAm8fYPI5AcMBG7eAW0B42XgfYvGlmAiC4vgwcPR8YmrRH4O59K8heqD3w/3ZkwfiK7clYzEZxHnOMwSxU7pSNffoRHkLHdzKoYX6HELbfzEX8hqCEgiWodsbin03xLeGaMqOcO2IGYnfpOkekFFz3gdnrHjJ6TBVvhlCy8y63ZQX1mDxLETbTk96rhKReFjTg552t1BvO6/Mk22vId92arjLRFh9rCfyx9xF5ezc0HdjMsRsjJzYWpZJdZJkIqFNk2SIKZQYA2p6KLRBO4kZfDC1wGnMT+mwt9SckmE+/VwyBGX8hRHI/MXPH9Q52agNF9eEO7/rpHKRsXYZd8Bw2XepIRbXZS0Zi4wVEIuMFRCLjBUQi4wVEIuM52w7PXZT3FMfbwxsXNtODTf03VoyFhkrIBYZKyAWGSsgFhkrIBYZKyAeSWV7FON6QOaZVGY9BPgHK/KZW2aWrhUAAAAASUVORK5CYII="></image></g><g class="cls-5"><image width="174" height="119" transform="translate(18.25 14.16)" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAK4AAAB3CAYAAACaPXsJAAAACXBIWXMAAAsSAAALEgHS3X78AAAIRElEQVR4Xu2d7YLjKgiGsSf3f6O7t7Dj+dGkYxM/QAEx4fkzu50EX5BQYmwnwJ+/EVKO/wVwLPE9S49nKwYknn4e/9h/nPM6viZl+rITKiyc2fyk2S2yhZ/EQ6qzx/EvgPATAX6qR38lfXxVj3wG1HjfEWIMjgtoC//SlwlW0kNbCXsmBAiocwh6tDAoSRSj/m7wg1AWud8q4rUF4SICxP8aajND8vo3kWw4mWM8i8SNLfyTdyoA6MUu7m0LAUtti2qspBH0Y8O9ZR9UlAiKPHOpjoNj02IApxtRxOCIQ2aSfbcxrnnLVieLotk04QzVWofwE7FmAOCc6JMg6MWBN4jyHm8OAJRahSyThiXDoJPSunSvuOCHkEdYSwByq3ACJRB1kC4GJaVU5ySnndq6jCBsvshp3A2EKy7qbaIHWdnCnMQP+BJfAVArQzuk+cCb7SB+/aCS73GlUBqKNDmcKPmXIj1/sRJMUpyZZeZbBeZBeBAWJWCeNLHaIP2l+nDpzwvjoO0Wzp93c9bLYnJFYIhBPnHGDVPvmWLIKElkhPMLO3ytApOZ2+FxKRMBAjJAR3pHOFYVvvYqOI5tPptsqnekEdmLZEx0rUVqcnk7clDgCqQ4lXXck0Ki4KOSY5+9054uIQxiYDJzJ7gfp0tB63EJh6LZbbZ1nH5PSnQEreGfxLRY4AeutgpTr77WWM1E/+ZoXVDpTjN9HxbyG//IV8GpZlIVNbTFof3cubQu7SGm04xfDZR/qINUaLcKdrRekdL2wrQuO8jDTDPZh54LrlxxJztTpiCMUy+xOpPJLbqroRA/BX57XCXholOm5MM4tHc5zNIiW1wtxzDRNukBhGB0BE0DMCYIga85Qvh3XVpEnJSj8zTWGBU0tHvc1biZOyhOPpPnFFHRAUA/tpXxtu9+7n0k6xWjiXZgrUKNA7WnR+4AG6Jhc/2KOyh/2YsUSxwM0In3xvXWUTvJ0F1xrmhfL3EXk2sGRNwwySWdL5eN6zGvC/8AwiKyMVyUCUFhHBJz8QBUHvmGkpjkdcwyDTuFK9BRhDFRe8FX3LPYkDxGrfQi6e4w2g4wYxiYrCsmRVVpZgDSpf4eN3dazVSQ749ug4epyfX7cYWChmk9RgbPfnbppgx5igox6qCp9FXcjlNESHRgP7v0gdqfE80/hklx2dBrcjtdV/sk564kQs6Puhsac/15VyxKmIkRJwSnCIcC9FZcTZTllZKR+jH+2hdpqEKTLYJEKLav3tOAk2Io+/aeLMKglnt0ghvj4AbDfSN5CwYTvUydbla/aca4KzqzuV9obqHBr+NqIOTkktRi8aqs0lDhssMFSk/EV1yxK1ISnGvrIbGHOo0VdsVlRnz3MW1V3NUQnLiphYJ4YaDblkK8sKenbP1vOacTu+04KAzHl5x4tROQfqJbhakwSCQH13nDEPthAlx0eKvwhYVZMsqs0ISwj70LOHpciBEgdlSkjCNpr0O258xhVkKiyQvsq7ilJvvzOjIalhfde0G6PgfT4khs57206f5ZcdKxEWPGV1LJEcc7SkyYi76K28ugg+TvgLhbQR+M35247sdNKM771zmGo5lKw8jEfEKjagcziAMAw6GyvzvsjJTcAOSvLl2ehd2tVtwDRB3Kg7BthqLWDidyp2CqeUrHsLbpdKhw2noVVwKNEBD38/ZXC0WILnGCqrgHarEkaLot1BhQJodq2yALbyRfSKxFqag1dAHhTCa79ipgXDYB3bXnQP1OMYaNMZz07w7rPc+RR2RuiEZRFb1CdrjfF7sq7sGgtDXpD9djiAE+FR2VIx0x7a+4vWiP5+ixzy0qWQchf6+C48wmAMAWqE36AfY0jcvvjmDj+1BI67hZaudndq4/FgNxuFMNGe9xa+eP2v5QMDR658oFm59PYTxgl/24KDpOEYGqfTTPicM5crRbhdbvYTwf1ED48iEC/mPXGQZOdVpE7Y3ki/Fpo7AJ79naDzbGO+2Ki4LFiF0S96q5SQzD9cuoiQZWARs/Av3LYU9AODTk2HPNuhREd0ZgqriLs0IMLC4tTtRzi8RtFqKLjws6LSwZ8zc0vo6Il3+osm6rsKhsq5Dz4Mhi4mlc2Ki4FjQ4NE5z1qrXXFN8jDP+5AyAT5WjiO6ktRL7wyHrq3U5aY2D+3EdR4xG69LVKlSuBefBoKsqA1t2MEI2ksWSTzhB0Obcl66Ke4Fig3IsRDs7wEYh+e2UeQeyb3eYFDkpVH03yXNViCG2AE/FJSCeVyV/Cq+P7ADTZBGZYxBykWc5bGGe7v+qbK0DiviEt/HlFzHUWwUS7NrYDdahDid1I0rVUYTN0DDZmzOh8K2H9jytfiNKlD/C43vc5Ujnizp31hK9RsM3262Cw8uN5rr/5izHjQKjwUoFEEPP9PfGwFCroCBEYYjHEkIhCU9BZ5oDW0/OnCurTI9yHnmP6yzJtcc9JzLiE9SXQ3oblxJ+cS0D99SXaPe4rd9naNp0nEHoqwrcSal1ifbA7avDhnKPmxlMfHzLVwYW8SAtxyYaFEHTeIgitPOcKM95o1xxF8DjsQT0HlcLwwk0UpTZV1xKGI7fwUgo2qsKDisebx7sVlwuVk6UkZLEicEYeo9rGZ+bIvevuBI8NaF63wEE4qVUcVUGcaQxNI2y67hOP8ampbfYUvisuCD2w/iqgmMGSi6+Wgc4jkU8cZ0lKa8qEMo2O9SGaqZWZwpKqwpELGpyTOGtgrMknrjOknjiOkviiessyRp7Ffxm7RfqikuOG8TT5qqCU8bnCwC8VXAWxRPXWRJPXGdJPHGdJfHEddYjxsKfRHUcy4QA/wM0JGxAD1YgyQAAAABJRU5ErkJggg=="></image></g><path class="cls-6" d="M256.56,127.2v-.08a1,1,0,0,0-1.62-.53,26.11,26.11,0,0,1-18.42,7.14q-11.31,0-18.46-6.46a20.64,20.64,0,0,1-7.14-16q0-12,8.91-18.42t25.5-6.42h9.36a1,1,0,0,0,1-1v-4a13.18,13.18,0,0,0-3.26-9.33q-3.27-3.5-9.92-3.5A14.86,14.86,0,0,0,233,71.6a9,9,0,0,0-3.48,5.34,2.45,2.45,0,0,1-2.37,2H215.11a2.42,2.42,0,0,1-2.4-2.76,19.27,19.27,0,0,1,3.89-8.8,27.37,27.37,0,0,1,11.13-8.43,39.22,39.22,0,0,1,15.72-3q13.17,0,21,6.63t8,18.63V115q0,10,2.75,16a.93.93,0,0,1,.09.4h0a1,1,0,0,1-1,1H262.93A6.48,6.48,0,0,1,256.56,127.2Zm-16.93-7a19.17,19.17,0,0,0,9.4-2.43,16.22,16.22,0,0,0,6.5-6.29,1.05,1.05,0,0,0,.13-.49V98.07a1,1,0,0,0-1-1h-8.11q-9.37,0-14.09,3.26a10.58,10.58,0,0,0-4.71,9.23,9.86,9.86,0,0,0,3.22,7.74A12.49,12.49,0,0,0,239.63,120.2Z"></path><path class="cls-6" d="M290.21,37.77a9,9,0,0,1,2.46-6.45q2.46-2.57,7-2.57t7.08,2.57a8.85,8.85,0,0,1,2.5,6.45,8.65,8.65,0,0,1-2.5,6.35q-2.49,2.54-7.08,2.53t-7-2.53A8.74,8.74,0,0,1,290.21,37.77Zm9.85,94.57h-.76a8,8,0,0,1-8.05-8V59.68a2.41,2.41,0,0,1,2.41-2.41h6.4a8,8,0,0,1,8.05,8v59A8,8,0,0,1,300.06,132.34Z"></path><path class="cls-6" d="M336.35,132.34h-.77a8,8,0,0,1-8-8V28.19A2.41,2.41,0,0,1,330,25.77h6.4a8,8,0,0,1,8,8.05v90.47A8,8,0,0,1,336.35,132.34Z"></path><path class="cls-6" d="M439.79,119.13a3.69,3.69,0,0,1-.82,2.3q-4.46,5.53-13.09,8.76a60.15,60.15,0,0,1-21.19,3.54,39.44,39.44,0,0,1-21.1-5.69,37.88,37.88,0,0,1-14.29-16.1,54.4,54.4,0,0,1-5.13-23.87V75.31q0-21,10.61-33.2T403.3,29.93q15.6,0,25.11,8a31.52,31.52,0,0,1,10.52,17.51,3.59,3.59,0,0,1-3.53,4.38h-1.26a3.57,3.57,0,0,1-3.5-2.7q-2.22-9.15-8.51-14.18-7.17-5.74-18.76-5.74-14.16,0-22.41,10.07t-8.26,28.4V87.56a50,50,0,0,0,3.92,20.38,31.06,31.06,0,0,0,11.24,13.71,29.72,29.72,0,0,0,16.83,4.86q11,0,18.94-3.47a22.56,22.56,0,0,0,6.59-4.22,3.51,3.51,0,0,0,1-2.53V95.22a3.61,3.61,0,0,0-3.61-3.61H407.74A3.61,3.61,0,0,1,404.13,88h0a3.61,3.61,0,0,1,3.61-3.6h28.44a3.6,3.6,0,0,1,3.61,3.6Z"></path><path class="cls-6" d="M472.54,95.46v32.61a4.27,4.27,0,0,1-4.27,4.27h0a4.27,4.27,0,0,1-4.27-4.27V35.59a4.27,4.27,0,0,1,4.27-4.27h30.15q15.75,0,24.87,8t9.12,22.13q0,14.22-8.77,22T498.21,91.2h-21.4A4.27,4.27,0,0,0,472.54,95.46Zm0-15.75A4.27,4.27,0,0,0,476.81,84h21.61q12.42,0,18.94-5.9t6.52-16.47q0-10.5-6.49-16.71T499,38.54H476.81a4.27,4.27,0,0,0-4.27,4.26Z"></path><path class="cls-6" d="M616.43,38.54H589a3.61,3.61,0,0,0-3.61,3.6v86.59a3.61,3.61,0,0,1-3.61,3.61h-1.32a3.61,3.61,0,0,1-3.6-3.61V42.14a3.61,3.61,0,0,0-3.61-3.6H545.94a3.61,3.61,0,0,1-3.61-3.61h0a3.61,3.61,0,0,1,3.61-3.61h70.49A3.61,3.61,0,0,1,620,34.93h0A3.61,3.61,0,0,1,616.43,38.54Z"></path></g></g></svg>
               </div>
               <div class="f-md-44 f-24 w600 white-clr lh140 col-12 mt20 mt-md50 text-center ">
              <span class="blue-gradient under"> World’s First ChatGPT3 AI</span>  - Powered Email Marketing Solution That Write, Design & Deliver Unlimited Profit-Pulling Emails Directly to Inbox for Maximum Traffic, Clicks & Opens 
               </div>
               <div class="f-20 f-md-24 lh140 w400 text-center white-clr mt20">              
               It is a must-have tool in your marketing arsenal that comes with a ONE-TIME FEE, and that will send all the existing money-sucking autoresponders back to their nest. </div>
            </div>
            <div class="row align-items-center mt20 mt-md50">
               <div class="col-12 col-md-10 mx-auto">
                  <img src="assets/images/product-box.webp" class="img-fluid d-block mx-auto" alt="Product">
               </div>
            </div>
         </div>
      </div>
      <!-- Proudly Section End  -->


      <!-------Coursova Starts----------->
      <!-------Exclusive Bonus----------->
      <div class="exclusive-bonus">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                  <div class="white-clr f-24 f-md-36 lh140 w600">
                     Exclusive Bonus #1 : Vidmaster
                  </div>
                  <div class="f-18 f-md-20 w500 mt20 white-clr">
                     20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
                  </div>
               </div>
            </div>
         </div>
      </div>

      <div class="header-section-vidmaster">
         <div class="container">
            <div class="row">

            <div class="col-12 mt-md40 mt20 text-center">
                  <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 858.2 150.9" style="enable-background:new 0 0 858.2 150.9; max-height: 80px;" xml:space="preserve">
                     <style type="text/css">
                        .st0z{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_1_);}
                        .st1z{fill-rule:evenodd;clip-rule:evenodd;fill:#3C4650;}
                        .st2z{fill-rule:evenodd;clip-rule:evenodd;fill:#1C262D;}
                        .st3z{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_2_);}
                        .st4z{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_3_);}
                        .st5z{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_4_);}
                        .st6z{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_5_);}
                        .st7z{fill:#FFFFFF;}
                     </style>
                     <g id="Layer_1-2">
                        <g>
                           <g>
                              <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="25.2183" y1="-779.9527" x2="12.9283" y2="-954.9427" gradientTransform="matrix(1 0 0 -1 0 -776)">
                                 <stop offset="0" style="stop-color:#CCCCCB"></stop>
                                 <stop offset="0.32" style="stop-color:#3C4650"></stop>
                                 <stop offset="0.7" style="stop-color:#252D33"></stop>
                                 <stop offset="1" style="stop-color:#181D20"></stop>
                              </linearGradient>
                              <path class="st0z" d="M40.1,3.1c-14.4-6-22.8-3.9-29.9,10.8C-0.2,35.8-2.6,80.4,3,112.9c5.4,20.7,22.1,25.1,32.2,15
                              c0.7-0.3,1.5-0.6,2.1-0.9c-4.3,1.9-9.1-0.3-10.5-4.8c-7.4-23.6-6.2-70,0.3-91.8C30.9,17.5,39.4,4,40.1,3.1L40.1,3.1z"></path>
                              <path class="st1z" d="M31.6,0.4C1.2,18.4,7.9,102.5,18.1,129.5c5.3,1.4,11.3,0.9,17-1.6c0.7-0.3,1.5-0.6,2.1-0.9
                              c-4.3,1.9-9.1-0.3-10.5-4.8c-7.4-23.6-4.8-64.4,1.9-88.3C32.9,18.4,39.3,4.6,40.1,3.1c-3.2-1.3-3.4-1.4-6.2-2.2L31.6,0.4
                              L31.6,0.4z"></path>
                              <path class="st2z" d="M2.4,109.5c0.2,1.2,0.3,2.3,0.6,3.4c3.4,15.5,18.4,20.7,32.2,15c0.7-0.3,1.5-0.6,2.1-0.9
                              c-4.3,1.9-9.1-0.3-10.5-4.8C13.8,125.2,5.6,121.7,2.4,109.5L2.4,109.5z"></path>
                              <linearGradient id="SVGID_2_" gradientUnits="userSpaceOnUse" x1="-5.0837" y1="-921.3506" x2="166.8963" y2="-831.7707" gradientTransform="matrix(1 0 0 -1 0 -776)">
                                 <stop offset="0" style="stop-color:#CCCCCB"></stop>
                                 <stop offset="0.32" style="stop-color:#3C4650"></stop>
                                 <stop offset="0.7" style="stop-color:#252D33"></stop>
                                 <stop offset="1" style="stop-color:#181D20"></stop>
                              </linearGradient>
                              <path class="st3z" d="M118.5,101.8c15-13.2,4.7-28.9-11.3-33.6c6.2,5.9,5.8,10.5-1.3,16.6c-10.3,8.8-22.8,17-34.3,24.1
                              c-11.4,6.9-24,13.9-36.3,19c-13.8,5.7-28.8,0.5-32.2-15c1.7,9.8,4.1,18.4,7.2,25c7.1,14.8,18.7,15.5,33.1,9.6
                              C65.7,138.2,99.7,117.5,118.5,101.8L118.5,101.8L118.5,101.8z"></path>
                              <path class="st1z" d="M120.4,83.8c-14.2,18.1-62,46.8-80.9,55.3c-11.8,5.2-25.1,7.1-31.1-5.5c0.6,1.6,1.1,2.9,1.8,4.3
                              c7.1,14.8,18.7,15.5,33.1,9.6c22.4-9.3,56.5-30,75.2-45.6C124.2,96.5,123.7,90.1,120.4,83.8L120.4,83.8L120.4,83.8z"></path>
                              <linearGradient id="SVGID_3_" gradientUnits="userSpaceOnUse" x1="-9.377" y1="-913.095" x2="162.593" y2="-823.515" gradientTransform="matrix(1 0 0 -1 0 -776)">
                                 <stop offset="0" style="stop-color:#CCCCCB"></stop>
                                 <stop offset="0.32" style="stop-color:#4E4F4F"></stop>
                                 <stop offset="0.7" style="stop-color:#383838"></stop>
                                 <stop offset="1" style="stop-color:#231F20"></stop>
                              </linearGradient>
                              <path class="st4z" d="M35.2,127.9c-13.8,5.7-28.8,0.5-32.2-15c0.2,1.2,0.4,2.5,0.7,3.7c4.3,15.7,18.5,20.2,33,13.8
                              c24.1-10.6,47-27.5,57.7-36.5c-7.4,5.4-15.3,10.4-22.8,15C60.1,115.8,47.5,122.8,35.2,127.9L35.2,127.9L35.2,127.9z"></path>
                              <path class="st2z" d="M115.3,104.5c1.1-0.9,2.1-1.8,3.2-2.6c11-10.1-0.8-24.2-11.3-33.6c4.9,4.8,5.7,8.7,2.1,13.1
                              C115.6,85.6,123.2,97.3,115.3,104.5L115.3,104.5L115.3,104.5z"></path>
                              <linearGradient id="SVGID_4_" gradientUnits="userSpaceOnUse" x1="39.6241" y1="-837.5" x2="136.5" y2="-837.5" gradientTransform="matrix(1 0 0 -1 0 -776)">
                                 <stop offset="0" style="stop-color:#ED2024"></stop>
                                 <stop offset="1" style="stop-color:#F05757"></stop>
                              </linearGradient>
                              <path class="st5z" d="M60,105.5c-3.4-18.2-3.5-36.9-0.7-55.1c2.6,15.8,8,31.4,15.4,46.2c8.1-5.2,16.1-10.7,23.5-16.8
                              c0.2-6.1,0.6-12,1.2-17.9c10.3,7.9,33,27.2,19.1,40c0.4-0.3,0.7-0.7,1.2-1c22.4-19.2,22.4-30.6,0-49.9
                              c-9.9-8.4-21.3-16.1-32.4-23c-3.1,12.2-5.1,24.7-6.1,37.3c-5-16.1-5.8-28-5.8-44.5c-7.5-4.3-15.2-8.5-23-12.3
                              c-13.2,33.6-16.3,70.8-8.7,106C49.2,111.7,54.6,108.7,60,105.5L60,105.5L60,105.5z"></path>
                              <linearGradient id="SVGID_5_" gradientUnits="userSpaceOnUse" x1="144.1735" y1="-864.6838" x2="14.8335" y2="-839.7738" gradientTransform="matrix(1 0 0 -1 0 -776)">
                                 <stop offset="0" style="stop-color:#ED2024"></stop>
                                 <stop offset="1" style="stop-color:#F05757"></stop>
                              </linearGradient>
                              <path class="st6z" d="M99.4,61.9c10.3,8,33,27.2,19.1,40l1.2-1c6.8-5.8,6.8-13,1.9-20.5C116.3,72.4,106.1,65.4,99.4,61.9
                              L99.4,61.9L99.4,61.9z"></path>
                           </g>
                           <g>
                              <path class="st7z" d="M834,63.8V50h-15.5h-8.2c3.2,0,5.4,2.2,5.7,4.5c0,4.4,0.1,9.4,0.1,15.6v54H834V90.6
                              c0-19.8,9.2-26.9,24.2-26.8V50.1C845.7,50.3,837.5,54.8,834,63.8L834,63.8z"></path>
                              <path class="st7z" d="M573.3,106.1c0,6.9,0.6,16.3,1,18.2h-17.1c-0.6-1.5-1-5.3-1.1-8.1c-2.7,4.4-8,9.8-21.5,9.8
                              c-17.7,0-25.3-11.6-25.3-23.1c0-16.8,13.4-24.5,35.2-24.5h11.3v-5.1c0-5.7-2-11.8-12.9-11.8c-9.9,0-11.9,4.5-13,10h-17.1
                              c1.1-12.2,8.6-23.2,30.7-23.1c19.3,0.1,29.8,7.7,29.8,25.1L573.3,106.1L573.3,106.1z M555.8,89.6h-9.6
                              c-13.2,0-18.9,3.9-18.9,12.1c0,6.2,4,11.1,11.9,11.1c14.7,0,16.5-10.1,16.5-21.1L555.8,89.6L555.8,89.6z"></path>
                              <path class="st7z" d="M745.2,90.8c0,11.2,5.7,20.8,16.7,20.8c9.6,0,12.5-4.3,14.6-9.1h18c-2.7,9.2-10.8,23.4-33.1,23.4
                              c-24,0-34.3-18.5-34.3-37.8c0-22.8,11.7-39.8,35-39.8c24.9,0,33.3,18.7,33.3,36.3c0,2.4,0,4.1-0.3,6.2H745.2L745.2,90.8z
                              M777.4,79.4c-0.2-9.8-4.5-18-15.4-18s-15.5,7.6-16.5,18H777.4L777.4,79.4z"></path>
                              <path class="st7z" d="M606.8,102.3c1.8,6.6,7,10.4,15.4,10.4s12-3.4,12-8.6s-3.3-7.8-15.1-10.7c-23.2-5.7-27.3-12.8-27.3-23.2
                              s7.7-21.9,29-21.9s29,11.9,29.8,22h-17.1c-0.8-3.4-3.2-9.1-13.5-9.1c-8,0-10.5,3.7-10.5,7.5c0,4.3,2.5,6.4,15.1,9.4
                              c24,5.6,27.8,13.8,27.8,24.7c0,12.5-9.7,23.1-30.9,23.1s-30.7-10.5-32.5-23.7L606.8,102.3L606.8,102.3z"></path>
                              <path class="st7z" d="M471,48.3c-12.3,0-18.9,5.6-23.1,12c-2.7-6.5-9-12-19.6-12c-11.2,0-17.3,5.5-20.9,11.4l0.1-9.7h-23.6
                              c3.4,0,5.8,2.5,5.8,5l0,0c0.1,4.7,0.1,9.4,0.1,14.1v55h17.7V82.9c0-13.2,4.5-19.8,14.1-19.8s11.9,7,11.9,15.3v45.8h17.6V81.7
                              c0-12,4-18.8,13.9-18.8s12.1,7.4,12.1,14.7v46.5h17.5V75.5C494.4,56,483.6,48.3,471,48.3L471,48.3z"></path>
                              <path class="st7z" d="M368.8,103.6V26.4h-17.9h-5.8c3.4,0,5.8,2.5,5.8,5l0,0v24.8c-1.8-3.6-7.5-7.9-19-7.9
                              c-20.8,0-33.5,16.7-33.5,39.5s11.9,38.1,30.7,38.1c11.4,0,18.2-3.9,21.8-10.4l0.1,8.7h18C368.8,117.3,368.8,110.5,368.8,103.6
                              L368.8,103.6z M334,111.4c-10.6,0-17.2-8.4-17.2-24.1s6.3-24.4,17.9-24.4c14.5,0,16.9,9.9,16.9,23.8
                              C351.5,99.2,349.2,111.3,334,111.4L334,111.4z"></path>
                              <g>
                                 <path class="st7z" d="M265.6,50h-5.8c3.4,0,5.8,2.5,5.8,5v69.2h17.9V50H265.6z"></path>
                                 <path class="st7z" d="M283.5,29.1v-2.7h-17.9V47l0,0C275.5,46.9,283.5,38.9,283.5,29.1L283.5,29.1z"></path>
                              </g>
                              <g>
                                 <path class="st7z" d="M712.3,63.8V50.1h-15.1l0,0h-17.9l0,0h-11.9v13.8h11.9v41.5c0,12.8,4.9,19.9,18.4,19.9
                                    c3.9,0,9.1-0.1,12.9-1.4v-12.6c-1.7,0.3-3.9,0.4-5.3,0.4c-6.3,0-8-2.7-8-8.8V63.8H712.3L712.3,63.8z"></path>
                                 <path class="st7z" d="M697,29.1v-2.7h-17.9V47l0,0C689,46.9,697,38.9,697,29.1L697,29.1z"></path>
                              </g>
                              <path class="st7z" d="M231.3,25.9l-16.3,47c-4.8,14-9.3,27.1-11.3,35.9h-0.3c-2.2-9.7-6.2-22.3-10.8-36.3L177,25.9h-25.7
                              c5.1,0,7.6,4.6,8.6,7.4l32.4,90.9h21.8l36.1-98.3L231.3,25.9L231.3,25.9z"></path>
                           </g>
                        </g>
                     </g>
                  </svg>
               </div>
               <div class="col-12 text-center mt20">
                  <div class="pre-heading2 f-20 f-md-24 w500 lh140 white-clr">
                     Encash the Latest TRAFFIC Trend of 2023…
                  </div>
               </div>
               <div class="col-12 mt-md50 mt30 f-md-45 f-28 w700 text-center white-clr lh140 text-capitalize">
                  Game-Changer AI APP Creates Stories, Reels, Boomerang, &amp; Short Videos with <span class="red-clr">Just One Keyword…</span> for Floods of FREE Traffic and Sales
               </div>
               <div class="col-12 mt-md25 mt20 f-16 f-md-20 w400 text-center lh140 white-clr text-capitalize">
               Even A Complete Beginner Can Drive Thousands of Visitors to Any Offer or Link  Instantly <br> |  No Camera | No Writing &amp; Editing | No Paid Traffic Ever…
               </div>
            </div>
            <div class="row d-flex flex-wrap mt20 mt-md40">
               <div class="col-md-7 col-12 min-md-video-width-left mt-md40 mt0">
                  <!-- <img src="assets/images/product-image.webp" class="img-fluid mx-auto d-block"> -->
                  <div class="col-12 responsive-video border-video">
                     <iframe src="https://vidmaster.dotcompal.com/video/embed/8x6aokyyl7" style="width:100%; height:100%" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                     </div>
                  
               </div>
               <div class="col-md-5 col-12 mt20 mt-md0 min-md-video-width-right">
                  <div class="calendar-wrap mb20 mb-md0">
                     <ul class="list-head pl0 m0 f-18 lh140 w400 white-clr">
                     <li>Drive Tsunami of FREE Traffic in Any Niche</li>
                     <li>Create Reels &amp; Shorts Using Just One Keyword </li>
                     <li>Create Boomerang Video to Engage Audience</li>
                     <li>Get Thousands of Visitors &amp; Sales to Any Offer or Page</li>
                     <li>Make Tons of Affiliate Commissions or Ad Profits</li> 
                     <li>Create Short Videos from Any Video, Text, or Just a Keyword</li>
                     <li>Add Background Music and/or Voiceover to Any Video</li>
                     <li>Pre-designed 50+ Shorts Video templates</li>
                     <li>25+ Vector Images to Choose From</li>
                     <li>100+ Social Sharing Platforms ForFREE Viral Traffic</li>
                     <li>100% Newbie Friendly, No Prior Experience Or Tech Skills Needed</li>
                     <li>No Camera Recording, No Voice, Or Complex Editing Required</li>
                     <li>Free Commercial License To Sell Video Services For High Profits</li>
                     </ul>
                  </div>
                  <div class="d-md-none">
                     <div class="f-18 f-md-26 w700 lh140 text-center white-clr">
                        Free Commercial License + Low 1-Time Price For Launch Period Only 
                     </div>
                     <div class="row">
                        <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center">
                           <a href="#buynow" class="cta-link-btn d-block px0">Get Instant Access To VidMaster</a>
                        </div>
                     </div>
                     <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                        <img src="assets/images/compaitable-with1.webp" class="img-responsive mx-xs-center md-img-right" alt="visa">
                        <div class="d-md-block d-none visible-md px-md30"><img src="assets/images/v-line.webp" class="img-fluid" alt="line">
                        </div>
                        <img src="assets/images/days-gurantee1.webp" class="img-responsive mx-xs-auto mt15 mt-md0" alt="30 days">
                     </div>
                     <div class="col-12 col-md-8 mx-auto mt30 d-md-none">
                        <img src="assets/images/limited-time.webp" class="img-fluid d-block mx-auto">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="tr-all">
         <picture>
            <source media="(min-width:767px)" srcset="assets/images/vidmaster.webp" class="img-fluid d-block mx-auto" style="width:100%">
            <source media="(min-width:320px)" srcset="assets/images/vidmaster-mview.webp" class="img-fluid d-block mx-auto">
            <img src="assets/images/vidmaster.webp" alt="Steps" class="img-fluid d-block mx-auto" style="width:100%">
         </picture>
      </div>
      <!------Coursova Section ends------>

      <!-------Exclusive Bonus----------->
      <div class="exclusive-bonus">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                  <div class="white-clr f-24 f-md-36 lh140 w600">
                     Exclusive Bonus #2 : Trendio
                  </div>
                  <div class="f-18 f-md-20 w500 mt20 white-clr">
                     20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!------Trendio Section------>

      <div class="tr-header-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row">
                     <div class="col-md-3 text-center mx-auto">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 912.39 246.83" style="enable-background:new 0 0 912.39 246.83; max-height:50px;color: #023047;" xml:space="preserve">
                           <style type="text/css">
                              .st0{fill:#FFFFFF;}
                              .st1{opacity:0.3;}
                              .st2{fill:url(#SVGID_1_);}
                              .st3{fill:url(#SVGID_00000092439463549223389810000006963263784374171300_);}
                              .st4{fill:url(#SVGID_00000015332009897132114970000017423936041397956233_);}
                              .st5{fill:#023047;}
                           </style>
                           <g>
                              <g>
                                 <path class="st0" d="M18.97,211.06L89,141.96l2.57,17.68l10.86,74.64l2.12-1.97l160.18-147.9l5.24-4.84l6.8-6.27l5.24-4.85 l-25.4-27.51l-12.03,11.11l-5.26,4.85l-107.95,99.68l-2.12,1.97l-11.28-77.58l-2.57-17.68L0,177.17c0.31,0.72,0.62,1.44,0.94,2.15 c0.59,1.34,1.2,2.67,1.83,3.99c0.48,1.03,0.98,2.06,1.5,3.09c0.37,0.76,0.76,1.54,1.17,2.31c2.57,5.02,5.43,10,8.57,14.93 c0.58,0.89,1.14,1.76,1.73,2.65L18.97,211.06z"></path>
                              </g>
                              <g>
                                 <g>
                                    <polygon class="st0" points="328.54,0 322.97,17.92 295.28,106.98 279.91,90.33 269.97,79.58 254.51,62.82 244.58,52.05 232.01,38.45 219.28,24.66 "></polygon>
                                 </g>
                              </g>
                           </g>
                           <g class="st1">
                              <g>
                                 <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="17.428" y1="42.82" x2="18.9726" y2="42.82" gradientTransform="matrix(1 0 0 -1 0 252.75)">
                                    <stop offset="0" style="stop-color:#000000"></stop>
                                    <stop offset="1" style="stop-color:#333333"></stop>
                                 </linearGradient>
                                 <path class="st2" d="M17.43,208.8l1.54,2.26c-0.37-0.53-0.73-1.05-1.09-1.58c-0.02-0.02-0.02-0.03-0.03-0.03 c-0.06-0.09-0.11-0.17-0.17-0.27C17.6,209.06,17.51,208.92,17.43,208.8z"></path>
                                 <linearGradient id="SVGID_00000177457867953882822120000005144526232562103955_" gradientUnits="userSpaceOnUse" x1="18.98" y1="70.45" x2="91.57" y2="70.45" gradientTransform="matrix(1 0 0 -1 0 252.75)">
                                    <stop offset="0" style="stop-color:#000000"></stop>
                                    <stop offset="1" style="stop-color:#333333"></stop>
                                 </linearGradient>
                                 <path style="fill:url(#SVGID_00000177457867953882822120000005144526232562103955_);" d="M89,141.96l2.57,17.68l-63.83,63 c-1.19-1.45-2.34-2.9-3.46-4.35c-1.84-2.4-3.6-4.81-5.3-7.22L89,141.96z"></path>
                                 <linearGradient id="SVGID_00000010989203515549635820000018393619450353154703_" gradientUnits="userSpaceOnUse" x1="104.55" y1="120.005" x2="333.22" y2="120.005" gradientTransform="matrix(1 0 0 -1 0 252.75)">
                                    <stop offset="0" style="stop-color:#000000"></stop>
                                    <stop offset="1" style="stop-color:#333333"></stop>
                                 </linearGradient>
                                 <polygon style="fill:url(#SVGID_00000010989203515549635820000018393619450353154703_);" points="333.22,15.61 299.96,122.58 274.65,95.18 107.11,249.88 104.55,232.31 264.73,84.41 269.97,79.58 279.91,90.33 295.28,106.98 322.97,17.92 "></polygon>
                              </g>
                           </g>
                           <g>
                              <g>
                                 <g>
                                    <path class="st5" d="M229.46,241.94c-12.28,5.37-23.49,8.06-33.57,8.06c-9.63,0-18.21-2.44-25.71-7.35 c-23.05-15.07-23.72-46.17-23.72-49.67V61.67h32.2v30h39.27v32.2h-39.27v69.04c0.07,4.46,1.88,18.1,9.2,22.83 c5.51,3.55,15.7,2.38,28.68-3.3L229.46,241.94z"></path>
                                 </g>
                              </g>
                           </g>
                           <g>
                              <g>
                                 <path class="st5" d="M250.4,164.29c0-15.97-0.24-27.35-0.97-38h25.9l0.97,22.51h0.97c5.81-16.7,19.61-25.17,32.19-25.17 c2.9,0,4.6,0.24,7.02,0.73v28.08c-2.42-0.48-5.08-0.97-8.71-0.97c-14.28,0-23.96,9.2-26.63,22.51c-0.48,2.66-0.97,5.81-0.97,9.2 v61H250.4V164.29z"></path>
                              </g>
                           </g>
                           <g>
                              <g>
                                 <path class="st5" d="M459.28,161.39c0-13.55-0.24-24.93-0.97-35.1h26.14l1.45,17.67h0.73c5.08-9.2,17.91-20.33,37.52-20.33 c20.57,0,41.87,13.31,41.87,50.59v69.95h-29.77v-66.56c0-16.94-6.29-29.77-22.51-29.77c-11.86,0-20.09,8.47-23.24,17.43 c-0.97,2.66-1.21,6.29-1.21,9.68v69.23h-30.01V161.39z"></path>
                              </g>
                           </g>
                           <g>
                              <g>
                                 <path class="st5" d="M706.41,72.31V211c0,12.1,0.48,25.17,0.97,33.16h-26.63l-1.21-18.64h-0.48c-7.02,13.07-21.3,21.3-38.49,21.3 c-28.08,0-50.35-23.96-50.35-60.27c-0.24-39.45,24.45-62.93,52.77-62.93c16.22,0,27.84,6.78,33.16,15.49h0.48v-66.8 C676.63,72.31,706.41,72.31,706.41,72.31z M676.64,175.43c0-2.42-0.24-5.33-0.73-7.75c-2.66-11.62-12.1-21.06-25.66-21.06 c-19.12,0-29.77,16.94-29.77,38.97c0,21.54,10.65,37.28,29.53,37.28c12.1,0,22.75-8.23,25.66-21.06c0.73-2.66,0.97-5.57,0.97-8.71 V175.43z"></path>
                              </g>
                           </g>
                           <path class="st0" d="M769.68,89.95c-0.11-0.53-0.24-1.04-0.39-1.55c-0.12-0.39-0.25-0.76-0.39-1.14c-0.12-0.32-0.25-0.64-0.39-0.95 c-0.12-0.27-0.25-0.54-0.39-0.81c-0.12-0.24-0.25-0.47-0.39-0.7c-0.12-0.21-0.25-0.42-0.39-0.63c-0.13-0.19-0.26-0.38-0.39-0.57 c-0.13-0.17-0.26-0.35-0.39-0.51s-0.26-0.32-0.39-0.47s-0.26-0.3-0.39-0.44s-0.26-0.28-0.39-0.41c-0.13-0.13-0.26-0.25-0.39-0.37 s-0.26-0.23-0.39-0.35c-0.13-0.11-0.26-0.22-0.39-0.33c-0.13-0.1-0.26-0.2-0.39-0.3c-0.13-0.1-0.26-0.19-0.39-0.28 s-0.26-0.18-0.39-0.27c-0.13-0.08-0.26-0.16-0.39-0.24c-0.13-0.08-0.26-0.16-0.39-0.24c-0.13-0.07-0.26-0.14-0.39-0.21 s-0.26-0.14-0.39-0.2s-0.26-0.13-0.39-0.19c-0.13-0.06-0.26-0.12-0.39-0.18s-0.26-0.11-0.39-0.17c-0.13-0.05-0.26-0.1-0.39-0.15 s-0.26-0.1-0.39-0.14s-0.26-0.09-0.39-0.13s-0.26-0.08-0.39-0.12s-0.26-0.07-0.39-0.11c-0.13-0.03-0.26-0.07-0.39-0.1 s-0.26-0.06-0.39-0.09s-0.26-0.05-0.39-0.08s-0.26-0.05-0.39-0.07s-0.26-0.04-0.39-0.06s-0.26-0.04-0.39-0.06s-0.26-0.03-0.39-0.04 s-0.26-0.03-0.39-0.04s-0.26-0.02-0.39-0.03s-0.26-0.02-0.39-0.03s-0.26-0.01-0.39-0.02c-0.13,0-0.26-0.01-0.39-0.01 s-0.26-0.01-0.39-0.01s-0.26,0.01-0.39,0.01s-0.26,0-0.39,0.01c-0.13,0-0.26,0.01-0.39,0.02c-0.13,0.01-0.26,0.02-0.39,0.03 s-0.26,0.02-0.39,0.03s-0.26,0.03-0.39,0.04c-0.13,0.02-0.26,0.03-0.39,0.04c-0.13,0.02-0.26,0.04-0.39,0.06s-0.26,0.04-0.39,0.06 s-0.26,0.05-0.39,0.08s-0.26,0.05-0.39,0.08s-0.26,0.06-0.39,0.09s-0.26,0.07-0.39,0.1c-0.13,0.04-0.26,0.07-0.39,0.11 s-0.26,0.08-0.39,0.12s-0.26,0.09-0.39,0.13c-0.13,0.05-0.26,0.09-0.39,0.14s-0.26,0.1-0.39,0.15s-0.26,0.11-0.39,0.16 c-0.13,0.06-0.26,0.12-0.39,0.18s-0.26,0.12-0.39,0.19s-0.26,0.14-0.39,0.21s-0.26,0.14-0.39,0.21c-0.13,0.08-0.26,0.16-0.39,0.24 c-0.13,0.08-0.26,0.16-0.39,0.24c-0.13,0.09-0.26,0.18-0.39,0.27s-0.26,0.19-0.39,0.28c-0.13,0.1-0.26,0.2-0.39,0.3 c-0.13,0.11-0.26,0.22-0.39,0.33s-0.26,0.23-0.39,0.34c-0.13,0.12-0.26,0.24-0.39,0.37c-0.13,0.13-0.26,0.27-0.39,0.4 c-0.13,0.14-0.26,0.28-0.39,0.43s-0.26,0.3-0.39,0.46c-0.13,0.16-0.26,0.33-0.39,0.5c-0.13,0.18-0.26,0.37-0.39,0.55 c-0.13,0.2-0.26,0.4-0.39,0.61c-0.13,0.22-0.26,0.45-0.39,0.68c-0.14,0.25-0.27,0.51-0.39,0.77c-0.14,0.3-0.27,0.6-0.39,0.9 c-0.14,0.36-0.27,0.73-0.39,1.1c-0.15,0.48-0.28,0.98-0.39,1.48c-0.25,1.18-0.39,2.42-0.39,3.7c0,1.27,0.14,2.49,0.39,3.67 c0.11,0.5,0.24,0.99,0.39,1.47c0.12,0.37,0.24,0.74,0.39,1.1c0.12,0.3,0.25,0.6,0.39,0.89c0.12,0.26,0.25,0.51,0.39,0.76 c0.12,0.23,0.25,0.45,0.39,0.67c0.12,0.2,0.25,0.41,0.39,0.6c0.13,0.19,0.25,0.37,0.39,0.55c0.13,0.17,0.25,0.34,0.39,0.5 c0.13,0.15,0.26,0.3,0.39,0.45c0.13,0.14,0.26,0.28,0.39,0.42c0.13,0.13,0.26,0.26,0.39,0.39c0.13,0.12,0.26,0.25,0.39,0.37 c0.13,0.11,0.26,0.22,0.39,0.33s0.26,0.21,0.39,0.32c0.13,0.1,0.26,0.2,0.39,0.3c0.13,0.09,0.26,0.18,0.39,0.27s0.26,0.18,0.39,0.26 s0.26,0.16,0.39,0.24c0.13,0.08,0.26,0.15,0.39,0.23c0.13,0.07,0.26,0.14,0.39,0.21s0.26,0.13,0.39,0.19 c0.13,0.06,0.26,0.13,0.39,0.19c0.13,0.06,0.26,0.11,0.39,0.17s0.26,0.11,0.39,0.17c0.13,0.05,0.26,0.1,0.39,0.14 c0.13,0.05,0.26,0.1,0.39,0.14s0.26,0.08,0.39,0.12s0.26,0.08,0.39,0.12s0.26,0.07,0.39,0.1s0.26,0.07,0.39,0.1s0.26,0.06,0.39,0.08 c0.13,0.03,0.26,0.06,0.39,0.08c0.13,0.02,0.26,0.05,0.39,0.07s0.26,0.04,0.39,0.06s0.26,0.04,0.39,0.05 c0.13,0.02,0.26,0.03,0.39,0.04s0.26,0.03,0.39,0.04s0.26,0.02,0.39,0.03s0.26,0.02,0.39,0.03s0.26,0.01,0.39,0.01 s0.26,0.01,0.39,0.01c0.05,0,0.1,0,0.15,0c0.08,0,0.16,0,0.24-0.01c0.13,0,0.26,0,0.39-0.01c0.13,0,0.26,0,0.39-0.01 s0.26-0.02,0.39-0.03s0.26-0.01,0.39-0.03c0.13-0.01,0.26-0.02,0.39-0.04c0.13-0.01,0.26-0.03,0.39-0.04 c0.13-0.02,0.26-0.03,0.39-0.05c0.13-0.02,0.26-0.04,0.39-0.06s0.26-0.04,0.39-0.06s0.26-0.05,0.39-0.08s0.26-0.05,0.39-0.08 s0.26-0.06,0.39-0.09s0.26-0.06,0.39-0.1s0.26-0.07,0.39-0.11s0.26-0.08,0.39-0.12s0.26-0.08,0.39-0.13 c0.13-0.04,0.26-0.09,0.39-0.14s0.26-0.1,0.39-0.15s0.26-0.11,0.39-0.16c0.13-0.06,0.26-0.11,0.39-0.17s0.26-0.12,0.39-0.18 s0.26-0.13,0.39-0.2s0.26-0.14,0.39-0.21s0.26-0.15,0.39-0.23s0.26-0.15,0.39-0.24c0.13-0.08,0.26-0.17,0.39-0.26 c0.13-0.09,0.26-0.18,0.39-0.27c0.13-0.1,0.26-0.19,0.39-0.29s0.26-0.21,0.39-0.32s0.26-0.22,0.39-0.33 c0.13-0.12,0.26-0.24,0.39-0.36c0.13-0.13,0.26-0.26,0.39-0.39c0.13-0.14,0.26-0.28,0.39-0.42c0.13-0.15,0.26-0.3,0.39-0.45 c0.13-0.16,0.26-0.33,0.39-0.49c0.13-0.18,0.26-0.36,0.39-0.54c0.13-0.2,0.26-0.4,0.39-0.6c0.14-0.22,0.26-0.44,0.39-0.67 c0.14-0.25,0.27-0.5,0.39-0.76c0.14-0.29,0.27-0.58,0.39-0.88c0.14-0.36,0.27-0.72,0.39-1.1c0.15-0.48,0.28-0.97,0.39-1.46 c0.25-1.17,0.39-2.4,0.39-3.66C770.03,92.19,769.9,91.05,769.68,89.95z"></path>
                           <g>
                              <g>
                                 <rect x="738.36" y="126.29" class="st5" width="30.01" height="117.88"></rect>
                              </g>
                           </g>
                           <g>
                              <g>
                                 <path class="st5" d="M912.39,184.14c0,43.33-30.5,62.69-60.51,62.69c-33.4,0-59.06-22.99-59.06-60.75 c0-38.73,25.41-62.45,61-62.45C888.91,123.63,912.39,148.32,912.39,184.14z M823.56,185.35c0,22.75,11.13,39.94,29.29,39.94 c16.94,0,28.8-16.7,28.8-40.42c0-18.4-8.23-39.45-28.56-39.45C832.03,145.41,823.56,165.74,823.56,185.35z"></path>
                              </g>
                           </g>
                           <g>
                              <path class="st5" d="M386,243.52c-2.95,0-5.91-0.22-8.88-0.66c-15.75-2.34-29.65-10.67-39.13-23.46 c-9.48-12.79-13.42-28.51-11.08-44.26c2.34-15.75,10.67-29.65,23.46-39.13c12.79-9.48,28.51-13.42,44.26-11.08 s29.65,10.67,39.13,23.46l7.61,10.26l-55.9,41.45l-15.21-20.51l33.41-24.77c-3.85-2.36-8.18-3.94-12.78-4.62 c-9-1.34-17.99,0.91-25.3,6.33s-12.07,13.36-13.41,22.37c-1.34,9,0.91,17.99,6.33,25.3c5.42,7.31,13.36,12.07,22.37,13.41 c9,1.34,17.99-0.91,25.3-6.33c4.08-3.02,7.35-6.8,9.72-11.22l22.5,12.08c-4.17,7.77-9.89,14.38-17.02,19.66 C411,239.48,398.69,243.52,386,243.52z"></path>
                           </g>
                        </svg>
                     </div>
                  </div>
               </div>
               <div class="col-12 text-center lh150 mt20 mt-md50 px15 px-md0">
                  <div class="trpre-heading f-20 f-md-22 w500 lh150 blue-gradient">
                     <span>
                     It's Time to Get Over Boring Content &amp; Cash into The Latest Trending Topics in 2022 
                     </span>
                  </div>
               </div>
               <div class="col-12 mt-md25 mt20 f-md-45 f-28 w500 text-center black-clr lh150">
                  Breakthrough A.I. Technology <span class="w700 blue-clr">Creates Traffic Pulling Websites Packed with Trendy Content &amp; Videos from Just One Keyword</span> in 60 Seconds... 
               </div>
               <div class="col-12 mt-md25 mt20 f-20 f-md-22 w500 text-center lh150 black-clr">
                  Anyone can easily create &amp; sell traffic generating websites for dentists, attorney, affiliates,<br class="d-none d-md-block"> coaches, SAAS, retail stores, book shops, gyms, spas, restaurants, &amp; 100+ other niches...
               </div>
            </div>
            <div class="row d-flex align-items-center flex-wrap mt20 mt-md40">
               <div class="col-md-7 col-12 min-md-video-width-left">
                  <div class="col-12 responsive-video">
                     <iframe src="https://trendio.dotcompal.com/video/embed/2p8pv1cndk" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                        box-shadow: none !important;" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                  </div>
               </div>
               <div class="col-md-5 col-12 mt20 mt-md0 min-md-video-width-right">
                  <div class="key-features-bg1">
                     <ul class="list-head1 pl0 m0 f-16 lh150 w500 black-clr">
                        <li>Works Seamlessly in Any Niche or Topic - <span class="w600">Just One Keyword</span> </li>
                        <li>All of That Without Content Creation, Editing, Camera, or Tech Hassles Ever </li>
                        <li>Drive Tons of FREE Viral Traffic Using the POWER Of Trendy Content </li>
                        <li>Grab More Authority, Engagement, &amp; Leads on your Business Website </li>
                        <li>Monetize Other's Content Legally with Banner Ads, Affiliate Offers &amp; AdSense </li>
                        <li class="w600">FREE COMMERCIAL LICENSE INCLUDED TO BUILD AN INCREDIBLE INCOME </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="tr-all">
         <picture>
            <source media="(min-width:767px)" srcset="assets/images/trendio.webp" class="img-fluid d-block mx-auto" style="width:100%">
            <source media="(min-width:320px)" srcset="assets/images/trendio-mview.webp" class="img-fluid d-block mx-auto">
            <img src="assets/images/trendio.webp" alt="Steps" class="img-fluid d-block mx-auto" style="width:100%">
         </picture>
        
      </div>
      <!------Trendio Section Ends------>
   
      <!-------Exclusive Bonus----------->
      <div class="exclusive-bonus">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                  <div class="white-clr f-24 f-md-36 lh140 w600">
                     Exclusive Bonus #3 : AppZilo
                  </div>
                  <div class="f-18 f-md-20 w500 mt20 white-clr">
                     20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
                  </div>
               </div>
            </div>
         </div>
      </div>

      <div class="header-section-appzilo">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-md-left text-center">
                  <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 650.9 161.1" style="enable-background:new 0 0 650.9 161.1; max-height:50px;" xml:space="preserve">
                     <style type="text/css">
                        .st0aa{fill:#FFFFFF;}
                        .st1aa{fill-rule:evenodd;clip-rule:evenodd;fill:#2089EA;}
                        .st2ch{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_1_);}
                        .st3aa{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_00000001627455377407541100000004222602826157794230_);}
                     </style>
                     <g>
                        <path class="st0aa" d="M221,39.2c4.4-2.4,9.6-3.6,15.5-3.6c6.9,0,13.2,1.8,18.9,5.3c5.6,3.5,10.1,8.5,13.4,15.1
                           c3.3,6.5,4.9,14.1,4.9,22.8c0,8.6-1.6,16.3-4.9,22.9c-3.3,6.6-7.7,11.7-13.3,15.2c-5.6,3.6-11.9,5.4-18.9,5.4
                           c-5.8,0-11-1.2-15.5-3.6c-4.5-2.4-8-5.5-10.5-9.4l0,51.9l-25.8,0l0-124.3l25.8,0l0,11.9C213.1,44.7,216.5,41.6,221,39.2z M242,63.5
                           c-3.6-3.7-8-5.5-13.2-5.5c-5.1,0-9.5,1.9-13,5.6c-3.6,3.7-5.4,8.8-5.4,15.2c0,6.4,1.8,11.5,5.4,15.2c3.6,3.7,7.9,5.6,13,5.6
                           s9.5-1.9,13.1-5.7c3.6-3.8,5.4-8.9,5.4-15.3C247.4,72.2,245.6,67.2,242,63.5z"></path>
                        <path class="st0aa" d="M323.4,39.2c4.4-2.4,9.6-3.6,15.5-3.6c6.9,0,13.2,1.8,18.9,5.3c5.6,3.5,10.1,8.5,13.4,15.1
                           c3.3,6.5,4.9,14.1,4.9,22.8c0,8.6-1.6,16.3-4.9,22.9c-3.3,6.6-7.7,11.7-13.3,15.2c-5.6,3.6-11.9,5.4-18.9,5.4
                           c-5.8,0-11-1.2-15.5-3.6c-4.5-2.4-8-5.5-10.5-9.4l0,51.9l-25.8,0l0-124.3l25.8,0l0,11.9C315.5,44.7,319,41.6,323.4,39.2z
                           M344.4,63.5c-3.6-3.7-8-5.5-13.2-5.5c-5.1,0-9.5,1.9-13,5.6c-3.6,3.7-5.4,8.8-5.4,15.2c0,6.4,1.8,11.5,5.4,15.2
                           c3.6,3.7,7.9,5.6,13,5.6c5.1,0,9.5-1.9,13.1-5.7c3.6-3.8,5.4-8.9,5.4-15.3C349.8,72.2,348,67.2,344.4,63.5z"></path>
                        <path class="st0aa" d="M417.1,99.8l45.6,0l0,21.1l-74.8,0l0-19.6l45.2-65.2l-45.3,0l0-21.1l74.8,0l0,19.6L417.1,99.8z"></path>
                        <path class="st0aa" d="M481.4,24c-2.9-2.7-4.3-6-4.3-9.9c0-4,1.4-7.4,4.3-10c2.9-2.7,6.6-4,11.1-4c4.4,0,8.1,1.3,10.9,4
                           c2.9,2.7,4.3,6,4.3,10c0,3.9-1.4,7.2-4.3,9.9c-2.9,2.7-6.5,4-10.9,4C488,28,484.3,26.6,481.4,24z M505.3,36.7l0,84.2l-25.8,0
                           l0-84.2L505.3,36.7z"></path>
                        <path class="st0aa" d="M549.8,9.3l0,111.6l-25.8,0l0-111.6L549.8,9.3z"></path>
                        <path class="st0aa" d="M584.6,116.8c-6.6-3.5-11.8-8.5-15.5-15.1c-3.8-6.5-5.7-14.2-5.7-22.9c0-8.6,1.9-16.3,5.7-22.9
                           c3.8-6.6,9-11.6,15.7-15.2c6.6-3.5,14.1-5.3,22.3-5.3c8.2,0,15.7,1.8,22.3,5.3c6.6,3.5,11.9,8.6,15.7,15.2
                           c3.8,6.6,5.7,14.2,5.7,22.9c0,8.6-1.9,16.3-5.8,22.9c-3.9,6.6-9.2,11.6-15.8,15.2c-6.7,3.5-14.2,5.3-22.4,5.3
                           C598.6,122.1,591.2,120.3,584.6,116.8z M619.4,94.3c3.5-3.6,5.2-8.8,5.2-15.5c0-6.7-1.7-11.9-5.1-15.5c-3.4-3.6-7.5-5.4-12.4-5.4
                           c-5,0-9.2,1.8-12.5,5.4c-3.3,3.6-5,8.8-5,15.6c0,6.7,1.6,11.9,4.9,15.5c3.3,3.6,7.4,5.4,12.3,5.4C611.8,99.8,616,98,619.4,94.3z"></path>
                     </g>
                     <g>
                        <path class="st2ch" d="M94.8,75.9l-4.6,9.3l-13.3,26.9l-17.1,34.6c-4.4,8.9-12.5,14.4-21.4,14.4H0l41.5-83.6l20-40.3
                           c4.4-0.2,8.8,1.4,12.8,4.6c3.7,2.9,7,7.1,9.7,12.3l0.8,1.7c0,0.1,0.1,0.1,0.1,0.2l3.1,6.2L94.8,75.9z"></path>
                        <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="28.8665" y1="152.9736" x2="86.1242" y2="43.0589">
                           <stop offset="0.5251" style="stop-color:#2089EA"></stop>
                           <stop offset="0.6138" style="stop-color:#1D7ADE"></stop>
                           <stop offset="0.7855" style="stop-color:#1751C0"></stop>
                           <stop offset="1" style="stop-color:#0D1793"></stop>
                        </linearGradient>
                        <path class="st2ch" d="M94.8,75.9l-4.6,9.3l-13.3,26.9l-17.1,34.6c-4.4,8.9-12.5,14.4-21.4,14.4H0l41.5-83.6l20-40.3
                           c4.4-0.2,8.8,1.4,12.8,4.6c3.7,2.9,7,7.1,9.7,12.3l0.8,1.7c0,0.1,0.1,0.1,0.1,0.2l3.1,6.2L94.8,75.9z"></path>
                        <linearGradient id="SVGID_00000110437950689616847500000012451418664872649149_" gradientUnits="userSpaceOnUse" x1="116.6752" y1="141.5592" x2="116.6752" y2="0">
                           <stop offset="0.3237" style="stop-color:#F4833F"></stop>
                           <stop offset="0.6981" style="stop-color:#F39242"></stop>
                           <stop offset="1" style="stop-color:#F2A246"></stop>
                        </linearGradient>
                        <path style="fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_00000110437950689616847500000012451418664872649149_);" d="
                           M171.9,141.6h-28.8c-10.9,0-20.9-6.8-26.4-17.7L90.8,71.4l-6-12.2l-2.7-5.4c0-0.1,0-0.1-0.1-0.2c-0.2-0.5-0.5-1-0.7-1.5
                           c-2.3-4.5-5.3-8.2-8.5-10.8c-3.5-2.8-7.4-4.3-11.3-4.1l6.2-12.5l5.9-11.8l1.7-3.4c0,0,0,0,0,0c2.7-4.6,6.5-7.9,11.1-9.1
                           c8.2-2.1,16.6,3.3,21.8,12.8c0,0,0,0,0,0l1.4,2.8c0,0,0,0,0,0l4.3,8.6L171.9,141.6z"></path>
                     </g>
                  </svg>
               </div>
               
               <div class="col-12 text-center mt20 mt-md50">
                  <div class="pre-heading-appzilo f-md-22 f-16 w500 lh140 white-clr">
                  World's #1 &amp; Fastest 1-Click iOS/Android Mobile App Builder At A Low One Time Price!
                  </div>
               </div>
               <div class="col-12 mt-md30 mt20 f-md-38 f-28 w600 text-center white-clr lh140">
               Brand New 1-Click Cloud App LETS YOU CONVERT YOUR EXISTING WEBSITE INTO AN iOS &amp; Android APP + UNLIMITED MOBILE APPS FOR IOS &amp; ANDROID FROM <u class="stats-headline1">SCRATCH</u>
               </div>
               <div class="col-12 mt-md30 mt20 text-center">
                  <div class="post-heading f-16 f-md-24 w500 text-center lh140 white-clr text-capitalize">
                     Create &amp; Sell Mobile Apps for BIG Profits to Affiliates, Coaches, Attorney, Dentists,<br class="d-none d-md-block"> Gyms, &amp; 10,000+ Other Niches | No Coding or Prior Experience Needed...
                  </div>
               </div>
               <div class="col-12 mt20 mt-md20">
                  <div class="row">
                     <div class="col-md-6 col-12  mt-md20">
                        <!-- <img src="https://cdn.oppyo.com/launches/appzilo/special/product-box.webp" class="img-fluid d-block mx-auto"> -->
                        <div class="col-12 responsive-video">
                           <iframe src="https://appzilo.dotcompal.com/video/embed/7fu418e5f4" style="width:100%; height:100%" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                        </div> 
                    </div>
                     <div class="col-md-6 col-12 mt20 mt-md0 ">
                        <div class="key-features-bg-appzilo">
                           <ul class="list-head-app pl0 m0 f-18 lh150 w400 white-clr">
                              <li>Create Unlimited iOS &amp; Android Apps </li>
                              <li>Comes With Simple Drag &amp; Drop Editor </li>
                              <li>Sell Limitless Apps to Online &amp; Local Businesses </li>
                              <li>Fastest and bug-free delivery.</li>
                              <li>Turn Any Website Into Fully Fledged Apps in Seconds </li>
                              <li>No App Store &amp; Play Store Approval </li>
                              <li>Comes With Pre-Built Templates </li>
                              <li>No Technical Skills Required </li>
                              <li>Google Ads Monetization </li>
                              <li>100% Newbie Friendly </li>
                              <li>One Time Price and Use Forever </li>
                              <li>Work For ANY Business in ANY Niche </li>
                              <li>Comes With Unlimited Commercial License </li>
                              <li>Ultra-Fast Customer Support </li>
                              <li>$200 Refund If Doesn’t Work for You </li>
                           </ul>
                        </div>
                        
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Header Section End -->
      <div class="gr-1">
      <div class="container-fluid p0">
         <picture>
            <source media="(min-width:768px)" srcset="assets/images/appzilo-ss.webp">
            <source media="(min-width:320px)" srcset="assets/images/appzilo-mview.webp">
            <img src="assets/images/appzilo-ss.webp" alt="Steps" style="width:100%;">
         </picture>
      </div>
   </div>

      <!-- Vocalic Section Start -->
      <div class="exclusive-bonus">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                  <div class="white-clr f-24 f-md-36 lh140 w600">
                     Exclusive Bonus #4 : Vocalic
                  </div>
                  <div class="f-18 f-md-20 w500 mt20 white-clr">
                     20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
                  </div>
               </div>
            </div>
         </div>
      </div>

      <div class="header-section-vc">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row align-items-center">
                     <div class="col-md-12 text-center">
                        <svg xmlns="http://www.w3.org/2000/svg" id="Layer_2" viewBox="0 0 290.61 100" style="max-height:56px;">
                           <defs>
                              <style>
                                 .cls-1x {
                                 fill: #fff;
                                 }
                                 .cls-1x,
                                 .cls-2x {
                                 fill-rule: evenodd;
                                 }
                                 .cls-2x,
                                 .cls-3x {
                                 fill: #00aced;
                                 }
                              </style>
                           </defs>
                           <g id="Layer_1-2">
                              <g>
                                 <g>
                                    <path class="cls-3x" d="M104.76,79.86l-5.11-6.34c-.16-.2-.45-.23-.65-.07-.37,.3-.75,.59-1.14,.87-3.69,2.66-7.98,4.34-12.49,4.9-1.11,.14-2.23,.21-3.35,.21s-2.24-.07-3.35-.21c-4.52-.56-8.8-2.24-12.49-4.9-.39-.28-.77-.57-1.14-.87-.2-.16-.49-.13-.65,.07l-5.11,6.34c-.16,.2-.13,.49,.07,.65,5.24,4.22,11.53,6.88,18.21,7.71,.37,.05,.74,.09,1.11,.12v11.67h6.7v-11.67c.37-.03,.74-.07,1.11-.12,6.68-.83,12.96-3.48,18.21-7.71,.2-.16,.23-.45,.07-.65Z">
                                    </path>
                                    <path class="cls-3x" d="M82.02,74.47c10.57,0,19.13-7.45,19.13-16.65V31.08c0-4.6-2.14-8.76-5.6-11.77-3.46-3.01-8.24-4.88-13.53-4.88-10.57,0-19.13,7.45-19.13,16.65v26.74c0,9.2,8.57,16.65,19.13,16.65Zm-11.14-41.53c0-5.35,4.99-9.69,11.14-9.69,3.08,0,5.86,1.08,7.87,2.84,2.01,1.75,3.26,4.18,3.26,6.85v23.02c0,5.35-4.99,9.69-11.14,9.69s-11.14-4.34-11.14-9.69v-23.02Z">
                                    </path>
                                    <path class="cls-3x" d="M82.02,38.89c1.71,0,3.1-1.39,3.1-3.1s-1.39-3.1-3.1-3.1-3.1,1.39-3.1,3.1,1.39,3.1,3.1,3.1Z">
                                    </path>
                                    <path class="cls-3x" d="M82.02,47.54c1.71,0,3.1-1.39,3.1-3.1s-1.39-3.1-3.1-3.1-3.1,1.39-3.1,3.1,1.39,3.1,3.1,3.1Z">
                                    </path>
                                    <path class="cls-3x" d="M82.02,56.2c1.71,0,3.1-1.39,3.1-3.1s-1.39-3.1-3.1-3.1-3.1,1.39-3.1,3.1,1.39,3.1,3.1,3.1Z">
                                    </path>
                                 </g>
                                 <path class="cls-1x" d="M25.02,58.54L10.61,26.14c-.08-.19-.25-.29-.45-.29H.5c-.18,0-.32,.08-.42,.23-.1,.15-.11,.31-.04,.47l20.92,46.71c.08,.19,.25,.29,.45,.29h8.22c.2,0,.37-.1,.45-.29L60.4,15.13c.07-.16,.06-.32-.03-.47-.1-.15-.24-.23-.42-.23h-9.67c-.2,0-.37,.11-.45,.29L26.07,58.54c-.1,.21-.29,.34-.53,.34-.23,0-.43-.13-.53-.34h0Z">
                                 </path>
                                 <path class="cls-1x" d="M117.96,52c-.13-.79-.2-1.58-.2-2.38s.07-1.6,.2-2.38c1.15-6.95,7.2-12.05,14.24-12.05,3.84,0,7.49,1.51,10.21,4.23l1.1,1.1c.19,.19,.49,.19,.68,0l6.03-6.03c.19-.19,.19-.49,0-.68l-1.1-1.1c-4.5-4.5-10.56-7.01-16.92-7.01-12.06,0-22.27,8.99-23.75,20.97-.12,.98-.18,1.97-.18,2.96s.06,1.98,.18,2.96c1.48,11.97,11.69,20.97,23.75,20.97,6.37,0,12.42-2.51,16.92-7.01l1.1-1.1c.19-.19,.19-.49,0-.68l-6.03-6.03c-.19-.19-.49-.19-.68,0l-1.1,1.1c-2.72,2.72-6.37,4.23-10.21,4.23-7.04,0-13.09-5.1-14.24-12.05h0Z">
                                 </path>
                                 <path class="cls-1x" d="M258.21,52c-.13-.79-.2-1.58-.2-2.38s.07-1.6,.2-2.38c1.15-6.95,7.2-12.05,14.24-12.05,3.84,0,7.49,1.51,10.21,4.23l1.1,1.1c.19,.19,.49,.19,.68,0l6.03-6.03c.19-.19,.19-.49,0-.68l-1.1-1.1c-4.5-4.5-10.56-7.01-16.92-7.01-12.06,0-22.27,8.99-23.75,20.97-.12,.98-.18,1.97-.18,2.96s.06,1.98,.18,2.96c1.48,11.97,11.69,20.97,23.75,20.97,6.37,0,12.42-2.51,16.92-7.01l1.1-1.1c.19-.19,.19-.49,0-.68l-6.03-6.03c-.19-.19-.49-.19-.68,0l-1.1,1.1c-2.72,2.72-6.37,4.23-10.21,4.23-7.04,0-13.09-5.1-14.24-12.05h0Z">
                                 </path>
                                 <path class="cls-1x" d="M174.91,25.87c-11.97,1.48-20.97,11.69-20.97,23.75s8.99,22.27,20.97,23.75c.98,.12,1.97,.18,2.96,.18s1.98-.06,2.96-.18c2.14-.26,4.24-.82,6.23-1.65,.19-.08,.3-.24,.3-.44v-9.77c0-.19-.09-.35-.27-.43-.17-.09-.35-.07-.5,.05-1.86,1.41-4.03,2.35-6.34,2.73-.79,.13-1.58,.2-2.38,.2s-1.6-.07-2.38-.2l-.2-.03c-6.86-1.23-11.85-7.24-11.85-14.21s5.1-13.09,12.05-14.24c.79-.13,1.58-.2,2.38-.2s1.59,.07,2.38,.2l.19,.03c6.87,1.23,11.87,7.24,11.87,14.21v22.7c0,.26,.22,.48,.48,.48h8.53c.26,0,.48-.22,.48-.48v-22.7c0-12.06-8.99-22.27-20.97-23.75-.98-.12-1.97-.18-2.96-.18s-1.98,.06-2.96,.18h0Z">
                                 </path>
                                 <path class="cls-1x" d="M210.57,0c-.33,0-.59,.27-.59,.59V72.96c0,.33,.27,.59,.59,.59h10.5c.33,0,.59-.27,.59-.59V.59c0-.33-.27-.59-.59-.59h-10.5Z">
                                 </path>
                                 <path class="cls-1x" d="M229.84,25.39c-.33,0-.59,.27-.59,.59v46.97c0,.33,.27,.59,.59,.59h10.5c.33,0,.59-.27,.59-.59V25.98c0-.33-.27-.59-.59-.59h-10.5Z">
                                 </path>
                                 <path class="cls-2x" d="M229.84,8.96c-.33,0-.59,.07-.59,.15v11.94c0,.08,.27,.15,.59,.15h10.5c.33,0,.59-.07,.59-.15V9.11c0-.08-.27-.15-.59-.15h-10.5Z">
                                 </path>
                              </g>
                           </g>
                        </svg>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50 text-center">
                  <div class="preheadline f-18 f-md-22 w800 blue-clr lh140">
                     <i><span class="red-clr">Attention:</span> Futuristic A.I. Technology Do All The Work For You!</i>
                  </div>
               </div>
               <div class="col-12 mt-md30 mt20 f-md-50 f-28 w600 text-center white-clr lh140">
                  <span class="w800"> Create Profit-Pulling Videos with Voiceovers   </span><br>
                  <span class="w800 f-30 f-md-55 highlight-heading">In Just 60 Seconds <u>without</u>...</span> <br>
                  Being in Front of Camera, Speaking A Single Word or Hiring Anyone Ever
               </div>
               <div class="col-12 mt-md35 mt20  text-center">
                  <div class="post-heading f-18 f-md-24 w600 text-center lh150 white-clr">
                     Create &amp; Sell Videos &amp; Voiceovers In ANY LANGUAGE For Big Profits To Dentists, Gyms, Spas, Cafes, Retail Stores, Book Shops, Affiliate Marketers, Coaches &amp; 100+ Other Niches… 
                  </div>
               </div>
            </div>
            <div class="row d-flex flex-wrap mt20 mt-md40">
               <div class="col-md-7 col-12 min-md-video-width-left">
                  <div class="col-12 responsive-video border-video">
                     <iframe src="https://vocalic.dotcompal.com/video/embed/g1mdnyzmub" style="width:100%; height:100%" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                  </div>
               </div>
               <div class="col-md-5 col-12 mt20 mt-md0 min-md-video-width-right">
                  <div class="calendar-wrap mb20 mb-md0">
                     <ul class="list-head pl0 m0 f-18 lh150 w400 white-clr">
                        <li>Tap Into Fast-Growing <span class="w700">$200 Billion Video &amp; Voiceover Industry</span></li>
                        <li><span class="w700">Create Stunning Videos In ANY Niche</span> With In-Built Video Creator</li>
                        <li><span class="w700">Create &amp; Publish YouTube Shorts</span> For Tons Of FREE Traffic</li>
                        <li><span class="w700">Make Passive Affiliate Commissions</span> Creating Product Review Videos</li>
                        <li>Real Human-Like Voiceover In <span class="w700">150+ Unique Voices And 30+ Languages</span></li>
                        <li>Convert Any Text Script Into A <span class="w700">Whiteboard &amp; Explainer Videos</span></li>
                        <li><span class="w700">Create Videos Using Just A Keyword</span> With Ready-To-Use Stock Images</li>
                        <li><span class="w700">Boost Engagement &amp; Sales </span> Using Videos</li>
                        <li><span class="w700">Add Background Music</span> To Your Videos.</li>
                        <li><span class="w700">Built-In Content Spinner</span> - Make Unique Scripts</li>
                        <li><span class="w700">Store Media Files </span> With Integrated MyDrive</li>
                        <li><span class="w700">100% Newbie Friendly </span> - No Tech Hassles Ever</li>
                        <li><span class="w700">Commercial License Included</span> To Build On Incredible Income Helping Clients</li>
                        <li>FREE Training To<span class="w700"> Find Tons Of Clients</span></li>                        
                     </ul>
                  </div>
                  <div class="d-md-none">
                     <div class="f-18 f-md-26 w800 lh150 text-center white-clr">
                        FREE Commercial License Included for A Limited Time!
                     </div>
                     <div class="f-16 f-md-18 lh150 w500 text-center mt10 white-clr">
                        Use Coupon Code <span class="w800 blue-gradient">"VOCALIC"</span> for an Extra <span class="w800 blue-gradient">10% Discount</span> 
                     </div>
                     <div class="row">
                        <div class="col-md-10 mx-auto col-12 mt20 mt-md20 text-center">
                           <a href="#buynow" class="cta-link-btn d-block px0">Get Instant Access To Vocalic</a>
                        </div>
                     </div>
                     <div class="col-12 d-flex align-items-center justify-content-center flex-wrap mt20 mt-md30">
                        <img src="https://cdn.oppyo.com/launches/viddeyo/special/compaitable-with1.webp" class="img-responsive mx-xs-center md-img-right" alt="visa">
                        <div class="d-md-block d-none visible-md px-md30"><img src="https://cdn.oppyo.com/launches/viddeyo/special/v-line.webp" class="img-fluid" alt="line">
                        </div>
                        <img src="https://cdn.oppyo.com/launches/viddeyo/special/days-gurantee1.webp" class="img-responsive mx-xs-auto mt15 mt-md0" alt="30 days">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <div class="gr-1">
         <div class="container-fluid p0">
            <picture>
               <source media="(min-width:768px)" srcset="assets/images/vocalic1.webp">
               <source media="(min-width:320px)" srcset="assets/images/vocalic1-mview.webp" style="width:100%" class="vidvee-mview">
               <img src="assets/images/vocalic1.webp" alt="Vidvee Steps" class="img-fluid" style="width: 100%;">
            </picture>
            <picture>
               <source media="(min-width:768px)" srcset="assets/images/vocalic2.webp">
               <source media="(min-width:320px)" srcset="assets/images/vocalic2-mview.webp">
               <img src="assets/images/vocalic2.webp" alt="Flowers" style="width:100%;">
            </picture>
         </div>
      </div>
      <!-- Vocalic Section ENds -->
    

      <!-- Bonus Section Header Start -->
      <div class="bonus-header">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-10 mx-auto heading-bg text-center">
                  <div class="f-24 f-md-36 lh140 w700"> When You Purchase MailGPT, You Also Get <br class="hidden-xs"> Instant Access To These 20 Exclusive Bonuses</div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus Section Header End -->

    
      <!-- Bonus #1 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 1</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus1.webp" class="img-fluid mx-auto d-block" alt="Bonus1">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        WP Email Timer Plus
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                       <b> <li>WP Email Timer Plus is a plugin that allows you to create beautiful countdown timers even INSIDE your emails!</b></li>
                       <li> This will help to increase conversions, sales and also clickthrough rate inside your emails because the moment someone opens your email, they immediately see the timer ticking to zero and urging them to take action right away.</li>
                       <li> Other than email, you will have the option to add the countdown timer to your blogs/websites as a widget.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #1 Section End -->

      <!-- Bonus #2 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 2</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30">
                  <div class="row">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="assets/images/bonus2.webp" class="img-fluid mx-auto d-block" alt="Bonus2">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md0 text-capitalize">
                        Traffic Beast
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">Whether you have a personal blog, business website, or are making money through online advertising, today's currency of success relies, almost exclusively, on the science of cultivating more significant traffic to your website. </span> </li>
                           <li>The traffic that you bring to your website is crucial because it helps you increase your rankings on the various search engines, which is how potential customers can find your company. </li>
                           <li>Unfortunately bringing more traffic to your site these days can be a challenge. With millions of competing websites, it can be difficult for potential customers to find your site.</li>
                           <li>The five powerful techniques outlined in this guide are geared toward a single purpose; helping you drive more traffic to your website.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #2 Section End -->

      <!-- Bonus #3 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 3</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus3.webp" class="img-fluid mx-auto d-block " alt="Bonus3">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Email Marketing Success 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">Email marketing for business isn’t a new concept, and it has been proven to be one of the best marketing tactics for return on investment. </span></li>
                           <li>With more than 205 billion emails being sent and received every day if your business isn't taking advantage of this powerful and massive marketing channel, then you are missing out on a highly effective way to reach your target audience. </li>
                           <li>Creating a successful email marketing campaign isn’t difficult, but it does require you to do more than just send out an occasional newsletter. </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #3 Section End -->

      <!-- Bonus #4 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 4</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="assets/images/bonus4.webp" class="img-fluid mx-auto d-block " alt="Bonus4">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Email Monetizer 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">Turning your email list into a passive income money maker isn’t as difficult, or time consuming as you may think.</span> </li>
                           <li>Every day, thousands of online marketers are transforming their mailing lists into powerful cash funnels, and quite often, they don’t even have their own product line!</li>
                           <li>This special report will make it easy for you to start making money with your subscriber base even if you’re just starting out.</li>
                           <li>It will show you how you can join the ranks of successful list builders quickly and easily, while increasing engagement, building your tribe and positioning yourself as a thought leader in your market.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #4 End -->

      <!-- Bonus #5 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 5</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus5.webp" class="img-fluid mx-auto d-block " alt="Bonus5">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Modern Email Marketing and Segmentation 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">        
                           <li class="w600">Failure in e-marketing comes in many different forms because people try many different things.</li>
                           <li>Social medial marketing faces many challenges because of the evolving algorithms of platforms like Facebook. It’s getting worse and worse with each passing year.</li>
                           <li>E-mail marketing is hands down the most powerful and effective form of online marketing.</li>
                           <li class="w600">This is a step-by-step guide to start earning REAL list marketing money with modern email marketing and segmentation techniques.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #5 End -->

      <!-- CTA Button Section Start -->
      <div class="cta-section-new">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 blue-gradient">"PRANSHUGPT"</span> for an Additional <span class="w700 blue-gradient">$5 Discount</span> on Commercial Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                     <span class="text-center">Grab MailGPT + My 20 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                        <span class="f-14 f-md-18 w500 ">Days</span>
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                        <span class="f-14 f-md-18 w500 ">Hours</span>
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                        <span class="f-14 f-md-18 w500 ">Mins</span>
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald w700">00</span>
                        <span class="f-14 f-md-18 w500 ">Sec</span>
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->
    <!-- Bonus #6 Start -->
    <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 6</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="assets/images/bonus6.webp" class="img-fluid mx-auto d-block " alt="Bonus6">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Market Storm Magazines 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">This is a collection of Internet Marketing Magazines with 380+ pages of quality content!</span></li>
                           <li>You can start your own monthly or annual magazine program and make 100% passive income. </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #6 End -->

      <!-- Bonus #7 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 7</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus7.webp" class="img-fluid mx-auto d-block " alt="Bonus7">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Find Your Niche 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">Owning a business has many advantages from being able to set your own hours to have the control to sell what you want.  </span></li>
                           <li>Unfortunately, too many new business owners fail within their first year. </li>
                           <li>While it isn't for lack of effort, those looking to start a new online business fail to complete the crucial first step; they fail to research to find a viable and profitable niche.</li>
                           <li>This comprehensive guide covers everything you need to know for finding your niche so you can stand out and create success faster.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #7 End -->

      <!-- Bonus #8 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 8</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus8.webp" class="img-fluid mx-auto d-block " alt="Bonus8">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        WP Email Countdown Plugin 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">With this plugin you can create unlimited email countdown optin pages. It works in Wordpress and any WP theme.</span> </li>
                           <li>Collect leads with your countdown page using only the HTML for any auto-responder service. Paste auto-responder code and it will automatically connect to your page.</li>
                           <li>Countdown to any date with a live text countdown that will redirect to any URL after and on the date that you choose. </li>
                           <li>Use the wordpress meta options panel to have complete control over your email countdown page. Edit a variety of options, including your logo or banner image.</li>     
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #8 End -->

      <!-- Bonus #9 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 9</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus9.webp" class="img-fluid mx-auto d-block " alt="Bonus9">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Email List Management Secrets 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">List maintenance is one of the most important subjects in online marketing. Your list is your number one and most basic bottom level output for your promotions.</span> </li>
                           <li>It’s expensive and time consuming to gather, but forms one of the most powerful resources and profit potential you have.</li>
                           <li>Depending on your business, there are several solutions that might be right for you. With this ebook you will learn the big five solutions to allow you to decide which one is going to make you the most cash.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #9 End -->

      <!-- Bonus #10 start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 10</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus10.webp" class="img-fluid mx-auto d-block " alt="Bonus10">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Boost Your Productivity 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">We need to be more proficient in our work to accomplish more. </span></li>
                           <li>In many cases, people fail to be productive because they lose focus and let their minds wander, leading to a loss in productivity. </li>
                           <li>This quick guide will reveal you basic ingredients of productivity and tehniques how to better manage your time.</li>
                           <li>This product contains all the features for building your list: List Building Report, 'Mobile Responsive' Minisite, Confirmation + Thank You Page etc..</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #10 End -->


      <!-- CTA Button Section Start -->
      <div class="cta-section-new">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 blue-gradient">"PRANSHUGPT"</span> for an Additional <span class="w700 blue-gradient">$5 Discount</span> on Commercial Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                     <span class="text-center">Grab MailGPT + My 20 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr"><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 smmltd">Days</span> </div><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">0300</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div><div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div></div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->
   <!-- Bonus #11 start -->
   <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 11</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus11.webp" class="img-fluid mx-auto d-block " alt="Bonus11">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        OptiRoi
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>OptiROI will help you maximize profits regardless of what niche you're in!</li>
                           <li>You can also use this technology to build bigger email lists, which equates to much more future revenue! If you want to outsmart and dominate your competition in today's crowded and highly-competitive landscape, then you need to be proactive with your marketing. </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #11 End -->

      <!-- Bonus #12 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 12</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus12.webp" class="img-fluid mx-auto d-block " alt="Bonus12">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        How to Keep Your Email Subscribers 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">Learn How to Keep Your Email Subscribers! </span></li>
                           <li>Indeed, the money is in the list. That's why you decided to build your own email list but as you go along, building a list is not just your task that you have to take care of.</li>
                           <li>There is the concern of how to make your list conversion increase and most of all how to keep your list intact or at least you have low number of attrition. </li>
                           <li>Well, if you will look to other business model, attrition is normal but if you will handle your list quite well, you can decrease it numbers and make more money from it.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #12 End -->

      <!-- Bonus #13 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 13</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus13.webp" class="img-fluid mx-auto d-block " alt="Bonus13">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Double Your Email Conversions 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li class="w600">Learn How to Double Your Email Conversions!</li>
                        <li>The money is in the list. You may already have heard this from many successful online entrepreneurs.</li>
                        <li>If you have been building your email list, your next challenge is how you will be able to make your email conversions higher.</li>
                        <li>Fortunately, inside this product is a podcast that you will give you the proven system that will guide you how to increase your email conversion rate and eventually make sales.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #13 End -->

      <!-- Bonus #14 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 14</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus14.webp" class="img-fluid mx-auto d-block " alt="Bonus14">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                           Email Marketing Basics Video Course 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">Discover How to Set Up Your Email Autoresponder withGetResponse So That You Can Grow an Email List That Gets Clicks and Converts into Sales…Starting Today!</span> </li>
                           <li>This video course will take you behind the scenes to help you understand how to build a relationship with your list… </li>
                           <li>It will show you how to plan out your email series, but also how to take the series and set them up on GetResponse.com. </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #14 End -->

      <!-- Bonus #15 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 15</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus15.webp" class="img-fluid mx-auto d-block " alt="Bonus15">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Xyber Email Assistant Software 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li class="w600">Do Your Customer Support with Ease Using Xyber Email Assistant! </li>
                           <li>If you are a current online business, customer support is necessary. This is because you can't be so sure that your business will work perfectly!</li>
                           <li>The good news though is that, you can now turbo-charge the growth of your business, while freeing Up Your Time With Reduced Customer Support Hassles! Stay On Top Of Your Business With The Ability To Instantly Respond To Email For More Satisfied Customers, Affiliates, and Partners!</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #15 End -->
      <!-- CTA Button Section Start -->
      <div class="cta-section-new">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 blue-gradient">"PRANSHUGPT"</span> for an Additional <span class="w700 blue-gradient">$5 Discount</span> on Commercial Licence
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                     <span class="text-center">Grab MailGPT + My 20 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <span class="f-14 f-md-18 smmltd">Days</span>
                      </div>
                      <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        
                        <span class="f-14 f-md-18 w500 smmltd">Hours</span>
                      </div>
                      <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        
                        <span class="f-14 f-md-18 w500 smmltd">Mins</span>
                      </div>
                      <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        
                        <span class="f-14 f-md-18 w500 smmltd">Sec</span> 
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->

         <!-- Bonus #16 Start -->
         <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 16</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus16.webp" class="img-fluid mx-auto d-block " alt="Bonus16">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Make Your Subject Lines Standout 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">Learn the Techniques to Make Your Email Subject Line Stand Out Multi Media!</span></li>
                           <li>The money is in the list. And if you are building your email list today, the next question is that, are your email series get opens?</li>
                           <li>Your subject line will certainly stand out, and your email will be opened if you make your email unique, useful to the reader, and focused on what the reader either needs to know or wants to know.</li>
                           <li>Maximize your email marketing efforts by simply having the highest results that you haven't experience before.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #16 End -->

      <!-- Bonus #17 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 17</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus17.webp" class="img-fluid mx-auto d-block " alt="Bonus17">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Viral List Autopilot 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>Finally, Discover How to Build a Highly Profitable List By Using This Untapped Viral Strategy!  </li>
                           <li>Starting Today! This video course will take you behind the scenes to help you understand how to build a higher converting list by leveraging other people’s lists!</li>
                           <li>Indeed, the money is in the list and if you are not implementing into your blog or in your business, you are missing a lot of potential customers.</li>
                           <li>Inside this product is a serious of video tutorial on how to build an email list on autopilot using the power of social media networking sites.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #17 End -->

      <!-- Bonus #18 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 18</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus18.webp" class="img-fluid mx-auto d-block " alt="Bonus18">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Rapid Lead Magnets 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li><span class="w600">How to create quick & easy 'Lead Magnet' funnels you can use to build targeted lists and attract buyers!</span></li>
                        <li>Lead Magnets are basically things that you'll give away for free in exchange for an email address so that you can follow up with a visitor or subscriber and ultimately get them to build a relationship with you and build rapport. In that way, you will be able to sell them your front and offers. </li>
                        <li>As long as everything is congruent and related to each other, you should have very high conversion. Think quality oer the amounts of the lead magnets that you have.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #18 End -->

      <!-- Bonus #19 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 19</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus19.webp" class="img-fluid mx-auto d-block " alt="Bonus19">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        CPA Email Marketing 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>
                              <span class="w600">Learn From This Audio, Give It Away To Build Your Email List & Sell The Whole Product With MRR</span> 
                           </li>
                           <li>One of the ways you can organize and automate your CPA network offers is through automatic email campaigns. If you already have a list of subscribers to some websites or blogs you own, you already have a means to do an email campaign with CPA offers. This approach also lends itself well to doing a “hands off” system approach that can work behind the scenes to generate cash, even when you are asleep.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #19 End -->

      <!-- Bonus #20 start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 20</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus20.webp" class="img-fluid mx-auto d-block " alt="Bonus20">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Email Marketing Expert 
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li class="w600">Doing business is more than a full-time job.</li>
                           <li>Your days are spent selling and procuring products, ensuring customer satisfaction and when you are home, you have to work on new products, ideas to improve your service, track finances and do the research to grow your business.</li>
                           <li>This leaves little or no time to learn new things. </li>
                           <li>This course has everything you need to know to boost your online reputation and GET HUNDREDS OF PEOPLE SINGING UP TO YOUR LISTS EVERY WEEK.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #20 End -->


      <!-- Huge Woth Section Start -->
      <div class="huge-area mt30 mt-md10">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="f-md-65 f-40 lh120 w700 white-clr">That's Huge Worth of</div>
                  <br>
                  <div class="f-md-60 f-40 lh120 w800 blue-gradient">$3300!</div>
               </div>
            </div>
         </div>
      </div>
      <!-- Huge Worth Section End -->

      <!-- text Area Start -->
      <div class="white-section pb0">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="f-md-36 f-25 lh140 w500">So what are you waiting for? You have a great opportunity <br class="hidden-xs"> ahead + <span class="w700">My 20 Bonus Products</span> are making it a <br class="hidden-xs"> <span class="w700">completely NO Brainer!!</span></div>
               </div>
            </div>
         </div>
      </div>
      <!-- text Area End -->

      <!-- CTA Button Section Start -->
      <div class="cta-btn-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-16 f-md-20 lh150 w500 text-center black-clr">
                  Use Coupon Code <span class="w700 blue-clr">"PRANSHUGPT"</span> for an Additional <span class="w700 blue-clr">$5 Discount</span> on Commercial Licence
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn px-md80">
                     Grab MailGPT + My 20 Exclusive Bonuses
                  </a>
               </div>

               
               <div class="col-12 mt15 mt-md20" align="center">
                  <h3 class="f-md-28 f-20 w500 text-center black">My Exclusive Bonuses Are Expiring in...</h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 col-md-10 mx-auto col-12 text-center">
                  <div class="countdown counter-black">
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                     <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                     <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->

      <!-- Footer Section Start -->
      <div class="footer-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 620.04 134.95" style="max-height:40px;"><defs><style>.cls-1{fill:none;}.cls-2{clip-path:url(#clip-path);}.cls-3{clip-path:url(#clip-path-2);}.cls-4{clip-path:url(#clip-path-3);}.cls-5{clip-path:url(#clip-path-4);}.cls-6{fill:#fff;}</style><clipPath id="clip-path"><path class="cls-1" d="M40.14,36.57l13,12.24v77.25a6.51,6.51,0,0,1-6.51,6.52h0a6.52,6.52,0,0,1-6.52-6.52Z"></path></clipPath><clipPath id="clip-path-2"><path class="cls-1" d="M170.36,36.57l-13,12.24v77.25a6.52,6.52,0,0,0,6.52,6.52h0a6.51,6.51,0,0,0,6.51-6.52Z"></path></clipPath><clipPath id="clip-path-3"><path class="cls-1" d="M169,15,108,72.83a4,4,0,0,1-5.49,0L41.59,15A6.51,6.51,0,0,1,46.07,3.73h0a6.56,6.56,0,0,1,4.49,1.78l50.57,48a6,6,0,0,0,8.33,0L160,5.52a6.5,6.5,0,0,1,4.48-1.79h0A6.51,6.51,0,0,1,169,15Z"></path></clipPath><clipPath id="clip-path-4"><path class="cls-1" d="M101.27,81.56,35,19A9.6,9.6,0,0,0,18.8,26v99.87a6.52,6.52,0,0,0,6.51,6.52h0a6.52,6.52,0,0,0,6.52-6.52V34L95.92,94.44l5.35,5a6.06,6.06,0,0,0,8.31,0l5.34-5L179,34v91.91a6.52,6.52,0,0,0,6.52,6.52h0a6.52,6.52,0,0,0,6.51-6.52V26a9.6,9.6,0,0,0-16.19-7L109.58,81.56A6.06,6.06,0,0,1,101.27,81.56Z"></path></clipPath></defs><title>MailGPT White Logo</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><g class="cls-2"><image width="90" height="99" transform="translate(-0.75 36.16)" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFoAAABjCAYAAAAb3RdnAAAACXBIWXMAAAsSAAALEgHS3X78AAAS0klEQVR4Xu2cTahtyVXHf2ufeyOKiNLddhJbE6XVNtF8dqskDsSRM0eRiIKoiIgfAyeKIgTEiYIDJQMRBBUxmpEzBw4caBvtl26bjtEmrbZGOunOaxQRUfuevRysj1pVZ597zv0495wHFrx7a9de9bH/9a//WlV7vyuqyimnp773LxTgmT/6oOyyPeUkpwz0kw5yJOHBBXzaZXDUJOUfoMCTH/pzfWqYgAchnSyjn/ywg1mH1+UVEXlgGH6yjNaArzC6z0syfKx7iukkGf3+73tag76yldE1f/rsPttlcJQk/kNBC9IdihXowu5TBfzkGP3+7396izY/2Aw/KY1+3w88vUxfyR+oFExH/c7y09PvkwIaFoCseXFkpTjLajfyV+RkwsGTkY73/uBfmhyHTOgumdDML2r3+Fhq83QsOTkdoH/oE4oWgBXQfXS5qI2yCHB/fRz9Pgmg3/Mjn9AGsDrIztR5B3MzPwA+2i0Afu/j33FnYJ+ERuskMIFOltdJYOU3RdLpLepy5lvBVv0eHOZd6vfRGf3uH/srDQaj2tg4MhyQmevJyWgzXt+BnBwV6Hf9+F9v6jIgRT5MMtq1zF75KoBrIfORAD+qdOgETAKTuGzgEiEpJ0x+7RISNiEpl8bVJRzMNEI4yIlymPj7aIz+lp98xmgaTE0Gqz17ve7YbmXMNDtvZyu7q52nnRHKLbP7aEB/8089owjIPIANdHICPeCArLWBotcHfC/9RhFuDvhRgH7nz9yzcK6CPI/gag9i2NHqgNtH3bDdBfgS2KPNeH1Dhh9Fo3XlmrxqWqyTtPLQ10lMg0N7U6/9OjR6anqdoV7ci07zXuStYCMU3HZ9Q/2+c0Z/088+q53uqjJdaEMkmLqN4dVuDAdjhUDR4F3sbheHZPedA/3Ezz/bdoFzD7jFyQXkuisswLU6druGg7WO3Wv1LgU8r7W3Ge2uCfidAv2Nv/hcp80ZQSgN5LwewFN6vV4CfG2ZbLfY763ftLq7JyXyuwG/M6C/4SN/o8wFyABjbvmR3R07twE+hoMUhkd/tY0CdrYVaQAvyrbajNeXAH5nztCcHebwwulNAisv9zOO2MSkc4yzjqnUrY6vOsy64REBb5faxoRfizvXMsguL1m21Wa8FkFVWTo/uRNGP/7Lz3encyETlcVLDE8nmSwu113eO9rmMH3bXqWqXbthrRd183e7cV05uTugRazzNQPAA2BbpYWi7aXORnTi7WfeijvNp/UDOwCPdvK3tjpLNht5A/zgQH/dr76gAU7d0cm6d2ZL4FXAR7C3Ak5rM4Gbe1soE0ntp9XJtiJp/V3qLdkMeeEOPjfQM/Gdm+mmOEsB++0gqu8uZFbTVQdSFUTFMFEFkYwuVIqd5+NFgQiEuOrK+q8TZO15fdRMvczXuts4mB2i4mOxKsnuaGPIm80BGf32X/9brbFyi4+haXVj+k45mWv+khUx1ifsiqRE/bzn9aj9+kVtJ+zzd7uxVb/1gEC/7aOf1g3HVxgcoVd3f9DvXstpIF9UkHZL0KKcLNhD6Qc6u65etBdJ88ci2Pf+8INyMOnQFaDiS0iRWVDx3woiis4WmsnaHEYu3wTcbctSFwU9x8tHe5oE6Q45EbefrdNQBre0CZCGp6iAaIdvTmBoi1q7Zm9F9z5mUcdBgH7st/5OZYpRagN8htAuA9vYyUSSSBB0FROyAF4CruhsNViBzKSO7wIcceAcTPAJb4NAfVLxJWET209HTkJcLAAe6SBA68ofLNCMhyx5UWO4BfnFSa7LxCjGVmfc+FMm60MVaztWxg7AZXZm5jitD+Y2bo9GSYcpzTmmwyyAS7v0sQuf/IMPxMzcPtBf9TsvKitB12rsnEyHdWoslMnYyiy2m1MHF4xt6xIVVMD1EjmJSV2HDAhKlQRBApgYiwMTkqKTTQIxWVFffMK81SWGJ9h2WS4s3TrQMamcCVwUsBV0FmTyByx5UUFnRSZ7INPtHswR8BauaWPZWm1L7mU1LNyUoIHdeBcraWcsPjWSFsOEpZUNTiG1+ZO/39gMtwz0mz/2GZXzAAA4MxymCxtGAMmMg+xsnNVZDslaMLsZGmOqfhfbAE8i75QbVsEGyLogJ2AvjOeezZtgQ0Bti6mBvZRuLbx79OMvlXDOGBQPEBuUOLtYDMEcuMjLG9rbdXnvYyMcdDuaXe5GO7uFOmU8GXLS22W90n6uKNr1s7/XsxlukdF9OEfu+sDKZFbmM+t/uiAfUEWabdFbPZdkpijOzvinpu+DnDR50LwVUiCzaTaQi79JQK/fVtkttJKhrCRvH5EEWIHnfncTZLgloB/5439QJvHtsw8IX7IOspat83zm+QBFsHg2mSVe1pa96XYD07CIGwyANzlBscn08eAT2U1K5n3yYwy+OnQlyBoi9mbUfmiAb0m3AnSGcxPOzj6vE0xvaHp7AD2zwZp+g0yaDxB6KaNjA1gbE3UiwdCZRf3O0OxSJ7mHw1RFVwyrwn4qII72c7/97YtshlsA+qE/+SdjcwwM2kPSHmw+p7ExdVCZ3XnuIye4DG1IRSz1ueW7DU/G1jYGwm84kD3gCyCHnSiytj7apJUxXJJuDLSuxHtrg0Gl7QIDsAoeYstwLR7DbgLetVlAlIsiTwmSPWzIzYacjEs+cFGTCWurBy5CvE6/EZjczpFVTPqe/81vuxTqGwH9FX/6stJpMjhybYanNvAl/WZtTx4PE/rdb0wc5NkmpANmTZOrmNBhchJw3ImJ+451AGuTk/kZC/Fq/5HP9mk7XHana4d3X/5n/6zRYXhdWYck2KBsMC0fy1ZioGr22w7z26usVmcj5JvpXiJEWbXtj2WH9lxGFsPHHOdCv6X+Cx/91kvZDDdgdDjAcEaxXFVpEcWMU6awrDIumS45+DzM0V5OtNRp/Uq+kO12jPRMzDGI2eQJ4lqtOFad95HOFc2Fqd4NWkCY4IXf2A0yXBPoL3v6s2pLS/O8Ih5MZrVJcJBte10eXCDeuIjC/CacUdaeVMAc0BHw6ng7aVljS18gDog2AC8O0ybdnSbAhY9LvJ0gixZnGW142/umawFtbHZmxKBVzFGJDzRYtNay9XaNrIc645mHKKwlJaA6o/nMnVScspXVEk4ytZbmJA0o2kQHQTwfEYpmfO/tVL/i4xEkz24+9WtPyRaINtKVgf7Se/+q5rWtQ7QM5Mwe2kAjI49YpiLiMlMAEpOA6pTA69IzMpyRTjC5PEUIZ6x3cBRfGRQgaYDXyZ0DOB+7YLG699WFmZXpuonNZenKQHdbVBXswN07F9LZGXu0sK8AHhoZIFLaU1CbAVPJCnLJz6710xpfGW21WOPSzra9/aXln6siHaagZ8CFVesOv9TqCfDpX3nf3myGKwL9Jc+/4uGcswKc1T6QWRdP4Wyn58Ct+okaJQBVmATxnWSycJSAEfDiMBug5Na5O9RXD+/m7Q4zn9P7TTkRB/yKae/w7os/9Tm1GScf0jonw7Pw2pkvodxSCNd/2+H5MTSLeyV0rLbdOMYwr5Z3Y7ykDVUvazay1uIU4cVfeu+V2AxXYbSAvXnwJVp/VudWl5mzLZ0kiklIsAg0AXIW0lhb20TEo4JBAnzZp34HMMH6tfc5VT8AcW6RoV3dCVb9xlahYP2++JH3XBlk2BPoL/r7z6u9n8Pes9EeRucKOSYPnsv8hpPsJSAncJvDVLWyM/EQjg7wcKhpWwEXocpOOslRrsYdZbGRmXSS1017AR3b6HyQCninjaPdwKKinXEKB/5MiJ1pR/y9xWHGcWs96wiHtxihaB+hIOKhYPErCCqNKsYGH5gX6SS89AvvuhabYQ+g3/TSqxoRAJAPqkZjYyvLgC/KSQAuCuth8lQg2sVsk9XiE3eh/ZHnIAGoabnDB8Ach/8+OXpu400mz9oYXfMxlii/QdoJtHbe10EJNgd4vqxZOgwagOxYtKJ4/pggSE1W9RDNy1ThXEpbmru5WPbjJmpJvwWxEC7iZRoRBGnPlLtf+Mefuz6bYQfQ5y+/pogzxssUIQ66bY6b86j6vS0cUyUZY04SP/BrUtADjnU0yIVNmKBnwrRWNmP5kKllwO1FBF2drr/K5okbp63h3dm/vKYGDo0tYeplGRbhdrG81Ma5/GK01Mv27Xox3NPW9vjitwsfs65uhGeN7Qv9e9iXL45rGOjtv/zT77wRm+ESRm+EQ5VNAELPWiDetIA/x0oMcH9dBepEceZjUYl4qR1GBThFgmIZF3YDuTkKprJWNs5OAmCk/74kRuTvOudzYboIGdH80Ofln3jHjUGGLUCvXvmCYqu7geAPuQl4XJPhVMCK5/SsAB511SZqQ7+Fzc/A8nTQ7Jb0W9YeSeBOU2lyQvS5XU5QmM8o4R8+2NtJ2zVa7J+uQeLB6j167QbcSfpDKLkJAK++p8NEGOJlttg1wO0j97DTDOmowEZ783bAdfLXaSif/dHbYTMsAD197r4aK61zVj7W8aUkNDYH4OIPik/DSogvjYi52sNhssYdFW35q133uzho0UsZmwpzxNuzdjJop3U0wKEdHEW7Z3KjzclS2mS0kxLIJSVg/z1htgfbABxBpV5HXfXPDsTLG0Cp3wvMVaEASznyxAsaO+23EJ835DGp+hguyFUQR6LdufIcE2b2Jj3wyg8/cWtshgHo6dX7hkSiZXkFUHcy+IO7Fscg42cvJ5Uxdm3V9gM8GJ6OTkmGt3wDEhFSTuIQ69zAtoGoTXoQJQ//477/voVwbkwZ3smr98NtEIzeyFuFdrF2eINZNCZCgIE5SWhtaWnH7QVcv6Os5TNUyzOQsay0OdSZLjQ1GKXLL4VyKHz+w19/q2yGymjfmKCERLdZjjyk0wFN/aZ8n6GQGxCz0jxUqoAkYwHEGTkeWCW7vT1R8xXESip6qzDuMEXFIol6L3aSsz9bTJ4v3Vc/9PitgwwOtHzhdYdRvL/LAY+HTPRX5JtjYllGlS0OMx2PT0oFfFNOMJDSSdbwjd1yEhudWdBzse+2xSc7HsOJcqhkjB7nUHcALjEmY4GgdsIHJidFv1vzkiUC3ScG0aUBrtl+A1whIgmvo2oT0QAP8PHKkhOEt20OU4tue6zu/b72PYdhM8CZ3A82lxSzjCTLpCunmxytgG+TEyCiBUXdIQn5wXdpSxRcn0hJ6c6wPZ9OconhuN0yw/MEz8fY2HSYJKpKB3aFXUsmwL7ULvApBeXcIYrTyUVk4HWaI/X70VbYE+x2Owc826mObrAZ8+kA3QG//t1fe1CkJwB9+KHWSe1OSkbI1bho59dmUwomLxPJ8woVyL+ptLINR/x9pXzLPrlNhGzezrwS21BMUP/cRNpMQv939CwutyG1vIrX9zEcOmV4lwU72e0XusDwvim3KYWV3XE/bgfj5rjej+GsY7sdNrqxiiJvLC52fu/fvuttB0d6A+i8cR3At9mljRdeIiewEBtTrin14rrGw4rLgduMclKPVmfl37/z8CDDZcekLidy//XmrRS6PAKigc9mdBJgS2DoYZzvMEeHac3Zsm/hnzighqq6oQ3OHdzkwebanawI8X9eNr6E8vbyldUdpa2M7oyuw+56f6gzshsKw4H80DHLo+7+ciIYsze+24iYWuE/PvDVd4b0zneGwBXYDRl7e9E+7AbahgfIv68Rbcdyj82POnjBcC0rQ8kdplVRui9Lxex7khw+7cXoMV2J4WPzC9cjw6te5/1Bh61cM592g37L0v9tVOU/n3zsztgMezJ6TFdiuBSwxuR1DNNBv2cQo59dT+JHmurshTz79j5Svwu74+2OqriUKLla7jBdi9E1bWV3d62Zl9FuyI/shmBxMNNvh16HubIZoahfxDXG8P9691vvHOkbAx3pSnKyUc5GnUXA46XsLrC9rAshFRD473e85c5BhmtKx1K6kpw4Eh3gQx0j4+Aw8wYGcvnAHMFDOEnArUvpAT9SujVGj2mnpGibjw2bBfuR4cnksgHpwj9lw1n+zxNvPgqb4SAvbSxtPT+JaxE0/0VZvd/bm//ywjw/gfiTmUTezznwcxL1M5DDPel+6WCMrmknu8OujmUbwxfYDfSvpSq73fR/H3/0aGyGW9Toy9JW/YZOm+PNeZJ3wWabfhOhoNerH7YfU5sj3Qmjx7QzQrlMv8frkeFK9zJYFN54+1celc1wJKBhD7CdidcFPM5LLr7m+CDDEYGOdKl+A7lR6cq25KFtUgBmWD/2yP8DXdNuhzkAvgXsAFoR5rc+fBIgw9GDnpb04YckQ8II1yKFd5SCabUp+XxtdmLpZBhd05X1e8FmfsvpsBlOiNE1bWV3QifEi1ZdsjkpiC2dJKPHtPHtiQ6/B4bPj54Wm+EBATrSIuAD2HqCIMOJSse21J2fQJOJdJYniTHwgDG6piWHqY88dLJI38lZxyHS4vnJCaf/A7yQ0Ala9tJfAAAAAElFTkSuQmCC"></image></g><g class="cls-3"><image width="90" height="99" transform="translate(121.25 36.16)" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFoAAABjCAYAAAAb3RdnAAAACXBIWXMAAAsSAAALEgHS3X78AAAO2klEQVR4XtWbzet1VRXHv2uf+1MQokDNxExD1MpAyMd8Cfoznhw1bRA0cBRUA2kSURGRVFhkb1aOmjVrEDyE+LxqKiYSRophDaJBFHl3g73X2mutvc85+/7u2/ktuM+5Z5919stnf/d3n3t/96EYI85yPHj+QgSA55/9FM3lHjPorIJ+8PyFqucXFwx7NZewtGAFnzV5nBlF9wJeqqrPhKJbNlHFIvGWWLSie1XsIV/89fJUvUhFnxbwkmNRit4l4KWpOswlHCrYh3cBGQSce+zCbFWHjKNbxy5V3JVzpDiadXQDBvoAqpyozi/9chkWcnBFdwPuxePyopxTfx0HiIMquut5GOgD1AuYgEu/eLSnxr3GQRS9bxWXcnUhv51t80CxV0XvG3BSca1gez3F5Z8fV9V7Ad0NGOiD7AFLWRuy3gxLOeHyzx7paW0vsXPr6PbhnmhgmVLxGOCuydxz7EzR3SruHXTTJvIFKm/r61xOJocn4cpPjqPqrRXdDRjogzwKWF0cAyzX2pCPGadW9EaAgfmBTgEee5qoVKzecJ5WPxEigKs/fniuNzuPUyl6Ix+eG5IHbMpo3ibk2gRgAJFyXXP92VNspOh9qrgLsDufBKzLgs0BAVd/eFhVdyl6I8A93fcqNuc15C7AOocAEDVUncqPEZOKPhpgfb2h0FEVM2Cdm+tIZWTKrv3goZ5e7ySaoDcCDMxD9gBNmQLcUqfP1XnuvpaKQdmffVk+vvD9w8CurGMjyD1dVDmjgPPpqQGjsdkRENEqA8C5BwxR9E4Bu+uTgHNR15OEem+P2S4CZgDnf8iWvfjkJ+dGtHWsjg7Y5Lh8dRi1iQqmva8JGbq+uUHtJgJwBMgakCnLF5QNxPySe4gQm5DJQjb3ljpjSC8AiIEQA/DxLzzfhWCbWHW1MAV5U8BwNkHmhjpHjumNWES+1lKxfwqR3HxuJg4om+UeY/qv4Kozc9cilPK0Kl1urMrInEs96hI/skWC9DipVbXD98t7EsgxtCFHovRhhoD7Hr/YpbnTBsUYce68+9M8YT4YnHqfANc5VZ4HzDkjgOHKW5/0mj6sle8m3j+l8PnL33igZ/QbRwDcDwPHmiH3wgaQzb2qAhlknWN8OPtq8lQFiPS9hMqHVV2Sz3WEsirkfI+/cilVq/FX4cq7bYLzJMYg8csBJiQA3IaqvwWYIUseygcYDTjdT4ABnur66Bcv78VCBPToT6hUaQ9gzhE/ZRCh5E75sK6v+DDMa8wmBHyGGgcgDoS4IgUYRsGpn3zP/lQ9vxmiDzBMDtCyCJMjr4aKGYaDPG0TBN7s+NxYBvGkkZxLP6XedP6RL13ZuaoNaFE1NwpYlSkFSR6cQgH4ZQ5oSPxqA7YQynk34JCAFo/W9+rzUsbnMeR6sPuoFU3lbaVQda09ESqPr5uBYRqw+ChV3tn24RYolUe1ijVkOVebbAzJau554upOeTe/vTv32IVYFEpNwIBXcbloJ4iP7Uc1UJ4sfmSDqpfrzOcVYF8PkUyQXlWVTfB9wU54VBPCk/HaV+4vHdsimh598Ve8MZZBcmcApQwuz35qFAN9bEAmpTj1XGyXdR64sglZ3oS8oVHa8Ia8+TUVTAI1EgDO13XIBkllM93hxjhelbaAzKECzIOCUol5TdgECpAKSgOwUR8/QZjHNc5T5wHytKPPeQwxWBVH1S7n3vW1F3ZiIaOgLz3zKAlgKMBABp87xdfMa3qjqz+VwQCWwYLr8hOpPFXq0+elPukfq5TryPXLIyCviIHVTOAPSbuIycVhH+sggwScTWgAo4CpAMlKlYEwYLDKIDArFROVe3V9uj8tm+D78/l6IMRVkIks/UdRfl4xH/7mi1urehL0Zf65a+4EQBYg9LFPwQUkSn0o4EdtQqmY22t+6OB+OMACmSdXFIui9iHfp67JU9CWMQkagAHMg7JA2yr2S5rLCmBVZwbsNzoBlyGYjU8pH837dF9K3dpiSv+5X3DP0KkwBuDOb/9xK1V3/a7jE5/9Q3nckyNNPK5RVZ4815aXOu1gGU65lwQaUK+I5uOa5GlVUxuwbo/PkRWu7YQIb3z+Y9zLjWJe0QAu/9T9MFAPzh3N3+tkECqPYNQoj1Fyjcyg46DeU7mPAbRsIuZ6zPcr3E5QbQYuJ7VvsH9zLoqdDDh1dIEGoMBZEHpg/IHBlPGA2fc0YBqpl+vIoAxgbRMacEiqFP/lTW9F+UulecBF/SXX3EOE2596Zd4CGtEN+srTj5B4MWDVSlSXqeU+ClgmhQdY7o+5TlahmShWJul7qNQ3FIBlEl2u638MfF++BqjHvqzy/DpNdIMGIAMpj1MEv9kJZKhzoBpsAazuD3pwyEuVChCgrUwqOfyJz0zshIoFXkhtrgfC+oQQT2yunujbnn51Y1VvBPrqjx4mEKpPdUUZkHKjJh6sAG0AZoCmHq1iqm1CWY1MkkBpA5Z7NWC5l4BB9UWVi1efUtUbgQZglqhRsQczBlivgDxo8cbQqGcMsEAsdQAobTrAZjIHbgdlFei6cq5shhp8Pt76zJ82UnXX452P+z/3XNR/5RCYAMQi+DrlzqtzQE0OkO/PA9d5zm7GvLWyJUznljIGWvJlcnUfgmpfCQkEvP2Zu3OL07GxooH8K0zXCQD1gLlz0OdKgZQhg8sB2ejGlKnujwMclHRt0odzOX8YAjgHKadlP7pOHvegBNURpwINwEIm3SmIUsSH1aDBqmC/yyBS3ghgPUEMa0igeHKaNjEGeJUf+/Sml+sy6hfgKH0Y0v0xAPEk4ObfvN5lCacG/cL38g8D/awyENLnMApmsKJEhqIBq6XMgOpNzwIuFsBwFLycux5KvvbcSsXqfQyQ53Ep4yejlQfQjlODBpAH0gYkUFnFMmCtnHJurIbByARZtQEKsNyX7rVfddp+JXXz5FFRa3D5anOUDzxsd0hwdRs3/vbPs6reCvSL332QWoDKORXghOJrDNjDcEoDqybkwfGLrYZKrrcJ7fXI93M7MtmtPghkbicd1yf5dX0oeatynIutQAMNQOTPkYCH3Jk5wA0FJ0XlugkltwG4qdDctix3s+m5PrAguM6VujaUI7fF7bzvd29MqvpUj3c+7nv8YjQ+zBMsXgmBV60AQn6kU/fq/OByfTsjbbCNWNuqc4z1ZAXb8zxRlCdR2ivtluuEf376Q4RGbK1oAHjpW+eoAhV4oDCDaw0QSk06n33QQ7ZqRnuVqG/fROUMx62i5MPB5MvGx+odyL1s2ZotbSS6/vtbTxhVAZV6moDhJsflS72c5+sM7fpFiVWdVFQoeSN9ANJkTSmYqAgk9+U9z/01/uuhD+YaSuxE0QDwytcfIOnk0FCY9uFAonivSL3hlc2oqNvUSY36xT8b+byxBlW3Uvx6FeQLpfVJUbidID8x+Tr3dyR2BhooHS4eaDvW3OhkOVsl6onRfq2XvfZvqR8MGfUkKCDedtar0l6ZFBp9z0JZn4QkDABxRXj3OsIN196qNr6dbIY67v3ylThqFX4Zy/usREI6IQU41Pn+WVvUqeszbWeoesJ0fdy2XiXShsoLrm/s1SKWcvz3fbcSVOzMozm4ExquXWo1EPO98wRgDVDAVKuIVG6ZBHOPrm+gUqdbPb6cYccVqbphVh/79vWvvh3/c+8HiLnsXNEAcM8TV+vHvXzeel+pMvhcB5jfDyrHWVZrE6tWRS5f85+6+Bor10/AamQV6HZyPQz/v3fdQsCeQAPA3V+9FiuoWiW8cVRgVL66Vh7/0v3rrCYNhN+LTQR1Pejrtj6fV02UAlwgK7DqKHXk8//d8X4C9mAdHGYZEkYUg1HATRVr2Cj1jAEem+BeizDK1fUB8g2eV7BX+fDmO/Hd226mvSkaQPqBoAJmOz6mYFLqc9fkqUTVNwVYAQOpVbCpTQQqE+SswuwRrGrZNNNY1rfcRHtTNOAVx6DGAKfzSlmcN9SADRhdZ6hzfNveJuTxzwEWcTTVS2IT0o6DHIlAf/9H3KuiAaQfCMoAe1RMCjDS4PTyBaZVHFwdyqbiSajalIlctfuzHkip2IK0Y+LJ0HXn40037lfRAOyyYlhjUPzgiZUIAeD/fFVN0gy8ph8Pjf7wKhhTMTXG1lL5TTcSgP09dei48zsvxVEwatBGxSpfNh6v4Il6ypMFlYlS6m75cGsVrK8L9npLxbovDjDHQUADwB1PvhyrQWnAWk0CIPXVbDjq/jGlaiVOKj3U98tHcfJH1QetYO3JVAPm2L915NCbWWUlGor/pNZrE1zP0LjGE+oBcxu8CtQHF+u7DcDcxgxgjoMpGgBuf+qVWA1edbx8pNU59n2P3axPQplQzvc+7NQqf0khrVLXV+5Dp4p1HEzRgBpsA5B/hBpXsIOQIVVguZ0Jm+A6artQ/dhCxToOqmgg/UCwpc7KV2VDsoMXiKFA7gKswDZXk/y+o5HnlbwBYI6DKhpQyiWo5TwOuIJN7n4Nf0rBTqnWKnS921nEWBxc0UD6gWD1jVmnTRg/V/DtxFlI3gLWq9AAXE+yWUVbQAaOBBoAbnn2tdj6MDOrMjUJ4s1zG52GvqK+dnYEmOPg1sFRnjA6Bj7mxzOA/UQWW1HHPdhEK46maADpB4IOUgJbg1uvaB5wNVEKHNejN709WMRYHE3RQEthGlgBFzPkKrdSpl8JDT8muAnA3iEDR1Y0kH4gOLXbi8V0bnTej0f3gAMB5jiqogEY3603PK16ZxMhq73yZlJWo8GWOg8JmOPoigbyDwQ9JG8noYZlNjq1KsRqxlbKAQFzLAI0ALz393+JoPS1ZBfgEaspvq7rSfnHggwswDo44kDWJjo2uqjK1icJpNiEUvkxAXMsRtFA+oGg8d3Ax1ZZmQiGvETAHItRNOA2sBbgfM348QkVwBn8kgBzLErRAHDDtbeisQzZ7Ap8hszfO2s1LxEysDBFAxCfFrB+I9Q+jaL0pQLmWJyigfQDQbPZZcDmQ8jCFexjkaAB4LrX/xZjsKD19x1nBTDH4qyDw39o4e8skNGeJcjAghUNAMOb7yRVnxSmZw0wx2IVDeSNcSAgAvHmswmY4/+kE9rZ2xC1UQAAAABJRU5ErkJggg=="></image></g><g class="cls-4"><image width="132" height="132" transform="translate(39.25 -0.84)" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIQAAACECAYAAABRRIOnAAAACXBIWXMAAAsSAAALEgHS3X78AAAIXUlEQVR4Xu2dW5rbKgyART7vf6PnbGGGPiTOgA3oDtjmf2hTB4QkFCGwkwb47/8Ii8WH7fvqGBYRAF5A40khNb2tOgU3+I1fGeH47s/n78IY8dSYh7K7Pzq/zk/Fvi38AJ0kcEJ64dgsTD/dOcTJp1lFFOaJQoUt/DJ7E5qHWqNaJtrfpi5RWgg2TE1Bf1qw4mzhF2uSEE8v5BxFvAKwdJFwUtvADm86q/iuIRAC3kTOLhvRQ5M9AkB3x4oYrmNsLBmOymXpjTgOlj3QgCGO48kprU+g05H3kjFSMdXYf51LAZNOQHwF5ViGqPWgCWjWFRURW/ihCTdlwJDFTJhewjKMFQNsBwDyuBsICjlRRYsqhDawJx0SXZJwq/EWUDHTwXahSP62k4KDyB2S0ykwdcT8hNYwR7jjYw12mHKP8Ladx9GUgw/BQecAlKK3NKUx+0sNQQ4WWHiGIAwyNUP1T4teRJE0YDQ6k/pWGkWADYyKSizyumFjTn9aAcOxaV+6OH2StswlYxI4xh44B65CmCcCteIrfIvjakZqrFwAlCVjFJOq5QbBXiwLY3MZXwAQ2/XONTPEQgRlrov3Mkj3LpI27C2XlGRM7NNyayjzI6SRIWLx5YnQ4S6lxgOKrldCcn+ohLyGiKcXZa72URa641I0bCQcXbc9pI1Mu6erBIM3IGllO+R4oiZDcKnugm6WYUp0cjEFzJ3bqYAsKu9skUZ86UgYlYc26MtE6uBPTDkpi0XqiZoelL03hpONR9g273TSD4B8DnHQqKOCbA66ofaht7ULxs5kv6EuATxqCGNxbux6YvZTMkyNkb4Qjq3eZbAwFAWgSMEcGl9WAoD2bW0KjKZqewlj2WcILZOpg9F8BJFjSy0TcWRwKciuZIh3S3VEeuHppAHEsN+ljLnP07nxDpiPnPkyhCUC00Z8CLCzmBjg9Mzn8bjg9F3b5H2OTX/nEFHgvTsxsfnohL5Cfuu7ZEuoXD+An0PsIM1Qpe8I0XXuUM5ifj9zlDVN//GOmHsvGS0eZDb6Df8A3xWCeDB1IR400absRWX9HKLu2XdyGbVIzDXjVS/MpSaZLWDFZOVtrDLuGi8Xdb4bCn8QTirrnOa8WrCcsXsOwhaRVk1TFbMzgHqGcLajOG56iXsPwVnfaXC2k5UhyJ8eC6WRynj4j55Z2NgdXOl3hsDbjaNawyB8voNwaQbov30PLLQMUL4JIcPc0u4WBF0NzyEIo0lxEJ0GgyYwzsWxkbJGYri24dvOI8zml4Jr26tSHO/8An9GLOHaA8yisisCY97gHc3mCF2WQlOdECNt+03c1hIkofxlCNyP98fYB+jh3QvqX9DdL2tmWWDP2AwhULiGxm/D+GaYxBHpy8Yv5+2BdMwwfD/EbIzn3u0EMA1ID9BfCAyEA749Qoi2btSGZITy+JF9Q4S+q7H/HsQuNwCcxwj5NcNt52iMvTkLCrPw5yD2JemvjtwgKSoDgPpIOAPPeAtvmnNwfvOUIUg/FrLQMY2PCwHBfbi2uA4lmGaYAXzV57nlNpy//X0Ee/8AKs8rYLBxF3XSorL7OUQsxESi0NUzjBSS2dWgt/s08O9laCAMNSzDeILZNBE25xAWMqhgY3kFDDbuxHBcktcQlzG6VdUi/6ZCuenUFaYhzOY79G9uzYS3ygGAu/typaMqzSXD7TPS0UARiH6lwlfsq8l8gW87d6jtujFOoaLP0muiH0KjYiSoImbokiH+VFlj7QLkLiV3a81s/kZoEz1DzMQVdU5AfS6JAEwmEZttZ4qpPFNhXyT+7gpawxwsOLXPL5Ds/XR59gMyPXBwL/poHikCoKibfYZ4OjP4U6HDoYaIubApDmcY1jGaLsq0b39z/t+m2VjBIYK+ZKTtktel2OBuq7pAtdOAGc2nwt92Eto3Zc7gLYIN96fsBPYTU1+E3dB+IwIG0+lB0JeMGo3+orlV6mOyXAl0sBh2BvhLRgkLGRYEwimgFC+5k6HPECmWsjKIgrFm1ttobDw17gOckNcQNZAdyFAQWy2XGwtRI7BZMo54yOwA6gvpLGNyJ8J2ydjxkDkDmF2cgMFkDUIVEFX7TzIVg1wJxMzaj4NkV+PpRVf8HsN3EntlUF+3Mkwnf6oyRBUPmU8A81vxPkHhmoJVVF6Jol/TbV0SMew5eHew33YCCJRZmGAwlz5LRove4y1YnAKCsnMwOcA54CCyHzcKclENIemzmJDCPDosGVKBl84RDaT+GMOmUljRNSMA2AlTMIEKoyFnCNfPb6pDQR/LmsVQVH+Ic6VBVEP05go63oWt+e5dJgJLC3ex0wDyktENtT6lNed8KcP6wRkumH5NVJ1PqE8qB7vSBuT3ork1DLN5PwhTfYkaYjRP8lG7hrDgQc6swkkZg/01Xw0xAm8fYPI5AcMBG7eAW0B42XgfYvGlmAiC4vgwcPR8YmrRH4O59K8heqD3w/3ZkwfiK7clYzEZxHnOMwSxU7pSNffoRHkLHdzKoYX6HELbfzEX8hqCEgiWodsbin03xLeGaMqOcO2IGYnfpOkekFFz3gdnrHjJ6TBVvhlCy8y63ZQX1mDxLETbTk96rhKReFjTg552t1BvO6/Mk22vId92arjLRFh9rCfyx9xF5ezc0HdjMsRsjJzYWpZJdZJkIqFNk2SIKZQYA2p6KLRBO4kZfDC1wGnMT+mwt9SckmE+/VwyBGX8hRHI/MXPH9Q52agNF9eEO7/rpHKRsXYZd8Bw2XepIRbXZS0Zi4wVEIuMFRCLjBUQi4wVEIuM52w7PXZT3FMfbwxsXNtODTf03VoyFhkrIBYZKyAWGSsgFhkrIBYZKyAeSWV7FON6QOaZVGY9BPgHK/KZW2aWrhUAAAAASUVORK5CYII="></image></g><g class="cls-5"><image width="174" height="119" transform="translate(18.25 14.16)" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAK4AAAB3CAYAAACaPXsJAAAACXBIWXMAAAsSAAALEgHS3X78AAAIRElEQVR4Xu2d7YLjKgiGsSf3f6O7t7Dj+dGkYxM/QAEx4fkzu50EX5BQYmwnwJ+/EVKO/wVwLPE9S49nKwYknn4e/9h/nPM6viZl+rITKiyc2fyk2S2yhZ/EQ6qzx/EvgPATAX6qR38lfXxVj3wG1HjfEWIMjgtoC//SlwlW0kNbCXsmBAiocwh6tDAoSRSj/m7wg1AWud8q4rUF4SICxP8aajND8vo3kWw4mWM8i8SNLfyTdyoA6MUu7m0LAUtti2qspBH0Y8O9ZR9UlAiKPHOpjoNj02IApxtRxOCIQ2aSfbcxrnnLVieLotk04QzVWofwE7FmAOCc6JMg6MWBN4jyHm8OAJRahSyThiXDoJPSunSvuOCHkEdYSwByq3ACJRB1kC4GJaVU5ySnndq6jCBsvshp3A2EKy7qbaIHWdnCnMQP+BJfAVArQzuk+cCb7SB+/aCS73GlUBqKNDmcKPmXIj1/sRJMUpyZZeZbBeZBeBAWJWCeNLHaIP2l+nDpzwvjoO0Wzp93c9bLYnJFYIhBPnHGDVPvmWLIKElkhPMLO3ytApOZ2+FxKRMBAjJAR3pHOFYVvvYqOI5tPptsqnekEdmLZEx0rUVqcnk7clDgCqQ4lXXck0Ki4KOSY5+9054uIQxiYDJzJ7gfp0tB63EJh6LZbbZ1nH5PSnQEreGfxLRY4AeutgpTr77WWM1E/+ZoXVDpTjN9HxbyG//IV8GpZlIVNbTFof3cubQu7SGm04xfDZR/qINUaLcKdrRekdL2wrQuO8jDTDPZh54LrlxxJztTpiCMUy+xOpPJLbqroRA/BX57XCXholOm5MM4tHc5zNIiW1wtxzDRNukBhGB0BE0DMCYIga85Qvh3XVpEnJSj8zTWGBU0tHvc1biZOyhOPpPnFFHRAUA/tpXxtu9+7n0k6xWjiXZgrUKNA7WnR+4AG6Jhc/2KOyh/2YsUSxwM0In3xvXWUTvJ0F1xrmhfL3EXk2sGRNwwySWdL5eN6zGvC/8AwiKyMVyUCUFhHBJz8QBUHvmGkpjkdcwyDTuFK9BRhDFRe8FX3LPYkDxGrfQi6e4w2g4wYxiYrCsmRVVpZgDSpf4eN3dazVSQ749ug4epyfX7cYWChmk9RgbPfnbppgx5igox6qCp9FXcjlNESHRgP7v0gdqfE80/hklx2dBrcjtdV/sk564kQs6Puhsac/15VyxKmIkRJwSnCIcC9FZcTZTllZKR+jH+2hdpqEKTLYJEKLav3tOAk2Io+/aeLMKglnt0ghvj4AbDfSN5CwYTvUydbla/aca4KzqzuV9obqHBr+NqIOTkktRi8aqs0lDhssMFSk/EV1yxK1ISnGvrIbGHOo0VdsVlRnz3MW1V3NUQnLiphYJ4YaDblkK8sKenbP1vOacTu+04KAzHl5x4tROQfqJbhakwSCQH13nDEPthAlx0eKvwhYVZMsqs0ISwj70LOHpciBEgdlSkjCNpr0O258xhVkKiyQvsq7ilJvvzOjIalhfde0G6PgfT4khs57206f5ZcdKxEWPGV1LJEcc7SkyYi76K28ugg+TvgLhbQR+M35247sdNKM771zmGo5lKw8jEfEKjagcziAMAw6GyvzvsjJTcAOSvLl2ehd2tVtwDRB3Kg7BthqLWDidyp2CqeUrHsLbpdKhw2noVVwKNEBD38/ZXC0WILnGCqrgHarEkaLot1BhQJodq2yALbyRfSKxFqag1dAHhTCa79ipgXDYB3bXnQP1OMYaNMZz07w7rPc+RR2RuiEZRFb1CdrjfF7sq7sGgtDXpD9djiAE+FR2VIx0x7a+4vWiP5+ixzy0qWQchf6+C48wmAMAWqE36AfY0jcvvjmDj+1BI67hZaudndq4/FgNxuFMNGe9xa+eP2v5QMDR658oFm59PYTxgl/24KDpOEYGqfTTPicM5crRbhdbvYTwf1ED48iEC/mPXGQZOdVpE7Y3ki/Fpo7AJ79naDzbGO+2Ki4LFiF0S96q5SQzD9cuoiQZWARs/Av3LYU9AODTk2HPNuhREd0ZgqriLs0IMLC4tTtRzi8RtFqKLjws6LSwZ8zc0vo6Il3+osm6rsKhsq5Dz4Mhi4mlc2Ki4FjQ4NE5z1qrXXFN8jDP+5AyAT5WjiO6ktRL7wyHrq3U5aY2D+3EdR4xG69LVKlSuBefBoKsqA1t2MEI2ksWSTzhB0Obcl66Ke4Fig3IsRDs7wEYh+e2UeQeyb3eYFDkpVH03yXNViCG2AE/FJSCeVyV/Cq+P7ADTZBGZYxBykWc5bGGe7v+qbK0DiviEt/HlFzHUWwUS7NrYDdahDid1I0rVUYTN0DDZmzOh8K2H9jytfiNKlD/C43vc5Ujnizp31hK9RsM3262Cw8uN5rr/5izHjQKjwUoFEEPP9PfGwFCroCBEYYjHEkIhCU9BZ5oDW0/OnCurTI9yHnmP6yzJtcc9JzLiE9SXQ3oblxJ+cS0D99SXaPe4rd9naNp0nEHoqwrcSal1ifbA7avDhnKPmxlMfHzLVwYW8SAtxyYaFEHTeIgitPOcKM95o1xxF8DjsQT0HlcLwwk0UpTZV1xKGI7fwUgo2qsKDisebx7sVlwuVk6UkZLEicEYeo9rGZ+bIvevuBI8NaF63wEE4qVUcVUGcaQxNI2y67hOP8ampbfYUvisuCD2w/iqgmMGSi6+Wgc4jkU8cZ0lKa8qEMo2O9SGaqZWZwpKqwpELGpyTOGtgrMknrjOknjiOkviiessyRp7Ffxm7RfqikuOG8TT5qqCU8bnCwC8VXAWxRPXWRJPXGdJPHGdJfHEddYjxsKfRHUcy4QA/wM0JGxAD1YgyQAAAABJRU5ErkJggg=="></image></g><path class="cls-6" d="M256.56,127.2v-.08a1,1,0,0,0-1.62-.53,26.11,26.11,0,0,1-18.42,7.14q-11.31,0-18.46-6.46a20.64,20.64,0,0,1-7.14-16q0-12,8.91-18.42t25.5-6.42h9.36a1,1,0,0,0,1-1v-4a13.18,13.18,0,0,0-3.26-9.33q-3.27-3.5-9.92-3.5A14.86,14.86,0,0,0,233,71.6a9,9,0,0,0-3.48,5.34,2.45,2.45,0,0,1-2.37,2H215.11a2.42,2.42,0,0,1-2.4-2.76,19.27,19.27,0,0,1,3.89-8.8,27.37,27.37,0,0,1,11.13-8.43,39.22,39.22,0,0,1,15.72-3q13.17,0,21,6.63t8,18.63V115q0,10,2.75,16a.93.93,0,0,1,.09.4h0a1,1,0,0,1-1,1H262.93A6.48,6.48,0,0,1,256.56,127.2Zm-16.93-7a19.17,19.17,0,0,0,9.4-2.43,16.22,16.22,0,0,0,6.5-6.29,1.05,1.05,0,0,0,.13-.49V98.07a1,1,0,0,0-1-1h-8.11q-9.37,0-14.09,3.26a10.58,10.58,0,0,0-4.71,9.23,9.86,9.86,0,0,0,3.22,7.74A12.49,12.49,0,0,0,239.63,120.2Z"></path><path class="cls-6" d="M290.21,37.77a9,9,0,0,1,2.46-6.45q2.46-2.57,7-2.57t7.08,2.57a8.85,8.85,0,0,1,2.5,6.45,8.65,8.65,0,0,1-2.5,6.35q-2.49,2.54-7.08,2.53t-7-2.53A8.74,8.74,0,0,1,290.21,37.77Zm9.85,94.57h-.76a8,8,0,0,1-8.05-8V59.68a2.41,2.41,0,0,1,2.41-2.41h6.4a8,8,0,0,1,8.05,8v59A8,8,0,0,1,300.06,132.34Z"></path><path class="cls-6" d="M336.35,132.34h-.77a8,8,0,0,1-8-8V28.19A2.41,2.41,0,0,1,330,25.77h6.4a8,8,0,0,1,8,8.05v90.47A8,8,0,0,1,336.35,132.34Z"></path><path class="cls-6" d="M439.79,119.13a3.69,3.69,0,0,1-.82,2.3q-4.46,5.53-13.09,8.76a60.15,60.15,0,0,1-21.19,3.54,39.44,39.44,0,0,1-21.1-5.69,37.88,37.88,0,0,1-14.29-16.1,54.4,54.4,0,0,1-5.13-23.87V75.31q0-21,10.61-33.2T403.3,29.93q15.6,0,25.11,8a31.52,31.52,0,0,1,10.52,17.51,3.59,3.59,0,0,1-3.53,4.38h-1.26a3.57,3.57,0,0,1-3.5-2.7q-2.22-9.15-8.51-14.18-7.17-5.74-18.76-5.74-14.16,0-22.41,10.07t-8.26,28.4V87.56a50,50,0,0,0,3.92,20.38,31.06,31.06,0,0,0,11.24,13.71,29.72,29.72,0,0,0,16.83,4.86q11,0,18.94-3.47a22.56,22.56,0,0,0,6.59-4.22,3.51,3.51,0,0,0,1-2.53V95.22a3.61,3.61,0,0,0-3.61-3.61H407.74A3.61,3.61,0,0,1,404.13,88h0a3.61,3.61,0,0,1,3.61-3.6h28.44a3.6,3.6,0,0,1,3.61,3.6Z"></path><path class="cls-6" d="M472.54,95.46v32.61a4.27,4.27,0,0,1-4.27,4.27h0a4.27,4.27,0,0,1-4.27-4.27V35.59a4.27,4.27,0,0,1,4.27-4.27h30.15q15.75,0,24.87,8t9.12,22.13q0,14.22-8.77,22T498.21,91.2h-21.4A4.27,4.27,0,0,0,472.54,95.46Zm0-15.75A4.27,4.27,0,0,0,476.81,84h21.61q12.42,0,18.94-5.9t6.52-16.47q0-10.5-6.49-16.71T499,38.54H476.81a4.27,4.27,0,0,0-4.27,4.26Z"></path><path class="cls-6" d="M616.43,38.54H589a3.61,3.61,0,0,0-3.61,3.6v86.59a3.61,3.61,0,0,1-3.61,3.61h-1.32a3.61,3.61,0,0,1-3.6-3.61V42.14a3.61,3.61,0,0,0-3.61-3.6H545.94a3.61,3.61,0,0,1-3.61-3.61h0a3.61,3.61,0,0,1,3.61-3.61h70.49A3.61,3.61,0,0,1,620,34.93h0A3.61,3.61,0,0,1,616.43,38.54Z"></path></g></g></svg>
                  <div class="f-14 f-md-16 w400 mt20 lh150 white-clr text-center" style="color:#7d8398;"><span class="w600">Note:</span> This site is not a part of the Facebook website or Facebook Inc. Additionally, this site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc.<br><br>
                  <div class=" mt20 mt-md40 white-clr text-center"> <script type="text/javascript" src="https://warriorplus.com/o2/disclaimer/d8j629" defer=""></script><div class="wplus_spdisclaimer"><strong>Disclaimer:</strong> <em>WarriorPlus is used to help manage the sale of products on this site. While WarriorPlus helps facilitate the sale, all payments are made directly to the product vendor and NOT WarriorPlus. Thus, all product questions, support inquiries and/or refund requests must be sent to the vendor. WarriorPlus's role should not be construed as an endorsement, approval or review of these products or any claim, statement or opinion used in the marketing of these products.</em></div></div>
                  </div>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-md-16 f-16 w400 lh140 white-clr text-xs-center">Copyright © MailGPT 2023</div>
                  <ul class="footer-ul w400 f-md-16 f-16 white-clr text-center text-md-right">
                     <li><a href="https://support.bizomart.com/hc/en-us " class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://mailgpt.live/legal/privacy-policy.html " class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://mailgpt.live/legal/terms-of-service.html " class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://mailgpt.live/legal/disclaimer.html " class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://mailgpt.live/legal/gdpr.html " class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://mailgpt.live/legal/dmca.html " class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://mailgpt.live/legal/anti-spam.html " class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!--Footer Section End -->
      
      <!-- timer --->
      <?php
         if ($now < $exp_date) {
         ?>
      <script type="text/javascript">
         // Count down milliseconds = server_end - server_now = client_end - client_now
         var server_end = <?php echo $exp_date; ?> * 1000;
         var server_now = <?php echo time(); ?> * 1000;
         var client_now = new Date().getTime();
         var end = server_end - server_now + client_now; // this is the real end time
         
         var noob = $('.countdown').length;
         
         var _second = 1000;
         var _minute = _second * 60;
         var _hour = _minute * 60;
         var _day = _hour * 24
         var timer;
         
         function showRemaining() {
         var now = new Date();
         var distance = end - now;
         if (distance < 0) {
         	clearInterval(timer);
         	document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
         	return;
         }
         
         var days = Math.floor(distance / _day);
         var hours = Math.floor((distance % _day) / _hour);
         var minutes = Math.floor((distance % _hour) / _minute);
         var seconds = Math.floor((distance % _minute) / _second);
         if (days < 10) {
         	days = "0" + days;
         }
         if (hours < 10) {
         	hours = "0" + hours;
         }
         if (minutes < 10) {
         	minutes = "0" + minutes;
         }
         if (seconds < 10) {
         	seconds = "0" + seconds;
         }
         var i;
         var countdown = document.getElementsByClassName('countdown');
         for (i = 0; i < noob; i++) {
         	countdown[i].innerHTML = '';
         
         	if (days) {
         		countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + days + '</span><span class="f-14 f-md-18 smmltd">Days</span> </div>';
         	}
         
         	countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + hours + '</span><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>';
         
         	countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">' + minutes + '</span><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>';
         
         	countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">' + seconds + '</span><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>';
         }
         
         }
         timer = setInterval(showRemaining, 1000);
         	
      </script>
      <?php
         } else {
         echo "Times Up";
         }
         ?>
      <!--- timer end-->
   </body>
</html>
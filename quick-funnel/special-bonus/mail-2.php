<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <!-- Tell the browser to be responsive to screen width -->
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
      <meta name="title" content="QuickFunnel Bonuses">
      <meta name="description" content="QuickFunnel Bonuses">
      <meta name="keywords" content="Have A Sneak Peak of A Breakthrough Technology That Analyzes, Detects & Fix Deadly Legal Flaws on Your Website Just by Copy Pasting a Single Line of Code">
      <meta property="og:image" content="https://www.quickfunnel.live/special-bonus/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Atul Pareek">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="QuickFunnel Bonuses">
      <meta property="og:description" content="Have A Sneak Peak of A Breakthrough Technology That Analyzes, Detects & Fix Deadly Legal Flaws on Your Website Just by Copy Pasting a Single Line of Code">
      <meta property="og:image" content="https://www.quickfunnel.live/special-bonus/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="QuickFunnel Bonuses">
      <meta property="twitter:description" content="Have A Sneak Peak of A Breakthrough Technology That Analyzes, Detects & Fix Deadly Legal Flaws on Your Website Just by Copy Pasting a Single Line of Code">
      <meta property="twitter:image" content="https://www.quickfunnel.live/special-bonus/thumbnail.png">
      <title>QuickFunnel Bonuses</title>
      <!-- Shortcut Icon  -->
      <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
      <!-- Css CDN Load Link -->
      <link rel="stylesheet" type="text/css" href="assets/css/bonus-style.css">
      
      <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&family=Crimson+Text:ital,wght@0,400;0,600;0,700;1,400;1,600;1,700&family=Great+Vibes&family=League+Spartan:wght@200;400;500;600;700;800;900&family=Merriweather:ital,wght@0,300;0,400;0,700;0,900;1,300;1,400;1,700;1,900&family=Niconne&family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
    
      <!-- required -->
      <!-- Start Editor required -->
      <link rel="stylesheet" href="https://cdn.oppyo.com/launches/yoseller/common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style.css" type="text/css">
      <script src="https://cdn.oppyo.com/launches/writerarc/common_assets/js/jquery.min.js"></script>
   
   </head>
   <body>
      <!-- New Timer  Start-->
      <?php
         $date = 'Feb 21 2023 11:00 PM EST';
         $exp_date = strtotime($date);
         $now = time();  
         /*
         
         $date = date('F d Y g:i:s A eO');
         $rand_time_add = rand(700, 1200);
         $exp_date = strtotime($date) + $rand_time_add;
         $now = time();*/
         
         if ($now < $exp_date) {
         ?>
      <?php
         } else {
         	echo "Times Up";
         }
         ?>
      <!-- New Timer End -->
      <?php
         if(!isset($_GET['afflink'])){
         $_GET['afflink'] = 'https://quickfunnel.live/special/';
         $_GET['name'] = 'Atul Pareek';      
         }
         ?>
         
     <!-- Header Section Start -->   
     <div class="header-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                  viewBox="0 0 693.1 109.7" style="enable-background:new 0 0 693.1 109.7; height:45px;" xml:space="preserve">
                     <style type="text/css">
                        .st0{fill:#FFFFFF;}
                        .st1{fill:#EF3E39;}
                        .st2{fill:#853594;}
                        .st3{fill:#EF3E3A;}
                     </style>
                     <g>
                        <g id="Layer_1-2">
                           <path class="st0" d="M140.4,0H18.9c-2.8-0.1-5.2,2.1-5.4,4.9s2.1,5.2,4.9,5.4c0.2,0,0.3,0,0.5,0h42.3c2.8,0,5.1,2.3,5.1,5.1
                           c0,2.8-2.3,5.1-5.1,5.1h-56c-2.8,0.1-5,2.5-4.9,5.4c0.1,2.7,2.3,4.8,4.9,4.9h35.6l0,0h13.5c2.8,0.1,5,2.5,4.9,5.4
                           c-0.1,2.7-2.3,4.8-4.9,4.9H46c-2.8,0-5.1,2.4-5.1,5.2c0,2.7,2.1,4.9,4.8,5.1h17.8c2.8,0,5.2,2.2,5.2,5.1c0,2.8-2.2,5.2-5.1,5.2h-3
                           c-1.3,0-2.6,0.6-3.5,1.5c-2,2-2,5.2-0.1,7.3c1,1,2.3,1.5,3.7,1.5h8v37.7L103.1,96V68.6l42.8-57.6c2.3-3,1.6-7.3-1.4-9.6
                           C143.3,0.5,141.9,0,140.4,0z M66.2,36c0-2.8,2.3-5.1,5.1-5.1h10c2.8,0.1,5,2.5,4.9,5.4c-0.1,2.7-2.3,4.8-4.9,4.9h-10
                           C68.5,41.1,66.2,38.8,66.2,36z"/>
                           <path class="st1" d="M34,46.3c0,2.8-2.3,5.1-5.1,5.1h-10c-2.8,0-5.1-2.3-5.1-5.1c0-2.8,2.3-5.1,5.1-5.1c0,0,0,0,0,0h10
                           C31.7,41.2,34,43.5,34,46.3z"/>
                           <path class="st1" d="M48.8,66.9c0,2.8-2.3,5.1-5.1,5.1h-2.9c-2.8,0-5.1-2.1-5.3-4.8c-0.1-1.5,0.4-3,1.5-4c0.9-0.9,2.2-1.5,3.5-1.5
                           h3.4C46.6,61.8,48.9,64.1,48.8,66.9z"/>
                           <path class="st2" d="M45.8,61.7L45.8,61.7z"/>
                           <path class="st2" d="M63.7,61.7L63.7,61.7z"/>
                           <path class="st2" d="M63.7,61.7L63.7,61.7z"/>
                           <path class="st3" d="M230,48.3c0,4.5-0.7,8.9-2.3,13.1c-1.5,3.9-3.7,7.5-6.5,10.5c-2.8,3-6.1,5.4-9.9,7c-8,3.3-17,3.3-24.9,0
                           c-3.7-1.6-7.1-4-9.8-7c-2.8-3.1-5-6.7-6.4-10.5c-1.6-4.2-2.4-8.7-2.3-13.1c0-4.5,0.7-9,2.3-13.2c1.4-3.9,3.6-7.5,6.4-10.5
                           c2.8-3,6.1-5.4,9.8-7c8-3.3,17-3.3,24.9,0c3.7,1.6,7.1,4,9.9,7c2.8,3.1,5,6.6,6.5,10.5C229.3,39.3,230,43.8,230,48.3z M222.1,48.3
                           c0.1-4.6-0.9-9.2-3-13.4c-1.9-3.7-4.7-6.9-8.2-9.1c-7.4-4.4-16.6-4.4-24,0c-3.5,2.3-6.3,5.4-8.2,9.1c-2.1,4.2-3.1,8.7-3,13.4
                           c-0.1,4.6,1,9.2,3,13.3c1.8,3.7,4.7,6.9,8.2,9.2c7.4,4.4,16.6,4.4,24,0c3.5-2.3,6.3-5.4,8.2-9.2C221.1,57.5,222.1,52.9,222.1,48.3
                           z M233.1,84.8c1.9,0.1,3.4,1.6,3.4,3.5c-0.1,1.3-0.7,2.5-1.7,3.4c-1.3,1.2-2.8,2.2-4.4,2.9c-1.8,0.9-3.7,1.5-5.7,2
                           c-1.8,0.5-3.7,0.7-5.6,0.7c-3.1,0.1-6.2-0.3-9.2-1.2c-2.1-0.7-4.2-1.5-6.2-2.6c-1.8-1-3.5-1.9-5.2-2.6c-1.9-0.8-3.9-1.2-6-1.2
                           c-1,0-2,0.2-2.9,0.6c-0.8,0.4-1.6,0.7-2.3,1.1c-0.8,0.4-1.7,0.6-2.6,0.6c-1,0-2-0.4-2.7-1.1c-0.6-0.7-1-1.6-1-2.5
                           c-0.1-1.4,0.8-2.8,2.1-3.2L200,79l8.4,0.8l-15.7,5.5l2.6-1.9c2.2,0,4.4,0.3,6.4,1.1c1.9,0.7,3.7,1.5,5.5,2.5
                           c1.8,0.9,3.6,1.8,5.5,2.5c2.1,0.8,4.4,1.1,6.6,1.1c1.8,0,3.7-0.3,5.4-1c1.2-0.5,2.4-1.2,3.6-1.9c0.8-0.6,1.7-1.3,2.4-2
                           C231.3,85.1,232.1,84.8,233.1,84.8L233.1,84.8z"/>
                           <path class="st3" d="M277.1,32.9c1,0,2,0.4,2.7,1.1c0.7,0.7,1,1.7,1,2.6v25.1c0,6.2-1.7,11-5.2,14.4s-8.3,5.1-14.5,5.1
                           c-6.2,0-11-1.7-14.4-5.1s-5.2-8.2-5.2-14.4V36.6c0-2,1.5-3.7,3.5-3.7c0,0,0.1,0,0.1,0c1,0,2,0.4,2.7,1.1c0.7,0.7,1,1.7,1,2.6v25.1
                           c0,4.2,1.1,7.3,3.2,9.5s5.2,3.2,9,3.2c4,0,7.1-1.1,9.2-3.2s3.2-5.3,3.2-9.5V36.6c0-2,1.5-3.7,3.5-3.7
                           C277,32.9,277,32.9,277.1,32.9L277.1,32.9z"/>
                           <path class="st3" d="M297.9,26c-1.2,0.1-2.4-0.2-3.4-0.9c-0.8-0.7-1.2-1.8-1.1-2.9v-1.3c-0.1-1.1,0.4-2.2,1.2-2.9
                           c1-0.7,2.2-1,3.4-0.9c1.2-0.1,2.4,0.2,3.3,0.9c0.8,0.7,1.2,1.8,1.1,2.9v1.3c0.1,1.1-0.3,2.1-1.1,2.9
                           C300.2,25.8,299.1,26.1,297.9,26z M301.5,76.8c0,1-0.4,2-1.1,2.7c-1.4,1.5-3.8,1.5-5.3,0c0,0,0,0,0,0c-0.7-0.7-1-1.7-1-2.7V35.9
                           c-0.1-2,1.5-3.8,3.5-3.9s3.8,1.5,3.9,3.5c0,0.1,0,0.3,0,0.4L301.5,76.8z"/>
                           <path class="st3" d="M336,31.7c2.9,0,5.7,0.3,8.5,1c2.2,0.5,4.2,1.5,6,2.8c1.3,0.9,2.2,2.4,2.2,4c0,0.8-0.3,1.6-0.8,2.3
                           c-0.5,0.7-1.3,1.1-2.2,1.1c-0.7,0-1.4-0.1-2.1-0.5c-0.5-0.3-1-0.7-1.5-1.2c-0.5-0.5-1.2-1-1.9-1.3c-1-0.5-2.1-0.8-3.2-1
                           c-1.4-0.2-2.8-0.4-4.2-0.4c-3.1-0.1-6.2,0.8-8.9,2.4c-2.5,1.6-4.6,3.8-6,6.5c-1.5,2.8-2.3,6-2.2,9.2c-0.1,3.2,0.7,6.4,2.1,9.2
                           c1.4,2.6,3.4,4.9,5.9,6.4c2.6,1.6,5.7,2.5,8.8,2.4c1.7,0,3.5-0.1,5.2-0.6c1.1-0.3,2.2-0.7,3.2-1.3c0.9-0.5,1.8-1.1,2.5-1.9
                           c1.4-1.3,3.5-1.2,4.8,0.1c0.6,0.7,0.9,1.6,0.9,2.5c0,1.1-0.8,2.3-2.3,3.5c-1.9,1.4-4,2.5-6.2,3.1c-2.9,0.9-5.9,1.3-8.9,1.3
                           c-4.3,0.1-8.6-1-12.3-3.3c-3.5-2.2-6.3-5.3-8.2-9c-2-3.9-3-8.3-2.9-12.7c-0.1-4.4,1-8.7,3-12.6C319.3,36.2,327.3,31.5,336,31.7z"
                           />
                           <path class="st3" d="M367.1,80.6c-1,0-2-0.4-2.7-1.1c-0.7-0.7-1-1.7-1-2.7V16c0-1,0.3-1.9,1-2.6c0.7-0.7,1.7-1.1,2.7-1.1
                           c2,0,3.7,1.5,3.7,3.5c0,0,0,0.1,0,0.1v60.8c0,1-0.4,2-1.1,2.7C369,80.2,368.1,80.6,367.1,80.6z M397.7,32.6c1,0,1.9,0.4,2.5,1.2
                           c1.3,1.2,1.4,3.2,0.2,4.6c-0.2,0.2-0.4,0.4-0.6,0.5L370,65l-0.3-8.6l25.5-22.7C395.8,33,396.7,32.6,397.7,32.6z M398.7,80.5
                           c-1,0-2-0.4-2.7-1.2l-19.7-20.9l5.5-5.1l19.5,20.8c0.7,0.7,1.1,1.7,1.1,2.8c0,1-0.4,2-1.3,2.6C400.4,80.1,399.6,80.4,398.7,80.5
                           L398.7,80.5z"/>
                           <path class="st0" d="M419,80.6c-1.6,0.1-3.1-0.5-4.2-1.6c-1.1-1.1-1.6-2.5-1.6-4V21.7c-0.1-3,2.3-5.6,5.3-5.7c0.1,0,0.2,0,0.3,0
                           h30c1.5,0,2.9,0.5,4,1.5c1.1,1,1.7,2.4,1.6,3.9c0,1.4-0.6,2.7-1.6,3.6c-1.1,1-2.5,1.6-4,1.6h-24.9l0.8-1.1v18.6l-0.6-1.2h20.6
                           c1.5,0,2.9,0.5,4,1.5c1.1,1,1.7,2.4,1.6,3.9c0,1.4-0.6,2.7-1.6,3.6c-1.1,1-2.5,1.6-4,1.6h-20.8l0.8-0.8V75c0,1.5-0.6,3-1.7,4
                           C421.9,80,420.5,80.6,419,80.6z"/>
                           <path class="st0" d="M499.3,31.3c1.5-0.1,3,0.5,4,1.6c1,1.1,1.6,2.5,1.6,4v23.4c0,6.5-1.8,11.7-5.4,15.5
                           c-3.6,3.8-8.9,5.7-15.7,5.7c-6.8,0-12-1.9-15.6-5.7s-5.4-9-5.4-15.5V36.9c0-1.5,0.5-2.9,1.6-4c2.2-2.2,5.7-2.2,7.9,0
                           c1,1.1,1.6,2.5,1.6,4v23.4c0,3.8,0.8,6.5,2.5,8.3s4.1,2.7,7.5,2.7s5.9-0.9,7.6-2.7s2.5-4.6,2.5-8.3V36.9c0-1.5,0.5-2.9,1.6-4
                           C496.4,31.8,497.8,31.2,499.3,31.3z"/>
                           <path class="st0" d="M543.2,30.3c4.6,0,8,1,10.4,2.9c2.4,1.9,4.1,4.6,4.9,7.6c0.9,3.4,1.4,7,1.3,10.6V75c0,1.5-0.5,2.9-1.6,4
                           c-2.2,2.2-5.7,2.2-7.9,0c-1-1.1-1.6-2.5-1.6-4V51.3c0-1.9-0.2-3.7-0.8-5.5c-0.5-1.6-1.5-2.9-2.8-3.9c-1.7-1.1-3.8-1.6-5.8-1.5
                           c-2.2-0.1-4.3,0.4-6.2,1.5c-1.6,0.9-3,2.3-3.9,3.9c-0.9,1.7-1.4,3.6-1.3,5.5V75c0,1.5-0.5,2.9-1.6,4c-2.2,2.2-5.7,2.2-7.9,0
                           c-1-1.1-1.6-2.5-1.6-4V36.9c0-1.5,0.5-2.9,1.6-4c2.2-2.2,5.7-2.2,7.9,0c1,1.1,1.6,2.5,1.6,4v4l-1.4-0.3c0.7-1.2,1.5-2.3,2.4-3.4
                           c1.1-1.3,2.3-2.4,3.7-3.4c1.5-1.1,3.1-1.9,4.8-2.5C539.2,30.7,541.2,30.3,543.2,30.3z"/>
                           <path class="st0" d="M598.1,30.3c4.5,0,8,1,10.4,2.9c2.4,2,4.1,4.6,4.9,7.6c0.9,3.4,1.4,7,1.3,10.6V75c0,1.5-0.5,2.9-1.6,4
                           c-2.2,2.2-5.7,2.2-7.9,0c-1-1.1-1.6-2.5-1.6-4V51.3c0-1.9-0.2-3.7-0.8-5.5c-0.5-1.6-1.5-2.9-2.8-3.9c-1.7-1.1-3.8-1.6-5.8-1.5
                           c-2.2-0.1-4.3,0.4-6.3,1.5c-1.6,0.9-3,2.3-3.9,3.9c-0.9,1.7-1.4,3.6-1.3,5.5V75c0,1.5-0.5,2.9-1.6,4c-2.2,2.2-5.7,2.2-7.9,0
                           c-1-1.1-1.6-2.5-1.6-4V36.9c0-1.5,0.5-2.9,1.6-4c2.2-2.2,5.7-2.2,7.9,0c1,1.1,1.6,2.5,1.6,4v4l-1.4-0.3c0.7-1.2,1.5-2.3,2.4-3.4
                           c1.1-1.3,2.3-2.4,3.7-3.4c1.5-1.1,3.1-1.9,4.8-2.5C594.2,30.7,596.1,30.3,598.1,30.3z"/>
                           <path class="st0" d="M649.9,81.5c-4.7,0.1-9.4-1-13.6-3.3c-3.7-2.1-6.8-5.2-8.8-8.9c-2.1-3.9-3.2-8.3-3.1-12.7
                           c-0.1-4.9,1-9.8,3.4-14.2c2.1-3.7,5.1-6.8,8.8-9c3.5-2,7.5-3.1,11.5-3.1c3.1,0,6.1,0.6,8.9,1.9c2.8,1.3,5.3,3.1,7.4,5.3
                           c2.2,2.3,3.9,4.9,5.1,7.8c1.3,3,1.9,6.2,1.9,9.4c0,1.4-0.7,2.7-1.8,3.6c-1.1,0.9-2.5,1.4-3.9,1.4h-35.2l-2.8-9.2h33.8l-2,1.8v-2.5
                           c-0.1-1.8-0.8-3.4-1.9-4.8c-1.1-1.4-2.6-2.6-4.2-3.4c-1.7-0.8-3.5-1.3-5.4-1.3c-1.8,0-3.5,0.2-5.2,0.7c-1.6,0.5-3,1.3-4.1,2.5
                           c-1.3,1.3-2.3,2.9-2.8,4.7c-0.7,2.4-1.1,4.9-1,7.5c-0.1,2.9,0.6,5.9,2.1,8.4c1.3,2.2,3.1,4,5.3,5.3c2.1,1.2,4.6,1.8,7,1.8
                           c1.8,0,3.7-0.1,5.4-0.6c1.1-0.3,2.3-0.7,3.3-1.3c0.8-0.5,1.6-1,2.3-1.3c1-0.5,2-0.8,3.1-0.8c2.6,0,4.7,2,4.7,4.6c0,0,0,0,0,0.1
                           c-0.1,1.8-1,3.5-2.6,4.5c-2,1.6-4.3,2.8-6.8,3.6C656,81,653,81.5,649.9,81.5z"/>
                           <path class="st0" d="M693.1,75c0.1,3.1-2.4,5.6-5.4,5.6c-0.1,0-0.1,0-0.2,0c-1.5,0-2.9-0.6-3.9-1.6c-1.1-1.1-1.6-2.5-1.6-4V18
                           c-0.1-3.1,2.4-5.6,5.4-5.6c0.1,0,0.1,0,0.2,0c1.5-0.1,2.9,0.5,3.9,1.6c1,1.1,1.6,2.5,1.5,4V75z"/>
                        </g>
                     </g>
                  </svg>
               </div>
               <div class="col-12 mt20">
                  <div class="row">
                  <div class="col-md-12 col-12 text-center mt30">
                  <div class="text-center">
                     <div class="f-md-32 f-20 lh140 w400 white-clr d-block d-md-flex align-items-center justify-content-center">
                        <span class="w500 red-clr"><?php echo $_GET['name'];?>'s</span> &nbsp;special bonus for QuickFunnel
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-12 text-center mt20">
                        <div class="f-16 f-md-20 w400 black-clr lh140 text-capitalize preheadline relative">
                        Grab QuickFunnels With Great Grand Bonuses Of The Year 2023, Before The Deal Ends...
                           <img src="assets/images/pre-heading-icon.webp" class="img-fluid d-none d-md-block gameimg">
                        </div>
                     </div>
                     <div class="col-12 mt-md50 mt20 f-md-42 f-24 w600 text-center white-clr lh140"> 
                     Have A Sneak Peak -  <span class="red-clr"> How You Can Launch Blazing-Fast Loading Funnels And Websites In Just 7 Minutes </span>  With No Tech Hassles & No Monthly Fee Ever...
                     </div>
                     <div class="col-12 mt-md30 mt20">
                        <img src="assets/images/hr.webp" alt="hr" class="mx-auto img-fluid d-block hr">
                     </div>
                     <div class="col-12 mt20 mt-md30 text-center">
                        <div class="f-18 f-md-20 w400 text-center lh140 white-clr text-capitalize">               
                        Introducing The Industry’s First Visual Funnel Planner, 400+ Beautiful Templates, Free-Flow Editor, And <br class="d-none d-md-block">Inbuilt E-Mail Marketing - All Rolled Into One Easy To Use Application!
                        </div>
                     </div>
                     <div class="col-12 col-md-10 mx-auto mt20 mt-md40">
                     <div class="video-box">
                        <div style="padding-bottom: 56.25%;position: relative;">
                           <iframe src="https://quickfunnel.dotcompal.com/video/embed/occtysjaap" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
                           </div>
      <!-- <div class="header-section-2">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-12 col-md-10 mx-auto mt20 mt-md40">
               <img src="assets/images/product-image.webp" class="img-fluid d-block mx-auto">
               </div>
            </div>
         </div>
      </div> -->
      <!-- Header Section End -->
  <!-- Step Section Start -->
  <div class="step-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-md-45 f-28 w700 lh140 text-center black-clr">
                     Create <span class="red-clr"> Amazing Profit Pulling Websites &amp; Funnels</span> in 3 Easy Steps
                     <img src="assets/images/line.webp" alt="Line" class="mx-auto d-block img-fluid mt5">
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md90">
               <div class="col-12 col-md-4">
                  <div class="steps-block">
                     <img src="assets/images/step1.webp" class="img-fluid mx-auto d-block" alt="Upload">
                     <div class="f-24 f-md-32 w700 black-clr lh150 mt20 mt-md30 text-center">
                        Choose
                     </div>
                     <div class="f-18 w400 black-clr lh150 mt15 text-center">
                        To start, just choose from 400+ ready-to-use, beautiful templates according to your business niche &amp; marketing goals.
                     </div>
                 </div>
               </div>
               <div class="col-12 col-md-4 mt20 mt-md0">
                  <div class="steps-block">
                     <img src="assets/images/step2.webp" class="img-fluid mx-auto d-block" alt="Upload">
                     <div class="f-24 f-md-32 w700 black-clr lh150 mt20 mt-md30 text-center">
                        Edit
                     </div>
                     <div class="f-18 w400 black-clr lh150 mt15 text-center">
                        Revolutionize your funnel creation with our next-generation Drag &amp; Drop page editor and free-flow funnel planner! Zero coding or design skills needed.
                     </div>
                 </div>
               </div>
               <div class="col-12 col-md-4 mt20 mt-md0">
                  <div class="steps-block">
                     <img src="assets/images/step3.webp" class="img-fluid mx-auto d-block" alt="Upload">
                     <div class="f-24 f-md-32 w700 black-clr lh150 mt20 mt-md30 text-center">
                        Publish &amp; Get Paid
                     </div>
                     <div class="f-18 w400 black-clr lh150 mt15 text-center">
                        Now publish your funnel &amp; website that works 24*7 for you &amp; bring automated sales.
                     </div>
                 </div>
               </div>
            </div>
            <div class="row mt20 mt-md90">
               <div class="col-12 f-18 f-md-20 w400 lh140 black-clr text-center">
                  It Just Takes <span class="w600">Minutes</span> to go live… <span class="w600"> No Technical &amp; Designing Skills</span> of Any Kind is Needed!
               </div>
               <div class="col-12 f-md-24 f-20 w400 lh140 black-clr text-center mt15">
                  Plus, with included FREE commercial license, <span class="w600">this is the easiest &amp; fastest way</span> to start 6 figure business and help desperate local businesses in no time!
               </div>
            </div>
         </div>
      </div>
      <!-- Step Section End -->
      <!-- CTA Btn Start-->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w600">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w600 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <!-- <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w600 red-clr">"ATULVIP"</span> for an Additional Discount on Commercial Licence
                  </div> -->
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab QuickFunnel + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                  Use Coupon Code <span class="w600 red-clr">"ATULBUNDLE"</span> for an Additional <span class="w600 red-clr">$30 Discount</span> on Bundle Deal
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="https://quickfunnel.live/bundle/" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab QuickFunnel Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 smmltd">Days</span>
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Sec</span>
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Btn End -->
      <div class="second-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-28 f-md-45 lh140 w400 text-center black-clr">
                     We Guarantee, <span class="w700 red-clr">This Is The Only Funnel &amp; <br class="d-none d-md-block"> Website Builder You’ll Ever Need…</span> 
                  </div>
               </div>
            </div>
            <div class="row mt0 mt-md50">   
               <div class="col-12 col-md-6">
                  <div class="feature-list l-feature-odd">
                     <div class="f-18 f-md-20 lh140 w400">
                       <span class="w600">Create Proven-To-Convert Funnels &amp; Websites</span>  In Just A Few Minutes In Any Niche
                     </div>
                  </div>
                  <div class="feature-list l-feature-even">
                     <div class="f-18 f-md-20 lh140 w400">
                       <span class="w600">Build Any Type Of Funnel– </span> Sales Funnel, Lead Funnel, Webinar And Product Launches Funnel
                     </div>
                  </div>
                  <div class="feature-list l-feature-odd">
                     <div class="f-18 f-md-20 lh140 w400">
                        <span class="w600">400+ High Converting &amp; Ready-To-Go Templates</span>
                     </div>
                  </div>
                  <div class="feature-list l-feature-even">
                     <div class="f-18 f-md-20 lh140 w400">
                       <span class="w600">Mobile Friendly &amp; Fast Loading Pages-</span> Never Lose A Single Visitor, Lead Or Sale.
                     </div>
                  </div>
                  <div class="feature-list l-feature-odd">
                     <div class="f-18 f-md-20 lh140 w400">
                       <span class="w600">Inbuilt Email Marketing &amp; Lead Management–</span>  Collect Unlimited Leads and Send Unlimited Emails
                     </div>
                  </div>
                  <div class="feature-list l-feature-even">
                     <div class="f-18 f-md-20 lh140 w400">
                       <span class="w600">SEO Friendly &amp; Social Media Optimized Pages</span>  For More Traffic.
                     </div>
                  </div>
                  <div class="feature-list l-feature-odd">
                     <div class="f-18 f-md-20 lh140 w400">
                       <span class="w600">Precise Analytics </span> To Measure &amp; Make The Right Decisions For Future Success!
                     </div>
                  </div>
                  <div class="feature-list l-feature-even">
                     <div class="f-18 f-md-20 lh140 w400">
                       <span class="w600">Automatic SSL Encryption-</span> 100% Unbreakable Security
                     </div>
                  </div>
                  <div class="feature-list l-feature-odd">
                     <div class="f-18 f-md-20 lh140 w600">
                        PLUS, YOU’LL ALSO RECEIVE FREE COMMERCIAL LICENCE WHEN YOU GET STARTED TODAY!
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-6">
                  <div class="f-18 lh140 w400">                             
                     <div class="feature-list r-feature-odd">
                        <div class="f-18 f-md-20 lh140 w600">
                           Industry’s First, Fully Drag &amp; Drop &amp; Visual Funnel Designer
                        </div>
                     </div>
                     <div class="feature-list r-feature-even">
                        <div class="f-18 f-md-20 lh140 w400">
                          <span class="w600">Create Any Type Of Landing Page -</span> Sales Page, Lead Page, Membership, Bonus, or Product Review Page
                        </div>
                     </div>
                     <div class="feature-list r-feature-odd">
                        <div class="f-18 f-md-20 lh140 w400">
                           <span class="w600">Next-Gen Drag And Drop Editor</span> To Create Pixel Perfect Pages Or Templates From Scratch 
                        </div>
                     </div>
                     <div class="feature-list r-feature-even">
                        <div class="f-18 f-md-20 lh140 w400">
                           User Friendly Business Central Dashboard- <span class="w600">Everything You Need, Under One Roof At Your Fingertips!</span> 
                        </div>
                     </div>
                     <div class="feature-list r-feature-odd">
                        <div class="f-18 f-md-20 lh140 w400">
                          <span class="w600">Cutting-Edge Autoresponder Integration-</span> Use Any Email Autoresponder Without Any Technical Glitches
                        </div>
                     </div>
                     <div class="feature-list r-feature-even">
                        <div class="f-18 f-md-20 lh140 w400">
                          <span class="w600">Grab Maximum Leads -</span> Show Pop-Ups Inside Landing Pages And Funnels Pages
                        </div>
                     </div>
                     <div class="feature-list r-feature-odd">
                        <div class="f-18 f-md-20 lh140 w600">
                           Unmatched Webinar Set &amp; Forget Integration
                        </div>
                     </div>
                     <div class="feature-list r-feature-even">
                        <div class="f-18 f-md-20 lh140 w400">
                          <span class="w600"> No Expensive Domains &amp; Hosting Services-</span> Host Everything On Our Lightning Fast Server.
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <section class="didyouknow-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-12 text-center">
                  <div class="f-28 f-md-45 w700 white-clr text-center lh140 head-design mt0 mt-md50">Do Funnels Still Work In 2023?</div>
                  <div class="f-28 f-md-100 w700 lh140 mt20 mt-md20">
                    <span class="black-clr"><img src="assets/images/thumbsup-smily.webp" alt="Thumbsup Smily"></span> <span class="red-clr">Yep,</span> <br>
                  </div>
                  <div class="f-24 f-md-28 w400 lh140 black-clr mt20 demo-text">
                     Here’s the 1 exact funnel that <span class="w600">got more than 32,000 visitors with 1,715 sales <br class="d-none d-md-block"> and total $156,837 in sales for one of my products.</span> 
                  </div>  
                  <div class="mt50 mt-md90">
                     <img src="assets/images/siteefy.webp" alt="Siteefy" class="img-fluid mx-auto d-block">
                  </div>
               </div>
            </div>
            <div class="row align-items-center mt20 mt-md70">
               <div class="col-12 col-md-12 text-center">
                  <div class="f-24 f-md-28 w600 lh140 black-clr mt20 demo-text">
                     And by using the power of funnels, we have made 651K in sales <br class="d-none d-md-block"> &amp; 378K in affiliate commissions in the last 10 months.
                  </div>  
                  <div class="mt50 mt-md90">
                     <img src="assets/images/website-process.webp" alt="Siteefy" class="img-fluid mx-auto d-block">
                  </div>
               </div>
            </div>
            <div class="didyouknow-box mt20 mt-md70">
               <div class="row">
                  <div class="col-12">
                     <div class="f-18 f-md-20 lh150 w400 black-clr">
                        Listen, if you’re going to compete and get the life you desire in this ever-growing digital marketing era, then you’re going to <span class="w600"> <u>need an online presence that converts.</u></span> 
                        <br><br>
                        That means you’ll need websites with fast pages that guide, engage, and convert visitors into customers. <span class="w600">And that’s what funnels do. </span> 
                        <br><br>
                       <span class="w600">You only need to setup a funnel once and it delivers RESULTS again and again on complete autopilot!</span> 
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
  <!-- CTA Btn Start-->
  <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w600">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w600 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <!-- <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w600 red-clr">"ATULVIP"</span> for an Additional  Discount on Commercial Licence
                  </div> -->
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab QuickFunnel + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                  Use Coupon Code <span class="w600 red-clr">"ATULBUNDLE"</span> for an Additional <span class="w600 red-clr">$30 Discount</span> on Bundle Deal
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="https://quickfunnel.live/bundle/" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab QuickFunnel Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 smmltd">Days</span>
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Sec</span>
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Btn End -->

      <div class="testimonial-section">
         <div class="container ">
            <div class="row ">
               <!-- <div class="col-12 f-24 f-md-28 w400 lh140 black-clr text-center ">
                  And it’s not just us. We also have <span class="w600">400+ TOP marketers</span> and more than <span class="w600">1000 customers</span> are successfully using it and ABSOLUTELY loving it.
               </div> -->
               <div class="col-12 f-28 f-md-45 w700 lh140 black-clr text-center">
                  <span class="red-clr">Checkout What Marketers &amp; Early Users</span>  Have to Say About QuickFunnel
               </div>
            </div>
            <div class="row mt20 mt-md80 align-items-center">
               <div class="col-md-6">
                  <div class="responsive-video border-video">
                     <iframe src="https://quickfunnel.dotcompal.com/video/embed/ccb2pf2a7f" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                  </div>
                  <div class="f-20 f-md-24 w600 lh140 black-clr text-center mt20 mt-md50">James Milo</div> 
                  <div class="f-18 f-md-20 w400 lh140 black-clr text-center mt10">21st Century Expert.com &amp; Video Animation Network&nbsp;</div>   
               </div>
               <div class="col-md-6 mt20 mt-md0">
                  <div class="responsive-video border-video">
                     <iframe src="https://quickfunnel.dotcompal.com/video/embed/pw7r36cz1j" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                  </div>
                  <div class="f-20 f-md-24 w600 lh140 black-clr text-center mt20 mt-md50">Craig McIntosh&nbsp;</div> 
                  <div class="f-18 f-md-20 w400 lh140 black-clr text-center mt10">Digital Marketing Consultant&nbsp;</div>  
               </div>
            </div>
            <!-- <div class="row mt20 mt-md80 align-items-center">
               <div class="col-md-6 order-md-2">
                  <div class="responsive-video border-video">
                     <iframe src="https://quickfunnel.dotcompal.com/video/embed/pw7r36cz1j" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important; box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                  </div>
               </div>
               <div class="col-md-6 mt20 mt-md0 order-md-1"> 
                  <div class="f-20 f-md-24 w600 lh140 black-clr text-center text-md-start">Craig McIntosh </div> 
                  <div class="f-18 f-md-20 w400 lh140 black-clr text-center text-md-start mt10">Digital Marketing Consultant </div>  
               </div>
            </div> -->

            <div class="row row-cols-md-2 row-cols-1 gx4 mt80">
               <div class="col mt50">
                  <div class="single-testimonial">
                     <div class="st-img">
                        <img src="assets/images/imreviw.webp" class="img-fluid d-block mx-auto" alt="Imreviw">
                     </div>
                     <div class="mt20 md-md50 f-24 f-md-28 w600 lh140 black-clr"> IMReview Squad </div>
                     <div class="stars mt10">
                        <a href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                     </div>
                     <img src="assets/images/quote.webp" alt="Quotes" class="mx-auto d-block img-fluid mt20 mt-md50">
                     <p class="mt20 f-18 f-md-20 lh140 w400 text-center">
                        Wow!!! It’s an amazing creation that allows me to build high-converting pages and funnels EASY &amp; FAST.
                        Apart from its features, the best part I personally love is that it comes at a one-time price for the next few days…Complete value for your money. Guys go for it…
                     </p>
                  </div>
               </div>
               <div class="col mt120 mt-md50">
                  <div class="single-testimonial">
                     <div class="st-img">
                        <img src="assets/images/samuel.webp" class="img-fluid d-block mx-auto" alt="Samuel Marco">
                     </div>
                     <div class="mt20 md-md50 f-24 f-md-28 w600 lh140 black-clr"> Samuel Marco  </div>
                     <div class="stars mt10">
                        <a href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                        <a href="#"><i class="fa fa-star"></i></a>
                     </div>
                     <img src="assets/images/quote.webp" alt="Quotes" class="mx-auto d-block img-fluid mt20 mt-md50">
                     <p class="mt20 f-18 f-md-20 lh140 w400 text-center">
                        QuickFunnel is next-level quality software. It's so easy to use, and the training included makes it even easier to build a page or funnel in any niche in a few minutes.
                        <br><br>
                        I love the fact that you can create high-converting sales funnels and pages without actually being a
                        technical nerd...all without any special skills. I say that this is a MUST-HAVE software for marketers! …
                     </p>
                  </div>
               </div>
            </div>

            <div id="features"></div>
         </div>
      </div>

      <div class="presenting-sec">
         
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-50 w600 red-clr lh140 presenting-head">
                     Proudly Presenting…
                  </div>
                  <div class="col-12 mt20 mt-md50">
                     <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 693.1 109.7" style="enable-background:new 0 0 693.1 109.7; height:45px;" xml:space="preserve">
                        <style type="text/css">
                           .st0{fill:#FFFFFF;}
                           .st1{fill:#EF3E39;}
                           .st2{fill:#853594;}
                           .st3{fill:#EF3E3A;}
                        </style>
                        <g>
                           <g id="Layer_1-2">
                              <path class="st0" d="M140.4,0H18.9c-2.8-0.1-5.2,2.1-5.4,4.9s2.1,5.2,4.9,5.4c0.2,0,0.3,0,0.5,0h42.3c2.8,0,5.1,2.3,5.1,5.1
                              c0,2.8-2.3,5.1-5.1,5.1h-56c-2.8,0.1-5,2.5-4.9,5.4c0.1,2.7,2.3,4.8,4.9,4.9h35.6l0,0h13.5c2.8,0.1,5,2.5,4.9,5.4
                              c-0.1,2.7-2.3,4.8-4.9,4.9H46c-2.8,0-5.1,2.4-5.1,5.2c0,2.7,2.1,4.9,4.8,5.1h17.8c2.8,0,5.2,2.2,5.2,5.1c0,2.8-2.2,5.2-5.1,5.2h-3
                              c-1.3,0-2.6,0.6-3.5,1.5c-2,2-2,5.2-0.1,7.3c1,1,2.3,1.5,3.7,1.5h8v37.7L103.1,96V68.6l42.8-57.6c2.3-3,1.6-7.3-1.4-9.6
                              C143.3,0.5,141.9,0,140.4,0z M66.2,36c0-2.8,2.3-5.1,5.1-5.1h10c2.8,0.1,5,2.5,4.9,5.4c-0.1,2.7-2.3,4.8-4.9,4.9h-10
                              C68.5,41.1,66.2,38.8,66.2,36z"></path>
                              <path class="st1" d="M34,46.3c0,2.8-2.3,5.1-5.1,5.1h-10c-2.8,0-5.1-2.3-5.1-5.1c0-2.8,2.3-5.1,5.1-5.1c0,0,0,0,0,0h10
                              C31.7,41.2,34,43.5,34,46.3z"></path>
                              <path class="st1" d="M48.8,66.9c0,2.8-2.3,5.1-5.1,5.1h-2.9c-2.8,0-5.1-2.1-5.3-4.8c-0.1-1.5,0.4-3,1.5-4c0.9-0.9,2.2-1.5,3.5-1.5
                              h3.4C46.6,61.8,48.9,64.1,48.8,66.9z"></path>
                              <path class="st2" d="M45.8,61.7L45.8,61.7z"></path>
                              <path class="st2" d="M63.7,61.7L63.7,61.7z"></path>
                              <path class="st2" d="M63.7,61.7L63.7,61.7z"></path>
                              <path class="st3" d="M230,48.3c0,4.5-0.7,8.9-2.3,13.1c-1.5,3.9-3.7,7.5-6.5,10.5c-2.8,3-6.1,5.4-9.9,7c-8,3.3-17,3.3-24.9,0
                              c-3.7-1.6-7.1-4-9.8-7c-2.8-3.1-5-6.7-6.4-10.5c-1.6-4.2-2.4-8.7-2.3-13.1c0-4.5,0.7-9,2.3-13.2c1.4-3.9,3.6-7.5,6.4-10.5
                              c2.8-3,6.1-5.4,9.8-7c8-3.3,17-3.3,24.9,0c3.7,1.6,7.1,4,9.9,7c2.8,3.1,5,6.6,6.5,10.5C229.3,39.3,230,43.8,230,48.3z M222.1,48.3
                              c0.1-4.6-0.9-9.2-3-13.4c-1.9-3.7-4.7-6.9-8.2-9.1c-7.4-4.4-16.6-4.4-24,0c-3.5,2.3-6.3,5.4-8.2,9.1c-2.1,4.2-3.1,8.7-3,13.4
                              c-0.1,4.6,1,9.2,3,13.3c1.8,3.7,4.7,6.9,8.2,9.2c7.4,4.4,16.6,4.4,24,0c3.5-2.3,6.3-5.4,8.2-9.2C221.1,57.5,222.1,52.9,222.1,48.3
                              z M233.1,84.8c1.9,0.1,3.4,1.6,3.4,3.5c-0.1,1.3-0.7,2.5-1.7,3.4c-1.3,1.2-2.8,2.2-4.4,2.9c-1.8,0.9-3.7,1.5-5.7,2
                              c-1.8,0.5-3.7,0.7-5.6,0.7c-3.1,0.1-6.2-0.3-9.2-1.2c-2.1-0.7-4.2-1.5-6.2-2.6c-1.8-1-3.5-1.9-5.2-2.6c-1.9-0.8-3.9-1.2-6-1.2
                              c-1,0-2,0.2-2.9,0.6c-0.8,0.4-1.6,0.7-2.3,1.1c-0.8,0.4-1.7,0.6-2.6,0.6c-1,0-2-0.4-2.7-1.1c-0.6-0.7-1-1.6-1-2.5
                              c-0.1-1.4,0.8-2.8,2.1-3.2L200,79l8.4,0.8l-15.7,5.5l2.6-1.9c2.2,0,4.4,0.3,6.4,1.1c1.9,0.7,3.7,1.5,5.5,2.5
                              c1.8,0.9,3.6,1.8,5.5,2.5c2.1,0.8,4.4,1.1,6.6,1.1c1.8,0,3.7-0.3,5.4-1c1.2-0.5,2.4-1.2,3.6-1.9c0.8-0.6,1.7-1.3,2.4-2
                              C231.3,85.1,232.1,84.8,233.1,84.8L233.1,84.8z"></path>
                              <path class="st3" d="M277.1,32.9c1,0,2,0.4,2.7,1.1c0.7,0.7,1,1.7,1,2.6v25.1c0,6.2-1.7,11-5.2,14.4s-8.3,5.1-14.5,5.1
                              c-6.2,0-11-1.7-14.4-5.1s-5.2-8.2-5.2-14.4V36.6c0-2,1.5-3.7,3.5-3.7c0,0,0.1,0,0.1,0c1,0,2,0.4,2.7,1.1c0.7,0.7,1,1.7,1,2.6v25.1
                              c0,4.2,1.1,7.3,3.2,9.5s5.2,3.2,9,3.2c4,0,7.1-1.1,9.2-3.2s3.2-5.3,3.2-9.5V36.6c0-2,1.5-3.7,3.5-3.7
                              C277,32.9,277,32.9,277.1,32.9L277.1,32.9z"></path>
                              <path class="st3" d="M297.9,26c-1.2,0.1-2.4-0.2-3.4-0.9c-0.8-0.7-1.2-1.8-1.1-2.9v-1.3c-0.1-1.1,0.4-2.2,1.2-2.9
                              c1-0.7,2.2-1,3.4-0.9c1.2-0.1,2.4,0.2,3.3,0.9c0.8,0.7,1.2,1.8,1.1,2.9v1.3c0.1,1.1-0.3,2.1-1.1,2.9
                              C300.2,25.8,299.1,26.1,297.9,26z M301.5,76.8c0,1-0.4,2-1.1,2.7c-1.4,1.5-3.8,1.5-5.3,0c0,0,0,0,0,0c-0.7-0.7-1-1.7-1-2.7V35.9
                              c-0.1-2,1.5-3.8,3.5-3.9s3.8,1.5,3.9,3.5c0,0.1,0,0.3,0,0.4L301.5,76.8z"></path>
                              <path class="st3" d="M336,31.7c2.9,0,5.7,0.3,8.5,1c2.2,0.5,4.2,1.5,6,2.8c1.3,0.9,2.2,2.4,2.2,4c0,0.8-0.3,1.6-0.8,2.3
                              c-0.5,0.7-1.3,1.1-2.2,1.1c-0.7,0-1.4-0.1-2.1-0.5c-0.5-0.3-1-0.7-1.5-1.2c-0.5-0.5-1.2-1-1.9-1.3c-1-0.5-2.1-0.8-3.2-1
                              c-1.4-0.2-2.8-0.4-4.2-0.4c-3.1-0.1-6.2,0.8-8.9,2.4c-2.5,1.6-4.6,3.8-6,6.5c-1.5,2.8-2.3,6-2.2,9.2c-0.1,3.2,0.7,6.4,2.1,9.2
                              c1.4,2.6,3.4,4.9,5.9,6.4c2.6,1.6,5.7,2.5,8.8,2.4c1.7,0,3.5-0.1,5.2-0.6c1.1-0.3,2.2-0.7,3.2-1.3c0.9-0.5,1.8-1.1,2.5-1.9
                              c1.4-1.3,3.5-1.2,4.8,0.1c0.6,0.7,0.9,1.6,0.9,2.5c0,1.1-0.8,2.3-2.3,3.5c-1.9,1.4-4,2.5-6.2,3.1c-2.9,0.9-5.9,1.3-8.9,1.3
                              c-4.3,0.1-8.6-1-12.3-3.3c-3.5-2.2-6.3-5.3-8.2-9c-2-3.9-3-8.3-2.9-12.7c-0.1-4.4,1-8.7,3-12.6C319.3,36.2,327.3,31.5,336,31.7z"></path>
                              <path class="st3" d="M367.1,80.6c-1,0-2-0.4-2.7-1.1c-0.7-0.7-1-1.7-1-2.7V16c0-1,0.3-1.9,1-2.6c0.7-0.7,1.7-1.1,2.7-1.1
                              c2,0,3.7,1.5,3.7,3.5c0,0,0,0.1,0,0.1v60.8c0,1-0.4,2-1.1,2.7C369,80.2,368.1,80.6,367.1,80.6z M397.7,32.6c1,0,1.9,0.4,2.5,1.2
                              c1.3,1.2,1.4,3.2,0.2,4.6c-0.2,0.2-0.4,0.4-0.6,0.5L370,65l-0.3-8.6l25.5-22.7C395.8,33,396.7,32.6,397.7,32.6z M398.7,80.5
                              c-1,0-2-0.4-2.7-1.2l-19.7-20.9l5.5-5.1l19.5,20.8c0.7,0.7,1.1,1.7,1.1,2.8c0,1-0.4,2-1.3,2.6C400.4,80.1,399.6,80.4,398.7,80.5
                              L398.7,80.5z"></path>
                              <path class="st0" d="M419,80.6c-1.6,0.1-3.1-0.5-4.2-1.6c-1.1-1.1-1.6-2.5-1.6-4V21.7c-0.1-3,2.3-5.6,5.3-5.7c0.1,0,0.2,0,0.3,0
                              h30c1.5,0,2.9,0.5,4,1.5c1.1,1,1.7,2.4,1.6,3.9c0,1.4-0.6,2.7-1.6,3.6c-1.1,1-2.5,1.6-4,1.6h-24.9l0.8-1.1v18.6l-0.6-1.2h20.6
                              c1.5,0,2.9,0.5,4,1.5c1.1,1,1.7,2.4,1.6,3.9c0,1.4-0.6,2.7-1.6,3.6c-1.1,1-2.5,1.6-4,1.6h-20.8l0.8-0.8V75c0,1.5-0.6,3-1.7,4
                              C421.9,80,420.5,80.6,419,80.6z"></path>
                              <path class="st0" d="M499.3,31.3c1.5-0.1,3,0.5,4,1.6c1,1.1,1.6,2.5,1.6,4v23.4c0,6.5-1.8,11.7-5.4,15.5
                              c-3.6,3.8-8.9,5.7-15.7,5.7c-6.8,0-12-1.9-15.6-5.7s-5.4-9-5.4-15.5V36.9c0-1.5,0.5-2.9,1.6-4c2.2-2.2,5.7-2.2,7.9,0
                              c1,1.1,1.6,2.5,1.6,4v23.4c0,3.8,0.8,6.5,2.5,8.3s4.1,2.7,7.5,2.7s5.9-0.9,7.6-2.7s2.5-4.6,2.5-8.3V36.9c0-1.5,0.5-2.9,1.6-4
                              C496.4,31.8,497.8,31.2,499.3,31.3z"></path>
                              <path class="st0" d="M543.2,30.3c4.6,0,8,1,10.4,2.9c2.4,1.9,4.1,4.6,4.9,7.6c0.9,3.4,1.4,7,1.3,10.6V75c0,1.5-0.5,2.9-1.6,4
                              c-2.2,2.2-5.7,2.2-7.9,0c-1-1.1-1.6-2.5-1.6-4V51.3c0-1.9-0.2-3.7-0.8-5.5c-0.5-1.6-1.5-2.9-2.8-3.9c-1.7-1.1-3.8-1.6-5.8-1.5
                              c-2.2-0.1-4.3,0.4-6.2,1.5c-1.6,0.9-3,2.3-3.9,3.9c-0.9,1.7-1.4,3.6-1.3,5.5V75c0,1.5-0.5,2.9-1.6,4c-2.2,2.2-5.7,2.2-7.9,0
                              c-1-1.1-1.6-2.5-1.6-4V36.9c0-1.5,0.5-2.9,1.6-4c2.2-2.2,5.7-2.2,7.9,0c1,1.1,1.6,2.5,1.6,4v4l-1.4-0.3c0.7-1.2,1.5-2.3,2.4-3.4
                              c1.1-1.3,2.3-2.4,3.7-3.4c1.5-1.1,3.1-1.9,4.8-2.5C539.2,30.7,541.2,30.3,543.2,30.3z"></path>
                              <path class="st0" d="M598.1,30.3c4.5,0,8,1,10.4,2.9c2.4,2,4.1,4.6,4.9,7.6c0.9,3.4,1.4,7,1.3,10.6V75c0,1.5-0.5,2.9-1.6,4
                              c-2.2,2.2-5.7,2.2-7.9,0c-1-1.1-1.6-2.5-1.6-4V51.3c0-1.9-0.2-3.7-0.8-5.5c-0.5-1.6-1.5-2.9-2.8-3.9c-1.7-1.1-3.8-1.6-5.8-1.5
                              c-2.2-0.1-4.3,0.4-6.3,1.5c-1.6,0.9-3,2.3-3.9,3.9c-0.9,1.7-1.4,3.6-1.3,5.5V75c0,1.5-0.5,2.9-1.6,4c-2.2,2.2-5.7,2.2-7.9,0
                              c-1-1.1-1.6-2.5-1.6-4V36.9c0-1.5,0.5-2.9,1.6-4c2.2-2.2,5.7-2.2,7.9,0c1,1.1,1.6,2.5,1.6,4v4l-1.4-0.3c0.7-1.2,1.5-2.3,2.4-3.4
                              c1.1-1.3,2.3-2.4,3.7-3.4c1.5-1.1,3.1-1.9,4.8-2.5C594.2,30.7,596.1,30.3,598.1,30.3z"></path>
                              <path class="st0" d="M649.9,81.5c-4.7,0.1-9.4-1-13.6-3.3c-3.7-2.1-6.8-5.2-8.8-8.9c-2.1-3.9-3.2-8.3-3.1-12.7
                              c-0.1-4.9,1-9.8,3.4-14.2c2.1-3.7,5.1-6.8,8.8-9c3.5-2,7.5-3.1,11.5-3.1c3.1,0,6.1,0.6,8.9,1.9c2.8,1.3,5.3,3.1,7.4,5.3
                              c2.2,2.3,3.9,4.9,5.1,7.8c1.3,3,1.9,6.2,1.9,9.4c0,1.4-0.7,2.7-1.8,3.6c-1.1,0.9-2.5,1.4-3.9,1.4h-35.2l-2.8-9.2h33.8l-2,1.8v-2.5
                              c-0.1-1.8-0.8-3.4-1.9-4.8c-1.1-1.4-2.6-2.6-4.2-3.4c-1.7-0.8-3.5-1.3-5.4-1.3c-1.8,0-3.5,0.2-5.2,0.7c-1.6,0.5-3,1.3-4.1,2.5
                              c-1.3,1.3-2.3,2.9-2.8,4.7c-0.7,2.4-1.1,4.9-1,7.5c-0.1,2.9,0.6,5.9,2.1,8.4c1.3,2.2,3.1,4,5.3,5.3c2.1,1.2,4.6,1.8,7,1.8
                              c1.8,0,3.7-0.1,5.4-0.6c1.1-0.3,2.3-0.7,3.3-1.3c0.8-0.5,1.6-1,2.3-1.3c1-0.5,2-0.8,3.1-0.8c2.6,0,4.7,2,4.7,4.6c0,0,0,0,0,0.1
                              c-0.1,1.8-1,3.5-2.6,4.5c-2,1.6-4.3,2.8-6.8,3.6C656,81,653,81.5,649.9,81.5z"></path>
                              <path class="st0" d="M693.1,75c0.1,3.1-2.4,5.6-5.4,5.6c-0.1,0-0.1,0-0.2,0c-1.5,0-2.9-0.6-3.9-1.6c-1.1-1.1-1.6-2.5-1.6-4V18
                              c-0.1-3.1,2.4-5.6,5.4-5.6c0.1,0,0.1,0,0.2,0c1.5-0.1,2.9,0.5,3.9,1.6c1,1.1,1.6,2.5,1.5,4V75z"></path>
                           </g>
                        </g>
                     </svg>
                  </div>
                  <div class="col-12 mt-md50 mt20 f-md-50 f-24 w600 text-center white-clr lh140">
                     Launch <span class="red-clr">Blazing-Fast Loading Funnels And Websites In Just 7 Minutes</span> With No Tech Hassles &amp; No Monthly Fee Ever…
                 </div>
                 <div class="col-12 mt20 mt-md30 lh140 f-18 f-md-20 w500 white-clr">
                  FREE Commercial License Included to Build an Incredible Income 
                 </div>
               </div>
            </div>
            <div class="row mt20 mt-md40 align-items-center">
               <div class="col-md-7 col-12 mx-auto">
                  <img src="assets/images/product-image.webp" class="img-fluid d-block mx-auto">
               
                     <!-- <div class="video-box">
                        <div style="padding-bottom: 56.25%;position: relative;">
                           <iframe src="https://restrosuite.dotcompal.com/video/embed/67fmqio11q" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                        </div>
                     </div> -->
               </div>
               <div class="col-md-5 col-12 mt30 mt-md0">
                  <div class="proudly-features">
                     <ul class="pl0 m0 f-18 f-md-20 lh150 w400 white-clr">
                       <li> <span class="red-clr caveat w700 f-24 f-md-30">Quick &amp; Easy</span> <br> Create Lightning-Fast Funnels Easily with Free-Flow Funnel Planner &amp; Drag-N-Drop Editor </li>
                       <li> <span class="red-clr caveat w700 f-24 f-md-30">Maximize Results</span> <br> Get Max Engagement, Max Leads &amp; Max Sales with High converting Funnels &amp; Pages  </li>
                       <li> <span class="red-clr caveat w700 f-24 f-md-30">Reliable &amp; Robust</span> <br> Battle-Tested Architecture to Handle Huge Load without Any Hassles</li>
                       <li> <span class="red-clr caveat w700 f-24 f-md-30">Proven &amp; Elegant</span> <br> Empowered with 400+ Proven Converting &amp; Elegant Templates </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
   </div>

      
      <!-------Exclusive Bonus----------->
      <div class="exclusive-bonus">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                  <div class="white-clr f-24 f-md-36 lh140 w500">
                     Exclusive Bonus #1 : Trendio
                  </div>
                  <div class="f-18 f-md-20 w500 mt20 white-clr">
                     20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="tr-header-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row">
                     <div class="col-md-3 text-center mx-auto">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 912.39 246.83" style="enable-background:new 0 0 912.39 246.83; max-height:50px;color: #023047;" xml:space="preserve">
                           <style type="text/css">
                              .st0{fill:#FFFFFF;}
                              .st1{opacity:0.3;}
                              .st2{fill:url(#SVGID_1_);}
                              .st3{fill:url(#SVGID_00000092439463549223389810000006963263784374171300_);}
                              .st4{fill:url(#SVGID_00000015332009897132114970000017423936041397956233_);}
                              .st5{fill:#023047;}
                           </style>
                           <g>
                              <g>
                                 <path class="st0" d="M18.97,211.06L89,141.96l2.57,17.68l10.86,74.64l2.12-1.97l160.18-147.9l5.24-4.84l6.8-6.27l5.24-4.85 l-25.4-27.51l-12.03,11.11l-5.26,4.85l-107.95,99.68l-2.12,1.97l-11.28-77.58l-2.57-17.68L0,177.17c0.31,0.72,0.62,1.44,0.94,2.15 c0.59,1.34,1.2,2.67,1.83,3.99c0.48,1.03,0.98,2.06,1.5,3.09c0.37,0.76,0.76,1.54,1.17,2.31c2.57,5.02,5.43,10,8.57,14.93 c0.58,0.89,1.14,1.76,1.73,2.65L18.97,211.06z"></path>
                              </g>
                              <g>
                                 <g>
                                    <polygon class="st0" points="328.54,0 322.97,17.92 295.28,106.98 279.91,90.33 269.97,79.58 254.51,62.82 244.58,52.05 232.01,38.45 219.28,24.66 "></polygon>
                                 </g>
                              </g>
                           </g>
                           <g class="st1">
                              <g>
                                 <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="17.428" y1="42.82" x2="18.9726" y2="42.82" gradientTransform="matrix(1 0 0 -1 0 252.75)">
                                    <stop offset="0" style="stop-color:#000000"></stop>
                                    <stop offset="1" style="stop-color:#333333"></stop>
                                 </linearGradient>
                                 <path class="st2" d="M17.43,208.8l1.54,2.26c-0.37-0.53-0.73-1.05-1.09-1.58c-0.02-0.02-0.02-0.03-0.03-0.03 c-0.06-0.09-0.11-0.17-0.17-0.27C17.6,209.06,17.51,208.92,17.43,208.8z"></path>
                                 <linearGradient id="SVGID_00000177457867953882822120000005144526232562103955_" gradientUnits="userSpaceOnUse" x1="18.98" y1="70.45" x2="91.57" y2="70.45" gradientTransform="matrix(1 0 0 -1 0 252.75)">
                                    <stop offset="0" style="stop-color:#000000"></stop>
                                    <stop offset="1" style="stop-color:#333333"></stop>
                                 </linearGradient>
                                 <path style="fill:url(#SVGID_00000177457867953882822120000005144526232562103955_);" d="M89,141.96l2.57,17.68l-63.83,63 c-1.19-1.45-2.34-2.9-3.46-4.35c-1.84-2.4-3.6-4.81-5.3-7.22L89,141.96z"></path>
                                 <linearGradient id="SVGID_00000010989203515549635820000018393619450353154703_" gradientUnits="userSpaceOnUse" x1="104.55" y1="120.005" x2="333.22" y2="120.005" gradientTransform="matrix(1 0 0 -1 0 252.75)">
                                    <stop offset="0" style="stop-color:#000000"></stop>
                                    <stop offset="1" style="stop-color:#333333"></stop>
                                 </linearGradient>
                                 <polygon style="fill:url(#SVGID_00000010989203515549635820000018393619450353154703_);" points="333.22,15.61 299.96,122.58 274.65,95.18 107.11,249.88 104.55,232.31 264.73,84.41 269.97,79.58 279.91,90.33 295.28,106.98 322.97,17.92 "></polygon>
                              </g>
                           </g>
                           <g>
                              <g>
                                 <g>
                                    <path class="st5" d="M229.46,241.94c-12.28,5.37-23.49,8.06-33.57,8.06c-9.63,0-18.21-2.44-25.71-7.35 c-23.05-15.07-23.72-46.17-23.72-49.67V61.67h32.2v30h39.27v32.2h-39.27v69.04c0.07,4.46,1.88,18.1,9.2,22.83 c5.51,3.55,15.7,2.38,28.68-3.3L229.46,241.94z"></path>
                                 </g>
                              </g>
                           </g>
                           <g>
                              <g>
                                 <path class="st5" d="M250.4,164.29c0-15.97-0.24-27.35-0.97-38h25.9l0.97,22.51h0.97c5.81-16.7,19.61-25.17,32.19-25.17 c2.9,0,4.6,0.24,7.02,0.73v28.08c-2.42-0.48-5.08-0.97-8.71-0.97c-14.28,0-23.96,9.2-26.63,22.51c-0.48,2.66-0.97,5.81-0.97,9.2 v61H250.4V164.29z"></path>
                              </g>
                           </g>
                           <g>
                              <g>
                                 <path class="st5" d="M459.28,161.39c0-13.55-0.24-24.93-0.97-35.1h26.14l1.45,17.67h0.73c5.08-9.2,17.91-20.33,37.52-20.33 c20.57,0,41.87,13.31,41.87,50.59v69.95h-29.77v-66.56c0-16.94-6.29-29.77-22.51-29.77c-11.86,0-20.09,8.47-23.24,17.43 c-0.97,2.66-1.21,6.29-1.21,9.68v69.23h-30.01V161.39z"></path>
                              </g>
                           </g>
                           <g>
                              <g>
                                 <path class="st5" d="M706.41,72.31V211c0,12.1,0.48,25.17,0.97,33.16h-26.63l-1.21-18.64h-0.48c-7.02,13.07-21.3,21.3-38.49,21.3 c-28.08,0-50.35-23.96-50.35-60.27c-0.24-39.45,24.45-62.93,52.77-62.93c16.22,0,27.84,6.78,33.16,15.49h0.48v-66.8 C676.63,72.31,706.41,72.31,706.41,72.31z M676.64,175.43c0-2.42-0.24-5.33-0.73-7.75c-2.66-11.62-12.1-21.06-25.66-21.06 c-19.12,0-29.77,16.94-29.77,38.97c0,21.54,10.65,37.28,29.53,37.28c12.1,0,22.75-8.23,25.66-21.06c0.73-2.66,0.97-5.57,0.97-8.71 V175.43z"></path>
                              </g>
                           </g>
                           <path class="st0" d="M769.68,89.95c-0.11-0.53-0.24-1.04-0.39-1.55c-0.12-0.39-0.25-0.76-0.39-1.14c-0.12-0.32-0.25-0.64-0.39-0.95 c-0.12-0.27-0.25-0.54-0.39-0.81c-0.12-0.24-0.25-0.47-0.39-0.7c-0.12-0.21-0.25-0.42-0.39-0.63c-0.13-0.19-0.26-0.38-0.39-0.57 c-0.13-0.17-0.26-0.35-0.39-0.51s-0.26-0.32-0.39-0.47s-0.26-0.3-0.39-0.44s-0.26-0.28-0.39-0.41c-0.13-0.13-0.26-0.25-0.39-0.37 s-0.26-0.23-0.39-0.35c-0.13-0.11-0.26-0.22-0.39-0.33c-0.13-0.1-0.26-0.2-0.39-0.3c-0.13-0.1-0.26-0.19-0.39-0.28 s-0.26-0.18-0.39-0.27c-0.13-0.08-0.26-0.16-0.39-0.24c-0.13-0.08-0.26-0.16-0.39-0.24c-0.13-0.07-0.26-0.14-0.39-0.21 s-0.26-0.14-0.39-0.2s-0.26-0.13-0.39-0.19c-0.13-0.06-0.26-0.12-0.39-0.18s-0.26-0.11-0.39-0.17c-0.13-0.05-0.26-0.1-0.39-0.15 s-0.26-0.1-0.39-0.14s-0.26-0.09-0.39-0.13s-0.26-0.08-0.39-0.12s-0.26-0.07-0.39-0.11c-0.13-0.03-0.26-0.07-0.39-0.1 s-0.26-0.06-0.39-0.09s-0.26-0.05-0.39-0.08s-0.26-0.05-0.39-0.07s-0.26-0.04-0.39-0.06s-0.26-0.04-0.39-0.06s-0.26-0.03-0.39-0.04 s-0.26-0.03-0.39-0.04s-0.26-0.02-0.39-0.03s-0.26-0.02-0.39-0.03s-0.26-0.01-0.39-0.02c-0.13,0-0.26-0.01-0.39-0.01 s-0.26-0.01-0.39-0.01s-0.26,0.01-0.39,0.01s-0.26,0-0.39,0.01c-0.13,0-0.26,0.01-0.39,0.02c-0.13,0.01-0.26,0.02-0.39,0.03 s-0.26,0.02-0.39,0.03s-0.26,0.03-0.39,0.04c-0.13,0.02-0.26,0.03-0.39,0.04c-0.13,0.02-0.26,0.04-0.39,0.06s-0.26,0.04-0.39,0.06 s-0.26,0.05-0.39,0.08s-0.26,0.05-0.39,0.08s-0.26,0.06-0.39,0.09s-0.26,0.07-0.39,0.1c-0.13,0.04-0.26,0.07-0.39,0.11 s-0.26,0.08-0.39,0.12s-0.26,0.09-0.39,0.13c-0.13,0.05-0.26,0.09-0.39,0.14s-0.26,0.1-0.39,0.15s-0.26,0.11-0.39,0.16 c-0.13,0.06-0.26,0.12-0.39,0.18s-0.26,0.12-0.39,0.19s-0.26,0.14-0.39,0.21s-0.26,0.14-0.39,0.21c-0.13,0.08-0.26,0.16-0.39,0.24 c-0.13,0.08-0.26,0.16-0.39,0.24c-0.13,0.09-0.26,0.18-0.39,0.27s-0.26,0.19-0.39,0.28c-0.13,0.1-0.26,0.2-0.39,0.3 c-0.13,0.11-0.26,0.22-0.39,0.33s-0.26,0.23-0.39,0.34c-0.13,0.12-0.26,0.24-0.39,0.37c-0.13,0.13-0.26,0.27-0.39,0.4 c-0.13,0.14-0.26,0.28-0.39,0.43s-0.26,0.3-0.39,0.46c-0.13,0.16-0.26,0.33-0.39,0.5c-0.13,0.18-0.26,0.37-0.39,0.55 c-0.13,0.2-0.26,0.4-0.39,0.61c-0.13,0.22-0.26,0.45-0.39,0.68c-0.14,0.25-0.27,0.51-0.39,0.77c-0.14,0.3-0.27,0.6-0.39,0.9 c-0.14,0.36-0.27,0.73-0.39,1.1c-0.15,0.48-0.28,0.98-0.39,1.48c-0.25,1.18-0.39,2.42-0.39,3.7c0,1.27,0.14,2.49,0.39,3.67 c0.11,0.5,0.24,0.99,0.39,1.47c0.12,0.37,0.24,0.74,0.39,1.1c0.12,0.3,0.25,0.6,0.39,0.89c0.12,0.26,0.25,0.51,0.39,0.76 c0.12,0.23,0.25,0.45,0.39,0.67c0.12,0.2,0.25,0.41,0.39,0.6c0.13,0.19,0.25,0.37,0.39,0.55c0.13,0.17,0.25,0.34,0.39,0.5 c0.13,0.15,0.26,0.3,0.39,0.45c0.13,0.14,0.26,0.28,0.39,0.42c0.13,0.13,0.26,0.26,0.39,0.39c0.13,0.12,0.26,0.25,0.39,0.37 c0.13,0.11,0.26,0.22,0.39,0.33s0.26,0.21,0.39,0.32c0.13,0.1,0.26,0.2,0.39,0.3c0.13,0.09,0.26,0.18,0.39,0.27s0.26,0.18,0.39,0.26 s0.26,0.16,0.39,0.24c0.13,0.08,0.26,0.15,0.39,0.23c0.13,0.07,0.26,0.14,0.39,0.21s0.26,0.13,0.39,0.19 c0.13,0.06,0.26,0.13,0.39,0.19c0.13,0.06,0.26,0.11,0.39,0.17s0.26,0.11,0.39,0.17c0.13,0.05,0.26,0.1,0.39,0.14 c0.13,0.05,0.26,0.1,0.39,0.14s0.26,0.08,0.39,0.12s0.26,0.08,0.39,0.12s0.26,0.07,0.39,0.1s0.26,0.07,0.39,0.1s0.26,0.06,0.39,0.08 c0.13,0.03,0.26,0.06,0.39,0.08c0.13,0.02,0.26,0.05,0.39,0.07s0.26,0.04,0.39,0.06s0.26,0.04,0.39,0.05 c0.13,0.02,0.26,0.03,0.39,0.04s0.26,0.03,0.39,0.04s0.26,0.02,0.39,0.03s0.26,0.02,0.39,0.03s0.26,0.01,0.39,0.01 s0.26,0.01,0.39,0.01c0.05,0,0.1,0,0.15,0c0.08,0,0.16,0,0.24-0.01c0.13,0,0.26,0,0.39-0.01c0.13,0,0.26,0,0.39-0.01 s0.26-0.02,0.39-0.03s0.26-0.01,0.39-0.03c0.13-0.01,0.26-0.02,0.39-0.04c0.13-0.01,0.26-0.03,0.39-0.04 c0.13-0.02,0.26-0.03,0.39-0.05c0.13-0.02,0.26-0.04,0.39-0.06s0.26-0.04,0.39-0.06s0.26-0.05,0.39-0.08s0.26-0.05,0.39-0.08 s0.26-0.06,0.39-0.09s0.26-0.06,0.39-0.1s0.26-0.07,0.39-0.11s0.26-0.08,0.39-0.12s0.26-0.08,0.39-0.13 c0.13-0.04,0.26-0.09,0.39-0.14s0.26-0.1,0.39-0.15s0.26-0.11,0.39-0.16c0.13-0.06,0.26-0.11,0.39-0.17s0.26-0.12,0.39-0.18 s0.26-0.13,0.39-0.2s0.26-0.14,0.39-0.21s0.26-0.15,0.39-0.23s0.26-0.15,0.39-0.24c0.13-0.08,0.26-0.17,0.39-0.26 c0.13-0.09,0.26-0.18,0.39-0.27c0.13-0.1,0.26-0.19,0.39-0.29s0.26-0.21,0.39-0.32s0.26-0.22,0.39-0.33 c0.13-0.12,0.26-0.24,0.39-0.36c0.13-0.13,0.26-0.26,0.39-0.39c0.13-0.14,0.26-0.28,0.39-0.42c0.13-0.15,0.26-0.3,0.39-0.45 c0.13-0.16,0.26-0.33,0.39-0.49c0.13-0.18,0.26-0.36,0.39-0.54c0.13-0.2,0.26-0.4,0.39-0.6c0.14-0.22,0.26-0.44,0.39-0.67 c0.14-0.25,0.27-0.5,0.39-0.76c0.14-0.29,0.27-0.58,0.39-0.88c0.14-0.36,0.27-0.72,0.39-1.1c0.15-0.48,0.28-0.97,0.39-1.46 c0.25-1.17,0.39-2.4,0.39-3.66C770.03,92.19,769.9,91.05,769.68,89.95z"></path>
                           <g>
                              <g>
                                 <rect x="738.36" y="126.29" class="st5" width="30.01" height="117.88"></rect>
                              </g>
                           </g>
                           <g>
                              <g>
                                 <path class="st5" d="M912.39,184.14c0,43.33-30.5,62.69-60.51,62.69c-33.4,0-59.06-22.99-59.06-60.75 c0-38.73,25.41-62.45,61-62.45C888.91,123.63,912.39,148.32,912.39,184.14z M823.56,185.35c0,22.75,11.13,39.94,29.29,39.94 c16.94,0,28.8-16.7,28.8-40.42c0-18.4-8.23-39.45-28.56-39.45C832.03,145.41,823.56,165.74,823.56,185.35z"></path>
                              </g>
                           </g>
                           <g>
                              <path class="st5" d="M386,243.52c-2.95,0-5.91-0.22-8.88-0.66c-15.75-2.34-29.65-10.67-39.13-23.46 c-9.48-12.79-13.42-28.51-11.08-44.26c2.34-15.75,10.67-29.65,23.46-39.13c12.79-9.48,28.51-13.42,44.26-11.08 s29.65,10.67,39.13,23.46l7.61,10.26l-55.9,41.45l-15.21-20.51l33.41-24.77c-3.85-2.36-8.18-3.94-12.78-4.62 c-9-1.34-17.99,0.91-25.3,6.33s-12.07,13.36-13.41,22.37c-1.34,9,0.91,17.99,6.33,25.3c5.42,7.31,13.36,12.07,22.37,13.41 c9,1.34,17.99-0.91,25.3-6.33c4.08-3.02,7.35-6.8,9.72-11.22l22.5,12.08c-4.17,7.77-9.89,14.38-17.02,19.66 C411,239.48,398.69,243.52,386,243.52z"></path>
                           </g>
                        </svg>
                     </div>
                  </div>
               </div>
               <div class="col-12 text-center lh150 mt20 mt-md50 px15 px-md0">
                  <div class="trpre-heading f-20 f-md-22 w500 lh150 yellow-clr">
                     <span>
                     It’s Time to Get Over Boring Content &amp; Cash into The Latest Trending Topics in 2022 
                     </span>
                  </div>
               </div>
               <div class="col-12 mt-md25 mt20 f-md-45 f-28 w500 text-center black-clr lh150">
                  Breakthrough A.I. Technology <span class="w600 blue-clr">Creates Traffic Pulling Websites Packed with Trendy Content &amp; Videos from Just One Keyword</span> in 60 Seconds... 
               </div>
               <div class="col-12 mt-md25 mt20 f-20 f-md-22 w500 text-center lh150 black-clr">
                  Anyone can easily create &amp; sell traffic generating websites for dentists, attorney, affiliates,<br class="d-none d-md-block"> coaches, SAAS, retail stores, book shops, gyms, spas, restaurants, &amp; 100+ other niches...
               </div>
            </div>
            <div class="row d-flex align-items-center flex-wrap mt20 mt-md40">
               <div class="col-md-7 col-12 min-md-video-width-left">
                  <div class="col-12 responsive-video">
                     <iframe src="https://trendio.dotcompal.com/video/embed/2p8pv1cndk" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                        box-shadow: none !important;" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                  </div>
               </div>
               <div class="col-md-5 col-12 mt20 mt-md0 min-md-video-width-right">
                  <div class="key-features-bg1">
                     <ul class="list-head1 pl0 m0 f-16 lh150 w500 black-clr">
                        <li>Works Seamlessly in Any Niche or Topic - <span class="w500">Just One Keyword</span> </li>
                        <li>All of That Without Content Creation, Editing, Camera, or Tech Hassles Ever </li>
                        <li>Drive Tons of FREE Viral Traffic Using the POWER Of Trendy Content </li>
                        <li>Grab More Authority, Engagement, &amp; Leads on your Business Website </li>
                        <li>Monetize Other’s Content Legally with Banner Ads, Affiliate Offers &amp; AdSense </li>
                        <li class="w500">FREE Commercial LICENSE INCLUDED TO BUILD AN INCREDIBLE INCOME </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Header Trendio End -->
      <div class="tr-all">
         <picture>
            <source media="(min-width:767px)" srcset="assets/images/trendio.webp" class="img-fluid d-block mx-auto" style="width:100%">
            <source media="(min-width:320px)" srcset="assets/images/trendio-mview.webp" class="img-fluid d-block mx-auto">
            <img src="assets/images/trendio.webp" alt="Steps" class="img-fluid d-block mx-auto" style="width:100%">
         </picture>
      </div>
      <!--Trendio ends-->

           <!-------WriterArc Starts----------->
      <!-------Exclusive Bonus----------->
      <div class="exclusive-bonus">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                  <div class="white-clr f-24 f-md-36 lh140 w500">
                     Exclusive Bonus #2 : WebPrimo
                  </div>
                  <div class="f-18 f-md-20 w500 mt20 white-clr">
                     20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
                  </div>
               </div>
            </div>
         </div>
      </div>

      <div class="WebPrimo-header-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12"><img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/webprimo-logo.webp" class="img-fluid d-block mx-auto mr-md-auto"></div>
                <div class="col-12 f-20 f-md-24 w500 text-center white-clr lh150 text-capitalize mt20 mt-md65">
                    <div>
                        The Simple 7 Minute Commercial Service That Gets Clients Daily (Anyone Can Do This)
                    </div>
                    <div class="mt5">
                        <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/viddeyo-post-head-sep.webp" class="img-fluid mx-auto d-block">
                    </div>
                </div>
                <div class="col-12 mt-md25 mt20">
                    <div class="f-md-45 f-28 w500 text-center white-clr lh150">
						First Ever on JVZoo…  <br class="d-none d-md-block">
						<span class="w600 f-md-42 WebPrimo-highlihgt-heading">The Game Changing Website Builder Lets You </span>  <br>Create Beautiful Local WordPress Websites &amp; Ecom Stores for Any Business in <span class="WebPrimo-higlight-text"> Just 7 Minutes</span>
                    </div>
                </div>
                <div class="col-12 mt20 mt-md20 f-md-22 f-20 w400 white-clr text-center">
					Easily create &amp; sell websites and stores for big profits to dentists, attorney, gyms, spas, restaurants, cafes, retail stores, book shops, coaches &amp; 100+ other niches...
                </div>
				  <div class="col-12 mt20 mt-md40">
				  <div class="row d-flex align-items-center flex-wrap">
                        <div class="col-md-7 col-12 min-md-video-width-left">

							<div class="col-12 responsive-video">
                                <iframe src="https://webprimo.dotcompal.com/video/embed/8ghfrcnhp5" style="width:100%; height:100%" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                            </div> 

                        </div>
                        <div class="col-md-5 col-12 mt20 mt-md0 min-md-video-width-right">
                            <div class="WebPrimo-key-features-bg">
							<ul class="list-head pl0 m0 f-16 lh150 w400 white-clr">
							<li>Tap Into Fast-Growing $284 Billion Website Creation Industry</li>
							<li>Help Businesses Go Digital in Their Hard Time (IN TODAYS DIGITAL AGE)</li>
							<li>Create Elegant, Fast loading &amp; SEO Friendly Website within 7 Minutes</li>
							<li>200+ Eye-Catching Templates for Local Niches Ready with Content</li>
							<li>Bonus Live Training - Find Tons of Local Clients Hassle-Free</li>
							<li>Commercial License Included to Build an Incredible Income Helping Clients!</li>
							</ul>
                            </div>
                        </div>
                    </div>
                    </div>
            </div>
        </div>
    </div>
    <div class="WebPrimo-list-section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="row webprimo-header-list-block">
                        <div class="col-12 col-md-6 header-list-back">
                            <div class="f-16 f-md-18 lh150 w400">
                                <ul class="webprimo-header-bordered-list pl0 webprimo-orange-list">
                                    <li><span class="w500">Create Ultra-Fast Loading &amp; Beautiful Websites</span></li>
                                    <li>Instantly Create Local Sites, E-com Sites, and Blogs-<span class="w500">For Any Business Need.</span>
									</li>
                                    <li>Built On World's No. 1, <span class="w500">Super-Customizable WP FRAMEWORK For BUSINESSES</span>
									</li>
                                    <li>SEO Optimized Websites for <span class="w500">More Search Traffic</span></li>
									<li><span class="w500">Get More Likes, Shares and Viral Traffic</span> with In-Built Social Media Tools</li>
									<li><span class="w500">Create 100% Mobile Friendly</span> And Ultra-Fast Loading Websites.</li>
									<li><span class="w500">Analytics &amp; Remarketing Ready</span> Websites</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 header-list-back">
                            <div class="f-16 f-md-18 lh150 w400">
                                <ul class="webprimo-header-bordered-list pl0 webprimo-orange-list">
                                    <li>Newbie Friendly. <span class="w500">No Tech Skills Needed. No Monthly Fees.</span></li>
                                    <li>Customize and Update Themes on Live Website Easily</li>
                                    <li>Loaded with 30+ Themes with Super <span class="w500">Customizable 200+ Templates</span> and 2000+ possible design combinations.</li>
                                    <li>Customise Websites <span class="w500">Without Touching Any Code</span></li>
                                    <li><span class="w500">Accept Payments</span> For Your Services &amp; Products With Seamless WooCommerce Integration</li>
									<li>Help Local Clients That Desperately Need Your Service And Make BIG Profits.</li>									
                                </ul>
                            </div>
                        </div>
						<div class="col-12  header-list-back">
                            <div class="f-16 f-md-18 lh150 w400">
                                <ul class="webprimo-header-bordered-list pl0 webprimo-orange-list">
									<li class="w500">Commercial License Included to Build an Incredible Income Helping Clients!</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

      <div class="gr-1">
         <div class="container-fluid p0">
            <picture>
               <source media="(min-width:768px)" srcset="assets/images/webprimo-ss.webp">
               <source media="(min-width:320px)" srcset="assets/images/webprimo-mview.webp" style="width:100%" class="img-fluid mx-auto">
               <img src="assets/images/webprimo-ss.webp" alt="WriterArc Steps" class="img-fluid" style="width: 100%;">
            </picture>
         </div>
      </div>

      <!-- WriterArc End -->

      <!-- Vocalic Section Start -->
      <div class="exclusive-bonus">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                  <div class="white-clr f-24 f-md-36 lh140 w500">
                     Exclusive Bonus #3 : Vocalic
                  </div>
                  <div class="f-18 f-md-20 w500 mt20 white-clr">
                     20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="header-section-vc">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row align-items-center">
                     <div class="col-md-12 text-center">
                        <svg xmlns="http://www.w3.org/2000/svg" id="Layer_2" viewBox="0 0 290.61 100" style="max-height:56px;">
                           <defs>
                              <style>
                                 .cls-1 {
                                 fill: #fff;
                                 }
                                 .cls-1,
                                 .cls-2 {
                                 fill-rule: evenodd;
                                 }
                                 .cls-2,
                                 .cls-3 {
                                 fill: #00aced;
                                 }
                              </style>
                           </defs>
                           <g id="Layer_1-2">
                              <g>
                                 <g>
                                    <path class="cls-3" d="M104.76,79.86l-5.11-6.34c-.16-.2-.45-.23-.65-.07-.37,.3-.75,.59-1.14,.87-3.69,2.66-7.98,4.34-12.49,4.9-1.11,.14-2.23,.21-3.35,.21s-2.24-.07-3.35-.21c-4.52-.56-8.8-2.24-12.49-4.9-.39-.28-.77-.57-1.14-.87-.2-.16-.49-.13-.65,.07l-5.11,6.34c-.16,.2-.13,.49,.07,.65,5.24,4.22,11.53,6.88,18.21,7.71,.37,.05,.74,.09,1.11,.12v11.67h6.7v-11.67c.37-.03,.74-.07,1.11-.12,6.68-.83,12.96-3.48,18.21-7.71,.2-.16,.23-.45,.07-.65Z">
                                    </path>
                                    <path class="cls-3" d="M82.02,74.47c10.57,0,19.13-7.45,19.13-16.65V31.08c0-4.6-2.14-8.76-5.6-11.77-3.46-3.01-8.24-4.88-13.53-4.88-10.57,0-19.13,7.45-19.13,16.65v26.74c0,9.2,8.57,16.65,19.13,16.65Zm-11.14-41.53c0-5.35,4.99-9.69,11.14-9.69,3.08,0,5.86,1.08,7.87,2.84,2.01,1.75,3.26,4.18,3.26,6.85v23.02c0,5.35-4.99,9.69-11.14,9.69s-11.14-4.34-11.14-9.69v-23.02Z">
                                    </path>
                                    <path class="cls-3" d="M82.02,38.89c1.71,0,3.1-1.39,3.1-3.1s-1.39-3.1-3.1-3.1-3.1,1.39-3.1,3.1,1.39,3.1,3.1,3.1Z">
                                    </path>
                                    <path class="cls-3" d="M82.02,47.54c1.71,0,3.1-1.39,3.1-3.1s-1.39-3.1-3.1-3.1-3.1,1.39-3.1,3.1,1.39,3.1,3.1,3.1Z">
                                    </path>
                                    <path class="cls-3" d="M82.02,56.2c1.71,0,3.1-1.39,3.1-3.1s-1.39-3.1-3.1-3.1-3.1,1.39-3.1,3.1,1.39,3.1,3.1,3.1Z">
                                    </path>
                                 </g>
                                 <path class="cls-1" d="M25.02,58.54L10.61,26.14c-.08-.19-.25-.29-.45-.29H.5c-.18,0-.32,.08-.42,.23-.1,.15-.11,.31-.04,.47l20.92,46.71c.08,.19,.25,.29,.45,.29h8.22c.2,0,.37-.1,.45-.29L60.4,15.13c.07-.16,.06-.32-.03-.47-.1-.15-.24-.23-.42-.23h-9.67c-.2,0-.37,.11-.45,.29L26.07,58.54c-.1,.21-.29,.34-.53,.34-.23,0-.43-.13-.53-.34h0Z">
                                 </path>
                                 <path class="cls-1" d="M117.96,52c-.13-.79-.2-1.58-.2-2.38s.07-1.6,.2-2.38c1.15-6.95,7.2-12.05,14.24-12.05,3.84,0,7.49,1.51,10.21,4.23l1.1,1.1c.19,.19,.49,.19,.68,0l6.03-6.03c.19-.19,.19-.49,0-.68l-1.1-1.1c-4.5-4.5-10.56-7.01-16.92-7.01-12.06,0-22.27,8.99-23.75,20.97-.12,.98-.18,1.97-.18,2.96s.06,1.98,.18,2.96c1.48,11.97,11.69,20.97,23.75,20.97,6.37,0,12.42-2.51,16.92-7.01l1.1-1.1c.19-.19,.19-.49,0-.68l-6.03-6.03c-.19-.19-.49-.19-.68,0l-1.1,1.1c-2.72,2.72-6.37,4.23-10.21,4.23-7.04,0-13.09-5.1-14.24-12.05h0Z">
                                 </path>
                                 <path class="cls-1" d="M258.21,52c-.13-.79-.2-1.58-.2-2.38s.07-1.6,.2-2.38c1.15-6.95,7.2-12.05,14.24-12.05,3.84,0,7.49,1.51,10.21,4.23l1.1,1.1c.19,.19,.49,.19,.68,0l6.03-6.03c.19-.19,.19-.49,0-.68l-1.1-1.1c-4.5-4.5-10.56-7.01-16.92-7.01-12.06,0-22.27,8.99-23.75,20.97-.12,.98-.18,1.97-.18,2.96s.06,1.98,.18,2.96c1.48,11.97,11.69,20.97,23.75,20.97,6.37,0,12.42-2.51,16.92-7.01l1.1-1.1c.19-.19,.19-.49,0-.68l-6.03-6.03c-.19-.19-.49-.19-.68,0l-1.1,1.1c-2.72,2.72-6.37,4.23-10.21,4.23-7.04,0-13.09-5.1-14.24-12.05h0Z">
                                 </path>
                                 <path class="cls-1" d="M174.91,25.87c-11.97,1.48-20.97,11.69-20.97,23.75s8.99,22.27,20.97,23.75c.98,.12,1.97,.18,2.96,.18s1.98-.06,2.96-.18c2.14-.26,4.24-.82,6.23-1.65,.19-.08,.3-.24,.3-.44v-9.77c0-.19-.09-.35-.27-.43-.17-.09-.35-.07-.5,.05-1.86,1.41-4.03,2.35-6.34,2.73-.79,.13-1.58,.2-2.38,.2s-1.6-.07-2.38-.2l-.2-.03c-6.86-1.23-11.85-7.24-11.85-14.21s5.1-13.09,12.05-14.24c.79-.13,1.58-.2,2.38-.2s1.59,.07,2.38,.2l.19,.03c6.87,1.23,11.87,7.24,11.87,14.21v22.7c0,.26,.22,.48,.48,.48h8.53c.26,0,.48-.22,.48-.48v-22.7c0-12.06-8.99-22.27-20.97-23.75-.98-.12-1.97-.18-2.96-.18s-1.98,.06-2.96,.18h0Z">
                                 </path>
                                 <path class="cls-1" d="M210.57,0c-.33,0-.59,.27-.59,.59V72.96c0,.33,.27,.59,.59,.59h10.5c.33,0,.59-.27,.59-.59V.59c0-.33-.27-.59-.59-.59h-10.5Z">
                                 </path>
                                 <path class="cls-1" d="M229.84,25.39c-.33,0-.59,.27-.59,.59v46.97c0,.33,.27,.59,.59,.59h10.5c.33,0,.59-.27,.59-.59V25.98c0-.33-.27-.59-.59-.59h-10.5Z">
                                 </path>
                                 <path class="cls-2" d="M229.84,8.96c-.33,0-.59,.07-.59,.15v11.94c0,.08,.27,.15,.59,.15h10.5c.33,0,.59-.07,.59-.15V9.11c0-.08-.27-.15-.59-.15h-10.5Z">
                                 </path>
                              </g>
                           </g>
                        </svg>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md50 text-center">
                  <div class="preheadline f-18 f-md-22 w800 blue-clr lh140">
                     <i><span class="red-clr">Attention:</span> Futuristic A.I. Technology Do All The Work For You!</i>
                  </div>
               </div>
               <div class="col-12 mt-md30 mt20 f-md-50 f-28 w500 text-center white-clr lh140">
                  <span class="w800"> Create Profit-Pulling Videos with Voiceovers   </span><br>
                  <span class="w800 f-30 f-md-55 highlight-heading">In Just 60 Seconds <u>without</u>...</span> <br>
                  Being in Front of Camera, Speaking A Single Word or Hiring Anyone Ever
               </div>
               <div class="col-12 mt-md35 mt20  text-center">
                  <div class="post-heading f-18 f-md-24 w500 text-center lh150 white-clr">
                     Create &amp; Sell Videos &amp; Voiceovers In ANY LANGUAGE For Big Profits To Dentists, Gyms, Spas, Cafes, Retail Stores, Book Shops, Affiliate Marketers, Coaches &amp; 100+ Other Niches… 
                  </div>
               </div>
            </div>
            <div class="row d-flex flex-wrap mt20 mt-md40">
               <div class="col-md-7 col-12 min-md-video-width-left mt0 mt-md40">
                  <div class="col-12 responsive-video border-video">
                     <iframe src="https://vocalic.dotcompal.com/video/embed/g1mdnyzmub" style="width:100%; height:100%" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                  </div>
                  
               </div>
               <div class="col-md-5 col-12 mt20 mt-md0 min-md-video-width-right">
                  <div class="calendar-wrap mb20 mb-md0">
                     <ul class="list-head pl0 m0 f-18 lh150 w400 white-clr">
                        <li>Tap Into Fast-Growing <span class="w600">$200 Billion Video &amp; Voiceover Industry</span></li>
                        <li><span class="w600">Create Stunning Videos In ANY Niche</span> With In-Built Video Creator</li>
                        <li><span class="w600">Create &amp; Publish YouTube Shorts</span> For Tons Of FREE Traffic</li>
                        <li><span class="w600">Make Passive Affiliate Commissions</span> Creating Product Review Videos</li>
                        <li>Real Human-Like Voiceover In <span class="w600">150+ Unique Voices And 30+ Languages</span></li>
                        <li>Convert Any Text Script Into A <span class="w600">Whiteboard &amp; Explainer Videos</span></li>
                        <li><span class="w600">Create Videos Using Just A Keyword</span> With Ready-To-Use ST&Ck Images</li>
                        <li><span class="w600">Boost Engagement &amp; Sales </span> Using Videos</li>
                        <li><span class="w600">Add Background Music</span> To Your Videos.</li>
                        <li><span class="w600">Built-In Content Spinner</span> - Make Unique Scripts</li>
                        <li><span class="w600">Store Media Files </span> With Integrated MyDrive</li>
                        <li><span class="w600">100% Newbie Friendly </span> - No Tech Hassles Ever</li>
                        <li><span class="w600">Commercial License Included</span> To Build On Incredible Income Helping Clients</li>
                        <li>FREE Training To<span class="w600"> Find Tons Of Clients</span></li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="gr-1">
         <div class="container-fluid p0">
            <picture>
               <source media="(min-width:768px)" srcset="assets/images/vocalic1.webp">
               <source media="(min-width:320px)" srcset="assets/images/vocalic1-mview.webp" style="width:100%" class="vidvee-mview">
               <img src="assets/images/vocalic1.webp" alt="Vidvee Steps" class="img-fluid" style="width: 100%;">
            </picture>
            <picture>
               <source media="(min-width:768px)" srcset="assets/images/vocalic2.webp">
               <source media="(min-width:320px)" srcset="assets/images/vocalic2-mview.webp">
               <img src="assets/images/vocalic2.webp" alt="Flowers" style="width:100%;">
            </picture>
         </div>
      </div>
      <!-- Vocalic Section ENds -->

      <!-------Kyza Starts----------->
      <!-------Exclusive Bonus----------->
      <div class="exclusive-bonus">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                  <div class="white-clr f-24 f-md-36 lh140 w500">
                     Exclusive Bonus #4 : Kyza
                  </div>
                  <div class="f-18 f-md-20 w500 mt20 white-clr">
                     20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="header-section-ky">
         <div class="container">
            <div class="row">
               <div class="col-md-3 col-md-12 mx-auto">
                  <img src="assets/images/kyza-logo.png" class="img-fluid d-block mx-auto">
               </div>
               <div class="col-12 text-center">
                  <div class="mt20 mt-md60 text-center px-sm15 px0">
                     <!--<div class="f-20 f-sm-24 w500 text-center post-heading lh140">
                        STOP Paying To Multiple Marketing Apps For Running Your Business!
                        </div>-->
                     <div class="f-20 f-sm-22 w500 text-center lh140 d-inline-flex align-items-center justify-content-center relative">
                        <img src="assets/images/stop.png" class="img-responsive mx-xs-center stop-img">
                        <div class="post-heading1">
                           <i>Paying 100s of Dollar Monthly To Multiple Marketing Apps! </i>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md60 text-center">
                  <div class="f-md-41 f-28 w600 black-clr lh140 line-center">	
                     Game Changer All-In-One Growth Suite... <br class="visible-lg">
                     ✔️Builds Beautiful Pages &amp; Websites ✔️Unlocks Branding Designs Kit ✔️Creates AI-Powered Optin Forms &amp; Pop-Ups, &amp; ✔️Lets You Send UNLIMITED Emails 					
                  </div>
                  <div class="mt-md25 mt20 f-20 f-md-26 w500 black-clr text-center lh140">
                     All From Single Dashboard Without Any Tech Skills and <u>Zero Monthly Fees</u>
                  </div>
                  <div class="col-12 col-md-12  mt20 mt-md40">
                     <div class="row d-flex flex-wrap align-items-center">
                        <div class="col-md-7 col-12 mt20 mt-md0">
                           <div class="responsive-video">  
                              <iframe class="embed-responsive-item" src="https://kyza.dotcompal.com/video/embed/nr4t9jffvd" style="width: 100%;height: 100%; background: transparent !important;
                                 box-shadow: none !important;" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                           </div>
                        </div>
                        <div class="col-md-5 col-12 mt20 mt-md0">
                           <div class="key-features-bg-ky">
                              <ul class="list-head-ky pl0 m0 f-18 f-md-19 lh140 w500 black-clr">
                                 <li>Create Unlimited Stunning Pages, Popups, And Forms </li>
                                 <li>Mobile, SEO, And Social Media Optimized Pages </li>
                                 <li>Send Unlimited Beautiful Emails For Tons Of Traffic &amp; Sales </li>
                                 <li>Over 100 Done-For-You High Converting Templates </li>
                                 <li>2 Million+ Stock Images &amp; Branding Graphics Kit </li>
                                 <li>Comes With Commercial License To Serve &amp; Charge Your Clients </li>
                                 <li>100% Newbie Friendly &amp; Step-By-Step Training Included  </li>
                              </ul>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="tr-all">
         <picture>
            <source media="(min-width:767px)" srcset="assets/images/kyza.webp" class="img-fluid d-block mx-auto" style="width:100%">
            <source media="(min-width:320px)" srcset="assets/images/kyza-mview.webp" class="img-fluid d-block mx-auto">
            <img src="assets/images/kyza.webp" alt="Steps" class="img-fluid d-block mx-auto" style="width:100%">
         </picture> 
      </div>
      <!-- Kyza Section End-->

      

      <!-- Bonus Section Header Start -->
      <div class="bonus-header">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-10 mx-auto heading-bg text-center">
                  <div class="f-24 f-md-36 lh140 w600"> When You Purchase QuickFunnel, You Also Get <br class="hidden-xs"> Instant Access To These 20 Exclusive Bonuses</div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus Section Header End -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w600">Bonus 1</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus1.webp" class="img-fluid mx-auto d-block" alt="Bonus1">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w600 lh140 bonus-title-color text-capitalize">
                        Sales Funnel Blueprint
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>Who Else Wants To Skyrocket Sales Up To 300%... With The Same Amount Of Traffic & Buyers! Here's How You Can Get Started In 48 Hours Or Less...</b></li>
                           <li>The Internet marketing business had evolved throughout years. If you are in this business long enough, you are one of those to witness the changes in the past decade. From the beginning, the Internet marketer will only offer one product to the buyers. </li>
                          <li>Moving on, today, we have a sales funnel. Changes happenso rapidly. All things were shaped to fulfill the needs of the buyers. Thanks to the evolution of Internet that happened in the past decades, the reach has grown in an epic proportion. With more and more audience, things have to change. </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #1 Section End -->
      <!-- Bonus #2 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w600">Bonus 2</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30">
                  <div class="row">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="assets/images/bonus2.webp" class="img-fluid mx-auto d-block" alt="Bonus2">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w600 lh140 bonus-title-color mt20 mt-md0 text-capitalize">
                        Wp Easy Optins
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b> If you're new to marketing and list building use this plug-in today to move yourself forward with your list building efforts fast and with little fuss.</b></li>
                           <li>If you’re not new to marketing then you will see the potential of owning full PLR right to this product.</li>
                           <li>When installed "Wp Easy Optins" automatically creates all the pages on your blog your funnel will need: Squeeze Page, Thank You Page, Oto Page, White List Page, and all the Legal Pages</li>
                           <li>"Wp Easy Optins" will give you the url's you'll need to go to your Autoresponder and set up a campaign.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #2 Section End -->
      <!-- Bonus #3 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w600">Bonus 3</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus3.webp" class="img-fluid mx-auto d-block " alt="Bonus3">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w600 lh140 bonus-title-color text-capitalize">
                        Multiple Product Funnel
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>PLR Audio On How to Create a Complete Product Funnel in Your Business - An Amazing Overview!</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #3 Section End -->
      <!-- Bonus #4 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w600">Bonus 4</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="assets/images/bonus4.webp" class="img-fluid mx-auto d-block " alt="Bonus4">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w600 lh140 bonus-title-color text-capitalize">
                        How To Create A Product Funnel Fast
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li> <b> This is imperative, because if you don't have a product line, you are leaving money on the table. </b> </li>
                           <li>You see, when someone buys from me one time, they often buy again and again and again - and if you don't have enough products in your funnel, they will buy from someone else. </li>
                           <li>Solve this fast!</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #4 End -->
      <!-- Bonus #5 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w600">Bonus 5</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus5.webp" class="img-fluid mx-auto d-block " alt="Bonus5">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w600 lh140 bonus-title-color text-capitalize">
                        WP Profit Page Creator
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li><b>Discover a Brand New and Highly Profitable WP Plugin that Once You Fire it Up, Will Generate You Endless Sales!</b> </li>
                        <li>If you are an online entrepreneur and you want to make so much profits out from your internet business, this amazing WordPress plugin is a huge help to you.</li>
                        <li>What this software does is that, it will create SEO friendly money making WordPress pages almost every with a little content.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #5 End -->
      <!-- CTA Button Section Start -->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w600">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w600 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <!-- <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w600 red-clr">"ATULVIP"</span> for an Additional Discount on Commercial Licence
                  </div> -->
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab QuickFunnel + My 20 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                  Use Coupon Code <span class="w600 red-clr">"ATULBUNDLE"</span> for an Additional <span class="w600 red-clr">$30 Discount</span> on Bundle Deal
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="https://quickfunnel.live/bundle/" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab QuickFunnel Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald w600">00</span>
                        <span class="f-14 f-md-18 w500 ">Days</span>
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald w600">00</span>
                        <span class="f-14 f-md-18 w500 ">Hours</span>
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald w600">00</span>
                        <span class="f-14 f-md-18 w500 ">Mins</span>
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald w600">00</span>
                        <span class="f-14 f-md-18 w500 ">Sec</span>
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->
     <!-- Bonus #6 Start -->
     <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w600">BONUS 6</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="assets/images/bonus6.webp" class="img-fluid mx-auto d-block " alt="Bonus6">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w600 lh140 bonus-title-color text-capitalize">
                        Affiliate Marketing Manager Software
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li> <b>12,590+ PLR Products Which You Can Resell And Keep 100% Of The Profits!</b></li> 
                           <li>We feature over 12,500 products with Private Label and Resale rights licenses, which mean that you can SELL, EDIT or even CLAIM the product inside as your own!</li>
                           <li>We've been helping THOUSANDS Of Webmasters & Marketers Start & Grow their own Internet Business... Today Is YOUR Turn!</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #6 End -->
      <!-- Bonus #7 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w600">BONUS 7</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus7.webp" class="img-fluid mx-auto d-block " alt="Bonus7">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w600 lh140 bonus-title-color text-capitalize">
                        Software Profit Mastery
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li><b>Why You Should Get Into The Lucrative Software Business?!</b></li>
                           <li>If you are a serious entrepreneur, venturing other types of business model is mostly open to you. And the fact is that, considering the boom of online business model, tapping into the software business platform is also a good choice to start with.</li>
                           <li>The thing is that, if you are not familiar with the environment, inside this video series are the necessary information that you need for you to study the ins and outs of this new type of online business model.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #7 End -->
      <!-- Bonus #8 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w600">BONUS 8</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus8.webp" class="img-fluid mx-auto d-block " alt="Bonus8">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w600 lh140 bonus-title-color text-capitalize">
                        YT Rank Analyzer
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li><b>Discover How to Dominate YouTube And Build MASSIVE Targeted Lists For FREE... By Using Software To Do ALL the Dirty Work!</b></li> 
                           <li>YouTube is now the second largest search engine and is the thirds most visited website in the world. If you are not into YouTube Marketing, then you waste a huge opportunity to attract more traffic and leads to subscribe your list.</li> 
                           <li>The good news is that inside this product is an amazing tool that will help you do the technical stuff and make your campaign effort more productive and scalable.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #8 End -->
      <!-- Bonus #9 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w600">BONUS 9</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus9.webp" class="img-fluid mx-auto d-block " alt="Bonus9">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w600 lh140 bonus-title-color text-capitalize">
                        Domains and Affiliate Marketing
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li><b>Learn More About Domains and Affiliate Marketing!</b></li>
                           <li>Most affiliate marketers are no stranger to the domain name market. At the very least, they probably have a domain name for their website or blog. And some have dozens of domains in their possession.</li>
                           <li>There are a few ways that domains can be used in affiliate marketing. Many affiliates create niche sites or blogs for the sole purpose of promoting affiliate programs. Some also purchase domains to redirect to their affiliate links. This makes the URL shorter, easier to remember and more appealing.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #9 End -->
      <!-- Bonus #10 start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w600">BONUS 10</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus10.webp" class="img-fluid mx-auto d-block " alt="Bonus10">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w600 lh140 bonus-title-color text-capitalize">
                        Boost Your Website Traffic
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>Businesses both large and small are always hoping that their target audience will be able to find their site among the thousands of websites they are competing against. </li>
                           <li>One of the best ways to do this is to utilize the free and paid methods for boosting website traffic. </li>
                           <li>However, like so many online marketing methods, it isn’t always clear on how to do this. Finding an effective way to boost the traffic to your website can not only be confusing, but it can also be a bit frustrating.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #10 End -->
      <!-- CTA Button Section Start -->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w600">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w600 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <!-- <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w600 red-clr">"ATULVIP"</span> for an Additional Discount on Commercial Licence
                  </div> -->
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab QuickFunnel + My 20 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                  Use Coupon Code <span class="w600 red-clr">"ATULBUNDLE"</span> for an Additional <span class="w600 red-clr">$30 Discount</span> on Bundle Deal
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="https://quickfunnel.live/bundle/" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab QuickFunnel Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 smmltd">Days</span> </div>
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                     <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                     <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->
    <!-- Bonus #11 start -->
    <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w600">BONUS 11</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus11.webp" class="img-fluid mx-auto d-block " alt="Bonus13">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w600 lh140 bonus-title-color text-capitalize">
                        Facebook Pixel Insert WP Plugin
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b> Facebook is one of great way to get traffic to your website and to your promotions. The problem is that a lot of people are using Facebook the wrong way.</b></li>   
                           <li>What there doing is they're running a general ad that are targeting audience such people in the US, people in New Zealand and other countries. And this results to lower conversion rate and high cost per result. So what you need to do in your ad is by using retargetting. </li>
                           <li>Add a Facebook tracking pixel to all of your WordPress site pages and posts in seconds and start taking advantage of retargeting to increase your sales and conversions. </li>
                           </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #11 End -->
      <!-- Bonus #12 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w600">BONUS 12</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus12.webp" class="img-fluid mx-auto d-block " alt="Bonus12">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w600 lh140 bonus-title-color text-capitalize">
                        The Social Media Traffic
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>Get Traffic From Social Media Platforms Using These Top 10 Tips!</li>
                           <li>Social media has the potential to become the largest source of online traffic for any company. That distinction is now claimed by search engines.</li>
                           <li>While search engines do have a wider footprint and record more activity than social media, the ability of companies to reach out to their target audience and in person is what makes social media special.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #12 End -->
      <!-- Bonus #13 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w600">BONUS 13</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus13.webp" class="img-fluid mx-auto d-block " alt="Bonus13">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w600 lh140 bonus-title-color text-capitalize">
                        Social Media Boom Software
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>12,590+ PLR Products Which You Can Resell And Keep 100% Of The Profits!</li>
                           <li>#1 PLR membership Since 2008, 100k members and growing, Updated almost DAILY</li>
                           <li>We feature over 12,500 products with Private Label and Resale rights licenses, which mean that you can SELL, EDIT or even CLAIM the product inside as your own!</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #13 End -->
      <!-- Bonus #14 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w600">BONUS 14</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus14.webp" class="img-fluid mx-auto d-block " alt="Bonus14">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w600 lh140 bonus-title-color text-capitalize">
                        Xyber Email Assistant Software
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                          <li> <b>Do Your Customer Support with Ease Using Xyber Email Assistant! </b> </li>
                          <li>If you are a current online business, customer support is necessary. This is because you can't be so sure that your business will work perfectly!</li>
                          <li>  The good news though is that, you can now turbo-charge the growth of your business, while freeing Up Your Time With Reduced Customer Support Hassles! Stay On Top Of Your Business With The Ability To Instantly Respond To Email For More Satisfied Customers, Affiliates, and Partners!</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #14 End -->
      <!-- Bonus #15 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w600">BONUS 15</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus15.webp" class="img-fluid mx-auto d-block " alt="Bonus15">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w600 lh140 bonus-title-color text-capitalize">
                        WP SEO Track Plugin
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                          <li><b> With this simple plugin you can get the true insight on your web traffic efforts in only seconds! </b>Watch as your social network shares increase, your google PageRank and more.</li> 
                          <li>Get a clear vision on what you need to start focusing on with your SEO efforts. Start more effective backlinks from Facebook, Twitter, Google and more.</li> 
                          <li>You will get all of the most important stats that you need to know for your SEO web traffic. Focus on the amount of shares you have on popular social sites like Facebook, Twitter, Stumble Upon and more.</li> 
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #15 End -->
      
      <!-- CTA Button Section Start -->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center ">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w600">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w600 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <!-- <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w600 red-clr">"ATULVIP"</span> for an Additional Discount on Commercial Licence
                  </div> -->
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab QuickFunnel + My 20 Exclusive Bonuses</span> <br>
                  </a>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                  Use Coupon Code <span class="w600 red-clr">"ATULBUNDLE"</span> for an Additional <span class="w600 red-clr">$30 Discount</span> on Bundle Deal
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="https://quickfunnel.live/bundle/" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab QuickFunnel Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 smmltd">Days</span> </div>
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                     <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                     <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->
<!-- Bonus #16 start -->
<div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w600">BONUS 16</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus16.webp" class="img-fluid mx-auto d-block " alt="Bonus16">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w600 lh140 bonus-title-color text-capitalize">
                        Automated Traffic Bot
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>The Biggest Yahoo! Secret That No One Is Telling You & How To Snag Thousands Of Visitors Using This Simple Trick!</b></li>   
                           <li>Would you like to increase your search Engine Rankings without complicated SEO? If you are tired of all the complicated search engine information out there, you're far from alone!</li>
                           <li>In fact, most people just plain give up on SEO because it's so complicated. Worse yet, the majority of people who do follow through with SEO usually see little to no results after all that work! </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #16 End -->
      <!-- Bonus #17 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w600">BONUS 17</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus17.webp" class="img-fluid mx-auto d-block " alt="Bonus17">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w600 lh140 bonus-title-color text-capitalize">
                        WP BotBlocker Plugin
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>Stop your wordpress site from being attacked by hackers using bots to try and bring down your site! </b></li>
                           <li>Website hacking has been an issue for many WordPress users for several years now. That's why WordPress Developers are making security plugins to defend WordPress sites.</li>
                           <li>If you are having the same problem, chances are you might want to also install this amazing WordPress called WP BotBlocker with will block any bots that will attempt to do brute entry of your website's backend. </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #17 End -->
      <!-- Bonus #18 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w600">BONUS 18</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus18.webp" class="img-fluid mx-auto d-block " alt="Bonus18">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w600 lh140 bonus-title-color text-capitalize">
                        Website Monitor
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li> <b>Automatically Know If Your Websites Go Down With Just 1 Click!</b></li>
                           <li>If you are having an online business, your website would be your best asset to market your services or products on the internet right?</li>
                           <li>The thing is that even if you are an I.T. or you have a person working for you as an I.T. for your online business, you still can't monitor your website 24/7 when it will go down.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #18 End -->
      <!-- Bonus #19 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w600">BONUS 19</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus19.webp" class="img-fluid mx-auto d-block " alt="Bonus19">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w600 lh140 bonus-title-color text-capitalize">
                        BIZ Landing Page Plugin
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                          <li> <b>You probably have noticed that most businesses online are listed in directories such as YellowPages only... Now with one wordpress plugin you can create an all-in-one website that will pull in multiple sources and display in one place.</b></li>
                          <li>This is a stand alone plugin that will create a business website in one landing page. Add tabbed content to keep your visitors staying on one page!</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #19 End -->
      <!-- Bonus #20 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w600">BONUS 20</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus20.webp" class="img-fluid mx-auto d-block " alt="Bonus20">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w600 lh140 bonus-title-color text-capitalize">
                        Seo Revolution
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                          <li><b> Today, only the smartest search engine optimizers with latest SEO techniques and strategies and the best content writers reach the pedestal where they are the number one hit on the SERPs. </b></li> 
                          <li>If you are thinking about revamping your SEO strategy, here is an excellent opportunity to attract tons of website traffic. </li> 
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #20 End -->
      <!-- Huge Woth Section Start -->
      <div class="huge-area mt30 mt-md10">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="f-md-65 f-40 lh120 w600 white-clr">That's Huge Worth of</div>
                  <br>
                  <div class="f-md-60 f-40 lh120 w800 yellow-clr">$3300!</div>
               </div>
            </div>
         </div>
      </div>
      <!-- Huge Worth Section End -->
      <!-- text Area Start -->
      <div class="white-section pb0">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="f-md-36 f-25 lh140 w500">So what are you waiting for? You have a great opportunity <br class="hidden-xs"> ahead + <span class="w600 ">My 20 Bonus Products</span> are making it a <br class="hidden-xs"> <span class="w600 ">completely NO Brainer!!</span></div>
               </div>
            </div>
         </div>
      </div>
      <!-- text Area End -->
      <!-- CTA Button Section Start -->
      <div class="cta-btn-section">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center">
               <!-- <div class="f-16 f-md-20 lh150 w400 text-center mt20 black-clr">
                     Use Coupon Code <span class="w600 red-clr">"ATULVIP"</span> for an Additional Discount on Commercial Licence
                  </div> -->
                  <a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn mt15 mt-md20">
                  <span class="text-center">Grab QuickFunnel + My 20 Exclusive Bonuses</span> 
                  </a>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 black-clr mt-md30">
                  Use Coupon Code <span class="w600 red-clr">"ATULBUNDLE"</span> for an Additional <span class="w600 red-clr">$30 Discount</span> on Bundle Deal
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
                  <a href="https://quickfunnel.live/bundle/" class="text-center bonusbuy-btn">
                  <span class="text-center">Grab QuickFunnel Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-12 mt15 mt-md20" align="center">
                  <h3 class="f-md-28 f-20 w500 text-center black">My Exclusive Bonuses Are Expiring in...</h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 col-md-10 mx-auto col-12 text-center">
                  <div class="countdown counter-black">
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                     <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                     <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->
      <div class="footer-section" style="font-size: 14px;">
         <div class="container " style="font-size: 14px;">
            <div class="row" style="font-size: 14px;">
               <div class="col-12 text-center" style="font-size: 14px;">
               <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 693.1 109.7" style="enable-background:new 0 0 693.1 109.7; height:45px;" xml:space="preserve">
                        <style type="text/css">
                           .st0{fill:#FFFFFF;}
                           .st1{fill:#EF3E39;}
                           .st2{fill:#853594;}
                           .st3{fill:#EF3E3A;}
                        </style>
                        <g>
	                        <g id="Layer_1-2">
		                        <path class="st0" d="M140.4,0H18.9c-2.8-0.1-5.2,2.1-5.4,4.9s2.1,5.2,4.9,5.4c0.2,0,0.3,0,0.5,0h42.3c2.8,0,5.1,2.3,5.1,5.1
                              c0,2.8-2.3,5.1-5.1,5.1h-56c-2.8,0.1-5,2.5-4.9,5.4c0.1,2.7,2.3,4.8,4.9,4.9h35.6l0,0h13.5c2.8,0.1,5,2.5,4.9,5.4
                              c-0.1,2.7-2.3,4.8-4.9,4.9H46c-2.8,0-5.1,2.4-5.1,5.2c0,2.7,2.1,4.9,4.8,5.1h17.8c2.8,0,5.2,2.2,5.2,5.1c0,2.8-2.2,5.2-5.1,5.2h-3
                              c-1.3,0-2.6,0.6-3.5,1.5c-2,2-2,5.2-0.1,7.3c1,1,2.3,1.5,3.7,1.5h8v37.7L103.1,96V68.6l42.8-57.6c2.3-3,1.6-7.3-1.4-9.6
                              C143.3,0.5,141.9,0,140.4,0z M66.2,36c0-2.8,2.3-5.1,5.1-5.1h10c2.8,0.1,5,2.5,4.9,5.4c-0.1,2.7-2.3,4.8-4.9,4.9h-10
                              C68.5,41.1,66.2,38.8,66.2,36z"></path>
		                        <path class="st1" d="M34,46.3c0,2.8-2.3,5.1-5.1,5.1h-10c-2.8,0-5.1-2.3-5.1-5.1c0-2.8,2.3-5.1,5.1-5.1c0,0,0,0,0,0h10
			                     C31.7,41.2,34,43.5,34,46.3z"></path>
		                        <path class="st1" d="M48.8,66.9c0,2.8-2.3,5.1-5.1,5.1h-2.9c-2.8,0-5.1-2.1-5.3-4.8c-0.1-1.5,0.4-3,1.5-4c0.9-0.9,2.2-1.5,3.5-1.5
			                     h3.4C46.6,61.8,48.9,64.1,48.8,66.9z"></path>
                              <path class="st2" d="M45.8,61.7L45.8,61.7z"></path>
                              <path class="st2" d="M63.7,61.7L63.7,61.7z"></path>
                              <path class="st2" d="M63.7,61.7L63.7,61.7z"></path>
                              <path class="st3" d="M230,48.3c0,4.5-0.7,8.9-2.3,13.1c-1.5,3.9-3.7,7.5-6.5,10.5c-2.8,3-6.1,5.4-9.9,7c-8,3.3-17,3.3-24.9,0
                              c-3.7-1.6-7.1-4-9.8-7c-2.8-3.1-5-6.7-6.4-10.5c-1.6-4.2-2.4-8.7-2.3-13.1c0-4.5,0.7-9,2.3-13.2c1.4-3.9,3.6-7.5,6.4-10.5
                              c2.8-3,6.1-5.4,9.8-7c8-3.3,17-3.3,24.9,0c3.7,1.6,7.1,4,9.9,7c2.8,3.1,5,6.6,6.5,10.5C229.3,39.3,230,43.8,230,48.3z M222.1,48.3
                              c0.1-4.6-0.9-9.2-3-13.4c-1.9-3.7-4.7-6.9-8.2-9.1c-7.4-4.4-16.6-4.4-24,0c-3.5,2.3-6.3,5.4-8.2,9.1c-2.1,4.2-3.1,8.7-3,13.4
                              c-0.1,4.6,1,9.2,3,13.3c1.8,3.7,4.7,6.9,8.2,9.2c7.4,4.4,16.6,4.4,24,0c3.5-2.3,6.3-5.4,8.2-9.2C221.1,57.5,222.1,52.9,222.1,48.3
                              z M233.1,84.8c1.9,0.1,3.4,1.6,3.4,3.5c-0.1,1.3-0.7,2.5-1.7,3.4c-1.3,1.2-2.8,2.2-4.4,2.9c-1.8,0.9-3.7,1.5-5.7,2
                              c-1.8,0.5-3.7,0.7-5.6,0.7c-3.1,0.1-6.2-0.3-9.2-1.2c-2.1-0.7-4.2-1.5-6.2-2.6c-1.8-1-3.5-1.9-5.2-2.6c-1.9-0.8-3.9-1.2-6-1.2
                              c-1,0-2,0.2-2.9,0.6c-0.8,0.4-1.6,0.7-2.3,1.1c-0.8,0.4-1.7,0.6-2.6,0.6c-1,0-2-0.4-2.7-1.1c-0.6-0.7-1-1.6-1-2.5
                              c-0.1-1.4,0.8-2.8,2.1-3.2L200,79l8.4,0.8l-15.7,5.5l2.6-1.9c2.2,0,4.4,0.3,6.4,1.1c1.9,0.7,3.7,1.5,5.5,2.5
                              c1.8,0.9,3.6,1.8,5.5,2.5c2.1,0.8,4.4,1.1,6.6,1.1c1.8,0,3.7-0.3,5.4-1c1.2-0.5,2.4-1.2,3.6-1.9c0.8-0.6,1.7-1.3,2.4-2
                              C231.3,85.1,232.1,84.8,233.1,84.8L233.1,84.8z"></path>
		                        <path class="st3" d="M277.1,32.9c1,0,2,0.4,2.7,1.1c0.7,0.7,1,1.7,1,2.6v25.1c0,6.2-1.7,11-5.2,14.4s-8.3,5.1-14.5,5.1
                              c-6.2,0-11-1.7-14.4-5.1s-5.2-8.2-5.2-14.4V36.6c0-2,1.5-3.7,3.5-3.7c0,0,0.1,0,0.1,0c1,0,2,0.4,2.7,1.1c0.7,0.7,1,1.7,1,2.6v25.1
                              c0,4.2,1.1,7.3,3.2,9.5s5.2,3.2,9,3.2c4,0,7.1-1.1,9.2-3.2s3.2-5.3,3.2-9.5V36.6c0-2,1.5-3.7,3.5-3.7
                              C277,32.9,277,32.9,277.1,32.9L277.1,32.9z"></path>
		                        <path class="st3" d="M297.9,26c-1.2,0.1-2.4-0.2-3.4-0.9c-0.8-0.7-1.2-1.8-1.1-2.9v-1.3c-0.1-1.1,0.4-2.2,1.2-2.9
                              c1-0.7,2.2-1,3.4-0.9c1.2-0.1,2.4,0.2,3.3,0.9c0.8,0.7,1.2,1.8,1.1,2.9v1.3c0.1,1.1-0.3,2.1-1.1,2.9
                              C300.2,25.8,299.1,26.1,297.9,26z M301.5,76.8c0,1-0.4,2-1.1,2.7c-1.4,1.5-3.8,1.5-5.3,0c0,0,0,0,0,0c-0.7-0.7-1-1.7-1-2.7V35.9
                              c-0.1-2,1.5-3.8,3.5-3.9s3.8,1.5,3.9,3.5c0,0.1,0,0.3,0,0.4L301.5,76.8z"></path>
		                        <path class="st3" d="M336,31.7c2.9,0,5.7,0.3,8.5,1c2.2,0.5,4.2,1.5,6,2.8c1.3,0.9,2.2,2.4,2.2,4c0,0.8-0.3,1.6-0.8,2.3
                              c-0.5,0.7-1.3,1.1-2.2,1.1c-0.7,0-1.4-0.1-2.1-0.5c-0.5-0.3-1-0.7-1.5-1.2c-0.5-0.5-1.2-1-1.9-1.3c-1-0.5-2.1-0.8-3.2-1
                              c-1.4-0.2-2.8-0.4-4.2-0.4c-3.1-0.1-6.2,0.8-8.9,2.4c-2.5,1.6-4.6,3.8-6,6.5c-1.5,2.8-2.3,6-2.2,9.2c-0.1,3.2,0.7,6.4,2.1,9.2
                              c1.4,2.6,3.4,4.9,5.9,6.4c2.6,1.6,5.7,2.5,8.8,2.4c1.7,0,3.5-0.1,5.2-0.6c1.1-0.3,2.2-0.7,3.2-1.3c0.9-0.5,1.8-1.1,2.5-1.9
                              c1.4-1.3,3.5-1.2,4.8,0.1c0.6,0.7,0.9,1.6,0.9,2.5c0,1.1-0.8,2.3-2.3,3.5c-1.9,1.4-4,2.5-6.2,3.1c-2.9,0.9-5.9,1.3-8.9,1.3
                              c-4.3,0.1-8.6-1-12.3-3.3c-3.5-2.2-6.3-5.3-8.2-9c-2-3.9-3-8.3-2.9-12.7c-0.1-4.4,1-8.7,3-12.6C319.3,36.2,327.3,31.5,336,31.7z"></path>
		                        <path class="st3" d="M367.1,80.6c-1,0-2-0.4-2.7-1.1c-0.7-0.7-1-1.7-1-2.7V16c0-1,0.3-1.9,1-2.6c0.7-0.7,1.7-1.1,2.7-1.1
                              c2,0,3.7,1.5,3.7,3.5c0,0,0,0.1,0,0.1v60.8c0,1-0.4,2-1.1,2.7C369,80.2,368.1,80.6,367.1,80.6z M397.7,32.6c1,0,1.9,0.4,2.5,1.2
                              c1.3,1.2,1.4,3.2,0.2,4.6c-0.2,0.2-0.4,0.4-0.6,0.5L370,65l-0.3-8.6l25.5-22.7C395.8,33,396.7,32.6,397.7,32.6z M398.7,80.5
                              c-1,0-2-0.4-2.7-1.2l-19.7-20.9l5.5-5.1l19.5,20.8c0.7,0.7,1.1,1.7,1.1,2.8c0,1-0.4,2-1.3,2.6C400.4,80.1,399.6,80.4,398.7,80.5
                              L398.7,80.5z"></path>
		                        <path class="st0" d="M419,80.6c-1.6,0.1-3.1-0.5-4.2-1.6c-1.1-1.1-1.6-2.5-1.6-4V21.7c-0.1-3,2.3-5.6,5.3-5.7c0.1,0,0.2,0,0.3,0
                              h30c1.5,0,2.9,0.5,4,1.5c1.1,1,1.7,2.4,1.6,3.9c0,1.4-0.6,2.7-1.6,3.6c-1.1,1-2.5,1.6-4,1.6h-24.9l0.8-1.1v18.6l-0.6-1.2h20.6
                              c1.5,0,2.9,0.5,4,1.5c1.1,1,1.7,2.4,1.6,3.9c0,1.4-0.6,2.7-1.6,3.6c-1.1,1-2.5,1.6-4,1.6h-20.8l0.8-0.8V75c0,1.5-0.6,3-1.7,4
                              C421.9,80,420.5,80.6,419,80.6z"></path>
		                        <path class="st0" d="M499.3,31.3c1.5-0.1,3,0.5,4,1.6c1,1.1,1.6,2.5,1.6,4v23.4c0,6.5-1.8,11.7-5.4,15.5
                              c-3.6,3.8-8.9,5.7-15.7,5.7c-6.8,0-12-1.9-15.6-5.7s-5.4-9-5.4-15.5V36.9c0-1.5,0.5-2.9,1.6-4c2.2-2.2,5.7-2.2,7.9,0
                              c1,1.1,1.6,2.5,1.6,4v23.4c0,3.8,0.8,6.5,2.5,8.3s4.1,2.7,7.5,2.7s5.9-0.9,7.6-2.7s2.5-4.6,2.5-8.3V36.9c0-1.5,0.5-2.9,1.6-4
                              C496.4,31.8,497.8,31.2,499.3,31.3z"></path>
		                        <path class="st0" d="M543.2,30.3c4.6,0,8,1,10.4,2.9c2.4,1.9,4.1,4.6,4.9,7.6c0.9,3.4,1.4,7,1.3,10.6V75c0,1.5-0.5,2.9-1.6,4
                              c-2.2,2.2-5.7,2.2-7.9,0c-1-1.1-1.6-2.5-1.6-4V51.3c0-1.9-0.2-3.7-0.8-5.5c-0.5-1.6-1.5-2.9-2.8-3.9c-1.7-1.1-3.8-1.6-5.8-1.5
                              c-2.2-0.1-4.3,0.4-6.2,1.5c-1.6,0.9-3,2.3-3.9,3.9c-0.9,1.7-1.4,3.6-1.3,5.5V75c0,1.5-0.5,2.9-1.6,4c-2.2,2.2-5.7,2.2-7.9,0
                              c-1-1.1-1.6-2.5-1.6-4V36.9c0-1.5,0.5-2.9,1.6-4c2.2-2.2,5.7-2.2,7.9,0c1,1.1,1.6,2.5,1.6,4v4l-1.4-0.3c0.7-1.2,1.5-2.3,2.4-3.4
                              c1.1-1.3,2.3-2.4,3.7-3.4c1.5-1.1,3.1-1.9,4.8-2.5C539.2,30.7,541.2,30.3,543.2,30.3z"></path>
		                        <path class="st0" d="M598.1,30.3c4.5,0,8,1,10.4,2.9c2.4,2,4.1,4.6,4.9,7.6c0.9,3.4,1.4,7,1.3,10.6V75c0,1.5-0.5,2.9-1.6,4
                              c-2.2,2.2-5.7,2.2-7.9,0c-1-1.1-1.6-2.5-1.6-4V51.3c0-1.9-0.2-3.7-0.8-5.5c-0.5-1.6-1.5-2.9-2.8-3.9c-1.7-1.1-3.8-1.6-5.8-1.5
                              c-2.2-0.1-4.3,0.4-6.3,1.5c-1.6,0.9-3,2.3-3.9,3.9c-0.9,1.7-1.4,3.6-1.3,5.5V75c0,1.5-0.5,2.9-1.6,4c-2.2,2.2-5.7,2.2-7.9,0
                              c-1-1.1-1.6-2.5-1.6-4V36.9c0-1.5,0.5-2.9,1.6-4c2.2-2.2,5.7-2.2,7.9,0c1,1.1,1.6,2.5,1.6,4v4l-1.4-0.3c0.7-1.2,1.5-2.3,2.4-3.4
                              c1.1-1.3,2.3-2.4,3.7-3.4c1.5-1.1,3.1-1.9,4.8-2.5C594.2,30.7,596.1,30.3,598.1,30.3z"></path>
		                        <path class="st0" d="M649.9,81.5c-4.7,0.1-9.4-1-13.6-3.3c-3.7-2.1-6.8-5.2-8.8-8.9c-2.1-3.9-3.2-8.3-3.1-12.7
                              c-0.1-4.9,1-9.8,3.4-14.2c2.1-3.7,5.1-6.8,8.8-9c3.5-2,7.5-3.1,11.5-3.1c3.1,0,6.1,0.6,8.9,1.9c2.8,1.3,5.3,3.1,7.4,5.3
                              c2.2,2.3,3.9,4.9,5.1,7.8c1.3,3,1.9,6.2,1.9,9.4c0,1.4-0.7,2.7-1.8,3.6c-1.1,0.9-2.5,1.4-3.9,1.4h-35.2l-2.8-9.2h33.8l-2,1.8v-2.5
                              c-0.1-1.8-0.8-3.4-1.9-4.8c-1.1-1.4-2.6-2.6-4.2-3.4c-1.7-0.8-3.5-1.3-5.4-1.3c-1.8,0-3.5,0.2-5.2,0.7c-1.6,0.5-3,1.3-4.1,2.5
                              c-1.3,1.3-2.3,2.9-2.8,4.7c-0.7,2.4-1.1,4.9-1,7.5c-0.1,2.9,0.6,5.9,2.1,8.4c1.3,2.2,3.1,4,5.3,5.3c2.1,1.2,4.6,1.8,7,1.8
                              c1.8,0,3.7-0.1,5.4-0.6c1.1-0.3,2.3-0.7,3.3-1.3c0.8-0.5,1.6-1,2.3-1.3c1-0.5,2-0.8,3.1-0.8c2.6,0,4.7,2,4.7,4.6c0,0,0,0,0,0.1
                              c-0.1,1.8-1,3.5-2.6,4.5c-2,1.6-4.3,2.8-6.8,3.6C656,81,653,81.5,649.9,81.5z"></path>
		                        <path class="st0" d="M693.1,75c0.1,3.1-2.4,5.6-5.4,5.6c-0.1,0-0.1,0-0.2,0c-1.5,0-2.9-0.6-3.9-1.6c-1.1-1.1-1.6-2.5-1.6-4V18
			                     c-0.1-3.1,2.4-5.6,5.4-5.6c0.1,0,0.1,0,0.2,0c1.5-0.1,2.9,0.5,3.9,1.6c1,1.1,1.6,2.5,1.5,4V75z"></path>
	                        </g>
                        </g>
                     </svg>
                  <div class="f-16 f-md-18 w300 mt20 lh150 white-clr text-center" style="font-size: 16px;">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40" style="font-size: 14px;">
                  <div class="f-16 f-md-18 w300 lh150 white-clr text-xs-center" style="font-size: 16px;">Copyright © QuickFunnel 2023</div>
                  <ul class="footer-ul f-16 f-md-18 w300 white-clr text-center text-md-right">
                     <li style="font-size: 16px;"><a href="https://support.oppyo.com/hc/en-us" class="white-clr t-decoration-none" style="font-size: 16px;">Contact</a> <span class="px5" style="font-size: 16px;">|</span> </li>
                     <li style="font-size: 16px;"><a href="http://quickfunnel.live/legal/privacy-policy.html" class="white-clr t-decoration-none" style="font-size: 16px;">Privacy</a> <span class="px5" style="font-size: 16px;">|</span> </li>
                     <li style="font-size: 16px;"><a href="http://quickfunnel.live/legal/terms-of-service.html" class="white-clr t-decoration-none" style="font-size: 16px;">T&amp;C</a> <span class="px5" style="font-size: 16px;">|</span> </li>
                     <li style="font-size: 16px;"><a href="http://quickfunnel.live/legal/disclaimer.html" class="white-clr t-decoration-none" style="font-size: 16px;">Disclaimer</a> <span class="px5" style="font-size: 16px;">|</span> </li>
                     <li style="font-size: 16px;"><a href="http://quickfunnel.live/legal/gdpr.html" class="white-clr t-decoration-none" style="font-size: 16px;">GDPR</a> <span class="px5" style="font-size: 16px;">|</span> </li>
                     <li style="font-size: 16px;"><a href="http://quickfunnel.live/legal/dmca.html" class="white-clr t-decoration-none" style="font-size: 16px;">DMCA</a> <span class="px5" style="font-size: 16px;">|</span> </li>
                     <li style="font-size: 16px;"><a href="http://quickfunnel.live/legal/anti-spam.html" class="white-clr t-decoration-none" style="font-size: 16px;">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!-- timer --->
      <?php
         if ($now < $exp_date) {
         ?>
      <script type="text/javascript">
         // Count down milliseconds = server_end - server_now = client_end - client_now
         var server_end = <?php echo $exp_date; ?> * 1000;
         var server_now = <?php echo time(); ?> * 1000;
         var client_now = new Date().getTime();
         var end = server_end - server_now + client_now; // this is the real end time
         
         var noob = $('.countdown').length;
         
         var _second = 1000;
         var _minute = _second * 60;
         var _hour = _minute * 60;
         var _day = _hour * 24
         var timer;
         
         function showRemaining() {
         var now = new Date();
         var distance = end - now;
         if (distance < 0) {
         	clearInterval(timer);
         	document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
         	return;
         }
         
         var days = Math.floor(distance / _day);
         var hours = Math.floor((distance % _day) / _hour);
         var minutes = Math.floor((distance % _hour) / _minute);
         var seconds = Math.floor((distance % _minute) / _second);
         if (days < 10) {
         	days = "0" + days;
         }
         if (hours < 10) {
         	hours = "0" + hours;
         }
         if (minutes < 10) {
         	minutes = "0" + minutes;
         }
         if (seconds < 10) {
         	seconds = "0" + seconds;
         }
         var i;
         var countdown = document.getElementsByClassName('countdown');
         for (i = 0; i < noob; i++) {
         	countdown[i].innerHTML = '';
         
         	if (days) {
         		countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + days + '</span><span class="f-14 f-md-18 smmltd">Days</span> </div>';
         	}
         
         	countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + hours + '</span><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>';
         
         	countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">' + minutes + '</span><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>';
         
         	countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">' + seconds + '</span><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>';
         }
         
         }
         timer = setInterval(showRemaining, 1000);
         	
      </script>
      <?php
         } else {
         echo "Times Up";
         }
         ?>
      <!--- timer end-->
   </body>
</html>
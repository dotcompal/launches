<!Doctype html>
<html>
   <head>
      <title>QuickFunnel Prelaunch</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=9">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
      <meta name="title" content="QuickFunnel | Prelaunch">
      <meta name="description" content="The Only One-Stop Trigger & Action Based Funnels and Website Solution For Low One-Time-Price">
      <meta name="keywords" content="QuickFunnel">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta property="og:image" content="https://www.quickfunnel.live/prelaunch/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Dr. Amit Pareek">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="QuickFunnel | Prelaunch">
      <meta property="og:description" content="The Only One-Stop Trigger & Action Based Funnels and Website Solution For Low One-Time-Price">
      <meta property="og:image" content="https://www.quickfunnel.live/prelaunch/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="QuickFunnel | Prelaunch">
      <meta property="twitter:description" content="The Only One-Stop Trigger & Action Based Funnels and Website Solution For Low One-Time-Price">
      <meta property="twitter:image" content="https://www.quickfunnel.live/prelaunch/prelaunch/thumbnail.png">
      <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
      <link rel="stylesheet" href="../common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style.css" type="text/css">
      <link rel="stylesheet" href="assets/css/aweberform.css" type="text/css">
      <!-- Start Editor required -->
      <script>
      function getUrlVars() {
         var vars = [],
            hash;
         var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
         for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
         }
         return vars;
      }
      var first = getUrlVars()["aid"];
      //alert(first);
      document.addEventListener('DOMContentLoaded', (event) => {
         document.getElementById('awf_field_aid').setAttribute('value', first);
      })
      //document.getElementById('myField').value = first;
   </script>
   </head>
   <body>
      <!-- Header Section Start -->
      <div class="header-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row align-items-center">
                     <div class="col-md-3 text-center text-md-start">
                     <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	                  viewBox="0 0 693.1 109.7" style="enable-background:new 0 0 693.1 109.7; height:45px;" xml:space="preserve">
                        <style type="text/css">
                           .st0{fill:#FFFFFF;}
                           .st1{fill:#EF3E39;}
                           .st2{fill:#853594;}
                           .st3{fill:#EF3E3A;}
                        </style>
                        <g>
	                        <g id="Layer_1-2">
		                        <path class="st0" d="M140.4,0H18.9c-2.8-0.1-5.2,2.1-5.4,4.9s2.1,5.2,4.9,5.4c0.2,0,0.3,0,0.5,0h42.3c2.8,0,5.1,2.3,5.1,5.1
                              c0,2.8-2.3,5.1-5.1,5.1h-56c-2.8,0.1-5,2.5-4.9,5.4c0.1,2.7,2.3,4.8,4.9,4.9h35.6l0,0h13.5c2.8,0.1,5,2.5,4.9,5.4
                              c-0.1,2.7-2.3,4.8-4.9,4.9H46c-2.8,0-5.1,2.4-5.1,5.2c0,2.7,2.1,4.9,4.8,5.1h17.8c2.8,0,5.2,2.2,5.2,5.1c0,2.8-2.2,5.2-5.1,5.2h-3
                              c-1.3,0-2.6,0.6-3.5,1.5c-2,2-2,5.2-0.1,7.3c1,1,2.3,1.5,3.7,1.5h8v37.7L103.1,96V68.6l42.8-57.6c2.3-3,1.6-7.3-1.4-9.6
                              C143.3,0.5,141.9,0,140.4,0z M66.2,36c0-2.8,2.3-5.1,5.1-5.1h10c2.8,0.1,5,2.5,4.9,5.4c-0.1,2.7-2.3,4.8-4.9,4.9h-10
                              C68.5,41.1,66.2,38.8,66.2,36z"/>
		                        <path class="st1" d="M34,46.3c0,2.8-2.3,5.1-5.1,5.1h-10c-2.8,0-5.1-2.3-5.1-5.1c0-2.8,2.3-5.1,5.1-5.1c0,0,0,0,0,0h10
			                     C31.7,41.2,34,43.5,34,46.3z"/>
		                        <path class="st1" d="M48.8,66.9c0,2.8-2.3,5.1-5.1,5.1h-2.9c-2.8,0-5.1-2.1-5.3-4.8c-0.1-1.5,0.4-3,1.5-4c0.9-0.9,2.2-1.5,3.5-1.5
			                     h3.4C46.6,61.8,48.9,64.1,48.8,66.9z"/>
                              <path class="st2" d="M45.8,61.7L45.8,61.7z"/>
                              <path class="st2" d="M63.7,61.7L63.7,61.7z"/>
                              <path class="st2" d="M63.7,61.7L63.7,61.7z"/>
                              <path class="st3" d="M230,48.3c0,4.5-0.7,8.9-2.3,13.1c-1.5,3.9-3.7,7.5-6.5,10.5c-2.8,3-6.1,5.4-9.9,7c-8,3.3-17,3.3-24.9,0
                              c-3.7-1.6-7.1-4-9.8-7c-2.8-3.1-5-6.7-6.4-10.5c-1.6-4.2-2.4-8.7-2.3-13.1c0-4.5,0.7-9,2.3-13.2c1.4-3.9,3.6-7.5,6.4-10.5
                              c2.8-3,6.1-5.4,9.8-7c8-3.3,17-3.3,24.9,0c3.7,1.6,7.1,4,9.9,7c2.8,3.1,5,6.6,6.5,10.5C229.3,39.3,230,43.8,230,48.3z M222.1,48.3
                              c0.1-4.6-0.9-9.2-3-13.4c-1.9-3.7-4.7-6.9-8.2-9.1c-7.4-4.4-16.6-4.4-24,0c-3.5,2.3-6.3,5.4-8.2,9.1c-2.1,4.2-3.1,8.7-3,13.4
                              c-0.1,4.6,1,9.2,3,13.3c1.8,3.7,4.7,6.9,8.2,9.2c7.4,4.4,16.6,4.4,24,0c3.5-2.3,6.3-5.4,8.2-9.2C221.1,57.5,222.1,52.9,222.1,48.3
                              z M233.1,84.8c1.9,0.1,3.4,1.6,3.4,3.5c-0.1,1.3-0.7,2.5-1.7,3.4c-1.3,1.2-2.8,2.2-4.4,2.9c-1.8,0.9-3.7,1.5-5.7,2
                              c-1.8,0.5-3.7,0.7-5.6,0.7c-3.1,0.1-6.2-0.3-9.2-1.2c-2.1-0.7-4.2-1.5-6.2-2.6c-1.8-1-3.5-1.9-5.2-2.6c-1.9-0.8-3.9-1.2-6-1.2
                              c-1,0-2,0.2-2.9,0.6c-0.8,0.4-1.6,0.7-2.3,1.1c-0.8,0.4-1.7,0.6-2.6,0.6c-1,0-2-0.4-2.7-1.1c-0.6-0.7-1-1.6-1-2.5
                              c-0.1-1.4,0.8-2.8,2.1-3.2L200,79l8.4,0.8l-15.7,5.5l2.6-1.9c2.2,0,4.4,0.3,6.4,1.1c1.9,0.7,3.7,1.5,5.5,2.5
                              c1.8,0.9,3.6,1.8,5.5,2.5c2.1,0.8,4.4,1.1,6.6,1.1c1.8,0,3.7-0.3,5.4-1c1.2-0.5,2.4-1.2,3.6-1.9c0.8-0.6,1.7-1.3,2.4-2
                              C231.3,85.1,232.1,84.8,233.1,84.8L233.1,84.8z"/>
		                        <path class="st3" d="M277.1,32.9c1,0,2,0.4,2.7,1.1c0.7,0.7,1,1.7,1,2.6v25.1c0,6.2-1.7,11-5.2,14.4s-8.3,5.1-14.5,5.1
                              c-6.2,0-11-1.7-14.4-5.1s-5.2-8.2-5.2-14.4V36.6c0-2,1.5-3.7,3.5-3.7c0,0,0.1,0,0.1,0c1,0,2,0.4,2.7,1.1c0.7,0.7,1,1.7,1,2.6v25.1
                              c0,4.2,1.1,7.3,3.2,9.5s5.2,3.2,9,3.2c4,0,7.1-1.1,9.2-3.2s3.2-5.3,3.2-9.5V36.6c0-2,1.5-3.7,3.5-3.7
                              C277,32.9,277,32.9,277.1,32.9L277.1,32.9z"/>
		                        <path class="st3" d="M297.9,26c-1.2,0.1-2.4-0.2-3.4-0.9c-0.8-0.7-1.2-1.8-1.1-2.9v-1.3c-0.1-1.1,0.4-2.2,1.2-2.9
                              c1-0.7,2.2-1,3.4-0.9c1.2-0.1,2.4,0.2,3.3,0.9c0.8,0.7,1.2,1.8,1.1,2.9v1.3c0.1,1.1-0.3,2.1-1.1,2.9
                              C300.2,25.8,299.1,26.1,297.9,26z M301.5,76.8c0,1-0.4,2-1.1,2.7c-1.4,1.5-3.8,1.5-5.3,0c0,0,0,0,0,0c-0.7-0.7-1-1.7-1-2.7V35.9
                              c-0.1-2,1.5-3.8,3.5-3.9s3.8,1.5,3.9,3.5c0,0.1,0,0.3,0,0.4L301.5,76.8z"/>
		                        <path class="st3" d="M336,31.7c2.9,0,5.7,0.3,8.5,1c2.2,0.5,4.2,1.5,6,2.8c1.3,0.9,2.2,2.4,2.2,4c0,0.8-0.3,1.6-0.8,2.3
                              c-0.5,0.7-1.3,1.1-2.2,1.1c-0.7,0-1.4-0.1-2.1-0.5c-0.5-0.3-1-0.7-1.5-1.2c-0.5-0.5-1.2-1-1.9-1.3c-1-0.5-2.1-0.8-3.2-1
                              c-1.4-0.2-2.8-0.4-4.2-0.4c-3.1-0.1-6.2,0.8-8.9,2.4c-2.5,1.6-4.6,3.8-6,6.5c-1.5,2.8-2.3,6-2.2,9.2c-0.1,3.2,0.7,6.4,2.1,9.2
                              c1.4,2.6,3.4,4.9,5.9,6.4c2.6,1.6,5.7,2.5,8.8,2.4c1.7,0,3.5-0.1,5.2-0.6c1.1-0.3,2.2-0.7,3.2-1.3c0.9-0.5,1.8-1.1,2.5-1.9
                              c1.4-1.3,3.5-1.2,4.8,0.1c0.6,0.7,0.9,1.6,0.9,2.5c0,1.1-0.8,2.3-2.3,3.5c-1.9,1.4-4,2.5-6.2,3.1c-2.9,0.9-5.9,1.3-8.9,1.3
                              c-4.3,0.1-8.6-1-12.3-3.3c-3.5-2.2-6.3-5.3-8.2-9c-2-3.9-3-8.3-2.9-12.7c-0.1-4.4,1-8.7,3-12.6C319.3,36.2,327.3,31.5,336,31.7z"
                              />
		                        <path class="st3" d="M367.1,80.6c-1,0-2-0.4-2.7-1.1c-0.7-0.7-1-1.7-1-2.7V16c0-1,0.3-1.9,1-2.6c0.7-0.7,1.7-1.1,2.7-1.1
                              c2,0,3.7,1.5,3.7,3.5c0,0,0,0.1,0,0.1v60.8c0,1-0.4,2-1.1,2.7C369,80.2,368.1,80.6,367.1,80.6z M397.7,32.6c1,0,1.9,0.4,2.5,1.2
                              c1.3,1.2,1.4,3.2,0.2,4.6c-0.2,0.2-0.4,0.4-0.6,0.5L370,65l-0.3-8.6l25.5-22.7C395.8,33,396.7,32.6,397.7,32.6z M398.7,80.5
                              c-1,0-2-0.4-2.7-1.2l-19.7-20.9l5.5-5.1l19.5,20.8c0.7,0.7,1.1,1.7,1.1,2.8c0,1-0.4,2-1.3,2.6C400.4,80.1,399.6,80.4,398.7,80.5
                              L398.7,80.5z"/>
		                        <path class="st0" d="M419,80.6c-1.6,0.1-3.1-0.5-4.2-1.6c-1.1-1.1-1.6-2.5-1.6-4V21.7c-0.1-3,2.3-5.6,5.3-5.7c0.1,0,0.2,0,0.3,0
                              h30c1.5,0,2.9,0.5,4,1.5c1.1,1,1.7,2.4,1.6,3.9c0,1.4-0.6,2.7-1.6,3.6c-1.1,1-2.5,1.6-4,1.6h-24.9l0.8-1.1v18.6l-0.6-1.2h20.6
                              c1.5,0,2.9,0.5,4,1.5c1.1,1,1.7,2.4,1.6,3.9c0,1.4-0.6,2.7-1.6,3.6c-1.1,1-2.5,1.6-4,1.6h-20.8l0.8-0.8V75c0,1.5-0.6,3-1.7,4
                              C421.9,80,420.5,80.6,419,80.6z"/>
		                        <path class="st0" d="M499.3,31.3c1.5-0.1,3,0.5,4,1.6c1,1.1,1.6,2.5,1.6,4v23.4c0,6.5-1.8,11.7-5.4,15.5
                              c-3.6,3.8-8.9,5.7-15.7,5.7c-6.8,0-12-1.9-15.6-5.7s-5.4-9-5.4-15.5V36.9c0-1.5,0.5-2.9,1.6-4c2.2-2.2,5.7-2.2,7.9,0
                              c1,1.1,1.6,2.5,1.6,4v23.4c0,3.8,0.8,6.5,2.5,8.3s4.1,2.7,7.5,2.7s5.9-0.9,7.6-2.7s2.5-4.6,2.5-8.3V36.9c0-1.5,0.5-2.9,1.6-4
                              C496.4,31.8,497.8,31.2,499.3,31.3z"/>
		                        <path class="st0" d="M543.2,30.3c4.6,0,8,1,10.4,2.9c2.4,1.9,4.1,4.6,4.9,7.6c0.9,3.4,1.4,7,1.3,10.6V75c0,1.5-0.5,2.9-1.6,4
                              c-2.2,2.2-5.7,2.2-7.9,0c-1-1.1-1.6-2.5-1.6-4V51.3c0-1.9-0.2-3.7-0.8-5.5c-0.5-1.6-1.5-2.9-2.8-3.9c-1.7-1.1-3.8-1.6-5.8-1.5
                              c-2.2-0.1-4.3,0.4-6.2,1.5c-1.6,0.9-3,2.3-3.9,3.9c-0.9,1.7-1.4,3.6-1.3,5.5V75c0,1.5-0.5,2.9-1.6,4c-2.2,2.2-5.7,2.2-7.9,0
                              c-1-1.1-1.6-2.5-1.6-4V36.9c0-1.5,0.5-2.9,1.6-4c2.2-2.2,5.7-2.2,7.9,0c1,1.1,1.6,2.5,1.6,4v4l-1.4-0.3c0.7-1.2,1.5-2.3,2.4-3.4
                              c1.1-1.3,2.3-2.4,3.7-3.4c1.5-1.1,3.1-1.9,4.8-2.5C539.2,30.7,541.2,30.3,543.2,30.3z"/>
		                        <path class="st0" d="M598.1,30.3c4.5,0,8,1,10.4,2.9c2.4,2,4.1,4.6,4.9,7.6c0.9,3.4,1.4,7,1.3,10.6V75c0,1.5-0.5,2.9-1.6,4
                              c-2.2,2.2-5.7,2.2-7.9,0c-1-1.1-1.6-2.5-1.6-4V51.3c0-1.9-0.2-3.7-0.8-5.5c-0.5-1.6-1.5-2.9-2.8-3.9c-1.7-1.1-3.8-1.6-5.8-1.5
                              c-2.2-0.1-4.3,0.4-6.3,1.5c-1.6,0.9-3,2.3-3.9,3.9c-0.9,1.7-1.4,3.6-1.3,5.5V75c0,1.5-0.5,2.9-1.6,4c-2.2,2.2-5.7,2.2-7.9,0
                              c-1-1.1-1.6-2.5-1.6-4V36.9c0-1.5,0.5-2.9,1.6-4c2.2-2.2,5.7-2.2,7.9,0c1,1.1,1.6,2.5,1.6,4v4l-1.4-0.3c0.7-1.2,1.5-2.3,2.4-3.4
                              c1.1-1.3,2.3-2.4,3.7-3.4c1.5-1.1,3.1-1.9,4.8-2.5C594.2,30.7,596.1,30.3,598.1,30.3z"/>
		                        <path class="st0" d="M649.9,81.5c-4.7,0.1-9.4-1-13.6-3.3c-3.7-2.1-6.8-5.2-8.8-8.9c-2.1-3.9-3.2-8.3-3.1-12.7
                              c-0.1-4.9,1-9.8,3.4-14.2c2.1-3.7,5.1-6.8,8.8-9c3.5-2,7.5-3.1,11.5-3.1c3.1,0,6.1,0.6,8.9,1.9c2.8,1.3,5.3,3.1,7.4,5.3
                              c2.2,2.3,3.9,4.9,5.1,7.8c1.3,3,1.9,6.2,1.9,9.4c0,1.4-0.7,2.7-1.8,3.6c-1.1,0.9-2.5,1.4-3.9,1.4h-35.2l-2.8-9.2h33.8l-2,1.8v-2.5
                              c-0.1-1.8-0.8-3.4-1.9-4.8c-1.1-1.4-2.6-2.6-4.2-3.4c-1.7-0.8-3.5-1.3-5.4-1.3c-1.8,0-3.5,0.2-5.2,0.7c-1.6,0.5-3,1.3-4.1,2.5
                              c-1.3,1.3-2.3,2.9-2.8,4.7c-0.7,2.4-1.1,4.9-1,7.5c-0.1,2.9,0.6,5.9,2.1,8.4c1.3,2.2,3.1,4,5.3,5.3c2.1,1.2,4.6,1.8,7,1.8
                              c1.8,0,3.7-0.1,5.4-0.6c1.1-0.3,2.3-0.7,3.3-1.3c0.8-0.5,1.6-1,2.3-1.3c1-0.5,2-0.8,3.1-0.8c2.6,0,4.7,2,4.7,4.6c0,0,0,0,0,0.1
                              c-0.1,1.8-1,3.5-2.6,4.5c-2,1.6-4.3,2.8-6.8,3.6C656,81,653,81.5,649.9,81.5z"/>
		                        <path class="st0" d="M693.1,75c0.1,3.1-2.4,5.6-5.4,5.6c-0.1,0-0.1,0-0.2,0c-1.5,0-2.9-0.6-3.9-1.6c-1.1-1.1-1.6-2.5-1.6-4V18
			                     c-0.1-3.1,2.4-5.6,5.4-5.6c0.1,0,0.1,0,0.2,0c1.5-0.1,2.9,0.5,3.9,1.6c1,1.1,1.6,2.5,1.5,4V75z"/>
	                        </g>
                        </g>
                     </svg>
                     </div>
                     <div class="col-md-9 mt20 mt-md5">
                        <ul class="leader-ul f-18 f-md-20 white-clr text-md-end text-center">
                           <li class="affiliate-link-btn"><a href="#book-seat" class="t-decoration-none ">Book Your Free Seat</a></li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md70">
               <div class="col-12 text-center">
                  <div class="f-16 f-md-20 w500 white-clr lh140 text-uppercase">
                  REGISTER FOR ONE-TIME-ONLY VALUE-PACKED FREE TRAINING & GET AN ASSURED GIFT + 10 FREE LICENSES
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 f-md-44 f-28 w700 text-center white-clr lh140">
               The Only One-Stop Trigger & Action Based <span class="red-clr"> Funnels and Website Solution </span> For Low One-Time-Price
                   </div>
               <div class="col-12 mt20 mt-md30 f-18 f-md-20 w500 text-center lh140 white-clr text-capitalize">
               In This Free Training, We Will Also Reveal, How You Can Start An Agency & Sell Products, services, Build Email List & Generate tons of Traffic to Boost Commission & Get Started Easily Without Any Tech Hassles.
            </div>
           
         </div>
      </div>
      <div class="container">
            <div class="row d-flex align-items-center flex-wrap mt30 mt-md70">
               <div class="col-md-7 col-12 min-md-video-width-left ">
                  <img src="assets/images/video-thumb.webp" class="img-fluid d-block mx-auto webinar" alt="Prelaunch">
               </div>
               <div class="col-md-5 col-12 mt20 mt-md0 min-md-video-width-right" id="form">
                  <div class="auto_responder_form inp-phold calendar-wrap" id="book-seat">
                     <div class="f-20 f-md-24 w700 text-center lh180 orange-clr1" editabletype="text" style="z-index:10;">
                        Register for Free Training <br>
                        <span class="f-20 f-md-24 w700 text-center black-clr" contenteditable="false">13<sup contenteditable="false">th</sup> Feb, 10 AM EST</span>
                     </div>
                     <div class="col-12 f-18 f-md-20 w500 lh150 text-center">
                        Book Your Seat now<br> (Limited to 100 Users)
                     </div>
                     <!-- Aweber Form Code -->
                     <div class="col-12 p0 mt15 mt-md20">
                     <form method="post" class="af-form-wrapper register-form-style1 " accept-charset="UTF-8" action="https://www.aweber.com/scripts/addlead.pl" style="z-index: 10;">
                           <div style="display: none;">
                              <input type="hidden" name="meta_web_form_id" value="2081593959" />
                              <input type="hidden" name="meta_split_id" value="" />
                              <input type="hidden" name="listname" value="awlist6446172" />
                              <input type="hidden" name="redirect" value="https://www.quickfunnel.live/prelaunch-thankyou" id="redirect_ed736e2aa37b85584ce7d4592c8f290e" />
                              <input type="hidden" name="meta_adtracking" value="My_Web_Form" />
                              <input type="hidden" name="meta_message" value="1" />
                              <input type="hidden" name="meta_required" value="name,email" />
                              <input type="hidden" name="meta_tooltip" value="" />
                           </div>
                           <div id="af-form-2081593959" class="af-form">
                              <div id="af-body-2081593959" class="af-body af-standards">
                                 <div class="af-element width-form1">
                                    <label class="previewLabel" for="awf_field-115292277"></label>
                                    <div class="af-textWrap">
                                       <div class="input-type" style="z-index: 11;">
                                          <!--<span class="input-group-addon border1px"><i class="fa fa-user" aria-hidden="true"></i></span>-->
                                          <input id="awf_field-115292277" type="text" name="name" class="text form-control custom-input input-field" value="" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="500" placeholder="Your Name" />
                                       </div>
                                    </div>
                                    <div class="af-clear bottom-margin"></div>
                                 </div>
                                 <div class="af-element width-form1">
                                    <label class="previewLabel" for="awf_field-115292278"> </label>
                                    <div class="af-textWrap">
                                       <div class="input-type" style="z-index: 11;">
                                          <!--<span class="input-group-addon border1px"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>-->
                                          <input class="text form-control custom-input input-field" id="awf_field-115292278" type="text" name="email" value="" tabindex="501" onFocus=" if (this.value == '') { this.value = ''; }" onBlur="if (this.value == '') { this.value='';} " placeholder="Your Email" />
                                       </div>
                                    </div>
                                    <div class="af-clear bottom-margin"></div>
                                 </div>
                                 <input type="hidden" id="awf_field_aid" class="text" name="custom aid" value="" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="502" />
                                 <div class="af-element buttonContainer button-type register-btn1 f-22 f-md-21 mt-md0">
                                    <input name="submit" id="af-submit-image-348552691" type="submit" class="image" style="max-width: 100%;" alt="Submit Form" tabindex="503" value="Book Your Free Seat" />
                                    <div class="af-clear bottom-margin"></div>
                                 </div>
                              </div>
                           </div>
                           <div style="display: none;"><img src="https://forms.aweber.com/form/displays.htm?id=TIws7ExsjCwMzA==" alt="Form" /></div>
                        </form>
                     </div>
                  </div>
               </div>
         </div>
      </div>
      </div>
      
      <!-- Header Section End -->

      <!-- Second Section Start -->
      <div class="second-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="col-12">
                     <div class="row">
                        <div class="col-12">
                           <div class="f-md-34 f-26 w700 lh140 text-capitalize text-center red-clr">
                           Why Attend This Free Training Webinar & What Will You Learn?
                           </div>
                           <div class="f-20 f-md-22 w600 lh150 text-capitalize text-center black-clr mt20">
                           Our 20+ Years Of Experience Is Packaged In This State-Of-Art 1-Hour Webinar Where You'll Learn:
                           </div>
                        </div>
                        <div class="col-12 mt20 mt-md50">
                           <ul class="features-list pl0 f-18 f-md-18 lh150 w400 black-clr text-capitalize">
                             <li>How You Can Sell more of your products, services & training using fast-loading funnels</li> 
                             <li>Build a huge email list for passive income</li> 
                             <li>Boost affiliate commissions using high converting review pages & sites.</li> 
                             <li>How you can build those funnels within minutes using 400+ DFY templates</li> 
                             <li>Also How To Sell high in-demand funnel & website creation services to clients with included commercial license.</li> 
                             <li>Lastly, everyone who attends this session will get a chance to Win a Free Gift as we are giving 10 licenses of our Premium solution – QuickFunnel </li> 
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md70">
            <div class="f-20 f-md-26 w600 lh150 text-capitalize text-center black-clr mt20">
            Hurry Up! Register For This Free Training Webinar & Join Our EarlyBird VIP List Now! 
                           </div>
               <div class="col-12 col-md-12 mx-auto mt30 mt-md40" id="form">
                  <div class="auto_responder_form inp-phold formbg" id="book-seat1">
                     <div class="f-md-45 f-26 lh140 w700 text-center white-clr" editabletype="text" style="z-index:10;">
                        So Make Sure You Do NOT Miss This Webinar
                     </div>
                     <div class="col-12 f-20 f-md-24 w400 text-center mt10 white-clr">
                        Register Now! And Join Us Live on February 13th, 10:00 AM EST
                     </div>
                      <!-- Aweber Form Code -->
                  <div class="col-12 p0 mt15 mt-md25" editabletype="form" style="z-index:10">
                     <!-- Aweber Form Code -->
                     <form method="post" class="af-form-wrapper register-form-style1 " accept-charset="UTF-8" action="https://www.aweber.com/scripts/addlead.pl" style="z-index: 10;">
                        <div style="display: none;">
                           <input type="hidden" name="meta_web_form_id" value="2081593959" />
                           <input type="hidden" name="meta_split_id" value="">
                           <input type="hidden" name="listname" value="awlist6446172">
                           <input type="hidden" name="redirect" value="https://www.quickfunnel.live/prelaunch-thankyou" id="redirect_ed736e2aa37b85584ce7d4592c8f290e">
                           <input type="hidden" name="meta_adtracking" value="My_Web_Form">
                           <input type="hidden" name="meta_message" value="1">
                           <input type="hidden" name="meta_required" value="name,email">
                           <input type="hidden" name="meta_tooltip" value="">
                        </div>
                        <div id="af-form-2081593959" class="af-form">
                           <div id="af-body-2081593959" class="af-body af-standards">
                              <div class="af-element width-form">
                                 <label class="previewLabel" for="awf_field-115292277"></label>
                                 <div class="af-textWrap">
                                    <div class="input-type" style="z-index: 11;">
                                       <!--<span class="input-group-addon border1px"><i class="fa fa-user" aria-hidden="true"></i></span>-->
                                       <input id="awf_field-115292277" type="text" name="name" class="text form-control custom-input input-field" value="" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="500" placeholder="Your Name">
                                    </div>
                                 </div>
                                 <div class="af-clear bottom-margin"></div>
                              </div>
                              <div class="af-element width-form">
                                 <label class="previewLabel" for="awf_field-115292278"> </label>
                                 <div class="af-textWrap">
                                    <div class="input-type" style="z-index: 11;">
                                       <!--<span class="input-group-addon border1px"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>-->
                                       <input class="text form-control custom-input input-field" id="awf_field-115292278" type="text" name="email" value="" tabindex="501" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " placeholder="Your Email">
                                    </div>
                                 </div>
                                 <div class="af-clear bottom-margin"></div>
                              </div>
                              <input type="hidden" id="awf_field_aid" class="text" name="custom aid" value="undefined" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="502">
                              <div class="af-element buttonContainer button-type register-btn1 f-22 f-md-21 width-form mt-md0">
                                 <input name="submit" id="af-submit-image-348552691" type="submit" class="image" style="max-width: 100%;" alt="Submit Form" tabindex="503" value="Book Your Free Seat">
                                 <div class="af-clear bottom-margin"></div>
                              </div>
                           </div>
                        </div>
                        <div style="display: none;"><img src="https://forms.aweber.com/form/displays.htm?id=TIws7ExsjCwMzA==" alt="Form"></div>
                     </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Header Section End -->
      
      <!--Footer Section Start -->
      <div class="footer-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
               <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	                  viewBox="0 0 693.1 109.7" style="enable-background:new 0 0 693.1 109.7; height:45px;" xml:space="preserve">
                        <style type="text/css">
                           .st0{fill:#FFFFFF;}
                           .st1{fill:#EF3E39;}
                           .st2{fill:#853594;}
                           .st3{fill:#EF3E3A;}
                        </style>
                        <g>
	                        <g id="Layer_1-2">
		                        <path class="st0" d="M140.4,0H18.9c-2.8-0.1-5.2,2.1-5.4,4.9s2.1,5.2,4.9,5.4c0.2,0,0.3,0,0.5,0h42.3c2.8,0,5.1,2.3,5.1,5.1
                              c0,2.8-2.3,5.1-5.1,5.1h-56c-2.8,0.1-5,2.5-4.9,5.4c0.1,2.7,2.3,4.8,4.9,4.9h35.6l0,0h13.5c2.8,0.1,5,2.5,4.9,5.4
                              c-0.1,2.7-2.3,4.8-4.9,4.9H46c-2.8,0-5.1,2.4-5.1,5.2c0,2.7,2.1,4.9,4.8,5.1h17.8c2.8,0,5.2,2.2,5.2,5.1c0,2.8-2.2,5.2-5.1,5.2h-3
                              c-1.3,0-2.6,0.6-3.5,1.5c-2,2-2,5.2-0.1,7.3c1,1,2.3,1.5,3.7,1.5h8v37.7L103.1,96V68.6l42.8-57.6c2.3-3,1.6-7.3-1.4-9.6
                              C143.3,0.5,141.9,0,140.4,0z M66.2,36c0-2.8,2.3-5.1,5.1-5.1h10c2.8,0.1,5,2.5,4.9,5.4c-0.1,2.7-2.3,4.8-4.9,4.9h-10
                              C68.5,41.1,66.2,38.8,66.2,36z"/>
		                        <path class="st1" d="M34,46.3c0,2.8-2.3,5.1-5.1,5.1h-10c-2.8,0-5.1-2.3-5.1-5.1c0-2.8,2.3-5.1,5.1-5.1c0,0,0,0,0,0h10
			                     C31.7,41.2,34,43.5,34,46.3z"/>
		                        <path class="st1" d="M48.8,66.9c0,2.8-2.3,5.1-5.1,5.1h-2.9c-2.8,0-5.1-2.1-5.3-4.8c-0.1-1.5,0.4-3,1.5-4c0.9-0.9,2.2-1.5,3.5-1.5
			                     h3.4C46.6,61.8,48.9,64.1,48.8,66.9z"/>
                              <path class="st2" d="M45.8,61.7L45.8,61.7z"/>
                              <path class="st2" d="M63.7,61.7L63.7,61.7z"/>
                              <path class="st2" d="M63.7,61.7L63.7,61.7z"/>
                              <path class="st3" d="M230,48.3c0,4.5-0.7,8.9-2.3,13.1c-1.5,3.9-3.7,7.5-6.5,10.5c-2.8,3-6.1,5.4-9.9,7c-8,3.3-17,3.3-24.9,0
                              c-3.7-1.6-7.1-4-9.8-7c-2.8-3.1-5-6.7-6.4-10.5c-1.6-4.2-2.4-8.7-2.3-13.1c0-4.5,0.7-9,2.3-13.2c1.4-3.9,3.6-7.5,6.4-10.5
                              c2.8-3,6.1-5.4,9.8-7c8-3.3,17-3.3,24.9,0c3.7,1.6,7.1,4,9.9,7c2.8,3.1,5,6.6,6.5,10.5C229.3,39.3,230,43.8,230,48.3z M222.1,48.3
                              c0.1-4.6-0.9-9.2-3-13.4c-1.9-3.7-4.7-6.9-8.2-9.1c-7.4-4.4-16.6-4.4-24,0c-3.5,2.3-6.3,5.4-8.2,9.1c-2.1,4.2-3.1,8.7-3,13.4
                              c-0.1,4.6,1,9.2,3,13.3c1.8,3.7,4.7,6.9,8.2,9.2c7.4,4.4,16.6,4.4,24,0c3.5-2.3,6.3-5.4,8.2-9.2C221.1,57.5,222.1,52.9,222.1,48.3
                              z M233.1,84.8c1.9,0.1,3.4,1.6,3.4,3.5c-0.1,1.3-0.7,2.5-1.7,3.4c-1.3,1.2-2.8,2.2-4.4,2.9c-1.8,0.9-3.7,1.5-5.7,2
                              c-1.8,0.5-3.7,0.7-5.6,0.7c-3.1,0.1-6.2-0.3-9.2-1.2c-2.1-0.7-4.2-1.5-6.2-2.6c-1.8-1-3.5-1.9-5.2-2.6c-1.9-0.8-3.9-1.2-6-1.2
                              c-1,0-2,0.2-2.9,0.6c-0.8,0.4-1.6,0.7-2.3,1.1c-0.8,0.4-1.7,0.6-2.6,0.6c-1,0-2-0.4-2.7-1.1c-0.6-0.7-1-1.6-1-2.5
                              c-0.1-1.4,0.8-2.8,2.1-3.2L200,79l8.4,0.8l-15.7,5.5l2.6-1.9c2.2,0,4.4,0.3,6.4,1.1c1.9,0.7,3.7,1.5,5.5,2.5
                              c1.8,0.9,3.6,1.8,5.5,2.5c2.1,0.8,4.4,1.1,6.6,1.1c1.8,0,3.7-0.3,5.4-1c1.2-0.5,2.4-1.2,3.6-1.9c0.8-0.6,1.7-1.3,2.4-2
                              C231.3,85.1,232.1,84.8,233.1,84.8L233.1,84.8z"/>
		                        <path class="st3" d="M277.1,32.9c1,0,2,0.4,2.7,1.1c0.7,0.7,1,1.7,1,2.6v25.1c0,6.2-1.7,11-5.2,14.4s-8.3,5.1-14.5,5.1
                              c-6.2,0-11-1.7-14.4-5.1s-5.2-8.2-5.2-14.4V36.6c0-2,1.5-3.7,3.5-3.7c0,0,0.1,0,0.1,0c1,0,2,0.4,2.7,1.1c0.7,0.7,1,1.7,1,2.6v25.1
                              c0,4.2,1.1,7.3,3.2,9.5s5.2,3.2,9,3.2c4,0,7.1-1.1,9.2-3.2s3.2-5.3,3.2-9.5V36.6c0-2,1.5-3.7,3.5-3.7
                              C277,32.9,277,32.9,277.1,32.9L277.1,32.9z"/>
		                        <path class="st3" d="M297.9,26c-1.2,0.1-2.4-0.2-3.4-0.9c-0.8-0.7-1.2-1.8-1.1-2.9v-1.3c-0.1-1.1,0.4-2.2,1.2-2.9
                              c1-0.7,2.2-1,3.4-0.9c1.2-0.1,2.4,0.2,3.3,0.9c0.8,0.7,1.2,1.8,1.1,2.9v1.3c0.1,1.1-0.3,2.1-1.1,2.9
                              C300.2,25.8,299.1,26.1,297.9,26z M301.5,76.8c0,1-0.4,2-1.1,2.7c-1.4,1.5-3.8,1.5-5.3,0c0,0,0,0,0,0c-0.7-0.7-1-1.7-1-2.7V35.9
                              c-0.1-2,1.5-3.8,3.5-3.9s3.8,1.5,3.9,3.5c0,0.1,0,0.3,0,0.4L301.5,76.8z"/>
		                        <path class="st3" d="M336,31.7c2.9,0,5.7,0.3,8.5,1c2.2,0.5,4.2,1.5,6,2.8c1.3,0.9,2.2,2.4,2.2,4c0,0.8-0.3,1.6-0.8,2.3
                              c-0.5,0.7-1.3,1.1-2.2,1.1c-0.7,0-1.4-0.1-2.1-0.5c-0.5-0.3-1-0.7-1.5-1.2c-0.5-0.5-1.2-1-1.9-1.3c-1-0.5-2.1-0.8-3.2-1
                              c-1.4-0.2-2.8-0.4-4.2-0.4c-3.1-0.1-6.2,0.8-8.9,2.4c-2.5,1.6-4.6,3.8-6,6.5c-1.5,2.8-2.3,6-2.2,9.2c-0.1,3.2,0.7,6.4,2.1,9.2
                              c1.4,2.6,3.4,4.9,5.9,6.4c2.6,1.6,5.7,2.5,8.8,2.4c1.7,0,3.5-0.1,5.2-0.6c1.1-0.3,2.2-0.7,3.2-1.3c0.9-0.5,1.8-1.1,2.5-1.9
                              c1.4-1.3,3.5-1.2,4.8,0.1c0.6,0.7,0.9,1.6,0.9,2.5c0,1.1-0.8,2.3-2.3,3.5c-1.9,1.4-4,2.5-6.2,3.1c-2.9,0.9-5.9,1.3-8.9,1.3
                              c-4.3,0.1-8.6-1-12.3-3.3c-3.5-2.2-6.3-5.3-8.2-9c-2-3.9-3-8.3-2.9-12.7c-0.1-4.4,1-8.7,3-12.6C319.3,36.2,327.3,31.5,336,31.7z"
                              />
		                        <path class="st3" d="M367.1,80.6c-1,0-2-0.4-2.7-1.1c-0.7-0.7-1-1.7-1-2.7V16c0-1,0.3-1.9,1-2.6c0.7-0.7,1.7-1.1,2.7-1.1
                              c2,0,3.7,1.5,3.7,3.5c0,0,0,0.1,0,0.1v60.8c0,1-0.4,2-1.1,2.7C369,80.2,368.1,80.6,367.1,80.6z M397.7,32.6c1,0,1.9,0.4,2.5,1.2
                              c1.3,1.2,1.4,3.2,0.2,4.6c-0.2,0.2-0.4,0.4-0.6,0.5L370,65l-0.3-8.6l25.5-22.7C395.8,33,396.7,32.6,397.7,32.6z M398.7,80.5
                              c-1,0-2-0.4-2.7-1.2l-19.7-20.9l5.5-5.1l19.5,20.8c0.7,0.7,1.1,1.7,1.1,2.8c0,1-0.4,2-1.3,2.6C400.4,80.1,399.6,80.4,398.7,80.5
                              L398.7,80.5z"/>
		                        <path class="st0" d="M419,80.6c-1.6,0.1-3.1-0.5-4.2-1.6c-1.1-1.1-1.6-2.5-1.6-4V21.7c-0.1-3,2.3-5.6,5.3-5.7c0.1,0,0.2,0,0.3,0
                              h30c1.5,0,2.9,0.5,4,1.5c1.1,1,1.7,2.4,1.6,3.9c0,1.4-0.6,2.7-1.6,3.6c-1.1,1-2.5,1.6-4,1.6h-24.9l0.8-1.1v18.6l-0.6-1.2h20.6
                              c1.5,0,2.9,0.5,4,1.5c1.1,1,1.7,2.4,1.6,3.9c0,1.4-0.6,2.7-1.6,3.6c-1.1,1-2.5,1.6-4,1.6h-20.8l0.8-0.8V75c0,1.5-0.6,3-1.7,4
                              C421.9,80,420.5,80.6,419,80.6z"/>
		                        <path class="st0" d="M499.3,31.3c1.5-0.1,3,0.5,4,1.6c1,1.1,1.6,2.5,1.6,4v23.4c0,6.5-1.8,11.7-5.4,15.5
                              c-3.6,3.8-8.9,5.7-15.7,5.7c-6.8,0-12-1.9-15.6-5.7s-5.4-9-5.4-15.5V36.9c0-1.5,0.5-2.9,1.6-4c2.2-2.2,5.7-2.2,7.9,0
                              c1,1.1,1.6,2.5,1.6,4v23.4c0,3.8,0.8,6.5,2.5,8.3s4.1,2.7,7.5,2.7s5.9-0.9,7.6-2.7s2.5-4.6,2.5-8.3V36.9c0-1.5,0.5-2.9,1.6-4
                              C496.4,31.8,497.8,31.2,499.3,31.3z"/>
		                        <path class="st0" d="M543.2,30.3c4.6,0,8,1,10.4,2.9c2.4,1.9,4.1,4.6,4.9,7.6c0.9,3.4,1.4,7,1.3,10.6V75c0,1.5-0.5,2.9-1.6,4
                              c-2.2,2.2-5.7,2.2-7.9,0c-1-1.1-1.6-2.5-1.6-4V51.3c0-1.9-0.2-3.7-0.8-5.5c-0.5-1.6-1.5-2.9-2.8-3.9c-1.7-1.1-3.8-1.6-5.8-1.5
                              c-2.2-0.1-4.3,0.4-6.2,1.5c-1.6,0.9-3,2.3-3.9,3.9c-0.9,1.7-1.4,3.6-1.3,5.5V75c0,1.5-0.5,2.9-1.6,4c-2.2,2.2-5.7,2.2-7.9,0
                              c-1-1.1-1.6-2.5-1.6-4V36.9c0-1.5,0.5-2.9,1.6-4c2.2-2.2,5.7-2.2,7.9,0c1,1.1,1.6,2.5,1.6,4v4l-1.4-0.3c0.7-1.2,1.5-2.3,2.4-3.4
                              c1.1-1.3,2.3-2.4,3.7-3.4c1.5-1.1,3.1-1.9,4.8-2.5C539.2,30.7,541.2,30.3,543.2,30.3z"/>
		                        <path class="st0" d="M598.1,30.3c4.5,0,8,1,10.4,2.9c2.4,2,4.1,4.6,4.9,7.6c0.9,3.4,1.4,7,1.3,10.6V75c0,1.5-0.5,2.9-1.6,4
                              c-2.2,2.2-5.7,2.2-7.9,0c-1-1.1-1.6-2.5-1.6-4V51.3c0-1.9-0.2-3.7-0.8-5.5c-0.5-1.6-1.5-2.9-2.8-3.9c-1.7-1.1-3.8-1.6-5.8-1.5
                              c-2.2-0.1-4.3,0.4-6.3,1.5c-1.6,0.9-3,2.3-3.9,3.9c-0.9,1.7-1.4,3.6-1.3,5.5V75c0,1.5-0.5,2.9-1.6,4c-2.2,2.2-5.7,2.2-7.9,0
                              c-1-1.1-1.6-2.5-1.6-4V36.9c0-1.5,0.5-2.9,1.6-4c2.2-2.2,5.7-2.2,7.9,0c1,1.1,1.6,2.5,1.6,4v4l-1.4-0.3c0.7-1.2,1.5-2.3,2.4-3.4
                              c1.1-1.3,2.3-2.4,3.7-3.4c1.5-1.1,3.1-1.9,4.8-2.5C594.2,30.7,596.1,30.3,598.1,30.3z"/>
		                        <path class="st0" d="M649.9,81.5c-4.7,0.1-9.4-1-13.6-3.3c-3.7-2.1-6.8-5.2-8.8-8.9c-2.1-3.9-3.2-8.3-3.1-12.7
                              c-0.1-4.9,1-9.8,3.4-14.2c2.1-3.7,5.1-6.8,8.8-9c3.5-2,7.5-3.1,11.5-3.1c3.1,0,6.1,0.6,8.9,1.9c2.8,1.3,5.3,3.1,7.4,5.3
                              c2.2,2.3,3.9,4.9,5.1,7.8c1.3,3,1.9,6.2,1.9,9.4c0,1.4-0.7,2.7-1.8,3.6c-1.1,0.9-2.5,1.4-3.9,1.4h-35.2l-2.8-9.2h33.8l-2,1.8v-2.5
                              c-0.1-1.8-0.8-3.4-1.9-4.8c-1.1-1.4-2.6-2.6-4.2-3.4c-1.7-0.8-3.5-1.3-5.4-1.3c-1.8,0-3.5,0.2-5.2,0.7c-1.6,0.5-3,1.3-4.1,2.5
                              c-1.3,1.3-2.3,2.9-2.8,4.7c-0.7,2.4-1.1,4.9-1,7.5c-0.1,2.9,0.6,5.9,2.1,8.4c1.3,2.2,3.1,4,5.3,5.3c2.1,1.2,4.6,1.8,7,1.8
                              c1.8,0,3.7-0.1,5.4-0.6c1.1-0.3,2.3-0.7,3.3-1.3c0.8-0.5,1.6-1,2.3-1.3c1-0.5,2-0.8,3.1-0.8c2.6,0,4.7,2,4.7,4.6c0,0,0,0,0,0.1
                              c-0.1,1.8-1,3.5-2.6,4.5c-2,1.6-4.3,2.8-6.8,3.6C656,81,653,81.5,649.9,81.5z"/>
		                        <path class="st0" d="M693.1,75c0.1,3.1-2.4,5.6-5.4,5.6c-0.1,0-0.1,0-0.2,0c-1.5,0-2.9-0.6-3.9-1.6c-1.1-1.1-1.6-2.5-1.6-4V18
			                     c-0.1-3.1,2.4-5.6,5.4-5.6c0.1,0,0.1,0,0.2,0c1.5-0.1,2.9,0.5,3.9,1.6c1,1.1,1.6,2.5,1.5,4V75z"/>
	                        </g>
                        </g>
                     </svg>
                  <div class="f-16 f-md-18 w300 mt20 lh150 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-16 f-md-18 w300 lh150 white-clr text-xs-center">Copyright © QuickFunnel 2023</div>
                  <ul class="footer-ul f-16 f-md-18 w300 white-clr text-center text-md-right mt20 mt-md0">
                     <li><a href="https://support.oppyo.com/hc/en-us"  target="_blank" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://quickfunnel.live/legal/privacy-policy.html"  target="_blank" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://quickfunnel.live/legal/terms-of-service.html"  target="_blank" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://quickfunnel.live/legal/disclaimer.html"  target="_blank" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://quickfunnel.live/legal/gdpr.html"  target="_blank" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://quickfunnel.live/legal/dmca.html"  target="_blank" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://quickfunnel.live/legal/anti-spam.html"  target="_blank" class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!--Footer Section End -->
      <script type="text/javascript">
         // Special handling for in-app browsers that don't always support new windows
         (function() {
             function browserSupportsNewWindows(userAgent) {
                 var rules = [
                     'FBIOS',
                     'Twitter for iPhone',
                     'WebView',
                     '(iPhone|iPod|iPad)(?!.*Safari\/)',
                     'Android.*(wv|\.0\.0\.0)'
                 ];
                 var pattern = new RegExp('(' + rules.join('|') + ')', 'ig');
                 return !pattern.test(userAgent);
             }
         
             if (!browserSupportsNewWindows(navigator.userAgent || navigator.vendor || window.opera)) {
                 document.getElementById('af-form-1316343921').parentElement.removeAttribute('target');
             }
         })();
      </script><script type="text/javascript">
         (function() {
             var IE = /*@cc_on!@*/false;
             if (!IE) { return; }
             if (document.compatMode && document.compatMode == 'BackCompat') {
                 if (document.getElementById("af-form-1316343921")) {
                     document.getElementById("af-form-1316343921").className = 'af-form af-quirksMode';
                 }
                 if (document.getElementById("af-body-1316343921")) {
                     document.getElementById("af-body-1316343921").className = "af-body inline af-quirksMode";
                 }
                 if (document.getElementById("af-header-1316343921")) {
                     document.getElementById("af-header-1316343921").className = "af-header af-quirksMode";
                 }
                 if (document.getElementById("af-footer-1316343921")) {
                     document.getElementById("af-footer-1316343921").className = "af-footer af-quirksMode";
                 }
             }
         })();
      </script>
   </body>
</html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- Tell the browser to be responsive to screen width -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=9">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
	  
	<meta name="title" content="VidVee | Special Bonus">
	<meta name="description" content="VidVee">
	<meta name="keywords" content="VidVee">

	<meta property="og:image" content="https://www.vidvee.com/special-bonus/thumbnail.png">
	<meta name="language" content="English">
	<meta name="revisit-after" content="1 days">
	<meta name="author" content="Dr. Amit Pareek">

	<!-- Open Graph / Facebook -->
	<meta property="og:type" content="website">
	<meta property="og:title" content="VidVee | Special Bonus">
	<meta property="og:description" content="VidVee">
	<meta property="og:image" content="https://www.vidvee.com/special-bonus/thumbnail.png">

	<!-- Twitter -->
	<meta property="twitter:card" content="summary_large_image">
	<meta property="twitter:title" content="VidVee | Special Bonus">
	<meta property="twitter:description" content="VidVee">
	<meta property="twitter:image" content="https://www.vidvee.com/special-bonus/thumbnail.png">


<title>VidVee  Bonuses</title>
<!-- Shortcut Icon  -->
<link rel="shortcut icon" href="assets/images/favicon.png"/>
<!-- Css CDN Load Link -->
<link rel="stylesheet" href="assets/css/font-awesome.min.css" type="text/css" />
<link rel="stylesheet" href="../common_assets/css/bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="../common_assets/css/general.css">
<link rel="stylesheet" href="assets/css/timer.css" type="text/css" />
<link rel="stylesheet" type="text/css" href="assets/css/bonus-style.css">
<link rel="stylesheet" type="text/css" href="assets/css/style.css">
<link rel="stylesheet" href="assets/css/style-feature.css" type="text/css">
<link rel="stylesheet" href="assets/css/style-bottom.css" type="text/css">
<!-- Font Family CDN Load Links -->
<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=League+Spartan:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
<!-- Javascript File Load -->
<script src="../common_assets/js/jquery.min.js"></script>
<script src="../common_assets/js/popper.min.js"></script>
<script src="../common_assets/js/bootstrap.min.js"></script>   
<!-- Buy Button Lazy load Script -->


<script>
 $(document).ready(function() {
  /* Every time the window is scrolled ... */
  $(window).scroll(function() {
	 /* Check the location of each desired element */
	 $('.hideme').each(function(i) {
		 var bottom_of_object = $(this).offset().top + $(this).outerHeight();
		 var bottom_of_window = $(window).scrollTop() + $(window).height();
		 /* If the object is completely visible in the window, fade it it */
		 if ((bottom_of_window - bottom_of_object) > -200) {
			 $(this).animate({
				 'opacity': '1'
			 }, 300);
		 }
	 });
  });
 });
</script>
<!-- Smooth Scrolling Script -->
<script>
 $(document).ready(function() {
  // Add smooth scrolling to all links
  $("a").on('click', function(event) {
 
	 // Make sure this.hash has a value before overriding default behavior
	 if (this.hash !== "") {
		 // Prevent default anchor click behavior
		 event.preventDefault();
 
		 // Store hash
		 var hash = this.hash;
 
		 // Using jQuery's animate() method to add smooth page scroll
		 // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
		 $('html, body').animate({
			 scrollTop: $(hash).offset().top
		 }, 800, function() {
 
			 // Add hash (#) to URL when done scrolling (default click behavior)
			 window.location.hash = hash;
		 });
	 } // End if
  });
 });
</script>


</head>
<body>


  <!-- New Timer  Start-->
  <?php
	 $date = 'March 16 2022 11:59 PM EST';
	 $exp_date = strtotime($date);
	 $now = time();  
	 /*
	 
	 $date = date('F d Y g:i:s A eO');
	 $rand_time_add = rand(700, 1200);
	 $exp_date = strtotime($date) + $rand_time_add;
	 $now = time();*/
	 
	 if ($now < $exp_date) {
	 ?>
  <?php
	 } else {
		 echo "Times Up";
	 }
	 ?>
  <!-- New Timer End -->
  <?php
	 if(!isset($_GET['afflink'])){
	 $_GET['afflink'] = 'https://cutt.ly/BA91eJw';
	 $_GET['name'] = 'Ayush Jain';      
	 }
	 ?>


	<div class="main-header">
        <div class="container">
            <div class="row">
				<div class="col-12">
					<div class="f-md-32 f-20 lh140 w400 text-center white-clr">
						<span class="w600"><?php echo $_GET['name'];?>'s</span> special bonus page for &nbsp; <img src="assets/images/white-logo.png" class="mt15 mt-md0">
					</div>
				</div>

				<div class="col-12 mt20 mt-md40 text-center">
					<div class="f-md-26 f-20 w600 lh130 post-heading"><span>Grab My 20 Exclusive Bonuses Before the Deal Ends…</span></div>
				</div>

				<div class="col-md-12 col-12 mt20 mt-md25 p-md0 px15">
					<div class="">
						<div class="f-md-50 f-30 w500 text-center white-clr lh140 line-center worksans">Revealing A <span class="w700 purple">Push-Button Video Technology</span> That Creates Branded Video Channels Packed with Awesome Videos, Drives FREE Viral Traffic & Makes Handsfree Commissions, Ad Profits & Sales ...</div>
					</div>
				</div>
				<div class="col-md-12 col-12 text-center mt20 mt-md30">
					<div class="f-md-22 f-20 lh140 w600 white-clr">Watch My Quick Review Video</div>
				</div>
            </div>
            <div class="row mt20 mt-md30">
                    <div class="video-frame col-12 col-md-10 mx-auto">
                       <!--  <img src="assets/images/productbox.png" class="img-fluid">-->
                       
                            <div class="embed-responsive embed-responsive-16by9">
                              <iframe class="embed-responsive-item" src="https://vidvee.dotcompal.com/video/embed/p38kde2678" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                            </div>
                       
                    </div>
                    <!-- <img src="assets/images/video-shadow.png" class="img-fluid hidden-xs"> -->
            </div>
        </div>
    </div>

	<!-- CTA Button Section Start -->
	<div class="cta-btn-section">
	   <div class="container">
		  <div class="row">
			 <div class="col-md-12 col-md-12 col-12 text-center">
				<div class="f-md-22 f-18 text-center mt3 black lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
				<div class="f-md-22 f-22 text-center purple lh120 w700 mt15 mt-md20 lite-black-clr">TAKE ACTION NOW!</div>
				<div class="f-md-22 f-17 lh120 w600 mt15 mt-md20">Use Discount Coupon <span class="w800 lite-black-clr">"videarly"</span> for Instant <span class="w700 lite-black-clr">10% OFF</span> on VidVee Advance Plan</div>
			 </div>
			 <div class="col-md-12 col-12 text-center mt15 mt-md20">
				<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
				<span class="text-center">Grab VidVee + My 20 Exclusive Bonuses</span> <br>
				</a>
			 </div>
			 <div class="col-12 mt15 mt-md20 text-center">
				<img src="assets/images/payment.png" class="img-fluid">
			 </div>
			 <div class="col-12 mt15 mt-md20" align="center">
				<h3 class="f-md-28 f-20 w500 text-center black">Coupon Is Expiring In... </h3>
			 </div>
			 <!-- Timer -->
			 <div class="col-12 text-center">
				<div class="countdown counter-black">
				   <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Days</span> </div>
				   <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
				   <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
				   <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
				</div>
			 </div>
			 <!-- Timer End -->
		  </div>
	   </div>
	</div>
	<!-- CTA Button Section End -->

  
<!----no instalation---->

<div class="no-installation">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="f-md-50 f-28 w400 lh130 text-capitalize text-center black-clr">
						
						Drive MASSIVE Traffic & Sales to Any Offer, <br class="d-none d-md-block"> Page or Link  <span class="w700 orange-clr">In Just 3 SIMPLE Steps…</span>
                    </div>
                </div>
				<div class="col-12 mt20 mt-md60">
						   <div class="row">
							  <div class="col-12 col-md-4">
								 <div class="steps-block">
									<div class="step-bg">
									   <div class="f-22 f-md-22 w700 white-clr">
										1.	Add A Video
									   </div>
									</div>
									<div class="mt15 mt-md25">
									   <img src="assets/images/step-1.png" alt="" class="img-fluid">
									</div>
									<div class="step-headline f-20 f-md-22 w400 black-clr lh130 mt15 mt-md-30 text-center">
									 <span class="w600">Just put a keyword/topic</span> & VidVee will curate top quality videos to choose from. You can also add your own videos in just a few clicks.
									</div>
								 </div>
							  </div>
							  <div class="col-12 col-md-4 mt20 mt-md0">
								 <div class="steps-block">
									<div class="step-bg">
									   <div class="f-22 f-md-22 w700 white-clr">
										 2.	Insert Link
									   </div>
									</div>
									<div class="mt15 mt-md25">
									   <img src="assets/images/step-2.png" alt="" class="img-fluid">
									</div>
									<div class="step-headline f-20 f-md-22 w400 black-clr lh130 mt15 mt-md-30 text-center">
									 <span class="w600">Now insert your link in our software to start selling </span>the offer. Just choose from the already available ready-to-go templates to get paid on autopilot
									</div>
								 </div>
							  </div>
							  <div class="col-12 col-md-4 mt20 mt-md0">
								 <div class="steps-block">
									<div class="step-bg">
									   <div class="f-22 f-md-22 w700 white-clr">
										 3.	Publish & Profit 
									   </div>
									</div>
									<div class="mt15 mt-md25">
									   <img src="assets/images/step-3.png" alt="" class="img-fluid">
									</div>
									<div class="step-headline f-20 f-md-22 w400 black-clr lh130 mt15 mt-md-30 text-center">
									 Now the action begins, <span class="w600">our software put this video on fly mode that is a SEO, Social & Viral Traffic Machine</span> and you watch your accounts to see the commissions rolling in.
									</div>
								 </div>
							  </div>
						   </div>
						</div>
				
              
               <div class="col-8 col-md-10 mx-auto mt20 mt-md70">
                    <div class="row">
                        <div class="col-12 col-md-4 col-md-4">
                            <img src="assets/images/no1.png" class="img-fluid d-block mx-auto">
                            <div class="w600 f-24 f-md-26 mt20 mt-md30 black-clr text-center lh130">
                               No Download/Installation 
                            </div>
                        </div>
                        <div class="col-12 col-md-4 mt20 mt-md0">
                            <img src="assets/images/no2.png" class="img-fluid d-block mx-auto">
                            <div class="w600 f-24 f-md-26 mt20 mt-md30 black-clr text-center lh130">
                             No Prior Knowledge                                           
                            </div>
                        </div>
                        <div class="col-12 col-md-4 mt20 mt-md0">
                            <img src="assets/images/no3.png" class="img-fluid d-block mx-auto">
                            <div class="w600 f-24 f-md-26 mt20 mt-md30 black-clr text-center lh130">
                              100% Beginners Friendly
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 mt20 mt-md40">
                    <div class="f-20 f-md-24 w400 text-center black-clr lh130">
                       In just 7 minutes, you can create your first profitable video channel to enjoy MASSIVE FREE traffic & commissions coming in your bank account on autopilot.
                    </div>
                </div>
            </div>
        </div>
    </div>
<!----no instalation---->

     <div class="proof-section">
        <div class="container">
            <div class="row">
                <div class="col-12 f-md-50 f-30 w700 lh130 text-center black-clr">
					Checkout The Floods of FREE Traffic<br class="d-none d-md-block"> That We Are Getting Hands-Free... 
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
                    <img src="assets/images/proof.png" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 mt20 mt-md80">
                    <div class="f-md-50 f-30 w700 lh130 text-center black-clr">
						Which Is Consistently Making Us $500-1000  <br class="d-none d-md-block"> In Commissions Each & Every Day
                    </div>
                </div>
                <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
                    <img src="assets/images/proof-03.png" class="img-fluid d-block mx-auto">
                </div>
				 <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
                    <img src="assets/images/proof-04.png" class="img-fluid d-block mx-auto">
                </div>
            </div>
        </div>
    </div>
	
	<!-- CTA Button Section Start -->
	<div class="cta-btn-section">
	   <div class="container">
		  <div class="row">
			 <div class="col-md-12 col-md-12 col-12 text-center">
				<div class="f-md-22 f-18 text-center mt3 black lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
				<div class="f-md-22 f-22 text-center purple lh120 w700 mt15 mt-md20 lite-black-clr">TAKE ACTION NOW!</div>
				<div class="f-md-22 f-17 lh120 w600 mt15 mt-md20">Use Discount Coupon <span class="w800 lite-black-clr">"videarly"</span> for Instant <span class="w700 lite-black-clr">10% OFF</span> on VidVee Advance Plan</div>
			 </div>
			 <div class="col-md-12 col-12 text-center mt15 mt-md20">
				<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
				<span class="text-center">Grab VidVee + My 20 Exclusive Bonuses</span> <br>
				</a>
			 </div>
			 <div class="col-12 mt15 mt-md20 text-center">
				<img src="assets/images/payment.png" class="img-fluid">
			 </div>
			 <div class="col-12 mt15 mt-md20" align="center">
				<h3 class="f-md-28 f-20 w500 text-center black">Coupon Is Expiring In... </h3>
			 </div>
			 <!-- Timer -->
			 <div class="col-12 text-center">
				<div class="countdown counter-black">
				   <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Days</span> </div>
				   <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
				   <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
				   <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
				</div>
			 </div>
			 <!-- Timer End -->
		  </div>
	   </div>
	</div>
	<!-- CTA Button Section End -->

	  <div class="testimonial-section2">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="f-md-50 f-30 lh130 w700 text-center black-clr">Checkout What VidVee Early Users Have to SAY</div>
                </div>
                <div class="col-12 mt50 mt-md70">
                    <div class="testi-block">
                        <div class="row d-flex align-items-center flex-wrap">
                            <div class="col-12 col-md-3 order-md-2">
                                <img src="assets/images/testi-img5.png" class="img-fluid d-block mx-auto" style="width: 200px;">
                            </div>
                            <div class="col-12 col-md-9 order-md-1 mt20 mt-md0 text-center text-md-left">
                                <div class="f-22 f-md-32 w600 shape-testi">Got 257 Visitors in 4 Days on My Weight Loss Channel</div>
                                <div class="f-20 f-md-22 w400 lh130 mt20">
                                  Hi, I am from Australia and promote weight loss products on ClickBank & Amazon as an affiliate. I recently got access to VidVee - I created account & launched my Video Channel on DAY 1 with around 15 VIDEOS… <span class="w600">That was amazingly EASY. And I got 257 visitors in 4 days on my weight loss channel.</span> Kudos to the team for creating a solution that can make the complete process fast & easy.
                                </div>
                                <div class="f-24 f-md-32 w700 lh120 blue-clr mt20">Phil Warne</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 mt50 mt-md70">
                    <div class="testi-block">
                        <div class="row d-flex align-items-center flex-wrap">
                            <div class="col-12 col-md-3 order-md-1">
                                <img src="assets/images/testi-img6.png" class="img-fluid d-block mx-auto" style="width: 200px;">
                            </div>
                            <div class="col-12 col-md-9 order-md-2 mt20 mt-md0 text-center text-md-left">
                                <div class="f-22 f-md-32 w600 shape-testi">It Takes Away All Hassles</div>
                                <div class="f-20 f-md-22 w400 lh130 mt20">
                                    The best part of VidVee is that <span class="w600">It takes away all hassles like content creation, video recording or editing, and driving traffic</span> to your videos, etc. I am sure this will give you complete value for your money and help you get the desired success in the long run.
                                </div>
                                <div class="f-24 f-md-32 w700 lh120 blue-clr mt20">Sam Berks</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 mt50 mt-md70">
                    <div class="testi-block">
                        <div class="row d-flex align-items-center flex-wrap">
                            <div class="col-12 col-md-3 order-md-2">
                                <img src="assets/images/testi-img4.png" class="img-fluid d-block mx-auto" style="width: 200px;">
                            </div>
                            <div class="col-12 col-md-9 order-md-1 mt20 mt-md0 text-center text-md-left">
                                <div class="f-22 f-md-32 w600 shape-testi">I Can Make Money with Complete freedom</div>
                                <div class="f-20 f-md-22 w400 lh130 mt20">
I have been in the Video Marketing arena for quite some time now, and I must say that VidVee will take the industry by storm.
<br><br>
The coolest part is that it enables me to monetise my channel my way. I can <span class="w600">make money with Amazon/Affiliate offers, AdSense & my own offers – complete freedom is what it provides me.</span> Something to check on a serious note.
                                </div>
                                <div class="f-24 f-md-32 w700 lh120 blue-clr mt20">Steve Moody</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	
	<div class="thatsall-section">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-10 mx-auto text-center">
                    <div class="f-30 f-md-50 w700 shape-testi1 purple-hd">No Doubt!</div>
                </div>
				 <div class="col-12 f-md-50 f-30 w700 white-clr lh130 text-center mt20">
                    Videos Are PRESENT & FUTURE of Internet...
                </div>
				 <div class="col-12 text-center f-md-32 f-22 w400 white-clr lh130 mt20">
                    Did you know!
                </div>
            </div>
            <div class="row mt-md70 mt30 align-items-center">
                <div class="col-md-8 col-12 order-md-6">
                    <div class="f-md-32 f-22 w400 white-clr lh130">
						  <span class="orange-clr w600">YouTube has 2 billion unique monthly visitors,</span>
and it is no.1 Way to Drive Red Hot Traffic to any offer in any niche today.

                    </div>
                </div>
                <div class="col-md-4 col-12 order-md-0 mt-md0 mt20">
                    <img src="assets/images/here1.png" class="img-fluid mx-auto d-block" alt="some images" />
                </div>
            </div>
            <div class="row mt-md50 mt30 align-items-center">
                <div class="col-md-8 col-12">
                    <div class="f-md-32 f-22 w400 white-clr lh130">
                        <span class="orange-clr w600">Over 92% internet users watch videos </span>
on social media, on YouTube or find them using search engines!

                    </div>
                </div>
                <div class="col-md-4 col-12 mt-md0 mt20">
                    <img src="assets/images/here2.png" class="img-fluid mx-auto d-block" alt="some images" />
                </div>
            </div>
            <div class="row mt-md50 mt30 align-items-center">
                <div class="col-md-8 col-12 order-md-6 p-md0">
                    <div class="f-md-32 f-22 w400 white-clr lh130">
                        <span class="orange-clr w600">Almost 5 Billion videos are watched </span>
on YouTube alone every single day. There’s a huge business opportunity

                    </div>
                </div>
                <div class="col-md-4 col-12 order-md-0 mt-md0 mt20">
                    <img src="assets/images/here3.png" class="img-fluid mx-auto d-block" alt="some images" />
                </div>
            </div>
           
        </div>
    </div>
	
	
	<div class="affiliate-section">
        <div class="container">
            <div class="row">
                <div class="col-12 f-22 f-md-24 w400 lh130 black-clr">
                    <span class="w600">The SECRET Is:</span><br><br> When you can combine selling top products, YouTube, and FREE Video traffic (SEO, Social & viral) to make sales & commissions, you’re the fastest way possible.
                </div>
                <div class="col-12 f-20 f-md-22 w400 lh130 black-clr mt20 mt-md20">
                    <img src="assets/images/fb-viral.png" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 mt20 mt-md50">
                    <div class="f-md-36 f-24 w600 lh130 text-capitalize text-center black-clr">
					
						Checkout Free Traffic & Commissions <br class="d-none d-md-block"> 
We’re Getting by Following This Proven System…

                    </div>
                </div>
				 <div class="col-12 col-md-10 mt20 mt-md30 mx-auto">
                    <img src="assets/images/proof-02.png" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 col-md-10 mt20 mt-md30 mx-auto">
                    <img src="assets/images/proof-01.png" class="img-fluid d-block mx-auto">
                </div>
				
            </div>
        </div>
    </div>
	
  <div class="proudly-section" id="product">
        <div class="container">
            <div class="row">
                <div class="col-12 f-md-36 f-24 w700 text-center white-clr lh130 px-md0">
                   Proudly Presenting...
                </div>
                <div class="col-12 mt-md30 mt20">
                    <img src="assets/images/big-logo.png" class="img-fluid d-block mx-auto">
                </div>
                <div class="col-12 mt-md70 mt20">
                    <img src="assets/images/proudly.png" class="img-fluid d-block mx-auto">
                </div>
                <div class="f-md-32 f-22 w500 white-clr lh130 col-12 mt-md30 mt20 text-center">
                 A Push-Button Easy Software Exploits A YouTube Loophole to Legally Give You Access To 800 Mn Videos & Creates Beautiful Video Channels Packed with Top Video Content in Any Niche with Zero Tech Hassles
                </div>
            </div>
        </div>
    </div>
		<div class="appsuite-section">
			   <div class="container">
				  <div class="row">
					<div class="col-12">						
                       <div class="f-md-50 f-30 w700 lh130 black-clr text-center">
						The Complete Video Business Builder You Ever Need To Create Videos, Drive Traffic, &amp; Pocket BIG Profits!
						</div>	
                     </div>
					<div class="col-12 mt20 mt-md50">
                        <div class="row">
                           <div class="col-12 col-md-4">
                              <div class="appsuite-block">
								 <img src="assets/images/icon1.png" class="img-fluid">	
                                 <div class="f-24 f-md-30 w600 lh140 blue-clr1 mt20">
                                   Complete Video Business Builder
                                 </div>
								  <div class="f-20 f-md-22 w400 lh140 black-clr mt15">
                               Create Videos, Drive FREE Traffic & Convert them into sales & commissions. Quick start & scale Big – all-in-one place.
                                 </div>
                              </div>
                           </div> 
							<div class="col-12 col-md-4 mt20 mt-md0">
                              <div class="appsuite-block"> 
								<img src="assets/images/icon2.png" class="img-fluid">
                                 <div class="f-24 f-md-30 w600 lh140 blue-clr1 mt20">
                                  Works For Every Niche
                                 </div>
								  <div class="f-20 f-md-22 w400 lh140 black-clr mt15">
                               Robust & Proven Technology pulls 100s of videos with just 1 keyword search about any topic. 
                                 </div>
                              </div>
                           </div>
						   <div class="col-12 col-md-4 mt20 mt-md0">
                              <div class="appsuite-block">  
                              <img src="assets/images/icon3.png" class="img-fluid">
                                 <div class="f-24 f-md-30 w600 lh140 blue-clr1 mt20">
                                  Commercial License Included 
                                 </div>
								  <div class="f-20 f-md-22 w400 lh140 black-clr mt15">
                               Offer branded, self-growing, and traffic generating video channels to clients & Pocket 100% Profit of Every Sale!
                                 </div>
                              </div>
                           </div>							
                        </div>
                     </div>	
					
					
				  </div>
			   </div>
			</div>
  	<!-- Bonus Section Header Start -->
	<div class="bonus-header">
		<div class="container">
			<div class="row">
				<div class="col-12 col-md-10 mx-auto heading-bg text-center">
					<div class="f-24 f-md-36 lh140 w700">When You Purchase VidVee, You Also Get <br class="d-lg-block d-none"> Instant Access To These 20 Exclusive Bonuses</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Bonus Section Header End -->

	<!-- Bonus #1 Section Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
			 <div class="col-12 text-center">
				<div class="bonus-title-bg">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 1</div>
				</div>
			 </div>
			 <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
				<div class="row">
				   <div class="col-md-5 col-12">
					  <img src="assets/images/bonus1.png" class="img-fluid">
				   </div>
				   <div class="col-md-7 col-12 mt20 mt-md0">
					  <div class="f-22 f-md-34 w600 lh140 bonus-title-color">
						 Vidinci High Margin Niche Solar Panels
					  </div>
					  <ul class="bonus-list f-20 f-md-22 lh140 w400 mt20 mt-md30 p0">
						<li><span class="w600">Vidinci</span> is a professional video editing software used by top hollywood producers to deliver blockbuster films.</li>
						<li>get an <span class="w600">asset set</span> of 3 hd mov files that include a title, 4 wipes and 4 lower thirds with different background covering a variety of niches.</li>
						<li>This Bonus with VidVee will ease your way to get best results in the Video Marketing Arena.</li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #1 Section End -->

	<!-- Bonus #2 Section Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
			 <div class="col-12 xstext1">
				<div class="bonus-title-bg">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 2</div>
				</div>
			 </div>
			 <div class="col-12 mt20 mt-md30">
				<div class="row">
				   <div class="col-md-5 col-12 order-md-7">
					  <img src="assets/images/bonus2.png" class="img-fluid">
				   </div>
				   <div class="col-md-7 col-12 order-md-5 mt20 mt-md0">
					  <div class="f-22 f-md-34 w600 lh140 bonus-title-color">
						Create Video with Camtasia 9
					  </div>
					  <ul class="bonus-list f-20 f-md-22 lh140 w400 mt20 mt-md30 p0">
						<li>Get A <span class="w600">42-Module Course</span> covering every aspect of <span class="w600">Camtasia 9!</span></li>
						<li>Learn Step-By-Step Screen Recording & Editing. Also, Learn to Create Video Presentation with Google Sheets or PowerPoint</li>
						<li>This Bonus with VidVee will help you Create Professional Videos For Your Business in Any Niche.</li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #2 Section End -->

	<!-- Bonus #3 Section Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
			 <div class="col-12 text-center">
				<div class="bonus-title-bg">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 3</div>
				</div>
			 </div>
			 <div class="col-12 mt20 mt-md30">
				<div class="row">
				   <div class="col-md-5 col-12">
					  <img src="assets/images/bonus3.png" class="img-fluid">
				   </div>
				   <div class="col-md-7 col-12 mt20 mt-md0">
					  <div class="f-22 f-md-34 w600 lh140 bonus-title-color">
						 Text To Speech Converter
					  </div>
					  <ul class="bonus-list f-20 f-md-22 lh140 w400 mt20 mt-md30 p0">
						 <li><span class="w600">The Text To Speech Converter </span>will turn any text into an audio file at the click of a button!</li>
						 <li>Use this bonus to create high quality podcasts directly from the text files and publish & profit on VidVee.</li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #3 Section End -->

	<!-- Bonus #4 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
			 <div class="col-12 xstext1">
				<div class="bonus-title-bg">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 4</div>
				</div>
			 </div>
			 <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
				<div class="row">
				   <div class="col-md-5 col-12 order-md-7">
					  <img src="assets/images/bonus4.png" class="img-fluid">
				   </div>
				   <div class="col-md-7 col-12 mt20 mt-md0 order-md-5">
					  <div class="f-22 f-md-34 w600 lh140 bonus-title-color">
						Power Branding For Affiliate Marketers
					  </div>
					  <ul class="bonus-list f-20 f-md-22 lh140 w400 mt20 mt-md30 p0">
						<li>Get a course teaching you how expert affiliate marketers brand private label videos for affiliate marketing purposes.</li>
						<li>This Bonus with VidVee will help you increase your affiliate sales by building& monetizing highly engaging branded videos.</li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #4 End -->

	<!-- Bonus #5 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
			 <div class="col-12 text-center">
				<div class="bonus-title-bg">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 5</div>
				</div>
			 </div>
			 <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
				<div class="row">
				   <div class="col-md-5 col-12">
					  <img src="assets/images/bonus5.png" class="img-fluid">
				   </div>
				   <div class="col-md-7 col-12 mt20 mt-md0">
					  <div class="f-22 f-md-34 w600 lh140 bonus-title-color">
						Video Economy
					  </div>
					  <ul class="bonus-list f-20 f-md-22 lh140 w400 mt20 mt-md30 p0">
							<li>Discover the secrets to use YouTube, Snapchat and other video platforms to reach a wider audience!</li>
							<li>This bonus is a plus to VidVee’s unique features to build a strong video marketing business in any niche.</li>	
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #5 End -->

	<!-- CTA Button Section Start -->
	<div class="cta-btn-section">
	   <div class="container">
		  <div class="row">
			 <div class="col-md-12 col-md-12 col-12 text-center">
				<div class="f-md-22 f-18 text-center mt3 black lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
				<div class="f-md-22 f-22 text-center purple lh120 w700 mt15 mt-md20 lite-black-clr">TAKE ACTION NOW!</div>
				<div class="f-md-22 f-17 lh120 w600 mt15 mt-md20">Use Discount Coupon <span class="w800 lite-black-clr">"videarly"</span> for Instant <span class="w700 lite-black-clr">10% OFF</span> on VidVee Advance Plan</div>
			 </div>
			 <div class="col-md-12 col-12 text-center mt15 mt-md20">
				<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
				<span class="text-center">Grab VidVee + My 20 Exclusive Bonuses</span> <br>
				</a>
			 </div>
			 <div class="col-12 mt15 mt-md20 text-center">
				<img src="assets/images/payment.png" class="img-fluid">
			 </div>
			 <div class="col-12 mt15 mt-md20" align="center">
				<h3 class="f-md-28 f-20 w500 text-center black">Coupon Is Expiring In... </h3>
			 </div>
			 <!-- Timer -->
			 <div class="col-12 text-center">
				<div class="countdown counter-black">
				   <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Days</span> </div>
				   <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
				   <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
				   <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
				</div>
			 </div>
			 <!-- Timer End -->
		  </div>
	   </div>
	</div>
	<!-- CTA Button Section End -->


	<!-- Bonus #6 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
			 <div class="col-12 xstext1">
				<div class="bonus-title-bg">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 6</div>
				</div>
			 </div>
			 <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
				<div class="row">
				   <div class="col-md-5 order-md-7 col-12">
					  <img src="assets/images/bonus6.png" class="img-fluid">
				   </div>
				   <div class="col-md-7 order-md-5 col-12 mt20 mt-md0">
					  <div class="f-22 f-md-34 w600 lh140 bonus-title-color">
						Backlinks Analyzer Software
					  </div>
					  <ul class="bonus-list f-20 f-md-22 lh140 w400 mt20 mt-md30 p0">
						<li>Get a powerful software that you can use to instantly analyze the quality of all your backlinks... with the click of a mouse!</li>
						<li>This bonus along with VidVee’s analytics will direct your business in the right direction & save a lot.</li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #6 End -->

	<!-- Bonus #7 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
			 <div class="col-12 text-center">
				<div class="bonus-title-bg">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 7</div>
				</div>
			 </div>
			 <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
				<div class="row">
				   <div class="col-md-5 col-12">
					  <img src="assets/images/bonus7.png" class="img-fluid">
				   </div>
				   <div class="col-md-7 col-12 mt20 mt-md0">
					  <div class="f-22 f-md-34 w600 lh140 bonus-title-color">
						Professional eMail Follow Up
					  </div>
					  <ul class="bonus-list f-20 f-md-22 lh140 w400 mt20 mt-md30 p0">
						 <li>Get <span class="w600">77 professionally created email-follow up</span> templates</li>			
						 <li>This bonus when used with VidVee’s personalized video email broadcasting, will turn your prospects and subscribers into customers.</li>			
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #7 End -->

	<!-- Bonus #8 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
			 <div class="col-12 xstext1">
				<div class="bonus-title-bg">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 8</div>
				</div>
			 </div>
			 <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
				<div class="row">
				   <div class="col-md-5 order-md-7 col-12">
					  <img src="assets/images/bonus8.png" class="img-fluid">
				   </div>
				   <div class="col-md-7 order-md-5 col-12 mt20 mt-md0">
					  <div class="f-22 f-md-34 w600 lh140 bonus-title-color">
						 SteemIt Made Easy
					  </div>
					  <ul class="bonus-list f-20 f-md-22 lh140 w400 mt20 mt-md30 p0">
						 <li>Learn more about SteemIt and cash-in with this Steem it software!</li>
						 <li>Seemit rewards its users in the form of digital currency for the contributions they make, whether content, curating content, or funding.</li>
						 <li>This revolutionary bonus when combined with VidVee can make you huge profits in the form of digital currency by sharing various video content on Seemit.</li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #8 End -->

	<!-- Bonus #9 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
			 <div class="col-12 text-center">
				<div class="bonus-title-bg">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 9</div>
				</div>
			 </div>
			 <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
				<div class="row">
				   <div class="col-md-5 col-12">
					  <img src="assets/images/bonus9.png" class="img-fluid">
				   </div>
				   <div class="col-md-7 col-12 mt20 mt-md0">
					  <div class="f-22 f-md-34 w600 lh140 bonus-title-color">
						 Power Effects
					  </div>
					  <ul class="bonus-list f-20 f-md-22 lh140 w400 mt20 mt-md30 p0">
						 <li><span class="w600">Power Effects</span> is a graphic-headline designing software to create your own animated headlines, subheads & graphic texts.</li>
						 <li>This bonus will help you design creative headlines with just a click to engage customers on the videos uploaded on VidVee </li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #9 End -->

	<!-- Bonus #10 start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
			 <div class="col-12 xstext1">
				<div class="bonus-title-bg">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 10</div>
				</div>
			 </div>
			 <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
				<div class="row">
				   <div class="col-md-5 order-md-7 col-12">
					  <img src="assets/images/bonus10.png" class="img-fluid">
				   </div>
				   <div class="col-md-7 order-md-5 col-12 mt20 mt-md0">
					  <div class="f-22 f-md-34 w600 lh140 bonus-title-color">
							WP Video Focus
					  </div>
					  <ul class="bonus-list f-20 f-md-22 lh140 w400 mt20 mt-md30 p0">
							<li><span class="w600">WP Video Focus</span> is a plugin that allows your videos to visibly continue playing as a widget in the corner when a user scrolls.</li>
							<li>This bonus is a plus feature to VidVee as it makes your video channels more engaging and user friendly.</li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #10 End -->


	<!-- CTA Button Section Start -->
	<div class="cta-btn-section">
	   <div class="container">
		  <div class="row">
			 <div class="col-md-12 col-md-12 col-12 text-center">
				<div class="f-md-22 f-18 text-center mt3 black lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
				<div class="f-md-22 f-22 text-center purple lh120 w700 mt15 mt-md20 lite-black-clr">TAKE ACTION NOW!</div>
				<div class="f-md-22 f-17 lh120 w600 mt15 mt-md20">Use Discount Coupon <span class="w800 lite-black-clr">"videarly"</span> for Instant <span class="w700 lite-black-clr">10% OFF</span> on VidVee Advance Plan</div>
			 </div>
			 <div class="col-md-12 col-12 text-center mt15 mt-md20">
				<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
				<span class="text-center">Grab VidVee + My 20 Exclusive Bonuses</span> <br>
				</a>
			 </div>
			 <div class="col-12 mt15 mt-md20 text-center">
				<img src="assets/images/payment.png" class="img-fluid">
			 </div>
			 <div class="col-12 mt15 mt-md20" align="center">
				<h3 class="f-md-28 f-20 w500 text-center black">Coupon Is Expiring In... </h3>
			 </div>
			 <!-- Timer -->
			 <div class="col-12 text-center">
				<div class="countdown counter-black">
				   <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Days</span> </div>
				   <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
				   <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
				   <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
				</div>
			 </div>
			 <!-- Timer End -->
		  </div>
	   </div>
	</div>
	<!-- CTA Button Section End -->


	<!-- Bonus #11 start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
			 <div class="col-12 text-center">
				<div class="bonus-title-bg">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 11</div>
				</div>
			 </div>
			 <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
				<div class="row">
				   <div class="col-md-5 col-12">
					  <img src="assets/images/bonus11.png" class="img-fluid">
				   </div>
				   <div class="col-md-7 col-12 mt20 mt-md0">
					  <div class="f-22 f-md-34 w600 lh140 bonus-title-color">
						Auto Support Bot
					  </div>
					  <ul class="bonus-list f-20 f-md-22 lh140 w400 mt20 mt-md30 p0">
							<li><span class="w600">Auto Support Bot</span> is a <span class="w600">24/7 live chatbot</span> on your websites and video pages.</li>
							<li>Use this bonus in VidVee to automate your customer support service by showing 24x7 live chatbot popup.</li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #11 End -->


	<!-- Bonus #12 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
			 <div class="col-12 xstext1">
				<div class="bonus-title-bg">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 12</div>
				</div>
			 </div>
			 <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
				<div class="row">
				   <div class="col-md-5 order-md-7 col-12">
					  <img src="assets/images/bonus12.png" class="img-fluid">
				   </div>
				   <div class="col-md-7 order-md-5 col-12 mt20 mt-md0">
					  <div class="f-22 f-md-34 w600 lh140 bonus-title-color">
						 Customer Loyalty
					  </div>
					  <ul class="bonus-list f-20 f-md-22 lh140 w400 mt20 mt-md30 p0">
							<li>Get an E-Book with stepwise tutorial teaching you the secrets of customer loyalty and retargeting without any extra advertising cost.</li>
							<li>Save huge amount of advertising cost by using this bonus to retarget and monetize your existing loyal content-consumers on VidVee.</li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #12 End -->

	<!-- Bonus #13 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
			 <div class="col-12 text-center">
				<div class="bonus-title-bg">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 13</div>
				</div>
			 </div>
			 <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
				<div class="row">
				   <div class="col-md-5 col-12">
					  <img src="assets/images/bonus13.png" class="img-fluid">
				   </div>
				   <div class="col-md-7 col-12 mt20 mt-md0">
					  <div class="f-22 f-md-34 w600 lh140 bonus-title-color">
						Marketing Minisite Template V53
					  </div>
					  <ul class="bonus-list f-20 f-md-22 lh140 w400 mt20 mt-md30 p0">
							<li>Get Highly Attractive Marketing Minisite Templates in various Niches.</li>
							<li>Make Your Minisite Standout from the Crowd Using this Bonus with Countless Features of VidVee</li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #13 End -->

	<!-- Bonus #14 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
			 <div class="col-12 xstext1">
				<div class="bonus-title-bg">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 14</div>
				</div>
			 </div>
			 <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
				<div class="row">
				   <div class="col-md-5 order-md-7 col-12">
					  <img src="assets/images/bonus14.png" class="img-fluid">
				   </div>
				   <div class="col-md-7 order-md-5 col-12 mt20 mt-md0">
					  <div class="f-22 f-md-34 w600 lh140 bonus-title-color">
						Social Messaging Apps For Marketers Video Upgrade
					  </div>
					  <ul class="bonus-list f-20 f-md-22 lh140 w400 mt20 mt-md30 p0">
							<li>With this video course you will learn how to use social media and social messaging apps together as a powerful marketing strategy.</li>
							<li>Use this bonus to build efficient video marketing strategy for your VidVee business channels.</li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #14 End -->


	<!-- Bonus #15 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
			 <div class="col-12 text-center">
				<div class="bonus-title-bg">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 15</div>
				</div>
			 </div>
			 <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
				<div class="row">
				   <div class="col-md-5 col-12">
					  <img src="assets/images/bonus15.png" class="img-fluid">
				   </div>
				   <div class="col-md-7 col-12 mt20 mt-md0">
					  <div class="f-22 f-md-34 w600 lh140 bonus-title-color">
						 Turbo GIF Animator
					  </div>
					  <ul class="bonus-list f-20 f-md-22 lh140 w400 mt20 mt-md30 p0">
							<li>Get turbo GIF animator that makes animated images in less than a minute!</li>
							<li>This bonus when combined with VidVee will help you create highly engaging GIFs on your VidVee Channels to grab consumers’ attention.</li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #15 End -->


	<!-- CTA Button Section Start -->
	<div class="cta-btn-section">
	   <div class="container">
		  <div class="row">
			 <div class="col-md-12 col-md-12 col-12 text-center">
				<div class="f-md-22 f-18 text-center mt3 black lh120 w400">Don't wait for the time when I will pull these bonuses away…</div>
				<div class="f-md-22 f-22 text-center purple lh120 w700 mt15 mt-md20 lite-black-clr">TAKE ACTION NOW!</div>
				<div class="f-md-22 f-17 lh120 w600 mt15 mt-md20">Use Discount Coupon <span class="w800 lite-black-clr">"videarly"</span> for Instant <span class="w700 lite-black-clr">10% OFF</span> on VidVee Advance Plan</div>
			 </div>
			 <div class="col-md-12 col-12 text-center mt15 mt-md20">
				<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
				<span class="text-center">Grab VidVee + My 20 Exclusive Bonuses</span> <br>
				</a>
			 </div>
			 <div class="col-12 mt15 mt-md20 text-center">
				<img src="assets/images/payment.png" class="img-fluid">
			 </div>
			 <div class="col-12 mt15 mt-md20" align="center">
				<h3 class="f-md-28 f-20 w500 text-center black">Coupon Is Expiring In... </h3>
			 </div>
			 <!-- Timer -->
			 <div class="col-12 text-center">
				<div class="countdown counter-black">
				   <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Days</span> </div>
				   <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
				   <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
				   <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
				</div>
			 </div>
			 <!-- Timer End -->
		  </div>
	   </div>
	</div>
	<!-- CTA Button Section End -->
	
	<!-- Bonus #16 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
			 <div class="col-12 xstext1">
				<div class="bonus-title-bg">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 16</div>
				</div>
			 </div>
			 <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
				<div class="row">
				   <div class="col-md-5 order-md-7 col-12">
					  <img src="assets/images/bonus16.png" class="img-fluid">
				   </div>
				   <div class="col-md-7 order-md-5 col-12 mt20 mt-md0">
					  <div class="f-22 f-md-34 w600 lh140 bonus-title-color">
						Project Manager
					  </div>
					  <ul class="bonus-list f-20 f-md-22 lh140 w400 mt20 mt-md30 p0">
						 <li>Project Manager is a software that allows you to create a list of tasks or use a predefined list to track the progress of the project easily and effectively.</li>
						 <li>This bonus will help you manage various business channels and projects created on VidVee in just few clicks.</li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #16 End -->

	<!-- Bonus #17 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
			 <div class="col-12 text-center">
				<div class="bonus-title-bg">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 17</div>
				</div>
			 </div>
			 <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
				<div class="row">
				   <div class="col-md-5 col-12">
					  <img src="assets/images/bonus17.png" class="img-fluid">
				   </div>
				   <div class="col-md-7 col-12 mt20 mt-md0">
					  <div class="f-22 f-md-34 w600 lh140 bonus-title-color">
						My Ad Rotator Software
					  </div>
					  <ul class="bonus-list f-20 f-md-22 lh140 w400 mt20 mt-md30 p0">
						 <li>My Ad Rotator Software is a software that rotates your various converting ads on the same ad-space on your web page.</li>		
						 <li>Using this Bonus you can show your own created ads for the different products on the same location on your video page created on VidVee.</li>			
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #17 End -->

	<!-- Bonus #18 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
			 <div class="col-12 xstext1">
				<div class="bonus-title-bg">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 18</div>
				</div>
			 </div>
			 <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
				<div class="row">
				   <div class="col-md-5 order-md-7 col-12">
					  <img src="assets/images/bonus18.png" class="img-fluid">
				   </div>
				   <div class="col-md-7 order-md-5 col-12 mt20 mt-md0">
					  <div class="f-22 f-md-34 w600 lh140 bonus-title-color">
						 GEO Visitor
					  </div>
					  <ul class="bonus-list f-20 f-md-22 lh140 w400 mt20 mt-md30 p0">
						 <li>GEO Visitor is a software that targets your content based on your visitors geographical location!</li>
						 <li>Use this bonus to show videos & build specific business on VidVee based upon your visitors’ geo-location.</li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #18 End -->

	<!-- Bonus #19 Start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
			 <div class="col-12 text-center">
				<div class="bonus-title-bg">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 19</div>
				</div>
			 </div>
			 <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
				<div class="row">
				   <div class="col-md-5 col-12">
					  <img src="assets/images/bonus19.png" class="img-fluid">
				   </div>
				   <div class="col-md-7 col-12 mt20 mt-md0">
					  <div class="f-22 f-md-34 w600 lh140 bonus-title-color">
						 True Tags Software
					  </div>
					  <ul class="bonus-list f-20 f-md-22 lh140 w400 mt20 mt-md30 p0">
							<li>True Tag Software is a tool that will rank your videos over your competitors and make your videos SEO Optimized</li>
							<li>Use this bonus in VidVee to boost your videos on the search engine and rank higher on Google.</li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #19 End -->

	<!-- Bonus #20 start -->
	<div class="section-bonus">
	   <div class="container">
		  <div class="row">
			 <div class="col-12 xstext1">
				<div class="bonus-title-bg">
				   <div class="f-22 f-md-28 lh120 w700">Bonus 20</div>
				</div>
			 </div>
			 <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
				<div class="row">
				   <div class="col-md-5 order-md-7 col-12">
					  <img src="assets/images/bonus20.png" class="img-fluid">
				   </div>
				   <div class="col-md-7 order-md-5 col-12 mt20 mt-md0">
					  <div class="f-22 f-md-34 w600 lh140 bonus-title-color">
						Customer List Builder
					  </div>
					  <ul class="bonus-list f-20 f-md-22 lh140 w400 mt20 mt-md30 p0">
							<li>Customer list builder is a software that links directly with PayPal, automatically adding your customer details as soon as they pay!</li>
							<li>Use this bonus in VidVee to build your email list and grow your sales with easy integration with PayPal.</li>
					  </ul>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<!-- Bonus #20 End -->

	<!-- Huge Woth Section Start -->
	<div class="huge-area mt30 mt-md10">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-12 text-center">
                    <div class="f-md-65 f-40 lh120 w700 white-clr">That’s Huge Worth of</div>
                    <br>
                    <div class="f-md-60 f-40 lh120 w800 purple">$2285!</div>
                </div>
            </div>
        </div>
    </div>
	<!-- Huge Worth Section End -->

	<!-- text Area Start -->
	<div class="white-section">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-12 text-center p0">
                	<div class="f-md-36 f-25 lh140 w400">So what are you waiting for? You have a great opportunity <br class="d-lg-block d-none"> ahead + <span class="w600">My 20 Bonus Products</span> are making it a <br class="d-lg-block d-none"> <b>completely NO Brainer!!</b></div>
            	</div>
			</div>
		</div>
	</div>
	<!-- text Area End -->

	<!-- CTA Button Section Start -->
	<div class="cta-btn-section">
	   <div class="container">
		  <div class="row">
			 <div class="col-md-12 col-12 text-center">
				<a href="<?php echo $_GET['afflink'];?>" class="text-center bonusbuy-btn">
				<span class="text-center">Grab VidVee + My 20 Exclusive Bonuses</span> <br>
				</a>
			 </div>
			 
			 <div class="col-12 mt15 mt-md20" align="center">
				<h3 class="f-md-30 f-20 w500 text-center black">My Exclusive Bonuses Are Expiring in...</h3>
			 </div>
			 <!-- Timer -->
			 <div class="col-12 text-center">
				<div class="countdown counter-black">
				   <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">01&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
				   <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">16&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
				   <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">59&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
				   <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">37</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
				</div>
			 </div>
			 <!-- Timer End -->
		  </div>
	   </div>
	</div>
	<!-- CTA Button Section End -->

	
	
	<!-- Footer Section Start -->
	<div class="strip_footer clear mt20 mt-md40">
        <div class="container">
            <div class="row">
				<div class="col-12 text-center">
					<img src="assets/images/white-logo.png" alt="logo" class="img-fluid">
				</div>
                <div class="col-md-12 col-md-12 col-12 f-18 f-md-18 w400 mt15 mt-md35 lh140 white-clr text-center">
                   Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc.                    
                </div>
			</div>
			<div class="row">
				<div class="col-md-3 col-12 f-18 f-md-20 w400 w400 mt10 mt-md58 lh140 white-clr text-xs-center d-left-m-center">Copyright © VidVee @2022</div>
				<div class="col-md-9 col-12 f-md-20 w400 f-18 white-clr mt10 mt-md60 d-right-m-center">
					<a href="https://support.bizomart.com/hc/en-us">Contact</a>&nbsp;&nbsp;|&nbsp;
					<a href="http://vidvee.com/legal/privacy-policy.html">Privacy</a>&nbsp;|&nbsp;
					<a href="http://vidvee.com/legal/terms-of-service.html">T&amp;C</a>&nbsp;|&nbsp;
					<a href="http://vidvee.com/legal/disclaimer.html">Disclaimer</a>&nbsp;|&nbsp;
					<a href="http://vidvee.com/legal/gdpr.html">GDPR</a>&nbsp;|&nbsp;
					<a href="http://vidvee.com/legal/dmca.html">DMCA</a>&nbsp;|&nbsp;
					<a href="http://vidvee.com/legal/anti-spam.html">Anti-Spam</a>
				</div>
            </div>
        </div>
    </div>
	<!-- Footer Section End -->


	<!-- Exit Pop-Box Start -->
	<!-- Load UC Bounce Modal CDN Start -->
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/ouibounce/0.0.11/ouibounce.min.js"></script>

	<!-- <div id="ouibounce-modal" style="z-index:77777; display:none;">
	    <div class="underlay"></div>
	    	<div class="modal popupbg" style="display:block; ">
	        <div class="modal-header" style="border:0; padding:0;">
	            <button type="button" class="close" onclick="document.getElementById('ouibounce-modal').style.display = 'none';">&times;</button>
	       	</div>

	        <div class="modal-body">
	            <div class="innerbody">
	                <div class="col-lg-12 col-md-12 col-md-12 col-md-offset-0 col-12 col-offset-0 mt2 xsmt2">
	                    <img src="assets/images/waitimg.png" class="img-fluid mt2 xsmt2">
	                    <h2 class="text-center mt2 w500 sm25 xs20 lh140">I HAVE SOMETHING SPECIAL<br /> FOR YOU</h2>
	                    <a href="<?php echo $_GET['afflink']; ?>" class="createfree md20 sm18 xs16" style="color:#ffffff;">Stay</a>
	                </div>
	            </div>
	        </div>
	    </div>
	</div> -->

	<div class="modal fade in" id="ouibounce-modal" role="dialog" style="display:none; overflow-y:auto;">
		<div class="modal-dialog modal-dg">
			<button type="button" class="close" onclick="document.getElementById('ouibounce-modal').style.display = 'none';">&times;</button>	 
			<div class="col-12 modal-content pop-bg pop-padding">			
				<div class="col-12 modal-body text-center border-pop px0">	
					<div class="col-12 px0">
						<img src="assets/images/waitimg.png" class="img-fluid">
					</div>
					
					<div class="col-12 text-center mt20 mt-md25 px0">
						<div class="f-20 f-md-28 lh140 w600">I HAVE SOMETHING SPECIAL <br class="d-lg-block d-none"> FOR YOU</div>
					</div>

					<div class="col-12 mt20 mt-md20">
						<div class="link-btn">
							<a href="<?php echo $_GET['afflink'];?>">STAY</a>
						</div>
					</div>
				</div>		
			</div>
		</div>
	</div>

	<!-- <script type="text/javascript">
        var _ouibounce = ouibounce(document.getElementById('ouibounce-modal'),{
        aggressive: true, //Making this true makes ouibounce not to obey "once per visitor" rule
        });
    </script> -->
	<!-- Load UC Bounce Modal CDN End -->
	<!-- Exit Pop-Box End -->


	<!-- Google Tag Manager (noscript) -->

<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K95FCV8"

height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

<!-- End Google Tag Manager (noscript) -->
  <!-- Facebook Pixel Code -->
  <script>
	 ! function(f, b, e, v, n, t, s) {
	  if (f.fbq) return;
	  n = f.fbq = function() {
		 n.callMethod ?
			 n.callMethod.apply(n, arguments) : n.queue.push(arguments)
	  };
	  if (!f._fbq) f._fbq = n;
	  n.push = n;
	  n.loaded = !0;
	  n.version = '2.0';
	  n.queue = [];
	  t = b.createElement(e);
	  t.async = !0;
	  t.src = v;
	  s = b.getElementsByTagName(e)[0];
	  s.parentNode.insertBefore(t, s)
	 }(window,
	  document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');
	 fbq('init', '1777584378755780');
	 fbq('track', 'PageView');
  </script>
  <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=1777584378755780&ev=PageView&noscript=1" /></noscript>
  <!-- DO NOT MODIFY -->
  <!-- End Facebook Pixel Code -->
  <!-- Google Code for Remarketing Tag -->
  <!-------------------------------------------------- Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup --------------------------------------------------->
  <div style="display:none;">
	 <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 748114601;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		var google_user_id = '<unique user id>';
		/* ]]> */
	 </script>
	 <script type="text/javascript" src="https://www.googleadservices.com/pagead/conversion.js"></script>
	 <noscript>
		<div style="display:inline;">
		   <img height="1" width="1" style="border-style:none;" alt="" src="https://googleads.g.doubleclick.net/pagead/viewthroughconversion/748114601/?guid=ON&amp;script=0&amp;userId=<unique user id>" />
		</div>
	 </noscript>
  </div>
  <!-- Google Code for Remarketing Tag -->
  <!-- timer --->
  <?php
	 if ($now < $exp_date) {
	 
	 ?>
  <script type="text/javascript">
	 // Count down milliseconds = server_end - server_now = client_end - client_now
	 var server_end = <?php echo $exp_date; ?> * 1000;
	 var server_now = <?php echo time(); ?> * 1000;
	 var client_now = new Date().getTime();
	 var end = server_end - server_now + client_now; // this is the real end time
	 
	 var noob = $('.countdown').length;
	 
	 var _second = 1000;
	 var _minute = _second * 60;
	 var _hour = _minute * 60;
	 var _day = _hour * 24
	 var timer;
	 
	 function showRemaining() {
	  var now = new Date();
	  var distance = end - now;
	  if (distance < 0) {
		 clearInterval(timer);
		 document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
		 return;
	  }
	 
	  var days = Math.floor(distance / _day);
	  var hours = Math.floor((distance % _day) / _hour);
	  var minutes = Math.floor((distance % _hour) / _minute);
	  var seconds = Math.floor((distance % _minute) / _second);
	  if (days < 10) {
		 days = "0" + days;
	  }
	  if (hours < 10) {
		 hours = "0" + hours;
	  }
	  if (minutes < 10) {
		 minutes = "0" + minutes;
	  }
	  if (seconds < 10) {
		 seconds = "0" + seconds;
	  }
	  var i;
	  var countdown = document.getElementsByClassName('countdown');
	  for (i = 0; i < noob; i++) {
		 countdown[i].innerHTML = '';
	 
		 if (days) {
			 countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + days + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 smmltd">Days</span> </div>';
		 }
	 
		 countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + hours + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>';
	 
		 countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">' + minutes + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>';
	 
		 countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">' + seconds + '</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>';
	  }
	 
	 }
	 timer = setInterval(showRemaining, 1000);
  </script>
  <?php
	 } else {
	 echo "Times Up";
	 }
	 ?>
  <!--- timer end-->
</body>
</html>

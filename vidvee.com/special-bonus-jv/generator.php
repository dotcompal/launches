<!DOCTYPE html>
<html>

<head>
    <title>Bonus Landing Page Generator</title>
    <link rel="icon" href="assets/images/favicon.png" type="image/png">
    <!-- Device & IE Compatibility Meta -->
    <meta http-equiv="X-UA-Compatible" content="IE=9" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
    <!--Load Google Fonts -->
   <link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=League+Spartan:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
    <!--Load External CSS -->
    <link rel="stylesheet" href="../common_assets/css/bootstrap.min.css" type="text/css">
	<link rel="stylesheet" href="../common_assets/css/general.css">
	<link rel="stylesheet" href="assets/css/generator.css" type="text/css" />

	<script src="../common_assets/js/jquery.min.js"></script>
	<script src="../common_assets/js/popper.min.js"></script>


</head>

<body>


    <div class="greysection">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-md-12 col-12 text-center">
                    <img src="assets/images/logo-black.png" class="img-fluid">
                </div>
            </div>
        </div>
    </div>

    <div class="whitesection">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-12 f-md-45 f-28 w700 text-center black-clr lh140">
                    Here's How You Can Generate <br class="d-none d-md-block"> Your Own Bonus Page Easily
                </div>
            </div>
        </div>
    </div>

    <div class="formsection">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-12 mb30 mb-sm0">
                    <img src="assets/images/productbox.png" class="img-fluid">
                </div>

                <?php //echo "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI']; ?>
                <?php //echo "http://" . $_SERVER['SERVER_NAME'] ."/". basename(__DIR__)."/index.php?name=".$name."&affid=".$affid."&pic=".$filename;  ?>
                <?php

                if(isset($_POST['submit'])) 
                {
	               $name=$_REQUEST['name'];
	               $afflink=$_REQUEST['afflink'];
                	//$picname=$_REQUEST['pic'];
                	/*$tmpname=$_FILES['pic']['tmp_name'];
                	$type=$_FILES['pic']['type'];
                	$size=$_FILES['pic']['size']/1024;
                	$rand=rand(1111111,9999999999);*/
	           if($afflink=="")
	           {
		
                echo 'Please fill the details.';
	           }
               
               else
               
               {
			
			     /*if(($type!="image/jpg" && $type!="image/jpeg" && $type!="image/png" && $type!="image/gif" && $type!="image/bmp") or $size>1024)
			     {
				    echo "Please upload image file (size must be less than 1 MB)";	
			     }
		  	   else
			 {*/
				//$filename=$rand."-".$picname;
				//move_uploaded_file($tmpname,"images/".$filename);
	           $url="https://www.vidvee.com/special-bonus/?afflink=".trim($afflink)."&name=".urlencode(trim($name));
				echo "<a target='_blank' href=".$url.">".$url."</a><br>";
				//header("Location:$url");
			//}	
	   }
	}
?>
				<div class="col-md-6 col-12">
					<form class="row" action="" method="post" enctype="multipart/form-data">
						<div class="col-12 form_area padding0 mt5 xsmt10">
							<div class="col-md-10 col-12 mt1 xsmt2 clear">
								<input type="text" name="name" placeholder="Your Name..." required>
							</div>

							<div class="col-md-10 col-12 mt20 xsmt4 clear">
								<input type="text" name="afflink" placeholder="Your Affiliate Link..." required>
							</div>

							<div class="col-md-10 col-12 f-24 f-md-30 white-clr center-block mt10 mt-sm20 w500 clear">
								<input type="submit" value="Generate Page" name="submit" class="f-md-30 f-24" />
							</div>
						</div>
					</form>
				</div>
            </div>
        </div>
    </div>

    <!--Footer Section Start -->
            <div class="space-section footer-bg">
                <div class="container px-sm15 px0">
                    <div class="row inner-content">
                        <div class="col-12 text-center">
                            <div> <img src="assets/images/white-logo.png" class="img-responsive center-block"> </div>
                            <div class="f-18 f-md-18 w400 mt20 lh130 white-clr text-center">Note: This site is not a part of the Facebook website or Facebook Inc. Additionally, This site is NOT endorsed by Facebook in any way. FACEBOOK &amp; INSTAGRAM are the trademarks of FACEBOOK, Inc. </div>
                        </div>

                    
                    <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                    <div class="f-18 f-md-20 w400 lh130 white-clr text-xs-center">Copyright © VidVee 2022</div>
                    <ul class="footer-ul w400 f-18 f-md-20 white-clr text-center text-md-right">
                        <li><a href="https://support.bizomart.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                        <li><a href="http://vidvee.com/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                        <li><a href="http://vidvee.com/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                        <li><a href="http://vidvee.com/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                        <li><a href="http://vidvee.com/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                        <li><a href="http://vidvee.com/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                        <li><a href="http://vidvee.com/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                    </ul>
                </div>
                </div>
                </div>
            </div>
            <!--Footer Section End -->

    <!-- Footer Section End -->


</body>

</html>
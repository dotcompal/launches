$(document).ready(function () {

	// Header Sticky js //
	// if ($(this).scrollTop() > 70) {
    //    $('.trans-header .saglus-header').addClass('bg-navbar');        
	// } 
	// else {
	// 	  $('.trans-header .saglus-header').removeClass('bg-navbar');          
	// }

	$('.trans-header .saglus-header').hover(function(){
			$(this).addClass('hover_bg_color');
			$(this).addClass('bg-navbar');
	}, function(){
		$(this).removeClass('hover_bg_color');
			if ($(document).scrollTop() < 70) {
				if($('.megamenu').hasClass('show')){
					$('.trans-header .saglus-header').addClass('bg-navbar');
				}
				else if(!$('.trans-header .saglus-header').hasClass('hover_bg_color')){
				$('.trans-header .saglus-header').removeClass('bg-navbar');
			}
		}
	})

	$(window).scroll(function(){
	    if ($(this).scrollTop() > 70) {
	        $('.trans-header .saglus-header').addClass('bg-navbar');          
	    } 
		else {
	        if($('.trans-header .saglus-header').hasClass('hover_bg_color')){
				$('.trans-header .saglus-header').addClass('bg-navbar');
			} 
			else if($('.megamenu').hasClass('show')){
					$('.trans-header .saglus-header').addClass('bg-navbar');
				}
			else{
				$('.trans-header .saglus-header').removeClass('bg-navbar');
			}
	    }
	});

	// CHANGE BG ON HOVER
	/*$('.trans-header .saglus-header').hover(function () {
		$(this).addClass("bg-navbar");
	},
	function () {
	if ($(document).scrollTop() < 70) {
			$(this).removeClass("bg-navbar");
		}
	});*/

	// MULTILEVEL MENU ON HOVER
	$('.new-custom-megamenu .tab-pane').removeClass("fade");
	$('.nav-item a').hover(function (e) {
	  	e.preventDefault()
	  	$(this).tab('show')
	})

	
    // $('.saglus-header .nav-item.dropdown').mouseenter(function() {
    // 	$(this).addClass('active').siblings().removeClass('active');
    // });
    // $('.saglus-header.bg-navbar').mouseleave(function() {
    // 	$(this).children().children().children(".nav-item.dropdown").addClass('dsfdfasdfsdfsdfsadf');
    // });

	
	// Business  FAQ's Accordion
	$(".business-img").click(function() {
	   var imagevalue = $(this).attr("id");
	   $(".collapse-ex").hide();
	   $(".collapse-ex").fadeOut(500).removeClass("d-block");
	   $("."+imagevalue).fadeIn(500).addClass( "d-block");
	});
	
	// Solutions Select js */
	$(".select-solutions .dropdown-menu a.dropdown-item").click(function(){
		var selText = $(this).text();
		$(this).parents('.pricing-spilt-dropdown').find('.selectedvalue').html(selText);
	});
	

	

	
	// Toggle Password 
		$(".toggle-password").click(function() {
        $(".toggle-icon").toggleClass("icon-show icon-hide");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });
	
    // Modal Popup Gender Select Dropdown //
	$(".dropdown-menu li a.select-dropvalue").click(function(){
     var selText = $(this).text();
     $(this).parents('.dropas-select').find('.dropdown-toggle').html('<span class="caret-right">'+selText+'</span>');
	});

	// Select option on select div show engage ab testing page //
	    $(".onselectoption").change(function(){

        $(this).find("option:selected").each(function(){

            var optionValue = $(this).attr("value");

            if(optionValue){

                $(".timeshow").not("." + optionValue).hide();

                $("." + optionValue).show();

            } else{

                $(".timeshow").hide();

            }

        });

    }).change();
	// End Modal Popup Gender Select Dropdown //
	
	// Stop Propagation  Dropdown Js //	  	
	$('div.dropdown-menu.stoppropagation').on('click', function (event) {
		event.stopPropagation();
	});
	$('div.dropdown-menu .stoppropagation').on('click', function (event) {
		event.stopPropagation();
	});
	
	// Tooltip js //
	$('[data-toggle="tooltip"]').tooltip();
	
	// Stickey Header Js //
	$(window).scroll(function () {
		var sticky = $('.sticky-fix'),
			scroll = $(window).scrollTop();
		if (scroll > 0) {
			if (!$('.virtual-header').length) {
				$virtual = $('<div class="virtual-header">');
				$virtual.css({
					'width': '100%',
					'height': sticky.css('height'),
					'position': 'relative'
				});
				sticky.after($virtual);
			}
			sticky.css({
				'position': 'fixed',
				'top': '0',
				'width': '100%',
				'z-index': '1089'
			});
		}
	});
	
	// Mobile Menu Toggle Button //
	//$(".navbar-toggler").click(function(){
	//  $(this).toggleClass("closebutton");  
	//});
	
	// Header Search Bar Script //
	$(".search-icon").click(function(){
        $(".search-text").toggleClass("searchwidth");	
		$(".expand-search").toggleClass("search-icon-bg");
    });
		
	var isOpen = false;
	$(".search-icon").click(function(){
		if(isOpen == false){
			$(".search-text").addClass('searchwidth');
			$(".search-text").focus();
			isOpen = true;
		} else {
			$(".search-text").removeClass('searchwidth');
			$(".search-text").focusout();
			isOpen = false;
		}
	}); 
	
	$(".search-icon").mouseup(function(){
			return false;
		});
	$(".expand-search").mouseup(function(){
			return false;
		});
	$(document).mouseup(function(){
		if(isOpen == true){
			$('.search-icon').css('display','inline-block');
			$(".search-icon").click();
		}
	});
	
	/*Add smooth scroll on hashtag anchors*/
	$("a[href='#jobopenings']").on('click', function(event) {    
		if (this.hash !== "") {      
		  event.preventDefault();      
		  var hash = this.hash;      
		  $('html, body').animate({
			scrollTop: $(hash).offset().top
		  }, 800, function(){
			window.location.hash = hash;
		  });
		} 
	  });
	  
		/*Add smooth scroll on hashtag anchors*/
		$("a[href='#featurescomparision']").on('click', function(event) {    
		if (this.hash !== "") {      
		  event.preventDefault();      
		  var hash = this.hash;      
		  $('html, body').animate({
			scrollTop: $(hash).offset().top
		  }, 800, function(){
			window.location.hash = hash;
		  });
		} 
	  });
	  
	  // Careers Popup Text Field Hide Show //
	$('#PasteTerxtShow,#PasteTerxtShow1').hide();
	$("#showPastTextDiv").click(function(){
		$("#PasteTerxtShow").toggle();
	});
	$("#showPastTextDiv1").click(function(){
		$("#PasteTerxtShow1").toggle();
	});

	
	// Scroll to Top Custom Javascript //	
	var scrollTrigger = 100; // px
	var offsetData = $('.footer-section').offset().top;
	var windowhight = screen.height;
			
	backToTop = function () {
	var scrollTop1 = $(window).scrollTop();
	var scrollTop = $(window).scrollTop() + windowhight;
	console.log(offsetData);			
	console.log(scrollTop +'>'+ offsetData);
	if (scrollTop1 > scrollTrigger) {				
		$('#back-to-top').addClass('show');				
		
		if (scrollTop > offsetData) {				
		$('#back-to-top').removeClass('show');				
		}			
		
	} else {
		$('#back-to-top').removeClass('show');
	}
};
backToTop();
	
    $(window).scroll(function (event) {
        backToTop();		
    });
	
    $('#back-to-top,.btn-topscroll').on('click', function (e) {
		
        e.preventDefault();		
        $('html,body').animate({
            scrollTop: 0
        }, 700);
    });
	
	// Privacy Policy Internal Linking Script */
		
	PrivacyLinkFix = function () {
		if($('.privacy-links-parent').length){
		var windowhight = screen.height;
		var SblogscrollPosSub = $('.privacy-links-parent').offset().top - 70;	
		var SblogoffsetData1 = $('.privacypageend').offset().top ; 
		
		var SblogscrollTop2 = $(window).scrollTop();
		var SblogscrollTop3 = $(window).scrollTop() + windowhight;
		console.log(SblogscrollTop2 +'>='+ SblogscrollPosSub)	;
		
		if (SblogscrollTop2 >= SblogscrollPosSub) {	
		console.log(SblogscrollPosSub);
		$('.sticky-privacylinks').addClass('privacylink-fix');		
		
		if (SblogscrollTop3 > SblogoffsetData1) {				
		$('.sticky-privacylinks').removeClass('privacylink-fix');				
		}		
		
		} 
				
		else {		
		$('.sticky-privacylinks').removeClass('privacylink-fix');
		}
		}
	};
	PrivacyLinkFix();

	$(window).scroll(function (event) {        
			PrivacyLinkFix();
		});  


	/*Add active class on hashtag anchors*/
	$(window).on('hashchange', function() {
		hash = window.location.hash;
		$('.sticky-privacylinks a').closest('li').removeClass('active');
		$('.sticky-privacylinks a[href=\"' + hash + '\"]').closest('li').addClass('active');
	});

	/*Add smooth scroll on hashtag anchors*/
	$(".sticky-privacylinks a").on('click', function(event) {    
		if (this.hash !== "") {      
		  event.preventDefault();      
		  var hash = this.hash;      
		  $('html, body').animate({
			scrollTop: $(hash).offset().top
		  }, 800, function(){
			window.location.hash = hash;
		  });
		} 
	  });
 	

	
});
var timer = 0;
function recheck() {
    var window_top = $(this).scrollTop();
    var window_height = $(this).height();
    var view_port_s = window_top;
    var view_port_e = window_top + window_height;
    
    if ( timer ) {
      clearTimeout( timer );
    }
    
    $('.fly').each(function(){
      var block = $(this);
      var block_top = block.offset().top;
      var block_height = block.height();
      
      if ( block_top < view_port_e ) {
        timer = setTimeout(function(){
          block.addClass('show-block');
        },100);       
      } else {
        timer = setTimeout(function(){
          block.removeClass('show-block');
        },100);          
      }
    });
}

$(function(){
  $(window).scroll(function(){
    recheck();
  });
  
  $(window).resize(function(){
     recheck();   
  });
  
  recheck();
});

// Solution Table Stickey Header
$(function(){
  var faq_section = '.business-owners';
var support_top = $(faq_section).offset().top - 80;
var sticky_elemnt = '.gallery-thumbnail';
	var sticky_section = '.solution-header';
 
    var aTop = $(sticky_elemnt).offset().top  + $(sticky_elemnt).height();
    var header_height = $('.trans-header').height();
    var solution_height = $('.solution-header').height();

 $(window).scroll(function(){
		
	console.log($(this).scrollTop() +'>='+ (aTop+header_height))
    if(( $(this).scrollTop()  >= (aTop + header_height) ) && support_top > $(this).scrollTop()+solution_height){
        if(!$(sticky_section).hasClass('solution-sticky-header')){
			$(sticky_section).addClass('solution-sticky-header');
			//$(sticky_section).css('top',0);
		}
	}else{
		$(sticky_section).removeClass('solution-sticky-header');
	}
  });
});

// Home Page FAQ Scroll Top Active Accordion
$('#business-accordion .collapse').on('show.bs.collapse', function(e) {
  var $card = $(this).closest('.card');
  var $open = $($(this).data('parent')).find('.collapse.show');
  var additionalOffset = 0;
  if($card.prevAll().filter($open.closest('.card')).length !== 0)
  {
        additionalOffset =  $open.height();
  }
  $('html,body').animate({
    scrollTop: $card.offset().top - additionalOffset - 100
  }, 500);
});

// Pricing Page Feature Comparision Scroll Top Active Accordion
$('#accordionEx1 .collapse').on('show.bs.collapse', function(e) {
  var $card1 = $(this).closest('.card');
  var $open1 = $($(this).data('parent')).find('.collapse.show');
  var additionalOffset1 = 0;
  if($card1.prevAll().filter($open1.closest('.card')).length !== 0)
  {
        additionalOffset1 =  $open1.height();
  }
  $('html,body').animate({
    scrollTop: $card1.offset().top - additionalOffset1 - 120
  }, 500);
});

$(document).ready(function () {

$(window).scroll(function () {
	var windowhight3 = screen.height;
	var scrolltable = $(window).scrollTop();
	var objectSelect4 = $(".comparision-section");
	var nextsection4 = $('.business-owners').offset().top - 160;
	var objectPosition4 = objectSelect4.offset().top - 40;
	if (scrolltable > objectPosition4) {
		$('.comparision-table-xs-view').addClass('sticky-table-header');
		$('.pricing-btn-xs').css('height','145');
	}
	else {
		$('.comparision-table-xs-view').removeClass('sticky-table-header');
		$('.pricing-btn-xs').css('height','auto');
	}
	
	if (scrolltable > nextsection4) {				
		$('.comparision-table-xs-view').removeClass('sticky-table-header');
		$('.pricing-btn-xs').css('height','auto');
	}
});
});

$('.accordian-body').on('shown.bs.collapse', function (e) {
	var $scc_panel = $(this).closest('td');
	$('html,body').animate({
		scrollTop: $scc_panel.offset().top - 200
	}, 500); 
});

$( document ).ready(function() {
// Responsive pricing table JS
$( "ul" ).on( "click", "li", function() {
	var pos = $(this).index()+2;
	$(".table-accordian tr").find('td:not(:eq(0))').hide();
	$("tr.pricing-btn-xs").find('td:not(:eq(0))').hide();
	$('.table-accordian td:nth-child('+pos+')').css('display','table-cell');
	$('.pricing-btn-xs td:nth-child('+pos+')').css('display','table-cell');
	$(".table-accordian tr").find('th:not(:eq(0))').hide();
	$(".pricing-btn-xs").find('th:not(:eq(0))').hide();
	$('li').removeClass('active');
	$(this).addClass('active');
});

// Initialize the media query
var mediaQuery = window.matchMedia('(min-width: 992px)');

// Add a listen event
mediaQuery.addListener(doSomething);

// Function to do something with the media query
function doSomething(mediaQuery) {    
if (mediaQuery.matches) {
  $('.pricing-xs-heading').attr('colspan',5);
} else {
  $('.pricing-xs-heading').attr('colspan',2);
}
}

// On load
doSomething(mediaQuery);
});


// ONclick Menu Hide */
$(document).on("click","a",function(){
        if($(this).closest('.inner-content-area').length){
            $(this).closest('.megamenu').addClass('d-none');
            object = $(this);
            setTimeout(function(){
                object.closest('.megamenu').removeClass('d-none');
            })
        }
    })
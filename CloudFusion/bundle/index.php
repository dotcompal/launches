<!Doctype html>
<html>
   <head>
      <title>CloudFusion | Bundle Deal</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=9">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
      <!------Meta Tags-------->
      <meta name="title" content="CloudFusion Exclusive Bundle Deal">
      <meta name="description" content="Get CloudFusion With All The Upgrades For 64% Off & Save Over $650 When You Grab This Highly-Discounted Bundle Deal">
      <meta name="keywords" content="CloudFusion">
      <meta property="og:image" content="https://www.getcloudfusion.co/bundle/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Dr. Amit Pareek">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="CloudFusion Exclusive Bundle Deal">
      <meta property="og:description" content="Get CloudFusion With All The Upgrades For 64% Off & Save Over $650 When You Grab This Highly-Discounted Bundle Deal">
      <meta property="og:image" content="https://www.getcloudfusion.co/bundle/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="CloudFusion Exclusive Bundle Deal">
      <meta property="twitter:description" content="Get CloudFusion With All The Upgrades For 64% Off & Save Over $650 When You Grab This Highly-Discounted Bundle Deal">
      <meta property="twitter:image" content="https://www.getcloudfusion.co/bundle/thumbnail.png">
      <!------Meta Tags-------->
      <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
      <link href="https://fonts.googleapis.com/css2?family=Kalam:wght@300;400;700&family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
      <!-- required -->
      <link rel="stylesheet" href="../common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style.css" type="text/css">
      <script type='text/javascript' src="../common_assets/js/jquery.min.js"></script>
      <link rel="stylesheet" href="assets/css/timer.css">
      <!-- <script src="assets/js/timer.js"></script> -->

      <!-- New Timer  Start-->
         <style>
            .timer-header-top{background: #000; padding-top:10px; padding-bottom:10px;}
         </style>  
      <script>
         (function(w, i, d, g, e, t, s) {
             if(window.businessDomain != undefined){
                 console.log("Your page have duplicate embed code. Please check it.");
                 return false;
             }
             businessDomain = 'CloudFusion';
             allowedDomain = 'getcloudfusion.co';
             if(!window.location.hostname.includes(allowedDomain)){
                 console.log("Your page have not authorized. Please check it.");
                 return false;
             }
             console.log("Your script is ready...");
             w[d] = w[d] || [];
             t = i.createElement(g);
             t.async = 1;
             t.src = e;
             s = i.getElementsByTagName(g)[0];
             s.parentNode.insertBefore(t, s);
         })(window, document, '_gscq', 'script', 'https://cdn.staticdcp.com/apps/engage/smart_engage/js/config-loader.js');
         </script>
          <style>
            .timer-header-top{background: #05071A; padding-top:10px; padding-bottom:10px;}
         </style>  
   </head>
      <div class="timer-header-top fixed-top">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-12 col-md-3">
                  <div class="tht-left f-14 f-md-16 white-clr w700 text-md-start text-center">
                      Coupon Code <span class="orange-clr">"CloudFusion"</span> <br class="d-block d-md-none"> for <span class="orange-clr"> $50 Discount </span> Ending In
                  </div>
               </div>
               <div class="col-7 col-md-6 text-center ">
                  <div class="countdown counter-white text-center hs_hours"><div class="timer-label text-center"><span class="f-16 f-md-24 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-12 f-md-15 w500 smmltd">Days</span> </div><div class="timer-label text-center"><span class="f-16 f-md-24 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-12 f-md-15 w500 smmltd">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-16 f-md-24 timerbg oswald">58&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-12 f-md-15 w500 smmltd">Mins</span> </div><div class="timer-label text-center "><span class="f-16 f-md-24 timerbg oswald">54</span><br><span class="f-12 f-md-15 w500 smmltd">Sec</span> </div></div>
               </div>
               <div class="col-5 col-md-3 text-md-end">
                  <div class="instant-btn1"><a href="https://www.jvzoo.com/b/109507/398672/2" class="t-decoration-none">Buy Now</a></div>
               </div>
            </div>
         </div>
      </div>
   <body>
      <!--1. Header Section Start -->
      <div class="header-section">
         <div class="container">
            <div class="row">
               <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 926.8 148.17" style="max-height:50px">
                  <defs>
                     <style>
                        .cls-1{fill:#0068ff;}
                        .cls-2{fill:#f89b1c;}
                        .cls-3{fill:#fff;}
                     </style>
                  </defs>
                  <g id="Layer_2" data-name="Layer 2">
                  <g id="Layer_1-2" data-name="Layer 1">
                  <path class="cls-1" d="M41.41,108.34a22.49,22.49,0,0,1-2.87-11c0-12.08,9.69-22.18,22.05-23l16.06-1.06L72.4,58.17a35,35,0,0,1-1.31-9.5c0-19.78,16.54-35.86,36.86-35.86a37.33,37.33,0,0,1,24.89,9.41,35.39,35.39,0,0,1,11.81,23.06l.59,6.21c4.11-3.79,8.07-7.79,11.84-12C152.67,17,132.35,0,108,0c-27.63,0-50,21.79-50,48.67A47.76,47.76,0,0,0,59.7,61.56c-19.17,1.26-34.33,16.8-34.33,35.77a35,35,0,0,0,3.28,14.81C32.55,111.06,36.83,109.79,41.41,108.34Z"></path>
                  <path class="cls-2" d="M165.9,50.66c10.88-20.3,14.58-35.47,14.58-35.47C142.66,114.85,2.87,139.07,0,140H163.24c25.39,0,46-20,46-44.71C209.2,71.47,190,52,165.9,50.66Zm-2.66,76.53H91.94a188.75,188.75,0,0,0,66.3-63.45l1.07-.12a34.8,34.8,0,0,1,3.93-.23c18.09,0,32.8,14.31,32.8,31.9S181.33,127.19,163.24,127.19Z"></path><text></text>
                  <path class="cls-3" d="M313.48,73.63H295.09A18.17,18.17,0,0,0,293,67.27a16.13,16.13,0,0,0-4-4.77,17.68,17.68,0,0,0-5.6-3,21.68,21.68,0,0,0-6.86-1A20.48,20.48,0,0,0,265,61.73a21.33,21.33,0,0,0-7.68,9.57,38.07,38.07,0,0,0-2.73,15.22A38.43,38.43,0,0,0,257.29,102a19.88,19.88,0,0,0,19.16,12.64,22.63,22.63,0,0,0,6.78-1,17.28,17.28,0,0,0,5.56-2.83,15.92,15.92,0,0,0,4-4.55,18.16,18.16,0,0,0,2.25-6.13l18.39.08a33.44,33.44,0,0,1-3.55,11.4,34.43,34.43,0,0,1-7.6,9.78,35.64,35.64,0,0,1-11.33,6.8,41.64,41.64,0,0,1-14.84,2.5,40.45,40.45,0,0,1-20.55-5.21,36.72,36.72,0,0,1-14.27-15.07q-5.24-9.87-5.23-23.88t5.29-23.93a37,37,0,0,1,14.36-15.05,40.29,40.29,0,0,1,20.4-5.18A44.26,44.26,0,0,1,290,44.45a35.54,35.54,0,0,1,11.36,6.11,32.24,32.24,0,0,1,8.08,9.8A36.94,36.94,0,0,1,313.48,73.63Z"></path>
                  <path class="cls-3" d="M344.08,43.53v86H326.19v-86Z"></path>
                  <path class="cls-3" d="M387.48,130.76a32.77,32.77,0,0,1-16.9-4.18,28.15,28.15,0,0,1-11-11.67,37.37,37.37,0,0,1-3.87-17.4A37.59,37.59,0,0,1,359.61,80a28.15,28.15,0,0,1,11-11.67,36.25,36.25,0,0,1,33.8,0,28.15,28.15,0,0,1,11,11.67,37.59,37.59,0,0,1,3.86,17.48,37.37,37.37,0,0,1-3.86,17.4,28.15,28.15,0,0,1-11,11.67A32.77,32.77,0,0,1,387.48,130.76Zm.08-13.85a11.05,11.05,0,0,0,7.43-2.54,15.66,15.66,0,0,0,4.52-7,33.67,33.67,0,0,0,0-20.07,15.81,15.81,0,0,0-4.52-7,11,11,0,0,0-7.43-2.56A11.29,11.29,0,0,0,380,80.39a15.48,15.48,0,0,0-4.58,7,33.67,33.67,0,0,0,0,20.07,15.34,15.34,0,0,0,4.58,7A11.38,11.38,0,0,0,387.56,116.91Z"></path>
                  <path class="cls-3" d="M472.23,102.05V65h17.88V129.5H473V117.79h-.68A19.08,19.08,0,0,1,465,126.9a21.41,21.41,0,0,1-12.32,3.44,21.69,21.69,0,0,1-11.37-2.94,20.09,20.09,0,0,1-7.67-8.35,28.77,28.77,0,0,1-2.79-13V65h17.89v37.87c0,3.8,1.05,6.81,3.06,9a10.41,10.41,0,0,0,8.1,3.32,12.78,12.78,0,0,0,6.05-1.49,11.58,11.58,0,0,0,4.55-4.43A13.75,13.75,0,0,0,472.23,102.05Z"></path>
                  <path class="cls-3" d="M528.14,130.55a24.16,24.16,0,0,1-13.28-3.8,25.78,25.78,0,0,1-9.4-11.21Q502,108.13,502,97.35q0-11.09,3.57-18.45a25.73,25.73,0,0,1,9.51-11,24.29,24.29,0,0,1,13-3.68,20,20,0,0,1,9,1.83A18.15,18.15,0,0,1,543,70.54a24.43,24.43,0,0,1,3.42,5.31H547V43.53h17.84v86H547.2V119.17h-.75a23.05,23.05,0,0,1-3.55,5.32,18.09,18.09,0,0,1-5.92,4.34A20.5,20.5,0,0,1,528.14,130.55Zm5.67-14.23a11.47,11.47,0,0,0,7.33-2.37,14.89,14.89,0,0,0,4.61-6.66,28.24,28.24,0,0,0,1.62-10,28.22,28.22,0,0,0-1.6-10,14.33,14.33,0,0,0-4.61-6.55,11.77,11.77,0,0,0-7.35-2.31,11.58,11.58,0,0,0-7.43,2.4,14.57,14.57,0,0,0-4.57,6.63,28.38,28.38,0,0,0-1.56,9.82,28.82,28.82,0,0,0,1.58,9.93,15,15,0,0,0,4.57,6.72A11.44,11.44,0,0,0,533.81,116.32Z"></path>
                  <path class="cls-3" d="M582.76,129.5v-86H634.3v9.24H593.17v29h37.27v9.24H593.17V129.5Z"></path>
                  <path class="cls-3" d="M687.36,103.14V65h9.91V129.5h-9.91V118.59h-.67a21,21,0,0,1-7.05,8.33q-4.78,3.42-12.09,3.42a21.41,21.41,0,0,1-10.75-2.67,18.33,18.33,0,0,1-7.38-8q-2.7-5.4-2.69-13.63V65h9.91v40.3q0,7.05,4,11.25a13.35,13.35,0,0,0,10.14,4.2,17,17,0,0,0,7.54-1.89,16.58,16.58,0,0,0,6.46-5.79A17.49,17.49,0,0,0,687.36,103.14Z"></path>
                  <path class="cls-3" d="M761.08,79.46,752.18,82a17.11,17.11,0,0,0-2.46-4.34,12.34,12.34,0,0,0-4.39-3.51,15.87,15.87,0,0,0-7.09-1.38,16.94,16.94,0,0,0-9.84,2.7,8.09,8.09,0,0,0-3.93,6.87,7.05,7.05,0,0,0,2.69,5.83q2.69,2.14,8.39,3.57l9.57,2.35q8.65,2.1,12.89,6.4a15.09,15.09,0,0,1,4.24,11.06,16.46,16.46,0,0,1-3.17,9.91,21.35,21.35,0,0,1-8.83,6.89,32.4,32.4,0,0,1-13.19,2.51q-9.85,0-16.33-4.28a19,19,0,0,1-8.18-12.51l9.4-2.35a13.09,13.09,0,0,0,5.1,7.81q3.76,2.59,9.85,2.6,6.92,0,11-3c2.72-2,4.09-4.34,4.09-7.11a7.61,7.61,0,0,0-2.35-5.65,15.26,15.26,0,0,0-7.22-3.42l-10.75-2.52q-8.85-2.1-13-6.53a15.61,15.61,0,0,1-4.14-11.1,15.82,15.82,0,0,1,3.09-9.65,20.63,20.63,0,0,1,8.44-6.59,29.41,29.41,0,0,1,12.15-2.4q9.57,0,15,4.2A22.7,22.7,0,0,1,761.08,79.46Z"></path>
                  <path class="cls-3" d="M782.23,54.28a6.94,6.94,0,0,1-5-2,6.44,6.44,0,0,1,0-9.48,7.24,7.24,0,0,1,10,0,6.46,6.46,0,0,1,0,9.48A7,7,0,0,1,782.23,54.28Zm-5,75.22V65h9.91V129.5Z"></path>
                  <path class="cls-3" d="M831.43,130.84a27.22,27.22,0,0,1-25.54-15.78,39.11,39.11,0,0,1-3.68-17.46A39.59,39.59,0,0,1,805.89,80,28.25,28.25,0,0,1,857,80a39.59,39.59,0,0,1,3.68,17.59A39.11,39.11,0,0,1,857,115.06a27.22,27.22,0,0,1-25.54,15.78Zm0-8.89a17,17,0,0,0,10.91-3.41,19.92,19.92,0,0,0,6.34-8.94,34.21,34.21,0,0,0,2.06-12,34.62,34.62,0,0,0-2.06-12,20.31,20.31,0,0,0-6.34-9,19,19,0,0,0-21.83,0,20.37,20.37,0,0,0-6.33,9,34.39,34.39,0,0,0-2.06,12,34,34,0,0,0,2.06,12,20,20,0,0,0,6.33,8.94A17,17,0,0,0,831.43,122Z"></path>
                  <path class="cls-3" d="M885.66,90.71V129.5h-9.9V65h9.57V75.1h.84a18.47,18.47,0,0,1,6.88-7.91q4.62-3,11.92-3a23.6,23.6,0,0,1,11.46,2.67,18.21,18.21,0,0,1,7.64,8.06c1.82,3.59,2.73,8.14,2.73,13.62v41h-9.9V89.2q0-7.59-3.95-11.86t-10.83-4.26a17.2,17.2,0,0,0-8.46,2.06,14.67,14.67,0,0,0-5.86,6A19.9,19.9,0,0,0,885.66,90.71Z"></path>
                  <text></text>
               </g>
            </g>
            </svg>
            </div>
            <div class="row">
               <div class="col-12 mt30 mt-md50 text-center">
                  <div class="preline f-18 f-md-24 w600 white-clr lh140">
                     <div class="preheadline">
                     <u class="red-clr">WARNING:</u> <span class="uppercase">This Limited Time DISCOUNTED Bundle Offer Expires Soon</span>
                  </div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 relative">
                <div class="mainheadline f-md-45 f-28 w400 text-center black-clr lh140">
                  <span class="w700"> Store & Deliver All Your Videos, Images, Trainings, Audios, & Media Files </span> at <span class="underline-text">Blazing-Fast Speed.</span> No Monthly Fee Ever…
                  </div>
               </div>
               
               <div class="col-12 col-md-9 mx-auto mt-md30 mt20 f-18 f-md-26 w500 text-center lh140 white-clr text-capitalize">
               Get CloudFusion With All The Upgrades For 70% Off & Save Over $697 When You Grab This Highly-Discounted Bundle Deal...
               </div>
               <div class="col-12 col-md-10 mt20 mt-md30 mx-auto">
                  <div class="responsive-video">
                     <iframe src="https://maxdrive.dotcompal.com/video/embed/6peomttpi8" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                     box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                     </div>
                  <!-- <img src="assets/images/pbb-img.webp" class="img-fluid d-block mx-auto img-shadow" alt="ProductBox"> -->
               </div>
            </div>
         </div>
      </div>
      <!--1. Header Section End -->
      <!---New Section Start---->
      <div class="new-section content-mbl-space">
      <div class="container">
             <div class="row">
                 <div class="col-12 f-md-45 f-28 w500 text-center black-clr lh170 line-center ">
                     Save $697 RIGHT AWAY-
                     <div class="gradient-clr w700 f-md-70 f-40">
                         Deal Ends Soon
                     </div>
                 </div>
             </div>
             <div class="row">
                 <div class="col-12 col-md-6">
                    <div class="f-16 f-md-18 lh140 w500">
                       <ul class="menu-list pl0 m0">
                          <li>GET Complete CloudFusion Package (FE + ALL Upgrades + Agency License) </li>
                          <li>No Monthly Payment Hassles- It's Just A One-Time Payment</li>
                          <li>GET Priority Support from Our Dedicated Support Engineers</li>
                       </ul>
                    </div>
                 </div>
                 <div class="col-12 col-md-6">
                    <div class="f-16 f-md-18 lh140 w500">
                       <ul class="menu-list pl0 m0">
                          <li>Provide Top-Notch Services to Your Clients</li>
                          <li>Grab All Benefits For A Low, ONE-TIME Discounted Price</li>
                          <li>GET 30-Days Money Back Guarantee</li>
                       </ul>
                    </div>
                 </div>
              </div>
             <div class="row">
                 <div class="col-12 mt20 mt-md40 f-md-26 f-22 lh140 w400 text-center black-clr">
                     <div class="f-24 f-md-36 w500 lh150 text-center black-clr">
                         <img src="assets/images/hurry.png " alt="Hurry " class="mr10 hurry-img">
                         <span class="red-clr ">Hurry!</span> Special One Time Price for Limited Time Only!
                     </div>
                     <div>
                         Use Coupon Code <span class="w600 orange-clr">"CloudFusion"</span> for an Additional <span class="w600 orange-clr">$50 Discount</span> 
                     </div>
                     <div class="mt20 red-clr w600">
                     <img src="assets/images/red-close.webp " alt="Hurry" class="mr10 close-img">
                        No Recurring | No Monthly | No Yearly
                     </div>
                     <div class="col-md-8 col-12 instant-btn mt20 f-24 f-md-30 mx-auto px0">
                         <a href="https://www.jvzoo.com/b/109507/398672/2">
                      GET Bundle Offer Now &nbsp;<i class="fa fa-angle-double-right f-32 f-md-42 angle-right" aria-hidden="true"></i>
                      </a>
                     </div>
                     <div class="col-12 mt-md20 mt20 f-18 f-md-18 w600 lh140 text-center black-clr">
                         No Download or Installation Required
                     </div>
                     <div class="col-12 mt20 mt-md20">
                         <img src="assets/images/compaitable-with.png" class="img-fluid mx-auto d-block">
                      </div>
                 </div>
             </div>
         </div>
      </div>
      <!--1. New Section End -->
      <!--2. Second Section Start -->
      <div class="section1">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-md-45 f-28 black-clr  text-center w700 lh140">
                     Here’s What You Get With <span class=" w700">Limited Time <br class="d-none d-md-block"> CloudFusion Discounted</span> Bundle Today
                  </div>
               </div>
               <!-- AGENCY BUNDLE START -->
               <div class="col-12 mt20 mt-md70 section-margin px-md-0">
                  <div class="col-12 plan1-shape">
                     <div class="col-md-10 col-12 px-md15 mt-md0 mt20 mx-auto">
                        <div class="pimg">
                           <img src="assets/images/commercial.webp" class="img-fluid d-block mx-auto" alt="Commercial">
                        </div>
                     </div>
                     <div class="row p0 mt20 mx-0 mt-md0">
                        <div class="col-12 p0 text-center mt-md50 mt20">
                           <div class="title-shape">
                              <div class="f-24 f-md-38 w600 lh150 white-clr text-center">
                                 CloudFusion Commercial ($97)
                              </div>
                           </div>
                        </div>
                        <div class="col-12 col-md-6 px0 px-md15">
                           <div class="f-20 f-md-20 lh150 w400 mt20 mt-md50">
                              <ul class="kaptick pl0">
                                 <li>
                                    <span class="w600">Host and Manage All Your Files from One Easy Dashboard –</span> Videos, Web Images, PDFs, Docs, Audio, and Zip Files.
                                 </li>
                                 <li>
                                    <span class="w600">Super-Fast Delivery (After All, Time Is Money!) - </span> This lead to More Engagement, Leads & Sales. 
                                 </li>
                                 <li>
                                    <span class="w600"> Play Sales, Demo & Training Videos in HD</span>  on Any Page, Site, or Members Area.
                                 </li>
                                 <li>
                                    <span class="w600">Tap Into HUGE E-Learning Industry - </span> Deliver Your Videos, Docs, and PDF Training on Done-For-You and Beautiful Doc Sharing Sites & Pages. 
                                 </li>
                                 <li>
                                    Use Our App to Help <span class="w600">Speed-Up Your Website, Landing Pages & Online Shops with Fast Loading & Optimized Images & Videos. </span> 
                                 </li>
                                 <li>
                                    Build Your Online Empire - <span class="w600">Deliver Digital Products Securely, Fast & Easy 
                                    </span>
                                 </li>
                                 <li>
                                    <span class="w600">Generate Tons of Leads & Affiliate Sales –  </span> Deliver Freebies & Affiliate Bonuses to Your Subscribers Without a Hitch
                                 </li>
                                 <li>
                                    <span class="w600">Share Your Files Privately </span>  with Your Clients, Customers & Team Members.
                                 </li>
                                 <li>
                                    <span class="w600">Free Hosting </span> Is Included, Unlimited Bandwidth & Upto 250 GB Storage 
                                 </li>
                                 <li>
                                    PLUS, <span class="w600">Limited Time Free Commercial License  </span> (only for today) to Serve Your Clients
                                 </li>
                                
                              </ul>
                           </div>
                        </div>
                        <div class="col-12 col-md-6 px0 px-md15">
                           <div class="f-20 f-md-20 lh150 w400 mt0 mt-md50">
                              <ul class="kaptick pl0 p0 m0">
                                 <li>Elegant, <span class="w600">SEO Optimized & 100% Mobile Responsive </span> File Share Pages </li>
                                 <li>Inbuilt <span class="w600">Lead Management System </li>
                                 <li><span class="w600">Access Files Anytime, Anywhere</span> Directly from The Cloud on Any Device!</li>
                                 <li><span class="w600">Highly Encrypted Unbreakable File Security</span> with SSL & OTP Enabled Login </li>
                                 <li>30 Days <span class="w600">Online Back-up & File recovery </span> So You Never Lose Your Precious Data and Files. </li>
                                 <li><span class="w600">Single Dashboard</span> to Manage All Business Files- No Need to Buy Multiple Apps 
                                 <li><span class="w600">Manage Files Effortlessly –</span> Share Multiple Files, Full Text Search & File Preview </li>
                                 <li>Inbuilt Elegant Video Player with <span class="w600">HDR Support</span> </li>
                                 <li><span class="w600">Real-Time Analytics </span>for Every Single File You Upload - See Downloads, Shares, etc.  </li>
                                 <li><span class="w600">Easy and Intuitive</span> to Use Software with <span class="w600">Step-by-Step Video Training </span></li>
                                 <li><span class="w600">100% Newbie Friendly</span> & Fully Cloud Based Software </li>
                                 <li><span class="w600">Live Chat -</span> Customer Support So You Never Get Stuck or Have any Issues </li>
                              </ul>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- AGENCY BUNDLE END -->
               <!-- ELITE BUNDLE START -->
               <div class="col-12 px-md0 mt40 mt-md140 section-margin">
                  <div class="col-12 plan2-shape">
                     <div class="col-md-12 mx-auto mt-md0 mt20">
                        <div class="pimg">
                           <img src="assets/images/elite.webp" class="img-fluid d-block mx-auto" alt="Elite">
                        </div>
                     </div>
                     <div class="row mt-md0 mt20 mx-0">
                        <div class="col-12 p0 text-center mt-md50 mt20">
                           <div class="title-shape">
                              <div class="f-22 f-md-28 w600 white-clr lh150 text-center">
                                 Pro Upgrade 1- CloudFusion Elite ($297)
                              </div>
                           </div>
                        </div>
                        <div class="col-12 col-md-6 px0 px-md15">
                           <div class="f-20 f-md-20 lh150 w400 mt20 mt-md50">
                              <ul class="kaptick pl0">
                                 <li> <span class="w600"> Break Free & Go Limitless- </span> Create Unlimited Subdomains/Businesses, Add Unlimited Custom Domains, Capture Unlimited Leads, Share Unlimited Files & Get Unlimited Views & Visitors, Get Unlimited Bandwidth, Create Unlimited Channels, Create Unlimited Folders & Sub Folders</li>
                                 <li> <span class="w600">Create UNLIMITED Custom Domains</span> Build Your Authority And Credibility</li>
                                 <li> <span class="w600">Share Or Publish UNLIMITED Files, Get UNLIMITED Views & UNLIMITED Visitors.</span> </li>
                                 <li> Create UNLIMITED – Beautiful Channels With Your LOGO <span class="w600">To Deliver Your Trainings, Files, And Documents Effortlessly</span></li>
                                 <li> <span class="w600">Keep Data Separate & Secured For Your Team Members</span> With Personal Drive For Them</li>
                                 <li> <span class="w600">Maximize Social Traffic</span> With Hassle-Free Social Media Sharing</li>
                                 <li> Cutting-Edge Autoresponder Integration To <span class="w600">Send Your Subscribers Autoresponder Sequence In Complete Automation</span></li>
                              </ul>
                           </div>
                        </div>
                        <div class="col-12 col-md-6 px0 px-md15">
                           <div class="f-20 f-md-20 lh150 w400 mt0 mt-md50">
                              <ul class="kaptick pl0 p0 m0">
                                 <li> <span class="w600">Get Your Subscribers Auto-Registered For Your Webinars </span>With Webinar Platform Integrations</li>
                                 <li> <span class="w600">Create UNLIMITED Businesses/Subdomains</span> To Keep Each Of Your Own And Your Client’s Business Project SeparateAutomation</li>
                                 <li>
                                    <span class="w600">Capture UNLIMITED Leads</span> To Maximize Lead Generation
                                 </li>
                                 <li>
                                    <span class="w600"> Get UNLIMITED Bandwidth</span> To Give Best User Experience
                                 </li>
                                 <li>
                                    <span class="w600"> Create UNLIMITED Folders & Their Sub-Folders</span> & Even Share Them With Your Clients Or Team Members <span class="w600">With Folder Management Feature</span>
                                 </li>
                                 <li>
                                    <span class="w600">Advanced Share Page Analytics </span> To Have Clear Insight Of What's Working & What's Not To Boost ROI
                                 </li>
                                 <li>
                                    <span class="w600">Strengthen Your Relationship With Your Customers </span> Using CRM Integrations
                                 </li>
                                 <li> Get All These Benefits At <span class="w600"> An Unparalleled Price</span></li>
                              </ul>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- ELITE BUNDLE END -->
               <!-- ENTERPRISE BUNDLE START -->
               <div class="col-12 px-md-0 mt20 mt-md70 section-margin">
                  <div class="col-12 plan1-shape">
                     <div class="col-md-12 mx-auto px-md15 mt-md0 mt20">
                        <div class="pimg">
                           <img src="assets/images/enterprise.webp" class="img-fluid d-block mx-auto" alt="Enterprise">
                        </div>
                     </div>
                     <div class="row p0 mt20 mx-0 mt-md0">
                        <div class="col-12 p0 text-center mt-md50 mt20">
                           <div class="title-shape">
                              <div class="f-22 f-md-28 w600 white-clr lh150 text-center">
                                 Pro Upgrade 2- CloudFusion Enterprise ($97)
                              </div>
                           </div>
                        </div>
                        <div class="col-12 col-md-6 px0 px-md15">
                           <div class="f-20 f-md-20 lh150 w400 mt20 mt-md50">
                              <ul class="kaptick pl0">
                                 <li>
                                    <span class="w600">Use CloudFusion To Upload Unlimited Videos-</span> Sales, Training, Prospecting, Informative Or Review Videos
                                 </li>
                                 <li>
                                    <span class="w600">Get An Attractive Video Player Completely Customizable To Your Exact Specifications To Give It Elegant Look</span>>
                                 </li>
                                 <li>
                                    <span class="w600">Customize Color And Theme Of Your Player To Give It More Attractive & Your Brand Feel</span>
                                 </li>
                                 <li>
                                    <span class="w600">Publish Videos On Your Branded Video Channels -</span> Give More Content To Your Audience And Get Connected With Them
                                 </li>
                                 <li>
                                    <span class="w600">Cater Viewers From Various Countries And Demographic Locations</span> With The Same Video Using Subtitles
                                 </li>
                                 <li>
                                    <span class="w600">Collect Leads And Sell Products Right Inside The Video And Boost Profits</span> 
                                 </li>
                                 <li>
                                    <span class="w600">Get Hordes Of Social Traffic For Your Offers</span> By Getting Your Videos Shared On Top Social Media Platforms
                                 </li>
                                 <li>
                                    <span class="w600">Know Exactly What’s Working For Your Videos With The POWER Of Deep Analytics To Boost Your PROFITS With Virtually NO Extra Efforts</span> 
                                 </li>
                              </ul>
                           </div>
                        </div>
                        <div class="col-12 col-md-6 px0 px-md15">
                           <div class="f-20 f-md-20 lh150 w400 mt0 mt-md50">
                              <ul class="kaptick pl0 p0 m0">
                                 <li>
                                    <span class="w600">Publish Your Videos Anywhere You Like In 3 Easy Steps -</span> Any Website, Landing Page, Online Shop Or Membership Site Using Just 1 Line Of Embed Code
                                 </li>
                                 <li> <span class="w600">Have Full Control & Give Unique Look To Your Videos With 5 Fully Customizable Video Players</span></li>
                                 <li> <span class="w600">Prove Yourself As PRO Entrepreneur- Customize Your Player Further With 8 Attractive And Eye-Catchy Video Frames To Get All Your Visitors Hooked To It</span></li>
                                 <li><span class="w600">Show Your Brand Name Or LOGO On Player And Present Yourself As An AUTHORITY. </span>This Feature Alone Is A HUGE Value For Your Business</li>
                                 <li><span class="w600">Unlock Our Advanced Advertisement Technology For Better Monetization With Video Ads, Image Ads, Text Ads</span></li>
                                 <li><span class="w600">Get 50+ PROVEN Converting And Ready-To-Use Lead, Promo And Social Ad Templates With Drag & Drop Editor To Make 100s Of Unique Video Ad Templates</span></li>
                                 <li><span class="w600">Capture More Leads & Know Visitor’s Feedback</span> One Time Fee By Letting All Your Visitors Interact On Your VIDEO PAGE Or Channel</li>
                              </ul>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- ENTERPRISE BUNDLE END -->
               <!-- BUSINESS DRIVE BUNDLE START -->
               <div class="col-12 px-md0 mt20 mt-md70 section-margin">
                  <div class="col-12 plan2-shape">
                     <div class="col-md-10 mx-auto mt-md0 mt20">
                        <div class="pimg">
                           <img src="assets/images/agency.webp" class="img-fluid d-block mx-auto" alt="Agency">
                        </div>
                     </div>
                     <div class="row mt-md0 mt20 mx-0">
                        <div class=" col-12 p0 text-center mt-md50 mt20">
                           <div class="title-shape">
                              <div class="f-22 f-md-28 w600 white-clr lh150 text-center">
                                 Pro Upgrade 3- CloudFusion Agency ($197)
                              </div>
                           </div>
                        </div>
                        <div class="col-12 col-md-6 px0 px-md15">
                           <div class="f-20 f-md-20 lh150 w400 mt20 mt-md50">
                              <ul class="kaptick pl0">
                                 <li><span class="w600">Directly Provide Trendy Content, Free Traffic Generation, & Lead Generation Services
                                 </span> and Charge Monthly or Recurring Amount For 100% Profits</li>
                                 <li>Comes with Dedicated Dashboard to Create <span class="w600">Accounts for the Customers in 3 Simple Clicks</span></li>
                                 <li><span class="w600"> Serve Unlimited Clients with Agency License </span></li>
                              </ul>
                           </div>
                        </div>
                        <div class="col-12 col-md-6 px0 px-md15">
                           <div class="f-20 f-md-20 lh150 w400 mt0 mt-md50">
                              <ul class="kaptick pl0 p0 m0">
                                 <li><span class="w600"> Completely Cloud-Based Tool, so no additional domain or hosting required</span></li>
                                
                                 <li><span class="w600"> Add Unlimited Team Members </span></li>
                                 <li><span class="w600"> Accurate Analysis for Team Member’s Activities For Effective Monitoring </span></li>
                              </ul>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- BUSINESS DRIVE BUNDLE END -->
               <!-- FAST ACTION BONUSES BUNDLE START -->
               <div class="col-12 px-md-0 mt20 mt-md70 section-margin">
                  <div class="col-12 plan1-shape">
                     <div class="mx-auto mt-md0 mt20">
                        <div class="pimg ">
                           <img src="assets/images/fast-action.webp" class="img-fluid d-block mx-auto" alt="Fast-Action">
                        </div>
                     </div>
                     <div class="row mt20">
                        <div class="col-12 p0 mt-md50 mt20 text-center">
                           <div class="title-shape1">
                              <div class="f-22 f-md-28 w600 white-clr lh150 text-center text-capitalize">
                                 And you're also getting these premium fast action bonuses worth ($309)
                              </div>
                           </div>
                        </div>
                        <div class="col-12 col-md-6 px0 px-md15">
                           <div class="f-20 f-md-20 lh150 w600 mt20 mt-md50">
                              <ul class="kaptick pl0">
                                 <li>Bonus #1 – Live Training - 0-10k a Month With CloudFusion Reloaded (First 1000 buyers only - $1000 Value) </li>
                                 <li>Bonus #2 – Private Access to Online Business VIP Club </li>
                                 <li>Bonus #3 – Video Training on How To Start Online Coaching Business</li>
                              </ul>
                           </div>
                        </div>
                        <div class="col-12 col-md-6 px0 px-md15">
                           <div class="f-20 f-md-20 lh150 w600 mt0 mt-md50">
                              <ul class="kaptick pl0 p0 m0">
                                 <li>Bonus #4 – Lead Generation Video Workshop</li>
                                 <li>Bonus #5 – Video Training on How To Make Money With Affiliate Marketing</li>
                              </ul>
                           </div>
                        </div>
                        <!-- <div class="col-12 col-md-6 px0 px-md15">
                           <div class="f-20 f-md-20 lh150 w600 mt20 mt-md50">
                               <ul class="kaptick pl0">
                                   <li>Super Value Bonus #5 – Video Training To Monetizing Your Website </li>
                                   <li>Super Value Bonus #6 – Training To Start Affiliate Marketing </li>
                                   <li>Super Value Bonus #7 – Video Training On Viral Marketing </li>
                                   <li>Super Value Bonus #8 – Video Marketing Profit Kit </li>
                               </ul>
                           </div>
                           </div> -->
                     </div>
                  </div>
               </div>
               <!-- FAST ACTION BONUSES BUNDLE END -->
            </div>
         </div>
      </div>
      <!--2. Second Section End -->
      <!--3. Third Section Start -->
      <div class="section2">
         <div class="container">
            <div class="row ">
               <div class="col-12 px0 px-md15" id="buybundle">
                  <div class="f-md-45 f-28 black-clr  text-center w700 lh140">
                     Grab This Exclusive BUNDLE DISCOUNT Today…
                  </div>
                  <div class="f-18 f-md-22 w400 lh150 text-center mt20 black-clr">
                     Use Coupon Code <span class="orange-clr w700">"CloudFusion"</span> for an Additional <span class="orange-clr w700"> $50</span> Discount
                  </div>
               </div>
            </div>
            <div class="row justify-content-center">
               <div class="col-md-6 col-12 mt25 mt-md50 px-md15">
                  <div class="tablebox1">
                     <div class="col-12 tablebox-inner-bg text-center">
                        <div class="relative col-12 p0">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 926.8 148.17" style="max-height:50px">
                              <defs>
                                 <style>
                                    .cls-1{fill:#0068ff;}
                                    .cls-2{fill:#f89b1c;}
                                    .cls-3{fill:#fff;}
                                 </style>
                              </defs>
                              <g id="Layer_2" data-name="Layer 2">
                              <g id="Layer_1-2" data-name="Layer 1">
                              <path class="cls-1" d="M41.41,108.34a22.49,22.49,0,0,1-2.87-11c0-12.08,9.69-22.18,22.05-23l16.06-1.06L72.4,58.17a35,35,0,0,1-1.31-9.5c0-19.78,16.54-35.86,36.86-35.86a37.33,37.33,0,0,1,24.89,9.41,35.39,35.39,0,0,1,11.81,23.06l.59,6.21c4.11-3.79,8.07-7.79,11.84-12C152.67,17,132.35,0,108,0c-27.63,0-50,21.79-50,48.67A47.76,47.76,0,0,0,59.7,61.56c-19.17,1.26-34.33,16.8-34.33,35.77a35,35,0,0,0,3.28,14.81C32.55,111.06,36.83,109.79,41.41,108.34Z"></path>
                              <path class="cls-2" d="M165.9,50.66c10.88-20.3,14.58-35.47,14.58-35.47C142.66,114.85,2.87,139.07,0,140H163.24c25.39,0,46-20,46-44.71C209.2,71.47,190,52,165.9,50.66Zm-2.66,76.53H91.94a188.75,188.75,0,0,0,66.3-63.45l1.07-.12a34.8,34.8,0,0,1,3.93-.23c18.09,0,32.8,14.31,32.8,31.9S181.33,127.19,163.24,127.19Z"></path><text></text>
                              <path class="cls-3" d="M313.48,73.63H295.09A18.17,18.17,0,0,0,293,67.27a16.13,16.13,0,0,0-4-4.77,17.68,17.68,0,0,0-5.6-3,21.68,21.68,0,0,0-6.86-1A20.48,20.48,0,0,0,265,61.73a21.33,21.33,0,0,0-7.68,9.57,38.07,38.07,0,0,0-2.73,15.22A38.43,38.43,0,0,0,257.29,102a19.88,19.88,0,0,0,19.16,12.64,22.63,22.63,0,0,0,6.78-1,17.28,17.28,0,0,0,5.56-2.83,15.92,15.92,0,0,0,4-4.55,18.16,18.16,0,0,0,2.25-6.13l18.39.08a33.44,33.44,0,0,1-3.55,11.4,34.43,34.43,0,0,1-7.6,9.78,35.64,35.64,0,0,1-11.33,6.8,41.64,41.64,0,0,1-14.84,2.5,40.45,40.45,0,0,1-20.55-5.21,36.72,36.72,0,0,1-14.27-15.07q-5.24-9.87-5.23-23.88t5.29-23.93a37,37,0,0,1,14.36-15.05,40.29,40.29,0,0,1,20.4-5.18A44.26,44.26,0,0,1,290,44.45a35.54,35.54,0,0,1,11.36,6.11,32.24,32.24,0,0,1,8.08,9.8A36.94,36.94,0,0,1,313.48,73.63Z"></path>
                              <path class="cls-3" d="M344.08,43.53v86H326.19v-86Z"></path>
                              <path class="cls-3" d="M387.48,130.76a32.77,32.77,0,0,1-16.9-4.18,28.15,28.15,0,0,1-11-11.67,37.37,37.37,0,0,1-3.87-17.4A37.59,37.59,0,0,1,359.61,80a28.15,28.15,0,0,1,11-11.67,36.25,36.25,0,0,1,33.8,0,28.15,28.15,0,0,1,11,11.67,37.59,37.59,0,0,1,3.86,17.48,37.37,37.37,0,0,1-3.86,17.4,28.15,28.15,0,0,1-11,11.67A32.77,32.77,0,0,1,387.48,130.76Zm.08-13.85a11.05,11.05,0,0,0,7.43-2.54,15.66,15.66,0,0,0,4.52-7,33.67,33.67,0,0,0,0-20.07,15.81,15.81,0,0,0-4.52-7,11,11,0,0,0-7.43-2.56A11.29,11.29,0,0,0,380,80.39a15.48,15.48,0,0,0-4.58,7,33.67,33.67,0,0,0,0,20.07,15.34,15.34,0,0,0,4.58,7A11.38,11.38,0,0,0,387.56,116.91Z"></path>
                              <path class="cls-3" d="M472.23,102.05V65h17.88V129.5H473V117.79h-.68A19.08,19.08,0,0,1,465,126.9a21.41,21.41,0,0,1-12.32,3.44,21.69,21.69,0,0,1-11.37-2.94,20.09,20.09,0,0,1-7.67-8.35,28.77,28.77,0,0,1-2.79-13V65h17.89v37.87c0,3.8,1.05,6.81,3.06,9a10.41,10.41,0,0,0,8.1,3.32,12.78,12.78,0,0,0,6.05-1.49,11.58,11.58,0,0,0,4.55-4.43A13.75,13.75,0,0,0,472.23,102.05Z"></path>
                              <path class="cls-3" d="M528.14,130.55a24.16,24.16,0,0,1-13.28-3.8,25.78,25.78,0,0,1-9.4-11.21Q502,108.13,502,97.35q0-11.09,3.57-18.45a25.73,25.73,0,0,1,9.51-11,24.29,24.29,0,0,1,13-3.68,20,20,0,0,1,9,1.83A18.15,18.15,0,0,1,543,70.54a24.43,24.43,0,0,1,3.42,5.31H547V43.53h17.84v86H547.2V119.17h-.75a23.05,23.05,0,0,1-3.55,5.32,18.09,18.09,0,0,1-5.92,4.34A20.5,20.5,0,0,1,528.14,130.55Zm5.67-14.23a11.47,11.47,0,0,0,7.33-2.37,14.89,14.89,0,0,0,4.61-6.66,28.24,28.24,0,0,0,1.62-10,28.22,28.22,0,0,0-1.6-10,14.33,14.33,0,0,0-4.61-6.55,11.77,11.77,0,0,0-7.35-2.31,11.58,11.58,0,0,0-7.43,2.4,14.57,14.57,0,0,0-4.57,6.63,28.38,28.38,0,0,0-1.56,9.82,28.82,28.82,0,0,0,1.58,9.93,15,15,0,0,0,4.57,6.72A11.44,11.44,0,0,0,533.81,116.32Z"></path>
                              <path class="cls-3" d="M582.76,129.5v-86H634.3v9.24H593.17v29h37.27v9.24H593.17V129.5Z"></path>
                              <path class="cls-3" d="M687.36,103.14V65h9.91V129.5h-9.91V118.59h-.67a21,21,0,0,1-7.05,8.33q-4.78,3.42-12.09,3.42a21.41,21.41,0,0,1-10.75-2.67,18.33,18.33,0,0,1-7.38-8q-2.7-5.4-2.69-13.63V65h9.91v40.3q0,7.05,4,11.25a13.35,13.35,0,0,0,10.14,4.2,17,17,0,0,0,7.54-1.89,16.58,16.58,0,0,0,6.46-5.79A17.49,17.49,0,0,0,687.36,103.14Z"></path>
                              <path class="cls-3" d="M761.08,79.46,752.18,82a17.11,17.11,0,0,0-2.46-4.34,12.34,12.34,0,0,0-4.39-3.51,15.87,15.87,0,0,0-7.09-1.38,16.94,16.94,0,0,0-9.84,2.7,8.09,8.09,0,0,0-3.93,6.87,7.05,7.05,0,0,0,2.69,5.83q2.69,2.14,8.39,3.57l9.57,2.35q8.65,2.1,12.89,6.4a15.09,15.09,0,0,1,4.24,11.06,16.46,16.46,0,0,1-3.17,9.91,21.35,21.35,0,0,1-8.83,6.89,32.4,32.4,0,0,1-13.19,2.51q-9.85,0-16.33-4.28a19,19,0,0,1-8.18-12.51l9.4-2.35a13.09,13.09,0,0,0,5.1,7.81q3.76,2.59,9.85,2.6,6.92,0,11-3c2.72-2,4.09-4.34,4.09-7.11a7.61,7.61,0,0,0-2.35-5.65,15.26,15.26,0,0,0-7.22-3.42l-10.75-2.52q-8.85-2.1-13-6.53a15.61,15.61,0,0,1-4.14-11.1,15.82,15.82,0,0,1,3.09-9.65,20.63,20.63,0,0,1,8.44-6.59,29.41,29.41,0,0,1,12.15-2.4q9.57,0,15,4.2A22.7,22.7,0,0,1,761.08,79.46Z"></path>
                              <path class="cls-3" d="M782.23,54.28a6.94,6.94,0,0,1-5-2,6.44,6.44,0,0,1,0-9.48,7.24,7.24,0,0,1,10,0,6.46,6.46,0,0,1,0,9.48A7,7,0,0,1,782.23,54.28Zm-5,75.22V65h9.91V129.5Z"></path>
                              <path class="cls-3" d="M831.43,130.84a27.22,27.22,0,0,1-25.54-15.78,39.11,39.11,0,0,1-3.68-17.46A39.59,39.59,0,0,1,805.89,80,28.25,28.25,0,0,1,857,80a39.59,39.59,0,0,1,3.68,17.59A39.11,39.11,0,0,1,857,115.06a27.22,27.22,0,0,1-25.54,15.78Zm0-8.89a17,17,0,0,0,10.91-3.41,19.92,19.92,0,0,0,6.34-8.94,34.21,34.21,0,0,0,2.06-12,34.62,34.62,0,0,0-2.06-12,20.31,20.31,0,0,0-6.34-9,19,19,0,0,0-21.83,0,20.37,20.37,0,0,0-6.33,9,34.39,34.39,0,0,0-2.06,12,34,34,0,0,0,2.06,12,20,20,0,0,0,6.33,8.94A17,17,0,0,0,831.43,122Z"></path>
                              <path class="cls-3" d="M885.66,90.71V129.5h-9.9V65h9.57V75.1h.84a18.47,18.47,0,0,1,6.88-7.91q4.62-3,11.92-3a23.6,23.6,0,0,1,11.46,2.67,18.21,18.21,0,0,1,7.64,8.06c1.82,3.59,2.73,8.14,2.73,13.62v41h-9.9V89.2q0-7.59-3.95-11.86t-10.83-4.26a17.2,17.2,0,0,0-8.46,2.06,14.67,14.67,0,0,0-5.86,6A19.9,19.9,0,0,0,885.66,90.71Z"></path>
                              <text></text>
                           </g>
                        </g>
                        </svg>
                        </div>
                        <div class="col-12 mt-md40 mt20 table-headline-shape">
                           <div class="w700 f-md-32 f-22 text-center text-uppercase black-clr lh150 ">
                              Bundle Offer
                           </div>
                        </div>
                        <!-- PLAN 1 START -->
                        <div class="row mx-0 odd-shape mt-md40 mt20">
                           <div class="col-md-9 col-12 px-md15 px0">
                              <div class="w600 f-md-28 f-22 text-left text-xs-center text-uppercase white-clr lh150 mt8">
                                 Commercial
                              </div>
                           </div>
                           <div class="col-md-3 col-12 px-md15 px0">
                              <div class="col-12 price-shape">
                                 <div class="w600 f-md-35 f-22 text-center black-clr lh150">
                                    $97
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- PLAN 1 END -->
                        <!-- PLAN 2 START -->
                        <div class="row mx-0 even-shape">
                           <div class="col-md-9 col-12 px-md15 px0">
                              <div class="w600 f-md-28 f-22 text-left text-xs-center text-uppercase white-clr lh150 mt8">
                                 ELITE
                              </div>
                           </div>
                           <div class="col-md-3 col-12 px-md15 px0">
                              <div class="col-12 price-shape">
                                 <div class="w600 f-md-35 f-22 text-center black-clr lh150">
                                    $297
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- PLAN 2 END -->
                        <!-- PLAN 3 START -->
                        <div class="row mx-0 odd-shape">
                           <div class="col-md-9 col-12 px-md15 px0">
                              <div class="w600 f-md-28 f-22 text-left text-xs-center text-uppercase white-clr lh150 mt8">
                                 ENTERPRISE
                              </div>
                           </div>
                           <div class="col-md-3 col-12 px-md15 px0">
                              <div class="col-12 price-shape">
                                 <div class="w600 f-md-35 f-22 text-center black-clr lh150">
                                    $97
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- PLAN 3 END -->
                        <!-- PLAN 4 START -->
                        <div class="row mx-0 even-shape">
                           <div class="col-md-9 col-12 px-md15 px0">
                              <div class="w600 f-md-28 f-22 text-left text-xs-center text-uppercase white-clr lh150 mt8">
                                 Agency
                              </div>
                           </div>
                           <div class="col-md-3 col-12 px-md15 px0">
                              <div class="col-12 price-shape">
                                 <div class="w600 f-md-35 f-22 text-center black-clr lh150">
                                    $197
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- PLAN 4 END -->
                        <!-- PLAN 5 START -->
                        <div class="row mx-0 odd-shape">
                           <div class="col-md-9 col-12 px-md15 px0">
                              <div class="w600 f-md-28 f-22 text-left text-xs-center text-uppercase white-clr lh150 mt8">
                                 FAST ACTION BONUSES
                              </div>
                           </div>
                           <div class="col-md-3 col-12 px-md15 px0">
                              <div class="col-12 price-shape">
                                 <div class="w600 f-md-35 f-22 text-center black-clr lh150">
                                    $309
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- PLAN 5 END -->
                        <div class="col-12 px-md0 px15 mt-md30 mt20 w500 f-md-35 f-22 white-clr text-center">
                           Regular <strike>$997</strike>
                        </div>
                        <div class="row justify-content-center mx-0 ">
                           <div class="col-md-8 col-12 now-price mt20 w700 f-md-60 f-32 white-clr text-center">
                              Now $297
                           </div>
                        </div>
                        <div class="col-12 px-md0 px15 mt20 w500 f-md-25 f-20 white-clr text-center">
                           One Time Fee
                        </div>
                        <div class="mt20 red-clr w600 f-22 d-md-27 lh140">
                           <img src="assets/images/red-close.webp " alt="Hurry" class="mr10 close-img">
                              No Recurring | No Monthly | No Yearly
                           </div>
                        <div class="row justify-content-center mx-0 ">
                           <div class="myfeatures f-md-25 f-16 w400 text-center lh160">
                              <div class="hideme-button">
                                 <a href="https://www.jvzoo.com/b/109507/398672/2"><img src="https://i.jvzoo.com/109507/398672/2" alt="CloudFusion Bundle" border="0" class="img-fluid d-block mx-auto"/></a> 
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--3. Third Section End -->
      <!----Second Section---->
      <div class="second-header-section">
         <div class="container ">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-md-45 f-24 w600 lh140 text-center white-clr">
                     TOP Reasons Why You Can't Ignore <br class="d-none d-md-block"> <span class="f-md-50 f-24 w700 "> CloudFusion Bundle Deal </span>
                  </div>
               </div>
            </div>
            <div class="row mt30 mt-md65">
               <div class="col-12 col-md-10 mx-auto">
                  <div class="step-shape">
                     <div class="f-20 f-md-22 w600 white-clr lh150">
                        Reason #1
                     </div>
                     <div class="f-18 f-md-18 w400 white-clr lh150 mt10 ">
                        Get All The Benefits Of CloudFusion & Premium Upgrades To Multiply Your Results... Save More Time, Work Like A Pro & Ultimately Boost Your Agency Profits
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt30">
                  <div class="step-shape">
                     <div class="f-20 f-md-22 w600 white-clr lh150">
                        Reason #2
                     </div>
                     <div class="f-18 f-md-18 w400 white-clr lh150 mt10 ">
                        Regular Price For CloudFusion, All Upgrades & Bonuses Is $997. You Are Saving $700 Today When You Grab The Exclusive Bundle Deal Now at ONLY $297.
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt30">
                  <div class="step-shape">
                     <div class="f-20 f-md-22 w600 white-clr lh150">
                        Reason #3
                     </div>
                     <div class="f-18 f-md-18 w400 white-clr lh150 mt10 ">
                        This Limited Time Additional $50 Coupon Will Expires As Soon As Timer Hits Zero So Take Action Now.
                     </div>
                  </div>
               </div>
               <div class="col-12 col-md-10 mx-auto mt30">
                  <div class="step-shape">
                     <div class="f-20 f-md-22 w600 white-clr lh150">
                        Reason #4
                     </div>
                     <div class="f-18 f-md-18 w400 white-clr lh150 mt10 ">
                        Get Priority Support From Our Dedicated Support Team to Get Assured Success.
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!----Second Section---->
      <div class="riskfree-section">
         <div class="container ">
            <div class="row align-items-center ">
               <div class="f-28 f-md-45 w700 white-clr text-center col-12 mb-md40">Test Drive CloudFusion Risk FREE For 30 Days
               </div>
               <div class="col-md-7 col-12 order-md-2">
                  <div class="f-md-18 f-18 w400 lh150 white-clr mt15 "><span class="w600">Your purchase is secured with our 30-day 100% money back guarantee.</span>
                     <br><br>
                     <span class="w600 ">So, you can buy with full confidence and try it for yourself and for your client’s risk free.</span> If you face any Issue or don’t get results you desired after using it, just raise a ticket on support desk
                     within 30 days and we'll refund you everything, down to the last penny.<br><br> We would like to clearly state that we do NOT offer a “no questions asked” money back guarantee. You must provide a genuine reason when you contact
                     our support and we will refund your hard-earned money.
                  </div>
               </div>
               <div class="col-md-5 order-md-1 col-12 mt20 mt-md0 ">
                  <img src="assets/images/riskfree-img.webp " class="img-fluid d-block mx-auto " alt="Risk Free">
               </div>
            </div>
         </div>
      </div>
      <!--3. Third Section Start -->
      <div class="section2">
         <div class="container">
            <div class="row ">
               <div class="col-12 px0 px-md15" id="buybundle">
                  <div class="f-md-45 f-28 f  text-center w700 lh140">
                     Limited Time Offer!
                  </div>
                  <div class="f-22 f-md-32 w700 lh150 text-center mt20 black-clr">
                     Get Complete Package of All <span class="w700">CloudFusion</span> Products<br class="d-md-block d-none"> for A Low One-Time Fee
                  </div>
               </div>
               <div class="col-12 f-22 f-md-22 w400 lh150 text-center mt20 black-clr">
                  Use Coupon Code <span class="orange-clr w700 ">"CloudFusion"</span> for an Additional <span class="orange-clr w700 "> $50</span> Discount
               </div>
            </div>
            <div class="row justify-content-center">
               <div class="col-md-6 col-12 mt25 mt-md50 px-md15">
                  <div class="tablebox1">
                     <div class="col-12 tablebox-inner-bg text-center">
                        <div class="relative col-12 p0">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 926.8 148.17" style="max-height:50px">
                              <defs>
                                 <style>
                                    .cls-1{fill:#0068ff;}
                                    .cls-2{fill:#f89b1c;}
                                    .cls-3{fill:#fff;}
                                 </style>
                              </defs>
                              <g id="Layer_2" data-name="Layer 2">
                              <g id="Layer_1-2" data-name="Layer 1">
                              <path class="cls-1" d="M41.41,108.34a22.49,22.49,0,0,1-2.87-11c0-12.08,9.69-22.18,22.05-23l16.06-1.06L72.4,58.17a35,35,0,0,1-1.31-9.5c0-19.78,16.54-35.86,36.86-35.86a37.33,37.33,0,0,1,24.89,9.41,35.39,35.39,0,0,1,11.81,23.06l.59,6.21c4.11-3.79,8.07-7.79,11.84-12C152.67,17,132.35,0,108,0c-27.63,0-50,21.79-50,48.67A47.76,47.76,0,0,0,59.7,61.56c-19.17,1.26-34.33,16.8-34.33,35.77a35,35,0,0,0,3.28,14.81C32.55,111.06,36.83,109.79,41.41,108.34Z"></path>
                              <path class="cls-2" d="M165.9,50.66c10.88-20.3,14.58-35.47,14.58-35.47C142.66,114.85,2.87,139.07,0,140H163.24c25.39,0,46-20,46-44.71C209.2,71.47,190,52,165.9,50.66Zm-2.66,76.53H91.94a188.75,188.75,0,0,0,66.3-63.45l1.07-.12a34.8,34.8,0,0,1,3.93-.23c18.09,0,32.8,14.31,32.8,31.9S181.33,127.19,163.24,127.19Z"></path><text></text>
                              <path class="cls-3" d="M313.48,73.63H295.09A18.17,18.17,0,0,0,293,67.27a16.13,16.13,0,0,0-4-4.77,17.68,17.68,0,0,0-5.6-3,21.68,21.68,0,0,0-6.86-1A20.48,20.48,0,0,0,265,61.73a21.33,21.33,0,0,0-7.68,9.57,38.07,38.07,0,0,0-2.73,15.22A38.43,38.43,0,0,0,257.29,102a19.88,19.88,0,0,0,19.16,12.64,22.63,22.63,0,0,0,6.78-1,17.28,17.28,0,0,0,5.56-2.83,15.92,15.92,0,0,0,4-4.55,18.16,18.16,0,0,0,2.25-6.13l18.39.08a33.44,33.44,0,0,1-3.55,11.4,34.43,34.43,0,0,1-7.6,9.78,35.64,35.64,0,0,1-11.33,6.8,41.64,41.64,0,0,1-14.84,2.5,40.45,40.45,0,0,1-20.55-5.21,36.72,36.72,0,0,1-14.27-15.07q-5.24-9.87-5.23-23.88t5.29-23.93a37,37,0,0,1,14.36-15.05,40.29,40.29,0,0,1,20.4-5.18A44.26,44.26,0,0,1,290,44.45a35.54,35.54,0,0,1,11.36,6.11,32.24,32.24,0,0,1,8.08,9.8A36.94,36.94,0,0,1,313.48,73.63Z"></path>
                              <path class="cls-3" d="M344.08,43.53v86H326.19v-86Z"></path>
                              <path class="cls-3" d="M387.48,130.76a32.77,32.77,0,0,1-16.9-4.18,28.15,28.15,0,0,1-11-11.67,37.37,37.37,0,0,1-3.87-17.4A37.59,37.59,0,0,1,359.61,80a28.15,28.15,0,0,1,11-11.67,36.25,36.25,0,0,1,33.8,0,28.15,28.15,0,0,1,11,11.67,37.59,37.59,0,0,1,3.86,17.48,37.37,37.37,0,0,1-3.86,17.4,28.15,28.15,0,0,1-11,11.67A32.77,32.77,0,0,1,387.48,130.76Zm.08-13.85a11.05,11.05,0,0,0,7.43-2.54,15.66,15.66,0,0,0,4.52-7,33.67,33.67,0,0,0,0-20.07,15.81,15.81,0,0,0-4.52-7,11,11,0,0,0-7.43-2.56A11.29,11.29,0,0,0,380,80.39a15.48,15.48,0,0,0-4.58,7,33.67,33.67,0,0,0,0,20.07,15.34,15.34,0,0,0,4.58,7A11.38,11.38,0,0,0,387.56,116.91Z"></path>
                              <path class="cls-3" d="M472.23,102.05V65h17.88V129.5H473V117.79h-.68A19.08,19.08,0,0,1,465,126.9a21.41,21.41,0,0,1-12.32,3.44,21.69,21.69,0,0,1-11.37-2.94,20.09,20.09,0,0,1-7.67-8.35,28.77,28.77,0,0,1-2.79-13V65h17.89v37.87c0,3.8,1.05,6.81,3.06,9a10.41,10.41,0,0,0,8.1,3.32,12.78,12.78,0,0,0,6.05-1.49,11.58,11.58,0,0,0,4.55-4.43A13.75,13.75,0,0,0,472.23,102.05Z"></path>
                              <path class="cls-3" d="M528.14,130.55a24.16,24.16,0,0,1-13.28-3.8,25.78,25.78,0,0,1-9.4-11.21Q502,108.13,502,97.35q0-11.09,3.57-18.45a25.73,25.73,0,0,1,9.51-11,24.29,24.29,0,0,1,13-3.68,20,20,0,0,1,9,1.83A18.15,18.15,0,0,1,543,70.54a24.43,24.43,0,0,1,3.42,5.31H547V43.53h17.84v86H547.2V119.17h-.75a23.05,23.05,0,0,1-3.55,5.32,18.09,18.09,0,0,1-5.92,4.34A20.5,20.5,0,0,1,528.14,130.55Zm5.67-14.23a11.47,11.47,0,0,0,7.33-2.37,14.89,14.89,0,0,0,4.61-6.66,28.24,28.24,0,0,0,1.62-10,28.22,28.22,0,0,0-1.6-10,14.33,14.33,0,0,0-4.61-6.55,11.77,11.77,0,0,0-7.35-2.31,11.58,11.58,0,0,0-7.43,2.4,14.57,14.57,0,0,0-4.57,6.63,28.38,28.38,0,0,0-1.56,9.82,28.82,28.82,0,0,0,1.58,9.93,15,15,0,0,0,4.57,6.72A11.44,11.44,0,0,0,533.81,116.32Z"></path>
                              <path class="cls-3" d="M582.76,129.5v-86H634.3v9.24H593.17v29h37.27v9.24H593.17V129.5Z"></path>
                              <path class="cls-3" d="M687.36,103.14V65h9.91V129.5h-9.91V118.59h-.67a21,21,0,0,1-7.05,8.33q-4.78,3.42-12.09,3.42a21.41,21.41,0,0,1-10.75-2.67,18.33,18.33,0,0,1-7.38-8q-2.7-5.4-2.69-13.63V65h9.91v40.3q0,7.05,4,11.25a13.35,13.35,0,0,0,10.14,4.2,17,17,0,0,0,7.54-1.89,16.58,16.58,0,0,0,6.46-5.79A17.49,17.49,0,0,0,687.36,103.14Z"></path>
                              <path class="cls-3" d="M761.08,79.46,752.18,82a17.11,17.11,0,0,0-2.46-4.34,12.34,12.34,0,0,0-4.39-3.51,15.87,15.87,0,0,0-7.09-1.38,16.94,16.94,0,0,0-9.84,2.7,8.09,8.09,0,0,0-3.93,6.87,7.05,7.05,0,0,0,2.69,5.83q2.69,2.14,8.39,3.57l9.57,2.35q8.65,2.1,12.89,6.4a15.09,15.09,0,0,1,4.24,11.06,16.46,16.46,0,0,1-3.17,9.91,21.35,21.35,0,0,1-8.83,6.89,32.4,32.4,0,0,1-13.19,2.51q-9.85,0-16.33-4.28a19,19,0,0,1-8.18-12.51l9.4-2.35a13.09,13.09,0,0,0,5.1,7.81q3.76,2.59,9.85,2.6,6.92,0,11-3c2.72-2,4.09-4.34,4.09-7.11a7.61,7.61,0,0,0-2.35-5.65,15.26,15.26,0,0,0-7.22-3.42l-10.75-2.52q-8.85-2.1-13-6.53a15.61,15.61,0,0,1-4.14-11.1,15.82,15.82,0,0,1,3.09-9.65,20.63,20.63,0,0,1,8.44-6.59,29.41,29.41,0,0,1,12.15-2.4q9.57,0,15,4.2A22.7,22.7,0,0,1,761.08,79.46Z"></path>
                              <path class="cls-3" d="M782.23,54.28a6.94,6.94,0,0,1-5-2,6.44,6.44,0,0,1,0-9.48,7.24,7.24,0,0,1,10,0,6.46,6.46,0,0,1,0,9.48A7,7,0,0,1,782.23,54.28Zm-5,75.22V65h9.91V129.5Z"></path>
                              <path class="cls-3" d="M831.43,130.84a27.22,27.22,0,0,1-25.54-15.78,39.11,39.11,0,0,1-3.68-17.46A39.59,39.59,0,0,1,805.89,80,28.25,28.25,0,0,1,857,80a39.59,39.59,0,0,1,3.68,17.59A39.11,39.11,0,0,1,857,115.06a27.22,27.22,0,0,1-25.54,15.78Zm0-8.89a17,17,0,0,0,10.91-3.41,19.92,19.92,0,0,0,6.34-8.94,34.21,34.21,0,0,0,2.06-12,34.62,34.62,0,0,0-2.06-12,20.31,20.31,0,0,0-6.34-9,19,19,0,0,0-21.83,0,20.37,20.37,0,0,0-6.33,9,34.39,34.39,0,0,0-2.06,12,34,34,0,0,0,2.06,12,20,20,0,0,0,6.33,8.94A17,17,0,0,0,831.43,122Z"></path>
                              <path class="cls-3" d="M885.66,90.71V129.5h-9.9V65h9.57V75.1h.84a18.47,18.47,0,0,1,6.88-7.91q4.62-3,11.92-3a23.6,23.6,0,0,1,11.46,2.67,18.21,18.21,0,0,1,7.64,8.06c1.82,3.59,2.73,8.14,2.73,13.62v41h-9.9V89.2q0-7.59-3.95-11.86t-10.83-4.26a17.2,17.2,0,0,0-8.46,2.06,14.67,14.67,0,0,0-5.86,6A19.9,19.9,0,0,0,885.66,90.71Z"></path>
                              <text></text>
                           </g>
                        </g>
                        </svg>
                        </div>
                        <div class="col-12 mt-md40 mt20 table-headline-shape">
                           <div class="w700 f-md-32 f-22 text-center text-uppercase black-clr lh150 ">
                              Bundle Offer
                           </div>
                        </div>
                        <!-- PLAN 1 START -->
                        <div class="row mx-0 odd-shape mt-md40 mt20">
                           <div class="col-md-9 col-12 px-md15 px0">
                              <div class="w600 f-md-28 f-22 text-left text-xs-center text-uppercase white-clr lh150 mt8">
                                 Commercial
                              </div>
                           </div>
                           <div class="col-md-3 col-12 px-md15 px0">
                              <div class="col-12 price-shape">
                                 <div class="w600 f-md-35 f-22 text-center black-clr lh150">
                                    $97
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- PLAN 1 END -->
                        <!-- PLAN 2 START -->
                        <div class="row mx-0 even-shape">
                           <div class="col-md-9 col-12 px-md15 px0">
                              <div class="w600 f-md-28 f-22 text-left text-xs-center text-uppercase white-clr lh150 mt8">
                                 ELITE
                              </div>
                           </div>
                           <div class="col-md-3 col-12 px-md15 px0">
                              <div class="col-12 price-shape">
                                 <div class="w600 f-md-35 f-22 text-center black-clr lh150">
                                    $297
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- PLAN 2 END -->
                        <!-- PLAN 3 START -->
                        <div class="row mx-0 odd-shape">
                           <div class="col-md-9 col-12 px-md15 px0">
                              <div class="w600 f-md-28 f-22 text-left text-xs-center text-uppercase white-clr lh150 mt8">
                                 ENTERPRISE
                              </div>
                           </div>
                           <div class="col-md-3 col-12 px-md15 px0">
                              <div class="col-12 price-shape">
                                 <div class="w600 f-md-35 f-22 text-center black-clr lh150">
                                    $97
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- PLAN 3 END -->
                        <!-- PLAN 4 START -->
                        <div class="row mx-0 even-shape">
                           <div class="col-md-9 col-12 px-md15 px0">
                              <div class="w600 f-md-28 f-22 text-left text-xs-center text-uppercase white-clr lh150 mt8">
                                 Agency
                              </div>
                           </div>
                           <div class="col-md-3 col-12 px-md15 px0">
                              <div class="col-12 price-shape">
                                 <div class="w600 f-md-35 f-22 text-center black-clr lh150">
                                    $197
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- PLAN 4 END -->
                        <!-- PLAN 5 START -->
                        <div class="row mx-0 odd-shape">
                           <div class="col-md-9 col-12 px-md15 px0">
                              <div class="w600 f-md-28 f-22 text-left text-xs-center text-uppercase white-clr lh150 mt8">
                                 FAST ACTION BONUSES
                              </div>
                           </div>
                           <div class="col-md-3 col-12 px-md15 px0">
                              <div class="col-12 price-shape">
                                 <div class="w600 f-md-35 f-22 text-center black-clr lh150">
                                    $309
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- PLAN 5 END -->
                        <div class="col-12 px-md0 px15 mt-md30 mt20 w500 f-md-35 f-22 white-clr text-center">
                           Regular <strike>$997</strike>
                        </div>
                        <div class="row justify-content-center mx-0 ">
                           <div class="col-md-8 col-12 now-price mt20 w700 f-md-60 f-32 white-clr text-center">
                              Now $297
                           </div>
                        </div>
                        <div class="col-12 px-md0 px15 mt20 w500 f-md-25 f-20 white-clr text-center">
                           One Time Fee
                        </div>
                        <div class="mt20 red-clr w600 f-22 d-md-27 lh140">
                           <img src="assets/images/red-close.webp" alt="Hurry" class="mr10 close-img">
                              No Recurring | No Monthly | No Yearly
                           </div>
                        <div class="row justify-content-center mx-0 ">
                           <div class="myfeatures f-md-25 f-16 w400 text-center lh160">
                              <div class="hideme-button">
                                 <a href="https://www.jvzoo.com/b/109507/398672/2"><img src="https://i.jvzoo.com/109507/398672/2" alt="CloudFusion Bundle" border="0" class="img-fluid mx-auto d-block"/></a> 
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-12 mt-md50 mt20">
                  <div class="f-md-36 f-24 black-clr  text-center w700 lh140">
                     We are always here by your side <br class="d-none d-md-block">
                     <span class="red-clr">Get in touch with us …</span>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--3. Third Section End -->
      <!--Footer Section Start -->
      <div class="footer-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 926.8 148.17" style="max-height:50px">
                     <defs>
                        <style>
                           .cls-1{fill:#0068ff;}
                           .cls-2{fill:#f89b1c;}
                           .cls-3{fill:#fff;}
                        </style>
                     </defs>
                     <g id="Layer_2" data-name="Layer 2">
                     <g id="Layer_1-2" data-name="Layer 1">
                     <path class="cls-1" d="M41.41,108.34a22.49,22.49,0,0,1-2.87-11c0-12.08,9.69-22.18,22.05-23l16.06-1.06L72.4,58.17a35,35,0,0,1-1.31-9.5c0-19.78,16.54-35.86,36.86-35.86a37.33,37.33,0,0,1,24.89,9.41,35.39,35.39,0,0,1,11.81,23.06l.59,6.21c4.11-3.79,8.07-7.79,11.84-12C152.67,17,132.35,0,108,0c-27.63,0-50,21.79-50,48.67A47.76,47.76,0,0,0,59.7,61.56c-19.17,1.26-34.33,16.8-34.33,35.77a35,35,0,0,0,3.28,14.81C32.55,111.06,36.83,109.79,41.41,108.34Z"></path>
                     <path class="cls-2" d="M165.9,50.66c10.88-20.3,14.58-35.47,14.58-35.47C142.66,114.85,2.87,139.07,0,140H163.24c25.39,0,46-20,46-44.71C209.2,71.47,190,52,165.9,50.66Zm-2.66,76.53H91.94a188.75,188.75,0,0,0,66.3-63.45l1.07-.12a34.8,34.8,0,0,1,3.93-.23c18.09,0,32.8,14.31,32.8,31.9S181.33,127.19,163.24,127.19Z"></path><text></text>
                     <path class="cls-3" d="M313.48,73.63H295.09A18.17,18.17,0,0,0,293,67.27a16.13,16.13,0,0,0-4-4.77,17.68,17.68,0,0,0-5.6-3,21.68,21.68,0,0,0-6.86-1A20.48,20.48,0,0,0,265,61.73a21.33,21.33,0,0,0-7.68,9.57,38.07,38.07,0,0,0-2.73,15.22A38.43,38.43,0,0,0,257.29,102a19.88,19.88,0,0,0,19.16,12.64,22.63,22.63,0,0,0,6.78-1,17.28,17.28,0,0,0,5.56-2.83,15.92,15.92,0,0,0,4-4.55,18.16,18.16,0,0,0,2.25-6.13l18.39.08a33.44,33.44,0,0,1-3.55,11.4,34.43,34.43,0,0,1-7.6,9.78,35.64,35.64,0,0,1-11.33,6.8,41.64,41.64,0,0,1-14.84,2.5,40.45,40.45,0,0,1-20.55-5.21,36.72,36.72,0,0,1-14.27-15.07q-5.24-9.87-5.23-23.88t5.29-23.93a37,37,0,0,1,14.36-15.05,40.29,40.29,0,0,1,20.4-5.18A44.26,44.26,0,0,1,290,44.45a35.54,35.54,0,0,1,11.36,6.11,32.24,32.24,0,0,1,8.08,9.8A36.94,36.94,0,0,1,313.48,73.63Z"></path>
                     <path class="cls-3" d="M344.08,43.53v86H326.19v-86Z"></path>
                     <path class="cls-3" d="M387.48,130.76a32.77,32.77,0,0,1-16.9-4.18,28.15,28.15,0,0,1-11-11.67,37.37,37.37,0,0,1-3.87-17.4A37.59,37.59,0,0,1,359.61,80a28.15,28.15,0,0,1,11-11.67,36.25,36.25,0,0,1,33.8,0,28.15,28.15,0,0,1,11,11.67,37.59,37.59,0,0,1,3.86,17.48,37.37,37.37,0,0,1-3.86,17.4,28.15,28.15,0,0,1-11,11.67A32.77,32.77,0,0,1,387.48,130.76Zm.08-13.85a11.05,11.05,0,0,0,7.43-2.54,15.66,15.66,0,0,0,4.52-7,33.67,33.67,0,0,0,0-20.07,15.81,15.81,0,0,0-4.52-7,11,11,0,0,0-7.43-2.56A11.29,11.29,0,0,0,380,80.39a15.48,15.48,0,0,0-4.58,7,33.67,33.67,0,0,0,0,20.07,15.34,15.34,0,0,0,4.58,7A11.38,11.38,0,0,0,387.56,116.91Z"></path>
                     <path class="cls-3" d="M472.23,102.05V65h17.88V129.5H473V117.79h-.68A19.08,19.08,0,0,1,465,126.9a21.41,21.41,0,0,1-12.32,3.44,21.69,21.69,0,0,1-11.37-2.94,20.09,20.09,0,0,1-7.67-8.35,28.77,28.77,0,0,1-2.79-13V65h17.89v37.87c0,3.8,1.05,6.81,3.06,9a10.41,10.41,0,0,0,8.1,3.32,12.78,12.78,0,0,0,6.05-1.49,11.58,11.58,0,0,0,4.55-4.43A13.75,13.75,0,0,0,472.23,102.05Z"></path>
                     <path class="cls-3" d="M528.14,130.55a24.16,24.16,0,0,1-13.28-3.8,25.78,25.78,0,0,1-9.4-11.21Q502,108.13,502,97.35q0-11.09,3.57-18.45a25.73,25.73,0,0,1,9.51-11,24.29,24.29,0,0,1,13-3.68,20,20,0,0,1,9,1.83A18.15,18.15,0,0,1,543,70.54a24.43,24.43,0,0,1,3.42,5.31H547V43.53h17.84v86H547.2V119.17h-.75a23.05,23.05,0,0,1-3.55,5.32,18.09,18.09,0,0,1-5.92,4.34A20.5,20.5,0,0,1,528.14,130.55Zm5.67-14.23a11.47,11.47,0,0,0,7.33-2.37,14.89,14.89,0,0,0,4.61-6.66,28.24,28.24,0,0,0,1.62-10,28.22,28.22,0,0,0-1.6-10,14.33,14.33,0,0,0-4.61-6.55,11.77,11.77,0,0,0-7.35-2.31,11.58,11.58,0,0,0-7.43,2.4,14.57,14.57,0,0,0-4.57,6.63,28.38,28.38,0,0,0-1.56,9.82,28.82,28.82,0,0,0,1.58,9.93,15,15,0,0,0,4.57,6.72A11.44,11.44,0,0,0,533.81,116.32Z"></path>
                     <path class="cls-3" d="M582.76,129.5v-86H634.3v9.24H593.17v29h37.27v9.24H593.17V129.5Z"></path>
                     <path class="cls-3" d="M687.36,103.14V65h9.91V129.5h-9.91V118.59h-.67a21,21,0,0,1-7.05,8.33q-4.78,3.42-12.09,3.42a21.41,21.41,0,0,1-10.75-2.67,18.33,18.33,0,0,1-7.38-8q-2.7-5.4-2.69-13.63V65h9.91v40.3q0,7.05,4,11.25a13.35,13.35,0,0,0,10.14,4.2,17,17,0,0,0,7.54-1.89,16.58,16.58,0,0,0,6.46-5.79A17.49,17.49,0,0,0,687.36,103.14Z"></path>
                     <path class="cls-3" d="M761.08,79.46,752.18,82a17.11,17.11,0,0,0-2.46-4.34,12.34,12.34,0,0,0-4.39-3.51,15.87,15.87,0,0,0-7.09-1.38,16.94,16.94,0,0,0-9.84,2.7,8.09,8.09,0,0,0-3.93,6.87,7.05,7.05,0,0,0,2.69,5.83q2.69,2.14,8.39,3.57l9.57,2.35q8.65,2.1,12.89,6.4a15.09,15.09,0,0,1,4.24,11.06,16.46,16.46,0,0,1-3.17,9.91,21.35,21.35,0,0,1-8.83,6.89,32.4,32.4,0,0,1-13.19,2.51q-9.85,0-16.33-4.28a19,19,0,0,1-8.18-12.51l9.4-2.35a13.09,13.09,0,0,0,5.1,7.81q3.76,2.59,9.85,2.6,6.92,0,11-3c2.72-2,4.09-4.34,4.09-7.11a7.61,7.61,0,0,0-2.35-5.65,15.26,15.26,0,0,0-7.22-3.42l-10.75-2.52q-8.85-2.1-13-6.53a15.61,15.61,0,0,1-4.14-11.1,15.82,15.82,0,0,1,3.09-9.65,20.63,20.63,0,0,1,8.44-6.59,29.41,29.41,0,0,1,12.15-2.4q9.57,0,15,4.2A22.7,22.7,0,0,1,761.08,79.46Z"></path>
                     <path class="cls-3" d="M782.23,54.28a6.94,6.94,0,0,1-5-2,6.44,6.44,0,0,1,0-9.48,7.24,7.24,0,0,1,10,0,6.46,6.46,0,0,1,0,9.48A7,7,0,0,1,782.23,54.28Zm-5,75.22V65h9.91V129.5Z"></path>
                     <path class="cls-3" d="M831.43,130.84a27.22,27.22,0,0,1-25.54-15.78,39.11,39.11,0,0,1-3.68-17.46A39.59,39.59,0,0,1,805.89,80,28.25,28.25,0,0,1,857,80a39.59,39.59,0,0,1,3.68,17.59A39.11,39.11,0,0,1,857,115.06a27.22,27.22,0,0,1-25.54,15.78Zm0-8.89a17,17,0,0,0,10.91-3.41,19.92,19.92,0,0,0,6.34-8.94,34.21,34.21,0,0,0,2.06-12,34.62,34.62,0,0,0-2.06-12,20.31,20.31,0,0,0-6.34-9,19,19,0,0,0-21.83,0,20.37,20.37,0,0,0-6.33,9,34.39,34.39,0,0,0-2.06,12,34,34,0,0,0,2.06,12,20,20,0,0,0,6.33,8.94A17,17,0,0,0,831.43,122Z"></path>
                     <path class="cls-3" d="M885.66,90.71V129.5h-9.9V65h9.57V75.1h.84a18.47,18.47,0,0,1,6.88-7.91q4.62-3,11.92-3a23.6,23.6,0,0,1,11.46,2.67,18.21,18.21,0,0,1,7.64,8.06c1.82,3.59,2.73,8.14,2.73,13.62v41h-9.9V89.2q0-7.59-3.95-11.86t-10.83-4.26a17.2,17.2,0,0,0-8.46,2.06,14.67,14.67,0,0,0-5.86,6A19.9,19.9,0,0,0,885.66,90.71Z"></path>
                     <text></text>
                  </g>
               </g>
               </svg>
                  
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-16 f-md-18 w400 lh140 white-clr text-xs-center">Copyright © CloudFusion</div>
                  <ul class="footer-ul w400 f-16 f-md-18 white-clr text-center text-md-right">
                     <li><a href="https://support.oppyo.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://getcloudfusion.co/legal/privacy-policy.html" class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://getcloudfusion.co/legal/terms-of-service.html" class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://getcloudfusion.co/legal/disclaimer.html" class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://getcloudfusion.co/legal/gdpr.html" class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://getcloudfusion.co/legal/dmca.html" class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://getcloudfusion.co/legal/anti-spam.html" class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!--Footer Section End -->
   
<!-- timer --->
<script type="text/javascript">
         var noob = $('.countdown').length;
         var stimeInSecs;
         var tickers;

         function timerBegin(seconds) {     
            stimeInSecs = parseInt(seconds);           
            showRemaining();
            //tickers = setInterval("showRemaining()", 1000);
         }

         function showRemaining() {

            var seconds = stimeInSecs;
               
            if (seconds > 0) {         
               stimeInSecs--;       
            } else {        
               clearInterval(tickers);       
               //timerBegin(59 * 60); 
            }

            var days = Math.floor(seconds / 86400);
      
            seconds %= 86400;
            
            var hours = Math.floor(seconds / 3600);
            
            seconds %= 3600;
            
            var minutes = Math.floor(seconds / 60);
            
            seconds %= 60;


            if (days < 10) {
               days = "0" + days;
            }
            if (hours < 10) {
               hours = "0" + hours;
            }
            if (minutes < 10) {
               minutes = "0" + minutes;
            }
            if (seconds < 10) {
               seconds = "0" + seconds;
            }
            var i;
            var countdown = document.getElementsByClassName('countdown');

            for (i = 0; i < noob; i++) {
               countdown[i].innerHTML = '';
            
               if (days) {
                  countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-16 f-md-24 timerbg oswald">' + days + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-12 f-md-15 w500 smmltd">Days</span> </div>';
               }
            
               countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-16 f-md-24 timerbg oswald">' + hours + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-12 f-md-15 w500 smmltd">Hours</span> </div>';
            
               countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"><span class="f-16 f-md-24 timerbg oswald">' + minutes + '&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-12 f-md-15 w500 smmltd">Mins</span> </div>';
            
               countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-16 f-md-24 timerbg oswald">' + seconds + '</span><br><span class="f-12 f-md-15 w500 smmltd">Sec</span> </div>';
            }
         
         }


      //timerBegin(59 * 60); 
      function getCookie(cname) {
            var name = cname + "=";
            var decodedCookie = decodeURIComponent(document.cookie);
            var ca = decodedCookie.split(';');
            for(var i = 0; i <ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        }
      var cnt = 60*60;
      function counter(){
         if(getCookie("cnt") > 0){
            cnt = getCookie("cnt");
         }
         cnt -= 1;
         document.cookie = "cnt="+ cnt;
         timerBegin(getCookie("cnt"));
         //jQuery("#counter").val(getCookie("cnt"));
         if(cnt>0){
            setTimeout(counter,1000);
        }
      }
      counter();
</script>
<!--- timer end-->

  <!-- Swiper JS -->
  <script src="https://cdn.jsdelivr.net/npm/swiper@10/swiper-bundle.min.js"></script>
              <!-- Initialize Swiper -->
  <script>
    var swiper = new Swiper(".mySwiper", {
      slidesPerView: 5,
      spaceBetween: 10,
      autoplay: {
        delay: 2500,
        disableOnInteraction: false,
      },
      pagination: {
        el: ".swiper-pagination",
        clickable: true,
      },
      breakpoints: {
        640: {
          slidesPerView: 2,
          spaceBetween: 20,
        },
        768: {
          slidesPerView: 4,
          spaceBetween: 30,
        },
        1024: {
          slidesPerView: 5,
          spaceBetween: 30,
        },
        1320:{
            slidesPerView: 6,
            spaceBetween: 30,
        },
        1500:{
            slidesPerView: 7,
            spaceBetween: 30,
        },
        1900:{
            slidesPerView: 8,
            spaceBetween: 30,
        },
      },
    });
  </script>    
  <script>
    var swiper = new Swiper(".mySwiper.bio-card", {
      slidesPerView: 5,
      spaceBetween: 10,
      autoplay: {
        delay: 2500,
        disableOnInteraction: false,
      },
      pagination: {
        el: ".swiper-pagination",
        clickable: true,
      },
      breakpoints: {
        640: {
          slidesPerView: 2,
          spaceBetween: 20,
        },
        768: {
          slidesPerView: 2,
          spaceBetween: 30,
        },
        1024: {
          slidesPerView: 3,
          spaceBetween: 30,
        },
      },
    });
  </script> 
   </body>
</html>
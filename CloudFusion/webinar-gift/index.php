<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<!-- Tell the browser to be responsive to screen width -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="title" content="CloudFusion Webinar Gift">
    <meta name="description" content="CloudFusion Webinar Gift">
    <meta name="keywords" content="Supercharge Your Website and Boost Engagement, Conversions, and Sales to Make All-Time Big Profits in Your Business">
    <meta property="og:image" content="https://www.getcloudfusion.co/webinar-gift/thumbnail.png">
    <meta name="language" content="English">
    <meta name="revisit-after" content="1 days">
    <meta name="author" content="Dr. Amit Pareek">
    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:title" content="CloudFusion Webinar Gift">
    <meta property="og:description" content="Supercharge Your Website and Boost Engagement, Conversions, and Sales to Make All-Time Big Profits in Your Business">
    <meta property="og:image" content="https://www.getcloudfusion.co/webinar-gift/thumbnail.png">
    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:title" content="CloudFusion Webinar Gift">
    <meta property="twitter:description" content="Supercharge Your Website and Boost Engagement, Conversions, and Sales to Make All-Time Big Profits in Your Business">
    <meta property="twitter:image" content="https://www.getcloudfusion.co/webinar-gift/thumbnail.png">

	<title>CloudFusion | Webinar Gift</title>
	<!-- Shortcut Icon  -->
	<link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Kalam:wght@300;400;700&family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
	<!-- Css CDN Load Link -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="../common_assets/css/bootstrap.min.css" type="text/css">
   	<link rel="stylesheet" type="text/css" href="assets/css/style.css">
   	<script src="https://cdn.oppyotest.com/launches/CloudFusion/common_assets/js/jquery.min.js"></script>

		
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-NMK2MBB');</script>
	<!-- End Google Tag Manager -->


</head>
<body>
	<a href="#" id="scroll" style="display: block;"><span></span></a>
	
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NMK2MBB"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) --> 

	<!-- New Timer  Start-->
	<?php
		$date = 'August 15 2023 11:00 AM EST';
		$exp_date = strtotime($date);
		$now = time();  
		/*
		
		$date = date('F d Y g:i:s A eO');
		$rand_time_add = rand(700, 1200);
		$exp_date = strtotime($date) + $rand_time_add;
		$now = time();*/
		
		if ($now < $exp_date) {
	?>
	<?php
		} else {
			echo "";
		}
	?>
	<!-- New Timer End -->

	<?php
		if(!isset($_GET['afflink'])){
		$_GET['afflink'] = 'https://getcloudfusion.co/special/';
		$_GET['name'] = 'Dr. Amit Pareek';      
		}
	?>

	<div class="header-sec" id="product">
        <div class="container">
            <div class="row">
               	<div class="col-12 text-center">
				   <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 926.8 148.17" style="max-height:55px"><defs><style>.cls-1{fill:#0068ff;}.cls-2{fill:#f89b1c;}.cls-3{fill:#fff;}</style></defs><title>Asset 12</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1" d="M41.41,108.34a22.49,22.49,0,0,1-2.87-11c0-12.08,9.69-22.18,22.05-23l16.06-1.06L72.4,58.17a35,35,0,0,1-1.31-9.5c0-19.78,16.54-35.86,36.86-35.86a37.33,37.33,0,0,1,24.89,9.41,35.39,35.39,0,0,1,11.81,23.06l.59,6.21c4.11-3.79,8.07-7.79,11.84-12C152.67,17,132.35,0,108,0c-27.63,0-50,21.79-50,48.67A47.76,47.76,0,0,0,59.7,61.56c-19.17,1.26-34.33,16.8-34.33,35.77a35,35,0,0,0,3.28,14.81C32.55,111.06,36.83,109.79,41.41,108.34Z"/><path class="cls-2" d="M165.9,50.66c10.88-20.3,14.58-35.47,14.58-35.47C142.66,114.85,2.87,139.07,0,140H163.24c25.39,0,46-20,46-44.71C209.2,71.47,190,52,165.9,50.66Zm-2.66,76.53H91.94a188.75,188.75,0,0,0,66.3-63.45l1.07-.12a34.8,34.8,0,0,1,3.93-.23c18.09,0,32.8,14.31,32.8,31.9S181.33,127.19,163.24,127.19Z"/><text/><path class="cls-3" d="M313.48,73.63H295.09A18.17,18.17,0,0,0,293,67.27a16.13,16.13,0,0,0-4-4.77,17.68,17.68,0,0,0-5.6-3,21.68,21.68,0,0,0-6.86-1A20.48,20.48,0,0,0,265,61.73a21.33,21.33,0,0,0-7.68,9.57,38.07,38.07,0,0,0-2.73,15.22A38.43,38.43,0,0,0,257.29,102a19.88,19.88,0,0,0,19.16,12.64,22.63,22.63,0,0,0,6.78-1,17.28,17.28,0,0,0,5.56-2.83,15.92,15.92,0,0,0,4-4.55,18.16,18.16,0,0,0,2.25-6.13l18.39.08a33.44,33.44,0,0,1-3.55,11.4,34.43,34.43,0,0,1-7.6,9.78,35.64,35.64,0,0,1-11.33,6.8,41.64,41.64,0,0,1-14.84,2.5,40.45,40.45,0,0,1-20.55-5.21,36.72,36.72,0,0,1-14.27-15.07q-5.24-9.87-5.23-23.88t5.29-23.93a37,37,0,0,1,14.36-15.05,40.29,40.29,0,0,1,20.4-5.18A44.26,44.26,0,0,1,290,44.45a35.54,35.54,0,0,1,11.36,6.11,32.24,32.24,0,0,1,8.08,9.8A36.94,36.94,0,0,1,313.48,73.63Z"/><path class="cls-3" d="M344.08,43.53v86H326.19v-86Z"/><path class="cls-3" d="M387.48,130.76a32.77,32.77,0,0,1-16.9-4.18,28.15,28.15,0,0,1-11-11.67,37.37,37.37,0,0,1-3.87-17.4A37.59,37.59,0,0,1,359.61,80a28.15,28.15,0,0,1,11-11.67,36.25,36.25,0,0,1,33.8,0,28.15,28.15,0,0,1,11,11.67,37.59,37.59,0,0,1,3.86,17.48,37.37,37.37,0,0,1-3.86,17.4,28.15,28.15,0,0,1-11,11.67A32.77,32.77,0,0,1,387.48,130.76Zm.08-13.85a11.05,11.05,0,0,0,7.43-2.54,15.66,15.66,0,0,0,4.52-7,33.67,33.67,0,0,0,0-20.07,15.81,15.81,0,0,0-4.52-7,11,11,0,0,0-7.43-2.56A11.29,11.29,0,0,0,380,80.39a15.48,15.48,0,0,0-4.58,7,33.67,33.67,0,0,0,0,20.07,15.34,15.34,0,0,0,4.58,7A11.38,11.38,0,0,0,387.56,116.91Z"/><path class="cls-3" d="M472.23,102.05V65h17.88V129.5H473V117.79h-.68A19.08,19.08,0,0,1,465,126.9a21.41,21.41,0,0,1-12.32,3.44,21.69,21.69,0,0,1-11.37-2.94,20.09,20.09,0,0,1-7.67-8.35,28.77,28.77,0,0,1-2.79-13V65h17.89v37.87c0,3.8,1.05,6.81,3.06,9a10.41,10.41,0,0,0,8.1,3.32,12.78,12.78,0,0,0,6.05-1.49,11.58,11.58,0,0,0,4.55-4.43A13.75,13.75,0,0,0,472.23,102.05Z"/><path class="cls-3" d="M528.14,130.55a24.16,24.16,0,0,1-13.28-3.8,25.78,25.78,0,0,1-9.4-11.21Q502,108.13,502,97.35q0-11.09,3.57-18.45a25.73,25.73,0,0,1,9.51-11,24.29,24.29,0,0,1,13-3.68,20,20,0,0,1,9,1.83A18.15,18.15,0,0,1,543,70.54a24.43,24.43,0,0,1,3.42,5.31H547V43.53h17.84v86H547.2V119.17h-.75a23.05,23.05,0,0,1-3.55,5.32,18.09,18.09,0,0,1-5.92,4.34A20.5,20.5,0,0,1,528.14,130.55Zm5.67-14.23a11.47,11.47,0,0,0,7.33-2.37,14.89,14.89,0,0,0,4.61-6.66,28.24,28.24,0,0,0,1.62-10,28.22,28.22,0,0,0-1.6-10,14.33,14.33,0,0,0-4.61-6.55,11.77,11.77,0,0,0-7.35-2.31,11.58,11.58,0,0,0-7.43,2.4,14.57,14.57,0,0,0-4.57,6.63,28.38,28.38,0,0,0-1.56,9.82,28.82,28.82,0,0,0,1.58,9.93,15,15,0,0,0,4.57,6.72A11.44,11.44,0,0,0,533.81,116.32Z"/><path class="cls-3" d="M582.76,129.5v-86H634.3v9.24H593.17v29h37.27v9.24H593.17V129.5Z"/><path class="cls-3" d="M687.36,103.14V65h9.91V129.5h-9.91V118.59h-.67a21,21,0,0,1-7.05,8.33q-4.78,3.42-12.09,3.42a21.41,21.41,0,0,1-10.75-2.67,18.33,18.33,0,0,1-7.38-8q-2.7-5.4-2.69-13.63V65h9.91v40.3q0,7.05,4,11.25a13.35,13.35,0,0,0,10.14,4.2,17,17,0,0,0,7.54-1.89,16.58,16.58,0,0,0,6.46-5.79A17.49,17.49,0,0,0,687.36,103.14Z"/><path class="cls-3" d="M761.08,79.46,752.18,82a17.11,17.11,0,0,0-2.46-4.34,12.34,12.34,0,0,0-4.39-3.51,15.87,15.87,0,0,0-7.09-1.38,16.94,16.94,0,0,0-9.84,2.7,8.09,8.09,0,0,0-3.93,6.87,7.05,7.05,0,0,0,2.69,5.83q2.69,2.14,8.39,3.57l9.57,2.35q8.65,2.1,12.89,6.4a15.09,15.09,0,0,1,4.24,11.06,16.46,16.46,0,0,1-3.17,9.91,21.35,21.35,0,0,1-8.83,6.89,32.4,32.4,0,0,1-13.19,2.51q-9.85,0-16.33-4.28a19,19,0,0,1-8.18-12.51l9.4-2.35a13.09,13.09,0,0,0,5.1,7.81q3.76,2.59,9.85,2.6,6.92,0,11-3c2.72-2,4.09-4.34,4.09-7.11a7.61,7.61,0,0,0-2.35-5.65,15.26,15.26,0,0,0-7.22-3.42l-10.75-2.52q-8.85-2.1-13-6.53a15.61,15.61,0,0,1-4.14-11.1,15.82,15.82,0,0,1,3.09-9.65,20.63,20.63,0,0,1,8.44-6.59,29.41,29.41,0,0,1,12.15-2.4q9.57,0,15,4.2A22.7,22.7,0,0,1,761.08,79.46Z"/><path class="cls-3" d="M782.23,54.28a6.94,6.94,0,0,1-5-2,6.44,6.44,0,0,1,0-9.48,7.24,7.24,0,0,1,10,0,6.46,6.46,0,0,1,0,9.48A7,7,0,0,1,782.23,54.28Zm-5,75.22V65h9.91V129.5Z"/><path class="cls-3" d="M831.43,130.84a27.22,27.22,0,0,1-25.54-15.78,39.11,39.11,0,0,1-3.68-17.46A39.59,39.59,0,0,1,805.89,80,28.25,28.25,0,0,1,857,80a39.59,39.59,0,0,1,3.68,17.59A39.11,39.11,0,0,1,857,115.06a27.22,27.22,0,0,1-25.54,15.78Zm0-8.89a17,17,0,0,0,10.91-3.41,19.92,19.92,0,0,0,6.34-8.94,34.21,34.21,0,0,0,2.06-12,34.62,34.62,0,0,0-2.06-12,20.31,20.31,0,0,0-6.34-9,19,19,0,0,0-21.83,0,20.37,20.37,0,0,0-6.33,9,34.39,34.39,0,0,0-2.06,12,34,34,0,0,0,2.06,12,20,20,0,0,0,6.33,8.94A17,17,0,0,0,831.43,122Z"/><path class="cls-3" d="M885.66,90.71V129.5h-9.9V65h9.57V75.1h.84a18.47,18.47,0,0,1,6.88-7.91q4.62-3,11.92-3a23.6,23.6,0,0,1,11.46,2.67,18.21,18.21,0,0,1,7.64,8.06c1.82,3.59,2.73,8.14,2.73,13.62v41h-9.9V89.2q0-7.59-3.95-11.86t-10.83-4.26a17.2,17.2,0,0,0-8.46,2.06,14.67,14.67,0,0,0-5.86,6A19.9,19.9,0,0,0,885.66,90.71Z"/><text/></g></g></svg>
               	</div>
               	<div class="col-12 mt20 mt-md30 relative">
                  <div class="mainheadline f-md-42 f-28 w400 text-center black-clr lh140">
                   <span class="w700 underline-text"> Host UNLIMITED Videos, Website Images, </span> <span class="w700 underline-text">Training, Audios or Any Media Files</span> at <br class="d-none d-md-block"> Blazing-Fast Speed at A Low One Time Fee
                  </div>
               </div>
               <div class="col-12 mt-md25 mt20 f-18 f-md-22 w500 text-center lh140 white-clr text-capitalize">
                  <div class="f-22 f-md-26 w500 lh140 white-clr text-capitalize">               
				  And…Supercharge Your Or Client’s Websites, Apps & Pages With Fast-Loading Media Content. NO Tech Hassles & Monthly Fee Ever…
                 </div>
               </div>
               <div class="col-12 mt-md30 mt20 text-center white-clr">
                   	<div class="f-md-36 f-28 lh160 w700 mt20 mt-md30">
				   		You've Earned It - <br class="d-none d-md-block">
						Enjoy This Assure Gift For Attending Our Webinar!
                   	</div>
                   	<div class="mt10 f-20 f-md-32 lh160">
                       <span class="tde">Download Your Assure Gifts Below</span>
                   	</div>
               	</div>
               	<div class="col-12 col-md-10 mx-auto mt20 mt-md30">
			   		<img src="assets/images/productbox.webp" alt="Product Box" class="mx-auto d-block img-fluid mt20 mt-md50">
               	</div>
            </div>

			<div class="row mt20 mt-md100 align-items-center">
               	<div class="col-md-6">
                  	<div class="f-22 f-md-28 lh140 w700 white-clr">
                     	<div class="option-design mr10 mr-md20">Bonus 1</div> 
                     	
                  	</div>
                     <div class="f-20 f-md-32 w600 lh140 white-clr mt20 mt-md30">
					 Evergreen Infographics Pack

                  	</div>
                  	
                  	<div class="f-20 f-md-22 w400 lh140 white-clr mt20">
					  <span class="w600">One of the most important types of visuals for a business owners in the online arena are infographics.</span>
					  <br><br>
					  Infographics are used to showcase statistics, to sell products, to advertise, promote, attract and connect better with your audience.
                  	</div>
                 	
					<div class="f-md-22 w700 lh160 d-btn mt20 mt-md30">
                       <a href="https://super-vip-bonuses.dotcompal.co/api/mydrive/docs/download_object?key=V2xoc1MyTkdjRVJUVkZwUFVrWkZkMVJ0Y0U1TmEzaEVVMjVrV2xkRk5UWmFSRWsxWlZad1JGTlVXa3BoVlc4MQ==" target="_blank">Download Now 🎁</a>
                    </div>
               	</div>
               	<div class="col-md-6 mt20 mt-md0">
                  	<img src="assets/images/bonus1.webp" alt="Bonus 1" class="mx-auto d-block img-fluid">
               	</div>
            </div>

			<div class="row mt20 mt-md100 align-items-center">
               	<div class="col-md-6 order-md-2">
                  	<div class="f-22 f-md-28 lh140 w700 white-clr">
                     	<div class="option-design mr10 mr-md20">Bonus 2</div> 
                     
                  	</div>
                  	<div class="f-20 f-md-32 w600 lh140 white-clr mt20 mt-md30">
					  The Complete Internet Marketing Strategy
                  	</div>
                 	<div class="f-20 f-md-22 w400 lh140 white-clr mt20">
                   <span class="w600">The internet can help you to live your dreams and to design the perfect lifestyle that will make you truly happy.</span>
				   <br><br>
				   As an internet marketer, you can make a truly ‘passive income’.
                  	</div>
					<div class="f-md-22 w700 lh160 d-btn mt20 mt-md30">
                       <a href="https://super-vip-bonuses.dotcompal.co/api/mydrive/docs/download_object?key=V2xoc1MyTkdjRVJUVkZwUFVrWkdObFJyVWtwbFZYaEVVMjVrV2xkRk5UWmFSRWsxWlZad1JGTlVXa3BoVlc4MQ==" target="_blank">Download Now 🎁</a>
                    </div>
               	</div>
               	<div class="col-md-6 mt20 mt-md0 order-md-1">
                  	<img src="assets/images/bonus2.webp" alt="Bonus 2" class="mx-auto d-block img-fluid">
               	</div>
            </div>

			<div class="row mt20 mt-md100 align-items-center">
               	<div class="col-md-6">
                  	<div class="f-22 f-md-28 lh140 w700 white-clr">
                     	<div class="option-design mr10 mr-md20">Bonus 3</div> <br>
                        
                  	</div>
                     <div class="f-20 f-md-32 w600 lh140 white-clr mt20 mt-md30">
					 Influencer Secrets Video Upgrade
                  	</div>
                  	
                 	<div class="f-20 f-md-22 w400 lh140 white-clr mt20">
                    <span class="w600">In order for you to be an influence master you need to know what influence really is.</span>
					<br><br>
					Credibility is everything with influence. You need to develop it and maintain it.
                           </div>
					<div class="f-md-22 w700 lh160 d-btn mt20 mt-md30">
                       <a href="https://super-vip-bonuses.dotcompal.co/api/mydrive/docs/download_object?key=V2xoc1MyTkdjRVJUVkZwUFVrWkdObFJyVWtaTlZYaEVVMjVrV2xkRk5UWmFSRWsxWlZad1JGTlVXa3BoVlc4MQ==" target="_blank">Download Now 🎁</a>
                    </div>
               	</div>
               	<div class="col-md-6 mt20 mt-md0">
                  	<img src="assets/images/bonus3.webp" alt="Bonus 3" class="mx-auto d-block img-fluid">
               	</div>
            </div>

			<div class="row mt20 mt-md100 align-items-center">
               	<div class="col-md-6 order-md-2">
                  	<div class="f-22 f-md-28 lh140 w700 white-clr">
                     	<div class="option-design mr10 mr-md20">Bonus 4</div> <br>
                     	
                  	</div>
                  	<div class="f-20 f-md-32 w600 lh140 white-clr mt20 mt-md30">
					  300 - DFY Logo Templates
                  	</div>
                 	<div class="f-20 f-md-22 w400 lh140 white-clr mt20">
						<span class="w600">300 Ready-Made Logo Designs - Great For Offline Clients!</span>
						<br><br>
						Building a successful online empire comes with the brand throughout your journey. That's why internet marketers and bloggers use the power of branding to get noticed and be remembered.
                  	</div>
					<div class="f-md-22 w700 lh160 d-btn mt20 mt-md30">
                       <a href="https://super-vip-bonuses.dotcompal.co/api/mydrive/docs/download_object?key=V2xoc1MyTkdjRVJUVkZwUFVrWkdObFJyVWtKTlJYaEVVMjVrV2xkRk5UWmFSRWsxWlZad1JGTlVXa3BoVlc4MQ==" target="_blank">Download Now 🎁</a>
                    </div>
               	</div>
               	<div class="col-md-6 mt20 mt-md0 order-md-1">
                  	<img src="assets/images/bonus4.webp" alt="Bonus 4" class="mx-auto d-block img-fluid">
               	</div>
            </div>

			<div class="row mt20 mt-md100 align-items-center">
               	<div class="col-md-6">
                  	<div class="f-22 f-md-28 lh140 w700 white-clr">
                     	<div class="option-design mr10 mr-md20">Bonus 5</div> <br>
                     	
                  	</div>
                     <div class="f-20 f-md-32 w600 lh140 white-clr mt20 mt-md30">
                     Traffic Machine
                  	</div>
                  	
                 	<div class="f-20 f-md-22 w600 lh140 white-clr mt20">
					 Are you a small business that has been struggling to increase the traffic to your website?<br><br>

Did you spend your valuable time building a website only to have it go unseen<br><br>

If your website isn’t bringing in the visitors like it should, then you may need to think your content marketing strategy.
                  	</div>
					<div class="f-md-22 w700 lh160 d-btn mt20 mt-md30">
                       <a href="https://super-vip-bonuses.dotcompal.co/api/mydrive/docs/download_object?key=V2xoc1MyTkdjRVJUVkZwUFVrWkdObFJyVWtabFZYaEVVMjVrV2xkRk5UWmFSRWsxWlZad1JGTlVXa3BoVlc4MQ==" target="_blank">Download Now 🎁</a>
                    </div>
               	</div>
               	<div class="col-md-6 mt20 mt-md0">
                  	<img src="assets/images/bonus5.webp" alt="Bonus 5" class="mx-auto d-block img-fluid">
               	</div>
            </div>

    	</div>
	</div>	
      
	<!-- Proudly Section End -->
 <!-- CTA Btn Start-->
 <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center mt20 mt-md20">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 yellow-clr">"Cloud"</span> for an Additional <span class="w700 yellow-clr">$10 Discount</span> on CloudFusion
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn px-md80">
                  <span class="text-center">Grab CloudFusion + 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                  Use Coupon Code <span class="w700 yellow-clr">"CloudFusion"</span> for an Additional <span class="w700 yellow-clr">$50 Discount</span> on Bundle Deal
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="https://getcloudfusion.co/bundle/" class="text-center bonusbuy-btn px-md80">
                  <span class="text-center">Grab CloudFusion Bundle + 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
            </div>
         </div>
      </div>
      <!-- CTA Btn End -->


	<!--Footer Section Start -->
	<div class="footer-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                   <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 926.8 148.17" style="max-height:55px"><defs><style>.cls-1{fill:#0068ff;}.cls-2{fill:#f89b1c;}.cls-3{fill:#fff;}</style></defs><title>Asset 12</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1" d="M41.41,108.34a22.49,22.49,0,0,1-2.87-11c0-12.08,9.69-22.18,22.05-23l16.06-1.06L72.4,58.17a35,35,0,0,1-1.31-9.5c0-19.78,16.54-35.86,36.86-35.86a37.33,37.33,0,0,1,24.89,9.41,35.39,35.39,0,0,1,11.81,23.06l.59,6.21c4.11-3.79,8.07-7.79,11.84-12C152.67,17,132.35,0,108,0c-27.63,0-50,21.79-50,48.67A47.76,47.76,0,0,0,59.7,61.56c-19.17,1.26-34.33,16.8-34.33,35.77a35,35,0,0,0,3.28,14.81C32.55,111.06,36.83,109.79,41.41,108.34Z"/><path class="cls-2" d="M165.9,50.66c10.88-20.3,14.58-35.47,14.58-35.47C142.66,114.85,2.87,139.07,0,140H163.24c25.39,0,46-20,46-44.71C209.2,71.47,190,52,165.9,50.66Zm-2.66,76.53H91.94a188.75,188.75,0,0,0,66.3-63.45l1.07-.12a34.8,34.8,0,0,1,3.93-.23c18.09,0,32.8,14.31,32.8,31.9S181.33,127.19,163.24,127.19Z"/><text/><path class="cls-3" d="M313.48,73.63H295.09A18.17,18.17,0,0,0,293,67.27a16.13,16.13,0,0,0-4-4.77,17.68,17.68,0,0,0-5.6-3,21.68,21.68,0,0,0-6.86-1A20.48,20.48,0,0,0,265,61.73a21.33,21.33,0,0,0-7.68,9.57,38.07,38.07,0,0,0-2.73,15.22A38.43,38.43,0,0,0,257.29,102a19.88,19.88,0,0,0,19.16,12.64,22.63,22.63,0,0,0,6.78-1,17.28,17.28,0,0,0,5.56-2.83,15.92,15.92,0,0,0,4-4.55,18.16,18.16,0,0,0,2.25-6.13l18.39.08a33.44,33.44,0,0,1-3.55,11.4,34.43,34.43,0,0,1-7.6,9.78,35.64,35.64,0,0,1-11.33,6.8,41.64,41.64,0,0,1-14.84,2.5,40.45,40.45,0,0,1-20.55-5.21,36.72,36.72,0,0,1-14.27-15.07q-5.24-9.87-5.23-23.88t5.29-23.93a37,37,0,0,1,14.36-15.05,40.29,40.29,0,0,1,20.4-5.18A44.26,44.26,0,0,1,290,44.45a35.54,35.54,0,0,1,11.36,6.11,32.24,32.24,0,0,1,8.08,9.8A36.94,36.94,0,0,1,313.48,73.63Z"/><path class="cls-3" d="M344.08,43.53v86H326.19v-86Z"/><path class="cls-3" d="M387.48,130.76a32.77,32.77,0,0,1-16.9-4.18,28.15,28.15,0,0,1-11-11.67,37.37,37.37,0,0,1-3.87-17.4A37.59,37.59,0,0,1,359.61,80a28.15,28.15,0,0,1,11-11.67,36.25,36.25,0,0,1,33.8,0,28.15,28.15,0,0,1,11,11.67,37.59,37.59,0,0,1,3.86,17.48,37.37,37.37,0,0,1-3.86,17.4,28.15,28.15,0,0,1-11,11.67A32.77,32.77,0,0,1,387.48,130.76Zm.08-13.85a11.05,11.05,0,0,0,7.43-2.54,15.66,15.66,0,0,0,4.52-7,33.67,33.67,0,0,0,0-20.07,15.81,15.81,0,0,0-4.52-7,11,11,0,0,0-7.43-2.56A11.29,11.29,0,0,0,380,80.39a15.48,15.48,0,0,0-4.58,7,33.67,33.67,0,0,0,0,20.07,15.34,15.34,0,0,0,4.58,7A11.38,11.38,0,0,0,387.56,116.91Z"/><path class="cls-3" d="M472.23,102.05V65h17.88V129.5H473V117.79h-.68A19.08,19.08,0,0,1,465,126.9a21.41,21.41,0,0,1-12.32,3.44,21.69,21.69,0,0,1-11.37-2.94,20.09,20.09,0,0,1-7.67-8.35,28.77,28.77,0,0,1-2.79-13V65h17.89v37.87c0,3.8,1.05,6.81,3.06,9a10.41,10.41,0,0,0,8.1,3.32,12.78,12.78,0,0,0,6.05-1.49,11.58,11.58,0,0,0,4.55-4.43A13.75,13.75,0,0,0,472.23,102.05Z"/><path class="cls-3" d="M528.14,130.55a24.16,24.16,0,0,1-13.28-3.8,25.78,25.78,0,0,1-9.4-11.21Q502,108.13,502,97.35q0-11.09,3.57-18.45a25.73,25.73,0,0,1,9.51-11,24.29,24.29,0,0,1,13-3.68,20,20,0,0,1,9,1.83A18.15,18.15,0,0,1,543,70.54a24.43,24.43,0,0,1,3.42,5.31H547V43.53h17.84v86H547.2V119.17h-.75a23.05,23.05,0,0,1-3.55,5.32,18.09,18.09,0,0,1-5.92,4.34A20.5,20.5,0,0,1,528.14,130.55Zm5.67-14.23a11.47,11.47,0,0,0,7.33-2.37,14.89,14.89,0,0,0,4.61-6.66,28.24,28.24,0,0,0,1.62-10,28.22,28.22,0,0,0-1.6-10,14.33,14.33,0,0,0-4.61-6.55,11.77,11.77,0,0,0-7.35-2.31,11.58,11.58,0,0,0-7.43,2.4,14.57,14.57,0,0,0-4.57,6.63,28.38,28.38,0,0,0-1.56,9.82,28.82,28.82,0,0,0,1.58,9.93,15,15,0,0,0,4.57,6.72A11.44,11.44,0,0,0,533.81,116.32Z"/><path class="cls-3" d="M582.76,129.5v-86H634.3v9.24H593.17v29h37.27v9.24H593.17V129.5Z"/><path class="cls-3" d="M687.36,103.14V65h9.91V129.5h-9.91V118.59h-.67a21,21,0,0,1-7.05,8.33q-4.78,3.42-12.09,3.42a21.41,21.41,0,0,1-10.75-2.67,18.33,18.33,0,0,1-7.38-8q-2.7-5.4-2.69-13.63V65h9.91v40.3q0,7.05,4,11.25a13.35,13.35,0,0,0,10.14,4.2,17,17,0,0,0,7.54-1.89,16.58,16.58,0,0,0,6.46-5.79A17.49,17.49,0,0,0,687.36,103.14Z"/><path class="cls-3" d="M761.08,79.46,752.18,82a17.11,17.11,0,0,0-2.46-4.34,12.34,12.34,0,0,0-4.39-3.51,15.87,15.87,0,0,0-7.09-1.38,16.94,16.94,0,0,0-9.84,2.7,8.09,8.09,0,0,0-3.93,6.87,7.05,7.05,0,0,0,2.69,5.83q2.69,2.14,8.39,3.57l9.57,2.35q8.65,2.1,12.89,6.4a15.09,15.09,0,0,1,4.24,11.06,16.46,16.46,0,0,1-3.17,9.91,21.35,21.35,0,0,1-8.83,6.89,32.4,32.4,0,0,1-13.19,2.51q-9.85,0-16.33-4.28a19,19,0,0,1-8.18-12.51l9.4-2.35a13.09,13.09,0,0,0,5.1,7.81q3.76,2.59,9.85,2.6,6.92,0,11-3c2.72-2,4.09-4.34,4.09-7.11a7.61,7.61,0,0,0-2.35-5.65,15.26,15.26,0,0,0-7.22-3.42l-10.75-2.52q-8.85-2.1-13-6.53a15.61,15.61,0,0,1-4.14-11.1,15.82,15.82,0,0,1,3.09-9.65,20.63,20.63,0,0,1,8.44-6.59,29.41,29.41,0,0,1,12.15-2.4q9.57,0,15,4.2A22.7,22.7,0,0,1,761.08,79.46Z"/><path class="cls-3" d="M782.23,54.28a6.94,6.94,0,0,1-5-2,6.44,6.44,0,0,1,0-9.48,7.24,7.24,0,0,1,10,0,6.46,6.46,0,0,1,0,9.48A7,7,0,0,1,782.23,54.28Zm-5,75.22V65h9.91V129.5Z"/><path class="cls-3" d="M831.43,130.84a27.22,27.22,0,0,1-25.54-15.78,39.11,39.11,0,0,1-3.68-17.46A39.59,39.59,0,0,1,805.89,80,28.25,28.25,0,0,1,857,80a39.59,39.59,0,0,1,3.68,17.59A39.11,39.11,0,0,1,857,115.06a27.22,27.22,0,0,1-25.54,15.78Zm0-8.89a17,17,0,0,0,10.91-3.41,19.92,19.92,0,0,0,6.34-8.94,34.21,34.21,0,0,0,2.06-12,34.62,34.62,0,0,0-2.06-12,20.31,20.31,0,0,0-6.34-9,19,19,0,0,0-21.83,0,20.37,20.37,0,0,0-6.33,9,34.39,34.39,0,0,0-2.06,12,34,34,0,0,0,2.06,12,20,20,0,0,0,6.33,8.94A17,17,0,0,0,831.43,122Z"/><path class="cls-3" d="M885.66,90.71V129.5h-9.9V65h9.57V75.1h.84a18.47,18.47,0,0,1,6.88-7.91q4.62-3,11.92-3a23.6,23.6,0,0,1,11.46,2.67,18.21,18.21,0,0,1,7.64,8.06c1.82,3.59,2.73,8.14,2.73,13.62v41h-9.9V89.2q0-7.59-3.95-11.86t-10.83-4.26a17.2,17.2,0,0,0-8.46,2.06,14.67,14.67,0,0,0-5.86,6A19.9,19.9,0,0,0,885.66,90.71Z"/><text/></g></g></svg>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-md-18 f-16 w400 lh140 white-clr text-xs-center">Copyright © CloudFusion 2023</div>
                  <ul class="footer-ul w400 f-md-18 f-16 white-clr text-center text-md-right">
                     <li><a href="https://support.oppyo.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://getcloudfusion.co/legal/privacy-policy.html " class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://getcloudfusion.co/legal/terms-of-service.html " class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://getcloudfusion.co/legal/disclaimer.html " class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://getcloudfusion.co/legal/gdpr.html " class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://getcloudfusion.co/legal/dmca.html " class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://getcloudfusion.co/legal/anti-spam.html " class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
    <!--Footer Section End -->


</body>
</html>

<html>
   <head>
      <title>CloudFusion | JV</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=9">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
      <meta name="title" content="CloudFusion | JV">
      <meta name="description" content="Host & Deliver UltraHD 4k Videos, Website Images, Training or Any Media Files at Lightning-Fast Speed">
      <meta name="keywords" content="CloudFusion">
      <meta property="og:image" content="https://www.getcloudfusion.co/jv/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Dr. Amit Pareek">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="CloudFusion | JV">
      <meta property="og:description" content="Host & Deliver UltraHD 4k Videos, Website Images, Training or Any Media Files at Lightning-Fast Speed">
      <meta property="og:image" content="https://www.getcloudfusion.co/jv/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="CloudFusion | JV">
      <meta property="twitter:description" content="Host & Deliver UltraHD 4k Videos, Website Images, Training or Any Media Files at Lightning-Fast Speed">
      <meta property="twitter:image" content="https://www.getcloudfusion.co/jv/thumbnail.png">
      <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&family=Kalam:wght@700&family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
      <!-- required -->
      <link rel="stylesheet" href="https://cdn.oppyo.com/launches/yodrive/common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" href="assets/css/style.css" type="text/css">
      <link rel="stylesheet" href="assets/css/timer.css" type="text/css">
      <script src="https://cdn.oppyo.com/launches/yodrive/common_assets/js/jquery.min.js"></script>
   </head>
   <body>
      <!-- New Timer  Start-->
      <?php
         $date = 'August 15 2023 10:00 AM EST';
         $exp_date = strtotime($date);
         $now = time();  
         if ($now < $exp_date) {
         ?>
      <?php
         } else {
          echo "Times Up";
         }
         ?>
      <!-- New Timer End -->
      <!-- Header Section Start -->
      <div class="header-section">
         <div class="container">
            <div class="row">
               <div class="col-md-3 col-12 text-center">
                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 926.8 148.17" style="max-height:50px">
                     <defs>
                        <style>
                           .cls-1{fill:#0068ff;}
                           .cls-2{fill:#f89b1c;}
                           .cls-3{fill:#fff;}
                        </style>
                     </defs>
                     <g id="Layer_2" data-name="Layer 2">
                     <g id="Layer_1-2" data-name="Layer 1">
                     <path class="cls-1" d="M41.41,108.34a22.49,22.49,0,0,1-2.87-11c0-12.08,9.69-22.18,22.05-23l16.06-1.06L72.4,58.17a35,35,0,0,1-1.31-9.5c0-19.78,16.54-35.86,36.86-35.86a37.33,37.33,0,0,1,24.89,9.41,35.39,35.39,0,0,1,11.81,23.06l.59,6.21c4.11-3.79,8.07-7.79,11.84-12C152.67,17,132.35,0,108,0c-27.63,0-50,21.79-50,48.67A47.76,47.76,0,0,0,59.7,61.56c-19.17,1.26-34.33,16.8-34.33,35.77a35,35,0,0,0,3.28,14.81C32.55,111.06,36.83,109.79,41.41,108.34Z"/>
                     <path class="cls-2" d="M165.9,50.66c10.88-20.3,14.58-35.47,14.58-35.47C142.66,114.85,2.87,139.07,0,140H163.24c25.39,0,46-20,46-44.71C209.2,71.47,190,52,165.9,50.66Zm-2.66,76.53H91.94a188.75,188.75,0,0,0,66.3-63.45l1.07-.12a34.8,34.8,0,0,1,3.93-.23c18.09,0,32.8,14.31,32.8,31.9S181.33,127.19,163.24,127.19Z"/><text/>
                     <path class="cls-3" d="M313.48,73.63H295.09A18.17,18.17,0,0,0,293,67.27a16.13,16.13,0,0,0-4-4.77,17.68,17.68,0,0,0-5.6-3,21.68,21.68,0,0,0-6.86-1A20.48,20.48,0,0,0,265,61.73a21.33,21.33,0,0,0-7.68,9.57,38.07,38.07,0,0,0-2.73,15.22A38.43,38.43,0,0,0,257.29,102a19.88,19.88,0,0,0,19.16,12.64,22.63,22.63,0,0,0,6.78-1,17.28,17.28,0,0,0,5.56-2.83,15.92,15.92,0,0,0,4-4.55,18.16,18.16,0,0,0,2.25-6.13l18.39.08a33.44,33.44,0,0,1-3.55,11.4,34.43,34.43,0,0,1-7.6,9.78,35.64,35.64,0,0,1-11.33,6.8,41.64,41.64,0,0,1-14.84,2.5,40.45,40.45,0,0,1-20.55-5.21,36.72,36.72,0,0,1-14.27-15.07q-5.24-9.87-5.23-23.88t5.29-23.93a37,37,0,0,1,14.36-15.05,40.29,40.29,0,0,1,20.4-5.18A44.26,44.26,0,0,1,290,44.45a35.54,35.54,0,0,1,11.36,6.11,32.24,32.24,0,0,1,8.08,9.8A36.94,36.94,0,0,1,313.48,73.63Z"/>
                     <path class="cls-3" d="M344.08,43.53v86H326.19v-86Z"/>
                     <path class="cls-3" d="M387.48,130.76a32.77,32.77,0,0,1-16.9-4.18,28.15,28.15,0,0,1-11-11.67,37.37,37.37,0,0,1-3.87-17.4A37.59,37.59,0,0,1,359.61,80a28.15,28.15,0,0,1,11-11.67,36.25,36.25,0,0,1,33.8,0,28.15,28.15,0,0,1,11,11.67,37.59,37.59,0,0,1,3.86,17.48,37.37,37.37,0,0,1-3.86,17.4,28.15,28.15,0,0,1-11,11.67A32.77,32.77,0,0,1,387.48,130.76Zm.08-13.85a11.05,11.05,0,0,0,7.43-2.54,15.66,15.66,0,0,0,4.52-7,33.67,33.67,0,0,0,0-20.07,15.81,15.81,0,0,0-4.52-7,11,11,0,0,0-7.43-2.56A11.29,11.29,0,0,0,380,80.39a15.48,15.48,0,0,0-4.58,7,33.67,33.67,0,0,0,0,20.07,15.34,15.34,0,0,0,4.58,7A11.38,11.38,0,0,0,387.56,116.91Z"/>
                     <path class="cls-3" d="M472.23,102.05V65h17.88V129.5H473V117.79h-.68A19.08,19.08,0,0,1,465,126.9a21.41,21.41,0,0,1-12.32,3.44,21.69,21.69,0,0,1-11.37-2.94,20.09,20.09,0,0,1-7.67-8.35,28.77,28.77,0,0,1-2.79-13V65h17.89v37.87c0,3.8,1.05,6.81,3.06,9a10.41,10.41,0,0,0,8.1,3.32,12.78,12.78,0,0,0,6.05-1.49,11.58,11.58,0,0,0,4.55-4.43A13.75,13.75,0,0,0,472.23,102.05Z"/>
                     <path class="cls-3" d="M528.14,130.55a24.16,24.16,0,0,1-13.28-3.8,25.78,25.78,0,0,1-9.4-11.21Q502,108.13,502,97.35q0-11.09,3.57-18.45a25.73,25.73,0,0,1,9.51-11,24.29,24.29,0,0,1,13-3.68,20,20,0,0,1,9,1.83A18.15,18.15,0,0,1,543,70.54a24.43,24.43,0,0,1,3.42,5.31H547V43.53h17.84v86H547.2V119.17h-.75a23.05,23.05,0,0,1-3.55,5.32,18.09,18.09,0,0,1-5.92,4.34A20.5,20.5,0,0,1,528.14,130.55Zm5.67-14.23a11.47,11.47,0,0,0,7.33-2.37,14.89,14.89,0,0,0,4.61-6.66,28.24,28.24,0,0,0,1.62-10,28.22,28.22,0,0,0-1.6-10,14.33,14.33,0,0,0-4.61-6.55,11.77,11.77,0,0,0-7.35-2.31,11.58,11.58,0,0,0-7.43,2.4,14.57,14.57,0,0,0-4.57,6.63,28.38,28.38,0,0,0-1.56,9.82,28.82,28.82,0,0,0,1.58,9.93,15,15,0,0,0,4.57,6.72A11.44,11.44,0,0,0,533.81,116.32Z"/>
                     <path class="cls-3" d="M582.76,129.5v-86H634.3v9.24H593.17v29h37.27v9.24H593.17V129.5Z"/>
                     <path class="cls-3" d="M687.36,103.14V65h9.91V129.5h-9.91V118.59h-.67a21,21,0,0,1-7.05,8.33q-4.78,3.42-12.09,3.42a21.41,21.41,0,0,1-10.75-2.67,18.33,18.33,0,0,1-7.38-8q-2.7-5.4-2.69-13.63V65h9.91v40.3q0,7.05,4,11.25a13.35,13.35,0,0,0,10.14,4.2,17,17,0,0,0,7.54-1.89,16.58,16.58,0,0,0,6.46-5.79A17.49,17.49,0,0,0,687.36,103.14Z"/>
                     <path class="cls-3" d="M761.08,79.46,752.18,82a17.11,17.11,0,0,0-2.46-4.34,12.34,12.34,0,0,0-4.39-3.51,15.87,15.87,0,0,0-7.09-1.38,16.94,16.94,0,0,0-9.84,2.7,8.09,8.09,0,0,0-3.93,6.87,7.05,7.05,0,0,0,2.69,5.83q2.69,2.14,8.39,3.57l9.57,2.35q8.65,2.1,12.89,6.4a15.09,15.09,0,0,1,4.24,11.06,16.46,16.46,0,0,1-3.17,9.91,21.35,21.35,0,0,1-8.83,6.89,32.4,32.4,0,0,1-13.19,2.51q-9.85,0-16.33-4.28a19,19,0,0,1-8.18-12.51l9.4-2.35a13.09,13.09,0,0,0,5.1,7.81q3.76,2.59,9.85,2.6,6.92,0,11-3c2.72-2,4.09-4.34,4.09-7.11a7.61,7.61,0,0,0-2.35-5.65,15.26,15.26,0,0,0-7.22-3.42l-10.75-2.52q-8.85-2.1-13-6.53a15.61,15.61,0,0,1-4.14-11.1,15.82,15.82,0,0,1,3.09-9.65,20.63,20.63,0,0,1,8.44-6.59,29.41,29.41,0,0,1,12.15-2.4q9.57,0,15,4.2A22.7,22.7,0,0,1,761.08,79.46Z"/>
                     <path class="cls-3" d="M782.23,54.28a6.94,6.94,0,0,1-5-2,6.44,6.44,0,0,1,0-9.48,7.24,7.24,0,0,1,10,0,6.46,6.46,0,0,1,0,9.48A7,7,0,0,1,782.23,54.28Zm-5,75.22V65h9.91V129.5Z"/>
                     <path class="cls-3" d="M831.43,130.84a27.22,27.22,0,0,1-25.54-15.78,39.11,39.11,0,0,1-3.68-17.46A39.59,39.59,0,0,1,805.89,80,28.25,28.25,0,0,1,857,80a39.59,39.59,0,0,1,3.68,17.59A39.11,39.11,0,0,1,857,115.06a27.22,27.22,0,0,1-25.54,15.78Zm0-8.89a17,17,0,0,0,10.91-3.41,19.92,19.92,0,0,0,6.34-8.94,34.21,34.21,0,0,0,2.06-12,34.62,34.62,0,0,0-2.06-12,20.31,20.31,0,0,0-6.34-9,19,19,0,0,0-21.83,0,20.37,20.37,0,0,0-6.33,9,34.39,34.39,0,0,0-2.06,12,34,34,0,0,0,2.06,12,20,20,0,0,0,6.33,8.94A17,17,0,0,0,831.43,122Z"/>
                     <path class="cls-3" d="M885.66,90.71V129.5h-9.9V65h9.57V75.1h.84a18.47,18.47,0,0,1,6.88-7.91q4.62-3,11.92-3a23.6,23.6,0,0,1,11.46,2.67,18.21,18.21,0,0,1,7.64,8.06c1.82,3.59,2.73,8.14,2.73,13.62v41h-9.9V89.2q0-7.59-3.95-11.86t-10.83-4.26a17.2,17.2,0,0,0-8.46,2.06,14.67,14.67,0,0,0-5.86,6A19.9,19.9,0,0,0,885.66,90.71Z"/>
                     <text/>
                  </g>
               </g>
               </svg>
            </div>
            <div class="col-md-9 mt15 mt-md7">
               <ul class="leader-ul f-20 w400 white-clr text-md-end text-center ">
                  <li>
                     <a href="https://docs.google.com/document/d/188iMT9Fplp2zqhydJQ0rIpdpa8Axs2of/edit?usp=sharing&ouid=117555941573669202344&rtpof=true&sd=true" target="_blank">JV Docs</a>
                  </li>
                  <li>
                     <a href="https://docs.google.com/document/d/1Big6KcecR5EOFkQBLrJz6IPvbORhFCYR/edit?usp=sharing&ouid=117555941573669202344&rtpof=true&sd=true" target="_blank">Swipes</a>
                  </li>
                  <li>
                     <a href="https://www.jvzoo.com/affiliate/affiliateinfonew/index/398674" class="affiliate-link-btn ml-md15 mt10 mt-md0" target="_blank"> Grab Your Affiliate Link</a>
                  </li>
               </ul>
            </div>
               <div class="col-12 mt20 mt-md75 text-center">
                  <div class="preheadline f-18 f-md-22 w600 white-clr lh140">
                  Join Us on August 15<sup>th</sup> for Something EXTRAORDINARY!
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 relative">
                  <div class="mainheadline f-md-45 f-28 w400 text-center black-clr lh140">
                  Lightning-Fast Platform to <span class="w700"> Host, Backup & Deliver Ultra HD 4K Video Trainings, Website Images, & Media Files </span> at <u>UNBEATABLE ONE-TIME Price...</u>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30">
                  <div class="f-18 f-md-24 w500 text-center lh140 white-clr text-capitalize">
                     <span class="orange-clr"> (Backed By World’s BIGGEST Architecture)</span><br>
                     Convert More Leads & Sales With FAST Videos | Boost Website Speed | Tap Into Fastest Growing E-Learning, Video Marketing, And Online Business
                  </div>
               </div>
               <div class="col-12 col-md-12 mt15 mt-md80">
                  <div class="row">
                     <div class="col-md-8 col-12">
                        <div class="video-box">
                           <div style="padding-bottom: 56.25%;position: relative;"><iframe src="https://cloudfusion.oppyo.com/video/embed/1lrz89rx2v" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                           box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe></div>
                        </div>
                     </div>
                     <div class="col-md-4 col-12 mt20 mt-md0">
                        <div class="calendar-wrap">
                           <img src="assets/images/date.webp" alt="Date" class="img-fluid mx-auto d-block">
                        </div>
                        <div class="clearfix"></div>
                        <div class="countdown counter-black mt15">
                           <div class="timer-label text-center"><span class="f-30 f-md-40 lh100 timerbg col-12">00</span><br><span class="f-14 w500">Days</span> </div>
                           <div class="timer-label text-center"><span class="f-30 f-md-40 lh100 timerbg">00</span><br><span class="f-14 w500">Hours</span> </div>
                           <div class="timer-label text-center"><span class="f-30 f-md-40 lh100 timerbg">00</span><br><span class="f-14 w500">Mins</span> </div>
                           <div class="timer-label text-center"><span class="f-30 f-md-40 lh100 timerbg">00</span><br><span class="f-14 w500">Sec</span> </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Header Section End -->
      <div class="live-section" id="live-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-6">
                  <div class="left-live-box">
                     <div class="f-28 f-md-45 w700 text-center text-capitalize black-clr lh140">
                        Subscribe To Our <br class="d-none d-md-block"> JV List
                     </div>
                     <div class="f-20 f-md-22 w500 text-center text-capitalize black-clr lh140 mt10">
                        And Be The First to Know Our Special Contest, Events and Discounts
                     </div>
                     <!-- Aweber Form Code -->
                     <div class="mt20 mt-md40">
                        <form method="post" class="af-form-wrapper mb-md5" accept-charset="UTF-8" action="https://www.aweber.com/scripts/addlead.pl">
                           <div style="display: none;">
                              <input type="hidden" name="meta_web_form_id" value="1703814699">
                              <input type="hidden" name="meta_split_id" value="">
                              <input type="hidden" name="listname" value="awlist6200898">
                              <input type="hidden" name="redirect" value="https://www.aweber.com/thankyou-coi.htm?m=text" id="redirect_3161ec97c09ca429c4183bf74a86fd90">
                              <input type="hidden" name="meta_adtracking" value="My_Web_Form">
                              <input type="hidden" name="meta_message" value="1">
                              <input type="hidden" name="meta_required" value="name,email">
                              <input type="hidden" name="meta_tooltip" value="">
                           </div>
                           <div id="af-form-1703814699" class="af-form">
                              <div id="af-body-1703814699" class="af-body af-standards row justify-content-center">
                                 <div class="af-element col-md-12">
                                    <label class="previewLabel" for="awf_field-115787791" style="display:none;">Name: </label>
                                    <div class="af-textWrap mb20 input-type">
                                       <input id="awf_field-115787791" class="frm-ctr-popup form-control input-field" type="text" name="name" placeholder="Your Name" value="" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " tabindex="500">
                                    </div>
                                    <div class="af-clear"></div>
                                 </div>
                                 <div class="af-element mb20 col-md-12">
                                    <label class="previewLabel" for="awf_field-115787792" style="display:none;">Email: </label>
                                    <div class="af-textWrap input-type">
                                       <input class="text frm-ctr-popup form-control input-field" id="awf_field-115787792" type="text" name="email" placeholder="Your Email" value="" tabindex="501" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} ">
                                    </div>
                                    <div class="af-clear"></div>
                                 </div>
                                 <div class="af-element buttonContainer button-type form-btn white-clr col-md-12">
                                    <input name="submit" class="submit f-20 f-md-24 white-clr center-block" type="submit" value="Subscribe For JV Updates" tabindex="502">
                                    <div class="af-clear"></div>
                                 </div>
                              </div>
                           </div>
                           <div style="display: none;"><img src="https://forms.aweber.com/form/displays.htm?id=zIwcTIxMbMwc" alt="image"></div>
                        </form>
                     </div>
                     <!-- Aweber Form Code -->
                  </div>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0 text-center">
                  <div class="right-live-box">
                     <div class="f-24 f-md-34 w700 text-center text-capitalize black-clr lh140">
                        Grab Your <span class="orange-clr">JVZoo</span> Affiliate Link to Jump on This Amazing Product Launch
                     </div>
                     <img src="assets/images/jvzoo.webp" class="img-fluid d-block mx-auto mt20 " alt="Jvzoo">
                     <div class="request-affiliate mt20">
                        <a href="https://www.jvzoo.com/affiliate/affiliateinfonew/index/398674" class="f-20 f-md-22 w600 mx-auto white-clr" target="_blank">Request FE Link</a>
                     </div>
                     <div class="request-affiliate mt20">
                        <a href="https://www.jvzoo.com/affiliate/affiliateinfonew/index/398672" class="f-20 f-md-22 w600 mx-auto white-clr" target="_blank">Request Bundle Link</a>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Header Section End -->
      <!-- Technology-Section -->
      <div class="technology-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-md-32 f-20 w400 text-center orange-clr">
                     Why Trust Us? <br> We Have 5+ Years of Experience in Hosting Files & Videos  
                  </div>
               </div>
               <div class="col-12 text-center mt20 mt-md30">
                  <div class="f-md-50 f-28 w700 text-center white-clr lh120 text-capitalize">
                  CloudFusion is backed by the same technology that has till now smartly Served...
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md50">
               <div class="col-12 col-md-10 mx-auto">
                  <div class="row">
                     <div class="col-md-6">
                        <div class="technology-box">
                           <img src="assets/images/cloud.webp" alt="cloud" class="img-fluid mx-auto d-block">
                           <div class="f-md-34 f-24 w700 text-capitalize black-clr mt15 mt-md20">69Million+
                              <br><span class="f-18 f-md-20 w500">No. of File Views /Downloads</span>
                           </div>
                        </div>
                        <div class="technology-box mt20 mt-md30">
                           <img src="assets/images/play.webp" alt="play" class="img-fluid mx-auto d-block">
                           <div class="f-md-34 f-24 w700 text-capitalize black-clr mt15 mt-md20">13,979,517+
                              <br><span class="f-18 f-md-20 w500">Video Plays In Minutes</span>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="technology-box mt20 mt-md0">
                           <img src="assets/images/web-hosting.webp" alt="Web-Hosting" class="img-fluid mx-auto d-block">
                           <div class="f-md-34 f-24 w700 text-capitalize black-clr mt15 mt-md20">410,763+
                              <br><span class="f-18 f-md-20 w500">No. Of Files Hosted</span>
                           </div>
                        </div>
                        <div class="technology-box mt20 mt-md30">
                           <img src="assets/images/portfolio.webp" alt="Protfolio" class="img-fluid mx-auto d-block">
                           <div class="f-md-34 f-24 w700 text-capitalize black-clr mt15 mt-md20">34,428+
                              <br><span class="f-18 f-md-20 w500">Total Businesses Created</span>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Technology-Section-End -->

      <!-- Online Business Section Starts -->
      <div class="online-business-sec">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-12 col-12">
               <div class="f-28 f-md-38 lh140 black-clr w400 text-center">
                     <span class="w700">File Hosting is Must Have & Backbone</span><br> to Run Any Business Online
                  </div>        
               </div>
               <div class="col-12 col-md-6">
                
                  <ul class="launch-list f-md-22 f-18 w400 mb0 mt-md80 mt20 pl0">
                     <div class="feature-list">
                        <div class="f-18 f-md-22 lh140 w400">
                           Play Ultra HD 4K Fast-Loading Videos on Any Page or Site
                        </div>
                     </div>
                     <div class="feature-list">
                        <div class="f-18 f-md-22 lh140 w400">
                           Tap Into $398 Billion E-learning Industry
                        </div>
                     </div>
                     <div class="feature-list">
                        <div class="f-18 f-md-22 lh140 w400">
                           Deliver Client Projects, Docs, Files & PDFs Securely
                        </div>
                     </div>
                     <div class="feature-list">
                        <div class="f-18 f-md-22 lh140 w400">
                           Boost Engagement, Leads, & Sales with Your Media Content
                        </div>
                     </div>
                  </ul>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md50">
                  <img src="assets/images/online-business-man.webp" class="img-fluid d-block mx-auto " alt="Phase1">
               </div>
            </div>
         </div>
      </div>
      <!-- Online Business Section Ends -->


      <!-- Proudly Section Starts -->
      <div class="proudly-sec" id="product">
         <div class="container">
            <div class="row">
               <div class="col-12 mt-md30 mt20 text-center">
                  <div class="f-38 f-md-50 w700 lh140 white-clr caveat proudly-head">
                     Proudly Presenting…
                  </div>
               </div>
               <div class="col-12 mt-md30 mt20 text-center">
                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 926.8 148.17" style="max-height:100px">
                     <defs>
                        <style>
                           .cls-1{fill:#0068ff;}
                           .cls-2{fill:#f89b1c;}
                           .cls-3{fill:#fff;}
                        </style>
                     </defs>
                     <g id="Layer_2" data-name="Layer 2">
                     <g id="Layer_1-2" data-name="Layer 1">
                     <path class="cls-1" d="M41.41,108.34a22.49,22.49,0,0,1-2.87-11c0-12.08,9.69-22.18,22.05-23l16.06-1.06L72.4,58.17a35,35,0,0,1-1.31-9.5c0-19.78,16.54-35.86,36.86-35.86a37.33,37.33,0,0,1,24.89,9.41,35.39,35.39,0,0,1,11.81,23.06l.59,6.21c4.11-3.79,8.07-7.79,11.84-12C152.67,17,132.35,0,108,0c-27.63,0-50,21.79-50,48.67A47.76,47.76,0,0,0,59.7,61.56c-19.17,1.26-34.33,16.8-34.33,35.77a35,35,0,0,0,3.28,14.81C32.55,111.06,36.83,109.79,41.41,108.34Z"/>
                     <path class="cls-2" d="M165.9,50.66c10.88-20.3,14.58-35.47,14.58-35.47C142.66,114.85,2.87,139.07,0,140H163.24c25.39,0,46-20,46-44.71C209.2,71.47,190,52,165.9,50.66Zm-2.66,76.53H91.94a188.75,188.75,0,0,0,66.3-63.45l1.07-.12a34.8,34.8,0,0,1,3.93-.23c18.09,0,32.8,14.31,32.8,31.9S181.33,127.19,163.24,127.19Z"/><text/>
                     <path class="cls-3" d="M313.48,73.63H295.09A18.17,18.17,0,0,0,293,67.27a16.13,16.13,0,0,0-4-4.77,17.68,17.68,0,0,0-5.6-3,21.68,21.68,0,0,0-6.86-1A20.48,20.48,0,0,0,265,61.73a21.33,21.33,0,0,0-7.68,9.57,38.07,38.07,0,0,0-2.73,15.22A38.43,38.43,0,0,0,257.29,102a19.88,19.88,0,0,0,19.16,12.64,22.63,22.63,0,0,0,6.78-1,17.28,17.28,0,0,0,5.56-2.83,15.92,15.92,0,0,0,4-4.55,18.16,18.16,0,0,0,2.25-6.13l18.39.08a33.44,33.44,0,0,1-3.55,11.4,34.43,34.43,0,0,1-7.6,9.78,35.64,35.64,0,0,1-11.33,6.8,41.64,41.64,0,0,1-14.84,2.5,40.45,40.45,0,0,1-20.55-5.21,36.72,36.72,0,0,1-14.27-15.07q-5.24-9.87-5.23-23.88t5.29-23.93a37,37,0,0,1,14.36-15.05,40.29,40.29,0,0,1,20.4-5.18A44.26,44.26,0,0,1,290,44.45a35.54,35.54,0,0,1,11.36,6.11,32.24,32.24,0,0,1,8.08,9.8A36.94,36.94,0,0,1,313.48,73.63Z"/>
                     <path class="cls-3" d="M344.08,43.53v86H326.19v-86Z"/>
                     <path class="cls-3" d="M387.48,130.76a32.77,32.77,0,0,1-16.9-4.18,28.15,28.15,0,0,1-11-11.67,37.37,37.37,0,0,1-3.87-17.4A37.59,37.59,0,0,1,359.61,80a28.15,28.15,0,0,1,11-11.67,36.25,36.25,0,0,1,33.8,0,28.15,28.15,0,0,1,11,11.67,37.59,37.59,0,0,1,3.86,17.48,37.37,37.37,0,0,1-3.86,17.4,28.15,28.15,0,0,1-11,11.67A32.77,32.77,0,0,1,387.48,130.76Zm.08-13.85a11.05,11.05,0,0,0,7.43-2.54,15.66,15.66,0,0,0,4.52-7,33.67,33.67,0,0,0,0-20.07,15.81,15.81,0,0,0-4.52-7,11,11,0,0,0-7.43-2.56A11.29,11.29,0,0,0,380,80.39a15.48,15.48,0,0,0-4.58,7,33.67,33.67,0,0,0,0,20.07,15.34,15.34,0,0,0,4.58,7A11.38,11.38,0,0,0,387.56,116.91Z"/>
                     <path class="cls-3" d="M472.23,102.05V65h17.88V129.5H473V117.79h-.68A19.08,19.08,0,0,1,465,126.9a21.41,21.41,0,0,1-12.32,3.44,21.69,21.69,0,0,1-11.37-2.94,20.09,20.09,0,0,1-7.67-8.35,28.77,28.77,0,0,1-2.79-13V65h17.89v37.87c0,3.8,1.05,6.81,3.06,9a10.41,10.41,0,0,0,8.1,3.32,12.78,12.78,0,0,0,6.05-1.49,11.58,11.58,0,0,0,4.55-4.43A13.75,13.75,0,0,0,472.23,102.05Z"/>
                     <path class="cls-3" d="M528.14,130.55a24.16,24.16,0,0,1-13.28-3.8,25.78,25.78,0,0,1-9.4-11.21Q502,108.13,502,97.35q0-11.09,3.57-18.45a25.73,25.73,0,0,1,9.51-11,24.29,24.29,0,0,1,13-3.68,20,20,0,0,1,9,1.83A18.15,18.15,0,0,1,543,70.54a24.43,24.43,0,0,1,3.42,5.31H547V43.53h17.84v86H547.2V119.17h-.75a23.05,23.05,0,0,1-3.55,5.32,18.09,18.09,0,0,1-5.92,4.34A20.5,20.5,0,0,1,528.14,130.55Zm5.67-14.23a11.47,11.47,0,0,0,7.33-2.37,14.89,14.89,0,0,0,4.61-6.66,28.24,28.24,0,0,0,1.62-10,28.22,28.22,0,0,0-1.6-10,14.33,14.33,0,0,0-4.61-6.55,11.77,11.77,0,0,0-7.35-2.31,11.58,11.58,0,0,0-7.43,2.4,14.57,14.57,0,0,0-4.57,6.63,28.38,28.38,0,0,0-1.56,9.82,28.82,28.82,0,0,0,1.58,9.93,15,15,0,0,0,4.57,6.72A11.44,11.44,0,0,0,533.81,116.32Z"/>
                     <path class="cls-3" d="M582.76,129.5v-86H634.3v9.24H593.17v29h37.27v9.24H593.17V129.5Z"/>
                     <path class="cls-3" d="M687.36,103.14V65h9.91V129.5h-9.91V118.59h-.67a21,21,0,0,1-7.05,8.33q-4.78,3.42-12.09,3.42a21.41,21.41,0,0,1-10.75-2.67,18.33,18.33,0,0,1-7.38-8q-2.7-5.4-2.69-13.63V65h9.91v40.3q0,7.05,4,11.25a13.35,13.35,0,0,0,10.14,4.2,17,17,0,0,0,7.54-1.89,16.58,16.58,0,0,0,6.46-5.79A17.49,17.49,0,0,0,687.36,103.14Z"/>
                     <path class="cls-3" d="M761.08,79.46,752.18,82a17.11,17.11,0,0,0-2.46-4.34,12.34,12.34,0,0,0-4.39-3.51,15.87,15.87,0,0,0-7.09-1.38,16.94,16.94,0,0,0-9.84,2.7,8.09,8.09,0,0,0-3.93,6.87,7.05,7.05,0,0,0,2.69,5.83q2.69,2.14,8.39,3.57l9.57,2.35q8.65,2.1,12.89,6.4a15.09,15.09,0,0,1,4.24,11.06,16.46,16.46,0,0,1-3.17,9.91,21.35,21.35,0,0,1-8.83,6.89,32.4,32.4,0,0,1-13.19,2.51q-9.85,0-16.33-4.28a19,19,0,0,1-8.18-12.51l9.4-2.35a13.09,13.09,0,0,0,5.1,7.81q3.76,2.59,9.85,2.6,6.92,0,11-3c2.72-2,4.09-4.34,4.09-7.11a7.61,7.61,0,0,0-2.35-5.65,15.26,15.26,0,0,0-7.22-3.42l-10.75-2.52q-8.85-2.1-13-6.53a15.61,15.61,0,0,1-4.14-11.1,15.82,15.82,0,0,1,3.09-9.65,20.63,20.63,0,0,1,8.44-6.59,29.41,29.41,0,0,1,12.15-2.4q9.57,0,15,4.2A22.7,22.7,0,0,1,761.08,79.46Z"/>
                     <path class="cls-3" d="M782.23,54.28a6.94,6.94,0,0,1-5-2,6.44,6.44,0,0,1,0-9.48,7.24,7.24,0,0,1,10,0,6.46,6.46,0,0,1,0,9.48A7,7,0,0,1,782.23,54.28Zm-5,75.22V65h9.91V129.5Z"/>
                     <path class="cls-3" d="M831.43,130.84a27.22,27.22,0,0,1-25.54-15.78,39.11,39.11,0,0,1-3.68-17.46A39.59,39.59,0,0,1,805.89,80,28.25,28.25,0,0,1,857,80a39.59,39.59,0,0,1,3.68,17.59A39.11,39.11,0,0,1,857,115.06a27.22,27.22,0,0,1-25.54,15.78Zm0-8.89a17,17,0,0,0,10.91-3.41,19.92,19.92,0,0,0,6.34-8.94,34.21,34.21,0,0,0,2.06-12,34.62,34.62,0,0,0-2.06-12,20.31,20.31,0,0,0-6.34-9,19,19,0,0,0-21.83,0,20.37,20.37,0,0,0-6.33,9,34.39,34.39,0,0,0-2.06,12,34,34,0,0,0,2.06,12,20,20,0,0,0,6.33,8.94A17,17,0,0,0,831.43,122Z"/>
                     <path class="cls-3" d="M885.66,90.71V129.5h-9.9V65h9.57V75.1h.84a18.47,18.47,0,0,1,6.88-7.91q4.62-3,11.92-3a23.6,23.6,0,0,1,11.46,2.67,18.21,18.21,0,0,1,7.64,8.06c1.82,3.59,2.73,8.14,2.73,13.62v41h-9.9V89.2q0-7.59-3.95-11.86t-10.83-4.26a17.2,17.2,0,0,0-8.46,2.06,14.67,14.67,0,0,0-5.86,6A19.9,19.9,0,0,0,885.66,90.71Z"/>
                     <text/>
                  </g>
               </g>
               </svg>
               </div>
               <div class="col-12 f-md-38 f-22 mt-md30 mt20 w600 text-center white-clr lh140">
                  Amazing Cloud-Based Platform to Host, Manage and Deliver Unlimited Images, Files, and Videos at Lightning-Fast Speed with Zero Tech Hassles!
               </div>
            </div>
            <div class="row mt30 mt-md50 align-items-center">
               <div class="col-12 col-md-10 mx-auto">
                  <img src="assets/images/product-image.webp" class="img-fluid d-block mx-auto img-animation">
               </div>
            </div>
         </div>
      </div>
      <!-- Proudly Section Ends -->

      <!-- Features Section Starts -->
      <div class="presenting-feature">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row feature-list-block">
                     <div class="col-12 col-md-6">
                        <div class="d-flex justify-content-md-start justify-content-center gap-4">
                           <div class="mt5"><img src="assets/images/n1.webp"></div>
                              <div class="f-18 f-md-22 lh140 w300 white-clr">
                                 <span class="w600 f-22 f-md-28 lh150">Unlimited Everything</span> <br>
                                 Host, Manage & Publish Unlimited PDFs, Docs, Audios, Videos, Zip Files or Any Other Marketing File
                              </div>
                           </div>
                           <div class="d-flex justify-content-md-start justify-content-center gap-4 mt20">
                              <div class="mt5"><img src="assets/images/n5.webp"></div>
                              <div class="f-16 f-md-22 lh140 w300 white-clr">
                                 <span class="w600 f-22 f-md-27 lh150">50+ More Cool Features In Store</span> <br>
                                 We’ve Left No Stone Unturned To Give You An Unmatched Experience
                              </div>
                           </div>
                           <div class="d-flex justify-content-md-start justify-content-center gap-4 mt20">
                              <div class="mt5"><img src="assets/images/n4.webp"></div>
                                 <div class="f-16 f-md-22 lh140 w300 white-clr">
                                    <span class="w600 f-22 f-md-28 lh150">No Worries of Paying Monthly</span> <br>
                                    During This Launch Special Deal, Get All Benefits At Limited Low One-Time-Fee.
                                 </div>
                              </div>
                           </div>
                           <div class="col-12 col-md-6">
                              <div class="d-flex justify-content-md-start justify-content-center gap-4 mt20 mt-md0">
                                 <div class="mt5"><img src="assets/images/n3.webp"></div>
                                 <div class="f-16 f-md-22 lh140 w300 white-clr">
                                    <span class="w600 f-22 f-md-28 lh150">Robust & Proven Solution</span> <br>
                                    Powerful Battle-Tested Architecture Happily Serving 133 Million+ Marketing Files
                                    </div>
                                 </div>
                                 <div class="d-flex justify-content-md-start justify-content-center gap-4 mt20">
                                    <div class="mt5">
                                       <img src="assets/images/n2.webp">
                                    </div>
                                    <div class="f-16 f-md-22 lh140 w300 white-clr">
                                       <span class="w600 f-22 f-md-28 lh150">Fast & Easy</span> <br>
                                       Deliver All Your Files at Lightning Fast Speed with Fast CDNs
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
         <!-- Features Section Ends -->


         <!-- List Section Starts -->
         <div class="feature-sec">
            <div class="container">
               <div class="row">
                  <div class="col-12 text-center">
                     <div class="f-md-45 f-28 w700 text-center">
                        CloudFusion Is Packed with GROUND - BREAKING Features That Makes It A Cut Above The Rest
                     </div>
                  </div>
               </div>
               <div class="row align-items-center mt20 mt-md50" data-aos="fade-left">
                  <div class="col-md-6 col-12 f-md-32 f-22 w600">
                     Store & Manage Video Training, Website Images, PDFs, Docs, Audios or Any File.
                  </div>
                  <div class="col-md-6 col-12 mt-md0 mt20">
                     <img src="assets/images/f1.webp" class="img-fluid mx-auto d-block">
                  </div>
               </div>
               <div class="row">
                  <div class="col-12">
                     <img src="assets/images/left-arrow.webp" class="img-fluid d-none d-md-block mx-auto" alt="step Arrow">
                  </div>
               </div>
               <div class="row align-items-center mt-md0 mt30" data-aos="fade-right">
                  <div class="col-md-6 col-12 f-md-32 f-22 w600 order-md-2">
                     Supercharge Your Websites, Landing Pages, Training & Client Projects By Delivering Images & Files At Lightning Fast Speed
                  </div>
                  <div class="col-md-6 col-12 mt-md0 mt20 order-md-1">
                     <img src="assets/images/f2.webp" class="img-fluid mx-auto d-block">
                  </div>
               </div>
               <div class="row">
                  <div class="col-12">
                     <img src="assets/images/right-arrow.webp" class="img-fluid d-none d-md-block mx-auto" alt="step Arrow">
                  </div>
               </div>
               <div class="row align-items-center mt-md0 mt30" data-aos="fade-left">
                  <div class="col-md-6 col-12 f-md-32 f-22 w600">
                     Accurate Analysis to Know Your User
                     Engagement On Your Files & Folders
                  </div>
                  <div class="col-md-6 col-12 mt-md0 mt20">
                     <img src="assets/images/f3.webp" class="img-fluid mx-auto d-block">
                  </div>
               </div>
               <div class="row">
                  <div class="col-12">
                     <img src="assets/images/left-arrow.webp" class="img-fluid d-none d-md-block mx-auto" alt="step Arrow">
                  </div>
               </div>
               <div class="row align-items-center mt-md0 mt30" data-aos="fade-right">
                  <div class="col-md-6 col-12 f-md-32 f-22 w600 order-md-2">
                     Publish Training, Files & Folders On Engaging Doc Sharing Channels Those Have Protected
                     Elegant & SEO Optimized Sharing Pages
                  </div>
                  <div class="col-md-6 col-12 mt-md0 mt20 order-md-1">
                     <img src="assets/images/f4.webp" class="img-fluid mx-auto d-block">
                  </div>
               </div>
            </div>
         </div>
      <!-- List Section Starts -->


      <div class="feature-list-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 f-md-45 f-28 w700 lh140 text-center black-clr">
                  CloudFusion Has TONS Of Other Innovative & Never Seen Before Features
               </div>
            </div>
            <div class="row  ">
               <div class="col-12 col-md-4 mt50">
                  <div class="feature-list-box">
                     <img src="assets/images/fr1.webp" class="img-fluid mx-auto d-block">
                     <p class="description">Speed-Up Your Website Speed with <span class="w600">Fast Loading &
                        Optimized Images</span> 
                     </p>
                  </div>
               </div>
               <div class="col-12 col-md-4 mt50">
                  <div class="feature-list-box">
                     <img src="assets/images/fr2.webp" class="img-fluid mx-auto d-block">
                     <p class="description">Impress Your Customers With <span class="w600">
                        Lightning Fast Trainings Videos & PDF Docs </span> 
                     </p>
                  </div>
               </div>
               <div class="col-12 col-md-4 mt50">
                  <div class="feature-list-box">
                     <img src="assets/images/fr3.webp" class="img-fluid mx-auto d-block">
                     <p class="description">Manage & Share <span class="w600">Multiple Files & Folders Effortlessly</span></p>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-12 col-md-4 mt50">
                  <div class="feature-list-box">
                     <img src="assets/images/fr4.webp" class="img-fluid mx-auto d-block">
                     <p class="description">Online Back-Up & 30 days File recovery</p>
                  </div>
               </div>
               <div class="col-12 col-md-4 mt50">
                  <div class="feature-list-box">
                     <img src="assets/images/fr5.webp" class="img-fluid mx-auto d-block">
                     <p class="description">
                        Unbreakable File Security with 
                        <spa class="w600">SSL & OTP Enabled Login</spa>
                     </p>
                  </div>
               </div>
               <div class="col-12 col-md-4 mt50">
                  <div class="feature-list-box">
                     <img src="assets/images/fr6.webp" class="img-fluid mx-auto d-block">
                     <p class="description"> <span class="w600">Add Your LOGO on Your Doc Channel</span> To Share Files On Your Branded Pages</p>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-12 col-md-4 mt50">
                  <div class="feature-list-box">
                     <img src="assets/images/fr7.webp" class="img-fluid mx-auto d-block">
                     <p class="description">Business Cloud, <span class="w600">Access Files
                        Anytime, Anywhere</span>
                     </p>
                  </div>
               </div>
               <div class="col-12 col-md-4 mt50">
                  <div class="feature-list-box">
                     <img src="assets/images/fr8.webp" class="img-fluid mx-auto d-block">
                     <p class="description">Get Unlimited Viral Traffic & Leads <span class="w600">from File Sharing Page</span> </p>
                  </div>
               </div>
               <div class="col-12 col-md-4 mt50">
                  <div class="feature-list-box">
                     <img src="assets/images/fr9.webp" class="img-fluid mx-auto d-block">
                     <p class="description">Single dashboard <span class="w600">To Manage All Type Of Files -</span>  No Need To Buy Multiple Apps.</p>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-12 col-md-4 mt50">
                  <div class="feature-list-box">
                     <img src="assets/images/fr10.webp" class="img-fluid mx-auto d-block">
                     <p class="description w600">Personal & Business CloudFusion</p>
                  </div>
               </div>
               <div class="col-12 col-md-4 mt50">
                  <div class="feature-list-box">
                     <img src="assets/images/fr11.webp" class="img-fluid mx-auto d-block">
                     <p class="description"> <span class="w600">Track every visitor</span>  on your sharing pages, Segment them according to their behaviour</p>
                  </div>
               </div>
               <div class="col-12 col-md-4 mt50">
                  <div class="feature-list-box">
                     <img src="assets/images/fr12.webp" class="img-fluid mx-auto d-block">
                     <p class="description">
                        Advanced Integrations <span class="w600">with AR, webinar’s, CRM, pixabay & other file storage services.</span> 
                     </p>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- DEMO SECTION START -->
            <div class="demo-sec">
               <div class="container">
                  <div class="row">
                     <div class="col-12">
                        <div class="f-md-50 f-28 w700 lh140 text-center white-clr">
                           Watch The Demo
                           <br class="d-none d-md-block">
                           Discover How Easy & Powerful It Is
                        </div>
                     </div>
                     <div class="col-12 col-md-10 mx-auto mt-md40 mt20">
                        <div class="video-box">
                           <div style="padding-bottom: 56.25%;position: relative;"><iframe src="https://cloudfusion.oppyo.com/video/embed/1lrz89rx2v"    style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                           box-shadow: none !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
      <!-- DEMO SECTION END -->
      <!-- POTENTIAL SECTION START -->
      <div class="potential-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-md-45 f-28 w700 text-center">
                     Here’s a List of All Potential Use Cases<br>
                     CloudFusion Can Be Used For...
                  </div>
               </div>
            </div>
            <div class="row  ">
               <div class="col-12 col-md-4 mt20 mt-md50">
                  <div class="potential-block">
                     <img src="assets/images/qsn-1.webp" class="img-fluid mx-auto d-block">                    
                  </div>
                  <div class="f-20 f-md-24 w500 lh140 w400 mt30 text-center">
                     Selling Digital/ Ecom Products
                  </div>
               </div>
               <div class="col-12 col-md-4 mt20 mt-md50">
                  <div class="potential-block">
                     <img src="assets/images/qsn-3.webp" class="img-fluid mx-auto d-block">                    
                  </div>
                  <div class="f-20 f-md-24 w500 lh140 w400 mt30 text-center">
                     Selling DFY Templates, Themes, Plugins
                  </div>
               </div>
               <div class="col-12 col-md-4 mt20 mt-md50">
                  <div class="potential-block">
                     <img src="assets/images/qsn-5.webp" class="img-fluid mx-auto d-block">                    
                  </div>
                  <div class="f-20 f-md-24 w500 lh140 w400 mt30 text-center">
                     Running Paid Ads For Lead Generation
                  </div>
               </div>
               <div class="col-12 col-md-4 mt20 mt-md50">
                  <div class="potential-block">
                     <img src="assets/images/qsn-7.webp" class="img-fluid mx-auto d-block">                    
                  </div>
                  <div class="f-20 f-md-24 w500 lh140 w400 mt30 text-center">
                     Selling Recurring Memberships
                  </div>
               </div>
               <div class="col-12 col-md-4 mt20 mt-md50">
                  <div class="potential-block">
                     <img src="assets/images/qsn-2.webp" class="img-fluid mx-auto d-block">                    
                  </div>
                  <div class="f-20 f-md-24 w500 lh140 w400 mt30 text-center">
                     E-Com Sellers
                  </div>
               </div>
               <div class="col-12 col-md-4 mt20 mt-md50">
                  <div class="potential-block">
                     <img src="assets/images/qsn-4.webp" class="img-fluid mx-auto d-block">                    
                  </div>
                  <div class="f-20 f-md-24 w500 lh140 w400 mt30 text-center">
                     All Info Product Sellers
                  </div>
               </div>
               <div class="col-12 col-md-4 mt20 mt-md50">
                  <div class="potential-block">
                     <img src="assets/images/qsn-6.webp" class="img-fluid mx-auto d-block">                    
                  </div>
                  <div class="f-20 f-md-24 w500 lh140 w400 mt30 text-center">
                     Delivering Docs & Video Trainings To Customers
                  </div>
               </div>
               <div class="col-12 col-md-4 mt20 mt-md50">
                  <div class="potential-block">
                     <img src="assets/images/qsn-8.webp" class="img-fluid mx-auto d-block">                    
                  </div>
                  <div class="f-20 f-md-24 w500 lh140 w400 mt30 text-center">
                     Selling Freelancing Services
                  </div>
               </div>
               <div class="col-12 col-md-4 mt20 mt-md50">
                  <div class="potential-block">
                     <img src="assets/images/qsn-9.webp" class="img-fluid mx-auto d-block">                    
                  </div>
                  <div class="f-20 f-md-24 w500 lh140 w400 mt30 text-center">
                     Selling High Ticket Coachings/ Product Webinars
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- POTENTIAL SECTION END -->

      <!-- Deep Funnel Section Start -->
      <div class="deep-funnel-sec">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-28 f-md-50 lh140 w700 black-clr">
                     Our Deep & High Converting Sales Funnel
                  </div>
                  <img src="assets/images/funnel.webp" alt="Funnel" class="mx-auto d-block img-fluid  mt20 mt-md80">
               </div>
            </div>
         </div>
      </div>

      <!-- Exciting Launch Section Start -->
      <div class="exciting-launch">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-md-50 f-28 black-clr w700 lh140">
                  This Exciting Launch Event Is Divided Into 2 Phases
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md50 align-items-center">
               <div class="col-12 col-md-6">
                  <div class="f-24 f-md-34 lh140 black-clr w700">
                     To Make You Max Commissions
                  </div>
                  <ul class="launch-list f-md-20 f-18 w400 mb0 mt-md30 mt20 pl20">
                     <li>All Leads Are Hardcoded</li>
                     <li>We'll Re-Market Your Leads Heavily</li>
                     <li>Pitch Bundle Offer On Webinars</li>
                  </ul>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0">
                  <img src="assets/images/phase1.webp" class="img-fluid d-block mx-auto " alt="Phase1">
               </div>
            </div>
            <div class="row mt20 mt-md50 align-items-center">
               <div class="col-12 col-md-6 order-md-2">
                  <div class="f-24 f-md-34 lh140 black-clr w700">
                     Big Opening Contest & Bundle Offer
                  </div>
                  <ul class="launch-list f-md-20 f-18 w400 mb0 mt-md30 mt20 pl20">
                     <li>High in Demand Product with Top Conversion</li>
                     <li>Deep Funnel To Make You Double Digit EPCs</li>
                     <li>Earn Up To $415/Sale</li>
                     <li>Huge $10,000 JV Prizes</li>
                  </ul>
               </div>
               <div class="col-12 col-md-6 mt20 mt-md0 order-md-1">
                  <img src="assets/images/phase2.webp" class="img-fluid d-block mx-auto " alt="Phase2">
               </div>
            </div>
         </div>
      </div>

      <!-- Prize Value Section Start -->
      <div class="prize-value">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="f-md-50 f-28 w700 lh140 white-clr">Get Ready to Grab Your Share of</div>
                  <div class="f-md-72 f-40 w700 orange-clr lh140">$10,000 JV Prizes</div>
               </div>
            </div>
         </div>
      </div>

      <!-- Contest Section Start -->
      <div class="contest-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-6">
                  <img src="assets/images/prelaunch.webp" alt="Prelaunch" class="mx-auto d-block img-fluid ">
               </div>
               <div class="col-md-6 mt20 mt-md0">
                  <img src="assets/images/exiciting-launch.webp" alt="Exiciting" class="mx-auto d-block img-fluid ">
               </div>
            </div>
         </div>
      </div>

      <!-- Reciprocate Section Start -->
         <div class="reciprocate-sec">
            <div class="container">
               <div class="row">
                  <div class="col-12 text-center">
                     <div class="f-md-45 f-28 lh140 w600 orange-clr">
                        Do We Reciprocate?
                     </div>
                     <div class="f-18 f-md-20 lh140 mt20 w400 text-center black-clr">
                        We've been in top positions on hundreds of launch leaderboards &amp; sent huge sales for our valued JVs.
                     </div>
                     <div class="f-18 f-md-20 lh140 mt20 w400 text-center black-clr">
                        So, if you have a top-notch product with top conversions AND that fits our list, we would love to drive loads of sales for you. Here's just some results from our recent promotions.
                     </div>
                  </div>
                  <div class="col-12 col-md-10 mx-auto mt20 mt-md50">
                     <div class="logos-effect">
                        <img src="assets/images/logos.webp" class="img-fluid d-block mx-auto " alt="Logos">
                     </div>
                  </div>
                  <div class="col-12 f-md-28 f-24 lh140 mt20 mt-md50 w600 text-center">
                     And The List Goes On And On...
                  </div>
               </div>
            </div>
         </div>

      <!-- Contact Section Start -->
      <div class="contact-section">
         <div class="container">
            <div class="container-box">
               <div class="row">
                  <div class="col-12 col-md-12 text-center">
                     <div class="f-28 f-md-45 w600 white-clr lh140">
                        Have any Query? Contact us Anytime
                     </div>
                  </div>
               </div>
               <div class="row mt20 mt-md80 justify-content-between">
                  <div class="col-12 col-md-8 mx-auto">
                     <div class="row">
                        <div class="col-12 col-md-6">
                           <div class="contact-shape">
                              <img src="assets/images/amit-pareek.webp" class="img-fluid d-block mx-auto minus " alt="Amit Pareek">
                              <div class="f-24 f-md-30 w600  lh140 text-center white-clr mt20">
                                 Dr. Amit Pareek
                              </div>
                              <div class="col-12 mt30 d-flex justify-content-center">
                                 <a href="http://facebook.com/Dr.AmitPareek" class="link-text mr20">
                                 <img src="assets/images/am-fb.webp" class="center-block " alt="Facebook">
                                 </a>
                                 <a href="skype:amit.pareek77" class="link-text">
                                    <div class="col-12 ">
                                       <img src="assets/images/am-skype.webp" class="center-block " alt="Skype">
                                    </div>
                                 </a>
                              </div>
                           </div>
                        </div>
                        <div class="col-12 col-md-6 mt20 mt-md0">
                           <div class="contact-shape">
                              <img src="assets/images/atul-pareek.webp" class="img-fluid d-block mx-auto minus " alt=" Atul Pareek">
                              <div class="f-24 f-md-30 w600  lh140 text-center white-clr mt20">
                                  Atul Pareek
                              </div>
                              <div class="col-12 mt30 d-flex justify-content-center">
                                 <a href="https://www.facebook.com/profile.php?id=100076904623319" class="link-text mr20">
                                    <img src="assets/images/am-fb.webp" class="center-block " alt="Facebook">
                                 </a>
                                 <a href="skype:live:.cid.c3d2302c4a2815e0" class="link-text">
                                    <div class="col-12 ">
                                       <img src="assets/images/am-skype.webp" class="center-block " alt="Skype">
                                    </div>
                                 </a>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <!-- Term Section Start -->
      <div class="term-sec">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="f-28 f-md-45 lh140 w600 black-clr text-center">
                     Affiliate Promotions Terms & Conditions
                  </div>
                  <div class="f-16 f-md-16 lh140 p-md0 mt-md20 mt20 w400 text-center">
                     YOU MUST READ AND AGREE TO THESE AFFILIATE TERMS before requesting your affiliate link and being a part of this launch. Violation of ANY of these terms is cause for immediate termination and instant removal from this launch and any other of our launches - current or past -- and you agree that your current commissions will be forfeited without recourse and you may be banned from our future launches. Some violations may also be cause for LEGAL ACTIONS.:
                  </div>
                  <ul class="terms-list pl0 m0 f-16 f-md-16 lh140 w400 mt20 mt-md50">
                     <li>1). All email contacts MUST be your OWN opt in email list. You cannot send to lists that have been purchased or “gifted” from other vendors, buy solo ads, use safe lists, or obtained by illegal means. Email lists that are not your own are considered spam.</li>
                     <li>2) You may NOT create social media pages NOR purchase domain names with the PRODUCT NAME or BRAND NAME AND you may NOT use the PRODUCT NAME or the NAME OF THE VENDOR as your “from” in your emails instead of your own name AS IF YOU ARE THE PRODUCT OWNER. This is IMPERSONATION and will not be tolerated.</li>
                     <li>3) You may NOT purchase domain name(s) with the same or similar name as the PRODUCT NAME or BRAND NAME nor CLONE or otherwise copy our site and use that site to sell our product as your own. Furthermore, you may not add our product - whether purchased through us or obtained in any other manner - and sell or offer it on any type of “membership” site where multiple people have access to this product for any kind of fee or arrangement. This constitutes theft of our intellectual property rights and considered FRAUDULENT and is cause for LEGAL ACTIONS. </li>
                     <li>4) You may not encourage nor ask for or show a person HOW TO REFUND their purchase from another affiliate in order for them to purchase the same product through you.</li>
                     <li>5) You may not post OTO links on Review Sites because this will lead to confusion and refunds for those people who do not purchase the FE first and end up with NO LOGIN &amp; NO software or main product. </li>
                     <li>6) You may not use "negative" campaigns such as "is Product Name / Owner Name a scam?" or any other method to attract controversial click thru rates that an ordinary person would deem to portray a negative view of the product. You may not use offensive nor negative domain names.</li>
                     <li>7) You may not use misleading claims, inaccurate information or false testimonials (or anything that does not comply with FTC guidelines).</li>
                     <li>8) You may not use gray-hat/black-hat marketing practices to drive sales or for any other reason.</li>
                     <li>9) You may not give cash rebates of any kind as it may increase refund rates.</li>
                     <li>10) You may not purchase from your own affiliate link. Any 'self' purchase commission may be nullified or held back.</li>
                  </ul>
                  <div class="f-16 f-md-16 lh140 mt20 w400">
                     <span class="w600">NOTE:</span> These terms may change at any time without notice. (Please check back here regularly).
                  </div>
                  <div class="f-16 f-md-16 lh140 mt20 w400">
                     <span class="w600">NOTE:</span> Affiliate payments will be set according to the platform rules.
                  </div>     
                  <div class="f-16 f-md-16 lh140 mt20 w400">
                     <span class="w600">CAUTION:</span> Do not send "raw" affiliate links. Utilize redirect links in emails &amp; website campaigns instead of your direct affiliate link. This increases conversions for both of us.
                  </div> 
                  <div class="f-16 f-md-16 lh140 mt20 w400">
                     We run a legitimate business, which means that we always correctly illustrate and represent our products and their features and benefits to the customer.
                  </div>
                  <div class="f-16 f-md-16 lh140 mt20 w400">
                     Please make sure you do the same.
                  </div>
               </div>
            </div>
         </div>
      </div>

      <!--Footer Section Start -->
      <div class="footer-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
               <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 926.8 148.17" style="max-height:50px">
                     <defs>
                        <style>
                           .cls-1{fill:#0068ff;}
                           .cls-2{fill:#f89b1c;}
                           .cls-3{fill:#fff;}
                        </style>
                     </defs>
                     <g id="Layer_2" data-name="Layer 2">
                     <g id="Layer_1-2" data-name="Layer 1">
                     <path class="cls-1" d="M41.41,108.34a22.49,22.49,0,0,1-2.87-11c0-12.08,9.69-22.18,22.05-23l16.06-1.06L72.4,58.17a35,35,0,0,1-1.31-9.5c0-19.78,16.54-35.86,36.86-35.86a37.33,37.33,0,0,1,24.89,9.41,35.39,35.39,0,0,1,11.81,23.06l.59,6.21c4.11-3.79,8.07-7.79,11.84-12C152.67,17,132.35,0,108,0c-27.63,0-50,21.79-50,48.67A47.76,47.76,0,0,0,59.7,61.56c-19.17,1.26-34.33,16.8-34.33,35.77a35,35,0,0,0,3.28,14.81C32.55,111.06,36.83,109.79,41.41,108.34Z"/>
                     <path class="cls-2" d="M165.9,50.66c10.88-20.3,14.58-35.47,14.58-35.47C142.66,114.85,2.87,139.07,0,140H163.24c25.39,0,46-20,46-44.71C209.2,71.47,190,52,165.9,50.66Zm-2.66,76.53H91.94a188.75,188.75,0,0,0,66.3-63.45l1.07-.12a34.8,34.8,0,0,1,3.93-.23c18.09,0,32.8,14.31,32.8,31.9S181.33,127.19,163.24,127.19Z"/><text/>
                     <path class="cls-3" d="M313.48,73.63H295.09A18.17,18.17,0,0,0,293,67.27a16.13,16.13,0,0,0-4-4.77,17.68,17.68,0,0,0-5.6-3,21.68,21.68,0,0,0-6.86-1A20.48,20.48,0,0,0,265,61.73a21.33,21.33,0,0,0-7.68,9.57,38.07,38.07,0,0,0-2.73,15.22A38.43,38.43,0,0,0,257.29,102a19.88,19.88,0,0,0,19.16,12.64,22.63,22.63,0,0,0,6.78-1,17.28,17.28,0,0,0,5.56-2.83,15.92,15.92,0,0,0,4-4.55,18.16,18.16,0,0,0,2.25-6.13l18.39.08a33.44,33.44,0,0,1-3.55,11.4,34.43,34.43,0,0,1-7.6,9.78,35.64,35.64,0,0,1-11.33,6.8,41.64,41.64,0,0,1-14.84,2.5,40.45,40.45,0,0,1-20.55-5.21,36.72,36.72,0,0,1-14.27-15.07q-5.24-9.87-5.23-23.88t5.29-23.93a37,37,0,0,1,14.36-15.05,40.29,40.29,0,0,1,20.4-5.18A44.26,44.26,0,0,1,290,44.45a35.54,35.54,0,0,1,11.36,6.11,32.24,32.24,0,0,1,8.08,9.8A36.94,36.94,0,0,1,313.48,73.63Z"/>
                     <path class="cls-3" d="M344.08,43.53v86H326.19v-86Z"/>
                     <path class="cls-3" d="M387.48,130.76a32.77,32.77,0,0,1-16.9-4.18,28.15,28.15,0,0,1-11-11.67,37.37,37.37,0,0,1-3.87-17.4A37.59,37.59,0,0,1,359.61,80a28.15,28.15,0,0,1,11-11.67,36.25,36.25,0,0,1,33.8,0,28.15,28.15,0,0,1,11,11.67,37.59,37.59,0,0,1,3.86,17.48,37.37,37.37,0,0,1-3.86,17.4,28.15,28.15,0,0,1-11,11.67A32.77,32.77,0,0,1,387.48,130.76Zm.08-13.85a11.05,11.05,0,0,0,7.43-2.54,15.66,15.66,0,0,0,4.52-7,33.67,33.67,0,0,0,0-20.07,15.81,15.81,0,0,0-4.52-7,11,11,0,0,0-7.43-2.56A11.29,11.29,0,0,0,380,80.39a15.48,15.48,0,0,0-4.58,7,33.67,33.67,0,0,0,0,20.07,15.34,15.34,0,0,0,4.58,7A11.38,11.38,0,0,0,387.56,116.91Z"/>
                     <path class="cls-3" d="M472.23,102.05V65h17.88V129.5H473V117.79h-.68A19.08,19.08,0,0,1,465,126.9a21.41,21.41,0,0,1-12.32,3.44,21.69,21.69,0,0,1-11.37-2.94,20.09,20.09,0,0,1-7.67-8.35,28.77,28.77,0,0,1-2.79-13V65h17.89v37.87c0,3.8,1.05,6.81,3.06,9a10.41,10.41,0,0,0,8.1,3.32,12.78,12.78,0,0,0,6.05-1.49,11.58,11.58,0,0,0,4.55-4.43A13.75,13.75,0,0,0,472.23,102.05Z"/>
                     <path class="cls-3" d="M528.14,130.55a24.16,24.16,0,0,1-13.28-3.8,25.78,25.78,0,0,1-9.4-11.21Q502,108.13,502,97.35q0-11.09,3.57-18.45a25.73,25.73,0,0,1,9.51-11,24.29,24.29,0,0,1,13-3.68,20,20,0,0,1,9,1.83A18.15,18.15,0,0,1,543,70.54a24.43,24.43,0,0,1,3.42,5.31H547V43.53h17.84v86H547.2V119.17h-.75a23.05,23.05,0,0,1-3.55,5.32,18.09,18.09,0,0,1-5.92,4.34A20.5,20.5,0,0,1,528.14,130.55Zm5.67-14.23a11.47,11.47,0,0,0,7.33-2.37,14.89,14.89,0,0,0,4.61-6.66,28.24,28.24,0,0,0,1.62-10,28.22,28.22,0,0,0-1.6-10,14.33,14.33,0,0,0-4.61-6.55,11.77,11.77,0,0,0-7.35-2.31,11.58,11.58,0,0,0-7.43,2.4,14.57,14.57,0,0,0-4.57,6.63,28.38,28.38,0,0,0-1.56,9.82,28.82,28.82,0,0,0,1.58,9.93,15,15,0,0,0,4.57,6.72A11.44,11.44,0,0,0,533.81,116.32Z"/>
                     <path class="cls-3" d="M582.76,129.5v-86H634.3v9.24H593.17v29h37.27v9.24H593.17V129.5Z"/>
                     <path class="cls-3" d="M687.36,103.14V65h9.91V129.5h-9.91V118.59h-.67a21,21,0,0,1-7.05,8.33q-4.78,3.42-12.09,3.42a21.41,21.41,0,0,1-10.75-2.67,18.33,18.33,0,0,1-7.38-8q-2.7-5.4-2.69-13.63V65h9.91v40.3q0,7.05,4,11.25a13.35,13.35,0,0,0,10.14,4.2,17,17,0,0,0,7.54-1.89,16.58,16.58,0,0,0,6.46-5.79A17.49,17.49,0,0,0,687.36,103.14Z"/>
                     <path class="cls-3" d="M761.08,79.46,752.18,82a17.11,17.11,0,0,0-2.46-4.34,12.34,12.34,0,0,0-4.39-3.51,15.87,15.87,0,0,0-7.09-1.38,16.94,16.94,0,0,0-9.84,2.7,8.09,8.09,0,0,0-3.93,6.87,7.05,7.05,0,0,0,2.69,5.83q2.69,2.14,8.39,3.57l9.57,2.35q8.65,2.1,12.89,6.4a15.09,15.09,0,0,1,4.24,11.06,16.46,16.46,0,0,1-3.17,9.91,21.35,21.35,0,0,1-8.83,6.89,32.4,32.4,0,0,1-13.19,2.51q-9.85,0-16.33-4.28a19,19,0,0,1-8.18-12.51l9.4-2.35a13.09,13.09,0,0,0,5.1,7.81q3.76,2.59,9.85,2.6,6.92,0,11-3c2.72-2,4.09-4.34,4.09-7.11a7.61,7.61,0,0,0-2.35-5.65,15.26,15.26,0,0,0-7.22-3.42l-10.75-2.52q-8.85-2.1-13-6.53a15.61,15.61,0,0,1-4.14-11.1,15.82,15.82,0,0,1,3.09-9.65,20.63,20.63,0,0,1,8.44-6.59,29.41,29.41,0,0,1,12.15-2.4q9.57,0,15,4.2A22.7,22.7,0,0,1,761.08,79.46Z"/>
                     <path class="cls-3" d="M782.23,54.28a6.94,6.94,0,0,1-5-2,6.44,6.44,0,0,1,0-9.48,7.24,7.24,0,0,1,10,0,6.46,6.46,0,0,1,0,9.48A7,7,0,0,1,782.23,54.28Zm-5,75.22V65h9.91V129.5Z"/>
                     <path class="cls-3" d="M831.43,130.84a27.22,27.22,0,0,1-25.54-15.78,39.11,39.11,0,0,1-3.68-17.46A39.59,39.59,0,0,1,805.89,80,28.25,28.25,0,0,1,857,80a39.59,39.59,0,0,1,3.68,17.59A39.11,39.11,0,0,1,857,115.06a27.22,27.22,0,0,1-25.54,15.78Zm0-8.89a17,17,0,0,0,10.91-3.41,19.92,19.92,0,0,0,6.34-8.94,34.21,34.21,0,0,0,2.06-12,34.62,34.62,0,0,0-2.06-12,20.31,20.31,0,0,0-6.34-9,19,19,0,0,0-21.83,0,20.37,20.37,0,0,0-6.33,9,34.39,34.39,0,0,0-2.06,12,34,34,0,0,0,2.06,12,20,20,0,0,0,6.33,8.94A17,17,0,0,0,831.43,122Z"/>
                     <path class="cls-3" d="M885.66,90.71V129.5h-9.9V65h9.57V75.1h.84a18.47,18.47,0,0,1,6.88-7.91q4.62-3,11.92-3a23.6,23.6,0,0,1,11.46,2.67,18.21,18.21,0,0,1,7.64,8.06c1.82,3.59,2.73,8.14,2.73,13.62v41h-9.9V89.2q0-7.59-3.95-11.86t-10.83-4.26a17.2,17.2,0,0,0-8.46,2.06,14.67,14.67,0,0,0-5.86,6A19.9,19.9,0,0,0,885.66,90.71Z"/>
                     <text/>
                  </g>
               </g>
               </svg>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md60">
                  <div class="f-md-18 f-16 w400 lh140 white-clr text-xs-center">Copyright © CloudFusion 2023</div>
                  <ul class="footer-ul w400 f-md-18 f-16 white-clr text-center text-md-right">
                     <li><a href="https://support.oppyo.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://getcloudfusion.co/legal/privacy-policy.html " class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://getcloudfusion.co/legal/terms-of-service.html " class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://getcloudfusion.co/legal/disclaimer.html " class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://getcloudfusion.co/legal/gdpr.html " class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://getcloudfusion.co/legal/dmca.html " class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://getcloudfusion.co/legal/anti-spam.html " class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!--Footer Section End -->
      <!-- timer --->
      <?php
         if ($now < $exp_date) {
         
         ?>
      <script type="text/javascript">
         // Count down milliseconds = server_end - server_now = client_end - client_now
         var server_end = <?php echo $exp_date; ?> * 1000;
         var server_now = <?php echo time(); ?> * 1000;
         var client_now = new Date().getTime();
         var end = server_end - server_now + client_now; // this is the real end time
         
         var noob = $('.countdown').length;
         
         var _second = 1000;
         var _minute = _second * 60;
         var _hour = _minute * 60;
         var _day = _hour * 24
         var timer;
         
         function showRemaining() {
             var now = new Date();
             var distance = end - now;
             if (distance < 0) {
                 clearInterval(timer);
                 document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
                 return;
             }
         
             var days = Math.floor(distance / _day);
             var hours = Math.floor((distance % _day) / _hour);
             var minutes = Math.floor((distance % _hour) / _minute);
             var seconds = Math.floor((distance % _minute) / _second);
             if (days < 10) {
                 days = "0" + days;
             }
             if (hours < 10) {
                 hours = "0" + hours;
             }
             if (minutes < 10) {
                 minutes = "0" + minutes;
             }
             if (seconds < 10) {
                 seconds = "0" + seconds;
             }
             var i;
             var countdown = document.getElementsByClassName('countdown');
             for (i = 0; i < noob; i++) {
                 countdown[i].innerHTML = '';
         
                 if (days) {
                     countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-30 f-md-40 timerbg">' + days + '</span><br><span class="f-16 w500">Days</span> </div>';
                 }
         
                 countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-30 f-md-40 timerbg">' + hours + '</span><br><span class="f-16 w500">Hours</span> </div>';
         
                 countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-30 f-md-40 timerbg">' + minutes + '</span><br><span class="f-16 w500">Mins</span> </div>';
         
                 countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-30 f-md-40 timerbg">' + seconds + '</span><br><span class="f-16 w500">Sec</span> </div>';
             }
         
         }
         timer = setInterval(showRemaining, 1000);
      </script>    

      <?php
         } else {
         echo "Times Up";
         }
         ?>

<script type="text/javascript">
// Special handling for in-app browsers that don't always support new windows
(function() {
    function browserSupportsNewWindows(userAgent) {
        var rules = [
            'FBIOS',
            'Twitter for iPhone',
            'WebView',
            '(iPhone|iPod|iPad)(?!.*Safari\/)',
            'Android.*(wv|\.0\.0\.0)'
        ];
        var pattern = new RegExp('(' + rules.join('|') + ')', 'ig');
        return !pattern.test(userAgent);
    }

    if (!browserSupportsNewWindows(navigator.userAgent || navigator.vendor || window.opera)) {
        document.getElementById('af-form-155455963').parentElement.removeAttribute('target');
    }
})();
</script>
<script type="text/javascript">
 
    (function() {
        var IE = /*@cc_on!@*/false;
        if (!IE) { return; }
        if (document.compatMode && document.compatMode == 'BackCompat') {
            if (document.getElementById("af-form-155455963")) {
                document.getElementById("af-form-155455963").className = 'af-form af-quirksMode';
            }
            if (document.getElementById("af-body-155455963")) {
                document.getElementById("af-body-155455963").className = "af-body inline af-quirksMode";
            }
            if (document.getElementById("af-header-155455963")) {
                document.getElementById("af-header-155455963").className = "af-header af-quirksMode";
            }
            if (document.getElementById("af-footer-155455963")) {
                document.getElementById("af-footer-155455963").className = "af-footer af-quirksMode";
            }
        }
    })();
 
</script>
   </body>
</html>
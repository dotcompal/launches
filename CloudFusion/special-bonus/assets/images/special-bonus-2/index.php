<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <!-- Tell the browser to be responsive to screen width -->
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
      <meta name="title" content="CloudFusion Bonuses">
      <meta name="description" content="Grab My 20 Exclusive Bonuses Before the Deal Ends...">
      <meta name="keywords" content="CloudFusion Bonuses">
      <meta property="og:image" content="https://www.getcloudfusion.co/special-bonus/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Dr. Amit Pareek">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="CloudFusion Bonuses">
      <meta property="og:description" content="Grab My 20 Exclusive Bonuses Before the Deal Ends...">
      <meta property="og:image" content="https://www.getcloudfusion.co/special-bonus/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="CloudFusion Bonuses">
      <meta property="twitter:description" content="Grab My 20 Exclusive Bonuses Before the Deal Ends...">
      <meta property="twitter:image" content="https://www.getcloudfusion.co/special-bonus/thumbnail.png">
      <title>CloudFusion Bonuses</title>
      <!-- Shortcut Icon  -->
      <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
      <!-- Start Editor required -->
      <link rel="stylesheet" href="../common_assets/css/bootstrap.min.css" type="text/css">
      <link rel="stylesheet" type="text/css" href="assets/css/bonus-style.css">
      <link href="https://fonts.googleapis.com/css2?family=Kalam:wght@300;400;700&family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
      <script src="../common_assets/js/jquery.min.js"></script>
   </head>
   <body>
       <!-- New Timer  Start-->
       <?php
         $date = 'August 15 2023 11:59 AM EST';
         $exp_date = strtotime($date);
         $now = time();  

         if ($now < $exp_date) {
         ?>
      <?php
         } else {
         	echo "Times Up";
         }
         ?>
      <!-- New Timer End -->
      <?php
         if(!isset($_GET['afflink'])){
         $_GET['afflink'] = 'https://getcloudfusion.co/special/';
         $_GET['name'] = 'Dr. Amit Pareek';      
         }
         else {
             $_GET['afflinkbundle'] = 'https://getcloudfusion.co/bundle/'.$_GET['afflink'];
             $fe_id_array=explode("/",$_GET['afflink']); 
             $bundle_id="398672";
             $_GET['afflinkbundle']=str_replace($fe_id_array[5],$bundle_id,$_GET['afflink']);
         }
         ?>
          <?php
         if(!isset($_GET['afflinkbundle'])){
         $_GET['afflinkbundle'] = 'https://getcloudfusion.co/bundle/';
         $_GET['name'] = 'Dr. Amit Pareek';      
         }
         ?>
      <!-- Header Section Start -->   
      <div class="main-header">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="text-center">
                     <div class="f-md-32 f-20 lh140 w400 white-clr d-block d-md-flex align-items-center justify-content-center">
                        <span class="w600 orange-clr2"><?php echo $_GET['name'];?>'s</span> &nbsp;special bonus for &nbsp;
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 926.8 148.17" style="max-height:55px;"><defs><style>.cls-1b{fill:#0068ff;}.cls-2{fill:#f89b1c;}.cls-3w{fill:#fff;}</style></defs><title>Asset 12</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1b" d="M41.41,108.34a22.49,22.49,0,0,1-2.87-11c0-12.08,9.69-22.18,22.05-23l16.06-1.06L72.4,58.17a35,35,0,0,1-1.31-9.5c0-19.78,16.54-35.86,36.86-35.86a37.33,37.33,0,0,1,24.89,9.41,35.39,35.39,0,0,1,11.81,23.06l.59,6.21c4.11-3.79,8.07-7.79,11.84-12C152.67,17,132.35,0,108,0c-27.63,0-50,21.79-50,48.67A47.76,47.76,0,0,0,59.7,61.56c-19.17,1.26-34.33,16.8-34.33,35.77a35,35,0,0,0,3.28,14.81C32.55,111.06,36.83,109.79,41.41,108.34Z"></path><path class="cls-2" d="M165.9,50.66c10.88-20.3,14.58-35.47,14.58-35.47C142.66,114.85,2.87,139.07,0,140H163.24c25.39,0,46-20,46-44.71C209.2,71.47,190,52,165.9,50.66Zm-2.66,76.53H91.94a188.75,188.75,0,0,0,66.3-63.45l1.07-.12a34.8,34.8,0,0,1,3.93-.23c18.09,0,32.8,14.31,32.8,31.9S181.33,127.19,163.24,127.19Z"></path><text></text><path class="cls-3w" d="M313.48,73.63H295.09A18.17,18.17,0,0,0,293,67.27a16.13,16.13,0,0,0-4-4.77,17.68,17.68,0,0,0-5.6-3,21.68,21.68,0,0,0-6.86-1A20.48,20.48,0,0,0,265,61.73a21.33,21.33,0,0,0-7.68,9.57,38.07,38.07,0,0,0-2.73,15.22A38.43,38.43,0,0,0,257.29,102a19.88,19.88,0,0,0,19.16,12.64,22.63,22.63,0,0,0,6.78-1,17.28,17.28,0,0,0,5.56-2.83,15.92,15.92,0,0,0,4-4.55,18.16,18.16,0,0,0,2.25-6.13l18.39.08a33.44,33.44,0,0,1-3.55,11.4,34.43,34.43,0,0,1-7.6,9.78,35.64,35.64,0,0,1-11.33,6.8,41.64,41.64,0,0,1-14.84,2.5,40.45,40.45,0,0,1-20.55-5.21,36.72,36.72,0,0,1-14.27-15.07q-5.24-9.87-5.23-23.88t5.29-23.93a37,37,0,0,1,14.36-15.05,40.29,40.29,0,0,1,20.4-5.18A44.26,44.26,0,0,1,290,44.45a35.54,35.54,0,0,1,11.36,6.11,32.24,32.24,0,0,1,8.08,9.8A36.94,36.94,0,0,1,313.48,73.63Z"></path><path class="cls-3w" d="M344.08,43.53v86H326.19v-86Z"></path><path class="cls-3w" d="M387.48,130.76a32.77,32.77,0,0,1-16.9-4.18,28.15,28.15,0,0,1-11-11.67,37.37,37.37,0,0,1-3.87-17.4A37.59,37.59,0,0,1,359.61,80a28.15,28.15,0,0,1,11-11.67,36.25,36.25,0,0,1,33.8,0,28.15,28.15,0,0,1,11,11.67,37.59,37.59,0,0,1,3.86,17.48,37.37,37.37,0,0,1-3.86,17.4,28.15,28.15,0,0,1-11,11.67A32.77,32.77,0,0,1,387.48,130.76Zm.08-13.85a11.05,11.05,0,0,0,7.43-2.54,15.66,15.66,0,0,0,4.52-7,33.67,33.67,0,0,0,0-20.07,15.81,15.81,0,0,0-4.52-7,11,11,0,0,0-7.43-2.56A11.29,11.29,0,0,0,380,80.39a15.48,15.48,0,0,0-4.58,7,33.67,33.67,0,0,0,0,20.07,15.34,15.34,0,0,0,4.58,7A11.38,11.38,0,0,0,387.56,116.91Z"></path><path class="cls-3w" d="M472.23,102.05V65h17.88V129.5H473V117.79h-.68A19.08,19.08,0,0,1,465,126.9a21.41,21.41,0,0,1-12.32,3.44,21.69,21.69,0,0,1-11.37-2.94,20.09,20.09,0,0,1-7.67-8.35,28.77,28.77,0,0,1-2.79-13V65h17.89v37.87c0,3.8,1.05,6.81,3.06,9a10.41,10.41,0,0,0,8.1,3.32,12.78,12.78,0,0,0,6.05-1.49,11.58,11.58,0,0,0,4.55-4.43A13.75,13.75,0,0,0,472.23,102.05Z"></path><path class="cls-3w" d="M528.14,130.55a24.16,24.16,0,0,1-13.28-3.8,25.78,25.78,0,0,1-9.4-11.21Q502,108.13,502,97.35q0-11.09,3.57-18.45a25.73,25.73,0,0,1,9.51-11,24.29,24.29,0,0,1,13-3.68,20,20,0,0,1,9,1.83A18.15,18.15,0,0,1,543,70.54a24.43,24.43,0,0,1,3.42,5.31H547V43.53h17.84v86H547.2V119.17h-.75a23.05,23.05,0,0,1-3.55,5.32,18.09,18.09,0,0,1-5.92,4.34A20.5,20.5,0,0,1,528.14,130.55Zm5.67-14.23a11.47,11.47,0,0,0,7.33-2.37,14.89,14.89,0,0,0,4.61-6.66,28.24,28.24,0,0,0,1.62-10,28.22,28.22,0,0,0-1.6-10,14.33,14.33,0,0,0-4.61-6.55,11.77,11.77,0,0,0-7.35-2.31,11.58,11.58,0,0,0-7.43,2.4,14.57,14.57,0,0,0-4.57,6.63,28.38,28.38,0,0,0-1.56,9.82,28.82,28.82,0,0,0,1.58,9.93,15,15,0,0,0,4.57,6.72A11.44,11.44,0,0,0,533.81,116.32Z"></path><path class="cls-3w" d="M582.76,129.5v-86H634.3v9.24H593.17v29h37.27v9.24H593.17V129.5Z"></path><path class="cls-3w" d="M687.36,103.14V65h9.91V129.5h-9.91V118.59h-.67a21,21,0,0,1-7.05,8.33q-4.78,3.42-12.09,3.42a21.41,21.41,0,0,1-10.75-2.67,18.33,18.33,0,0,1-7.38-8q-2.7-5.4-2.69-13.63V65h9.91v40.3q0,7.05,4,11.25a13.35,13.35,0,0,0,10.14,4.2,17,17,0,0,0,7.54-1.89,16.58,16.58,0,0,0,6.46-5.79A17.49,17.49,0,0,0,687.36,103.14Z"></path><path class="cls-3w" d="M761.08,79.46,752.18,82a17.11,17.11,0,0,0-2.46-4.34,12.34,12.34,0,0,0-4.39-3.51,15.87,15.87,0,0,0-7.09-1.38,16.94,16.94,0,0,0-9.84,2.7,8.09,8.09,0,0,0-3.93,6.87,7.05,7.05,0,0,0,2.69,5.83q2.69,2.14,8.39,3.57l9.57,2.35q8.65,2.1,12.89,6.4a15.09,15.09,0,0,1,4.24,11.06,16.46,16.46,0,0,1-3.17,9.91,21.35,21.35,0,0,1-8.83,6.89,32.4,32.4,0,0,1-13.19,2.51q-9.85,0-16.33-4.28a19,19,0,0,1-8.18-12.51l9.4-2.35a13.09,13.09,0,0,0,5.1,7.81q3.76,2.59,9.85,2.6,6.92,0,11-3c2.72-2,4.09-4.34,4.09-7.11a7.61,7.61,0,0,0-2.35-5.65,15.26,15.26,0,0,0-7.22-3.42l-10.75-2.52q-8.85-2.1-13-6.53a15.61,15.61,0,0,1-4.14-11.1,15.82,15.82,0,0,1,3.09-9.65,20.63,20.63,0,0,1,8.44-6.59,29.41,29.41,0,0,1,12.15-2.4q9.57,0,15,4.2A22.7,22.7,0,0,1,761.08,79.46Z"></path><path class="cls-3w" d="M782.23,54.28a6.94,6.94,0,0,1-5-2,6.44,6.44,0,0,1,0-9.48,7.24,7.24,0,0,1,10,0,6.46,6.46,0,0,1,0,9.48A7,7,0,0,1,782.23,54.28Zm-5,75.22V65h9.91V129.5Z"></path><path class="cls-3w" d="M831.43,130.84a27.22,27.22,0,0,1-25.54-15.78,39.11,39.11,0,0,1-3.68-17.46A39.59,39.59,0,0,1,805.89,80,28.25,28.25,0,0,1,857,80a39.59,39.59,0,0,1,3.68,17.59A39.11,39.11,0,0,1,857,115.06a27.22,27.22,0,0,1-25.54,15.78Zm0-8.89a17,17,0,0,0,10.91-3.41,19.92,19.92,0,0,0,6.34-8.94,34.21,34.21,0,0,0,2.06-12,34.62,34.62,0,0,0-2.06-12,20.31,20.31,0,0,0-6.34-9,19,19,0,0,0-21.83,0,20.37,20.37,0,0,0-6.33,9,34.39,34.39,0,0,0-2.06,12,34,34,0,0,0,2.06,12,20,20,0,0,0,6.33,8.94A17,17,0,0,0,831.43,122Z"></path><path class="cls-3w" d="M885.66,90.71V129.5h-9.9V65h9.57V75.1h.84a18.47,18.47,0,0,1,6.88-7.91q4.62-3,11.92-3a23.6,23.6,0,0,1,11.46,2.67,18.21,18.21,0,0,1,7.64,8.06c1.82,3.59,2.73,8.14,2.73,13.62v41h-9.9V89.2q0-7.59-3.95-11.86t-10.83-4.26a17.2,17.2,0,0,0-8.46,2.06,14.67,14.67,0,0,0-5.86,6A19.9,19.9,0,0,0,885.66,90.71Z"></path><text></text></g></g></svg>
                     </div>
                  </div>
                  <div class="col-12 mt20 mt-md50 text-center">
                     <div class="preheadline f-20 f-md-22 w500 lh140">
                     Grab My 20 Exclusive Bonuses Before the Deal Ends...
                     </div>
                  </div>
                  <div class="col-12 mt20 mt-md50 relative">
                     <div class="mainheadline f-md-45 f-28 w400 text-center black-clr lh140">
                     <span class="w700 underline-text"> Host UNLIMITED Videos, Website Images,</span> <span class="w700 underline-text"> Training, Audios or Any Media Files </span> at at Blazing-Fast Speed at A Low One Time Fee
                     </div>
                  </div>
                  <div class="col-12 mt-md40 mt20 f-18 f-md-24 w400 text-center lh150 white-clr text-capitalize">
                     And… Supercharge Your or Client’s Websites, Apps &amp; Pages with Fast-Loading Media Content.<br class="d-none d-md-block"> NO Tech Hassles &amp; Monthly Fee Ever…
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md40">
               <div class="col-md-10 col-12 mx-auto">
                  <!-- <div class="col-12 responsive-video border-video">
                     <iframe src="https://yoseller.dotcompal.com/video/mydrive/182331" style="width:100%; height:100%" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                  </div> -->
                  <img src="assets/images/product-box.webp" class="img-fluid mx-auto d-block" alt="ProductBox">
               </div>
            </div>
         </div>
      </div>
      <!-- Header Section End -->
     

      <div class="step-section">
         <div class="container">
            <div class="row">
               <div class="col-lg-12">
                  <div class="f-md-40 f-28 w400 lh140 text-center black-clr">
                     Captivate Your Audience with Your Media Content<br class="d-none d-md-block"><span class="w600"> In 3 Easy Steps...</span>
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md70">
               <div class="col-md-4 col-12 relative">
                  <div class="step-box">
                     <div class="step-title f-20 f-md-22 w600">step 1</div>
                     <div class="d-flex align-items-center mt30">
                        <div class="step-icon mr20">
                           <img src="https://cdn.oppyo.com/launches/yodrive/special/upload.webp " class="img-fluid d-block" alt="step one">
                        </div>
                        <div class="f-22 f-md-24 w600 black-clr">Upload</div>
                     </div>
                     <div class="w400 f-16 f-md-18 lh140 mt25">
                        Simply drag and drop your files into CloudFusion or upload from your PC. It supports almost all type of files - videos, Images, audios, documents etc.
                     </div>
                  </div>
                  <img src="https://cdn.oppyo.com/launches/yodrive/special/step-arrow.webp" class="img-fluid d-none d-md-block step-arrow" alt="step Arrow">
               </div>
               <div class="col-md-4 col-12 relative mt20 mt-md0">
                  <div class="step-box">
                     <div class="step-title f-20 f-md-22 w600">step 2</div>
                     <div class="d-flex align-items-center mt30">
                        <div class="step-icon mr20">
                           <img src="https://cdn.oppyo.com/launches/yodrive/special/code.webp " class="img-fluid d-block" alt="step two">
                        </div>
                        <div class="f-22 f-md-24 w600 black-clr">Get File URL</div>
                     </div>
                     <div class="w400 f-16 f-md-18 lh140 mt25">
                        CloudFusion gives you a single line of code to share your media anywhere after optimizing it according to internet speed &amp; make multiple resolutions for faster delivery on any device.
                     </div>
                  </div>
                  <img src="https://cdn.oppyo.com/launches/yodrive/special/step-arrow1.webp " class="img-fluid d-none d-md-block step-arrow" alt="step Arrow">
               </div>
               <div class="col-md-4 col-12 mt20 mt-md0">
                  <div class="step-box">
                     <div class="step-title f-20 f-md-22 w600">step 3</div>
                     <div class="d-flex align-items-center mt30">
                        <div class="step-icon mr20">
                           <img src="https://cdn.oppyo.com/launches/yodrive/special/publish.webp " class="img-fluid d-block" alt="step three">
                        </div>
                        <div class="f-22 f-md-24 w600 black-clr">Publish &amp; Profit</div>
                     </div>
                     <div class="w400 f-16 f-md-18 lh140 mt25">
                        Just paste the code on any page or get a secure DFY file sharing URL to publish your content anywhere online to start getting eyeballs and get paid.
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      



       <!-- CTA Btn Start-->
       <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700  orange-clr2">"Cloud"</span> for an Additional <span class="w700  orange-clr2">$10 Discount</span> on CloudFusion
                  </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn px-md80">
                  <span class="text-center">Grab CloudFusion + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                  Use Coupon Code <span class="w700  orange-clr2">"CloudFusion" </span> for an Additional <span class="w700  orange-clr2">$50 Discount</span> on Bundle Deal
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflinkbundle']; ?>" class="text-center bonusbuy-btn px-md80">
                  <span class="text-center">Grab CloudFusion Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
                <!-- Timer -->
                <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 smmltd">Days</span>
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Sec</span>
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Btn End -->

      <!-- Features Section Start -->
      <div class="list-section">
         <div class="container">
            <div class="row">
               <div class="f-md-45 f-28 w400 lh140 text-capitalize text-center black-clr">
                  “We Guarantee, CloudFusion is Going to Be The <span class="w700 orange-clr2"> 
                     <br class="d-none d-md-block">Last Cloud Hosting App You’ll Ever Need”</span>
               </div>
               <div class="col-12 mt20 mt-md50">
                  <div class="row header-list-block gx-md-5">
                     <div class="col-12 col-md-6">
                        <ul class="features-list pl0 f-16 f-md-18 lh140 w400 black-clr text-capitalize">
                           <li><span class="w600">Host and Manage All Your Files from One Easy Dashboard –</span> Videos, Web Images, PDFs, Docs, Audio, and Zip Files.</li>
                           <li><span class="w600">Super-Fast Delivery (After All, Time Is Money!) - </span> This lead to More Engagement, Leads &amp; Sales. </li>
                           <li><span class="w600"> Play Sales, Demo &amp; Training Videos in HD</span>  on Any Page, Site, or Members Area.</li>
                           <li><span class="w600">Tap Into HUGE E-Learning Industry - </span> Deliver Your Videos, Docs, and PDF Training on Done-For-You and Beautiful Doc Sharing Sites &amp; Pages. </li>
                           <li>Use Our App to Help <span class="w600">Speed-Up Your Website, Landing Pages &amp; Online Shops with Fast Loading &amp; Optimized Images &amp; Videos. </span> </li>
                           <li>Build Your Online Empire - <span class="w600">Deliver Digital Products Securely, Fast &amp; Easy </span></li>
                           <li><span class="w600">Generate Tons of Leads &amp; Affiliate Sales –  </span> Deliver Freebies &amp; Affiliate Bonuses to Your Subscribers Without a Hitch</li>
                           <li><span class="w600">Share Your Files Privately </span>  with Your Clients, Customers &amp; Team Members.</li>
                           <li><span class="w600">Free Hosting </span> Is Included, Unlimited Bandwidth &amp; Upto 250 GB Storage </li>
                           <li> PLUS, <span class="w600">Limited Time Free Commercial License  </span> (only for today) to Serve Your Clients</li>
                        </ul>
                     </div>
                     <div class="col-12 col-md-6 mt10 mt-md0">
                        <ul class="features-list pl0 f-16 f-md-18 lh140 w400 black-clr text-capitalize">
                           <li>Elegant, <span class="w600">SEO Optimized &amp; 100% Mobile Responsive </span> File Share Pages </li>
                           <li>Inbuilt <span class="w600">Lead Management System </span></li>
                           <li><span class="w600">Access Files Anytime, Anywhere</span> Directly from The Cloud on Any Device!</li>
                           <li><span class="w600">Highly Encrypted Unbreakable File Security</span> with SSL &amp; OTP Enabled Login </li>
                           <li>30 Days <span class="w600">Online Back-up &amp; File recovery </span> So You Never Lose Your Precious Data and Files. </li>
                           <li><span class="w600">Single Dashboard</span> to Manage All Business Files- No Need to Buy Multiple Apps 
                           </li><li><span class="w600">Manage Files Effortlessly –</span> Share Multiple Files, Full Text Search &amp; File Preview </li>
                           <li>Inbuilt Elegant Video Player with <span class="w600">HDR Support</span> </li>
                           <li><span class="w600">Real-Time Analytics </span>for Every Single File You Upload - See Downloads, Shares, etc.  </li>
                           <li><span class="w600">Easy and Intuitive</span> to Use Software with <span class="w600">Step-by-Step Video Training </span></li>
                           <li><span class="w600">100% Newbie Friendly</span> &amp; Fully Cloud Based Software </li>
                           <li><span class="w600">Live Chat -</span> Customer Support So You Never Get Stuck or Have any Issues </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Features Section End -->



     
     
     
      <!-- Bonus Section Header Start -->
      <div class="bonus-header">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-10 mx-auto heading-bg text-center">
                  <div class="f-24 f-md-36 lh140 w700"> When You Purchase CloudFusion, You Also Get <br class="hidden-xs"> Instant Access To These 20 Exclusive Bonuses</div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus Section Header End -->
       <!-- Bonus #1 Section Start -->
       <div class="section-bonus">
         <div class="container">
            <div class="row">
               
               <div class="col-12 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                  
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus1.webp" class="img-fluid mx-auto d-block" alt="Bonus1">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="bonus-title-bg">
                        <div class="f-22 f-md-28 lh120 w700">BONUS 1</div>
                      </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md30">
                        50 Animated Video Backgrounds

                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li class="w600">
                           Nice PRO Looking Animated Backgrounds For Your Videos
                           </li>
                           <li>
                           Are you looking for an animated video backgrounds that are easy to use? Motion background that you will surely love? Then, download this one!
                           </li>
                           <li>
                           This tool provides 50 animated video background that are created magnificently! Some shapes are elegant abstract particle background. These animated videos will definitely of great use for your videos!
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #1 Section End -->

      <!-- Bonus #2 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               
               <div class="col-12">
                  <div class="row align-items-center">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="assets/images/bonus2.webp" class="img-fluid mx-auto d-block" alt="Bonus2">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                     <div class="bonus-title-bg">
                         <div class="f-22 f-md-28 lh120 w700">BONUS 2</div>
                      </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md30">
                        100 Video Transition Backgrounds
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li class="w600">
                           Nice PRO Looking Transition Backgrounds For Your Videos
                           </li>
                           <li>
                           Are you looking for video that could be a transition backgrounds that are easy to use? Transition background that you could could use for your video edition? Then, download this one!
                           </li>
                           <li>
                           This tool provides 100 videos that are created thoroughly! This product assures you that the videos are easily to use as well as it has high definition so that your output will look like a professionally-made project.
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #2 Section End -->

      <!-- Bonus #3 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus3.webp" class="img-fluid mx-auto d-block " alt="Bonus3">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="bonus-title-bg">
                           <div class="f-22 f-md-28 lh120 w700">BONUS 3</div>
                         </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md30">
                        Pro Music Tracks
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li class="w600">More Professional Quality Sound Tracks For Your Marketing Needs!</li>
                           <li>Professional Quality Music Tracks For Your Marketing Needs! These music tracks vary in length from 30 seconds to 5 minutes!</li>
                           <li>With this professional music track you can seperate your plain ordinary video from professional videos that used professional music or audio tracks. So if you're an audio engineer or a video creator, these tracks are very helpful in dubbing your text slides, creating an effects or insert it in your logo animation.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #3 Section End -->

      <!-- Bonus #4 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="assets/images/bonus4.webp" class="img-fluid mx-auto d-block " alt="Bonus4">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                     <div class="bonus-title-bg">
                         <div class="f-22 f-md-28 lh120 w700">BONUS 4</div>
                     </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md30">
                        47 Motion Videos!

                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>
                           Attention video producers and resellers of Rich Media products Still paying $50 at Digital Juice for your motion video background loops? This 47 motion video background loops will give your video productions that POLISHED FEEL you've been after.
                           </li>
                           <li>
                           Television producers have been using motion video loops and elements for years. Ever see those notices that warn about "viewer discretion is advised" etc? They use motion video loops. And now that many internet marketers are producing their video advertisements, video demos, and video info-products, you'll find many of these same VIDEO MOTION ELEMENTS in their arsenal of tools.
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #4 End -->

      <!-- Bonus #5 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                 
               </div>
               <div class="col-12 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus5.webp" class="img-fluid mx-auto d-block " alt="Bonus5">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md30">
                     <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 5</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-colo mt20 mt-md30">
                        Beautiful Outdoors Stock Images

                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li class="w600">
                           Stock Images For You To Use In Your Projects And Your Clients Projects. Plus You Can Resell Them!
                           </li>
                           <li>
                           Many successful online business owners have said that making money online is easy as a piece of cake as long as you have all the ingredients in doing the process.
                           </li>
                           <li>
                           And one of those ingredients is graphics or images which is a huge help in marketing your product or service online using social media networking sites.
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #5 End -->

      <!-- CTA Button Section Start -->
      <!-- CTA Btn Start-->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 orange-clr2">"Cloud"</span> for an Additional <span class="w700 orange-clr2">$10 Discount</span> on CloudFusion
                  </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn px-md80">
                  <span class="text-center">Grab CloudFusion + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                  Use Coupon Code <span class="w700 orange-clr2">"CloudFusion" </span> for an Additional <span class="w700 orange-clr2">$50 Discount</span> on Bundle Deal
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflinkbundle']; ?>" class="text-center bonusbuy-btn px-md80">
                  <span class="text-center">Grab CloudFusion Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr"><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00</span><span class="f-14 f-md-18 smmltd">Days</span> </div><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00</span><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">00</span><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div><div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">00</span><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div></div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->
       <!-- Bonus #6 Start -->
       <div class="section-bonus">
         <div class="container">
            <div class="row">
               
               <div class="col-12 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="assets/images/bonus6.webp" class="img-fluid mx-auto d-block " alt="Bonus6">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                     <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 6</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md30">
                        Shopify Traffic

                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>
                           Compared to 10 years ago, starting an online e-store is not as hard anymore today. Driven by the dream of having an internet lifestyle, and the rewarding monthly income, everyone is triggered to own a business for a better life.
                           </li>
                           <li>
                           With all the platforms and opportunities available, it's easy to kick start an online business anytime you want. Even if you have little budget, you can start an online shop on a small scale.
                           </li>
                           <li>
                           Shopify Traffic is a series of training course that will teach you how to generate traffic to your Shopify e-store with effective methods and platform from personal experience and culmination of researches together with years of studies.
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #6 End -->

      <!-- Bonus #7 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               
               <div class="col-12 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus7.webp" class="img-fluid mx-auto d-block " alt="Bonus7">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="bonus-title-bg">
                         <div class="f-22 f-md-28 lh120 w700">BONUS 7</div>
                     </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md30">
                        JVZoo Training Videos

                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li class="w600">
                           Learn How to Use JVZoo to Make Money Online!
                           </li>
                           <li>
                           If you are into selling digital products, selling it using a reputable platform will give you the ease of selling your products as well managing your affiliates if you wish to do so.
                           </li>
                           <li>
                           You may already have heard about JVZoo. Like Clickbank, this marketplace and platform is a huge help to make your online business a huge success on the internet.
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #7 End -->

      <!-- Bonus #8 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               
               <div class="col-12  d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus8.webp" class="img-fluid mx-auto d-block " alt="Bonus8">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 8</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt2- mt-md30">
                        Double Your Email Campaign Conversion Rates

                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li class="w600">
                           How to Double Your Email Campaign Conversion Rates!
                           </li>
                           <li>
                           The money is in the list. This words came form the very mouth of those many successful online business owners. 
                           </li>
                           <li>
                           The thing is that, building a list is not your only job, you also need to learn how to maximize your list to make it more profitable.
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #8 End -->

      <!-- Bonus #9 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
              
               <div class="col-12 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus9.webp" class="img-fluid mx-auto d-block " alt="Bonus9">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 9</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md30">
                        WordPress Speed Video Course
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li class="w600">
                           Brand New Techie Training Videos That Are Brain-Dead Simple To Follow!
                           </li>
                           <li>
                           Website loading speed matters this is one of the factors to get your site rank higher in Google search results. If your website is built with WordPress, optimizing your website is very easy to do.
                           </li>
                           <li>Inside this video course is a series of training on how you can speed up the loading of your WordPress website easily even if you haven't done this before.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #9 End -->

      <!-- Bonus #10 start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
              
               <div class="col-12 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus10.webp" class="img-fluid mx-auto d-block " alt="Bonus10">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="bonus-title-bg">
                           <div class="f-22 f-md-28 lh120 w700">BONUS 10</div>
                        </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md30">
                        Bitcoin Profit Secrets Video Upgrade

                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>
                           <span class="w600">Whether you heard of Bitcoin years ago (but didn’t take action), or you just heard of it today, anyone can profit from Bitcoin!</span> Don’t be scared of this new technology because this video course will take you by the hand and teach you everything you need to know to succeed.
                           </li>
                           <li>
                           It will give you the background on Bitcoin, how it started, who developed it, why it was developed in the first place, and why it’s so much better than any national currency on earth. 
                           </li>
                           <li>
                           You will also learn how to acquire your first bitcoin, how to mine it, how to trade or invest it, and so much more!
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #10 End -->


     <!-- CTA Btn Start-->
     <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 orange-clr2">"Cloud"</span> for an Additional <span class="w700 orange-clr2">$10 Discount</span> on CloudFusion
                  </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn px-md80">
                  <span class="text-center">Grab CloudFusion + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                  Use Coupon Code <span class="w700 orange-clr2">"CloudFusion" </span> for an Additional <span class="w700 orange-clr2">$50 Discount</span> on Bundle Deal
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflinkbundle']; ?>" class="text-center bonusbuy-btn px-md80">
                  <span class="text-center">Grab CloudFusion Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr"><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00</span><span class="f-14 f-md-18 smmltd">Days</span> </div><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00</span><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">00</span><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div><div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">00</span><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div></div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Btn End -->
       <!-- Bonus #11 start -->
       <div class="section-bonus">
         <div class="container">
            <div class="row">
               
               <div class="col-12 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus11.webp" class="img-fluid mx-auto d-block " alt="Bonus11">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 11</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md30">
                        Forex Trading Fortunes

                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">Forex trading is about currency trading on Forex market. </span> The basic principle that operates on every market, applies here as well: in order to make money, you have to buy low then sell high. That's the whole philosophy.</li>
                           <li>Although there is a strong potential of earnings on Forex market, you should keep in mind that there are risks as well. Knowing the basics only would not be enough. A correct plan of investment and a strategy for it are strongly recommended.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #11 End -->

      <!-- Bonus #12 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
              
               <div class="col-12 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus12.webp" class="img-fluid mx-auto d-block " alt="Bonus12">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 12</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md30">
                        Sales Funnel Optimization Strategies Video Upgrade

                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li class="w600">Whether you make your money through ad clicks using the Adsense monetization platform or you sell affiliate products or your own services or you run your own online drop shipping store, you're trying to convert people from simple clickers of links and readers of your content to cold hard cash.</li>
                           <li>The sales funnel model helps you craft together working strategies that would help you turn your content and traffic into cash.</li>
                           <li>This video course teaches you how to optimize your sales funnel. It will show you key strategies that will help you maximize conversions and thereby maximize your profits.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #12 End -->

      <!-- Bonus #13 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
              
               <div class="col-12 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus13.webp" class="img-fluid mx-auto d-block " alt="Bonus13">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 13</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md30">
                        Seo Revolution Video Upgrade

                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li class="w600">There is plenty of information available online to manipulate the rankings in one way or another, but one factor is constant: SEO’s usage is on an ever-increasing rise. </li>
                        <li>
                        This is a complete collection of 15 High Definition videos with step by step content. 
                        </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #13 End -->

      <!-- Bonus #14 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               
               <div class="col-12 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus14.webp" class="img-fluid mx-auto d-block " alt="Bonus14">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 14</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md30">
                        Buying Traffic to Generate Massive Website Visitors

                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>An intrinsic understanding of your competition and how to better them is the most important component of any digital marketing strategy. Today, there is such small distances between you and your competition - it's unlikely that you're offering anything that cannot be bought on some other website.</li>
                           <li>Now, when a consumer wants and needs a product or information, all they must do is input their desires into the Google search and peruse the search engine results page.</li>
                           <li>Standing out in the search results is by far the most important part of driving traffic to your site. In this ebook, you will learn about the main and most popular techniques to drive traffic to your website.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #14 End -->

      <!-- Bonus #15 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               
               <div class="col-12 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus15.webp" class="img-fluid mx-auto d-block " alt="Bonus15">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 15</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md30">
                        US Free Ads Traffic Video Course

                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li class="w600">With 1000s of visitors a month and a high Alexa ranking this classified ad source is a gold mine for those who take advantage of this!</li>
                           <li>Traffic is very important to every online business owners. Traffic is where you can find the source of qualified leads that will turn into a loyal customer if you have built a good relationship to them.</li>
                           <li>The thing is that traffic generation is always been a challenge to every website owners. That's why trying many sources they can is also a good move to know which traffic source is good for their online business.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #15 End -->


     <!-- CTA Btn Start-->
     <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 orange-clr2">"Cloud"</span> for an Additional <span class="w700 orange-clr2">$10 Discount</span> on CloudFusion
                  </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn px-md80">
                  <span class="text-center">Grab CloudFusion + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                  Use Coupon Code <span class="w700 orange-clr2">"CloudFusion" </span> for an Additional <span class="w700 orange-clr2">$50 Discount</span> on Bundle Deal
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflinkbundle']; ?>" class="text-center bonusbuy-btn px-md80">
                  <span class="text-center">Grab CloudFusion Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr"><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00</span><span class="f-14 f-md-18 smmltd">Days</span> </div><div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00</span><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div><div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">00</span><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div><div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">00</span><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div></div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Btn End -->
      <!-- Bonus #16 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
              
               <div class="col-12 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus16.webp" class="img-fluid mx-auto d-block " alt="Bonus16">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 16</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md30">
                        Youtube Success Step By Step

                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><span class="w600">YouTube Success Step By Step is a new powerful report that explains to the reader how they can create a successful YouTube channel in the fastest possible time.</span> Readers of the report will learn everything that they need to know about choosing the right subject for their YouTube channel and publishing the most engaging video content on it.</li>
                           <li>Readers will learn that they must choose the right subject or niche for their YouTube channel. They will understand why the theme of their channel is so important and how to make the right decision about this based on the needs of their audience. There are examples of the best video content to create for a successful channel.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #16 End -->

      <!-- Bonus #17 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
              
               <div class="col-12 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus17.webp" class="img-fluid mx-auto d-block " alt="Bonus17">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 17</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md30">
                        Virtual Networking Success

                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li class="w600">Networking when you are not in the office can be hard, but it may be one of the best ways to grow your business and make sure that you meet new people.</li>
                           <li>Virtual networking allows this to happen, whether you are in the office or work from anywhere.</li>
                           <li>Through virtual networking, you will be able to take advantage of online platforms, social media, and even online webinars and conferences in order to form connections with others, even when you are not able to meet them in-person.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #17 End -->

      <!-- Bonus #18 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               
               <div class="col-12 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus18.webp" class="img-fluid mx-auto d-block " alt="Bonus18">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 18</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md30">
                        Start Your Own Hosting Company

                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>If you’re reading this then congratulations, you’re well on your way to starting a wildly successful Internet business in website hosting! Everybody who’s anybody has a website these days from production artists to babysitters. Competitive web hosting services are among the fastest growing online businesses around the globe!</li>
                           <li>Before you start your journey into online success you need to make sure that this endeavor is really for you. The beauty of this kind of online business is that it’s pretty easy to set up and when you get tired of it there are “escape” or “exit” plans for cashing in and abandoning ship.</li>
                           
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #18 End -->

      <!-- Bonus #19 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               
               <div class="col-12 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus19.webp" class="img-fluid mx-auto d-block " alt="Bonus19">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                     <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 19</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md30">
                        Instant Email Popup Generator

                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>Opens Your Visitors Default Email Program With Your Message, Subject Line And Email Address</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #19 End -->

      <!-- Bonus #20 start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
              
               <div class="col-12 d-flex align-content-center flex-wrap">
                  <div class="row align-items-center">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus20.webp" class="img-fluid mx-auto d-block " alt="Bonus20">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                     <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 20</div>
                  </div>
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md30">
                        Easy Popup Generator

                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li> <span class="w600">No one likes popups now a days, but let's face it.</span> There are times when they are absolutely neccessary, or at least vital, to the layout and deployment workings of your website.</li>
                           <li><span class="w600">This software will enable you to create simple popup windows for your own sites.</span> Great little tool for those who need to display images, screenshots or open links within a new window. Customize whether or not to display the browser address bar, status bar and so much more...</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #20 End -->
      <!-- Huge Woth Section Start -->
      <div class="huge-area mt30 mt-md10">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="f-md-65 f-40 lh120 w700 white-clr">That's Huge Worth of</div>
                  <br>
                  <div class="f-md-60 f-40 lh120 w800 orange-clr2">$3300!</div>
               </div>
            </div>
         </div>
      </div>
      <!-- Huge Worth Section End -->
      <!-- text Area Start -->
      <div class="white-section pb0">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="f-md-36 f-25 lh140 w500">So what are you waiting for? You have a great opportunity <br class="hidden-xs"> ahead + <span class="w700 ">My 20 Bonus Products</span> are making it a <br class="hidden-xs"> <span class="w700 ">completely NO Brainer!!</span></div>
               </div>
            </div>
         </div>
      </div>
      <!-- text Area End -->
      <!-- CTA Button Section Start -->
      <div class="cta-btn-section">
         <div class="container">
            <div class="row">
            <div class="f-16 f-md-20 lh150 w400 text-center mt20 black-clr">
                     Use Coupon Code <span class="w700 yellow-clr">"Cloud"</span> for an Additional <span class="w700 yellow-clr">$10 Discount</span> on CloudFusion
                  </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn px-md80">
                  <span class="text-center">Grab CloudFusion + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 black-clr mt-md30">
                  Use Coupon Code <span class="w700 yellow-clr">"CloudFusion" </span> for an Additional <span class="w700 yellow-clr">$50 Discount</span> on Bundle Deal
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflinkbundle']; ?>" class="text-center bonusbuy-btn px-md80">
                  <span class="text-center">Grab CloudFusion Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-12 mt15 mt-md20" align="center">
                  <h3 class="f-md-28 f-20 w500 text-center black">My Exclusive Bonuses Are Expiring in...</h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-black black-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <span class="f-14 f-md-18 smmltd">Days</span>
                      </div>
                      <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <span class="f-14 f-md-18 w500 smmltd">Hours</span>
                      </div>
                      <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <span class="f-14 f-md-18 w500 smmltd">Sec</span> 
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->
      <div class="footer-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 926.8 148.17" style="max-height:55px;"><defs><style>.cls-1b{fill:#0068ff;}.cls-2{fill:#f89b1c;}.cls-3w{fill:#fff;}</style></defs><title>Asset 12</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1b" d="M41.41,108.34a22.49,22.49,0,0,1-2.87-11c0-12.08,9.69-22.18,22.05-23l16.06-1.06L72.4,58.17a35,35,0,0,1-1.31-9.5c0-19.78,16.54-35.86,36.86-35.86a37.33,37.33,0,0,1,24.89,9.41,35.39,35.39,0,0,1,11.81,23.06l.59,6.21c4.11-3.79,8.07-7.79,11.84-12C152.67,17,132.35,0,108,0c-27.63,0-50,21.79-50,48.67A47.76,47.76,0,0,0,59.7,61.56c-19.17,1.26-34.33,16.8-34.33,35.77a35,35,0,0,0,3.28,14.81C32.55,111.06,36.83,109.79,41.41,108.34Z"></path><path class="cls-2" d="M165.9,50.66c10.88-20.3,14.58-35.47,14.58-35.47C142.66,114.85,2.87,139.07,0,140H163.24c25.39,0,46-20,46-44.71C209.2,71.47,190,52,165.9,50.66Zm-2.66,76.53H91.94a188.75,188.75,0,0,0,66.3-63.45l1.07-.12a34.8,34.8,0,0,1,3.93-.23c18.09,0,32.8,14.31,32.8,31.9S181.33,127.19,163.24,127.19Z"></path><text></text><path class="cls-3w" d="M313.48,73.63H295.09A18.17,18.17,0,0,0,293,67.27a16.13,16.13,0,0,0-4-4.77,17.68,17.68,0,0,0-5.6-3,21.68,21.68,0,0,0-6.86-1A20.48,20.48,0,0,0,265,61.73a21.33,21.33,0,0,0-7.68,9.57,38.07,38.07,0,0,0-2.73,15.22A38.43,38.43,0,0,0,257.29,102a19.88,19.88,0,0,0,19.16,12.64,22.63,22.63,0,0,0,6.78-1,17.28,17.28,0,0,0,5.56-2.83,15.92,15.92,0,0,0,4-4.55,18.16,18.16,0,0,0,2.25-6.13l18.39.08a33.44,33.44,0,0,1-3.55,11.4,34.43,34.43,0,0,1-7.6,9.78,35.64,35.64,0,0,1-11.33,6.8,41.64,41.64,0,0,1-14.84,2.5,40.45,40.45,0,0,1-20.55-5.21,36.72,36.72,0,0,1-14.27-15.07q-5.24-9.87-5.23-23.88t5.29-23.93a37,37,0,0,1,14.36-15.05,40.29,40.29,0,0,1,20.4-5.18A44.26,44.26,0,0,1,290,44.45a35.54,35.54,0,0,1,11.36,6.11,32.24,32.24,0,0,1,8.08,9.8A36.94,36.94,0,0,1,313.48,73.63Z"></path><path class="cls-3w" d="M344.08,43.53v86H326.19v-86Z"></path><path class="cls-3w" d="M387.48,130.76a32.77,32.77,0,0,1-16.9-4.18,28.15,28.15,0,0,1-11-11.67,37.37,37.37,0,0,1-3.87-17.4A37.59,37.59,0,0,1,359.61,80a28.15,28.15,0,0,1,11-11.67,36.25,36.25,0,0,1,33.8,0,28.15,28.15,0,0,1,11,11.67,37.59,37.59,0,0,1,3.86,17.48,37.37,37.37,0,0,1-3.86,17.4,28.15,28.15,0,0,1-11,11.67A32.77,32.77,0,0,1,387.48,130.76Zm.08-13.85a11.05,11.05,0,0,0,7.43-2.54,15.66,15.66,0,0,0,4.52-7,33.67,33.67,0,0,0,0-20.07,15.81,15.81,0,0,0-4.52-7,11,11,0,0,0-7.43-2.56A11.29,11.29,0,0,0,380,80.39a15.48,15.48,0,0,0-4.58,7,33.67,33.67,0,0,0,0,20.07,15.34,15.34,0,0,0,4.58,7A11.38,11.38,0,0,0,387.56,116.91Z"></path><path class="cls-3w" d="M472.23,102.05V65h17.88V129.5H473V117.79h-.68A19.08,19.08,0,0,1,465,126.9a21.41,21.41,0,0,1-12.32,3.44,21.69,21.69,0,0,1-11.37-2.94,20.09,20.09,0,0,1-7.67-8.35,28.77,28.77,0,0,1-2.79-13V65h17.89v37.87c0,3.8,1.05,6.81,3.06,9a10.41,10.41,0,0,0,8.1,3.32,12.78,12.78,0,0,0,6.05-1.49,11.58,11.58,0,0,0,4.55-4.43A13.75,13.75,0,0,0,472.23,102.05Z"></path><path class="cls-3w" d="M528.14,130.55a24.16,24.16,0,0,1-13.28-3.8,25.78,25.78,0,0,1-9.4-11.21Q502,108.13,502,97.35q0-11.09,3.57-18.45a25.73,25.73,0,0,1,9.51-11,24.29,24.29,0,0,1,13-3.68,20,20,0,0,1,9,1.83A18.15,18.15,0,0,1,543,70.54a24.43,24.43,0,0,1,3.42,5.31H547V43.53h17.84v86H547.2V119.17h-.75a23.05,23.05,0,0,1-3.55,5.32,18.09,18.09,0,0,1-5.92,4.34A20.5,20.5,0,0,1,528.14,130.55Zm5.67-14.23a11.47,11.47,0,0,0,7.33-2.37,14.89,14.89,0,0,0,4.61-6.66,28.24,28.24,0,0,0,1.62-10,28.22,28.22,0,0,0-1.6-10,14.33,14.33,0,0,0-4.61-6.55,11.77,11.77,0,0,0-7.35-2.31,11.58,11.58,0,0,0-7.43,2.4,14.57,14.57,0,0,0-4.57,6.63,28.38,28.38,0,0,0-1.56,9.82,28.82,28.82,0,0,0,1.58,9.93,15,15,0,0,0,4.57,6.72A11.44,11.44,0,0,0,533.81,116.32Z"></path><path class="cls-3w" d="M582.76,129.5v-86H634.3v9.24H593.17v29h37.27v9.24H593.17V129.5Z"></path><path class="cls-3w" d="M687.36,103.14V65h9.91V129.5h-9.91V118.59h-.67a21,21,0,0,1-7.05,8.33q-4.78,3.42-12.09,3.42a21.41,21.41,0,0,1-10.75-2.67,18.33,18.33,0,0,1-7.38-8q-2.7-5.4-2.69-13.63V65h9.91v40.3q0,7.05,4,11.25a13.35,13.35,0,0,0,10.14,4.2,17,17,0,0,0,7.54-1.89,16.58,16.58,0,0,0,6.46-5.79A17.49,17.49,0,0,0,687.36,103.14Z"></path><path class="cls-3w" d="M761.08,79.46,752.18,82a17.11,17.11,0,0,0-2.46-4.34,12.34,12.34,0,0,0-4.39-3.51,15.87,15.87,0,0,0-7.09-1.38,16.94,16.94,0,0,0-9.84,2.7,8.09,8.09,0,0,0-3.93,6.87,7.05,7.05,0,0,0,2.69,5.83q2.69,2.14,8.39,3.57l9.57,2.35q8.65,2.1,12.89,6.4a15.09,15.09,0,0,1,4.24,11.06,16.46,16.46,0,0,1-3.17,9.91,21.35,21.35,0,0,1-8.83,6.89,32.4,32.4,0,0,1-13.19,2.51q-9.85,0-16.33-4.28a19,19,0,0,1-8.18-12.51l9.4-2.35a13.09,13.09,0,0,0,5.1,7.81q3.76,2.59,9.85,2.6,6.92,0,11-3c2.72-2,4.09-4.34,4.09-7.11a7.61,7.61,0,0,0-2.35-5.65,15.26,15.26,0,0,0-7.22-3.42l-10.75-2.52q-8.85-2.1-13-6.53a15.61,15.61,0,0,1-4.14-11.1,15.82,15.82,0,0,1,3.09-9.65,20.63,20.63,0,0,1,8.44-6.59,29.41,29.41,0,0,1,12.15-2.4q9.57,0,15,4.2A22.7,22.7,0,0,1,761.08,79.46Z"></path><path class="cls-3w" d="M782.23,54.28a6.94,6.94,0,0,1-5-2,6.44,6.44,0,0,1,0-9.48,7.24,7.24,0,0,1,10,0,6.46,6.46,0,0,1,0,9.48A7,7,0,0,1,782.23,54.28Zm-5,75.22V65h9.91V129.5Z"></path><path class="cls-3w" d="M831.43,130.84a27.22,27.22,0,0,1-25.54-15.78,39.11,39.11,0,0,1-3.68-17.46A39.59,39.59,0,0,1,805.89,80,28.25,28.25,0,0,1,857,80a39.59,39.59,0,0,1,3.68,17.59A39.11,39.11,0,0,1,857,115.06a27.22,27.22,0,0,1-25.54,15.78Zm0-8.89a17,17,0,0,0,10.91-3.41,19.92,19.92,0,0,0,6.34-8.94,34.21,34.21,0,0,0,2.06-12,34.62,34.62,0,0,0-2.06-12,20.31,20.31,0,0,0-6.34-9,19,19,0,0,0-21.83,0,20.37,20.37,0,0,0-6.33,9,34.39,34.39,0,0,0-2.06,12,34,34,0,0,0,2.06,12,20,20,0,0,0,6.33,8.94A17,17,0,0,0,831.43,122Z"></path><path class="cls-3w" d="M885.66,90.71V129.5h-9.9V65h9.57V75.1h.84a18.47,18.47,0,0,1,6.88-7.91q4.62-3,11.92-3a23.6,23.6,0,0,1,11.46,2.67,18.21,18.21,0,0,1,7.64,8.06c1.82,3.59,2.73,8.14,2.73,13.62v41h-9.9V89.2q0-7.59-3.95-11.86t-10.83-4.26a17.2,17.2,0,0,0-8.46,2.06,14.67,14.67,0,0,0-5.86,6A19.9,19.9,0,0,0,885.66,90.71Z"></path><text></text></g></g></svg>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-md-18 f-16 w400 lh140 white-clr text-xs-center">Copyright © CloudFusion 2023</div>
                  <ul class="footer-ul w400 f-md-18 f-16 white-clr text-center text-md-right">
                     <li><a href="https://support.oppyo.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://getcloudfusion.co/legal/privacy-policy.html " class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://getcloudfusion.co/legal/terms-of-service.html " class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://getcloudfusion.co/legal/disclaimer.html " class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://getcloudfusion.co/legal/gdpr.html " class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://getcloudfusion.co/legal/dmca.html " class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://getcloudfusion.co/legal/anti-spam.html " class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
       <!-- timer --->
       <?php
         if ($now < $exp_date) {
         ?>
      <script type="text/javascript">
         // Count down milliseconds = server_end - server_now = client_end - client_now
         var server_end = <?php echo $exp_date; ?> * 1000;
         var server_now = <?php echo time(); ?> * 1000;
         var client_now = new Date().getTime();
         var end = server_end - server_now + client_now; // this is the real end time
         
         var noob = $('.countdown').length;
         
         var _second = 1000;
         var _minute = _second * 60;
         var _hour = _minute * 60;
         var _day = _hour * 24
         var timer;
         
         function showRemaining() {
         var now = new Date();
         var distance = end - now;
         if (distance < 0) {
         	clearInterval(timer);
         	document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
         	return;
         }
         
         var days = Math.floor(distance / _day);
         var hours = Math.floor((distance % _day) / _hour);
         var minutes = Math.floor((distance % _hour) / _minute);
         var seconds = Math.floor((distance % _minute) / _second);
         if (days < 10) {
         	days = "0" + days;
         }
         if (hours < 10) {
         	hours = "0" + hours;
         }
         if (minutes < 10) {
         	minutes = "0" + minutes;
         }
         if (seconds < 10) {
         	seconds = "0" + seconds;
         }
         var i;
         var countdown = document.getElementsByClassName('countdown');
         for (i = 0; i < noob; i++) {
         	countdown[i].innerHTML = '';
         
         	if (days) {
         		countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + days + '</span><span class="f-14 f-md-18 smmltd">Days</span> </div>';
         	}
         
         	countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + hours + '</span><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>';
         
         	countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">' + minutes + '</span><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>';
         
         	countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">' + seconds + '</span><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>';
         }
         
         }
         timer = setInterval(showRemaining, 1000);
         	
      </script>
      <?php
         } else {
         echo "Times Up";
         }
         ?>
      <!--- timer end-->
   </body>
</html>
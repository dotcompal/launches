<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <!-- Tell the browser to be responsive to screen width -->
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
      <meta name="title" content="NexusGPT Bonuses">
      <meta name="description" content="Grab My 20 Exclusive Bonuses Before the Deal Ends...">
      <meta name="keywords" content="NexusGPT Bonuses">
      <meta property="og:image" content="https://www.getnexusgpt.com/special-bonus/thumbnail.png">
      <meta name="language" content="English">
      <meta name="revisit-after" content="1 days">
      <meta name="author" content="Dr. Amit Pareek">
      <!-- Open Graph / Facebook -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="NexusGPT Bonuses">
      <meta property="og:description" content="Grab My 20 Exclusive Bonuses Before the Deal Ends...">
      <meta property="og:image" content="https://www.getnexusgpt.com/special-bonus/thumbnail.png">
      <!-- Twitter -->
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:title" content="NexusGPT Bonuses">
      <meta property="twitter:description" content="Grab My 20 Exclusive Bonuses Before the Deal Ends...">
      <meta property="twitter:image" content="https://www.getnexusgpt.com/special-bonus/thumbnail.png">
      <title>NexusGPT Bonuses</title>
      <!-- Shortcut Icon  -->
      <link rel="icon" href="../common_assets/images/favicon.png" type="image/png">
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
      <link rel="stylesheet" href="../common_assets/css/bootstrap.min.css" type="text/css">
      <script src="https://cdn.oppyotest.com/launches/sellero/common_assets/js/jquery.min.js"></script>
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <!-- Start Editor required -->
      <link rel="stylesheet" href="assets/css/style.css" type="text/css">
      <link rel="stylesheet" type="text/css" href="assets/css/bonus-style.css">
   </head>
   <body>
      <!-- New Timer  Start-->
      <?php
         $date = 'July 22 2023 11:59 AM EST';
         $exp_date = strtotime($date);
         $now = time();  
         if ($now < $exp_date) {
         ?>
      <?php
         } else {
         	echo "Times Up";
         }
         ?>
      <!-- New Timer End -->
      <?php
         if(!isset($_GET['afflink'])){
         $_GET['afflink'] = 'https://jvz4.com/c/10103/397652/';
         $_GET['name'] = 'Dr. Amit Pareek';      
         }
         ?>
          <?php
         if(!isset($_GET['afflinkbundle'])){
         $_GET['afflinkbundle'] = 'https://jvz3.com/c/10103/397650/';
         $_GET['name'] = 'Dr. Amit Pareek';      
         }
         ?>
      <!-- Header Section Start -->   
      <div class="main-header">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="text-center">
                     <div class="f-md-32 f-20 lh140 w400 white-clr d-block d-md-flex align-items-center justify-content-center">
                        <span class="w600 yellow-clr"><?php echo $_GET['name'];?>'s</span> &nbsp;special bonus for &nbsp;
                        <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1073.21 250" style="max-height:65px;">
                        <defs>
                           <style>
                              .cls-1 {
                              fill: #fff;
                              }

                              .cls-2 {
                              fill-rule: evenodd;
                              }

                              .cls-2, .cls-3 {
                              fill: #00a4ff;
                              }

                              .cls-4 {
                              fill: #f19b10;
                              }
                           </style>
                        </defs>
                        <g id="Layer_1-2" data-name="Layer 1">
                           <g>
                              <g>
                                 <g>
                                    <path class="cls-1" d="m872.38,109.17c-3.33-7.67-8.42-13.69-15.25-18.08-6.83-4.39-14.86-6.58-24.08-6.58-8.67,0-16.45,2-23.33,6-6.89,4-12.33,9.7-16.33,17.08-4,7.39-6,15.97-6,25.75s2,18.39,6,25.83c4,7.45,9.44,13.17,16.33,17.17,6.89,4,14.67,6,23.33,6,8.11,0,15.42-1.75,21.92-5.25s11.72-8.5,15.67-15c3.94-6.5,6.19-14.08,6.75-22.75h-49v-9.5h61.33v8.5c-.56,10.22-3.33,19.47-8.33,27.75-5,8.28-11.7,14.81-20.08,19.58-8.39,4.78-17.81,7.17-28.25,7.17s-20.56-2.53-29.33-7.58c-8.78-5.05-15.69-12.11-20.75-21.17-5.06-9.05-7.58-19.3-7.58-30.75s2.53-21.69,7.58-30.75c5.05-9.05,11.97-16.11,20.75-21.17,8.78-5.05,18.55-7.58,29.33-7.58,12.44,0,23.33,3.11,32.67,9.33,9.33,6.22,16.11,14.89,20.33,26h-13.67Z"></path>
                                    <path class="cls-1" d="m975.71,132.83c-6.56,6.11-16.39,9.17-29.5,9.17h-23.5v49.67h-11.67v-116.5h35.17c13,0,22.8,3.06,29.42,9.17,6.61,6.11,9.92,14.22,9.92,24.33s-3.28,18.06-9.83,24.17Zm-2.17-24.17c0-7.78-2.17-13.67-6.5-17.67-4.33-4-11.28-6-20.83-6h-23.5v47h23.5c18.22,0,27.33-7.78,27.33-23.33Z"></path>
                                    <path class="cls-1" d="m1073.21,75.17v9.67h-32.33v106.83h-11.67v-106.83h-32.5v-9.67h76.5Z"></path>
                                 </g>
                                 <g>
                                    <path class="cls-1" d="m322.77,192.08h-28.5l-47.67-72.17v72.17h-28.5v-117h28.5l47.67,72.5v-72.5h28.5v117Z"></path>
                                    <path class="cls-1" d="m505.27,192.08l-23.83-35.83-21,35.83h-32.33l37.5-59.5-38.33-57.5h33.17l23.5,35.33,20.67-35.33h32.33l-37.17,59,38.67,58h-33.17Z"></path>
                                    <path class="cls-1" d="m580.77,75.08v70c0,7,1.72,12.39,5.17,16.17,3.44,3.78,8.5,5.67,15.17,5.67s11.78-1.89,15.33-5.67c3.55-3.78,5.33-9.17,5.33-16.17v-70h28.5v69.83c0,10.45-2.22,19.28-6.67,26.5-4.45,7.22-10.42,12.67-17.92,16.33-7.5,3.67-15.86,5.5-25.08,5.5s-17.47-1.8-24.75-5.42c-7.28-3.61-13.03-9.05-17.25-16.33-4.22-7.28-6.33-16.14-6.33-26.58v-69.83h28.5Z"></path>
                                    <path class="cls-1" d="m689.6,189.08c-6.78-2.78-12.2-6.89-16.25-12.33-4.06-5.44-6.2-12-6.42-19.67h30.33c.44,4.33,1.94,7.64,4.5,9.92,2.55,2.28,5.89,3.42,10,3.42s7.55-.97,10-2.92c2.44-1.94,3.67-4.64,3.67-8.08,0-2.89-.97-5.28-2.92-7.17-1.95-1.89-4.33-3.44-7.17-4.67-2.83-1.22-6.86-2.61-12.08-4.17-7.56-2.33-13.72-4.67-18.5-7-4.78-2.33-8.89-5.78-12.33-10.33-3.45-4.55-5.17-10.5-5.17-17.83,0-10.89,3.94-19.42,11.83-25.58,7.89-6.17,18.17-9.25,30.83-9.25s23.28,3.08,31.17,9.25c7.89,6.17,12.11,14.75,12.67,25.75h-30.83c-.22-3.78-1.61-6.75-4.17-8.92-2.56-2.17-5.83-3.25-9.83-3.25-3.45,0-6.22.92-8.33,2.75-2.11,1.83-3.17,4.47-3.17,7.92,0,3.78,1.78,6.72,5.33,8.83,3.55,2.11,9.11,4.39,16.67,6.83,7.55,2.56,13.69,5,18.42,7.33,4.72,2.33,8.8,5.72,12.25,10.17,3.44,4.45,5.17,10.17,5.17,17.17s-1.7,12.72-5.08,18.17c-3.39,5.45-8.31,9.78-14.75,13-6.45,3.22-14.06,4.83-22.83,4.83s-16.22-1.39-23-4.17Z"></path>
                                    <rect class="cls-1" x="343.41" y="75.29" width="71.69" height="22.79"></rect>
                                    <rect class="cls-1" x="343.41" y="121.79" width="71.69" height="22.79"></rect>
                                    <rect class="cls-1" x="343.41" y="169.5" width="71.69" height="22.79"></rect>
                                 </g>
                              </g>
                              <g>
                                 <path class="cls-3" d="m76.03.47L13.55,16.04C5.96,17.93,0,28.08,0,38.78v147.74c0,.3,0,.61,0,.91v23.78c0,10.7,5.96,20.86,13.55,22.75l62.48,15.56c10.52,2.63,19.39-5.93,19.39-19.22v-112.1c0-.25,0-.5,0-.75V19.7c0-13.29-8.88-21.85-19.4-19.22Zm-34.9,227.64c-2.77-1.31-4.37-4.32-4.37-7.13s1.61-5.83,4.37-7.13c4.4-2.08,10.41.47,10.41,7.13s-6.01,9.22-10.41,7.13ZM54.19,23.79l-19.12,4.39c-1.93.44-3.86-.76-4.3-2.69-.06-.27-.09-.54-.09-.81,0-1.63,1.12-3.11,2.78-3.49l19.13-4.39c1.92-.45,3.85.76,4.29,2.69.07.27.09.54.09.81,0,1.63-1.12,3.11-2.78,3.49Z"></path>
                                 <path class="cls-4" d="m166.8,83.52l-44.46-11.07c-7.48-1.87-13.8,4.23-13.8,13.68v149.86c0,9.46,6.32,15.55,13.8,13.68l44.46-11.07c5.41-1.35,9.64-8.57,9.64-16.18v-122.7c0-7.61-4.23-14.84-9.64-16.18Zm-19.62,150.91c-3.13,1.47-7.41-.34-7.41-5.08s4.27-6.56,7.41-5.08c1.97.93,3.11,3.08,3.11,5.08s-1.14,4.14-3.11,5.08Zm6.79-144.13c-.33,1.31-1.65,2.11-2.96,1.79l-12.66-2.64c-1.5-.42-2.2-1.68-2.2-2.85,0-.23.03-.46.08-.68.32-1.32,1.65-2.12,2.96-1.79l12.98,3.21c1.12.28,1.87,1.27,1.87,2.37,0,.2-.03.39-.08.59Z"></path>
                                 <g>
                                    <path class="cls-2" d="m163.94,67.4h0c-.95,1.77-2.97,2.67-4.92,2.18-1.95-.48-3.32-2.22-3.34-4.23.96-3.86,1.37-7.85,1.22-11.85-.25-7.01-2.24-14.05-6.12-20.47-.17-.27-.33-.54-.51-.81-.25-.41-.52-.81-.79-1.2-.38-.57-.78-1.12-1.19-1.66-.09-.12-.18-.23-.27-.35-.22-.28-.43-.55-.66-.82-2.05-2.55-4.37-4.81-6.87-6.76-.3-.24-.6-.48-.91-.69-.43-.32-.85-.62-1.28-.92-1.09-.75-2.21-1.44-3.35-2.07-3.35-1.87-6.9-3.26-10.53-4.15-3.93-.97-7.96-1.37-11.97-1.21-1.96-.42-3.39-2.12-3.47-4.12v-.18c0-1.93,1.26-3.64,3.12-4.21,9.06-.36,18.22,1.67,26.45,6.12.54.29,1.08.59,1.62.91.36.21.71.43,1.06.64.35.22.7.44,1.05.67,2.31,1.53,4.53,3.25,6.63,5.19,1.68,1.56,3.25,3.2,4.68,4.92.22.27.43.54.65.81.2.25.4.5.59.75.33.43.65.86.96,1.29.25.34.49.69.73,1.04.24.35.47.71.7,1.06.23.36.46.72.67,1.07.16.25.31.51.46.77.36.61.71,1.22,1.03,1.84.32.59.62,1.19.92,1.8.18.38.36.75.53,1.13.17.38.34.76.5,1.14.17.38.32.77.48,1.15.27.68.52,1.35.76,2.03,3.37,9.57,3.71,19.71,1.38,29.18Z"></path>
                                    <g>
                                       <path class="cls-2" d="m116.46,56.27c-.03-.25-.08-.5-.16-.74,0-.02,0-.02,0-.04-.02-.07-.04-.13-.07-.18,0-.05-.03-.1-.05-.14,0,0,0-.03-.02-.03,0,0,0,0,0-.02-.04-.11-.09-.2-.15-.3,0-.03-.02-.04-.03-.07-.05-.09-.12-.19-.18-.28,0-.02-.02-.05-.04-.07-.03-.04-.06-.09-.09-.13-.04-.04-.08-.08-.12-.12,0,0,0-.02-.03-.03,0,0-.02-.03-.03-.04-.08-.08-.15-.16-.23-.23-.02-.03-.05-.05-.07-.08-.06-.05-.12-.1-.18-.15,0,0-.02-.02-.03-.03l-.3-.2s-.04-.03-.06-.04c-.04-.03-.09-.05-.13-.08-.04-.02-.08-.03-.13-.05h0s0-.02,0-.02c-.02,0-.05-.03-.07-.03-.08-.04-.18-.08-.27-.12-.02,0-.03,0-.04,0-.04-.02-.09-.03-.13-.04-.06-.02-.11-.02-.17-.04s-.12-.03-.19-.05c-.03-.02-.08-.03-.12-.03h-.02l-.18-.03s-.1-.02-.15-.02c-.07,0-.13,0-.19,0h-.54c-.13.02-.25.03-.38.07-.07,0-.16.03-.23.05-.03,0-.05.02-.08.03-.02,0-.06.02-.09.02h-.02c-.11.04-.21.08-.32.13-.02,0-.04.02-.06.03,0,0-.02,0-.02,0-.09.04-.18.09-.28.14h0c-.11.07-.21.13-.31.2-.02,0-.02,0-.03.03-.04.02-.08.05-.13.08,0,0-.02.02-.03.03-.04.03-.08.06-.13.11,0,0-.02,0-.03.03-.05.03-.09.07-.13.12-.03.03-.07.06-.09.09-.02.02-.04.04-.07.07-.03.02-.05.05-.08.08-.07.08-.13.17-.2.25-.05.07-.1.15-.15.23-.02.02-.03.03-.03.05-.02.02-.03.06-.05.08-.03.05-.06.11-.08.17-.03.03-.03.04-.03.07-.04.08-.08.16-.11.23,0,0,0,.02,0,.03-.03.07-.07.16-.08.23-.02.04-.03.07-.04.12-.04.12-.07.25-.09.38,0,.07-.02.13-.02.21,0,0,0,.03,0,.04,0,.02,0,.03,0,.04,0,.05-.02.11,0,.16-.03.2-.02.4,0,.6,0,.05,0,.09,0,.13,0,.03,0,.08.02.11,0,.03.02.07.03.1,0,.1.02.19.05.28,0,.03.02.04.02.07.03.12.08.25.12.37.04.09.08.17.13.27,0,.02,0,.03.02.04.02.04.04.09.07.13,0,.02.02.03.03.06,0,.02.03.05.05.07,0,0,.02.03.03.04s.02.04.04.06c.03.07.07.12.12.18.02.03.03.05.06.07,0,0,0,.02,0,.02.04.06.08.11.13.16.03.05.08.09.12.13.02.03.04.04.06.07,0,0,.02.02.03.03.03.03.05.05.08.07.06.06.11.11.17.16.03.03.06.05.08.07.02.03.05.04.07.06.08.06.15.11.23.15.06.04.13.08.19.12,0,0,0,0,0,0,.06.03.11.07.17.09l.18.08s.06.03.08.04c.09.03.18.07.28.09.05.03.1.04.15.05.05.02.1.03.16.03.02.02.04.02.07.02.03,0,.08.02.11.02.08.02.17.03.25.04.03,0,.07,0,.1,0,.12.02.24.02.37.02.09,0,.19,0,.29,0,.08,0,.17-.02.26-.03.1-.02.2-.04.3-.07.09-.03.19-.06.29-.09,0,0,.02,0,.04,0h0c.12-.04.22-.09.33-.14.08-.03.15-.07.22-.1.11-.05.21-.11.3-.17,0,0,.02,0,.03-.02.02,0,.02-.02.04-.02.03-.03.08-.05.11-.08,0,0,0,0,.02,0,.02-.02.04-.03.06-.05,0,0,.02,0,.03-.03.1-.07.19-.15.28-.23.02-.02.03-.03.04-.04.09-.09.18-.18.26-.28.1-.12.19-.24.28-.37.02-.03.05-.08.07-.11.04-.05.07-.11.1-.17.02-.03.04-.07.06-.11.13-.26.23-.53.3-.82,0,0,0-.03,0-.04.08-.34.11-.7.08-1.06,0-.07,0-.13,0-.2Z"></path>
                                       <path class="cls-2" d="m147.39,63.31c-.95,1.77-2.97,2.67-4.93,2.18-1.95-.48-3.32-2.22-3.34-4.23.57-2.32.83-4.71.73-7.12,0-.2-.02-.39-.03-.59-.23-4.02-1.43-8.03-3.64-11.72-.96-1.58-2.05-3.03-3.27-4.31-.5-.55-1.03-1.07-1.58-1.54-.1-.1-.21-.19-.31-.28-.3-.27-.61-.53-.92-.77-.14-.11-.28-.22-.42-.31,0-.02,0-.02-.02-.02,0-.02-.02-.02-.03-.02-.16-.13-.32-.25-.48-.36,0,0,0,0,0,0-.16-.12-.32-.23-.48-.33-.12-.11-.26-.19-.39-.28-2.51-1.67-5.23-2.83-8.04-3.52-2.37-.58-4.82-.82-7.24-.71-.19,0-.39,0-.58.02-1.97-.37-3.45-2.02-3.58-4.02-.13-2.01,1.11-3.84,3.01-4.48,5.91-.4,11.91.74,17.38,3.45.04.02.08.03.11.05,0,0,.02,0,.02.02.25.13.49.25.74.38.34.17.68.37,1.02.56.34.2.67.4,1.02.61.23.14.45.28.68.43.44.29.89.59,1.32.9,0,0,.02,0,.02.02.21.15.41.3.61.45l.02.02c.62.48,1.24.98,1.85,1.5.24.21.48.43.72.65.22.2.43.4.64.61.93.89,1.79,1.83,2.59,2.81.27.33.53.66.78.99.17.23.34.46.5.68.17.23.33.46.49.69.21.3.41.6.61.91,0,.03.02.04.03.06.38.59.74,1.19,1.08,1.8.18.32.34.63.51.95.14.28.28.56.42.84.14.28.27.57.4.85.13.28.25.57.37.86,2.9,6.91,3.33,14.38,1.63,21.32Z"></path>
                                    </g>
                                    <path class="cls-2" d="m130.85,59.22c-.95,1.77-2.98,2.67-4.93,2.18-1.94-.48-3.32-2.22-3.34-4.23.19-.75.28-1.53.25-2.32v-.14c0-.15-.02-.3-.03-.45-.12-1.24-.52-2.48-1.2-3.62-.54-.9-1.22-1.65-1.98-2.27-.03-.02-.07-.05-.1-.07-.05-.04-.09-.08-.13-.1-.07-.06-.14-.11-.21-.16,0,0-.02,0-.03-.02-.08-.07-.17-.12-.26-.17-.23-.17-.48-.33-.72-.44-.71-.42-1.46-.7-2.22-.88-.87-.21-1.75-.28-2.62-.22-.06,0-.12.02-.17.03-.14,0-.28.03-.42.04-2-.22-3.59-1.76-3.88-3.74-.28-1.99.81-3.92,2.66-4.69,4.02-.58,8.25.27,11.86,2.65.24.15.48.32.72.49-.08-.06-.17-.12-.25-.17.17.12.35.24.53.38.2.14.38.29.57.45.07.05.15.11.22.18.18.15.36.31.53.47-.1-.1-.19-.19-.29-.28.15.13.3.28.44.41,0,0,.01,0,.01.01,0,0,.01,0,.02.01,1.14,1.05,2.16,2.28,3,3.68,2.45,4.06,3.02,8.72,1.97,12.98Z"></path>
                                 </g>
                              </g>
                           </g>
                        </g>
                     </svg>
                     </div>
                  </div>

                  <div class="col-12 mt20 mt-md50 text-center">
                  <div class="preheadline f-20 f-md-22 w500 lh140">
                  Grab My 20 Exclusive Bonuses Before the Deal Ends...
                  </div>
               </div>
               <div class="col-12 mt80 head-design relative">
                  <div class="gametext">
                     First-to-JVZoo Technology
                  </div>
                  <div class=" f-md-43 f-28 w400 text-center black-clr lh140">
                  <span class="underline-text w600">The Futuristic NFC Tech App Transforms Your Marketing, </span> Creates 100% Contactless &amp; AI-Powered Digital Business Cards, Generates Leads, Followers, Reviews, &amp; Sales with <span class="underline-text"> Just One Touch.</span>
                  </div>
               </div>
               <div class="col-12 mt-md40 mt20 f-22 f-md-25 w400 text-center lh140 white-clr text-capitalize">
               No More Physical Business Cards Sharing (Old-Fashioned & Not Working) & No More Paying To Freelancing Team To Talk To Prospects & Close Them Into Sales… Automate Everything. No Tech Skills Of Any Kind Needed. No Monthly Fee Ever…
               </div>
            </div>
            </div>
            <div class="row mt20 mt-md40">
               <div class="col-md-10 col-12 mx-auto">
                  <!-- <div class="col-12 responsive-video border-video">
                     <iframe src="https://yoseller.dotcompal.com/video/mydrive/182331" style="width:100%; height:100%" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                  </div> -->
                  <img src="assets/images/product-box.webp" class="img-fluid mx-auto d-block" alt="ProductBox">
               </div>
            </div>
            <!-- <div class="row row-cols-md-2 row-cols-xl-2 row-cols-1 mt20 mt-md40 calendar-wrap mx-auto">
               <div class="col">
                  <ul class="list-head pl0 m0 f-18 f-md-18 lh150 w400 white-clr">
                  <li> Launch Your &amp; Client's Products &amp; Services Online with ZERO Tech Hassles.</li>
                  </ul>
               </div>
               <div class="col">
                  <ul class="list-head pl0 m0 f-18 f-md-18 lh150 w400 white-clr">
                  <li>Profit with Agency/Reseller/PLR Right Products You Ever Purchased.</li>
                  </ul>
               </div>
               <div class="col">
                  <ul class="list-head pl0 m0 f-18 f-md-18 lh150 w400 white-clr">
                  <li>Create Stunning Websites, E-stores and Membership Sites Easily</li>
                  </ul>
               </div>
               <div class="col">
                  <ul class="list-head pl0 m0 f-18 f-md-18 lh150 w400 white-clr">
                  <li>Sell On Your Own Marketplace &amp; Keep 100% Profits</li>
                  </ul>
               </div>
               <div class="col">
                  <ul class="list-head pl0 m0 f-18 f-md-18 lh150 w400 white-clr">
                  <li>Smart-Checkout Links To Get Orders Directly from Social Media, Emails or Anywhere Else</li>
                  </ul>
               </div>
               <div class="col">
                  <ul class="list-head pl0 m0 f-18 f-md-18 lh150 w400 white-clr">
                  <li>Sell High In-Demand Services with <span class="w600 white-clr">Included Commercial License.</span></li>
                  </ul>
               </div>
            </div> -->
         </div>
      </div>
      <!-- Header Section End -->
     
     
      <!-- CTA Btn End -->
      <!-- Features Section Start -->
      <div class="second-section">
         <div class="container">
            <div class="second-sec-list">
               <div class="row">
                  <div class="col-12">
                     <div class="f-28 f-md-45 lh140 w600 text-center black-clr">
                        <span class="w700 yellow-clr">We Guarantee,</span>  The ultimate solution for <br class="d-none d-md-block">
                         your business growth needs.… 
                     </div>
                  </div>
               </div>
               <div class="row mt0 mt-md50">   
                  <div class="col-12 col-md-6">
                     <div class="feature-list">
                        <div class="f-18 f-md-20 lh140 w400">
                           <span class="w600">First-to-Market</span> NFC Tech Digital Business Cards with GPT-Assistance That brings a revolutionary combination 
                        </div>
                     </div>
                     <div class="feature-list">
                        <div class="f-18 f-md-20 lh140 w400">
                        <span class="w600">Capture Unlimited Leads </span> with Ready to Use NFC Digital Business cards.
                        </div>
                     </div>
                     <div class="feature-list">
                        <div class="f-18 f-md-20 lh140 w400">
                          <span class="w600">Supercharge your Online Presence</span> with NFC Cards 
                        </div>
                     </div>
                     <div class="feature-list">
                        <div class="f-18 f-md-20 lh140 w400">
                           <span class="w600">Market & Sell</span> your Products, Course, Services   
                        </div>
                     </div>
   
                     <div class="feature-list">
                        <div class="f-18 f-md-20 lh140 w400">
                           All-in-One Online Platform for <span class="w600">Creators, Marketers & Local Business Owners </span> 
                        </div>
                     </div>
                     <div class="feature-list">
                        <div class="f-18 f-md-20 lh140 w400">
                           <span class="w600">Create Unlimited</span> Beautiful, Mobile Ready Business Cards 
                        </div>
                     </div>
                     <div class="feature-list">
                        <div class="f-18 f-md-20 lh140 w400">
                           <span class="w600">Precise Tracking & Analytics To Measure Every Click -</span> Make The Right Decisions For Future Success! 
                        </div>
                     </div>
                     <div class="feature-list">
                        <div class="f-18 f-md-20 lh140 w400">
                           <span class="w600">SEO Friendly</span> & <span class="w600">Social Media Optimized</span>  Business Cards for More Traffic 
                        </div>
                     </div>
                     <div class="feature-list">
                        <div class="f-18 f-md-20 lh140 w400">
                           <span class="w600">100% GDPR</span> and <span class="w600">Can-Spam</span> Compliant 
                        </div>
                     </div>
                     <div class="feature-list">
                        <div class="f-18 f-md-20 lh140 w400">
                           <span class="w600">Plus, you'll also receive Commercial to Start Selling 
                           Pro Level Services to your Clients Right Away </span>
                        </div>
                     </div>
                  </div>
                  <div class="col-12 col-md-6">
                     <div class="f-18 lh140 w400">   
                        <div class="feature-list">
                           <div class="f-18 f-md-20 lh140 w400">
                              Let <span class="w600"> GPT Assistance will Manage all your Business Cards 24X7</span>
                           </div>
                        </div>                          
                        <div class="feature-list">
                           <div class="f-18 f-md-20 lh140 w400">
                              <span class="w600"> Customize GPT Assistance </span> according to your brand, Image, colour, and theme 
                           </div>
                        </div>
                        <div class="feature-list">
                           <div class="f-18 f-md-20 lh140 w400">
                              <span class="w600"> Next-Gen Drag And Drop Editor </span> To Create Pixel Perfect Business cards from Scratch 
                           </div>
                        </div>
                        <div class="feature-list">
                           <div class="f-18 f-md-20 lh140 w400">
                              <span class="w600"> Grab More Leads & Manage Them – </span> Manage Your Prospects & Customers with Inbuilt Lead Management 
                           </div>
                        </div>
                        <div class="feature-list">
                           <div class="f-18 f-md-20 lh140 w400">
                              Connect NexusGPT with your Favourite Tools, <span class="w600">20+ Integrations</span> with Autoresponders, and other Service 
                           </div>
                        </div>
                        <div class="feature-list">
                           <div class="f-18 f-md-20 lh140 w400">
                              <span class="w600">128 Bit Secured,</span> SSL Encryption For Maximum Security To Your Videos, Files, And Data 
                           </div>
                        </div>
                        <div class="feature-list">
                           <div class="f-18 f-md-20 lh140 w400">
                              <span class="w600">Completely Cloud-Based -</span> No Domain, Hosting, or Installation Required 
                           </div>
                        </div>
                        <div class="feature-list">
                           <div class="f-18 f-md-20 lh140 w400">
                              <span class="w600">Step-By-Step Video Training</span> and Tutorials Included 
                           </div>
                        </div>
                        <div class="feature-list">
                           <div class="f-18 f-md-20 lh140 w400">
                              <span class="w600">24*5 Customer Support</span>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>   
         </div>
      </div>
      <!-- Features Section End -->


     <!-- CTA Btn Start-->
     <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center mt20 mt-md20">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 yellow-clr">"AMITNEXUS"</span> for an Additional <span class="w700 yellow-clr">$3 Discount</span> on NexusGPT
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn px-md80">
                  <span class="text-center">Grab NexusGPT + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                  Use Coupon Code <span class="w700 yellow-clr">"NEXUSGPT"</span> for an Additional <span class="w700 yellow-clr">$50 Discount</span> on Bundle Deal
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflinkbundle']; ?>" class="text-center bonusbuy-btn px-md80">
                  <span class="text-center">Grab NexusGPT Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 smmltd">Days</span>
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Sec</span>
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Btn End -->
     
      <div class="exclusive-bonus">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                  <div class="white-clr f-24 f-md-36 lh140 w600">
                     Exclusive Bonus #1 : WebPrimo
                  </div>
                  <div class="f-18 f-md-20 w500 mt20 white-clr">
                     20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="WebPrimo-header-section">
         <div class="container">
            <div class="row">
               <div class="col-md-12"><img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/webprimo-logo.webp" class="img-fluid d-block mx-auto mr-md-auto"></div>
               <div class="col-12 f-20 f-md-24 w500 text-center white-clr lh150 text-capitalize mt20 mt-md65">
                  <div>
                     The Simple 7 Minute Agency Service That Gets Clients Daily (Anyone Can Do This)
                  </div>
                  <div class="mt5">
                     <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/viddeyo-post-head-sep.webp" class="img-fluid mx-auto d-block">
                  </div>
               </div>
               <div class="col-12 mt-md25 mt20">
                  <div class="f-md-45 f-28 w600 text-center white-clr lh150">
                     First Ever on JVZoo…  <br class="d-none d-md-block">
                     <span class="w700 f-md-42 WebPrimo-highlihgt-heading">The Game Changing Website Builder Lets You </span>  <br>Create Beautiful Local WordPress Websites &amp; Ecom Stores for Any Business in <span class="WebPrimo-higlight-text"> Just 7 Minutes</span>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md20 f-md-22 f-20 w400 white-clr text-center">
                  Easily create &amp; sell websites and stores for big profits to dentists, attorney, gyms, spas, restaurants, cafes, retail stores, book shops, coaches &amp; 100+ other niches...
               </div>
               <div class="col-12 mt20 mt-md40">
                  <div class="row d-flex align-items-center flex-wrap">
                     <div class="col-md-7 col-12 min-md-video-width-left">
                     <div class="col-12 responsive-video">
                     <iframe src="https://webprimo.dotcompal.com/video/embed/8ghfrcnhp5" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                        box-shadow: none !important;" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                  </div>
                     </div>
                     <div class="col-md-5 col-12 mt20 mt-md0 min-md-video-width-right">
                        <div class="WebPrimo-key-features-bg">
                           <ul class="list-head pl0 m0 f-16 lh150 w400 white-clr">
                              <li>Tap Into Fast-Growing $284 Billion Website Creation Industry</li>
                              <li>Help Businesses Go Digital in Their Hard Time (IN TODAYS DIGITAL AGE)</li>
                              <li>Create Elegant, Fast loading &amp; SEO Friendly Website within 7 Minutes</li>
                              <li>200+ Eye-Catching Templates for Local Niches Ready with Content</li>
                              <li>Bonus Live Training - Find Tons of Local Clients Hassle-Free</li>
                              <li>Agency License Included to Build an Incredible Income Helping Clients!</li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="WebPrimo-list-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row webprimo-header-list-block">
                     <div class="col-12 col-md-6 header-list-back">
                        <div class="f-16 f-md-18 lh150 w400">
                           <ul class="webprimo-header-bordered-list pl0 webprimo-orange-list">
                              <li><span class="w600">Create Ultra-Fast Loading &amp; Beautiful Websites</span></li>
                              <li>Instantly Create Local Sites, E-com Sites, and Blogs-<span class="w600">For Any Business Need.</span>
                              </li>
                              <li>Built On World's No. 1, <span class="w600">Super-Customizable WP FRAMEWORK For BUSINESSES</span>
                              </li>
                              <li>SEO Optimized Websites for <span class="w600">More Search Traffic</span></li>
                              <li><span class="w600">Get More Likes, Shares and Viral Traffic</span> with In-Built Social Media Tools</li>
                              <li><span class="w600">Create 100% Mobile Friendly</span> And Ultra-Fast Loading Websites.</li>
                              <li><span class="w600">Analytics &amp; Remarketing Ready</span> Websites</li>
                           </ul>
                        </div>
                     </div>
                     <div class="col-12 col-md-6 header-list-back">
                        <div class="f-16 f-md-18 lh150 w400">
                           <ul class="webprimo-header-bordered-list pl0 webprimo-orange-list">
                              <li>Newbie Friendly. <span class="w600">No Tech Skills Needed. No Monthly Fees.</span></li>
                              <li>Customize and Update Themes on Live Website Easily</li>
                              <li>Loaded with 30+ Themes with Super <span class="w600">Customizable 200+ Templates</span> and 2000+ possible design combinations.</li>
                              <li>Customise Websites <span class="w600">Without Touching Any Code</span></li>
                              <li><span class="w600">Accept Payments</span> For Your Services &amp; Products With Seamless WooCommerce Integration</li>
                              <li>Help Local Clients That Desperately Need Your Service And Make BIG Profits.</li>
                           </ul>
                        </div>
                     </div>
                     <div class="col-12  header-list-back">
                        <div class="f-16 f-md-18 lh150 w400">
                           <ul class="webprimo-header-bordered-list pl0 webprimo-orange-list">
                              <li class="w600">Agency License Included to Build an Incredible Income Helping Clients!</li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="gr-1">
         <div class="container-fluid p0">
            <picture>
               <source media="(min-width:768px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/webprimo-steps.webp">
               <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/webprimo-steps-mview.webp" style="width:100%" class="vidvee-mview">
               <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/webprimo-steps.webp" alt="Vidvee Steps" class="img-fluid" style="width: 100%;">
            </picture>
            <picture>
               <source media="(min-width:768px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/webprimo-doyouknow.webp">
               <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/webprimo-doyouknow-mview.webp">
               <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/webprimo-doyouknow.webp" alt="Flowers" style="width:100%;">
            </picture>
            <picture>
               <source media="(min-width:768px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/webprimo-introducing.webp">
               <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/yodrive/special-bonus/webprimo-introducing-mview.webp">
               <img src="https://cdn.oppyo.com/launches/yodrive/special-bonus/webprimo-introducing.webp" alt="Flowers" style="width:100%;">
            </picture>
         </div>
      </div>
      <!-------Exclusive Bonus----------->
      <div class="exclusive-bonus">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                  <div class="white-clr f-24 f-md-36 lh140 w600">
                     Exclusive Bonus #2 : Trendio
                  </div>
                  <div class="f-18 f-md-20 w500 mt20 white-clr">
                     20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!------Trendio Section------>
      <div class="tr-header-section">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="row">
                     <div class="col-md-3 text-center mx-auto">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 912.39 246.83" style="enable-background:new 0 0 912.39 246.83; max-height:50px;color: #023047;" xml:space="preserve">
                           <style type="text/css">
                              .st0{fill:#FFFFFF;}
                              .st1{opacity:0.3;}
                              .st2{fill:url(#SVGID_1_);}
                              .st3{fill:url(#SVGID_00000092439463549223389810000006963263784374171300_);}
                              .st4{fill:url(#SVGID_00000015332009897132114970000017423936041397956233_);}
                              .st5{fill:#023047;}
                           </style>
                           <g>
                              <g>
                                 <path class="st0" d="M18.97,211.06L89,141.96l2.57,17.68l10.86,74.64l2.12-1.97l160.18-147.9l5.24-4.84l6.8-6.27l5.24-4.85 l-25.4-27.51l-12.03,11.11l-5.26,4.85l-107.95,99.68l-2.12,1.97l-11.28-77.58l-2.57-17.68L0,177.17c0.31,0.72,0.62,1.44,0.94,2.15 c0.59,1.34,1.2,2.67,1.83,3.99c0.48,1.03,0.98,2.06,1.5,3.09c0.37,0.76,0.76,1.54,1.17,2.31c2.57,5.02,5.43,10,8.57,14.93 c0.58,0.89,1.14,1.76,1.73,2.65L18.97,211.06z"></path>
                              </g>
                              <g>
                                 <g>
                                    <polygon class="st0" points="328.54,0 322.97,17.92 295.28,106.98 279.91,90.33 269.97,79.58 254.51,62.82 244.58,52.05 232.01,38.45 219.28,24.66 "></polygon>
                                 </g>
                              </g>
                           </g>
                           <g class="st1">
                              <g>
                                 <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="17.428" y1="42.82" x2="18.9726" y2="42.82" gradientTransform="matrix(1 0 0 -1 0 252.75)">
                                    <stop offset="0" style="stop-color:#000000"></stop>
                                    <stop offset="1" style="stop-color:#333333"></stop>
                                 </linearGradient>
                                 <path class="st2" d="M17.43,208.8l1.54,2.26c-0.37-0.53-0.73-1.05-1.09-1.58c-0.02-0.02-0.02-0.03-0.03-0.03 c-0.06-0.09-0.11-0.17-0.17-0.27C17.6,209.06,17.51,208.92,17.43,208.8z"></path>
                                 <linearGradient id="SVGID_00000177457867953882822120000005144526232562103955_" gradientUnits="userSpaceOnUse" x1="18.98" y1="70.45" x2="91.57" y2="70.45" gradientTransform="matrix(1 0 0 -1 0 252.75)">
                                    <stop offset="0" style="stop-color:#000000"></stop>
                                    <stop offset="1" style="stop-color:#333333"></stop>
                                 </linearGradient>
                                 <path style="fill:url(#SVGID_00000177457867953882822120000005144526232562103955_);" d="M89,141.96l2.57,17.68l-63.83,63 c-1.19-1.45-2.34-2.9-3.46-4.35c-1.84-2.4-3.6-4.81-5.3-7.22L89,141.96z"></path>
                                 <linearGradient id="SVGID_00000010989203515549635820000018393619450353154703_" gradientUnits="userSpaceOnUse" x1="104.55" y1="120.005" x2="333.22" y2="120.005" gradientTransform="matrix(1 0 0 -1 0 252.75)">
                                    <stop offset="0" style="stop-color:#000000"></stop>
                                    <stop offset="1" style="stop-color:#333333"></stop>
                                 </linearGradient>
                                 <polygon style="fill:url(#SVGID_00000010989203515549635820000018393619450353154703_);" points="333.22,15.61 299.96,122.58 274.65,95.18 107.11,249.88 104.55,232.31 264.73,84.41 269.97,79.58 279.91,90.33 295.28,106.98 322.97,17.92 "></polygon>
                              </g>
                           </g>
                           <g>
                              <g>
                                 <g>
                                    <path class="st5" d="M229.46,241.94c-12.28,5.37-23.49,8.06-33.57,8.06c-9.63,0-18.21-2.44-25.71-7.35 c-23.05-15.07-23.72-46.17-23.72-49.67V61.67h32.2v30h39.27v32.2h-39.27v69.04c0.07,4.46,1.88,18.1,9.2,22.83 c5.51,3.55,15.7,2.38,28.68-3.3L229.46,241.94z"></path>
                                 </g>
                              </g>
                           </g>
                           <g>
                              <g>
                                 <path class="st5" d="M250.4,164.29c0-15.97-0.24-27.35-0.97-38h25.9l0.97,22.51h0.97c5.81-16.7,19.61-25.17,32.19-25.17 c2.9,0,4.6,0.24,7.02,0.73v28.08c-2.42-0.48-5.08-0.97-8.71-0.97c-14.28,0-23.96,9.2-26.63,22.51c-0.48,2.66-0.97,5.81-0.97,9.2 v61H250.4V164.29z"></path>
                              </g>
                           </g>
                           <g>
                              <g>
                                 <path class="st5" d="M459.28,161.39c0-13.55-0.24-24.93-0.97-35.1h26.14l1.45,17.67h0.73c5.08-9.2,17.91-20.33,37.52-20.33 c20.57,0,41.87,13.31,41.87,50.59v69.95h-29.77v-66.56c0-16.94-6.29-29.77-22.51-29.77c-11.86,0-20.09,8.47-23.24,17.43 c-0.97,2.66-1.21,6.29-1.21,9.68v69.23h-30.01V161.39z"></path>
                              </g>
                           </g>
                           <g>
                              <g>
                                 <path class="st5" d="M706.41,72.31V211c0,12.1,0.48,25.17,0.97,33.16h-26.63l-1.21-18.64h-0.48c-7.02,13.07-21.3,21.3-38.49,21.3 c-28.08,0-50.35-23.96-50.35-60.27c-0.24-39.45,24.45-62.93,52.77-62.93c16.22,0,27.84,6.78,33.16,15.49h0.48v-66.8 C676.63,72.31,706.41,72.31,706.41,72.31z M676.64,175.43c0-2.42-0.24-5.33-0.73-7.75c-2.66-11.62-12.1-21.06-25.66-21.06 c-19.12,0-29.77,16.94-29.77,38.97c0,21.54,10.65,37.28,29.53,37.28c12.1,0,22.75-8.23,25.66-21.06c0.73-2.66,0.97-5.57,0.97-8.71 V175.43z"></path>
                              </g>
                           </g>
                           <path class="st0" d="M769.68,89.95c-0.11-0.53-0.24-1.04-0.39-1.55c-0.12-0.39-0.25-0.76-0.39-1.14c-0.12-0.32-0.25-0.64-0.39-0.95 c-0.12-0.27-0.25-0.54-0.39-0.81c-0.12-0.24-0.25-0.47-0.39-0.7c-0.12-0.21-0.25-0.42-0.39-0.63c-0.13-0.19-0.26-0.38-0.39-0.57 c-0.13-0.17-0.26-0.35-0.39-0.51s-0.26-0.32-0.39-0.47s-0.26-0.3-0.39-0.44s-0.26-0.28-0.39-0.41c-0.13-0.13-0.26-0.25-0.39-0.37 s-0.26-0.23-0.39-0.35c-0.13-0.11-0.26-0.22-0.39-0.33c-0.13-0.1-0.26-0.2-0.39-0.3c-0.13-0.1-0.26-0.19-0.39-0.28 s-0.26-0.18-0.39-0.27c-0.13-0.08-0.26-0.16-0.39-0.24c-0.13-0.08-0.26-0.16-0.39-0.24c-0.13-0.07-0.26-0.14-0.39-0.21 s-0.26-0.14-0.39-0.2s-0.26-0.13-0.39-0.19c-0.13-0.06-0.26-0.12-0.39-0.18s-0.26-0.11-0.39-0.17c-0.13-0.05-0.26-0.1-0.39-0.15 s-0.26-0.1-0.39-0.14s-0.26-0.09-0.39-0.13s-0.26-0.08-0.39-0.12s-0.26-0.07-0.39-0.11c-0.13-0.03-0.26-0.07-0.39-0.1 s-0.26-0.06-0.39-0.09s-0.26-0.05-0.39-0.08s-0.26-0.05-0.39-0.07s-0.26-0.04-0.39-0.06s-0.26-0.04-0.39-0.06s-0.26-0.03-0.39-0.04 s-0.26-0.03-0.39-0.04s-0.26-0.02-0.39-0.03s-0.26-0.02-0.39-0.03s-0.26-0.01-0.39-0.02c-0.13,0-0.26-0.01-0.39-0.01 s-0.26-0.01-0.39-0.01s-0.26,0.01-0.39,0.01s-0.26,0-0.39,0.01c-0.13,0-0.26,0.01-0.39,0.02c-0.13,0.01-0.26,0.02-0.39,0.03 s-0.26,0.02-0.39,0.03s-0.26,0.03-0.39,0.04c-0.13,0.02-0.26,0.03-0.39,0.04c-0.13,0.02-0.26,0.04-0.39,0.06s-0.26,0.04-0.39,0.06 s-0.26,0.05-0.39,0.08s-0.26,0.05-0.39,0.08s-0.26,0.06-0.39,0.09s-0.26,0.07-0.39,0.1c-0.13,0.04-0.26,0.07-0.39,0.11 s-0.26,0.08-0.39,0.12s-0.26,0.09-0.39,0.13c-0.13,0.05-0.26,0.09-0.39,0.14s-0.26,0.1-0.39,0.15s-0.26,0.11-0.39,0.16 c-0.13,0.06-0.26,0.12-0.39,0.18s-0.26,0.12-0.39,0.19s-0.26,0.14-0.39,0.21s-0.26,0.14-0.39,0.21c-0.13,0.08-0.26,0.16-0.39,0.24 c-0.13,0.08-0.26,0.16-0.39,0.24c-0.13,0.09-0.26,0.18-0.39,0.27s-0.26,0.19-0.39,0.28c-0.13,0.1-0.26,0.2-0.39,0.3 c-0.13,0.11-0.26,0.22-0.39,0.33s-0.26,0.23-0.39,0.34c-0.13,0.12-0.26,0.24-0.39,0.37c-0.13,0.13-0.26,0.27-0.39,0.4 c-0.13,0.14-0.26,0.28-0.39,0.43s-0.26,0.3-0.39,0.46c-0.13,0.16-0.26,0.33-0.39,0.5c-0.13,0.18-0.26,0.37-0.39,0.55 c-0.13,0.2-0.26,0.4-0.39,0.61c-0.13,0.22-0.26,0.45-0.39,0.68c-0.14,0.25-0.27,0.51-0.39,0.77c-0.14,0.3-0.27,0.6-0.39,0.9 c-0.14,0.36-0.27,0.73-0.39,1.1c-0.15,0.48-0.28,0.98-0.39,1.48c-0.25,1.18-0.39,2.42-0.39,3.7c0,1.27,0.14,2.49,0.39,3.67 c0.11,0.5,0.24,0.99,0.39,1.47c0.12,0.37,0.24,0.74,0.39,1.1c0.12,0.3,0.25,0.6,0.39,0.89c0.12,0.26,0.25,0.51,0.39,0.76 c0.12,0.23,0.25,0.45,0.39,0.67c0.12,0.2,0.25,0.41,0.39,0.6c0.13,0.19,0.25,0.37,0.39,0.55c0.13,0.17,0.25,0.34,0.39,0.5 c0.13,0.15,0.26,0.3,0.39,0.45c0.13,0.14,0.26,0.28,0.39,0.42c0.13,0.13,0.26,0.26,0.39,0.39c0.13,0.12,0.26,0.25,0.39,0.37 c0.13,0.11,0.26,0.22,0.39,0.33s0.26,0.21,0.39,0.32c0.13,0.1,0.26,0.2,0.39,0.3c0.13,0.09,0.26,0.18,0.39,0.27s0.26,0.18,0.39,0.26 s0.26,0.16,0.39,0.24c0.13,0.08,0.26,0.15,0.39,0.23c0.13,0.07,0.26,0.14,0.39,0.21s0.26,0.13,0.39,0.19 c0.13,0.06,0.26,0.13,0.39,0.19c0.13,0.06,0.26,0.11,0.39,0.17s0.26,0.11,0.39,0.17c0.13,0.05,0.26,0.1,0.39,0.14 c0.13,0.05,0.26,0.1,0.39,0.14s0.26,0.08,0.39,0.12s0.26,0.08,0.39,0.12s0.26,0.07,0.39,0.1s0.26,0.07,0.39,0.1s0.26,0.06,0.39,0.08 c0.13,0.03,0.26,0.06,0.39,0.08c0.13,0.02,0.26,0.05,0.39,0.07s0.26,0.04,0.39,0.06s0.26,0.04,0.39,0.05 c0.13,0.02,0.26,0.03,0.39,0.04s0.26,0.03,0.39,0.04s0.26,0.02,0.39,0.03s0.26,0.02,0.39,0.03s0.26,0.01,0.39,0.01 s0.26,0.01,0.39,0.01c0.05,0,0.1,0,0.15,0c0.08,0,0.16,0,0.24-0.01c0.13,0,0.26,0,0.39-0.01c0.13,0,0.26,0,0.39-0.01 s0.26-0.02,0.39-0.03s0.26-0.01,0.39-0.03c0.13-0.01,0.26-0.02,0.39-0.04c0.13-0.01,0.26-0.03,0.39-0.04 c0.13-0.02,0.26-0.03,0.39-0.05c0.13-0.02,0.26-0.04,0.39-0.06s0.26-0.04,0.39-0.06s0.26-0.05,0.39-0.08s0.26-0.05,0.39-0.08 s0.26-0.06,0.39-0.09s0.26-0.06,0.39-0.1s0.26-0.07,0.39-0.11s0.26-0.08,0.39-0.12s0.26-0.08,0.39-0.13 c0.13-0.04,0.26-0.09,0.39-0.14s0.26-0.1,0.39-0.15s0.26-0.11,0.39-0.16c0.13-0.06,0.26-0.11,0.39-0.17s0.26-0.12,0.39-0.18 s0.26-0.13,0.39-0.2s0.26-0.14,0.39-0.21s0.26-0.15,0.39-0.23s0.26-0.15,0.39-0.24c0.13-0.08,0.26-0.17,0.39-0.26 c0.13-0.09,0.26-0.18,0.39-0.27c0.13-0.1,0.26-0.19,0.39-0.29s0.26-0.21,0.39-0.32s0.26-0.22,0.39-0.33 c0.13-0.12,0.26-0.24,0.39-0.36c0.13-0.13,0.26-0.26,0.39-0.39c0.13-0.14,0.26-0.28,0.39-0.42c0.13-0.15,0.26-0.3,0.39-0.45 c0.13-0.16,0.26-0.33,0.39-0.49c0.13-0.18,0.26-0.36,0.39-0.54c0.13-0.2,0.26-0.4,0.39-0.6c0.14-0.22,0.26-0.44,0.39-0.67 c0.14-0.25,0.27-0.5,0.39-0.76c0.14-0.29,0.27-0.58,0.39-0.88c0.14-0.36,0.27-0.72,0.39-1.1c0.15-0.48,0.28-0.97,0.39-1.46 c0.25-1.17,0.39-2.4,0.39-3.66C770.03,92.19,769.9,91.05,769.68,89.95z"></path>
                           <g>
                              <g>
                                 <rect x="738.36" y="126.29" class="st5" width="30.01" height="117.88"></rect>
                              </g>
                           </g>
                           <g>
                              <g>
                                 <path class="st5" d="M912.39,184.14c0,43.33-30.5,62.69-60.51,62.69c-33.4,0-59.06-22.99-59.06-60.75 c0-38.73,25.41-62.45,61-62.45C888.91,123.63,912.39,148.32,912.39,184.14z M823.56,185.35c0,22.75,11.13,39.94,29.29,39.94 c16.94,0,28.8-16.7,28.8-40.42c0-18.4-8.23-39.45-28.56-39.45C832.03,145.41,823.56,165.74,823.56,185.35z"></path>
                              </g>
                           </g>
                           <g>
                              <path class="st5" d="M386,243.52c-2.95,0-5.91-0.22-8.88-0.66c-15.75-2.34-29.65-10.67-39.13-23.46 c-9.48-12.79-13.42-28.51-11.08-44.26c2.34-15.75,10.67-29.65,23.46-39.13c12.79-9.48,28.51-13.42,44.26-11.08 s29.65,10.67,39.13,23.46l7.61,10.26l-55.9,41.45l-15.21-20.51l33.41-24.77c-3.85-2.36-8.18-3.94-12.78-4.62 c-9-1.34-17.99,0.91-25.3,6.33s-12.07,13.36-13.41,22.37c-1.34,9,0.91,17.99,6.33,25.3c5.42,7.31,13.36,12.07,22.37,13.41 c9,1.34,17.99-0.91,25.3-6.33c4.08-3.02,7.35-6.8,9.72-11.22l22.5,12.08c-4.17,7.77-9.89,14.38-17.02,19.66 C411,239.48,398.69,243.52,386,243.52z"></path>
                           </g>
                        </svg>
                     </div>
                  </div>
               </div>
               <div class="col-12 text-center lh150 mt20 mt-md50 px15 px-md0">
                  <div class="trpre-heading f-20 f-md-22 w500 lh150 yellow-clr">
                     <span>
                     It’s Time to Get Over Boring Content &amp; Cash into The Latest Trending Topics in 2022 
                     </span>
                  </div>
               </div>
               <div class="col-12 mt-md25 mt20 f-md-45 f-28 w500 text-center black-clr lh150">
                  Breakthrough A.I. Technology <span class="w700 yellow-clr">Creates Traffic Pulling Websites Packed with Trendy Content &amp; Videos from Just One Keyword</span> in 60 Seconds... 
               </div>
               <div class="col-12 mt-md25 mt20 f-20 f-md-22 w500 text-center lh150 black-clr">
                  Anyone can easily create &amp; sell traffic generating websites for dentists, attorney, affiliates,<br class="d-none d-md-block"> coaches, SAAS, retail stores, book shops, gyms, spas, restaurants, &amp; 100+ other niches...
               </div>
            </div>
            <div class="row d-flex align-items-center flex-wrap mt20 mt-md40">
               <div class="col-md-7 col-12 min-md-video-width-left">
                  <div class="col-12 responsive-video">
                     <iframe src="https://trendio.dotcompal.com/video/embed/2p8pv1cndk" style=" position: absolute;top: 0;left: 0;width: 100%;height: 100%; background: transparent !important;
                        box-shadow: none !important;" frameborder="0" allow="fullscreen" allowfullscreen=""></iframe>
                  </div>
               </div>
               <div class="col-md-5 col-12 mt20 mt-md0 min-md-video-width-right">
                  <div class="key-features-bg1">
                     <ul class="list-head1 pl0 m0 f-16 lh150 w500 black-clr">
                        <li>Works Seamlessly in Any Niche or Topic - <span class="w600">Just One Keyword</span> </li>
                        <li>All of That Without Content Creation, Editing, Camera, or Tech Hassles Ever </li>
                        <li>Drive Tons of FREE Viral Traffic Using the POWER Of Trendy Content </li>
                        <li>Grab More Authority, Engagement, &amp; Leads on your Business Website </li>
                        <li>Monetize Other’s Content Legally with Banner Ads, Affiliate Offers &amp; AdSense </li>
                        <li class="w600">FREE AGENCY LICENSE INCLUDED TO BUILD AN INCREDIBLE INCOME </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="tr-all">
         <picture>
            <source media="(min-width:767px)" srcset="https://cdn.oppyo.com/launches/jobiin/special-bonus/tr-lst-dt.webp" class="img-fluid d-block mx-auto" style="width:100%">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/jobiin/special-bonus/tr-lst-mb.webp" class="img-fluid d-block mx-auto">
            <img src="https://cdn.oppyo.com/launches/jobiin/special-bonus/tr-lst-dt.webp" alt="Steps" class="img-fluid d-block mx-auto" style="width:100%">
         </picture>
         <picture>
            <source media="(min-width:767px)" srcset="https://cdn.oppyo.com/launches/jobiin/special-bonus/tr-dt.webp" class="img-fluid d-block mx-auto" style="width:100%">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/jobiin/special-bonus/tr-mb.webp" class="img-fluid d-block mx-auto">
            <img src="https://cdn.oppyo.com/launches/jobiin/special-bonus/tr-dt.webp" alt="Steps" class="img-fluid d-block mx-auto" style="width:100%">
         </picture>
         <picture>
            <source media="(min-width:767px)" srcset="https://cdn.oppyo.com/launches/jobiin/special-bonus/pr-dt.webp" class="img-fluid d-block mx-auto" style="width:100%">
            <source media="(min-width:320px)" srcset="https://cdn.oppyo.com/launches/jobiin/special-bonus/pr-mb.webp" class="img-fluid d-block mx-auto">
            <img src="https://cdn.oppyo.com/launches/jobiin/special-bonus/pr-dt.webp" alt="Steps" class="img-fluid d-block mx-auto" style="width:100%">
         </picture>
      </div>
      <!------Trendio Section Ends------>
      <!-------Exclusive Bonus----------->
      <div class="exclusive-bonus">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                  <div class="white-clr f-24 f-md-36 lh140 w600">
                     Exclusive Bonus #3 : WriterArc
                  </div>
                  <div class="f-18 f-md-20 w500 mt20 white-clr">
                     20 ACCOUNTS TO SELL as FE BONUS 200 ACCOUNTS TO SELL as ANY OTO BONUS <br> If you already have this bonus, you will get more licenses to sell (really great to have more)
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="writer-header-section">
         <div class="container">
            <div class="row">
               <div class="col-12 d-flex align-items-center justify-content-center flex-wrap flex-md-nowrap">
                  <svg viewBox="0 0 247 41" fill="none" xmlns="http://www.w3.org/2000/svg" style="height:30px;">
                     <path d="M46.7097 6.24429L37.4161 39.478H29.5526L23.3077 15.8145L16.778 39.478L8.96167 39.5251L0 6.24429H7.15048L13.0122 32.0507L19.7796 6.24429H27.2149L33.6011 31.9074L39.5121 6.24429H46.7097Z" fill="white"></path>
                     <path d="M61.0803 13.9091C62.4632 13.1149 64.0429 12.7179 65.8234 12.7179V19.7175H64.0593C61.9613 19.7175 60.3816 20.2107 59.3162 21.1931C58.2508 22.1776 57.7202 23.8906 57.7202 26.3344V39.476H51.0471V13.1006H57.7202V17.196C58.5786 15.8002 59.6973 14.7052 61.0803 13.9111V13.9091Z" fill="white"></path>
                     <path d="M70.7549 8.83947C69.9764 8.09448 69.5871 7.1653 69.5871 6.05396C69.5871 4.94262 69.9764 4.01548 70.7549 3.26845C71.5335 2.52347 72.5108 2.14893 73.6868 2.14893C74.8629 2.14893 75.8381 2.52142 76.6187 3.26845C77.3973 4.01548 77.7866 4.94262 77.7866 6.05396C77.7866 7.1653 77.3973 8.09448 76.6187 8.83947C75.8402 9.5865 74.8629 9.959 73.6868 9.959C72.5108 9.959 71.5335 9.5865 70.7549 8.83947ZM76.9752 13.1006V39.476H70.3021V13.1006H76.9752Z" fill="white"></path>
                     <path d="M91.3213 18.5755V31.3364C91.3213 32.2247 91.5364 32.8673 91.9646 33.2644C92.3928 33.6614 93.1161 33.8599 94.1323 33.8599H97.2302V39.478H93.0362C87.4121 39.478 84.599 36.7478 84.599 31.2893V18.5775H81.454V13.1027H84.599V6.57996H91.3192V13.1027H97.2302V18.5775H91.3192L91.3213 18.5755Z" fill="white"></path>
                     <path d="M126.402 28.2889H107.097C107.255 30.1944 107.923 31.6864 109.099 32.765C110.275 33.8436 111.72 34.3839 113.437 34.3839C115.916 34.3839 117.678 33.3217 118.727 31.1931H125.924C125.162 33.7331 123.699 35.8186 121.54 37.4539C119.378 39.0892 116.725 39.9058 113.58 39.9058C111.037 39.9058 108.757 39.343 106.741 38.2152C104.723 37.0896 103.149 35.4932 102.022 33.4301C100.894 31.3671 100.33 28.9868 100.33 26.2893C100.33 23.5918 100.885 21.1645 101.998 19.0994C103.11 17.0364 104.667 15.4502 106.669 14.3389C108.671 13.2275 110.974 12.6729 113.58 12.6729C116.186 12.6729 118.337 13.2132 120.325 14.2918C122.31 15.3704 123.851 16.9033 124.949 18.8866C126.045 20.8698 126.594 23.1477 126.594 25.7183C126.594 26.67 126.531 27.5276 126.404 28.2889H126.402ZM119.681 23.8129C119.649 22.0998 119.03 20.7265 117.823 19.695C116.614 18.6635 115.137 18.1477 113.389 18.1477C111.736 18.1477 110.347 18.6471 109.218 19.6479C108.089 20.6487 107.399 22.0364 107.145 23.8149H119.679L119.681 23.8129Z" fill="white"></path>
                     <path d="M141.485 13.9091C142.868 13.1149 144.448 12.7179 146.228 12.7179V19.7175H144.464C142.366 19.7175 140.787 20.2107 139.721 21.1931C138.656 22.1776 138.125 23.8906 138.125 26.3344V39.476H131.452V13.1006H138.125V17.196C138.984 15.8002 140.102 14.7052 141.485 13.9111V13.9091Z" fill="white"></path>
                     <path d="M213.892 14.0034C215.275 13.2093 216.854 12.8122 218.635 12.8122V19.8118H216.871C214.773 19.8118 213.193 20.3051 212.128 21.2875C211.062 22.2719 210.532 23.985 210.532 26.4287V39.5703H203.858V13.1929H210.532V17.2883C211.39 15.8925 212.509 14.7975 213.892 14.0034Z" fill="white"></path>
                     <path d="M223.066 19.2162C224.179 17.1696 225.72 15.5813 227.691 14.4557C229.66 13.33 231.915 12.7651 234.458 12.7651C237.73 12.7651 240.441 13.5818 242.584 15.217C244.729 16.8523 246.165 19.1446 246.897 22.0979H239.699C239.318 20.9559 238.675 20.0594 237.769 19.4086C236.863 18.7578 235.743 18.4323 234.409 18.4323C232.503 18.4323 230.993 19.1221 229.881 20.5036C228.768 21.8851 228.213 23.8437 228.213 26.3836C228.213 28.9235 228.768 30.8351 229.881 32.2166C230.993 33.5981 232.501 34.2879 234.409 34.2879C237.109 34.2879 238.873 33.0824 239.699 30.6694H246.897C246.165 33.5265 244.721 35.7962 242.559 37.4786C240.398 39.161 237.697 40.0021 234.456 40.0021C231.913 40.0021 229.658 39.4393 227.689 38.3116C225.718 37.1859 224.177 35.5977 223.064 33.5511C221.952 31.5044 221.397 29.1159 221.397 26.3857C221.397 23.6554 221.952 21.267 223.064 19.2203L223.066 19.2162Z" fill="white"></path>
                     <path d="M187.664 20.0164C184.058 18.8846 180.219 18.2726 176.24 18.2726C171.331 18.2726 166.639 19.2018 162.33 20.8923L174.404 0V9.42898C173.372 9.7851 172.628 10.7675 172.628 11.9218C172.628 13.377 173.81 14.5579 175.269 14.5579C176.728 14.5579 177.91 13.377 177.91 11.9218C177.91 10.7695 177.172 9.79124 176.142 9.43307V0.0818666L187.662 20.0164H187.664Z" fill="#5055BE"></path>
                     <path d="M198.911 39.474C192.278 28.3013 180.084 20.8105 166.137 20.8105C164.83 20.8105 163.537 20.876 162.263 21.0049C162.164 21.0152 162.064 21.0254 161.966 21.0356C158.315 21.4327 154.817 22.3475 151.549 23.7024C153.113 23.7024 154.655 23.7966 156.169 23.9808C157.518 24.1425 158.843 24.3758 160.142 24.6766L151.584 39.4822C157.862 34.5068 165.748 31.4675 174.337 31.2689C174.636 31.2628 174.935 31.2587 175.234 31.2587C184.196 31.2587 192.434 34.3512 198.939 39.5272L198.911 39.4761V39.474Z" fill="white"></path>
                  </svg>
               </div>
               <div class="col-12 mt20 mt-md40 text-center">
                  <div class="f-20 f-md-22 w500 blue-clr1 lh140">
                     Never Waste Your Time &amp; Money on Manually Writing Boring Content Again…
                  </div>
               </div>
               <div class="col-12 mt-md30 mt20 f-md-50 f-28 w700 text-center white-clr lh140">
                 <span style="border-bottom:4px solid #fff">Futuristic A.I. Technology</span>  Creates Stunning Content for <span class="gradient-orange"> Any Local or Online Niche 10X Faster…Just by Using a Keyword</span>
               </div>
               <div class="col-12 mt-md30 mt20 text-center">
                  <div class="post-headline f-18 f-md-22 w700 text-center lh160 white-clr text-capitalize">
                     You Sit Back &amp; Relax, WriterArc Will Create Top Converting Content for You <br class="d-none d-md-block">  No Prior Skill Needed | No Monthly Fee Ever…
                  </div>
               </div>
            </div>
            <div class="row mt20 mt-md50">
               <div class="col-md-7 col-12 min-md-video-width-left">
                  <!-- <img src="assets/images/product-box.webp" class="img-fluid d-block mx-auto"> -->
                  <div class="col-12 responsive-video border-video">
                     <iframe src="https://writerarc.dotcompal.com/video/embed/spru84q6w8" style="width:100%; height:100%" frameborder="0" allow="autoplay; fullscreen" allowfullscreen=""></iframe>
                  </div>
               </div>
               <div class="col-md-5 col-12 min-md-video-width-right mt20 mt-md0">
                  <ul class="list-head pl0 m0 f-18 f-md-20 lh150 w500 white-clr">
                     <li><span class="orange-clr1">Create Content</span> for Any Local or Online Business</li>
                     <li><span class="orange-clr1"> Let Our Super Powerful</span> A.I. Engine Do the Heavy Lifting</li> 
                     <li><span class="orange-clr1">Preloaded with 50+ Copywriting Templates, </span> like Ads, Video Scripts, Website Content, and much more.</li> 
                     <li><span class="orange-clr1">Works in 35+ Languages</span>  and 22+ Tons of Writing</li>
                     <li> <span class="orange-clr1">Download Your Content</span> in Word/PDF Format</li>
                     <li><span class="orange-clr1">Free Commercial License – </span>Sell Service to Local Clients for BIG Profits</li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <div class="gr-1">
         <div class="container-fluid p0">
            <picture>
               <source media="(min-width:768px)" srcset="assets/images/writerarc-steps.webp">
               <source media="(min-width:320px)" srcset="assets/images/writerarc-mview-steps.webp" style="width:100%" class="img-fluid mx-auto">
               <img src="assets/images/writerarc-steps.webp" alt="WriterArc Steps" class="img-fluid" style="width: 100%;">
            </picture>
         </div>
      </div>
      <!-- Bonus Section Header Start -->
      <div class="bonus-header">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-10 mx-auto heading-bg text-center">
                  <div class="f-24 f-md-36 lh140 w700"> When You Purchase NexusGPT, You Also Get <br class="hidden-xs"> Instant Access To These 20 Exclusive Bonuses</div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus Section Header End -->
      <!-- Bonus #1 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 1</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus1.webp" class="img-fluid mx-auto d-block" alt="Bonus1">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Easy Video Sales Pages
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>Create video sales pages using a proven, winning formula! Simple sucessful formula for quick sales pages all the time!  </b></li>
                           <li> Sales page plays a very big role in converting your website visitors into buyers. If you create a sales page for granted, you will just waste your time, effort and money in selling your services or products you offer.</li>
                           <li> In this amazing software, you are about to have those qualifications and expect a huge sale in your offers.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #1 Section End -->
      <!-- Bonus #2 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 2</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30">
                  <div class="row">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="assets/images/bonus2.webp" class="img-fluid mx-auto d-block" alt="Bonus2">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color mt20 mt-md0 text-capitalize">
                        100 Mobile Web Templates
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>Make Thousands of Dollars Online Offering Mobile Friendly Websites to Your Local Business Clients!</li>
                           <li>Since the launch of the latest Google Algorithm update called Mobilegeddon, local business owners, internet marketers, webmasters and SEO's are now concern about the looks of their website in various types of mobile devices as this will also impact the website rankings in mobile search.</li>
                           <li>Though this will not affect the website's ranking if you are browsing the website in desktop computers, this is a huge loss if you will not optimize your website for mobile devices like tablets and smartphones.</li>
                          
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #2 Section End -->
      <!-- Bonus #3 Section Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 3</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus3.webp" class="img-fluid mx-auto d-block " alt="Bonus3">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Modern Affiliate Marketing Video Upgrade
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>Picking the Right Affiliate Program to Promote all Boils Down to ROI.</b></li>
                           <li>Make no mistake, if you want to succeed with your affiliate marketing business, you have to focus on ROI. If you have a fuzzy idea of what return on investment means, you're playing the game wrong.</li>
                           <li>With this video course you will learn the secrets of modern affiliate marketing!</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #3 Section End -->
      <!-- Bonus #4 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 4</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="assets/images/bonus4.webp" class="img-fluid mx-auto d-block " alt="Bonus4">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        SEO And Tracking
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>In this 6-part video course you will learn how to turn long or affiliate links into short and pretty links.</b></li>
                           <li>Also it will teach you how to use 'related posts' to help visitors find related content. </li>
                          
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #4 End -->
      <!-- Bonus #5 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 5</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus5.webp" class="img-fluid mx-auto d-block " alt="Bonus5">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Social Media Marketing Boost
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>Discover 100 Powerful Social Media Marketing Tips That Will Boost Your Following, Gain Authority And Increase Engagement On Social Media!</b></li>
                           <li>Social Media Marketing is indeed a huge help for many internet marketers to boost their website traffic, make sales and build authority.</li>
                           <li>The thing is that, if you just got started in the industry, you might be thinking that you can learn it by reading various blogs that you can think of. </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #5 End -->
      <!-- CTA Btn Start-->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center mt20 mt-md20">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 yellow-clr">"AMITNEXUS"</span> for an Additional <span class="w700 yellow-clr">$3 Discount</span> on NexusGPT
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn px-md80">
                  <span class="text-center">Grab NexusGPT + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                  Use Coupon Code <span class="w700 yellow-clr">"NEXUSGPT"</span> for an Additional <span class="w700 yellow-clr">$50 Discount</span> on Bundle Deal
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflinkbundle']; ?>" class="text-center bonusbuy-btn px-md80">
                  <span class="text-center">Grab NexusGPT Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 smmltd">Days</span>
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Sec</span>
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Btn End -->
     <!-- Bonus #6 Start -->
     <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 6</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12 order-md-2">
                        <img src="assets/images/bonus6.webp" class="img-fluid mx-auto d-block " alt="Bonus6">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0 order-md-1">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Mobile ECommerce Simplified
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b> Brand New High Quality PLR Lead Magnet Targets Ecommerce and Online Store Owners</b> </li>
                           <li>This mobile eCommerce video course explains how to create user-friendly, mobile optimized web stores in less than 24 hours.</li>
                           <li>Today, more people than ever are shopping online using their mobile devices and the number is growing daily! This means that if you (or your client's) business is not optimized for mobile, money is being left on the table.</li>
                          
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #6 End -->
      <!-- Bonus #7 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 7</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus7.webp" class="img-fluid mx-auto d-block " alt="Bonus7">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Web Traffic Excellence
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                          
                           <li><b>Discover How to Build Hundreds of Content Rich, Dynamically Changing, Keyword Covered Web Pages in Mere Minutes. Start Building the High-Quality Content Sites You Need to Succeed on the Internet Today! </b></li>
                           <li>This amazing software program holds the secret key to chopping up syndicated content and public domain works to help you win the search engine game, get higher rankings and bring in more traffic than you can ever hope to handle! </li>
                           
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #7 End -->
      <!-- Bonus #8 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 8</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus8.webp" class="img-fluid mx-auto d-block " alt="Bonus8">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        SEO And PPC Ninja Calculator
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>Quickly Make And Brand Your Own Iframes!</b></li>
                           <li>This iFrame generator helps you to instantly create customized iFrames for your web site/blog. Fill in the fields below and within seconds you will receive the HTML code for your custom iFrame.</li>
                           <li>Simply follow the steps below to generate your iFrame code, and freely use it on your website blogs. </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #8 End -->
      <!-- Bonus #9 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 9</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus9.webp" class="img-fluid mx-auto d-block " alt="Bonus9">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Sell More With These Content Writing Tips
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li>Learn How You Can Sell More with Content Marketing!</li>
                           <li>Exceptional content is the key component in the transmission of your message, generating traffic, and selling your product or service. However, there are several other elements that will make your content or effective with your readers and prompt them to take the action you want them to take.</b></li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #9 End -->
      <!-- Bonus #10 start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 10</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus10.webp" class="img-fluid mx-auto d-block " alt="Bonus10">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Doubling Your Sales With These Tricks
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>Learn How to Double your Sales with These Tricks!  </b></li>
                           <li>Many successful digital entrepreneurs have likely already stated this at one point or another, but this is very important, which is why this report will going to say it one more time. </li>
                           <li>Offer a free sample or some type of trial period. Whatever your specific service is, make sure that you provide one with a seven-day trial that is just one single dollar. This can include a video, a chapter or some type of product that you think will incentivize your custom toward choosing you for a prolonged period of time.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #10 End -->
      <!-- CTA Btn Start-->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center mt20 mt-md20">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 yellow-clr">"AMITNEXUS"</span> for an Additional <span class="w700 yellow-clr">$3 Discount</span> on NexusGPT
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn px-md80">
                  <span class="text-center">Grab NexusGPT + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                  Use Coupon Code <span class="w700 yellow-clr">"NEXUSGPT"</span> for an Additional <span class="w700 yellow-clr">$50 Discount</span> on Bundle Deal
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflinkbundle']; ?>" class="text-center bonusbuy-btn px-md80">
                  <span class="text-center">Grab NexusGPT Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 smmltd">Days</span>
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Sec</span>
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Btn End -->
       <!-- Bonus #11 start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 11</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus11.webp" class="img-fluid mx-auto d-block " alt="Bonus13">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        WP In-Content Popup Pro
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li><b>WP In-Content Popup Pro is a new plugin that lets you create attention grabbing popups within your content.</b></li>
                           <li>You can trigger in-content video popups, image popups, text popups, or content popups which you can use to showcase your product, article or even your profile.  Additionally, you can add a secondary content popup that can contain optin forms, buy buttons, or social sharing icons.This will help boost your traffic, sales and email lists. </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #11 End -->
      <!-- Bonus #12 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 12</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus12.webp" class="img-fluid mx-auto d-block " alt="Bonus12">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Sales Funnel Optimization Strategies Video Upgrade
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>Whether you make your money through ad clicks using the Adsense monetization platform or you sell affiliate products or your own services or you run your own online drop shipping store, you're trying to convert people from simple clickers of links and readers of your content to cold hard cash.</b></li>
                           <li>The sales funnel model helps you craft together working strategies that would help you turn your content and traffic into cash. </li>
                           <li>This video course teaches you how to optimize your sales funnel. It will show you key strategies that will help you maximize conversions and thereby maximize your profits.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #12 End -->
      <!-- Bonus #13 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 13</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus13.webp" class="img-fluid mx-auto d-block " alt="Bonus13">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Introduction to Postcard Marketing
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                          
                           <li>The Introduction to Postcard Marketing Video Course will provide you with a basic overview of Postcard marketing. You will receive three straight to the point lessons that will teach you exactly what it is, how it works and how you can use to effectively use it to market your own profitable business.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #13 End -->
      <!-- Bonus #14 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 14</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus14.webp" class="img-fluid mx-auto d-block " alt="Bonus14">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Buying Traffic to Generate Massive Website Visitors
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                       

                           <li>An intrinsic understanding of your competition and how to better them is the most important component of any digital marketing strategy. Today, there is such small distances between you and your competition - it's unlikely that you're offering anything that cannot be bought on some other website. </li>
                           <li>Now, when a consumer wants and needs a product or information, all they must do is input their desires into the Google search and peruse the search engine results page.</li>
                           <li>Standing out in the search results is by far the most important part of driving traffic to your site. In this ebook, you will learn about the main and most popular techniques to drive traffic to your website.</li>
                          
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #14 End -->
      <!-- Bonus #15 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 15</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus15.webp" class="img-fluid mx-auto d-block " alt="Bonus15">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Easy Survey Generator
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>It’s the software your competitors don’t want you to know! Who Else Wants To Discover The Ultimate Secret For Getting Into Your Prospect’s Heads And Boosting Your Chances For Riches! </b> </li>
                           <li>Knowing the right information about what are the wants of your audience is really a wise strategy to boost your conversion rate and, of course, a lot of profits to your online business.</li>
                           <li>Now the question is that how are you going to do that? Well inside this amazing product is a software that will create an interactive survey to your blog readers or web traffic.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #15 End -->
      <!-- CTA Btn Start-->
      <div class="dark-cta-sec">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-md-12 col-12 text-center mt20 mt-md20">
                  <div class="f-md-24 f-20 text-center mt3 black white-clr lh120 w700">Don't wait for the time when I will pull these bonuses away…</div>
                  <div class="f-md-36 f-28 text-center  lh120 w700 mt20 mt-md20 white-clr">TAKE ACTION NOW!</div>
                  <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr">
                     Use Coupon Code <span class="w700 yellow-clr">"AMITNEXUS"</span> for an Additional <span class="w700 yellow-clr">$3 Discount</span> on NexusGPT
                  </div>
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn px-md80">
                  <span class="text-center">Grab NexusGPT + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 white-clr mt-md30">
                  Use Coupon Code <span class="w700 yellow-clr">"NEXUSGPT"</span> for an Additional <span class="w700 yellow-clr">$50 Discount</span> on Bundle Deal
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflinkbundle']; ?>" class="text-center bonusbuy-btn px-md80">
                  <span class="text-center">Grab NexusGPT Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-md-10 mx-auto col-md-12 col-12 mt15 mt-md40">
                  <img src="assets/images/payment.webp" class="img-fluid mx-auto d-block" alt="Payment">
               </div>
               <div class="col-12 mt15 mt-md40" align="center">
                  <h3 class="f-md-22 f-20 w500 text-center black white-clr">Coupon Is Expiring In... </h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 mx-auto col-md-10 col-12 text-center">
                  <div class="countdown counter-white white-clr">
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 smmltd">Days</span>
                     </div>
                     <div class="timer-label text-center">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Hours</span> 
                     </div>
                     <div class="timer-label text-center timer-mrgn">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Mins</span> 
                     </div>
                     <div class="timer-label text-center ">
                        <span class="f-31 f-md-60 timerbg oswald">00</span>
                        <br>
                        <span class="f-14 f-md-18 w500 smmltd">Sec</span>
                     </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Btn End -->
       <!-- Bonus #16 start -->
       <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 16</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus16.webp" class="img-fluid mx-auto d-block " alt="Bonus16">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Youtube Success Step By Step
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li>
                           <b>YouTube Success Step By Step is a new powerful report that explains to the reader how they can create a successful YouTube channel in the fastest possible time. </b>Readers of the report will learn everything that they need to know about choosing the right subject for their YouTube channel and publishing the most engaging video content on it.
                        </li>
                        <li>Readers will learn that they must choose the right subject or niche for their YouTube channel. They will understand why the theme of their channel is so important and how to make the right decision about this based on the needs of their audience. There are examples of the best video content to create for a successful channel.
                        </li>
                       
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #16 End -->
      <!-- Bonus #17 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 17</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus17.webp" class="img-fluid mx-auto d-block " alt="Bonus12">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Virtual Networking Success
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li><b>Networking when you are not in the office can be hard, but it may be one of the best ways to grow your business and make sure that you meet new people.</b></li>
                        <li>Virtual networking allows this to happen, whether you are in the office or work from anywhere.</li>
                        <li>Through virtual networking, you will be able to take advantage of online platforms, social media, and even online webinars and conferences in order to form connections with others, even when you are not able to meet them in-person.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #17 End -->
      <!-- Bonus #18 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 18</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus18.webp" class="img-fluid mx-auto d-block " alt="Bonus13">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Power Of Visualization Video Upgrade
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>Attracting your best life can be challenging. You are bound to face obstacles such as fear, failure, and disappointments that will make you feel like a hopeless failure.</b></li>
                           <li>However, it is not impossible.</li>
                           <li>The first step to transforming your life is having a vision.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #18 End -->
      <!-- Bonus #19 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 xstext1">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 19</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 order-md-2 col-12">
                        <img src="assets/images/bonus19.webp" class="img-fluid mx-auto d-block " alt="Bonus14">
                     </div>
                     <div class="col-md-7 order-md-1 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Blogging Traffic Mantra
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                        <li> <b>Making a living as a blogger has to be one of the sweetest gigs out there. As a blogger, you’ll be able to earn passive income which means that your money will flow in even as you’re sleeping, travelling or relaxing with friends.</b></li>
                           <li>You’re no long trading time for income and this is the point you need to get to if you want to really be free and financially independent.</li>
                           <li>This is all about working hard and smart now so that you can reap the benefits later. Too many people approach blogging in the wrong way, thinking that they can just write a few posts on a semi-regular basis and that that will be enough to ensure their success.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #19 End -->
      <!-- Bonus #20 Start -->
      <div class="section-bonus">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <div class="bonus-title-bg">
                     <div class="f-22 f-md-28 lh120 w700">BONUS 20</div>
                  </div>
               </div>
               <div class="col-12 mt20 mt-md30 d-flex align-content-center flex-wrap">
                  <div class="row">
                     <div class="col-md-5 col-12">
                        <img src="assets/images/bonus20.webp" class="img-fluid mx-auto d-block " alt="Bonus15">
                     </div>
                     <div class="col-md-7 col-12 mt20 mt-md0">
                        <div class="f-22 f-md-32 w700 lh140 bonus-title-color text-capitalize">
                        Content Marketing Formula
                        </div>
                        <ul class="bonus-list f-18 f-md-20 w400 lh140 mt20 mt-md30 p0">
                           <li><b>Content marketing is currently one of the biggest trends in digital marketing as a whole and is an area that many website owners and brands are investing in heavily right now thanks to the impressive returns that they are seeing.</b> </li>
                           <li>IBut content marketing is a complex and broad term that encompasses a number of different strategies and activities. </li>
                           <li>In order for it to be successful, you need to have a good understanding of what it is, how it works and how you can best adapt it to work for your particular brand.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Bonus #20 End -->
      <!-- Huge Woth Section Start -->
      <div class="huge-area mt30 mt-md10">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="f-md-65 f-40 lh120 w700 white-clr">That's Huge Worth of</div>
                  <br>
                  <div class="f-md-60 f-40 lh120 w800 yellow-clr">$3300!</div>
               </div>
            </div>
         </div>
      </div>
      <!-- Huge Worth Section End -->
      <!-- text Area Start -->
      <div class="white-section pb0">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-12 text-center">
                  <div class="f-md-36 f-25 lh140 w500">So what are you waiting for? You have a great opportunity <br class="hidden-xs"> ahead + <span class="w700">My 20 Bonus Products</span> are making it a <br class="hidden-xs"> <span class="w700">completely NO Brainer!!</span></div>
               </div>
            </div>
         </div>
      </div>
      <!-- text Area End -->
      <!-- CTA Button Section Start -->
      <div class="cta-btn-section">
         <div class="container">
            <div class="row">
               <div class="col-12 f-16 f-md-20 lh150 w500 text-center black-clr">
                  Use Coupon Code <span class="w700 yellow-clr">"AMITNEXUS"</span> for an Additional <span class="w700 yellow-clr">$3 Discount</span> on NexusGPT
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflink']; ?>" class="text-center bonusbuy-btn px-md80">
                  Grab NexusGPT + My 20 Exclusive Bonuses
                  </a>
               </div>
               <div class="f-16 f-md-20 lh150 w400 text-center mt20 black-clr mt-md30">
                  Use Coupon Code <span class="w700 yellow-clr">"NEXUSGPT"</span> for an Additional <span class="w700 yellow-clr">$50 Discount</span> on Bundle Deal
               </div>
               <div class="col-md-12 col-md-offset-0 col-md-10 col-md-offset-1 col-12 col-offset-0 text-center mt15 mt-md20">
               <a href="<?php echo $_GET['afflinkbundle']; ?>" class="text-center bonusbuy-btn px-md80">
                  <span class="text-center">Grab NexusGPT Bundle + My 20 Exclusive Bonuses</span>
                  </a>
               </div>
               <div class="col-12 mt15 mt-md20" align="center">
                  <h3 class="f-md-28 f-20 w500 text-center black">My Exclusive Bonuses Are Expiring in...</h3>
               </div>
               <!-- Timer -->
               <div class="col-md-8 col-md-10 mx-auto col-12 text-center">
                  <div class="countdown counter-black">
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd ">Days</span> </div>
                     <div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>
                     <div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">00&nbsp;&nbsp;:&nbsp;&nbsp;</span><br><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>
                     <div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">00</span><br><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>
                  </div>
               </div>
               <!-- Timer End -->
            </div>
         </div>
      </div>
      <!-- CTA Button Section End -->
      <!-- Footer Section Start -->
      <div class="footer-section">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1073.21 250" style="max-height:65px;">
                     <defs>
                        <style>
                           .cls-1 {
                           fill: #fff;
                           }

                           .cls-2 {
                           fill-rule: evenodd;
                           }

                           .cls-2, .cls-3 {
                           fill: #00a4ff;
                           }

                           .cls-4 {
                           fill: #f19b10;
                           }
                        </style>
                     </defs>
                     <g id="Layer_1-2" data-name="Layer 1">
                        <g>
                           <g>
                              <g>
                                 <path class="cls-1" d="m872.38,109.17c-3.33-7.67-8.42-13.69-15.25-18.08-6.83-4.39-14.86-6.58-24.08-6.58-8.67,0-16.45,2-23.33,6-6.89,4-12.33,9.7-16.33,17.08-4,7.39-6,15.97-6,25.75s2,18.39,6,25.83c4,7.45,9.44,13.17,16.33,17.17,6.89,4,14.67,6,23.33,6,8.11,0,15.42-1.75,21.92-5.25s11.72-8.5,15.67-15c3.94-6.5,6.19-14.08,6.75-22.75h-49v-9.5h61.33v8.5c-.56,10.22-3.33,19.47-8.33,27.75-5,8.28-11.7,14.81-20.08,19.58-8.39,4.78-17.81,7.17-28.25,7.17s-20.56-2.53-29.33-7.58c-8.78-5.05-15.69-12.11-20.75-21.17-5.06-9.05-7.58-19.3-7.58-30.75s2.53-21.69,7.58-30.75c5.05-9.05,11.97-16.11,20.75-21.17,8.78-5.05,18.55-7.58,29.33-7.58,12.44,0,23.33,3.11,32.67,9.33,9.33,6.22,16.11,14.89,20.33,26h-13.67Z"></path>
                                 <path class="cls-1" d="m975.71,132.83c-6.56,6.11-16.39,9.17-29.5,9.17h-23.5v49.67h-11.67v-116.5h35.17c13,0,22.8,3.06,29.42,9.17,6.61,6.11,9.92,14.22,9.92,24.33s-3.28,18.06-9.83,24.17Zm-2.17-24.17c0-7.78-2.17-13.67-6.5-17.67-4.33-4-11.28-6-20.83-6h-23.5v47h23.5c18.22,0,27.33-7.78,27.33-23.33Z"></path>
                                 <path class="cls-1" d="m1073.21,75.17v9.67h-32.33v106.83h-11.67v-106.83h-32.5v-9.67h76.5Z"></path>
                              </g>
                              <g>
                                 <path class="cls-1" d="m322.77,192.08h-28.5l-47.67-72.17v72.17h-28.5v-117h28.5l47.67,72.5v-72.5h28.5v117Z"></path>
                                 <path class="cls-1" d="m505.27,192.08l-23.83-35.83-21,35.83h-32.33l37.5-59.5-38.33-57.5h33.17l23.5,35.33,20.67-35.33h32.33l-37.17,59,38.67,58h-33.17Z"></path>
                                 <path class="cls-1" d="m580.77,75.08v70c0,7,1.72,12.39,5.17,16.17,3.44,3.78,8.5,5.67,15.17,5.67s11.78-1.89,15.33-5.67c3.55-3.78,5.33-9.17,5.33-16.17v-70h28.5v69.83c0,10.45-2.22,19.28-6.67,26.5-4.45,7.22-10.42,12.67-17.92,16.33-7.5,3.67-15.86,5.5-25.08,5.5s-17.47-1.8-24.75-5.42c-7.28-3.61-13.03-9.05-17.25-16.33-4.22-7.28-6.33-16.14-6.33-26.58v-69.83h28.5Z"></path>
                                 <path class="cls-1" d="m689.6,189.08c-6.78-2.78-12.2-6.89-16.25-12.33-4.06-5.44-6.2-12-6.42-19.67h30.33c.44,4.33,1.94,7.64,4.5,9.92,2.55,2.28,5.89,3.42,10,3.42s7.55-.97,10-2.92c2.44-1.94,3.67-4.64,3.67-8.08,0-2.89-.97-5.28-2.92-7.17-1.95-1.89-4.33-3.44-7.17-4.67-2.83-1.22-6.86-2.61-12.08-4.17-7.56-2.33-13.72-4.67-18.5-7-4.78-2.33-8.89-5.78-12.33-10.33-3.45-4.55-5.17-10.5-5.17-17.83,0-10.89,3.94-19.42,11.83-25.58,7.89-6.17,18.17-9.25,30.83-9.25s23.28,3.08,31.17,9.25c7.89,6.17,12.11,14.75,12.67,25.75h-30.83c-.22-3.78-1.61-6.75-4.17-8.92-2.56-2.17-5.83-3.25-9.83-3.25-3.45,0-6.22.92-8.33,2.75-2.11,1.83-3.17,4.47-3.17,7.92,0,3.78,1.78,6.72,5.33,8.83,3.55,2.11,9.11,4.39,16.67,6.83,7.55,2.56,13.69,5,18.42,7.33,4.72,2.33,8.8,5.72,12.25,10.17,3.44,4.45,5.17,10.17,5.17,17.17s-1.7,12.72-5.08,18.17c-3.39,5.45-8.31,9.78-14.75,13-6.45,3.22-14.06,4.83-22.83,4.83s-16.22-1.39-23-4.17Z"></path>
                                 <rect class="cls-1" x="343.41" y="75.29" width="71.69" height="22.79"></rect>
                                 <rect class="cls-1" x="343.41" y="121.79" width="71.69" height="22.79"></rect>
                                 <rect class="cls-1" x="343.41" y="169.5" width="71.69" height="22.79"></rect>
                              </g>
                           </g>
                           <g>
                              <path class="cls-3" d="m76.03.47L13.55,16.04C5.96,17.93,0,28.08,0,38.78v147.74c0,.3,0,.61,0,.91v23.78c0,10.7,5.96,20.86,13.55,22.75l62.48,15.56c10.52,2.63,19.39-5.93,19.39-19.22v-112.1c0-.25,0-.5,0-.75V19.7c0-13.29-8.88-21.85-19.4-19.22Zm-34.9,227.64c-2.77-1.31-4.37-4.32-4.37-7.13s1.61-5.83,4.37-7.13c4.4-2.08,10.41.47,10.41,7.13s-6.01,9.22-10.41,7.13ZM54.19,23.79l-19.12,4.39c-1.93.44-3.86-.76-4.3-2.69-.06-.27-.09-.54-.09-.81,0-1.63,1.12-3.11,2.78-3.49l19.13-4.39c1.92-.45,3.85.76,4.29,2.69.07.27.09.54.09.81,0,1.63-1.12,3.11-2.78,3.49Z"></path>
                              <path class="cls-4" d="m166.8,83.52l-44.46-11.07c-7.48-1.87-13.8,4.23-13.8,13.68v149.86c0,9.46,6.32,15.55,13.8,13.68l44.46-11.07c5.41-1.35,9.64-8.57,9.64-16.18v-122.7c0-7.61-4.23-14.84-9.64-16.18Zm-19.62,150.91c-3.13,1.47-7.41-.34-7.41-5.08s4.27-6.56,7.41-5.08c1.97.93,3.11,3.08,3.11,5.08s-1.14,4.14-3.11,5.08Zm6.79-144.13c-.33,1.31-1.65,2.11-2.96,1.79l-12.66-2.64c-1.5-.42-2.2-1.68-2.2-2.85,0-.23.03-.46.08-.68.32-1.32,1.65-2.12,2.96-1.79l12.98,3.21c1.12.28,1.87,1.27,1.87,2.37,0,.2-.03.39-.08.59Z"></path>
                              <g>
                                 <path class="cls-2" d="m163.94,67.4h0c-.95,1.77-2.97,2.67-4.92,2.18-1.95-.48-3.32-2.22-3.34-4.23.96-3.86,1.37-7.85,1.22-11.85-.25-7.01-2.24-14.05-6.12-20.47-.17-.27-.33-.54-.51-.81-.25-.41-.52-.81-.79-1.2-.38-.57-.78-1.12-1.19-1.66-.09-.12-.18-.23-.27-.35-.22-.28-.43-.55-.66-.82-2.05-2.55-4.37-4.81-6.87-6.76-.3-.24-.6-.48-.91-.69-.43-.32-.85-.62-1.28-.92-1.09-.75-2.21-1.44-3.35-2.07-3.35-1.87-6.9-3.26-10.53-4.15-3.93-.97-7.96-1.37-11.97-1.21-1.96-.42-3.39-2.12-3.47-4.12v-.18c0-1.93,1.26-3.64,3.12-4.21,9.06-.36,18.22,1.67,26.45,6.12.54.29,1.08.59,1.62.91.36.21.71.43,1.06.64.35.22.7.44,1.05.67,2.31,1.53,4.53,3.25,6.63,5.19,1.68,1.56,3.25,3.2,4.68,4.92.22.27.43.54.65.81.2.25.4.5.59.75.33.43.65.86.96,1.29.25.34.49.69.73,1.04.24.35.47.71.7,1.06.23.36.46.72.67,1.07.16.25.31.51.46.77.36.61.71,1.22,1.03,1.84.32.59.62,1.19.92,1.8.18.38.36.75.53,1.13.17.38.34.76.5,1.14.17.38.32.77.48,1.15.27.68.52,1.35.76,2.03,3.37,9.57,3.71,19.71,1.38,29.18Z"></path>
                                 <g>
                                    <path class="cls-2" d="m116.46,56.27c-.03-.25-.08-.5-.16-.74,0-.02,0-.02,0-.04-.02-.07-.04-.13-.07-.18,0-.05-.03-.1-.05-.14,0,0,0-.03-.02-.03,0,0,0,0,0-.02-.04-.11-.09-.2-.15-.3,0-.03-.02-.04-.03-.07-.05-.09-.12-.19-.18-.28,0-.02-.02-.05-.04-.07-.03-.04-.06-.09-.09-.13-.04-.04-.08-.08-.12-.12,0,0,0-.02-.03-.03,0,0-.02-.03-.03-.04-.08-.08-.15-.16-.23-.23-.02-.03-.05-.05-.07-.08-.06-.05-.12-.1-.18-.15,0,0-.02-.02-.03-.03l-.3-.2s-.04-.03-.06-.04c-.04-.03-.09-.05-.13-.08-.04-.02-.08-.03-.13-.05h0s0-.02,0-.02c-.02,0-.05-.03-.07-.03-.08-.04-.18-.08-.27-.12-.02,0-.03,0-.04,0-.04-.02-.09-.03-.13-.04-.06-.02-.11-.02-.17-.04s-.12-.03-.19-.05c-.03-.02-.08-.03-.12-.03h-.02l-.18-.03s-.1-.02-.15-.02c-.07,0-.13,0-.19,0h-.54c-.13.02-.25.03-.38.07-.07,0-.16.03-.23.05-.03,0-.05.02-.08.03-.02,0-.06.02-.09.02h-.02c-.11.04-.21.08-.32.13-.02,0-.04.02-.06.03,0,0-.02,0-.02,0-.09.04-.18.09-.28.14h0c-.11.07-.21.13-.31.2-.02,0-.02,0-.03.03-.04.02-.08.05-.13.08,0,0-.02.02-.03.03-.04.03-.08.06-.13.11,0,0-.02,0-.03.03-.05.03-.09.07-.13.12-.03.03-.07.06-.09.09-.02.02-.04.04-.07.07-.03.02-.05.05-.08.08-.07.08-.13.17-.2.25-.05.07-.1.15-.15.23-.02.02-.03.03-.03.05-.02.02-.03.06-.05.08-.03.05-.06.11-.08.17-.03.03-.03.04-.03.07-.04.08-.08.16-.11.23,0,0,0,.02,0,.03-.03.07-.07.16-.08.23-.02.04-.03.07-.04.12-.04.12-.07.25-.09.38,0,.07-.02.13-.02.21,0,0,0,.03,0,.04,0,.02,0,.03,0,.04,0,.05-.02.11,0,.16-.03.2-.02.4,0,.6,0,.05,0,.09,0,.13,0,.03,0,.08.02.11,0,.03.02.07.03.1,0,.1.02.19.05.28,0,.03.02.04.02.07.03.12.08.25.12.37.04.09.08.17.13.27,0,.02,0,.03.02.04.02.04.04.09.07.13,0,.02.02.03.03.06,0,.02.03.05.05.07,0,0,.02.03.03.04s.02.04.04.06c.03.07.07.12.12.18.02.03.03.05.06.07,0,0,0,.02,0,.02.04.06.08.11.13.16.03.05.08.09.12.13.02.03.04.04.06.07,0,0,.02.02.03.03.03.03.05.05.08.07.06.06.11.11.17.16.03.03.06.05.08.07.02.03.05.04.07.06.08.06.15.11.23.15.06.04.13.08.19.12,0,0,0,0,0,0,.06.03.11.07.17.09l.18.08s.06.03.08.04c.09.03.18.07.28.09.05.03.1.04.15.05.05.02.1.03.16.03.02.02.04.02.07.02.03,0,.08.02.11.02.08.02.17.03.25.04.03,0,.07,0,.1,0,.12.02.24.02.37.02.09,0,.19,0,.29,0,.08,0,.17-.02.26-.03.1-.02.2-.04.3-.07.09-.03.19-.06.29-.09,0,0,.02,0,.04,0h0c.12-.04.22-.09.33-.14.08-.03.15-.07.22-.1.11-.05.21-.11.3-.17,0,0,.02,0,.03-.02.02,0,.02-.02.04-.02.03-.03.08-.05.11-.08,0,0,0,0,.02,0,.02-.02.04-.03.06-.05,0,0,.02,0,.03-.03.1-.07.19-.15.28-.23.02-.02.03-.03.04-.04.09-.09.18-.18.26-.28.1-.12.19-.24.28-.37.02-.03.05-.08.07-.11.04-.05.07-.11.1-.17.02-.03.04-.07.06-.11.13-.26.23-.53.3-.82,0,0,0-.03,0-.04.08-.34.11-.7.08-1.06,0-.07,0-.13,0-.2Z"></path>
                                    <path class="cls-2" d="m147.39,63.31c-.95,1.77-2.97,2.67-4.93,2.18-1.95-.48-3.32-2.22-3.34-4.23.57-2.32.83-4.71.73-7.12,0-.2-.02-.39-.03-.59-.23-4.02-1.43-8.03-3.64-11.72-.96-1.58-2.05-3.03-3.27-4.31-.5-.55-1.03-1.07-1.58-1.54-.1-.1-.21-.19-.31-.28-.3-.27-.61-.53-.92-.77-.14-.11-.28-.22-.42-.31,0-.02,0-.02-.02-.02,0-.02-.02-.02-.03-.02-.16-.13-.32-.25-.48-.36,0,0,0,0,0,0-.16-.12-.32-.23-.48-.33-.12-.11-.26-.19-.39-.28-2.51-1.67-5.23-2.83-8.04-3.52-2.37-.58-4.82-.82-7.24-.71-.19,0-.39,0-.58.02-1.97-.37-3.45-2.02-3.58-4.02-.13-2.01,1.11-3.84,3.01-4.48,5.91-.4,11.91.74,17.38,3.45.04.02.08.03.11.05,0,0,.02,0,.02.02.25.13.49.25.74.38.34.17.68.37,1.02.56.34.2.67.4,1.02.61.23.14.45.28.68.43.44.29.89.59,1.32.9,0,0,.02,0,.02.02.21.15.41.3.61.45l.02.02c.62.48,1.24.98,1.85,1.5.24.21.48.43.72.65.22.2.43.4.64.61.93.89,1.79,1.83,2.59,2.81.27.33.53.66.78.99.17.23.34.46.5.68.17.23.33.46.49.69.21.3.41.6.61.91,0,.03.02.04.03.06.38.59.74,1.19,1.08,1.8.18.32.34.63.51.95.14.28.28.56.42.84.14.28.27.57.4.85.13.28.25.57.37.86,2.9,6.91,3.33,14.38,1.63,21.32Z"></path>
                                 </g>
                                 <path class="cls-2" d="m130.85,59.22c-.95,1.77-2.98,2.67-4.93,2.18-1.94-.48-3.32-2.22-3.34-4.23.19-.75.28-1.53.25-2.32v-.14c0-.15-.02-.3-.03-.45-.12-1.24-.52-2.48-1.2-3.62-.54-.9-1.22-1.65-1.98-2.27-.03-.02-.07-.05-.1-.07-.05-.04-.09-.08-.13-.1-.07-.06-.14-.11-.21-.16,0,0-.02,0-.03-.02-.08-.07-.17-.12-.26-.17-.23-.17-.48-.33-.72-.44-.71-.42-1.46-.7-2.22-.88-.87-.21-1.75-.28-2.62-.22-.06,0-.12.02-.17.03-.14,0-.28.03-.42.04-2-.22-3.59-1.76-3.88-3.74-.28-1.99.81-3.92,2.66-4.69,4.02-.58,8.25.27,11.86,2.65.24.15.48.32.72.49-.08-.06-.17-.12-.25-.17.17.12.35.24.53.38.2.14.38.29.57.45.07.05.15.11.22.18.18.15.36.31.53.47-.1-.1-.19-.19-.29-.28.15.13.3.28.44.41,0,0,.01,0,.01.01,0,0,.01,0,.02.01,1.14,1.05,2.16,2.28,3,3.68,2.45,4.06,3.02,8.72,1.97,12.98Z"></path>
                              </g>
                           </g>
                        </g>
                     </g>
                  </svg>
               </div>
               <div class="col-12 d-flex justify-content-center justify-content-md-between flex-wrap flex-md-nowrap mt20 mt-md40">
                  <div class="f-md-18 f-16 w400 lh140 white-clr text-xs-center">Copyright © NexusGPT 2023</div>
                  <ul class="footer-ul w400 f-md-18 f-16 white-clr text-center text-md-right">
                     <li><a href="https://support.oppyo.com/hc/en-us" class="white-clr t-decoration-none">Contact</a> <span class="px5">|</span> </li>
                     <li><a href="http://getnexusgpt.com/legal/privacy-policy.html " class="white-clr t-decoration-none">Privacy</a> <span class="px5">|</span> </li>
                     <li><a href="http://getnexusgpt.com/legal/terms-of-service.html " class="white-clr t-decoration-none">T&amp;C</a> <span class="px5">|</span> </li>
                     <li><a href="http://getnexusgpt.com/legal/disclaimer.html " class="white-clr t-decoration-none">Disclaimer</a> <span class="px5">|</span> </li>
                     <li><a href="http://getnexusgpt.com/legal/gdpr.html " class="white-clr t-decoration-none">GDPR</a> <span class="px5">|</span> </li>
                     <li><a href="http://getnexusgpt.com/legal/dmca.html " class="white-clr t-decoration-none">DMCA</a> <span class="px5">|</span> </li>
                     <li><a href="http://getnexusgpt.com/legal/anti-spam.html " class="white-clr t-decoration-none">Anti-Spam</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!--Footer Section End -->
      <!-- timer --->
      <?php
         if ($now < $exp_date) {
         ?>
      <script type="text/javascript">
         // Count down milliseconds = server_end - server_now = client_end - client_now
         var server_end = <?php echo $exp_date; ?> * 1000;
         var server_now = <?php echo time(); ?> * 1000;
         var client_now = new Date().getTime();
         var end = server_end - server_now + client_now; // this is the real end time
         
         var noob = $('.countdown').length;
         
         var _second = 1000;
         var _minute = _second * 60;
         var _hour = _minute * 60;
         var _day = _hour * 24
         var timer;
         
         function showRemaining() {
         var now = new Date();
         var distance = end - now;
         if (distance < 0) {
         	clearInterval(timer);
         	document.getElementsByClassName('countdown').innerHTML = 'EXPIRED!';
         	return;
         }
         
         var days = Math.floor(distance / _day);
         var hours = Math.floor((distance % _day) / _hour);
         var minutes = Math.floor((distance % _hour) / _minute);
         var seconds = Math.floor((distance % _minute) / _second);
         if (days < 10) {
         	days = "0" + days;
         }
         if (hours < 10) {
         	hours = "0" + hours;
         }
         if (minutes < 10) {
         	minutes = "0" + minutes;
         }
         if (seconds < 10) {
         	seconds = "0" + seconds;
         }
         var i;
         var countdown = document.getElementsByClassName('countdown');
         for (i = 0; i < noob; i++) {
         	countdown[i].innerHTML = '';
         
         	if (days) {
         		countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + days + '</span><span class="f-14 f-md-18 smmltd">Days</span> </div>';
         	}
         
         	countdown[i].innerHTML += '<div class="timer-label text-center"><span class="f-31 f-md-60 timerbg oswald">' + hours + '</span><span class="f-14 f-md-18 w500 smmltd">Hours</span> </div>';
         
         	countdown[i].innerHTML += '<div class="timer-label text-center timer-mrgn"><span class="f-31 f-md-60 timerbg oswald">' + minutes + '</span><span class="f-14 f-md-18 w500 smmltd">Mins</span> </div>';
         
         	countdown[i].innerHTML += '<div class="timer-label text-center "><span class="f-31 f-md-60 timerbg oswald">' + seconds + '</span><span class="f-14 f-md-18 w500 smmltd">Sec</span> </div>';
         }
         
         }
         timer = setInterval(showRemaining, 1000);
         	
      </script>
      <?php
         } else {
         echo "Times Up";
         }
         ?>
      <!--- timer end-->
   </body>
</html>